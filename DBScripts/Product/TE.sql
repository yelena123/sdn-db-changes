-- DBTicket TE-1339
update label set value='                                              Shipments are subject to charges for actual or dimensional weight in accordance with the Agreement.UPS -MI reserves the right to  institute a  fuel  surcharge  on some or  all shipments without prior notice.  The fuel surcharge will be applied to Services and for such periods as UPS-MI; in its sole discretion, may determine necessary. The current fuel surcharge is described at'  
where key='ShipmentsAreSubjectTo1';

update label set value=' Client agrees to make payment for Services as provided in the Service Terms and Conditions.'  
where key='ShipmentsAreSubjectTo2';

update label set value='                                                                    THE SERVICES ARE PROVIDED BY UPS -MI "AS IS", WITHOUT WARRANTY OF ANY KIND. WITHOUT LIMIATIONS OF THE FOREGOING, UPS -MI DOES NOT WARRANT THAT THE SERVICES BEING PROVIDED BY UPS -MI HEREUNDER WILL BE PROVIDED FREE OF OMISSIONS, ERRORS, DELAYS OR INTERRUPTIONS.'   
where key='TheServicesProvided';

commit;

-- DBTicket TE-1353
update label set value = 'CAPITALIZED TERMS USED HEREIN AND NOT OTHERWISE DEFINED SHALL HAVE THE MEANINGS SET FORTH ON EXHIBIT A OF THE SERVICE TERMS AND CONDITIONS .' 
where key='TheServicesToBe3';

update label set value='PUNITIVE, INDIRECT OR INCIDENTAL DAMAGES, WEHTER ARISING IN CONTRACT, TORT OR OTHERWISE, EVEN IF UP S-MI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DEMAGES. ' 
where key='UPSMIEntireLiability4';

update label set value=' Shipments are subject to charges for actual or dimensional weight in accordance with the Agreement.UPS -MI reserves the right to institute a fuel surcharge on some or all shipments without prior notice. The fuel surcharge will be applied to Services and for such periods as UPS-MI; in its sole discretion, may determine necessary. The current fuel surcharge is described at'
where key='ShipmentsAreSubjectTo1';

update label set value=' THE SERVICES ARE PROVIDED BY UPS -MI "AS IS", WITHOUT WARRANTY OF ANY KIND. WITHOUT LIMIATIONS OF THE FOREGOING, UPS -MI DOES NOT WARRANT THAT THE SERVICES BEING PROVIDED BY UPS -MI HEREUNDER WILL BE PROVIDED FREE OF OMISSIONS, ERRORS, DELAYS OR INTERRUPTIONS.' 
where key='TheServicesProvided' ;

update label set value=' Client agrees to make payment for Services as provided in the Service Terms and Conditions.' 
where key ='ShipmentsAreSubjectTo2';

commit;

-- DBTicket MACR00819463
MERGE INTO PARAM_DEF P
	USING (SELECT 'assgnLPNtoShpmntUsgStpRte' PARAM_DEF_ID, 'COM_TEPE' PARAM_GROUP_ID
		FROM DUAL) D
	ON (P.PARAM_DEF_ID = D.PARAM_DEF_ID AND P.PARAM_GROUP_ID = D.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
insert (Param_Def_Id,Param_Group_Id,Param_Subgroup_Id,Param_Name,Description,Is_Disabled,Is_Required,Data_Type,Ui_Type,Min_Value,Is_Min_Inc,Max_Value,Is_Max_Inc,Dflt_Value,Display_Order,Value_Generator_Class,Is_Localizable,Param_Category,Carr_Dflt_Value,Bp_Dflt_Value,Param_Validator_Class)
VALUES('assgnLPNtoShpmntUsgStpRte','COM_TEPE','SROUTE','Assign LPN to shipment based on stop''s route','Assign LPN to shipment based on stop''s route',0,0,6,'CHECKBOX',null,0,null,0,'false',254,null,1,null,null,null,null);

commit;

-- DBTicket TE-343
MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'UPSMailInnovations' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UPSMailInnovations',
               'UPS Mail Innovations',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'SM' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'SM',
               ' SM',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ShipmentFrom' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ShipmentFrom',
               'Shipment From',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'AccountNumber' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'AccountNumber',
               'Account Number',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'StreetAddress' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'StreetAddress',
               'Street Address',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'PhoneNumber' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'PhoneNumber',
               'Phone Number',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ShipmentControl' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ShipmentControl',
               'Shipment Control #',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ProductOrServices' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ProductOrServices',
               '1. Product or Services (Please check all that apply)',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'BPM' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'BPM',
               'BPM',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'LightweightParcels' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LightweightParcels',
               'Lightweight Parcels',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'MediaMail' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'MediaMail',
               'Media Mail',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'MailLogicExpedited' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'MailLogicExpedited',
               'Mail Logic Expedited',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'MailLogicSAVEROrigin' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'MailLogicSAVEROrigin',
               'Mail Logic SAVER Origin',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'PieceCount' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'PieceCount',
               'Piece Count',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'CostCenterNameNumber' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'CostCenterNameNumber',
               'Cost Center Name/Number',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'SpecialInstructions' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'SpecialInstructions',
               'Special Instructions',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME,
                   'UPSMailInnovationsDriverCourier (Internal Use Only)' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UPSMailInnovationsDriverCourier (Internal Use Only)',
               'UPS Mail Innovations Driver/Courier (Internal Use Only)',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ReceivingMailTerminal' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ReceivingMailTerminal',
               'Receiving Mail Terminal (Internal Use Only)',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'CustomerSignatureRequired' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'CustomerSignatureRequired',
               'Customer Signature Required',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'PickupAgentSignature' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'PickupAgentSignature',
               'Pickup Agent''s Signature',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'PrintName' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'PrintName',
               'Print Name',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Containers' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Containers',
               'Containers',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'WeightlLbs' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'WeightlLbs',
               'Weight (lbs.)',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'CustomerSignature' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'CustomerSignature',
               'Customer Signature',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'PickupAgentCompanyName' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'PickupAgentCompanyName',
               'Pickup Agent Company Name',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'MailType' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'MailType',
               'Mail Type',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'MailLogicPriority' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'MailLogicPriority',
               'Mail Logic Priority',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'MailLogicSAVEREconomy' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'MailLogicSAVEREconomy',
               'Mail Logic SAVER Economy',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Letter' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Letter',
               'Letter',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Packets' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Packets',
               'Packets',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'USDomesticServices' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'USDomesticServices',
               'US Domestic Services',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME,
                   'AdditionalInternationalServices' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'AdditionalInternationalServices',
               'Additional International Services',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'InternationalServices' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'InternationalServices',
               'International Services',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'PieceWeight' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'PieceWeight',
               'Piece Weight (If Identical)',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'TotalWeightShipment' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'TotalWeightShipment',
               'Total Weight of Shipment',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'TotalMailPieces' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'TotalMailPieces',
               '* Total Mail Pieces',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'CostCenter' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'CostCenter',
               '3. Cost Center',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'NumberOfContainers' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'NumberOfContainers',
               'Number of Containers',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'AddedServices' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'AddedServices',
               'Added Services',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'USPSServices' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'USPSServices',
               'USPS Services',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'CanadaPostServices' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'CanadaPostServices',
               'Canada Post Services',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Polybagging' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Polybagging',
               'Polybagging',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'AddressingLABEL1ing' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'AddressingLABEL1ing',
               'Addressing/LABEL1ing',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Sealing' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Sealing',
               'Sealing',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ForAdditionalCharge' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ForAdditionalCharge',
               '*For an additional charge',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'IPA' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'IPA',
               'IPA',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ISAL' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ISAL',
               'ISAL',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Lettermail' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Lettermail',
               'Lettermail',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Admail' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Admail',
               'Admail',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Parcel' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Parcel',
               'Parcel',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Oz' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Oz',
               '(oz.)',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Lbs' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Lbs',
               '(lbs.)',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'BoxCarton' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'BoxCarton',
               'Box/Carton',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Hampers' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Hampers',
               'Hampers',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Gaylord' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Gaylord',
               'Gaylord',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'APC' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'APC',
               'APC',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'MailTubs' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'MailTubs',
               'MailTubs',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'UPSMailInnovationBags' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UPSMailInnovationBags',
               'UPS Mail Innovation Bags',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'SkidsPallets' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'SkidsPallets',
               'Skids/Pallets',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ShipmentInformation' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ShipmentInformation',
               '2. Shipment Information',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'IfPieceCount' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'IfPieceCount',
                 '* If Piece Count is provided, a 5% variance is accepted by the USPS',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ShipmentMadePursaunt' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'ShipmentMadePursaunt',
                 'SHIPMENT IS MADE PURSUANT UNDER THE CONDITIONS SET FORTH ON THIS PAGE AND THE REVERSE HEREOF. I (on behalf of Client) have read, understand and agree to those terms and conditions. I understand and agree that in the event of loss of, damage to, or delay of shipment, UPS-MI''s liability is limited as set forth on the REVERSE HEREOF.',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'UPSMiOperationsCenter' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UPSMiOperationsCenter',
               'UPS MI OPERATIONS CENTER',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ConditionOfContract' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ConditionOfContract',
               'CONDITIONS OF CONTRACT',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'TermsAndConditions' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'TermsAndConditions',
               'Terms and Conditions.',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'UPSMailAdd' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UPSMailAdd',
               'http://www.upsmailinnovations.com.',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ClientResponsibility' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ClientResponsibility',
               'Client Responsibility.',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Charges' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Charges',
               'Charges.',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ByTenderingThisShipment' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'ByTenderingThisShipment',
                 'By tendering this shipment to UPS-MI for transportation, Client agrees to the limitations set forth in the Agreement,and affirms the description of the shipment as recited on this Shipment Control Form. In addition, by tendering this shipment to UPS-MI for transportation, Client acknowledges and c ertifies that such shipment complies with all USPS definitions of Standard Mail, and unless otherwise separated and identified, does contain other types of mail.',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ClientCertification' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ClientCertification',
               'Client Certification.',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'DisclaimerOfWarranties' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'DisclaimerOfWarranties',
               'Disclaimer of Warranties.',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'LimitationsLiability' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LimitationsLiability',
               'Limitations of Liability.',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Indemnification' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Indemnification',
               'Indemnification.',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ClaimsTimeLimitsProcedures' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ClaimsTimeLimitsProcedures',
               'Claims, Time Limits, and Procedures.',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'TheServicesProvided' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'TheServicesProvided',
                 '     THE SERVICES ARE PROVIDED BY UPS -MI "AS IS", WITHOUT WARRANTY OF ANY KIND. WITHOUT LIMIATIONS OF THE FOREGOING, UPS -MI DOES NOT WARRANT THAT THE SERVICES BEING PROVIDED BY UPS -MI HEREUNDER WILL BE PROVIDED FREE OF OMISSIONS, ERRORS, DELAYS OR INTERRUPTIONS.',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ClientShallBeResponsible' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'ClientShallBeResponsible',
                 'Client shall be responsible for fulfilling, preparing, LABEL1ing, addressing and properly describing the contents of this shipment so as to ensure proper transportation with ordinary care in handling.',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'TheServicesToBe1' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'TheServicesToBe1',
                 '  The services to be performed by UPS-MI  hereunder are governed by the terms  and con ditions contained on this Shipment Control Form and the Service Terms and Conditions, which are incorporated herein by this reference. The Service Terms and',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'TheServicesToBe2' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'TheServicesToBe2',
                 'CONDITIONS ARE PUBLISHED ON THE INTERNET AT                                                          THE  SERVICE TERMS  AND  CONDITIONS AND  THE TERMS  AND CONDITIONS CONTAINED ON THIS SHIPMENT CONTROL FORM AS WELL AS ANY WRITING THAT HAS BEEN EXECUTED BY DULY AUTHORIZED REPRESENTATIVES OF EACH OF THE PARTIES HERE TO AND  REFERENCES AND EXPRESSLY INCORPORATES THE SERVICE TERMS AND CONDITIONS SHALL COLLECTIVELY BE  REFERRED TO HEREIN AS THE"Agreement". ',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'TheServicesToBe3' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'TheServicesToBe3',
                 'CAPITALIZED TERMS USED HEREIN AND NOT OTHERWISE DEFINED SHALL HAVE THE MEANINGS SET FORTH ON EXHIBIT A OF THE SERVICE TERMS AND CONDITIONS.',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ShipmentsAreSubjectTo1' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'ShipmentsAreSubjectTo1',
                 'Shipments are subject to charges for actual or dimensional weight in accordance with the Agreement.UPS -MI reserves the right to  institute a  fuel  surcharge  on some or  all shipments without prior notice.  The fuel surcharge will be applied to Services and for such periods as UPS-MI',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ShipmentsAreSubjectTo2' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'ShipmentsAreSubjectTo2',
                 '                                                                                                                                                                                                                                 Client agrees to make payment for Services as provided in the Service Terms and Conditions.',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'UPSMIEntireLiability1' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'UPSMIEntireLiability1',
                 '                  UPS-MI''s entire liability to Client for any and all claims, causes of action, damages, losses, costs, expenses or other liabilities of any nature whatsoever incurred by Client in connection with the Agreement, including, without limitation, attorney''s fees, any liabilities resulting from any omissions, errors, delays or interruptions in the Services and the preparation or delivery of any Pieces being provided by UPS-MI hereunder ("Liabilities"), shall, in each instance ',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'UPSMIEntireLiability2' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'UPSMIEntireLiability2',
                 'AND WITHOUT REGARD TO THE VALUE OF THE CONTENTS OF ANY PIECE(S),BE LIMITED TO THE ACTUAL FEES AND EXPENSES CHARGED BY UPS -MI (EXCLUDING POSTAGE FEES CHARGED) TO CLIENT FOR THE SPECIFIC SERVICE PERFORMED THAT GAVE RISE TO THE LIABILITY. UPS-MI SHALL NOT BE LIABLE FOR FAILURE TO DELIVER ANY PIECES IF SUCH FAILURE IS DUE TO CLIENT''S ERROR, OMISSION OR NEGLIGENCE. UPON DELIVERY TO THE USPS, THE USPS BECOMES THE PARTY RESPONSIBLE FOR CLIENT''S PIECES. UPON DELIVERY TO THE USPS, UPS-MI SHALL HAVE NO ',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'UPSMIEntireLiability3' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'UPSMIEntireLiability3',
                 'liability of any nature whatsoever arising from acts or omissions of the USPS, inc luding, without limitation, liability for any loss, miscarriage, negligent transmission, damage, delay, or failure of deliver y of any Pieces or other matter. NOTWITHSTANDING ANYTHING IN THE AGREEMENT TO THE CONTRARY, IN NO EVENT SHALL UPS -MI BE LIABLE TO CLIENT, CLIENT''S CUSTOMERS OR ANY OTHER THIRD PARTY FOR LOSS OF USE, REVENUE OR PROFITS, BUSINESS INTERRUPTION, OR FOR ANY SPECIAL, CONSEQUENTIAL, EXEMPLARY, ',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'UPSMIEntireLiability4' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'UPSMIEntireLiability4',
                 'PUNITIVE, INDIRECT OR INCIDENTAL DAMAGES, WEHTER ARISING IN CONTRACT, TORT OR OTHERWISE, EVEN IF UP S-MI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ClientWill1' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'ClientWill1',
                 'Client will, at Client''s sole cost and expense, indemnify and hold UPS -MI, its affiliates and their respective officers, directors, employees, agents, successors, and assigns harmless from and against any and all losses, damages, judgments, costs and expenses, including attorney''s fees, arising out of or related to any third party claim based on personal injury, damage to tangible property or otherwise arising from the Services per formed, ',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ClientWill2' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'ClientWill2',
                 'caused by the actions of Client , its affiliates and/or their respective officers, directors ,employees, agents, successors, and assigns.',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ExceptAsSetForthBelow1' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'ExceptAsSetForthBelow1',
                 'Except as set forth below, receipt by the consignee of the shipment with out written notification of damage on the delivery receipt shall be prima facie evidence that shipment has been delivered in good condition. All claims, including claims for overcharges, must be made in writing to UPS-MI within 120 days after the date of receipt of the shipment by UPS-MI. Damage to and/or loss of the ',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ExceptAsSetForthBelow2' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'ExceptAsSetForthBelow2',
                 'shipment must be reported to UPS -MI in writing within 15 days after delivery of the shipment, with the right of UPS -MI to make inspection of the shipment within 15 days after receipt of such notice. UPS-MI shall not be liable in any action to enforce a claim unless the above procedures have been met by the proper claimant, and unless such action is brought within 6 months of the date UPS -MIaccepted the shipment for transportation.',
                 'Languages');
				 
MERGE INTO MESSAGE_MASTER A
USING (SELECT  '111501604' MSG_ID,
                '111501604' KEY,
                'te' ILS_MODULE,
                'ErrorMessage' BUNDLE_NAME
     FROM DUAL) B
     on ( A.KEY=B.KEY and  A.BUNDLE_NAME=B.BUNDLE_NAME )
     WHEN NOT  MATCHED THEN
     INSERT (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG)
VALUES(SEQ_MESSAGE_MASTER_ID.nextval,'111501604','111501604','te',NULL,'UPS Mail Innovations services require a cost center on the order','ErrorMessage','SYSTEM','ERROR',NULL,'N');

MERGE INTO MESSAGE_DISPLAY MD
USING (SELECT '111501604' DISP_KEY, 'en' TO_LANG, '20' SCREEN_TYPE_ID FROM DUAL) D
ON (MD.DISP_KEY = D.DISP_KEY AND MD.SCREEN_TYPE_ID = D.SCREEN_TYPE_ID AND MD.TO_LANG = D.TO_LANG)
WHEN NOT MATCHED THEN
INSERT(MESSAGE_DISPLAY_ID,DISP_KEY,DISP_VALUE,TO_LANG,SCREEN_TYPE_ID,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM) 
values(SEQ_MESSAGE_DISPLAY_ID.nextval,'111501604','UPS Mail Innovations services require a cost center on the order','en','20','WMADMIN',current_timestamp,'WMADMIN',current_timestamp);

MERGE INTO MESSAGE_MASTER A
USING (SELECT  '111501605' MSG_ID,
                '111501605' KEY,
                'te' ILS_MODULE,
                'ErrorMessage' BUNDLE_NAME
     FROM DUAL) B
     on ( A.KEY=B.KEY and  A.BUNDLE_NAME=B.BUNDLE_NAME )
     WHEN NOT  MATCHED THEN
     INSERT(MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval,'111501605','111501605','te',NULL,'Delivery Confirmation requires a postal endorsement for this service level','ErrorMessage','SYSTEM','ERROR',NULL,'N');

MERGE INTO MESSAGE_DISPLAY MD
USING (SELECT '111501605' DISP_KEY, 'en' TO_LANG, '20' SCREEN_TYPE_ID FROM DUAL) D
ON (MD.DISP_KEY = D.DISP_KEY AND MD.SCREEN_TYPE_ID = D.SCREEN_TYPE_ID AND MD.TO_LANG = D.TO_LANG)
WHEN NOT MATCHED THEN
INSERT(MESSAGE_DISPLAY_ID,DISP_KEY,DISP_VALUE,TO_LANG,SCREEN_TYPE_ID,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM) 
values(SEQ_MESSAGE_DISPLAY_ID.nextval,'111501605','Delivery Confirmation requires a postal endorsement for this service level','en','20','WMADMIN',current_timestamp,'WMADMIN',current_timestamp);

MERGE INTO MESSAGE_MASTER A
USING (SELECT  '111501606' MSG_ID,
                '111501606' KEY,
                'te' ILS_MODULE,
                'ErrorMessage' BUNDLE_NAME
     FROM DUAL) B
     on ( A.KEY=B.KEY and  A.BUNDLE_NAME=B.BUNDLE_NAME )
     WHEN NOT  MATCHED THEN
     INSERT(MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval,'111501606','111501606','te',NULL,'Time Sensitive Material is not allowed with Delivery Confirmation','ErrorMessage','SYSTEM','ERROR',NULL,'N');

MERGE INTO MESSAGE_DISPLAY MD
USING (SELECT '111501606' DISP_KEY, 'en' TO_LANG, '20' SCREEN_TYPE_ID FROM DUAL) D
ON (MD.DISP_KEY = D.DISP_KEY AND MD.SCREEN_TYPE_ID = D.SCREEN_TYPE_ID AND MD.TO_LANG = D.TO_LANG)
WHEN NOT MATCHED THEN
INSERT(MESSAGE_DISPLAY_ID,DISP_KEY,DISP_VALUE,TO_LANG,SCREEN_TYPE_ID,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM) 
values(SEQ_MESSAGE_DISPLAY_ID.nextval,'111501606','Time Sensitive Material is not allowed with Delivery Confirmation','en','20','WMADMIN',current_timestamp,'WMADMIN',current_timestamp);

commit;                 

--changes for MACR00830548
-- DBTicket MACR00830577
merge into param_def pd1
    using (select 'USE_CACHED_PARCEL_RATING' param_def_id, 'COM_TEPE' param_group_id from dual) pd2
    on (pd1.param_def_id=pd2.param_def_id and pd1.param_group_id=pd2.param_group_id)
    when not matched then
INSERT (PARAM_DEF_ID,PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_NAME,DESCRIPTION,IS_DISABLED,IS_REQUIRED,DATA_TYPE,UI_TYPE,MIN_VALUE,IS_MIN_INC,MAX_VALUE,IS_MAX_INC,DFLT_VALUE,DISPLAY_ORDER,VALUE_GENERATOR_CLASS,IS_LOCALIZABLE,PARAM_CATEGORY,CARR_DFLT_VALUE,BP_DFLT_VALUE,PARAM_VALIDATOR_CLASS) 
values ('USE_CACHED_PARCEL_RATING','COM_TEPE',null,'Use cached Parcel Rating for UPS or USPS','Use cached Parcel Rating for UPS or USPS',0,0,6,'CHECKBOX',null,0,null,0,'false',16,null,1,'Planning',null,null,null);

merge into param_def pd1
    using (select 'USE_CUSTOM_PARCEL_TRANSIT_TIME' param_def_id, 'COM_TEPE' param_group_id from dual) pd2
    on (pd1.param_def_id=pd2.param_def_id and pd1.param_group_id=pd2.param_group_id)
    when not matched then
INSERT (PARAM_DEF_ID,PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_NAME,DESCRIPTION,IS_DISABLED,IS_REQUIRED,DATA_TYPE,UI_TYPE,MIN_VALUE,IS_MIN_INC,MAX_VALUE,IS_MAX_INC,DFLT_VALUE,DISPLAY_ORDER,VALUE_GENERATOR_CLASS,IS_LOCALIZABLE,PARAM_CATEGORY,CARR_DFLT_VALUE,BP_DFLT_VALUE,PARAM_VALIDATOR_CLASS) 
values ('USE_CUSTOM_PARCEL_TRANSIT_TIME','COM_TEPE',null,'Use custom transit time for parcel for UPS or USPS','Use custom transit time for parcel for UPS or USPS',0,0,6,'CHECKBOX',null,0,null,0,'false',16,null,1,'Planning',null,null,null);

merge into param_def pd1
    using (select 'USE_FEDEX_SINGLECALL_FOR_TRANSITTIME_AND_RATE' param_def_id, 'COM_TEPE' param_group_id from dual) pd2
    on (pd1.param_def_id=pd2.param_def_id and pd1.param_group_id=pd2.param_group_id)
    when not matched then
INSERT (PARAM_DEF_ID,PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_NAME,DESCRIPTION,IS_DISABLED,IS_REQUIRED,DATA_TYPE,UI_TYPE,MIN_VALUE,IS_MIN_INC,MAX_VALUE,IS_MAX_INC,DFLT_VALUE,DISPLAY_ORDER,VALUE_GENERATOR_CLASS,IS_LOCALIZABLE,PARAM_CATEGORY,CARR_DFLT_VALUE,BP_DFLT_VALUE,PARAM_VALIDATOR_CLASS) 
values ('USE_FEDEX_SINGLECALL_FOR_TRANSITTIME_AND_RATE','COM_TEPE',null,'Use Fedex single call for Transit Time and Rate calculation','Use Fedex single call for Transit Time and Rate calculation',0,0,6,'CHECKBOX',null,0,null,0,'false',16,null,1,'Planning',null,null,null);

commit;

-- DBTicket MACR00858179
MERGE INTO UI_MENU_SCREEN UMS1
     USING (SELECT SCREEN_ID
              FROM UI_MENU_SCREEN
             WHERE SCREEN_ID = 40000031
            UNION
            SELECT 40000031 FROM DUAL) UMS2
        ON (UMS1.SCREEN_ID = UMS2.SCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (UI_MENU_SCREEN_ID,
               MENU_ID,
               SCREEN_ID,
               PERSONALIZATION_LEVEL,
               PERSONALIZATION_ID,
               USER_ID)
       VALUES (UI_MENU_SCREEN_SEQ.NEXTVAL,
               1,
               40000031,
               'SYSTEM',
               NULL,
               NULL);

exec sequpdt ('UI_MENU_ITEM','UI_MENU_ITEM_ID','UI_MENU_ITEM_SEQ');
MERGE INTO UI_MENU_ITEM A
     USING (SELECT 'Save' AS DISPLAY_TEXT,'#{parcelTransitTimeBackingBean.saveAction}' as LINK,(select min(ui_menu_Screen_id) from ui_menu_screen where screen_id='40000031') AS UI_MENU_SCREEN_ID,1 AS MENU_ID FROM DUAL) B
        ON (A.DISPLAY_TEXT = B.DISPLAY_TEXT
            AND A.LINK=B.LINK
            AND A.UI_MENU_SCREEN_ID=B.UI_MENU_SCREEN_ID
            AND A.MENU_ID=B.MENU_ID)
WHEN NOT MATCHED
THEN
   INSERT(UI_MENU_ITEM_ID,UI_MENU_SCREEN_ID,MENU_ID,IS_DISPLAY,DISPLAY_TEXT,LINK,LINK_TYPE,LOCN_FLAG,ITEM_SEQ_NBR,UI_MENU_COMP_ID,IS_MORE,AJAX_ACTION,AJAX_RERENDER,AJAX_ONCOMPLETE,RENDERED,IMAGE_URL) 
   VALUES(ui_menu_item_seq.nextval,(select ui_menu_screen_id from ui_menu_screen where screen_id = '40000031'),1,1,'Save','#{parcelTransitTimeBackingBean.saveAction}',
'JSFA',7,3,'dataForm:page-content_footer-panel',0,null,null,null,'TRUE',null);

----/*MERGE INTO XMENU_ITEM L
----     USING (SELECT 'Tariff' NAME, '/te/parcelRate/ui/ParcelTransitTime.xhtml' NAVIGATION_KEY,'Contract Management' BASE_SECTION_NAME
----              FROM DUAL) B
----        ON (L.NAME = B.NAME AND L.NAVIGATION_KEY = B.NAVIGATION_KEY AND L.BASE_SECTION_NAME=B.BASE_SECTION_NAME)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XMENU_ITEM_ID,
----                        NAME,
----                        SHORT_NAME,
----                        NAVIGATION_KEY,
----                        ICON,
----                        BGCOLOR,
----                        BASE_SECTION_NAME,
----                        BASE_PART_NAME)
----       VALUES ( (SELECT NVL(MAX(XMENU_ITEM_ID), 0) + 1
----                 FROM XMENU_ITEM
----                WHERE TO_CHAR(XMENU_ITEM_ID) LIKE '3600%'),
----             'Tariff',
----             'Tariff',
----             '/te/parcelRate/ui/ParcelTransitTime.xhtml',
----             'default-icon',
----             '#002E5F',
----             'Contract Management',
----             'GENERAL');
----
----INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,
----                             XBASE_MENU_PART_ID,
----                             XMENU_ITEM_ID,
----                             SEQUENCE)
----     VALUES (SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
----               (SELECT XBASE_MENU_PART_ID
----                  FROM XBASE_MENU_PART
----                 WHERE NAME = 'Contract Management'
----                       AND XBASE_MENU_SECTION_ID IN
----                              (SELECT XBASE_MENU_SECTION_ID
----                                 FROM XBASE_MENU_SECTION
----                                WHERE NAME = 'Contract Management')),
----               (Select Xmenu_Item_Id From Xmenu_Item Where Name='Tariff' AND BASE_SECTION_NAME='Contract Management' AND NAVIGATION_KEY='/te/parcelRate/ui/ParcelTransitTime.xhtml'),
----               SEQ_XBASE_MENU_ITEM_ID.NEXTVAL);
----
----MERGE INTO XMENU_ITEM_APP L
----     USING (SELECT  (SELECT XMENU_ITEM_ID
----                  FROM XMENU_ITEM
----                 WHERE     NAME = 'Tariff'
----                       AND BASE_SECTION_NAME = 'Contract Management'
----                       AND BASE_PART_NAME = 'GENERAL'
----                       AND NAVIGATION_KEY='/te/parcelRate/ui/ParcelTransitTime.xhtml') XMENU_ITEM_ID, (SELECT APP_ID
----                  FROM APP
----                 WHERE APP_SHORT_NAME = 'WMS') APP_ID
----              FROM DUAL) B
----        ON (L.XMENU_ITEM_ID = B.XMENU_ITEM_ID AND L.APP_ID = B.APP_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT   (XMENU_ITEM_ID, APP_ID)
----       VALUES (
----               (SELECT XMENU_ITEM_ID
----                  FROM XMENU_ITEM
----                 WHERE     NAME = 'Tariff'
----                       AND BASE_SECTION_NAME = 'Contract Management'
----                       AND BASE_PART_NAME = 'GENERAL'
----                       AND NAVIGATION_KEY='/te/parcelRate/ui/ParcelTransitTime.xhtml'),
----               (SELECT APP_ID
----                  FROM APP
----                 WHERE APP_SHORT_NAME = 'WMS'));
----
----MERGE INTO XMENU_ITEM_PERMISSION L
----     USING (SELECT (SELECT XMENU_ITEM_ID
----                  FROM XMENU_ITEM
----                 WHERE     NAME = 'Tariff'
----                       AND BASE_SECTION_NAME = 'Contract Management'
----                       AND BASE_PART_NAME = 'GENERAL'
----                       AND NAVIGATION_KEY= '/te/parcelRate/ui/ParcelTransitTime.xhtml') XMENU_ITEM_ID,'VPCLTRANSITTIME' PERMISSION_CODE
----              FROM DUAL) B
----        ON (L.XMENU_ITEM_ID = B.XMENU_ITEM_ID AND L.PERMISSION_CODE = B.PERMISSION_CODE)
----WHEN NOT MATCHED
----THEN
----   INSERT  (XMENU_ITEM_ID, PERMISSION_CODE)
----       VALUES (
----               (SELECT XMENU_ITEM_ID
----                  FROM XMENU_ITEM
----                 WHERE     NAME = 'Tariff'
----                       AND BASE_SECTION_NAME = 'Contract Management'
----                       AND BASE_PART_NAME = 'GENERAL'
----                       AND NAVIGATION_KEY= '/te/parcelRate/ui/ParcelTransitTime.xhtml'),
----               'VPCLTRANSITTIME');
----
----MERGE INTO XMENU_ITEM_PERMISSION L
----     USING (SELECT (SELECT XMENU_ITEM_ID
----                  FROM XMENU_ITEM
----                 WHERE     NAME = 'Tariff'
----                       AND BASE_SECTION_NAME = 'Contract Management'
----                       AND BASE_PART_NAME = 'GENERAL'
----                       AND NAVIGATION_KEY= '/te/parcelRate/ui/ParcelTransitTime.xhtml') XMENU_ITEM_ID,'VPCLTRANSITTIME' PERMISSION_CODE
----              FROM DUAL) B
----        ON (L.XMENU_ITEM_ID = B.XMENU_ITEM_ID AND L.PERMISSION_CODE = B.PERMISSION_CODE)
----WHEN NOT MATCHED
----THEN
----   INSERT  (XMENU_ITEM_ID, PERMISSION_CODE)
----       VALUES (
----               (SELECT XMENU_ITEM_ID
----                  FROM XMENU_ITEM
----                 WHERE     NAME = 'Tariff'
----                       AND BASE_SECTION_NAME = 'Contract Management'
----                       AND BASE_PART_NAME = 'GENERAL'
----                       AND NAVIGATION_KEY= '/te/parcelRate/ui/ParcelTransitTime.xhtml'),
----               'APCLTRANSITTIME');*/              

-------------------------------------------------------------------------------------------------
MERGE INTO UI_MENU_SCREEN UMS1
     USING (SELECT SCREEN_ID
              FROM UI_MENU_SCREEN
             WHERE SCREEN_ID = 40000030
            UNION
            SELECT 40000030 FROM DUAL) UMS2
        ON (UMS1.SCREEN_ID = UMS2.SCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (UI_MENU_SCREEN_ID,
               MENU_ID,
               SCREEN_ID,
               PERSONALIZATION_LEVEL,
               PERSONALIZATION_ID,
               USER_ID)
       VALUES (UI_MENU_SCREEN_SEQ.NEXTVAL,
               1,
               40000030,
               'SYSTEM',
               NULL,
               NULL);

MERGE INTO UI_MENU_ITEM A
     USING (SELECT 'Add' AS DISPLAY_TEXT,'AddNewRow(''dataTable'')' as LINK,(select min(ui_menu_Screen_id) from ui_menu_screen where screen_id='40000030') AS UI_MENU_SCREEN_ID,1 AS MENU_ID FROM DUAL) B
        ON (A.DISPLAY_TEXT = B.DISPLAY_TEXT
            AND A.LINK=B.LINK
            AND A.UI_MENU_SCREEN_ID=B.UI_MENU_SCREEN_ID
            AND A.MENU_ID=B.MENU_ID)
WHEN NOT MATCHED
THEN
   INSERT(UI_MENU_ITEM_ID,UI_MENU_SCREEN_ID,MENU_ID,IS_DISPLAY,DISPLAY_TEXT,LINK,LINK_TYPE,LOCN_FLAG,ITEM_SEQ_NBR,UI_MENU_COMP_ID,IS_MORE,AJAX_ACTION,AJAX_RERENDER,AJAX_ONCOMPLETE,RENDERED,IMAGE_URL) 
   VALUES(ui_menu_item_seq.nextval,(select ui_menu_screen_id from ui_menu_screen where screen_id = '40000030'),1,1,'Add','AddNewRow(''dataTable'')',
'JS',7,1,'dataForm:page-content_footer-panel',0,null,null,null,'TRUE',null);

MERGE INTO UI_MENU_ITEM A
     USING (SELECT 'Delete' AS DISPLAY_TEXT,'return navCheck(''parcelZoneRateBackingBean'',''deleteAction'',''dataTable'')' as LINK,(select min(ui_menu_Screen_id) from ui_menu_screen where screen_id='40000030') AS UI_MENU_SCREEN_ID,1 AS MENU_ID FROM DUAL) B
        ON (A.DISPLAY_TEXT = B.DISPLAY_TEXT
            AND A.LINK=B.LINK
            AND A.UI_MENU_SCREEN_ID=B.UI_MENU_SCREEN_ID
            AND A.MENU_ID=B.MENU_ID)
WHEN NOT MATCHED
THEN
   INSERT(UI_MENU_ITEM_ID,UI_MENU_SCREEN_ID,MENU_ID,IS_DISPLAY,DISPLAY_TEXT,LINK,LINK_TYPE,LOCN_FLAG,ITEM_SEQ_NBR,UI_MENU_COMP_ID,IS_MORE,AJAX_ACTION,AJAX_RERENDER,AJAX_ONCOMPLETE,RENDERED,IMAGE_URL)  
   VALUES(ui_menu_item_seq.nextval,(select ui_menu_screen_id from ui_menu_screen where screen_id = '40000030'),1,1,'Delete',
'return navCheck(''parcelZoneRateBackingBean'',''deleteAction'',''dataTable'')',
'JS',7,2,'dataForm:page-content_footer-panel',0,null,null,null,'TRUE',null);

MERGE INTO UI_MENU_ITEM A
     USING (SELECT 'Save' AS DISPLAY_TEXT,'#{parcelZoneRateBackingBean.saveAction}' as LINK,(select min(ui_menu_Screen_id) from ui_menu_screen where screen_id='40000030') AS UI_MENU_SCREEN_ID,1 AS MENU_ID FROM DUAL) B
        ON (A.DISPLAY_TEXT = B.DISPLAY_TEXT
            AND A.LINK=B.LINK
            AND A.UI_MENU_SCREEN_ID=B.UI_MENU_SCREEN_ID
            AND A.MENU_ID=B.MENU_ID)
WHEN NOT MATCHED
THEN
   INSERT(UI_MENU_ITEM_ID,UI_MENU_SCREEN_ID,MENU_ID,IS_DISPLAY,DISPLAY_TEXT,LINK,LINK_TYPE,LOCN_FLAG,ITEM_SEQ_NBR,UI_MENU_COMP_ID,IS_MORE,AJAX_ACTION,AJAX_RERENDER,AJAX_ONCOMPLETE,RENDERED,IMAGE_URL) 
   VALUES(ui_menu_item_seq.nextval,(select ui_menu_screen_id from ui_menu_screen where screen_id = '40000030'),1,1,'Save','#{parcelZoneRateBackingBean.saveAction}',
'JSFA',7,3,'dataForm:page-content_footer-panel',0,null,null,null,'TRUE',null);

----/*MERGE INTO XMENU_ITEM L
----     USING (SELECT 'Tariff' NAME, '/te/parcelRate/ui/ParcelZoneRate.xhtml' NAVIGATION_KEY,'Contract Management' BASE_SECTION_NAME
----              FROM DUAL) B
----        ON (L.NAME = B.NAME AND L.NAVIGATION_KEY = B.NAVIGATION_KEY AND L.BASE_SECTION_NAME=B.BASE_SECTION_NAME)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XMENU_ITEM_ID,
----                        NAME,
----                        SHORT_NAME,
----                        NAVIGATION_KEY,
----                        ICON,
----                        BGCOLOR,
----                        BASE_SECTION_NAME,
----                        BASE_PART_NAME)
----       VALUES ( (SELECT NVL(MAX(XMENU_ITEM_ID), 0) + 1
----                 FROM XMENU_ITEM
----                WHERE TO_CHAR(XMENU_ITEM_ID) LIKE '3600%'),
----             'Tariff',
----             'Tariff',
----             '/te/parcelRate/ui/ParcelZoneRate.xhtml',
----             'default-icon',
----             '#002E5F',
----             'Contract Management',
----             'GENERAL');			 
----
----MERGE INTO XMENU_ITEM_APP L
----     USING (SELECT  (SELECT XMENU_ITEM_ID
----                  FROM XMENU_ITEM
----                 WHERE     NAME = 'Tariff'
----                       AND BASE_SECTION_NAME = 'Contract Management'
----                       AND BASE_PART_NAME = 'GENERAL'
----                       AND NAVIGATION_KEY='/te/parcelRate/ui/ParcelZoneRate.xhtml') XMENU_ITEM_ID, (SELECT APP_ID
----                  FROM APP
----                 WHERE APP_SHORT_NAME = 'WMS') APP_ID
----              FROM DUAL) B
----        ON (L.XMENU_ITEM_ID = B.XMENU_ITEM_ID AND L.APP_ID = B.APP_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT   (XMENU_ITEM_ID, APP_ID)
----       VALUES (
----               (SELECT XMENU_ITEM_ID
----                  FROM XMENU_ITEM
----                 WHERE     NAME = 'Tariff'
----                       AND BASE_SECTION_NAME = 'Contract Management'
----                       AND BASE_PART_NAME = 'GENERAL'
----                       AND NAVIGATION_KEY='/te/parcelRate/ui/ParcelZoneRate.xhtml'),
----               (SELECT APP_ID
----                  FROM APP
----                 WHERE APP_SHORT_NAME = 'WMS'));				 
----
----MERGE INTO XMENU_ITEM_PERMISSION L
----     USING (SELECT (SELECT XMENU_ITEM_ID
----                  FROM XMENU_ITEM
----                 WHERE     NAME = 'Tariff'
----                       AND BASE_SECTION_NAME = 'Contract Management'
----                       AND BASE_PART_NAME = 'GENERAL'
----                       AND NAVIGATION_KEY= '/te/parcelRate/ui/ParcelZoneRate.xhtml') XMENU_ITEM_ID,'VPCLZONERATE' PERMISSION_CODE
----              FROM DUAL) B
----        ON (L.XMENU_ITEM_ID = B.XMENU_ITEM_ID AND L.PERMISSION_CODE = B.PERMISSION_CODE)
----WHEN NOT MATCHED
----THEN
----   INSERT  (XMENU_ITEM_ID, PERMISSION_CODE)
----       VALUES (
----               (SELECT XMENU_ITEM_ID
----                  FROM XMENU_ITEM
----                 WHERE     NAME = 'Tariff'
----                       AND BASE_SECTION_NAME = 'Contract Management'
----                       AND BASE_PART_NAME = 'GENERAL'
----                       AND NAVIGATION_KEY= '/te/parcelRate/ui/ParcelZoneRate.xhtml'),
----               'VPCLZONERATE');		   
----			   
----MERGE INTO XMENU_ITEM_PERMISSION L
----     USING (SELECT (SELECT XMENU_ITEM_ID
----                  FROM XMENU_ITEM
----                 WHERE     NAME = 'Tariff'
----                       AND BASE_SECTION_NAME = 'Contract Management'
----                       AND BASE_PART_NAME = 'GENERAL'
----                       AND NAVIGATION_KEY= '/te/parcelRate/ui/ParcelZoneRate.xhtml') XMENU_ITEM_ID,'APCLZONERATE' PERMISSION_CODE
----              FROM DUAL) B
----        ON (L.XMENU_ITEM_ID = B.XMENU_ITEM_ID AND L.PERMISSION_CODE = B.PERMISSION_CODE)
----WHEN NOT MATCHED
----THEN
----   INSERT  (XMENU_ITEM_ID, PERMISSION_CODE)
----       VALUES (
----               (SELECT XMENU_ITEM_ID
----                  FROM XMENU_ITEM
----                 WHERE     NAME = 'Tariff'
----                       AND BASE_SECTION_NAME = 'Contract Management'
----                       AND BASE_PART_NAME = 'GENERAL'
----                       AND NAVIGATION_KEY= '/te/parcelRate/ui/ParcelZoneRate.xhtml'),
----               'APCLZONERATE');*/	   

MERGE INTO MESSAGE_MASTER A
USING (SELECT  '9514' MSG_ID,
                '111509514' KEY,
                'te' ILS_MODULE,
                'ErrorMessage' BUNDLE_NAME
     FROM DUAL) B
     on ( A.KEY=B.KEY and  A.BUNDLE_NAME=B.BUNDLE_NAME )
     WHEN NOT  MATCHED THEN
     INSERT (message_master_id,msg_id,key,ils_module,msg_module,msg,bundle_name,msg_class,msg_type,ovride_role,log_flag)
VALUES(SEQ_MESSAGE_MASTER_ID.NEXTVAL,'9514','111509514','te','SHIPPING','Minimum value cannot be greater than maximum value','ErrorMessage','SYSTEM','ERROR',NULL,NULL);

MERGE INTO MESSAGE_DISPLAY MD
USING (SELECT '111509514' DISP_KEY, 'en' TO_LANG, '20' SCREEN_TYPE_ID, -1 BU_ID FROM DUAL) D
ON (MD.DISP_KEY = D.DISP_KEY AND MD.SCREEN_TYPE_ID = D.SCREEN_TYPE_ID AND MD.TO_LANG = D.TO_LANG AND MD.BU_ID=D.BU_ID)
WHEN NOT MATCHED THEN
INSERT (MESSAGE_DISPLAY_ID,DISP_KEY,DISP_VALUE,TO_LANG,SCREEN_TYPE_ID,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,BU_ID) 
values(SEQ_MESSAGE_DISPLAY_ID.NEXTVAL,'111509514','Minimum value cannot be greater than maximum value','en','20',null,current_timestamp,null,current_timestamp,-1);

MERGE INTO MESSAGE_MASTER A
USING (SELECT  '9515' MSG_ID,
                '111509515' KEY,
                'te' ILS_MODULE,
                'ErrorMessage' BUNDLE_NAME
     FROM DUAL) B
     on ( A.KEY=B.KEY and  A.BUNDLE_NAME=B.BUNDLE_NAME )
     WHEN NOT  MATCHED THEN
     INSERT (message_master_id,msg_id,key,ils_module,msg_module,msg,bundle_name,msg_class,msg_type,ovride_role,log_flag)
VALUES(SEQ_MESSAGE_MASTER_ID.NEXTVAL,'9515','111509515','te','SHIPPING','Both minimum and maximum values cannot be zero','ErrorMessage','SYSTEM','ERROR',NULL,NULL);

MERGE INTO MESSAGE_DISPLAY MD
USING (SELECT '111509515' DISP_KEY, 'en' TO_LANG, '20' SCREEN_TYPE_ID, -1 BU_ID FROM DUAL) D
ON (MD.DISP_KEY = D.DISP_KEY AND MD.SCREEN_TYPE_ID = D.SCREEN_TYPE_ID AND MD.TO_LANG = D.TO_LANG AND MD.BU_ID=D.BU_ID)
WHEN NOT MATCHED THEN
INSERT(MESSAGE_DISPLAY_ID,DISP_KEY,DISP_VALUE,TO_LANG,SCREEN_TYPE_ID,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,BU_ID) 
values(SEQ_MESSAGE_DISPLAY_ID.nextval,'111509515','Both minimum and maximum values cannot be zero','en','20',null,current_timestamp,null,current_timestamp,-1);

MERGE INTO MESSAGE_MASTER A
USING (SELECT  '9516' MSG_ID,
                '111509516' KEY,
                'te' ILS_MODULE,
                'ErrorMessage' BUNDLE_NAME
     FROM DUAL) B
     on ( A.KEY=B.KEY and  A.BUNDLE_NAME=B.BUNDLE_NAME )
     WHEN NOT  MATCHED THEN
     INSERT (message_master_id,msg_id,key,ils_module,msg_module,msg,bundle_name,msg_class,msg_type,ovride_role,log_flag)
VALUES(SEQ_MESSAGE_MASTER_ID.NEXTVAL,'9516','111509516','te','SHIPPING','Minimum/Maximum  values specified overlaps with existing range','ErrorMessage','SYSTEM','ERROR',NULL,NULL);

MERGE INTO MESSAGE_DISPLAY MD
USING (SELECT '111509516' DISP_KEY, 'en' TO_LANG, '20' SCREEN_TYPE_ID, -1 BU_ID FROM DUAL) D
ON (MD.DISP_KEY = D.DISP_KEY AND MD.SCREEN_TYPE_ID = D.SCREEN_TYPE_ID AND MD.TO_LANG = D.TO_LANG AND MD.BU_ID=D.BU_ID)
WHEN NOT MATCHED THEN
INSERT(MESSAGE_DISPLAY_ID,DISP_KEY,DISP_VALUE,TO_LANG,SCREEN_TYPE_ID,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,BU_ID) 
values(SEQ_MESSAGE_DISPLAY_ID.nextval,'111509516','Minimum/Maximum  values specified overlaps with existing range','en','20',null,current_timestamp,null,current_timestamp,-1);
       
MERGE INTO OBJECT_TYPE_TABLE L
     USING (SELECT 'ParcelZoneRates' OBJECT_TYPE_NAME FROM DUAL) B
        ON (L.OBJECT_TYPE_NAME = B.OBJECT_TYPE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.OBJECT_TYPE_ID,
               L.OBJECT_TYPE_NAME,
               L.OBJECT_TYPE_DESC)
       VALUES((select max(OBJECT_TYPE_ID) + 1 from object_type_table),'ParcelZoneRates','Parcel Zone Rates');

MERGE INTO CUSTOM_VIEW_UIATTR L
     USING (SELECT 'Parcel Zone Rates' CUSTOM_VIEW_HEADER,'/te/parcelRate/ui/ParcelZoneRate.xhtml' CUSTOM_VIEW_ID FROM DUAL) B
        ON (L.CUSTOM_VIEW_HEADER = B.CUSTOM_VIEW_HEADER and L.CUSTOM_VIEW_ID=B.CUSTOM_VIEW_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.CUSTOM_VIEW_UIATTR_ID, L.CUSTOM_VIEW_HEADER, L.CUSTOM_VIEW_SAVE_ACTION, L.CUSTOM_VIEW_CANCEL_ACTION, L.CUSTOM_VIEW_RSD_ACTION, L.CUSTOM_VIEW_ID, L.OBJECT_TYPE_ID, L.CUSTOMIZE_VIEW_TYPE_ID)
       VALUES((select max(CUSTOM_VIEW_UIATTR_ID) + 1 from custom_view_uiattr),'Parcel Zone Rates','parcelZoneRatesCustomizeAction','parcelZoneRatesCustomizeAction','parcelZoneRatesCustomizeAction',
'/te/parcelRate/ui/ParcelZoneRate.xhtml',(select OBJECT_TYPE_ID from object_type_table where object_type_name = 'ParcelZoneRates'),4);

COMMIT;

-- DBTicket MACR00849975
DECLARE
      v_seq_count       NUMBER(1);
BEGIN
    
   SELECT count(*)
   INTO v_seq_count
   FROM user_sequences
   WHERE SEQUENCE_NAME = 'SEQ_PARCEL_RATE_CALENDER_ID';

   IF v_seq_count =0
   THEN
      EXECUTE IMMEDIATE 'CREATE SEQUENCE SEQ_PARCEL_RATE_CALENDER_ID MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

DECLARE
      v_seq_count       NUMBER(1);
BEGIN
    
   SELECT count(*)
   INTO v_seq_count
   FROM user_sequences
   WHERE SEQUENCE_NAME = 'SEQ_PARCEL_RATE_DATA_ID';

   IF v_seq_count =0
   THEN
      EXECUTE IMMEDIATE 'CREATE SEQUENCE SEQ_PARCEL_RATE_DATA_ID MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

DECLARE
      v_tab_count       NUMBER(1);
BEGIN
    
   SELECT count(*)
   INTO v_tab_count
   FROM user_tables
   WHERE TABLE_NAME = 'PARCEL_ZONE_DATA';

   IF v_tab_count =0
   THEN
      EXECUTE IMMEDIATE 'CREATE TABLE PARCEL_ZONE_DATA( PARCEL_ZONE_DATA_ID NUMBER(10,0),
                         CARRIER_CODE VARCHAR2(10), 
                         ORIGIN_FROM_ZIP VARCHAR2(10), 
                         ORIGIN_TO_ZIP VARCHAR2(10), 
                         DEST_FROM_ZIP VARCHAR2(10), 
                         DEST_TO_ZIP  VARCHAR2(10),  
                         DEST_CNTRY VARCHAR2(2), 
                         ZONES_PER_SL VARCHAR2(500), 
                         TRANSIT_DAYS_PER_SL VARCHAR2(500),
                         CREATED_DATE_TIME TIMESTAMP(6) DEFAULT SYSDATE) tablespace te_bst_dt_tbs';
      EXECUTE IMMEDIATE 'ALTER TABLE PARCEL_ZONE_DATA ADD CONSTRAINT PK_PRCL_ZN_DATA PRIMARY KEY(PARCEL_ZONE_DATA_ID) USING INDEX TABLESPACE TE_BST_IDX_TBS';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/ 
 
DECLARE
      v_tab_count       NUMBER(1);
BEGIN
    
   SELECT count(*)
   INTO v_tab_count
   FROM user_tables
   WHERE TABLE_NAME = 'PARCEL_TRANSIT_TIME';

   IF v_tab_count =0
   THEN
      EXECUTE IMMEDIATE ' CREATE TABLE PARCEL_TRANSIT_TIME( PARCEL_TRANSIT_TIME_ID NUMBER(10,0),
                          TC_COMPANY_ID NUMBER(8,0),
                          CARRIER_ID NUMBER(12,0),  
                          SERVICE_LEVEL_ID NUMBER(10), 
                          ORIGIN_ZIP VARCHAR2(10), 
                          DEST_FROM_ZIP VARCHAR2(10), 
                          DEST_TO_ZIP VARCHAR2(10), 
                          DAYS_FROM_ORIGIN  NUMBER(3), 
                          DEST_CNTRY VARCHAR2(2),  
                          CREATED_DATE_TIME TIMESTAMP(6) DEFAULT SYSDATE) tablespace te_bst_dt_tbs';
      EXECUTE IMMEDIATE 'ALTER TABLE PARCEL_TRANSIT_TIME ADD CONSTRAINT PK_PRCL_TR_TM PRIMARY KEY(PARCEL_TRANSIT_TIME_ID) USING INDEX TABLESPACE TE_BST_IDX_TBS';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/  
 
DECLARE
      v_tab_count       NUMBER(1);
BEGIN
    
   SELECT count(*)
   INTO v_tab_count
   FROM user_tables
   WHERE TABLE_NAME = 'PARCEL_RATE_CALENDER';

   IF v_tab_count =0
   THEN
      EXECUTE IMMEDIATE ' CREATE TABLE PARCEL_RATE_CALENDER(PARCEL_RATE_CALENDER_ID NUMBER(10,0), 
                          TC_COMPANY_ID  NUMBER(8) NOT NULL, 
                          CARRIER_ID NUMBER(12,0),    
                          EFFECTIVE_DT  DATE ,         
                          EXPIRATION_DT  DATE ,
                          MOD_DATE_TIME  TIMESTAMP(6),USER_ID VARCHAR2(50),
                          CREATED_DATE_TIME TIMESTAMP(6) DEFAULT SYSDATE) tablespace te_bst_dt_tbs';
      EXECUTE IMMEDIATE ' ALTER TABLE PARCEL_RATE_CALENDER ADD CONSTRAINT PK_PRCL_RT_CAL PRIMARY KEY(PARCEL_RATE_CALENDER_ID) USING INDEX TABLESPACE TE_BST_IDX_TBS';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/  
 
DECLARE
      v_tab_count       NUMBER(1);
BEGIN
    
   SELECT count(*)
   INTO v_tab_count
   FROM user_tables
   WHERE TABLE_NAME = 'PARCEL_RATE_DATA';

   IF v_tab_count =0
   THEN
      EXECUTE IMMEDIATE 'CREATE TABLE PARCEL_RATE_DATA( PARCEL_RATE_DATA_ID NUMBER(10,0),
                          TC_COMPANY_ID 	NUMBER(8,0),
                          MOT_ID  NUMBER(12),
                          SERVICE_LEVEL_ID NUMBER(10), 
                          TIER             VARCHAR2(2),
                          ZONE            VARCHAR2(10),
                          FROM_WT    		 NUMBER(14,3), 
                          TO_WT				   NUMBER(14,3), 
                          RATE_CALC_MTHD  	VARCHAR2(4),
                          INCR_CHRG          NUMBER(14,3),
                          MIN_CHRG           NUMBER(14,3),
                          INCRMNT            NUMBER(14,3),
                          CALC_RATE          NUMBER(14,3),
                          MOD_DATE_TIME      TIMESTAMP(6),
                          USER_ID           VARCHAR2(50),
                          CREATED_DATE_TIME   TIMESTAMP(6) DEFAULT SYSDATE,
                          PARCEL_RATE_CALENDER_ID NUMBER(10,0)) tablespace te_bst_dt_tbs';
      EXECUTE IMMEDIATE 'ALTER TABLE PARCEL_RATE_DATA ADD CONSTRAINT PK_PRCL_RT_DATA PRIMARY KEY (PARCEL_RATE_DATA_ID) USING INDEX TABLESPACE TE_BST_IDX_TBS';
      EXECUTE IMMEDIATE 'ALTER TABLE PARCEL_RATE_DATA ADD CONSTRAINT PARCEL_RATE_CALENDER_FK1 FOREIGN KEY (PARCEL_RATE_CALENDER_ID) REFERENCES  PARCEL_RATE_CALENDER (PARCEL_RATE_CALENDER_ID) ON DELETE SET NULL ENABLE';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/   

create or replace
procedure wm_parcel_rate_processing(
    p_tc_company_id     in number,
    p_carrier_code   in parcel_zone_data.carrier_code%type default null,
    p_effective_date  in parcel_rate_calender.effective_dt%type default sysdate,
    p_expiration_date in parcel_rate_calender.expiration_dt%type default sysdate,
    p_user_id          in parcel_rate_data.user_id%type default 'manh')
as
  v_serv_type          varchar2(200);
  v_service_level_type      varchar2(50);
  v_parcel_rate_data_seq_id number;
  v_parcel_rate_calendar_id   number;
  v_carrier_id              number ;
  v_calendar_count          number;
  begin
  v_parcel_rate_calendar_id := 0;
  v_calendar_count := 0;
  -- first check if there is any existing calendar for the given carrier
  
   select carrier_id into v_carrier_id
    from carrier_code
    where carrier_code=p_carrier_code
    and tc_company_id =p_tc_company_id;
   
  
  select count(1) into v_calendar_count
  from parcel_rate_calender
  where carrier_id  = v_carrier_id
  and expiration_dt = p_expiration_date
  and effective_dt  = p_effective_date;
    
  if (v_calendar_count > 0) then
    select parcel_rate_calender_id
    into v_parcel_rate_calendar_id
    from parcel_rate_calender
    where carrier_id        = v_carrier_id
    and expiration_dt         = p_expiration_date
    and effective_dt          = p_effective_date;
  end if;
  
  -- if there is existing calerndar with the same date range then delete the calendar and re-create it
  if (v_parcel_rate_calendar_id > 0) then
    delete
    from parcel_rate_data
    where parcel_rate_calender_id = v_parcel_rate_calendar_id;
  else
    
     -- if overlapping calendars are there then adjust the dates of existing calendars
    update parcel_rate_calender
    set expiration_dt = p_effective_date - 1
    where carrier_id  = v_carrier_id
    and expiration_dt > p_effective_date
    and expiration_dt < p_expiration_date;
    
    update parcel_rate_calender
    set effective_dt  = p_expiration_date + 1
    where carrier_id  = v_carrier_id
    and effective_dt  > p_effective_date
    and expiration_dt > p_expiration_date;
    
    select seq_parcel_rate_calender_id.nextval
    into v_parcel_rate_calendar_id
    from dual ;
    
    insert
    into parcel_rate_calender
      (
        parcel_rate_calender_id,
        carrier_id ,
        tc_company_id,
        effective_dt,
        expiration_dt,
        mod_date_time,
        user_id
      )
      values
      (
        v_parcel_rate_calendar_id,
        v_carrier_id,
        p_tc_company_id,
        p_effective_date,
        p_expiration_date,
        sysdate,
        p_user_id
      );
      
  end if;
  for ratezonerecord in
  (select sl.tc_company_id tc_company_id,
      (select mot_id
      from mot
      where mot.tc_company_id = sl.tc_company_id
      and mot.mot             = ipr.mot
      ) mot_id,
      sl.service_level_id service_level_id,
      ipr.tier tier ,
      substr(ipr.zone,1,3) zone,
      ipr.from_wt from_wt,
      ipr.to_wt to_wt,
      ipr.rate_calc_mthd rate_calc_mthd,
      ipr.incr_chrg ,
      ipr.min_chrg ,
      ipr.incrmnt ,
      ipr.calc_rate ,
      p_user_id
    from service_level sl
    join (select * from inpt_carrier_service where carrier_code=p_carrier_code) ics
    on sl.service_level = ics.service_level_type
    left join inpt_parcl_rate ipr
    on ics.serv_type       = ipr.serv_type
    where sl.tc_company_id = p_tc_company_id
    and ipr.zone is not null
    order by ipr.from_wt ,
      ipr.zone
  )
  loop
    insert
    into parcel_rate_data
      (
        parcel_rate_data_id,
        tc_company_id ,
        mot_id ,
        service_level_id ,
        tier ,
        zone ,
        from_wt ,
        to_wt ,
        rate_calc_mthd ,
        incr_chrg ,
        min_chrg ,
        incrmnt ,
        calc_rate ,
        mod_date_time ,
        user_id ,
        created_date_time ,
        parcel_rate_calender_id
      )
      values
      (
        seq_parcel_rate_data_id.nextval,
        ratezonerecord.tc_company_id,
        ratezonerecord.mot_id,
        ratezonerecord.service_level_id,
        ratezonerecord.tier,
        ratezonerecord.zone,
        ratezonerecord.from_wt,
        ratezonerecord.to_wt,
        ratezonerecord.rate_calc_mthd,
        ratezonerecord.incr_chrg,
        ratezonerecord.min_chrg,
        ratezonerecord.incrmnt,
        ratezonerecord.calc_rate,
        sysdate,
        p_user_id,
        sysdate,
        v_parcel_rate_calendar_id
      );
  end loop;
 
end ;

/

create or replace
procedure wm_parcel_zone_processing(
    p_tc_company_id     in number,
    p_carrier_code in parcel_zone_data.carrier_code%type default null,
    p_origin_zip     in varchar2)
as
    v_serv_type          VARCHAR2(2000);
    v_transit_time       VARCHAR2(2000);
    v_service_level_type VARCHAR2(500);
    v_carrier_id         NUMBER;
    v_postal_code_like   VARCHAR2(10);
begin
  -- this procedure moves records from inpt_parcl_zone table to parcel_zone_data
  -- multiple rows compressed into a single record to minimize the data set
  -- first delete zone records for the given carrier code and origin zip if already exists

  
    delete
    from parcel_zone_data
    where origin_from_zip    is not null
    and origin_to_zip        is not null
    and dest_from_zip        is not null
    and dest_to_zip          is not null
    and length(dest_from_zip) >1
    and length(dest_to_zip)   >1
    and p_origin_zip between origin_from_zip and origin_to_zip
    and (dest_cntry  = 'US'
    or dest_cntry    = 'PR')
    and carrier_code = p_carrier_code;
    
     -- delete international zone records for the given carrier code, it will be re-created. 
    delete
    from parcel_zone_data
    where  carrier_code = p_carrier_code
    and dest_cntry  != 'US'
    and dest_cntry    != 'PR';

  
  -- create new set of records from inpt_parcl_zone

    for zonerecord in
    ( select distinct origin_from_zip,
      origin_to_zip,
      dest_from_zip,
      dest_to_zip,
      dest_cntry
    from inpt_parcl_zone
    where origin_from_zip    is not null
    and origin_to_zip        is not null
    and dest_from_zip        is not null
    and dest_to_zip          is not null
    and length(dest_from_zip) >1
    and length(dest_to_zip)   >1
    and p_origin_zip between origin_from_zip and origin_to_zip
    and (dest_cntry = 'US'
    or dest_cntry   = 'PR')
    and serv_type  in
      (select serv_type
      from inpt_carrier_service
      where carrier_code = p_carrier_code
      )
    union all
    select distinct origin_from_zip,
      origin_to_zip,
      dest_from_zip,
      dest_to_zip,
      dest_cntry
    from inpt_parcl_zone
    where dest_cntry != 'US'
    and dest_cntry   != 'PR'
    and serv_type    in
      (select serv_type
      from inpt_carrier_service
      where carrier_code = p_carrier_code
      )
    )
    loop
      v_serv_type    := '';
      v_transit_time :='';
      for servtyperecord in
      (select serv_type,
        trim(zone) zone,
        days_from_orgn
      from inpt_parcl_zone
      where origin_from_zip=zonerecord.origin_from_zip
      and (origin_to_zip   =zonerecord.origin_to_zip
      or origin_to_zip    is null)
      and dest_from_zip    =zonerecord.dest_from_zip
      and dest_to_zip      =zonerecord.dest_to_zip
      and dest_cntry       = zonerecord.dest_cntry
      )
      loop
        begin
          select distinct service_level_type
          into v_service_level_type
          from inpt_carrier_service
          where inpt_carrier_service.serv_type=servtyperecord.serv_type;
          v_serv_type                     := v_serv_type || v_service_level_type || '-' ||servtyperecord.zone || '|';
          v_transit_time                  := v_transit_time || v_service_level_type || '-' || servtyperecord.days_from_orgn || '|';
        exception
        when no_data_found then
          continue;
        end;
      end loop ;
      insert
      into parcel_zone_data
        (
          parcel_zone_data_id,
          carrier_code,
          origin_from_zip,
          origin_to_zip,
          dest_from_zip,
          dest_to_zip ,
          dest_cntry,
          zones_per_sl,
          transit_days_per_sl,
          created_date_time
        )
        values
        (
          seq_parcel_rate_data_id.nextval,
          p_carrier_code,
          zonerecord.origin_from_zip,
          zonerecord.origin_to_zip,
          zonerecord.dest_from_zip,
          zonerecord.dest_to_zip,
          zonerecord.dest_cntry,
          substr(v_serv_type,1,length(v_serv_type)      -1),
          substr(v_transit_time,1,length(v_transit_time)-1),
          sysdate
        );
    end loop;


  select carrier_id
  into v_carrier_id
  from carrier_code
  where carrier_code=p_carrier_code
  and tc_company_id =p_tc_company_id;
  -- delete existing transit time records for the given carrier and
  -- create new records
  delete
  from parcel_transit_time
  where carrier_id  =v_carrier_id
  and tc_company_id =p_tc_company_id
  and origin_zip    = p_origin_zip;
  v_postal_code_like  := p_origin_zip || '%' ;
  insert
  into parcel_transit_time
    (
      parcel_transit_time_id,
      tc_company_id,
      carrier_id,
      service_level_id,
      origin_zip,
      dest_from_zip,
      dest_to_zip,
      days_from_origin,
      dest_cntry,
      created_date_time
    )
    (select seq_parcel_rate_data_id.nextval,
        p_tc_company_id,
        v_carrier_id,
        sl.service_level_id,
        substr(itr.postal_code,1,5) origin_zip,
        case
          when length(substr(itr.postal_code,6,5)) = 3
          then (substr(itr.postal_code,6,5)
            || '00')
          else substr(itr.postal_code,6,5)
        end dest_from_zip,
        case
          when length(substr(itr.postal_code,6,5)) = 3
          then ( substr(itr.postal_code,6,5)
            || '99' )
          else substr(itr.postal_code,6,5)
        end dest_to_zip,
        itr.transit_time_in_days,
        'US',
        sysdate
      from service_level sl
      left join inpt_carrier_service ics
      on sl.service_level = ics.service_level_type
      left join inpt_transit_time itr
      on ics.serv_type       = itr.serv_type
      where sl.tc_company_id = p_tc_company_id
      and itr.carrier_code   = p_carrier_code
      and itr.postal_code like v_postal_code_like
    );
end ;
/

-- DBTicket TE-91
-- This needs to go after the activities MACR00830577,MACR00858179 and MACR00849975 respectively

DECLARE
      v_seq_count       NUMBER(1);
BEGIN
    
   SELECT count(*)
   INTO v_seq_count
   FROM user_sequences
   WHERE SEQUENCE_NAME = 'SEQ_PARCEL_RATE_CALENDER_ID';

   IF v_seq_count =0
   THEN
      EXECUTE IMMEDIATE 'CREATE SEQUENCE SEQ_PARCEL_RATE_CALENDER_ID MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

DECLARE
      v_seq_count       NUMBER(1);
BEGIN
    
   SELECT count(*)
   INTO v_seq_count
   FROM user_sequences
   WHERE SEQUENCE_NAME = 'SEQ_PARCEL_RATE_DATA_ID';

   IF v_seq_count =0
   THEN
      EXECUTE IMMEDIATE 'CREATE SEQUENCE SEQ_PARCEL_RATE_DATA_ID MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

DECLARE
      v_tab_count       NUMBER(1);
BEGIN
    
   SELECT count(*)
   INTO v_tab_count
   FROM user_tables
   WHERE TABLE_NAME = 'PARCEL_ZONE_DATA';

   IF v_tab_count =0
   THEN
      EXECUTE IMMEDIATE 'CREATE TABLE PARCEL_ZONE_DATA( PARCEL_ZONE_DATA_ID NUMBER(10,0),
                         CARRIER_CODE VARCHAR2(10), 
                         ORIGIN_FROM_ZIP VARCHAR2(10), 
                         ORIGIN_TO_ZIP VARCHAR2(10), 
                         DEST_FROM_ZIP VARCHAR2(10), 
                         DEST_TO_ZIP  VARCHAR2(10),  
                         DEST_CNTRY VARCHAR2(2), 
                         ZONES_PER_SL VARCHAR2(500), 
                         TRANSIT_DAYS_PER_SL VARCHAR2(500),
                         CREATED_DATE_TIME TIMESTAMP(6) DEFAULT SYSDATE) tablespace te_bst_dt_tbs';
      EXECUTE IMMEDIATE 'ALTER TABLE PARCEL_ZONE_DATA ADD CONSTRAINT PK_PRCL_ZN_DATA PRIMARY KEY(PARCEL_ZONE_DATA_ID) USING INDEX TABLESPACE TE_BST_IDX_TBS';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/ 
 
DECLARE
      v_tab_count       NUMBER(1);
BEGIN
    
   SELECT count(*)
   INTO v_tab_count
   FROM user_tables
   WHERE TABLE_NAME = 'PARCEL_TRANSIT_TIME';

   IF v_tab_count =0
   THEN
      EXECUTE IMMEDIATE ' CREATE TABLE PARCEL_TRANSIT_TIME( PARCEL_TRANSIT_TIME_ID NUMBER(10,0),
                          TC_COMPANY_ID NUMBER(8,0),
                          CARRIER_ID NUMBER(12,0),  
                          SERVICE_LEVEL_ID NUMBER(10), 
                          ORIGIN_ZIP VARCHAR2(10), 
                          DEST_FROM_ZIP VARCHAR2(10), 
                          DEST_TO_ZIP VARCHAR2(10), 
                          DAYS_FROM_ORIGIN  NUMBER(3), 
                          DEST_CNTRY VARCHAR2(2),  
                          CREATED_DATE_TIME TIMESTAMP(6) DEFAULT SYSDATE) tablespace te_bst_dt_tbs';
      EXECUTE IMMEDIATE 'ALTER TABLE PARCEL_TRANSIT_TIME ADD CONSTRAINT PK_PRCL_TR_TM PRIMARY KEY(PARCEL_TRANSIT_TIME_ID) USING INDEX TABLESPACE TE_BST_IDX_TBS';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/  
 
DECLARE
      v_tab_count       NUMBER(1);
BEGIN
    
   SELECT count(*)
   INTO v_tab_count
   FROM user_tables
   WHERE TABLE_NAME = 'PARCEL_RATE_CALENDER';

   IF v_tab_count =0
   THEN
      EXECUTE IMMEDIATE ' CREATE TABLE PARCEL_RATE_CALENDER(PARCEL_RATE_CALENDER_ID NUMBER(10,0), 
                          TC_COMPANY_ID  NUMBER(8) NOT NULL, 
                          CARRIER_ID NUMBER(12,0),    
                          EFFECTIVE_DT  DATE ,         
                          EXPIRATION_DT  DATE ,
                          MOD_DATE_TIME  TIMESTAMP(6),USER_ID VARCHAR2(50),
                          CREATED_DATE_TIME TIMESTAMP(6) DEFAULT SYSDATE) tablespace te_bst_dt_tbs';
      EXECUTE IMMEDIATE ' ALTER TABLE PARCEL_RATE_CALENDER ADD CONSTRAINT PK_PRCL_RT_CAL PRIMARY KEY(PARCEL_RATE_CALENDER_ID) USING INDEX TABLESPACE TE_BST_IDX_TBS';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/  
 
DECLARE
      v_tab_count       NUMBER(1);
BEGIN
    
   SELECT count(*)
   INTO v_tab_count
   FROM user_tables
   WHERE TABLE_NAME = 'PARCEL_RATE_DATA';

   IF v_tab_count =0
   THEN
      EXECUTE IMMEDIATE 'CREATE TABLE PARCEL_RATE_DATA( PARCEL_RATE_DATA_ID NUMBER(10,0),
                          TC_COMPANY_ID 	NUMBER(8,0),
                          MOT_ID  NUMBER(12),
                          SERVICE_LEVEL_ID NUMBER(10), 
                          TIER             VARCHAR2(2),
                          ZONE            VARCHAR2(10),
                          FROM_WT    		 NUMBER(14,3), 
                          TO_WT				   NUMBER(14,3), 
                          RATE_CALC_MTHD  	VARCHAR2(4),
                          INCR_CHRG          NUMBER(14,3),
                          MIN_CHRG           NUMBER(14,3),
                          INCRMNT            NUMBER(14,3),
                          CALC_RATE          NUMBER(14,3),
                          MOD_DATE_TIME      TIMESTAMP(6),
                          USER_ID           VARCHAR2(50),
                          CREATED_DATE_TIME   TIMESTAMP(6) DEFAULT SYSDATE,
                          PARCEL_RATE_CALENDER_ID NUMBER(10,0)) tablespace te_bst_dt_tbs';
      EXECUTE IMMEDIATE 'ALTER TABLE PARCEL_RATE_DATA ADD CONSTRAINT PK_PRCL_RT_DATA PRIMARY KEY (PARCEL_RATE_DATA_ID) USING INDEX TABLESPACE TE_BST_IDX_TBS';
      EXECUTE IMMEDIATE 'ALTER TABLE PARCEL_RATE_DATA ADD CONSTRAINT PARCEL_RATE_CALENDER_FK1 FOREIGN KEY (PARCEL_RATE_CALENDER_ID) REFERENCES  PARCEL_RATE_CALENDER (PARCEL_RATE_CALENDER_ID) ON DELETE SET NULL ENABLE';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/   

create or replace procedure wm_parcel_rate_processing(
    p_tc_company_id     in number,
    p_carrier_code   in parcel_zone_data.carrier_code%type default null,
    p_effective_date  in parcel_rate_calender.effective_dt%type default sysdate,
    p_expiration_date in parcel_rate_calender.expiration_dt%type default sysdate,
    p_user_id          in parcel_rate_data.user_id%type default 'manh')

as
  v_serv_type          varchar2(200);
  v_service_level_type      varchar2(50);
  v_parcel_rate_data_seq_id number;
  v_parcel_rate_calendar_id   number;
  v_carrier_id              number ;
  v_calendar_count          number;
  begin
  v_parcel_rate_calendar_id := 0;
  v_calendar_count := 0;
  -- first check if there is any existing calendar for the given carrier

   select carrier_id into v_carrier_id
    from carrier_code
    where carrier_code=p_carrier_code
    and tc_company_id =p_tc_company_id;

  select count(1) into v_calendar_count
  from parcel_rate_calender
  where carrier_id  = v_carrier_id
  and expiration_dt = p_expiration_date
  and effective_dt  = p_effective_date;

  if (v_calendar_count > 0) then
    select parcel_rate_calender_id
    into v_parcel_rate_calendar_id
    from parcel_rate_calender
    where carrier_id        = v_carrier_id
    and expiration_dt         = p_expiration_date
    and effective_dt          = p_effective_date;
  end if;

  -- if there is existing calerndar with the same date range then delete the calendar and re-create it
  if (v_parcel_rate_calendar_id > 0) then
    delete
    from parcel_rate_data
    where parcel_rate_calender_id = v_parcel_rate_calendar_id;
  else

     -- if overlapping calendars are there then adjust the dates of existing calendars
    update parcel_rate_calender
    set expiration_dt = p_effective_date - 1
    where carrier_id  = v_carrier_id
    and expiration_dt > p_effective_date
    and expiration_dt < p_expiration_date;

    update parcel_rate_calender
    set effective_dt  = p_expiration_date + 1
    where carrier_id  = v_carrier_id
    and effective_dt  > p_effective_date
    and expiration_dt > p_expiration_date;

    select seq_parcel_rate_calender_id.nextval
    into v_parcel_rate_calendar_id
    from dual ;

    insert
    into parcel_rate_calender
      (
        parcel_rate_calender_id,
        carrier_id ,
        tc_company_id,
        effective_dt,
        expiration_dt,
        mod_date_time,
        user_id
      )
      values
      (
        v_parcel_rate_calendar_id,
        v_carrier_id,
        p_tc_company_id,
        p_effective_date,
        p_expiration_date,
        sysdate,
        p_user_id
      );

  end if;
  for ratezonerecord in
  (select sl.tc_company_id tc_company_id,
      (select mot_id
      from mot
      where mot.tc_company_id = sl.tc_company_id
      and mot.mot             = ipr.mot
      ) mot_id,
      sl.service_level_id service_level_id,
      ipr.tier tier ,
      substr(ipr.zone,1,3) zone,
      ipr.from_wt from_wt,
      ipr.to_wt to_wt,
      ipr.rate_calc_mthd rate_calc_mthd,
      ipr.incr_chrg ,
      ipr.min_chrg ,
      ipr.incrmnt ,
      ipr.calc_rate ,
      p_user_id
    from service_level sl
    join (select * from inpt_carrier_service where carrier_code=p_carrier_code) ics

    on sl.service_level = ics.service_level_type
    left join inpt_parcl_rate ipr
    on ics.serv_type       = ipr.serv_type
    where sl.tc_company_id = p_tc_company_id
    and ipr.zone is not null
    order by ipr.from_wt ,
      ipr.zone
  )
  loop
    insert
    into parcel_rate_data
      (
        parcel_rate_data_id,
        tc_company_id ,
        mot_id ,
        service_level_id ,
        tier ,
        zone ,
        from_wt ,
        to_wt ,
        rate_calc_mthd ,
        incr_chrg ,
        min_chrg ,
        incrmnt ,
        calc_rate ,
        mod_date_time ,
        user_id ,
        created_date_time ,
        parcel_rate_calender_id
      )
      values
      (
        seq_parcel_rate_data_id.nextval,
        ratezonerecord.tc_company_id,
        ratezonerecord.mot_id,
        ratezonerecord.service_level_id,
        ratezonerecord.tier,
        ratezonerecord.zone,
        ratezonerecord.from_wt,
        ratezonerecord.to_wt,
        ratezonerecord.rate_calc_mthd,
        ratezonerecord.incr_chrg,
        ratezonerecord.min_chrg,
        ratezonerecord.incrmnt,
        ratezonerecord.calc_rate,
        sysdate,
        p_user_id,
        sysdate,
        v_parcel_rate_calendar_id
      );
  end loop;
end;
/
show errors ;

create or replace
procedure wm_parcel_zone_processing(
    p_tc_company_id     in number,
    p_carrier_code in parcel_zone_data.carrier_code%type default null,
    p_origin_zip     in varchar2)
as
    v_serv_type          VARCHAR2(2000);
    v_transit_time       VARCHAR2(2000);
    v_service_level_type VARCHAR2(500);
    v_carrier_id         NUMBER;
    v_postal_code_like   VARCHAR2(10);
begin
  -- this procedure moves records from inpt_parcl_zone table to parcel_zone_data
  -- multiple rows compressed into a single record to minimize the data set
  -- first delete zone records for the given carrier code and origin zip if already exists

  
    delete
    from parcel_zone_data
    where origin_from_zip    is not null
    and origin_to_zip        is not null
    and dest_from_zip        is not null
    and dest_to_zip          is not null
    and length(dest_from_zip) >1
    and length(dest_to_zip)   >1
    and p_origin_zip between origin_from_zip and origin_to_zip
    and (dest_cntry  = 'US'
    or dest_cntry    = 'PR')
    and carrier_code = p_carrier_code;
    
     -- delete international zone records for the given carrier code, it will be re-created. 
    delete
    from parcel_zone_data
    where  carrier_code = p_carrier_code
    and dest_cntry  != 'US'
    and dest_cntry    != 'PR';

  
  -- create new set of records from inpt_parcl_zone

    for zonerecord in
    ( select distinct origin_from_zip,
      origin_to_zip,
      dest_from_zip,
      dest_to_zip,
      dest_cntry
    from inpt_parcl_zone
    where origin_from_zip    is not null
    and origin_to_zip        is not null
    and dest_from_zip        is not null
    and dest_to_zip          is not null
    and length(dest_from_zip) >1
    and length(dest_to_zip)   >1
    and p_origin_zip between origin_from_zip and origin_to_zip
    and (dest_cntry = 'US'
    or dest_cntry   = 'PR')
    and serv_type  in
      (select serv_type
      from inpt_carrier_service
      where carrier_code = p_carrier_code
      )
    union all
    select distinct origin_from_zip,
      origin_to_zip,
      dest_from_zip,
      dest_to_zip,
      dest_cntry
    from inpt_parcl_zone
    where dest_cntry != 'US'
    and dest_cntry   != 'PR'
    and serv_type    in
      (select serv_type
      from inpt_carrier_service
      where carrier_code = p_carrier_code
      )
    )
    loop
      v_serv_type    := '';
      v_transit_time :='';
      for servtyperecord in
      (select serv_type,
        trim(zone) zone,
        days_from_orgn
      from inpt_parcl_zone
      where origin_from_zip=zonerecord.origin_from_zip
      and (origin_to_zip   =zonerecord.origin_to_zip
      or origin_to_zip    is null)
      and dest_from_zip    =zonerecord.dest_from_zip
      and dest_to_zip      =zonerecord.dest_to_zip
      and dest_cntry       = zonerecord.dest_cntry
      )
      loop
        begin
          select distinct service_level_type
          into v_service_level_type
          from inpt_carrier_service
          where inpt_carrier_service.serv_type=servtyperecord.serv_type;
          v_serv_type                     := v_serv_type || v_service_level_type || '-' ||servtyperecord.zone || '|';
          v_transit_time                  := v_transit_time || v_service_level_type || '-' || servtyperecord.days_from_orgn || '|';
        exception
        when no_data_found then
          continue;
        end;
      end loop ;
      insert
      into parcel_zone_data
        (
          parcel_zone_data_id,
          carrier_code,
          origin_from_zip,
          origin_to_zip,
          dest_from_zip,
          dest_to_zip ,
          dest_cntry,
          zones_per_sl,
          transit_days_per_sl,
          created_date_time
        )
        values
        (
          seq_parcel_rate_data_id.nextval,
          p_carrier_code,
          zonerecord.origin_from_zip,
          zonerecord.origin_to_zip,
          zonerecord.dest_from_zip,
          zonerecord.dest_to_zip,
          zonerecord.dest_cntry,
          substr(v_serv_type,1,length(v_serv_type)      -1),
          substr(v_transit_time,1,length(v_transit_time)-1),
          sysdate
        );
    end loop;


  select carrier_id
  into v_carrier_id
  from carrier_code
  where carrier_code=p_carrier_code
  and tc_company_id =p_tc_company_id;
  -- delete existing transit time records for the given carrier and
  -- create new records
  delete
  from parcel_transit_time
  where carrier_id  =v_carrier_id
  and tc_company_id =p_tc_company_id
  and origin_zip    = p_origin_zip;
  v_postal_code_like  := p_origin_zip || '%' ;
  insert
  into parcel_transit_time
    (
      parcel_transit_time_id,
      tc_company_id,
      carrier_id,
      service_level_id,
      origin_zip,
      dest_from_zip,
      dest_to_zip,
      days_from_origin,
      dest_cntry,
      created_date_time
    )
    (select seq_parcel_rate_data_id.nextval,
        p_tc_company_id,
        v_carrier_id,
        sl.service_level_id,
        case
          when length(itr.postal_code) > 5
              then substr(itr.postal_code,1,5)
          else p_origin_zip
        end origin_zip ,
        case
          when length(itr.postal_code) = 3
          then ( itr.postal_code
            || '00' )
          when length(itr.postal_code) = 5
          then itr.postal_code
          when length(itr.postal_code) > 5
          and length(substr(itr.postal_code,6,5)) = 3
          then (substr(itr.postal_code,6,5)
            || '00')
          else substr(itr.postal_code,6,5)
        end dest_from_zip,
        case
          when length(itr.postal_code) = 3
          then ( itr.postal_code
            || '99' )
          when length(itr.postal_code) = 5
          then itr.postal_code
          when (length(itr.postal_code) > 5
          and length(substr(itr.postal_code,6,5)) = 3)
          then ( substr(itr.postal_code,6,5)
            || '99' )
          else substr(itr.postal_code,6,5)
        end dest_to_zip ,
        itr.transit_time_in_days,
        'US',
        sysdate
      from service_level sl
      left join inpt_carrier_service ics
      on sl.service_level = ics.service_level_type
      left join inpt_transit_time itr
      on ics.serv_type       = itr.serv_type
      where sl.tc_company_id = p_tc_company_id
      and itr.carrier_code   = p_carrier_code
      --and itr.postal_code like v_postal_code_like
    );
end ;
/

show errors ;

-- DBTicket TE-1743
MERGE INTO LABEL L
     USING (SELECT 'TE' BUNDLE_NAME, 'CreateRtlRteShipment' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'CreateRtlRteShipment',
               'Create Retail Route Shipment',
               'TE');
			   
commit;

-- DBTicket TE-1761
create sequence val_job_hdr_id_seq increment by 1 start with 1 nomaxvalue;
create sequence val_job_dtl_id_seq increment by 1 start with 1 nomaxvalue;
create sequence val_sql_id_seq increment by 1 start with 1 nomaxvalue;
create sequence val_txn_id_seq increment by 1 start with 1 nomaxvalue;

create global temporary table tmp_val_parm
(
    txn_id            number(9),
    user_id           varchar2(15),
    log_lvl           number(1)       not null,
    pgm_id            varchar2(40)    not null,
    module            varchar2(10)    not null,
    msg_id            varchar2(40)    not null,
    ref_code_1        varchar2(3)     not null,
    ref_code_2        varchar2(3),
    ref_value_2       varchar2(128)
) on commit delete rows;

create table val_sql
(
    sql_id              number(9) not null,
    ident               varchar2(20) not null,
    sql_desc            varchar2(100),
    is_locked           number(1) default 0,
    sql_text            clob not null,
    raw_text            clob not null,
    format_sql          number(1) default 1,
    create_date_time    date,
    mod_date_time       date,
    user_id             varchar2(15),
    text_len            number(9) not null,
    owner               varchar2(15),
    bind_var_list       varchar2(1000)
) tablespace SLMDATA;

alter table val_sql add constraint pk_val_sql
 primary key (sql_id) using index tablespace SLMINDX;

alter table val_sql add constraint uk_val_sql 
 unique (ident) using index tablespace SLMINDX;

create table val_tag_type
(
    type          varchar2(3)   not null,
    ui_desc       varchar2(30)  not null
) tablespace SLMDATA;

alter table val_tag_type add constraint pk_val_tag_type
 primary key (type) using index tablespace SLMINDX;

create table val_tag
(
    tag           varchar2(15)  not null,
    ui_desc       varchar2(30)  not null
) tablespace SLMDATA;

alter table val_tag add constraint pk_val_tag
 primary key (tag) using index tablespace SLMINDX;

create table val_tag_type_xref
(
    type          varchar2(3)   not null,
    tag           varchar2(15)  not null
) tablespace SLMDATA;

alter table val_tag_type_xref add constraint pk_val_tag_type_xref
 primary key(type, tag) using index tablespace SLMINDX;

alter table val_tag_type_xref add constraint fk_vttx_to_vtt
 foreign key(type) references val_tag_type(type);

alter table val_tag_type_xref add constraint fk_vttx_to_vt
 foreign key(tag) references val_tag(tag);

create table val_sql_tags
(
    type          varchar2(3)   not null,
    tag           varchar2(15)  not null,
    sql_id        number(9)     not null
) tablespace SLMDATA;

alter table val_sql_tags add constraint pk_val_sql_tags
 primary key(type, tag, sql_id) using index tablespace SLMINDX;

alter table val_sql_tags add constraint fk_vst_to_vttx 
 foreign key(type, tag) references val_tag_type_xref(type, tag);

alter table val_sql_tags add constraint fk_vst_to_vs 
 foreign key(sql_id) references val_sql(sql_id) on delete cascade;

create table val_parsed_select_list
(
    sql_id      number(9)  not null,
    pos         number(2)  not null,
    col_type    number(9) not null
) tablespace SLMDATA;

alter table val_parsed_select_list add constraint pk_vpsl 
 primary key (sql_id, pos) using index tablespace SLMINDX;

alter table val_parsed_select_list add constraint fk_vpsl_to_vs 
 foreign key(sql_id) references val_sql(sql_id) on delete cascade;

create table val_job_hdr
(
    val_job_hdr_id    number(9)    not null,
    job_name          varchar2(20) not null,
    job_desc          varchar2(100),
    owner             varchar2(15),
    is_locked         number(1) default 0,
    create_date_time  date,
    mod_date_time     date,
    user_id           varchar2(15)
) tablespace SLMDATA;

alter table val_job_hdr add constraint pk_val_job_hdr 
 primary key(val_job_hdr_id) using index tablespace SLMINDX;

alter table val_job_hdr add constraint uk_val_job_hdr 
 unique(job_name) using index tablespace SLMINDX;

create table val_job_dtl
(
    val_job_dtl_id    number(9) not null,
    val_job_hdr_id    number(9) not null,
    sql_id            number(9) not null,
    is_locked         number(1) default 0,
    create_date_time  date,
    mod_date_time     date,
    user_id           varchar2(15)
) tablespace SLMDATA;

alter table val_job_dtl add constraint pk_val_job_dtl 
 primary key(val_job_dtl_id) using index tablespace SLMINDX;

alter table val_job_dtl add constraint fk_vjd_to_vjh 
 foreign key(val_job_hdr_id)  references val_job_hdr(val_job_hdr_id) on delete cascade;

alter table val_job_dtl add constraint fk_vjd_to_vs 
 foreign key(sql_id) references val_sql(sql_id);

create table val_job_tags
(
    type              varchar2(3)  not null,
    tag               varchar2(15) not null,
    val_job_hdr_id    number(9)    not null
) tablespace SLMDATA;

alter table val_job_tags add constraint pk_val_job_tags 
 primary key(type, tag, val_job_hdr_id) using index tablespace SLMINDX;

alter table val_job_tags add constraint fk_vjt_to_vjh 
 foreign key(val_job_hdr_id) references val_job_hdr(val_job_hdr_id) on delete cascade;

create table val_sql_binds
(
    val_job_dtl_id  number(9)    not null,
    bind_var        varchar2(15) not null,
    bind_val        varchar2(50)
) tablespace SLMDATA;

alter table val_sql_binds add constraint pk_val_sql_binds 
 primary key(val_job_dtl_id, bind_var) using index tablespace SLMINDX;

alter table val_sql_binds add constraint c_vsb 
 check(regexp_like(bind_var, '^[^: ]*$'));

alter table val_sql_binds add constraint fk_vsb_to_vjd 
 foreign key(val_job_dtl_id) references val_job_dtl(val_job_dtl_id) on delete cascade;

create table val_expected_results
(
    val_job_dtl_id    number(9)  not null,
    row_num           number(9)  not null,
    pos               number(2)  not null,
    expected_value    varchar2(50) not null
) tablespace SLMDATA;

alter table val_expected_results add constraint pk_val_expected_results 
 primary key(val_job_dtl_id, row_num, pos) using index tablespace SLMINDX;

alter table val_expected_results add constraint fk_vr_to_vjd 
 foreign key(val_job_dtl_id) references val_job_dtl(val_job_dtl_id) on delete cascade;

create table val_result_hist
(
    txn_id            number(9)  not null,
    val_job_dtl_id    number(9)  not null,
    last_run_passed   number(1),
    create_date_time  date,
    user_id           varchar2(15),
    msg               varchar2(512)
) tablespace SLMDATA;

alter table val_result_hist add constraint pk_val_result_hist 
 primary key(txn_id, val_job_dtl_id) using index tablespace SLMINDX;

alter table val_result_hist add constraint fk_vrh_to_vjd 
 foreign key(val_job_dtl_id) references val_job_dtl(val_job_dtl_id) on delete cascade;

create table val_runtime_binds
(
    txn_id            number(9)    not null,
    val_job_dtl_id    number(9)    not null,
    bind_var          varchar2(15) not null,
    bind_val          varchar2(50),
    create_date_time  date,
    mod_date_time     date,
    user_id           varchar2(15)
) tablespace SLMDATA;

alter table val_runtime_binds add constraint pk_val_runtime_binds 
 primary key(txn_id, val_job_dtl_id, bind_var) using index tablespace SLMINDX;

alter table val_runtime_binds add constraint fk_vrb_to_vjd 
 foreign key(val_job_dtl_id) references val_job_dtl(val_job_dtl_id) on delete cascade;

create table val_job_seq
(
    app_hook        varchar2(20) not null,
    seq_nbr         number(3)    not null,
    job_name        varchar2(20) not null,
    pgm_id          varchar2(20) not null
) tablespace SLMDATA;

alter table val_job_seq add constraint pk_val_job_seq 
 primary key(app_hook, seq_nbr) using index tablespace SLMINDX;

alter table val_job_seq add constraint fk_vjs_to_vjh 
 foreign key(job_name) references val_job_hdr(job_name);

create or replace procedure wm_val_auton_log
(
    p_msg           in msg_log.msg%type,
    p_module        in msg_log.module%type,
    p_msg_id        in msg_log.msg_id%type,
    p_ref_code_1    in msg_log.ref_code_1%type,
    p_ref_value_1   in msg_log.ref_value_1%type,
    p_user_id       in user_profile.user_id%type,
    p_pgm_id        in msg_log.pgm_id%type,
    p_ref_code_2    in msg_log.ref_code_2%type,
    p_ref_value_2   in msg_log.ref_value_2%type
)
as
    pragma autonomous_transaction;
begin
    insert into msg_log
    (
        msg_log_id, module, msg_id, ref_value_1, msg, create_date_time, 
        mod_date_time, user_id, log_date_time, pgm_id, ref_code_1, ref_code_2,
        ref_value_2
    )
    values
    (
        msg_log_id_seq.nextval, p_module, p_msg_id, p_ref_value_1, p_msg,
        sysdate, sysdate, p_user_id, sysdate, p_pgm_id, p_ref_code_1,
        p_ref_code_2, p_ref_value_2
    );
    commit;
end;
/
show errors;

create or replace procedure wm_val_log
(
    p_msg           in msg_log.msg%type,
    p_sql_log_lvl   in number default 1,
    p_module        in msg_log.module%type default null,
    p_msg_id        in msg_log.msg_id%type default null,
    p_ref_code_2    in msg_log.ref_code_2%type default null,
    p_ref_value_2   in msg_log.ref_value_2%type default null
)
as
    v_pgm_id        msg_log.pgm_id%type;
    v_user_id       user_profile.user_id%type;
    v_max_log_lvl   number(1);
    v_module        msg_log.module%type;
    v_msg_id        msg_log.msg_id%type;
    v_ref_code_1    msg_log.ref_code_1%type;
    v_ref_value_1   msg_log.ref_value_1%type;
    v_ref_code_2    msg_log.ref_code_2%type;
    v_ref_value_2   msg_log.ref_value_2%type;
begin
    select t.log_lvl, t.pgm_id, t.user_id, coalesce(p_module, t.module, 'WAVE'), 
        coalesce(p_msg_id, t.msg_id, '1094'), t.ref_code_1, to_char(t.txn_id),
        coalesce(p_ref_code_2, t.ref_code_2), 
        coalesce(p_ref_value_2, t.ref_value_2)
    into v_max_log_lvl, v_pgm_id, v_user_id, v_module, v_msg_id, v_ref_code_1, 
        v_ref_value_1, v_ref_code_2, v_ref_value_2
    from tmp_val_parm t;

    if (p_sql_log_lvl <= v_max_log_lvl)
    then
        wm_val_auton_log(p_msg, v_module, v_msg_id, v_ref_code_1, v_ref_value_1,
            v_user_id, v_pgm_id, v_ref_code_2, v_ref_value_2);
    end if;    
end;
/
show errors;

create or replace procedure wm_val_lock_unlock_sql
(
    p_sql_id    in val_sql.sql_id%type default null,
    p_ident     in val_sql.ident%type default null,
    p_lock      in val_sql.is_locked%type
)
as
    v_sql_id    val_sql.sql_id%type;
    v_ident     val_sql.ident%type;
begin
    update val_sql vs
    set vs.is_locked = p_lock
    where vs.sql_id = p_sql_id or vs.ident = p_ident
    returning vs.sql_id, vs.ident into v_sql_id, v_ident;
    dbms_output.put_line(case when p_lock = 1 then 'Locked' else 'Unlocked' end
        || ' sql ' || v_sql_id || '(' || v_ident || ')');
    
    commit;
end;
/
show errors;

create or replace procedure wm_val_add_sql_tag
(
    p_2_tuple       in varchar2,
    p_ident         in val_sql.ident%type default null,
    p_sql_id        in val_sql.sql_id%type default null
)
as
    v_sql_id        val_sql.sql_id%type := p_sql_id;
    v_2_tuple       varchar2(20) := replace(p_2_tuple, ' ', '');
begin
    if (v_sql_id is null)
    then
        select vs.sql_id
        into v_sql_id
        from val_sql vs
        where vs.ident = p_ident;
    end if;

    insert into val_sql_tags
    (
        type, sql_id, tag
    )
    values
    (
        substr(v_2_tuple, 1, instr(v_2_tuple, ',') - 1), v_sql_id,
        substr(v_2_tuple, instr(v_2_tuple, ',') + 1)
    );
end;
/
show errors;

create or replace trigger wm_val_prep_sql
before insert or update on val_sql
referencing new as new
for each row
declare
    v_job_dependency  number(1);
    v_bind_var_list   val_sql.bind_var_list%type;
    v_bind_var        val_sql_binds.bind_var%type;
    v_bind_delim      varchar2(2) := 'bv';
    v_bind_pattern    varchar2(20) := ':[a-z]+\w*';
    --v_oper_pattern    varchar2(20) := '(<|>|(like)|+|-|=) *';
begin
    if (:new.format_sql = 0)
    then
        :new.sql_text := :new.raw_text;
    else
        :new.sql_text := regexp_replace(replace(replace(replace(trim(:new.raw_text), chr(10), ' '), chr(9), ' '), chr(13), ' '), '( ){2,}', ' ');
        if (instr(:new.sql_text, '''', 1, 1) = 0)
        then
            :new.sql_text := lower(:new.sql_text);
        end if;
    end if;

    for i in 1..regexp_count(:new.sql_text, v_bind_pattern, 1, 'i')
    loop
        v_bind_var := regexp_substr(:new.sql_text, v_bind_pattern, 1, i, 'i');
        exit when v_bind_var is null;
        v_bind_var_list := v_bind_var_list || substr(v_bind_var, 2) || ',';
    end loop;
    
    if (updating and coalesce(v_bind_var_list, ' ') != coalesce(:old.bind_var_list, ' '))
    then
        select case when exists
        (
            select 1
            from val_job_dtl vjd
            where vjd.sql_id = :new.sql_id
        ) then 1 else 0 end
        into v_job_dependency
        from dual;
        
        if (v_job_dependency = 1)
        then
            raise_application_error(-20050, 'Cannot modify bind vars. Purge dependent jobs first.');
        end if;
    end if;
    :new.bind_var_list := lower(v_bind_var_list);

    :new.text_len := length(replace(:new.sql_text, ' '));
end;
/
show errors;

create or replace trigger wm_val_derive_select_info
after insert or update on val_sql
referencing new as new
for each row
declare
    v_cursor        int := dbms_sql.open_cursor;
    v_col_list      dbms_sql.desc_tab2;
    v_col_count     int;
    v_max_cols      number(2) := 5;
-- todo: need to normalize chars and bytes
    v_max_col_len   number(3) := 200;
    v_current_date  date := sysdate;
begin
    dbms_sql.parse(v_cursor, :new.sql_text, dbms_sql.native);
    dbms_sql.describe_columns2(v_cursor, v_col_count, v_col_list);
    if (v_col_count > v_max_cols)
    then
        raise_application_error(-20050, 'Max number of selected columns '
            || 'supported is currently ' || v_max_cols);
    /*elsif (v_col_count between 2 and v_max_cols)
    then
        :new.is_simple = 0;
    else
        :new.is_simple = 1;*/
    end if;

    delete from val_parsed_select_list vpsl
    where vpsl.sql_id = :new.sql_id;

    for i in 1..v_col_count
    loop
        if (v_col_list(i).col_type not in (1, 2))
        then
            raise_application_error(-20050, 'Supported data types of selected '
                || 'columns are currently varchar2 and number.');
        end if;
        
        if (v_col_list(i).col_max_len > v_max_col_len)
        then
            raise_application_error(-20050, 'Selected column width/precision of '
                || v_col_list(i).col_max_len || ' exceeds supported maximum.');
        end if;

        insert into val_parsed_select_list
        (
            sql_id, pos, col_type
        )
        values
        (
            :new.sql_id, i, v_col_list(i).col_type
        );
    end loop;
    
    dbms_sql.close_cursor(v_cursor);
end;
/
show errors;
create or replace procedure wm_val_create_sql_data
(
    p_sql_desc      in val_sql.sql_desc%type,
    p_ident         in val_sql.ident%type,
    p_raw_text      in val_sql.raw_text%type,
    p_format_sql    in number default 1
)
as
begin
    delete from val_sql vs
    where vs.ident = p_ident;

    insert into val_sql
    (
        sql_id, sql_desc, ident, create_date_time, mod_date_time, raw_text,
        format_sql
    )
    values
    (
        val_sql_id_seq.nextval, p_sql_desc, p_ident, sysdate, sysdate,
        p_raw_text, p_format_sql
    );
end;
/
show errors;

create or replace procedure wm_val_lock_unlock_job_dtl
(
    p_job_name  in val_job_hdr.job_name%type,
    p_ident     in val_sql.ident%type,
    p_lock      in val_sql.is_locked%type
)
as
    v_vjd_id    val_job_dtl.val_job_dtl_id%type;
begin
    update val_job_dtl vjd
    set vjd.is_locked = p_lock, vjd.mod_date_time = sysdate
    where exists
        (
            select 1
            from val_job_hdr vjh
            where vjh.val_job_hdr_id = vjd.val_job_hdr_id
                and vjh.job_name = p_job_name
        )
        and exists
        (
            select 1
            from val_sql vs
            where vs.sql_id = vjd.sql_id and vs.ident = p_ident
        )
    returning vjd.val_job_dtl_id into v_vjd_id;
    dbms_output.put_line(case when p_lock = 1 then 'Locked' else 'Unlocked' end 
        || ' job dtl ' || v_vjd_id || '(sql: ' || p_ident || ')');
    
    commit;
end;
/
show errors;
create or replace procedure wm_val_create_job_hdr
(
    p_job_name          in val_job_hdr.job_name%type,
    p_job_desc          in val_job_hdr.job_desc%type,
    p_vjh_id            out number
)
as
begin
    delete from val_job_hdr vjh
    where vjh.job_name = p_job_name;

    -- create a scenario as a 'job' and link pertinent sqls to it as job dtls
    p_vjh_id := val_job_hdr_id_seq.nextval;
    insert into val_job_hdr
    (
        val_job_hdr_id, job_name, job_desc, create_date_time, mod_date_time
    )
    values
    (
        p_vjh_id, p_job_name, p_job_desc, sysdate, sysdate
    );
end;
/
show errors;

create or replace procedure wm_val_create_job_dtl
(
    p_vjh_id    in val_job_hdr.val_job_hdr_id%type,
    p_ident     in val_sql.ident%type default null,
    p_sql_id    in val_job_dtl.sql_id%type default null,
    p_user_id   in user_profile.user_id%type default null,
    p_vjd_id    out val_job_dtl.val_job_dtl_id%type
)
as
    v_sql_id    val_job_dtl.sql_id%type := p_sql_id;
begin
    if ((p_ident is null and p_sql_id is null)
        or (p_ident is not null and p_sql_id is not null))
    then
        raise_application_error(-20050, 'Invalid SQL data.');
    end if;

    if (p_ident is not null)
    then
        begin
            select vs.sql_id
            into v_sql_id 
            from val_sql vs 
            where vs.ident = p_ident;
        exception
            when no_data_found then
                raise_application_error(-20050, 'Cant find sql with identifier ' || p_ident);
        end;
    end if;

    p_vjd_id := val_job_dtl_id_seq.nextval;
    insert into val_job_dtl
    (
        val_job_hdr_id, val_job_dtl_id, sql_id, create_date_time, mod_date_time,
        user_id
    )
    values
    (
        p_vjh_id, p_vjd_id, v_sql_id, sysdate, sysdate, p_user_id
    );
end;
/
show errors;
create or replace procedure wm_val_add_result_to_sql
(
    p_vjd_id          in val_job_dtl.val_job_dtl_id%type,
    p_row_num         in val_expected_results.row_num%type, 
    p_pos             in val_expected_results.pos%type,
    p_expected_value  in val_expected_results.expected_value%type
)
as
begin
    -- persist expected result data
    insert into val_expected_results
    (
        val_job_dtl_id, row_num, pos, expected_value
    )
    values
    (
        p_vjd_id, p_row_num, p_pos, p_expected_value
    );
end;
/
show errors;
create or replace procedure wm_val_add_bind_var_to_sql
(
    p_vjd_id    in val_job_dtl.val_job_dtl_id%type,
    p_bind_var  in val_sql_binds.bind_var%type,
    p_bind_val  in val_sql_binds.bind_val%type
)
as
begin
    -- persist any preset bind values for the sqls to be used within the above
    -- job details
    -- UI should validate bind var entry in the context of each sql
    -- bind vars known only at runtime need to be filled in by the calling code
    -- or a wrapper at some point prior to validation
    insert into val_sql_binds
    (
        val_job_dtl_id, bind_var, bind_val
    )
    values
    (
        p_vjd_id, lower(p_bind_var), p_bind_val
    );
end;
/
show errors;
create or replace procedure wm_val_clone_job
(
    p_old_job_name        in val_job_hdr.job_name%type,
    p_new_job_name        in val_job_hdr.job_name%type,
    p_new_job_desc        in val_job_hdr.job_desc%type default null
)
as
    v_new_vjh_id    val_job_hdr.val_job_hdr_id%type;
    v_new_vjd_id    val_job_dtl.val_job_dtl_id%type;
begin
    wm_val_create_job_hdr(p_job_name => p_new_job_name,
        p_job_desc => p_new_job_desc, p_vjh_id => v_new_vjh_id);
    
    for dtl_rec in
    (
        select vjd.val_job_dtl_id old_dtl, vs.ident ident
        from val_job_dtl vjd
        join val_sql vs on vs.sql_id = vjd.sql_id
        where vjd.val_job_hdr_id =
        (
            select vjh.val_job_hdr_id
            from val_job_hdr vjh
            where vjh.job_name = p_old_job_name
        )
    )
    loop
        wm_val_create_job_dtl(v_new_vjh_id, p_ident => dtl_rec.ident,
            p_vjd_id => v_new_vjd_id);

        for bind_rec in
        (
            select vsb.bind_var, vsb.bind_val
            from val_sql_binds vsb
            where vsb.val_job_dtl_id = dtl_rec.old_dtl
        )
        loop
            wm_val_add_bind_var_to_sql(v_new_vjd_id,
                p_bind_var => bind_rec.bind_var, p_bind_val => bind_rec.bind_val);
        end loop;
        
        for result_rec in
        (
            select ver.row_num, ver.pos, ver.expected_value
            from val_expected_results ver
            where ver.val_job_dtl_id = dtl_rec.old_dtl
        )
        loop
            wm_val_add_result_to_sql(v_new_vjd_id, p_row_num => result_rec.row_num,
                p_pos => result_rec.pos, 
                p_expected_value => result_rec.expected_value);
        end loop;
    end loop;

    commit;
end;
/
show errors;

create or replace force view val_job_result_summ_vw
(
    flow_name, pgm_id, intrnl_job_name, curr_num_sqls, curr_disabled, val_txn_id,
    passed, failed, first_sql_end_time, last_sql_end_time
)
as
with curr_job_data as
(
    select vjh.job_name, vjh.val_job_hdr_id, count(*) curr_num_sqls,
        sum(case when vjd.is_locked = 1 or vs.is_locked = 1 then 1 
        else 0 end) curr_disabled
    from val_job_hdr vjh
    join val_job_dtl vjd on vjd.val_job_hdr_id = vjh.val_job_hdr_id
    join val_sql vs on vs.sql_id = vjd.sql_id
    group by vjh.job_name, vjh.val_job_hdr_id
)
select vjs.app_hook flow_name, vjs.pgm_id, cjd.job_name intrnl_job_name, 
    min(cjd.curr_num_sqls) curr_num_sqls, min(cjd.curr_disabled) curr_disabled,
    vrh.txn_id val_txn_id, sum(vrh.last_run_passed) passed,
    sum(decode(vrh.last_run_passed, 0, 1, decode(vrh.txn_id, null, null, 0))) failed,
    min(vrh.create_date_time) first_sql_end_time,
    max(vrh.create_date_time) last_sql_end_time
from val_job_seq vjs
join curr_job_data cjd on cjd.job_name = vjs.job_name
join val_job_dtl vjd on vjd.val_job_hdr_id = cjd.val_job_hdr_id
left join val_result_hist vrh on vrh.val_job_dtl_id = vjd.val_job_dtl_id
group by vjs.app_hook, vjs.pgm_id, cjd.job_name, vrh.txn_id;
/

create or replace function wm_val_get_txn_id
return val_result_hist.txn_id%type
as
    v_txn_id val_result_hist.txn_id%type;
begin
    select t.txn_id
    into v_txn_id
    from tmp_val_parm t;
    
    return v_txn_id;
end;
/
show errors;
create or replace procedure wm_val_auton_log_result
(
    p_user_id         in user_profile.user_id%type,
    p_txn_id          in val_result_hist.txn_id%type,
    p_val_job_dtl_id  in val_job_dtl.val_job_dtl_id%type,
    p_msg             in val_result_hist.msg%type,
    p_last_run_passed in val_result_hist.last_run_passed%type
)
as
    pragma autonomous_transaction;
begin
    insert into val_result_hist
    (
        txn_id, user_id, create_date_time, val_job_dtl_id, last_run_passed, msg
    )
    values
    (
        p_txn_id, p_user_id, sysdate, p_val_job_dtl_id, p_last_run_passed, p_msg
    );
    
    commit;
end;
/
show errors;
create or replace procedure wm_val_add_job_result
(
    p_val_job_dtl_id  in val_job_dtl.val_job_dtl_id%type,
    p_msg             in val_result_hist.msg%type,
    p_last_run_passed in val_result_hist.last_run_passed%type
)
as
    v_user_id         user_profile.user_id%type;
    v_txn_id          val_result_hist.txn_id%type;
begin
    select t.txn_id, t.user_id
    into v_txn_id, v_user_id
    from tmp_val_parm t;

    wm_val_auton_log_result(v_user_id, v_txn_id, p_val_job_dtl_id, p_msg,
        p_last_run_passed);
    wm_val_log('Validation ' || p_val_job_dtl_id 
        || case p_last_run_passed when 1 then ' passed' else ' failed' end
        || ', msg: <' || p_msg || '>');
end;
/
show errors;
create or replace procedure wm_val_get_runtime_bind_vars
(
    p_user_id           in varchar2,
    p_job_name          in val_job_hdr.job_name%type,
    p_rt_bind_list_csv  in varchar2,
    p_txn_id            in val_result_hist.txn_id%type
)
as
    v_rt_bind_list_csv_lc varchar2(1000) := lower(p_rt_bind_list_csv);
    v_bind_val            val_runtime_binds.bind_val%type;
    v_bind_val_len        number(5);
    v_start_pos           number(5);
    pragma autonomous_transaction;
begin
    for sql_rec in
    (
        select vjd.val_job_dtl_id, vsb.bind_var
        from val_job_hdr vjh
        join val_job_dtl vjd on vjd.val_job_hdr_id = vjh.val_job_hdr_id
            and vjd.is_locked = 0
        join val_sql_binds vsb on vsb.val_job_dtl_id = vjd.val_job_dtl_id
            and vsb.bind_val is null
        where vjh.job_name = p_job_name and vjh.is_locked = 0
    )
    loop
        v_start_pos := instr(v_rt_bind_list_csv_lc, sql_rec.bind_var, 1, 1);
        if (v_start_pos = 0)
        then
            continue;
        end if;
        
        v_start_pos := v_start_pos + length(sql_rec.bind_var) + 1;
        v_bind_val_len := instr(p_rt_bind_list_csv, ',', v_start_pos, 1)
            - v_start_pos;
        v_bind_val := substr(p_rt_bind_list_csv, v_start_pos, v_bind_val_len);

        insert into val_runtime_binds
        (
            txn_id, val_job_dtl_id, bind_var, bind_val, user_id, 
            create_date_time, mod_date_time
        )
        values
        (
            p_txn_id, sql_rec.val_job_dtl_id, sql_rec.bind_var, v_bind_val,
            p_user_id, sysdate, sysdate
        );
    end loop;
    
    commit;
end;
/
show errors;
create or replace procedure wm_val_exec_sql
(
    p_cursor          in number,
    p_val_job_dtl_id  in val_job_dtl.val_job_dtl_id%type,
    p_sql_id          in val_job_dtl.sql_id%type,
    p_sql_text        in val_sql.sql_text%type,
    p_bind_var_list   in val_sql.bind_var_list%type
)
as
    v_txn_id            val_result_hist.txn_id%type := wm_val_get_txn_id();
    v_sql_passed        number(1);
    v_num_actual_rows   number(9);
    v_num_expected_rows number(9);
    v_curr_row_num      number(9);
    v_int_col           number(9);
    v_varchar2_col      varchar2(50);
    v_max_char_len      number(3) := 50;
    v_msg               val_result_hist.msg%type;
begin
    wm_val_log('SQL ' || p_sql_id || ': ' || substr(p_sql_text, 1, 500));
    v_sql_passed := 0;
    dbms_sql.parse(p_cursor, p_sql_text, dbms_sql.native);

    -- run time bind values can override pre-configured values
    for bind_rec in
    (
        select vsb.bind_var, coalesce(vrb.bind_val, vsb.bind_val) bind_val
        from val_sql_binds vsb
        left join val_runtime_binds vrb on vrb.bind_var = vsb.bind_var
            and vrb.val_job_dtl_id = vsb.val_job_dtl_id
            and vrb.txn_id = v_txn_id
        where vsb.val_job_dtl_id = p_val_job_dtl_id
    )
    loop
        if (instr(p_bind_var_list, bind_rec.bind_var, 1, 1) = 0)
        then
            raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to configured bind var :' || bind_rec.bind_var 
                || ' not found in parsed bind list');
        end if;

        if (bind_rec.bind_val is null)
        then
            raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to bind var :' || bind_rec.bind_var 
                || ' not being bound');
        end if;
        dbms_sql.bind_variable(p_cursor, bind_rec.bind_var, bind_rec.bind_val);
        wm_val_log('Bound ' || bind_rec.bind_val || ' to :' || bind_rec.bind_var,
            p_sql_log_lvl => 2);
    end loop;

    -- define datatype for each selected column in the sql
    for col_rec in
    (
        select vpsl.pos, vpsl.col_type
        from val_parsed_select_list vpsl
        where vpsl.sql_id = p_sql_id
        order by vpsl.pos
    )
    loop
        if (col_rec.col_type = 2)
        then
            dbms_sql.define_column(p_cursor, col_rec.pos, v_int_col);
            wm_val_log('Defined integer col at pos ' || col_rec.pos, p_sql_log_lvl => 2);
        elsif (col_rec.col_type = 1)
        then
            dbms_sql.define_column(p_cursor, col_rec.pos, v_varchar2_col,
                v_max_char_len);
            wm_val_log('Defined varchar2 col at pos ' || col_rec.pos, p_sql_log_lvl => 2);
        else
            raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to finding unsupported data type '
                || col_rec.col_type);
        end if;
    end loop;

    select max(ver.row_num)
    into v_num_expected_rows
    from val_expected_results ver
    where ver.val_job_dtl_id = p_val_job_dtl_id;

    -- run it and fetch returned rows
    v_num_actual_rows := dbms_sql.execute(p_cursor);
    /*
    -- enable this for sql injection
    if (v_num_actual_rows != v_num_expected_rows)
    then
        update val_job_dtl vjd
        set vjd.mod_date_time = sysdate, vjd.last_run_passed = 0
        where vjd.val_job_dtl_id = p_val_job_dtl_id;
        dbms_output.put_line('Skipped processing Job dtl ' || p_val_job_dtl_id || ',sql ' || p_sql_id
            || ' expected rows ' || v_num_expected_rows || ', actual ' || v_num_actual_rows);
        
        -- skip validations of row level output
        continue;
    end if;
    */

    v_sql_passed := 0;
    v_curr_row_num := 0;
    while (dbms_sql.fetch_rows(p_cursor) > 0)
    loop
        -- for each selected column, based on datatype, get selected data
        -- into appropriate vars and compare
        v_curr_row_num := v_curr_row_num + 1;
        wm_val_log('Processing row ' || v_curr_row_num, p_sql_log_lvl => 2);
        if (v_curr_row_num > v_num_expected_rows)
        then
            raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to fetching more rows than expected (' 
                || v_num_expected_rows || ')');
        end if;

        for col_rec in
        (
            -- UI needs to ensure that all expected result columns are entered into the results table
            select vpsl.pos, vpsl.col_type, ver.expected_value
            from val_parsed_select_list vpsl
            join val_expected_results ver on ver.pos = vpsl.pos
                and ver.row_num = v_curr_row_num
                and ver.val_job_dtl_id = p_val_job_dtl_id
            where vpsl.sql_id = p_sql_id
            order by vpsl.pos
        )
        loop
            v_msg := null;
            v_varchar2_col := null;
            v_int_col := null;
            if (col_rec.col_type = 2)
            then
                dbms_sql.column_value(p_cursor, col_rec.pos, v_int_col);
                v_sql_passed := case when v_int_col = to_number(col_rec.expected_value)
                    then 1 else 0 end;
            elsif (col_rec.col_type = 1)
            then
                dbms_sql.column_value(p_cursor, col_rec.pos, v_varchar2_col);
                v_sql_passed := case when v_varchar2_col = col_rec.expected_value
                    then 1 else 0 end;
            else
                raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to finding unsupported data type '
                || col_rec.col_type);
            end if;

            wm_val_log('Expecting ' || col_rec.expected_value || ' at ('
                || col_rec.pos || ',' || v_curr_row_num || '), found '
                || coalesce(v_varchar2_col, to_char(v_int_col), 'null'), 
                p_sql_log_lvl => 2);

            if (v_sql_passed = 0)
            then
                raise_application_error(-20050, 'Sql ' || p_sql_id
                    || ' failed due to finding ' 
                    || coalesce(v_varchar2_col, to_char(v_int_col), 'null')
                    || ' in (row ' || v_curr_row_num || ', Pos ' || col_rec.pos
                    || '), expected ' || col_rec.expected_value );
            end if;
        end loop; -- columns fetch

        if (v_sql_passed = 0)
        then
            raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to not finding matching result data');
        end if;
    end loop; -- rows fetch

    if (v_curr_row_num < v_num_expected_rows and v_msg is null)
    then
        raise_application_error(-20050, 'Sql ' || p_sql_id || 
            ' failed due to returning fewer rows (' || v_curr_row_num 
            || ') than expected (' || v_num_expected_rows || ')');
    end if;

    wm_val_add_job_result(p_val_job_dtl_id,
        case v_sql_passed when 0 then v_msg else null end, v_sql_passed);
end;
/
show errors;
create or replace procedure wm_val_exec_job_intrnl
(
    p_user_id           in user_profile.user_id%type,
    p_job_name          in val_job_hdr.job_name%type,
    p_txn_id            in val_result_hist.txn_id%type,
    p_rt_bind_list_csv  in varchar2
)
as
    v_cursor              int := dbms_sql.open_cursor;
    udef_excp             exception;
    pragma exception_init (udef_excp, -20050);
begin
    if (p_rt_bind_list_csv is not null)
    then
        wm_val_get_runtime_bind_vars(p_user_id, p_job_name, p_rt_bind_list_csv,
            p_txn_id);
    end if;

    for sql_rec in
    (
        select vjd.val_job_dtl_id, vjd.sql_id, vs.sql_text, vs.bind_var_list
        from val_job_hdr vjh
        join val_job_dtl vjd on vjd.val_job_hdr_id = vjh.val_job_hdr_id
            and vjd.is_locked = 0
        join val_sql vs on vs.sql_id = vjd.sql_id and vs.is_locked = 0
        where vjh.job_name = p_job_name and vjh.is_locked = 0
        order by vjd.val_job_dtl_id
    )
    loop
        begin
            wm_val_exec_sql(v_cursor, sql_rec.val_job_dtl_id, sql_rec.sql_id,
                sql_rec.sql_text, sql_rec.bind_var_list);
        exception
            when udef_excp then
                wm_val_add_job_result(sql_rec.val_job_dtl_id, p_msg => sqlerrm,
                    p_last_run_passed => 0);
        end;
    end loop;

    dbms_sql.close_cursor(v_cursor);
end;
/
show errors;
create or replace procedure wm_val_exec_job
(
    p_user_id           in  user_profile.user_id%type,
    p_app_hook          in  val_job_seq.app_hook%type,
    p_pgm_id            in  val_job_seq.pgm_id%type,
    p_rt_bind_list_csv  in  varchar2,
    p_log_lvl           number default 0,
    p_txn_id            out val_result_hist.txn_id%type
)
as
begin
    for job_rec in
    (
        select rownum rn, vjs.job_name
        from val_job_seq vjs
        where vjs.app_hook = p_app_hook
            and vjs.pgm_id = coalesce(p_pgm_id, vjs.pgm_id)
        order by vjs.seq_nbr
    )
    loop
        if (job_rec.rn = 1)
        then
            p_txn_id := val_txn_id_seq.nextval;
            delete from tmp_val_parm t;
            insert into tmp_val_parm
            (
                user_id, txn_id, pgm_id, log_lvl, module, msg_id, ref_code_1,
                ref_value_2
            )
            values
            (
                p_user_id, p_txn_id, 'wm_val_exec_job', p_log_lvl, 'WAVE', 
                '1094', 'VAL', p_app_hook || '|' || p_pgm_id
            );
        end if;

        wm_val_log('Beginning validation of ' || job_rec.job_name
            || ', runtime binds are <' || substr(p_rt_bind_list_csv, 1, 400)
            || '>');
        wm_val_exec_job_intrnl(p_user_id, job_rec.job_name, p_txn_id, 
            p_rt_bind_list_csv);
        wm_val_log('Completed validation ' || job_rec.job_name);
    end loop;
end;
/
show errors;

create or replace procedure wm_val_exec_job
(
    p_user_id           in  user_profile.user_id%type,
    p_app_hook          in  val_job_seq.app_hook%type,
    p_pgm_id            in  val_job_seq.pgm_id%type,
    p_rt_bind_list_csv  in  varchar2,
    p_log_lvl           number default 0,
    p_txn_id            in out val_result_hist.txn_id%type
)
as
begin
    for job_rec in
    (
        select rownum rn, vjs.job_name
        from val_job_seq vjs
        where vjs.app_hook = p_app_hook
            and vjs.pgm_id = coalesce(p_pgm_id, vjs.pgm_id)
        order by vjs.seq_nbr
    )
    loop
        if (job_rec.rn = 1)
        then
            if (p_txn_id is null)
            then
                p_txn_id := val_txn_id_seq.nextval;
            end if;
            delete from tmp_val_parm t;
            insert into tmp_val_parm
            (
                user_id, txn_id, pgm_id, log_lvl, module, msg_id, ref_code_1,
                ref_value_2
            )
            values
            (
                p_user_id, p_txn_id, 'wm_val_exec_job', p_log_lvl, 'WAVE', 
                '1094', 'VAL', p_app_hook || '|' || p_pgm_id
            );
        end if;

        wm_val_log('Beginning validation of ' || job_rec.job_name
            || ', runtime binds are <' || substr(p_rt_bind_list_csv, 1, 400)
            || '>');
        wm_val_exec_job_intrnl(p_user_id, job_rec.job_name, p_txn_id, 
            p_rt_bind_list_csv);
        wm_val_log('Completed validation ' || job_rec.job_name);
    end loop;
end;
/
show errors;

-- DBTicket TE-1312
MERGE INTO RULE_COLM_LIST A
     USING (SELECT 'ORDERS' TBL_NAME,
                   'REF_FIELD_1' COLM_NAME,
                   'Orders Ref Field1' COLM_DESC
              FROM DUAL) B
        ON (    A.COLM_NAME = B.COLM_NAME
            AND A.TBL_NAME = B.TBL_NAME
            AND A.COLM_DESC = B.COLM_DESC)
WHEN NOT MATCHED
THEN
   INSERT     (RULE_COLM_LIST_ID,
               TBL_NAME,
               COLM_NAME,
               COLM_DESC,
               DISBL_FLAG,
               create_date_time,
               mod_date_time,
               USER_ID,
               WM_VERSION_ID,
               RULE_COLM_ID)
       VALUES (RULE_COLM_LIST_ID_SEQ.NEXTVAL,
               'ORDERS',
               'REF_FIELD_1',
               'Orders Ref Field1',
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'WMADMIN',
               1,
               RULE_COLM_LIST_ID_SEQ.NEXTVAL);

MERGE INTO RULE_COLM_LIST A
     USING (SELECT 'ORDERS' TBL_NAME,
                   'REF_FIELD_2' COLM_NAME,
                   'Orders Ref Field2' COLM_DESC
              FROM DUAL) B
        ON (    A.COLM_NAME = B.COLM_NAME
            AND A.TBL_NAME = B.TBL_NAME
            AND A.COLM_DESC = B.COLM_DESC)
WHEN NOT MATCHED
THEN
   INSERT     (RULE_COLM_LIST_ID,
               TBL_NAME,
               COLM_NAME,
               COLM_DESC,
               DISBL_FLAG,
               create_date_time,
               mod_date_time,
               USER_ID,
               WM_VERSION_ID,
               rule_colm_id)
       VALUES (RULE_COLM_LIST_ID_SEQ.NEXTVAL,
               'ORDERS',
               'REF_FIELD_2',
               'Orders Ref Field2',
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'WMADMIN',
               1,
               RULE_COLM_LIST_ID_SEQ.NEXTVAL);


MERGE INTO RULE_COLM_LIST A
     USING (SELECT 'ORDERS' TBL_NAME,
                   'REF_FIELD_3' COLM_NAME,
                   'Orders Ref Field3' COLM_DESC
              FROM DUAL) B
        ON (    A.COLM_NAME = B.COLM_NAME
            AND A.TBL_NAME = B.TBL_NAME
            AND A.COLM_DESC = B.COLM_DESC)
WHEN NOT MATCHED
THEN
   INSERT     (RULE_COLM_LIST_ID,
               TBL_NAME,
               COLM_NAME,
               COLM_DESC,
               DISBL_FLAG,
               create_date_time,
               mod_date_time,
               USER_ID,
               WM_VERSION_ID,
               rule_colm_id)
       VALUES (RULE_COLM_LIST_ID_SEQ.NEXTVAL,
               'ORDERS',
               'REF_FIELD_3',
               'Orders Ref Field3',
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'WMADMIN',
               1,
               RULE_COLM_LIST_ID_SEQ.NEXTVAL);

MERGE INTO RULE_COLM_LIST A
     USING (SELECT 'ORDERS' TBL_NAME,
                   'REF_FIELD_4' COLM_NAME,
                   'Orders Ref Field4' COLM_DESC
              FROM DUAL) B
        ON (    A.COLM_NAME = B.COLM_NAME
            AND A.TBL_NAME = B.TBL_NAME
            AND A.COLM_DESC = B.COLM_DESC)
WHEN NOT MATCHED
THEN
   INSERT     (RULE_COLM_LIST_ID,
               TBL_NAME,
               COLM_NAME,
               COLM_DESC,
               DISBL_FLAG,
               create_date_time,
               mod_date_time,
               USER_ID,
               WM_VERSION_ID,
               rule_colm_id)
       VALUES (RULE_COLM_LIST_ID_SEQ.NEXTVAL,
               'ORDERS',
               'REF_FIELD_4',
               'Orders Ref Field4',
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'WMADMIN',
               1,
               RULE_COLM_LIST_ID_SEQ.NEXTVAL);

MERGE INTO RULE_COLM_LIST A
     USING (SELECT 'ORDERS' TBL_NAME,
                   'REF_FIELD_5' COLM_NAME,
                   'Orders Ref Field5' COLM_DESC
              FROM DUAL) B
        ON (    A.COLM_NAME = B.COLM_NAME
            AND A.TBL_NAME = B.TBL_NAME
            AND A.COLM_DESC = B.COLM_DESC)
WHEN NOT MATCHED
THEN
   INSERT     (RULE_COLM_LIST_ID,
               TBL_NAME,
               COLM_NAME,
               COLM_DESC,
               DISBL_FLAG,
               create_date_time,
               mod_date_time,
               USER_ID,
               WM_VERSION_ID,
               rule_colm_id)
       VALUES (RULE_COLM_LIST_ID_SEQ.NEXTVAL,
               'ORDERS',
               'REF_FIELD_5',
               'Orders Ref Field5',
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'WMADMIN',
               1,
               RULE_COLM_LIST_ID_SEQ.NEXTVAL);

MERGE INTO RULE_COLM_LIST A
     USING (SELECT 'ORDERS' TBL_NAME,
                   'REF_FIELD_6' COLM_NAME,
                   'Orders Ref Field6' COLM_DESC
              FROM DUAL) B
        ON (    A.COLM_NAME = B.COLM_NAME
            AND A.TBL_NAME = B.TBL_NAME
            AND A.COLM_DESC = B.COLM_DESC)
WHEN NOT MATCHED
THEN
   INSERT     (RULE_COLM_LIST_ID,
               TBL_NAME,
               COLM_NAME,
               COLM_DESC,
               DISBL_FLAG,
               create_date_time,
               mod_date_time,
               USER_ID,
               WM_VERSION_ID,
               rule_colm_id)
       VALUES (RULE_COLM_LIST_ID_SEQ.NEXTVAL,
               'ORDERS',
               'REF_FIELD_6',
               'Orders Ref Field6',
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'WMADMIN',
               1,
               RULE_COLM_LIST_ID_SEQ.NEXTVAL);

MERGE INTO RULE_COLM_LIST A
     USING (SELECT 'ORDERS' TBL_NAME,
                   'REF_FIELD_7' COLM_NAME,
                   'Orders Ref Field7' COLM_DESC
              FROM DUAL) B
        ON (    A.COLM_NAME = B.COLM_NAME
            AND A.TBL_NAME = B.TBL_NAME
            AND A.COLM_DESC = B.COLM_DESC)
WHEN NOT MATCHED
THEN
   INSERT     (RULE_COLM_LIST_ID,
               TBL_NAME,
               COLM_NAME,
               COLM_DESC,
               DISBL_FLAG,
               create_date_time,
               mod_date_time,
               USER_ID,
               WM_VERSION_ID,
               rule_colm_id)
       VALUES (RULE_COLM_LIST_ID_SEQ.NEXTVAL,
               'ORDERS',
               'REF_FIELD_7',
               'Orders Ref Field7',
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'WMADMIN',
               1,
               RULE_COLM_LIST_ID_SEQ.NEXTVAL);

MERGE INTO RULE_COLM_LIST A
     USING (SELECT 'ORDERS' TBL_NAME,
                   'REF_FIELD_8' COLM_NAME,
                   'Orders Ref Field8' COLM_DESC
              FROM DUAL) B
        ON (    A.COLM_NAME = B.COLM_NAME
            AND A.TBL_NAME = B.TBL_NAME
            AND A.COLM_DESC = B.COLM_DESC)
WHEN NOT MATCHED
THEN
   INSERT     (RULE_COLM_LIST_ID,
               TBL_NAME,
               COLM_NAME,
               COLM_DESC,
               DISBL_FLAG,
               create_date_time,
               mod_date_time,
               USER_ID,
               WM_VERSION_ID,
               rule_colm_id)
       VALUES (RULE_COLM_LIST_ID_SEQ.NEXTVAL,
               'ORDERS',
               'REF_FIELD_8',
               'Orders Ref Field8',
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'WMADMIN',
               1,
               RULE_COLM_LIST_ID_SEQ.NEXTVAL);

MERGE INTO RULE_COLM_LIST A
     USING (SELECT 'ORDERS' TBL_NAME,
                   'REF_FIELD_9' COLM_NAME,
                   'Orders Ref Field9' COLM_DESC
              FROM DUAL) B
        ON (    A.COLM_NAME = B.COLM_NAME
            AND A.TBL_NAME = B.TBL_NAME
            AND A.COLM_DESC = B.COLM_DESC)
WHEN NOT MATCHED
THEN
   INSERT     (RULE_COLM_LIST_ID,
               TBL_NAME,
               COLM_NAME,
               COLM_DESC,
               DISBL_FLAG,
               create_date_time,
               mod_date_time,
               USER_ID,
               WM_VERSION_ID,
               rule_colm_id)
       VALUES (RULE_COLM_LIST_ID_SEQ.NEXTVAL,
               'ORDERS',
               'REF_FIELD_9',
               'Orders Ref Field9',
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'WMADMIN',
               1,
               RULE_COLM_LIST_ID_SEQ.NEXTVAL);

MERGE INTO RULE_COLM_LIST A
     USING (SELECT 'ORDERS' TBL_NAME,
                   'REF_FIELD_10' COLM_NAME,
                   'Orders Ref Field10' COLM_DESC
              FROM DUAL) B
        ON (    A.COLM_NAME = B.COLM_NAME
            AND A.TBL_NAME = B.TBL_NAME
            AND A.COLM_DESC = B.COLM_DESC)
WHEN NOT MATCHED
THEN
   INSERT     (RULE_COLM_LIST_ID,
               TBL_NAME,
               COLM_NAME,
               COLM_DESC,
               DISBL_FLAG,
               create_date_time,
               mod_date_time,
               USER_ID,
               WM_VERSION_ID,
               rule_colm_id)
       VALUES (RULE_COLM_LIST_ID_SEQ.NEXTVAL,
               'ORDERS',
               'REF_FIELD_10',
               'Orders Ref Field10',
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'WMADMIN',
               1,
               RULE_COLM_LIST_ID_SEQ.NEXTVAL);
			   
commit;

-- DBTicket TE-1775
delete from val_tag_type_xref;
delete from val_tag;
delete from val_tag_type;

/*
tag types:
1 - attributes that desribe the selected data; again - this can include
  keywords that convey aggregated or more complicated information
  examples - sum, qty, count, status, variance, data, lpn, invn, 
2 - data source selected or selected from; does not always have to reference a
  table as we may need to convey more complicated information that the SQL may
  compute
  examples - pkt, item, order, pix, invn, alloc, locn, pld, pix, lpn, invn, aid,
          - task, 
3 - driving entity for the SQL
  examples - wave, order, lpn, pix, locn, store, item, invn, ilpn, olpn, 
4 - modules of interest
  examples - pw, agg, cs, wave, packing
5 - verticals
  examples - retail, 
6 - description
7 - 
500 to 999 - custom tag types
  examples - anything custom to enhance searchability
*/

insert into val_tag_type
(
    type, ui_desc
)
select '1', 'What Selected' from dual union all
select '2', 'Data Source' from dual union all
select '3', 'Driver' from dual union all
select '4', 'Modules Of Interest' from dual union all
select '5', 'Verticals' from dual union all
select '6', 'Description' from dual;

insert into val_tag
(
    tag, ui_desc
)
select 'count', 'Count' from dual union all
select 'oli', 'Order Line Item' from dual union all
select 'item', 'Item' from dual union all
select 'olpn', 'oLpn' from dual union all
select 'sum', 'Sum' from dual union all
select 'pkt', 'Pickticket' from dual union all
select 'status', 'Status' from dual union all
select 'store', 'Store' from dual union all
select 'lpn', 'Lpn' from dual union all
select 'pix', 'Pix' from dual union all
select 'wave', 'Wave' from dual union all
select 'retail', 'Retail' from dual union all
select 'ilpn', 'iLpn' from dual union all
select 'locn', 'Location' from dual union all
select 'aid', 'Allocation' from dual union all
select 'pld', 'PLD' from dual union all
select 'invn', 'Inventory' from dual union all
select 'qty', 'Quantity' from dual union all
select 'order', 'Order' from dual union all
select 'task', 'Task' from dual union all
select 'health', 'Health Check' from dual union all
select 'bool', 'Boolean' from dual union all
select 'stddev', 'Standard Deviation' from dual union all
select 'pw', 'Pack Wave' from dual union all
select 'vas', 'VAS' from dual;

insert into val_tag_type_xref
(
    type, tag
)
select '1', 'count' from dual union all
select '1', 'bool' from dual union all
select '1', 'stddev' from dual union all
select '1', 'ilpn' from dual union all
select '1', 'olpn' from dual union all
select '1', 'invn' from dual union all
select '1', 'locn' from dual union all
select '1', 'lpn' from dual union all
select '1', 'oli' from dual union all
select '1', 'qty' from dual union all
select '1', 'status' from dual union all
select '1', 'sum' from dual union all
select '2', 'aid' from dual union all
select '2', 'invn' from dual union all
select '2', 'lpn' from dual union all
select '2', 'oli' from dual union all
select '2', 'order' from dual union all
select '2', 'pix' from dual union all
select '2', 'pkt' from dual union all
select '2', 'pld' from dual union all
select '2', 'task' from dual union all
select '2', 'vas' from dual union all
select '2', 'wave' from dual union all
select '3', 'ilpn' from dual union all
select '3', 'item' from dual union all
select '3', 'locn' from dual union all
select '3', 'olpn' from dual union all
select '3', 'order' from dual union all
select '3', 'status' from dual union all
select '3', 'store' from dual union all
select '3', 'wave' from dual union all
select '3', 'oli' from dual union all
select '4', 'pkt' from dual union all
select '4', 'retail' from dual union all
select '4', 'wave' from dual union all
select '4', 'pw' from dual union all
select '5', 'retail' from dual union all
select '6', 'health' from dual ;

commit;

-- DBTicket TE-2531
alter table val_sql add sql_hints varchar2(200);

create or replace procedure wm_val_add_hints_to_sql
(
    p_hints         in val_sql.sql_hints%type,
    p_ident         in val_sql.ident%type default null,
    p_sql_id        in val_sql.sql_id%type default null
)
as
    v_sql_id        val_sql.sql_id%type;
    v_ident         val_sql.ident%type;
begin
    update val_sql vs
    set vs.sql_hints = trim(p_hints), vs.mod_date_time = sysdate
    where vs.ident = p_ident or vs.sql_id = p_sql_id
    returning vs.sql_id, vs.ident into v_sql_id, v_ident;
    
    dbms_output.put_line('Added hint(s) ' || p_hints || ' to sql '
        || v_sql_id || '(' || v_ident || ')');
    
    commit;
end;
/
show errors;

create or replace procedure wm_val_exec_job_intrnl
(
    p_user_id           in user_profile.user_id%type,
    p_job_name          in val_job_hdr.job_name%type,
    p_txn_id            in val_result_hist.txn_id%type,
    p_rt_bind_list_csv  in varchar2
)
as
    v_cursor              int := dbms_sql.open_cursor;
    udef_excp             exception;
    pragma exception_init (udef_excp, -20050);
begin
    if (p_rt_bind_list_csv is not null)
    then
        wm_val_get_runtime_bind_vars(p_user_id, p_job_name, p_rt_bind_list_csv,
            p_txn_id);
    end if;

    for sql_rec in
    (
        select vjd.val_job_dtl_id, vjd.sql_id, vs.bind_var_list,
            regexp_replace(vs.sql_text, '/\*\+[ ]*HINT[ ]*\*/',
                '/*+ ' || regexp_replace(vs.sql_hints, '[\/*]', '') || '*/', 1, 1, 'i') sql_text
        from val_job_hdr vjh
        join val_job_dtl vjd on vjd.val_job_hdr_id = vjh.val_job_hdr_id
            and vjd.is_locked = 0
        join val_sql vs on vs.sql_id = vjd.sql_id and vs.is_locked = 0
        where vjh.job_name = p_job_name and vjh.is_locked = 0
        order by vjd.val_job_dtl_id
    )
    loop
        begin
            wm_val_exec_sql(v_cursor, sql_rec.val_job_dtl_id, sql_rec.sql_id,
                sql_rec.sql_text, sql_rec.bind_var_list);
        exception
            when udef_excp then
                wm_val_add_job_result(sql_rec.val_job_dtl_id, p_msg => sqlerrm,
                    p_last_run_passed => 0);
        end;
    end loop;

    dbms_sql.close_cursor(v_cursor);
end;
/
show errors;

-- DBTicket TE-1772
UPDATE XMENU_ITEM
   SET SCREEN_MODE = 2
 WHERE NAVIGATION_KEY = '/te/manual/planning/jsp/viewManualShipmentWorkspace.jsflps';

COMMIT;

-- DBTicket TE-2532
update label set value = 'CAPITALIZED TERMS USED H593021EREIN AND NOT OTHERWISE DEFINED SHALL HAVE THE MEANINGS SET FORTH ON EXHIBIT A OF THE SERVICE TERMS AND CONDITIONS .'  where key = 'TheServicesToBe3';

update label set value = ' Client agrees to make payment for Services as provided in the Service Terms and Conditions.'
where key = 'ShipmentsAreSubjectTo2';

update label set value = '                                      THE SERVICES ARE PROVIDED BY UPS -MI "AS IS", WITHOUT WARRANTY OF ANY KIND. WITHOUT LIMIATIONS OF THE FOREGOING, UPS -MI DOES NOT WARRANT THAT THE SERVICES BEING PROVIDED BY UPS -MI HEREUNDER WILL BE PROVIDED FREE OF OMISSIONS, ERRORS, DELAYS OR INTERRUPTIONS.' where key = 'TheServicesProvided';

update label set value = 'PUNITIVE, INDIRECT OR INCIDENTAL DAMAGES, WEHTER ARISING IN CONTRACT, TORT OR OTHERWISE, EVEN IF UP S-MI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. ' where key = 'UPSMIEntireLiability4';

commit;

-- DBTicket TE-2540
MERGE INTO MESSAGE_MASTER A
USING (SELECT  '70061' MSG_ID,
                '70061' KEY,
                'teplanning' ILS_MODULE,
                'ErrorMessage' BUNDLE_NAME
     FROM DUAL) B
     on ( A.KEY=B.KEY and  A.BUNDLE_NAME=B.BUNDLE_NAME )
     WHEN NOT  MATCHED THEN
INSERT(Message_Master_Id,Msg_Id,Key,Ils_Module,Msg_Module,Msg,Bundle_Name,Msg_Class,Msg_Type,Ovride_Role,Log_Flag)
Values(Seq_Message_Master_Id.Nextval, '70061', '70061', 'teplanning', Null, 'Order(s) being processed by Routing Wave. Cannot continue further.','ErrorMessage', Null, Null, Null, Null);

MERGE INTO MESSAGE_MASTER A
USING (SELECT  '70062' MSG_ID,
                '70062' KEY,
                'teplanning' ILS_MODULE,
                'ErrorMessage' BUNDLE_NAME
     FROM DUAL) B
     on ( A.KEY=B.KEY and  A.BUNDLE_NAME=B.BUNDLE_NAME )
     WHEN NOT  MATCHED THEN
INSERT(Message_Master_Id,Msg_Id,Key,Ils_Module,Msg_Module,Msg,Bundle_Name,Msg_Class,Msg_Type,Ovride_Role,Log_Flag)
Values(Seq_Message_Master_Id.Nextval, '70062', '70062','teplanning', Null, 'Order in MLP Workspace. Hence not considered for Routing wave.', 'ErrorMessage', Null, Null, Null, Null);

commit;

-- DBTicket TE-2539
MERGE INTO MESSAGE_MASTER A
USING (SELECT  '70063' MSG_ID,
                '70063' KEY,
                'teplanning' ILS_MODULE,
                'ErrorMessage' BUNDLE_NAME
     FROM DUAL) B
     on ( A.KEY=B.KEY and  A.BUNDLE_NAME=B.BUNDLE_NAME )
     WHEN NOT  MATCHED THEN
INSERT(Message_Master_Id,Msg_Id,Key,Ils_Module,Msg_Module,Msg,Bundle_Name,Msg_Class,Msg_Type,Ovride_Role,Log_Flag)
Values(Seq_Message_Master_Id.Nextval, '70063', '70063','teplanning', Null,'{0} for selected {1}; cannot continue','ErrorMessage', Null,Null,Null,Null);

commit;

-- DBTicket TE-402
create or replace procedure wm_unplan_splits_intrnl
(
    p_user_id          in user_profile.user_id%type,
    p_is_cs_or_cm      in number default 0,
    p_mode             in varchar2 default null
)
as
    type t_shipment_id is table of shipment.shipment_id%type index by binary_integer;
    va_shpmt_id        t_shipment_id;
    v_row_count        number(9) := 0;
begin
    if (p_mode = '_unplan_')
    then
        -- do not unplan a split if any lpn is manifested or above
        delete from tmp_unplan_splits t
        where exists
        (
            select 1
            from lpn l
            where l.order_id = t.order_id
                and coalesce(l.order_split_id, -1) = coalesce(t.order_split_id, -1)
                and l.lpn_type = 1 and l.inbound_outbound_indicator = 'O'
                and l.lpn_facility_status between 40 and 90
        );
        v_row_count := sql%rowcount;
        wm_cs_log('Splits removed from unplanning due to invalid lpn status '
            || v_row_count, p_sql_log_level => case when v_row_count > 0 then 0 else 1 end);
    end if;

    if (p_is_cs_or_cm = 0)
    then
        -- CS currently unassigns lpns separately; this is in order to be
        -- able to consolidate them under an unplanned split
        update lpn l
        set l.tc_shipment_id = null, l.shipment_id = null, l.plan_load_id = null,
            l.ship_via = null, l.tracking_nbr = null, l.alt_tracking_nbr = null,
            l.last_updated_dttm = sysdate, l.last_updated_source = p_user_id,
            l.last_updated_source_type = 1, l.init_ship_via = null,
            l.distribution_leg_carrier_id = null, l.distribution_leg_mode_id = null,
            l.distribution_lev_svce_level_id = null
        where exists
        (
            select 1
            from tmp_unplan_splits t
            where t.order_id = l.order_id
                and coalesce(t.order_split_id, -1) = coalesce(l.order_split_id, -1)
        );
        wm_cs_log('Lpns unassigned ' || sql%rowcount);
    end if;

    insert into tmp_stop_action
    (
        shipment_id, stop_seq, stop_action_seq, static_route_id
    )
    select distinct sao.shipment_id, sao.stop_seq, sao.stop_action_seq, s.static_route_id
    from stop_action_order sao
    join shipment s on s.shipment_id = sao.shipment_id
    where exists
    (
        select 1
        from tmp_unplan_splits t
        where t.order_id = sao.order_id
            and coalesce(t.order_split_id, -1) = coalesce(sao.order_split_id, -1)
    );
    wm_cs_log('Collected stop action rows ' || sql%rowcount, p_sql_log_level => 2);

    delete from order_movement om
    where exists
    (
        select 1
        from tmp_unplan_splits t
        where t.order_id = om.order_id
            and coalesce(t.order_split_id, -1) = coalesce(om.order_split_id, -1)
    );
    wm_cs_log('Deleted OM ' || sql%rowcount);

    delete from stop_action_order sao
    where exists
    (
        select 1
        from tmp_unplan_splits t
        where t.order_id = sao.order_id
            and coalesce(t.order_split_id, -1) = coalesce(sao.order_split_id, -1)
    );
    wm_cs_log('Deleted SAO ' || sql%rowcount);

    delete from stop_action sa
    where exists
        (
            select 1
            from tmp_stop_action t
            where t.shipment_id = sa.shipment_id and t.stop_seq = sa.stop_seq
                and t.stop_action_seq = sa.stop_action_seq
                and t.static_route_id is null
        )
        and not exists
        (
            select 1
            from stop_action_order sao
            where sao.shipment_id = sa.shipment_id
                and sao.stop_seq = sa.stop_seq
                and sao.stop_action_seq = sa.stop_action_seq
        );
    wm_cs_log('Deleted stop action ' || sql%rowcount);

    -- Added call to delete records from STOP EXTN tables.
    te_remove_stop_extn_data;

    delete from stop s
    where exists
        (
            select 1
            from tmp_stop_action t
            where t.shipment_id = s.shipment_id and t.stop_seq = s.stop_seq
        )
        and not exists
        (
            select 1
            from stop_action sa
            where sa.shipment_id = s.shipment_id and sa.stop_seq = s.stop_seq
        );
    wm_cs_log('Deleted stops ' || sql%rowcount);

    -- cancel splits in CS/CM flows as we are recreating and consolidating them
    update order_split os
    set os.order_split_status = 5, os.last_updated_dttm = sysdate,
        os.last_updated_source = p_user_id, os.last_updated_source_type = 1,
        os.shipment_id = null, os.assigned_mot_id = null,
        os.assigned_service_level_id = null, os.assigned_carrier_id = null,
        os.assigned_equipment_id = null, os.dynamic_routing_reqd = null,
        os.line_haul_ship_via = null, os.zone_skip_hub_location_id = null,
        os.distribution_ship_via = null, os.order_loading_seq = null,
        os.is_cancelled = decode(p_is_cs_or_cm, 1, 1, 0),
        os.original_assigned_ship_via = decode(p_is_cs_or_cm, 1, null, os.original_assigned_ship_via)
    where exists
    (
        select 1
        from tmp_unplan_splits t
        where t.order_split_id = os.order_split_id
    );
    wm_cs_log('Splits ' || case p_is_cs_or_cm when 1 then 'cancelled ' else 'unplanned ' end || sql%rowcount);

    merge into orders o
    using
    (
        select t.order_id, coalesce(min(os.order_split_status), 5) order_split_status
        from tmp_unplan_splits t
        left join order_split os on os.order_id = t.order_id and os.is_cancelled = 0
        group by t.order_id
    ) iv on (iv.order_id = o.order_id)
    when matched then
    update set o.actual_cost = null, o.actual_cost_currency_code = null,
        o.assigned_carrier_id = null, o.assigned_mot_id = null,
        o.assigned_service_level_id = null, o.assigned_equipment_id = null,
        o.distribution_ship_via = null, o.dynamic_routing_reqd = null,
        o.last_updated_dttm = sysdate, o.last_updated_source = p_user_id,
        o.last_updated_source_type = 1, o.line_haul_ship_via = null,
        o.shipment_id = null, o.tc_shipment_id = null,
        o.zone_skip_hub_location_id = null, o.order_loading_seq = null,
        o.path_id = null, o.path_set_id = null, o.plan_d_facility_alias_id = null,
        o.plan_d_facility_id = null,
        o.order_status = iv.order_split_status
    where iv.order_split_status != o.order_status or o.shipment_id is not null;
    wm_cs_log('Orders needing unplanning ' || sql%rowcount);

    if (p_is_cs_or_cm = 0)
    then
        -- assumption: a shipment is never empty when closed, so complete 
        -- unplanning isnt realistic for CS/CM; in other flows, delete/cancel 
        -- shpmts if all orders are completely unplanned
        select distinct t.shipment_id
        bulk collect into va_shpmt_id
        from tmp_stop_action t
        where not exists
        (
            select 1
            from stop_action_order sao
            where sao.shipment_id = t.shipment_id
        );
        wm_cs_log('Stop action rows to remove ' || sql%rowcount, p_sql_log_level => 2);
    
        forall i in 1..va_shpmt_id.count
        delete from shipment_accessorial sa
        where sa.shipment_id = va_shpmt_id(i);
        wm_cs_log('Deleted accessorials ' || sql%rowcount);
        
        forall i in 1..va_shpmt_id.count
        delete from shipment_commodity sc
        where sc.shipment_id = va_shpmt_id(i);
        wm_cs_log('Deleted commodities ' || sql%rowcount);
        
        forall i in 1..va_shpmt_id.count
        delete from shipment_size ss
        where ss.shipment_id = va_shpmt_id(i);
        wm_cs_log('Deleted shpmt sizes ' || sql%rowcount);
    
        forall i in 1..va_shpmt_id.count
        update shipment s
        set s.is_cancelled = 1, s.shipment_status = 120, s.o_address = null,
           s.o_city = null, s.o_country_code = null, s.o_county = null,
           s.o_facility_id = null, s.o_postal_code = null, s.o_state_prov = null,
           s.d_address = null, s.d_city = null, s.d_country_code = null,
           s.d_county = null, s.d_facility_id = null, s.d_facility_number = null,
           s.d_postal_code = null, s.d_state_prov = null, s.total_cost = null,
           s.linehaul_cost = null, s.num_stops = 0, s.num_docks = 0,
           s.direct_distance = null, s.distance = null,
           s.out_of_route_distance = null, s.estimated_cost = null,
           s.earned_income = null, s.assigned_carrier_id = null,
           s.assigned_service_level_id = null, s.assigned_mot_id = null,
           s.assigned_equipment_id = null, s.assigned_ship_via = null,
           s.last_updated_dttm = sysdate, s.last_updated_source = p_user_id,
           s.last_updated_source_type = 1
        where s.shipment_id = va_shpmt_id(i);
        wm_cs_log('Shipments cancelled ' || sql%rowcount);
    end if;
end;
/

-- DBTicket TE-2543
create or replace procedure wm_unplan_splits_intrnl
(
    p_user_id          in user_profile.user_id%type,
    p_is_cs_or_cm      in number default 0,
    p_mode             in varchar2 default null
)
as
    type t_shipment_id is table of shipment.shipment_id%type index by binary_integer;
    va_shpmt_id        t_shipment_id;
    v_row_count        number(9) := 0;
begin
    if (p_mode = '_unplan_')
    then
        -- do not unplan a split if any lpn is manifested or above
        delete from tmp_unplan_splits t
        where exists
        (
            select 1
            from lpn l
            where l.order_id = t.order_id
                and coalesce(l.order_split_id, -1) = coalesce(t.order_split_id, -1)
                and l.lpn_type = 1 and l.inbound_outbound_indicator = 'O'
                and l.lpn_facility_status between 40 and 90
        );
        v_row_count := sql%rowcount;
        wm_cs_log('Splits removed from unplanning due to invalid lpn status '
            || v_row_count, p_sql_log_level => case when v_row_count > 0 then 0 else 1 end);
    end if;

    if (p_is_cs_or_cm = 0)
    then
        -- CS currently unassigns lpns separately; this is in order to be
        -- able to consolidate them under an unplanned split
        update lpn l
        set l.tc_shipment_id = null, l.shipment_id = null, l.plan_load_id = null,
            l.ship_via = null, l.tracking_nbr = null, l.alt_tracking_nbr = null,
            l.last_updated_dttm = sysdate, l.last_updated_source = p_user_id,
            l.last_updated_source_type = 1, l.init_ship_via = null,
            l.distribution_leg_carrier_id = null, l.distribution_leg_mode_id = null,
            l.distribution_lev_svce_level_id = null
        where exists
        (
            select 1
            from tmp_unplan_splits t
            where t.order_id = l.order_id
                and coalesce(t.order_split_id, -1) = coalesce(l.order_split_id, -1)
        );
        wm_cs_log('Lpns unassigned ' || sql%rowcount);
    end if;

    insert into tmp_stop_action
    (
        shipment_id, stop_seq, stop_action_seq, static_route_id
    )
    select distinct sao.shipment_id, sao.stop_seq, sao.stop_action_seq, s.static_route_id
    from stop_action_order sao
    join shipment s on s.shipment_id = sao.shipment_id
    where exists
    (
        select 1
        from tmp_unplan_splits t
        where t.order_id = sao.order_id
            and coalesce(t.order_split_id, -1) = coalesce(sao.order_split_id, -1)
    );
    wm_cs_log('Collected stop action rows ' || sql%rowcount, p_sql_log_level => 2);

    delete from order_movement om
    where exists
    (
        select 1
        from tmp_unplan_splits t
        where t.order_id = om.order_id
            and coalesce(t.order_split_id, -1) = coalesce(om.order_split_id, -1)
    );
    wm_cs_log('Deleted OM ' || sql%rowcount);

    delete from stop_action_order sao
    where exists
    (
        select 1
        from tmp_unplan_splits t
        where t.order_id = sao.order_id
            and coalesce(t.order_split_id, -1) = coalesce(sao.order_split_id, -1)
    );
    wm_cs_log('Deleted SAO ' || sql%rowcount);

    delete from stop_action sa
    where exists
        (
            select 1
            from tmp_stop_action t
            where t.shipment_id = sa.shipment_id and t.stop_seq = sa.stop_seq
                and t.stop_action_seq = sa.stop_action_seq
                and t.static_route_id is null
        )
        and not exists
        (
            select 1
            from stop_action_order sao
            where sao.shipment_id = sa.shipment_id
                and sao.stop_seq = sa.stop_seq
                and sao.stop_action_seq = sa.stop_action_seq
        );
    wm_cs_log('Deleted stop action ' || sql%rowcount);

    -- Added call to delete records from STOP EXTN tables.
    te_remove_stop_extn_data;

    delete from stop s
    where exists
        (
            select 1
            from tmp_stop_action t
            where t.shipment_id = s.shipment_id and t.stop_seq = s.stop_seq
        )
        and not exists
        (
            select 1
            from stop_action sa
            where sa.shipment_id = s.shipment_id and sa.stop_seq = s.stop_seq
        );
    wm_cs_log('Deleted stops ' || sql%rowcount);

    -- cancel splits in CS/CM flows as we are recreating and consolidating them
    update order_split os
    set os.order_split_status = 5, os.last_updated_dttm = sysdate,
        os.last_updated_source = p_user_id, os.last_updated_source_type = 1,
        os.shipment_id = null, os.assigned_mot_id = null,
        os.assigned_service_level_id = null, os.assigned_carrier_id = null,
        os.assigned_equipment_id = null, os.dynamic_routing_reqd = null,
        os.line_haul_ship_via = null, os.zone_skip_hub_location_id = null,
        os.distribution_ship_via = null, os.order_loading_seq = null,
        os.is_cancelled = decode(p_is_cs_or_cm, 1, 1, 0),
        os.original_assigned_ship_via = decode(p_is_cs_or_cm, 1, null, os.original_assigned_ship_via),
        os.cons_run_id = null
    where exists
    (
        select 1
        from tmp_unplan_splits t
        where t.order_split_id = os.order_split_id
    );
    wm_cs_log('Splits ' || case p_is_cs_or_cm when 1 then 'cancelled ' else 'unplanned ' end || sql%rowcount);

    merge into orders o
    using
    (
        select t.order_id, coalesce(min(os.order_split_status), 5) order_split_status
        from tmp_unplan_splits t
        left join order_split os on os.order_id = t.order_id and os.is_cancelled = 0
        group by t.order_id
    ) iv on (iv.order_id = o.order_id)
    when matched then
    update set o.actual_cost = null, o.actual_cost_currency_code = null,
        o.assigned_carrier_id = null, o.assigned_mot_id = null,
        o.assigned_service_level_id = null, o.assigned_equipment_id = null,
        o.distribution_ship_via = null, o.dynamic_routing_reqd = null,
        o.last_updated_dttm = sysdate, o.last_updated_source = p_user_id,
        o.last_updated_source_type = 1, o.line_haul_ship_via = null,
        o.shipment_id = null, o.tc_shipment_id = null,
        o.zone_skip_hub_location_id = null, o.order_loading_seq = null,
        o.path_id = null, o.path_set_id = null, o.plan_d_facility_alias_id = null,
        o.plan_d_facility_id = null, o.cons_run_id = null, 
        o.order_status = iv.order_split_status
    where iv.order_split_status != o.order_status or o.shipment_id is not null;
    wm_cs_log('Orders needing unplanning ' || sql%rowcount);


    if (p_is_cs_or_cm = 0)
    then
    
       -- nullify cons run id from orders if its not done 
       -- if order was partially unplanned
       update orders o set o.cons_run_id = null
       where o.order_status = 5 and o.cons_run_id is not null
           and exists
           (
               select 1
               from tmp_unplan_splits t
               where t.order_id = o.order_id
            );
        wm_cs_log('Nullify cons_run_id on orders partially unplanned '|| sql%rowcount);
        
        -- assumption: a shipment is never empty when closed, so complete 
        -- unplanning isnt realistic for CS/CM; in other flows, delete/cancel 
        -- shpmts if all orders are completely unplanned
        select distinct t.shipment_id
        bulk collect into va_shpmt_id
        from tmp_stop_action t
        where not exists
        (
            select 1
            from stop_action_order sao
            where sao.shipment_id = t.shipment_id
        );
        wm_cs_log('Stop action rows to remove ' || sql%rowcount, p_sql_log_level => 2);
    
        forall i in 1..va_shpmt_id.count
        delete from shipment_accessorial sa
        where sa.shipment_id = va_shpmt_id(i);
        wm_cs_log('Deleted accessorials ' || sql%rowcount);
        
        forall i in 1..va_shpmt_id.count
        delete from shipment_commodity sc
        where sc.shipment_id = va_shpmt_id(i);
        wm_cs_log('Deleted commodities ' || sql%rowcount);
        
        forall i in 1..va_shpmt_id.count
        delete from shipment_size ss
        where ss.shipment_id = va_shpmt_id(i);
        wm_cs_log('Deleted shpmt sizes ' || sql%rowcount);
    
        forall i in 1..va_shpmt_id.count
        update shipment s
        set s.is_cancelled = 1, s.shipment_status = 120, s.o_address = null,
           s.o_city = null, s.o_country_code = null, s.o_county = null,
           s.o_facility_id = null, s.o_postal_code = null, s.o_state_prov = null,
           s.d_address = null, s.d_city = null, s.d_country_code = null,
           s.d_county = null, s.d_facility_id = null, s.d_facility_number = null,
           s.d_postal_code = null, s.d_state_prov = null, s.total_cost = null,
           s.linehaul_cost = null, s.num_stops = 0, s.num_docks = 0,
           s.direct_distance = null, s.distance = null,
           s.out_of_route_distance = null, s.estimated_cost = null,
           s.earned_income = null, s.assigned_carrier_id = null,
           s.assigned_service_level_id = null, s.assigned_mot_id = null,
           s.assigned_equipment_id = null, s.assigned_ship_via = null,
           s.last_updated_dttm = sysdate, s.last_updated_source = p_user_id,
           s.last_updated_source_type = 1
        where s.shipment_id = va_shpmt_id(i);
        wm_cs_log('Shipments cancelled ' || sql%rowcount);
    end if;
end;
/

-- DBTicket TE-2550
insert into val_tag
(
    tag, ui_desc
)
select 'asn', 'ASN' from dual;

insert into val_tag_type_xref
(
    type, tag
)
select '2', 'asn' from dual union all
select '3', 'asn' from dual;

commit;

-- DBTicket TE-2557
update label set value = 'CAPITALIZED TERMS USED HEREIN AND NOT OTHERWISE DEFINED SHALL HAVE THE MEANINGS SET FORTH ON EXHIBIT A OF THE SERVICE TERMS AND CONDITIONS.' 
where key = 'TheServicesToBe3';

update label set value = '                                                                                                                                                                                                          		               Client agrees to make payment for Services as provided in the Service Terms and Conditions.'
where key = 'ShipmentsAreSubjectTo2';

update label set value = '                                             THE SERVICES ARE PROVIDED BY UPS -MI "AS IS", WITHOUT WARRANTY OF ANY KIND. WITHOUT LIMIATIONS OF THE FOREGOING, UPS -MI DOES NOT WARRANT THAT THE SERVICES BEING PROVIDED BY UPS -MI HEREUNDER WILL BE PROVIDED FREE OF OMISSIONS, ERRORS, DELAYS OR INTERRUPTIONS.'
where key = 'TheServicesProvided';

update label set value = 'PUNITIVE, INDIRECT OR INCIDENTAL DAMAGES, WEHTER ARISING IN CONTRACT, TORT OR OTHERWISE, EVEN IF UP S-MI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.'
where key = 'UPSMIEntireLiability4';

commit;

-- DBTicket TE-2576

drop table ws_stop cascade constraints purge;
drop table WS_ORDER_LINE_ITEM cascade constraints purge;
drop table WS_ORDER_LPN cascade constraints purge;
drop table WS_ORDER cascade constraints purge;
drop table WS_SHIPMENT cascade constraints purge;

-- DBTicket TE-2579 
-- Reference to WM ticket TE-2576

DROP VIEW WS_ORDERS_SHIPMENT_VIEW;

CREATE OR REPLACE procedure manh_wave_load_orders_for_rule
(
    p_wave_by_rule          in varchar2,
    p_list_of_shpmts        in varchar2,
    p_list_of_orders        in varchar2,
    p_facility_id           in whse_master.whse_master_id%type,
    p_perf_pickng_wave      in ship_wave_parm.perf_pickng_wave%type,
    p_perf_rte              in ship_wave_parm.perf_rte%type,
    p_preview_wave_flag     in number,
    p_wave_type_indic       in wave_parm.wave_type_indic%type,
    p_tc_company_id         in wave_parm.tc_company_id%type,
    p_pull_all_swc          in wave_parm.pull_all_swc%type,
    p_rule_id               in wave_rule_parm.rule_id%type,
    p_rule_type             in rule_hdr.rule_type%type,
    p_max_qty_per_rule      in wave_rule_parm.units_capcty%type,
    p_max_lines_per_rule    in wave_rule_parm.max_order_lines%type,
    p_is_wave_by_shpmt      in varchar2,
    p_max_orders_per_rule   in wave_rule_parm.max_orders%type,
    p_user_id               in user_profile.user_id%type,
    p_whse                  in facility.whse%type,
    p_ship_wave_nbr         in ship_wave_parm.ship_wave_nbr%type,
    p_pick_wave_nbr         in ship_wave_parm.pick_wave_nbr%type,
    p_rte_wave_option       in opt_param.param_value%type,
    p_chase_wave            in wave_parm.chase_wave%type
)
as
    v_order_selection_sql   varchar2(32600);
    v_do_type               orders.do_type%type
        := case when p_wave_type_indic = '1' then 20 else 10 end;
    v_line_item_count       number(9) := 0;
    v_failure_msg           msg_log.msg%type;
    v_lang_id               language.language_suffix%type;
begin
    manh_wave_build_selection_rule(p_wave_by_rule, p_list_of_shpmts,
        p_list_of_orders, p_facility_id, p_perf_pickng_wave, p_perf_rte,
        p_preview_wave_flag, p_wave_type_indic, p_tc_company_id, p_pull_all_swc,
        p_rule_id, p_rule_type, p_is_wave_by_shpmt, p_rte_wave_option,p_chase_wave,v_order_selection_sql);

    -- get the list of orders to be processed in this wave
    begin
        if (p_tc_company_id is not null)
        then
            execute immediate v_order_selection_sql using p_facility_id,
                p_tc_company_id;
        else
            execute immediate v_order_selection_sql using p_facility_id;
        end if;
        v_line_item_count := sql%rowcount;
        wm_cs_log('Loaded ' || sql%rowcount || ' OLI''s for rule ' || p_rule_id, p_sql_log_level => 1);
    exception
        when others then
-- todo: log this error
            raise;
    end;

    select la.language_suffix
    into v_lang_id
    from ucl_user uu
    join locale l on l.locale_id = uu.locale_id
    join language la on la.language_id = l.language_id
    where uu.user_name = p_user_id;

    v_failure_msg := get_msg_info('OUTBGEN', '7785', v_lang_id);
    insert into msg_log
    (
        msg_log_id, module, pgm_id, log_date_time, create_date_time, msg_id,
        mod_date_time, msg, user_id, whse, cd_master_id, ref_code_1, ref_value_1,
        ref_code_2, ref_value_2, ref_code_4, ref_value_4, ref_value_3
    )
    select msg_log_id_seq.nextval, 'OUTBGEN', 'Selection SP', sysdate, sysdate,
        '7785', sysdate, replace(v_failure_msg, '{0}', iv.tc_order_id) msg,
        p_user_id, p_whse, p_tc_company_id, '19', p_ship_wave_nbr, '06',
        p_pick_wave_nbr, '4', 'D', '2'
    from
    (
        select distinct o.tc_order_id
        from tmp_wave_selected_orders t
        join orders o on o.order_id = t.order_id
     ) iv;

    delete from tmp_wave_selected_orders t
    where exists
    (
        select 1
        from orders o
        where o.order_id = t.order_id
    );

    if (p_wave_by_rule = 'Y' and p_perf_pickng_wave = '1')
    then
        -- trim returned capacities here since they are rule selection lvl parms
        if (p_max_lines_per_rule < 999999999)
        then
            delete from tmp_wave_selected_orders t
            where t.id > p_max_lines_per_rule;
        end if;

        if (p_max_orders_per_rule < 999999999)
        then
            delete from tmp_wave_selected_orders t
            where exists
            (
                select 1
                from
                (
                    select iv.order_id,
                        row_number() over(order by iv.min_id) order_pos_no_holes
                    from
                    (
                        select t1.order_id, min(t1.id) min_id
                        from tmp_wave_selected_orders t1
                        group by t1.order_id
                    ) iv
                ) iv2
                where iv2.order_id = t.order_id
                    and iv2.order_pos_no_holes > p_max_orders_per_rule
            );
            wm_cs_log('Dropped ' || sql%rowcount || ' OLI''s due to max qty per rule being '
                || p_max_qty_per_rule, p_sql_log_level => 1);
        end if;

        if (p_max_qty_per_rule < 999999999)
        then
            delete from tmp_wave_selected_orders t
            where t.id >=
            (
                select min(t2.id)
                from
                (
                    select t1.id, sum(t1.need_qty) over(order by t1.id) rng_sum
                    from tmp_wave_selected_orders t1
                ) t2
                where t2.rng_sum > p_max_qty_per_rule
            );
            wm_cs_log('Dropped ' || sql%rowcount || ' OLI''s due to max lines per rule being '
                || p_max_lines_per_rule, p_sql_log_level => 1);
        end if;
    end if;

    if (p_pull_all_swc = 'Y' and p_perf_pickng_wave = '1'
        and p_wave_by_rule = 'Y' and p_is_wave_by_shpmt != 'Y')
    then
-- todo: swc with vas flow has gap; need to avoid pulling non-vas lines in
-- other orders with same swc
        -- insert additional lines with the same swc as those already
        -- selected; per CV, swc's are always together - all are either
        -- fully allocated (or) none are allocated at all (110)
        insert into tmp_wave_selected_orders
        (
            order_id, line_item_id, is_swc_pull, order_qty, allocated_qty,
            pre_pack_flag, ship_group_id, item_id, ppack_grp_code, assort_nbr,
            invn_type, prod_stat, cntry_of_orgn, batch_nbr, item_attr_1,
            item_attr_2, item_attr_3, item_attr_4, item_attr_5, sku_sub_code_id,
            sku_sub_code_value, do_dtl_status, batch_requirement_type, id,
            perform_vas, need_qty
        )
        select oli.order_id, oli.line_item_id, 1 is_swc_pull, oli.order_qty,
            oli.allocated_qty,
            case when o.pre_pack_flag in (1, 2) and oli.ppack_qty = 0
                and oli.assort_nbr is not null
                and oli.ppack_grp_code is not null
                then 1 else 0 end pre_pack_flag,
            o.ship_group_id, oli.item_id, oli.ppack_grp_code, oli.assort_nbr,
            oli.invn_type, oli.prod_stat, oli.cntry_of_orgn, oli.batch_nbr,
            oli.item_attr_1, oli.item_attr_2, oli.item_attr_3, oli.item_attr_4,
            oli.item_attr_5, oli.sku_sub_code_id, oli.sku_sub_code_value,
            oli.do_dtl_status, coalesce(oli.batch_requirement_type, '2'),
            (v_line_item_count + rownum) id,
            case when oli.vas_process_type = '1' then
                case when o.wm_order_status = 20 then 2 else 1 end
            else 0 end perform_vas,
            (oli.order_qty - oli.allocated_qty) need_qty
        from order_line_item oli
        join orders o on o.order_id = oli.order_id
        join
        (
            select distinct t.ship_group_id
            from tmp_wave_selected_orders t
        ) t1 on t1.ship_group_id = o.ship_group_id
        where o.o_facility_id = p_facility_id and o.is_original_order = 1
            and o.do_status = 110 and o.do_type = v_do_type
            and oli.allocation_source = 10 and oli.do_dtl_status = 110
            and (case when p_tc_company_id is null then o.tc_company_id
                else p_tc_company_id end) = o.tc_company_id
            and coalesce(oli.fulfillment_type, '1') != '2'
            and not exists
            (
                select 1
                from tmp_wave_selected_orders t2
                where t2.order_id = oli.order_id
                    and t2.line_item_id = oli.line_item_id
            )
            and (o.wm_order_status = 20 or not(oli.vas_process_type = '1'
                and oli.pick_locn_id is not null)) and o.has_import_error = 0;

        wm_cs_log('Pulled in more OLI''s for SWC proc ' || sql%rowcount, p_sql_log_level => 1);
    end if; -- swc inclusion
end;
/



CREATE OR REPLACE procedure wm_unplan_routed_orders
(
    p_user_id       in user_profile.user_id%type,
    p_cons_run_id   in orders.cons_run_id%type,
    p_order_id      in orders.order_id%type,
    p_order_split_id      in order_split.order_split_id%type,
    p_lock_time_out in number,
    p_debug_flag    in number,
    p_delete_stops    in number default 0 -- Control if we need to delete stop and stop action
)
as
begin

    if coalesce(p_cons_run_id, -1) > 0
    then

-- check: agg/orig?
-- todo: contention analysis
        insert into tmp_planned_orders
        (
         order_id, order_split_id
        )
        select o.order_id, coalesce(os.order_split_id, -1)
        from orders o
        left join order_split os on os.order_id = o.order_id and os.order_split_status = 10
        where o.cons_run_id = p_cons_run_id
        and not exists
        (
            select 1
            from lpn l
            where l.order_id = o.order_id
                and coalesce(l.order_split_id, -1) = coalesce(os.order_split_id, -1)
                and l.lpn_facility_status >= 40
        );

    else
       insert into tmp_planned_orders
       (
           order_id, order_split_id
       )
       select o.order_id, coalesce(os.order_split_id, -1)
       from orders o
       left join order_split os on os.order_id = o.order_id and os.order_split_status = 10 and os.order_split_id = p_order_split_id
       where o.order_id = p_order_id
           and not exists
           (
               select 1
               from lpn l
               where l.order_id = o.order_id
                   and coalesce(l.order_split_id, -1) = coalesce(os.order_split_id, -1)
                   and l.lpn_facility_status >= 40
           );
         end if;

    -- we cant determine that the ship_via was only assigned during routing and
    -- not at any other time; so we nullify it anyway
    update lpn l
    set l.tc_shipment_id = null, l.shipment_id = null, l.plan_load_id = null,
        l.ship_via = null, l.tracking_nbr = null, l.alt_tracking_nbr = null,
        l.last_updated_dttm = sysdate, l.last_updated_source = p_user_id,
        l.last_updated_source_type = 1, l.init_ship_via = null,
        l.distribution_leg_carrier_id = null, l.distribution_leg_mode_id = null,
        l.distribution_lev_svce_level_id = null
    where exists
    (
        select 1
        from tmp_planned_orders t
        where t.order_id = l.order_id
            and t.order_split_id = coalesce(l.order_split_id, -1)
    );

    insert into tmp_stop_action
    (
        shipment_id, stop_seq, stop_action_seq, static_route_id
    )
    select distinct sao.shipment_id, sao.stop_seq, sao.stop_action_seq, s.static_route_id
    from stop_action_order sao join shipment s on sao.shipment_id = s.shipment_id
    where exists
    (
        select 1
        from tmp_planned_orders t
        where t.order_id = sao.order_id
            and t.order_split_id = coalesce(sao.order_split_id, -1)
    );

   delete from order_movement om
    where exists
    (
        select 1
        from tmp_planned_orders t
        where t.order_id = om.order_id
            and t.order_split_id = coalesce(om.order_split_id, -1)
    );

    delete from stop_action_order sao
    where exists
    (
        select 1
        from tmp_planned_orders t
        where t.order_id = sao.order_id
            and t.order_split_id = coalesce(sao.order_split_id, -1)
    );

    delete from stop_action sa
    where exists
        (
            select 1
            from tmp_stop_action t
            where t.shipment_id = sa.shipment_id and t.stop_seq = sa.stop_seq
                and t.stop_action_seq = sa.stop_action_seq
        )
        and not exists
        (
            select 1
            from stop_action_order sao
            where sao.shipment_id = sa.shipment_id
                and sao.stop_seq = sa.stop_seq
                and sao.stop_action_seq = sa.stop_action_seq
        )and p_delete_stops = 1; -- Control deletion based on parameter...

-- check: per Karthik, having holes in stop_seq is not an issue; need to review
    delete from stop s
    where exists
        (
            select 1
            from tmp_stop_action t
            where t.shipment_id = s.shipment_id and t.stop_seq = s.stop_seq
        )
        and not exists
        (
            select 1
            from stop_action sa
            where sa.shipment_id = s.shipment_id and sa.stop_seq = s.stop_seq
        );

    update order_split os
    set os.order_split_status = 5, os.last_updated_dttm = sysdate,
        os.last_updated_source = p_user_id, os.last_updated_source_type = 1,
        os.shipment_id = null, os.assigned_mot_id = null,
        os.assigned_service_level_id = null, os.assigned_carrier_id = null,
        os.assigned_equipment_id = null, os.dynamic_routing_reqd = null,
        os.line_haul_ship_via = null, os.zone_skip_hub_location_id = null,
        os.distribution_ship_via = null, --os.actual_cost = null,
        --os.actual_cost_currency_code = null,
        os.baseline_cost = null,
        os.baseline_cost_currency_code = null, os.original_assigned_ship_via = null,
        os.order_loading_seq = null
    where exists
    (
        select 1
        from tmp_planned_orders t
        where t.order_split_id = coalesce(os.order_split_id, -1)
            and t.order_split_id > 0
    );

-- todo: swap two fields for 2012; override billing
    update orders o
    set o.actual_cost = null, o.actual_cost_currency_code = null,
        o.assigned_carrier_id = null, o.assigned_mot_id = null,
        o.assigned_service_level_id = null, o.assigned_equipment_id = null,
        o.baseline_cost = null, o.baseline_cost_currency_code = null,
        o.distribution_ship_via = null, o.dynamic_routing_reqd = null,
        o.last_updated_dttm = sysdate, o.last_updated_source = p_user_id,
        o.last_updated_source_type = 1, o.line_haul_ship_via = null,
        o.shipment_id = null, o.tc_shipment_id = null,
        o.zone_skip_hub_location_id = null, o.order_loading_seq = null,
        o.path_id = null, o.path_set_id = null, o.plan_d_facility_alias_id = null,
        o.plan_d_facility_id = null,
        o.order_status =
        (
            select coalesce(min(os.order_split_status), 5)
            from order_split os
            where os.order_id = o.order_id
        )
    where exists
    (
        select 1
        from tmp_planned_orders t
        where t.order_id = o.order_id
-- check: why do we need these nullification updates if the above fields are already tracked only on the split level?
-- so filter below added for unsplit orders only; arun to verify
            --and t.order_split_id < 0
    );

    -- clean up child data for cancelled shpmts
    delete from shipment_accessorial sa
    where exists
        (
            select 1
            from tmp_stop_action t
            where t.shipment_id = sa.shipment_id
        )
        and not exists
        (
            select 1
            from stop_action_order sao
            where sao.shipment_id = sa.shipment_id
        );

    delete from shipment_commodity sc
    where exists
        (
            select 1
            from tmp_stop_action t
            where t.shipment_id = sc.shipment_id
        )
        and not exists
        (
            select 1
            from stop_action_order sao
            where sao.shipment_id = sc.shipment_id
        );

    delete from shipment_size ss
    where exists
        (
            select 1
            from tmp_stop_action t
            where t.shipment_id = ss.shipment_id
        )
        and not exists
        (
            select 1
            from stop_action_order sao
            where sao.shipment_id = ss.shipment_id
        );

    update shipment s
    set s.is_cancelled = 1, s.shipment_status = 120, s.o_address = null,
        s.o_city = null, s.o_country_code = null, s.o_county = null,
        s.o_facility_id = null, s.o_postal_code = null, s.o_state_prov = null,
        s.d_address = null, s.d_city = null, s.d_country_code = null,
        s.d_county = null, s.d_facility_id = null, s.d_facility_number = null,
        s.d_postal_code = null, s.d_state_prov = null, s.baseline_cost = null,
        s.baseline_cost_currency_code = null, s.total_cost = null,
        s.linehaul_cost = null, s.num_stops = 0, s.num_docks = 0,
        s.direct_distance = null, s.distance = null,
        s.out_of_route_distance = null, s.estimated_cost = null,
        s.earned_income = null, s.assigned_carrier_id = null,
        s.assigned_service_level_id = null, s.assigned_mot_id = null,
        s.assigned_equipment_id = null, s.assigned_ship_via = null,
        s.last_updated_dttm = sysdate, s.last_updated_source = p_user_id,
        s.last_updated_source_type = 1
    where exists
        (
            select 1
            from tmp_stop_action t
            where t.shipment_id = s.shipment_id
        )
        and not exists
        (
            select 1
            from stop_action sa
            where sa.shipment_id = s.shipment_id
        );

-- todo: remove at an order/split level shipment_accessorial, shipment_size for updated shpmts; need to test and see if app will rebuild after SP deletes all data for shipment

-- todo: account for preplanning where there are no lpn's; currently this only accounts for printed to weighed olpns
-- check: will save shpmt in app do all of this anyway? arun to confirm
-- todo: pickup/delv times on stop/shipment? arun to provide details
    merge into shipment s
    using
    (
        select st.shipment_id, count(distinct st.stop_seq) num_stops,
            sum(coalesce(l.weight, l.estimated_weight)) wt,
            sum(coalesce(l.actual_volume, l.estimated_volume)) vol,
            null pickup_start_dttm,
            null pickup_end_dttm,
            null delivery_start_dttm,
            null delivery_end_dttm
        from stop st
        left join lpn l on l.shipment_id = st.shipment_id
        where exists
        (
            select 1
            from tmp_stop_action t
            where t.shipment_id = st.shipment_id
        )
        group by st.shipment_id
    ) iv on (iv.shipment_id = s.shipment_id)
    when matched then
    update set s.last_updated_source = p_user_id, s.planned_weight = iv.wt,
        s.planned_volume = iv.vol, s.last_updated_dttm = sysdate,
        s.pickup_start_dttm = iv.pickup_start_dttm,
        s.pickup_end_dttm = iv.pickup_end_dttm,
        s.delivery_start_dttm = iv.delivery_start_dttm,
        s.delivery_end_dttm = iv.delivery_end_dttm,
        s.bill_of_lading_number = (case when iv.num_stops = 2 then null
            else s.bill_of_lading_number end);

    -- no commit here as app needs to rerate in the same transaction
end;
/

-- DBTicket TE-2581
MERGE INTO RESOURCES A
     USING (SELECT '/te/manifest/ui/BundlesReport.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manifest/ui/BundlesReport.xhtml',
               'ACM',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manifest/ui/editDODestinationAddress.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manifest/ui/editDODestinationAddress.xhtml',
               'MNF',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manifest/ui/fedexServerTranInqDetail.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manifest/ui/fedexServerTranInqDetail.xhtml',
               'MNF',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manifest/ui/CloseManifest.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manifest/ui/CloseManifest.xhtml',
               'ASHP',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manifest/ui/GenerateSED.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manifest/ui/GenerateSED.xhtml',
               'MNF',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manifest/ui/InternationalDocumentList.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manifest/ui/InternationalDocumentList.xhtml',
               'MNF',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manifest/ui/LabelReport.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manifest/ui/LabelReport.xhtml',
               'ACM',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manifest/ui/LabelTranslations.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manifest/ui/LabelTranslations.xhtml',
               'MNF',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manifest/ui/LiteralTranslationsList.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manifest/ui/LiteralTranslationsList.xhtml',
               'MNF',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manifest/ui/manifestedLpnDetail.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manifest/ui/manifestedLpnDetail.xhtml',
               'MNF',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manifest/ui/manifestedLpnExceptionList.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manifest/ui/manifestedLpnExceptionList.xhtml',
               'MNF',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manifest/ui/manifestHdrDetail.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manifest/ui/manifestHdrDetail.xhtml',
               'MNF',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manifest/ui/manifestLpnChrgList.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manifest/ui/manifestLpnChrgList.xhtml',
               'MNF',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manifest/ui/manifestLpnDetailist.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manifest/ui/manifestLpnDetailist.xhtml',
               'MNF',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manifest/ui/MessageLogList.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manifest/ui/MessageLogList.xhtml',
               'SYSCTRL',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manifest/ui/MessageMasterList.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manifest/ui/MessageMasterList.xhtml',
               'MNF',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manifest/ui/warningDialog.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manifest/ui/warningDialog.xhtml',
               'MNF',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manifest/ui/weightLpnDetail.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manifest/ui/weightLpnDetail.xhtml',
               'MNF',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manifest/ui/weightManifestDetail.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manifest/ui/weightManifestDetail.xhtml',
               'MNF',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manifest/ui/weightOrderDetail.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manifest/ui/weightOrderDetail.xhtml',
               'MNF',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/infeasibility/servicelevelcustomer/jsp/AddInfeasibilities.xhtml'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/infeasibility/servicelevelcustomer/jsp/AddInfeasibilities.xhtml',
                 'TRANS',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/infeasibility/servicelevelcustomer/jsp/ViewInfeasibilities.xhtml'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/infeasibility/servicelevelcustomer/jsp/ViewInfeasibilities.xhtml',
                 'TRANS',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/infeasibility/servicelevelfacility/jsp/AddInfeasibilities.xhtml'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/infeasibility/servicelevelfacility/jsp/AddInfeasibilities.xhtml',
                 'TRANS',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manual/planning/jsp/AssignShipVia.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manual/planning/jsp/AssignShipVia.xhtml',
               'OSE',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manual/planning/jsp/buttonsPanel.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manual/planning/jsp/buttonsPanel.xhtml',
               'OSE',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manual/planning/jsp/CloseShipment.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manual/planning/jsp/CloseShipment.xhtml',
               'OSE',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manual/planning/jsp/hardCheckErrors.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manual/planning/jsp/hardCheckErrors.xhtml',
               'OSE',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manual/planning/jsp/softCheckErrors.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manual/planning/jsp/softCheckErrors.xhtml',
               'OSE',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manual/planning/jsp/softCheckErrorsOverride.xhtml'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manual/planning/jsp/softCheckErrorsOverride.xhtml',
               'OSE',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manual/planning/jsp/softCheckErrorsOverrideTemplate.xhtml'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/manual/planning/jsp/softCheckErrorsOverrideTemplate.xhtml',
                 'OSE',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manual/planning/jsp/StaticRouteCheckResult.xhtml'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manual/planning/jsp/StaticRouteCheckResult.xhtml',
               'OSE',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manual/planning/jsp/viewDistributionOrderSize.xhtml'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manual/planning/jsp/viewDistributionOrderSize.xhtml',
               'OSE',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manual/planning/jsp/viewShipmentList.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manual/planning/jsp/viewShipmentList.xhtml',
               'OSE',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manual/planning/jsp/viewShipmentSize.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manual/planning/jsp/viewShipmentSize.xhtml',
               'OSE',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manual/planning/jsp/viewStopResequenceDetails.xhtml'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manual/planning/jsp/viewStopResequenceDetails.xhtml',
               'OSE',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/jsp/FilterListPopup.xhtml'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/jsp/FilterListPopup.xhtml',
                 'ASHP',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/jsp/NewFilterPopup.xhtml'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/optimizationmgmt/consolidation/jsp/NewFilterPopup.xhtml',
               'ASHP',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/parcelRate/ui/ParcelTransitTime.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/parcelRate/ui/ParcelTransitTime.xhtml',
               'CNTRMGT',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/parcelRate/ui/ParcelZoneRate.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/parcelRate/ui/ParcelZoneRate.xhtml',
               'CNTRMGT',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/filter/jsp/FilterList.jsp' AS URI, 1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/filter/jsp/FilterList.jsp',
               'CMA',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/filter/jsp/FilterOnFilters.jsp' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/filter/jsp/FilterOnFilters.jsp',
               'CMA',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manifest/ui/openpldfile.jsp' AS URI, 1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manifest/ui/openpldfile.jsp',
               'CMA',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manual/planning/jsp/processSplitOrderPopup.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manual/planning/jsp/processSplitOrderPopup.jsp',
               'CMA',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manual/planning/jsp/SplitOrderPopup.jsp' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manual/planning/jsp/SplitOrderPopup.jsp',
               'CMA',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/manual/planning/msp/mspWorkspace.jsp' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/manual/planning/msp/mspWorkspace.jsp',
               'CMA',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/jsp/PrefixedOrderPresentation.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/jsp/PrefixedOrderPresentation.jsp',
                 'CMA',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/jsp/PrefixedShipmentPresentation.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/jsp/PrefixedShipmentPresentation.jsp',
                 'CMA',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/jsp/ProcessConsolidationRun.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/jsp/ProcessConsolidationRun.jsp',
                 'CMA',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/jsp/ProcessReplanningOrders.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/jsp/ProcessReplanningOrders.jsp',
                 'CMA',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/jsp/ProcessSizeMap.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/optimizationmgmt/consolidation/jsp/ProcessSizeMap.jsp',
               'CMA',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/jsp/RejectOrdersConfirmationPopup.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/jsp/RejectOrdersConfirmationPopup.jsp',
                 'CMA',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/jsp/ReplanOrdersConfirmationPopup.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/jsp/ReplanOrdersConfirmationPopup.jsp',
                 'CMA',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/jsp/StartRunConfirmationPopup.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/jsp/StartRunConfirmationPopup.jsp',
                 'CMA',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/jsp/TEConsolidationRunAnalysisOnGMap.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/jsp/TEConsolidationRunAnalysisOnGMap.jsp',
                 'CMA',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/jsp/TERunRejectionPage.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/jsp/TERunRejectionPage.jsp',
                 'CMA',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/jsp/ViewConsolidationWorkspace.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/jsp/ViewConsolidationWorkspace.jsp',
                 'CMA',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/jsp/ViewOrderPathAnalysis.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/jsp/ViewOrderPathAnalysis.jsp',
                 'CMA',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/parameter/jsp/ConsolidationEngineSelectInclude.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/parameter/jsp/ConsolidationEngineSelectInclude.jsp',
                 'CMA',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/parameter/jsp/ConsolidationParametersInclude.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/parameter/jsp/ConsolidationParametersInclude.jsp',
                 'CMA',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/parameter/jsp/EditPlanningEngineParameters.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/parameter/jsp/EditPlanningEngineParameters.jsp',
                 'CMA',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/parameter/jsp/ProcessParameterSet.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/parameter/jsp/ProcessParameterSet.jsp',
                 'CMA',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/parameter/jsp/ViewPlanningEngineParameters.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/parameter/jsp/ViewPlanningEngineParameters.jsp',
                 'CMA',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/parameter/jsp/ViewPlanningEngineParametersProcess.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/parameter/jsp/ViewPlanningEngineParametersProcess.jsp',
                 'CMA',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/order/jsp/OrderListPresentation.jsp' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/order/jsp/OrderListPresentation.jsp',
               'CMA',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/order/jsp/ProcessOrderListPresentation.jsp' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/order/jsp/ProcessOrderListPresentation.jsp',
               'CMA',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/ProcessMMtoVMConversion.jsp' AS URI, 1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/ProcessMMtoVMConversion.jsp',
               'CMA',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/ProcessXMLCommManagerImport.jsp' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/ProcessXMLCommManagerImport.jsp',
               'CMA',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/shipserv/jsp/EditShipmentAssignedResources.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/shipserv/jsp/EditShipmentAssignedResources.jsp',
               'CMA',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/shipserv/jsp/getAssignedShipments.jsp' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/shipserv/jsp/getAssignedShipments.jsp',
               'CMA',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/shipserv/jsp/MovementDetails.jsp' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/shipserv/jsp/MovementDetails.jsp',
               'CMA',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/shipserv/jsp/processShipmentAssignedResources.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/shipserv/jsp/processShipmentAssignedResources.jsp',
               'CMA',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/admin/parameter/jsp/ViewTEParameters.jsp' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/admin/parameter/jsp/ViewTEParameters.jsp',
               'CMA',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/admin/parameter/jsp/EditTEParametersProcess.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/admin/parameter/jsp/EditTEParametersProcess.jsp',
               'CMA',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/admin/parameter/jsp/EditTEParameters.jsp' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/admin/parameter/jsp/EditTEParameters.jsp',
               'CMA',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/admin/parameter/jsp/EditParameters.jsp' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/admin/parameter/jsp/EditParameters.jsp',
               'CMA',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/filter/jsp/ApplyDefault.jsp' AS URI, 1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/filter/jsp/ApplyDefault.jsp',
               'CMA',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/filter/jsp/FilterDetails.jsp' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/filter/jsp/FilterDetails.jsp',
               'CMA',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/jsp/SelectConsolidationEngine.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/jsp/SelectConsolidationEngine.jsp',
                 'CMA',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/jsp/EditSizeMap.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/optimizationmgmt/consolidation/jsp/EditSizeMap.jsp',
               'CMA',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/jsp/ParameterSetListPresentation.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/jsp/ParameterSetListPresentation.jsp',
                 'CMA',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/parameter/jsp/ParameterSetListPresentation.jsp '
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/parameter/jsp/ParameterSetListPresentation.jsp ',
                 'CMA',
                 1,
                 NULL);

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'MRFARPTDFN' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/manifest/ui/BundlesReport.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/manifest/ui/BundlesReport.xhtml'),
               'MRFARPTDFN');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'WGHLPN' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/manifest/ui/editDODestinationAddress.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/manifest/ui/editDODestinationAddress.xhtml'),
                 'WGHLPN');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'WMVWKAREA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/manifest/ui/fedexServerTranInqDetail.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/manifest/ui/fedexServerTranInqDetail.xhtml'),
                 'WMVWKAREA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'WMVWKAREA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/manifest/ui/CloseManifest.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/manifest/ui/CloseManifest.xhtml'),
               'WMVWKAREA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'WMVWKAREA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/manifest/ui/GenerateSED.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/manifest/ui/GenerateSED.xhtml'),
               'WMVWKAREA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'WMVWKAREA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/manifest/ui/InternationalDocumentList.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/manifest/ui/InternationalDocumentList.xhtml'),
                 'WMVWKAREA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'MXREF' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/manifest/ui/LabelReport.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/manifest/ui/LabelReport.xhtml'),
               'MXREF');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'MXREF' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/manifest/ui/LabelTranslations.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/manifest/ui/LabelTranslations.xhtml'),
               'MXREF');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'WMVWKAREA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/manifest/ui/LiteralTranslationsList.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/manifest/ui/LiteralTranslationsList.xhtml'),
                 'WMVWKAREA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'WMVWKAREA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/manifest/ui/manifestedLpnDetail.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/manifest/ui/manifestedLpnDetail.xhtml'),
               'WMVWKAREA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'WMVWKAREA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/manifest/ui/manifestedLpnExceptionList.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/manifest/ui/manifestedLpnExceptionList.xhtml'),
                 'WMVWKAREA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'WMVWKAREA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/manifest/ui/manifestHdrDetail.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/manifest/ui/manifestHdrDetail.xhtml'),
               'WMVWKAREA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'WMVWKAREA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/manifest/ui/manifestLpnChrgList.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/manifest/ui/manifestLpnChrgList.xhtml'),
               'WMVWKAREA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'WMVWKAREA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/manifest/ui/manifestLpnDetailist.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/manifest/ui/manifestLpnDetailist.xhtml'),
               'WMVWKAREA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'WMVMSGLOG' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/manifest/ui/MessageLogList.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/manifest/ui/MessageLogList.xhtml'),
               'WMVMSGLOG');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'WMVWKAREA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/manifest/ui/MessageMasterList.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/manifest/ui/MessageMasterList.xhtml'),
               'WMVWKAREA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'WMVWKAREA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/manifest/ui/warningDialog.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/manifest/ui/warningDialog.xhtml'),
               'WMVWKAREA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'MNFLPN' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/manifest/ui/weightLpnDetail.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/manifest/ui/weightLpnDetail.xhtml'),
               'MNFLPN');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'MNFLPN' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/manifest/ui/weightManifestDetail.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/manifest/ui/weightManifestDetail.xhtml'),
               'MNFLPN');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'MNFLPN' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/manifest/ui/weightOrderDetail.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/manifest/ui/weightOrderDetail.xhtml'),
               'MNFLPN');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'VBD' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/infeasibility/servicelevelcustomer/jsp/AddInfeasibilities.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/infeasibility/servicelevelcustomer/jsp/AddInfeasibilities.xhtml'),
                 'VBD');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'VBD' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/infeasibility/servicelevelcustomer/jsp/ViewInfeasibilities.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/infeasibility/servicelevelcustomer/jsp/ViewInfeasibilities.xhtml'),
                 'VBD');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'VBD' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/infeasibility/servicelevelfacility/jsp/AddInfeasibilities.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/infeasibility/servicelevelfacility/jsp/AddInfeasibilities.xhtml'),
                 'VBD');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'MSA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/manual/planning/jsp/AssignShipVia.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/manual/planning/jsp/AssignShipVia.xhtml'),
               'MSA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'MSA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/manual/planning/jsp/buttonsPanel.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/manual/planning/jsp/buttonsPanel.xhtml'),
               'MSA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'MSA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/manual/planning/jsp/CloseShipment.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/manual/planning/jsp/CloseShipment.xhtml'),
               'MSA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'MSA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/manual/planning/jsp/hardCheckErrors.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/manual/planning/jsp/hardCheckErrors.xhtml'),
                 'MSA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'MSA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/manual/planning/jsp/softCheckErrors.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/manual/planning/jsp/softCheckErrors.xhtml'),
                 'MSA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'MSA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/manual/planning/jsp/softCheckErrorsOverride.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/manual/planning/jsp/softCheckErrorsOverride.xhtml'),
                 'MSA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'MSA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/manual/planning/jsp/softCheckErrorsOverrideTemplate.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/manual/planning/jsp/softCheckErrorsOverrideTemplate.xhtml'),
                 'MSA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'AST' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/manual/planning/jsp/StaticRouteCheckResult.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/manual/planning/jsp/StaticRouteCheckResult.xhtml'),
                 'AST');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'MSA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/manual/planning/jsp/viewDistributionOrderSize.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/manual/planning/jsp/viewDistributionOrderSize.xhtml'),
                 'MSA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'MSA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/manual/planning/jsp/viewShipmentList.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/manual/planning/jsp/viewShipmentList.xhtml'),
                 'MSA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'MSA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/manual/planning/jsp/viewShipmentSize.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/manual/planning/jsp/viewShipmentSize.xhtml'),
                 'MSA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'MSA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/manual/planning/jsp/viewStopResequenceDetails.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/manual/planning/jsp/viewStopResequenceDetails.xhtml'),
                 'MSA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/jsp/FilterListPopup.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/jsp/FilterListPopup.xhtml'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/jsp/NewFilterPopup.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/jsp/NewFilterPopup.xhtml'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID,
                   'VPCLTRANSITTIME' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/parcelRate/ui/ParcelTransitTime.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/parcelRate/ui/ParcelTransitTime.xhtml'),
               'VPCLTRANSITTIME');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID,
                   'VPCLTRANSITTIME' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/parcelRate/ui/ParcelZoneRate.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/parcelRate/ui/ParcelZoneRate.xhtml'),
               'VPCLTRANSITTIME');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/filter/jsp/FilterList.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/filter/jsp/FilterList.jsp'),
               'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/filter/jsp/FilterOnFilters.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/filter/jsp/FilterOnFilters.jsp'),
               'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/manifest/ui/openpldfile.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/manifest/ui/openpldfile.jsp'),
               'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/manual/planning/jsp/processSplitOrderPopup.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/manual/planning/jsp/processSplitOrderPopup.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/manual/planning/jsp/SplitOrderPopup.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/manual/planning/jsp/SplitOrderPopup.jsp'),
               'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/manual/planning/msp/mspWorkspace.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/manual/planning/msp/mspWorkspace.jsp'),
               'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/jsp/PrefixedOrderPresentation.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/jsp/PrefixedOrderPresentation.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/jsp/PrefixedShipmentPresentation.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/jsp/PrefixedShipmentPresentation.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/jsp/ProcessConsolidationRun.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/jsp/ProcessConsolidationRun.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/jsp/ProcessReplanningOrders.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/jsp/ProcessReplanningOrders.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/jsp/ProcessSizeMap.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/jsp/ProcessSizeMap.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/jsp/RejectOrdersConfirmationPopup.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/jsp/RejectOrdersConfirmationPopup.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/jsp/ReplanOrdersConfirmationPopup.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/jsp/ReplanOrdersConfirmationPopup.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/jsp/StartRunConfirmationPopup.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/jsp/StartRunConfirmationPopup.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/jsp/TEConsolidationRunAnalysisOnGMap.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/jsp/TEConsolidationRunAnalysisOnGMap.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/jsp/TERunRejectionPage.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/jsp/TERunRejectionPage.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/jsp/ViewConsolidationWorkspace.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/jsp/ViewConsolidationWorkspace.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/jsp/ViewOrderPathAnalysis.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/jsp/ViewOrderPathAnalysis.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/parameter/jsp/ConsolidationEngineSelectInclude.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/parameter/jsp/ConsolidationEngineSelectInclude.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/parameter/jsp/ConsolidationParametersInclude.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/parameter/jsp/ConsolidationParametersInclude.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/parameter/jsp/EditPlanningEngineParameters.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/parameter/jsp/EditPlanningEngineParameters.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/parameter/jsp/ProcessParameterSet.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/parameter/jsp/ProcessParameterSet.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/parameter/jsp/ViewPlanningEngineParameters.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/parameter/jsp/ViewPlanningEngineParameters.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/parameter/jsp/ViewPlanningEngineParametersProcess.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/parameter/jsp/ViewPlanningEngineParametersProcess.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/order/jsp/OrderListPresentation.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/order/jsp/OrderListPresentation.jsp'),
               'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/order/jsp/ProcessOrderListPresentation.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/order/jsp/ProcessOrderListPresentation.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/ProcessMMtoVMConversion.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/ProcessMMtoVMConversion.jsp'),
               'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/ProcessXMLCommManagerImport.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/ProcessXMLCommManagerImport.jsp'),
               'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/shipserv/jsp/EditShipmentAssignedResources.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/shipserv/jsp/EditShipmentAssignedResources.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/shipserv/jsp/getAssignedShipments.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/shipserv/jsp/getAssignedShipments.jsp'),
               'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/shipserv/jsp/MovementDetails.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/shipserv/jsp/MovementDetails.jsp'),
               'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/shipserv/jsp/processShipmentAssignedResources.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/shipserv/jsp/processShipmentAssignedResources.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/admin/parameter/jsp/ViewTEParameters.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/te/admin/parameter/jsp/ViewTEParameters.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/admin/parameter/jsp/EditTEParametersProcess.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/admin/parameter/jsp/EditTEParametersProcess.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/admin/parameter/jsp/EditTEParameters.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/te/admin/parameter/jsp/EditTEParameters.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/admin/parameter/jsp/EditParameters.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/admin/parameter/jsp/EditParameters.jsp'),
               'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/filter/jsp/ApplyDefault.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/filter/jsp/ApplyDefault.jsp'),
               'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/filter/jsp/FilterDetails.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/te/filter/jsp/FilterDetails.jsp'),
               'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/jsp/SelectConsolidationEngine.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/jsp/SelectConsolidationEngine.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/jsp/EditSizeMap.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/jsp/EditSizeMap.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/jsp/ParameterSetListPresentation.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/jsp/ParameterSetListPresentation.jsp'),
                 'CMA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/parameter/jsp/ParameterSetListPresentation.jsp ') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/te/optimizationmgmt/consolidation/parameter/jsp/ParameterSetListPresentation.jsp '),
                 'CMA');

COMMIT;

-- DBTicket WM-35904
create or replace procedure manh_wave_load_orders_for_rule
(
    p_wave_by_rule          in varchar2,
    p_list_of_shpmts        in varchar2,
    p_list_of_orders        in varchar2,
    p_facility_id           in whse_master.whse_master_id%type,
    p_perf_pickng_wave      in ship_wave_parm.perf_pickng_wave%type,
    p_perf_rte              in ship_wave_parm.perf_rte%type,
    p_preview_wave_flag     in number,
    p_wave_type_indic       in wave_parm.wave_type_indic%type,
    p_tc_company_id         in wave_parm.tc_company_id%type,
    p_pull_all_swc          in wave_parm.pull_all_swc%type,
    p_rule_id               in wave_rule_parm.rule_id%type,
    p_rule_type             in rule_hdr.rule_type%type,
    p_max_qty_per_rule      in wave_rule_parm.units_capcty%type,
    p_max_lines_per_rule    in wave_rule_parm.max_order_lines%type,
    p_is_wave_by_shpmt      in varchar2,
    p_max_orders_per_rule   in wave_rule_parm.max_orders%type,
    p_user_id               in user_profile.user_id%type,
    p_whse                  in facility.whse%type,
    p_ship_wave_nbr         in ship_wave_parm.ship_wave_nbr%type,
    p_pick_wave_nbr         in ship_wave_parm.pick_wave_nbr%type,
    p_rte_wave_option       in opt_param.param_value%type,
    p_chase_wave            in wave_parm.chase_wave%type
)
as
    v_order_selection_sql   varchar2(32600);
    v_do_type               orders.do_type%type 
        := case when p_wave_type_indic = '1' then 20 else 10 end;
    v_line_item_count       number(9) := 0;
    v_failure_msg           msg_log.msg%type;
    v_lang_id               language.language_suffix%type;
begin
    manh_wave_build_selection_rule(p_wave_by_rule, p_list_of_shpmts,
        p_list_of_orders, p_facility_id, p_perf_pickng_wave, p_perf_rte,
        p_preview_wave_flag, p_wave_type_indic, p_tc_company_id, p_pull_all_swc,
        p_rule_id, p_rule_type, p_is_wave_by_shpmt, p_rte_wave_option,p_chase_wave,v_order_selection_sql);

    -- get the list of orders to be processed in this wave
    begin
        if (p_tc_company_id is not null)
        then
            execute immediate v_order_selection_sql using p_facility_id, 
                p_tc_company_id;
        else
            execute immediate v_order_selection_sql using p_facility_id;
        end if;
        v_line_item_count := sql%rowcount;
        wm_cs_log('Loaded ' || sql%rowcount || ' OLI''s for rule ' || p_rule_id, p_sql_log_level => 1);
    exception
        when others then
-- todo: log this error
            raise;
    end;
    
    select la.language_suffix
    into v_lang_id
    from ucl_user uu
    join locale l on l.locale_id = uu.locale_id
    join language la on la.language_id = l.language_id
    where uu.user_name = p_user_id;

    if (p_wave_by_rule = 'Y' and p_perf_pickng_wave = '1')
    then
        -- trim returned capacities here since they are rule selection lvl parms
        if (p_max_lines_per_rule < 999999999)
        then    
            delete from tmp_wave_selected_orders t
            where t.id > p_max_lines_per_rule;
        end if;

        if (p_max_orders_per_rule < 999999999)
        then
            delete from tmp_wave_selected_orders t
            where exists
            (
                select 1
                from
                (
                    select iv.order_id,
                        row_number() over(order by iv.min_id) order_pos_no_holes
                    from
                    (
                        select t1.order_id, min(t1.id) min_id
                        from tmp_wave_selected_orders t1
                        group by t1.order_id
                    ) iv
                ) iv2
                where iv2.order_id = t.order_id
                    and iv2.order_pos_no_holes > p_max_orders_per_rule
            );
            wm_cs_log('Dropped ' || sql%rowcount || ' OLI''s due to max qty per rule being '
                || p_max_qty_per_rule, p_sql_log_level => 1);
        end if;

        if (p_max_qty_per_rule < 999999999)
        then
            delete from tmp_wave_selected_orders t
            where t.id >=
            (
                select min(t2.id)
                from
                (
                    select t1.id, sum(t1.need_qty) over(order by t1.id) rng_sum
                    from tmp_wave_selected_orders t1
                ) t2
                where t2.rng_sum > p_max_qty_per_rule
            );
            wm_cs_log('Dropped ' || sql%rowcount || ' OLI''s due to max lines per rule being '
                || p_max_lines_per_rule, p_sql_log_level => 1);
        end if;
    end if;

    if (p_pull_all_swc = 'Y' and p_perf_pickng_wave = '1'
        and p_wave_by_rule = 'Y' and p_is_wave_by_shpmt != 'Y')
    then
-- todo: swc with vas flow has gap; need to avoid pulling non-vas lines in
-- other orders with same swc
        -- insert additional lines with the same swc as those already
        -- selected; per CV, swc's are always together - all are either
        -- fully allocated (or) none are allocated at all (110)
        insert into tmp_wave_selected_orders
        (
            order_id, line_item_id, is_swc_pull, order_qty, allocated_qty,
            pre_pack_flag, ship_group_id, item_id, ppack_grp_code, assort_nbr,
            invn_type, prod_stat, cntry_of_orgn, batch_nbr, item_attr_1, 
            item_attr_2, item_attr_3, item_attr_4, item_attr_5, sku_sub_code_id,
            sku_sub_code_value, do_dtl_status, batch_requirement_type, id,
            perform_vas, need_qty
        )
        select oli.order_id, oli.line_item_id, 1 is_swc_pull, oli.order_qty,
            oli.allocated_qty,
            case when o.pre_pack_flag in (1, 2) and oli.ppack_qty = 0
                and oli.assort_nbr is not null 
                and oli.ppack_grp_code is not null
                then 1 else 0 end pre_pack_flag,
            o.ship_group_id, oli.item_id, oli.ppack_grp_code, oli.assort_nbr,
            oli.invn_type, oli.prod_stat, oli.cntry_of_orgn, oli.batch_nbr,
            oli.item_attr_1, oli.item_attr_2, oli.item_attr_3, oli.item_attr_4,
            oli.item_attr_5, oli.sku_sub_code_id, oli.sku_sub_code_value, 
            oli.do_dtl_status, coalesce(oli.batch_requirement_type, '2'),
            (v_line_item_count + rownum) id,
            case when oli.vas_process_type = '1' then
                case when o.wm_order_status = 20 then 2 else 1 end
            else 0 end perform_vas,
            (oli.order_qty - oli.allocated_qty) need_qty
        from order_line_item oli
        join orders o on o.order_id = oli.order_id
        join 
        (
            select distinct t.ship_group_id
            from tmp_wave_selected_orders t
        ) t1 on t1.ship_group_id = o.ship_group_id
        where o.o_facility_id = p_facility_id and o.is_original_order = 1
            and o.do_status = 110 and o.do_type = v_do_type
            and oli.allocation_source = 10 and oli.do_dtl_status = 110
            and (case when p_tc_company_id is null then o.tc_company_id
                else p_tc_company_id end) = o.tc_company_id
            and coalesce(oli.fulfillment_type, '1') != '2'
            and not exists
            (
                select 1
                from tmp_wave_selected_orders t2
                where t2.order_id = oli.order_id 
                    and t2.line_item_id = oli.line_item_id
            )
            and (o.wm_order_status = 20 or not(oli.vas_process_type = '1'
                and oli.pick_locn_id is not null)) and o.has_import_error = 0;

        wm_cs_log('Pulled in more OLI''s for SWC proc ' || sql%rowcount, p_sql_log_level => 1);
    end if; -- swc inclusion
end;
/

-- DBTicket TE-2592
DECLARE
      v_col_count       NUMBER(1);
BEGIN    
   SELECT count(*)
   INTO v_col_count
   FROM user_tab_columns
   WHERE COLUMN_NAME = 'MINIMUM_RATE' AND TABLE_NAME='INPT_PARCL_RATE';

   IF v_col_count =0
   THEN
      EXECUTE IMMEDIATE 'ALTER TABLE INPT_PARCL_RATE ADD MINIMUM_RATE NUMBER(14,3)';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

COMMENT ON COLUMN INPT_PARCL_RATE.MINIMUM_RATE IS 'Minimum Parcel Rate';

-- DBTicket TE-2594
update label set value='Sub Risk' where key='SubRisk' and bundle_name = 'Languages';
update label set value='UN ID' where key='UNID' and bundle_name = 'Languages';
update label set value='Pack Group' where key='PackGrp' and bundle_name = 'Languages';
update label set value='Mass Capacity' where key='MassCapc' and bundle_name = 'Languages';
commit;

-- DBTicket TE-2602
INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000000', '200000000', 'te', 'MSP', 'Selected Orders must have the same Billing Method.', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N'); 

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000001', '200000001', 'te', 'MSP', 'One or more selected orders are already assigned to a Shipment {0}.', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N'); 

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000002', '200000002', 'te', 'MSP', 'One or more selected orders are already Canceled {0}.', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N');  

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000003', '200000003', 'te', 'MSP', 'One or more selected orders are part of wave process.', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N'); 

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG ) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000004', '200000004', 'te', 'MSP', 'Orders designated {0} does not match.', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N');   

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000005', '200000005', 'te', 'MSP', 'Orders designated ship via does not match with shipment assigned ship via.', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N' ); 

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000006', '200000006', 'te', 'MSP', 'Orders billing method does not match with shipment billing method.', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N'); 

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG ) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000007', '200000007', 'te', 'MSP', 'Shipment {0} is closed', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N'); 

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS,MSG_TYPE, OVRIDE_ROLE, LOG_FLAG) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000008', '200000008', 'te', 'MSP', 'Assignment has been stopped for the shipment {0}.', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N'); 

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000009', '200000009', 'te', 'MSP', 'The selection contains oLPNs that are already loaded.', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N');

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000010', '200000010', 'te', 'MSP', 'Shipment status is greater than accepted.', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N');

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000011', '200000011', 'te', 'MSP', 'Cannot create Shipment for Orders that have a Retail Route Assigned.', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N');

INSERT INTO message_master( MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000012', '200000012', 'te', 'MSP', 'Cannot create Shipment for LPN that have a Retail Route Assigned.', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N');

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000013', '200000013', 'te', 'MSP', 'One or more selected oLPNs are already assigned to a Shipment {0}.', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N');

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000014', '200000014', 'te', 'MSP', 'One or more selected LPNs are already Canceled {0}.', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N');

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000015', '200000015', 'te', 'MSP', 'Customer order cannot be assigned to Retail route shipment.', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N');

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000016', '200000016', 'te', 'MSP', 'Customer order LPN cannot be assigned to Retail route shipment.', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N');

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000017', '200000017', 'te', 'MSP', 'Order does not have a retail route.', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N');

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000018', '200000018', 'te', 'MSP', 'LPN does not have a retail route.', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N');

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000019', '200000019', 'te', 'MSP', 'Order''s Retail route does not match the Shipment.', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N');

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000020', '200000020', 'te', 'MSP', 'LPN''s Retail route does not match the Shipment', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N');

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000021', '200000021', 'te', 'MSP', 'Cannot assign Order with retail route to a non-static route shipment', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N');

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000022', '200000022', 'te', 'MSP', 'Cannot assign LPN with retail route to a non-static route shipment', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N');

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG) 
VALUES ( SEQ_MESSAGE_MASTER_ID.nextval, '200000023', '200000023', 'te', 'MSP', 'Selection does not contain a unique static route. Cannot assign to Shipment.', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N');

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG) 
VALUES ( SEQ_MESSAGE_MASTER_ID.nextval, '200000024', '200000024', 'te', 'MSP', 'Cannot combine shipment with different static routes.', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N');

INSERT INTO message_master(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000025', '200000025', 'te', 'MSP', 'Shipment {0} is cancelled', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N');

commit;

-- DBTicket TE-2613
update xmenu_item set screen_mode = 0, navigation_key = 'te/manual/planning/msp/mspWorkspace.jsp' 
where navigation_key = '/te/manual/planning/jsp/viewManualShipmentWorkspace.jsflps' and name = 'Manual Load Planning';

MERGE INTO RESOURCES A
     USING (SELECT '/manh/te/manual/planning/msp/mspWorkspace.jsp' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/manh/te/manual/planning/msp/mspWorkspace.jsp',
               'MSHP',
               1,
               NULL);
			   
MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'MSA' AS PERMISSION_CODE
          FROM RESOURCES
         WHERE RESOURCES.URI = '/manh/te/manual/planning/msp/mspWorkspace.jsp') RP1 
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
        AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
        VALUES ( rp1.resource_id,rp1.permission_code);

commit;

-- -- DBTicket TE-2618
-- ALTER TABLE CONS_TEMPLATE drop CONSTRAINT CON_TMP_FK4;
-- 
-- ALTER TABLE CONS_TEMPLATE
--   ADD CONSTRAINT CON_TMP_FK4
-- FOREIGN KEY (FILTER_ID)
-- REFERENCES XFILTER(XFILTER_ID);

-- DBTicket TE-2625
EXEC SEQUPDT('XSCREEN_LABEL','XSCREEN_LABEL_ID','SEQUENCE_XSCREEN_LABEL_ID');

update xmenu_item set screen_mode=0, navigation_key = 'MLP.screen.MLPScreen' where navigation_key = 'te/manual/planning/msp/mspWorkspace.jsp';

--INSERT INTO xscreen (XSCREEN_ID, NAME, DESCRIPTION, SCREEN_TYPE, SCREEN_VERSION, SCREEN_MODE, IS_RESIZABLE, CREATED_SOURCE, CREATED_SOURCE_TYPE, CREATED_DTTM, LAST_UPDATED_SOURCE, LAST_UPDATED_SOURCE_TYPE, LAST_UPDATED_DTTM, DEFAULT_X, DEFAULT_Y, DEFAULT_WIDTH, DEFAULT_HEIGHT, CUSTOM_ATTRIB_OBJ_TYPE ) 
--VALUES (360001, 'MLP', 'Manual Load Planning', 2,2,0,1, NULL, 1, current_timestamp, NULL, 1, current_timestamp, 125,125,600,400, NULL ); 

MERGE INTO BUNDLE_META_DATA A
     USING (SELECT 'MLP' AS BUNDLE_NAME FROM DUAL) B
        ON (A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (BUNDLE_META_DATA_ID,
               BUNDLE_NAME,
               DESCRIPTION,
               DEF_SCREEN_TYPE_ID,
               DEF_APP_ID,
               HINT,
               IS_ACTIVE,
               IS_LABEL_BUNDLE,
               DEF_BU_ID)
       VALUES (seq_bundle_meta_data_id.NEXTVAL,
               'MLP',
               'TE MLP',
               10,
               NULL,
               NULL,
               1,
               1,
               -1);
       
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'unassignedOrderAssign','Assign','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'unassignedOrderSingle','Single','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'unassignedOrderCombine','Combine','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'unassignedOrderTitle','Unassigned Order List','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'OrderId','Order Id','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'Origin','Origin','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'Destination','Destination','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'fulfillmentStatus','Fulfillment Status','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'pickupStart','Pickup Start','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'deliveryEnd','Delivery End','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'orderPagingDisplay','Displaying orders {0} - {1} of {2}','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'orderPagingEmptyMsg','No orders to display','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'lpnAssign','Assign','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'lpnCreateShipment','Create Shipment','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'lpnId','Lpn Id','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'lpnStatus','Lpn Status','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'lpnFacilityStatus','Lpn Facility Status','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'lpnWeight','Weight','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'lpnPalletId','Pallet Id','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'lpnPagingDisplay','Displaying lpns {0} - {1} of {2}','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'lpnPagingEmptyMsg','No lpns to display','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'createAppointment','Create Appointment','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'closeShipment','Close Shipment','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'suspendRelease','Suspend/Release','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'loadingTasks','Loading Tasks','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'chaseTasks','Chase Tasks','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'stopResumeAssignment','Stop/Resume Assignment','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'printBOL','Print BOL','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'printAirWayBill','Print AirWway Bill','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'printManifest','Print Manifest','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'printShipment','Print Shipment','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'printInternational','Print International Doc','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'generateElectronicSED','Generate Electronic SED','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'cancelShipment','Deconsolidate Shipments','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'combineShipments','Combine Shipments','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'editShipVia','Edit Ship Via','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'shipmentPagingDisplay','Displaying shipments {0} - {1} of {2}','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'shipmentPagingEmptyMsg','No shipments to display','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'shipmentId','Shipment Id','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'businessUnit','Business Unit','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'assignedShipVia','Ship Via','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'shipmentStatus','Shipment Status','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'assignedEquipment','Equipment','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'shipmentTitle','Shipment List','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'orderUnAssign','Unassign','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'orderSplit','Split','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'orderMove','Move','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'lpnUnAssign','Unassign','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'lpnSplit','Split','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'lpnMove','Move','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'shipmentPopupOK','Ok','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'shipmentPopupCancel','Cancel','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'stopSequence','Stop Sequence','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'facility','Facility','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'dockId','Dock ID','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'city','City','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'state','State','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'stopAction','Stop Action','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'shipmentTabOrderTitle','Orders','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'shipmentTabStopTitle','Stops','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'shipmentTabLpnTitle','Lpns','MLP');
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'shipVia','Ship Via','MLP');

insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'unassignedOrderSingle');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'unassignedOrderAssign');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'unassignedOrderCombine');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'unassignedOrderTitle');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'OrderId');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Origin');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Destination');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'fulfillmentStatus');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'pickupStart');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'deliveryEnd');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'orderPagingDisplay');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'orderPagingEmptyMsg');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'lpnAssign');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'lpnCreateShipment');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'lpnId');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'lpnStatus');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'lpnFacilityStatus');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'lpnWeight');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'lpnPalletId');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'lpnPagingDisplay');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'lpnPagingEmptyMsg');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'createAppointment');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'closeShipment');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'suspendRelease');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'loadingTasks');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'chaseTasks');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'stopResumeAssignment');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'printBOL');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'printAirWayBill');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'printManifest');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'printShipment');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'printInternational');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'generateElectronicSED');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'cancelShipment');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'combineShipments');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'editShipVia');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'shipmentPagingDisplay');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'shipmentPagingEmptyMsg');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'shipmentId');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'businessUnit');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'assignedShipVia');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'shipmentStatus');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'assignedEquipment');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'shipmentTitle');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'orderUnAssign');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'orderSplit');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'orderMove');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'lpnUnAssign');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'lpnSplit');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'lpnMove');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'shipmentPopupOK');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'shipmentPopupCancel');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'stopSequence');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'facility');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'dockId');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'city');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'state');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'stopAction');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'shipmentTabOrderTitle');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'shipmentTabStopTitle');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'shipmentTabLpnTitle');
insert into xscreen_label (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'shipVia');

commit;

-- DBTicket TE-2623
MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/jsp/ConsolidationRunDetailsPresentation.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/jsp/ConsolidationRunDetailsPresentation.jsp',
                 'ACM',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/jsp/ConsolidationRunTemplateListPresentation.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/jsp/ConsolidationRunTemplateListPresentation.jsp',
                 'ACM',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/parameter/jsp/ConsolidationRunTemplateDetailsPresentation.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/parameter/jsp/ConsolidationRunTemplateDetailsPresentation.jsp',
                 'ACM',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/jsp/ConsolidationRunListPresentation.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/jsp/ConsolidationRunListPresentation.jsp',
                 'ACM',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/jsp/ProcessConsolidationRunTemplate.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/jsp/ProcessConsolidationRunTemplate.jsp',
                 'ACM',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/admin/SurePostAddressDataLoader.jsp' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/admin/SurePostAddressDataLoader.jsp',
               'ACM',
               1,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/jsp/ProcessMultipleConsolidationRuntemplates.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/jsp/ProcessMultipleConsolidationRuntemplates.jsp',
                 'ACM',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/parameter/jsp/EditConsolidationParameters.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/parameter/jsp/EditConsolidationParameters.jsp',
                 'ACM',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/parameter/jsp/EditPlanningEngineParametersProcess.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/parameter/jsp/EditPlanningEngineParametersProcess.jsp',
                 'ACM',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/parame/ter/jsp/ViewConsolidationParameters.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/parame/ter/jsp/ViewConsolidationParameters.jsp',
                 'ACM',
                 1,
                 NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/shipserv/jsp/teShipViaLookup.jsp' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/shipserv/jsp/teShipViaLookup.jsp',
               'ACM',
               1,
               NULL);

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID,
                   'MCO' AS PERMISSION_CODE,
                   1 AS URI_TYPE_ID
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/jsp/ConsolidationRunDetailsPresentation.jsp'
                   AND RESOURCES.URI_TYPE_ID = 1) RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (RP1.RESOURCE_ID, RP1.PERMISSION_CODE);

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID,
                   'COP' AS PERMISSION_CODE,
                   1 AS URI_TYPE_ID
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/jsp/ConsolidationRunTemplateListPresentation.jsp'
                   AND RESOURCES.URI_TYPE_ID = 1) RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (RP1.RESOURCE_ID, RP1.PERMISSION_CODE);

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID,
                   'CMA' AS PERMISSION_CODE,
                   1 AS URI_TYPE_ID
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/parameter/jsp/ConsolidationRunTemplateDetailsPresentation.jsp'
                   AND RESOURCES.URI_TYPE_ID = 1) RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (RP1.RESOURCE_ID, RP1.PERMISSION_CODE);

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID,
                   'COP' AS PERMISSION_CODE,
                   1 AS URI_TYPE_ID
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/jsp/ConsolidationRunListPresentation.jsp'
                   AND RESOURCES.URI_TYPE_ID = 1) RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (RP1.RESOURCE_ID, RP1.PERMISSION_CODE);

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID,
                   'COP' AS PERMISSION_CODE,
                   1 AS URI_TYPE_ID
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/jsp/ProcessConsolidationRunTemplate.jsp'
                   AND RESOURCES.URI_TYPE_ID = 1) RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (RP1.RESOURCE_ID, RP1.PERMISSION_CODE);

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID,
                   'APCDIT' AS PERMISSION_CODE,
                   1 AS URI_TYPE_ID
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/admin/SurePostAddressDataLoader.jsp'
                   AND RESOURCES.URI_TYPE_ID = 1) RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (RP1.RESOURCE_ID, RP1.PERMISSION_CODE);

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID,
                   'COP' AS PERMISSION_CODE,
                   1 AS URI_TYPE_ID
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/jsp/ProcessMultipleConsolidationRuntemplates.jsp'
                   AND RESOURCES.URI_TYPE_ID = 1) RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (RP1.RESOURCE_ID, RP1.PERMISSION_CODE);

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID,
                   'CMA' AS PERMISSION_CODE,
                   1 AS URI_TYPE_ID
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/parameter/jsp/EditConsolidationParameters.jsp'
                   AND RESOURCES.URI_TYPE_ID = 1) RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (RP1.RESOURCE_ID, RP1.PERMISSION_CODE);

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID,
                   'CMA' AS PERMISSION_CODE,
                   1 AS URI_TYPE_ID
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/parameter/jsp/EditPlanningEngineParametersProcess.jsp'
                   AND RESOURCES.URI_TYPE_ID = 1) RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (RP1.RESOURCE_ID, RP1.PERMISSION_CODE);

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID,
                   'CMA' AS PERMISSION_CODE,
                   1 AS URI_TYPE_ID
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/parame/ter/jsp/ViewConsolidationParameters.jsp'
                   AND RESOURCES.URI_TYPE_ID = 1) RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (RP1.RESOURCE_ID, RP1.PERMISSION_CODE);

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID,
                   'CMA' AS PERMISSION_CODE,
                   1 AS URI_TYPE_ID
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/shipserv/jsp/teShipViaLookup.jsp'
                   AND RESOURCES.URI_TYPE_ID = 1) RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (RP1.RESOURCE_ID, RP1.PERMISSION_CODE);

COMMIT;

-- DBTicket TE-2627
BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE HAZMAT_UPS_INTL ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE HAZMAT_UPS_DOM ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE HAZMAT_FEDEX_GND ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE SHIP_VIA_EXCLUDE ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE USER_PROFILE ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE OPT_PARAM_LIST ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE OPT_PARAM ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE OPT_ENGINE_TYPE ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE OPT_ENGINE ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE LITRL_XREF ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE LABEL_XREF ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE LABEL_SUBSTITUTIONS ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE ENTERPRISE_MASTER ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE EDI_XREF ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE EC_MESSAGE_CONSUMER ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE EC_MESSAGE ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE EC_CONSUMER_MSG_TYPE ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE EC_CONSUMER ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE COMPANY_OPT_ENGINE ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/

CREATE OR REPLACE TRIGGER HAZMAT_UPS_INT_LST_UPDT_DT_TRG
  BEFORE UPDATE ON HAZMAT_UPS_INTL
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER HAZMAT_UPS_DOM_LST_UPDT_DT_TRG
  BEFORE UPDATE ON HAZMAT_UPS_DOM
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER HAZMAT_FEDEX_G_LST_UPDT_DT_TRG
  BEFORE UPDATE ON HAZMAT_FEDEX_GND
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER SHIP_VIA_EXCLU_LST_UPDT_DT_TRG
  BEFORE UPDATE ON SHIP_VIA_EXCLUDE
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER USER_PROFILE_LST_UPDT_DT_TRG
  BEFORE UPDATE ON USER_PROFILE
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER OPT_PARAM_LIST_LST_UPDT_DT_TRG
  BEFORE UPDATE ON OPT_PARAM_LIST
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER OPT_PARAM_LST_UPDT_DT_TRG
  BEFORE UPDATE ON OPT_PARAM
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER OPT_ENGINE_TYP_LST_UPDT_DT_TRG
  BEFORE UPDATE ON OPT_ENGINE_TYPE
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER OPT_ENGINE_LST_UPDT_DT_TRG
  BEFORE UPDATE ON OPT_ENGINE
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER LITRL_XREF_LST_UPDT_DT_TRG
  BEFORE UPDATE ON LITRL_XREF
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER LABEL_XREF_LST_UPDT_DT_TRG
  BEFORE UPDATE ON LABEL_XREF
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER LABEL_SUBSTITU_LST_UPDT_DT_TRG
  BEFORE UPDATE ON LABEL_SUBSTITUTIONS
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER ENTERPRISE_MAS_LST_UPDT_DT_TRG
  BEFORE UPDATE ON ENTERPRISE_MASTER
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER EDI_XREF_LST_UPDT_DT_TRG
  BEFORE UPDATE ON EDI_XREF
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER EC_MESSAGE_CON_LST_UPDT_DT_TRG
  BEFORE UPDATE ON EC_MESSAGE_CONSUMER
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER EC_MESSAGE_LST_UPDT_DT_TRG
  BEFORE UPDATE ON EC_MESSAGE
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER EC_CONSUMER_MS_LST_UPDT_DT_TRG
  BEFORE UPDATE ON EC_CONSUMER_MSG_TYPE
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER EC_CONSUMER_LST_UPDT_DT_TRG
  BEFORE UPDATE ON EC_CONSUMER
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER CONS_TEMPLATE_LST_UPDT_DT_TRG
  BEFORE UPDATE ON CONS_TEMPLATE
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER COMPANY_OPT_EN_LST_UPDT_DT_TRG
  BEFORE UPDATE ON COMPANY_OPT_ENGINE
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
END;
/

---- DBTicket TE-2637
--update xmenu_item set screen_version=2, navigation_key = 'MLP.screen.MLPScreen' where name = 'Manual Load Planning' and navigation_key = '/te/manual/planning/jsp/viewManualShipmentWorkspace.jsflps' and base_section_name = 'Distribution';
--commit;

-- DBTicket TE-2635

DECLARE
v_constraint_count NUMBER;
BEGIN
    SELECT COUNT (*)
        INTO v_constraint_count
        from user_constraints
       WHERE constraint_name = 'CON_TMP_FK4';
   
   IF (v_constraint_count > 0)
   THEN      
      EXECUTE IMMEDIATE 'alter table CONS_TEMPLATE disable constraint CON_TMP_FK4';            
   END IF;
   
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (SQLERRM);      
END;
/


-- DBTicket TE-2634
insert into val_tag
(
    tag, ui_desc
)
select 'wo', 'Work Order' from dual union all
select 'cc', 'Cycle Count' from dual union all
select 'shpmt', 'Shipment' from dual;

insert into val_tag_type_xref
(
    type, tag
)
select '1', 'shpmt' from dual union all
select '1', 'item' from dual union all
select '1', 'wo' from dual union all
select '2', 'wo' from dual union all
select '2', 'ilpn' from dual union all
select '2', 'shpmt' from dual union all
select '2', 'cc' from dual union all
select '3', 'wo' from dual union all
select '3', 'shpmt' from dual union all
select '4', 'cc' from dual;

commit;

create or replace procedure wm_val_exec_sql
(
    p_cursor          in number,
    p_val_job_dtl_id  in val_job_dtl.val_job_dtl_id%type,
    p_sql_id          in val_job_dtl.sql_id%type,
    p_sql_text        in val_sql.sql_text%type,
    p_bind_var_list   in val_sql.bind_var_list%type
)
as
    v_txn_id            val_result_hist.txn_id%type := wm_val_get_txn_id();
    v_sql_passed        number(1);
    v_num_actual_rows   number(9);
    v_num_expected_rows number(9);
    v_curr_row_num      number(9);
    v_int_col           number(9);
    v_varchar2_col      varchar2(50);
    v_max_char_len      number(3) := 50;
    v_msg               val_result_hist.msg%type;
begin
    wm_val_log('SQL ' || p_sql_id || ': ' || substr(p_sql_text, 1, 500));
    v_sql_passed := 0;
    dbms_sql.parse(p_cursor, p_sql_text, dbms_sql.native);

    -- run time bind values can override pre-configured values
    for bind_rec in
    (
        select vsb.bind_var, coalesce(vrb.bind_val, vsb.bind_val) bind_val
        from val_sql_binds vsb
        left join val_runtime_binds vrb on vrb.bind_var = vsb.bind_var
            and vrb.val_job_dtl_id = vsb.val_job_dtl_id
            and vrb.txn_id = v_txn_id
        where vsb.val_job_dtl_id = p_val_job_dtl_id
    )
    loop
        if (instr(p_bind_var_list, bind_rec.bind_var, 1, 1) = 0)
        then
            raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to configured bind var :' || bind_rec.bind_var 
                || ' not found in parsed bind list');
        end if;

        dbms_sql.bind_variable(p_cursor, bind_rec.bind_var, bind_rec.bind_val);
        wm_val_log('Bound <' || bind_rec.bind_val || '> to :' || bind_rec.bind_var,
            p_sql_log_lvl => 2);
    end loop;

    -- define datatype for each selected column in the sql
    for col_rec in
    (
        select vpsl.pos, vpsl.col_type
        from val_parsed_select_list vpsl
        where vpsl.sql_id = p_sql_id
        order by vpsl.pos
    )
    loop
        if (col_rec.col_type = 2)
        then
            dbms_sql.define_column(p_cursor, col_rec.pos, v_int_col);
            wm_val_log('Defined integer col at pos ' || col_rec.pos, p_sql_log_lvl => 2);
        elsif (col_rec.col_type in (1, 96))
        then
            dbms_sql.define_column(p_cursor, col_rec.pos, v_varchar2_col,
                v_max_char_len);
            wm_val_log('Defined varchar2 col at pos ' || col_rec.pos, p_sql_log_lvl => 2);
        else
            raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to finding unsupported data type '
                || col_rec.col_type);
        end if;
    end loop;

    select max(ver.row_num)
    into v_num_expected_rows
    from val_expected_results ver
    where ver.val_job_dtl_id = p_val_job_dtl_id;

    -- run it and fetch returned rows
    v_num_actual_rows := dbms_sql.execute(p_cursor);
    /*
    -- enable this for sql injection
    if (v_num_actual_rows != v_num_expected_rows)
    then
        update val_job_dtl vjd
        set vjd.mod_date_time = sysdate, vjd.last_run_passed = 0
        where vjd.val_job_dtl_id = p_val_job_dtl_id;
        dbms_output.put_line('Skipped processing Job dtl ' || p_val_job_dtl_id || ',sql ' || p_sql_id
            || ' expected rows ' || v_num_expected_rows || ', actual ' || v_num_actual_rows);
        
        -- skip validations of row level output
        continue;
    end if;
    */

    v_sql_passed := 0;
    v_curr_row_num := 0;
    while (dbms_sql.fetch_rows(p_cursor) > 0)
    loop
        -- for each selected column, based on datatype, get selected data
        -- into appropriate vars and compare
        v_curr_row_num := v_curr_row_num + 1;
        wm_val_log('Processing row ' || v_curr_row_num, p_sql_log_lvl => 2);
        if (v_curr_row_num > v_num_expected_rows)
        then
            raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to fetching more rows than expected (' 
                || v_num_expected_rows || ')');
        end if;

        for col_rec in
        (
            -- UI needs to ensure that all expected result columns are entered into the results table
            select vpsl.pos, vpsl.col_type, ver.expected_value
            from val_parsed_select_list vpsl
            join val_expected_results ver on ver.pos = vpsl.pos
                and ver.row_num = v_curr_row_num
                and ver.val_job_dtl_id = p_val_job_dtl_id
            where vpsl.sql_id = p_sql_id
            order by vpsl.pos
        )
        loop
            v_msg := null;
            v_varchar2_col := null;
            v_int_col := null;
            if (col_rec.col_type = 2)
            then
                dbms_sql.column_value(p_cursor, col_rec.pos, v_int_col);
                v_sql_passed := case when v_int_col = to_number(col_rec.expected_value)
                    then 1 else 0 end;
            elsif (col_rec.col_type in (1, 96))
            then
                dbms_sql.column_value(p_cursor, col_rec.pos, v_varchar2_col);
                v_sql_passed := case when v_varchar2_col = col_rec.expected_value
                    then 1 else 0 end;
            else
                raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to finding unsupported data type '
                || col_rec.col_type);
            end if;

            wm_val_log('Expecting ' || col_rec.expected_value || ' at ('
                || col_rec.pos || ',' || v_curr_row_num || '), found '
                || coalesce(v_varchar2_col, to_char(v_int_col), 'null'), 
                p_sql_log_lvl => 2);

            if (v_sql_passed = 0)
            then
                raise_application_error(-20050, 'Sql ' || p_sql_id
                    || ' failed due to finding ' 
                    || coalesce(v_varchar2_col, to_char(v_int_col), 'null')
                    || ' in (row ' || v_curr_row_num || ', Pos ' || col_rec.pos
                    || '), expected ' || col_rec.expected_value );
            end if;
        end loop; -- columns fetch

        if (v_sql_passed = 0)
        then
            raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to not finding matching result data');
        end if;
    end loop; -- rows fetch

    if (v_curr_row_num < v_num_expected_rows and v_msg is null)
    then
        raise_application_error(-20050, 'Sql ' || p_sql_id || 
            ' failed due to returning fewer rows (' || v_curr_row_num 
            || ') than expected (' || v_num_expected_rows || ')');
    end if;

    wm_val_add_job_result(p_val_job_dtl_id,
        case v_sql_passed when 0 then v_msg else null end, v_sql_passed);
end;
/
show errors;

create or replace trigger wm_val_derive_select_info
after insert or update on val_sql
referencing new as new
for each row
declare
    v_cursor        int := dbms_sql.open_cursor;
    v_col_list      dbms_sql.desc_tab2;
    v_col_count     int;
    v_max_cols      number(2) := 5;
-- todo: need to normalize chars and bytes
    v_max_col_len   number(3) := 200;
    v_current_date  date := sysdate;
begin
    dbms_sql.parse(v_cursor, :new.sql_text, dbms_sql.native);
    dbms_sql.describe_columns2(v_cursor, v_col_count, v_col_list);
    if (v_col_count > v_max_cols)
    then
        raise_application_error(-20050, 'Max number of selected columns '
            || 'supported is currently ' || v_max_cols);
    /*elsif (v_col_count between 2 and v_max_cols)
    then
        :new.is_simple = 0;
    else
        :new.is_simple = 1;*/
    end if;

    delete from val_parsed_select_list vpsl
    where vpsl.sql_id = :new.sql_id;

    for i in 1..v_col_count
    loop
        if (v_col_list(i).col_type not in (1, 2, 96))
        then
            raise_application_error(-20050, 'Supported data types of selected '
                || 'columns are currently varchar2/char/number (found ' 
                || v_col_list(i).col_type || ').');
        end if;
        
        if (v_col_list(i).col_max_len > v_max_col_len)
        then
            raise_application_error(-20050, 'Selected column width/precision of '
                || v_col_list(i).col_max_len || ' exceeds supported maximum.');
        end if;

        insert into val_parsed_select_list
        (
            sql_id, pos, col_type
        )
        values
        (
            :new.sql_id, i, v_col_list(i).col_type
        );
    end loop;
    
    dbms_sql.close_cursor(v_cursor);
end;
/
show errors;

create or replace trigger wm_val_prep_sql
before insert or update on val_sql
referencing new as new
for each row
declare
    v_job_dependency  number(1);
    v_bind_var_list   val_sql.bind_var_list%type;
    v_bind_var        val_sql_binds.bind_var%type;
    v_bind_delim      varchar2(2) := 'bv';
    v_bind_pattern    varchar2(20) := ':[a-z]+\w*';
    --v_oper_pattern    varchar2(20) := '(<|>|(like)|+|-|=) *';
begin
    if (:new.format_sql = 0)
    then
        :new.sql_text := :new.raw_text;
    else
        :new.sql_text := regexp_replace(replace(replace(replace(trim(:new.raw_text), chr(10), ' '), chr(9), ' '), chr(13), ' '), '( ){2,}', ' ');
        if (instr(:new.sql_text, '''', 1, 1) = 0)
        then
            :new.sql_text := lower(:new.sql_text);
        end if;
    end if;

    for i in 1..regexp_count(:new.sql_text, v_bind_pattern, 1, 'i')
    loop
        v_bind_var := regexp_substr(:new.sql_text, v_bind_pattern, 1, i, 'i');
        exit when v_bind_var is null;
        v_bind_var_list := v_bind_var_list || substr(v_bind_var, 2) || ',';
    end loop;
    v_bind_var_list := lower(v_bind_var_list);

    if (updating and coalesce(v_bind_var_list, ' ') != coalesce(:old.bind_var_list, ' '))
    then
        select case when exists
        (
            select 1
            from val_job_dtl vjd
            where vjd.sql_id = :new.sql_id
        ) then 1 else 0 end
        into v_job_dependency
        from dual;
        
        if (v_job_dependency = 1)
        then
            raise_application_error(-20050, 'Cannot modify bind vars. Purge dependent jobs first.');
        end if;
    end if;
    :new.bind_var_list := v_bind_var_list;

    :new.text_len := length(replace(:new.sql_text, ' '));
end;
/
show errors;

-- DBTicket TE-2641
update xmenu_item set screen_version=2 
where name = 'Manual Load Planning' 
and navigation_key = 'MLP.screen.MLPScreen' 
and base_section_name = 'Distribution';

commit;

-- DBTicket TE-2622
ALTER TABLE PARCEL_ZONE_DATA MODIFY (ZONES_PER_SL VARCHAR2(1000), TRANSIT_DAYS_PER_SL VARCHAR2(1000));

-- DBTicket TE-2646
update resource_permission set permission_code = 'MNFLPN' where resource_id = (select resource_id from resources where uri = '/te/manifest/ui/fedexServerTranInqDetail.xhtml');
update resource_permission set permission_code = 'MNFLPN' where resource_id = (select resource_id from resources where uri = '/te/manifest/ui/CloseManifest.xhtml');
update resource_permission set permission_code = 'MNFLPN' where resource_id = (select resource_id from resources where uri = '/te/manifest/ui/GenerateSED.xhtml');
update resource_permission set permission_code = 'MNFLPN' where resource_id = (select resource_id from resources where uri = '/te/manifest/ui/InternationalDocumentList.xhtml');
update resource_permission set permission_code = 'MNFLPN' where resource_id = (select resource_id from resources where uri = '/te/manifest/ui/LiteralTranslationsList.xhtml');
update resource_permission set permission_code = 'MNFLPN' where resource_id = (select resource_id from resources where uri = '/te/manifest/ui/manifestedLpnDetail.xhtml');
update resource_permission set permission_code = 'MNFLPN' where resource_id = (select resource_id from resources where uri = '/te/manifest/ui/manifestedLpnExceptionList.xhtml');
update resource_permission set permission_code = 'MNFLPN' where resource_id = (select resource_id from resources where uri = '/te/manifest/ui/manifestHdrDetail.xhtml');
update resource_permission set permission_code = 'MNFLPN' where resource_id = (select resource_id from resources where uri = '/te/manifest/ui/manifestLpnChrgList.xhtml');
update resource_permission set permission_code = 'MNFLPN' where resource_id = (select resource_id from resources where uri = '/te/manifest/ui/manifestLpnDetailist.xhtml');
update resource_permission set permission_code = 'MNFLPN' where resource_id = (select resource_id from resources where uri = '/te/manifest/ui/MessageMasterList.xhtml');
update resource_permission set permission_code = 'MNFLPN' where resource_id = (select resource_id from resources where uri = '/te/manifest/ui/warningDialog.xhtml');
commit;

-- DBTicket TE-2655
DELETE FROM RESOURCE_PERMISSION
      WHERE RESOURCE_ID =
               (SELECT RESOURCE_ID
                  FROM RESOURCES
                 WHERE URI =
                          '/te/optimizationmgmt/consolidation/parame/ter/jsp/ViewConsolidationParameters.jsp' AND URI_TYPE_ID = 1)
            AND PERMISSION_CODE = 'CMA';

DELETE FROM RESOURCES WHERE URI = '/te/optimizationmgmt/consolidation/parame/ter/jsp/ViewConsolidationParameters.jsp' AND URI_TYPE_ID = 1 AND HTTP_METHOD IS NULL;

MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/parameter/jsp/ViewConsolidationParameters.jsp' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
INSERT (RESOURCE_ID, URI, MODULE, URI_TYPE_ID, HTTP_METHOD) 
VALUES (SEQ_RESOURCE_ID.NEXTVAL, '/te/optimizationmgmt/consolidation/parameter/jsp/ViewConsolidationParameters.jsp', 'ACM', 1, NULL); 

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID,
                   'CMA' AS PERMISSION_CODE,
                   1 AS URI_TYPE_ID
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/parameter/jsp/ViewConsolidationParameters.jsp'
                   AND RESOURCES.URI_TYPE_ID = 1) RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
INSERT (RESOURCE_ID, PERMISSION_CODE)
VALUES (RP1.RESOURCE_ID, RP1.PERMISSION_CODE);

commit;

-- DBTicket TE-2658
update resource_permission set permission_code='COP' 
where resource_id in(select resource_id from RESOURCES where uri = '/te/optimizationmgmt/consolidation/jsp/ConsolidationRunDetailsPresentation.jsp');

commit;

-- DBTicket TE-2672
EXEC SEQUPDT('XSCREEN_LABEL','XSCREEN_LABEL_ID','SEQUENCE_XSCREEN_LABEL_ID');
MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'mode' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT(LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
   values (seq_label_id.nextVal,'mode','MODE','MLP');
   
MERGE INTO LABEL L
 USING (SELECT 'MLP' BUNDLE_NAME, 'serviceLevel' KEY FROM DUAL) B
    ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT (LABEL_ID,KEY,VALUE,BUNDLE_NAME)
   values (seq_label_id.nextVal,'serviceLevel','Service Level','MLP');
   
MERGE INTO LABEL L
 USING (SELECT 'MLP' BUNDLE_NAME, 'carrier' KEY FROM DUAL) B
    ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT (LABEL_ID,KEY,VALUE,BUNDLE_NAME)
   values (seq_label_id.nextVal,'carrier','Carrier','MLP');
   
MERGE INTO LABEL L
 USING (SELECT 'MLP' BUNDLE_NAME, 'shipViaPagingDisplay' KEY FROM DUAL) B
    ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT (LABEL_ID,KEY,VALUE,BUNDLE_NAME)
   values (seq_label_id.nextVal,'shipViaPagingDisplay','Displaying Ship Via {0} - {1} of {2}','MLP');
   
MERGE INTO LABEL L
 USING (SELECT 'MLP' BUNDLE_NAME, 'shipViaPagingEmptyMsg' KEY FROM DUAL) B
    ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
   values (seq_label_id.nextVal,'shipViaPagingEmptyMsg','No Ship Via to display','MLP');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'mode' LABEL_KEY,
                   '360001' XSCREEN_ID,
                   'MLP' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
	INSERT (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)  
	VALUES (sequence_xscreen_label_id.nextval, '360001', 'MLP', 'mode');
       
MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'serviceLevel' LABEL_KEY,
                   '360001' XSCREEN_ID,
                   'MLP' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
	INSERT (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)  
	VALUES (sequence_xscreen_label_id.nextval, '360001', 'MLP', 'serviceLevel');
       
MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'carrier' LABEL_KEY,
                   '360001' XSCREEN_ID,
                   'MLP' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
	INSERT (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)  
	VALUES (sequence_xscreen_label_id.nextval, '360001', 'MLP', 'carrier');
       
MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'shipViaPagingDisplay' LABEL_KEY,
                   '360001' XSCREEN_ID,
                   'MLP' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
	INSERT (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)  
	VALUES (sequence_xscreen_label_id.nextval, '360001', 'MLP', 'shipViaPagingDisplay');
       
MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'shipViaPagingEmptyMsg' LABEL_KEY,
                   '360001' XSCREEN_ID,
                   'MLP' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
	INSERT (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)  
	VALUES (sequence_xscreen_label_id.nextval, '360001', 'MLP', 'shipViaPagingEmptyMsg');
	
commit;

-- DBTicket TE-2675
MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/parameter/jsp/ConsolidationRunTemplateDetailsPresentation.jsp'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/te/optimizationmgmt/consolidation/parameter/jsp/ConsolidationRunTemplateDetailsPresentation.jsp',
                 'ACM',
                 1,
                 NULL);

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/parameter/jsp/ConsolidationRunTemplateDetailsPresentation.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (rp1.resource_id, rp1.permission_code);
	   
commit;	   

-- DBTicket TE-2686
MERGE INTO RESOURCES A
     USING (SELECT '/te/optimizationmgmt/consolidation/jsp/StartMultipleRunConfirmationPopup.jsp' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
    INSERT (RESOURCE_ID,
           URI,
           MODULE,
           URI_TYPE_ID,
           HTTP_METHOD)
    VALUES (SEQ_RESOURCE_ID.NEXTVAL,
           '/te/optimizationmgmt/consolidation/jsp/StartMultipleRunConfirmationPopup.jsp',
           'ACM',
           1,
           NULL);
               
MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'COP' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/optimizationmgmt/consolidation/jsp/StartMultipleRunConfirmationPopup.jsp') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
    INSERT (RESOURCE_ID, PERMISSION_CODE)
    VALUES ( (SELECT RESOURCE_ID
               FROM RESOURCES
              WHERE URI = '/te/optimizationmgmt/consolidation/jsp/StartMultipleRunConfirmationPopup.jsp'),
           'COP');		   
commit;

-- DBTicket TE-2685
DELETE FROM ui_menu_item
      WHERE LINK IN
               ('#{parcelZoneRateBackingBean.saveAction}',
                'return navCheck(''parcelZoneRateBackingBean'',''deleteAction'',''dataTable'')',
                'AddNewRow(''dataTable'')')
            AND ui_menu_screen_id IN (SELECT ui_menu_screen_id
                                        FROM ui_menu_screen
                                       WHERE screen_id = '40000030');

MERGE INTO UI_MENU_SCREEN UMS1
     USING (SELECT SCREEN_ID
              FROM UI_MENU_SCREEN
             WHERE SCREEN_ID = 40000034
            UNION
            SELECT 40000034 FROM DUAL) UMS2
        ON (UMS1.SCREEN_ID = UMS2.SCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (UI_MENU_SCREEN_ID,
               MENU_ID,
               SCREEN_ID,
               PERSONALIZATION_LEVEL,
               PERSONALIZATION_ID,
               USER_ID)
       VALUES (UI_MENU_SCREEN_SEQ.NEXTVAL,
               1,
               40000034,
               'SYSTEM',
               NULL,
               NULL);

MERGE INTO UI_MENU_ITEM A
     USING (SELECT 'Add' AS DISPLAY_TEXT,
                   'AddNewRow(''dataTable'')' AS LINK,
                   (SELECT MIN (ui_menu_Screen_id)
                      FROM ui_menu_screen
                     WHERE screen_id = '40000034')
                      AS UI_MENU_SCREEN_ID,
                   1 AS MENU_ID
              FROM DUAL) B
        ON (    A.DISPLAY_TEXT = B.DISPLAY_TEXT
            AND A.LINK = B.LINK
            AND A.UI_MENU_SCREEN_ID = B.UI_MENU_SCREEN_ID
            AND A.MENU_ID = B.MENU_ID)
WHEN NOT MATCHED
THEN
   INSERT     (UI_MENU_ITEM_ID,
               UI_MENU_SCREEN_ID,
               MENU_ID,
               IS_DISPLAY,
               DISPLAY_TEXT,
               LINK,
               LINK_TYPE,
               LOCN_FLAG,
               ITEM_SEQ_NBR,
               UI_MENU_COMP_ID,
               IS_MORE,
               AJAX_ACTION,
               AJAX_RERENDER,
               AJAX_ONCOMPLETE,
               RENDERED,
               IMAGE_URL)
       VALUES (ui_menu_item_seq.NEXTVAL,
               (SELECT ui_menu_screen_id
                  FROM ui_menu_screen
                 WHERE screen_id = '40000034'),
               1,
               1,
               'Add',
               'AddNewRow(''dataTable'')',
               'JS',
               7,
               1,
               'dataForm:page-content_footer-panel',
               0,
               NULL,
               NULL,
               NULL,
               'TRUE',
               NULL);

MERGE INTO UI_MENU_ITEM A
     USING (SELECT 'Delete' AS DISPLAY_TEXT,
                   'return navCheck(''parcelZoneRateBackingBean'',''deleteAction'',''dataTable'')'
                      AS LINK,
                   (SELECT MIN (ui_menu_Screen_id)
                      FROM ui_menu_screen
                     WHERE screen_id = '40000034')
                      AS UI_MENU_SCREEN_ID,
                   1 AS MENU_ID
              FROM DUAL) B
        ON (    A.DISPLAY_TEXT = B.DISPLAY_TEXT
            AND A.LINK = B.LINK
            AND A.UI_MENU_SCREEN_ID = B.UI_MENU_SCREEN_ID
            AND A.MENU_ID = B.MENU_ID)
WHEN NOT MATCHED
THEN
   INSERT     (UI_MENU_ITEM_ID,
               UI_MENU_SCREEN_ID,
               MENU_ID,
               IS_DISPLAY,
               DISPLAY_TEXT,
               LINK,
               LINK_TYPE,
               LOCN_FLAG,
               ITEM_SEQ_NBR,
               UI_MENU_COMP_ID,
               IS_MORE,
               AJAX_ACTION,
               AJAX_RERENDER,
               AJAX_ONCOMPLETE,
               RENDERED,
               IMAGE_URL)
       VALUES (
                 ui_menu_item_seq.NEXTVAL,
                 (SELECT ui_menu_screen_id
                    FROM ui_menu_screen
                   WHERE screen_id = '40000034'),
                 1,
                 1,
                 'Delete',
                 'return navCheck(''parcelZoneRateBackingBean'',''deleteAction'',''dataTable'')',
                 'JS',
                 7,
                 2,
                 'dataForm:page-content_footer-panel',
                 0,
                 NULL,
                 NULL,
                 NULL,
                 'TRUE',
                 NULL);

MERGE INTO UI_MENU_ITEM A
     USING (SELECT 'Save' AS DISPLAY_TEXT,
                   '#{parcelZoneRateBackingBean.saveAction}' AS LINK,
                   (SELECT MIN (ui_menu_Screen_id)
                      FROM ui_menu_screen
                     WHERE screen_id = '40000034')
                      AS UI_MENU_SCREEN_ID,
                   1 AS MENU_ID
              FROM DUAL) B
        ON (    A.DISPLAY_TEXT = B.DISPLAY_TEXT
            AND A.LINK = B.LINK
            AND A.UI_MENU_SCREEN_ID = B.UI_MENU_SCREEN_ID
            AND A.MENU_ID = B.MENU_ID)
WHEN NOT MATCHED
THEN
   INSERT     (UI_MENU_ITEM_ID,
               UI_MENU_SCREEN_ID,
               MENU_ID,
               IS_DISPLAY,
               DISPLAY_TEXT,
               LINK,
               LINK_TYPE,
               LOCN_FLAG,
               ITEM_SEQ_NBR,
               UI_MENU_COMP_ID,
               IS_MORE,
               AJAX_ACTION,
               AJAX_RERENDER,
               AJAX_ONCOMPLETE,
               RENDERED,
               IMAGE_URL)
       VALUES (ui_menu_item_seq.NEXTVAL,
               (SELECT ui_menu_screen_id
                  FROM ui_menu_screen
                 WHERE screen_id = '40000034'),
               1,
               1,
               'Save',
               '#{parcelZoneRateBackingBean.saveAction}',
               'JSFA',
               7,
               3,
               'dataForm:page-content_footer-panel',
               0,
               NULL,
               NULL,
               NULL,
               'TRUE',
               NULL);

DELETE FROM ui_menu_item
      WHERE ui_menu_screen_id IN (SELECT ui_menu_screen_id
                                    FROM ui_menu_screen
                                   WHERE screen_id = '40000031')
            AND link IN ('#{parcelTransitTimeBackingBean.saveAction}');

MERGE INTO UI_MENU_SCREEN UMS1
     USING (SELECT SCREEN_ID
              FROM UI_MENU_SCREEN
             WHERE SCREEN_ID = 40000035
            UNION
            SELECT 40000035 FROM DUAL) UMS2
        ON (UMS1.SCREEN_ID = UMS2.SCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (UI_MENU_SCREEN_ID,
               MENU_ID,
               SCREEN_ID,
               PERSONALIZATION_LEVEL,
               PERSONALIZATION_ID,
               USER_ID)
       VALUES (ui_menu_Screen_seq.NEXTVAL,
               '1',
               '40000035',
               'SYSTEM',
               NULL,
               NULL);

MERGE INTO UI_MENU_ITEM A
     USING (SELECT 'Save' AS DISPLAY_TEXT,
                   '#{parcelTransitTimeBackingBean.saveAction}' AS LINK,
                   (SELECT MIN (ui_menu_Screen_id)
                      FROM ui_menu_screen
                     WHERE screen_id = '40000035')
                      AS UI_MENU_SCREEN_ID,
                   1 AS MENU_ID
              FROM DUAL) B
        ON (    A.DISPLAY_TEXT = B.DISPLAY_TEXT
            AND A.LINK = B.LINK
            AND A.UI_MENU_SCREEN_ID = B.UI_MENU_SCREEN_ID
            AND A.MENU_ID = B.MENU_ID)
WHEN NOT MATCHED
THEN
   INSERT     (UI_MENU_ITEM_ID,
               UI_MENU_SCREEN_ID,
               MENU_ID,
               IS_DISPLAY,
               DISPLAY_TEXT,
               LINK,
               LINK_TYPE,
               LOCN_FLAG,
               ITEM_SEQ_NBR,
               UI_MENU_COMP_ID,
               IS_MORE,
               AJAX_ACTION,
               AJAX_RERENDER,
               AJAX_ONCOMPLETE,
               RENDERED,
               IMAGE_URL)
       VALUES (ui_menu_item_seq.NEXTVAL,
               (SELECT ui_menu_screen_id
                  FROM ui_menu_screen
                 WHERE screen_id = '40000035'),
               1,
               1,
               'Save',
               '#{parcelTransitTimeBackingBean.saveAction}',
               'JSFA',
               7,
               3,
               'dataForm:page-content_footer-panel',
               0,
               NULL,
               NULL,
               NULL,
               'TRUE',
               NULL);
			   
MERGE INTO XMENU_ITEM L
     USING (SELECT 'Tariff' NAME, '/te/parcelRate/ui/ParcelTransitTime.xhtml' NAVIGATION_KEY,'Contract Management' BASE_SECTION_NAME
              FROM DUAL) B
        ON (L.NAME = B.NAME AND L.NAVIGATION_KEY = B.NAVIGATION_KEY AND L.BASE_SECTION_NAME=B.BASE_SECTION_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (XMENU_ITEM_ID,
                        NAME,
                        SHORT_NAME,
                        NAVIGATION_KEY,
                        ICON,
                        BGCOLOR,
                        BASE_SECTION_NAME,
                        BASE_PART_NAME)
       VALUES ( (SELECT NVL(MAX(XMENU_ITEM_ID), 360000) + 1
                 FROM XMENU_ITEM
                WHERE TO_CHAR(XMENU_ITEM_ID) LIKE '3600%'),
             'Tariff',
             'Tariff',
             '/te/parcelRate/ui/ParcelTransitTime.xhtml',
             'default-icon',
             '#002E5F',
             'Contract Management',
             'GENERAL');

MERGE INTO XMENU_ITEM_APP L
     USING (SELECT  (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE     NAME = 'Tariff'
                       AND BASE_SECTION_NAME = 'Contract Management'
                       AND BASE_PART_NAME = 'GENERAL'
                       AND NAVIGATION_KEY='/te/parcelRate/ui/ParcelTransitTime.xhtml') XMENU_ITEM_ID, (SELECT APP_ID
                  FROM APP
                 WHERE APP_SHORT_NAME = 'WMS') APP_ID
              FROM DUAL) B
        ON (L.XMENU_ITEM_ID = B.XMENU_ITEM_ID AND L.APP_ID = B.APP_ID)
WHEN NOT MATCHED
THEN
   INSERT   (XMENU_ITEM_ID, APP_ID)
       VALUES (
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE     NAME = 'Tariff'
                       AND BASE_SECTION_NAME = 'Contract Management'
                       AND BASE_PART_NAME = 'GENERAL'
                       AND NAVIGATION_KEY='/te/parcelRate/ui/ParcelTransitTime.xhtml'),
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_SHORT_NAME = 'WMS'));

MERGE INTO XMENU_ITEM_PERMISSION L
     USING (SELECT (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE     NAME = 'Tariff'
                       AND BASE_SECTION_NAME = 'Contract Management'
                       AND BASE_PART_NAME = 'GENERAL'
                       AND NAVIGATION_KEY= '/te/parcelRate/ui/ParcelTransitTime.xhtml') XMENU_ITEM_ID,'VPCLTRANSITTIME' PERMISSION_CODE
              FROM DUAL) B
        ON (L.XMENU_ITEM_ID = B.XMENU_ITEM_ID AND L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT  (XMENU_ITEM_ID, PERMISSION_CODE)
       VALUES (
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE     NAME = 'Tariff'
                       AND BASE_SECTION_NAME = 'Contract Management'
                       AND BASE_PART_NAME = 'GENERAL'
                       AND NAVIGATION_KEY= '/te/parcelRate/ui/ParcelTransitTime.xhtml'),
               'VPCLTRANSITTIME');

MERGE INTO XMENU_ITEM_PERMISSION L
     USING (SELECT (SELECT XMENU_ITEM_ID
                 FROM XMENU_ITEM
                 WHERE     NAME = 'Tariff'
                       AND BASE_SECTION_NAME = 'Contract Management'
                       AND BASE_PART_NAME = 'GENERAL'
                       AND NAVIGATION_KEY= '/te/parcelRate/ui/ParcelTransitTime.xhtml') XMENU_ITEM_ID,'APCLTRANSITTIME' PERMISSION_CODE
              FROM DUAL) B
        ON (L.XMENU_ITEM_ID = B.XMENU_ITEM_ID AND L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT  (XMENU_ITEM_ID, PERMISSION_CODE)
       VALUES (
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE     NAME = 'Tariff'
                       AND BASE_SECTION_NAME = 'Contract Management'
                       AND BASE_PART_NAME = 'GENERAL'
                       AND NAVIGATION_KEY= '/te/parcelRate/ui/ParcelTransitTime.xhtml'),
               'APCLTRANSITTIME');

MERGE INTO XMENU_ITEM L
     USING (SELECT 'Tariff' NAME, '/te/parcelRate/ui/ParcelZoneRate.xhtml' NAVIGATION_KEY,'Contract Management' BASE_SECTION_NAME
              FROM DUAL) B
        ON (L.NAME = B.NAME AND L.NAVIGATION_KEY = B.NAVIGATION_KEY AND L.BASE_SECTION_NAME=B.BASE_SECTION_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (XMENU_ITEM_ID,
                        NAME,
                        SHORT_NAME,
                        NAVIGATION_KEY,
                        ICON,
                        BGCOLOR,
                        BASE_SECTION_NAME,
                        BASE_PART_NAME)
       VALUES ( (SELECT NVL(MAX(XMENU_ITEM_ID), 360000) + 1
                 FROM XMENU_ITEM
                WHERE TO_CHAR(XMENU_ITEM_ID) LIKE '3600%'),
             'Tariff',
             'Tariff',
             '/te/parcelRate/ui/ParcelZoneRate.xhtml',
             'default-icon',
             '#002E5F',
             'Contract Management',
             'GENERAL');                                           

MERGE INTO XMENU_ITEM_APP L
     USING (SELECT  (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE     NAME = 'Tariff'
                       AND BASE_SECTION_NAME = 'Contract Management'
                       AND BASE_PART_NAME = 'GENERAL'
                       AND NAVIGATION_KEY='/te/parcelRate/ui/ParcelZoneRate.xhtml') XMENU_ITEM_ID, (SELECT APP_ID
                  FROM APP
                 WHERE APP_SHORT_NAME = 'WMS') APP_ID
              FROM DUAL) B
        ON (L.XMENU_ITEM_ID = B.XMENU_ITEM_ID AND L.APP_ID = B.APP_ID)
WHEN NOT MATCHED
THEN
   INSERT   (XMENU_ITEM_ID, APP_ID)
       VALUES (
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE     NAME = 'Tariff'
                       AND BASE_SECTION_NAME = 'Contract Management'
                       AND BASE_PART_NAME = 'GENERAL'
                       AND NAVIGATION_KEY='/te/parcelRate/ui/ParcelZoneRate.xhtml'),
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_SHORT_NAME = 'WMS'));                                                 

MERGE INTO XMENU_ITEM_PERMISSION L
     USING (SELECT (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE     NAME = 'Tariff'
                       AND BASE_SECTION_NAME = 'Contract Management'
                      AND BASE_PART_NAME = 'GENERAL'
                       AND NAVIGATION_KEY= '/te/parcelRate/ui/ParcelZoneRate.xhtml') XMENU_ITEM_ID,'VPCLZONERATE' PERMISSION_CODE
              FROM DUAL) B
        ON (L.XMENU_ITEM_ID = B.XMENU_ITEM_ID AND L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT  (XMENU_ITEM_ID, PERMISSION_CODE)
       VALUES (
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE     NAME = 'Tariff'
                       AND BASE_SECTION_NAME = 'Contract Management'
                       AND BASE_PART_NAME = 'GENERAL'
                       AND NAVIGATION_KEY= '/te/parcelRate/ui/ParcelZoneRate.xhtml'),
               'VPCLZONERATE');                               
                                                   
MERGE INTO XMENU_ITEM_PERMISSION L
     USING (SELECT (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE     NAME = 'Tariff'
                       AND BASE_SECTION_NAME = 'Contract Management'
                       AND BASE_PART_NAME = 'GENERAL'
                       AND NAVIGATION_KEY= '/te/parcelRate/ui/ParcelZoneRate.xhtml') XMENU_ITEM_ID,'APCLZONERATE' PERMISSION_CODE
              FROM DUAL) B
        ON (L.XMENU_ITEM_ID = B.XMENU_ITEM_ID AND L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT  (XMENU_ITEM_ID, PERMISSION_CODE)
       VALUES (
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE     NAME = 'Tariff'
                       AND BASE_SECTION_NAME = 'Contract Management'
                       AND BASE_PART_NAME = 'GENERAL'
                       AND NAVIGATION_KEY= '/te/parcelRate/ui/ParcelZoneRate.xhtml'),
               'APCLZONERATE');
			   			   
commit;			   

-- DBTicket TE-2697
DELETE FROM resource_permission
      WHERE resource_id =
               (SELECT resource_id
                  FROM RESOURCES
                 WHERE uri =
                          '/te/optimizationmgmt/consolidation/jsp/ProcessMultipleConsolidationRuntemplates.jsp');

DELETE FROM resource_permission
      WHERE resource_id =
               (SELECT resource_id
                  FROM RESOURCES
                 WHERE uri =
                          '/te/optimizationmgmt/consolidation/jsp/ProcessMultipleConsolidationRunTemplates.jsp');

DELETE FROM resources
      WHERE URI =
               '/te/optimizationmgmt/consolidation/jsp/ProcessMultipleConsolidationRunTemplates.jsp';

UPDATE RESOURCES
   SET URI =
          '/te/optimizationmgmt/consolidation/jsp/ProcessMultipleConsolidationRunTemplates.jsp'
 WHERE URI =
          '/te/optimizationmgmt/consolidation/jsp/ProcessMultipleConsolidationRuntemplates.jsp';

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID,
                   'COP' AS PERMISSION_CODE,
                   1 AS URI_TYPE_ID
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/te/optimizationmgmt/consolidation/jsp/ProcessMultipleConsolidationRunTemplates.jsp'
                   AND RESOURCES.URI_TYPE_ID = 1) RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (RP1.RESOURCE_ID, RP1.PERMISSION_CODE);

COMMIT;  
 
-- DBTicket TE-2712
call quiet_drop('procedure','wm_unplan_splits_intrnl');
call quiet_drop('procedure','manh_wave_load_orders_for_rule');
call quiet_drop('procedure','wm_unplan_routed_orders');

-- commenting this ticket from here as it is moved to WM through ticket WM-41984
---- DBTicket TE-2725
--create or replace procedure wm_process_variance_for_cs
--(
--    p_user_id             in user_profile.user_id%type,
--    p_tc_company_id       in shipment.tc_company_id%type,
--    p_shipment_id         in shipment.shipment_id%type,
--    p_manifest_id         in shipment.manifest_id%type,
--    p_caller_id           in varchar2 default null
--)
--as
--    v_use_locking               number(1) := 0;
--    v_exec_pre_cleanup          varchar2(1);
--    v_max_log_lvl               number(1) := 0;
--    v_commit_freq               number(5) := 99999;
--    v_mode_id                   sys_code.code_id%type := 'CS';
--    v_row_count                 number(9) := 0;
--    v_act_trk_tran_nbr      prod_trkg_tran.tran_nbr%type;
--    v_act_trk_rec_count     number(9) := 0;
--    v_shipment_ref_code     varchar2(3) := '41';
--    v_order_ref_code        varchar2(3) := '01';
--    v_lpn_ref_code          varchar2(3) := '02';
--    type t_lpn_id is        table of lpn.lpn_id%type index by binary_integer;
--    va_lpn_id               t_lpn_id;
--begin
--    update shipment s
--    set s.shipment_closed_indicator = 1, s.last_updated_dttm = sysdate,
--        s.last_updated_source = p_user_id
--    where s.shipment_id = p_shipment_id or s.manifest_id = p_manifest_id;
--    commit;
--
--    select /*+ result_cache */ to_number(coalesce(trim(substr(sc.misc_flags, 3, 5)), '99999')),
--        case when substr(sc.misc_flags, 2, 1) = 'Y' then 1 else 0 end,
--        to_number(coalesce(trim(substr(sc.misc_flags, 8, 1)), '0')),
--        coalesce(substr(sc.misc_flags, 9, 1), 'N')
--    into v_commit_freq, v_use_locking, v_max_log_lvl, v_exec_pre_cleanup
--    from sys_code sc 
--    where sc.rec_type = 'S' and sc.code_type = '520' and sc.code_id = v_mode_id;
--    v_commit_freq := case v_commit_freq when 0 then 99999 else v_commit_freq end;
--
--    delete from tmp_store_master;
--    delete from tmp_order_splits_master;
--    delete from tmp_log_parm;
--    insert into tmp_log_parm
--    (
--        log_lvl, pgm_id, module, msg_id, ref_code_1, ref_value_1, user_id
--    )
--    values
--    (
--        v_max_log_lvl, 'wm_process_variance_for_cs', 'WAVE', '1094', v_mode_id,
--        to_char(coalesce(p_shipment_id, p_manifest_id)), p_user_id
--    );
--
--    if (v_exec_pre_cleanup = 'Y')
--    then
--        wm_cs_log('Beginning custom sanitization');
--        wm_pre_cs_cleanup();
--    end if;
--
--    if (p_shipment_id is not null)
--    then
--        select l.lpn_id
--        bulk collect into va_lpn_id
--        from lpn l
--        where l.lpn_facility_status < 40
--            and l.shipment_id =
--            (
--                select s.shipment_id
--                from shipment s
--                where s.shipment_id = p_shipment_id and s.static_route_id is not null
--            );
--        v_row_count := sql%rowcount;
--        if (v_row_count > 0)
--        then
--            wm_cs_log('Updated ' || v_row_count || ' static retail routed lpns; returning.', p_sql_log_level => 0);
--        end if;
--    else
--        select l.lpn_id
--        bulk collect into va_lpn_id
--        from lpn l
--        where l.lpn_facility_status < 40
--            and l.static_route_id is not null
--            and l.shipment_id in
--            (
--                select s.shipment_id
--                from shipment s
--                where s.manifest_id= p_manifest_id and s.shipment_status < 80 
--                    and s.is_cancelled = 0
--            );
--
--        v_row_count := sql%rowcount;
--        if (v_row_count > 0)
--        then
--            wm_cs_log('Updated ' || v_row_count || ' static retail routed lpns; continuing.', p_sql_log_level => 0);
--        end if;
--    end if;
--        wm_cs_log('Collected ' || va_lpn_id.count || ' static retail routed lpns;', p_sql_log_level => 1);
--    
--    if (va_lpn_id.count > 0)
--    then
--        -- activity tracking is at the origin facility level
--        select decode(wp.trk_prod_flag, 'Y', prod_trkg_tran_id_seq.nextval, null)
--        into v_act_trk_tran_nbr
--        from whse_parameters wp
--        join facility f on f.facility_id = wp.whse_master_id
--        where f.facility_id =
--        (
--            select mh.o_facility_id
--            from manifest_hdr mh
--            where mh.manifest_id = p_manifest_id
--            union all
--            select s.o_facility_number
--            from shipment s
--            where s.shipment_id = p_shipment_id
--        );
--        
--        if (v_act_trk_tran_nbr is not null)
--        then
--            delete from tmp_act_trk_lpns;
--            forall i in 1..va_lpn_id.count
--            insert into tmp_act_trk_lpns t (lpn_id)
--            values (va_lpn_id(i));
--
--            insert into prod_trkg_tran
--            (
--                prod_trkg_tran_id, tran_type, tran_code, tran_nbr, seq_nbr, whse,
--                menu_optn_name, user_id, cd_master_id, ref_code_id_1, ref_code_id_2, 
--                ref_code_id_3, ref_field_1, ref_field_2, ref_field_3, mod_date_time,
--                create_date_time, module_name
--            ) 
--            select prod_trkg_tran_id_seq.nextval, '800', '013', v_act_trk_tran_nbr,
--                v_act_trk_rec_count + rownum seq_nbr, f.whse, p_caller_id, p_user_id, 
--                l.tc_company_id, v_shipment_ref_code, v_order_ref_code, v_lpn_ref_code,
--                l.tc_shipment_id ref_field_1, l.tc_order_id ref_field_2, 
--                l.tc_lpn_id ref_field_3, sysdate, sysdate, 'Shipping'
--            from tmp_act_trk_lpns t
--            join lpn l on l.lpn_id = t.lpn_id
--            join facility f on f.facility_id = l.c_facility_id;
--            v_act_trk_rec_count := v_act_trk_rec_count + sql%rowcount;
--            wm_cs_log('Wrote act trk recs for lpns ' || v_act_trk_rec_count, p_sql_log_level => 1);
--        end if;
--        
--        forall i in 1..va_lpn_id.count
--        update lpn l
--        set l.shipment_id = null, l.tc_shipment_id = null,
--            l.last_updated_source = p_user_id, l.last_updated_dttm = sysdate  
--        where l.lpn_id = va_lpn_id(i);
--        wm_cs_log('updated ' || sql%rowcount || ' static retail routed lpns; returning.', p_sql_log_level => 1);
--    end if;
--
--    if ((p_shipment_id is not null or p_manifest_id is not null) and va_lpn_id.count > 0)
--    then
--        return;
--    end if;
--
--    if (v_use_locking = 1)
--    then
--        -- turn off for non-retail CM or CS
--        if (p_manifest_id is not null)
--        then
--            select case when exists
--            (
--                select 1
--                from manifested_lpn ml
--                join lpn l on l.lpn_id = ml.lpn_id
--                join orders o on o.order_id = l.order_id and o.d_facility_id is null
--                where ml.manifest_id = p_manifest_id
--            ) then 0 else 1 end
--            into v_use_locking
--            from dual;
--        else
--            select case when exists
--                (
--                    select 1
--                    from orders o 
--                    where o.shipment_id = p_shipment_id and o.d_facility_id is null
--                )
--                or exists
--                (
--                    select 1
--                    from order_split os
--                    join orders o on o.order_id = os.order_id
--                        and o.d_facility_id is null
--                    where os.shipment_id = p_shipment_id and os.is_cancelled = 0
--                ) then 0 else 1 end
--            into v_use_locking
--            from dual;
--        end if;
--
--        if (v_use_locking = 0)
--        then
--            wm_cs_log('Locking turned off!', p_sql_log_level => 0);
--        end if;
--    end if;
--
--    wm_cs_log('Beginning base sanitization');
--    wm_sanitize_orphaned_lpns(p_user_id, p_shipment_id, p_manifest_id);
--    commit;
--
--    wm_cs_log('Closing shpmt/manifest', p_sql_log_level => 0);
--    wm_load_splits_for_input_crit(v_use_locking, p_manifest_id => p_manifest_id,
--        p_cs_shipment_id => p_shipment_id, p_mode => '_close_');
--
--    wm_remove_variance_wrapper(p_user_id, p_tc_company_id, v_use_locking,
--        v_commit_freq, p_caller_id, p_shipment_id, p_manifest_id,
--        v_act_trk_tran_nbr, v_act_trk_rec_count);
--
--    -- recalc wt/vol after handling variance
--    merge into shipment s
--    using
--    (
--        select s.shipment_id, sum(coalesce(l.weight, l.estimated_weight, 0)) wt,
--            sum(coalesce(l.actual_volume, l.estimated_volume, 0)) vol
--        from shipment s
--        join lpn l on l.shipment_id = s.shipment_id
--        where s.shipment_id = p_shipment_id or s.manifest_id = p_manifest_id
--        group by s.shipment_id
--    ) iv on (iv.shipment_id = s.shipment_id)
--    when matched then
--    update set s.last_updated_source = p_user_id, s.planned_weight = iv.wt,
--        s.planned_volume = iv.vol, s.last_updated_dttm = sysdate;        
--    wm_cs_log('Wt/vol recalculated for shpmts ' || sql%rowcount);
--
--    commit;
--    wm_cs_log('Closed shpmt/manifest', p_sql_log_level => 0);
--end;
--/
--
--show errors;

-- DBTicket TE-2750
CREATE OR REPLACE FORCE VIEW MANIFEST_EXCEPTIONS
(
   TC_COMPANY_ID,
   MANIFEST_ID,
   LPN_ID,
   TC_LPN_ID,
   TC_PARENT_LPN_ID,
   TC_SHIPMENT_ID,
   TC_ORDER_ID,
   LPN_FACILITY_STATUS
)
AS
   (SELECT L.TC_COMPANY_ID L1,
           ML.MANIFEST_ID L2,
           L.LPN_ID L3,
           L.TC_LPN_ID L4,
           L.TC_PARENT_LPN_ID L5,
           L.TC_SHIPMENT_ID L6,
           L.TC_ORDER_ID || '-' || L.ORDER_ID L7,
           TO_CHAR (L.LPN_FACILITY_STATUS) L8
      FROM LPN L, MANIFESTED_LPN ML
     WHERE     L.SHIPMENT_ID = ML.SHIPMENT_ID
           AND ML.MANIFEST_ID IN (SELECT manifest_id
                                    FROM manifest_hdr
                                   WHERE manifest_status_id = 10)
           AND L.LPN_FACILITY_STATUS < 40
    UNION
    SELECT RS.COID,
           RS.mid L0,
           RS.LINEITEMID L1,
           RS.TCORDERID L2,
           RS.TCORDERID L3,
           TO_CHAR (RS.ORDERID) L4,
           TO_CHAR (RS.QTYVARIANCE) L5,
           RS.QTYUOM L6
      FROM (  SELECT ORD.TC_COMPANY_ID COID,
                     MLPN.MANIFEST_ID mid,
                     ORD.ORDER_ID ORDERID,
                     ORD.TC_ORDER_ID TCORDERID,
                     OLI.LINE_ITEM_ID AS LINEITEMID,
                     (COALESCE (OLI.ORDER_QTY, 0)
                      - COALESCE (OLI.USER_CANCELED_QTY, 0)
                      - SUM (
                           CASE
                              WHEN L.LPN_FACILITY_STATUS < 20
                              THEN
                                 COALESCE (LD.INITIAL_QTY, 0)
                              ELSE
                                 COALESCE (LD.SIZE_VALUE, 0)
                           END))
                        QTYVARIANCE,
                     MIN (SIZE_UOM.SIZE_UOM) AS QTYUOM
                FROM MANIFESTED_LPN MLPN
                     INNER JOIN ORDERS ORD
                        ON ORD.ORDER_ID = MLPN.ORDER_ID
                           AND ORD.IS_CANCELLED = 0
                           AND MLPN.MANIFEST_ID IN
                                  (SELECT manifest_id
                                     FROM manifest_hdr
                                    WHERE manifest_status_id = 10)
                     INNER JOIN ORDER_LINE_ITEM OLI
                        ON OLI.ORDER_ID = ORD.ORDER_ID AND OLI.IS_CANCELLED = 0
                     INNER JOIN LPN L
                        ON     L.ORDER_ID = ORD.ORDER_ID
                           AND L.LPN_FACILITY_STATUS < 99
                           AND L.LPN_TYPE = 1
                           AND L.INBOUND_OUTBOUND_INDICATOR = 'O'
                     LEFT JOIN LPN_DETAIL LD
                        ON LD.LPN_ID = L.LPN_ID
                           AND LD.DISTRIBUTION_ORDER_DTL_ID = OLI.LINE_ITEM_ID
                     LEFT JOIN SIZE_UOM
                        ON SIZE_UOM.SIZE_UOM_ID = OLI.QTY_UOM_ID_BASE
            GROUP BY ORD.TC_COMPANY_ID,
                     MLPN.MANIFEST_ID,
                     ORD.ORDER_ID,
                     ORD.TC_ORDER_ID,
                     OLI.LINE_ITEM_ID,
                     OLI.ORDER_QTY,
                     OLI.USER_CANCELED_QTY) RS
     WHERE RS.QTYVARIANCE > 0);
     /
	 
-- DBTicket TE-2769
MERGE INTO MESSAGE_MASTER A
USING (SELECT  '200000026' MSG_ID,
                '200000026' KEY,
                'te' ILS_MODULE,
                'ErrorMessage' BUNDLE_NAME
     FROM DUAL) B
     on ( A.KEY=B.KEY and  A.BUNDLE_NAME=B.BUNDLE_NAME )
     WHEN NOT  MATCHED THEN
INSERT(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG ) 
VALUES(SEQ_MESSAGE_MASTER_ID.nextval, '200000026', '200000026', 'te', 'MSP', 'One or more selected lpns are part of wave process.', 'ErrorMessage', 'SYSTEM', 'ERROR', NULL, 'N' );   

update label set VALUE = 'Stop Assignment' where KEY='stopResumeAssignment' and BUNDLE_NAME='MLP';

commit;

-- DBTicket TE-2774
delete from message_display where disp_key in (select key from message_master where msg_module='MSP');
delete from message_master where MSG_MODULE='MSP';
  
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000000','200000000','te','MSP','Selected orders must have the same billing method.','ErrorMessage','SYSTEM','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000001','200000001','te','MSP','One or more selected orders are already assigned to a shipment {0}.','ErrorMessage','SYSTEM','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000002','200000002','te','MSP','One or more selected orders are already canceled {0}.','ErrorMessage','SYSTEM','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000003','200000003','te','MSP','One or more selected orders are part of wave process.','ErrorMessage','SYSTEM','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000004','200000004','te','MSP','Orders designated {0} does not match.','ErrorMessage','USER','WARNING',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000005','200000005','te','MSP','Orders designated ship via does not match with shipment assigned ship via.','ErrorMessage','USER','WARNING',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000006','200000006','te','MSP','Orders billing method does not match with shipment billing method.','ErrorMessage','SYSTEM','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000007','200000007','te','MSP','Shipment {0} is closed.','ErrorMessage','SYSTEM','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000008','200000008','te','MSP','Assignment has been stopped for the shipment {0}.','ErrorMessage','SYSTEM','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000009','200000009','te','MSP','The selection contains oLPNs that are already loaded.','ErrorMessage','SYSTEM','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000010','200000010','te','MSP','Shipment status is greater than accepted.','ErrorMessage','SYSTEM','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000011','200000011','te','MSP','Cannot create shipment for retail store distribution order(s).','ErrorMessage','SYSTEM','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000012','200000012','te','MSP','Cannot create shipment for retail store distribution oLPN(s).','ErrorMessage','SYSTEM','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000013','200000013','te','MSP','One or more selected oLPNs are already assigned to a Shipment {0}.','ErrorMessage','SYSTEM','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000014','200000014','te','MSP','One or more selected oLPNs are already Canceled {0}.','ErrorMessage','SYSTEM','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000015','200000015','te','MSP','Customer order cannot be assigned to retail route shipment.','ErrorMessage','SYSTEM','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000016','200000016','te','MSP','Customer order oLPN cannot be assigned to retail route shipment.','ErrorMessage','SYSTEM','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000017','200000017','te','MSP','Order does not have a retail route.','ErrorMessage','USER','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000018','200000018','te','MSP','oLPN does not have a retail route.','ErrorMessage','USER','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000019','200000019','te','MSP','Order''s Retail route does not match with the shipment route.','ErrorMessage','USER','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000020','200000020','te','MSP','oLPN''s Retail route does not match with the shipment route.','ErrorMessage','USER','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000021','200000021','te','MSP','Cannot assign order with retail route to a non-static route shipment.','ErrorMessage','SYSTEM','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000022','200000022','te','MSP','Cannot assign oLPN(s) with retail route to a non-static route shipment','ErrorMessage','SYSTEM','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000023','200000023','te','MSP','Selection does not contain a unique static route. Cannot assign to shipment.','ErrorMessage','SYSTEM','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000024','200000024','te','MSP','One or more selected oLPNs are part of wave process.','ErrorMessage','SYSTEM','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000025','200000025','te','MSP','Shipment {0} is cancelled.','ErrorMessage','SYSTEM','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000026','200000026','te','MSP','Retail oLPN does not have destination facility. Cannot Assign.','ErrorMessage','SYSTEM','ERROR',null,'N');
Insert into message_master (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values (SEQ_MESSAGE_MASTER_ID.nextval,'200000027','200000027','te','MSP','Shipment does not have matching destination facility for selected oLPN(s).','ErrorMessage','USER','WARNING',null,'N');

commit;

-- DBTicket TE-2778
alter table val_sql_binds add bind_val_int number(10);
alter table val_runtime_binds add bind_val_int number(10);

insert into val_tag
(
    tag, ui_desc
)
select 'rte', 'Route' from dual;


insert into val_tag_type_xref
(
    type, tag
)
select '2', 'rte' from dual union all
select '3', 'rte' from dual;

commit;

create or replace procedure wm_val_lock_unlock_sql
(
    p_sql_id    in val_sql.sql_id%type default null,
    p_ident     in val_sql.ident%type default null,
    p_lock      in val_sql.is_locked%type
)
as
    v_sql_id    val_sql.sql_id%type;
    v_ident     val_sql.ident%type;
begin
    update val_sql vs
    set vs.is_locked = p_lock
    where vs.sql_id = p_sql_id or vs.ident = p_ident
    returning vs.sql_id, vs.ident into v_sql_id, v_ident;
    dbms_output.put_line(case when p_lock = 1 then 'Locked' else 'Unlocked' end
        || ' sql ' || v_sql_id || '(' || v_ident || ')');
    
    commit;
end;
/

create or replace trigger wm_val_prep_sql
before insert or update on val_sql
referencing new as new
for each row
declare
    v_job_dependency  number(1);
    v_bind_var_list   val_sql.bind_var_list%type;
    v_bind_var        val_sql_binds.bind_var%type;
    v_bind_delim      varchar2(2) := 'bv';
    v_bind_pattern    varchar2(20) := ':[a-z]+\w*';
    --v_oper_pattern    varchar2(20) := '(<|>|(like)|+|-|=) *';
begin
    if (:new.format_sql = 0)
    then
        :new.sql_text := :new.raw_text;
    else
        :new.sql_text := regexp_replace(replace(replace(replace(trim(:new.raw_text), chr(10), ' '), chr(9), ' '), chr(13), ' '), '( ){2,}', ' ');
        if (instr(:new.sql_text, '''', 1, 1) = 0)
        then
            :new.sql_text := lower(:new.sql_text);
        end if;
    end if;

    for i in 1..regexp_count(:new.sql_text, v_bind_pattern, 1, 'i')
    loop
        v_bind_var := regexp_substr(:new.sql_text, v_bind_pattern, 1, i, 'i');
        exit when v_bind_var is null;
        v_bind_var_list := v_bind_var_list || substr(v_bind_var, 2) || ',';
    end loop;
    v_bind_var_list := lower(v_bind_var_list);

    if (updating and coalesce(v_bind_var_list, ' ') != coalesce(:old.bind_var_list, ' '))
    then
        select case when exists
        (
            select 1
            from val_job_dtl vjd
            where vjd.sql_id = :new.sql_id
        ) then 1 else 0 end
        into v_job_dependency
        from dual;
        
        if (v_job_dependency = 1)
        then
            raise_application_error(-20050, 'Cannot modify bind vars. Purge dependent jobs first.');
        end if;
    end if;
    :new.bind_var_list := v_bind_var_list;

    :new.text_len := length(replace(:new.sql_text, ' '));
end;
/

create or replace trigger wm_val_derive_select_info
after insert or update on val_sql
referencing new as new
for each row
declare
    v_cursor        int := dbms_sql.open_cursor;
    v_col_list      dbms_sql.desc_tab2;
    v_col_count     int;
    v_max_cols      number(2) := 5;
-- todo: need to normalize chars and bytes
    v_max_col_len   number(3) := 200;
    v_current_date  date := sysdate;
begin
    dbms_sql.parse(v_cursor, :new.sql_text, dbms_sql.native);
    dbms_sql.describe_columns2(v_cursor, v_col_count, v_col_list);
    if (v_col_count > v_max_cols)
    then
        raise_application_error(-20050, 'Max number of selected columns '
            || 'supported is currently ' || v_max_cols);
    /*elsif (v_col_count between 2 and v_max_cols)
    then
        :new.is_simple = 0;
    else
        :new.is_simple = 1;*/
    end if;

    delete from val_parsed_select_list vpsl
    where vpsl.sql_id = :new.sql_id;

    for i in 1..v_col_count
    loop
        if (v_col_list(i).col_type not in (1, 2, 96))
        then
            raise_application_error(-20050, 'Supported data types of selected '
                || 'columns are currently varchar2/char/number (found ' 
                || v_col_list(i).col_type || ').');
        end if;
        
        if (v_col_list(i).col_max_len > v_max_col_len)
        then
            raise_application_error(-20050, 'Selected column width/precision of '
                || v_col_list(i).col_max_len || ' exceeds supported maximum.');
        end if;

        insert into val_parsed_select_list
        (
            sql_id, pos, col_type
        )
        values
        (
            :new.sql_id, i, v_col_list(i).col_type
        );
    end loop;
    
    dbms_sql.close_cursor(v_cursor);
end;
/

create or replace procedure wm_val_add_hints_to_sql
(
    p_hints         in val_sql.sql_hints%type,
    p_ident         in val_sql.ident%type default null,
    p_sql_id        in val_sql.sql_id%type default null
)
as
    v_sql_id        val_sql.sql_id%type;
    v_ident         val_sql.ident%type;
begin
    update val_sql vs
    set vs.sql_hints = trim(p_hints), vs.mod_date_time = sysdate
    where vs.ident = p_ident or vs.sql_id = p_sql_id
    returning vs.sql_id, vs.ident into v_sql_id, v_ident;
    
    dbms_output.put_line('Added hint(s) ' || p_hints || ' to sql '
        || v_sql_id || '(' || v_ident || ')');
    
    commit;
end;
/

create or replace procedure wm_val_add_bind_var_to_sql
(
    p_vjd_id        in val_job_dtl.val_job_dtl_id%type,
    p_bind_var      in val_sql_binds.bind_var%type,
    p_bind_val      in val_sql_binds.bind_val%type default null,
    p_bind_val_int  in val_sql_binds.bind_val_int%type default null
)
as
begin
    -- persist any preset bind values for the sqls to be used within the above
    -- job details
    -- UI should validate bind var entry in the context of each sql
    -- bind vars known only at runtime need to be filled in by the calling code
    -- or a wrapper at some point prior to validation
    insert into val_sql_binds
    (
        val_job_dtl_id, bind_var, bind_val, bind_val_int
    )
    values
    (
        p_vjd_id, lower(p_bind_var), p_bind_val, p_bind_val_int
    );
end;
/

create or replace procedure wm_val_get_runtime_bind_vars
(
    p_user_id           in varchar2,
    p_job_name          in val_job_hdr.job_name%type,
    p_rt_bind_list_csv  in varchar2,
    p_txn_id            in val_result_hist.txn_id%type
)
as
    v_rt_bind_list_csv_lc varchar2(1000) := lower(p_rt_bind_list_csv);
    v_bind_val            val_runtime_binds.bind_val%type;
    v_bind_val_int        val_runtime_binds.bind_val_int%type;
    v_bind_val_len        number(5);
    v_start_pos           number(5);
    pragma autonomous_transaction;
begin
    for sql_rec in
    (
        select vjd.val_job_dtl_id, vsb.bind_var
        from val_job_hdr vjh
        join val_job_dtl vjd on vjd.val_job_hdr_id = vjh.val_job_hdr_id
            and vjd.is_locked = 0
        join val_sql_binds vsb on vsb.val_job_dtl_id = vjd.val_job_dtl_id
            and vsb.bind_val is null
        where vjh.job_name = p_job_name and vjh.is_locked = 0
    )
    loop
        v_start_pos := instr(v_rt_bind_list_csv_lc, sql_rec.bind_var, 1, 1);
        if (v_start_pos = 0)
        then
            continue;
        end if;
        
        v_start_pos := v_start_pos + length(sql_rec.bind_var) + 1;
        v_bind_val_len := instr(p_rt_bind_list_csv, ',', v_start_pos, 1)
            - v_start_pos;
        v_bind_val := substr(p_rt_bind_list_csv, v_start_pos, v_bind_val_len);

        insert into val_runtime_binds
        (
            txn_id, val_job_dtl_id, bind_var, bind_val, user_id, 
            create_date_time, mod_date_time, bind_val_int
        )
        values
        (
            p_txn_id, sql_rec.val_job_dtl_id, sql_rec.bind_var, v_bind_val,
            p_user_id, sysdate, sysdate, v_bind_val_int
        );
    end loop;
    
    commit;
end;
/

create or replace procedure wm_val_exec_sql
(
    p_cursor          in number,
    p_val_job_dtl_id  in val_job_dtl.val_job_dtl_id%type,
    p_sql_id          in val_job_dtl.sql_id%type,
    p_sql_text        in val_sql.sql_text%type,
    p_bind_var_list   in val_sql.bind_var_list%type
)
as
    v_txn_id            val_result_hist.txn_id%type := wm_val_get_txn_id();
    v_sql_passed        number(1);
    v_num_actual_rows   number(9);
    v_num_expected_rows number(9);
    v_curr_row_num      number(9);
    v_int_col           number(9);
    v_varchar2_col      varchar2(50);
    v_max_char_len      number(3) := 50;
    v_msg               val_result_hist.msg%type;
begin
    wm_val_log('SQL ' || p_sql_id || ': ' || substr(p_sql_text, 1, 500));
    v_sql_passed := 0;
    dbms_sql.parse(p_cursor, p_sql_text, dbms_sql.native);

    -- run time bind values can override pre-configured values
    for bind_rec in
    (
        select vsb.bind_var, coalesce(vrb.bind_val, vsb.bind_val) bind_val,
            coalesce(vrb.bind_val_int, vsb.bind_val_int) bind_val_int
        from val_sql_binds vsb
        left join val_runtime_binds vrb on vrb.bind_var = vsb.bind_var
            and vrb.val_job_dtl_id = vsb.val_job_dtl_id
            and vrb.txn_id = v_txn_id
        where vsb.val_job_dtl_id = p_val_job_dtl_id
    )
    loop
        if (instr(p_bind_var_list, bind_rec.bind_var, 1, 1) = 0)
        then
            raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to configured bind var :' || bind_rec.bind_var 
                || ' not found in parsed bind list');
        end if;

        if (substr(bind_rec.bind_var, 1, 2) = 'i_')
        then
            dbms_sql.bind_variable(p_cursor, bind_rec.bind_var, bind_rec.bind_val_int);
            wm_val_log('Bound <' || bind_rec.bind_val_int || '> to :' || bind_rec.bind_var,
                p_sql_log_lvl => 2);
        else
            dbms_sql.bind_variable(p_cursor, bind_rec.bind_var, bind_rec.bind_val);
            wm_val_log('Bound <' || bind_rec.bind_val || '> to :' || bind_rec.bind_var,
                p_sql_log_lvl => 2);
        end if;
    end loop;

    -- define datatype for each selected column in the sql
    for col_rec in
    (
        select vpsl.pos, vpsl.col_type
        from val_parsed_select_list vpsl
        where vpsl.sql_id = p_sql_id
        order by vpsl.pos
    )
    loop
        if (col_rec.col_type = 2)
        then
            dbms_sql.define_column(p_cursor, col_rec.pos, v_int_col);
            wm_val_log('Defined integer col at pos ' || col_rec.pos, p_sql_log_lvl => 2);
        elsif (col_rec.col_type in (1, 96))
        then
            dbms_sql.define_column(p_cursor, col_rec.pos, v_varchar2_col,
                v_max_char_len);
            wm_val_log('Defined varchar2 col at pos ' || col_rec.pos, p_sql_log_lvl => 2);
        else
            raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to finding unsupported data type '
                || col_rec.col_type);
        end if;
    end loop;

    select max(ver.row_num)
    into v_num_expected_rows
    from val_expected_results ver
    where ver.val_job_dtl_id = p_val_job_dtl_id;

    -- run it and fetch returned rows
    v_num_actual_rows := dbms_sql.execute(p_cursor);
    /*
    -- enable this for sql injection
    if (v_num_actual_rows != v_num_expected_rows)
    then
        update val_job_dtl vjd
        set vjd.mod_date_time = sysdate, vjd.last_run_passed = 0
        where vjd.val_job_dtl_id = p_val_job_dtl_id;
        dbms_output.put_line('Skipped processing Job dtl ' || p_val_job_dtl_id || ',sql ' || p_sql_id
            || ' expected rows ' || v_num_expected_rows || ', actual ' || v_num_actual_rows);
        
        -- skip validations of row level output
        continue;
    end if;
    */

    v_sql_passed := 0;
    v_curr_row_num := 0;
    while (dbms_sql.fetch_rows(p_cursor) > 0)
    loop
        -- for each selected column, based on datatype, get selected data
        -- into appropriate vars and compare
        v_curr_row_num := v_curr_row_num + 1;
        wm_val_log('Processing row ' || v_curr_row_num, p_sql_log_lvl => 2);
        if (v_curr_row_num > v_num_expected_rows)
        then
            raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to fetching more rows than expected (' 
                || v_num_expected_rows || ')');
        end if;

        for col_rec in
        (
            -- UI needs to ensure that all expected result columns are entered into the results table
            select vpsl.pos, vpsl.col_type, ver.expected_value
            from val_parsed_select_list vpsl
            join val_expected_results ver on ver.pos = vpsl.pos
                and ver.row_num = v_curr_row_num
                and ver.val_job_dtl_id = p_val_job_dtl_id
            where vpsl.sql_id = p_sql_id
            order by vpsl.pos
        )
        loop
            v_msg := null;
            v_varchar2_col := null;
            v_int_col := null;
            if (col_rec.col_type = 2)
            then
                dbms_sql.column_value(p_cursor, col_rec.pos, v_int_col);
                v_sql_passed := case when v_int_col = to_number(col_rec.expected_value)
                    then 1 else 0 end;
            elsif (col_rec.col_type in (1, 96))
            then
                dbms_sql.column_value(p_cursor, col_rec.pos, v_varchar2_col);
                v_sql_passed := case when v_varchar2_col = col_rec.expected_value
                    then 1 else 0 end;
            else
                raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to finding unsupported data type '
                || col_rec.col_type);
            end if;

            wm_val_log('Expecting ' || col_rec.expected_value || ' at ('
                || col_rec.pos || ',' || v_curr_row_num || '), found '
                || coalesce(v_varchar2_col, to_char(v_int_col), 'null'), 
                p_sql_log_lvl => 2);

            if (v_sql_passed = 0)
            then
                raise_application_error(-20050, 'Sql ' || p_sql_id
                    || ' failed due to finding ' 
                    || coalesce(v_varchar2_col, to_char(v_int_col), 'null')
                    || ' in (row ' || v_curr_row_num || ', Pos ' || col_rec.pos
                    || '), expected ' || col_rec.expected_value );
            end if;
        end loop; -- columns fetch

        if (v_sql_passed = 0)
        then
            raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to not finding matching result data');
        end if;
    end loop; -- rows fetch

    if (v_curr_row_num < v_num_expected_rows and v_msg is null)
    then
        raise_application_error(-20050, 'Sql ' || p_sql_id || 
            ' failed due to returning fewer rows (' || v_curr_row_num 
            || ') than expected (' || v_num_expected_rows || ')');
    end if;

    wm_val_add_job_result(p_val_job_dtl_id,
        case v_sql_passed when 0 then v_msg else null end, v_sql_passed);
end;
/

create or replace procedure wm_val_exec_job_intrnl
(
    p_user_id           in user_profile.user_id%type,
    p_job_name          in val_job_hdr.job_name%type,
    p_txn_id            in val_result_hist.txn_id%type,
    p_rt_bind_list_csv  in varchar2,
    p_cursor            in number
)
as
    udef_excp             exception;
    pragma exception_init (udef_excp, -20050);
begin
    if (p_rt_bind_list_csv is not null)
    then
        wm_val_get_runtime_bind_vars(p_user_id, p_job_name, p_rt_bind_list_csv,
            p_txn_id);
    end if;

    for sql_rec in
    (
        select vjd.val_job_dtl_id, vjd.sql_id, vs.bind_var_list,
            regexp_replace(vs.sql_text, '/\*\+[ ]*HINT[ ]*\*/',
                '/*+ ' || regexp_replace(vs.sql_hints, '[\/*]', '') || '*/', 1, 1, 'i') sql_text
        from val_job_hdr vjh
        join val_job_dtl vjd on vjd.val_job_hdr_id = vjh.val_job_hdr_id
            and vjd.is_locked = 0
        join val_sql vs on vs.sql_id = vjd.sql_id and vs.is_locked = 0
        where vjh.job_name = p_job_name and vjh.is_locked = 0
        order by vjd.val_job_dtl_id
    )
    loop
        begin
            wm_val_exec_sql(p_cursor, sql_rec.val_job_dtl_id, sql_rec.sql_id,
                sql_rec.sql_text, sql_rec.bind_var_list);
        exception
            when udef_excp then
                wm_val_add_job_result(sql_rec.val_job_dtl_id, p_msg => sqlerrm,
                    p_last_run_passed => 0);
        end;
    end loop;
end;
/

create or replace procedure wm_val_exec_job
(
    p_user_id           in  user_profile.user_id%type,
    p_app_hook          in  val_job_seq.app_hook%type,
    p_pgm_id            in  val_job_seq.pgm_id%type,
    p_rt_bind_list_csv  in  varchar2,
    p_log_lvl           number default 0,
    p_txn_id            in out val_result_hist.txn_id%type
)
as
    v_cursor            int;
begin
    for job_rec in
    (
        select rownum rn, vjs.job_name
        from val_job_seq vjs
        where vjs.app_hook = p_app_hook
            and vjs.pgm_id = coalesce(p_pgm_id, vjs.pgm_id)
        order by vjs.seq_nbr
    )
    loop
        if (job_rec.rn = 1)
        then
            v_cursor := dbms_sql.open_cursor;
            if (p_txn_id is null)
            then
                p_txn_id := val_txn_id_seq.nextval;
            end if;
            delete from tmp_val_parm t;
            insert into tmp_val_parm
            (
                user_id, txn_id, pgm_id, log_lvl, module, msg_id, ref_code_1,
                ref_value_2
            )
            values
            (
                p_user_id, p_txn_id, 'wm_val_exec_job', p_log_lvl, 'WAVE', 
                '1094', 'VAL', p_app_hook || '|' || p_pgm_id
            );
        end if;

        wm_val_log('Beginning validation of ' || job_rec.job_name
            || ', runtime binds are <' || substr(p_rt_bind_list_csv, 1, 400)
            || '>');
        wm_val_exec_job_intrnl(p_user_id, job_rec.job_name, p_txn_id, 
            p_rt_bind_list_csv, v_cursor);
        wm_val_log('Completed validation ' || job_rec.job_name);
    end loop;
    
    if (dbms_sql.is_open(v_cursor) = true)
    then
        dbms_sql.close_cursor(v_cursor);
    end if;
end;
/

-- DBTicket TE-2802

DELETE FROM message_display
      WHERE disp_key = '200000001';

DELETE FROM message_master
      WHERE key = '200000001';

MERGE INTO MESSAGE_MASTER A
     USING (SELECT '200000001' MSG_ID,
                   '200000001' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (message_master_id,
               msg_id,
               key,
               ils_module,
               msg_module,
               msg,
               bundle_name,
               msg_class,
               msg_type,
               ovride_role,
               log_flag,
               created_dttm,
               last_updated_dttm)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '200000001',
               '200000001',
               'te',
               'MSP',
               'One or more selected orders
{0} are already assigned to a shipment.',
               'ErrorMessage',
               'SYSTEM',
               'ERROR',
               NULL,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

UPDATE xmenu_item
   SET name = 'Manual Shipment Planning',
       short_name = 'Manual Shipment Planning'
 WHERE navigation_key = 'MLP.screen.MLPScreen';

MERGE INTO MESSAGE_MASTER A
     USING (SELECT '200000028' MSG_ID,
                   '200000028' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (message_master_id,
               msg_id,
               key,
               ils_module,
               msg_module,
               msg,
               bundle_name,
               msg_class,
               msg_type,
               ovride_role,
               log_flag,
               created_dttm,
               last_updated_dttm)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '200000028',
               '200000028',
               'te',
               'MSP',
               'oLPN {0}
part of Pallet with more than one order, cannot continue.',
               'ErrorMessage',
               'SYSTEM',
               'ERROR',
               NULL,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

MERGE INTO MESSAGE_MASTER A
     USING (SELECT '200000029' MSG_ID,
                   '200000029' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (message_master_id,
               msg_id,
               key,
               ils_module,
               msg_module,
               msg,
               bundle_name,
               msg_class,
               msg_type,
               ovride_role,
               log_flag,
               created_dttm,
               last_updated_dttm)
       VALUES (
                 SEQ_MESSAGE_MASTER_ID.NEXTVAL,
                 '200000029',
                 '200000029',
                 'te',
                 'MSP',
                 'oLPN
{0} part of Pallet with one or more shipments, cannot continue.',
                 'ErrorMessage',
                 'SYSTEM',
                 'ERROR',
                 NULL,
                 'N',
                 CURRENT_TIMESTAMP,
                 CURRENT_TIMESTAMP);

MERGE INTO MESSAGE_MASTER A
     USING (SELECT '200000030' MSG_ID,
                   '200000030' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (message_master_id,
               msg_id,
               key,
               ils_module,
               msg_module,
               msg,
               bundle_name,
               msg_class,
               msg_type,
               ovride_role,
               log_flag,
               created_dttm,
               last_updated_dttm)
       VALUES (
                 SEQ_MESSAGE_MASTER_ID.NEXTVAL,
                 '200000030',
                 '200000030',
                 'te',
                 'MSP',
                 'oLPN {0}
is part of Pallet, all oLPNs of pallet will be assigned to the Shipment.',
                 'ErrorMessage',
                 'USER',
                 'WARNING',
                 NULL,
                 'N',
                 CURRENT_TIMESTAMP,
                 CURRENT_TIMESTAMP);

MERGE INTO MESSAGE_MASTER A
     USING (SELECT '200000031' MSG_ID,
                   '200000031' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (message_master_id,
               msg_id,
               key,
               ils_module,
               msg_module,
               msg,
               bundle_name,
               msg_class,
               msg_type,
               ovride_role,
               log_flag,
               created_dttm,
               last_updated_dttm)
       VALUES (
                 SEQ_MESSAGE_MASTER_ID.NEXTVAL,
                 '200000031',
                 '200000031',
                 'te',
                 'MSP',
                 'oLPN
{0} is part of Pallet, all oLPNs of pallet will be un-assigned from the Shipment.',
                 'ErrorMessage',
                 'USER',
                 'WARNING',
                 NULL,
                 'N',
                 CURRENT_TIMESTAMP,
                 CURRENT_TIMESTAMP);

MERGE INTO MESSAGE_MASTER A
     USING (SELECT '200000032' MSG_ID,
                   '200000032' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (message_master_id,
               msg_id,
               key,
               ils_module,
               msg_module,
               msg,
               bundle_name,
               msg_class,
               msg_type,
               ovride_role,
               log_flag,
               created_dttm,
               last_updated_dttm)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '200000032',
               '200000032',
               'te',
               'MSP',
               'Orders {0}
oLPN(s) is on a pallet.',
               'ErrorMessage',
               'USER',
               'WARNING',
               NULL,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

COMMIT;

-- DBTicket TE-2835
MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'LoadCapacityInformation' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LoadCapacityInformation',
               'Shipment Capacity Information',
               'MLP');

MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'Trailer number' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Trailer number',
               'Trailer number',
               'MLP');

MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'Trailer Type' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Trailer Type',
               'Trailer Type',
               'MLP');

MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'Route' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Route',
               'Route',
               'MLP');

MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'Dock Door' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Dock Door',
               'Dock Door',
               'MLP');

MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'Appointment' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Appointment',
               'Appointment',
               'MLP');
			   
MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'Appointment Status' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Appointment Status',
               'Appointment Status',
               'MLP');
			   
MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'Scheduled Date/Time' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Scheduled Date/Time',
               'Scheduled Date/Time',
               'MLP');
			   
MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'Selected Order/ oLPN Capacity' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Selected Order/ oLPN Capacity',
               'Selected Order/ oLPN Capacity',
               'MLP');
			   
MERGE INTO LABEL L
     USING (SELECT 'CBOTrans' BUNDLE_NAME, 'LPNweight' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LPNweight',
               'LPN Weight',
               'CBOTrans');

MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'LPN Volume' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LPN Volume',
               'LPN Volume',
               'MLP');

MERGE INTO LABEL L
     USING (SELECT 'CBOTrans' BUNDLE_NAME, 'PlannedWeight' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'PlannedWeight',
               'Planned Weight',
               'CBOTrans');

MERGE INTO LABEL L
     USING (SELECT 'CBOTrans' BUNDLE_NAME, 'PlannedVolume' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'PlannedVolume',
               'Planned Volume',
               'CBOTrans');

MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'oLPNs' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'oLPNs',
               'oLPNs',
               'MLP');

MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'Pallets' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Pallets',
               'Pallets',
               'MLP');

MERGE INTO LABEL L
     USING (SELECT 'CBO' BUNDLE_NAME, 'Weight' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Weight',
               'Weight',
               'CBO');

MERGE INTO LABEL L
     USING (SELECT 'CBOTrans' BUNDLE_NAME, 'Volume' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Volume',
               'Volume',
               'CBOTrans');

MERGE INTO LABEL L
     USING (SELECT 'TEPlanningLanguages' BUNDLE_NAME, 'oLPNStatus' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'oLPNStatus',
               'oLPN Status',
               'TEPlanningLanguages');

MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'Assigned' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Assigned',
               'Assigned',
               'MLP');

MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'Loaded' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Loaded',
               'Loaded',
               'MLP');			
			   
MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'OrderInformation' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'OrderInformation',
               'Order Information',
               'MLP');		

MERGE INTO LABEL L
     USING (SELECT 'TE' BUNDLE_NAME, 'Type' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Type',
               'Type',
               'TE');	

MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'Order Date Time' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Order Date Time',
               'Order Date Time',
               'MLP');	

MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'Designated Ship Via' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Designated Ship Via',
               'Designated Ship Via',
               'MLP');	

MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'Available Order/ oLPN Capacity' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Available Order/ oLPN Capacity',
               'Available Order/ oLPN Capacity',
               'MLP');				   
               
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'LoadCapacityInformation');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Trailer number');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Trailer Type');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Route');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Dock Door');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Appointment');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Appointment Status');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Scheduled Date/Time');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Selected Order/ oLPN Capacity');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'CBOTrans', 'LPNweight');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'LPN Volume');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'CBOTrans', 'PlannedWeight');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'CBOTrans', 'PlannedVolume');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'oLPNs');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Pallets');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'CBO', 'Weight');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'CBOTrans', 'Volume');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'TEPlanningLanguages', 'oLPNStatus');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Assigned');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Loaded');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'OrderInformation');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'TE', 'Type');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Order Date Time');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Designated Ship Via');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Available Order/ oLPN Capacity');

commit;

-- DBTicket TE-2842
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
values (seq_label_id.nextVal,'lastUpdatedDttm','lastUpdatedDttm','manifest_label');

Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
values (seq_label_id.nextVal,'CurrencyCode','CurrencyCode','manifest_label');

Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
values (seq_label_id.nextVal,'CreatedSource','CreatedSource','manifest_label');

Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
values (seq_label_id.nextVal,'LastUpdatedSource','LastUpdatedSource','manifest_label');

Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
values (seq_label_id.nextVal,'EDIFileName','EDIFileName','manifest_label');

Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
values (seq_label_id.nextVal,'UpsPickupRecordNumber','UpsPickupRecordNumber','manifest_label');

Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
values (seq_label_id.nextVal,'PrintHazmatDocuments','Print Hazmat Documents','manifest_ExtMenuScreenLabels');

Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
values (seq_label_id.nextVal,'ViewPLD','ViewPLD','manifest_ExtMenuScreenLabels');

commit;

-- DBTicket TE-2840
MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'Order Size' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Order Size',
               'Order Size',
               'MLP');

MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'Shipment Size' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Shipment Size',
               'Shipment Size',
               'MLP');

INSERT INTO xscreen_label (XSCREEN_LABEL_ID,
                           XSCREEN_ID,
                           LABEL_BUNDLE_NAME,
                           LABEL_KEY)
     VALUES (sequence_xscreen_label_id.NEXTVAL,
             360001,
             'MLP',
             'Order Size');

INSERT INTO xscreen_label (XSCREEN_LABEL_ID,
                           XSCREEN_ID,
                           LABEL_BUNDLE_NAME,
                           LABEL_KEY)
     VALUES (sequence_xscreen_label_id.NEXTVAL,
             360001,
             'MLP',
             'Shipment Size');

COMMIT;


-- DBTicket TE-2843

INSERT INTO message_master( MESSAGE_MASTER_ID,
                  MSG_ID,
                  KEY,
                  ILS_MODULE,
                  MSG_MODULE,
                  MSG,
                  BUNDLE_NAME,
                  MSG_CLASS,
                  MSG_TYPE,
                  OVRIDE_ROLE,
                  LOG_FLAG,
                  CREATED_DTTM,
                  LAST_UPDATED_DTTM
                 )
         VALUES(SEQ_MESSAGE_MASTER_ID.nextval,
                  '200000033',
                  '200000033',
                  'te',
                  'MSP',
                  'The selected order {0} is now part of a parent order and will no longer be shown on the screen. The operation cannot be performed.',
                  'ErrorMessage',
                  'SYSTEM',
                  'ERROR',
                   NULL,
                  'N',
                   sysdate,
                   sysdate
                 );


INSERT INTO message_master( MESSAGE_MASTER_ID,
                         MSG_ID,
                         KEY,
                         ILS_MODULE,
                         MSG_MODULE,
                         MSG,
                         BUNDLE_NAME,
                         MSG_CLASS,
                         MSG_TYPE,
                         OVRIDE_ROLE,
                         LOG_FLAG,
                         CREATED_DTTM,
                         LAST_UPDATED_DTTM
                         )
                 VALUES(SEQ_MESSAGE_MASTER_ID.nextval,
                         '200000034',
                         '200000034',
                         'te',
                         'MSP',
                         'Orders {0} oLPN(s) is on a mixed order pallet.',
                         'ErrorMessage',
                         'USER',
                         'ERROR',
                          NULL,
                         'N',
                          sysdate,
                          sysdate
                          );
                          
COMMIT;						  

-- DBTicket TE-2847
MERGE INTO LABEL L
     USING (SELECT 'ParamDef' BUNDLE_NAME, 'assign_to_active_shipment' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES(seq_label_id.NEXTVAL,
             'assign_to_active_shipment',
             'Assign LPN to open shipment',
             'ParamDef');             

INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (seq_label_id.NEXTVAL,
             'active_Shipment_Rule_Override_Shipment',
             'Active Shipment Rule Override Shipment',
             'ParamDef');

INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (seq_label_id.NEXTVAL,
             'byPass_bulk_rating_for_LPNs_with_LPN_level_rates',
             'By Pass Bulk Rating For LPNs with LPN level Rates',
             'ParamDef');

INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (seq_label_id.NEXTVAL,
             'allow_multiple_routes_with_same_seq',
             'Allow multiple routes with same sequence',
             'ParamDef');

INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (seq_label_id.NEXTVAL,
             'Static Route',
             'Static Route',
             'ParamDef');

INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (seq_label_id.NEXTVAL,
             'validate_closing_manifests_with_future_dates',
             'Validate Closing Manifests With Future Dates',
             'ParamDef');

INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (seq_label_id.NEXTVAL,
             'enabled_LM_interface_for_weigh_and_manifest_oLPNs',
             'Enabled LM Interface for Weigh and Manifest oLPNs',
             'ParamDef');


INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (seq_label_id.NEXTVAL,
             'validate_billing_account_number_against_facility _address_key',
             'Enabled LM Interface for Weigh and Manifest oLPNs',
             'ParamDef');

INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (seq_label_id.NEXTVAL,
             'validate_DO_billing_information_for_parcel_carriers',
             'Validate DO Billing information for Parcel carriers',
             'ParamDef');

INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (seq_label_id.NEXTVAL,
             'validate_billing_method_on_the_DO_is_supported_by _carrier',
             'Validate billing method on the DO is supported by carrier',
             'ParamDef');


INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (seq_label_id.NEXTVAL,
             'fedEx_smart_post_trading_partner_id',
             'FedEx Smart post Trading Partner ID',
             'ParamDef');

INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (seq_label_id.NEXTVAL,
             'orders_dsg_ship_via_matches_shipment_asg_ship _via',
             'Orders Dsg Ship Via Matches Shipment Asg Ship Via',
             'ParamDef');

INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (seq_label_id.NEXTVAL,
             'use_store_procedure_for_variance_updates',
             'Use Store Procedure for variance updates',
             'ParamDef');

INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (seq_label_id.NEXTVAL,
             'use_store_procedure_for_assigning_OLPNs_to _shipment',
             'Use Store Procedure for assigning OLPNs To Shipment',
             'ParamDef');

INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (seq_label_id.NEXTVAL,
             'allow_retail_routing_at_LPN_level',
             'Allow Retail Routing at LPN Level',
             'ParamDef');

INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (seq_label_id.NEXTVAL,
             'LM_ELS_activity_code_for_weigh_and_manifest _oLPNs',
             'LM ELS Activity Code for Weigh and Manifest oLPNs',
             'ParamDef');

INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (seq_label_id.NEXTVAL,
             'display_warning_message_if_Ship_date_<_current _date',
             'Display warning message if Ship date < Current date',
             'ParamDef');

INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (
               seq_label_id.NEXTVAL,
               'display_warning_messge_if_actual_weight_fall _outside_tolerance',
               'Display warning messge if actual weight fall outside tolerance',
               'ParamDef');

INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (seq_label_id.NEXTVAL,
             'LTL/TL_manifest_type',
             'LTL/TL Manifest Type',
             'ParamDef');

INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (seq_label_id.NEXTVAL,
             'bill_of_lading_type',
             'Bill of Lading Type',
             'ParamDef');

INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (seq_label_id.NEXTVAL,
             'number_of_orders_per_batch_for_routing',
             'Number Of Orders Per Batch For Routing',
             'ParamDef');

INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (seq_label_id.NEXTVAL,
             'suppress_USPS_postage_statements',
             'Suppress USPS Postage Statements',
             'ParamDef');

INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (seq_label_id.NEXTVAL,
             'print_hazmat_docs_at_manifest',
             'Print Hazmat docs at manifest',
             'ParamDef');

INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (seq_label_id.NEXTVAL,
             'override_order_pickup_start_DTTM_for _manifest',
             'Override Order Pickup Start DTTM for Manifest',
             'ParamDef');

INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (
               seq_label_id.NEXTVAL,
               'check_order"s_designated_ship_via_and_shipment''s _assigned_ship_via_match',
               'Check Order"s Designated Ship Via and Shipment''s Assigned Ship Via match',
               'ParamDef');

INSERT INTO label (LABEL_ID,
                   KEY,
                   VALUE,
                   BUNDLE_NAME)
     VALUES (seq_label_id.NEXTVAL,
             'assign_LPN_to_shipment_based_on_stop''s _route',
             'Assign LPN to shipment based on stop''s route',
             'ParamDef');
             
commit;

-- DBTicket TE-2849
--update xscreen set description = 'Shipment Planning Workspace' where xscreen_id=360001;
update xmenu_item set name='Shipment Planning Workspace', short_name='Shipment Planning Workspace' where navigation_key='MLP.screen.MLPScreen';
commit;

-- DBTicket TE-2864
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'mlpPageSize','50','MLP');
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'mlpPageSize');
update xmenu_item set screen_mode=1 where navigation_key = 'MLP.screen.MLPScreen';
commit;

-- DBTicket TE-2870
Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
values (seq_label_id.nextVal,'Order Filter','Order Filter','MLP');

insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) 
values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Order Filter');

Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
values (seq_label_id.nextVal,'Shipment Filter','Shipment Filter','MLP');

insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) 
values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Shipment Filter');

MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'Shipment Filter' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Shipment Filter',
               'Shipment Filter',
               'MLP');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'Shipment Filter' LABEL_KEY,
                   '360001' XSCREEN_ID,
                   'MLP' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
	INSERT (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)  
	VALUES (sequence_xscreen_label_id.nextval, '360001', 'MLP', 'Shipment Filter');

Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
values (seq_label_id.nextVal,'deconsolidate shipment','deconsolidate shipment','MLP');

insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) 
values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'deconsolidate shipment');

Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
values (seq_label_id.nextVal,'Clears order filter and applies default filter','Clears order filter and applies default filter','MLP');

insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) 
values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Clears order filter and applies default filter');

Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
values (seq_label_id.nextVal,'Clears shipment filter and applies default filter','Clears shipment filter and applies default filter','MLP');

insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) 
values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Clears shipment filter and applies default filter');

Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
values (seq_label_id.nextVal,'Hide filter bar','Hide filter bar','MLP');

insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) 
values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Hide filter bar');

Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
values (seq_label_id.nextVal,'Show filter bar','Show filter bar','MLP');

insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) 
values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Show filter bar');

Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
values (seq_label_id.nextVal,'Is equal to','Is equal to','MLP');

insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) 
values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Is equal to');

Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
values (seq_label_id.nextVal,'Is not equal to','Is not equal to','MLP');

insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) 
values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Is not equal to');

Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
values (seq_label_id.nextVal,'Great than or equal','Great than or equal','MLP');

insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) 
values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Great than or equal');

Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
values (seq_label_id.nextVal,'Less than or equal','Less than or equal','MLP');

insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) 
values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Less than or equal');

Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
values (seq_label_id.nextVal,'Great than','Great than','MLP');

insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) 
values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Great than');

Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
values (seq_label_id.nextVal,'Less than','Less than','MLP');

insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) 
values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Less than');

Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
values (seq_label_id.nextVal,'Transaction Summary','Transaction Summary','MLP');

insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) 
values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Transaction Summary');

Insert into label (LABEL_ID,KEY,VALUE,BUNDLE_NAME) 
values (seq_label_id.nextVal,'Messages','Messages','MLP');

insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) 
values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Messages');

commit;

-- DBTicket TE-2874
DELETE FROM VCP_COMPANY_PARAMETER
      WHERE PARAM_DEF_ID IN
               ('active_shipment_rule_override_shipment',
                'lpn_level_retail_routing');

DELETE FROM PARAM_SPEC_VALUE
      WHERE PARAM_DEF_ID IN
               ('active_shipment_rule_override_shipment',
                'lpn_level_retail_routing');

DELETE FROM COMPANY_PARAMETER
      WHERE PARAM_DEF_ID IN
               ('active_shipment_rule_override_shipment',
                'lpn_level_retail_routing');

DELETE FROM PARAM_DEF
      WHERE PARAM_DEF_ID IN
               ('active_shipment_rule_override_shipment',
                'lpn_level_retail_routing');

COMMIT;

-- DBTicket TE-2893
insert into val_tag
(
    tag, ui_desc
)
select 'acttrk', 'Activity Tracking' from dual;


insert into val_tag_type_xref
(
    type, tag
)
select '2', 'acttrk' from dual;

commit;

create or replace procedure wm_val_exec_sql
(
    p_cursor          in number,
    p_val_job_dtl_id  in val_job_dtl.val_job_dtl_id%type,
    p_sql_id          in val_job_dtl.sql_id%type,
    p_sql_text        in val_sql.sql_text%type,
    p_bind_var_list   in val_sql.bind_var_list%type
)
as
    v_txn_id            val_result_hist.txn_id%type := wm_val_get_txn_id();
    v_sql_passed        number(1);
    v_num_actual_rows   number(9);
    v_num_expected_rows number(9);
    v_curr_row_num      number(9);
    v_int_col           number(9);
    v_varchar2_col      varchar2(50);
    v_max_char_len      number(3) := 50;
    v_msg               val_result_hist.msg%type;
begin
    wm_val_log('SQL ' || p_sql_id || ': ' || substr(p_sql_text, 1, 500));
    v_sql_passed := 0;
    dbms_sql.parse(p_cursor, p_sql_text, dbms_sql.native);

    -- run time bind values can override pre-configured values
    for bind_rec in
    (
        select vsb.bind_var, coalesce(vrb.bind_val, vsb.bind_val) bind_val,
            coalesce(vrb.bind_val_int, vsb.bind_val_int) bind_val_int
        from val_sql_binds vsb
        left join val_runtime_binds vrb on vrb.bind_var = vsb.bind_var
            and vrb.val_job_dtl_id = vsb.val_job_dtl_id
            and vrb.txn_id = v_txn_id
        where vsb.val_job_dtl_id = p_val_job_dtl_id
    )
    loop
        if (instr(p_bind_var_list, bind_rec.bind_var, 1, 1) = 0)
        then
            raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to configured bind var :' || bind_rec.bind_var 
                || ' not found in parsed bind list');
        end if;

        if (substr(bind_rec.bind_var, 1, 2) = 'i_')
        then
            dbms_sql.bind_variable(p_cursor, bind_rec.bind_var, bind_rec.bind_val_int);
            wm_val_log('Bound <' || bind_rec.bind_val_int || '> to :' || bind_rec.bind_var,
                p_sql_log_lvl => 2);
        else
            dbms_sql.bind_variable(p_cursor, bind_rec.bind_var, bind_rec.bind_val);
            wm_val_log('Bound <' || bind_rec.bind_val || '> to :' || bind_rec.bind_var,
                p_sql_log_lvl => 2);
        end if;
    end loop;

    -- define datatype for each selected column in the sql
    for col_rec in
    (
        select vpsl.pos, vpsl.col_type
        from val_parsed_select_list vpsl
        where vpsl.sql_id = p_sql_id
        order by vpsl.pos
    )
    loop
        if (col_rec.col_type = 2)
        then
            dbms_sql.define_column(p_cursor, col_rec.pos, v_int_col);
            wm_val_log('Defined integer col at pos ' || col_rec.pos, p_sql_log_lvl => 2);
        elsif (col_rec.col_type in (1, 96))
        then
            dbms_sql.define_column(p_cursor, col_rec.pos, v_varchar2_col,
                v_max_char_len);
            wm_val_log('Defined varchar2 col at pos ' || col_rec.pos, p_sql_log_lvl => 2);
        else
            raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to finding unsupported data type '
                || col_rec.col_type);
        end if;
    end loop;

    select max(ver.row_num)
    into v_num_expected_rows
    from val_expected_results ver
    where ver.val_job_dtl_id = p_val_job_dtl_id;

    -- run it and fetch returned rows
    v_num_actual_rows := dbms_sql.execute(p_cursor);
    /*
    -- enable this for sql injection
    if (v_num_actual_rows != v_num_expected_rows)
    then
        update val_job_dtl vjd
        set vjd.mod_date_time = sysdate, vjd.last_run_passed = 0
        where vjd.val_job_dtl_id = p_val_job_dtl_id;
        dbms_output.put_line('Skipped processing Job dtl ' || p_val_job_dtl_id || ',sql ' || p_sql_id
            || ' expected rows ' || v_num_expected_rows || ', actual ' || v_num_actual_rows);
        
        -- skip validations of row level output
        continue;
    end if;
    */

    v_sql_passed := 0;
    v_curr_row_num := 0;
    while (dbms_sql.fetch_rows(p_cursor) > 0)
    loop
        -- for each selected column, based on datatype, get selected data
        -- into appropriate vars and compare
        v_curr_row_num := v_curr_row_num + 1;
        wm_val_log('Processing row ' || v_curr_row_num, p_sql_log_lvl => 2);
        if (v_curr_row_num > v_num_expected_rows)
        then
            raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to fetching more rows than expected (' 
                || v_num_expected_rows || ')');
        end if;

        for col_rec in
        (
            -- UI needs to ensure that all expected result columns are entered into the results table
            select vpsl.pos, vpsl.col_type, ver.expected_value
            from val_parsed_select_list vpsl
            join val_expected_results ver on ver.pos = vpsl.pos
                and ver.row_num = v_curr_row_num
                and ver.val_job_dtl_id = p_val_job_dtl_id
            where vpsl.sql_id = p_sql_id
            order by vpsl.pos
        )
        loop
            v_msg := null;
            v_varchar2_col := null;
            v_int_col := null;
            if (col_rec.col_type = 2)
            then
                dbms_sql.column_value(p_cursor, col_rec.pos, v_int_col);
                v_sql_passed := case when v_int_col = to_number(col_rec.expected_value)
                    then 1 else 0 end;
            elsif (col_rec.col_type in (1, 96))
            then
                dbms_sql.column_value(p_cursor, col_rec.pos, v_varchar2_col);
                v_sql_passed := case when v_varchar2_col = col_rec.expected_value
                    then 1 else 0 end;
            else
                raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to finding unsupported data type '
                || col_rec.col_type);
            end if;

            wm_val_log('Expecting ' || col_rec.expected_value || ' at ('
                || col_rec.pos || ',' || v_curr_row_num || '), found '
                || coalesce(v_varchar2_col, to_char(v_int_col), 'null'), 
                p_sql_log_lvl => 2);

            if (v_sql_passed = 0)
            then
                raise_application_error(-20050, 'Sql ' || p_sql_id
                    || ' failed due to finding ' 
                    || coalesce(v_varchar2_col, to_char(v_int_col), 'null')
                    || ' in (row ' || v_curr_row_num || ', Pos ' || col_rec.pos
                    || '), expected ' || col_rec.expected_value );
            end if;
        end loop; -- columns fetch

        if (v_sql_passed = 0)
        then
            raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to not finding matching result data');
        end if;
    end loop; -- rows fetch

    if (v_curr_row_num < v_num_expected_rows and v_msg is null)
    then
        raise_application_error(-20050, 'Sql ' || p_sql_id || 
            ' failed due to returning fewer rows (' || v_curr_row_num 
            || ') than expected (' || v_num_expected_rows || ')');
    end if;

    wm_val_add_job_result(p_val_job_dtl_id,
        case v_sql_passed when 0 then v_msg else 'Ok' end, v_sql_passed);
end;
/
show errors;

-- DBTicket TE-2899
--removing invalid object from te as they are moved to WM
call quiet_drop('procedure','wm_process_variance_for_cs');

-- DBTicket TE-2900
MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'Not Available' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Not Available',
               'Not Available',
               'MLP');
               
insert into xscreen_label(XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) 
values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'Not Available');

commit;

-- DBTicket TE-2736
-- Above are DB2 specific changes

-- DBTicket TE-2867
-- Above are DB2 specific changes

-- DBTicket TE-2912
update label
   set value =
          '                           The services to be performed by UPS-MI hereunder are governed by the terms and conditions contained on this Shipment Control Form and the Service Terms and Conditions, which are incorporated herein by this reference. The Service Terms and Conditions are published on the Internet at '
 where key = 'TheServicesToBe1';

update label
   set value =
          '                                       Client shall be responsible for fulfilling, preparing, labeling, addressing and properly describing the contents of this shipment so as to ensure proper transportation with ordinary care in handling.'
 where key = 'ClientShallBeResponsible';

update label
   set value =
          '             Shipments are subject to charges for actual or dimensional weight in accordance with the Agreement.UPS -MI reserves the right to institute a fuel surcharge on some or all shipments without prior notice. The fuel surcharge will be applied to Services and for such periods as UPS-MI; in its sole discretion, may determine necessary. The current fuel surcharge is described at'
 where key = 'ShipmentsAreSubjectTo1';

update label
   set value =
          '                                       UPS-MI''s entire liability to Client for any and all claims, causes of action, damages, losses, costs, expenses or other liabilities of any nature whatsoever incurred by Client in connection with the Agreement, including, without limitation, attorney''s fees, any liabilities resulting from any omissions, errors, delays or interruptions in the Services and the preparation or delivery of any '
 where key = 'UPSMIEntireLiability1';

update label
   set value =
          ' Pieces being provided by UPS-MI hereunder ("Liabilities"), shall, in each instance and without regard to the value of the contents of any Piece(s), be limited to the actual fees and expenses charged by UPS -MI (excluding postage fees charged) to Client for the specific Service performed that gave rise to the Liability. UPS-MI shall not be liable for failure to deliver any Pieces if such failure is due to '
 where key = 'UPSMIEntireLiability2';

update label
   set value =
          'Client''s error, omission or negligence. Upon delivery to the USPS, the USPS becomes the party responsible for Client''s Pieces. Upon delivery to the USPS, UPS-MI shall have no liability of any nature whatsoever arising from acts or omissions of the USPS, including, without limitation, liability for any loss, miscarriage, negligent transmission, damage, delay, or failure of delivery of any Pieces or other matter. '
 where key = 'UPSMIEntireLiability3';

update label
   set value =
          'NOTWITHSTANDING ANYTHING IN THE AGREEMENT TO THE CONTRARY, IN NO EVENT SHALL UPS -MI BE LIABLE TO CLIENT, CLIENT''S CUSTOMERS OR ANY OTHER THIRD PARTY FOR LOSS OF USE, REVENUE OR PROFITS, BUSINESS INTERRUPTION, OR FOR ANY SPECIAL, CONSEQUENTIAL, EXEMPLARY, PUNITIVE, INDIRECT OR INCIDENTAL DAMAGES, WEHTER ARISING IN CONTRACT, TORT OR OTHERWISE, EVEN IF UP S-MI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.'
 where key = 'UPSMIEntireLiability4';

update label
   set value =
          '                       Client will, at Client''s sole cost and expense, indemnify and hold UPS -MI, its affiliates and their respective officers, directors, employees, agents, successors, and assigns harmless from and against any and all losses, damages, judgments, costs and expenses, including attorney''s fees, arising out of or related to any third party claim based on personal injury, damage to tangible property or otherwise arising from the Services per formed, '
 where key = 'ClientWill1';

commit;

-- DBTicket TE-2935
update resources set uri ='/te/optimizationmgmt/consolidation/jsp/ConsolidationRunTemplateDetailsPresentation.jsp' 
where uri = '/te/optimizationmgmt/consolidation/parameter/jsp/ConsolidationRunTemplateDetailsPresentation.jsp';
commit;

-- DBTicket TE-2936
update label set value='Unassigned Distribution Order List' where key='unassignedOrderTitle' and bundle_name='MLP';
update label set value='LPN' where key='lpnId' and bundle_name='MLP';
update label set value='Distribution Order' where key='OrderId' and bundle_name='MLP';
update label set value='LPN Status' where key='lpnStatus' and bundle_name='MLP';
update label set value='LPN Facility Status' where key='lpnFacilityStatus' and bundle_name='MLP';
update label set value='Pallet' where key='lpnPalletId' and bundle_name='MLP';
update label set value='Shipment' where key='shipmentId' and bundle_name='MLP';
update label set value='Assigned Ship Via' where key='assignedShipVia' and bundle_name='MLP';
update label set value='Distribution Orders' where key='shipmentTabOrderTitle' and bundle_name='MLP';
update label set value='oLPNs' where key='shipmentTabLpnTitle' and bundle_name='MLP';

Insert into label(LABEL_ID,KEY,VALUE,BUNDLE_NAME) values (seq_label_id.nextVal,'unassignedLpnTitle','Unassigned oLPN List','MLP');
insert into xscreen_label(XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'unassignedLpnTitle');

insert into label(label_id,key,value,bundle_name) values (seq_label_id.nextval,'route','Route','MLP');
insert into xscreen_label(XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'route');

update label set value='Distribution Order View' where key='Order Filter' and bundle_name='MLP';
update label set value='Shipment View' where key='Shipment Filter' and bundle_name='MLP';

insert into xscreen_label(XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)  values (sequence_xscreen_label_id.nextval, 360001, 'mps', 'pagingDisplayMsg');
insert into xscreen_label(XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)  values (sequence_xscreen_label_id.nextval, 360001, 'mps', 'noDataDisplayMsg');

delete from xscreen_label where label_bundle_name='MLP' and label_key in ('lpnPagingDisplay', 'lpnPagingEmptyMsg', 'orderPagingDisplay', 'orderPagingEmptyMsg', 'shipViaPagingDisplay', 'shipViaPagingEmptyMsg', 'shipmentpagingdisplay', 'shipmentPagingEmptyMsg');
delete from label where bundle_name='MLP' and key in ('lpnPagingDisplay', 'lpnPagingEmptyMsg', 'orderPagingDisplay', 'orderPagingEmptyMsg', 'shipViaPagingDisplay', 'shipViaPagingEmptyMsg', 'shipmentpagingdisplay', 'shipmentPagingEmptyMsg');

insert into label(label_id,key,value,bundle_name) values (seq_label_id.nextval,'selectOrderView','Select a view from Distribution Orders view to retrieve data','MLP');
insert into label(label_id,key,value,bundle_name) values (seq_label_id.nextval,'selectShipmentView','Select a view from the Shipment view to retrieve data','MLP');

insert into xscreen_label(XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'selectOrderView');
insert into xscreen_label(XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY) values (sequence_xscreen_label_id.nextval, 360001, 'MLP', 'selectShipmentView');

commit;

-- DBTicket TE-2941
create or replace trigger wm_val_derive_select_info
after insert or update on val_sql
referencing new as new
for each row
declare
    v_cursor        int := dbms_sql.open_cursor;
    v_col_list      dbms_sql.desc_tab2;
    v_col_count     int;
-- todo: need to normalize chars and bytes
    v_max_col_len   number(3) := 200;
    v_current_date  date := sysdate;
begin
    dbms_sql.parse(v_cursor, :new.sql_text, dbms_sql.native);
    dbms_sql.describe_columns2(v_cursor, v_col_count, v_col_list);

    delete from val_parsed_select_list vpsl
    where vpsl.sql_id = :new.sql_id;

    for i in 1..v_col_count
    loop
        if (v_col_list(i).col_type not in (1, 2, 96))
        then
            raise_application_error(-20050, 'Supported data types of selected '
                || 'columns are currently varchar2/char/number (found ' 
                || v_col_list(i).col_type || ').');
        end if;
        
        if (v_col_list(i).col_max_len > v_max_col_len)
        then
            raise_application_error(-20050, 'Selected column width/precision of '
                || v_col_list(i).col_max_len || ' exceeds supported maximum.');
        end if;

        insert into val_parsed_select_list
        (
            sql_id, pos, col_type
        )
        values
        (
            :new.sql_id, i, v_col_list(i).col_type
        );
    end loop;
    
    dbms_sql.close_cursor(v_cursor);
end;
/
show errors;

-- DBTicket WM-41950
update xmenu_item set name = 'Parcel Transit Time', short_name = 'Parcel Transit Time' where navigation_key = '/te/parcelRate/ui/ParcelTransitTime.xhtml';
update xmenu_item set name = 'Parcel Zone Rates', short_name = 'Parcel Zone Rates' where navigation_key = '/te/parcelRate/ui/ParcelZoneRate.xhtml';

merge into label l
     using (select 'TEPageTitle' bundle_name, 'ParcelZoneRate' key from dual) b
        on (l.key = b.key and l.bundle_name = b.bundle_name)
when not matched
then
   insert     (label_id,
               key,
               value,
               bundle_name)
       values (seq_label_id.nextval,
               'ParcelZoneRate',
               'Parcel Zone Rates',
               'TEPageTitle');

merge into label l
     using (select 'TEPageTitle' bundle_name, 'ParcelTransitTime' key
              from dual) b
        on (l.key = b.key and l.bundle_name = b.bundle_name)
when not matched
then
   insert     (label_id,
               key,
               value,
               bundle_name)
       values (seq_label_id.nextval,
               'ParcelTransitTime',
               'Parcel Transit Time',
               'TEPageTitle');

merge into label l
     using (select 'manifest_ExtMenuScreenId' bundle_name,
                   'VIEW_PARCEL_ZONE_RATE' key
              from dual) b
        on (l.key = b.key and l.bundle_name = b.bundle_name)
when not matched
then
   insert     (label_id,
               key,
               value,
               bundle_name)
       values (seq_label_id.nextval,
               'VIEW_PARCEL_ZONE_RATE',
               '40000034',
               'manifest_ExtMenuScreenId');

merge into label l
     using (select 'manifest_ExtMenuScreenId' bundle_name,
                   'VIEW_PARCEL_TRANSIT_TIME' key
              from dual) b
        on (l.key = b.key and l.bundle_name = b.bundle_name)
when not matched
then
   insert     (label_id,
               key,
               value,
               bundle_name)
       values (seq_label_id.nextval,
               'VIEW_PARCEL_TRANSIT_TIME',
               '40000035',
               'manifest_ExtMenuScreenId');
commit;

-- DBTicket TE-2995
alter table manifest_hdr add constraint mnfst_typ_fk foreign key (manifest_type_id) references manifest_type (manifest_type_id);

-- DBTicket TE-3003
DELETE FROM ui_menu_item_permission
      WHERE ui_menu_item_id IN
               (SELECT ui_menu_item_id
                  FROM ui_menu_item
                 WHERE ui_menu_screen_id IN (SELECT ui_menu_screen_id
                                               FROM ui_menu_screen
                                              WHERE screen_id = '40000008')
                       AND Display_text IN ('Copy', 'Delete'));

DELETE FROM ui_menu_item
      WHERE ui_menu_item_id IN
               (SELECT ui_menu_item_id
                  FROM ui_menu_item
                 WHERE ui_menu_screen_id IN (SELECT ui_menu_screen_id
                                               FROM ui_menu_screen
                                              WHERE screen_id = '40000008')
                       AND Display_text IN ('Copy', 'Delete'));

commit;

-- DBTicket TE-2968
MERGE INTO RULE_COLM_LIST A
     USING (SELECT 'LPN' TBL_NAME,
                   'REF_FIELD_1' COLM_NAME,
                   'LPN Ref Field1' COLM_DESC
              FROM DUAL) B
        ON (    A.COLM_NAME = B.COLM_NAME
            AND A.TBL_NAME = B.TBL_NAME
            AND A.COLM_DESC = B.COLM_DESC)
WHEN NOT MATCHED
THEN
   INSERT     (RULE_COLM_LIST_ID,
               TBL_NAME,
               COLM_NAME,
               COLM_DESC,
               DISBL_FLAG,
               create_date_time,
               mod_date_time,
               USER_ID,
               WM_VERSION_ID,
               RULE_COLM_ID)
       VALUES (RULE_COLM_LIST_ID_SEQ.NEXTVAL,
               'LPN',
               'REF_FIELD_1',
               'LPN Ref Field1',
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'WMADMIN',
               1,
               RULE_COLM_LIST_ID_SEQ.NEXTVAL);

MERGE INTO RULE_COLM_LIST A
     USING (SELECT 'LPN' TBL_NAME,
                   'REF_FIELD_2' COLM_NAME,
                   'LPN Ref Field2' COLM_DESC
              FROM DUAL) B
        ON (    A.COLM_NAME = B.COLM_NAME
            AND A.TBL_NAME = B.TBL_NAME
            AND A.COLM_DESC = B.COLM_DESC)
WHEN NOT MATCHED
THEN
   INSERT     (RULE_COLM_LIST_ID,
               TBL_NAME,
               COLM_NAME,
               COLM_DESC,
               DISBL_FLAG,
               create_date_time,
               mod_date_time,
               USER_ID,
               WM_VERSION_ID,
               rule_colm_id)
       VALUES (RULE_COLM_LIST_ID_SEQ.NEXTVAL,
               'LPN',
               'REF_FIELD_2',
               'LPN Ref Field2',
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'WMADMIN',
               1,
               RULE_COLM_LIST_ID_SEQ.NEXTVAL);

MERGE INTO RULE_COLM_LIST A
     USING (SELECT 'LPN' TBL_NAME,
                   'REF_FIELD_3' COLM_NAME,
                   'LPN Ref Field3' COLM_DESC
              FROM DUAL) B
        ON (    A.COLM_NAME = B.COLM_NAME
            AND A.TBL_NAME = B.TBL_NAME
            AND A.COLM_DESC = B.COLM_DESC)
WHEN NOT MATCHED
THEN
   INSERT     (RULE_COLM_LIST_ID,
               TBL_NAME,
               COLM_NAME,
               COLM_DESC,
               DISBL_FLAG,
               create_date_time,
               mod_date_time,
               USER_ID,
               WM_VERSION_ID,
               rule_colm_id)
       VALUES (RULE_COLM_LIST_ID_SEQ.NEXTVAL,
               'LPN',
               'REF_FIELD_3',
               'LPN Ref Field3',
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'WMADMIN',
               1,
               RULE_COLM_LIST_ID_SEQ.NEXTVAL);

MERGE INTO RULE_COLM_LIST A
     USING (SELECT 'LPN' TBL_NAME,
                   'REF_FIELD_4' COLM_NAME,
                   'LPN Ref Field4' COLM_DESC
              FROM DUAL) B
        ON (    A.COLM_NAME = B.COLM_NAME
            AND A.TBL_NAME = B.TBL_NAME
            AND A.COLM_DESC = B.COLM_DESC)
WHEN NOT MATCHED
THEN
   INSERT     (RULE_COLM_LIST_ID,
               TBL_NAME,
               COLM_NAME,
               COLM_DESC,
               DISBL_FLAG,
               create_date_time,
               mod_date_time,
               USER_ID,
               WM_VERSION_ID,
               rule_colm_id)
       VALUES (RULE_COLM_LIST_ID_SEQ.NEXTVAL,
               'LPN',
               'REF_FIELD_4',
               'LPN Ref Field4',
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'WMADMIN',
               1,
               RULE_COLM_LIST_ID_SEQ.NEXTVAL);

MERGE INTO RULE_COLM_LIST A
     USING (SELECT 'LPN' TBL_NAME,
                   'REF_FIELD_5' COLM_NAME,
                   'LPN Ref Field5' COLM_DESC
              FROM DUAL) B
        ON (    A.COLM_NAME = B.COLM_NAME
            AND A.TBL_NAME = B.TBL_NAME
            AND A.COLM_DESC = B.COLM_DESC)
WHEN NOT MATCHED
THEN
   INSERT     (RULE_COLM_LIST_ID,
               TBL_NAME,
               COLM_NAME,
               COLM_DESC,
               DISBL_FLAG,
               create_date_time,
               mod_date_time,
               USER_ID,
               WM_VERSION_ID,
               rule_colm_id)
       VALUES (RULE_COLM_LIST_ID_SEQ.NEXTVAL,
               'LPN',
               'REF_FIELD_5',
               'LPN Ref Field5',
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'WMADMIN',
               1,
               RULE_COLM_LIST_ID_SEQ.NEXTVAL);

MERGE INTO RULE_COLM_LIST A
     USING (SELECT 'LPN' TBL_NAME,
                   'REF_FIELD_6' COLM_NAME,
                   'LPN Ref Field6' COLM_DESC
              FROM DUAL) B
        ON (    A.COLM_NAME = B.COLM_NAME
            AND A.TBL_NAME = B.TBL_NAME
            AND A.COLM_DESC = B.COLM_DESC)
WHEN NOT MATCHED
THEN
   INSERT     (RULE_COLM_LIST_ID,
               TBL_NAME,
               COLM_NAME,
               COLM_DESC,
               DISBL_FLAG,
               create_date_time,
               mod_date_time,
               USER_ID,
               WM_VERSION_ID,
               rule_colm_id)
       VALUES (RULE_COLM_LIST_ID_SEQ.NEXTVAL,
               'LPN',
               'REF_FIELD_6',
               'LPN Ref Field6',
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'WMADMIN',
               1,
               RULE_COLM_LIST_ID_SEQ.NEXTVAL);

MERGE INTO RULE_COLM_LIST A
     USING (SELECT 'LPN' TBL_NAME,
                   'REF_FIELD_7' COLM_NAME,
                   'LPN Ref Field7' COLM_DESC
              FROM DUAL) B
        ON (    A.COLM_NAME = B.COLM_NAME
            AND A.TBL_NAME = B.TBL_NAME
            AND A.COLM_DESC = B.COLM_DESC)
WHEN NOT MATCHED
THEN
   INSERT     (RULE_COLM_LIST_ID,
               TBL_NAME,
               COLM_NAME,
               COLM_DESC,
               DISBL_FLAG,
               create_date_time,
               mod_date_time,
               USER_ID,
               WM_VERSION_ID,
               rule_colm_id)
       VALUES (RULE_COLM_LIST_ID_SEQ.NEXTVAL,
               'LPN',
               'REF_FIELD_7',
               'LPN Ref Field7',
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'WMADMIN',
               1,
               RULE_COLM_LIST_ID_SEQ.NEXTVAL);

MERGE INTO RULE_COLM_LIST A
     USING (SELECT 'LPN' TBL_NAME,
                   'REF_FIELD_8' COLM_NAME,
                   'LPN Ref Field8' COLM_DESC
              FROM DUAL) B
        ON (    A.COLM_NAME = B.COLM_NAME
            AND A.TBL_NAME = B.TBL_NAME
            AND A.COLM_DESC = B.COLM_DESC)
WHEN NOT MATCHED
THEN
   INSERT     (RULE_COLM_LIST_ID,
               TBL_NAME,
               COLM_NAME,
               COLM_DESC,
               DISBL_FLAG,
               create_date_time,
               mod_date_time,
               USER_ID,
               WM_VERSION_ID,
               rule_colm_id)
       VALUES (RULE_COLM_LIST_ID_SEQ.NEXTVAL,
               'LPN',
               'REF_FIELD_8',
               'LPN Ref Field8',
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'WMADMIN',
               1,
               RULE_COLM_LIST_ID_SEQ.NEXTVAL);

MERGE INTO RULE_COLM_LIST A
     USING (SELECT 'LPN' TBL_NAME,
                   'REF_FIELD_9' COLM_NAME,
                   'LPN Ref Field9' COLM_DESC
              FROM DUAL) B
        ON (    A.COLM_NAME = B.COLM_NAME
            AND A.TBL_NAME = B.TBL_NAME
            AND A.COLM_DESC = B.COLM_DESC)
WHEN NOT MATCHED
THEN
   INSERT     (RULE_COLM_LIST_ID,
               TBL_NAME,
               COLM_NAME,
               COLM_DESC,
               DISBL_FLAG,
               create_date_time,
               mod_date_time,
               USER_ID,
               WM_VERSION_ID,
               rule_colm_id)
       VALUES (RULE_COLM_LIST_ID_SEQ.NEXTVAL,
               'LPN',
               'REF_FIELD_9',
               'LPN Ref Field9',
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'WMADMIN',
               1,
               RULE_COLM_LIST_ID_SEQ.NEXTVAL);

MERGE INTO RULE_COLM_LIST A
     USING (SELECT 'LPN' TBL_NAME,
                   'REF_FIELD_10' COLM_NAME,
                   'LPN Ref Field10' COLM_DESC
              FROM DUAL) B
        ON (    A.COLM_NAME = B.COLM_NAME
            AND A.TBL_NAME = B.TBL_NAME
            AND A.COLM_DESC = B.COLM_DESC)
WHEN NOT MATCHED
THEN
   INSERT     (RULE_COLM_LIST_ID,
               TBL_NAME,
               COLM_NAME,
               COLM_DESC,
               DISBL_FLAG,
               create_date_time,
               mod_date_time,
               USER_ID,
               WM_VERSION_ID,
               rule_colm_id)
       VALUES (RULE_COLM_LIST_ID_SEQ.NEXTVAL,
               'LPN',
               'REF_FIELD_10',
               'LPN Ref Field10',
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'WMADMIN',
               1,
               RULE_COLM_LIST_ID_SEQ.NEXTVAL);

COMMIT;

-- DBTicket TE-2918
update label set value ='Airway Bill*' where key='Air_Waybill';
UPDATE LABEL SET VALUE ='Airway Bill' WHERE KEY='AirWayBill' AND BUNDLE_NAME='Languages'; 
UPDATE LABEL SET VALUE ='Airway Bill' WHERE KEY='AirWayBill' AND BUNDLE_NAME='TEPlanningLanguages';
UPDATE LABEL SET VALUE ='Airway Bill Nbr:' WHERE KEY='AIRWAYBILLNBR'; 
Commit;

-- DBTicket TE-3075
DELETE FROM PARAM_SPEC_VALUE
      WHERE param_def_id IN
               ('display_designated_ship_via_in_mlp',
                'cancel_mlp_if_inactive_after_x_minutes');

DELETE FROM VCP_COMPANY_PARAMETER
      WHERE param_def_id IN
               ('display_designated_ship_via_in_mlp',
                'cancel_mlp_if_inactive_after_x_minutes');

DELETE FROM company_parameter
      WHERE param_def_id IN
               ('display_designated_ship_via_in_mlp',
                'cancel_mlp_if_inactive_after_x_minutes');

DELETE FROM param_def
      WHERE param_def_id IN
               ('display_designated_ship_via_in_mlp',
                'cancel_mlp_if_inactive_after_x_minutes');

COMMIT;

-- DBTicket TE-3052
MERGE INTO PARAM_DEF P
     USING (SELECT 'VALIDATE_GUARANTEED_DELIVERY' PARAM_DEF_ID,
                   'COM_TEPE' PARAM_GROUP_ID
              FROM DUAL) D
        ON (P.PARAM_DEF_ID = D.PARAM_DEF_ID
            AND P.PARAM_GROUP_ID = D.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
                       PARAM_GROUP_ID,
                       PARAM_SUBGROUP_ID,
                       PARAM_NAME,
                       DESCRIPTION,
                       IS_DISABLED,
                       IS_REQUIRED,
                       DATA_TYPE,
                       UI_TYPE,
                       MIN_VALUE,
                       IS_MIN_INC,
                       MAX_VALUE,
                       IS_MAX_INC,
                       DFLT_VALUE,
                       DISPLAY_ORDER,
                       VALUE_GENERATOR_CLASS,
                       IS_LOCALIZABLE,
                       PARAM_CATEGORY,
                       CARR_DFLT_VALUE,
                       BP_DFLT_VALUE,
                       PARAM_VALIDATOR_CLASS,
                       CREATED_DTTM,
                       LAST_UPDATED_DTTM)
     VALUES (
               'VALIDATE_GUARANTEED_DELIVERY',
               'COM_TEPE',
               NULL,
               'Validate if oLPNs classified for guaranteed delivery meet the promised delivery date',
               'Validate if oLPNs classified for guaranteed delivery meet the promised delivery date',
               0,
               0,
               10,
               'DROPDOWN',
               NULL,
               0,
               NULL,
               0,
               NULL,
               16,
               'com.manh.te.planning.finitevalue.TimeFeasibilityCheckConfigureFV',
               1,
               'Planning',
               NULL,
               NULL,
               NULL,
               current_timestamp,
               current_timestamp);              
               
MERGE INTO LABEL L
     USING (SELECT 'SysCode' BUNDLE_NAME,
                   'Validate if oLPNs classified for guaranteed delivery meet the promised delivery date' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Validate if oLPNs classified for guaranteed delivery meet the promised delivery date',
               'Validate if oLPNs classified for guaranteed delivery meet the promised delivery date',
               'SysCode');               

MERGE INTO LABEL L
     USING (SELECT 'SysCode' BUNDLE_NAME,
                   '9100- oLPNs for Promised Delivery' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               '9100- oLPNs for Promised Delivery',
               '9100- oLPNs for Promised Delivery',
               'SysCode');
            
MERGE INTO MESSAGE_MASTER A
     USING (SELECT '1109016516' MSG_ID,
                   '1109016516' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
                            BUNDLE_NAME,
                            KEY,
                            MSG,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG_ID,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG)
     VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
             'ErrorMessage',
             '1109016516',
             'oLPN {0} cannot meet promised delivery. Needs upgrade',
             'te',
             NULL,
             '1109016516',
             'USER',
             'WARNING',
             NULL,
             'Y');            
               
MERGE INTO MESSAGE_MASTER A
     USING (SELECT '1109016517' MSG_ID,
                   '1109016517' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
                            BUNDLE_NAME,
                            KEY,
                            MSG,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG_ID,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               'ErrorMessage',
               '1109016517',
               'Pallet {0} has oLPNs that are mixed with guaranteed delivery oLPNs',
               'te',
               NULL,
               '1109016517',
               'SYSTEM',
               'ERROR',
               NULL,
               'Y');

MERGE INTO MESSAGE_MASTER A
     USING (SELECT '1109016518' MSG_ID,
                   '1109016518' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
                            BUNDLE_NAME,
                            KEY,
                            MSG,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG_ID,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               'ErrorMessage',
               '1109016518',
               'Pallet {0} has oLPN(s) that cannot meet promised delivery date. Needs upgrade',
               'te',
               NULL,
               '1109016518',
               'USER',
               'WARNING',
               NULL,
               'Y');              
              
commit;              

-- DBTicket TE-3130
MERGE INTO MESSAGE_MASTER A
     USING (SELECT '1109016519' MSG_ID,
                   '1109016519' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (message_master_id,
                            bundle_name,
                            key,
                            msg,
                            ils_module,
                            msg_module,
                            msg_id,
                            msg_class,
                            msg_type,
                            ovride_role,
                            log_flag)
     values (seq_message_master_id.nextval,
             'ErrorMessage',
             '1109016519',
             'oLPN %s on pallet %s is mixed with guaranteed delivery oLPNs',
             'te',
             null,
             '1109016519',
             'USER',
             'NONDISPLAY',
             null,
             'Y');            
               
MERGE INTO MESSAGE_MASTER A
     USING (SELECT '1109016522' MSG_ID,
                   '1109016522' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (message_master_id,
                            bundle_name,
                            key,
                            msg,
                            ils_module,
                            msg_module,
                            msg_id,
                            msg_class,
                            msg_type,
                            ovride_role,
                            log_flag)
     values (seq_message_master_id.nextval,
               'ErrorMessage',
               '1109016522',
               'oLPN %s on pallet %s cannot meet promised delivery date. Needs upgrade',
               'te',
               null,
               '1109016522',
               'USER',
               'NONDISPLAY',
               null,
               'Y');                 
commit;

-- DBTicket TE-3144
UPDATE param_def
   SET PARAM_SUBGROUP_ID = 'CLSHIP'
 WHERE param_def_id = 'VALIDATE_GUARANTEED_DELIVERY';
 
MERGE INTO LABEL L
     USING (SELECT 'MLP' BUNDLE_NAME, 'isGuaranteedDelivery' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (seq_label_id.NEXTVAL,
             'isGuaranteedDelivery',
             'Is Guaranteed Delivery',
             'MLP');             
             
-- MERGE INTO XSCREEN_LABEL L
     -- USING (SELECT 'isGuaranteedDelivery' LABEL_KEY,
                   -- '360001' XSCREEN_ID,
                   -- 'MLP' LABEL_BUNDLE_NAME
              -- FROM DUAL) B
        -- ON (    L.LABEL_KEY = B.LABEL_KEY
            -- AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            -- AND L.XSCREEN_ID = B.XSCREEN_ID)
-- WHEN NOT MATCHED
-- THEN
	-- INSERT (XSCREEN_LABEL_ID, XSCREEN_ID, LABEL_BUNDLE_NAME, LABEL_KEY)  
	-- VALUES (sequence_xscreen_label_id.NEXTVAL,
             -- 360001,
             -- 'MLP',
             -- 'isGuaranteedDelivery');      

COMMIT;

-- DBTicket TE-3146
MERGE INTO MESSAGE_MASTER A
     USING (SELECT '200000035' MSG_ID,
                   '200000035' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
             '200000035',
             '200000035',
             'te',
             'MSP',
             'Shipment {0} has been modified, please try again.',
             'ErrorMessage',
             'USER',
             'ERROR',
             NULL,
             'N',
             current_timestamp,
             current_timestamp);            

MERGE INTO MESSAGE_MASTER A
     USING (SELECT '200000036' MSG_ID,
                   '200000036' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
             '200000036',
             '200000036',
             'te',
             'MSP',
             'RTE_TO should be same.',
             'ErrorMessage',
             'USER',
             'WARNING',
             NULL,
             'N',
             current_timestamp,
             current_timestamp);             

MERGE INTO MESSAGE_MASTER A
     USING (SELECT '200000037' MSG_ID,
                   '200000037' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
             '200000037',
             '200000037',
             'te',
             'MSP',
             'RTE_TYPE_1 should be same.',
             'ErrorMessage',
             'USER',
             'WARNING',
             NULL,
             'N',
             current_timestamp,
             current_timestamp);            
             
MERGE INTO MESSAGE_MASTER A
     USING (SELECT '200000038' MSG_ID,
                   '200000038' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
             '200000038',
             '200000038',
             'te',
             'MSP',
             'RTE_TYPE_2 should be same.',
             'ErrorMessage',
             'USER',
             'WARNING',
             NULL,
             'N',
             current_timestamp,
             current_timestamp);            

MERGE INTO MESSAGE_MASTER A
     USING (SELECT '200000039' MSG_ID,
                   '200000039' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
             '200000039',
             '200000039',
             'te',
             'MSP',
             'RTE_SWC_NBR should be same.',
             'ErrorMessage',
             'USER',
             'WARNING',
             NULL,
             'N',
             current_timestamp,
             current_timestamp);             
             
commit;

-- DBTicket TE-3181
exec manh_move_table_index('VAL_EXPECTED_RESULTS', 'TE_BST_DT_TBS', 'TE_BST_IDX_TBS');
exec manh_move_table_index('VAL_JOB_DTL', 'TE_BST_DT_TBS', 'TE_BST_IDX_TBS');
exec manh_move_table_index('VAL_JOB_HDR', 'TE_BST_DT_TBS', 'TE_BST_IDX_TBS');
exec manh_move_table_index('VAL_JOB_SEQ', 'TE_BST_DT_TBS', 'TE_BST_IDX_TBS');
exec manh_move_table_index('VAL_JOB_TAGS', 'TE_BST_DT_TBS', 'TE_BST_IDX_TBS');
exec manh_move_table_index('VAL_PARSED_SELECT_LIST', 'TE_BST_DT_TBS', 'TE_BST_IDX_TBS');
exec manh_move_table_index('VAL_RESULT_HIST', 'TE_BST_DT_TBS', 'TE_BST_IDX_TBS');
exec manh_move_table_index('VAL_RUNTIME_BINDS', 'TE_BST_DT_TBS', 'TE_BST_IDX_TBS');
exec manh_move_table_index('VAL_SQL', 'TE_BST_DT_TBS', 'TE_BST_IDX_TBS');
exec manh_move_table_index('VAL_SQL_BINDS', 'TE_BST_DT_TBS', 'TE_BST_IDX_TBS');
exec manh_move_table_index('VAL_SQL_TAGS', 'TE_BST_DT_TBS', 'TE_BST_IDX_TBS');
exec manh_move_table_index('VAL_TAG', 'TE_BST_DT_TBS', 'TE_BST_IDX_TBS');
exec manh_move_table_index('VAL_TAG_TYPE', 'TE_BST_DT_TBS', 'TE_BST_IDX_TBS');
exec manh_move_table_index('VAL_TAG_TYPE_XREF', 'TE_BST_DT_TBS', 'TE_BST_IDX_TBS');

begin
   execute immediate
      'alter table val_sql move lob (sql_text) store as (tablespace te_bst_idx_tbs)';
exception
   when others
   then
      null;
end;
/

begin
   execute immediate
      'alter table val_sql move lob (raw_text) store as (tablespace te_bst_idx_tbs)';
exception
   when others
   then
      null;
end;
/

-- DBTicket TE-3185
declare 
    v_idx_sql varchar2(4000);
begin
   for cur_idx
      in (select index_name, tablespace_name from user_indexes where status <> 'VALID' and table_name like 'VAL%'
            and index_type in ('NORMAL', 'NORMAL/REV', 'BITMAP', 'FUNCTION-BASED NORMAL', 'FUNCTION-BASED NORMAL/REV', 'FUNCTION-BASED BITMAP'))
   loop
      v_idx_sql := 'alter index ' || cur_idx.index_name || ' rebuild tablespace ' || cur_idx.tablespace_name;
      execute immediate v_idx_sql;
   end loop;
exception
   when others
   then
      null;
end;
/

-- DBTicket TE-3182 PLSQL
@DBScripts/Product/PLSQL_Objects/wm_auton_get_nxt_up_cnt_id.sql
@DBScripts/Product/PLSQL_Objects/wm_auton_find_or_create_manif.sql
@DBScripts/Product/PLSQL_Objects/wm_get_manifest_for_shpmt.sql

-- DBTicket TE-3175 PLSQL
@DBScripts/Product/PLSQL_Objects/wm_auton_find_or_create_shpmt.sql
@DBScripts/Product/PLSQL_Objects/wm_get_shpmt_for_inpt_crit.sql

-- DBTicket TE-3270 PLSQL
@DBScripts/Product/PLSQL_Objects/wm_auton_find_or_create_shpmt.sql

-- DBTicket TE-3046
DECLARE
      v_col_count       NUMBER(1);
BEGIN    
   SELECT count(*)
   INTO v_col_count
   FROM user_tab_columns
   WHERE TABLE_NAME = 'VAL_PARSED_SELECT_LIST' 
   AND COLUMN_NAME='COL_NAME' ;

   IF v_col_count=0
   THEN
      EXECUTE IMMEDIATE  'alter table VAL_PARSED_SELECT_LIST add COL_NAME varchar2(30)';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

alter table val_sql_binds modify bind_var varchar2(30);
alter table val_runtime_binds modify bind_var varchar2(30);

insert into val_tag
(
    tag, ui_desc
)
select 'data', 'Data' from dual union all
select 'psi', 'Picking Shorts' from dual;


insert into val_tag_type_xref
(
    type, tag
)
select '1', 'data' from dual union all
select '2', 'psi' from dual;

COMMIT;

-- DBTicket TE-3626 PLSQL
-- LInked PLSQL tag for above ticket TE-3046
@DBScripts/Product/PLSQL_Objects/wm_val_derive_select_info.sql
@DBScripts/Product/PLSQL_Objects/wm_val_get_bind_subs_sql.sql
@DBScripts/Product/PLSQL_Objects/val_job_sql_vw.sql
@DBScripts/Product/PLSQL_Objects/val_job_result_vw.sql
@DBScripts/Product/PLSQL_Objects/val_sql_tag_summ_vw.sql
@DBScripts/Product/PLSQL_Objects/val_job_result_summ_vw.sql

-- DBTicket TE-3263 PLSQL
@DBScripts/Product/PLSQL_Objects/wm_val_exec_sql.sql
@DBScripts/Product/PLSQL_Objects/wm_val_exec_job_intrnl.sql
@DBScripts/Product/PLSQL_Objects/wm_val_exec_job.sql

-- DBTicket TE-3284 PLSQL
@DBScripts/Product/PLSQL_Objects/wm_auton_find_or_create_manif.sql
@DBScripts/Product/PLSQL_Objects/wm_auton_find_or_create_shpmt.sql
@DBScripts/Product/PLSQL_Objects/wm_get_manifest_for_shpmt.sql
@DBScripts/Product/PLSQL_Objects/wm_get_shpmt_for_inpt_crit.sql

-- DBTicket TE-3377 PLSQL
@DBScripts/Product/PLSQL_Objects/wm_auton_find_or_create_manif.sql
@DBScripts/Product/PLSQL_Objects/wm_get_manifest_for_shpmt.sql

-- DBTicket TE-3187
comment on table VAL_EXPECTED_RESULTS is 'expected results repository';
comment on table VAL_JOB_DTL is 'repository of job details';
comment on table VAL_JOB_HDR is 'repository of job header';
comment on table VAL_JOB_SEQ is 'repository of job details execution sequence';
comment on table VAL_JOB_TAGS is 'repository of job tags';
comment on table VAL_PARSED_SELECT_LIST is 'repository of selected columns in the sql''s in jobs';
comment on table VAL_RESULT_HIST is 'repository of job result history';
comment on table VAL_RUNTIME_BINDS is 'repository to store run time binds provided for job sql';
comment on table VAL_SQL is 'sql repository';
comment on table VAL_SQL_BINDS is 'repository of static binds';
comment on table VAL_SQL_TAGS is 'repository of sql tags';
comment on table VAL_TAG is 'repository of tag descriptions';
comment on table VAL_TAG_TYPE is 'repository of tag types descriptions';
comment on table VAL_TAG_TYPE_XREF is 'repository of tag types';
comment on table VAL_EXPECTED_RESULTS is 'repository of expected results';

-- val_sql
comment on column val_sql.sql_id is 'Primary key';
comment on column val_sql.ident is 'Unique text identifier for each sql';
comment on column val_sql.sql_desc is 'Description';
comment on column val_sql.is_locked is 'Whether sql is disabled or not';
comment on column val_sql.sql_text is 'Formatted text';
comment on column val_sql.raw_text is 'Raw text';
comment on column val_sql.format_sql is 'Control formatting';
comment on column val_sql.create_date_time is 'Date created';
comment on column val_sql.mod_date_time is 'Date modified';
comment on column val_sql.user_id is 'Last modified user';
comment on column val_sql.text_len is 'Text length of sql';
comment on column val_sql.owner is 'Sql owner';
comment on column val_sql.bind_var_list is 'List of bind variables within sql';
comment on column val_sql.sql_hints is 'Optional hints for sql';
 
-- val_tag_type
comment on column val_tag_type.type is 'Tag type';
comment on column val_tag_type.ui_desc is 'Description';
 
-- val_tag
comment on column val_tag.tag is 'Tag';
comment on column val_tag.ui_desc is 'Description';

-- val_tag_type_xref
comment on column val_tag_type_xref.type is 'Tag type';
comment on column val_tag_type_xref.tag is 'Tag';

-- val_sql_tags
comment on column val_sql_tags.type is 'Tag type';
comment on column val_sql_tags.tag is 'Tag';
comment on column val_sql_tags.sql_id is 'Primary key of the sql table';

-- val_parsed_select_list
comment on column val_parsed_select_list.col_type is 'Data type indicator';
comment on column val_parsed_select_list.pos is 'Position of selected data';
comment on column val_parsed_select_list.sql_id is 'Primary key of the sql';
comment on column val_parsed_select_list.col_name is 'Name of column selected';

-- val_job_hdr
comment on column val_job_hdr.val_job_hdr_id is 'Primary key';
comment on column val_job_hdr.job_name is 'Validation job name';
comment on column val_job_hdr.job_desc is 'Description';
comment on column val_job_hdr.owner is 'Job owner';
comment on column val_job_hdr.is_locked is 'Whether job is disabled or not';
comment on column val_job_hdr.create_date_time is 'Created date';
comment on column val_job_hdr.mod_date_time is 'Modified date';
comment on column val_job_hdr.user_id is 'Last modified user';

-- val_job_dtl
comment on column val_job_dtl.val_job_dtl_id is 'Primary key';
comment on column val_job_dtl.val_job_hdr_id is 'Primary key of the job header';
comment on column val_job_dtl.sql_id is 'Primary key of the sql';
comment on column val_job_dtl.is_locked is 'Whether sql within job is disabled';
comment on column val_job_dtl.create_date_time is 'Created date';
comment on column val_job_dtl.mod_date_time is 'Modified date';
comment on column val_job_dtl.user_id is 'Last modified user';

-- val_job_tags
comment on column val_job_tags.type is 'Tag type';
comment on column val_job_tags.tag is 'Tag';
comment on column val_job_tags.val_job_hdr_id is 'Primary key of the job header';

-- val_sql_binds
comment on column val_sql_binds.val_job_dtl_id is 'Primary key of the job detail';
comment on column val_sql_binds.bind_var is 'Bind variable name';
comment on column val_sql_binds.bind_val is 'Character data type Bind value';
comment on column val_sql_binds.bind_val_int is 'Numeric data type bind value';

-- val_expected_results
comment on column val_expected_results.val_job_dtl_id is 'Primary key of the job detail';
comment on column val_expected_results.row_num is 'Result row value';
comment on column val_expected_results.pos is 'Result column position value';
comment on column val_expected_results.expected_value is 'Expected result';

-- val_result_hist
comment on column val_result_hist.txn_id is 'Transaction ID';
comment on column val_result_hist.val_job_dtl_id is 'Primary key of the job detail';
comment on column val_result_hist.last_run_passed is 'Pass or fail';
comment on column val_result_hist.create_date_time is 'Created date';
comment on column val_result_hist.user_id is 'Modified date';
comment on column val_result_hist.msg is 'Message text';

-- val_runtime_binds
comment on column val_runtime_binds.txn_id is 'Transaction ID';
comment on column val_runtime_binds.val_job_dtl_id is 'Primary key of the job detail';
comment on column val_runtime_binds.bind_var is 'Bind variable name';
comment on column val_runtime_binds.bind_val is 'Character data type Bind value';
comment on column val_runtime_binds.create_date_time is 'Created date';
comment on column val_runtime_binds.mod_date_time is 'Modified date';
comment on column val_runtime_binds.user_id is 'Last modified user';
comment on column val_runtime_binds.bind_val_int is 'Numeric data type bind value';

-- val_job_seq
comment on column val_job_seq.app_hook is 'Calling program''s hook point';
comment on column val_job_seq.seq_nbr is 'Called sequence';
comment on column val_job_seq.job_name is 'Validation job name';
comment on column val_job_seq.pgm_id is 'Calling program';

-- DBTicket TE-3618
-- db2 only

-- DBTicket TE-3597
-- DBTicket TE-3605
-- only db2

-- DBTicket TE-3590
INSERT INTO param_subgroup (param_group_id,
                            param_subgroup_id,
                            param_subgroup_name,
                            display_order)
     VALUES ('COM_TEPE',
             'MNFT',
             'Manifesting',
             2);

INSERT INTO param_grouping (param_grouping_id,
                            param_grouping_name,
                            param_group_id,
                            param_subgroup_id)
     VALUES ( (SELECT MAX (param_grouping_id) FROM param_grouping) + 1,
             'TEParameters',
             'COM_TEPE',
             'MNFT');

UPDATE param_def
   SET param_subgroup_id = 'MNFT', param_category = 'Manifesting'
 WHERE param_def_id = 'handling_option_for_partially_manifest_shipment';

UPDATE param_def
   SET param_subgroup_id = 'MNFT', param_category = 'Manifesting'
 WHERE param_def_id = 'print_shipping_labels_after_manifesting';

UPDATE param_def
   SET param_subgroup_id = 'MNFT', param_category = 'Manifesting'
 WHERE param_def_id = 'manifest_els_activity_code';

UPDATE param_def
   SET param_subgroup_id = 'MNFT', param_category = 'Manifesting'
 WHERE param_def_id = 'manifest_lm_interface';

UPDATE param_def
   SET param_subgroup_id = 'MNFT', param_category = 'Manifesting'
 WHERE param_def_id = 'print_hazmat_docs_at_manifest';

UPDATE param_def
   SET param_subgroup_id = 'MNFT', param_category = 'Manifesting'
 WHERE param_def_id = 'bypass_bulkrating_for_caselevel_rating';

UPDATE param_def
   SET param_subgroup_id = 'MNFT', param_category = 'Manifesting'
 WHERE param_def_id = 'display_olpn_ship_date_warning';

UPDATE param_def
   SET param_subgroup_id = 'MNFT', param_category = 'Manifesting'
 WHERE param_def_id = 'generate_sed_on_close_manifest';

UPDATE param_def
   SET param_subgroup_id = 'MNFT', param_category = 'Manifesting'
 WHERE param_def_id = 'handling_future_manifest_close_date';

UPDATE param_def
   SET param_subgroup_id = 'MNFT', param_category = 'Manifesting'
 WHERE param_def_id = 'LTL_TL_MANIFEST_TYPE';

UPDATE param_def
   SET param_subgroup_id = 'MNFT', param_category = 'Manifesting'
 WHERE param_def_id = 'override_order_pickup_start_dttm_for_manifest';

UPDATE param_def
   SET param_subgroup_id = 'MNFT', param_category = 'Manifesting'
 WHERE param_def_id = 'VALIDATE_GUARANTEED_DELIVERY';
 
COMMIT;

-- DBTicket TE-3669
MERGE INTO MESSAGE_MASTER A
 USING (SELECT '9517' MSG_ID,
 '111509517' KEY,
 'te' ILS_MODULE,
 'ErrorMessage' BUNDLE_NAME
 FROM DUAL) B
 ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
 WHEN NOT MATCHED
 THEN
 INSERT (MESSAGE_MASTER_ID,
 MSG_ID,
 KEY,
 ILS_MODULE,
 MSG_MODULE,
 MSG,
 BUNDLE_NAME,
 MSG_CLASS,
 MSG_TYPE,
 OVRIDE_ROLE,
 LOG_FLAG)
 VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
 B.MSG_ID,
 B.KEY,
 B.ILS_MODULE,
 'SHIPPING',
 'oLPN is not valid for UPS Final mile as destination is USPS delivered',
 B.BUNDLE_NAME,
 'SYSTEM',
 'ERROR',
 NULL,
 'N');
 
commit;

-- DBTicket TE-3688
update xmenu_item set navigation_key = 'maui://Wms.screen.mlp.MLP'
 where name = 'Shipment Planning Workspace';
commit;

-- DBTicket TE-3671 PLSQL
@DBScripts/Product/PLSQL_Objects/wm_parcel_zone_processing.sql

-- DBTicket TE-3726
update param_def set dflt_value = 'false' where param_def_id = 'enable_time_feasibility_validation';
commit;

-- DBTicket TE-3589
merge into label l
     using (select 'MLP' bundle_name, 'Type' key from dual) b
        on (l.key = b.key and l.bundle_name = b.bundle_name)
when not matched
then
   insert     (label_id,
               key,
               value,
               bundle_name)
       values (seq_label_id.nextval,
               'Type',
               'Type',
               'MLP');

merge into label l
     using (select 'MLP' bundle_name, 'oLPNStatus' key from dual) b
        on (l.key = b.key and l.bundle_name = b.bundle_name)
when not matched
then
   insert     (label_id,
               key,
               value,
               bundle_name)
       values (seq_label_id.nextval,
               'oLPNStatus',
               'oLPN Status',
               'MLP');

merge into label l
     using (select 'MLP' bundle_name, 'PlannedVolume' key from dual) b
        on (l.key = b.key and l.bundle_name = b.bundle_name)
when not matched
then
   insert     (label_id,
               key,
               value,
               bundle_name)
       values (seq_label_id.nextval,
               'PlannedVolume',
               'Planned Volume',
               'MLP');

merge into label l
     using (select 'MLP' bundle_name, 'PlannedWeight' key from dual) b
        on (l.key = b.key and l.bundle_name = b.bundle_name)
when not matched
then
   insert     (label_id,
               key,
               value,
               bundle_name)
       values (seq_label_id.nextval,
               'PlannedWeight',
               'Planned Weight',
               'MLP');

merge into label l
     using (select 'MLP' bundle_name, 'LPNweight' key from dual) b
        on (l.key = b.key and l.bundle_name = b.bundle_name)
when not matched
then
   insert     (label_id,
               key,
               value,
               bundle_name)
       values (seq_label_id.nextval,
               'LPNweight',
               'LPN weight',
               'MLP');
commit;

-- DBTicket TE-3283
merge into param_def t
using (
    select 'enable_tf_by_standard_mot' param_def_id,
            'COM_TEPE' param_group_id
    from dual
    ) s
    on (s.param_def_id = t.param_def_id and s.param_group_id = t.param_group_id)
when not matched then
    insert (PARAM_DEF_ID,
           PARAM_GROUP_ID,
           PARAM_SUBGROUP_ID,
           PARAM_NAME,
           DESCRIPTION,
           IS_DISABLED,
           IS_REQUIRED,
           DATA_TYPE,
           UI_TYPE,
           MIN_VALUE,
           IS_MIN_INC,
           MAX_VALUE,
           IS_MAX_INC,
           DFLT_VALUE,
           DISPLAY_ORDER,
           VALUE_GENERATOR_CLASS,
           IS_LOCALIZABLE,
           PARAM_CATEGORY,
           CARR_DFLT_VALUE,
           BP_DFLT_VALUE,
           PARAM_VALIDATOR_CLASS,
           CREATED_DTTM,
           LAST_UPDATED_DTTM)
    values ('enable_tf_by_standard_mot',
             'COM_TEPE',
             'CLSHIP',
             'enable_tf_by_standard_mot',
             'Enable Time Feasibility by Standard Mode',
             0,
             0,
             11,
             'MULTISELSTRLIST',
             NULL,
             0,
             NULL,
             0,
             '28',
             NULL,
             'com.manh.te.planning.finitevalue.StandardModesTE',
             0,
             'Planning',
             NULL,
             NULL,
             NULL,
             CURRENT_TIMESTAMP,
             CURRENT_TIMESTAMP);


exec sequpdt('LABEL','LABEL_ID', 'SEQ_LABEL_ID');
merge into label t
using (
    select 'ParamDef' bundle_name, 'enable_tf_by_standard_mot' key
    from dual
    ) s
    on (s.bundle_name = t.bundle_name and s.key = t.key)
when not matched then
    INSERT (LABEL_ID,
            KEY,
            VALUE,
            BUNDLE_NAME)
     VALUES (SEQ_LABEL_ID.NEXTVAL,
             'enable_tf_by_standard_mot',
             'Enable Time Feasibility by Standard Mode',
             'ParamDef');

commit;

-- DBTicket TE-3315
merge into label l
     using (select 'TariffImport' bundle_name, 'BasedataFromRootCompany' key
              from dual) b
        on (l.key = b.key and l.bundle_name = b.bundle_name)
when not matched
then
   insert     (label_id,
               key,
               value,
               bundle_name)
       values (seq_label_id.nextval,
               'BasedataFromRootCompany',
               'Basedata From Root Company',
               'TariffImport');
commit;

-- DBTicket TE-3325
update param_def set dflt_value = 'true'
 where param_def_id in ('USE_CACHED_PARCEL_RATING', 'USE_CUSTOM_PARCEL_TRANSIT_TIME', 'USE_FEDEX_SINGLECALL_FOR_TRANSITTIME_AND_RATE');
commit;

-- DBTicket TE-3275
update xmenu_item set screen_mode = 1 where name = 'Manual Load Planning';
commit;

-- DBTicket TE-3353
update label set value='Service Level at Root Company' where key='BasedataFromRootCompany' and bundle_name='TariffImport';
commit;

-- DBTicket TE-3365
MERGE INTO MESSAGE_MASTER A
     USING (SELECT '70064' MSG_ID,
                   '70064' KEY,
                   'teplanning' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '70064',
               '70064',
               'teplanning',
               'null',
               'Business Unit of Shipment {0} should match with that of Shipment {1}',
               'ErrorMessage',
               'null',
               'null',
               'null',
               NULL);               
               
COMMIT;

-- DBTicket TE-3367
DELETE FROM company_parameter
      WHERE param_Def_id = 'enable_time_feasibility_validation';

DELETE FROM company_parameter
      WHERE param_Def_id = 'time_feasibility_level';

DELETE FROM company_parameter
      WHERE param_Def_id = 'ignore_tariff_mot_servicelevel_protectionlevel';

DELETE FROM company_parameter
      WHERE param_Def_id = 'adjust_time_fsbl_for_current_date_time';

DELETE FROM company_parameter
      WHERE param_Def_id = 'KEY_DTTM';
      
COMMIT;

-- DBTicket TE-3593
merge into message_master a
     using (select '200000040' key, 'ErrorMessage' bundle_name from dual) b
        on (a.key = b.key and a.bundle_name = b.bundle_name)
when not matched
then
   insert     (message_master_id,
               msg_id,
               key,
               ils_module,
               msg_module,
               msg,
               bundle_name,
               msg_class,
               msg_type,
               ovride_role,
               log_flag,
               created_dttm,
               last_updated_dttm)
       values (
                 seq_message_master_id.nextval,
                 '200000040',
                 '200000040',
                 'te',
                 'MSP',
                 'Orders Business Unit incompatible with shipment Business Unit.',
                 'ErrorMessage',
                 'SYSTEM',
                 'ERROR',
                 null,
                 'N',
                 sysdate,
                 sysdate);
commit;

-- DBTicket TE-3734 PLSQL
@DBScripts/Product/PLSQL_Objects/wm_parcel_rate_processing.sql
@DBScripts/Product/PLSQL_Objects/wm_parcel_zone_processing.sql
@DBScripts/Product/PLSQL_Objects/wm_auton_find_or_create_shpmt.sql
@DBScripts/Product/PLSQL_Objects/wm_get_manifest_for_shpmt.sql

-- DBTicket TE-3735
update param_def set dflt_value = 'true' where param_def_id = 'enable_time_feasibility_validation';
commit;

-- DBTicket TE-3751
delete from label_display where disp_key='Assign LPNs To Next Shipment' and created_source='HandlingOptForPartialManifest';
delete from label where key='1' and value='Assign LPNs To Next Shipment' and bundle_name='HandlingOptForPartialManifest';
commit;

-- DBTicket TE-3788
MERGE INTO company_parameter c
     USING (SELECT company_id tc_company_id, 'VALIDATE_GUARANTEED_DELIVERY' param_def_id, 'COM_TEPE' param_group_id FROM company) c1
        ON (c.tc_company_id = c1.tc_company_id
        AND c.param_def_id = c1.param_def_id
        AND c.param_group_id = c1.param_group_id)
WHEN NOT MATCHED
THEN
   INSERT     (TC_COMPANY_ID,
               PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_VALUE,
               ERROR_CODE,
               CARR_PARAM_VALUE,
               Bp_Param_Value,
               Mark_For_Deletion,
               ADDITIONAL_QUALIFIER)
       VALUES (c1.tc_company_id,
               'VALIDATE_GUARANTEED_DELIVERY',
               'COM_TEPE',
               'Packing and Manifesting',
               NULL,
               NULL,
               NULL,
               0,
               -1);
COMMIT;

-- DBTicket TE-3797
MERGE INTO company_parameter c
     USING (SELECT COMPANY_ID tc_company_id, 'enable_tf_by_standard_mot' param_def_id,
                   'COM_TEPE' param_group_id FROM COMPANY) c1
        ON (c.tc_company_id = c1.tc_company_id
            AND c.param_def_id = c1.param_def_id
            AND c.param_group_id = c1.param_group_id)
WHEN NOT MATCHED
THEN
   INSERT     (TC_COMPANY_ID,
               PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_VALUE,
               ERROR_CODE,
               CARR_PARAM_VALUE,
               Bp_Param_Value,
               Mark_For_Deletion,
               ADDITIONAL_QUALIFIER)
       VALUES (c1.tc_company_id,
               'enable_tf_by_standard_mot',
               'COM_TEPE',
               '28',
               NULL,
               NULL,
               NULL,
               0,
               -1);
COMMIT;

-- DBTicket TE-3787
UPDATE label
   SET VALUE = 'SHIPPER''S DECLARATION FOR DANGEROUS GOODS'
 WHERE key = 'ShipperDeclarationForDangerGoods';

UPDATE label
   SET VALUE =
          'UN Number or Identification Number, Proper Shipping Name, Class or Division (Subsidiary Risk), Packing Group (if required), and all other required information.'
 WHERE key = 'DtlsNote';

UPDATE label
   SET VALUE =
          'FOR RADIOACTIVE MATERIAL SHIPMENT ACCEPTABLE FOR PASSENGER AIRCRAFT, THE SHIPMENT CONTAINS RADIOACTIVE MATERIAL INTENDED FOR USE IN OR INCIDENT TO RESEARCH, MEDICAL DIAGNOSIS, OR TREATMENT, ADR EUROPEAN TRANSPORT STATEMENT: CARRIAGE IN ACCORDANCE WITH 1.1.4.2.1'
 WHERE key = 'RadioactiveNote';

COMMIT;

-- DBTicket TE-3832
COMMENT ON COLUMN INPT_PARCL_RATE.MINIMUM_RATE IS 'Minimum Parcel Rate';

---- DBTicket TE-3836
-- MERGE INTO PARAM_DEF P
     -- USING (SELECT 'ENABLE_EXTERNAL_PARCEL_INTEGRATION' PARAM_DEF_ID,
                   -- 'COM_TEPE' PARAM_GROUP_ID
              -- FROM DUAL) D
        -- ON (P.PARAM_DEF_ID = D.PARAM_DEF_ID
            -- AND P.PARAM_GROUP_ID = D.PARAM_GROUP_ID)
-- WHEN NOT MATCHED
-- THEN
   -- INSERT     (PARAM_DEF_ID,
               -- PARAM_GROUP_ID,
               -- PARAM_SUBGROUP_ID,
               -- PARAM_NAME,
               -- DESCRIPTION,
               -- IS_DISABLED,
               -- IS_REQUIRED,
               -- DATA_TYPE,
               -- UI_TYPE,
               -- MIN_VALUE,
               -- IS_MIN_INC,
               -- MAX_VALUE,
               -- IS_MAX_INC,
               -- DFLT_VALUE,
               -- DISPLAY_ORDER,
               -- VALUE_GENERATOR_CLASS,
               -- IS_LOCALIZABLE,
               -- PARAM_CATEGORY,
               -- CARR_DFLT_VALUE,
               -- BP_DFLT_VALUE,
               -- PARAM_VALIDATOR_CLASS,
               -- CREATED_DTTM,
               -- LAST_UPDATED_DTTM,
               -- PARAM_LEVEL)
       -- VALUES ('ENABLE_EXTERNAL_PARCEL_INTEGRATION',
               -- 'COM_TEPE',
               -- 'CLSHIP',
               -- 'Enable External Parcel Integration',
               -- 'Enable External Parcel Integration',
               -- 0,
               -- 0,
               -- 6,
               -- 'CHECKBOX',
               -- NULL,
               -- 0,
               -- NULL,
               -- 0,
               -- 'false',
               -- NULL,
               -- NULL,
               -- 1,
               -- 'Planning',
               -- NULL,
               -- NULL,
               -- NULL,
               -- CURRENT_TIMESTAMP,
               -- CURRENT_TIMESTAMP,
               -- 0);

-- MERGE INTO PARAM_DEF P
     -- USING (SELECT 'USE_VELOCITY_TEMPLATES_FOR_EPI_REQUESTS' PARAM_DEF_ID,
                   -- 'COM_TEPE' PARAM_GROUP_ID
              -- FROM DUAL) D
        -- ON (P.PARAM_DEF_ID = D.PARAM_DEF_ID
            -- AND P.PARAM_GROUP_ID = D.PARAM_GROUP_ID)
-- WHEN NOT MATCHED
-- THEN
   -- INSERT     (PARAM_DEF_ID,
               -- PARAM_GROUP_ID,
               -- PARAM_SUBGROUP_ID,
               -- PARAM_NAME,
               -- DESCRIPTION,
               -- IS_DISABLED,
               -- IS_REQUIRED,
               -- DATA_TYPE,
               -- UI_TYPE,
               -- MIN_VALUE,
               -- IS_MIN_INC,
               -- MAX_VALUE,
               -- IS_MAX_INC,
               -- DFLT_VALUE,
               -- DISPLAY_ORDER,
               -- VALUE_GENERATOR_CLASS,
               -- IS_LOCALIZABLE,
               -- PARAM_CATEGORY,
               -- CARR_DFLT_VALUE,
               -- BP_DFLT_VALUE,
               -- PARAM_VALIDATOR_CLASS,
               -- CREATED_DTTM,
               -- LAST_UPDATED_DTTM,
               -- PARAM_LEVEL)
       -- VALUES ('USE_VELOCITY_TEMPLATES_FOR_EPI_REQUESTS',
               -- 'COM_TEPE',
               -- 'CLSHIP',
               -- 'Use Velocity Templates for EPI Requests',
               -- 'Use Velocity Templates for EPI Requests',
               -- 0,
               -- 0,
               -- 6,
               -- 'CHECKBOX',
               -- NULL,
               -- 0,
               -- NULL,
               -- 0,
               -- 'false',
               -- NULL,
               -- NULL,
               -- 1,
               -- 'Planning',
               -- NULL,
               -- NULL,
               -- NULL,
               -- CURRENT_TIMESTAMP,
               -- CURRENT_TIMESTAMP,
               -- 0);
               
-- COMMIT;               

-- DBTicket TE-3802
INSERT INTO ui_menu_item (UI_MENU_ITEM_ID,
                          UI_MENU_SCREEN_ID,
                          MENU_ID,
                          IS_DISPLAY,
                          DISPLAY_TEXT,
                          LINK,
                          LINK_TYPE,
                          LOCN_FLAG,
                          ITEM_SEQ_NBR,
                          UI_MENU_COMP_ID,
                          IS_MORE,
                          AJAX_ACTION,
                          AJAX_RERENDER,
                          AJAX_ONCOMPLETE,
                          RENDERED,
                          IMAGE_URL)
     VALUES (UI_MENU_ITEM_SEQ.NEXTVAL,
             (SELECT ui_menu_Screen_id
                      FROM ui_menu_screen
                     WHERE screen_id = '40000001'),
             1,
             0,
             'Invoice',
             'javascript:invoice()',
             'JS',
             7,
             14,
             'dataForm:page-content_footer-panel',
             1,
             NULL,
             NULL,
             NULL,
             'TRUE',
             NULL);

INSERT INTO PARAM_DEF (PARAM_DEF_ID,
                       PARAM_GROUP_ID,
                       PARAM_SUBGROUP_ID,
                       PARAM_NAME,
                       DESCRIPTION,
                       IS_DISABLED,
                       IS_REQUIRED,
                       DATA_TYPE,
                       UI_TYPE,
                       MIN_VALUE,
                       IS_MIN_INC,
                       MAX_VALUE,
                       IS_MAX_INC,
                       DFLT_VALUE,
                       DISPLAY_ORDER,
                       VALUE_GENERATOR_CLASS,
                       IS_LOCALIZABLE,
                       PARAM_CATEGORY,
                       CARR_DFLT_VALUE,
                       BP_DFLT_VALUE,
                       PARAM_VALIDATOR_CLASS,
                       CREATED_DTTM,
                       LAST_UPDATED_DTTM,
                       PARAM_LEVEL)
     VALUES ('generate_invoice_on_close_manifest',
             'COM_TEPE',
             'MNFT',
             'Generate Invoice on Close Manifest',
             'Generate Invoice on Close Manifest',
             0,
             0,
             6,
             'CHECKBOX',
             NULL,
             0,
             NULL,
             0,
             'false',
             260,
             NULL,
             1,
             'Manifesting',
             NULL,
             NULL,
             NULL,
             current_timestamp,
             current_timestamp,
             0);
             
COMMIT;

-- DBTicket TE-3811
insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'Import',
             'Import',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'CDTWizard',
             'Master Data Import',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'CarrierName',
             'Carrier Name',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'CDTRates',
             'Parcel Rates',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'CDTRatesUI',
             'Parcel Rate Import',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'ServiceLevel',
             'Service Level',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'AccessorialRates',
             'Accessorial Rates',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'Rates',
             'Rates',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'EffectiveDate',
             'Effective Date',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'UPSHundredWeightTier',
             'UPS HundredWeight Tier',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'CDTPOA',
             'Parcel Origin Attributes',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'CDTZonesUI',
             'Parcel Zone Import',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'DomesticZones',
             'Domestic Zones',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'ContractedTransitTimes',
             'Contracted Transit Times',
             'manifest_label',
             sysdate,
             sysdate);

-- xmenu entries
insert into xmenu_item (xmenu_item_id,
                        name,
                        short_name,
                        navigation_key,
                        tile_key,
                        icon,
                        bgcolor,
                        screen_mode,
                        screen_version,
                        base_section_name,
                        base_part_name,
                        created_source,
                        created_source_type,
                        created_dttm,
                        last_updated_source,
                        last_updated_source_type,
                        last_updated_dttm,
                        is_default)
     values ( (select nvl(max(xmenu_item_id), 3600000) + 1 from xmenu_item where to_char(xmenu_item_id) like '3600%'),
             'Parcel Carrier Data Import - Zones',
             'Parcel Carrier Data Import - Zones',
             '/te/cdt/ui/CDTMasterData.jsflps',
             null,
             'default-icon',
             '#8AF03A',
             0,
             1,
             'Configuration',
             'General',
             null,
             1,
             sysdate,
             null,
             1,
             sysdate,
             0);

insert into xmenu_item_app (xmenu_item_id,
                            app_id,
                            created_source,
                            created_source_type,
                            created_dttm,
                            last_updated_source,
                            last_updated_source_type,
                            last_updated_dttm)
     values ( (select xmenu_item_id
                 from xmenu_item
                where navigation_key = '/te/cdt/ui/CDTMasterData.jsflps'),
             (select app_id
                from app
               where app_name = 'Warehouse Management'),
             null,
             1,
             sysdate,
             null,
             1,
             sysdate);

insert into xmenu_item_permission (xmenu_item_id,
                                   permission_code,
                                   created_source,
                                   created_source_type,
                                   created_dttm,
                                   last_updated_source,
                                   last_updated_source_type,
                                   last_updated_dttm)
     values ( (select xmenu_item_id
                 from xmenu_item
                where navigation_key = '/te/cdt/ui/CDTMasterData.jsflps'),
             'COP',
             null,
             1,
             sysdate,
             null,
             1,
             sysdate);

exec sequpdt('FILTER_LAYOUT', 'FILTER_LAYOUT_ID', 'SEQ_FILTER_LAYOUT_ID');
insert into filter_layout (filter_layout_id,
                           object_type,
                           field_position,
                           is_required,
                           field_name,
                           field_label,
                           field_operators,
                           field_type,
                           value_generator_class,
                           selection_page_url,
                           operator_type,
                           sub_object_type,
                           table_name,
                           default_value,
                           is_localizable,
                           hibernate_field_name,
                           lookup_attribute,
                           screen_type,
                           columnscreen_type,
                           products_available)
     values (
               seq_filter_layout_id.nextval,
               'CDT_ZONES_FILTER',
               3,
               1,
               'Carrier Code',
               'Carrier',
               '=,!=',
               'UPPERCASE_STRING',
               null,
               '/lps/resources/editControl/scripts/idLookup.js?lookupType=Carrier&permission_code=VBD&paginReq=false',
               'COMP',
               null,
               null,
               null,
               0,
               null,
               '#{cboFilterLookupBackingBean.getBUMap},#{cboFilterLookupBackingBean.getOptionConstructMap}',
               null,
               null,
               null);

insert into filter_layout (filter_layout_id,
                           object_type,
                           field_position,
                           is_required,
                           field_name,
                           field_label,
                           field_operators,
                           field_type,
                           value_generator_class,
                           selection_page_url,
                           operator_type,
                           sub_object_type,
                           table_name,
                           default_value,
                           is_localizable,
                           hibernate_field_name,
                           lookup_attribute,
                           screen_type,
                           columnscreen_type,
                           products_available)
     values (seq_filter_layout_id.nextval,
             'CDT_ZONES_FILTER',
             2,
             1,
             'Tc Company Id',
             'BusinessUnit',
             '=',
             'anycase_string',
             'com.manh.common.ui.filter.BusinessUnitValueGenerator',
             null,
             'SELECT',
             null,
             null,
             null,
             0,
             null,
             null,
             12,
             null,
             null);

insert into filter_layout (filter_layout_id,
                           object_type,
                           field_position,
                           is_required,
                           field_name,
                           field_label,
                           field_operators,
                           field_type,
                           value_generator_class,
                           selection_page_url,
                           operator_type,
                           sub_object_type,
                           table_name,
                           default_value,
                           is_localizable,
                           hibernate_field_name,
                           lookup_attribute,
                           screen_type,
                           columnscreen_type,
                           products_available)
     values (
               seq_filter_layout_id.nextval,
               'CDT_ZONES_FILTER',
               1,
               1,
               'Facility ID',
               'Facility ID',
               '=,!=',
               'UPPERCASE_STRING',
               null,
               '/basedata/lookup/jsp/idLookup.jsp?lookupType=Facility&permission_code=VSFAC&object_type=Facility&paginReq=false',
               'COMP',
               null,
               null,
               null,
               0,
               null,
               '#{cboFilterLookupBackingBean.getBUMap},#{cboFilterLookupBackingBean.getOptionConstructMap}',
               12,
               null,
               null);


insert into filter_object (filter_object, screen_type, created_dttm, last_updated_dttm)
     values ('CDT_ZONES_FILTER',
             12,
             sysdate,
             null);


insert into filter_layout (filter_layout_id,
                           object_type,
                           field_position,
                           is_required,
                           field_name,
                           field_label,
                           field_operators,
                           field_type,
                           value_generator_class,
                           selection_page_url,
                           operator_type,
                           sub_object_type,
                           table_name,
                           default_value,
                           is_localizable,
                           hibernate_field_name,
                           lookup_attribute,
                           screen_type,
                           columnscreen_type,
                           products_available)
     values (
               seq_filter_layout_id.nextval,
               'CDT_RATES_FILTER',
               3,
               1,
               'Carrier Code',
               'Carrier',
               '=,!=',
               'UPPERCASE_STRING',
               null,
               '/lps/resources/editControl/scripts/idLookup.js?lookupType=Carrier&permission_code=VBD&paginReq=false',
               'COMP',
               null,
               null,
               null,
               0,
               null,
               '#{cboFilterLookupBackingBean.getBUMap},#{cboFilterLookupBackingBean.getOptionConstructMap}',
               null,
               null,
               null);

insert into filter_layout (filter_layout_id,
                           object_type,
                           field_position,
                           is_required,
                           field_name,
                           field_label,
                           field_operators,
                           field_type,
                           value_generator_class,
                           selection_page_url,
                           operator_type,
                           sub_object_type,
                           table_name,
                           default_value,
                           is_localizable,
                           hibernate_field_name,
                           lookup_attribute,
                           screen_type,
                           columnscreen_type,
                           products_available)
     values (seq_filter_layout_id.nextval,
             'CDT_RATES_FILTER',
             2,
             1,
             'Tc Company Id',
             'BusinessUnit',
             '=',
             'anycase_string',
             'com.manh.common.ui.filter.BusinessUnitValueGenerator',
             null,
             'SELECT',
             null,
             null,
             null,
             0,
             null,
             null,
             12,
             null,
             null);

insert into filter_layout (filter_layout_id,
                           object_type,
                           field_position,
                           is_required,
                           field_name,
                           field_label,
                           field_operators,
                           field_type,
                           value_generator_class,
                           selection_page_url,
                           operator_type,
                           sub_object_type,
                           table_name,
                           default_value,
                           is_localizable,
                           hibernate_field_name,
                           lookup_attribute,
                           screen_type,
                           columnscreen_type,
                           products_available)
     values (
               seq_filter_layout_id.nextval,
               'CDT_RATES_FILTER',
               1,
               1,
               'Facility ID',
               'Facility ID',
               '=,!=',
               'UPPERCASE_STRING',
               null,
               '/basedata/lookup/jsp/idLookup.jsp?lookupType=Facility&permission_code=VSFAC&object_type=Facility&paginReq=false',
               'COMP',
               null,
               null,
               null,
               0,
               null,
               '#{cboFilterLookupBackingBean.getBUMap},#{cboFilterLookupBackingBean.getOptionConstructMap}',
               12,
               null,
               null);

insert into filter_object (filter_object, screen_type, created_dttm, last_updated_dttm)
     values ('CDT_RATES_FILTER',
             12,
             sysdate,
             null);

insert into filter_layout (filter_layout_id,
                           object_type,
                           field_position,
                           is_required,
                           field_name,
                           field_label,
                           field_operators,
                           field_type,
                           value_generator_class,
                           selection_page_url,
                           operator_type,
                           sub_object_type,
                           table_name,
                           default_value,
                           is_localizable,
                           hibernate_field_name,
                           lookup_attribute,
                           screen_type,
                           columnscreen_type,
                           products_available)
     values (
               seq_filter_layout_id.nextval,
               'CDT_POA_FILTER',
               3,
               1,
               'Carrier Code',
               'Carrier',
               '=,!=',
               'UPPERCASE_STRING',
               null,
               '/lps/resources/editControl/scripts/idLookup.js?lookupType=Carrier&permission_code=VBD&paginReq=false',
               'COMP',
               null,
               null,
               null,
               0,
               null,
               '#{cboFilterLookupBackingBean.getBUMap},#{cboFilterLookupBackingBean.getOptionConstructMap}',
               null,
               null,
               null);

insert into filter_layout (filter_layout_id,
                           object_type,
                           field_position,
                           is_required,
                           field_name,
                           field_label,
                           field_operators,
                           field_type,
                           value_generator_class,
                           selection_page_url,
                           operator_type,
                           sub_object_type,
                           table_name,
                           default_value,
                           is_localizable,
                           hibernate_field_name,
                           lookup_attribute,
                           screen_type,
                           columnscreen_type,
                           products_available)
     values (seq_filter_layout_id.nextval,
             'CDT_POA_FILTER',
             2,
             1,
             'Tc Company Id',
             'BusinessUnit',
             '=',
             'anycase_string',
             'com.manh.common.ui.filter.BusinessUnitValueGenerator',
             null,
             'SELECT',
             null,
             null,
             null,
             0,
             null,
             null,
             12,
             null,
             null);

insert into filter_layout (filter_layout_id,
                           object_type,
                           field_position,
                           is_required,
                           field_name,
                           field_label,
                           field_operators,
                           field_type,
                           value_generator_class,
                           selection_page_url,
                           operator_type,
                           sub_object_type,
                           table_name,
                           default_value,
                           is_localizable,
                           hibernate_field_name,
                           lookup_attribute,
                           screen_type,
                           columnscreen_type,
                           products_available)
     values (
               seq_filter_layout_id.nextval,
               'CDT_POA_FILTER',
               1,
               1,
               'Facility ID',
               'Facility ID',
               '=,!=',
               'UPPERCASE_STRING',
               null,
               '/basedata/lookup/jsp/idLookup.jsp?lookupType=Facility&permission_code=VSFAC&object_type=Facility&paginReq=false',
               'COMP',
               null,
               null,
               null,
               0,
               null,
               '#{cboFilterLookupBackingBean.getBUMap},#{cboFilterLookupBackingBean.getOptionConstructMap}',
               12,
               null,
               null);

insert into filter_object (filter_object, screen_type, created_dttm, last_updated_dttm)
     values ('CDT_POA_FILTER',
             12,
             sysdate,
             null);

create sequence seq_parcel_zone_data_id
   minvalue 1
   maxvalue 9999999999
   increment by 1
   start with 1
   cache 20
   noorder
   nocycle;

alter table parcel_rate_calender add (service_level_id number(12) default 0 not null);
comment on column parcel_rate_calender.service_level_id is 'Service Level Id';

alter table parcel_zone_data add (last_updated_dttm timestamp default systimestamp, user_id varchar2(20) default 'MANH');
comment on column parcel_zone_data.last_updated_dttm is 'Last Updated Date';
comment on column parcel_zone_data.user_id is 'Last Edited by user';

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'ParcelOriginAttributeDetails',
             'Parcel Origin Attribute Details',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'OriginPostalCode_SC',
             'Origin postal code',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'OriginPostalCode',
             'Origin Postal code',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'CarrierCode_SC',
             'Carrier code',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'CarrierCode',
             'Carrier code',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'Mode',
             'Mode',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'ShipperAccountNumber',
             'Shipper Account Number',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'ShipperAccountNumber_SC',
             'Shipper Account Number',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'ShipperAccountNumberMask',
             'Shipper Account Number Mask',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'ShipperAccountNumberMask_SC',
             'Shipper account number mask',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'shipAcctUsrKey',
             'Shipper account user key',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'shipAcctPass',
             'Shipper account password',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'ManifestType_SC',
             'ManifestType_SC',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'TrackingNumberGenSeqID',
             'Tracking Number Gen. Seq. ID',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'TrackingNumberGenSeqID_SC',
             'Tracking number Gen. Seq. ID',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'ConsolidateDOtoParcelShipment',
             'Consolidate DO to Parcel Shipment?',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'ConsolidateDOtoParcelShipment_SC',
             'Consolidate DO to Parcel Shipment?',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'HubID',
             'Hub ID',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'ElectronicManifestRequired_SC',
             'ElectronicManifestRequired_SC',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'OBCommMappingandTransmissionHandler',
             'OB Comm. Mapping and Transmission Handler',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'OBCommMappingandTransmissionHandler_SC',
             'OB Comm. mapping and transmission handler',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'LiveShipmentLocation',
             'Live Load Location',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'LiveShipmentLocation_SC',
             'Live load location',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'CarrierPickUpRequired',
             'Carrier pickup request required',
             'manifest_label',
             sysdate,
             sysdate);

insert into label (label_id,
                   key,
                   value,
                   bundle_name,
                   created_dttm,
                   last_updated_dttm)
     values (seq_label_id.nextval,
             'CarrierPickUpRequired_SC',
             'Carrier pickup request required',
             'manifest_label',
             sysdate,
             sysdate);
commit;

-- DBTicket TE-3838
delete from message_display where disp_key in (select key from message_master where key='1109016516');
delete from message_master where key='1109016516';

update message_master set msg='oLPN {0} cannot meet promised delivery.' where key='110906519';
update message_display set disp_value='oLPN {0} cannot meet promised delivery.' where disp_key='110906519';

update message_master set msg='Pallet {0} has oLPN(s) that cannot meet promised delivery date.'
where key='1109016518';

update message_display set disp_value='Pallet {0} has oLPN(s) that cannot meet promised delivery date.' 
where disp_key='1109016518';

update message_master set msg='oLPN %s on pallet %s cannot meet promised delivery date.' 
where key='1109016522';

update message_display set disp_value='oLPN %s on pallet %s cannot meet promised delivery date.'
where disp_key='1109016522';

commit;

-- DBTicket TE-3881
exec sequpdt ('FILTER_LAYOUT', 'FILTER_LAYOUT_ID', 'SEQ_FILTER_LAYOUT_ID');
exec sequpdt ('RESOURCES', 'RESOURCE_ID', 'SEQ_RESOURCE_ID');

UPDATE XMENU_ITEM
   SET short_name = 'Parcel Carrier Data Wizard'
 WHERE navigation_key = '/te/cdt/ui/CDTMasterData.jsflps';

INSERT INTO FILTER_OBJECT (FILTER_OBJECT,
                           SCREEN_TYPE,
                           CREATED_DTTM,
                           LAST_UPDATED_DTTM)
     VALUES ('CDT_MASTER_FILTER',
             12,
             CURRENT_TIMESTAMP,
             NULL);

INSERT INTO FILTER_LAYOUT (FILTER_LAYOUT_ID,
                           OBJECT_TYPE,
                           FIELD_POSITION,
                           IS_REQUIRED,
                           FIELD_NAME,
                           FIELD_LABEL,
                           FIELD_OPERATORS,
                           FIELD_TYPE,
                           VALUE_GENERATOR_CLASS,
                           SELECTION_PAGE_URL,
                           OPERATOR_TYPE,
                           SUB_OBJECT_TYPE,
                           TABLE_NAME,
                           DEFAULT_VALUE,
                           IS_LOCALIZABLE,
                           HIBERNATE_FIELD_NAME,
                           LOOKUP_ATTRIBUTE,
                           SCREEN_TYPE,
                           COLUMNSCREEN_TYPE,
                           PRODUCTS_AVAILABLE)
     VALUES (seq_filter_layout_id.nextval,
             'CDT_MASTER_FILTER',
             2,
             1,
             'Tc Company Id',
             'BusinessUnit',
             '=',
             'anycase_string',
             'com.manh.common.ui.filter.BusinessUnitValueGenerator',
             NULL,
             'SELECT',
             NULL,
             NULL,
             NULL,
             0,
             NULL,
             NULL,
             12,
             NULL,
             NULL);

INSERT INTO FILTER_LAYOUT (FILTER_LAYOUT_ID,
                           OBJECT_TYPE,
                           FIELD_POSITION,
                           IS_REQUIRED,
                           FIELD_NAME,
                           FIELD_LABEL,
                           FIELD_OPERATORS,
                           FIELD_TYPE,
                           VALUE_GENERATOR_CLASS,
                           SELECTION_PAGE_URL,
                           OPERATOR_TYPE,
                           SUB_OBJECT_TYPE,
                           TABLE_NAME,
                           DEFAULT_VALUE,
                           IS_LOCALIZABLE,
                           HIBERNATE_FIELD_NAME,
                           lookup_attribute,
                           screen_type,
                           columnscreen_type,
                           products_available)
     VALUES (seq_filter_layout_id.nextval,
               'CDT_MASTER_FILTER',
               1,
               1,
               'Facility ID',
               'Facility ID',
               '=,!=',
               'UPPERCASE_STRING',
               NULL,
               '/basedata/lookup/jsp/idLookup.jsp?lookupType=Facility'||'&'||'permission_code=VSFAC'||'&'||'object_type=Facility'||'&'||'paginReq=false',
               'COMP',
               NULL,
               NULL,
               NULL,
               0,
               NULL,
               '#{cboFilterLookupBackingBean.getBUMap},#{cboFilterLookupBackingBean.getOptionConstructMap}',
               12,
               NULL,
               NULL);

MERGE INTO RESOURCES A
     USING (SELECT '/te/cdt/ui/CDTMasterData.xhtml' AS URI, 1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/cdt/ui/CDTMasterData.xhtml',
               'ACM',
               1,
               NULL);

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'COP' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/cdt/ui/CDTMasterData.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (rp1.resource_id, rp1.permission_code);
       
COMMIT;

-- DBTicket TE-3885
MERGE INTO RESOURCES A
     USING (SELECT '/te/cdt/ui/CDTZones.xhtml' AS URI, 1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/cdt/ui/CDTZones.xhtml',
               'ACM',
               1,
               NULL);

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'COP' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/cdt/ui/CDTZones.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (rp1.resource_id, rp1.permission_code);



MERGE INTO RESOURCES A
     USING (SELECT '/te/cdt/ui/CDTRates.xhtml' AS URI, 1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/cdt/ui/CDTRates.xhtml',
               'ACM',
               1,
               NULL);

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'COP' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/cdt/ui/CDTRates.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (rp1.resource_id, rp1.permission_code);

MERGE INTO RESOURCES A
     USING (SELECT '/te/cdt/ui/CDTPOAImport.xhtml' AS URI, 1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/cdt/ui/CDTPOAImport.xhtml',
               'ACM',
               1,
               NULL);

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'COP' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/cdt/ui/CDTPOAImport.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (rp1.resource_id, rp1.permission_code);
       
COMMIT;       

-- DBTicket TE-3890
MERGE INTO company_parameter c
     USING (SELECT COMPANY_ID tc_company_id, 'generate_invoice_on_close_manifest' param_def_id,
                   'COM_TEPE' param_group_id
                      FROM COMPANY) c1
        ON (    c.tc_company_id = c1.tc_company_id
            AND c.param_def_id = c1.param_def_id
            AND c.param_group_id = c1.param_group_id)
WHEN NOT MATCHED
THEN
   INSERT     (TC_COMPANY_ID,
               PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_VALUE,
               ERROR_CODE,
               CARR_PARAM_VALUE,
               Bp_Param_Value,
               Mark_For_Deletion,
               ADDITIONAL_QUALIFIER)
       VALUES (c1.tc_company_id,
               'generate_invoice_on_close_manifest',
               'COM_TEPE',
               'false',
               NULL,
               NULL,
               NULL,
               0,
               -1);
COMMIT;

-- DBTicket TE-3898
insert into message_master (message_master_id,
                            msg_id,
                            key,
                            ils_module,
                            msg_module,
                            msg,
                            bundle_name,
                            msg_class,
                            msg_type,
                            ovride_role,
                            log_flag)
     values (seq_message_master_id.nextval,
             '9921001',
             '9921001',
             'teplanning',
             null,
             'EPI {0} failed for LPN {1}: {2}',
             'ErrorMessage',
             null,
             'ERROR',
             null,
             'Y');
commit;

-- DBTicket TE-3906
update param_def
   set dflt_value = 'true'
 where param_def_id = 'bypass_bulkrating_for_caselevel_rating';

update company_parameter
   set param_value = 'true'
 where param_def_id = 'bypass_bulkrating_for_caselevel_rating';

commit;

-- DBTicket TE-3919
update xmenu_item
   set name = 'Parcel Configuration Wizard',
       short_name = 'Parcel Configuration Wizard'
 where navigation_key = '/te/cdt/ui/CDTMasterData.jsflps' and base_section_name = 'Configuration';

commit;

-- DBTicket TE-3930
---- related to te3683 validation toolkit ddl changes.
delete from val_job_seq;
delete from val_job_hdr;
delete from val_sql;

DECLARE
  num_rows integer;
BEGIN
   SELECT count(*)
       INTO num_rows
   FROM user_constraints
   WHERE constraint_name = 'PK_VAL_JOB_SEQ';

   IF num_rows = 1 THEN 
       EXECUTE IMMEDIATE 'ALTER TABLE val_job_seq drop CONSTRAINT PK_VAL_JOB_SEQ';
   END IF;
END;
/

call quiet_drop('index','pk_val_job_seq');
alter table val_job_seq add constraint pk_val_job_seq  primary key(app_hook, pgm_id, seq_nbr) using index tablespace te_bst_idx_tbs;

alter table tmp_val_parm add parent_txn_id number(9);
alter table val_result_hist add parent_txn_id number(9) not null;

DECLARE
  num_rows integer;
BEGIN
   SELECT count(*)
      INTO num_rows
   FROM user_constraints
   WHERE constraint_name = 'FK_VRB_TO_VJD';

   IF num_rows = 1 THEN 
       EXECUTE IMMEDIATE 'ALTER TABLE val_runtime_binds drop constraint fk_vrb_to_vjd';
   END IF;
       EXECUTE IMMEDIATE 'ALTER TABLE val_runtime_binds add constraint fk_vrb_to_vjd foreign key(txn_id, val_job_dtl_id) references val_result_hist(txn_id, val_job_dtl_id) on delete cascade';
   
END;
/


-- DBTicket TE-3683 PLSQL
@DBScripts/Product/PLSQL_Objects/wm_val_get_user_id.sql
@DBScripts/Product/PLSQL_Objects/wm_val_get_bind_subs_sql.sql
@DBScripts/Product/PLSQL_Objects/wm_get_bind_values_for_job_dtl.sql
@DBScripts/Product/PLSQL_Objects/wm_get_exp_results_for_job_dtl.sql
@DBScripts/Product/PLSQL_Objects/wm_val_get_col_list_for_sql.sql
@DBScripts/Product/PLSQL_Objects/wm_val_get_parent_txn_id.sql
@DBScripts/Product/PLSQL_Objects/wm_val_log.sql
@DBScripts/Product/PLSQL_Objects/wm_val_auton_log_result.sql
@DBScripts/Product/PLSQL_Objects/wm_val_add_job_result.sql
@DBScripts/Product/PLSQL_Objects/wm_val_create_sql_data.sql
@DBScripts/Product/PLSQL_Objects/wm_val_exec_sql_intrnl.sql
@DBScripts/Product/PLSQL_Objects/wm_val_exec_job_intrnl.sql
@DBScripts/Product/PLSQL_Objects/wm_val_extract_runtime_binds.sql
@DBScripts/Product/PLSQL_Objects/wm_val_exec_job.sql
@DBScripts/Product/PLSQL_Objects/wm_val_exec_sql.sql
@DBScripts/Product/PLSQL_Objects/val_job_result_summ_vw.sql
@DBScripts/Product/PLSQL_Objects/val_job_data_vw.sql
@DBScripts/Product/PLSQL_Objects/val_sql_data_vw.sql
@DBScripts/Product/PLSQL_Objects/val_job_result_vw.sql

-- DBTicket TE-3934
alter table val_job_hdr modify job_name varchar2(30);
alter table val_job_seq modify job_name varchar2(30);

-- DBTicket TE-3932
Insert into MESSAGE_MASTER (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG,CREATED_DTTM,LAST_UPDATED_DTTM) 
values (SEQ_MESSAGE_MASTER_ID.NEXTVAL,'9920162','9920162','te','cdt','Please select a Facility','ErrorMessage','SYSTEM','ERROR',null,NULL,sysdate,sysdate);

Insert into MESSAGE_MASTER (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG,CREATED_DTTM,LAST_UPDATED_DTTM) 
values (SEQ_MESSAGE_MASTER_ID.NEXTVAL,'9920152','9920152','te','cdt','Please select a BU','ErrorMessage','SYSTEM','ERROR',null,NULL,sysdate,sysdate);

Insert into MESSAGE_MASTER (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG,CREATED_DTTM,LAST_UPDATED_DTTM) 
values (SEQ_MESSAGE_MASTER_ID.NEXTVAL,'9920153','9920153','te','cdt','Please select a Carrier','ErrorMessage','SYSTEM','ERROR',null,NULL,sysdate,sysdate);

Insert into MESSAGE_MASTER (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG,CREATED_DTTM,LAST_UPDATED_DTTM) 
values (SEQ_MESSAGE_MASTER_ID.NEXTVAL,'9920154','9920154','te','cdt','Please select a Service Level','ErrorMessage','SYSTEM','ERROR',null,NULL,sysdate,sysdate);

Insert into MESSAGE_MASTER (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG,CREATED_DTTM,LAST_UPDATED_DTTM) 
values (SEQ_MESSAGE_MASTER_ID.NEXTVAL,'9920155','9920155','te','cdt','Please select a File to import','ErrorMessage','SYSTEM','ERROR',null,NULL,sysdate,sysdate);

Insert into MESSAGE_MASTER (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG,CREATED_DTTM,LAST_UPDATED_DTTM) 
values (SEQ_MESSAGE_MASTER_ID.NEXTVAL,'9920156','9920156','te','cdt','Please select an effective date','ErrorMessage','SYSTEM','ERROR',null,NULL,sysdate,sysdate);

Insert into MESSAGE_MASTER (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG,CREATED_DTTM,LAST_UPDATED_DTTM) 
values (SEQ_MESSAGE_MASTER_ID.NEXTVAL,'9920157','9920157','te','cdt','Please select an future date','ErrorMessage','SYSTEM','ERROR',null,NULL,sysdate,sysdate);

Insert into MESSAGE_MASTER (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG,CREATED_DTTM,LAST_UPDATED_DTTM) 
values (SEQ_MESSAGE_MASTER_ID.NEXTVAL,'9920158','9920158','te','cdt','No data specified for import','ErrorMessage','SYSTEM','ERROR',null,NULL,sysdate,sysdate);

Insert into MESSAGE_MASTER (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG,CREATED_DTTM,LAST_UPDATED_DTTM) 
values (SEQ_MESSAGE_MASTER_ID.NEXTVAL,'9920159','9920159','te','cdt','Master Data Import Successful','ErrorMessage','SYSTEM','INFORM',null,NULL,sysdate,sysdate);

Insert into MESSAGE_MASTER (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG,CREATED_DTTM,LAST_UPDATED_DTTM) 
values (SEQ_MESSAGE_MASTER_ID.NEXTVAL,'9920160','9920160','te','cdt','%Filename% Import Failed, please check the log','ErrorMessage','SYSTEM','INFORM',null,NULL,sysdate,sysdate);

Insert into MESSAGE_MASTER (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG,CREATED_DTTM,LAST_UPDATED_DTTM) 
values (SEQ_MESSAGE_MASTER_ID.NEXTVAL,'9920161','9920161','te','cdt','%Filename% Import Successful','ErrorMessage','SYSTEM','INFORM',null,NULL,sysdate,sysdate);

commit;

-- DBTicket TE-3957
UPDATE MESSAGE_MASTER
   SET MSG = '{0} Import Failed, please check the log'
 WHERE MSG_ID = '9920160';

UPDATE MESSAGE_MASTER
   SET MSG = '{0} Import Successful'
 WHERE MSG_ID = '9920161';

UPDATE label
   SET VALUE = 'Parcel Rate Import'
 WHERE key = 'CDTRates';

MERGE INTO LABEL L
     USING (SELECT 'manifest_label' BUNDLE_NAME, 'ImportMasterData' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'ImportMasterData',
               'Import Master Data',
               'manifest_label');

MERGE INTO LABEL L
     USING (SELECT 'manifest_label' BUNDLE_NAME, 'SkipToImportZones' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'SkipToImportZones',
               'Parcel Zone Import',
               'manifest_label');

COMMIT;            

-- DBTicket TE-3937
MERGE INTO PARAM_DEF P
     USING (SELECT 'CACHE_VELOCITY_TEMPLATES_FOR_EPI_REQUESTS' PARAM_DEF_ID,
                   'COM_TEPE' PARAM_GROUP_ID
              FROM DUAL) D
        ON (P.PARAM_DEF_ID = D.PARAM_DEF_ID
            AND P.PARAM_GROUP_ID = D.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM,
               PARAM_LEVEL)
       VALUES ('CACHE_VELOCITY_TEMPLATES_FOR_EPI_REQUESTS',
               'COM_TEPE',
               'CLSHIP',
               'Cache Velocity Templates for EPI Requests',
               'Cache Velocity Templates for EPI Requests',
               0,
               0,
               6,
               'CHECKBOX',
               NULL,
               0,
               NULL,
               0,
               'true',
               NULL,
               NULL,
               1,
               'Planning',
               NULL,
               NULL,
               NULL,
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               0);
               
COMMIT;               

-- DBTicket TE-3973
UPDATE label
   SET VALUE = 'Electronic manifest required?'
 WHERE bundle_name = 'manifest_label'
       AND key = 'ElectronicManifestRequired_SC';

UPDATE label
   SET VALUE = 'Manifest type'
 WHERE bundle_name = 'manifest_label' AND key = 'ManifestType_SC';

UPDATE LABEL
   SET VALUE = 'Parcel Base Data Import'
 WHERE BUNDLE_NAME = 'manifest_label' AND key = 'CDTWizard';

UPDATE LABEL
   SET VALUE = 'Parcel Origin Attributes Bulk Update'
 WHERE BUNDLE_NAME = 'manifest_label' AND key = 'CDTPOA';

UPDATE label
   SET VALUE = 'Parcel Base Data Import'
 WHERE bundle_name = 'manifest_label' AND key = 'ImportMasterData';

UPDATE LABEL
   SET VALUE = 'Parcel Zones Import'
 WHERE BUNDLE_NAME = 'manifest_label' AND key = 'CDTZonesUI';

UPDATE LABEL
   SET VALUE = 'Parcel Rates Import'
 WHERE BUNDLE_NAME = 'manifest_label' AND key = 'CDTRates';

UPDATE label
   SET VALUE = 'Parcel Rates Import'
 WHERE bundle_name = 'manifest_label' AND key = 'CDTRatesUI';
 
COMMIT;

-- DBTicket TE-3980
insert into message_master (message_master_id,
                            msg_id,
                            key,
                            ils_module,
                            msg_module,
                            msg,
                            bundle_name,
                            msg_class,
                            msg_type,
                            ovride_role,
                            log_flag,
                            created_dttm,
                            last_updated_dttm)
     values (seq_message_master_id.nextval,
             '9920164',
             '9920164',
             'te',
             'cdt',
             'Zone file Origin Zip invalid for Selected Facility',
             'ErrorMessage',
             'SYSTEM',
             'ERROR',
             null,
             null,
             sysdate,
             sysdate);

insert into message_master (message_master_id,
                            msg_id,
                            key,
                            ils_module,
                            msg_module,
                            msg,
                            bundle_name,
                            msg_class,
                            msg_type,
                            ovride_role,
                            log_flag,
                            created_dttm,
                            last_updated_dttm)
     values (seq_message_master_id.nextval,
             '9920163',
             '9920163',
             'te',
             'cdt',
             'Invalid File Format',
             'ErrorMessage',
             'SYSTEM',
             'ERROR',
             null,
             null,
             sysdate,
             sysdate);

insert into message_master (message_master_id,
                            msg_id,
                            key,
                            ils_module,
                            msg_module,
                            msg,
                            bundle_name,
                            msg_class,
                            msg_type,
                            ovride_role,
                            log_flag,
                            created_dttm,
                            last_updated_dttm)
     values (seq_message_master_id.nextval,
             '9920165',
             '9920165',
             'te',
             'cdt',
             'Zones and Rates are handled by FedEx server',
             'ErrorMessage',
             'SYSTEM',
             'ERROR',
             null,
             null,
             sysdate,
             sysdate);

insert into message_master (message_master_id,
                            msg_id,
                            key,
                            ils_module,
                            msg_module,
                            msg,
                            bundle_name,
                            msg_class,
                            msg_type,
                            ovride_role,
                            log_flag,
                            created_dttm,
                            last_updated_dttm)
     values (seq_message_master_id.nextval,
             '9920166',
             '9920166',
             'te',
             'cdt',
             'Incorrect File Selected',
             'ErrorMessage',
             'SYSTEM',
             'ERROR',
             null,
             null,
             sysdate,
             sysdate);

insert into message_master (message_master_id,
                            msg_id,
                            key,
                            ils_module,
                            msg_module,
                            msg,
                            bundle_name,
                            msg_class,
                            msg_type,
                            ovride_role,
                            log_flag,
                            created_dttm,
                            last_updated_dttm)
     values (seq_message_master_id.nextval,
             '9920167',
             '9920167',
             'te',
             'cdt',
             'Import Zone Data before importing rates',
             'ErrorMessage',
             'SYSTEM',
             'ERROR',
             null,
             null,
             sysdate,
             sysdate);

update message_master
   set msg = 'Please select a future date'
 where key = '9920157';

update message_display
   set disp_value = 'Please select a future date'
 where disp_key = '9920157';

update xmenu_item
   set name = 'Import Parcel Carrier Base Data',
       short_name = 'Import Parcel Carrier Base Data'
 where navigation_key = '/te/cdt/ui/CDTMasterData.jsflps';

insert into xmenu_item (xmenu_item_id,
                        name,
                        short_name,
                        navigation_key,
                        tile_key,
                        icon,
                        bgcolor,
                        screen_mode,
                        screen_version,
                        base_section_name,
                        base_part_name,
                        created_source,
                        created_source_type,
                        created_dttm,
                        last_updated_source,
                        last_updated_source_type,
                        last_updated_dttm,
                        is_default)
     values ((select nvl(max(xmenu_item_id), 3600000) + 1 from xmenu_item where to_char(xmenu_item_id) like '3600%'),
             'Import Parcel Zones',
             'Import Parcel Zones',
             '/te/cdt/ui/CDTZones.jsflps',
             null,
             'default-icon',
             '#8AF03A',
             0,
             1,
             'Configuration',
             'General',
             null,
             1,
             sysdate,
             null,
             1,
             sysdate,
             0);

insert into xmenu_item_app (xmenu_item_id,
                            app_id,
                            created_source,
                            created_source_type,
                            created_dttm,
                            last_updated_source,
                            last_updated_source_type,
                            last_updated_dttm)
     values ( (select xmenu_item_id
                 from xmenu_item
                where navigation_key = '/te/cdt/ui/CDTZones.jsflps'),
             36,
             null,
             1,
             sysdate,
             null,
             1,
             sysdate);

insert into xmenu_item_permission (xmenu_item_id,
                                   permission_code,
                                   created_source,
                                   created_source_type,
                                   created_dttm,
                                   last_updated_source,
                                   last_updated_source_type,
                                   last_updated_dttm)
     values ( (select xmenu_item_id
                 from xmenu_item
                where navigation_key = '/te/cdt/ui/CDTZones.jsflps'),
             'COP',
             null,
             1,
             sysdate,
             null,
             1,
             sysdate);

insert into xmenu_item (xmenu_item_id,
                        name,
                        short_name,
                        navigation_key,
                        tile_key,
                        icon,
                        bgcolor,
                        screen_mode,
                        screen_version,
                        base_section_name,
                        base_part_name,
                        created_source,
                        created_source_type,
                        created_dttm,
                        last_updated_source,
                        last_updated_source_type,
                        last_updated_dttm,
                        is_default)
     values ((select nvl(max(xmenu_item_id), 3600000) + 1 from xmenu_item where to_char(xmenu_item_id) like '3600%'),
             'Import Parcel Rates',
             'Import Parcel Rates',
             '/te/cdt/ui/CDTRates.jsflps',
             null,
             'default-icon',
             '#8AF03A',
             0,
             1,
             'Configuration',
             'General',
             null,
             1,
             sysdate,
             null,
             1,
             sysdate,
             0);

insert into xmenu_item_app (xmenu_item_id,
                            app_id,
                            created_source,
                            created_source_type,
                            created_dttm,
                            last_updated_source,
                            last_updated_source_type,
                            last_updated_dttm)
     values ( (select xmenu_item_id
                 from xmenu_item
                where navigation_key = '/te/cdt/ui/CDTRates.jsflps'),
             36,
             null,
             1,
             sysdate,
             null,
             1,
             sysdate);

insert into xmenu_item_permission (xmenu_item_id,
                                   permission_code,
                                   created_source,
                                   created_source_type,
                                   created_dttm,
                                   last_updated_source,
                                   last_updated_source_type,
                                   last_updated_dttm)
     values ( (select xmenu_item_id
                 from xmenu_item
                where navigation_key = '/te/cdt/ui/CDTRates.jsflps'),
             'COP',
             null,
             1,
             sysdate,
             null,
             1,
             sysdate);

insert into xmenu_item (xmenu_item_id,
                        name,
                        short_name,
                        navigation_key,
                        tile_key,
                        icon,
                        bgcolor,
                        screen_mode,
                        screen_version,
                        base_section_name,
                        base_part_name,
                        created_source,
                        created_source_type,
                        created_dttm,
                        last_updated_source,
                        last_updated_source_type,
                        last_updated_dttm,
                        is_default)
     values ((select nvl(max(xmenu_item_id), 3600000) + 1 from xmenu_item where to_char(xmenu_item_id) like '3600%'),
             'Import Parcel Origin Attributes',
             'Import Parcel Origin Attributes',
             '/te/cdt/ui/CDTPOAImport.jsflps',
             null,
             'default-icon',
             '#8AF03A',
             0,
             1,
             'Configuration',
             'General',
             null,
             1,
             sysdate,
             null,
             1,
             sysdate,
             0);

insert into xmenu_item_app (xmenu_item_id,
                            app_id,
                            created_source,
                            created_source_type,
                            created_dttm,
                            last_updated_source,
                            last_updated_source_type,
                            last_updated_dttm)
     values ( (select xmenu_item_id
                 from xmenu_item
                where navigation_key = '/te/cdt/ui/CDTPOAImport.jsflps'),
             36,
             null,
             1,
             sysdate,
             null,
             1,
             sysdate);

insert into xmenu_item_permission (xmenu_item_id,
                                   permission_code,
                                   created_source,
                                   created_source_type,
                                   created_dttm,
                                   last_updated_source,
                                   last_updated_source_type,
                                   last_updated_dttm)
     values ( (select xmenu_item_id
                 from xmenu_item
                where navigation_key = '/te/cdt/ui/CDTPOAImport.jsflps'),
             'COP',
             null,
             1,
             sysdate,
             null,
             1,
             sysdate);

commit;

-- DBTicket TE-3992
UPDATE message_master
   SET msg = 'Please select a Business Unit'
 WHERE key = '9920152';

COMMIT;

-- DBTicket TE-3987
UPDATE xmenu_item
   SET Name = 'Parcel Base Data Import',
       Short_name = 'Parcel Base Data Import'
 WHERE navigation_key = '/te/cdt/ui/CDTMasterData.jsflps';

UPDATE xmenu_item
   SET Name = 'Parcel Zones Import', Short_name = 'Parcel Zones Import'
 WHERE navigation_key = '/te/cdt/ui/CDTZones.jsflps';

UPDATE xmenu_item
   SET Name = 'Parcel Rates Import', Short_name = 'Parcel Rates Import'
 WHERE navigation_key = '/te/cdt/ui/CDTRates.jsflps';

UPDATE xmenu_item
   SET Name = 'Parcel Origin Attributes Bulk Update',
       Short_name = 'Parcel Origin Attributes Bulk Update'
 WHERE navigation_key = '/te/cdt/ui/CDTPOAImport.jsflps';

INSERT INTO FILTER_LAYOUT (FILTER_LAYOUT_ID,
                           OBJECT_TYPE,
                           FIELD_POSITION,
                           IS_REQUIRED,
                           FIELD_NAME,
                           FIELD_LABEL,
                           FIELD_OPERATORS,
                           FIELD_TYPE,
                           VALUE_GENERATOR_CLASS,
                           SELECTION_PAGE_URL,
                           OPERATOR_TYPE,
                           SUB_OBJECT_TYPE,
                           TABLE_NAME,
                           DEFAULT_VALUE,
                           IS_LOCALIZABLE,
                           HIBERNATE_FIELD_NAME,
                           lookup_attribute,
                           screen_type,
                           columnscreen_type,
                           products_available)
     VALUES (SEQ_FILTER_LAYOUT_ID.nextval,
             'CDT_RATES_FILTER',
             4,
             0,
             'Service Level Type',
             'Service Level Type',
             '=,!=',
             'STRING',
             NULL,
             NULL,
             'COMP',
             NULL,
             NULL,
             0,
             0,
             NULL,
             NULL,
             12,
             NULL,
             NULL);

INSERT INTO FILTER_LAYOUT (FILTER_LAYOUT_ID,
                           OBJECT_TYPE,
                           FIELD_POSITION,
                           IS_REQUIRED,
                           FIELD_NAME,
                           FIELD_LABEL,
                           FIELD_OPERATORS,
                           FIELD_TYPE,
                           VALUE_GENERATOR_CLASS,
                           SELECTION_PAGE_URL,
                           OPERATOR_TYPE,
                           SUB_OBJECT_TYPE,
                           TABLE_NAME,
                           DEFAULT_VALUE,
                           IS_LOCALIZABLE,
                           HIBERNATE_FIELD_NAME,
                           lookup_attribute,
                           screen_type,
                           columnscreen_type,
                           products_available)
     VALUES (SEQ_FILTER_LAYOUT_ID.nextval,
             'CDT_ZONES_FILTER',
             4,
             0,
             'Service Level Type',
             'Service Level Type',
             '=,!=',
             'STRING',
             NULL,
             NULL,
             'COMP',
             NULL,
             NULL,
             0,
             0,
             NULL,
             NULL,
             12,
             NULL,
             NULL);

INSERT INTO FILTER_LAYOUT (FILTER_LAYOUT_ID,
                           OBJECT_TYPE,
                           FIELD_POSITION,
                           IS_REQUIRED,
                           FIELD_NAME,
                           FIELD_LABEL,
                           FIELD_OPERATORS,
                           FIELD_TYPE,
                           VALUE_GENERATOR_CLASS,
                           SELECTION_PAGE_URL,
                           OPERATOR_TYPE,
                           SUB_OBJECT_TYPE,
                           TABLE_NAME,
                           DEFAULT_VALUE,
                           IS_LOCALIZABLE,
                           HIBERNATE_FIELD_NAME,
                           lookup_attribute,
                           screen_type,
                           columnscreen_type,
                           products_available)
     VALUES (SEQ_FILTER_LAYOUT_ID.nextval,
             'CDT_POA_FILTER',
             4,
             0,
             'Service Level Type',
             'Service Level Type',
             '=,!=',
             'STRING',
             NULL,
             NULL,
             'COMP',
             NULL,
             NULL,
             0,
             0,
             NULL,
             NULL,
             12,
             NULL,
             NULL);
             
COMMIT;  

-- DBTicket TE-3946
alter table val_result_hist add app_hook varchar2(20);
alter table val_result_hist add pgm_id varchar2(20);

alter table tmp_val_parm add app_hook varchar2(20);
alter table tmp_val_parm add val_pgm_id varchar2(20);

-- DBTicket TE-4001 PLSQL
@DBScripts/Product/PLSQL_Objects/wm_val_auton_log_result.sql
@DBScripts/Product/PLSQL_Objects/wm_val_add_job_result.sql
@DBScripts/Product/PLSQL_Objects/wm_val_exec_job.sql
@DBScripts/Product/PLSQL_Objects/val_job_result_vw.sql
@DBScripts/Product/PLSQL_Objects/val_job_result_summ_vw.sql

-- DBTicket TE-4009
UPDATE XMENU_ITEM
   SET name = 'Upload UPS Surepost Address and Routing code data'
 WHERE navigation_key = '/te/admin/SurePostAddressDataLoader.jsp';

UPDATE XMENU_ITEM
   SET short_name = 'Upload UPS Surepost Address and Routing code data'
 WHERE navigation_key = '/te/admin/SurePostAddressDataLoader.jsp';

COMMIT;

-- DBTicket TE-4011
UPDATE MESSAGE_MASTER
   SET MSG_CLASS = 'USER'
 WHERE MSG_ID = '1104' AND MSG_MODULE = 'SHIPPING';

COMMIT;

-- DBTicket TE-4041
update PARAM_DEF
set dflt_value = 'false'
where param_def_id = 'USE_FEDEX_SINGLECALL_FOR_TRANSITTIME_AND_RATE';

commit;

-- DBTicket TE-3913

DELETE FROM XMENU_ITEM_PERMISSION
      WHERE XMENU_ITEM_ID IN
               (SELECT xmenu_item_id
                  FROM XMENU_ITEM
                 WHERE (navigation_key LIKE '%ParcelZoneRate.xhtml%'
                        OR navigation_key LIKE '%ParcelTransitTime.xhtml%'));

DELETE FROM XMENU_ITEM_APP
      WHERE XMENU_ITEM_ID IN
               (SELECT xmenu_item_id
                  FROM XMENU_ITEM
                 WHERE (navigation_key LIKE '%ParcelZoneRate.xhtml%'
                        OR navigation_key LIKE '%ParcelTransitTime.xhtml%'));

DELETE FROM XBASE_MENU_ITEM
      WHERE XMENU_ITEM_ID IN
               (SELECT xmenu_item_id
                  FROM XMENU_ITEM
                 WHERE (navigation_key LIKE '%ParcelZoneRate.xhtml%'
                        OR navigation_key LIKE '%ParcelTransitTime.xhtml%'));

DELETE FROM XMENU_ITEM
      WHERE (navigation_key LIKE '%ParcelZoneRate.xhtml%')
            OR (navigation_key LIKE '%ParcelTransitTime.xhtml%');
			
COMMIT;

-- DBTicket TE-4028
ALTER TABLE PARCEL_RATE_CALENDER MODIFY MOD_DATE_TIME DEFAULT SYSDATE;

-- DBTicket TE-3926
MERGE INTO RESOURCES A
     USING (SELECT '/te/parcelRate/ui/ParcelDataZones.xhtml'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/te/parcelRate/ui/ParcelDataZones.xhtml',
               'VPCLZONERATE',
               1,
               NULL);

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CHIO' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/te/parcelRate/ui/ParcelDataZones.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (rp1.resource_id, rp1.permission_code);
       
COMMIT;       

-- DBTicket TE-4048
UPDATE FILTER_LAYOUT SET FIELD_OPERATORS ='=' WHERE OBJECT_TYPE='CDT_MASTER_FILTER' AND FIELD_NAME ='Facility ID';
UPDATE FILTER_LAYOUT SET FIELD_OPERATORS ='=' WHERE OBJECT_TYPE='CDT_RATES_FILTER' AND FIELD_NAME ='Service Level Type';
UPDATE FILTER_LAYOUT SET FIELD_OPERATORS ='=' WHERE OBJECT_TYPE='CDT_RATES_FILTER' AND FIELD_NAME ='Facility ID';
UPDATE FILTER_LAYOUT SET FIELD_OPERATORS ='=' WHERE OBJECT_TYPE='CDT_RATES_FILTER' AND FIELD_NAME ='Carrier Code';
UPDATE FILTER_LAYOUT SET FIELD_OPERATORS ='=' WHERE OBJECT_TYPE='CDT_ZONES_FILTER' AND FIELD_NAME ='Service Level Type';
UPDATE FILTER_LAYOUT SET FIELD_OPERATORS ='=' WHERE OBJECT_TYPE='CDT_ZONES_FILTER' AND FIELD_NAME ='Facility ID';
UPDATE FILTER_LAYOUT SET FIELD_OPERATORS ='=' WHERE OBJECT_TYPE='CDT_ZONES_FILTER' AND FIELD_NAME ='Carrier Code';
UPDATE FILTER_LAYOUT SET FIELD_OPERATORS ='=' WHERE OBJECT_TYPE='CDT_POA_FILTER' AND FIELD_NAME ='Service Level Type';
UPDATE FILTER_LAYOUT SET FIELD_OPERATORS ='=' WHERE OBJECT_TYPE='CDT_POA_FILTER' AND FIELD_NAME ='Facility ID';
UPDATE FILTER_LAYOUT SET FIELD_OPERATORS ='=' WHERE OBJECT_TYPE='CDT_POA_FILTER' AND FIELD_NAME ='Carrier Code';
commit;

-- DBTicket TE-4137

MERGE INTO message_master a
     USING (SELECT '9921003' key, 'ErrorMessage' bundle_name FROM DUAL) b
        ON (a.key = b.key AND a.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (message_master_id,
               msg_id,
               key,
               ils_module,
               msg_module,
               msg,
               bundle_name,
               msg_class,
               msg_type,
               ovride_role,
               log_flag)
       VALUES (
                 seq_message_master_id.NEXTVAL,
                 '9921003',
                 '9921003',
                 'teplanning',
                 NULL,
                 'External Parcel Integration is disabled. Please enable the parameter for company Id: {0}',
                 'ErrorMessage',
                 NULL,
                 'ERROR',
                 NULL,
                 'Y');

MERGE INTO message_master a
     USING (SELECT '9921004' key, 'ErrorMessage' bundle_name FROM DUAL) b
        ON (a.key = b.key AND a.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (message_master_id,
               msg_id,
               key,
               ils_module,
               msg_module,
               msg,
               bundle_name,
               msg_class,
               msg_type,
               ovride_role,
               log_flag)
       VALUES (
                 seq_message_master_id.NEXTVAL,
                 '9921004',
                 '9921004',
                 'teplanning',
                 NULL,
                 'EPI {0} Operation failed because EPI Carrier Code is not defined for Carrier: {1}',
                 'ErrorMessage',
                 NULL,
                 'ERROR',
                 NULL,
                 'Y');


MERGE INTO message_master a
     USING (SELECT '9921005' key, 'ErrorMessage' bundle_name FROM DUAL) b
        ON (a.key = b.key AND a.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (message_master_id,
               msg_id,
               key,
               ils_module,
               msg_module,
               msg,
               bundle_name,
               msg_class,
               msg_type,
               ovride_role,
               log_flag)
       VALUES (
                 seq_message_master_id.NEXTVAL,
                 '9921005',
                 '9921005',
                 'teplanning',
                 NULL,
                 'EPI {0} Operation failed because EPI Service Level Code is not defined for Service Level: {1}',
                 'ErrorMessage',
                 NULL,
                 'ERROR',
                 NULL,
                 'Y');
				
COMMIT;

-- DBTicket TE-4193 PLSQL
@DBScripts/Product/PLSQL_Objects/val_job_result_summ_vw.sql
@DBScripts/Product/PLSQL_Objects/val_job_result_vw.sql
@DBScripts/Product/PLSQL_Objects/val_job_sql_vw.sql
@DBScripts/Product/PLSQL_Objects/val_sql_tag_summ_vw.sql

-- DBTicket TE-4219
MERGE INTO MESSAGE_MASTER A
     USING (SELECT '1109016666' MSG_ID,
                   '1109016666' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
               MSG_ID,
               KEY,
               ILS_MODULE,
               MSG_MODULE,
               MSG,
               BUNDLE_NAME,
               MSG_CLASS,
               MSG_TYPE,
               OVRIDE_ROLE,
               LOG_FLAG,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '1109016666',
               '1109016666',
               'te',
               NULL,
               'The field Substitution string is required.',
               'ErrorMessage',
               NULL,
               NULL,
               NULL,
               NULL,
               CURRENT_TIMESTAMP,
               NULL);

MERGE INTO MESSAGE_MASTER A
     USING (SELECT '1109017777' MSG_ID,
                   '1109017777' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
               MSG_ID,
               KEY,
               ILS_MODULE,
               MSG_MODULE,
               MSG,
               BUNDLE_NAME,
               MSG_CLASS,
               MSG_TYPE,
               OVRIDE_ROLE,
               LOG_FLAG,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '1109017777',
               '1109017777',
               'te',
               NULL,
               'The field Data Source is required.',
               'ErrorMessage',
               NULL,
               NULL,
               NULL,
               NULL,
               CURRENT_TIMESTAMP,
               NULL);
               
COMMIT;               

-- DBTicket TE-4237
---- Not required for oracle

-- DBTicket TE-4275
UPDATE label
   SET VALUE = 'Created Source'
 WHERE key = 'CreatedSource' AND bundle_name = 'manifest_label';

UPDATE label
   SET VALUE = 'Last Updated Dttm'
 WHERE key = 'lastUpdatedDttm' AND bundle_name = 'manifest_label';

UPDATE label
   SET VALUE = 'Currency Code'
 WHERE key = 'CurrencyCode' AND bundle_name = 'manifest_label';

UPDATE label
   SET VALUE = 'Last Updated Source'
 WHERE key = 'LastUpdatedSource' AND bundle_name = 'manifest_label';

UPDATE label
   SET VALUE = 'Ups Pickup Record Number'
 WHERE key = 'UpsPickupRecordNumber' AND bundle_name = 'manifest_label';
 
COMMIT;

---- DBTicket TE-4298
-- insert into param_subgroup (param_group_id,
                            -- param_subgroup_id,
                            -- param_subgroup_name,
                            -- display_order,
                            -- created_dttm,
                            -- last_updated_dttm)
     -- values ('COM_TEPE',
             -- 'EPI',
             -- 'External Parcel Integration (EPI)',
             -- 3,
             -- sysdate,
             -- sysdate);

-- insert into param_def (param_def_id,
                       -- param_group_id,
                       -- param_subgroup_id,
                       -- param_name,
                       -- description,
                       -- is_disabled,
                       -- is_required,
                       -- data_type,
                       -- ui_type,
                       -- min_value,
                       -- is_min_inc,
                       -- max_value,
                       -- is_max_inc,
                       -- dflt_value,
                       -- display_order,
                       -- value_generator_class,
                       -- is_localizable,
                       -- param_category,
                       -- carr_dflt_value,
                       -- bp_dflt_value,
                       -- param_validator_class,
                       -- created_dttm,
                       -- last_updated_dttm,
                       -- param_level)
     -- values (
               -- 'GENERATE_LABEL_TRACKINGNBRS_IN_WAVE_USING_EPI',
               -- 'COM_TEPE',
               -- 'EPI',
               -- 'Generate Labels and Tracking Nbrs during Routing Wave using EPI',
               -- 'Generate Labels and Tracking Nbrs during Routing Wave using EPI',
               -- 0,
               -- 0,
               -- 6,
               -- 'CHECKBOX',
               -- null,
               -- 0,
               -- null,
               -- 0,
               -- 'false',
               -- -1,
               -- null,
               -- 1,
               -- 'EPI',
               -- null,
               -- null,
               -- null,
               -- sysdate,
               -- sysdate,
               -- 0);

-- insert into param_grouping
     -- values ( (select max (param_grouping_id) from param_grouping) + 1,
             -- 'TEParameters',
             -- 'COM_TEPE',
             -- 'EPI');

-- update param_def
   -- set display_order = -1,
       -- dflt_value = 'false',
       -- param_group_id = 'COM_TEPE',
       -- param_subgroup_id = 'EPI',
       -- param_category = 'EPI'
 -- where param_def_id in
          -- ('ENABLE_EXTERNAL_PARCEL_INTEGRATION',
           -- 'USE_VELOCITY_TEMPLATES_FOR_EPI_REQUESTS',
           -- 'CACHE_VELOCITY_TEMPLATES_FOR_EPI_REQUESTS',
           -- 'GENERATE_LABEL_TRACKINGNBRS_IN_WAVE_USING_EPI');
-- commit;

-- DBTicket TE-4319
DELETE FROM ui_menu_item_permission
      WHERE ui_menu_item_id IN
               (SELECT ui_menu_item_id
                  FROM ui_menu_item
                 WHERE ui_menu_screen_id IN (SELECT ui_menu_screen_id
                                               FROM ui_menu_screen
                                              WHERE screen_id = '40000002')
                       AND Display_text IN ('ManifestLpnsList')
                       AND LINK_TYPE = 'JS');

DELETE FROM ui_menu_item
      WHERE ui_menu_item_id IN
               (SELECT ui_menu_item_id
                  FROM ui_menu_item
                 WHERE ui_menu_screen_id IN (SELECT ui_menu_screen_id
                                               FROM ui_menu_screen
                                              WHERE screen_id = '40000002')
                       AND Display_text IN ('ManifestLpnsList')
                       AND LINK_TYPE = 'JS');
                       
COMMIT;

-- DBTicket TE-4364
UPDATE opt_engine
   SET description = 'TE Planning Engine'
 WHERE description = 'Distribution Order Planning Engine'
       AND opt_engine_id = 'TEPE';

COMMIT;

-- DBTicket TE-4383
UPDATE nxt_up_cnt
   SET pfx_field = NULL, pfx_len = 0, chk_digit_len = 0
 WHERE rec_type_id = 'T04' AND user_id = 'WMADMIN';
 
COMMIT; 

-- DBTicket TE-4406

INSERT INTO FEDEX_ACC_OPT_DYNSURGE_MAP ( RESP_SUR_FIELD_ID,ACCESSORIAL_GROUP_CODE) 
VALUES  ('120','HAZ');

commit;

-- DBTicket TE-4424
MERGE INTO NXT_UP_CNT NUC
     USING (SELECT 'CN2' REC_TYPE_ID, '' USER_ID FROM DUAL) NUC1
        ON (NUC.REC_TYPE_ID = NUC1.REC_TYPE_ID AND NUC1.USER_ID IS NULL)
WHEN NOT MATCHED
THEN
   INSERT     (CD_MASTER_ID,
               REC_TYPE_ID,
               PFX_LEN,
               START_NBR,
               END_NBR,
               CURR_NBR,
               NBR_LEN,
               INCR_VALUE,
               NXT_START_NBR,
               NXT_END_NBR,
               CHK_DIGIT_LEN,
               REPEAT_RANGE,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               NXT_UP_CNT_ID,
               HIBERNATE_VERSION,
               FACILITY_ID,
               WHSE,
               REV_ORDER,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES (1,
               'CN2',
               0,
               1,
               999999999,
               538,
               9,
               1,
               1,
               999999999,
               0,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               NXT_UP_CNT_ID_SEQ.NEXTVAL,
               0,
               1,
               '01',
               0,
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

COMMIT;

-- DBTicket TE-4541

update filter_layout set field_operators='BT', operator_type='BETWEEN' where object_type='TE_MANIFEST_HEADER_LIST' and field_name='MANIFEST_HDR.CREATED_DTTM';
update filter_layout set field_operators='BT', operator_type='BETWEEN' where object_type='TE_MANIFEST_HEADER_LIST' and field_name='MANIFEST_HDR.SCHED_PICKUP_DATE';
update filter_layout set field_operators='BT', operator_type='BETWEEN' where object_type='TE_MANIFEST_HEADER_LIST' and field_name='MANIFEST_HDR.ACTUAL_PICKUP_DATE';

commit;

-- DBTicket TE-4535
MERGE INTO LABEL L
     USING (SELECT 'manifest_label' BUNDLE_NAME, 'CreateRoutingLane' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'CreateRoutingLane',
               'Create Routing Lane',
               'manifest_label');
               
COMMIT;

-- DBTicket TE-4568

MERGE INTO MESSAGE_MASTER a
     USING (SELECT '1109019519' KEY, 'ErrorMessage' BUNDLE_NAME FROM DUAL) B
        ON (a.BUNDLE_NAME = b.BUNDLE_NAME AND a.KEY = B.KEY)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
               MSG_ID,
               KEY,
               ILS_MODULE,
               MSG,
               BUNDLE_NAME,
               MSG_CLASS)
       VALUES (
                 SEQ_MESSAGE_MASTER_ID.NEXTVAL,
                 '1109019519',
                 '1109019519',
                 'te',
                 'Record already exists for this combination of Customer, Bill to Facility, Destination Facility, Carrier Type and Ship Via.',
                 'ErrorMessage',
                 'SYSTEM');

COMMIT;

-- DBTicket DB-104
-- insert into param_def (param_def_id,
                       -- param_group_id,
                       -- param_subgroup_id,
                       -- param_name,
                       -- description,
                       -- is_disabled,
                       -- is_required,
                       -- data_type,
                       -- ui_type,
                       -- min_value,
                       -- is_min_inc,
                       -- max_value,
                       -- is_max_inc,
                       -- dflt_value,
                       -- display_order,
                       -- value_generator_class,
                       -- is_localizable,
                       -- param_category,
                       -- carr_dflt_value,
                       -- bp_dflt_value,
                       -- param_validator_class,
                       -- created_dttm,
                       -- last_updated_dttm,
                       -- param_level)
     -- values ('PERFORM_RATE_SHOP_ONLY_DURING_WAVE',
             -- 'COM_TEPE',
             -- 'EPI',
             -- 'Perform Rate Shop Only during wave; No Tracking Nbr Generation',
             -- 'Perform Rate Shop Only during wave; No Tracking Nbr Generation',
             -- 0,
             -- 0,
             -- 6,
             -- 'CHECKBOX',
             -- null,
             -- 0,
             -- null,
             -- 0,
             -- 'false',
             -- -1,
             -- null,
             -- 1,
             -- 'EPI',
             -- null,
             -- null,
             -- null,
             -- sysdate,
             -- sysdate,
             -- 0);

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'Orders selected for EPI',
             'Number of D.O.s selected for EPI',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'Orders that could not be routed through EPI',
             'Number of D.O.s that could not be routed through EPI',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'Orders planned through EPI',
             'Number of D.O.s planned through EPI',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'LPNs that could not be routed through EPI',
             'Number of LPNs that could not be routed through EPI',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'LPNs planned through EPI',
             'Number of LPNs planned through EPI',
             'TE');

commit;

-- DBTicket TE-4290
update label 
set bundle_name='TE' 
where key='Bypass Shipment Planning';
commit;

-- DBTicket DB-160
---- related to DB-104
update param_def
   set display_order = 1
 where param_group_id = 'COM_TEPE' and param_subgroup_id = 'EPI';

delete from company_parameter
 where param_def_id = 'GENERATE_LABEL_TRACKINGNBRS_IN_WAVE_USING_EPI'
   and param_group_id = 'COM_TEPE';

delete from param_spec_value
 where param_def_id = 'GENERATE_LABEL_TRACKINGNBRS_IN_WAVE_USING_EPI'
   and param_group_id = 'COM_TEPE';

delete from vcp_company_parameter
 where param_def_id = 'GENERATE_LABEL_TRACKINGNBRS_IN_WAVE_USING_EPI'
   and param_group_id = 'COM_TEPE';

delete from param_def
 where param_def_id = 'GENERATE_LABEL_TRACKINGNBRS_IN_WAVE_USING_EPI'
   and param_group_id = 'COM_TEPE'
   and param_subgroup_id = 'EPI';

commit;

-- DBTicket TE-4602

MERGE INTO MESSAGE_MASTER A
     USING (SELECT '9920168' MSG_ID,
                   '9920168' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
               MSG_ID,
               KEY,
               ILS_MODULE,
               MSG_MODULE,
               MSG,
               BUNDLE_NAME,
               MSG_CLASS,
               MSG_TYPE,
               OVRIDE_ROLE,
               LOG_FLAG,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               B.MSG_ID,
               B.KEY,
               B.ILS_MODULE,
               'cdt',
               '{0} Domestic Zones imported successfully',
               B.BUNDLE_NAME,
               'SYSTEM',
               'INFORM',
               NULL,
               NULL,
               SYSDATE,
               SYSDATE);

MERGE INTO MESSAGE_MASTER A
     USING (SELECT '9920169' MSG_ID,
                   '9920169' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
               MSG_ID,
               KEY,
               ILS_MODULE,
               MSG_MODULE,
               MSG,
               BUNDLE_NAME,
               MSG_CLASS,
               MSG_TYPE,
               OVRIDE_ROLE,
               LOG_FLAG,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               B.MSG_ID,
               B.KEY,
               B.ILS_MODULE,
               'cdt',
               '{0} Rates imported successfully',
               B.BUNDLE_NAME,
               'SYSTEM',
               'INFORM',
               NULL,
               NULL,
               SYSDATE,
               SYSDATE);

MERGE INTO MESSAGE_MASTER A
     USING (SELECT '9920170' MSG_ID,
                   '9920170' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
               MSG_ID,
               KEY,
               ILS_MODULE,
               MSG_MODULE,
               MSG,
               BUNDLE_NAME,
               MSG_CLASS,
               MSG_TYPE,
               OVRIDE_ROLE,
               LOG_FLAG,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               B.MSG_ID,
               B.KEY,
               B.ILS_MODULE,
               'cdt',
               '{0} Accessorial Rates imported successfully',
               B.BUNDLE_NAME,
               'SYSTEM',
               'INFORM',
               NULL,
               NULL,
               SYSDATE,
               SYSDATE);

MERGE INTO MESSAGE_MASTER A
     USING (SELECT '9920171' MSG_ID,
                   '9920171' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
               MSG_ID,
               KEY,
               ILS_MODULE,
               MSG_MODULE,
               MSG,
               BUNDLE_NAME,
               MSG_CLASS,
               MSG_TYPE,
               OVRIDE_ROLE,
               LOG_FLAG,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES (
                 SEQ_MESSAGE_MASTER_ID.NEXTVAL,
                 B.MSG_ID,
                 B.KEY,
                 B.ILS_MODULE,
                 'cdt',
                 'One or more selected service levels does not have Zone data in the file selected',
                 B.BUNDLE_NAME,
                 'SYSTEM',
                 'INFORM',
                 NULL,
                 NULL,
                 SYSDATE,
                 SYSDATE);

MERGE INTO MESSAGE_MASTER A
     USING (SELECT '9920172' MSG_ID,
                   '9920172' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
               MSG_ID,
               KEY,
               ILS_MODULE,
               MSG_MODULE,
               MSG,
               BUNDLE_NAME,
               MSG_CLASS,
               MSG_TYPE,
               OVRIDE_ROLE,
               LOG_FLAG,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES (
                 SEQ_MESSAGE_MASTER_ID.NEXTVAL,
                 B.MSG_ID,
                 B.KEY,
                 B.ILS_MODULE,
                 'cdt',
                 'One or more selected service levels does not have Rate data in the file selected',
                 B.BUNDLE_NAME,
                 'SYSTEM',
                 'INFORM',
                 NULL,
                 NULL,
                 SYSDATE,
                 SYSDATE);

MERGE INTO MESSAGE_MASTER A
     USING (SELECT '9920173' MSG_ID,
                   '9920173' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
               MSG_ID,
               KEY,
               ILS_MODULE,
               MSG_MODULE,
               MSG,
               BUNDLE_NAME,
               MSG_CLASS,
               MSG_TYPE,
               OVRIDE_ROLE,
               LOG_FLAG,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES (
                 SEQ_MESSAGE_MASTER_ID.NEXTVAL,
                 B.MSG_ID,
                 B.KEY,
                 B.ILS_MODULE,
                 'cdt',
                 'One or more selected service levels does not have Accessorial Rates data in the file selected',
                 B.BUNDLE_NAME,
                 'SYSTEM',
                 'INFORM',
                 NULL,
                 NULL,
                 SYSDATE,
                 SYSDATE);

UPDATE message_master
   SET msg = 'Master Data Import Successful for following files {0}'
 WHERE msg_id = '9920159';
 
COMMIT;

-- DBTicket DB-98
DECLARE
   v_tab_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_tab_count
     FROM user_tables
    WHERE TABLE_NAME = 'CALLOUT_EVENT';

   IF v_tab_count = 0
   THEN
      EXECUTE IMMEDIATE
         'CREATE TABLE CALLOUT_EVENT
(
   CALLOUT_EVENT_ID      NUMBER (9) NOT NULL,
   EVENT_NAME            VARCHAR2 (100) NOT NULL,
   EVENT_CLASS_NAME      VARCHAR2 (200) NOT NULL,
   CREATED_DTTM          TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL,
   CREATED_SOURCE        VARCHAR2 (50),
   LAST_UPDATED_DTTM     TIMESTAMP,
   LAST_UPDATED_SOURCE   VARCHAR2 (50)
)
TABLESPACE TE_BST_DT_TBS';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

DECLARE
   v_constraint_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_constraint_count
     FROM user_constraints
    WHERE constraint_name = 'CALLOUT_EVENT_PK';

   IF v_constraint_count = 0
   THEN
      EXECUTE IMMEDIATE
         'ALTER TABLE CALLOUT_EVENT ADD CONSTRAINT CALLOUT_EVENT_PK PRIMARY KEY (CALLOUT_EVENT_ID) USING INDEX TABLESPACE TE_BST_IDX_TBS';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

DECLARE
   v_constraint_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_constraint_count
     FROM user_constraints
    WHERE constraint_name = 'CALLOUT_EVENT_UK1';

   IF v_constraint_count = 0
   THEN
      EXECUTE IMMEDIATE
         'ALTER TABLE CALLOUT_EVENT ADD CONSTRAINT CALLOUT_EVENT_UK1 UNIQUE (EVENT_NAME) USING INDEX TABLESPACE TE_BST_IDX_TBS';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

DECLARE
   v_constraint_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_constraint_count
     FROM user_constraints
    WHERE constraint_name = 'CALLOUT_EVENT_UK2';

   IF v_constraint_count = 0
   THEN
      EXECUTE IMMEDIATE
         'ALTER TABLE CALLOUT_EVENT ADD CONSTRAINT CALLOUT_EVENT_UK2 UNIQUE (EVENT_CLASS_NAME) USING INDEX TABLESPACE TE_BST_IDX_TBS';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

CREATE OR REPLACE TRIGGER CALLOUT_EVENT_LAST_UPDATED
   BEFORE UPDATE
   ON CALLOUT_EVENT
   FOR EACH ROW
BEGIN
   :NEW.LAST_UPDATED_DTTM := SYSTIMESTAMP;
END;
/

DECLARE
   v_tab_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_tab_count
     FROM user_tables
    WHERE TABLE_NAME = 'CALLOUT_SCRIPT';

   IF v_tab_count = 0
   THEN
      EXECUTE IMMEDIATE
         'CREATE TABLE CALLOUT_SCRIPT
(
   CALLOUT_SCRIPT_ID     NUMBER (9) NOT NULL,
   SCRIPT_NAME           VARCHAR2 (1000) NOT NULL,
   SCRIPT_BODY           CLOB NOT NULL,
   LAST_MODIFIED         DATE NOT NULL,
   CREATED_DTTM          TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL,
   CREATED_SOURCE        VARCHAR2 (50),
   LAST_UPDATED_DTTM     TIMESTAMP,
   LAST_UPDATED_SOURCE   VARCHAR2 (50)
)
TABLESPACE TE_BST_DT_TBS';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

DECLARE
   v_constraint_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_constraint_count
     FROM user_constraints
    WHERE constraint_name = 'CALLOUT_SCRIPT_PK';

   IF v_constraint_count = 0
   THEN
      EXECUTE IMMEDIATE
         'ALTER TABLE CALLOUT_SCRIPT ADD CONSTRAINT CALLOUT_SCRIPT_PK PRIMARY KEY (CALLOUT_SCRIPT_ID) USING INDEX TABLESPACE TE_BST_IDX_TBS';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

DECLARE
   v_constraint_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_constraint_count
     FROM user_constraints
    WHERE constraint_name = 'CALLOUT_SCRIPT_UK1';

   IF v_constraint_count = 0
   THEN
      EXECUTE IMMEDIATE
         'ALTER TABLE CALLOUT_SCRIPT ADD CONSTRAINT CALLOUT_SCRIPT_UK1 UNIQUE (SCRIPT_NAME) USING INDEX TABLESPACE TE_BST_IDX_TBS';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

CREATE OR REPLACE TRIGGER CALLOUT_SCRIPT_LAST_MODIFIED
   BEFORE INSERT OR UPDATE
   ON CALLOUT_SCRIPT
   FOR EACH ROW
BEGIN
   :NEW.LAST_MODIFIED := SYSDATE;
END;
/

CREATE OR REPLACE TRIGGER CALLOUT_SCRIPT_LAST_UPDATED
   BEFORE UPDATE
   ON CALLOUT_SCRIPT
   FOR EACH ROW
BEGIN
   :NEW.LAST_UPDATED_DTTM := SYSTIMESTAMP;
END;
/

DECLARE
   v_tab_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_tab_count
     FROM user_tables
    WHERE TABLE_NAME = 'CALLOUT_MAPPING';

   IF v_tab_count = 0
   THEN
      EXECUTE IMMEDIATE
         'CREATE TABLE CALLOUT_MAPPING
(
   CALLOUT_MAPPING_ID    NUMBER (9, 0) NOT NULL,
   CALLOUT_EVENT_ID      NUMBER (9, 0) NOT NULL,
   FACILITY_ID           NUMBER (9, 0),
   COMPANY_ID            NUMBER (9, 0),
   CALLOUT_SCRIPT_ID     NUMBER (9, 0) NOT NULL,
   CREATED_DTTM          TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL,
   CREATED_SOURCE        VARCHAR2 (50),
   LAST_UPDATED_DTTM     TIMESTAMP,
   LAST_UPDATED_SOURCE   VARCHAR2 (50)
)
TABLESPACE TE_BST_DT_TBS';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

DECLARE
   v_constraint_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_constraint_count
     FROM user_constraints
    WHERE constraint_name = 'CALLOUT_MAPPING_PK';

   IF v_constraint_count = 0
   THEN
      EXECUTE IMMEDIATE
         'ALTER TABLE CALLOUT_MAPPING ADD CONSTRAINT CALLOUT_MAPPING_PK PRIMARY KEY (CALLOUT_MAPPING_ID) USING INDEX TABLESPACE TE_BST_IDX_TBS';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

DECLARE
   v_constraint_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_constraint_count
     FROM user_constraints
    WHERE constraint_name = 'CALLOUT_MAPPING_UK1';

   IF v_constraint_count = 0
   THEN
      EXECUTE IMMEDIATE
         'ALTER TABLE CALLOUT_MAPPING ADD CONSTRAINT CALLOUT_MAPPING_UK1 UNIQUE(CALLOUT_EVENT_ID, FACILITY_ID, COMPANY_ID) USING INDEX TABLESPACE TE_BST_IDX_TBS';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

DECLARE
   v_constraint_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_constraint_count
     FROM user_constraints
    WHERE constraint_name = 'CALLOUT_MAPPING_FK1';

   IF v_constraint_count = 0
   THEN
      EXECUTE IMMEDIATE
         'ALTER TABLE CALLOUT_MAPPING ADD CONSTRAINT CALLOUT_MAPPING_FK1 FOREIGN KEY(CALLOUT_SCRIPT_ID) REFERENCES CALLOUT_SCRIPT (CALLOUT_SCRIPT_ID)';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

DECLARE
   v_constraint_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_constraint_count
     FROM user_constraints
    WHERE constraint_name = 'CALLOUT_MAPPING_FK2';

   IF v_constraint_count = 0
   THEN
      EXECUTE IMMEDIATE
         'ALTER TABLE CALLOUT_MAPPING ADD CONSTRAINT CALLOUT_MAPPING_FK2 FOREIGN KEY(CALLOUT_EVENT_ID) REFERENCES CALLOUT_EVENT (CALLOUT_EVENT_ID)';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

DECLARE
   v_constraint_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_constraint_count
     FROM user_constraints
    WHERE constraint_name = 'CALLOUT_MAPPING_FK3';

   IF v_constraint_count = 0
   THEN
      EXECUTE IMMEDIATE
         'ALTER TABLE CALLOUT_MAPPING ADD CONSTRAINT CALLOUT_MAPPING_FK3 FOREIGN KEY(FACILITY_ID) REFERENCES FACILITY (FACILITY_ID)';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

DECLARE
   v_constraint_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_constraint_count
     FROM user_constraints
    WHERE constraint_name = 'CALLOUT_MAPPING_FK4';

   IF v_constraint_count = 0
   THEN
      EXECUTE IMMEDIATE
         'ALTER TABLE CALLOUT_MAPPING ADD CONSTRAINT CALLOUT_MAPPING_FK4 FOREIGN KEY(COMPANY_ID) REFERENCES COMPANY (COMPANY_ID)';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

DECLARE
   v_index_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_index_count
     FROM user_indexes
    WHERE index_name = 'CALLOUT_MAPPING_FK1_IDX';

   IF v_index_count = 0
   THEN
      EXECUTE IMMEDIATE
         'CREATE INDEX CALLOUT_MAPPING_FK1_IDX ON CALLOUT_MAPPING (CALLOUT_SCRIPT_ID)';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

DECLARE
   v_index_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_index_count
     FROM user_indexes
    WHERE index_name = 'CALLOUT_MAPPING_FK2_IDX';

   IF v_index_count = 0
   THEN
      EXECUTE IMMEDIATE
         'CREATE INDEX CALLOUT_MAPPING_FK2_IDX ON CALLOUT_MAPPING (CALLOUT_EVENT_ID)';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

DECLARE
   v_index_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_index_count
     FROM user_indexes
    WHERE index_name = 'CALLOUT_MAPPING_FK3_IDX';

   IF v_index_count = 0
   THEN
      EXECUTE IMMEDIATE
         'CREATE INDEX CALLOUT_MAPPING_FK3_IDX ON CALLOUT_MAPPING (FACILITY_ID)';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

DECLARE
   v_index_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_index_count
     FROM user_indexes
    WHERE index_name = 'CALLOUT_MAPPING_FK4_IDX';

   IF v_index_count = 0
   THEN
      EXECUTE IMMEDIATE
         'CREATE INDEX CALLOUT_MAPPING_FK4_IDX ON CALLOUT_MAPPING (COMPANY_ID)';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

CREATE OR REPLACE TRIGGER CALLOUT_MAPPING_LAST_UPDATED
   BEFORE UPDATE
   ON CALLOUT_MAPPING
   FOR EACH ROW
BEGIN
   :NEW.LAST_UPDATED_DTTM := SYSTIMESTAMP;
END;
/

DECLARE
   v_seq_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_seq_count
     FROM user_sequences
    WHERE SEQUENCE_NAME = 'CALLOUT_SCRIPT_ID_SEQ';

   IF v_seq_count = 0
   THEN
      EXECUTE IMMEDIATE 'CREATE SEQUENCE CALLOUT_SCRIPT_ID_SEQ
   MINVALUE 1
   MAXVALUE 999999999
   INCREMENT BY 1
   START WITH 1
   CACHE 20
   NOORDER
   NOCYCLE';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

DECLARE
   v_seq_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_seq_count
     FROM user_sequences
    WHERE SEQUENCE_NAME = 'CALLOUT_EVENT_ID_SEQ';

   IF v_seq_count = 0
   THEN
      EXECUTE IMMEDIATE 'CREATE SEQUENCE CALLOUT_EVENT_ID_SEQ
   MINVALUE 1
   MAXVALUE 999999999
   INCREMENT BY 1
   START WITH 1
   CACHE 20
   NOORDER
   NOCYCLE';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

DECLARE
   v_seq_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_seq_count
     FROM user_sequences
    WHERE SEQUENCE_NAME = 'CALLOUT_MAPPING_ID_SEQ';

   IF v_seq_count = 0
   THEN
      EXECUTE IMMEDIATE 'CREATE SEQUENCE CALLOUT_MAPPING_ID_SEQ
   MINVALUE 1
   MAXVALUE 999999999
   INCREMENT BY 1
   START WITH 1
   CACHE 20
   NOORDER
   NOCYCLE';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/
   
-- DBTicket DB-319
MERGE INTO MESSAGE_MASTER A
     USING (SELECT '1109019522' MSG_ID,
                   '1109019522' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
               MSG_ID,
               KEY,
               ILS_MODULE,
               MSG,
               BUNDLE_NAME,
               MSG_CLASS)
       VALUES (
                 SEQ_MESSAGE_MASTER_ID.NEXTVAL,
                 '1109019522',
                 '1109019522',
                 'te',
                 'No matching zones found with service level {0} for the given destination.',
                 'ErrorMessage',
                 'SYSTEM');
                 
COMMIT;

-- DBTicket DB-363
insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'EpiPackageStatus',
             'EPI Package Status',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'EpiCarrierId',
             'EPI Carrier Id',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'EpiPackageId',
             'EPI Package Id',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'EpiManifestId',
             'EPI Manifest Id',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'EpiServiceLevelId',
             'EPI Service Level Id',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'EpiServiceGroup',
             'EPI Service Group',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'EpiShipmentId',
             'EPI Shipment Id',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'EpiParameter',
             'EPI Parameter',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'EpiEnableParam',
             'Enable External Parcel Integration',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'DEMANIFEST',
             'DeManifest',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'MANIFEST',
             'Manifest',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'CLOSEMANIFEST',
             'Close Manifest',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'ROUTINGWAVE',
             'Routing Wave',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'PLANNING_ENGINE',
             'TE Planning Engine',
             'TE');

insert into message_master (message_master_id,
                            msg_id,
                            key,
                            ils_module,
                            msg_module,
                            msg,
                            bundle_name,
                            msg_class,
                            msg_type,
                            ovride_role,
                            log_flag)
     values (seq_message_master_id.nextval,
             '9921006',
             '9921006',
             'te',
             'epi',
             '{0} failed because {1} {2} is invalid',
             'ErrorMessage',
             null,
             'ERROR',
             null,
             'Y');

insert into message_master (message_master_id,
                            msg_id,
                            key,
                            ils_module,
                            msg_module,
                            msg,
                            bundle_name,
                            msg_class,
                            msg_type,
                            ovride_role,
                            log_flag)
     values (seq_message_master_id.nextval,
             '9921007',
             '9921007',
             'te',
             'epi',
             '{0} failed because {1} {2} is not defined',
             'ErrorMessage',
             null,
             'ERROR',
             null,
             'Y');

insert into message_master (message_master_id,
                            msg_id,
                            key,
                            ils_module,
                            msg_module,
                            msg,
                            bundle_name,
                            msg_class,
                            msg_type,
                            ovride_role,
                            log_flag)
     values (seq_message_master_id.nextval,
             '9921008',
             '9921008',
             'te',
             'epi',
             '{0} failed because {1} {2} is not set or enabled',
             'ErrorMessage',
             null,
             'ERROR',
             null,
             'Y');

commit;

-- DBTicket DB-519
insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'ForEpiCarrierService',
             'for EPI Carrier Service',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'EPI',
             'EPI',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'EpiCarrierService',
             'EPI Carrier Service',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'EPI_ROUTING',
             'EPI Routing',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'EPI_ROUTINGWAVE',
             'EPI Routing Wave',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'EPI_RATESHOP',
             'EPI Rate Shop',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'EpiCarrierServiceGroup',
             'EPI Carrier Service Group',
             'TE');

insert into label (label_id,
                   key,
                   value,
                   bundle_name)
     values (seq_label_id.nextval,
             'EpiPackage',
             'EPI Package',
             'TE');

commit;

-- DBTicket DB-576
UPDATE Param_Def
   SET Display_Order = 268, param_subgroup_id = 'CLSHIP'
 WHERE Param_Def_Id = 'max_number_of_threads_for_pld_generation';

UPDATE Param_Def
   SET Display_Order = 269, param_subgroup_id = 'CLSHIP'
 WHERE Param_Def_Id = 'min_number_of_tasks_per_thread_for_pld_generation';

COMMIT;

-- DBTicket DB-631
ALTER TABLE MANIFEST_HDR MODIFY(SHIPMENT_ID NUMBER(10));

ALTER TABLE CONS_ORDER_PATH MODIFY(ERROR_MSG_ID NUMBER(10));

-- DBTicket DB-2274
---- Only for DB2 as it is a table reorg.

-- DBTicket DB-863
insert into param_grouping
(
param_grouping_id, param_grouping_name, param_group_id, param_subgroup_id
)
values
(
(select max(param_grouping_id) + 1 from param_grouping),
'TEParameters',
'COM_TEPE',
'Threads'
) ;

update param_def 
set 
param_category = 'Planning', 
display_order = 1 
where 
param_def_id in ('number_of_threads','min_tasks_per_thread') ;

COMMIT;

-- DBTicket DB-1010
INSERT INTO callout_event (callout_event_id,
                           event_class_name,
                           event_name,
                           created_source)
     VALUES (CALLOUT_EVENT_ID_SEQ.nextval,
             'com.manh.epicommon.event.EPIShipRequestBuilt',
             'EPI Ship Request Built',
             'SEED');

INSERT INTO callout_event (callout_event_id,
                           event_class_name,
                           event_name,
                           created_source)
     VALUES (CALLOUT_EVENT_ID_SEQ.nextval,
             'com.manh.epicommon.event.EPIVoidRequestBuilt',
             'EPI Void Request Built',
             'SEED');

INSERT INTO callout_event (callout_event_id,
                           event_class_name,
                           event_name,
                           created_source)
     VALUES (CALLOUT_EVENT_ID_SEQ.nextval,
             'com.manh.epicommon.event.EPIShipReleaseRequestBuilt',
             'EPI Ship Release Request Built',
             'SEED');

INSERT INTO callout_event (callout_event_id,
                           event_class_name,
                           event_name,
                           created_source)
     VALUES (CALLOUT_EVENT_ID_SEQ.nextval,
             'com.manh.epicommon.event.EPIPrintRequestBuilt',
             'EPI Print Request Built',
             'SEED');

INSERT INTO callout_event (callout_event_id,
                           event_class_name,
                           event_name,
                           created_source)
     VALUES (CALLOUT_EVENT_ID_SEQ.nextval,
             'com.manh.epicommon.event.EPIRateShopRequestBuilt',
             'EPI Rate Shop Request Built',
             'SEED');

INSERT INTO callout_event (callout_event_id,
                           event_class_name,
                           event_name,
                           created_source)
     VALUES (CALLOUT_EVENT_ID_SEQ.nextval,
             'com.manh.epicommon.event.EPIShipResponseReceived',
             'EPI Ship Response Received',
             'SEED');

INSERT INTO callout_event (callout_event_id,
                           event_class_name,
                           event_name,
                           created_source)
     VALUES (CALLOUT_EVENT_ID_SEQ.nextval,
             'com.manh.epicommon.event.EPIVoidResponseReceived',
             'EPI Void Response Received',
             'SEED');

INSERT INTO callout_event (callout_event_id,
                           event_class_name,
                           event_name,
                           created_source)
     VALUES (CALLOUT_EVENT_ID_SEQ.nextval,
             'com.manh.epicommon.event.EPIShipReleaseResponseReceived',
             'EPI Ship Release Response Received',
             'SEED');

INSERT INTO callout_event (callout_event_id,
                           event_class_name,
                           event_name,
                           created_source)
     VALUES (CALLOUT_EVENT_ID_SEQ.nextval,
             'com.manh.epicommon.event.EPIPrintResponseReceived',
             'EPI Print Response Received',
             'SEED');

INSERT INTO callout_event (callout_event_id,
                           event_class_name,
                           event_name,
                           created_source)
     VALUES (CALLOUT_EVENT_ID_SEQ.nextval,
             'com.manh.epicommon.event.EPIRateShopResponseReceived',
             'EPI Rate Shop Response Received',
             'SEED');
             
COMMIT;

-- DBTicket DB-1071
MERGE INTO MESSAGE_MASTER A
     USING (SELECT '9920174' MSG_ID,
                   '9920174' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (Message_Master_Id,
               Msg_Id,
               Key,
               Ils_Module,
               Msg_Module,
               Msg,
               Bundle_Name,
               Msg_Class,
               Msg_Type,
               Ovride_Role,
               Log_Flag,
               Created_Dttm,
               Last_Updated_Dttm)
       VALUES (
                 Seq_Message_Master_Id.NEXTVAL,
                 '9920174',
                 '9920174',
                 'te',
                 NULL,
                 'International or Export service cannot be used for domestic destinations and vice versa',
                 'ErrorMessage',
                 'SYSTEM',
                 'ERROR',
                 NULL,
                 'N',
                 CURRENT_TIMESTAMP,
                 CURRENT_TIMESTAMP);
                 
COMMIT;                 

-- DBTicket DB-1229

MERGE INTO MESSAGE_MASTER A
USING (SELECT  '9921009' MSG_ID,'9921009' KEY, 'te' ILS_MODULE, 'ErrorMessage' BUNDLE_NAME
         FROM DUAL) B
on (A.KEY=B.KEY and  A.BUNDLE_NAME=B.BUNDLE_NAME)
WHEN NOT  MATCHED THEN
INSERT(MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG)
values(SEQ_MESSAGE_MASTER_ID.nextval, '9921009', '9921009', 'te', 'cdt', 'Error importing EPI carrier services info from provider {0}. {1}', 'ErrorMessage', null, 'ERROR', NULL, 'Y');

COMMIT;

-- DBTicket DB-1191
INSERT INTO NXT_UP_CNT (CD_MASTER_ID,
                        REC_TYPE_ID,
                        PFX_FIELD,
                        PFX_LEN,
                        START_NBR,
                        END_NBR,
                        CURR_NBR,
                        NBR_LEN,
                        INCR_VALUE,
                        NXT_START_NBR,
                        NXT_END_NBR,
                        CHK_DIGIT_TYPE,
                        CHK_DIGIT_LEN,
                        REPEAT_RANGE,
                        CREATE_DATE_TIME,
                        MOD_DATE_TIME,
                        USER_ID,
                        NXT_UP_CNT_ID,
                        HIBERNATE_VERSION,
                        FACILITY_ID,
                        WHSE,
                        PFX_TYPE,
                        REV_ORDER,
                        CREATED_DTTM,
                        LAST_UPDATED_DTTM)
     VALUES (NULL,
             '159',
             'E',
             1,
             0,
             999,
             0,
             4,
             1,
             0,
             999,
             NULL,
             0,
             'N',
             SYSDATE,
             SYSDATE,
             NULL,
             NXT_UP_CNT_ID_SEQ.NEXTVAL,
             1,
             NULL,
             '*',
             NULL,
             NULL,
             SYSTIMESTAMP,
             SYSTIMESTAMP);

COMMIT;

-- DBTicket DB-1440 PLSQL
@DBScripts/Product/PLSQL_Objects/wm_parcel_zone_processing.sql

-- DBTicket DB-1609
UPDATE MESSAGE_MASTER
SET MSG = 'EPI {0} failed for {1} {2}: {3}'
WHERE MSG_ID = '9921001';  

MERGE INTO MESSAGE_MASTER A
     USING (SELECT '9921002' MSG_ID,
                   '9921002' KEY,
                   'teplanning' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
               MSG_ID,
               KEY,
               ILS_MODULE,
               MSG_MODULE,
               MSG,
               BUNDLE_NAME,
               MSG_CLASS,
               MSG_TYPE,
               OVRIDE_ROLE,
               LOG_FLAG)
       VALUES (
                 SEQ_MESSAGE_MASTER_ID.NEXTVAL,
                 '9921002',
                 '9921002',
                 'teplanning',
                 'null',
                 'EPI {0} has following warnings for {1} {2}: {3}',
                 'ErrorMessage',
                 NULL,
                 'WARNING',
                 NULL,
                 'Y')
WHEN MATCHED                
THEN  
    UPDATE SET MSG = 'EPI {0} has following warnings for {1} {2}: {3}'
    WHERE MSG_ID = '9921002';
	
COMMIT;	

-- DBTicket DB-1948
DECLARE
      v_col_count       NUMBER(1);
BEGIN    
   SELECT count(*)
   INTO v_col_count
   FROM user_tab_columns
   WHERE COLUMN_NAME = 'OWNER' AND TABLE_NAME='VAL_JOB_SEQ';

   IF v_col_count =0
   THEN
      EXECUTE IMMEDIATE 'ALTER TABLE VAL_JOB_SEQ ADD OWNER VARCHAR2(50)';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

COMMENT ON COLUMN VAL_JOB_SEQ.OWNER IS 'val job owner info';

-- DBTicket DB-1951 PLSQL
@DBScripts/Product/PLSQL_Objects/val_job_result_summ_vw.sql


-- DBTicket DB-1969

MERGE INTO MESSAGE_MASTER M
     USING (SELECT '1109019523' KEY, 'ErrorMessage' BUNDLE_NAME FROM DUAL) D
        ON (M.KEY = D.KEY AND M.BUNDLE_NAME = D.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
               MSG_ID,
               KEY,
               ILS_MODULE,
               MSG_MODULE,
               MSG,
               BUNDLE_NAME,
               MSG_CLASS,
               MSG_TYPE,
               OVRIDE_ROLE,
               LOG_FLAG)
       VALUES (
                 SEQ_MESSAGE_MASTER_ID.NEXTVAL,
                 '1109019523',
                 '1109019523',
                 'te',
                 NULL,
                 'Invalid Action Type – Use M to manifest or D to de-manifest',
                 'ErrorMessage',
                 NULL,
                 NULL,
                 NULL,
                 NULL);

MERGE INTO resources r
     USING (SELECT 'services/rest/te/manifest/scanLPNForManifest' uri,
                   'TE' module,
                   2 uri_type_id,
                   'POST' http_method
              FROM DUAL) b
        ON (    r.uri = b.uri
            AND r.module = b.module
            AND r.uri_type_id = b.uri_type_id
            AND r.http_method = b.http_method)
WHEN NOT MATCHED
THEN
   INSERT     (resource_id,
               uri,
               module,
               uri_type_id,
               http_method)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               'services/rest/te/manifest/scanLPNForManifest',
               'TE',
               2,
               'POST');

MERGE INTO resource_permission rp
     USING (SELECT resource_id, 'MNF' permission_code
              FROM (SELECT resource_id
                      FROM resources
                     WHERE     uri =
                                  'services/rest/te/manifest/scanLPNForManifest'
                           AND module = 'TE')) b
        ON (    rp.resource_id = b.resource_id
            AND rp.permission_code = b.permission_code)
WHEN NOT MATCHED
THEN
   INSERT     (resource_id, permission_code)
       VALUES (
                 (SELECT resource_id
                    FROM resources
                   WHERE     uri =
                                'services/rest/te/manifest/scanLPNForManifest'
                         AND module = 'TE'),
                 'MNF');

COMMIT;

-- DBTicket DB-2031
alter table ec_consumer modify (user_id varchar2(50));
alter table ec_consumer_msg_type modify (user_id varchar2(50));
alter table ec_message modify (user_id varchar2(50));
alter table ec_message_consumer modify (user_id varchar2(50));
alter table edi_xref modify (user_id varchar2(50));
alter table enterprise_master modify (user_id varchar2(50));
alter table hazmat_fedex_gnd modify (user_id varchar2(50));
alter table hazmat_ups_dom modify (user_id varchar2(50));
alter table hazmat_ups_intl modify (user_id varchar2(50));
alter table label_xref modify (user_id varchar2(50));
alter table litrl_xref modify (user_id varchar2(50));
alter table lpn_accessorial modify (created_source varchar2(50));
alter table lpn_accessorial modify (last_updated_source varchar2(50));
alter table manifested_lpn modify (created_source varchar2(50));
alter table manifested_lpn modify (last_updated_source varchar2(50));
alter table manifest_hdr modify (last_updated_source varchar2(50));
alter table manifest_hdr modify (created_source varchar2(50));
alter table msg_log modify (user_id varchar2(50));
alter table parcel_zone_data modify (user_id varchar2(50));
alter table prt_queue_monitor modify (created_source varchar2(50));
alter table prt_queue_monitor modify (last_updated_source varchar2(50));
alter table sec_user modify (user_id varchar2(50));
alter table sure_post_address_data modify (user_id varchar2(50));
alter table ups_emt_upload_rpt modify (user_id varchar2(50));
alter table user_profile modify (user_id varchar2(50));
alter table user_task_grp modify (user_id varchar2(50));
alter table val_job_dtl modify (user_id varchar2(50));
alter table val_job_hdr modify (user_id varchar2(50));
alter table val_result_hist modify (user_id varchar2(50));
alter table val_runtime_binds modify (user_id varchar2(50));
alter table val_sql modify (user_id varchar2(50));
alter table msg_log modify (app_user_id varchar2(50));
alter table msg_log modify (ovride_user_id varchar2(50));
alter table user_profile modify (db_user_id varchar2(50));
alter table user_profile modify (emplye_id varchar2(50));
alter table user_profile modify (db_user_id varchar2(50));
alter table sec_user modify (login_user_id varchar2(50));

-- DBTicket DB-2220
alter table ec_consumer modify (ior varchar2(4000));

-- DBTicket DB-2276
UPDATE label
   SET VALUE = REPLACE (VALUE, 'EPI', '[EPI]')
 WHERE VALUE LIKE '%EPI%' AND BUNDLE_NAME = 'TE';

INSERT INTO LITERAL (LITERAL_ID,
                     FROM_LITERAL,
                     TO_LITERAL,
                     TO_LANG,
                     SCREEN_TYPE_ID,
                     CREATED_SOURCE,
                     CREATED_DTTM,
                     LAST_UPDATED_SOURCE,
                     LAST_UPDATED_DTTM,
                     BU_ID)
     VALUES (SEQ_LITERAL_ID.NEXTVAL,
             'EPI',
             'EPI',
             'en',
             10,
             'EPI',
             current_timestamp,
             'EPI',
             current_timestamp,
             -1);

COMMIT;

-- DBTicket TE-5009
MERGE INTO param_def pd1
     USING (SELECT 'max_rows_to_display_in_mlp' param_def_id,
                   'COM_TEPE' param_group_id
              FROM DUAL) pd2
        ON (pd1.param_def_id = pd2.param_def_id
            AND pd1.param_group_id = pd2.param_group_id)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS)
       VALUES ('max_rows_to_display_in_mlp',
               'COM_TEPE',
               'CLSHIP',
               'Maximum Rows to display in MLP',
               NULL,
               0,
               0,
               7,
               'TEXTBOX',
               3,
               1,
               99999999,
               0,
               500,
               263,
               NULL,
               1,
               'Shipment Planning',
               NULL,
               NULL,
               NULL);

COMMIT;

-- DBTicket TE-5025
MERGE INTO PARAM_DEF P
     USING (SELECT 'trust_store_certificate_name' PARAM_DEF_ID,
                   'COM_TEPE' PARAM_GROUP_ID
              FROM DUAL) D
        ON (P.PARAM_DEF_ID = D.PARAM_DEF_ID
            AND P.PARAM_GROUP_ID = D.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
                       PARAM_GROUP_ID,
                       PARAM_SUBGROUP_ID,
                       PARAM_NAME,
                       DESCRIPTION,
                       IS_DISABLED,
                       IS_REQUIRED,
                       DATA_TYPE,
                       UI_TYPE,
                       MIN_VALUE,
                       IS_MIN_INC,
                       MAX_VALUE,
                       IS_MAX_INC,
                       DFLT_VALUE,
                       DISPLAY_ORDER,
                       VALUE_GENERATOR_CLASS,
                       IS_LOCALIZABLE,
                       PARAM_CATEGORY,
                       CARR_DFLT_VALUE,
                       BP_DFLT_VALUE,
                       PARAM_VALIDATOR_CLASS,
                       CREATED_DTTM,
                       LAST_UPDATED_DTTM)
     VALUES ('trust_store_certificate_name',
             'COM_TEPE',
             'CLSHIP',
             'Trust Store Certificate Name',
             'Trust Store Certificate Name',
             0,
             0,
             4,
             'TEXTBOX',
             NULL,
             0,
             NULL,
             0,
             '1',
             20,
             NULL,
             1,
             'Manifest',
             NULL,
             NULL,
             NULL,
             SYSDATE,
             SYSDATE);

             
MERGE INTO PARAM_DEF P
     USING (SELECT 'trust_store_password' PARAM_DEF_ID,
                   'COM_TEPE' PARAM_GROUP_ID
              FROM DUAL) D
        ON (P.PARAM_DEF_ID = D.PARAM_DEF_ID
            AND P.PARAM_GROUP_ID = D.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT    (PARAM_DEF_ID,
                       PARAM_GROUP_ID,
                       PARAM_SUBGROUP_ID,
                       PARAM_NAME,
                       DESCRIPTION,
                       IS_DISABLED,
                       IS_REQUIRED,
                       DATA_TYPE,
                       UI_TYPE,
                       MIN_VALUE,
                       IS_MIN_INC,
                       MAX_VALUE,
                       IS_MAX_INC,
                       DFLT_VALUE,
                       DISPLAY_ORDER,
                       VALUE_GENERATOR_CLASS,
                       IS_LOCALIZABLE,
                       PARAM_CATEGORY,
                       CARR_DFLT_VALUE,
                       BP_DFLT_VALUE,
                       PARAM_VALIDATOR_CLASS,
                       CREATED_DTTM,
                       LAST_UPDATED_DTTM)
     VALUES ('trust_store_password',
             'COM_TEPE',
             'CLSHIP',
             'Trust Store Password',
             'Trust Store Password',
             0,
             0,
             4,
             'TEXTBOX',
             NULL,
             0,
             NULL,
             0,
             '1',
             21,
             NULL,
             1,
             'Manifest',
             NULL,
             NULL,
             NULL,
             SYSDATE,
             SYSDATE);
COMMIT;

-- DBTicket DB-2417
MERGE INTO MESSAGE_MASTER A
     USING (SELECT '99910471' MSG_ID,
                   '99910471' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (message_master_id,
               msg_id,
               key,
               ils_module,
               msg_module,
               msg,
               bundle_name,
               msg_class,
               msg_type,
               ovride_role,
               log_flag)
       VALUES (seq_message_master_id.NEXTVAL,
               '99910471',
               '99910471',
               'te',
               'epi',
               '{0} failed because {1} {2} is marked for deletion',
               'ErrorMessage',
               NULL,
               'ERROR',
               NULL,
               'Y');
               
COMMIT;

-- DBTicket DB-2597
-----related to WM-83039
MERGE INTO param_def c
     USING (SELECT 'weigh_and_manifest_ui_PandA' param_def_id,
                   'COM_TEPE' param_group_id,
                   'MNFT' param_subgroup_id
              FROM DUAL) c1
        ON (    c.param_def_id = c1.param_def_id
            AND c.param_group_id = c1.param_group_id
            AND c.param_subgroup_id = c1.param_subgroup_id)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM,
               PARAM_LEVEL)
       VALUES ('weigh_and_manifest_ui_PandA',
               'COM_TEPE',
               'MNFT',
               'Weigh and Manifest UI - Send Labels to Print and Apply',
               'Weigh and Manifest UI - Send Labels to Print and Apply',
               0,
               0,
               6,
               'CHECKBOX',
               NULL,
               0,
               NULL,
               0,
               'false',
               260,
               NULL,
               1,
               'Manifesting',
               NULL,
               NULL,
               NULL,
               SYSDATE,
               SYSDATE,
               0);

MERGE INTO param_def c
     USING (SELECT 'weigh_and_manifest_mhe_PandA' param_def_id,
                   'COM_TEPE' param_group_id,
                   'MNFT' param_subgroup_id
              FROM DUAL) c1
        ON (    c.param_def_id = c1.param_def_id
            AND c.param_group_id = c1.param_group_id
            AND c.param_subgroup_id = c1.param_subgroup_id)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM,
               PARAM_LEVEL)
       VALUES ('weigh_and_manifest_mhe_PandA',
               'COM_TEPE',
               'MNFT',
               'Weigh and Manifest MHE - Send Labels to Print and Apply',
               'Weigh and Manifest MHE - Send Labels to Print and Apply',
               0,
               0,
               6,
               'CHECKBOX',
               NULL,
               0,
               NULL,
               0,
               'false',
               260,
               NULL,
               1,
               'Manifesting',
               NULL,
               NULL,
               NULL,
               SYSDATE,
               SYSDATE,
               0);

MERGE INTO company_parameter c
     USING (SELECT 1 tc_company_id,
                   'weigh_and_manifest_ui_PandA' param_def_id,
                   'COM_TEPE' param_group_id
              FROM DUAL) c1
        ON (    c.tc_company_id = c1.tc_company_id
            AND c.param_def_id = c1.param_def_id
            AND c.param_group_id = c1.param_group_id)
WHEN NOT MATCHED
THEN
   INSERT     (TC_COMPANY_ID,
               PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_VALUE,
               ERROR_CODE,
               CARR_PARAM_VALUE,
               Bp_Param_Value,
               Mark_For_Deletion,
               ADDITIONAL_QUALIFIER)
       VALUES (1,
               'weigh_and_manifest_ui_PandA',
               'COM_TEPE',
               'false',
               NULL,
               NULL,
               NULL,
               0,
               -1);

MERGE INTO company_parameter c
     USING (SELECT 1 tc_company_id,
                   'weigh_and_manifest_mhe_PandA' param_def_id,
                   'COM_TEPE' param_group_id
              FROM DUAL) c1
        ON (    c.tc_company_id = c1.tc_company_id
            AND c.param_def_id = c1.param_def_id
            AND c.param_group_id = c1.param_group_id)
WHEN NOT MATCHED
THEN
   INSERT     (TC_COMPANY_ID,
               PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_VALUE,
               ERROR_CODE,
               CARR_PARAM_VALUE,
               Bp_Param_Value,
               Mark_For_Deletion,
               ADDITIONAL_QUALIFIER)
       VALUES (1,
               'weigh_and_manifest_mhe_PandA',
               'COM_TEPE',
               'false',
               NULL,
               NULL,
               NULL,
               0,
               -1);

COMMIT;

-- DBTicket DB-2621
MERGE INTO UI_MENU_SCREEN UMS1
     USING (SELECT SCREEN_ID
              FROM UI_MENU_SCREEN
             WHERE SCREEN_ID = 440000103
            UNION
            SELECT 440000103 FROM DUAL) UMS2
        ON (UMS1.SCREEN_ID = UMS2.SCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (UI_MENU_SCREEN_ID,
               MENU_ID,
               SCREEN_ID,
               PERSONALIZATION_LEVEL,
               PERSONALIZATION_ID,
               USER_ID)
       VALUES (UI_MENU_SCREEN_SEQ.NEXTVAL,
               1,
               440000103,
               'SYSTEM',
               NULL,
               NULL);

MERGE INTO UI_MENU_ITEM A
     USING (SELECT 'PrintEPIIntlReports' AS DISPLAY_TEXT,
                   '#{WMLPNHandlerBackingBean.printIntlReports}' AS LINK,
                   (SELECT MIN (ui_menu_Screen_id)
                      FROM ui_menu_screen
                     WHERE screen_id = '440000103')
                      AS UI_MENU_SCREEN_ID,
                   1 AS MENU_ID
              FROM DUAL) B
        ON (    A.DISPLAY_TEXT = B.DISPLAY_TEXT
            AND A.LINK = B.LINK
            AND A.UI_MENU_SCREEN_ID = B.UI_MENU_SCREEN_ID
            AND A.MENU_ID = B.MENU_ID)
WHEN NOT MATCHED
THEN
   INSERT     (UI_MENU_ITEM_ID,
               UI_MENU_SCREEN_ID,
               MENU_ID,
               IS_DISPLAY,
               DISPLAY_TEXT,
               LINK,
               LINK_TYPE,
               LOCN_FLAG,
               ITEM_SEQ_NBR,
               UI_MENU_COMP_ID,
               IS_MORE,
               AJAX_RERENDER,
               RENDERED)
       VALUES (UI_MENU_ITEM_SEQ.NEXTVAL,
               (SELECT UI_MENU_SCREEN_ID
                  FROM UI_MENU_SCREEN
                 WHERE SCREEN_ID = '440000103'),
               1,
               0,
               'PrintEPIIntlReports',
               '#{WMLPNHandlerBackingBean.printIntlReports}',
               'JSFA',
               7,
               1,
               'dataForm:page-content_footer-panel',
               0,
               'listPanel, errorPanel,ribbonMenuPanel',
               'TRUE');

MERGE INTO UI_MENU_ITEM A
     USING (SELECT 'Cancel' AS DISPLAY_TEXT,
                   '#{WMLPNHandlerBackingBean.canceloLPNAction}' AS LINK,
                   (SELECT MIN (ui_menu_Screen_id)
                      FROM ui_menu_screen
                     WHERE screen_id = '440000103')
                      AS UI_MENU_SCREEN_ID,
                   1 AS MENU_ID
              FROM DUAL) B
        ON (    A.DISPLAY_TEXT = B.DISPLAY_TEXT
            AND A.LINK = B.LINK
            AND A.UI_MENU_SCREEN_ID = B.UI_MENU_SCREEN_ID
            AND A.MENU_ID = B.MENU_ID)
WHEN NOT MATCHED
THEN
   INSERT     (UI_MENU_ITEM_ID,
               UI_MENU_SCREEN_ID,
               MENU_ID,
               IS_DISPLAY,
               DISPLAY_TEXT,
               LINK,
               LINK_TYPE,
               LOCN_FLAG,
               ITEM_SEQ_NBR,
               UI_MENU_COMP_ID,
               IS_MORE,
               AJAX_RERENDER,
               RENDERED)
       VALUES (UI_MENU_ITEM_SEQ.NEXTVAL,
               (SELECT UI_MENU_SCREEN_ID
                  FROM UI_MENU_SCREEN
                 WHERE SCREEN_ID = '440000103'),
               1,
               0,
               'Cancel',
               '#{WMLPNHandlerBackingBean.canceloLPNAction}',
               'JSFA',
               7,
               1,
               'dataForm:page-content_footer-panel',
               0,
               NULL,
               'TRUE');

MERGE INTO RESOURCES A
     USING (SELECT '/wm/inventory/ui/PrintIntlReports.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/wm/inventory/ui/PrintIntlReports.xhtml',
               'INVNMGMT',
               1,
               NULL);

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'WMALPN' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/wm/inventory/ui/PrintIntlReports.xhtml') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (rp1.resource_id, rp1.permission_code);

INSERT INTO Lrf_Report_Def (Report_Def_Id_O,
                            Description,
                            Dbname,
                            TYPE,
                            Viewperm,
                            Editperm,
                            Category,
                            Subcategory,
                            Report_Def_Id,
                            Report_Name,
                            Report_File_Name,
                            Priority,
                            Reset_Count,
                            Reset_Interval,
                            Retry_Count,
                            Retry_Interval,
                            Datasource)
     VALUES ('03',
             'EPI Intl Reports',
             'DB2DB',
             'Jasper',
             'AAPA',
             'AAPA',
             'WM',
             'OUTBOUND',
             seq_lrf_report_def_id.NEXTVAL,
             'EPIIntlReports',
             'EPIIntlReports',
             -1,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL);

INSERT INTO LRF_REPORT_DEF_LAYOUT (REPORT_DEF_LAYOUT_ID,
                                   FIELD_POSITION,
                                   IS_REQUIRED,
                                   FIELD_NAME,
                                   FIELD_LABEL,
                                   FIELD_OPERATORS,
                                   FIELD_TYPE,
                                   VALUE_GENERATOR_CLASS,
                                   SELECTION_PAGE_URL,
                                   OPERATOR_TYPE,
                                   SUB_OBJECT_TYPE,
                                   TABLE_NAME,
                                   DEFAULT_VALUE,
                                   IS_LOCALIZABLE,
                                   SEND_PARAMETER,
                                   HIBERNATE_FIELD_NAME,
                                   LOOKUP_ATTRIBUTE,
                                   MAX_DATE_DIFF,
                                   REPORT_DEF_ID,
                                   CUST_VALID_CLASS,
                                   HIDDEN_PARAM_CLASS,
                                   DATA_CONFIG_QUERY,
                                   LOOKUP_CONFIG_QUERY,
                                   RENDERED)
     VALUES (
               (lrf_report_def_layout_id_seq.NEXTVAL),
               0,
               1,
               'LPN.TC_LPN_ID',
               'LPN ID',
               '=',
               'STRING',
               NULL,
               NULL,
               'REPORT_TEXT_ITEM',
               NULL,
               NULL,
               NULL,
               0,
               0,
               'tcLpnId',
               NULL,
               NULL,
               (SELECT max(Report_Def_Id)
                  FROM Lrf_Report_Def
                 WHERE     Report_Name = 'EPIIntlReports'
                       AND Category = 'WM'
                       AND Subcategory = 'OUTBOUND'),
               NULL,
               NULL,
               NULL,
               NULL,
               NULL);

INSERT INTO LRF_REPORT_DEF_LAYOUT (REPORT_DEF_LAYOUT_ID,
                                   FIELD_POSITION,
                                   IS_REQUIRED,
                                   FIELD_NAME,
                                   FIELD_LABEL,
                                   FIELD_OPERATORS,
                                   FIELD_TYPE,
                                   VALUE_GENERATOR_CLASS,
                                   SELECTION_PAGE_URL,
                                   OPERATOR_TYPE,
                                   SUB_OBJECT_TYPE,
                                   TABLE_NAME,
                                   DEFAULT_VALUE,
                                   IS_LOCALIZABLE,
                                   SEND_PARAMETER,
                                   HIBERNATE_FIELD_NAME,
                                   LOOKUP_ATTRIBUTE,
                                   MAX_DATE_DIFF,
                                   REPORT_DEF_ID,
                                   CUST_VALID_CLASS,
                                   HIDDEN_PARAM_CLASS,
                                   DATA_CONFIG_QUERY,
                                   LOOKUP_CONFIG_QUERY,
                                   RENDERED)
     VALUES (
               (lrf_report_def_layout_id_seq.NEXTVAL),
               0,
               1,
               'FACILITY.WHSE',
               'Warehouse',
               '=',
               'STRING',
               NULL,
               NULL,
               'REPORT_TEXT_ITEM',
               NULL,
               NULL,
               NULL,
               0,
               0,
               'whse',
               NULL,
               NULL,
               (SELECT max(Report_Def_Id)
                  FROM Lrf_Report_Def
                 WHERE     Report_Name = 'EPIIntlReports'
                       AND Category = 'WM'
                       AND Subcategory = 'OUTBOUND'),
               NULL,
               NULL,
               NULL,
               NULL,
               NULL);
			   
COMMIT;			   

-- DBTicket DB-2683
UPDATE UI_MENU_ITEM
   SET DISPLAY_TEXT = 'PrintEPIDoc' 
 WHERE LINK = '#{WMLPNHandlerBackingBean.printIntlReports}';
 
COMMIT;

-- DBTicket DB-2814
DELETE FROM VCP_COMPANY_PARAMETER
      WHERE PARAM_DEF_ID IN
               ('ENABLE_EXTERNAL_PARCEL_INTEGRATION',
                'PERFORM_RATE_SHOP_ONLY_DURING_WAVE')
            AND PARAM_GROUP_ID = 'COM_TEPE';

DELETE FROM PARAM_SPEC_VALUE
      WHERE PARAM_DEF_ID IN
               ('ENABLE_EXTERNAL_PARCEL_INTEGRATION',
                'PERFORM_RATE_SHOP_ONLY_DURING_WAVE')
            AND PARAM_GROUP_ID = 'COM_TEPE';

DELETE FROM COMPANY_PARAMETER
      WHERE PARAM_DEF_ID IN
               ('ENABLE_EXTERNAL_PARCEL_INTEGRATION',
                'PERFORM_RATE_SHOP_ONLY_DURING_WAVE')
            AND PARAM_GROUP_ID = 'COM_TEPE';

DELETE FROM PARAM_DEF
      WHERE PARAM_DEF_ID IN
               ('ENABLE_EXTERNAL_PARCEL_INTEGRATION',
                'PERFORM_RATE_SHOP_ONLY_DURING_WAVE')
            AND PARAM_GROUP_ID = 'COM_TEPE';
            
COMMIT;

-- DBTicket DB-2836
DELETE FROM VCP_COMPANY_PARAMETER
      WHERE PARAM_DEF_ID IN
               ('USE_VELOCITY_TEMPLATES_FOR_EPI_REQUESTS',
                'CACHE_VELOCITY_TEMPLATES_FOR_EPI_REQUESTS')
            AND PARAM_GROUP_ID = 'COM_TEPE';

DELETE FROM PARAM_SPEC_VALUE
      WHERE PARAM_DEF_ID IN
               ('USE_VELOCITY_TEMPLATES_FOR_EPI_REQUESTS',
                'CACHE_VELOCITY_TEMPLATES_FOR_EPI_REQUESTS')
            AND PARAM_GROUP_ID = 'COM_TEPE';

DELETE FROM COMPANY_PARAMETER
      WHERE PARAM_DEF_ID IN
               ('USE_VELOCITY_TEMPLATES_FOR_EPI_REQUESTS',
                'CACHE_VELOCITY_TEMPLATES_FOR_EPI_REQUESTS')
            AND PARAM_GROUP_ID = 'COM_TEPE';

DELETE FROM PARAM_DEF
      WHERE PARAM_DEF_ID IN
               ('USE_VELOCITY_TEMPLATES_FOR_EPI_REQUESTS',
                'CACHE_VELOCITY_TEMPLATES_FOR_EPI_REQUESTS')
            AND PARAM_GROUP_ID = 'COM_TEPE';

COMMIT;

-- DBTicket DB-2677
CREATE SEQUENCE SEQ_EPI_END_OF_DAY_ID MINVALUE 1
                                      MAXVALUE 999999999999999999
                                      START WITH 1
                                      INCREMENT BY 1
                                      NOCACHE
                                      NOCYCLE;

CREATE TABLE EPI_END_OF_DAY
(
   EPI_END_OF_DAY_ID      NUMBER (18),
   EPI_MANIFEST_ID        VARCHAR2 (100),
   TC_COMPANY_ID          NUMBER (9) NOT NULL,
   CARRIER_ID             NUMBER (12) NOT NULL,
   O_FACILITY_ID          NUMBER (10) NOT NULL,
   O_FACILITY_ALIAS_ID    VARCHAR2 (16),
   EOD_POLL_COUNT         NUMBER (5),
   EOD_PACKAGE_COUNT      NUMBER (12),
   EOD_STATUS             VARCHAR2 (15) NOT NULL,
   EPI_PROFILE            VARCHAR2 (100) NOT NULL,
   EOD_KEY_1              VARCHAR2 (100),
   EOD_KEY_2              VARCHAR2 (100),
   EOD_KEY_3              VARCHAR2 (100),
   EOD_OUTPUT_TYPE_LIST   VARCHAR2 (200),
   CREATED_SOURCE         VARCHAR2 (100) NOT NULL,
   LAST_UPDATED_SOURCE    VARCHAR2 (100),
   EOD_REQUEST_DTTM       TIMESTAMP (6) NOT NULL,
   EOD_CLOSED_DTTM        TIMESTAMP (6),
   CREATED_DTTM           TIMESTAMP (6) DEFAULT SYSTIMESTAMP,
   LAST_UPDATED_DTTM      TIMESTAMP (6)
)
TABLESPACE TE_BST_DT_TBS;

CREATE UNIQUE INDEX UK_EPI_END_OF_DAY_ID_IDX
   ON EPI_END_OF_DAY (EPI_END_OF_DAY_ID)
   TABLESPACE TE_BST_IDX_TBS;

CREATE INDEX EPI_EOD_COMPANY_FAC_STATUS_IDX
   ON EPI_END_OF_DAY (TC_COMPANY_ID, O_FACILITY_ID, EOD_STATUS)
   TABLESPACE TE_BST_IDX_TBS;

CREATE INDEX EPI_EOD_COMPANY_STATUS_IDX
   ON EPI_END_OF_DAY (TC_COMPANY_ID, EOD_STATUS)
   TABLESPACE TE_BST_IDX_TBS;

ALTER TABLE EPI_END_OF_DAY
   ADD CONSTRAINT PK_EPI_END_OF_DAY_ID PRIMARY KEY (EPI_END_OF_DAY_ID)
       USING INDEX TABLESPACE TE_BST_IDX_TBS;

ALTER TABLE EPI_END_OF_DAY
   ADD CONSTRAINT FK_ENDOFDAY_CARRIER_ID FOREIGN KEY (CARRIER_ID)
           REFERENCES CARRIER_CODE (CARRIER_ID);

DECLARE
   v_index_count   NUMBER (1);
   v_priv_count    NUMBER (1);
   lv_schema       VARCHAR2 (50);
BEGIN
   SELECT COUNT (*)
     INTO v_index_count
     FROM user_constraints
    WHERE constraint_name = 'FK_ENDOFDAY_COMPANY_ID';

   SELECT COUNT (*)
     INTO v_priv_count
     FROM user_tables
    WHERE table_name = 'COMPANY';

   IF v_index_count = 0
   THEN
      IF v_priv_count = 1
      THEN
         EXECUTE IMMEDIATE
            'ALTER TABLE EPI_END_OF_DAY ADD CONSTRAINT FK_ENDOFDAY_COMPANY_ID FOREIGN KEY (TC_COMPANY_ID) REFERENCES COMPANY (COMPANY_ID)';
      ELSE
         BEGIN
            SELECT DISTINCT owner
              INTO lv_schema
              FROM user_tab_privs
             WHERE table_name = 'COMPANY';
         EXCEPTION
            WHEN OTHERS
            THEN
               v_index_count := 1;
               DBMS_OUTPUT.PUT_LINE (SQLERRM);
         END;

         EXECUTE IMMEDIATE
               'ALTER TABLE EPI_END_OF_DAY ADD CONSTRAINT FK_ENDOFDAY_COMPANY_ID FOREIGN KEY (TC_COMPANY_ID) REFERENCES '
            || LV_SCHEMA
            || '.COMPANY (COMPANY_ID)';
      END IF;
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (SQLERRM);
END;
/

ALTER TABLE EPI_END_OF_DAY
   ADD CONSTRAINT FK_ENDOFDAY_FACILITY_ID FOREIGN KEY (O_FACILITY_ID)
           REFERENCES FACILITY (FACILITY_ID);

COMMENT ON TABLE EPI_END_OF_DAY IS
   'new table for End of Day process aka close manifest';
COMMENT ON COLUMN EPI_END_OF_DAY.EPI_END_OF_DAY_ID IS 'PRIMARY KEY';
COMMENT ON COLUMN EPI_END_OF_DAY.EPI_MANIFEST_ID IS
   'END OF DAY ID FOR EXTERNAL SYSTEM';
COMMENT ON COLUMN EPI_END_OF_DAY.TC_COMPANY_ID IS
   'BUSINESS UNIT REQUESTING END OF DAY';
COMMENT ON COLUMN EPI_END_OF_DAY.CARRIER_ID IS
   'CARRIER REQUESTING END OF DAY';
COMMENT ON COLUMN EPI_END_OF_DAY.O_FACILITY_ID IS
   'ORIGIN FACILITY ID TRIGGERING END OF DAY PROCESS';
COMMENT ON COLUMN EPI_END_OF_DAY.O_FACILITY_ALIAS_ID IS
   'ORIGIN FACILITY TRIGGERING END OF DAY PROCESS';
COMMENT ON COLUMN EPI_END_OF_DAY.EOD_POLL_COUNT IS
   'NUMER OF TIMES REQUEST SENT TO EXTERNAL SYSTEM BEFORE END OF DAY PROCESS IS COMPLETED';
COMMENT ON COLUMN EPI_END_OF_DAY.EOD_PACKAGE_COUNT IS
   'TOTAL PACKAGES PROCESSED AS PART OF END OF DAY ID';
COMMENT ON COLUMN EPI_END_OF_DAY.EOD_STATUS IS
   'LATEST STATUS OF END OF DAY REQUEST';
COMMENT ON COLUMN EPI_END_OF_DAY.EPI_PROFILE IS
   'USER ACCOUNT REQUESTING END OF DAY PROCESS';
COMMENT ON COLUMN EPI_END_OF_DAY.EOD_KEY_1 IS
   'PLACEHOLDER FOR EXTERNAL INFORMATION';
COMMENT ON COLUMN EPI_END_OF_DAY.EOD_KEY_2 IS
   'PLACEHOLDER FOR EXTERNAL INFORMATION';
COMMENT ON COLUMN EPI_END_OF_DAY.EOD_KEY_3 IS
   'PLACEHOLDER FOR EXTERNAL INFORMATION';
COMMENT ON COLUMN EPI_END_OF_DAY.EOD_OUTPUT_TYPE_LIST IS
   'INFORMATION ABOUT DOCUMENTS REQUESTED AS PART OF END OF DAY';
COMMENT ON COLUMN EPI_END_OF_DAY.CREATED_SOURCE IS
   'SOURCE INITIATING END OF DAY PROCESS';
COMMENT ON COLUMN EPI_END_OF_DAY.LAST_UPDATED_SOURCE IS
   'SOURCE FINISHING END OF DAY PROCESS';
COMMENT ON COLUMN EPI_END_OF_DAY.EOD_REQUEST_DTTM IS
   'DATE WHEN END OF DAY PROCESS IS INITIATED';
COMMENT ON COLUMN EPI_END_OF_DAY.EOD_CLOSED_DTTM IS
   'DATE WHEN END OF DAY PROCESS IS FINISHED';
COMMENT ON COLUMN EPI_END_OF_DAY.CREATED_DTTM IS 'CREATION DATE.';
COMMENT ON COLUMN EPI_END_OF_DAY.LAST_UPDATED_DTTM IS 'UPDATION DATE.';

-- DBTicket DB-2648
comment on table COMPANY_OPT_ENGINE is 'list of the engines to which this shipper has access';
comment on table CONS_RUN is 'Routing Run IDs';
comment on table CONS_RUN_DTL is 'Routing Run Details are added';
comment on table CONS_TEMPLATE is ' Routing Run Templates created';
comment on table EDI_XREF is 'Table for configuring EDI Xref';
comment on table HAZMAT_FEDEX_GND is ' Table to include Hazmat Classes for Fedex';
comment on table HAZMAT_UPS_DOM is ' Table to include Hazmat Classes for UPS Domestic';
comment on table HAZMAT_UPS_INTL is ' Table to include Hazmat Classes for UPS International';
comment on table MSG_LOG is 'Used to log Error Messages If Non-interactive of log_flag enabled for messages';
comment on table OPT_ENGINE is ' Planning Engines';
comment on table OPT_ENGINE_TYPE is 'list of the engines to which the shipper has access';
comment on table OPT_PARAM is 'Routing Wave Params of each of the Routing Wave Parameter created';
comment on table OPT_PARAM_LIST is 'Routing Wave Parameters Created List';
comment on table PARCEL_RATE_CALENDER is 'Parcel Carriers Rates List';
comment on table PARCEL_RATE_DATA is ' Parcel Carriers Rates for each Service to operate in each zone';
comment on table PARCEL_TRANSIT_TIME is ' Parcel Carriers Transit times for each of the service';
comment on table PARCEL_ZONE_DATA is ' Parcel Carrier Zones info';
comment on table SHIP_VIA_EXCLUDE is ' ShipVias to exclude during Hazmat Shipping';
comment on table SURE_POST_ADDRESS_DATA is 'Table to indicate Surepost Addresses';
comment on table UPS_EMT_UPLOAD_RPT is ' UPS Upload info logged here';

-- DBTicket DB-3072
MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'HighValueSummaryReport' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'HighValueSummaryReport',
               'High Value Summary Report',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'SpecialInstructions' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'SpecialInstructions',
               'Special Instructions',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'InstructionsHdr' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'InstructionsHdr',
                 'This shipment requires that you follow the special procedures below:',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Instructions1' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'Instructions1',
                 'You must give this package to a UPS Driver or UPS Customer Center representative.  The UPS Store, other third party retailers (including Authorized Shipping Outlets), and UPS Drop Boxes are not acceptable UPS locations for this package.',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Instructions2' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'Instructions2',
                 'Two copies of this receipt will be printed. Provide one copy to UPS and ensure the other copy is signed by the UPS Driver or a UPS Customer Center representative and returned to you. The signed copy of this receipt is your proof that UPS has accepted the package(s), and will be required to submit a claim.',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Instructions3' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Instructions3',
               'Confirm the shipment is properly packed:',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Instructions3_1' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Instructions3_1',
               '- Packaging and tape are in good condition',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Instructions3_2' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Instructions3_2',
               '- Contents do not move when the package is moved or shaken',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Instructions3_3' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Instructions3_3',
               '- Label is legible and properly fastened to the package',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Responsibility' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Responsibility',
               'Responsibility for Loss or Damage',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ResponsibilityDesc1' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'ResponsibilityDesc1',
                 'UPS’s liability for loss or damage to each domestic package or international shipment is limited to $100 without a declaration of value.',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ResponsibilityDesc2' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'ResponsibilityDesc2',
                 'Unless a greater value is recorded in the declared value field of the UPS shipping system used, the shipper agrees that the released value of each package covered by this receipt is no greater than $100, which is a reasonable value under the circumstances surrounding the transportation. To increase UPS'||''''||'s'||' limit of liability for loss or damage, a shipper may declare a higher value and pay an additional charge.',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ResponsibilityDesc3' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'ResponsibilityDesc3',
                 'See the UPS Tariff/Terms and Conditions of Service ("UPS Terms") at www.ups.com for UPS'||''''||'s'||' liability limits, maximum declared values, and other terms of service. UPS does not accept for transportation and shippers are prohibited from shipping, packages with a value of more than $50,000. The only exception to the $50,000 per package limit is for a package eligible for the Enhanced Maximum Declared Value of $70,000 per package, as set forth in the UPS Terms.',
                 'Languages');

-- MERGE INTO LABEL L
     -- USING (SELECT 'Languages' BUNDLE_NAME, 'ResponsibilityDesc4' KEY
              -- FROM DUAL) B
        -- ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
-- WHEN NOT MATCHED
-- THEN
   -- INSERT     (L.LABEL_ID,
               -- L.KEY,
               -- L.VALUE,
               -- L.BUNDLE_NAME)
       -- VALUES (
                 -- SEQ_LABEL_ID.NEXTVAL,
                 -- 'ResponsibilityDesc4',
                 -- 'A package is eligible only if it meets the following requirements. The package must be (i) a domestic shipment; (ii) tendered pursuant to shipper'||''''||'s'|| ' Scheduled Pickup Service;(iii) a UPS Next Day Air delivery service is the service level selected;(iv) processed for shipment using a UPS Shipping System (declarations of value on paper Source Documents are not eligible for Enhanced Maximum Declared Value);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        AND (v) does NOT contain hazardous material OR A Perishable Commodity.',
                 -- 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ResponsibilityDesc5' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'ResponsibilityDesc5',
                 'Claims not made within nine months after delivery of the package (sixty days for international shipments), or in the case of failure to make delivery, nine months after a reasonable time for delivery has elapsed (sixty days for international shipments), shall be deemed waived.',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ResponsibilityDesc6' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'ResponsibilityDesc6',
                 'The entry of a C.O.D. amount is not a declaration of value for carriage purposes. All checks or other negotiable instruments tendered in payment of C.O.D. will be accepted by UPS at shipper'||''''||'s'|| ' risk. UPS shall not be liable for any special, incidental, or consequential damages. All shipments are subject to the terms and conditions contained in the UPS Terms, which can be found at www.ups.com.',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'UPSAccountNumber' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UPSAccountNumber',
               'UPS Account Number:',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ShipmentPickupNumber' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ShipmentPickupNumber',
               'Shipment Record Number/Pickup Record Number:',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Tracking#' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Tracking#',
               'Tracking #',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ShipmentID#' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ShipmentID#',
               'Shipment ID #',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'DeclaredValue' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'DeclaredValue',
               'Declared Value',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'TotalHighValuePackages' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'TotalHighValuePackages',
               'Total Number of High Value Packages =',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'UPSDriverRepresentative' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UPSDriverRepresentative',
               'UPS Driver/Representative:',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'footer' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'footer',
                 'Scan the package and sign one copy of this receipt and return it to the customer. The second copy of the receipt should accompany the package.',
                 'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ReceivedBy' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ReceivedBy',
               'Received by: ',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Time:' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Time:',
               'Time: ',
               'Languages');

MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'Packages' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Packages',
               'Packages: ',
               'Languages');
               
COMMIT;

-- DBTicket DB-3153
MERGE INTO LABEL L
     USING (SELECT 'Languages' BUNDLE_NAME, 'ResponsibilityDesc4' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'ResponsibilityDesc4',
                    'A package is eligible only if it meets the following requirements. The package must be (i) a domestic shipment; (ii) tendered pursuant to shipper'
                 || ''''
                 || 's'
                 || ' Scheduled Pickup Service;(iii) a UPS Next Day Air delivery service is the service level selected;(iv) processed for shipment using a UPS Shipping System (declarations of value on paper Source Documents are not eligible for Enhanced Maximum Declared Value); AND (v) does NOT contain hazardous material OR A Perishable Commodity.',
                 'Languages');
                 
COMMIT;

-- DBTicket DB-3149

MERGE INTO BUNDLE_META_DATA A
     USING (SELECT 'EPI' AS BUNDLE_NAME FROM DUAL) B
        ON (A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (BUNDLE_META_DATA_ID,
               BUNDLE_NAME,
               DESCRIPTION,
               DEF_SCREEN_TYPE_ID,
               DEF_APP_ID,
               HINT,
               IS_ACTIVE,
               IS_LABEL_BUNDLE,
               DEF_BU_ID)
       VALUES (seq_bundle_meta_data_id.NEXTVAL,
               'EPI',
               'EPI application labels',
               10,
               NULL,
               NULL,
               1,
               1,
               -1);

UPDATE label
   SET BUNDLE_NAME = 'EPI'
 WHERE key = 'EPI_MANIFEST_ID' AND BUNDLE_NAME = 'TE';

UPDATE label
   SET BUNDLE_NAME = 'EPI'
 WHERE key = 'EPI_RATESHOP' AND BUNDLE_NAME = 'TE';

UPDATE label
   SET BUNDLE_NAME = 'EPI'
 WHERE key = 'EPI_ROUTING' AND BUNDLE_NAME = 'TE';

UPDATE label
   SET BUNDLE_NAME = 'EPI'
 WHERE key = 'EPI_ROUTINGWAVE' AND BUNDLE_NAME = 'TE';

UPDATE label
   SET BUNDLE_NAME = 'EPI'
 WHERE key = 'EpiCarrierId' AND BUNDLE_NAME = 'TE';

UPDATE label
   SET BUNDLE_NAME = 'EPI'
 WHERE key = 'EpiCarrierService' AND BUNDLE_NAME = 'TE';

UPDATE label
   SET BUNDLE_NAME = 'EPI'
 WHERE key = 'EpiCarrierServiceGroup' AND BUNDLE_NAME = 'TE';

UPDATE label
   SET BUNDLE_NAME = 'EPI'
 WHERE key = 'EpiEnableParam' AND BUNDLE_NAME = 'TE';

UPDATE label
   SET BUNDLE_NAME = 'EPI'
 WHERE     key = 'EpiManifestId'
       AND VALUE = '[EPI] Manifest Id'
       AND BUNDLE_NAME = 'TE';

UPDATE label
   SET BUNDLE_NAME = 'EPI'
 WHERE key = 'EpiPackage' AND BUNDLE_NAME = 'TE';

UPDATE label
   SET BUNDLE_NAME = 'EPI'
 WHERE     key = 'EpiPackageId'
       AND VALUE = '[EPI] Package Id'
       AND BUNDLE_NAME = 'TE';

UPDATE label
   SET BUNDLE_NAME = 'EPI'
 WHERE     key = 'EpiPackageStatus'
       AND VALUE = '[EPI] Package Status'
       AND BUNDLE_NAME = 'TE';

UPDATE label
   SET BUNDLE_NAME = 'EPI'
 WHERE key = 'EpiParameter' AND BUNDLE_NAME = 'TE';

UPDATE label
   SET BUNDLE_NAME = 'EPI'
 WHERE     key = 'EpiServiceGroup'
       AND VALUE = '[EPI] Service Group'
       AND BUNDLE_NAME = 'TE';

UPDATE label
   SET BUNDLE_NAME = 'EPI'
 WHERE key = 'EpiServiceLevelId' AND BUNDLE_NAME = 'TE';

UPDATE label
   SET BUNDLE_NAME = 'EPI'
 WHERE     key = 'EpiShipmentId'
       AND VALUE = '[EPI] Shipment Id'
       AND BUNDLE_NAME = 'TE';

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'epiEndOfDayId' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'epiEndOfDayId',
               '[EPI] End of Day Id',
               'EPI');

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'epiManifestId' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'epiManifestId',
               '[EPI] Manifest Id',
               'EPI');

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'tcCompanyId' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'tcCompanyId',
               'Business Unit',
               'EPI');

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'carrierId' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'carrierId',
               'Carrier Name',
               'EPI');

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'oFacilityId' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'oFacilityId',
               'Facility Id',
               'EPI');

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'oFacilityAliasId' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'oFacilityAliasId',
               'Facility Id',
               'EPI');

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'eodPackageCount' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'eodPackageCount',
               'Package Count',
               'EPI');

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'eodStatus' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'eodStatus',
               'Status',
               'EPI');

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'epiProfile' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'epiProfile',
               '[EPI] Profile',
               'EPI');

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'eodKey1' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'eodKey1',
               'EOD Key 1',
               'EPI');

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'eodKey2' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'eodKey2',
               'EOD Key 2',
               'EPI');

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'eodKey3' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'eodKey3',
               'EOD Key 3',
               'EPI');

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'eodOutputTypeList' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'eodOutputTypeList',
               'EOD Output Type List',
               'EPI');

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'createdSource' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'createdSource',
               'Created Source',
               'EPI');

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'lastUpdatedSource' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'lastUpdatedSource',
               'Last Updated Source',
               'EPI');

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'eodRequestDttm' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'eodRequestDttm',
               'EOD Request DTTM',
               'EPI');

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'eodClosedDttm' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'eodClosedDttm',
               'EOD Closed DTTM',
               'EPI');

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'createdDttm' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'createdDttm',
               'Created DTTM',
               'EPI');

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'lastUpdatedDttm' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'lastUpdatedDttm',
               'Last Updated DTTM',
               'EPI');
           
MERGE INTO RESOURCES A
     USING (SELECT '/services/rest/epi_common/EPIEndOfDayService/epiEndOfDayService/getCarriers'
                      AS URI,
                   2 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               B.URI,
               'EPI',
               2,
               'GET');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'VCR' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/services/rest/epi_common/EPIEndOfDayService/epiEndOfDayService/getCarriers'
                   AND MODULE = 'EPI') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (rp1.resource_id, rp1.permission_code);

UPDATE message_master
   SET ils_module = 'epi'
 WHERE msg = 'EPI {0} failed for {1} {2}: {3}';

UPDATE message_master
   SET ils_module = 'epi'
 WHERE msg =
          'EPI {0} Operation failed because EPI Carrier Code is not defined for Carrier: {1}';

UPDATE message_master
   SET ils_module = 'epi'
 WHERE msg =
          'EPI {0} Operation failed because EPI Service Level Code is not defined for Service Level: {1}';

UPDATE message_master
   SET ils_module = 'epi'
 WHERE msg = 'EPI {0} has following warnings for {1} {2}: {3}';

UPDATE message_master
   SET ils_module = 'epi'
 WHERE msg =
          'Error importing EPI carrier services info from provider {0}. {1}';
		  
COMMIT;

-- DBTicket DB-3212
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '1785009',
               '1785009',
               'epi',
               '',
               'End Of Day Process is either Requested or is In Progress for Carrier: {0} ,Facility: {1} , Date: {2}',
               'ErrorMessage',
               'SYSTEM',
               'ERROR',
               'null',
               'N',
               current_timestamp,
               current_timestamp);


INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '1785010',
               '1785010',
               'epi',
               '',
               'Selected carrier does not support end of day process. Cannot Initiate',
               'ErrorMessage',
               'SYSTEM',
               'ERROR',
               'null',
               'N',
               current_timestamp,
               current_timestamp);
 COMMIT;              
               
 RENAME SEQ_EPI_END_OF_DAY_ID  TO SEQ_EOD_REQUEST_ID;

 ALTER TABLE EPI_END_OF_DAY RENAME COLUMN EPI_END_OF_DAY_ID  TO EOD_REQUEST_ID;

 ALTER TABLE EPI_END_OF_DAY DROP COLUMN EOD_OUTPUT_TYPE_LIST;
 
 ALTER INDEX UK_EPI_END_OF_DAY_ID_IDX RENAME TO UK_EOD_REQUEST_ID_IDX; 
 

-- DBTicket DB-2926
MERGE INTO RESOURCES A
     USING (SELECT '/teFilterDispatcherServices.serv' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/teFilterDispatcherServices.serv',
               'CMA',
               1,
               NULL);
                    
MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CMA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/teFilterDispatcherServices.serv') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/teFilterDispatcherServices.serv'),
               'MRFARPTDFN');
COMMIT;			   
 
-- DBTicket DB-3309
INSERT INTO LRF_REPORT_DEF (Report_Def_Id_O,
                            Description,
                            Dbname,
                            TYPE,
                            Viewperm,
                            Editperm,
                            Category,
                            Subcategory,
                            Report_Def_Id,
                            Report_Name,
                            Report_File_Name,
                            Priority,
                            Reset_Count,
                            Reset_Interval,
                            Retry_Count,
                            Retry_Interval,
                            Datasource)
     VALUES ('03',
             'EPI EOD Documents Reports',
             'DB2DB',
             'Jasper',
             'AAPA',
             'AAPA',
             'WM',
             'OUTBOUND',
             seq_lrf_report_def_id.NEXTVAL,
             'EPIEODDocumentsReports',
             'EPIEODDocumentsReports',
             -1,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL);

INSERT INTO LRF_REPORT_DEF_LAYOUT (REPORT_DEF_LAYOUT_ID,
                                   FIELD_POSITION,
                                   IS_REQUIRED,
                                   FIELD_NAME,
                                   FIELD_LABEL,
                                   FIELD_OPERATORS,
                                   FIELD_TYPE,
                                   VALUE_GENERATOR_CLASS,
                                   SELECTION_PAGE_URL,
                                   OPERATOR_TYPE,
                                   SUB_OBJECT_TYPE,
                                   TABLE_NAME,
                                   DEFAULT_VALUE,
                                   IS_LOCALIZABLE,
                                   SEND_PARAMETER,
                                   HIBERNATE_FIELD_NAME,
                                   LOOKUP_ATTRIBUTE,
                                   MAX_DATE_DIFF,
                                   REPORT_DEF_ID,
                                   CUST_VALID_CLASS,
                                   HIDDEN_PARAM_CLASS,
                                   DATA_CONFIG_QUERY,
                                   LOOKUP_CONFIG_QUERY,
                                   RENDERED)
        VALUES (
                  lrf_report_def_layout_id_seq.NEXTVAL,
                  0,
                  1,
                  'EOD.EOD_REQUEST_ID',
                  'EOD  REQUEST ID',
                  '=',
                  'STRING',
                  NULL,
                  NULL,
                  'REPORT_TEXT_ITEM',
                  NULL,
                  NULL,
                  NULL,
                  0,
                  0,
                  'epiEndOfDayId',
                  NULL,
                  NULL,
                  (SELECT MAX (Report_Def_Id)
                     FROM Lrf_Report_Def
                    WHERE     Report_Name = 'EPIEODDocumentsReports'
                          AND Category = 'WM'
                          AND Subcategory = 'OUTBOUND'),
                  NULL,
                  NULL,
                  NULL,
                  NULL,
                  NULL);
                  
MERGE INTO LABEL L
     USING (SELECT 'TE' BUNDLE_NAME, 'epiEndOfDayId' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES (Seq_Label_Id.NEXTVAL,
               'epiEndOfDayId',
               'EPI End of Day Id',
               'TE');

MERGE INTO RESOURCES A
     USING (SELECT '/services/rest/epi_common/EPIEndOfDayService/epiEndOfDayService/EpiEndOfDayDocumentActionParameters'
                      AS URI,
                   2 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/services/rest/epi_common/EPIEndOfDayService/epiEndOfDayService/EpiEndOfDayDocumentActionParameters',
                 'TE',
                 2,
                 'GET');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'VCR' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/services/rest/epi_common/EPIEndOfDayService/epiEndOfDayService/EpiEndOfDayDocumentActionParameters')
           RP1
        ON (    RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (rp1.resource_id, rp1.permission_code);
       
COMMIT;

-- DBTicket DB-3349
MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'eodPollCount' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
                     L.KEY,
                     L.VALUE,
                     L.BUNDLE_NAME)
     VALUES (Seq_Label_Id.NEXTVAL,
             'eodPollCount',
             'Poll Count',
             'EPI');
               
MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'carrierCode' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
                     L.KEY,
                     L.VALUE,
                     L.BUNDLE_NAME)
     VALUES (Seq_Label_Id.NEXTVAL,
             'carrierCode',
             'Carrier Name',
             'EPI');


MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'tcCompanyIdName' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
                     L.KEY,
                     L.VALUE,
                     L.BUNDLE_NAME)
     VALUES (Seq_Label_Id.NEXTVAL,
             'tcCompanyIdName',
             'Business Unit',
             'EPI');

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'eodStatusDesc' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
                     L.KEY,
                     L.VALUE,
                     L.BUNDLE_NAME)
     VALUES (Seq_Label_Id.NEXTVAL,
             'eodStatusDesc',
             'Status',
             'EPI');

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'oFacilityIdName' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
                     L.KEY,
                     L.VALUE,
                     L.BUNDLE_NAME)
     VALUES (Seq_Label_Id.NEXTVAL,
             'oFacilityIdName',
             'Facility Id',
             'EPI');

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'initiateEndOfDay' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
                     L.KEY,
                     L.VALUE,
                     L.BUNDLE_NAME)
     VALUES (Seq_Label_Id.NEXTVAL,
             'initiateEndOfDay',
             'Initiate End Of Day',
             'EPI');

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'manifestedLpns' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
                     L.KEY,
                     L.VALUE,
                     L.BUNDLE_NAME)
     VALUES (Seq_Label_Id.NEXTVAL,
             'manifestedLpns',
             'Manifested Lpns',
             'EPI');


UPDATE Lrf_Report_Def
   SET category = 'EPI', Subcategory = 'EPI'
 WHERE report_name LIKE 'EPIIntlReports';

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
             '1785011',
             '1785011',
             'epi',
             '',
             'End of Day Process is not yet completed',
             'ErrorMessage',
             'SYSTEM',
             'ERROR',
             'null',
             'N',
             current_timestamp,
             current_timestamp);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
             '1785012',
             '1785012',
             'epi',
             '',
             'End of Day Record Initiated Successfully',
             'ErrorMessage',
             'SYSTEM',
             'INFORM',
             'null',
             'N',
             current_timestamp,
             current_timestamp);
			 
COMMIT;	


-- DBTicket DB-3444
-----Correction for DB-2926
UPDATE RESOURCE_PERMISSION
   SET PERMISSION_CODE = 'CMA'
 WHERE resource_id IN (SELECT RESOURCE_ID
                         FROM RESOURCES
                        WHERE URI = '/teFilterDispatcherServices.serv');
COMMIT;						

-- DBTicket DB-3458
call quiet_drop('procedure', 'wm_val_get_runtime_bind_vars');
alter table val_runtime_binds drop constraint fk_vrb_to_vjd;
alter table val_runtime_binds add constraint fk_vrb_to_vjd 
 foreign key(val_job_dtl_id) references val_job_dtl(val_job_dtl_id) on delete cascade;

call quiet_drop('type', 't_bind_obj force');
create type t_bind_obj as object
(
    bind_var varchar2(30),
    bind_val varchar2(50)
)
/
create or replace type ta_bind_obj as table of t_bind_obj
/

-- DBTicket DB-3030 PLSQL
@DBScripts/Product/PLSQL_Objects/val_job_sql_vw.sql
@DBScripts/Product/PLSQL_Objects/val_job_data_vw.sql
@DBScripts/Product/PLSQL_Objects/val_sql_data_vw.sql
@DBScripts/Product/PLSQL_Objects/wm_val_prep_sql.sql
@DBScripts/Product/PLSQL_Objects/wm_val_derive_select_info.sql
@DBScripts/Product/PLSQL_Objects/wm_val_add_hints_to_sql.sql
@DBScripts/Product/PLSQL_Objects/wm_val_parse_csv_bind_data.sql
@DBScripts/Product/PLSQL_Objects/wm_val_define_cursor_columns.sql
@DBScripts/Product/PLSQL_Objects/wm_val_apply_bind_val_to_sql.sql
@DBScripts/Product/PLSQL_Objects/wm_val_exec_sql_intrnl.sql
@DBScripts/Product/PLSQL_Objects/wm_val_exec_sql.sql
@DBScripts/Product/PLSQL_Objects/wm_val_exec_job_intrnl.sql

-- DBTicket DB-3461
alter table EPI_END_OF_DAY add ERROR_DETAILS VARCHAR2(2000);

-- DBTicket DB-3510
alter table EPI_END_OF_DAY add EOD_EXP_PKG_COUNT NUMBER(12);

-- DBTicket DB-3445
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '1159016515',
               '1159016515',
               'te',
               '',
               'Change of ship via using Weigh action is permitted only from parcel to non-parcel',
               'ErrorMessage',
               'SYSTEM',
               'ERROR',
               'null',
               'N',
               current_timestamp,
               current_timestamp);
			   
MERGE INTO LABEL L
     USING (SELECT 'TE' BUNDLE_NAME, 'printDocument' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (Seq_Label_Id.NEXTVAL,
               'printDocument',
               'Print Document',
               'TE');

UPDATE label
   SET VALUE = 'Facility ID'
 WHERE key = 'oFacilityIdName';
COMMIT;

-- DBTicket DB-3644
alter table val_result_hist modify pgm_id  varchar2(40);
commit;

-- DBTicket DB-3618
-- (DB2 only)

-- DBTicket DB-3634

alter table val_job_seq modify pgm_id  varchar2(40);

-- DBTicket DB-3635

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'eodExpectedPackageCount' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Expected Number of LPNs',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'errorDetails' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Error Details',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'printDocument' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Print Documents',
               B.BUNDLE_NAME);

UPDATE label
   SET VALUE = 'EPI Request ID'
 WHERE key = 'epiEndOfDayId' AND bundle_name = 'EPI';

UPDATE label
   SET VALUE = 'Manifested LPNs'
 WHERE key = 'manifestedLpns' AND bundle_name = 'EPI';

UPDATE label
   SET VALUE = 'Print Documents'
 WHERE key = 'printDocument' AND bundle_name = 'EPI';

UPDATE label
   SET VALUE = 'Facility ID'
 WHERE key = 'oFacilityAliasId' AND bundle_name = 'EPI';

UPDATE label
   SET VALUE = 'Error Details'
 WHERE key = 'errorDetails' AND bundle_name = 'EPI';

MERGE INTO MESSAGE_MASTER A
     USING (SELECT '1159016915' MSG_ID,
                   '1159016915' KEY,
                   'epi' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
               MSG_ID,
               KEY,
               ILS_MODULE,
               MSG_MODULE,
               MSG,
               BUNDLE_NAME,
               MSG_CLASS,
               MSG_TYPE,
               OVRIDE_ROLE,
               LOG_FLAG)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               B.MSG_ID,
               B.KEY,
               B.ILS_MODULE,
               NULL,
               'Transaction Successful.',
               B.BUNDLE_NAME,
               'SYSTEM',
               'INFORM',
               NULL,
               'Y');

MERGE INTO MESSAGE_MASTER A
     USING (SELECT '1159016916' MSG_ID,
                   '1159016916' KEY,
                   'epi' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
               MSG_ID,
               KEY,
               ILS_MODULE,
               MSG_MODULE,
               MSG,
               BUNDLE_NAME,
               MSG_CLASS,
               MSG_TYPE,
               OVRIDE_ROLE,
               LOG_FLAG)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               B.MSG_ID,
               B.KEY,
               B.ILS_MODULE,
               NULL,
               'Transaction Failed.',
               B.BUNDLE_NAME,
               'SYSTEM',
               'INFORM',
               NULL,
               'N');
               
COMMIT;

-- DBTicket DB-3709
alter table EPI_END_OF_DAY add SERVICE_LEVEL_GROUPING VARCHAR2(200);

-- DBTicket DB-3781
UPDATE label
   SET VALUE = 'EPI Manifest ID'
 WHERE key = 'epiManifestId' AND bundle_name = 'EPI';
 
COMMIT;

-- DBTicket DB-3786
MERGE INTO MESSAGE_MASTER A
     USING (SELECT '9518' MSG_ID,
                   '110909518' KEY,
                   'wm' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
               MSG_ID,
               KEY,
               ILS_MODULE,
               MSG_MODULE,
               MSG,
               BUNDLE_NAME,
               MSG_CLASS,
               MSG_TYPE,
               OVRIDE_ROLE,
               LOG_FLAG,
               LAST_UPDATED_DTTM)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               B.MSG_ID,
               B.KEY,
               B.ILS_MODULE,
               'SHIPPING',
               'Some oLPNs on the Pallet Locked, Cannot continue',
               B.BUNDLE_NAME,
               'USER',
               'WARNING',
               NULL,
               'Y',
               NULL);

MERGE INTO MESSAGE_MASTER A
     USING (SELECT '9519' MSG_ID,
                   '110909519' KEY,
                   'wm' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
               MSG_ID,
               KEY,
               ILS_MODULE,
               MSG_MODULE,
               MSG,
               BUNDLE_NAME,
               MSG_CLASS,
               MSG_TYPE,
               OVRIDE_ROLE,
               LOG_FLAG,
               LAST_UPDATED_DTTM)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               B.MSG_ID,
               B.KEY,
               B.ILS_MODULE,
               'SHIPPING',
               'Some oLPNs on the Pallet Locked, Cannot continue',
               B.BUNDLE_NAME,
               'SYSTEM',
               'ERROR',
               NULL,
               'Y',
               NULL);
			   
COMMIT;

-- DBTicket DB-3871
UPDATE label
   SET VALUE = 'de-manifested'
 WHERE key = 'DEMANIFEST';

UPDATE message_master
   SET msg = 'Selected oLPN(s) cannot be {0} as {1} is {2}'
 WHERE key = '9921006';
 
MERGE INTO LABEL L
       USING (SELECT 'TE' BUNDLE_NAME, 'EpiPackageStatus' KEY
                FROM DUAL) B
          ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
  WHEN NOT MATCHED
  THEN
  INSERT(L.LABEL_ID,
                 L.KEY,
                 L.VALUE,
                 L.BUNDLE_NAME)
  VALUES (Seq_Label_Id.NEXTVAL,
             'EpiPackageStatus',
             'EPI Package Status',
             'TE');
COMMIT;

-- DBTicket DB-3924
DECLARE
v_constraint_count NUMBER;
BEGIN
    SELECT COUNT (*)
        INTO v_constraint_count
        from user_constraints
       WHERE constraint_name = 'CON_TMP_FK4';
   
   IF (v_constraint_count > 0)
   THEN      
      EXECUTE IMMEDIATE 'alter table CONS_TEMPLATE drop constraint CON_TMP_FK4';            
   END IF;
   
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (SQLERRM);      
END;
/

-- DBTicket DB-3948
UPDATE label
   SET VALUE = 'EPI Shipment ID'
 WHERE key = 'EpiShipmentId' AND bundle_name = 'EPI';

UPDATE label
   SET VALUE = 'EPI Package ID'
 WHERE key = 'EpiPackageId' AND bundle_name = 'EPI';

COMMIT; 

-- DBTicket DB-4017
MERGE INTO LABEL L
     USING (SELECT 'EPI' BUNDLE_NAME, 'EPINumberofLPNs' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'EPINumberofLPNs',
               'EPI Number of LPNs',
               'EPI');

COMMIT;

-- DBTicket DB-4048
DELETE FROM xmenu_item_permission
      WHERE PERMISSION_CODE = 'PRTEPIDOC'
            AND XMENU_ITEM_ID =
                   (SELECT XMENU_ITEM_ID
                      FROM XMENU_ITEM
                     WHERE NAVIGATION_KEY =
                              'maui://Epicommon.screen.endofday.EndOfDay');
							  
COMMIT;

-- DBTicket DB-4055
INSERT INTO LRF_REPORT_DEF_LAYOUT (REPORT_DEF_LAYOUT_ID,
                                   FIELD_POSITION,
                                   IS_REQUIRED,
                                   FIELD_NAME,
                                   FIELD_LABEL,
                                   FIELD_OPERATORS,
                                   FIELD_TYPE,
                                   VALUE_GENERATOR_CLASS,
                                   SELECTION_PAGE_URL,
                                   OPERATOR_TYPE,
                                   SUB_OBJECT_TYPE,
                                   TABLE_NAME,
                                   DEFAULT_VALUE,
                                   IS_LOCALIZABLE,
                                   SEND_PARAMETER,
                                   HIBERNATE_FIELD_NAME,
                                   LOOKUP_ATTRIBUTE,
                                   MAX_DATE_DIFF,
                                   REPORT_DEF_ID,
                                   CUST_VALID_CLASS,
                                   HIDDEN_PARAM_CLASS,
                                   DATA_CONFIG_QUERY,
                                   LOOKUP_CONFIG_QUERY,
                                   RENDERED)
     VALUES (
               lrf_report_def_layout_id_seq.NEXTVAL,
               0,
               1,
               'LPN.EPI_DOC_OUTPUT_TYPE',
               'EPI Document Type',
               '=',
               'STRING',
               NULL,
               NULL,
               'REPORT_TEXT_ITEM',
               NULL,
               NULL,
               NULL,
               0,
               0,
               'epiDocOutputType',
               NULL,
               NULL,
               (SELECT max(Report_Def_Id)
                  FROM Lrf_Report_Def
                 WHERE     Report_Name = 'EPIIntlReports'
                       AND Category = 'EPI'
                       AND Subcategory = 'EPI'),
               NULL,
               NULL,
               NULL,
               NULL,
               NULL);
			   
COMMIT;

-- DBTicket DB-4089
DELETE FROM FILTER_LAYOUT
      WHERE field_name = 'EDI_XREF.SHIPVIA';

DELETE FROM FILTER_LAYOUT
      WHERE field_name = 'EDI_XREF.CARR_TYPE';

INSERT INTO FILTER_LAYOUT (FILTER_LAYOUT_ID,
                           FIELD_POSITION,
                           IS_REQUIRED,
                           FIELD_NAME,
                           FIELD_LABEL,
                           FIELD_OPERATORS,
                           FIELD_TYPE,
                           VALUE_GENERATOR_CLASS,
                           SELECTION_PAGE_URL,
                           OPERATOR_TYPE,
                           FILTER_SUB_OBJECT_ID,
                           TABLE_NAME,
                           DEFAULT_VALUE,
                           IS_LOCALIZABLE,
                           HIBERNATE_FIELD_NAME,
                           PRODUCTS_AVAILABLE,
                           SCREEN_TYPE,
                           COLUMNSCREEN_TYPE,
                           LOOKUP_ATTRIBUTE,
                           SUB_OBJECT_TYPE,
                           OBJECT_TYPE)
     VALUES (SEQ_FILTER_LAYOUT_ID.NEXTVAL,
             40,
             1,
             'EDI_XREF.CARR_TYPE',
             'Carrier Type',
             '=',
             'NUMBER',
             'com.manh.common.fv.ParcelCarrierType',
             NULL,
             'MULTIPLE_STATE_COMP',
             NULL,
             NULL,
             NULL,
             0,
             'XREF.carrType',
             NULL,
             4,
             NULL,
             NULL,
             NULL,
             'EDI_PLD_CROSSREFERENCE');

INSERT INTO FILTER_LAYOUT (FILTER_LAYOUT_ID,
                           FIELD_POSITION,
                           IS_REQUIRED,
                           FIELD_NAME,
                           FIELD_LABEL,
                           FIELD_OPERATORS,
                           FIELD_TYPE,
                           VALUE_GENERATOR_CLASS,
                           SELECTION_PAGE_URL,
                           OPERATOR_TYPE,
                           FILTER_SUB_OBJECT_ID,
                           TABLE_NAME,
                           DEFAULT_VALUE,
                           IS_LOCALIZABLE,
                           HIBERNATE_FIELD_NAME,
                           PRODUCTS_AVAILABLE,
                           SCREEN_TYPE,
                           COLUMNSCREEN_TYPE,
                           LOOKUP_ATTRIBUTE,
                           SUB_OBJECT_TYPE,
                           OBJECT_TYPE,
                           CREATED_DTTM,
                           LAST_UPDATED_DTTM)
     VALUES (
               SEQ_FILTER_LAYOUT_ID.NEXTVAL,
               50,
               1,
               'EDI_XREF.SHIPVIA',
               'Ship Via',
               '=',
               'ANYCASE_STRING',
               NULL,
               '/lps/resources/editControl/lookup/idLookup.jsflps?lookupType=Ship Via',
               'COMP',
               NULL,
               NULL,
               NULL,
               0,
               'XREF.shipVia',
               NULL,
               12,
               NULL,
               '#{cboTransFilterLookupBackingBean.getBUMap},#{cboTransFilterLookupBackingBean.getOptionConstructMap}',
               NULL,
               'EDI_PLD_CROSSREFERENCE',
               SYSDATE,
               NULL);
COMMIT;

-- DBTicket DB-4211
Insert into MESSAGE_MASTER (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG,CREATED_DTTM,LAST_UPDATED_DTTM) 
values (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1115013214', '1115013214','te','manifest',
'Manifest Type required for service level
{0}
on Parcel Origin Attributes.'
,'ErrorMessage','SYSTEM','ERROR',null,'N',sysdate,sysdate);
commit ;


-- DBTicket DB-4254
MERGE INTO RESOURCES A
     USING (SELECT '/services/rest/OutboundServices/WMShipmentActionService/wmShipmentActions/closeShipment'
                      AS URI,
                   2 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/services/rest/OutboundServices/WMShipmentActionService/wmShipmentActions/closeShipment',
                 'OTBND',
                 2,
                 'POST');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'CLSHP' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/services/rest/OutboundServices/WMShipmentActionService/wmShipmentActions/closeShipment') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (rp1.resource_id, rp1.permission_code);
commit;	

-- DBTicket DB-3935
BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE USER_PROFILE ADD MOBILE_HELP_TEXT VARCHAR2(1 ) DEFAULT ''N'' ';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/

-- DBTicket DB-4509
UPDATE tran_parm
   SET LITRL = 'Not Used'
 WHERE pgm_id = 'lib/libloadtrailer' AND from_posn = 19;

COMMIT;

-- DBTicket DB-4571
------DB2 Only

-- DBTicket DB-4607
----- Currently changes are for DB2

-- DBTicket DB-4643
UPDATE tran_parm
   SET PARM_DESC = 'Not Used'
WHERE pgm_id = 'lib/libloadtrailer' AND from_posn = 19;
COMMIT;

-- DBTicket DB-4735
comment on table ENTERPRISE_MASTER is 'Enterprise level flags and configurations.';
comment on table LABEL_SUBSTITUTIONS is 'Label printing substitutions can be specified here.';
comment on table USER_PROFILE is 'This table is used to store user profile details';
comment on table USER_TASK_GRP is 'This table is used to specify task groups that user is eligible';

-- DBTicket DB-4842
COMMENT ON TABLE CONS_ORDER_PATH IS 'CONS_ORDER_PATH';
COMMENT ON TABLE CONS_RUN_DTL_TYPE IS 'CONS_RUN_DTL_TYPE';
COMMENT ON TABLE CONSTEMP_SCHED_EVENT IS 'CONSTEMP_SCHED_EVENT';
COMMENT ON TABLE EC_CONSUMER IS 'EC_CONSUMER';
COMMENT ON TABLE EC_CONSUMER_MSG_TYPE IS 'EC_CONSUMER_MSG_TYPE';
COMMENT ON TABLE EC_MESSAGE IS 'EC_MESSAGE';
COMMENT ON TABLE EC_MESSAGE_CONSUMER IS 'EC_MESSAGE_CONSUMER';
COMMENT ON TABLE LABEL_XREF IS 'LABEL_XREF';
COMMENT ON TABLE LITRL_XREF IS 'LITRL_XREF';
COMMENT ON TABLE MLP_WS_MONITOR IS 'MLP_WS_MONITOR';
COMMENT ON TABLE SEC_USER IS 'SEC_USER';
COMMENT ON TABLE CALLOUT_EVENT IS 'CALLOUT_EVENT';
COMMENT ON TABLE CALLOUT_MAPPING IS 'CALLOUT_MAPPING';
COMMENT ON TABLE CALLOUT_SCRIPT IS 'CALLOUT_SCRIPT';


-- DBTicket DB-4631
CREATE OR REPLACE VIEW COMMODITY_CLASS_VIEW
AS
   (  SELECT COUNT (1) PALLETCOUNT,
             LISTAGG ('''' || palletId || ''',')
                WITHIN GROUP (ORDER BY palletId)
                TCPARENTLPNIDLIST,
             SHIPID
        FROM (  SELECT LISTAGG (description_short || ' ')
                          WITHIN GROUP (ORDER BY commodity_code_id)
                          grp,
                       tc_parent_lpn_id palletId,
                       shipment_id SHIPID
                  FROM (SELECT DISTINCT cc.description_short description_short,
                                        cc.commodity_code_id commodity_code_id,
                                        l.tc_parent_lpn_id tc_parent_lpn_id,
                                        l.shipment_id shipment_id
                          FROM lpn l
                               INNER JOIN LPN_DETAIL ld ON l.lpn_id = ld.lpn_id
                               INNER JOIN ITEM_CBO ic ON ic.item_id = ld.item_id
                               INNER JOIN commodity_code cc
                                  ON     cc.COMMODITY_CODE_ID =
                                            ic.commodity_code_id
                                     AND l.tc_parent_lpn_id IS NOT NULL)
              GROUP BY tc_parent_lpn_id, shipment_id
                HAVING shipment_id IS NOT NULL)
    GROUP BY GRP, SHIPID);
	
-- DBTicket DB-5423

MERGE INTO application_configuration L
     USING (SELECT 'fedex_max_pool_size' key FROM DUAL) B
        ON (L.KEY = B.KEY)
WHEN NOT MATCHED
THEN
   INSERT     (key,
               VALUE,
               module,
               APPLY_TO_SERVER,
               COMMENTS,
               TAGS)
       VALUES ('fedex_max_pool_size',
               '200',
               'global',
               'ALL',
               'EMPTY_CLOB ()',
               NULL);
			   
MERGE INTO application_configuration L
     USING (SELECT 'fedex_max_wait_time' key FROM DUAL) B
        ON (L.KEY = B.KEY)
WHEN NOT MATCHED
THEN
   INSERT     (key,
               VALUE,
               module,
               APPLY_TO_SERVER,
               COMMENTS,
               TAGS)
       VALUES ('fedex_max_wait_time',
               '8000',
               'global',
               'ALL',
               'EMPTY_CLOB ()',
               NULL);

MERGE INTO PARAM_SUBGROUP A
     USING (SELECT 'COM_FAC' PARAM_GROUP_ID, 'TE' PARAM_SUBGROUP_ID FROM DUAL)
           B
        ON (    A.PARAM_GROUP_ID = B.PARAM_GROUP_ID
            AND A.PARAM_SUBGROUP_ID = B.PARAM_SUBGROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_SUBGROUP_NAME,
               DISPLAY_ORDER,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('COM_FAC',
               'TE',
               'TE',
               1,
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

INSERT INTO PARAM_GROUPING (PARAM_GROUPING_ID,
                            PARAM_GROUPING_NAME,
                            PARAM_GROUP_ID,
                            PARAM_SUBGROUP_ID)
     VALUES ( (SELECT MAX (PARAM_GROUPING_ID) + 1
                 FROM PARAM_GROUPING),
             'FacCompanyParams',
             'COM_FAC',
             'TE');

MERGE INTO PARAM_DEF P
     USING (SELECT 'BYPASS_FEDEX_SOCKET_POOL' PARAM_DEF_ID,
                   'COM_FAC' PARAM_GROUP_ID
              FROM DUAL) D
        ON (    P.PARAM_DEF_ID = D.PARAM_DEF_ID
            AND P.PARAM_GROUP_ID = D.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM,
               PARAM_LEVEL)
       VALUES ('BYPASS_FEDEX_SOCKET_POOL',
               'COM_FAC',
               'TE',
               'Bypass FedEx Socket Pool',
               'Bypass FedEx Socket Pool',
               0,
               0,
               6,
               'CHECKBOX',
               NULL,
               0,
               NULL,
               0,
               'false',
               1,
               NULL,
               1,
               'Planning',
               NULL,
               NULL,
               NULL,
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               1);

COMMIT;

-- DBTicket DB-5563
MERGE INTO PARAM_DEF P
     USING (SELECT 'activate_shipment_commit_frequency' PARAM_DEF_ID,
                   'COM_TEPE' PARAM_GROUP_ID
              FROM DUAL) D
        ON (    P.PARAM_DEF_ID = D.PARAM_DEF_ID
            AND P.PARAM_GROUP_ID = D.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM,
               PARAM_LEVEL)
       VALUES ('activate_shipment_commit_frequency',
               'COM_TEPE',
               'SROUTE',
               'Activate Shipment Commit Frequency',
               'Activate Shipment Commit Frequency',
               0,
               0,
               4,
               'TEXTBOX',
               NULL,
               0,
               NULL,
               0,
               '0',
               270,
               'null',
               1,
               'Static Route',
               'null',
               'null',
               NULL,
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               0);
               
COMMIT;

-- DBTicket DB-5582
MERGE INTO BUNDLE_META_DATA A
     USING (SELECT 'ConsolidationExternalStatus' AS BUNDLE_NAME FROM DUAL) B
        ON (A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (BUNDLE_META_DATA_ID,
               BUNDLE_NAME,
               DESCRIPTION,
               DEF_SCREEN_TYPE_ID,
               DEF_APP_ID,
               HINT,
               IS_ACTIVE,
               IS_LABEL_BUNDLE,
               DEF_BU_ID)
       VALUES (seq_bundle_meta_data_id.NEXTVAL,
               'ConsolidationExternalStatus',
               'ConsolidationExternalStatus',
               10,
               NULL,
               NULL,
               1,
               1,
               -1);

MERGE INTO LABEL L
     USING (SELECT 'ConsolidationExternalStatus' BUNDLE_NAME, 'Complete' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Complete',
               'Complete',
               'ConsolidationExternalStatus');

MERGE INTO LABEL L
     USING (SELECT 'ConsolidationExternalStatus' BUNDLE_NAME, 'Failed' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Failed',
               'Failed',
               'ConsolidationExternalStatus');

MERGE INTO LABEL L
     USING (SELECT 'ConsolidationExternalStatus' BUNDLE_NAME,
                   'Selecting Orders' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Selecting Orders',
               'Selecting Orders',
               'ConsolidationExternalStatus');

MERGE INTO LABEL L
     USING (SELECT 'ConsolidationExternalStatus' BUNDLE_NAME,
                   'Selecting Purchase Orders' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Selecting Purchase Orders',
               'Selecting Purchase Orders',
               'ConsolidationExternalStatus');

MERGE INTO LABEL L
     USING (SELECT 'ConsolidationExternalStatus' BUNDLE_NAME,
                   'Preprocessing' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Preprocessing',
               'Preprocessing',
               'ConsolidationExternalStatus');

MERGE INTO LABEL L
     USING (SELECT 'ConsolidationExternalStatus' BUNDLE_NAME, 'Running' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Running',
               'Running',
               'ConsolidationExternalStatus');

MERGE INTO LABEL L
     USING (SELECT 'ConsolidationExternalStatus' BUNDLE_NAME,
                   'Postprocessing' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Postprocessing',
               'Postprocessing',
               'ConsolidationExternalStatus');

MERGE INTO LABEL L
     USING (SELECT 'ConsolidationExternalStatus' BUNDLE_NAME, 'Cancelled' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Cancelled',
               'Cancelled',
               'ConsolidationExternalStatus');

MERGE INTO BUNDLE_META_DATA A
     USING (SELECT 'ConsPostRunStatus' AS BUNDLE_NAME FROM DUAL) B
        ON (A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (BUNDLE_META_DATA_ID,
               BUNDLE_NAME,
               DESCRIPTION,
               DEF_SCREEN_TYPE_ID,
               DEF_APP_ID,
               HINT,
               IS_ACTIVE,
               IS_LABEL_BUNDLE,
               DEF_BU_ID)
       VALUES (seq_bundle_meta_data_id.NEXTVAL,
               'ConsPostRunStatus',
               'ConsPostRunStatus',
               10,
               NULL,
               NULL,
               1,
               1,
               -1);

MERGE INTO LABEL L
     USING (SELECT 'ConsPostRunStatus' BUNDLE_NAME, 'None' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'None',
               'None',
               'ConsPostRunStatus');

MERGE INTO LABEL L
     USING (SELECT 'ConsPostRunStatus' BUNDLE_NAME, 'Reviewed' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Reviewed',
               'Reviewed',
               'ConsPostRunStatus');

MERGE INTO LABEL L
     USING (SELECT 'ConsPostRunStatus' BUNDLE_NAME, 'Rejected' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Rejected',
               'Rejected',
               'ConsPostRunStatus');

MERGE INTO LABEL L
     USING (SELECT 'ConsPostRunStatus' BUNDLE_NAME, 'Partially Rejected' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Partially Rejected',
               'Partially Rejected',
               'ConsPostRunStatus');

COMMIT;

-- DBTicket DB-6480
MERGE INTO MESSAGE_MASTER A
     USING (SELECT '9003893' MSG_ID,
                   '9003893' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
               MSG_ID,
               KEY,
               ILS_MODULE,
               MSG_MODULE,
               MSG,
               BUNDLE_NAME,
               MSG_CLASS,
               MSG_TYPE,
               OVRIDE_ROLE,
               LOG_FLAG,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES (
                 SEQ_MESSAGE_MASTER_ID.NEXTVAL,
                 '9003893',
                 '9003893',
                 'te',
                 'manifest',
                 'Cannot close manifest {0}. Tracking number is not present on one or more oLPNs.',
                 'ErrorMessage',
                 'SYSTEM',
                 'ERROR',
                 NULL,
                 'N',
                 CURRENT_TIMESTAMP,
                 CURRENT_TIMESTAMP);
                 
COMMIT;

-- DBTicket DB-7087
MERGE INTO PARAM_DEF P
     USING (SELECT 'ail_els_activity_code' PARAM_DEF_ID, 'COM_TEPE' PARAM_GROUP_ID
              FROM DUAL) D
        ON (    P.PARAM_DEF_ID = D.PARAM_DEF_ID
            AND P.PARAM_GROUP_ID = D.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS
               )
       VALUES (D.PARAM_DEF_ID,
               D.PARAM_GROUP_ID,
               'MNFT',
             'LM ELS Activity Code for AIL',
             'LM ELS Activity Code for AIL',
               0,
             0,
             4,
             'TEXTBOX',
               NULL,
             0,
             NULL,
             0,
             '',
             270,
               NULL,
             1,
             'Manifesting',
             NULL,
             NULL,
             NULL);

COMMIT;

-- DBTicket DB-7091
MERGE INTO MESSAGE_MASTER A
     USING (SELECT '1115013216' MSG_ID,
                   '1115013216' KEY,
                   'te' ILS_MODULE,
                   'ErrorMessage' BUNDLE_NAME
              FROM DUAL) B
        ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (MESSAGE_MASTER_ID,
               MSG_ID,
               KEY,
               ILS_MODULE,
               MSG_MODULE,
               MSG,
               BUNDLE_NAME,
               MSG_CLASS,
               MSG_TYPE,
               OVRIDE_ROLE,
               LOG_FLAG)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               B.MSG_ID,
               B.KEY,
               B.ILS_MODULE,
               NULL,
               'Action valid only for parcel carrier type UPS',
               B.BUNDLE_NAME,
               'SYSTEM',
               'ERROR',
               NULL,
               NULL);

UPDATE label
   SET VALUE = 'Reprint UPS Pickup Summary'
 WHERE     key = 'ReprintPickupSummary'
       AND bundle_name = 'manifest_ExtMenuScreenLabels';
       
COMMIT;

-- DBTicket DB-7555
UPDATE FILTER_LAYOUT
   SET SCREEN_type = '12'
 WHERE object_type = 'CDT_POA_FILTER';

UPDATE FILTER_LAYOUT
   SET SCREEN_TYPE = '12'
 WHERE OBJECT_TYPE = 'CDT_RATES_FILTER';

UPDATE FILTER_LAYOUT
   SET SCREEN_TYPE = '12'
 WHERE OBJECT_TYPE = 'CDT_ZONES_FILTER';
 
COMMIT;

-- DBTicket DB-8045
alter sequence SEQ_CONS_ORDER_PATH_ID maxvalue 999999999999;
alter sequence SEQ_CONS_RUN_ID maxvalue 999999999999;
alter sequence SEQ_CONS_RUN_DTL_ID maxvalue 999999999999;
alter sequence SEQ_CONS_TEMPLATE_ID maxvalue 999999999999;
alter sequence EC_CONSUMER_ID_SEQ maxvalue 999999999;
alter sequence EC_CONSUMER_MSG_TYPE_ID_SEQ maxvalue 999999999;
alter sequence EC_MESSAGE_ID_SEQ maxvalue 999999999;
alter sequence EC_MESSAGE_CONSUMER_ID_SEQ maxvalue 999999999;
alter sequence EDI_XREF_ID_SEQ maxvalue 999999999;
alter sequence ENTERPRISE_MASTER_ID_SEQ maxvalue 999999999;
alter sequence HAZMAT_FEDEX_GROUND_ID_SEQ maxvalue 999999999;
alter sequence HAZMAT_UPS_DOM_ALL_ID_SEQ maxvalue 999999999;
alter sequence HAZMAT_UPS_INTL_ID_SEQ maxvalue 999999999;
alter sequence SEQ_LABEL_SUBSTITUTIONS_ID maxvalue 999999999;
alter sequence LABEL_XREF_ID_SEQ maxvalue 999999999;
alter sequence LITRL_XREF_ID_SEQ maxvalue 999999999;
alter sequence LPN_ACCESSORIAL_ID_SEQ maxvalue 9999999999;
alter sequence MANIFEST_HDR_ID_SEQ maxvalue 999999999;
alter sequence MSG_DTL_ID_SEQ maxvalue 999999999;
alter sequence MSG_LOG_ID_SEQ maxvalue 999999999;
alter sequence MSG_MASTER_ID_SEQ maxvalue 999999999;
alter sequence SEQ_OPT_PARAM_LIST_ID maxvalue 99999999;
alter sequence SEQ_PARCEL_RATE_CALENDER_ID maxvalue 9999999999;
alter sequence SEQ_PARCEL_RATE_DATA_ID maxvalue 9999999999;
alter sequence PRT_QUEUE_MONITOR_SEQ maxvalue 9999999999;
alter sequence SEC_USER_ID_SEQ maxvalue 999999999;
alter sequence SURE_POST_ADDRESS_DATA_ID_SEQ maxvalue 99999999;
alter sequence VAL_TXN_ID_SEQ maxvalue 999999999;
alter sequence UPS_EMT_UPLOAD_RPT_ID_SEQ maxvalue 999999999;
alter sequence USER_PROFILE_ID_SEQ maxvalue 999999999;
alter sequence USER_TASK_GRP_ID_SEQ maxvalue 999999999;
alter sequence VAL_JOB_DTL_ID_SEQ maxvalue 999999999;
alter sequence VAL_JOB_HDR_ID_SEQ maxvalue 999999999;
alter sequence VAL_SQL_ID_SEQ maxvalue 999999999;

-- DBTicket DB-8209
CREATE INDEX PTT_COMP_CAR_SL_OZ_IDX
   ON PARCEL_TRANSIT_TIME (TC_COMPANY_ID,
                           CARRIER_ID,
                           SERVICE_LEVEL_ID,
                           ORIGIN_ZIP)
   TABLESPACE TE_BST_IDX_TBS;

CREATE INDEX PRC_COMP_CAL_MOT_SL_ZONE_IDX
   ON PARCEL_RATE_DATA (TC_COMPANY_ID,
                        PARCEL_RATE_CALENDER_ID,
                        ZONE,
                        SERVICE_LEVEL_ID,
                        MOT_ID)
   TABLESPACE TE_BST_IDX_TBS;

CREATE INDEX PZD_CAR_CNT_ZONE_OZ_IDX
   ON PARCEL_ZONE_DATA (CARRIER_CODE,
                        DEST_CNTRY,
                        ZONES_PER_SL,
                        DEST_FROM_ZIP,
                        DEST_TO_ZIP)
   TABLESPACE TE_BST_IDX_TBS;

-- DBTicket DB-8286   
UPDATE NXT_UP_CNT
   SET START_NBR = (SELECT MAX (TRAN_PARM_ID) FROM TRAN_PARM) + 1,
       CURR_NBR = (SELECT MAX (TRAN_PARM_ID) FROM TRAN_PARM) + 1
 WHERE rec_type_id = 'TXT';
 
 COMMIT;
 
-- DBTicket DB-8453
UPDATE label
   SET VALUE = 'Gross Weight (kg)'
 WHERE key = 'GROSSWT' AND bundle_name = 'Languages';
 
COMMIT;  

-- DBTicket DB-9374
UPDATE message_master
   SET msg = 'Billing Method should be the same on Orders'
 WHERE msg_id = '40005';
 
COMMIT;

-- DBTicket DB-9384
UPDATE label
   SET bundle_name = 'Navigation'
 WHERE VALUE = 'Print Blind Labels - Case';
 
COMMIT;

-- DBTicket DB-9442
UPDATE label
   SET VALUE =
          'These items are controlled by the U.S. government and authorized for export only to the country of ultimate destination for use by the ultimate consignee or end-user(s) herein identified. '
 WHERE key = 'INV_1';

UPDATE label
   SET VALUE =
          'They may not be resold, transferred, or otherwise disposed of, to any other country or to any person other than the authorized ultimate consignee or end-user(s), either in their original form or after being incorporated into other items, without first obtaining approval from the U.S. government or as otherwise authorized by U.S. law and regulations.'
 WHERE key = 'INV_2';

COMMIT;
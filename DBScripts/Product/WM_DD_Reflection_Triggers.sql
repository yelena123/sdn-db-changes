EXEC quiet_drop('TRIGGER','COMPANY_AI')  ;
EXEC quiet_drop('TRIGGER','USR_AFT_DEL');
EXEC quiet_drop('TRIGGER','USR_AFT_INS') ;
EXEC quiet_drop('TRIGGER','CMPNY_AFT_INS') ;
EXEC SEQUPDT('PRICE_TYPE_CBO','PRICE_TYPE_ID','PRICE_TYPE_ID_SEQ');

CREATE OR REPLACE TRIGGER CMPNY_AFT_INS
 AFTER INSERT
 ON &1..COMPANY
 REFERENCING NEW AS N
 FOR EACH ROW
BEGIN
 INSERT INTO CD_MASTER (CD_MASTER_ID,
                      company_name,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID)
    VALUES (:N.COMPANY_ID,
            :N.COMPANY_NAME,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM');
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'AUTO_CREATE_BATCH_FLAG',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'BATCH_CTRL_FLAG',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'BATCH_ROLE_ID',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'CASE_LOCK_CODE_EXP_REC',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'CASE_LOCK_CODE_HELD',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'COLOR_MASK',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'COLOR_OFFSET',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'COLOR_SEPTR',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'COLOR_SFX_MASK',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'COLOR_SFX_OFFSET',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'COLOR_SFX_SEPTR',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'DFLT_BATCH_STAT_CODE',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'DSP_ITEM_DESC_FLAG',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'LOCK_CODE_INVALID',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'PICK_LOCK_CODE_EXP_REC',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'PICK_LOCK_CODE_HELD',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'PROC_WHSE_XFER',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'QUAL_MASK',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'QUAL_OFFSET',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'QUAL_SEPTR',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'RECV_BATCH',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SEASON_MASK',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SEASON_OFFSET',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SEASON_SEPTR',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SEASON_YR_MASK',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SEASON_YR_OFFSET',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SEASON_YR_SEPTR',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SEC_DIM_MASK',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SEC_DIM_OFFSET',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SEC_DIM_SEPTR',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SIZE_DESC_MASK',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SIZE_DESC_OFFSET',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SIZE_DESC_SEPTR',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SKU_MASK',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SKU_OFFSET_MASK',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'STYLE_MASK',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'STYLE_OFFSET',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'STYLE_SEPTR',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'STYLE_SFX_MASK',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'STYLE_SFX_OFFSET',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'STYLE_SFX_SEPTR',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'UCC_EAN_CO_PFX',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
END;
/

ALTER TRIGGER CMPNY_AFT_INS ENABLE;

-- DBTicket DB-2073
CREATE OR REPLACE TRIGGER USR_AFT_INS
   AFTER INSERT
   ON &1..UCL_USER
   FOR EACH ROW
BEGIN
   IF (:NEW.COPY_FROM_USER IS NULL)
   THEN
      INSERT INTO USER_PROFILE (USER_PROFILE_ID,
                                LOGIN_USER_ID,
                                MENU_ID,
                                RF_MENU_ID,
                                CREATE_DATE_TIME,
                                MOD_DATE_TIME,
                                USER_ID)
           VALUES (:NEW.UCL_USER_ID,
                   :NEW.USER_NAME,
                   2124,
                   3111,
                   :NEW.CREATED_DTTM,
                   :NEW.CREATED_DTTM,
                   'SYSTEM');
   ELSE
      INSERT INTO USER_PROFILE (USER_PROFILE_ID,
                                LOGIN_USER_ID,
                                MENU_ID,
                                RF_MENU_ID,
                                RESTR_TASK_GRP_TO_DFLT,
                                RESTR_MENU_MODE_TO_DFLT,
                                DFLT_RF_MENU_MODE,
                                LANG_ID,
                                LAST_LOCN,
                                LAST_WORK_GRP,
                                LAST_WORK_AREA,
                                ALLOW_TASK_INT_CHG,
                                NBR_OF_TASK_TO_DSP,
                                CURR_TASK_GRP,
                                TASK_GRP_JUMP_FLAG,
                                AUTO_3PL_LOGIN_FLAG,
                                VOCOLLECT_PUTAWAY_FLAG,
                                VOCOLLECT_REPLEN_FLAG,
                                VOCOLLECT_PACKING_FLAG,
                                CLS_TIMEZONE_ID,
                                WM_VERSION_ID,
                                DFLT_TASK_INT,
                                CREATE_DATE_TIME,
                                MOD_DATE_TIME,
                                USER_ID)
         (SELECT :NEW.UCL_USER_ID,
                 :NEW.USER_NAME,
                 MENU_ID,
                 RF_MENU_ID,
                 RESTR_TASK_GRP_TO_DFLT,
                 RESTR_MENU_MODE_TO_DFLT,
                 DFLT_RF_MENU_MODE,
                 LANG_ID,
                 LAST_LOCN,
                 LAST_WORK_GRP,
                 LAST_WORK_AREA,
                 ALLOW_TASK_INT_CHG,
                 NBR_OF_TASK_TO_DSP,
                 CURR_TASK_GRP,
                 TASK_GRP_JUMP_FLAG,
                 AUTO_3PL_LOGIN_FLAG,
                 VOCOLLECT_PUTAWAY_FLAG,
                 VOCOLLECT_REPLEN_FLAG,
                 VOCOLLECT_PACKING_FLAG,
                 CLS_TIMEZONE_ID,
                 1,
                 DFLT_TASK_INT,
                 :NEW.CREATED_DTTM,
                 :NEW.CREATED_DTTM,
                 'SYSTEM'
            FROM USER_PROFILE
           WHERE USER_PROFILE.LOGIN_USER_ID = :NEW.COPY_FROM_USER);

      INSERT INTO USER_TASK_GRP (LOGIN_USER_ID,
                                 TASK_GRP,
                                 STAT_CODE,
                                 USER_ID,
                                 CREATE_DATE_TIME,
                                 MOD_DATE_TIME,
                                 TASK_GRP_JUMP_PRTY,
                                 REOCCUR_TASK_INTERVAL,
                                 USER_TASK_GRP_ID,
                                 USER_PROFILE_ID,
                                 WM_VERSION_ID,
                                 REOCCUR_INT)
         (SELECT :NEW.USER_NAME,
                 TASK_GRP,
                 STAT_CODE,
                 USER_ID,
                 CREATE_DATE_TIME,
                 MOD_DATE_TIME,
                 TASK_GRP_JUMP_PRTY,
                 REOCCUR_TASK_INTERVAL,
                 USER_TASK_GRP_ID_SEQ.NEXTVAL,
                 :NEW.UCL_USER_ID,
                 WM_VERSION_ID,
                 REOCCUR_INT
            FROM USER_TASK_GRP
           WHERE LOGIN_USER_ID = :NEW.COPY_FROM_USER);

      INSERT INTO LRF_USER_PRO (USER_PRO_ID,
                                EMPLYE_ID,
                                USER_ID,
                                PRTR_REQSTR,
                                CREATED_DTTM,
                                LAST_UPDATED_DTTM)
         (SELECT USER_PROFILE_ID_SEQ.NEXTVAL,
                 EMPLYE_ID,
                 :NEW.USER_NAME,
                 PRTR_REQSTR,
                 :NEW.CREATED_DTTM,
                 :NEW.CREATED_DTTM
            FROM LRF_USER_PRO
           WHERE USER_ID = :NEW.COPY_FROM_USER);

      INSERT INTO USER_DEFAULT (USER_DEFAULT_ID,
                                UCL_USER_ID,
                                PARAMETER_NAME,
                                PARAMETER_VALUE,
                                CREATED_DTTM,
                                LAST_UPDATED_DTTM)
         (SELECT SEQ_USER_DEFAULT_ID.NEXTVAL,
                 :NEW.UCL_USER_ID,
                 PARAMETER_NAME,
                 PARAMETER_VALUE,
                 :NEW.CREATED_DTTM,
                 :NEW.CREATED_DTTM
            FROM USER_DEFAULT
           WHERE UCL_USER_ID IN
                    (SELECT UU.USER_PROFILE_ID
                       FROM USER_PROFILE UU
                      WHERE UU.LOGIN_USER_ID = :NEW.COPY_FROM_USER)
                 AND PARAMETER_NAME NOT IN
                        ('USER_DEFAULT_BU_ID', 'USER_DEFAULT_REGION_ID'));
   END IF;
END;
/

CREATE OR REPLACE TRIGGER USR_AFT_DEL
 AFTER DELETE
 ON &1..UCL_USER
 FOR EACH ROW
BEGIN
 DELETE FROM USER_PROFILE
     WHERE LOGIN_USER_ID = :OLD.USER_NAME;
END;
/

ALTER TRIGGER USR_AFT_DEL ENABLE;

-----DB-1365
CREATE OR REPLACE TRIGGER COMPANY_AI
 AFTER INSERT
 ON &1..COMPANY
 REFERENCING NEW AS NEW
 FOR EACH ROW
DECLARE
 V_DL_ID     NUMBER;
 V_COUNT     NUMBER;
 VGENID      NUMBER;
 V_SHIP_ID   NUMBER;
BEGIN
 SELECT COMPANY_TYPE_ID
 INTO V_SHIP_ID
 FROM COMPANY_TYPE
WHERE LOWER (DESCRIPTION) = LOWER ('Shipper');
 IF (V_SHIP_ID = :NEW.COMPANY_TYPE_ID)
 THEN
  INSERT INTO RS_CONFIG (RS_CONFIG_ID,
                         TC_COMPANY_ID,
                         RS_CONFIG_NAME,
                         IS_VALID,
                         IS_ACTIVE,
                         CREATED_SOURCE_TYPE,
                         CREATED_SOURCE,
                         LAST_UPDATED_SOURCE_TYPE,
                         LAST_UPDATED_SOURCE)
       VALUES (SEQ_RS_CONFIG_ID.NEXTVAL,
               :NEW.COMPANY_ID,
               'Default',
               1,
               1,
               2,
               'LDAP',
               2,
               'LDAP');
  INSERT INTO PERFORMANCE_FACTOR (PERFORMANCE_FACTOR_ID,
                                  TC_COMPANY_ID,
                                  PERFORMANCE_FACTOR_NAME,
                                  PF_TYPE,
                                  DESCRIPTION,
                                  DEFAULT_VALUE,
                                  IS_USE_FLAG,
                                  CREATED_SOURCE_TYPE,
                                  CREATED_SOURCE,
                                  LAST_UPDATED_SOURCE_TYPE,
                                  LAST_UPDATED_SOURCE)
       VALUES (SEQ_PERFORMANCE_FACTOR_ID.NEXTVAL,
               :NEW.COMPANY_ID,
               'ON TIME',
               3,
               'On Time',
               0,
               0,
               2,
               'LDAP',
               2,
               'LDAP');
  INSERT INTO PERFORMANCE_FACTOR (PERFORMANCE_FACTOR_ID,
                                  TC_COMPANY_ID,
                                  PERFORMANCE_FACTOR_NAME,
                                  PF_TYPE,
                                  DESCRIPTION,
                                  DEFAULT_VALUE,
                                  IS_USE_FLAG,
                                  CREATED_SOURCE_TYPE,
                                  CREATED_SOURCE,
                                  LAST_UPDATED_SOURCE_TYPE,
                                  LAST_UPDATED_SOURCE)
       VALUES (SEQ_PERFORMANCE_FACTOR_ID.NEXTVAL,
               :NEW.COMPANY_ID,
               'ACCEPT RATIO',
               2,
               'Accept Rat',
               0,
               0,
               2,
               'LDAP',
               2,
               'LDAP');
  INSERT INTO RS_AREA (TC_COMPANY_ID,
                       RS_AREA_ID,
                       RS_AREA,
                       DESCRIPTION,
                       COMMENTS,
                       CREATED_SOURCE,
                       LAST_UPDATED_SOURCE)
       VALUES (
                 :NEW.COMPANY_ID,
                 RS_AREA_ID_SEQ.NEXTVAL,
                 'RSArealess',
                 'RSArealess',
                 'For Internal Use Only.  Used for shipments that can not be assigned to a RSArea.',
                 '0',
                 '0');
  INSERT INTO ILM_REASON_CODES (REASON_CODE_ID,
                                TYPE,
                                TC_COMPANY_ID,
                                DESCRIPTION)
       VALUES (17,
               'TRLR',
               :NEW.COMPANY_ID,
               'Yard Audit Lock');
  INSERT INTO ILM_REASON_CODES (REASON_CODE_ID,
                                TYPE,
                                TC_COMPANY_ID,
                                DESCRIPTION)
       VALUES (12,
               'TASK',
               :NEW.COMPANY_ID,
               'A trailer already in location');
  INSERT INTO CLAIM_ACTION_CODE (CLAIM_ACTION_CODE,
                                 CLAIM_ACTION_TYPE,
                                 TC_COMPANY_ID,
                                 DESCRIPTION)
     SELECT CLAIM_ACTION_CODE,
            CLAIM_ACTION_TYPE,
            :NEW.COMPANY_ID,
            DESCRIPTION
       FROM CLAIM_SYSTEM_ACTION_CODE SA
      WHERE NOT EXISTS
                   (SELECT 1
                      FROM CLAIM_ACTION_CODE A
                     WHERE A.CLAIM_ACTION_CODE = SA.CLAIM_ACTION_CODE
                           AND A.TC_COMPANY_ID = :NEW.COMPANY_ID);
 END IF;
 INSERT INTO DISTRIBUTION_LIST (TC_COMPANY_ID,
                              DESCRIPTION,
                              IS_PREFERRED_LIST,
                              TC_CONTACT_ID)
    VALUES (:NEW.COMPANY_ID,
            'All Company Carriers',
            1,
            0);
 INSERT INTO DISTRIBUTION_LIST (TC_COMPANY_ID,
                              DESCRIPTION,
                              IS_PREFERRED_LIST,
                              TC_CONTACT_ID)
    VALUES (:NEW.COMPANY_ID,
            'All Lane Carriers',
            2,
            0);
 UPDATE RS_CONFIG
  SET RS_CONFIG_RANK = NULL
WHERE TC_COMPANY_ID = :NEW.COMPANY_ID;
 INSERT INTO INVOICE_RESP_CODE
    VALUES (SEQ_INVOICE_RESP_CODE_ID.NEXTVAL,
            :NEW.COMPANY_ID,
            'ADTR',
            'FAP Auditor',
            0,
            0);
 INSERT INTO INVOICE_RESP_CODE
    VALUES (SEQ_INVOICE_RESP_CODE_ID.NEXTVAL,
            :NEW.COMPANY_ID,
            'CARR',
            'Carrier',
            0,
            1);
 INSERT INTO INVOICE_RESP_CODE
    VALUES (SEQ_INVOICE_RESP_CODE_ID.NEXTVAL,
            :NEW.COMPANY_ID,
            'CRLS',
            'Carrier Relations',
            0,
            0);
 INSERT INTO INVOICE_RESP_CODE
    VALUES (SEQ_INVOICE_RESP_CODE_ID.NEXTVAL,
            :NEW.COMPANY_ID,
            'MNGR',
            'FAP Manager',
            0,
            0);
 INSERT INTO INVOICE_RESP_CODE
    VALUES (SEQ_INVOICE_RESP_CODE_ID.NEXTVAL,
            :NEW.COMPANY_ID,
            'DTRC',
            'Detention Resolution Center',
            0,
            0);
 INSERT INTO PRICE_TYPE_CBO (PRICE_TYPE_ID,
                           COMPANY_ID,
                           AUDIT_CREATED_SOURCE,
                           AUDIT_LAST_UPDATED_SOURCE,
                           OWNED_BY,
                           AUDIT_LAST_UPDATED_DTTM,
                           VERSION,
                           AUDIT_CREATED_DTTM,
                           NAME,
                           DESCRIPTION,
                           AUDIT_LAST_UPDATED_SOURCE_TYPE,
                           AUDIT_CREATED_SOURCE_TYPE,
                           AUDIT_TRANSACTION,
                           AUDIT_PARTY_ID,
                           MARK_FOR_DELETION)
    VALUES (PRICE_TYPE_ID_SEQ.NEXTVAL,
            :NEW.COMPANY_ID,
            '1',
            '1',
            11,
            SYSDATE,
            1,
            SYSDATE,
            'REGULAR',
            '11',
            1,
            11,
            '11',
            11,
            0);
 INSERT INTO PRICE_TYPE_CBO (PRICE_TYPE_ID,
                           COMPANY_ID,
                           AUDIT_CREATED_SOURCE,
                           AUDIT_LAST_UPDATED_SOURCE,
                           OWNED_BY,
                           AUDIT_LAST_UPDATED_DTTM,
                           VERSION,
                           AUDIT_CREATED_DTTM,
                           NAME,
                           DESCRIPTION,
                           AUDIT_LAST_UPDATED_SOURCE_TYPE,
                           AUDIT_CREATED_SOURCE_TYPE,
                           AUDIT_TRANSACTION,
                           AUDIT_PARTY_ID,
                           MARK_FOR_DELETION)
    VALUES (PRICE_TYPE_ID_SEQ.NEXTVAL,
            :NEW.COMPANY_ID,
            '1',
            '1',
            11,
            SYSDATE,
            1,
            SYSDATE,
            'PROMOTIONAL',
            '12',
            1,
            11,
            '11',
            11,
            0);
 IF (:NEW.PARENT_COMPANY_ID = -1)
 THEN
  INSERT INTO OM_SCHED_EVENT (EVENT_ID,
                              SCHEDULED_DTTM,
                              EVENT_OBJECTS,
                              EVENT_TIMESTAMP,
                              OM_CATEGORY,
                              EVENT_TYPE,
                              EVENT_EXP_DATE,
                              EVENT_CNT,
                              EVENT_FREQ_IN_DAYS,
                              EVENT_FREQ_PER_DAY,
                              EXECUTED_DTTM,
                              IS_EXECUTED,
                              EVENT_FREQ_IN_DAY_OF_MONTH)
       VALUES (
                 SEQ_EVENT_ID.NEXTVAL,
                 SYSDATE,
                 '{eventProcessorClass=com.manh.baseservices.util.defaultbasedata.AutoCreateBaseDataScheduler, shipperPK='
                 || TO_CHAR (:NEW.COMPANY_ID)
                 || '}',
                 SYSDATE,
                 null,
                 0,
                 NULL,
                 0,
                 0,
                 0,
                 NULL,
                 0,
                 0);
 END IF;
END;
/

ALTER TRIGGER COMPANY_AI ENABLE;

-- DBTicket DB-3842
CREATE OR REPLACE TRIGGER USER_COPY_TRIGGER
   AFTER INSERT
   ON &1..UCL_USER
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
   WHEN (NEW.COPY_FROM_USER IS NOT NULL)
DECLARE
   E_EMP_ID                UCL_USER.UCL_USER_ID%TYPE;
   E_EMP_DTL_ID            E_EMP_DTL.EMP_DTL_ID%TYPE;
   E_EMP_INC_ID            E_EMP_INC.EMP_INC_ID%TYPE;
   E_EMP_PAY_OVERRIDE_ID   E_EMP_PAY_OVERRIDE.EMP_PAY_OVERRIDE_ID%TYPE;
   E_EMP_REFLECT_STD_ID    E_EMP_REFLECT_STD_CONFIG.EMP_REFLECT_STD_ID%TYPE;
   E_TEAM_EMP_CONFIG_ID    E_TEAM_EMP_CONFIG.TEAM_EMP_CONFIG_ID%TYPE;
BEGIN
   SELECT EMP_ID
     INTO E_EMP_ID
     FROM VIEW_UCL_USER
    WHERE LOGIN_USER_ID = :NEW.COPY_FROM_USER AND IS_ACTIVE = 1;

   IF E_EMP_ID IS NOT NULL
   THEN
      INSERT INTO E_EMP_DTL
         SELECT :NEW.UCL_USER_ID,
                EFF_DATE_TIME,
                EMP_STAT_ID,
                PAY_RATE,
                PAY_SCALE_ID,
                SPVSR_EMP_ID,
                DEPT_ID,
                SHIFT_ID,
                ROLE_ID,
                USER_DEF_FIELD_1,
                USER_DEF_FIELD_2,
                CMNT,
                SYSDATE,
                SYSDATE,
                :NEW.USER_NAME,
                WHSE,
                JOB_FUNC_ID,
                STARTUP_TIME,
                CLEANUP_TIME,
                MISC_TXT_1,
                MISC_TXT_2,
                MISC_NUM_1,
                MISC_NUM_2,
                DFLT_PERF_GOAL,
                VERSION_ID,
                IS_SUPER,
                EMP_DTL_ID_SEQ.NEXTVAL,
                SYSDATE,
                NULL,
                EXCLUDE_AUTO_CICO
           FROM (SELECT *
                   FROM E_EMP_DTL
                  WHERE EFF_DATE_TIME IN
                           (  SELECT MAX (EFF_DATE_TIME)
                                FROM E_EMP_DTL
                            GROUP BY EMP_ID, WHSE
                              HAVING WHSE IN
                                        (SELECT DISTINCT WHSE FROM E_EMP_DTL)
                                     AND EMP_ID = E_EMP_ID)
                        AND EMP_ID = E_EMP_ID);

      INSERT INTO E_EMP_INC
         SELECT E_EMP_INC_ID_SEQ.NEXTVAL,
                :NEW.UCL_USER_ID,
                INC_CODE_ID,
                EFF_BEGIN_DATE,
                EFF_END_DATE,
                WHSE,
                MISC_TXT_1,
                MISC_TXT_2,
                MISC_NUM_1,
                MISC_NUM_2,
                SYSDATE,
                SYSDATE,
                :NEW.USER_NAME,
                VERSION_ID
           FROM (SELECT *
                   FROM E_EMP_INC
                  WHERE EFF_BEGIN_DATE IN
                           (  SELECT MAX (EFF_BEGIN_DATE)
                                FROM E_EMP_INC
                            GROUP BY EMP_ID, WHSE
                              HAVING WHSE IN
                                        (SELECT DISTINCT WHSE FROM E_EMP_INC)
                                     AND EMP_ID = E_EMP_ID)
                        AND EMP_ID = E_EMP_ID);

      INSERT INTO E_EMP_PAY_OVERRIDE
         SELECT E_EMP_PAY_OVERRIDE_ID_SEQ.NEXTVAL,
                :NEW.UCL_USER_ID,
                JOB_FUNC_ID,
                SHIFT_ID,
                PAY_SCALE_ID,
                EFF_BEGIN_DATE,
                EFF_END_DATE,
                WHSE,
                MISC_TXT_1,
                MISC_TXT_2,
                MISC_NUM_1,
                MISC_NUM_2,
                SYSDATE,
                SYSDATE,
                :NEW.USER_NAME,
                VERSION_ID
           FROM (SELECT *
                   FROM E_EMP_PAY_OVERRIDE
                  WHERE EFF_BEGIN_DATE IN
                           (  SELECT MAX (EFF_BEGIN_DATE)
                                FROM E_EMP_PAY_OVERRIDE
                            GROUP BY EMP_ID, WHSE
                              HAVING WHSE IN
                                        (SELECT DISTINCT WHSE
                                           FROM E_EMP_PAY_OVERRIDE)
                                     AND EMP_ID = E_EMP_ID)
                        AND EMP_ID = E_EMP_ID);

      INSERT INTO E_EMP_PERF_GOAL
         SELECT :NEW.UCL_USER_ID,
                ACT_ID,
                EFF_DATE,
                PERF_GOAL,
                ADDNL_PAY_AMT,
                SYSDATE,
                SYSDATE,
                :NEW.USER_NAME,
                WHSE,
                MISC_TXT_1,
                MISC_TXT_2,
                MISC_NUM_1,
                MISC_NUM_2,
                VERSION_ID
           FROM (SELECT *
                   FROM E_EMP_PERF_GOAL
                  WHERE EFF_DATE IN
                           (  SELECT MAX (EFF_DATE)
                                FROM E_EMP_PERF_GOAL
                            GROUP BY EMP_ID, WHSE
                              HAVING WHSE IN
                                        (SELECT DISTINCT WHSE
                                           FROM E_EMP_PERF_GOAL)
                                     AND EMP_ID = E_EMP_ID)
                        AND EMP_ID = E_EMP_ID);

      INSERT INTO E_EMP_REFLECT_STD_CONFIG
         SELECT EMP_REFLECT_STD_ID_SEQ.NEXTVAL,
                :NEW.UCL_USER_ID,
                EFF_BEGIN_DATE,
                EFF_END_DATE,
                DAY_ID,
                START_TIME,
                END_TIME,
                REFLECT_GROUP_CODE,
                WHSE,
                STAT_CODE,
                SYSDATE,
                SYSDATE,
                :NEW.USER_NAME,
                MISC_TXT_1,
                MISC_TXT_2,
                MISC_NUM_1,
                MISC_NUM_2,
                VERSION_ID
           FROM (SELECT *
                   FROM E_EMP_REFLECT_STD_CONFIG
                  WHERE EFF_BEGIN_DATE IN
                           (  SELECT MAX (EFF_BEGIN_DATE)
                                FROM E_EMP_REFLECT_STD_CONFIG
                            GROUP BY EMP_ID, WHSE
                              HAVING WHSE IN
                                        (SELECT DISTINCT WHSE
                                           FROM E_EMP_REFLECT_STD_CONFIG)
                                     AND EMP_ID = E_EMP_ID)
                        AND EMP_ID = E_EMP_ID);

      INSERT INTO E_TEAM_EMP_CONFIG
         SELECT E_TEAM_EMP_CONFIG_ID_SEQ.NEXTVAL,
                :NEW.UCL_USER_ID,
                EFF_BEGIN_DATE,
                EFF_END_DATE,
                DAY_ID,
                TEAM_ID,
                ACT_ID,
                START_TIME,
                END_TIME,
                SYSDATE,
                SYSDATE,
                :NEW.USER_NAME,
                WHSE,
                STAT_CODE,
                MISC_TXT_1,
                MISC_TXT_2,
                MISC_NUM_1,
                MISC_NUM_2,
                VERSION_ID
           FROM (SELECT *
                   FROM E_TEAM_EMP_CONFIG
                  WHERE EFF_BEGIN_DATE IN
                           (  SELECT MAX (EFF_BEGIN_DATE)
                                FROM E_TEAM_EMP_CONFIG
                            GROUP BY EMP_ID, WHSE
                              HAVING WHSE IN
                                        (SELECT DISTINCT WHSE
                                           FROM E_TEAM_EMP_CONFIG)
                                     AND EMP_ID = E_EMP_ID)
                        AND EMP_ID = E_EMP_ID);
   END IF;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      E_EMP_ID := NULL;
END;
/

-- DBTicket DB-4126
CREATE OR REPLACE TRIGGER USER_COPY_TRIGGER
   AFTER INSERT
   ON &1..UCL_USER
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
   WHEN (NEW.COPY_FROM_USER IS NOT NULL)
DECLARE
   E_EMP_ID                UCL_USER.UCL_USER_ID%TYPE;
   E_EMP_DTL_ID            E_EMP_DTL.EMP_DTL_ID%TYPE;
   E_EMP_INC_ID            E_EMP_INC.EMP_INC_ID%TYPE;
   E_EMP_PAY_OVERRIDE_ID   E_EMP_PAY_OVERRIDE.EMP_PAY_OVERRIDE_ID%TYPE;
   E_EMP_REFLECT_STD_ID    E_EMP_REFLECT_STD_CONFIG.EMP_REFLECT_STD_ID%TYPE;
   E_TEAM_EMP_CONFIG_ID    E_TEAM_EMP_CONFIG.TEAM_EMP_CONFIG_ID%TYPE;
BEGIN
   SELECT EMP_ID
     INTO E_EMP_ID
     FROM VIEW_UCL_USER
    WHERE LOGIN_USER_ID = :NEW.COPY_FROM_USER AND IS_ACTIVE = 1;

   IF E_EMP_ID IS NOT NULL
   THEN
      INSERT INTO E_EMP_DTL
         SELECT :NEW.UCL_USER_ID,
                EFF_DATE_TIME,
                EMP_STAT_ID,
                PAY_RATE,
                PAY_SCALE_ID,
                SPVSR_EMP_ID,
                DEPT_ID,
                SHIFT_ID,
                ROLE_ID,
                USER_DEF_FIELD_1,
                USER_DEF_FIELD_2,
                CMNT,
                SYSDATE,
                SYSDATE,
                :NEW.USER_NAME,
                WHSE,
                JOB_FUNC_ID,
                STARTUP_TIME,
                CLEANUP_TIME,
                MISC_TXT_1,
                MISC_TXT_2,
                MISC_NUM_1,
                MISC_NUM_2,
                DFLT_PERF_GOAL,
                VERSION_ID,
                IS_SUPER,
                EMP_DTL_ID_SEQ.NEXTVAL,
                SYSDATE,
                NULL,
                EXCLUDE_AUTO_CICO
           FROM (SELECT *
                   FROM E_EMP_DTL
                  WHERE EFF_DATE_TIME IN
                           (  SELECT MAX (EFF_DATE_TIME)
                                FROM E_EMP_DTL
                            GROUP BY EMP_ID, WHSE, EMP_STAT_ID
                              HAVING WHSE IN
                                        (SELECT DISTINCT WHSE FROM E_EMP_DTL)
                                     AND EMP_ID = E_EMP_ID and EMP_STAT_ID != (SELECT EMP_STAT_ID  FROM E_EMP_STAT_CODE WHERE EMP_STAT_CODE = 'INACTIVE'))
                        AND EMP_ID = E_EMP_ID);

      INSERT INTO E_EMP_INC
         SELECT E_EMP_INC_ID_SEQ.NEXTVAL,
                :NEW.UCL_USER_ID,
                INC_CODE_ID,
                EFF_BEGIN_DATE,
                EFF_END_DATE,
                WHSE,
                MISC_TXT_1,
                MISC_TXT_2,
                MISC_NUM_1,
                MISC_NUM_2,
                SYSDATE,
                SYSDATE,
                :NEW.USER_NAME,
                VERSION_ID
           FROM (SELECT *
                   FROM E_EMP_INC
                  WHERE EFF_BEGIN_DATE IN
                           (  SELECT MAX (EFF_BEGIN_DATE)
                                FROM E_EMP_INC
                            GROUP BY EMP_ID, WHSE
                              HAVING WHSE IN
                                        (SELECT DISTINCT WHSE FROM E_EMP_INC)
                                     AND EMP_ID IN (SELECT EMP_ID FROM E_EMP_DTL WHERE EMP_ID = E_EMP_ID AND EMP_STAT_ID != (SELECT EMP_STAT_ID  FROM E_EMP_STAT_CODE WHERE EMP_STAT_CODE = 'INACTIVE')))
                        AND EMP_ID = E_EMP_ID);

      INSERT INTO E_EMP_PAY_OVERRIDE
         SELECT E_EMP_PAY_OVERRIDE_ID_SEQ.NEXTVAL,
                :NEW.UCL_USER_ID,
                JOB_FUNC_ID,
                SHIFT_ID,
                PAY_SCALE_ID,
                EFF_BEGIN_DATE,
                EFF_END_DATE,
                WHSE,
                MISC_TXT_1,
                MISC_TXT_2,
                MISC_NUM_1,
                MISC_NUM_2,
                SYSDATE,
                SYSDATE,
                :NEW.USER_NAME,
                VERSION_ID
           FROM (SELECT *
                   FROM E_EMP_PAY_OVERRIDE
                  WHERE EFF_BEGIN_DATE IN
                           (  SELECT MAX (EFF_BEGIN_DATE)
                                FROM E_EMP_PAY_OVERRIDE
                            GROUP BY EMP_ID, WHSE
                              HAVING WHSE IN
                                        (SELECT DISTINCT WHSE
                                           FROM E_EMP_PAY_OVERRIDE)
                                     AND EMP_ID IN (SELECT EMP_ID FROM E_EMP_DTL WHERE EMP_ID = E_EMP_ID AND EMP_STAT_ID != (SELECT EMP_STAT_ID  FROM E_EMP_STAT_CODE WHERE EMP_STAT_CODE = 'INACTIVE')))
                        AND EMP_ID = E_EMP_ID);

      INSERT INTO E_EMP_PERF_GOAL
         SELECT :NEW.UCL_USER_ID,
                ACT_ID,
                EFF_DATE,
                PERF_GOAL,
                ADDNL_PAY_AMT,
                SYSDATE,
                SYSDATE,
                :NEW.USER_NAME,
                WHSE,
                MISC_TXT_1,
                MISC_TXT_2,
                MISC_NUM_1,
                MISC_NUM_2,
                VERSION_ID
           FROM (SELECT *
                   FROM E_EMP_PERF_GOAL
                  WHERE EFF_DATE IN
                           (  SELECT MAX (EFF_DATE)
                                FROM E_EMP_PERF_GOAL
                            GROUP BY EMP_ID, WHSE
                              HAVING WHSE IN
                                        (SELECT DISTINCT WHSE
                                           FROM E_EMP_PERF_GOAL)
                                     AND EMP_ID IN (SELECT EMP_ID FROM E_EMP_DTL WHERE EMP_ID = E_EMP_ID AND EMP_STAT_ID != (SELECT EMP_STAT_ID  FROM E_EMP_STAT_CODE WHERE EMP_STAT_CODE = 'INACTIVE')))
                        AND EMP_ID = E_EMP_ID);

      INSERT INTO E_EMP_REFLECT_STD_CONFIG
         SELECT EMP_REFLECT_STD_ID_SEQ.NEXTVAL,
                :NEW.UCL_USER_ID,
                EFF_BEGIN_DATE,
                EFF_END_DATE,
                DAY_ID,
                START_TIME,
                END_TIME,
                REFLECT_GROUP_CODE,
                WHSE,
                STAT_CODE,
                SYSDATE,
                SYSDATE,
                :NEW.USER_NAME,
                MISC_TXT_1,
                MISC_TXT_2,
                MISC_NUM_1,
                MISC_NUM_2,
                VERSION_ID
           FROM (SELECT *
                   FROM E_EMP_REFLECT_STD_CONFIG
                  WHERE EFF_BEGIN_DATE IN
                           (  SELECT MAX (EFF_BEGIN_DATE)
                                FROM E_EMP_REFLECT_STD_CONFIG
                            GROUP BY EMP_ID, WHSE
                              HAVING WHSE IN
                                        (SELECT DISTINCT WHSE
                                           FROM E_EMP_REFLECT_STD_CONFIG)
                                     AND EMP_ID IN (SELECT EMP_ID FROM E_EMP_DTL WHERE EMP_ID = E_EMP_ID AND EMP_STAT_ID != (SELECT EMP_STAT_ID  FROM E_EMP_STAT_CODE WHERE EMP_STAT_CODE = 'INACTIVE')))
                        AND EMP_ID = E_EMP_ID);

      INSERT INTO E_TEAM_EMP_CONFIG
         SELECT E_TEAM_EMP_CONFIG_ID_SEQ.NEXTVAL,
                :NEW.UCL_USER_ID,
                EFF_BEGIN_DATE,
                EFF_END_DATE,
                DAY_ID,
                TEAM_ID,
                ACT_ID,
                START_TIME,
                END_TIME,
                SYSDATE,
                SYSDATE,
                :NEW.USER_NAME,
                WHSE,
                STAT_CODE,
                MISC_TXT_1,
                MISC_TXT_2,
                MISC_NUM_1,
                MISC_NUM_2,
                VERSION_ID
           FROM (SELECT *
                   FROM E_TEAM_EMP_CONFIG
                  WHERE EFF_BEGIN_DATE IN
                           (  SELECT MAX (EFF_BEGIN_DATE)
                                FROM E_TEAM_EMP_CONFIG
                            GROUP BY EMP_ID, WHSE
                              HAVING WHSE IN
                                        (SELECT DISTINCT WHSE
                                           FROM E_TEAM_EMP_CONFIG)
                                     AND EMP_ID IN (SELECT EMP_ID FROM E_EMP_DTL WHERE EMP_ID = E_EMP_ID AND EMP_STAT_ID != (SELECT EMP_STAT_ID  FROM E_EMP_STAT_CODE WHERE EMP_STAT_CODE = 'INACTIVE')))
                        AND EMP_ID = E_EMP_ID);
   END IF;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      E_EMP_ID := NULL;
END;
/

-- DBTicket DB-4446
CREATE OR REPLACE TRIGGER USER_COPY_TRIGGER
   AFTER INSERT
   ON UCL_USER
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
   WHEN (NEW.COPY_FROM_USER IS NOT NULL)
DECLARE
   E_EMP_ID                UCL_USER.UCL_USER_ID%TYPE;
   E_EMP_DTL_ID            E_EMP_DTL.EMP_DTL_ID%TYPE;
   E_EMP_INC_ID            E_EMP_INC.EMP_INC_ID%TYPE;
   E_EMP_PAY_OVERRIDE_ID   E_EMP_PAY_OVERRIDE.EMP_PAY_OVERRIDE_ID%TYPE;
   E_EMP_REFLECT_STD_ID    E_EMP_REFLECT_STD_CONFIG.EMP_REFLECT_STD_ID%TYPE;
   E_TEAM_EMP_CONFIG_ID    E_TEAM_EMP_CONFIG.TEAM_EMP_CONFIG_ID%TYPE;
BEGIN
   SELECT USER_PROFILE_ID
     INTO E_EMP_ID
     FROM USER_PROFILE
    WHERE LOGIN_USER_ID = :NEW.COPY_FROM_USER ;

   IF E_EMP_ID IS NOT NULL
   THEN
      INSERT INTO E_EMP_DTL
         SELECT :NEW.UCL_USER_ID,
                EFF_DATE_TIME,
                EMP_STAT_ID,
                PAY_RATE,
                PAY_SCALE_ID,
                SPVSR_EMP_ID,
                DEPT_ID,
                SHIFT_ID,
                ROLE_ID,
                USER_DEF_FIELD_1,
                USER_DEF_FIELD_2,
                CMNT,
                SYSDATE,
                SYSDATE,
                :NEW.USER_NAME,
                WHSE,
                JOB_FUNC_ID,
                STARTUP_TIME,
                CLEANUP_TIME,
                MISC_TXT_1,
                MISC_TXT_2,
                MISC_NUM_1,
                MISC_NUM_2,
                DFLT_PERF_GOAL,
                VERSION_ID,
                IS_SUPER,
                EMP_DTL_ID_SEQ.NEXTVAL,
                SYSDATE,
                NULL,
                EXCLUDE_AUTO_CICO
           FROM (SELECT *
                   FROM E_EMP_DTL
                  WHERE EFF_DATE_TIME IN
                           (  SELECT MAX (EFF_DATE_TIME)
                                FROM E_EMP_DTL
                            GROUP BY EMP_ID, WHSE, EMP_STAT_ID
                              HAVING WHSE IN
                                        (SELECT DISTINCT WHSE FROM E_EMP_DTL)
                                     AND EMP_ID = E_EMP_ID and EMP_STAT_ID != (SELECT EMP_STAT_ID  FROM E_EMP_STAT_CODE WHERE EMP_STAT_CODE = 'INACTIVE'))
                        AND EMP_ID = E_EMP_ID);

      INSERT INTO E_EMP_INC
         SELECT E_EMP_INC_ID_SEQ.NEXTVAL,
                :NEW.UCL_USER_ID,
                INC_CODE_ID,
                EFF_BEGIN_DATE,
                EFF_END_DATE,
                WHSE,
                MISC_TXT_1,
                MISC_TXT_2,
                MISC_NUM_1,
                MISC_NUM_2,
                SYSDATE,
                SYSDATE,
                :NEW.USER_NAME,
                VERSION_ID
           FROM (SELECT *
                   FROM E_EMP_INC
                  WHERE EFF_BEGIN_DATE IN
                           (  SELECT MAX (EFF_BEGIN_DATE)
                                FROM E_EMP_INC
                            GROUP BY EMP_ID, WHSE
                              HAVING WHSE IN
                                        (SELECT DISTINCT WHSE FROM E_EMP_INC)
                                     AND EMP_ID IN (SELECT EMP_ID FROM E_EMP_DTL WHERE EMP_ID = E_EMP_ID AND EMP_STAT_ID != (SELECT EMP_STAT_ID  FROM E_EMP_STAT_CODE WHERE EMP_STAT_CODE = 'INACTIVE')))
                        AND EMP_ID = E_EMP_ID);

      INSERT INTO E_EMP_PAY_OVERRIDE
         SELECT E_EMP_PAY_OVERRIDE_ID_SEQ.NEXTVAL,
                :NEW.UCL_USER_ID,
                JOB_FUNC_ID,
                SHIFT_ID,
                PAY_SCALE_ID,
                EFF_BEGIN_DATE,
                EFF_END_DATE,
                WHSE,
                MISC_TXT_1,
                MISC_TXT_2,
                MISC_NUM_1,
                MISC_NUM_2,
                SYSDATE,
                SYSDATE,
                :NEW.USER_NAME,
                VERSION_ID
           FROM (SELECT *
                   FROM E_EMP_PAY_OVERRIDE
                  WHERE EFF_BEGIN_DATE IN
                           (  SELECT MAX (EFF_BEGIN_DATE)
                                FROM E_EMP_PAY_OVERRIDE
                            GROUP BY EMP_ID, WHSE
                              HAVING WHSE IN
                                        (SELECT DISTINCT WHSE
                                           FROM E_EMP_PAY_OVERRIDE)
                                     AND EMP_ID IN (SELECT EMP_ID FROM E_EMP_DTL WHERE EMP_ID = E_EMP_ID AND EMP_STAT_ID != (SELECT EMP_STAT_ID  FROM E_EMP_STAT_CODE WHERE EMP_STAT_CODE = 'INACTIVE')))
                        AND EMP_ID = E_EMP_ID);

      INSERT INTO E_EMP_PERF_GOAL
         SELECT :NEW.UCL_USER_ID,
                ACT_ID,
                EFF_DATE,
                PERF_GOAL,
                ADDNL_PAY_AMT,
                SYSDATE,
                SYSDATE,
                :NEW.USER_NAME,
                WHSE,
                MISC_TXT_1,
                MISC_TXT_2,
                MISC_NUM_1,
                MISC_NUM_2,
                VERSION_ID
           FROM (SELECT *
                   FROM E_EMP_PERF_GOAL
                  WHERE EFF_DATE IN
                           (  SELECT MAX (EFF_DATE)
                                FROM E_EMP_PERF_GOAL
                            GROUP BY EMP_ID, WHSE
                              HAVING WHSE IN
                                        (SELECT DISTINCT WHSE
                                           FROM E_EMP_PERF_GOAL)
                                     AND EMP_ID IN (SELECT EMP_ID FROM E_EMP_DTL WHERE EMP_ID = E_EMP_ID AND EMP_STAT_ID != (SELECT EMP_STAT_ID  FROM E_EMP_STAT_CODE WHERE EMP_STAT_CODE = 'INACTIVE')))
                        AND EMP_ID = E_EMP_ID);

      INSERT INTO E_EMP_REFLECT_STD_CONFIG
         SELECT EMP_REFLECT_STD_ID_SEQ.NEXTVAL,
                :NEW.UCL_USER_ID,
                EFF_BEGIN_DATE,
                EFF_END_DATE,
                DAY_ID,
                START_TIME,
                END_TIME,
                REFLECT_GROUP_CODE,
                WHSE,
                STAT_CODE,
                SYSDATE,
                SYSDATE,
                :NEW.USER_NAME,
                MISC_TXT_1,
                MISC_TXT_2,
                MISC_NUM_1,
                MISC_NUM_2,
                VERSION_ID
           FROM (SELECT *
                   FROM E_EMP_REFLECT_STD_CONFIG
                  WHERE EFF_BEGIN_DATE IN
                           (  SELECT MAX (EFF_BEGIN_DATE)
                                FROM E_EMP_REFLECT_STD_CONFIG
                            GROUP BY EMP_ID, WHSE
                              HAVING WHSE IN
                                        (SELECT DISTINCT WHSE
                                           FROM E_EMP_REFLECT_STD_CONFIG)
                                     AND EMP_ID IN (SELECT EMP_ID FROM E_EMP_DTL WHERE EMP_ID = E_EMP_ID AND EMP_STAT_ID != (SELECT EMP_STAT_ID  FROM E_EMP_STAT_CODE WHERE EMP_STAT_CODE = 'INACTIVE')))
                        AND EMP_ID = E_EMP_ID);

      INSERT INTO E_TEAM_EMP_CONFIG
         SELECT E_TEAM_EMP_CONFIG_ID_SEQ.NEXTVAL,
                :NEW.UCL_USER_ID,
                EFF_BEGIN_DATE,
                EFF_END_DATE,
                DAY_ID,
                TEAM_ID,
                ACT_ID,
                START_TIME,
                END_TIME,
                SYSDATE,
                SYSDATE,
                :NEW.USER_NAME,
                WHSE,
                STAT_CODE,
                MISC_TXT_1,
                MISC_TXT_2,
                MISC_NUM_1,
                MISC_NUM_2,
                VERSION_ID
           FROM (SELECT *
                   FROM E_TEAM_EMP_CONFIG
                  WHERE EFF_BEGIN_DATE IN
                           (  SELECT MAX (EFF_BEGIN_DATE)
                                FROM E_TEAM_EMP_CONFIG
                            GROUP BY EMP_ID, WHSE
                              HAVING WHSE IN
                                        (SELECT DISTINCT WHSE
                                           FROM E_TEAM_EMP_CONFIG)
                                     AND EMP_ID IN (SELECT EMP_ID FROM E_EMP_DTL WHERE EMP_ID = E_EMP_ID AND EMP_STAT_ID != (SELECT EMP_STAT_ID  FROM E_EMP_STAT_CODE WHERE EMP_STAT_CODE = 'INACTIVE')))
                        AND EMP_ID = E_EMP_ID);
   END IF;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      E_EMP_ID := NULL;
END;
/

exit;

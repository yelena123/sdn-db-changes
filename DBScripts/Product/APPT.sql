-- DBTicket APPT-143

alter TABLE APPOINTMENT_REPORT move tablespace APPT_TXN_DT_TBS ;
alter TABLE APPT_OBJECT_FEASIBLE_APPT move tablespace APPT_TXN_DT_TBS ;
alter TABLE APPT_OBJECT_INFEASIBLE_APPT move tablespace APPT_TXN_DT_TBS ;
alter TABLE APPT_OBJECT_PK move tablespace APPT_TXN_DT_TBS ;
alter TABLE APPT_OBJECT_SLOT_GROUP_SLOT move tablespace APPT_TXN_DT_TBS ;
alter TABLE APPT_SETUP_UPLOAD move tablespace APPT_TXN_DT_TBS ;
alter TABLE APPT_UPLOAD_STATUS move tablespace APPT_TXN_DT_TBS ;
alter TABLE APPT_UPLOAD_TYPE move  tablespace APPT_TXN_DT_TBS ;
alter TABLE ILM_ACTIVITY_TYPES move  tablespace APPT_TXN_DT_TBS ;
alter TABLE ILM_APPOINTMENTS move  tablespace APPT_TXN_DT_TBS ;
alter TABLE ILM_APPOINTMENTS_CHARGEBACK move  tablespace APPT_TXN_DT_TBS ;
alter TABLE ILM_APPOINTMENT_CREATION_TYPE move  tablespace APPT_TXN_DT_TBS ;
alter TABLE ILM_APPOINTMENT_EVENTS move  tablespace APPT_TXN_DT_TBS ;
alter TABLE ILM_APPOINTMENT_OBJECTS move  tablespace APPT_TXN_DT_TBS ;
alter TABLE ILM_APPOINTMENT_STATUS move  tablespace APPT_TXN_DT_TBS ;
alter TABLE ILM_APPOINTMENT_UTILIZATION move  tablespace APPT_TXN_DT_TBS ;
alter TABLE ILM_APPT_CHARGE_BACK_CODES move  tablespace APPT_TXN_DT_TBS ;
alter TABLE ILM_APPT_CUSTOM_ATTRIBUTE move  tablespace APPT_TXN_DT_TBS ;
alter TABLE ILM_APPT_DOCKS move  tablespace APPT_TXN_DT_TBS ;
alter TABLE ILM_APPT_EQUIPMENTS move  tablespace APPT_TXN_DT_TBS ;
alter TABLE ILM_APPT_LOAD_CONFIG move  tablespace APPT_TXN_DT_TBS ;
alter TABLE ILM_APPT_NOTES move  tablespace APPT_TXN_DT_TBS ;
alter TABLE ILM_APPT_SIZES move  tablespace APPT_TXN_DT_TBS ;
alter TABLE ILM_APPT_SLOT_UTILIZATION move  tablespace APPT_TXN_DT_TBS ;
alter TABLE ILM_APPT_UTIL_NEW move  tablespace APPT_TXN_DT_TBS ;
alter TABLE ILM_REQUESTOR_TYPE move  tablespace APPT_TXN_DT_TBS ;
alter TABLE SHIFT move  tablespace APPT_TXN_DT_TBS ;
alter TABLE SLOT_RECOMMENDATION_CRITERIA move  tablespace APPT_TXN_DT_TBS ;
alter TABLE SLOT_TOTAL_CAP_USAGE_DETAIL move  tablespace APPT_TXN_DT_TBS ;
alter TABLE SLOT_USAGE_DETAIL move  tablespace APPT_TXN_DT_TBS ;
alter TABLE UN_ASSIGNED_APPT_OBJECT move  tablespace APPT_TXN_DT_TBS ;

declare
v_stmt varchar2(200);
begin
for l_index in (
select index_name from user_indexes 
where table_name in ('APPOINTMENT_REPORT',
'APPT_OBJECT_FEASIBLE_APPT',
'APPT_OBJECT_INFEASIBLE_APPT',
'APPT_OBJECT_PK',
'APPT_OBJECT_SLOT_GROUP_SLOT',
'APPT_SETUP_UPLOAD',
'APPT_UPLOAD_STATUS',
'APPT_UPLOAD_TYPE',
'ILM_ACTIVITY_TYPES',
'ILM_APPOINTMENTS',
'ILM_APPOINTMENTS_CHARGEBACK',
'ILM_APPOINTMENT_CREATION_TYPE',
'ILM_APPOINTMENT_EVENTS',
'ILM_APPOINTMENT_OBJECTS',
'ILM_APPOINTMENT_STATUS',
'ILM_APPOINTMENT_UTILIZATION',
'ILM_APPT_CHARGE_BACK_CODES',
'ILM_APPT_CUSTOM_ATTRIBUTE',
'ILM_APPT_DOCKS',
'ILM_APPT_EQUIPMENTS',
'ILM_APPT_LOAD_CONFIG',
'ILM_APPT_NOTES',
'ILM_APPT_SIZES',
'ILM_APPT_SLOT_UTILIZATION',
'ILM_APPT_UTIL_NEW',
'ILM_REQUESTOR_TYPE',
'SHIFT',
'SLOT_RECOMMENDATION_CRITERIA',
'SLOT_TOTAL_CAP_USAGE_DETAIL',
'SLOT_USAGE_DETAIL',
'UN_ASSIGNED_APPT_OBJECT')
 and index_type='NORMAL' order by 1)
loop
v_stmt := 'alter index ' || l_index.index_name || ' rebuild tablespace APPT_TXN_IDX_TBS ';
dbms_output.put_line(v_stmt);
execute immediate (v_stmt);
end loop;
end;
/

ALTER TABLE APPT_SETUP_UPLOAD MOVE LOB (UPLOADED_FILE,PROCESSED_FILE) STORE AS (TABLESPACE APPT_TXN_IDX_TBS);



-- DBTicket APPT-137

MERGE INTO LABEL L USING
(
   SELECT
   'BD' BUNDLE_NAME, 'Priority Schedule' KEY
   FROM DUAL
)
B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN
INSERT
(LABEL_ID,KEY,VALUE,BUNDLE_NAME)
VALUES
  (
    SEQ_LABEL_ID.NEXTVAL,
    'Priority Schedule',
    'Priority Schedule',
    'BD'
  );


MERGE INTO LABEL L USING
(
   SELECT
   'BD' BUNDLE_NAME, 'EditPriority' KEY
   FROM DUAL
)
B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN
INSERT
(LABEL_ID,KEY,VALUE,BUNDLE_NAME)
VALUES
  (
    SEQ_LABEL_ID.NEXTVAL,
    'EditPriority',
    'Edit Priority',
    'BD'
  );


COMMIT;

EXEC MANH_CREATE_SYNONYM('PRIORITIZATION_SCHEDULE');
EXEC MANH_CREATE_SYNONYM('SEQ_PRIOR_SCHDL_ID');


-- DBTicket APPT-139

UPDATE label
SET value      = 'Allow change of appointment times when related objects are changed'
--previous value was 'Allow change of appointment times when related objects are changeddddd'
WHERE KEY      = 'allow_change_of_appointment_times'
AND bundle_name='ParamDef'; 

UPDATE label
SET value      = 'Use Lightweight Shipment For Appointment Scheduling'
--previous value was 'Use Lightweight Shipment For Appointment Schedulinggggg '
WHERE KEY      = 'Use_Lightweight_Shipment_For_Appt_Scheduling'
AND bundle_name='ParamDef';

UPDATE param_def
SET value_generator_class = 'com.manh.appointment.ui.finitevalue.ILMAppointmentDataSync'
  -- previous value was 'com.manh.yms.appointment.finitevalue.ILMAppointmentDataSync'
WHERE param_group_id = 'ILM'
AND param_def_id     = 'allow_change_of_appointment_times'; 

UPDATE param_def
SET value_generator_class = 'com.manh.appointment.ui.finitevalue.AppointmentIdGenerationFiniteValue'
  -- previous value was 'com.manh.yms.appointment.finitevalue.AppointmentIdGenerationFiniteValue'
WHERE param_group_id = 'ILM'
AND param_def_id     = 'appt_id_generator'; 

UPDATE param_def
SET value_generator_class = 'com.manh.cboframework.fv.SizeUOM'
  -- previous value was 'com.manh.common.fv.SizeUOM'
WHERE param_group_id = 'ILM'
AND param_def_id     = 'size_uom'; 

UPDATE param_def
SET value_generator_class = 'com.manh.appointment.ui.finitevalue.ILMAnchorTypes'
  -- previous value was 'com.manh.yms.appointment.finitevalue.ILMAnchorTypes'
WHERE param_group_id = 'ILM'
AND param_def_id     = 'anchor_appointments'; 

UPDATE param_def
SET value_generator_class = 'com.manh.appointment.ui.finitevalue.ILMAppointmentType'
  -- previous value was 'com.manh.yms.appointment.finitevalue.ILMAppointmentType'
WHERE param_group_id = 'ILM'
AND param_def_id     = 'driver_is_required_for_appointment_types'; 

UPDATE param_def
SET value_generator_class = 'com.manh.appointment.ui.finitevalue.ILMAppointmentStatus'
  -- previous value was 'com.manh.yms.appointment.finitevalue.ILMAppointmentStatus'
WHERE param_group_id = 'ILM'
AND param_def_id     = 'appt_edit_end_stat'; 

UPDATE param_def
SET value_generator_class = 'com.manh.appointment.ui.finitevalue.ILMAppointmentStatus'
  -- previous value was 'com.manh.yms.appointment.finitevalue.ILMAppointmentStatus'
WHERE param_group_id = 'ILM'
AND param_def_id     = 'appt_edit_end_stat_for_carrier_vendor'; 


COMMIT;


-- DBTicket APPT-157

ALTER TABLE ILM_APPOINTMENTS ADD ( ACTUAL_CHECKIN_DTTM TIMESTAMP,SCHEDULED_DTTM TIMESTAMP);

COMMENT ON column ILM_APPOINTMENTS.ACTUAL_CHECKIN_DTTM
IS
  'Used to store the check-in time of the trailer';

  COMMENT ON column ILM_APPOINTMENTS.SCHEDULED_DTTM
IS
  'Used to store the original scheduled time of the appointment';




-- DBTicket APPT-179


MERGE INTO RESOURCES A
     USING (SELECT '/appointment/ui/jsf/appointmentList.xhtml' AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsf/appointmentList.xhtml',
               'APPT',
               1,
               NULL);

			   
			   
MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'VAPT' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI='/appointment/ui/jsf/appointmentList.xhtml' and RESOURCES.MODULE='APPT') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT resource_id
                  FROM resources
                 WHERE URI='/appointment/ui/jsf/appointmentList.xhtml' and MODULE='APPT'),
               'VAPT');


COMMIT;


-- DBTicket APPT-180

----MERGE INTO XSCREEN L
----     USING (SELECT 'Appointment Calendar' NAME, '240010' XSCREEN_ID FROM DUAL) B
----        ON (L.NAME = B.NAME AND L.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XSCREEN_ID,
----               NAME,
----               DESCRIPTION,
----               SCREEN_TYPE,
----               SCREEN_VERSION,
----               SCREEN_MODE,
----               IS_RESIZABLE,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               CREATED_DTTM,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               LAST_UPDATED_DTTM,
----               DEFAULT_X,
----               DEFAULT_Y,
----               DEFAULT_WIDTH,
----               DEFAULT_HEIGHT,
----               CUSTOM_ATTRIB_OBJ_TYPE)
----       VALUES (240010,
----               'Appointment Calendar',
----               'Appointment Calendar',
----               1,
----               2,
----               0,
----               1,
----               NULL,
----               1,
----               SYSTIMESTAMP,
----               NULL,
----               1,
----               SYSTIMESTAMP,
----               125,
----               125,
----               600,
----               400,
----               NULL);



MERGE INTO RESOURCES L
     USING (SELECT '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/facilityChanged'
                      URI,
                   2 URI_TYPE_ID,
                   'GET' HTTP_METHOD
              FROM DUAL) B
        ON (    L.URI = B.URI
            AND L.URI_TYPE_ID = B.URI_TYPE_ID
            AND L.HTTP_METHOD = B.HTTP_METHOD)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/facilityChanged',
                 'APPT',
                 2,
                 'GET');


MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'VIEWSTOREORDER' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI LIKE
                      '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/facilityChanged'
                   AND HTTP_METHOD = 'GET') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/facilityChanged'
                         AND URI_TYPE_ID = 2
                         AND HTTP_METHOD = 'GET'),
                 'VAPT');


MERGE INTO RESOURCES L
     USING (SELECT '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/getCapacityUtilization'
                      URI,
                   2 URI_TYPE_ID,
                   'GET' HTTP_METHOD
              FROM DUAL) B
        ON (    L.URI = B.URI
            AND L.URI_TYPE_ID = B.URI_TYPE_ID
            AND L.HTTP_METHOD = B.HTTP_METHOD)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/getCapacityUtilization',
                 'APPT',
                 2,
                 'GET');



MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'VIEWSTOREORDER' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI LIKE
                      '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/getCapacityUtilization'
                   AND HTTP_METHOD = 'GET') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/getCapacityUtilization'
                         AND URI_TYPE_ID = 2
                         AND HTTP_METHOD = 'GET'),
                 'VAPT');


MERGE INTO RESOURCES L
     USING (SELECT '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/getAppointmentsOfSlot'
                      URI,
                   2 URI_TYPE_ID,
                   'GET' HTTP_METHOD
              FROM DUAL) B
        ON (    L.URI = B.URI
            AND L.URI_TYPE_ID = B.URI_TYPE_ID
            AND L.HTTP_METHOD = B.HTTP_METHOD)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/getAppointmentsOfSlot',
                 'APPT',
                 2,
                 'GET');



MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'VIEWSTOREORDER' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI LIKE
                      '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/getAppointmentsOfSlot'
                   AND HTTP_METHOD = 'GET') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/getAppointmentsOfSlot'
                         AND URI_TYPE_ID = 2
                         AND HTTP_METHOD = 'GET'),
                 'VAPT');


COMMIT;


-- DBTicket APPT-189

insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) values((select NVL(max(XMENU_ITEM_ID),240000)+1 from XMENU_ITEM where TO_CHAR(XMENU_ITEM_ID) like '2400%'),'Appointments','Appointments','/appointment/ui/jsf/appointmentList.jsflps','','default-icon','#002E5F',5,1,'Yard Management',0);
insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) values((select NVL(max(XMENU_ITEM_ID),240000)+1 from XMENU_ITEM where TO_CHAR(XMENU_ITEM_ID) like '2400%'),'Add Appointment','Add Appointment','/appointment/ui/jsf/scheduleAppointment.jsflps','','default-icon','#002E5F',5,1,'Yard Management',0);
insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) values((select NVL(max(XMENU_ITEM_ID),240000)+1 from XMENU_ITEM where TO_CHAR(XMENU_ITEM_ID) like '2400%'),'Appointment Calendar','Appointment Calendar','/appointment/ui/jsp/appointmentPlanning.jsp? calendarType=YARDCALENDAR','','default-icon','#002E5F',5,1,'Yard Management',0);
insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) values((select NVL(max(XMENU_ITEM_ID),240000)+1 from XMENU_ITEM where TO_CHAR(XMENU_ITEM_ID) like '2400%'),'Appointment Report','Appointment Report','/appointment/ui/jsp/appointmentReport.jsp? calendarType=YARDCALENDAR','','default-icon','#002E5F',5,1,'Yard Management',0);
insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) values((select NVL(max(XMENU_ITEM_ID),240000)+1 from XMENU_ITEM where TO_CHAR(XMENU_ITEM_ID) like '2400%'),'Slot-Appointment Infeasibility','Slot-Appointment Infeasibility','/appointment/infeasibility/jsp/SlotAppointmentTypeInfeasibilitySetup.jsp','','default-icon','#002E5F',5,1,'Business-Rules',0);
insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) values((select NVL(max(XMENU_ITEM_ID),240000)+1 from XMENU_ITEM where TO_CHAR(XMENU_ITEM_ID) like '2400%'),'Slot-Dock Infeasibility','Slot-Dock Infeasibility','/appointment/infeasibility/jsp/SlotDockInfeasibilitySetup.jsp','','default-icon','#002E5F',5,1,'Business-Rules',0);
insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) values((select NVL(max(XMENU_ITEM_ID),240000)+1 from XMENU_ITEM where TO_CHAR(XMENU_ITEM_ID) like '2400%'),'Slot-Equipment Infeasibility','Slot-Equipment Infeasibility','/appointment/infeasibility/jsp/SlotEquipmentInfeasibilitySetup.jsp','','default-icon','#002E5F',5,1,'Business-Rules',0);
insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) values((select NVL(max(XMENU_ITEM_ID),240000)+1 from XMENU_ITEM where TO_CHAR(XMENU_ITEM_ID) like '2400%'),'Slot-Load Configuration Infeasibility','Slot-Load Configuration Infeasibility','/appointment/infeasibility/jsp/SlotLoadConfigTypeInfeasibilitySetup.jsp','','default-icon','#002E5F',5,1,'Business-Rules',0);
insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) values((select NVL(max(XMENU_ITEM_ID),240000)+1 from XMENU_ITEM where TO_CHAR(XMENU_ITEM_ID) like '2400%'),'Slot-Prod. Class Infeasibility','Slot-Prod. Class Infeasibility','/appointment/infeasibility/jsp/SlotProductClassInfeasibilitySetup.jsp','','default-icon','#002E5F',5,1,'Business-Rules',0);
insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) values((select NVL(max(XMENU_ITEM_ID),240000)+1 from XMENU_ITEM where TO_CHAR(XMENU_ITEM_ID) like '2400%'),'Slot-Prot. Level Infeasibility','Slot-Prot. Level Infeasibility','/appointment/infeasibility/jsp/SlotProtectionLevelInfeasibilitySetup.jsp','','default-icon','#002E5F',5,1,'Business-Rules',0);
insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) values((select NVL(max(XMENU_ITEM_ID),240000)+1 from XMENU_ITEM where TO_CHAR(XMENU_ITEM_ID) like '2400%'),'Appointment Setup','Appointment Setup','/appointment/facility/FacilityList.jsflps','','default-icon','#002E5F',0,1,'ExtendedEnterprise',0);
insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) values((select NVL(max(XMENU_ITEM_ID),240000)+1 from XMENU_ITEM where TO_CHAR(XMENU_ITEM_ID) like '2400%'),'Appointments - Archive','Appointments - Archive','/appointment/ui/jsf/appointmentList.jsflps?isArchive=true','','default-icon','#002E5F',5,1,'Yard Management',0);
insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) values((select NVL(max(XMENU_ITEM_ID),240000)+1 from XMENU_ITEM where TO_CHAR(XMENU_ITEM_ID) like '2400%'),'Appointments','Appointments','/appointment/ui/jsf/appointmentList.jsflps','','default-icon','#002E5F',5,1,'Logistics Gateway',0);
insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) values((select NVL(max(XMENU_ITEM_ID),240000)+1 from XMENU_ITEM where TO_CHAR(XMENU_ITEM_ID) like '2400%'),'Appointments VCP','Appointments VCP','/appointment/ui/jsf/appointmentList.jsflps','','default-icon','#002E5F',5,1,'Transportation Lifecycle Management',0);
insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) values((select NVL(max(XMENU_ITEM_ID),240000)+1 from XMENU_ITEM where TO_CHAR(XMENU_ITEM_ID) like '2400%'),'Add Appointment VCP','Add Appointment VCP','/appointment/ui/jsf/scheduleAppointment.jsflps','','default-icon','#002E5F',5,1,'Transportation Lifecycle Management',0);

insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where NAME='Appointments' and NAVIGATION_KEY='/appointment/ui/jsf/appointmentList.jsflps' and BASE_SECTION_NAME='Yard Management'),'VAPT');	
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where NAME='Add Appointment' and NAVIGATION_KEY='/appointment/ui/jsf/scheduleAppointment.jsflps' and BASE_SECTION_NAME='Yard Management'),'AAPT');
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where NAME='Appointment Calendar' and NAVIGATION_KEY='/appointment/ui/jsp/appointmentPlanning.jsp? calendarType=YARDCALENDAR' and BASE_SECTION_NAME='Yard Management'),'VCLD');
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where NAME='Appointment Report' and NAVIGATION_KEY='/appointment/ui/jsp/appointmentReport.jsp? calendarType=YARDCALENDAR' and BASE_SECTION_NAME='Yard Management'),'VDS');
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where NAME='Slot-Appointment Infeasibility' and NAVIGATION_KEY='/appointment/infeasibility/jsp/SlotAppointmentTypeInfeasibilitySetup.jsp' and BASE_SECTION_NAME='Business-Rules'),'VAPT');
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where NAME='Slot-Dock Infeasibility' and NAVIGATION_KEY='/appointment/infeasibility/jsp/SlotDockInfeasibilitySetup.jsp' and BASE_SECTION_NAME='Business-Rules'),'VBR');
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where NAME='Slot-Equipment Infeasibility' and NAVIGATION_KEY='/appointment/infeasibility/jsp/SlotEquipmentInfeasibilitySetup.jsp' and BASE_SECTION_NAME='Business-Rules')                                  ,'VBR');
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where NAME='Slot-Load Configuration Infeasibility' and NAVIGATION_KEY='/appointment/infeasibility/jsp/SlotLoadConfigTypeInfeasibilitySetup.jsp' and BASE_SECTION_NAME='Business-Rules')                    ,'VBR');
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where NAME='Slot-Prod. Class Infeasibility' and NAVIGATION_KEY='/appointment/infeasibility/jsp/SlotProductClassInfeasibilitySetup.jsp' and BASE_SECTION_NAME='Business-Rules')                             ,'VBR');
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where NAME='Slot-Prot. Level Infeasibility' and NAVIGATION_KEY='/appointment/infeasibility/jsp/SlotProtectionLevelInfeasibilitySetup.jsp' and BASE_SECTION_NAME='Business-Rules')                         ,'VBR');
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where NAME='Appointment Setup' and NAVIGATION_KEY='/appointment/facility/FacilityList.jsflps' and BASE_SECTION_NAME='ExtendedEnterprise')                                                                  ,'VBR');
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where NAME='Appointments - Archive' and NAVIGATION_KEY='/appointment/ui/jsf/appointmentList.jsflps?isArchive=true' and BASE_SECTION_NAME='Yard Management')                                                ,'AAST');
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where NAME='Appointments' and NAVIGATION_KEY='/appointment/ui/jsf/appointmentList.jsflps' and BASE_SECTION_NAME='Logistics Gateway')                                                                       ,'VAPT');
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where NAME='Appointments VCP' and NAVIGATION_KEY='/appointment/ui/jsf/appointmentList.jsflps' and BASE_SECTION_NAME='Transportation Lifecycle Management')                                                ,'VAPT');
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where NAME='Add Appointment VCP' and NAVIGATION_KEY='/appointment/ui/jsf/scheduleAppointment.jsflps' and BASE_SECTION_NAME='Transportation Lifecycle Management' )                                         ,'VAPT');

Commit;



-- DBTicket APPT-190

DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_REQUESTOR_TYPE' and column_name='CREATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_REQUESTOR_TYPE ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/


DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_REQUESTOR_TYPE' and column_name='LAST_UPDATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_REQUESTOR_TYPE ADD (LAST_UPDATED_DTTM TIMESTAMP)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/



DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_APPT_UTIL_NEW' and column_name='CREATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_APPT_UTIL_NEW ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/


DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_APPT_UTIL_NEW' and column_name='LAST_UPDATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_APPT_UTIL_NEW ADD (LAST_UPDATED_DTTM TIMESTAMP)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/



DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_APPT_SLOT_UTILIZATION' and column_name='CREATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_APPT_SLOT_UTILIZATION ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/


DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_APPT_SLOT_UTILIZATION' and column_name='LAST_UPDATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_APPT_SLOT_UTILIZATION ADD (LAST_UPDATED_DTTM TIMESTAMP)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/



DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_APPT_SIZES' and column_name='CREATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_APPT_SIZES ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/


DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_APPT_SIZES' and column_name='LAST_UPDATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_APPT_SIZES ADD (LAST_UPDATED_DTTM TIMESTAMP)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/



DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_APPT_LOAD_CONFIG' and column_name='CREATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_APPT_LOAD_CONFIG ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/


DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_APPT_LOAD_CONFIG' and column_name='LAST_UPDATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_APPT_LOAD_CONFIG ADD (LAST_UPDATED_DTTM TIMESTAMP)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/



DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_APPT_CUSTOM_ATTRIBUTE' and column_name='CREATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_APPT_CUSTOM_ATTRIBUTE ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/


DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_APPT_CUSTOM_ATTRIBUTE' and column_name='LAST_UPDATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_APPT_CUSTOM_ATTRIBUTE ADD (LAST_UPDATED_DTTM TIMESTAMP)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/


DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_APPT_CHARGE_BACK_CODES' and column_name='CREATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_APPT_CHARGE_BACK_CODES ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/


DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_APPT_CHARGE_BACK_CODES' and column_name='LAST_UPDATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_APPT_CHARGE_BACK_CODES ADD (LAST_UPDATED_DTTM TIMESTAMP)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/

DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_APPOINTMENT_UTILIZATION' and column_name='CREATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_APPOINTMENT_UTILIZATION ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/


DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_APPOINTMENT_UTILIZATION' and column_name='LAST_UPDATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_APPOINTMENT_UTILIZATION ADD (LAST_UPDATED_DTTM TIMESTAMP)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/

DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_APPOINTMENT_STATUS' and column_name='CREATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_APPOINTMENT_STATUS ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/


DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_APPOINTMENT_STATUS' and column_name='LAST_UPDATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_APPOINTMENT_STATUS ADD (LAST_UPDATED_DTTM TIMESTAMP)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/

DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_APPOINTMENT_OBJECTS' and column_name='CREATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_APPOINTMENT_OBJECTS ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/


DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_APPOINTMENT_OBJECTS' and column_name='LAST_UPDATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_APPOINTMENT_OBJECTS ADD (LAST_UPDATED_DTTM TIMESTAMP)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/

DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_APPOINTMENT_CREATION_TYPE' and column_name='CREATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_APPOINTMENT_CREATION_TYPE ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/


DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_APPOINTMENT_CREATION_TYPE' and column_name='LAST_UPDATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_APPOINTMENT_CREATION_TYPE ADD (LAST_UPDATED_DTTM TIMESTAMP)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/


DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_ACTIVITY_TYPES' and column_name='CREATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_ACTIVITY_TYPES ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/


DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_ACTIVITY_TYPES' and column_name='LAST_UPDATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_ACTIVITY_TYPES ADD (LAST_UPDATED_DTTM TIMESTAMP)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/



DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_APPOINTMENTS' and column_name='CREATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_APPOINTMENTS ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/


DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_APPOINTMENTS' and column_name='LAST_UPDATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_APPOINTMENTS ADD (LAST_UPDATED_DTTM TIMESTAMP)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/



CREATE OR REPLACE TRIGGER ILM_REQUESTOR_LST_UPDT_DT_TRG
  BEFORE UPDATE ON ILM_REQUESTOR_TYPE
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
  :new.CREATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER ILM_APPT_UTIL__LST_UPDT_DT_TRG
  BEFORE UPDATE ON ILM_APPT_UTIL_NEW
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
  :new.CREATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER ILM_APPT_SLOT__LST_UPDT_DT_TRG
  BEFORE UPDATE ON ILM_APPT_SLOT_UTILIZATION
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
  :new.CREATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER ILM_APPT_SIZES_LST_UPDT_DT_TRG
  BEFORE UPDATE ON ILM_APPT_SIZES
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
  :new.CREATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER ILM_APPT_LOAD__LST_UPDT_DT_TRG
  BEFORE UPDATE ON ILM_APPT_LOAD_CONFIG
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
  :new.CREATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER ILM_APPT_CUSTO_LST_UPDT_DT_TRG
  BEFORE UPDATE ON ILM_APPT_CUSTOM_ATTRIBUTE
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
  :new.CREATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER ILM_APPT_CHARG_LST_UPDT_DT_TRG
  BEFORE UPDATE ON ILM_APPT_CHARGE_BACK_CODES
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
  :new.CREATED_DTTM := CURRENT_TIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER ILM_ACTIVITY_T_LST_UPDT_DT_TRG
  BEFORE UPDATE ON ILM_ACTIVITY_TYPES
  FOR EACH ROW
begin
  :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP;
  :new.CREATED_DTTM := CURRENT_TIMESTAMP;
END;
/


CREATE OR REPLACE TRIGGER  ILM_APPTMENTS_LST_UPDT_DT_TRG 
BEFORE UPDATE ON ILM_APPOINTMENTS 
 FOR EACH ROW 
 BEGIN 
 :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP; 
 :new.CREATED_DTTM := CURRENT_TIMESTAMP; 
 END;
 /
 
-- DBTicket APPT-187
 
MERGE INTO RESOURCES L
     USING (SELECT '/appointment/admin/jsp/AppointmentImport.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/admin/jsp/AppointmentImport.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/admin/parameter/jsp/EditAppointmentParameters.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/admin/parameter/jsp/EditAppointmentParameters.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/admin/parameter/jsp/EditAppointmentParametersProcess.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/admin/parameter/jsp/EditAppointmentParametersProcess.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/admin/parameter/jsp/EditParameters.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/admin/parameter/jsp/EditParameters.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/admin/parameter/jsp/EditParametersProcess.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/admin/parameter/jsp/EditParametersProcess.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/admin/parameter/jsp/ParametersInclude.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/admin/parameter/jsp/ParametersInclude.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/admin/parameter/jsp/ViewParameters.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/admin/parameter/jsp/ViewParameters.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/import/ExportAppSetupData.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/import/ExportAppSetupData.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/import/ImportAppSetupDataLoader.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/import/ImportAppSetupDataLoader.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/dockEquipment/jsp/AddInfeasibilities.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/dockEquipment/jsp/AddInfeasibilities.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/dockEquipment/jsp/ProcessAddInfeasibilities.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/dockEquipment/jsp/ProcessAddInfeasibilities.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/dockEquipment/jsp/ProcessRemoveInfeasibilities.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/dockEquipment/jsp/ProcessRemoveInfeasibilities.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/dockEquipment/jsp/ViewInfeasibilities.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/dockEquipment/jsp/ViewInfeasibilities.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/dockProductClass/jsp/AddInfeasibilities.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/dockProductClass/jsp/AddInfeasibilities.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/dockProductClass/jsp/ProcessAddInfeasibilities.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/dockProductClass/jsp/ProcessAddInfeasibilities.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/dockProductClass/jsp/ProcessRemoveInfeasibilities.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/dockProductClass/jsp/ProcessRemoveInfeasibilities.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/dockProductClass/jsp/ViewInfeasibilities.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/dockProductClass/jsp/ViewInfeasibilities.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/dockProtectionLevel/jsp/AddInfeasibilities.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/dockProtectionLevel/jsp/AddInfeasibilities.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/dockProtectionLevel/jsp/ProcessAddInfeasibilities.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/dockProtectionLevel/jsp/ProcessAddInfeasibilities.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/dockProtectionLevel/jsp/ProcessRemoveInfeasibilities.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/dockProtectionLevel/jsp/ProcessRemoveInfeasibilities.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/dockProtectionLevel/jsp/ViewInfeasibilities.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/dockProtectionLevel/jsp/ViewInfeasibilities.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/jsp/AppointmentTypeDockInfeasibility.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/jsp/AppointmentTypeDockInfeasibility.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/jsp/AppointmentTypeDockInfeasibilitySetup.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/jsp/AppointmentTypeDockInfeasibilitySetup.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/jsp/relatedCompanyId.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/infeasibility/jsp/relatedCompanyId.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/jsp/SlotAppointmentTypeInfeasibility.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/jsp/SlotAppointmentTypeInfeasibility.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/jsp/SlotLoadConfigTypeInfeasibility.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/jsp/SlotLoadConfigTypeInfeasibility.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/jsp/SlotProductClassInfeasibility.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/jsp/SlotProductClassInfeasibility.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/jsp/UpdateDockAppointmentTypeInfeasibility.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/jsp/UpdateDockAppointmentTypeInfeasibility.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/jsp/UpdateSlotAppointmentTypeInfeasibility.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/jsp/UpdateSlotAppointmentTypeInfeasibility.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/jsp/UpdateSlotDockInfeasibility.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/jsp/UpdateSlotDockInfeasibility.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/jsp/UpdateSlotEquipmentInfeasibility.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/jsp/UpdateSlotEquipmentInfeasibility.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/jsp/UpdateSlotLoadConfigTypeInfeasibility.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/jsp/UpdateSlotLoadConfigTypeInfeasibility.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/jsp/UpdateSlotProductClassInfeasibility.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/jsp/UpdateSlotProductClassInfeasibility.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/infeasibility/jsp/UpdateSlotProtectionLevelInfeasibility.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/infeasibility/jsp/UpdateSlotProtectionLevelInfeasibility.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/lookup/jsp/apptPOLookup.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/lookup/jsp/apptPOLookup.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/lookup/jsp/idLookup.jsp' URI, 1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/lookup/jsp/idLookup.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/lookup/jsp/idLookupHFI.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/lookup/jsp/idLookupHFI.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/lookup/jsp/shipmentLookup.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/lookup/jsp/shipmentLookup.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/lookup/ui/ApptPOLookUp.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/lookup/ui/ApptPOLookUp.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/lookup/ui/ApptPOLookUpDialog.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/lookup/ui/ApptPOLookUpDialog.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/lookup/ui/defaultLookupTemplate.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/lookup/ui/defaultLookupTemplate.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/lookup/ui/ILMDriverLookUp.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/lookup/ui/ILMDriverLookUp.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/lookup/ui/ILMSlotGroupLookUp.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/lookup/ui/ILMSlotGroupLookUp.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/lookup/ui/ILMSlotGroupLookupDialog.xhtml'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/lookup/ui/ILMSlotGroupLookupDialog.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/lookup/ui/ILMSlotLookUp.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/lookup/ui/ILMSlotLookUp.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/lookup/ui/ILMSlotLookupDialog.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/lookup/ui/ILMSlotLookupDialog.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/lookup/ui/NumRowPopUp.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/lookup/ui/NumRowPopUp.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/lookup/ui/NumRowPopUpDialog.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/lookup/ui/NumRowPopUpDialog.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/lookup/ui/TrailerLookUp.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/lookup/ui/TrailerLookUp.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/lookup/ui/TrailerLookUpDialog.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/lookup/ui/TrailerLookUpDialog.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/order/jsp/ValidatePOForAppointments.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/order/jsp/ValidatePOForAppointments.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/AppointmentAnalysisReportCreate.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/AppointmentAnalysisReportCreate.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/AppointmentSlot.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/appointmentslot/jsp/AppointmentSlot.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/AppointmentSlotGroupList.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/AppointmentSlotGroupList.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/AppointmentSlotList.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/AppointmentSlotList.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/AppointmentSlotSchdDetail.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/AppointmentSlotSchdDetail.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/ApptSlots.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/appointmentslot/jsp/ApptSlots.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/EditAppointmentSupplierCategory.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/EditAppointmentSupplierCategory.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/FacilityApptSlotsCreate.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/FacilityApptSlotsCreate.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/FacilityApptSlotsUpdate.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/FacilityApptSlotsUpdate.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/FacilitySupplierCreate.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/FacilitySupplierCreate.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/FacilitySupplierUpdate.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/FacilitySupplierUpdate.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/FaclitySupplierUpdate.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/FaclitySupplierUpdate.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/InvokeApptSetupProcessingWithoutScheduler.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/InvokeApptSetupProcessingWithoutScheduler.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/NewApptSlots.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/appointmentslot/jsp/NewApptSlots.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/NewSuppliers.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/appointmentslot/jsp/NewSuppliers.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/processAppointmentReport.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/processAppointmentReport.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/processCreateFacilityRateExceptions.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/processCreateFacilityRateExceptions.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/processCreateSlot.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/processCreateSlot.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/processCreateSlotGroup.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/processCreateSlotGroup.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/processCreateSlotSchedule.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/processCreateSlotSchedule.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/processDeleteSlotGroup.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/processDeleteSlotGroup.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/processDownload.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/appointmentslot/jsp/processDownload.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/processFacilityRateExceptions.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/processFacilityRateExceptions.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/processOverBookingPercentage.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/processOverBookingPercentage.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/processSaveSlot.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/appointmentslot/jsp/processSaveSlot.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/processSaveSlotDurations.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/processSaveSlotDurations.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/processSlot.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/appointmentslot/jsp/processSlot.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/processSlotGroup.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/appointmentslot/jsp/processSlotGroup.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/processSlotSchedule.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/processSlotSchedule.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/processSlotScheduleDetails.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/processSlotScheduleDetails.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/SlotScheduleDetailsList.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/SlotScheduleDetailsList.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/Suppliers.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/appointmentslot/jsp/Suppliers.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/appointmentslot/jsp/updateAppointmentSupplierCategory.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/appointmentslot/jsp/updateAppointmentSupplierCategory.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/feasibility/jsp/ApptslotFeasibility.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/feasibility/jsp/ApptslotFeasibility.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/feasibility/jsp/FacilityFeasibilitySetup.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/feasibility/jsp/FacilityFeasibilitySetup.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/feasibility/jsp/FacilityNTDFeasibilitySetup.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/feasibility/jsp/FacilityNTDFeasibilitySetup.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/feasibility/jsp/FeasibilityDetailForCustWaitTime.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/feasibility/jsp/FeasibilityDetailForCustWaitTime.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/feasibility/jsp/FeasibilityDetailPresentation.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/feasibility/jsp/FeasibilityDetailPresentation.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/feasibility/jsp/FeasibilityDetailPresentationForAllFeasibles.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/feasibility/jsp/FeasibilityDetailPresentationForAllFeasibles.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/feasibility/jsp/FeasibilityEntry.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/feasibility/jsp/FeasibilityEntry.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/feasibility/jsp/FeasibilityList.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/feasibility/jsp/FeasibilityList.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/feasibility/jsp/FeasibilityProcessor.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/feasibility/jsp/FeasibilityProcessor.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/feasibility/jsp/FeasibilitySetup.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/feasibility/jsp/FeasibilitySetup.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/feasibility/jsp/FeasibilitySheet.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/feasibility/jsp/FeasibilitySheet.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/feasibility/jsp/NTDFeasibilitySetup.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/feasibility/jsp/NTDFeasibilitySetup.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/feasibility/jsp/PrioritySchedule.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/feasibility/jsp/PrioritySchedule.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/feasibility/jsp/priorityScheduleDetails.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/feasibility/jsp/priorityScheduleDetails.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/feasibility/jsp/ProcessApptslotFeasibility.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/feasibility/jsp/ProcessApptslotFeasibility.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/feasibility/jsp/processPrioritySchedule.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/feasibility/jsp/processPrioritySchedule.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/feasibility/jsp/ProritySetup.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/feasibility/jsp/ProritySetup.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/feasibility/jsp/SlotFeasibilitySetup.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/feasibility/jsp/SlotFeasibilitySetup.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/feasibility/jsp/YardZonePrioritySetup.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/feasibility/jsp/YardZonePrioritySetup.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/shift/jsp/ProcessShiftList.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/shift/jsp/ProcessShiftList.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/shift/jsp/ShiftList.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/shift/jsp/ShiftList.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/TotalCapacity/jsp/FacilityTotalCapacityList.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/TotalCapacity/jsp/FacilityTotalCapacityList.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/TotalCapacity/jsp/processTotalCapacity.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/TotalCapacity/jsp/processTotalCapacity.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/TotalCapacity/jsp/SlotTotalCapacityList.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/TotalCapacity/jsp/SlotTotalCapacityList.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/TotalCapacity/jsp/TotalCapacity.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/TotalCapacity/jsp/TotalCapacity.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/TotalCapacity/jsp/TotalCapacityDetailList.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/TotalCapacity/jsp/TotalCapacityDetailList.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/TotalCapacity/jsp/TotalCapacityDetailProcessor.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/TotalCapacity/jsp/TotalCapacityDetailProcessor.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/TotalCapacity/jsp/TotalCapacityList.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/TotalCapacity/jsp/TotalCapacityList.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/TotalCapacity/jsp/TotalCapacityProcessor.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/TotalCapacity/jsp/TotalCapacityProcessor.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/TotalCapacity/jsp/TotalCapacityTable.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/setup/TotalCapacity/jsp/TotalCapacityTable.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/setup/TotalCapacity/jsp/YardZoneTotalCapacityList.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/setup/TotalCapacity/jsp/YardZoneTotalCapacityList.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/shipserv/jsp/ValidateShipmentForAppointments.jsp'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (
                 SEQ_RESOURCE_ID.NEXTVAL,
                 '/appointment/shipserv/jsp/ValidateShipmentForAppointments.jsp',
                 'ACM',
                 1,
                 '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/softcheckerrors/jsp/SoftCheckErrors.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/softcheckerrors/jsp/SoftCheckErrors.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsf/appointmentAdditionalDetails.xhtml'
                      URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsf/appointmentAdditionalDetails.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsf/appointmentAuditTrail.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsf/appointmentAuditTrail.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsf/appointmentCreate.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsf/appointmentCreate.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsf/appointmentDetails.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsf/appointmentDetails.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsf/appointmentDockDetails.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsf/appointmentDockDetails.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsf/appointmentMenu.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsf/appointmentMenu.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsf/appointmentObjDetails.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsf/appointmentObjDetails.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsf/appointmentRecommendations.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsf/appointmentRecommendations.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsf/appointmentslotDetails.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsf/appointmentslotDetails.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsf/appointmentWarnings.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsf/appointmentWarnings.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsf/ApptDockDoorLookUp.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsf/ApptDockDoorLookUp.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsf/ApptDockLookUp.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsf/ApptDockLookUp.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsf/apptLevelling.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsf/apptLevelling.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsf/CarrierAppointmentList.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsf/CarrierAppointmentList.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsf/DockLookUpGetsDockId.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsf/DockLookUpGetsDockId.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsf/scheduleAppointmentCarrier.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsf/scheduleAppointmentCarrier.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsp/appointmentCalendar.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsp/appointmentCalendar.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsp/appointmentPlanning.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsp/appointmentPlanning.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsp/AppointmentRecurrence.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsp/AppointmentRecurrence.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsp/appointmentReport.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsp/appointmentReport.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsp/apptSlotCalendar.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsp/apptSlotCalendar.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsp/apptSlotReport.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsp/apptSlotReport.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsp/createAppointment.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsp/createAppointment.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsp/header.jsp' URI, 1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsp/header.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsp/popupHeaderWithPrintHelp.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsp/popupHeaderWithPrintHelp.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsp/POShipmentSelect.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsp/POShipmentSelect.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsp/processAppointmentApprove.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsp/processAppointmentApprove.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsp/ProcessAppointmentRecurrence.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsp/ProcessAppointmentRecurrence.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsp/ProcessPOShipmentSelect.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsp/ProcessPOShipmentSelect.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsp/rejectAppointment.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsp/rejectAppointment.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsp/YardCalendar.jsp' URI, 1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsp/YardCalendar.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/util/jsf/ymsSoftcheckErrors.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/util/jsf/ymsSoftcheckErrors.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/util/jsf/ymsSoftcheckErrorsTask.xhtml' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/util/jsf/ymsSoftcheckErrorsTask.xhtml',
               'ACM',
               1,
               '');

MERGE INTO RESOURCES L
     USING (SELECT '/appointment/util/jsp/NumRowsPopup.jsp' URI,
                   1 URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/util/jsp/NumRowsPopup.jsp',
               'ACM',
               1,
               '');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/admin/jsp/AppointmentImport.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/admin/jsp/AppointmentImport.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/admin/jsp/AppointmentImport.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/admin/jsp/AppointmentImport.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/admin/parameter/jsp/EditAppointmentParameters.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/admin/parameter/jsp/EditAppointmentParameters.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/admin/parameter/jsp/EditAppointmentParameters.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/admin/parameter/jsp/EditAppointmentParameters.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/admin/parameter/jsp/EditAppointmentParametersProcess.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/admin/parameter/jsp/EditAppointmentParametersProcess.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/admin/parameter/jsp/EditAppointmentParametersProcess.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/admin/parameter/jsp/EditAppointmentParametersProcess.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/admin/parameter/jsp/EditParameters.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/admin/parameter/jsp/EditParameters.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/admin/parameter/jsp/EditParameters.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/admin/parameter/jsp/EditParameters.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/admin/parameter/jsp/EditParametersProcess.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/admin/parameter/jsp/EditParametersProcess.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/admin/parameter/jsp/EditParametersProcess.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/admin/parameter/jsp/EditParametersProcess.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/admin/parameter/jsp/ParametersInclude.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/admin/parameter/jsp/ParametersInclude.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/admin/parameter/jsp/ParametersInclude.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/admin/parameter/jsp/ParametersInclude.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/admin/parameter/jsp/ViewParameters.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/admin/parameter/jsp/ViewParameters.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/admin/parameter/jsp/ViewParameters.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/admin/parameter/jsp/ViewParameters.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/import/ExportAppSetupData.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/import/ExportAppSetupData.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/import/ExportAppSetupData.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/import/ExportAppSetupData.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/import/ImportAppSetupDataLoader.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/import/ImportAppSetupDataLoader.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/import/ImportAppSetupDataLoader.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/import/ImportAppSetupDataLoader.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockEquipment/jsp/AddInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockEquipment/jsp/AddInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockEquipment/jsp/AddInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockEquipment/jsp/AddInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockEquipment/jsp/ProcessAddInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockEquipment/jsp/ProcessAddInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockEquipment/jsp/ProcessAddInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockEquipment/jsp/ProcessAddInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockEquipment/jsp/ProcessRemoveInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockEquipment/jsp/ProcessRemoveInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockEquipment/jsp/ProcessRemoveInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockEquipment/jsp/ProcessRemoveInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockEquipment/jsp/ViewInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockEquipment/jsp/ViewInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockEquipment/jsp/ViewInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockEquipment/jsp/ViewInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockProductClass/jsp/AddInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockProductClass/jsp/AddInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockProductClass/jsp/AddInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockProductClass/jsp/AddInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockProductClass/jsp/ProcessAddInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockProductClass/jsp/ProcessAddInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockProductClass/jsp/ProcessAddInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockProductClass/jsp/ProcessAddInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockProductClass/jsp/ProcessRemoveInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockProductClass/jsp/ProcessRemoveInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockProductClass/jsp/ProcessRemoveInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockProductClass/jsp/ProcessRemoveInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockProductClass/jsp/ViewInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockProductClass/jsp/ViewInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockProductClass/jsp/ViewInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockProductClass/jsp/ViewInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockProtectionLevel/jsp/AddInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockProtectionLevel/jsp/AddInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockProtectionLevel/jsp/AddInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockProtectionLevel/jsp/AddInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockProtectionLevel/jsp/ProcessAddInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockProtectionLevel/jsp/ProcessAddInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockProtectionLevel/jsp/ProcessAddInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockProtectionLevel/jsp/ProcessAddInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockProtectionLevel/jsp/ProcessRemoveInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockProtectionLevel/jsp/ProcessRemoveInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockProtectionLevel/jsp/ProcessRemoveInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockProtectionLevel/jsp/ProcessRemoveInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockProtectionLevel/jsp/ViewInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockProtectionLevel/jsp/ViewInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/dockProtectionLevel/jsp/ViewInfeasibilities.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/dockProtectionLevel/jsp/ViewInfeasibilities.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/AppointmentTypeDockInfeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/AppointmentTypeDockInfeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/AppointmentTypeDockInfeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/AppointmentTypeDockInfeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/AppointmentTypeDockInfeasibilitySetup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/AppointmentTypeDockInfeasibilitySetup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/AppointmentTypeDockInfeasibilitySetup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/AppointmentTypeDockInfeasibilitySetup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/relatedCompanyId.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/relatedCompanyId.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/relatedCompanyId.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/relatedCompanyId.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/SlotAppointmentTypeInfeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/SlotAppointmentTypeInfeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/SlotAppointmentTypeInfeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/SlotAppointmentTypeInfeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/SlotLoadConfigTypeInfeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/SlotLoadConfigTypeInfeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/SlotLoadConfigTypeInfeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/SlotLoadConfigTypeInfeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/SlotProductClassInfeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/SlotProductClassInfeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/SlotProductClassInfeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/SlotProductClassInfeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/UpdateDockAppointmentTypeInfeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/UpdateDockAppointmentTypeInfeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/UpdateDockAppointmentTypeInfeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/UpdateDockAppointmentTypeInfeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/UpdateSlotAppointmentTypeInfeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/UpdateSlotAppointmentTypeInfeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/UpdateSlotAppointmentTypeInfeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/UpdateSlotAppointmentTypeInfeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/UpdateSlotDockInfeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/UpdateSlotDockInfeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/UpdateSlotDockInfeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/UpdateSlotDockInfeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/UpdateSlotEquipmentInfeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/UpdateSlotEquipmentInfeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/UpdateSlotEquipmentInfeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/UpdateSlotEquipmentInfeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/UpdateSlotLoadConfigTypeInfeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/UpdateSlotLoadConfigTypeInfeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/UpdateSlotLoadConfigTypeInfeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/UpdateSlotLoadConfigTypeInfeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/UpdateSlotProductClassInfeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/UpdateSlotProductClassInfeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/UpdateSlotProductClassInfeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/UpdateSlotProductClassInfeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/UpdateSlotProtectionLevelInfeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/UpdateSlotProtectionLevelInfeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/infeasibility/jsp/UpdateSlotProtectionLevelInfeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/infeasibility/jsp/UpdateSlotProtectionLevelInfeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/lookup/jsp/apptPOLookup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/lookup/jsp/apptPOLookup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/lookup/jsp/apptPOLookup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/lookup/jsp/apptPOLookup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/lookup/jsp/idLookup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/lookup/jsp/idLookup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/lookup/jsp/idLookup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/lookup/jsp/idLookup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/lookup/jsp/idLookupHFI.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/lookup/jsp/idLookupHFI.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/lookup/jsp/idLookupHFI.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/lookup/jsp/idLookupHFI.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/jsp/shipmentLookup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/lookup/jsp/shipmentLookup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/jsp/shipmentLookup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/lookup/jsp/shipmentLookup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/ui/ApptPOLookUp.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/lookup/ui/ApptPOLookUp.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/ui/ApptPOLookUp.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/lookup/ui/ApptPOLookUp.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/ui/ApptPOLookUpDialog.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/lookup/ui/ApptPOLookUpDialog.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/ui/ApptPOLookUpDialog.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/lookup/ui/ApptPOLookUpDialog.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/ui/defaultLookupTemplate.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/lookup/ui/defaultLookupTemplate.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/ui/defaultLookupTemplate.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/lookup/ui/defaultLookupTemplate.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/ui/ILMDriverLookUp.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/lookup/ui/ILMDriverLookUp.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/ui/ILMDriverLookUp.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/lookup/ui/ILMDriverLookUp.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/ui/ILMSlotGroupLookUp.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/lookup/ui/ILMSlotGroupLookUp.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/ui/ILMSlotGroupLookUp.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/lookup/ui/ILMSlotGroupLookUp.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/ui/ILMSlotGroupLookupDialog.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/lookup/ui/ILMSlotGroupLookupDialog.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/ui/ILMSlotGroupLookupDialog.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/lookup/ui/ILMSlotGroupLookupDialog.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/ui/ILMSlotLookUp.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/lookup/ui/ILMSlotLookUp.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/ui/ILMSlotLookUp.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/lookup/ui/ILMSlotLookUp.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/ui/ILMSlotLookupDialog.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/lookup/ui/ILMSlotLookupDialog.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/ui/ILMSlotLookupDialog.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/lookup/ui/ILMSlotLookupDialog.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/lookup/ui/NumRowPopUp.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/lookup/ui/NumRowPopUp.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/lookup/ui/NumRowPopUp.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/lookup/ui/NumRowPopUp.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/ui/NumRowPopUpDialog.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/lookup/ui/NumRowPopUpDialog.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/ui/NumRowPopUpDialog.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/lookup/ui/NumRowPopUpDialog.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/ui/TrailerLookUp.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/lookup/ui/TrailerLookUp.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/ui/TrailerLookUp.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/lookup/ui/TrailerLookUp.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/ui/TrailerLookUpDialog.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/lookup/ui/TrailerLookUpDialog.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/lookup/ui/TrailerLookUpDialog.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/lookup/ui/TrailerLookUpDialog.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/order/jsp/ValidatePOForAppointments.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/order/jsp/ValidatePOForAppointments.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/order/jsp/ValidatePOForAppointments.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/order/jsp/ValidatePOForAppointments.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/AppointmentAnalysisReportCreate.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/AppointmentAnalysisReportCreate.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/AppointmentAnalysisReportCreate.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/AppointmentAnalysisReportCreate.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/AppointmentSlot.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/AppointmentSlot.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/AppointmentSlot.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/AppointmentSlot.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/AppointmentSlotGroupList.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/AppointmentSlotGroupList.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/AppointmentSlotGroupList.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/AppointmentSlotGroupList.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/AppointmentSlotList.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/AppointmentSlotList.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/AppointmentSlotList.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/AppointmentSlotList.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/AppointmentSlotSchdDetail.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/AppointmentSlotSchdDetail.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/AppointmentSlotSchdDetail.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/AppointmentSlotSchdDetail.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/ApptSlots.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/ApptSlots.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/ApptSlots.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/ApptSlots.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/EditAppointmentSupplierCategory.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/EditAppointmentSupplierCategory.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/EditAppointmentSupplierCategory.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/EditAppointmentSupplierCategory.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/FacilityApptSlotsCreate.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/FacilityApptSlotsCreate.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/FacilityApptSlotsCreate.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/FacilityApptSlotsCreate.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/FacilityApptSlotsUpdate.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/FacilityApptSlotsUpdate.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/FacilityApptSlotsUpdate.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/FacilityApptSlotsUpdate.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/FacilitySupplierCreate.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/FacilitySupplierCreate.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/FacilitySupplierCreate.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/FacilitySupplierCreate.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/FacilitySupplierUpdate.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/FacilitySupplierUpdate.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/FacilitySupplierUpdate.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/FacilitySupplierUpdate.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/FaclitySupplierUpdate.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/FaclitySupplierUpdate.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/FaclitySupplierUpdate.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/FaclitySupplierUpdate.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/InvokeApptSetupProcessingWithoutScheduler.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/InvokeApptSetupProcessingWithoutScheduler.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/InvokeApptSetupProcessingWithoutScheduler.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/InvokeApptSetupProcessingWithoutScheduler.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/NewApptSlots.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/NewApptSlots.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/NewApptSlots.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/NewApptSlots.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/NewSuppliers.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/NewSuppliers.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/NewSuppliers.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/NewSuppliers.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processAppointmentReport.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processAppointmentReport.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processAppointmentReport.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processAppointmentReport.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processCreateFacilityRateExceptions.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processCreateFacilityRateExceptions.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processCreateFacilityRateExceptions.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processCreateFacilityRateExceptions.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processCreateSlot.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processCreateSlot.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processCreateSlot.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processCreateSlot.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processCreateSlotGroup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processCreateSlotGroup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processCreateSlotGroup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processCreateSlotGroup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processCreateSlotSchedule.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processCreateSlotSchedule.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processCreateSlotSchedule.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processCreateSlotSchedule.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processDeleteSlotGroup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processDeleteSlotGroup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processDeleteSlotGroup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processDeleteSlotGroup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processDownload.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processDownload.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processDownload.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processDownload.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processFacilityRateExceptions.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processFacilityRateExceptions.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processFacilityRateExceptions.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processFacilityRateExceptions.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processOverBookingPercentage.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processOverBookingPercentage.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processOverBookingPercentage.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processOverBookingPercentage.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processSaveSlot.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processSaveSlot.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processSaveSlot.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processSaveSlot.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processSaveSlotDurations.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processSaveSlotDurations.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processSaveSlotDurations.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processSaveSlotDurations.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processSlot.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processSlot.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processSlot.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processSlot.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processSlotGroup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processSlotGroup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processSlotGroup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processSlotGroup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processSlotSchedule.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processSlotSchedule.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processSlotSchedule.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processSlotSchedule.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processSlotScheduleDetails.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processSlotScheduleDetails.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/processSlotScheduleDetails.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/processSlotScheduleDetails.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/SlotScheduleDetailsList.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/SlotScheduleDetailsList.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/SlotScheduleDetailsList.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/SlotScheduleDetailsList.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/Suppliers.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/Suppliers.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/Suppliers.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/Suppliers.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/updateAppointmentSupplierCategory.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/updateAppointmentSupplierCategory.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/appointmentslot/jsp/updateAppointmentSupplierCategory.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/appointmentslot/jsp/updateAppointmentSupplierCategory.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/ApptslotFeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/ApptslotFeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/ApptslotFeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/ApptslotFeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/FacilityFeasibilitySetup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/FacilityFeasibilitySetup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/FacilityFeasibilitySetup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/FacilityFeasibilitySetup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/FacilityNTDFeasibilitySetup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/FacilityNTDFeasibilitySetup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/FacilityNTDFeasibilitySetup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/FacilityNTDFeasibilitySetup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/FeasibilityDetailForCustWaitTime.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/FeasibilityDetailForCustWaitTime.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/FeasibilityDetailForCustWaitTime.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/FeasibilityDetailForCustWaitTime.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/FeasibilityDetailPresentation.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/FeasibilityDetailPresentation.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/FeasibilityDetailPresentation.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/FeasibilityDetailPresentation.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/FeasibilityDetailPresentationForAllFeasibles.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/FeasibilityDetailPresentationForAllFeasibles.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/FeasibilityDetailPresentationForAllFeasibles.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/FeasibilityDetailPresentationForAllFeasibles.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/FeasibilityEntry.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/FeasibilityEntry.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/FeasibilityEntry.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/FeasibilityEntry.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/FeasibilityList.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/FeasibilityList.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/FeasibilityList.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/FeasibilityList.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/FeasibilityProcessor.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/FeasibilityProcessor.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/FeasibilityProcessor.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/FeasibilityProcessor.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/FeasibilitySetup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/FeasibilitySetup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/FeasibilitySetup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/FeasibilitySetup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/FeasibilitySheet.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/FeasibilitySheet.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/FeasibilitySheet.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/FeasibilitySheet.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/NTDFeasibilitySetup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/NTDFeasibilitySetup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/NTDFeasibilitySetup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/NTDFeasibilitySetup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/PrioritySchedule.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/PrioritySchedule.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/PrioritySchedule.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/PrioritySchedule.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/priorityScheduleDetails.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/priorityScheduleDetails.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/priorityScheduleDetails.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/priorityScheduleDetails.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/ProcessApptslotFeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/ProcessApptslotFeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/ProcessApptslotFeasibility.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/ProcessApptslotFeasibility.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/processPrioritySchedule.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/processPrioritySchedule.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/processPrioritySchedule.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/processPrioritySchedule.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/ProritySetup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/ProritySetup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/ProritySetup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/ProritySetup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/SlotFeasibilitySetup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/SlotFeasibilitySetup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/SlotFeasibilitySetup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/SlotFeasibilitySetup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/YardZonePrioritySetup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/YardZonePrioritySetup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/feasibility/jsp/YardZonePrioritySetup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/feasibility/jsp/YardZonePrioritySetup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/shift/jsp/ProcessShiftList.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/shift/jsp/ProcessShiftList.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/shift/jsp/ProcessShiftList.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/shift/jsp/ProcessShiftList.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/shift/jsp/ShiftList.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/setup/shift/jsp/ShiftList.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/shift/jsp/ShiftList.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/setup/shift/jsp/ShiftList.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/TotalCapacity/jsp/FacilityTotalCapacityList.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/TotalCapacity/jsp/FacilityTotalCapacityList.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/TotalCapacity/jsp/FacilityTotalCapacityList.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/TotalCapacity/jsp/FacilityTotalCapacityList.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/TotalCapacity/jsp/processTotalCapacity.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/TotalCapacity/jsp/processTotalCapacity.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/TotalCapacity/jsp/processTotalCapacity.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/TotalCapacity/jsp/processTotalCapacity.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/TotalCapacity/jsp/SlotTotalCapacityList.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/TotalCapacity/jsp/SlotTotalCapacityList.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/TotalCapacity/jsp/SlotTotalCapacityList.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/TotalCapacity/jsp/SlotTotalCapacityList.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/TotalCapacity/jsp/TotalCapacity.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/TotalCapacity/jsp/TotalCapacity.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/TotalCapacity/jsp/TotalCapacity.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/TotalCapacity/jsp/TotalCapacity.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/TotalCapacity/jsp/TotalCapacityDetailList.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/TotalCapacity/jsp/TotalCapacityDetailList.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/TotalCapacity/jsp/TotalCapacityDetailList.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/TotalCapacity/jsp/TotalCapacityDetailList.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/TotalCapacity/jsp/TotalCapacityDetailProcessor.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/TotalCapacity/jsp/TotalCapacityDetailProcessor.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/TotalCapacity/jsp/TotalCapacityDetailProcessor.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/TotalCapacity/jsp/TotalCapacityDetailProcessor.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/TotalCapacity/jsp/TotalCapacityList.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/TotalCapacity/jsp/TotalCapacityList.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/TotalCapacity/jsp/TotalCapacityList.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/TotalCapacity/jsp/TotalCapacityList.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/TotalCapacity/jsp/TotalCapacityProcessor.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/TotalCapacity/jsp/TotalCapacityProcessor.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/TotalCapacity/jsp/TotalCapacityProcessor.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/TotalCapacity/jsp/TotalCapacityProcessor.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/TotalCapacity/jsp/TotalCapacityTable.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/TotalCapacity/jsp/TotalCapacityTable.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/TotalCapacity/jsp/TotalCapacityTable.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/TotalCapacity/jsp/TotalCapacityTable.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/TotalCapacity/jsp/YardZoneTotalCapacityList.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/TotalCapacity/jsp/YardZoneTotalCapacityList.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/setup/TotalCapacity/jsp/YardZoneTotalCapacityList.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/setup/TotalCapacity/jsp/YardZoneTotalCapacityList.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/shipserv/jsp/ValidateShipmentForAppointments.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/shipserv/jsp/ValidateShipmentForAppointments.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/shipserv/jsp/ValidateShipmentForAppointments.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/shipserv/jsp/ValidateShipmentForAppointments.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/softcheckerrors/jsp/SoftCheckErrors.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/softcheckerrors/jsp/SoftCheckErrors.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/softcheckerrors/jsp/SoftCheckErrors.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/softcheckerrors/jsp/SoftCheckErrors.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/appointmentAdditionalDetails.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsf/appointmentAdditionalDetails.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/appointmentAdditionalDetails.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsf/appointmentAdditionalDetails.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/appointmentAuditTrail.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsf/appointmentAuditTrail.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/appointmentAuditTrail.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsf/appointmentAuditTrail.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/appointmentCreate.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsf/appointmentCreate.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/appointmentCreate.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsf/appointmentCreate.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/appointmentDetails.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsf/appointmentDetails.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/appointmentDetails.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsf/appointmentDetails.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/appointmentDockDetails.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsf/appointmentDockDetails.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/appointmentDockDetails.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsf/appointmentDockDetails.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/appointmentMenu.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsf/appointmentMenu.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/appointmentMenu.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsf/appointmentMenu.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/appointmentObjDetails.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsf/appointmentObjDetails.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/appointmentObjDetails.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsf/appointmentObjDetails.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/appointmentRecommendations.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsf/appointmentRecommendations.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/appointmentRecommendations.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsf/appointmentRecommendations.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/appointmentslotDetails.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsf/appointmentslotDetails.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/appointmentslotDetails.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsf/appointmentslotDetails.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/appointmentWarnings.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsf/appointmentWarnings.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/appointmentWarnings.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsf/appointmentWarnings.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/ApptDockDoorLookUp.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsf/ApptDockDoorLookUp.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/ApptDockDoorLookUp.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsf/ApptDockDoorLookUp.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/ui/jsf/ApptDockLookUp.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsf/ApptDockLookUp.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/ui/jsf/ApptDockLookUp.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsf/ApptDockLookUp.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/ui/jsf/apptLevelling.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsf/apptLevelling.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/ui/jsf/apptLevelling.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsf/apptLevelling.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/CarrierAppointmentList.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsf/CarrierAppointmentList.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/CarrierAppointmentList.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsf/CarrierAppointmentList.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/DockLookUpGetsDockId.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsf/DockLookUpGetsDockId.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/DockLookUpGetsDockId.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsf/DockLookUpGetsDockId.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/scheduleAppointmentCarrier.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsf/scheduleAppointmentCarrier.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/scheduleAppointmentCarrier.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsf/scheduleAppointmentCarrier.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsp/appointmentCalendar.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsp/appointmentCalendar.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsp/appointmentCalendar.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsp/appointmentCalendar.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsp/appointmentPlanning.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsp/appointmentPlanning.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsp/appointmentPlanning.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsp/appointmentPlanning.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsp/AppointmentRecurrence.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsp/AppointmentRecurrence.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsp/AppointmentRecurrence.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsp/AppointmentRecurrence.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsp/appointmentReport.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsp/appointmentReport.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsp/appointmentReport.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsp/appointmentReport.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/ui/jsp/apptSlotCalendar.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsp/apptSlotCalendar.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/ui/jsp/apptSlotCalendar.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsp/apptSlotCalendar.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/ui/jsp/apptSlotReport.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsp/apptSlotReport.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/ui/jsp/apptSlotReport.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsp/apptSlotReport.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsp/createAppointment.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsp/createAppointment.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsp/createAppointment.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsp/createAppointment.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/ui/jsp/header.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsp/header.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/ui/jsp/header.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsp/header.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsp/popupHeaderWithPrintHelp.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsp/popupHeaderWithPrintHelp.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsp/popupHeaderWithPrintHelp.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsp/popupHeaderWithPrintHelp.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/ui/jsp/POShipmentSelect.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsp/POShipmentSelect.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/ui/jsp/POShipmentSelect.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsp/POShipmentSelect.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsp/processAppointmentApprove.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsp/processAppointmentApprove.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsp/processAppointmentApprove.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsp/processAppointmentApprove.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsp/ProcessAppointmentRecurrence.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsp/ProcessAppointmentRecurrence.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsp/ProcessAppointmentRecurrence.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsp/ProcessAppointmentRecurrence.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsp/ProcessPOShipmentSelect.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsp/ProcessPOShipmentSelect.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsp/ProcessPOShipmentSelect.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/ui/jsp/ProcessPOShipmentSelect.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsp/rejectAppointment.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsp/rejectAppointment.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsp/rejectAppointment.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsp/rejectAppointment.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/ui/jsp/YardCalendar.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsp/YardCalendar.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/ui/jsp/YardCalendar.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsp/YardCalendar.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/util/jsf/ymsSoftcheckErrors.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/util/jsf/ymsSoftcheckErrors.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/util/jsf/ymsSoftcheckErrors.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/util/jsf/ymsSoftcheckErrors.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/util/jsf/ymsSoftcheckErrorsTask.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/util/jsf/ymsSoftcheckErrorsTask.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/util/jsf/ymsSoftcheckErrorsTask.xhtml'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI =
                            '/appointment/util/jsf/ymsSoftcheckErrorsTask.xhtml'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLCA' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/util/jsp/NumRowsPopup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/util/jsp/NumRowsPopup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLCA');

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'UCLU' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/appointment/util/jsp/NumRowsPopup.jsp'
                   AND RESOURCES.MODULE = 'ACM') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/util/jsp/NumRowsPopup.jsp'
                         AND RESOURCES.MODULE = 'ACM'),
                 'UCLU');
				 
COMMIT;				 



-- DBTicket APPT-199

UPDATE FILTER_LAYOUT
SET value_generator_class='com.manh.appointment.ui.finitevalue.ILMLoadPosition'
WHERE object_type        ='UI_APPOINTMENT'
AND value_generator_class='com.manh.yms.appointment.finitevalue.ILMLoadPosition';

UPDATE FILTER_LAYOUT
SET value_generator_class='com.manh.appointment.ui.finitevalue.ILMAppointmentType'
WHERE object_type        ='UI_APPOINTMENT'
AND value_generator_class='com.manh.yms.appointment.finitevalue.ILMAppointmentType';

UPDATE FILTER_LAYOUT
SET value_generator_class='com.manh.appointment.ui.finitevalue.ILMAppointmentStatus'
WHERE object_type        ='UI_APPOINTMENT'
AND value_generator_class='com.manh.yms.appointment.finitevalue.ILMAppointmentStatus';

UPDATE FILTER_LAYOUT
SET value_generator_class='com.manh.appointment.ui.finitevalue.ReservationType'
WHERE object_type        ='UI_APPOINTMENT'
AND value_generator_class='com.manh.yms.appointment.finitevalue.ReservationType';

UPDATE FILTER_LAYOUT
SET value_generator_class='com.manh.appointment.ui.finitevalue.ILMAppointmentType'
WHERE object_type        ='UI_APPOINTMENT_VENDOR'
AND value_generator_class='com.manh.yms.appointment.finitevalue.ILMAppointmentType';

UPDATE FILTER_LAYOUT
SET value_generator_class='com.manh.appointment.ui.finitevalue.ReservationType'
WHERE object_type        ='UI_APPOINTMENT_VENDOR'
AND value_generator_class='com.manh.yms.appointment.finitevalue.ReservationType';

COMMIT;


-- DBTicket APPT-206

MERGE INTO RESOURCES L USING
  (SELECT '/services/rest/tpe/TPEApptActionService/tpeApptActions/getStopApptDttm' URI,
      2 URI_TYPE_ID,
      'GET' HTTP_METHOD
    FROM DUAL
  )
  B ON (L.URI = B.URI AND L.URI_TYPE_ID=B.URI_TYPE_ID AND B.HTTP_METHOD IS NULL)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.RESOURCE_ID,
      L.URI,
      L.MODULE,
      L.URI_TYPE_ID,
      L.HTTP_METHOD
    )
    VALUES
    (
      SEQ_RESOURCE_ID.NEXTVAL,
      '/services/rest/tpe/TPEApptActionService/tpeApptActions/getStopApptDttm',
      'CNTRMGT',
      2,
      'GET'
    );

MERGE INTO RESOURCE_PERMISSION RP USING
  (SELECT RESOURCES.RESOURCE_ID,
      'VSH' AS PERMISSION_CODE
    FROM RESOURCES
    WHERE RESOURCES.URI        ='/services/rest/tpe/TPEApptActionService/tpeApptActions/getStopApptDttm'
    AND RESOURCES.URI_TYPE_ID  =2
    AND HTTP_METHOD='GET'
  )
  RP1 ON (RP.RESOURCE_ID = RP1.RESOURCE_ID AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED THEN
  INSERT
    (
      RESOURCE_ID,
      PERMISSION_CODE
    )
    VALUES
    (
      (SELECT RESOURCE_ID
        FROM RESOURCES
        WHERE URI            ='/services/rest/tpe/TPEApptActionService/tpeApptActions/getStopApptDttm'
        AND RESOURCES.MODULE ='CNTRMGT'
      )
      ,
      'VSH'
    );
 
COMMIT; 


-- DBTicket APPT-205

MERGE INTO RESOURCES L
     USING (SELECT '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/getRecommendations' AS URI,
                   2 AS URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/getRecommendations',
               'APPT',
               2,
               'GET');




MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'AAPT' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/getRecommendations' AND URI_TYPE_ID = 2 AND HTTP_METHOD ='GET') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE     URI = '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/getRecommendations'
                         AND URI_TYPE_ID = 2
                         AND HTTP_METHOD ='GET'),
                 'AAPT');


MERGE INTO RESOURCES L
     USING (SELECT '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/saveRecommendation' AS URI,
                   2 AS URI_TYPE_ID
              FROM DUAL) B
        ON (L.URI = B.URI AND L.URI_TYPE_ID = B.URI_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/saveRecommendation',
               'APPT',
               2,
               'GET');




MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'AAPT' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI = '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/saveRecommendation' AND URI_TYPE_ID = 2 AND HTTP_METHOD ='GET') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE     URI = '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/saveRecommendation'
                         AND URI_TYPE_ID = 2
                         AND HTTP_METHOD ='GET'),
                 'AAPT');

COMMIT;



-- DBTicket APPT-212

MERGE INTO RESOURCES L USING (SELECT '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/cleanup' URI,2 URI_TYPE_ID,'GET' HTTP_METHOD FROM DUAL) B ON (L.URI = B.URI AND L.URI_TYPE_ID=B.URI_TYPE_ID AND L.HTTP_METHOD=B.HTTP_METHOD) WHEN NOT MATCHED THEN INSERT (L.RESOURCE_ID, L.URI, L.MODULE, L.URI_TYPE_ID,L.HTTP_METHOD) VALUES (SEQ_RESOURCE_ID.NEXTVAL,'/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/cleanup','VAPT',2,'GET');

MERGE INTO RESOURCE_PERMISSION RP USING (SELECT RESOURCES.RESOURCE_ID,'VAPT' AS PERMISSION_CODE FROM RESOURCES WHERE RESOURCES.URI ='/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/cleanup' AND RESOURCES.URI_TYPE_ID =2 AND RESOURCES.HTTP_METHOD IS NULL) RP1 ON (RP.RESOURCE_ID = RP1.RESOURCE_ID AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE) WHEN NOT MATCHED THEN INSERT     (RESOURCE_ID, PERMISSION_CODE) VALUES ( (SELECT RESOURCE_ID FROM RESOURCES WHERE URI ='/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/cleanup' AND RESOURCES.MODULE ='VAPT'),'VAPT');

MERGE INTO LABEL L USING (SELECT 'ILM' BUNDLE_NAME,'SlotCapacities' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'SlotCapacities','Slot Capacities','ILM');
MERGE INTO LABEL L USING (SELECT 'ILM' BUNDLE_NAME,'TotalCapacity' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'TotalCapacity','Total Capacity','ILM');
MERGE INTO LABEL L USING (SELECT 'ILM' BUNDLE_NAME,'UtilizedCapacity' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'UtilizedCapacity','Utilized Capacity','ILM');
MERGE INTO LABEL L USING (SELECT 'ILM' BUNDLE_NAME,'UtilizedCapacity' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'UtilizedCapacity','Utilized Capacity','ILM');
MERGE INTO LABEL L USING (SELECT 'ILM' BUNDLE_NAME,'CarryOverCapacity' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'CarryOverCapacity','Carry Over Capacity','ILM');

COMMIT;


-- DBTicket APPT-219

DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_tab_columns
                        where table_name='ILM_APPOINTMENTS' and column_name='CREATED_DTTM';
   IF (V_CNT = 0)
   THEN
      V_SQL := 'ALTER TABLE ILM_APPOINTMENTS ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL)';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/

-- DBTicket APPT-227

UPDATE xbase_menu_item
   SET XBASE_MENU_PART_ID =
          (SELECT MAX(XBASE_MENU_PART_ID)
             FROM xbase_menu_part
            WHERE name = 'Yard Management')
 WHERE XMENU_ITEM_ID =
          (SELECT XMENU_ITEM_ID
             FROM xmenu_item
            WHERE navigation_key =
                     '/appointment/facility/FacilityList.jsflps');

commit;

-- DBTicket APPT-234

----delete from xscreen_label where xscreen_id = 240010;

delete from label_display where disp_key like 'Slot Capacities' and created_source ='ILM';
delete from label where key like 'SlotCapacities' and bundle_name = 'ILM';
delete from label_display where disp_key like 'Total Capacity' and created_source ='ILM';
delete from label where key like 'TotalCapacity' and bundle_name = 'ILM';
delete from label_display where disp_key like 'Utilized Capacity' and created_source ='ILM';
delete from label where key like 'UtilizedCapacity' and bundle_name = 'ILM';
delete from label_display where disp_key like 'Carry Over Capacity' and created_source ='ILM' ;
delete from label where key like 'CarryOverCapacity' and bundle_name = 'ILM';

COMMIT;

MERGE INTO LABEL L
     USING (SELECT 'ILM' BUNDLE_NAME, 'SlotCapacities' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'SlotCapacities',
               'Slot Capacities',
               'ILM');

MERGE INTO LABEL L
     USING (SELECT 'ILM' BUNDLE_NAME, 'TotalCapacity' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'TotalCapacity',
               'Total Capacity',
               'ILM');

MERGE INTO LABEL L
     USING (SELECT 'ILM' BUNDLE_NAME, 'UtilizedCapacity' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UtilizedCapacity',
               'Utilized Capacity',
               'ILM');

MERGE INTO LABEL L
     USING (SELECT 'ILM' BUNDLE_NAME, 'CarryOverCapacity' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'CarryOverCapacity',
               'Carry Over Capacity',
               'ILM');

----MERGE INTO XSCREEN_LABEL L
----     USING (SELECT 'Recommendations' LABEL_KEY,
----                   240010 XSCREEN_ID,
----                   'ILM' LABEL_BUNDLE_NAME
----              FROM DUAL) B
----        ON (    L.LABEL_KEY = B.LABEL_KEY
----            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
----            AND L.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XSCREEN_LABEL_ID,
----               XSCREEN_ID,
----               LABEL_KEY,
----               LABEL_BUNDLE_NAME)
----       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
----               '240010',
----               'Recommendations',
----               'ILM');
----
----MERGE INTO XSCREEN_LABEL L
----     USING (SELECT 'Appointments' LABEL_KEY,
----                   240010 XSCREEN_ID,
----                   'ILM' LABEL_BUNDLE_NAME
----              FROM DUAL) B
----        ON (    L.LABEL_KEY = B.LABEL_KEY
----            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
----            AND L.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XSCREEN_LABEL_ID,
----               XSCREEN_ID,
----               LABEL_KEY,
----               LABEL_BUNDLE_NAME)
----       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
----               '240010',
----               'Appointments',
----               'ILM');
----
----MERGE INTO XSCREEN_LABEL L
----     USING (SELECT 'Facility' LABEL_KEY,
----                   240010 XSCREEN_ID,
----                   'ILM' LABEL_BUNDLE_NAME
----              FROM DUAL) B
----        ON (    L.LABEL_KEY = B.LABEL_KEY
----            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
----            AND L.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XSCREEN_LABEL_ID,
----               XSCREEN_ID,
----               LABEL_KEY,
----               LABEL_BUNDLE_NAME)
----       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
----               '240010',
----               'Facility',
----               'ILM');
----
----MERGE INTO XSCREEN_LABEL L
----     USING (SELECT 'Save' LABEL_KEY,
----                   240010 XSCREEN_ID,
----                   'ILM' LABEL_BUNDLE_NAME
----              FROM DUAL) B
----        ON (    L.LABEL_KEY = B.LABEL_KEY
----            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
----            AND L.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XSCREEN_LABEL_ID,
----               XSCREEN_ID,
----               LABEL_KEY,
----               LABEL_BUNDLE_NAME)
----       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
----               '240010',
----               'Save',
----               'ILM');
----
----MERGE INTO XSCREEN_LABEL L
----     USING (SELECT 'Cancel' LABEL_KEY,
----                   240010 XSCREEN_ID,
----                   'ILM' LABEL_BUNDLE_NAME
----              FROM DUAL) B
----        ON (    L.LABEL_KEY = B.LABEL_KEY
----            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
----            AND L.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XSCREEN_LABEL_ID,
----               XSCREEN_ID,
----               LABEL_KEY,
----               LABEL_BUNDLE_NAME)
----       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
----               '240010',
----               'Cancel',
----               'ILM');
----
----MERGE INTO XSCREEN_LABEL L
----     USING (SELECT 'Add' LABEL_KEY,
----                   240010 XSCREEN_ID,
----                   'ILM' LABEL_BUNDLE_NAME
----              FROM DUAL) B
----        ON (    L.LABEL_KEY = B.LABEL_KEY
----            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
----            AND L.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XSCREEN_LABEL_ID,
----               XSCREEN_ID,
----               LABEL_KEY,
----               LABEL_BUNDLE_NAME)
----       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
----               '240010',
----               'Add',
----               'ILM');
----
----MERGE INTO XSCREEN_LABEL L
----     USING (SELECT 'Edit' LABEL_KEY,
----                   240010 XSCREEN_ID,
----                   'ILM' LABEL_BUNDLE_NAME
----              FROM DUAL) B
----        ON (    L.LABEL_KEY = B.LABEL_KEY
----            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
----            AND L.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XSCREEN_LABEL_ID,
----               XSCREEN_ID,
----               LABEL_KEY,
----               LABEL_BUNDLE_NAME)
----       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
----               '240010',
----               'Edit',
----               'ILM');
----
----MERGE INTO XSCREEN_LABEL L
----     USING (SELECT 'View' LABEL_KEY,
----                   240010 XSCREEN_ID,
----                   'ILM' LABEL_BUNDLE_NAME
----              FROM DUAL) B
----        ON (    L.LABEL_KEY = B.LABEL_KEY
----            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
----            AND L.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XSCREEN_LABEL_ID,
----               XSCREEN_ID,
----               LABEL_KEY,
----               LABEL_BUNDLE_NAME)
----       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
----               '240010',
----               'View',
----               'ILM');
----
----MERGE INTO XSCREEN_LABEL L
----     USING (SELECT 'Recommend\ Time\ Slots' LABEL_KEY,
----                   240010 XSCREEN_ID,
----                   'ILM' LABEL_BUNDLE_NAME
----              FROM DUAL) B
----        ON (    L.LABEL_KEY = B.LABEL_KEY
----            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
----            AND L.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XSCREEN_LABEL_ID,
----               XSCREEN_ID,
----               LABEL_KEY,
----               LABEL_BUNDLE_NAME)
----       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
----               '240010',
----               'Recommend\ Time\ Slots',
----               'ILM');
----
----
----MERGE INTO XSCREEN_LABEL L
----     USING (SELECT 'Duration' LABEL_KEY,
----                   240010 XSCREEN_ID,
----                   'ILM' LABEL_BUNDLE_NAME
----              FROM DUAL) B
----        ON (    L.LABEL_KEY = B.LABEL_KEY
----            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
----            AND L.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XSCREEN_LABEL_ID,
----               XSCREEN_ID,
----               LABEL_KEY,
----               LABEL_BUNDLE_NAME)
----       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
----               '240010',
----               'Duration',
----               'ILM');
----
----MERGE INTO XSCREEN_LABEL L
----     USING (SELECT 'Carrier' LABEL_KEY,
----                   240010 XSCREEN_ID,
----                   'ILM' LABEL_BUNDLE_NAME
----              FROM DUAL) B
----        ON (    L.LABEL_KEY = B.LABEL_KEY
----            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
----            AND L.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XSCREEN_LABEL_ID,
----               XSCREEN_ID,
----               LABEL_KEY,
----               LABEL_BUNDLE_NAME)
----       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
----               '240010',
----               'Carrier',
----               'ILM');
----
----MERGE INTO XSCREEN_LABEL L
----     USING (SELECT 'Appointment\ Objects' LABEL_KEY,
----                   240010 XSCREEN_ID,
----                   'ILM' LABEL_BUNDLE_NAME
----              FROM DUAL) B
----        ON (    L.LABEL_KEY = B.LABEL_KEY
----            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
----            AND L.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XSCREEN_LABEL_ID,
----               XSCREEN_ID,
----               LABEL_KEY,
----               LABEL_BUNDLE_NAME)
----       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
----               '240010',
----               'Appointment\ Objects',
----               'ILM');
----
----MERGE INTO XSCREEN_LABEL L
----     USING (SELECT 'SlotCapacities' LABEL_KEY,
----                   240010 XSCREEN_ID,
----                   'ILM' LABEL_BUNDLE_NAME
----              FROM DUAL) B
----        ON (    L.LABEL_KEY = B.LABEL_KEY
----            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
----            AND L.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XSCREEN_LABEL_ID,
----               XSCREEN_ID,
----               LABEL_KEY,
----               LABEL_BUNDLE_NAME)
----       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
----               '240010',
----               'SlotCapacities',
----               'ILM');
----
----MERGE INTO XSCREEN_LABEL L
----     USING (SELECT 'TotalCapacity' LABEL_KEY,
----                   240010 XSCREEN_ID,
----                   'ILM' LABEL_BUNDLE_NAME
----              FROM DUAL) B
----        ON (    L.LABEL_KEY = B.LABEL_KEY
----            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
----            AND L.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XSCREEN_LABEL_ID,
----               XSCREEN_ID,
----               LABEL_KEY,
----               LABEL_BUNDLE_NAME)
----       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
----               '240010',
----               'TotalCapacity',
----               'ILM');
----
----MERGE INTO XSCREEN_LABEL L
----     USING (SELECT 'UtilizedCapacity' LABEL_KEY,
----                   240010 XSCREEN_ID,
----                   'ILM' LABEL_BUNDLE_NAME
----              FROM DUAL) B
----        ON (    L.LABEL_KEY = B.LABEL_KEY
----            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
----            AND L.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XSCREEN_LABEL_ID,
----               XSCREEN_ID,
----               LABEL_KEY,
----               LABEL_BUNDLE_NAME)
----       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
----               '240010',
----               'UtilizedCapacity',
----               'ILM');
----
----MERGE INTO XSCREEN_LABEL L
----     USING (SELECT 'CarryOverCapacity' LABEL_KEY,
----                   240010 XSCREEN_ID,
----                   'ILM' LABEL_BUNDLE_NAME
----              FROM DUAL) B
----        ON (    L.LABEL_KEY = B.LABEL_KEY
----            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
----            AND L.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XSCREEN_LABEL_ID,
----               XSCREEN_ID,
----               LABEL_KEY,
----               LABEL_BUNDLE_NAME)
----       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
----               '240010',
----               'CarryOverCapacity',
----               'ILM');
			   
COMMIT;			   

-- DBTicket APPT-236

update param_def set value_generator_class='com.manh.appointment.ui.finitevalue.MissingAppointmentFields' where param_category='Appointment'
and param_def_id='warning_appointment_fields' and value_generator_class='com.manh.yms.appointment.finitevalue.MissingAppointmentFields';

Commit;

-- DBTicket APPT-243

update param_def set value_generator_class='com.manh.appointment.ui.finitevalue.MissingAppointmentFields' 
where param_category='Appointment'and value_generator_class='com.manh.yms.appointment.finitevalue.MissingAppointmentFields';

COMMIT;

-- DBTicket APPT-246

UPDATE custom_view_uiattr
SET custom_view_id  ='/appointment/ui/jsf/appointmentList.xhtml'
WHERE custom_view_id='/ilm/appointment/jsf/appointmentList.xhtml';

UPDATE custom_view_uiattr
SET custom_view_id  ='/appointment/ui/jsf/scheduleAppointment.xhtml'
WHERE custom_view_id='/ilm/appointment/jsf/scheduleAppointment.xhtml';

COMMIT;

-- DBTicket APPT-260

delete from RESOURCE_PERMISSION where RESOURCE_ID=(SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsf/chartcreatorrequest.jsf'
                         AND RESOURCES.MODULE = 'ACM') and PERMISSION_CODE='UCLCA';

delete from RESOURCE_PERMISSION where RESOURCE_ID=(SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsf/chartcreatorrequest.jsf'
                         AND RESOURCES.MODULE = 'ACM') and PERMISSION_CODE='UCLU';						 
						 
delete from resources where uri='/appointment/ui/jsf/chartcreatorrequest.jsf' and MODULE='ACM';
						 
MERGE INTO RESOURCES L
     USING (SELECT '/appointment/ui/jsf/chartcreatorrequest.jsf' URI,
                   1 URI_TYPE_ID,
                   NULL HTTP_METHOD
              FROM DUAL) B
        ON (    L.URI = B.URI
            AND L.URI_TYPE_ID = B.URI_TYPE_ID
            AND B.HTTP_METHOD IS NULL)
WHEN NOT MATCHED
THEN
   INSERT     (L.RESOURCE_ID,
               L.URI,
               L.MODULE,
               L.URI_TYPE_ID,
               L.HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/appointment/ui/jsf/chartcreatorrequest.jsf',
               'APPT',
               1,
               NULL);

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, ' VAPT' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/appointment/ui/jsf/chartcreatorrequest.jsf'
                   AND RESOURCES.URI_TYPE_ID = 1
                   AND RESOURCES.HTTP_METHOD IS NULL) RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE URI = '/appointment/ui/jsf/chartcreatorrequest.jsf'
                         AND RESOURCES.MODULE = 'APPT'),
                 'VAPT');
				 
COMMIT;				 		 

-- DBTicket APPT-274

BEGIN
FOR rec IN 
(
SELECT CHECKIN_DTTM,APPOINTMENT_ID FROM ilm_appointments WHERE CHECKIN_DTTM IS NOT NULL AND actual_checkin_dttm IS NULL AND SCHEDULED_DTTM IS NULL
)
LOOP
update ilm_appointments i set i.actual_checkin_dttm= rec.CHECKIN_DTTM,i.SCHEDULED_DTTM=rec.CHECKIN_DTTM  where i.APPOINTMENT_ID =rec.APPOINTMENT_ID;
END LOOP;
commit;
END;
/



-- DBTicket APPT-306

MERGE INTO APPT_UPLOAD_TYPE L USING
(SELECT 5 APPT_UPLOAD_TYPE FROM DUAL
) B ON (L.APPT_UPLOAD_TYPE = B.APPT_UPLOAD_TYPE)
WHEN NOT MATCHED THEN
  INSERT
    (
      APPT_UPLOAD_TYPE,
      DESCRIPTION
    )
    VALUES
    (
      5,
      'Appointment Priority Setup'
    ); 
    
COMMIT;    


-- DBTicket APPT-217

insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) values(2409981,'Appointment Parameters','Appointment Parameters','/appointment/admin/parameter/jsp/ViewAppointmentParameters.jsp','','default-icon','#002E5F',5,1,'Business-Rules',0);
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where NAME='Appointment Parameters' and BASE_SECTION_NAME='Business-Rules' and NAVIGATION_KEY='/appointment/admin/parameter/jsp/ViewAppointmentParameters.jsp'),'VBR');

COMMIT;

-- DBTicket APPT-326

MERGE INTO PARAM_DEF P USING
(SELECT 'use_new_Calendar' PARAM_DEF_ID, 'ILM' PARAM_GROUP_ID FROM DUAL
) D ON (P.PARAM_DEF_ID = D.PARAM_DEF_ID AND P.PARAM_GROUP_ID = D.PARAM_GROUP_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      PARAM_DEF_ID ,
      PARAM_GROUP_ID ,
      PARAM_SUBGROUP_ID ,
      PARAM_NAME ,
      DESCRIPTION ,
      IS_DISABLED ,
      IS_REQUIRED ,
      DATA_TYPE ,
      UI_TYPE ,
      MIN_VALUE ,
      IS_MIN_INC ,
      MAX_VALUE ,
      IS_MAX_INC ,
      DFLT_VALUE ,
      DISPLAY_ORDER ,
      VALUE_GENERATOR_CLASS ,
      IS_LOCALIZABLE ,
      PARAM_CATEGORY ,
      CARR_DFLT_VALUE ,
      BP_DFLT_VALUE ,
      PARAM_VALIDATOR_CLASS ,
      CREATED_DTTM ,
      LAST_UPDATED_DTTM
    )
    VALUES
    (
      'use_new_Calendar',
      'ILM',
      'YARD',
      'Use new Calendar',
      'Use new Calendar' ,
      0,
      0,
      6,
      'CHECKBOX',
      NULL,
      0,
      7,
      1,
      NULL,
      18,
      NULL,
      1,
      NULL,
      NULL,
      NULL,
      NULL,
      systimestamp,
      systimestamp
    );

COMMIT;
  
  
-- DBTicket APPT-329

UPDATE resources
SET MODULE = 'APPT'
WHERE uri LIKE '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/cleanup';

MERGE INTO RESOURCES L USING
(SELECT '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/updateAppointmentInfo' URI,
  2 URI_TYPE_ID,
  'GET' HTTP_METHOD
FROM DUAL
) B ON (L.URI = B.URI AND L.URI_TYPE_ID=B.URI_TYPE_ID AND L.HTTP_METHOD=B.HTTP_METHOD)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.RESOURCE_ID,
      L.URI,
      L.MODULE,
      L.URI_TYPE_ID,
      L.HTTP_METHOD
    )
    VALUES
    (
      SEQ_RESOURCE_ID.NEXTVAL,
      '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/updateAppointmentInfo',
      'APPT',
      2,
      'GET'
    );

MERGE INTO RESOURCE_PERMISSION RP USING
  (SELECT RESOURCES.RESOURCE_ID,
      'AAPT' AS PERMISSION_CODE
    FROM RESOURCES
    WHERE RESOURCES.URI       ='/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/updateAppointmentInfo'
    AND RESOURCES.URI_TYPE_ID =2
    AND RESOURCES.HTTP_METHOD ='GET'
  )
  RP1 ON (RP.RESOURCE_ID = RP1.RESOURCE_ID AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED THEN
  INSERT
    (
      RESOURCE_ID,
      PERMISSION_CODE
    )
    VALUES
    (
      (SELECT RESOURCE_ID
        FROM RESOURCES
        WHERE URI            ='/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/updateAppointmentInfo'
        AND RESOURCES.MODULE ='APPT'
      )
      ,
      'AAPT'
    );
    
COMMIT;      

-- DBTicket APPT-335

UPDATE xmenu_item SET navigation_key  ='/appointment/infeasibility/dockEquipment/jsp/ViewInfeasibilities.jsp' WHERE navigation_key='/ilm/infeasibility/dockEquipment/jsp/ViewInfeasibilities.jsp';
UPDATE xmenu_item SET navigation_key  ='/appointment/infeasibility/jsp/AppointmentTypeDockInfeasibilitySetup.jsp' WHERE navigation_key='/ilm/infeasibility/jsp/AppointmentTypeDockInfeasibilitySetup.jsp';
UPDATE xmenu_item SET navigation_key  ='/appointment/infeasibility/dockProductClass/jsp/ViewInfeasibilities.jsp' WHERE navigation_key='/ilm/infeasibility/dockProductClass/jsp/ViewInfeasibilities.jsp';
UPDATE xmenu_item SET navigation_key  ='/appointment/infeasibility/dockProtectionLevel/jsp/ViewInfeasibilities.jsp' WHERE navigation_key='/ilm/infeasibility/dockProtectionLevel/jsp/ViewInfeasibilities.jsp';
UPDATE xmenu_item SET navigation_key  ='/appointment/infeasibility/jsp/AppointmentTypeDockInfeasibilitySetup.jsp' WHERE navigation_key='/ilm/infeasibility/jsp/AppointmentTypeDockInfeasibilitySetup.jsp';

COMMIT;

-- DBTicket APPT-338

update xmenu_item set navigation_key='/appointment/ui/jsp/appointmentReport.jsp?calendarType=YARDCALENDAR' WHERE navigation_key like '/appointment/ui/jsp/appointmentReport.jsp?%';

COMMIT;

-- DBTicket APPT-339

MERGE INTO RESOURCES L USING (SELECT '/appointment/ui/jsf/calendarApptTransit.xhtml' URI,1 URI_TYPE_ID,null HTTP_METHOD FROM DUAL) B ON (L.URI = B.URI AND L.URI_TYPE_ID=B.URI_TYPE_ID AND B.HTTP_METHOD IS NULL) WHEN NOT MATCHED THEN INSERT (L.RESOURCE_ID, L.URI, L.MODULE, L.URI_TYPE_ID,L.HTTP_METHOD) VALUES (SEQ_RESOURCE_ID.NEXTVAL,'/appointment/ui/jsf/calendarApptTransit.xhtml','APPT',1,null);

MERGE INTO RESOURCE_PERMISSION RP USING (SELECT RESOURCES.RESOURCE_ID,'AAPT' AS PERMISSION_CODE FROM RESOURCES WHERE RESOURCES.URI ='/appointment/ui/jsf/calendarApptTransit.xhtml' AND RESOURCES.URI_TYPE_ID =1 AND RESOURCES.HTTP_METHOD IS NULL) RP1 ON (RP.RESOURCE_ID = RP1.RESOURCE_ID AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE) WHEN NOT MATCHED THEN INSERT     (RESOURCE_ID, PERMISSION_CODE) VALUES ( (SELECT RESOURCE_ID FROM RESOURCES WHERE URI ='/appointment/ui/jsf/calendarApptTransit.xhtml' AND RESOURCES.MODULE ='APPT'),'AAPT');

COMMIT;


-- DBTicket APPT-346

DELETE FROM company_parameter WHERE param_def_id = 'use_new_Calendar';

DELETE FROM opt_param WHERE param_def_id = 'use_new_Calendar';

DELETE
FROM param_def
WHERE param_def_id = 'use_new_Calendar';

MERGE INTO PARAM_DEF P USING
(SELECT 'Appointment_Calendar_Screen' PARAM_DEF_ID,
  'ILM' PARAM_GROUP_ID
FROM DUAL
) D ON (P.PARAM_DEF_ID = D.PARAM_DEF_ID AND P.PARAM_GROUP_ID = D.PARAM_GROUP_ID)
WHEN NOT MATCHED THEN
  INSERT (PARAM_DEF_ID ,
    PARAM_GROUP_ID ,
    PARAM_SUBGROUP_ID ,
    PARAM_NAME ,
    DESCRIPTION ,
    IS_DISABLED ,
    IS_REQUIRED ,
    DATA_TYPE ,
    UI_TYPE ,
    MIN_VALUE ,
    IS_MIN_INC ,
    MAX_VALUE ,
    IS_MAX_INC ,
    DFLT_VALUE ,
    DISPLAY_ORDER ,
    VALUE_GENERATOR_CLASS ,
    IS_LOCALIZABLE ,
    PARAM_CATEGORY ,
    CARR_DFLT_VALUE ,
    BP_DFLT_VALUE ,
    PARAM_VALIDATOR_CLASS ,
    CREATED_DTTM ,
    LAST_UPDATED_DTTM ) VALUES
  (
    'Appointment_Calendar_Screen', 'ILM','YARD', 'Appointment Calendar Screen ','Appointment Calendar Screen ', 0,0,11, 'DROPDOWN', 0,0,0,0, 'Regular',NULL, 'com.manh.appointment.ui.finitevalue.AppointmentCalendarScreen', '1','null','null','null','null',systimestamp, systimestamp
  )
  ;

UPDATE xmenu_item
  SET navigation_key   = '/appointment/ui/jsp/oldOrNewCalendar.jsp'
  WHERE navigation_key = '/appointment/ui/jsp/appointmentPlanning.jsp? calendarType=YARDCALENDAR' ;

MERGE INTO RESOURCES L USING
  (SELECT '/appointment/ui/jsp/oldOrNewCalendar.jsp' URI,
    1 URI_TYPE_ID,
    NULL HTTP_METHOD
  FROM DUAL
  ) B ON (L.URI = B.URI AND L.URI_TYPE_ID=B.URI_TYPE_ID AND B.HTTP_METHOD IS NULL)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.RESOURCE_ID,
      L.URI,
      L.MODULE,
      L.URI_TYPE_ID,
      L.HTTP_METHOD
    )
    VALUES
    (
      SEQ_RESOURCE_ID.NEXTVAL,
      '/appointment/ui/jsp/oldOrNewCalendar.jsp',
      'APPT',
      1,
      NULL
    );

MERGE INTO RESOURCE_PERMISSION RP USING
  (SELECT RESOURCES.RESOURCE_ID,
      'VAPT' AS PERMISSION_CODE
    FROM RESOURCES
    WHERE RESOURCES.URI        ='/appointment/ui/jsp/oldOrNewCalendar.jsp'
    AND RESOURCES.URI_TYPE_ID  =1
    AND RESOURCES.HTTP_METHOD IS NULL
  )
  RP1 ON (RP.RESOURCE_ID = RP1.RESOURCE_ID AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED THEN
  INSERT
    (
      RESOURCE_ID,
      PERMISSION_CODE
    )
    VALUES
    (
      (SELECT RESOURCE_ID
        FROM RESOURCES
        WHERE URI            ='/appointment/ui/jsp/oldOrNewCalendar.jsp'
        AND RESOURCES.MODULE ='APPT'
      )
      ,
      'VAPT'
    ); 
    
COMMIT;    

-- DBTicket TPE-36323

DELETE FROM RESOURCE_PERMISSION WHERE RESOURCE_ID IN (SELECT RESOURCE_ID FROM RESOURCES WHERE URI ='/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/cleanup' AND RESOURCES.URI_TYPE_ID =2 AND RESOURCES.HTTP_METHOD ='GET');
DELETE FROM RESOURCES WHERE URI ='/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/cleanup' AND RESOURCES.URI_TYPE_ID =2 AND RESOURCES.HTTP_METHOD ='GET';
MERGE INTO RESOURCES L USING (SELECT '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/cleanup' URI,2 URI_TYPE_ID,'GET' HTTP_METHOD FROM DUAL) B ON (L.URI = B.URI AND L.URI_TYPE_ID=B.URI_TYPE_ID AND L.HTTP_METHOD=B.HTTP_METHOD) WHEN NOT MATCHED THEN INSERT (L.RESOURCE_ID, L.URI, L.MODULE, L.URI_TYPE_ID,L.HTTP_METHOD) VALUES (SEQ_RESOURCE_ID.NEXTVAL,'/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/cleanup','VAPT',2,'GET');
MERGE INTO RESOURCE_PERMISSION RP USING (SELECT RESOURCES.RESOURCE_ID,'VAPT' AS PERMISSION_CODE FROM RESOURCES WHERE RESOURCES.URI ='/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/cleanup' AND RESOURCES.URI_TYPE_ID =2 AND RESOURCES.HTTP_METHOD ='GET') RP1 ON (RP.RESOURCE_ID = RP1.RESOURCE_ID AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE) WHEN NOT MATCHED THEN INSERT     (RESOURCE_ID, PERMISSION_CODE) VALUES ( (SELECT RESOURCE_ID FROM RESOURCES WHERE URI ='/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/cleanup' AND RESOURCES.MODULE ='VAPT'),'VAPT');

COMMIT;

-- DBTicket APPT-362

----delete from xscreen_label where xscreen_id = 240010 and label_bundle_name = 'TPE';

MERGE INTO LABEL L USING (SELECT 'ILM' BUNDLE_NAME,'AppointmentCalendar' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'AppointmentCalendar','Appointment Calendar','ILM');
MERGE INTO LABEL L USING (SELECT 'ILM' BUNDLE_NAME,'Week' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'Week','Week','ILM');
MERGE INTO LABEL L USING (SELECT 'ILM' BUNDLE_NAME,'Slot' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'Slot','Slot','ILM');

----MERGE INTO XSCREEN_LABEL L USING (SELECT 'AppointmentCalendar' LABEL_KEY,240010 XSCREEN_ID,'ILM' LABEL_BUNDLE_NAME FROM DUAL) B ON (L.LABEL_KEY = B.LABEL_KEY AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME AND L.XSCREEN_ID = B.XSCREEN_ID) WHEN NOT MATCHED THEN INSERT (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_KEY,LABEL_BUNDLE_NAME) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,'240010','AppointmentCalendar','ILM');
----MERGE INTO XSCREEN_LABEL L USING (SELECT 'Week' LABEL_KEY,240010 XSCREEN_ID,'ILM' LABEL_BUNDLE_NAME FROM DUAL) B ON (L.LABEL_KEY = B.LABEL_KEY AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME AND L.XSCREEN_ID = B.XSCREEN_ID) WHEN NOT MATCHED THEN INSERT (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_KEY,LABEL_BUNDLE_NAME) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,'240010','Week','ILM');
----MERGE INTO XSCREEN_LABEL L USING (SELECT 'Slot' LABEL_KEY,240010 XSCREEN_ID,'ILM' LABEL_BUNDLE_NAME FROM DUAL) B ON (L.LABEL_KEY = B.LABEL_KEY AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME AND L.XSCREEN_ID = B.XSCREEN_ID) WHEN NOT MATCHED THEN INSERT (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_KEY,LABEL_BUNDLE_NAME) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,'240010','Slot','ILM');
----
COMMIT;

-- DBTicket APPT-345

DELETE FROM XBASE_MENU_ITEM where XMENU_ITEM_ID in (2400126,2400127,2400128,2400129);
DELETE FROM XMENU_ITEM_PARAMETER where XMENU_ITEM_ID in (2400126,2400127,2400128,2400129);
DELETE FROM XMENU_ITEM_PERMISSION where XMENU_ITEM_ID in (2400126,2400127,2400128,2400129);
DELETE FROM XMENU_ITEM_APP WHERE  XMENU_ITEM_ID in (2400126,2400127,2400128,2400129);
DELETE FROM XQUICK_MENU_ITEM WHERE XMENU_ITEM_ID in (2400126,2400127,2400128,2400129);
DELETE FROM XMENU_ITEM WHERE  XMENU_ITEM_ID in (2400126,2400127,2400128,2400129);

insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) values(2400126,'Dock - Equipment','Dock-Equip.','/appointment/infeasibility/dockEquipment/jsp/ViewInfeasibilities.jsp','','default-icon','#002E5F',5,1,'Business-Rules',0);
insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) values(2400127,'Dock - Product Class','Dock-Prod. Class','/appointment/infeasibility/dockProductClass/jsp/ViewInfeasibilities.jsp','','default-icon','#002E5F',5,1,'Business-Rules',0);
insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) values(2400128,'Dock - Protection Level','Dock-Prot. Level','/appointment/infeasibility/dockProtectionLevel/jsp/ViewInfeasibilities.jsp','','default-icon','#002E5F',5,1,'Business-Rules',0);
insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) values(2400129,'Dock - Appointment Type','Dock-Appt. Type','/appointment/infeasibility/jsp/AppointmentTypeDockInfeasibilitySetup.jsp','','default-icon','#002E5F',5,1,'Business-Rules',0);



insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values(2400126,'VBR');
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values(2400127,'VBR');
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values(2400128,'VBR');
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values(2400129,'VBR');

COMMIT;


-- DBTicket APPT-438

COMMENT ON TABLE UN_ASSIGNED_APPT_OBJECT
IS
  'Details of PO or Shipment which is not associated with any appointment after planner run';
  COMMENT ON TABLE SLOT_RECOMMENDATION_CRITERIA
IS
  'Recommendation criteria at slot level will be stored here';
  COMMENT ON TABLE ILM_APPT_UTIL_NEW
IS
  'Utilization details of capacity will be stored here';
  COMMENT ON TABLE APPT_OBJECT_INFEASIBLE_APPT
IS
  'Stores feasible appointment and times for planning';
  COMMENT ON TABLE APPT_OBJECT_PK
IS
  'Stores primary key for the tables which are used in appointment report';


COMMENT ON COLUMN APPT_OBJECT_PK.APPT_OBJECT_PK_ID IS 'Internal id of planned Appointment';
COMMENT ON COLUMN APPT_OBJECT_PK.APPT_OBJECT_TYPE IS 'Transaction Object type for which appointment is planned in Appointment Report';
COMMENT ON COLUMN APPT_OBJECT_PK.KEY_DTTM IS 'Date Time at which the Appointment Report got executed';
COMMENT ON COLUMN APPT_OBJECT_PK.APPT_OBJECT_ID IS 'Internal id of Object assoicated to the Appointment';
COMMENT ON COLUMN APPT_OBJECT_PK.FACILITY_ID IS 'Internal id for facility for which Appointment Report is scheduled';


-- DBTicket APPT-520
----Moved the DDL to CBO Script
----CREATE INDEX OLI_STAT_ITEM_COMP_ORD_IDX ON ORDER_LINE_ITEM (DO_DTL_STATUS,ITEM_ID,TC_COMPANY_ID,ORDER_ID) TABLESPACE APPT_TXN_IDX_TBS;


-- DBTicket APPT-522


INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ILM_CONCAT','FUNCTION','APPT','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ILM_DETAILS_CONCAT','FUNCTION','APPT','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('INS_APPOINTMENT_EVENT','PROCEDURE','APPT','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ILM_ACTIVITY_T_LST_UPDT_DT_TRG','TRIGGER','APPT','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ILM_APPOI_AU_TR1','TRIGGER','APPT','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ILM_APPTMENTS_LST_UPDT_DT_TRG','TRIGGER','APPT','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ILM_APPT_CHARG_LST_UPDT_DT_TRG','TRIGGER','APPT','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ILM_APPT_CUSTO_LST_UPDT_DT_TRG','TRIGGER','APPT','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ILM_APPT_LOAD__LST_UPDT_DT_TRG','TRIGGER','APPT','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ILM_APPT_SIZES_LST_UPDT_DT_TRG','TRIGGER','APPT','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ILM_APPT_SLOT__LST_UPDT_DT_TRG','TRIGGER','APPT','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ILM_APPT_UTIL__LST_UPDT_DT_TRG','TRIGGER','APPT','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ILM_APPT__AU_TR1','TRIGGER','APPT','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ILM_REASO_AU_TR1','TRIGGER','APPT','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ILM_REQUESTOR_LST_UPDT_DT_TRG','TRIGGER','APPT','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ILM_APPOINTMENT_VIEW','VIEW','APPT','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ILM_APPT_EQUIPMENTS_VIEW','VIEW','APPT','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ILM_APPT_TRAILER','VIEW','APPT','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ILM_BILL_SEALNUMBER_VIEW','VIEW','APPT','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ILM_LOCATION_VIEW','VIEW','APPT','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ILM_TASKS_DEST_LOCATION_VIEW','VIEW','APPT','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ILM_TASKS_TRAILER_VIEW','VIEW','APPT','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ILM_TRAILER_VIEW','VIEW','APPT','2014');
COMMIT;


-- DBTicket APPT-582
----Only DB2 related changes	


-- DBTicket APPT-683

MERGE INTO LABEL L
     USING (SELECT 'ILM' BUNDLE_NAME, 'RemainingCapacity' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'RemainingCapacity',
               'Remaining Capacity',
               'ILM');
			   
			   
----MERGE INTO XSCREEN_LABEL L
----     USING (SELECT 'RemainingCapacity' LABEL_KEY,
----                   240010 XSCREEN_ID,
----                   'ILM' LABEL_BUNDLE_NAME
----              FROM DUAL) B
----        ON (    L.LABEL_KEY = B.LABEL_KEY
----            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
----            AND L.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XSCREEN_LABEL_ID,
----               XSCREEN_ID,
----               LABEL_KEY,
----               LABEL_BUNDLE_NAME)
----       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
----               '240010',
----               'RemainingCapacity',
----               'ILM');			

COMMIT;			   


-- DBTicket APPT-715

create or replace
PROCEDURE INS_APPOINTMENT_EVENT 
(
   vAPPOINTMENTID   IN ILM_APPOINTMENT_EVENTS.APPOINTMENT_ID%TYPE,
   vFieldName       IN ILM_APPOINTMENT_EVENTS.FIELD_NAME%TYPE,
   vOldValue        IN ILM_APPOINTMENT_EVENTS.OLD_VALUE%TYPE,
   vNewValue        IN ILM_APPOINTMENT_EVENTS.NEW_VALUE%TYPE,
   vSourceType      IN ILM_APPOINTMENT_EVENTS.CREATED_SOURCE_TYPE%TYPE,
   vSource          IN ILM_APPOINTMENT_EVENTS.CREATED_SOURCE%TYPE
   )
AS
   vEventSeq           NUMBER (8, 0);
   l_old_value         VARCHAR2 (500) := voldvalue;
   l_new_value         VARCHAR2 (500) := vnewValue;
   l_appt_type_desc    VARCHAR2 (50);
   l_appt_type_desc2   VARCHAR2 (50);
BEGIN
   SELECT NVL (MAX (EVENT_SEQ), 0) + 1
     INTO vEventSeq
     FROM ILM_APPOINTMENT_EVENTS
    WHERE APPOINTMENT_ID = vAPPOINTMENTID;

   IF vFieldName = 'APPT_TYPE'
   THEN
      SELECT DESCRIPTION
        INTO l_old_value
        FROM ILM_APPOINTMENT_TYPE
       WHERE APPT_TYPE = voldvalue;

      SELECT DESCRIPTION
        INTO l_new_value
        FROM ILM_APPOINTMENT_TYPE
       WHERE APPT_TYPE = vnewvalue;
   END IF;

   IF vFieldName = 'APPT_STATUS'
   THEN
      SELECT DESCRIPTION
        INTO l_old_value
        FROM ILM_APPOINTMENT_STATUS
       WHERE APPT_STATUS_CODE = voldvalue;

      SELECT DESCRIPTION
        INTO l_new_value
        FROM ILM_APPOINTMENT_STATUS
       WHERE APPT_STATUS_CODE = vnewvalue;
   END IF;

   IF vFieldName = 'CARRIER_CODE'
   THEN
      SELECT CARRIER_CODE
        INTO l_old_value
        FROM CARRIER_CODE
       WHERE CARRIER_ID = voldvalue;

      SELECT CARRIER_CODE
        INTO l_new_value
        FROM CARRIER_CODE
       WHERE CARRIER_ID = vnewvalue;
   END IF;

   IF vFieldName = 'EQUIPMENT_ID'
   THEN
      SELECT EQUIPMENT_CODE
        INTO l_old_value
        FROM EQUIPMENT
       WHERE EQUIPMENT_ID = voldvalue;

      SELECT EQUIPMENT_CODE
        INTO l_new_value
        FROM EQUIPMENT
       WHERE EQUIPMENT_ID = vnewvalue;
   END IF;

   IF vFieldName = 'LOADING_TYPE'
   THEN
      SELECT DESCRIPTION
        INTO l_old_value
        FROM LOADING_TYPE
       WHERE LOADING_TYPE = voldvalue;

      SELECT DESCRIPTION
        INTO l_new_value
        FROM LOADING_TYPE
       WHERE LOADING_TYPE = vnewvalue;
   END IF;

   IF vFieldName = 'REQUEST_COMM_TYPE'
   THEN
      SELECT DESCRIPTION
        INTO l_old_value
        FROM COMM_METHOD
       WHERE COMM_METHOD = voldvalue;

      SELECT DESCRIPTION
        INTO l_new_value
        FROM COMM_METHOD
       WHERE COMM_METHOD = vnewvalue;
   END IF;

   IF vFieldName = 'DRIVER_ID'
   THEN
      l_old_value := getdriver (voldvalue);
      l_new_value := getdriver (vnewvalue);
   END IF;

   INSERT INTO ILM_APPOINTMENT_EVENTS (APPOINTMENT_ID,
                                       EVENT_SEQ,
                                       FIELD_NAME,
                                       OLD_VALUE,
                                       NEW_VALUE,
                                       CREATED_SOURCE_TYPE,
                                       CREATED_SOURCE,
                                       CREATED_DTTM)
        VALUES (vAPPOINTMENTID,
                vEventSeq,
                vFieldName,
                l_old_value,
                l_new_value,
                vSourceType,
                vSource,
                SYSDATE);
END;
/


-- DBTicket APPT-581
ALTER TABLE ILM_APPOINTMENTS ADD BP_ID NUMBER(10);
COMMENT ON COLUMN ILM_APPOINTMENTS.BP_ID IS 'This column stores bussiness partner id';
ALTER TABLE ILM_APPOINTMENTS ADD CONSTRAINT BP_ID_FK FOREIGN KEY (BP_ID) REFERENCES BUSINESS_PARTNER(BP_ID);


-- DBTicket APPT-709 PLSQL
@DBScripts/Product/PLSQL_Objects/ILM_TRAILER_VIEW.sql

 
-- DBTicket APPT-721

ALTER TABLE ILM_APPOINTMENT_EVENTS MODIFY EVENT_SEQ NUMBER(8); 

create or replace
PROCEDURE INS_APPOINTMENT_EVENT 
(
   vAPPOINTMENTID   IN ILM_APPOINTMENT_EVENTS.APPOINTMENT_ID%TYPE,
   vFieldName       IN ILM_APPOINTMENT_EVENTS.FIELD_NAME%TYPE,
   vOldValue        IN ILM_APPOINTMENT_EVENTS.OLD_VALUE%TYPE,
   vNewValue        IN ILM_APPOINTMENT_EVENTS.NEW_VALUE%TYPE,
   vSourceType      IN ILM_APPOINTMENT_EVENTS.CREATED_SOURCE_TYPE%TYPE,
   vSource          IN ILM_APPOINTMENT_EVENTS.CREATED_SOURCE%TYPE
   )
AS
   vEventSeq           NUMBER (8, 0);
   l_old_value         VARCHAR2 (500) := voldvalue;
   l_new_value         VARCHAR2 (500) := vnewValue;
   l_appt_type_desc    VARCHAR2 (50);
   l_appt_type_desc2   VARCHAR2 (50);
BEGIN
   SELECT NVL (MAX (EVENT_SEQ), 0) + 1
     INTO vEventSeq
     FROM ILM_APPOINTMENT_EVENTS
    WHERE APPOINTMENT_ID = vAPPOINTMENTID;

   IF vFieldName = 'APPT_TYPE'
   THEN
      SELECT DESCRIPTION
        INTO l_old_value
        FROM ILM_APPOINTMENT_TYPE
       WHERE APPT_TYPE = voldvalue;

      SELECT DESCRIPTION
        INTO l_new_value
        FROM ILM_APPOINTMENT_TYPE
       WHERE APPT_TYPE = vnewvalue;
   END IF;

   IF vFieldName = 'APPT_STATUS'
   THEN
      SELECT DESCRIPTION
        INTO l_old_value
        FROM ILM_APPOINTMENT_STATUS
       WHERE APPT_STATUS_CODE = voldvalue;

      SELECT DESCRIPTION
        INTO l_new_value
        FROM ILM_APPOINTMENT_STATUS
       WHERE APPT_STATUS_CODE = vnewvalue;
   END IF;

   IF vFieldName = 'CARRIER_CODE'
   THEN
      SELECT CARRIER_CODE
        INTO l_old_value
        FROM CARRIER_CODE
       WHERE CARRIER_ID = voldvalue;

      SELECT CARRIER_CODE
        INTO l_new_value
        FROM CARRIER_CODE
       WHERE CARRIER_ID = vnewvalue;
   END IF;

   IF vFieldName = 'EQUIPMENT_ID'
   THEN
      SELECT EQUIPMENT_CODE
        INTO l_old_value
        FROM EQUIPMENT
       WHERE EQUIPMENT_ID = voldvalue;

      SELECT EQUIPMENT_CODE
        INTO l_new_value
        FROM EQUIPMENT
       WHERE EQUIPMENT_ID = vnewvalue;
   END IF;

   IF vFieldName = 'LOADING_TYPE'
   THEN
      SELECT DESCRIPTION
        INTO l_old_value
        FROM LOADING_TYPE
       WHERE LOADING_TYPE = voldvalue;

      SELECT DESCRIPTION
        INTO l_new_value
        FROM LOADING_TYPE
       WHERE LOADING_TYPE = vnewvalue;
   END IF;

   IF vFieldName = 'REQUEST_COMM_TYPE'
   THEN
      SELECT DESCRIPTION
        INTO l_old_value
        FROM COMM_METHOD
       WHERE COMM_METHOD = voldvalue;

      SELECT DESCRIPTION
        INTO l_new_value
        FROM COMM_METHOD
       WHERE COMM_METHOD = vnewvalue;
   END IF;

   IF vFieldName = 'DRIVER_ID'
   THEN
      l_old_value := getdriver (voldvalue);
      l_new_value := getdriver (vnewvalue);
   END IF;

   INSERT INTO ILM_APPOINTMENT_EVENTS (APPOINTMENT_ID,
                                       EVENT_SEQ,
                                       FIELD_NAME,
                                       OLD_VALUE,
                                       NEW_VALUE,
                                       CREATED_SOURCE_TYPE,
                                       CREATED_SOURCE,
                                       CREATED_DTTM)
        VALUES (vAPPOINTMENTID,
                vEventSeq,
                vFieldName,
                l_old_value,
                l_new_value,
                vSourceType,
                vSource,
                SYSDATE);
END;
/


------ DBTicket APPT-782
----
----DECLARE
----V_CNT NUMBER;
----BEGIN
----   SELECT COUNT(*) INTO V_CNT 
----                        from user_constraints 
----                        where constraint_name='APPT_OBJECT_PK';
----   IF (V_CNT > 0)
----   THEN
----      EXECUTE IMMEDIATE 'ALTER TABLE APPT_OBJECT_FEASIBLE_APPT DROP CONSTRAINT APPT_OBJECT_PK USING INDEX';
----      EXECUTE IMMEDIATE 'ALTER TABLE APPT_OBJECT_FEASIBLE_APPT ADD CONSTRAINT APPT_OBJECT_FEASIBLE_APPT_PK PRIMARY KEY (APPT_OBJECT_PK_ID) USING TABLESPACE APPT_TXN_IDX_TBS';
----   END IF;
----EXCEPTION
----   WHEN OTHERS
----   THEN
----      NULL;
----END;
----/
----SHOW ERRORS;
----
----COMMIT;


-- DBTicket APPT-701


delete  from XMENU_ITEM_PERMISSION  where XMENU_ITEM_ID in (select XMENU_ITEM_ID from XMENU_ITEM where NAME='Slot-Appointment Infeasibility' and NAVIGATION_KEY='/appointment/infeasibility/jsp/SlotAppointmentTypeInfeasibilitySetup.jsp' and BASE_SECTION_NAME='Business-Rules');
delete  from XMENU_ITEM_PERMISSION  where XMENU_ITEM_ID in (select XMENU_ITEM_ID from XMENU_ITEM where NAME='Appointment Setup' and NAVIGATION_KEY='/appointment/facility/FacilityList.jsflps' and BASE_SECTION_NAME='ExtendedEnterprise');
delete  from XMENU_ITEM_PERMISSION  where XMENU_ITEM_ID in (select XMENU_ITEM_ID from XMENU_ITEM where NAME='Appointments - Archive' and NAVIGATION_KEY='/appointment/ui/jsf/appointmentList.jsflps?isArchive=true' and BASE_SECTION_NAME='Yard Management');
delete  from XMENU_ITEM_PERMISSION  where XMENU_ITEM_ID in (select XMENU_ITEM_ID from XMENU_ITEM where NAME='Add Appointment VCP' and NAVIGATION_KEY='/appointment/ui/jsf/scheduleAppointment.jsflps' and BASE_SECTION_NAME='Transportation Lifecycle Management' );


MERGE INTO XMENU_ITEM_PERMISSION L
     USING (SELECT (select XMENU_ITEM_ID from XMENU_ITEM where NAME='Slot-Appointment Infeasibility' and NAVIGATION_KEY='/appointment/infeasibility/jsp/SlotAppointmentTypeInfeasibilitySetup.jsp' and BASE_SECTION_NAME='Business-Rules') XMENU_ITEM_ID,'VBR' PERMISSION_CODE
              FROM DUAL) B
        ON (L.XMENU_ITEM_ID = B.XMENU_ITEM_ID AND L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT  (XMENU_ITEM_ID, PERMISSION_CODE)
       VALUES (
               (select XMENU_ITEM_ID from XMENU_ITEM where NAME='Slot-Appointment Infeasibility' and NAVIGATION_KEY='/appointment/infeasibility/jsp/SlotAppointmentTypeInfeasibilitySetup.jsp' and BASE_SECTION_NAME='Business-Rules'),
               'VBR');	
			   

MERGE INTO XMENU_ITEM_PERMISSION L
     USING (SELECT (select XMENU_ITEM_ID from XMENU_ITEM where NAME='Appointment Setup' and NAVIGATION_KEY='/appointment/facility/FacilityList.jsflps' and BASE_SECTION_NAME='ExtendedEnterprise') XMENU_ITEM_ID,'AAST' PERMISSION_CODE
              FROM DUAL) B
        ON (L.XMENU_ITEM_ID = B.XMENU_ITEM_ID AND L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT  (XMENU_ITEM_ID, PERMISSION_CODE)
       VALUES (
               (select XMENU_ITEM_ID from XMENU_ITEM where NAME='Appointment Setup' and NAVIGATION_KEY='/appointment/facility/FacilityList.jsflps' and BASE_SECTION_NAME='ExtendedEnterprise'),
               'AAST');	
			   

MERGE INTO XMENU_ITEM_PERMISSION L
     USING (SELECT (select XMENU_ITEM_ID from XMENU_ITEM where NAME='Appointments - Archive' and NAVIGATION_KEY='/appointment/ui/jsf/appointmentList.jsflps?isArchive=true' and BASE_SECTION_NAME='Yard Management') XMENU_ITEM_ID,'VAPT' PERMISSION_CODE
              FROM DUAL) B
        ON (L.XMENU_ITEM_ID = B.XMENU_ITEM_ID AND L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT  (XMENU_ITEM_ID, PERMISSION_CODE)
       VALUES (
               (select XMENU_ITEM_ID from XMENU_ITEM where NAME='Appointments - Archive' and NAVIGATION_KEY='/appointment/ui/jsf/appointmentList.jsflps?isArchive=true' and BASE_SECTION_NAME='Yard Management'),
               'VAPT');	

MERGE INTO XMENU_ITEM_PERMISSION L
     USING (SELECT (select XMENU_ITEM_ID from XMENU_ITEM where NAME='Add Appointment VCP' and NAVIGATION_KEY='/appointment/ui/jsf/scheduleAppointment.jsflps' and BASE_SECTION_NAME='Transportation Lifecycle Management') XMENU_ITEM_ID,'AAPT' PERMISSION_CODE
              FROM DUAL) B
        ON (L.XMENU_ITEM_ID = B.XMENU_ITEM_ID AND L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT  (XMENU_ITEM_ID, PERMISSION_CODE)
       VALUES (
               (select XMENU_ITEM_ID from XMENU_ITEM where NAME='Add Appointment VCP' and NAVIGATION_KEY='/appointment/ui/jsf/scheduleAppointment.jsflps' and BASE_SECTION_NAME='Transportation Lifecycle Management'),
               'AAPT');	
			   
COMMIT;

-- DBTicket APPT-829

MERGE INTO RESOURCES A
     USING (SELECT '/docMgmtImportFileServlet.serv'
                      AS URI,
                   1 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/docMgmtImportFileServlet.serv',
               'APPT',
               1,
               NULL);

MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'AAST' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI =
                      '/docMgmtImportFileServlet.serv') RP1
        ON (    RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES (
                 (SELECT RESOURCE_ID
                    FROM RESOURCES
                   WHERE     URI =
                                '/docMgmtImportFileServlet.serv'
                         AND MODULE = 'APPT'
                         AND HTTP_METHOD IS NULL),
                 'AAST');	

commit;


-- DBTicket APPT-792

MERGE INTO LABEL L USING
(SELECT 'ym_label' BUNDLE_NAME, 'totalApptObjs' KEY FROM DUAL
) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.LABEL_ID,
      L.KEY,
      L.VALUE,
      L.BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      'totalApptObjs',
      'Total Number of Appointment Objects : ',
      'ym_label'
    );

COMMIT;


-- DBTicket APPT-837
  
MERGE INTO LABEL L
USING (SELECT 'ym_label' BUNDLE_NAME, 'AppointmentsSumSubject' KEY
                FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT(LABEL_ID,key,value,BUNDLE_NAME) 
values(SEQ_LABEL_ID.nextval, 'AppointmentsSumSubject', 'Appointment(s) Summary','ym_label');
 
COMMIT;  

-- DBTicket APPT-844

begin
  execute immediate('ALTER TABLE APPT_OBJECT_FEASIBLE_APPT rename constraint APPT_OBJECT_PK to APPT_OBJECT_FEASIBLE_APPT_PK');
  exception
    when others then
      null;
end;
/

begin
  execute immediate('alter index APPT_OBJECT_PK rename to APPT_OBJECT_FEASIBLE_APPT_PK');
  exception
    when others then
      null;
end;
/


-- DBTicket APPT-883

UPDATE lrf_report_def_layout
SET field_name    = 'A.ACTUAL_CHECKIN_DTTM'
WHERE field_name  = 'A.CHECKIN_DTTM'
AND report_def_id =
  (SELECT report_def_id
  FROM lrf_report_def
  WHERE report_name = 'AppointmentScheduled'
  );
  
COMMIT;


-- DBTicket APPT-871

MERGE INTO LABEL L
USING (SELECT 'ym_label' BUNDLE_NAME, 'Arrival' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT(LABEL_ID,key,value,BUNDLE_NAME) 
values(SEQ_LABEL_ID.nextval, 'Arrival', 'Arrival','ym_label');

MERGE INTO LABEL L
USING (SELECT 'ym_label' BUNDLE_NAME, 'Depart' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT(LABEL_ID,key,value,BUNDLE_NAME) 
values(SEQ_LABEL_ID.nextval, 'Depart', 'Depart','ym_label');

MERGE INTO LABEL L
USING (SELECT 'ym_label' BUNDLE_NAME, 'openInMailClient' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT(LABEL_ID,key,value,BUNDLE_NAME) 
values(SEQ_LABEL_ID.nextval, 'openInMailClient', 'Open in mail client','ym_label');

MERGE INTO LABEL L
USING (SELECT 'ym_label' BUNDLE_NAME, 'urlToLongLabel' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT(LABEL_ID,key,value,BUNDLE_NAME) 
values(SEQ_LABEL_ID.nextval, 'urlToLongLabel', 'Too many appointments selected','ym_label');

COMMIT;

-- DBTicket APPT-774

DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_DOCK_DOOR_DOCK_DOOR_ST_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_DOCK_DOOR_DOCK_DOOR_ST_1_IX ON DOCK_DOOR(DOCK_DOOR_STATUS) TABLESPACE CBO_TXN_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_DOCK_DOOR_REF_APPOINTM_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_DOCK_DOOR_REF_APPOINTM_1_IX ON DOCK_DOOR_REF(APPOINTMENT_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_FEASIBILITY_ILM_DYNAMI_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_FEASIBILITY_ILM_DYNAMI_1_IX ON FEASIBILITY(ILM_DYNAMIC_GROUP) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_FEASIBILITY_ILM_OBJECT_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_FEASIBILITY_ILM_OBJECT_1_IX ON FEASIBILITY(ILM_OBJECT_TYPE) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPOINTMENTS_FACDOOR_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPOINTMENTS_FACDOOR_IX ON ILM_APPOINTMENTS(FACILITY_ID,DOOR_TYPE_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPOINTMENTS_ORIGI_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPOINTMENTS_ORIGI_1_IX ON ILM_APPOINTMENTS(ORIGIN_FACILITY) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPOINTMENTS_BUSIN_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPOINTMENTS_BUSIN_1_IX ON ILM_APPOINTMENTS(COMPANY_ID,BUSINESS_PARTNER_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPOINTMENT_EVENTS_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPOINTMENT_EVENTS_1_IX ON ILM_APPOINTMENT_EVENTS(CREATED_SOURCE_TYPE) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPOINTMENT_OBJECT_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPOINTMENT_OBJECT_1_IX ON ILM_APPOINTMENT_OBJECTS(APPT_OBJ_TYPE) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPOINTMENT_UTILIZ_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPOINTMENT_UTILIZ_1_IX ON ILM_APPOINTMENT_UTILIZATION(FACILITY_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPT_DOCKS_APPOINT_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPT_DOCKS_APPOINT_1_IX ON ILM_APPT_DOCKS(APPOINTMENT_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPT_DOCKS_FACDOCK_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPT_DOCKS_FACDOCK_1_IX ON ILM_APPT_DOCKS(FACILITY_ID,DOCK_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPT_DOCKS_PROTECT_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPT_DOCKS_PROTECT_1_IX ON ILM_APPT_DOCKS(PROTECTION_LEVEL_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPT_EQUIPMENTS_EQ_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPT_EQUIPMENTS_EQ_1_IX ON ILM_APPT_EQUIPMENTS(EQUIPMENT_TYPE) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPT_LOAD_CONFIG_T_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPT_LOAD_CONFIG_T_1_IX ON ILM_APPT_LOAD_CONFIG(TC_COMPANY_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPT_NOTES_APPOINT_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPT_NOTES_APPOINT_1_IX ON ILM_APPT_NOTES(APPOINTMENT_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPT_NOTES_TC_COMP_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPT_NOTES_TC_COMP_1_IX ON ILM_APPT_NOTES(TC_COMPANY_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPT_SIZES_PROTECT_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPT_SIZES_PROTECT_1_IX ON ILM_APPT_SIZES(PROTECTION_LEVEL_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPT_SIZES_SIZE_UO_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPT_SIZES_SIZE_UO_1_IX ON ILM_APPT_SIZES(SIZE_UOM_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPT_SIZES_TC_COMP_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPT_SIZES_TC_COMP_1_IX ON ILM_APPT_SIZES(TC_COMPANY_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPT_SLOT_UTILIZAT_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPT_SLOT_UTILIZAT_1_IX ON ILM_APPT_SLOT_UTILIZATION(FACILITY_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_DOCK_DOOR_EVENTS_C_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_DOCK_DOOR_EVENTS_C_1_IX ON ILM_DOCK_DOOR_EVENTS(CREATED_SOURCE_TYPE) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_TASKS_COMPANY_ID_1_IX';     IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_TASKS_COMPANY_ID_1_IX ON ILM_TASKS(COMPANY_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_TASKS_PARENT_TASK__1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_TASKS_PARENT_TASK__1_IX ON ILM_TASKS(PARENT_TASK_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_TASK_EVENTS_CREATE_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_TASK_EVENTS_CREATE_1_IX ON ILM_TASK_EVENTS(CREATED_SOURCE_TYPE) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_TRAILER_EVENTS_CRE_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_TRAILER_EVENTS_CRE_1_IX ON ILM_TRAILER_EVENTS(CREATED_SOURCE_TYPE) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_YARD_ACTIVITY_ACTI_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_YARD_ACTIVITY_ACTI_1_IX ON ILM_YARD_ACTIVITY(ACTIVITY_TYPE) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_YARD_ACTIVITY_APPO_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_YARD_ACTIVITY_APPO_1_IX ON ILM_YARD_ACTIVITY(APPOINTMENT_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_YARD_ACTIVITY_COMP_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_YARD_ACTIVITY_COMP_1_IX ON ILM_YARD_ACTIVITY(COMPANY_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_YARD_ACTIVITY_TASK_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_YARD_ACTIVITY_TASK_1_IX ON ILM_YARD_ACTIVITY(TASK_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_YARD_AUDIT_TASK_ID_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_YARD_AUDIT_TASK_ID_1_IX ON ILM_YARD_AUDIT(TASK_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_LOCATION_ILM_OBJECT_TY_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_LOCATION_ILM_OBJECT_TY_1_IX ON LOCATION(ILM_OBJECT_TYPE) TABLESPACE CBO_BASE_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_TOTAL_CAPACITY_ILM_OBJ_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_TOTAL_CAPACITY_ILM_OBJ_1_IX ON TOTAL_CAPACITY(ILM_OBJECT_TYPE) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_TOTAL_CAPACITY_DTL_ILM_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_TOTAL_CAPACITY_DTL_ILM_1_IX ON TOTAL_CAPACITY_DTL(ILM_DYNAMIC_GROUP) TABLESPACE YMS_BST_IDX_TBS INITRANS 10;';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/


-- DBTicket APPT-956

CREATE OR REPLACE TRIGGER  ILM_APPTMENTS_LST_UPDT_DT_TRG 
BEFORE UPDATE ON ILM_APPOINTMENTS 
 FOR EACH ROW 
 BEGIN 
 :new.LAST_UPDATED_DTTM := CURRENT_TIMESTAMP; 
  END;
 /
 
 
-- DBTicket APPT-957

MERGE INTO LABEL L
USING (SELECT 'ym_label' BUNDLE_NAME,
'DriverLicenseExpiry' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (LABEL_ID,
KEY,
VALUE,
BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'DriverLicenseExpiry',
'Driver license expiry',
'ym_label');


MERGE INTO LABEL L
USING (SELECT 'ym_label' BUNDLE_NAME,
'driverLicenseCountry' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (LABEL_ID,
KEY,
VALUE,
BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'driverLicenseCountry',
'Driver license country',
'ym_label');


MERGE INTO LABEL L
USING (SELECT 'ym_label' BUNDLE_NAME,
'appointments' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (LABEL_ID,
KEY,
VALUE,
BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'appointments',
'Appointments',
'ym_label');

COMMIT; 

-- DBTicket APPT-959
EXEC QUIET_DROP ('SEQUENCE' ,'ILM_APPOINTMENT_ID_SEQ');
EXEC MANH_CREATE_SYNONYM('ILM_APPOINTMENT_ID_SEQ');

-- DBTicket APPT-1052
declare
   v_seq   integer;
   v_col   integer;
begin
   select max (appointment_id) into v_col from ilm_appointments;

   select ilm_appointment_id_seq.nextval into v_seq from dual;

   while (v_seq <= v_col)
   loop
       select ilm_appointment_id_seq.nextval into v_seq from dual;
   end loop;
end;
/

-- DBTicket APPT-920
MERGE INTO LABEL L USING (SELECT 'ILM' BUNDLE_NAME, 'Day' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN
INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'Day', 'Day', 'ILM');

----MERGE INTO XCOMPONENT L USING (SELECT 'Tpe.component.AppointmentCalendar' name FROM DUAL) B 
----ON (L.name = B.name) 
----WHEN NOT MATCHED THEN 
----INSERT (L.XCOMPONENT_ID,L.NAME,L.XLEGACY_ID) VALUES (SEQ_XCOMPONENT_ID.NEXTVAL,B.name,240010);
----
----
----MERGE INTO XCOMPONENT_LABEL L USING (SELECT (SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=240010) XCOMPONENT_ID,'ILM' LABEL_BUNDLE_NAME,'Day' LABEL_KEY FROM DUAL) B
----ON (L.XCOMPONENT_ID = B.XCOMPONENT_ID AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME AND L.LABEL_KEY=B.LABEL_KEY)
----WHEN NOT MATCHED THEN
----INSERT (L.XCOMPONENT_LABEL_ID,L.XCOMPONENT_ID,L.LABEL_BUNDLE_NAME,L.LABEL_KEY) VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,B.XCOMPONENT_ID,B.LABEL_BUNDLE_NAME,B.LABEL_KEY);

commit;

-- DBTicket APPT-963

UPDATE FILTER_LAYOUT
   SET FIELD_OPERATORS = 'DYNAMIC',
       OPERATOR_TYPE = 'DYNAMIC',
       VALUE_GENERATOR_CLASS = 'com.manh.yms.lookup.ui.TrailerFilterControl',
       SELECTION_PAGE_URL =
          '/lps/resources/editControl/lookup/idLookup.jsflps?lookupType=Trailer&permission_code=VAPT&paginReq=false',
       LOOKUP_ATTRIBUTE = NULL
 WHERE FIELD_NAME = 'activity.equipmentInstance.equipInstRefCarrier'
       AND OBJECT_TYPE = 'TRAILERS_ACTIVITY_TRAIL';

UPDATE FILTER_LAYOUT
   SET FIELD_OPERATORS = 'DYNAMIC',
       OPERATOR_TYPE = 'DYNAMIC',
       VALUE_GENERATOR_CLASS = 'com.manh.yms.lookup.ui.TractorFilterControl',
       SELECTION_PAGE_URL =
          '/lps/resources/editControl/lookup/idLookup.jsflps?lookupType=Tractor&permission_code=VAPT&paginReq=false',
       LOOKUP_ATTRIBUTE = NULL
 WHERE FIELD_NAME = 'tractor.equipmentInstanceData.equipInstRefCarrier'
       AND OBJECT_TYPE = 'ILM_CHECKOUT';

COMMIT;

-- DBTicket APPT-964

MERGE INTO LABEL L
     USING (SELECT 'ym_label' BUNDLE_NAME, 'BusinessPartnername' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'BusinessPartnername',
               'Business Partner Name',
               'ym_label');

COMMIT;

-- DBTicket APPT-965


MERGE INTO LABEL L
     USING (SELECT 'ILM' BUNDLE_NAME, 'facilityToolTip' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'facilityToolTip',
               'Enter Facility',
               'ILM');

COMMIT;               


-- DBTicket APPT-966

MERGE INTO LABEL L
     USING (SELECT 'ym_label' BUNDLE_NAME, 'licenseExpiryDate' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'licenseExpiryDate',
               'Driver license expiry',
               'ym_label');

COMMIT;

-- DBTicket APPT-989

MERGE INTO LABEL L
USING (SELECT 'ym_label' BUNDLE_NAME, 'asnIds' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT(LABEL_ID,key,value,BUNDLE_NAME) 
values(SEQ_LABEL_ID.nextval, 'asnIds', 'ASN(s)','ym_label');

MERGE INTO LABEL L
USING (SELECT 'ym_label' BUNDLE_NAME, 'shipmentIds' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT(LABEL_ID,key,value,BUNDLE_NAME) 
values(SEQ_LABEL_ID.nextval, 'shipmentIds', 'Shipment ID (s)','ym_label');

MERGE INTO LABEL L
USING (SELECT 'ym_label' BUNDLE_NAME, 'poIds' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT(LABEL_ID,key,value,BUNDLE_NAME) 
values(SEQ_LABEL_ID.nextval, 'poIds', 'Purchase Order(s)','ym_label');

MERGE INTO LABEL L
USING (SELECT 'ym_label' BUNDLE_NAME, 'andMore' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT(LABEL_ID,key,value,BUNDLE_NAME) 
values(SEQ_LABEL_ID.nextval, 'andMore', '... And More','ym_label');

commit;

-- DBTicket APPT-997

update label_display set disp_value = 'Reject is applicable for checked-in appointments. Invalid Appointment status to Reject'
where disp_key = 'Invalid Appointment status to Reject';

commit;

-- DBTicket APPT-1001

MERGE INTO LABEL L
     USING (SELECT 'ym_label' BUNDLE_NAME, 'appEmailDisclaimer' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'appEmailDisclaimer',
                 'Appointment(s) data were valid at the time this email was sent. Appointment(s) are subject to change and might be modified later. This information is intended for use of the email recipient only.',
                 'ym_label');

COMMIT;

-- DBTicket APPT-1065

update label set value = 'Appointment Scheduled Departure Time' where bundle_name = 'ym_label' and key = 'Depart';
update label set value = 'Appointment Scheduled Start Time' where bundle_name = 'ym_label' and key = 'Arrival';

commit;

-- DBTicket APPT-1137

DELETE FROM UI_MENU_ITEM
      WHERE UI_MENU_SCREEN_ID IN (SELECT UI_MENU_SCREEN_ID
                                    FROM UI_MENU_SCREEN
                                   WHERE SCREEN_ID = '99003')
            AND display_text = 'warningIndicator';

COMMIT;


-- DBTicket APPT-1156

UPDATE custom_attribute
SET VALUE_POPULATOR_CLASS  ='com.manh.appointment.ui.core.InheritAppointmentCustomAttributeFromShipmentPO'
WHERE VALUE_POPULATOR_CLASS='com.manh.yms.appointment.core.InheritAppointmentCustomAttributeFromShipmentPO';

COMMIT;

-- DBTicket APPT-1195

update xmenu_item set navigation_key = 'Appointment.calendar.screen.Calendar', screen_version = 2 where navigation_key = '/appointment/ui/jsp/oldOrNewCalendar.jsp'; 
commit;


-- DBTicket APPT-1218

MERGE INTO LABEL L
     USING (SELECT 'ym_label' BUNDLE_NAME, 'actualCheckInDttm' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'actualCheckInDttm',
                 'Actual CheckIn Date/Time',
                 'ym_label');

MERGE INTO LABEL L
     USING (SELECT 'ILMAppointmentStatus' BUNDLE_NAME, 'Allocated for door' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'Allocated for door',
                 'Allocated for door',
                 'ym_label');
				 
MERGE INTO LABEL L
     USING (SELECT 'ILMAppointmentStatus' BUNDLE_NAME, 'Checked In' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'Checked In',
                 'Checked In',
                 'ILMAppointmentStatus');

MERGE INTO LABEL L
     USING (SELECT 'ILMAppointmentStatus' BUNDLE_NAME, 'Canceled' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'Canceled',
                 'Canceled',
                 'ILMAppointmentStatus');

MERGE INTO LABEL L
     USING (SELECT 'ILMAppointmentStatus' BUNDLE_NAME, 'Partly Complete' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'Partly Complete',
                 'Partly Complete',
                 'ILMAppointmentStatus');

MERGE INTO LABEL L
     USING (SELECT 'ILMAppointmentStatus' BUNDLE_NAME, 'In Transit' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'In Transit',
                 'In Transit',
                 'ILMAppointmentStatus');

MERGE INTO LABEL L
     USING (SELECT 'ILMAppointmentStatus' BUNDLE_NAME, 'In Progress' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'In Progress',
                 'In Progress',
                 'ILMAppointmentStatus');
			 
COMMIT;


-- DBTicket APPT-1237

UPDATE label
SET value = 'Estimated trailer duration (min)'
WHERE KEY = 'EstTrailerDurationMts';

UPDATE label
SET value = 'Estimated tractor duration (min)'
WHERE KEY = 'EstTractorDurationMts';

UPDATE label
SET value = 'Actual checkin date/time'
WHERE KEY = 'actualCheckInDttm';

UPDATE label
SET value       = 'Contact number'
WHERE KEY       = 'contactNum'
AND bundle_name = 'ym_label';

COMMIT;

-- DBTicket APPT-1240

delete from label where bundle_name = 'ym_label' and key ='Allocated for door';

MERGE INTO LABEL L
     USING (SELECT 'ILMAppointmentStatus' BUNDLE_NAME, 'Allocated for door' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'Allocated for door',
                 'Allocated for door',
                 'ILMAppointmentStatus');
				 
Commit;

-- DBTicket APPT-1271

MERGE INTO LABEL L
     USING (SELECT 'ym_label' BUNDLE_NAME, 'OpeninMailClient' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'OpeninMailClient',
               'Open in Mail Client',
               'ym_label');
			   
Commit;

-- DBTicket APPT-1225

update ilm_appointments
   set bp_id =
          (select business_partner.bp_id
             from business_partner
            where business_partner.tc_company_id = ilm_appointments.company_id
                  and business_partner.business_partner_id = ilm_appointments.business_partner_id)
where ilm_appointments.bp_id is null and ilm_appointments.business_partner_id is not null;

commit;

-- DBTicket APPT-1303
MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'Create Appointment' key FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'Create Appointment',
               'Create Appointment',
               'ILM');

-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'Create Appointment' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);

MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'RemainingCapacity' key FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'RemainingCapacity',
               'Remaining Capacity',
               'ILM');


-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'RemainingCapacity' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);

MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'Status' key FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'Status',
               'Status',
               'ILM');


-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'Status' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);

MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'Invalid' key FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'Invalid',
               'Invalid',
               'ILM');


-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'Invalid' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);


MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'Unscheduled' key FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'Unscheduled',
               'Unscheduled',
               'ILM');


-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'Unscheduled' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);

MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'Scheduled' key FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'Scheduled',
               'Scheduled',
               'ILM');


-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'Scheduled' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);

MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'Confirmed' key FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'Confirmed',
               'Confirmed',
               'ILM');


-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'Confirmed' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);

MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'Checked In' key FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'Checked In',
               'Checked In',
               'ILM');


-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'Checked In' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);

MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'Allocated for door' key FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'Allocated for door',
               'Allocated for door',
               'ILM');


-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'Allocated for door' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);

MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'Unloading' key FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'Unloading',
               'Unloading',
               'ILM');


-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'Unloading' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);

MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'Loading' key FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'Loading',
               'Loading',
               'ILM');


-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'Loading' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);

MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'Complete' key FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'Complete',
               'Complete',
               'ILM');


-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'Complete' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);

MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'Canceled' key FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'Canceled',
               'Canceled',
               'ILM');


-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'Canceled' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);

MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'Loaded' key FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'Loaded',
               'Loaded',
               'ILM');

-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'Loaded' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);

MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'Unloaded' key FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'Unloaded',
               'Unloaded',
               'ILM');


-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'Unloaded' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);

MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'Overdue' key FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'Overdue',
               'Overdue',
               'ILM');


-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'Overdue' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);

MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'Partly Complete' key FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'Partly Complete',
               'Partly Complete',
               'ILM');


-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'Partly Complete' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);

MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'Countered' key FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'Countered',
               'Countered',
               'ILM');


-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'Countered' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);


MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'Requested' key FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'Requested',
               'Requested',
               'ILM');


-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'Requested' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);


MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'In Progress' key FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'In Progress',
               'In Progress',
               'ILM');


-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'In Progress' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);


MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'In Transit' key FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'In Transit',
               'In Transit',
               'ILM');


-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'In Transit' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);

MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'Check-In' key FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'Check-In',
               'Check-In',
               'ILM');

-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'Check-In' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);

MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'UI Parameters' key FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'UI Parameters',
               'UI Parameters',
               'ILM');

-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'UI Parameters' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);


MERGE INTO label l
     USING (SELECT 'ILM' bundle_name, 'Display Checked-In count' key
              FROM DUAL) b
        ON (l.key = b.key AND l.bundle_name = b.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (label_id,
               key,
               VALUE,
               bundle_name)
       VALUES (seq_label_id.NEXTVAL,
               'Display Checked-In count',
               'Display Checked-In count',
               'ILM');

-- MERGE INTO XCOMPONENT_LABEL L
--      USING (SELECT (SELECT XCOMPONENT_ID
--                       FROM XCOMPONENT
--                      WHERE XLEGACY_ID = 240010)
--                       XCOMPONENT_ID,
--                    'ILM' LABEL_BUNDLE_NAME,
--                    'Display Checked-In count' LABEL_KEY
--               FROM DUAL) B
--         ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--             AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--             AND L.LABEL_KEY = B.LABEL_KEY)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (L.XCOMPONENT_LABEL_ID,
--                L.XCOMPONENT_ID,
--                L.LABEL_BUNDLE_NAME,
--                L.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                B.XCOMPONENT_ID,
--                B.LABEL_BUNDLE_NAME,
--                B.LABEL_KEY);
               
COMMIT;

-- DBTicket APPT-1323

MERGE INTO LABEL L
     USING (SELECT 'ILM' BUNDLE_NAME, 'FacilityMustBeProvidedToFindTheDock' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (seq_label_id.NEXTVAL,
               'FacilityMustBeProvidedToFindTheDock',
               'Facility must be provided to find the dock',
               'ILM');
			   
MERGE INTO LABEL L
     USING (SELECT 'ILM' BUNDLE_NAME, 'DockAssIsReqFindDD' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (seq_label_id.NEXTVAL,
               'DockAssIsReqFindDD',
               'Dock assigned is required to find the dock door',
               'ILM');

COMMIT;	

-- DBTicket APPT-1335

update param_def set min_value = 1, data_type = 1 where param_def_id = 'time_uom' and param_group_id = 'ILM' and param_subgroup_id = 'YARD';
commit;

-- DBTicket DB-132

update filter_layout set HIBERNATE_FIELD_NAME = 'CustomPOSearch' , FIELD_NAME = 'CustomPOSearch'
where object_type like 'UI_APPOINTMENT' and field_label='Purchase Order';

Commit;

-- DBTicket DB-101

MERGE INTO LABEL L
USING (SELECT 'ILM' BUNDLE_NAME, 'appointmentDetails' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'appointmentDetails',
'Appointment Details',
'ILM');

commit;

-- DBTicket DB-385

ALTER TABLE ILM_APPT_LOAD_CONFIG ADD LOAD_CONFIG_ID_TMP NUMBER(8);
UPDATE ILM_APPT_LOAD_CONFIG SET LOAD_CONFIG_ID_TMP=LOAD_CONFIG_ID;
alter table ILM_APPT_LOAD_CONFIG modify LOAD_CONFIG_ID NULL;
update ILM_APPT_LOAD_CONFIG set LOAD_CONFIG_ID=NULL;
alter table ILM_APPT_LOAD_CONFIG modify LOAD_CONFIG_ID number (8);
update ILM_APPT_LOAD_CONFIG set LOAD_CONFIG_ID=LOAD_CONFIG_ID_TMP;
alter table ILM_APPT_LOAD_CONFIG modify LOAD_CONFIG_ID not null;
alter table ILM_APPT_LOAD_CONFIG drop column LOAD_CONFIG_ID_TMP;

ALTER TABLE ILM_APPT_SIZES MODIFY ORDER_ID NUMBER(10);

ALTER TABLE ILM_APPOINTMENTS ADD APPT_STATUS_TMP NUMBER(4);
UPDATE ILM_APPOINTMENTS SET APPT_STATUS_TMP=APPT_STATUS;

DECLARE
   allready_null EXCEPTION;
   PRAGMA EXCEPTION_INIT(allready_null, -1451);
BEGIN
   execute immediate 'alter table ILM_APPOINTMENTS modify APPT_STATUS NULL';
EXCEPTION
   WHEN allready_null THEN
      null; -- handle the error
END;
/

--alter table ILM_APPOINTMENTS modify APPT_STATUS NULL;
update ILM_APPOINTMENTS set APPT_STATUS=NULL;
alter table ILM_APPOINTMENTS modify APPT_STATUS number (4);
update ILM_APPOINTMENTS set APPT_STATUS=APPT_STATUS_TMP;
alter table ILM_APPOINTMENTS modify APPT_STATUS not null;
alter table ILM_APPOINTMENTS drop column APPT_STATUS_TMP;

ALTER TABLE ILM_APPOINTMENT_OBJECTS ADD APPT_OBJ_TYPE_TMP NUMBER(3);
UPDATE ILM_APPOINTMENT_OBJECTS SET APPT_OBJ_TYPE_TMP=APPT_OBJ_TYPE;
alter table ILM_APPOINTMENT_OBJECTS modify APPT_OBJ_TYPE NULL;
update ILM_APPOINTMENT_OBJECTS set APPT_OBJ_TYPE=NULL;
alter table ILM_APPOINTMENT_OBJECTS modify APPT_OBJ_TYPE number (3);
update ILM_APPOINTMENT_OBJECTS set APPT_OBJ_TYPE=APPT_OBJ_TYPE_TMP;
alter table ILM_APPOINTMENT_OBJECTS modify APPT_OBJ_TYPE not null;
alter table ILM_APPOINTMENT_OBJECTS drop column APPT_OBJ_TYPE_TMP;

ALTER TABLE ILM_APPOINTMENT_EVENTS ADD APPOINTMENT_ID_TMP NUMBER(9);
UPDATE ILM_APPOINTMENT_EVENTS SET APPOINTMENT_ID_TMP=APPOINTMENT_ID;
ALTER TABLE ILM_APPOINTMENT_EVENTS DROP PRIMARY KEY DROP INDEX;
alter table ILM_APPOINTMENT_EVENTS modify APPOINTMENT_ID null;
update ILM_APPOINTMENT_EVENTS set APPOINTMENT_ID=null;
alter table ILM_APPOINTMENT_EVENTS modify APPOINTMENT_ID number(9);
update ILM_APPOINTMENT_EVENTS set APPOINTMENT_ID=APPOINTMENT_ID_TMP;
ALTER TABLE ILM_APPOINTMENT_EVENTS ADD CONSTRAINT PK_ILM_APPOINTMENT_EVENTS  PRIMARY KEY  (APPOINTMENT_ID, EVENT_SEQ) USING INDEX TABLESPACE CM_BST4K_IDX_TBS;
alter table ILM_APPOINTMENT_EVENTS drop column APPOINTMENT_ID_TMP;

-- DBTicket DB-532 PLSQL
@DBScripts/Product/PLSQL_Objects/ILM_APPT_CUSTO_LST_UPDT_DT_TRG.sql
@DBScripts/Product/PLSQL_Objects/ILM_APPT_CUST_TRG_B_I_U.sql

-- DBTicket DB-1125

UPDATE FILTER_LAYOUT
SET VALUE_GENERATOR_CLASS = 'com.manh.appointment.lookup.filtercontrol.DockDoorFilterControl'
WHERE VALUE_GENERATOR_CLASS = 'com.manh.yms.lookup.ui.ILMDockDoorFilterControl'
AND OBJECT_TYPE = 'UI_APPOINTMENT';

UPDATE FILTER_LAYOUT
SET VALUE_GENERATOR_CLASS = 'com.manh.appointment.lookup.filtercontrol.DockFilterControl'
WHERE VALUE_GENERATOR_CLASS = 'com.manh.yms.lookup.ui.ILMDockFilterControl'
AND OBJECT_TYPE = 'UI_APPOINTMENT';

UPDATE FILTER_LAYOUT
SET VALUE_GENERATOR_CLASS = 'com.manh.appointment.lookup.filtercontrol.TrailerFilterControl'
WHERE VALUE_GENERATOR_CLASS = 'com.manh.yms.lookup.ui.TrailerFilterControl'
AND OBJECT_TYPE = 'UI_APPOINTMENT';

UPDATE FILTER_LAYOUT
SET VALUE_GENERATOR_CLASS = 'com.manh.appointment.lookup.filtercontrol.PurchaseOrderFilterControl'
WHERE VALUE_GENERATOR_CLASS = 'com.manh.yms.lookup.ui.ILMApptPOLookUpFilterControl'
AND OBJECT_TYPE = 'UI_APPOINTMENT';

COMMIT;

-- DBTicket DB-1260

MERGE INTO RESOURCES A
     USING (SELECT '/services/rest/appointment/AppointmentRestServices/appointmentRestServices/getAppointmentTimesForPO' AS URI,
                   2 AS URI_TYPE_ID
              FROM DUAL) B
        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID,
               HTTP_METHOD)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/services/rest/appointment/AppointmentRestServices/appointmentRestServices/getAppointmentTimesForPO',
				'APPT', 2,'GET');


MERGE INTO RESOURCE_PERMISSION RP
     USING (SELECT RESOURCES.RESOURCE_ID, 'VAPT' AS PERMISSION_CODE
              FROM RESOURCES
             WHERE RESOURCES.URI='/services/rest/appointment/AppointmentRestServices/appointmentRestServices/getAppointmentTimesForPO' and RESOURCES.MODULE='APPT') RP1
        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT resource_id
                  FROM resources
                 WHERE URI='/services/rest/appointment/AppointmentRestServices/appointmentRestServices/getAppointmentTimesForPO' and MODULE='APPT'),
               'VAPT');

Commit;

-------- DBTicket DB-1320
---------Commented due to the defect task DB-1517
--------EXEC MANH_CREATE_SYNONYM('FEASIBILITY_RULE_HDR');
--------EXEC MANH_CREATE_SYNONYM('SEQ_FEASIBILITY_RULE_HDRID');

COMMIT;

-- DBTicket DB-1394

UPDATE RESOURCES SET URI = '/services/rest/appointment/AppointmentCaledarService/appointmentCalendarService/updateAppointmentInfo' where uri = '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/updateAppointmentInfo';
UPDATE RESOURCES SET URI = '/services/rest/appointment/AppointmentCaledarService/appointmentCalendarService/cleanup' where uri = '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/cleanup';
UPDATE RESOURCES SET URI = '/services/rest/appointment/AppointmentCaledarService/appointmentCalendarService/getRecommendations' where uri = '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/getRecommendations';
UPDATE RESOURCES SET URI = '/services/rest/appointment/AppointmentCaledarService/appointmentCalendarService/saveRecommendation' where uri = '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/saveRecommendation';
UPDATE RESOURCES SET URI = '/services/rest/appointment/AppointmentCaledarService/appointmentCalendarService/facilityChanged' where uri = '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/facilityChanged';
UPDATE RESOURCES SET URI = '/services/rest/appointment/AppointmentCaledarService/appointmentCalendarService/getCapacityUtilization' where uri = '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/getCapacityUtilization';
UPDATE RESOURCES SET URI = '/services/rest/appointment/AppointmentCaledarService/appointmentCalendarService/getAppointmentsOfSlot' where uri = '/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/getAppointmentsOfSlot';

COMMIT;

-- DBTicket DB-1517
EXEC MANH_CREATE_SYNONYM('FEASIBILITY_RULE_HDR');
---- EXEC MANH_CREATE_SYNONYM('SEQ_FEASIBILITY_RULE_HDRID');

-- DBTicket DB-1671

update xmenu_item set navigation_key = '/ilm/ui/rf/common/RFMenu.jsflps' where NAVIGATION_KEY = '/ilm/rfclient/ui/RFMenu.jsflps';

COMMIT;

-- DBTicket DB-1636
MERGE INTO LABEL L
     USING (SELECT 'ILM' BUNDLE_NAME, 'appointmentCalendarSearchText' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'appointmentCalendarSearchText',
               'Search Appointments, POs, Shipments, ASNs',
               'ILM');
COMMIT;			   

-- DBTicket DB-1864

UPDATE RESOURCES SET URI = '/services/rest/appointment/AppointmentCalendarService/appointmentCalendarService/updateAppointmentInfo' where uri = '/services/rest/appointment/AppointmentCaledarService/appointmentCalendarService/updateAppointmentInfo';
UPDATE RESOURCES SET URI = '/services/rest/appointment/AppointmentCalendarService/appointmentCalendarService/cleanup' where uri = '/services/rest/appointment/AppointmentCaledarService/appointmentCalendarService/cleanup';
UPDATE RESOURCES SET URI = '/services/rest/appointment/AppointmentCalendarService/appointmentCalendarService/getRecommendations' where uri = '/services/rest/appointment/AppointmentCaledarService/appointmentCalendarService/getRecommendations';
UPDATE RESOURCES SET URI = '/services/rest/appointment/AppointmentCalendarService/appointmentCalendarService/saveRecommendation' where uri = '/services/rest/appointment/AppointmentCaledarService/appointmentCalendarService/saveRecommendation';
UPDATE RESOURCES SET URI = '/services/rest/appointment/AppointmentCalendarService/appointmentCalendarService/facilityChanged' where uri = '/services/rest/appointment/AppointmentCaledarService/appointmentCalendarService/facilityChanged';
UPDATE RESOURCES SET URI = '/services/rest/appointment/AppointmentCalendarService/appointmentCalendarService/getCapacityUtilization' where uri = '/services/rest/appointment/AppointmentCaledarService/appointmentCalendarService/getCapacityUtilization';
UPDATE RESOURCES SET URI = '/services/rest/appointment/AppointmentCalendarService/appointmentCalendarService/getAppointmentsOfSlot' where uri = '/services/rest/appointment/AppointmentCaledarService/appointmentCalendarService/getAppointmentsOfSlot';

COMMIT;

-- DBTicket DB-1941

UPDATE FILTER_LAYOUT SET LOOKUP_ATTRIBUTE = '#{ymslookupBackingBean.getBUMap},#{ymslookupBackingBean.getOptionConstructMap}'
WHERE OBJECT_TYPE = 'UI_APPOINTMENT' AND FIELD_NAME = 'shpmtObj.shipment.tcShipmentIdString';

UPDATE FILTER_LAYOUT SET LOOKUP_ATTRIBUTE = '#{ymslookupBackingBean.getBUMap},#{ymslookupBackingBean.getOptionConstructMap}'
WHERE OBJECT_TYPE = 'UI_APPOINTMENT' AND FIELD_NAME = 'asnObj.asn.TCASNIdString';

commit;

-- DBTicket DB-2223

exec quiet_drop('SEQUENCE', 'SEQ_ILM_APPT_ATTRIBUTE_SEQ');
exec quiet_drop('SEQUENCE', 'ILM_ID_SEQ');

COMMIT;

-- DBTicket DB-2310

DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_DOCK_DOOR_REF_APPOINTM_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_DOCK_DOOR_REF_APPOINTM_1_IX ON DOCK_DOOR_REF(APPOINTMENT_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPOINTMENTS_FACDOOR_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPOINTMENTS_FACDOOR_IX ON ILM_APPOINTMENTS(FACILITY_ID,DOOR_TYPE_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPOINTMENTS_ORIGI_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPOINTMENTS_ORIGI_1_IX ON ILM_APPOINTMENTS(ORIGIN_FACILITY) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPOINTMENTS_BUSIN_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPOINTMENTS_BUSIN_1_IX ON ILM_APPOINTMENTS(COMPANY_ID,BUSINESS_PARTNER_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPOINTMENT_EVENTS_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPOINTMENT_EVENTS_1_IX ON ILM_APPOINTMENT_EVENTS(CREATED_SOURCE_TYPE) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPOINTMENT_OBJECT_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPOINTMENT_OBJECT_1_IX ON ILM_APPOINTMENT_OBJECTS(APPT_OBJ_TYPE) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPOINTMENT_UTILIZ_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPOINTMENT_UTILIZ_1_IX ON ILM_APPOINTMENT_UTILIZATION(FACILITY_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPT_DOCKS_APPOINT_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPT_DOCKS_APPOINT_1_IX ON ILM_APPT_DOCKS(APPOINTMENT_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPT_DOCKS_FACDOCK_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPT_DOCKS_FACDOCK_1_IX ON ILM_APPT_DOCKS(FACILITY_ID,DOCK_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPT_DOCKS_PROTECT_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPT_DOCKS_PROTECT_1_IX ON ILM_APPT_DOCKS(PROTECTION_LEVEL_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPT_EQUIPMENTS_EQ_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPT_EQUIPMENTS_EQ_1_IX ON ILM_APPT_EQUIPMENTS(EQUIPMENT_TYPE) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPT_LOAD_CONFIG_T_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPT_LOAD_CONFIG_T_1_IX ON ILM_APPT_LOAD_CONFIG(TC_COMPANY_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPT_NOTES_APPOINT_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPT_NOTES_APPOINT_1_IX ON ILM_APPT_NOTES(APPOINTMENT_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPT_NOTES_TC_COMP_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPT_NOTES_TC_COMP_1_IX ON ILM_APPT_NOTES(TC_COMPANY_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPT_SIZES_PROTECT_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPT_SIZES_PROTECT_1_IX ON ILM_APPT_SIZES(PROTECTION_LEVEL_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPT_SIZES_SIZE_UO_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPT_SIZES_SIZE_UO_1_IX ON ILM_APPT_SIZES(SIZE_UOM_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPT_SIZES_TC_COMP_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPT_SIZES_TC_COMP_1_IX ON ILM_APPT_SIZES(TC_COMPANY_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_APPT_SLOT_UTILIZAT_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_APPT_SLOT_UTILIZAT_1_IX ON ILM_APPT_SLOT_UTILIZATION(FACILITY_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_DOCK_DOOR_EVENTS_C_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_DOCK_DOOR_EVENTS_C_1_IX ON ILM_DOCK_DOOR_EVENTS(CREATED_SOURCE_TYPE) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_TASKS_COMPANY_ID_1_IX';     IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_TASKS_COMPANY_ID_1_IX ON ILM_TASKS(COMPANY_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_TASKS_PARENT_TASK_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_TASKS_PARENT_TASK_1_IX ON ILM_TASKS(PARENT_TASK_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_TASK_EVENTS_CREATE_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_TASK_EVENTS_CREATE_1_IX ON ILM_TASK_EVENTS(CREATED_SOURCE_TYPE) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_TRAILER_EVENTS_CRE_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_TRAILER_EVENTS_CRE_1_IX ON ILM_TRAILER_EVENTS(CREATED_SOURCE_TYPE) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_YARD_ACTIVITY_ACTI_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_YARD_ACTIVITY_ACTI_1_IX ON ILM_YARD_ACTIVITY(ACTIVITY_TYPE) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_YARD_ACTIVITY_APPO_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_YARD_ACTIVITY_APPO_1_IX ON ILM_YARD_ACTIVITY(APPOINTMENT_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_YARD_ACTIVITY_COMP_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_YARD_ACTIVITY_COMP_1_IX ON ILM_YARD_ACTIVITY(COMPANY_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_YARD_ACTIVITY_TASK_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_YARD_ACTIVITY_TASK_1_IX ON ILM_YARD_ACTIVITY(TASK_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/
DECLARE v_index_count NUMBER(1); BEGIN SELECT count(*)   INTO v_index_count   FROM user_indexes   WHERE index_name = 'TM_ILM_YARD_AUDIT_TASK_ID_1_IX';   IF v_index_count =0   THEN EXECUTE IMMEDIATE 'CREATE INDEX TM_ILM_YARD_AUDIT_TASK_ID_1_IX ON ILM_YARD_AUDIT(TASK_ID) TABLESPACE YMS_BST_IDX_TBS INITRANS 10';  END IF;EXCEPTION   WHEN OTHERS   THEN NULL;END;
/


-- DBTicket DB-2272

MERGE INTO BUNDLE_META_DATA A
USING (SELECT 'solr' AS BUNDLE_NAME FROM DUAL) B
ON (A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (BUNDLE_META_DATA_ID,
BUNDLE_NAME,
DESCRIPTION,
DEF_SCREEN_TYPE_ID,
IS_ACTIVE,
IS_LABEL_BUNDLE)
VALUES (seq_bundle_meta_data_id.NEXTVAL,
'solr',
'Global search labels',
'10',
1,
1);

MERGE INTO LABEL L USING (SELECT 'solr' BUNDLE_NAME,'asns' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'asns', 'ASNs', 'solr');
MERGE INTO LABEL L USING (SELECT 'solr' BUNDLE_NAME,'appointmentType' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'appointmentType', 'Type', 'solr');
MERGE INTO LABEL L USING (SELECT 'solr' BUNDLE_NAME,'appointment' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'appointment', 'Appointment', 'solr');
MERGE INTO LABEL L USING (SELECT 'solr' BUNDLE_NAME,'appointmentId' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'appointmentId', 'Appointment Id', 'solr');
MERGE INTO LABEL L USING (SELECT 'solr' BUNDLE_NAME,'carrierCode' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'carrierCode', 'Carrier', 'solr');
MERGE INTO LABEL L USING (SELECT 'solr' BUNDLE_NAME,'purchaseOrders' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'purchaseOrders', 'Purchase Orders', 'solr');
MERGE INTO LABEL L USING (SELECT 'solr' BUNDLE_NAME,'facilityAliasId' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'facilityAliasId', 'Facility', 'solr');
MERGE INTO LABEL L USING (SELECT 'solr' BUNDLE_NAME,'comments' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'comments', 'Comments', 'solr');
MERGE INTO LABEL L USING (SELECT 'solr' BUNDLE_NAME,'trailerName' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'trailerName', 'Trailer', 'solr');
MERGE INTO LABEL L USING (SELECT 'solr' BUNDLE_NAME,'scheduledDttm' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'scheduledDttm', 'Suggested Start Date/Time', 'solr');
MERGE INTO LABEL L USING (SELECT 'solr' BUNDLE_NAME,'billOfLadingNumber' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'billOfLadingNumber', 'BOL Number', 'solr');
MERGE INTO LABEL L USING (SELECT 'solr' BUNDLE_NAME,'shipments' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'shipments', 'Shipments', 'solr');
MERGE INTO LABEL L USING (SELECT 'solr' BUNDLE_NAME,'doorName' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'doorName', 'Door Assigned', 'solr');
MERGE INTO LABEL L USING (SELECT 'solr' BUNDLE_NAME,'estimatedDepartureDttm' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'estimatedDepartureDttm', 'Estimated Departure Date/Time', 'solr');
MERGE INTO LABEL L USING (SELECT 'solr' BUNDLE_NAME,'dockName' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'dockName', 'Dock Assigned', 'solr');
MERGE INTO LABEL L USING (SELECT 'solr' BUNDLE_NAME,'proNumber' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'proNumber', 'PRO Number', 'solr');
MERGE INTO LABEL L USING (SELECT 'solr' BUNDLE_NAME,'loadPosition' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'loadPosition', 'Load Position', 'solr');
MERGE INTO LABEL L USING (SELECT 'solr' BUNDLE_NAME,'appointmentStatus' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'appointmentStatus', 'Status', 'solr');
MERGE INTO LABEL L USING (SELECT 'solr' BUNDLE_NAME,'actualCheckInDttm' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'actualCheckInDttm', 'Actual Checkin Date/Time', 'solr');
MERGE INTO LABEL L USING (SELECT 'solr' BUNDLE_NAME,'shipperCompany' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'shipperCompany', 'Shipper Company', 'solr');
MERGE INTO LABEL L USING (SELECT 'solr' BUNDLE_NAME,'shipmentType' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'shipmentType', 'Shipment Type', 'solr');
MERGE INTO LABEL L USING (SELECT 'solr' BUNDLE_NAME,'controlNumber' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'controlNumber', 'Control Number', 'solr');

COMMIT;

-- DBTicket DB-2582

DELETE FROM company_parameter where param_def_id like 'Use_Lightweight_Shipment_For_Appt_Scheduling';

DELETE FROM PARAM_DEF WHERE param_def_id like 'Use_Lightweight_Shipment_For_Appt_Scheduling';

COMMIT;

-- DBTicket DB-2774
EXEC MANH_CREATE_SYNONYM('FEASIBILITY_RULE_HDR_SEQ');

COMMIT;

-- DBTicket DB-2871

EXEC QUIET_DROP('TRIGGER','ILM_APPT_CUST_TRG_B_I_U');

CREATE OR REPLACE
TRIGGER ILM_APPT_CUST_TRG
AFTER UPDATE
ON ILM_APPT_CUSTOM_ATTRIBUTE REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
vLAST_UPDATED_SOURCE_TYPE NUMBER;
vLAST_UPDATED_SOURCE	VARCHAR2(32);
BEGIN
SELECT LAST_UPDATED_SOURCE_TYPE, LAST_UPDATED_SOURCE INTO vLAST_UPDATED_SOURCE_TYPE, vLAST_UPDATED_SOURCE
FROM ILM_APPOINTMENTS 
WHERE APPOINTMENT_ID = :OLD.APPOINTMENT_ID ; 

IF ( :NEW.ATTRIBUTE_VALUE IS NULL AND :OLD.ATTRIBUTE_VALUE IS NOT NULL OR
:NEW.ATTRIBUTE_VALUE IS NOT NULL AND :OLD.ATTRIBUTE_VALUE IS NULL OR
:NEW.ATTRIBUTE_VALUE <> :OLD.ATTRIBUTE_VALUE)
THEN
INS_APPOINTMENT_EVENT (	:OLD.APPOINTMENT_ID,
:OLD.ATTRIBUTE_NAME,
:OLD.ATTRIBUTE_VALUE,
:NEW.ATTRIBUTE_VALUE,
vLAST_UPDATED_SOURCE_TYPE,
vLAST_UPDATED_SOURCE
);
END IF;
END;
/

-- DBTicket DB-2952

MERGE INTO label l 
USING (select 'chooseFacilityMessage' KEY, 'ILM' bundle_name from dual) l_p 
ON (l.bundle_name = l_p.bundle_name AND l.key = l_p.key) 
WHEN NOT MATCHED THEN 
INSERT (label_id, key, value, bundle_name) 
VALUES (SEQ_LABEL_ID.nextval, 'chooseFacilityMessage', 'Please choose a facility.', 'ILM');

COMMIT;

-- DBTicket DB-3187

INSERT INTO JMS_DESTINATION_INFO(NAME,OWNER,DESTINATION_TYPE,PROVIDER_NAME ,DLQ_PREFIX,MAX_RETRIES)
 VALUES( 'jms.queue.AutoAppointmentQueue' , 'APPT' , 'QUEUE', 'default_amq' , NULL, NULL );

insert into param_def 
(PARAM_DEF_ID ,PARAM_GROUP_ID ,PARAM_SUBGROUP_ID ,PARAM_NAME ,DESCRIPTION ,IS_DISABLED ,IS_REQUIRED ,DATA_TYPE ,UI_TYPE ,MIN_VALUE ,
IS_MIN_INC ,MAX_VALUE ,IS_MAX_INC ,DFLT_VALUE ,DISPLAY_ORDER ,VALUE_GENERATOR_CLASS ,IS_LOCALIZABLE ,PARAM_CATEGORY ,CARR_DFLT_VALUE ,BP_DFLT_VALUE ,PARAM_VALIDATOR_CLASS ,CREATED_DTTM ,LAST_UPDATED_DTTM)
values (
'SHIPMENT_STATUS_AUTO_APPT',
'ILM','YARD',
'Shipment status to automatically schedule appointments','Shipment status to automatically schedule appointments',
0,0,11,
'DROPDOWN',
19,0,71,0,
50,16,
'com.manh.cbo.transactional.domain.shipment.subobjects.ShipmentStatus',
'1',null,null,null,null,sysdate, sysdate);

Commit;

-- DBTicket DB-3240

MERGE INTO PARAM_DEF P
     USING (SELECT 'Populate_BP_On_Appt_By_Sizeuom' PARAM_DEF_ID,
                   'ILM' PARAM_GROUP_ID
              FROM DUAL) D
        ON (P.PARAM_DEF_ID = D.PARAM_DEF_ID
            AND P.PARAM_GROUP_ID = D.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS)
       VALUES ('Populate_BP_On_Appt_By_Sizeuom',
               'ILM',
               'YARD',
              'Size UOM for Business Partner evaluation on appointment',
              'Size UOM for Business Partner evaluation on appointment',
               0,
               0,
               11,
               'DROPDOWN',
               NULL,
               0,
               NULL,
               0,
               0,
               NULL,
               'com.manh.appointment.ui.finitevalue.DefaultSizeUOMParamList',
               0,
               NULL,
               NULL,
               NULL,
               NULL);
COMMIT;

-- DBTicket DB-3943

update ilm_appointments set appt_status = '9' where appt_status in ( 6,7,8,11,12,14,19 ) and appt_type in (20,30); 

update ilm_appointments set appt_status = '5' where appt_status in ( 6,7,8,11,12,14,19 ) and appt_type not in (20,30); 

update ilm_appointments set appt_status = '10' where appt_status in ( 1,13 ); 

delete from ILM_APPOINTMENT_STATUS where appt_status_code in (1,7,8,11,12,13,19);

Commit;

-- DBTicket DB-3984 PLSQL

@DBScripts/Product/PLSQL_Objects/ILM_APPT_CUST_TRG_B_I_U.sql

COMMIT;


-- DBTicket DB-3901

declare
V_COUNT1 NUMBER;
begin
for CUR_1 in 
(select FEASIBILITY_ID,TC_COMPANY_ID,ILM_DYNAMIC_GROUP,IS_ALL_MEMBER_FEASIBLE,ILM_OBJECT_TYPE,
FEASIBILITY_OBJID_PK1,FEASIBILITY_OBJID_PK2,FEASIBILITY_OBJID_PK3,FEASIBILITY_OBJID_PK4,
PRIORITY_VALUE ,CUSTOM_GROUP_NAME,START_DTTM,END_DTTM,FEASIBILITY_NAME,MARK_FOR_DELETION,
CREATED_DTTM,LAST_UPDATED_DTTM from FEASIBILITY where ILM_DYNAMIC_GROUP=4)
LOOP
BEGIN
update FEASIBILITY_DTL FD set FD.VALUE1='LU'  where FD.VALUE1='10' and FD.FEASIBILITY_ID=CUR_1.FEASIBILITY_ID;
update FEASIBILITY_DTL FD set FD.VALUE1='DU'  where FD.VALUE1='20' and FD.FEASIBILITY_ID=CUR_1.FEASIBILITY_ID;
update FEASIBILITY_DTL FD set FD.VALUE1='DE'  where FD.VALUE1='30' and FD.FEASIBILITY_ID=CUR_1.FEASIBILITY_ID;
update FEASIBILITY_DTL FD set FD.VALUE1='LL'  where FD.VALUE1='40' and FD.FEASIBILITY_ID=CUR_1.FEASIBILITY_ID;
update FEASIBILITY_DTL FD set FD.VALUE1='PL'  where FD.VALUE1='50' and FD.FEASIBILITY_ID=CUR_1.FEASIBILITY_ID;
update FEASIBILITY_DTL FD set FD.VALUE1='PE'  where FD.VALUE1='60' and FD.FEASIBILITY_ID=CUR_1.FEASIBILITY_ID;

select COUNT(*) into V_COUNT1
from FEASIBILITY_DTL FD where FD.VALUE1='70' and FD.FEASIBILITY_ID=CUR_1.FEASIBILITY_ID;

delete from FEASIBILITY_DTL FD where FD.VALUE1='70' and FD.FEASIBILITY_ID=CUR_1.FEASIBILITY_ID;

IF V_COUNT1 = 0 THEN
delete from FEASIBILITY where FEASIBILITY_ID= (Select FD.FEASIBILITY_ID from FEASIBILITY_DTL FD WHERE FD.VALUE1='70' and FD.FEASIBILITY_ID=CUR_1.FEASIBILITY_ID);
end if;

EXCEPTION
when OTHERS then
null;
END;
end LOOP;

for CUR_2 in
(select FEASIBILITY_ID,TC_COMPANY_ID,ILM_DYNAMIC_GROUP,IS_ALL_MEMBER_FEASIBLE,ILM_OBJECT_TYPE,
FEASIBILITY_OBJID_PK1,FEASIBILITY_OBJID_PK2,FEASIBILITY_OBJID_PK3,FEASIBILITY_OBJID_PK4,
PRIORITY_VALUE ,CUSTOM_GROUP_NAME,START_DTTM,END_DTTM,FEASIBILITY_NAME,MARK_FOR_DELETION,
CREATED_DTTM,LAST_UPDATED_DTTM from FEASIBILITY where ILM_DYNAMIC_GROUP=52)
LOOP
BEGIN
update FEASIBILITY_DTL FD set FD.VALUE1 = ( select CARRIER_CODE from CARRIER_CODE where CARRIER_ID = FD.VALUE1 ) where  FD.FEASIBILITY_ID=CUR_2.FEASIBILITY_ID;
EXCEPTION
when OTHERS then
null;
END;
end LOOP;


for CUR_3 in
(select FEASIBILITY_ID,TC_COMPANY_ID,ILM_DYNAMIC_GROUP,IS_ALL_MEMBER_FEASIBLE,ILM_OBJECT_TYPE,
FEASIBILITY_OBJID_PK1,FEASIBILITY_OBJID_PK2,FEASIBILITY_OBJID_PK3,FEASIBILITY_OBJID_PK4,
PRIORITY_VALUE ,CUSTOM_GROUP_NAME,START_DTTM,END_DTTM,FEASIBILITY_NAME,MARK_FOR_DELETION,
CREATED_DTTM,LAST_UPDATED_DTTM from FEASIBILITY where ILM_DYNAMIC_GROUP=8)
LOOP
begin
update FEASIBILITY_DTL FD set FD.VALUE1 = ( select EQUIPMENT_CODE from EQUIPMENT where EQUIPMENT_ID = FD.VALUE1 ) where  FD.FEASIBILITY_ID=CUR_3.FEASIBILITY_ID;
EXCEPTION
when OTHERS then
null;
END;
end LOOP;

END;
/

COMMIT;

-- DBTicket DB-4400

update filter_layout set field_name = 'ILMApptData.apptDock.dockDoorData.id.dockId' , 
hibernate_field_name = 'ILMApptData.apptDock.dockDoorData.id.dockId' where object_type = 'UI_APPOINTMENT' and FIELD_LABEL = 'Dock assigned';

COMMIT;

-- DBTicket DB-4505

update label set key = 'Shift Name' where key = 'Shift\ Name' and BUNDLE_NAME = 'ILM';

COMMIT;

-- DBTicket DB-4411 PLSQL

@DBScripts/Product/PLSQL_Objects/ILM_APPT_TRG.sql

-- DBTicket DB-4586

MERGE INTO RESOURCES R
USING (SELECT '/services/rest/appointment/AppointmentCalendarService/appointmentCalendarService/appointmentSearch' URI,
         2 URI_TYPE_ID,'GET' HTTP_METHOD FROM dual) B ON (R.URI = B.URI AND R.URI_TYPE_ID = B.URI_TYPE_ID AND R.HTTP_METHOD = B.HTTP_METHOD)
WHEN NOT MATCHED THEN INSERT VALUES (
  SEQ_RESOURCE_ID.nextval, '/services/rest/appointment/AppointmentCalendarService/appointmentCalendarService/appointmentSearch',
  'APPT',2,'GET');

MERGE INTO RESOURCE_PERMISSION RP
USING (
        SELECT resource_id
        FROM RESOURCES
        WHERE uri = '/services/rest/appointment/AppointmentCalendarService/appointmentCalendarService/appointmentSearch'
              AND URI_TYPE_ID = 2
              AND HTTP_METHOD = 'GET'
      ) B ON (RP.RESOURCE_ID = B.RESOURCE_ID)
WHEN NOT MATCHED THEN INSERT VALUES ((
  SELECT resource_id
  FROM RESOURCES
  WHERE uri =
        '/services/rest/appointment/AppointmentCalendarService/appointmentCalendarService/appointmentSearch'
        AND URI_TYPE_ID = 2
        AND HTTP_METHOD = 'GET'), 'VAPT'
  );
  
COMMIT;

-- DBTicket DB-4614 PLSQL

@DBScripts/Product/PLSQL_Objects/ILM_APPT_TRG.sql

-- DBTicket DB-5505

EXEC QUIET_DROP('TRIGGER','ILM_APPT_CUST_TRG_B_I_U');

CREATE OR REPLACE
TRIGGER ILM_APPT_CUST_TRG
AFTER UPDATE
ON ILM_APPT_CUSTOM_ATTRIBUTE REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
vLAST_UPDATED_SOURCE_TYPE NUMBER;
vLAST_UPDATED_SOURCE	VARCHAR2(32);
BEGIN
SELECT LAST_UPDATED_SOURCE_TYPE, LAST_UPDATED_SOURCE INTO vLAST_UPDATED_SOURCE_TYPE, vLAST_UPDATED_SOURCE
FROM ILM_APPOINTMENTS 
WHERE APPOINTMENT_ID = :OLD.APPOINTMENT_ID ; 

IF ( :NEW.ATTRIBUTE_VALUE IS NULL AND :OLD.ATTRIBUTE_VALUE IS NOT NULL OR
:NEW.ATTRIBUTE_VALUE IS NOT NULL AND :OLD.ATTRIBUTE_VALUE IS NULL OR
:NEW.ATTRIBUTE_VALUE <> :OLD.ATTRIBUTE_VALUE)
THEN
INS_APPOINTMENT_EVENT (	:OLD.APPOINTMENT_ID,
:OLD.ATTRIBUTE_NAME,
:OLD.ATTRIBUTE_VALUE,
:NEW.ATTRIBUTE_VALUE,
vLAST_UPDATED_SOURCE_TYPE,
vLAST_UPDATED_SOURCE
);
END IF;
END;
/

-- DBTicket DB-5515

Delete from COMPANY_PARAMETER 
where param_def_id in ('missing_checkin_appointment_fields',
                       'warning_checkin_appointment_fields',
                       'mandatory_checkin_appointment_fields');

DELETE from param_def 
where param_def_id in ('missing_checkin_appointment_fields',
                       'warning_checkin_appointment_fields',
                       'mandatory_checkin_appointment_fields');
					   
update PARAM_DEF
set VALUE_GENERATOR_CLASS = 'com.manh.appointment.ui.finitevalue.MissingAppointmentFields'
where param_def_id in ('missing_appt_fields_alert',
                       'warning_appointment_fields',
                       'mandatory_appointment_fields');

COMMIT;

-- DBTicket DB-6897

update filter_layout set field_label = 'Planned Dock' where field_label = 'Dock assigned' and object_type = 'UI_APPOINTMENT';
update filter_layout set field_label = 'Planned Door' where field_label = 'Door assigned' and object_type = 'UI_APPOINTMENT';

MERGE INTO LABEL L
USING (SELECT 'FilterFieldLabel' BUNDLE_NAME, 'Planned Dock' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'Planned Dock',
'Planned Dock',
'FilterFieldLabel');

MERGE INTO LABEL L
USING (SELECT 'FilterFieldLabel' BUNDLE_NAME, 'Planned Door' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'Planned Door',
'Planned Door',
'FilterFieldLabel');

COMMIT;

-- DBTicket DB-6939

MERGE INTO LABEL L
USING (SELECT 'ILM' BUNDLE_NAME, 'Overlapping intervals are not valid' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'Overlapping intervals are not valid',
'Overlapping intervals are not valid',
'ILM');

MERGE INTO LABEL L
USING (SELECT 'ILM' BUNDLE_NAME, 'Slot time intervals must be in multiples of company level time interval' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'Slot time intervals must be in multiples of company level time interval',
'Slot time intervals must be in multiples of company level time interval',
'ILM');

MERGE INTO LABEL L
USING (SELECT 'ILM' BUNDLE_NAME, 'Time interval does not fit within the schedule' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'Time interval does not fit within the schedule',
'Time interval does not fit within the schedule',
'ILM');

commit;

-- DBTicket DB-7404

MERGE INTO FILTER_OBJECT_MAPPING L USING (SELECT 'APPT' OBJECT_TYPE, 'UI_APPOINTMENT' FILTER_OBJECT FROM DUAL) B
ON (L.OBJECT_TYPE = B.OBJECT_TYPE AND L.FILTER_OBJECT = B.FILTER_OBJECT)
WHEN NOT MATCHED THEN INSERT (L.OBJECT_TYPE,L.FILTER_OBJECT)
VALUES ('APPT','UI_APPOINTMENT');

COMMIT;

-- DBTicket DB-8007

ALTER TABLE ILM_APPOINTMENTS MODIFY CREATED_SOURCE VARCHAR2(50);
ALTER TABLE ILM_APPOINTMENTS MODIFY LAST_UPDATED_SOURCE VARCHAR2(50);
ALTER TABLE ILM_APPOINTMENT_EVENTS MODIFY CREATED_SOURCE VARCHAR2(50);

-- DBTicket DB-8031
alter sequence SEQ_APPOINTMENT_REPORT_ID maxvalue 999999999999;
alter sequence APPT_OBJECT_PK_ID_SEQ maxvalue 9999999999;
alter sequence SEQ_APPT_SETUP_UPLOAD_ID maxvalue 9999999999;
alter sequence ILM_CHGBACK_SEQ maxvalue 999999999;
alter sequence ILM_APPT_OBJ_SEQ maxvalue 999999999;
alter sequence SEQ_ILM_APPT_DOCKS_ID maxvalue 999999999;
alter sequence SEQ_ILM_APPT_LOAD_CONFIG_ID maxvalue 999999999;
alter sequence SEQ_ILM_APPT_NOTES_ID maxvalue 999999999;
alter sequence SEQ_ILM_APPT_SIZES_ID maxvalue 999999999;
alter sequence SEQ_ILM_APPT_UTILIZATION_ID maxvalue 99999999;
alter sequence SHIFT_ID_SEQ maxvalue 999999999;

-- DBTicket DB-8634

update ilm_appointments
set appt_status = 10
where appt_status = 2;

update ilm_appointments
set appt_status = 3
where appt_status = 4;

update ilm_appointments
set appt_status = 5
where appt_status in (6, 14);

delete from ILM_APPOINTMENT_STATUS where APPT_STATUS_CODE in (2,4,6,14);

COMMIT;

-- DBTicket DB-8679

update COMPANY_PARAMETER set param_value = 5
where PARAM_DEF_ID in ('appt_edit_end_stat_for_carrier_vendor', 'appt_edit_end_stat')
and PARAM_VALUE not in (select APPT_STATUS_CODE from ILM_APPOINTMENT_STATUS);

COMMIT;

-- DBTicket DB-8733

update param_def set dflt_value = 9 where param_def_id = 'appt_edit_end_stat';
update param_def set dflt_value = 5 where param_def_id = 'appt_edit_end_stat_for_carrier_vendor';

commit;

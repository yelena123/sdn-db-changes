-- DBTicket DB-1107
-- Seed scripts of CA Purge 2016 Deployment
@@DBScripts/Seed/Product/CA/DeployPurge.sql

-- Extra Check to have default purge only option in case of upgrade
UPDATE param_def
   SET dflt_value = 1
 WHERE param_def_id = 'ARCHIVE_PURGE_FLAG' AND param_group_id = 'ARCHIVE';

UPDATE company_parameter
   SET param_value = 1
 WHERE param_def_id = 'ARCHIVE_PURGE_FLAG' AND param_group_id = 'ARCHIVE';
COMMIT; 

-- DBTicket SIF-800
MERGE INTO PARAM_DEF P
     USING (SELECT 'ARCHIVE_MOBILE_ACTIVITY_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) D
        ON (P.PARAM_DEF_ID = D.PARAM_DEF_ID
            AND P.PARAM_GROUP_ID = D.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               IS_MIN_INC,
               IS_MAX_INC,
               DFLT_VALUE,
               IS_LOCALIZABLE)
       VALUES (
                 'ARCHIVE_MOBILE_ACTIVITY_WINDOW',
                 'ARCHIVE',
                 'Days after which MOBILE_ACTIVITY data can be archived/purged',
                 'Days after which MOBILE_ACTIVITY data can be archived/purged',
                 0,
                 0,
                 1,
                 'TEXTBOX',
                 0,
                 0,
                 '30',
                 0);


MERGE INTO PARAM_DEF P
     USING (SELECT 'ARCHIVE_MOBILE_ACTIVITY_STATUS' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) D
        ON (P.PARAM_DEF_ID = D.PARAM_DEF_ID
            AND P.PARAM_GROUP_ID = D.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               IS_MIN_INC,
               IS_MAX_INC,
               DFLT_VALUE,
               IS_LOCALIZABLE)
       VALUES (
                 'ARCHIVE_MOBILE_ACTIVITY_STATUS',
                 'ARCHIVE',
                 'Minimum status of MOBILE_ACTIVITY data which can be can be archived/purged',
                 'Minimum status of MOBILE_ACTIVITY data which can be archived/purged',
                 0,
                 0,
                 1,
                 'TEXTBOX',
                 0,
                 0,
                 '30',
                 0);

MERGE INTO PARAM_DEF P
     USING (SELECT 'ARCHIVE_STORE_SHIPMENT_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) D
        ON (P.PARAM_DEF_ID = D.PARAM_DEF_ID
            AND P.PARAM_GROUP_ID = D.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               IS_MIN_INC,
               IS_MAX_INC,
               DFLT_VALUE,
               IS_LOCALIZABLE)
       VALUES ('ARCHIVE_STORE_SHIPMENT_WINDOW',
               'ARCHIVE',
               'Days after which STORE_SHIPMENT data can be archived/purged',
               'Days after which STORE_SHIPMENT data can be archived/purged',
               0,
               0,
               1,
               'TEXTBOX',
               0,
               0,
               '30',
               0);

MERGE INTO PARAM_DEF P
     USING (SELECT 'ARCHIVE_STORE_SHIPMENT_STATUS' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) D
        ON (P.PARAM_DEF_ID = D.PARAM_DEF_ID
            AND P.PARAM_GROUP_ID = D.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               IS_MIN_INC,
               IS_MAX_INC,
               DFLT_VALUE,
               IS_LOCALIZABLE)
       VALUES (
                 'ARCHIVE_STORE_SHIPMENT_STATUS',
                 'ARCHIVE',
                 'Minimum status of STORE_SHIPMENT data which can be can be archived/purged',
                 'Minimum status of MOBILE_ACTIVITY data which can be archived/purged',
                 0,
                 0,
                 1,
                 'TEXTBOX',
                 0,
                 0,
                 '20',
                 0);

COMMIT;				 

-- DBTicket EEM-688 PLSQL
@DBScripts/Product/PLSQL_Objects/transient_purge.sql
@DBScripts/Product/PLSQL_Objects/eem_mobile_purge.sql
 
-- DBTicket ARCH-100 PLSQL
		 
-- DBTicket TPE-36316 PLSQL

-- DBTicket TPE-36745 PLSQL

-- DBTicket DOM-16731

CREATE TABLE ARCHIVE_PARAM_DEF
(
PARAM_DEF_ID      VARCHAR2(30)  NOT NULL,
PARAM_DESC      VARCHAR2(100)  NOT NULL,
PARAM_TYPE      VARCHAR2(10)  NOT NULL,
PARAM_VALUE   NUMBER       NOT NULL,
CREATED_DTTM  TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
updated_dttm  TIMESTAMP
) TABLESPACE CBO_STAT_DT_TBS;

CREATE OR REPLACE TRIGGER ARCHIVE_PARAM_DEF_UPDT_TRG 
BEFORE UPDATE ON ARCHIVE_PARAM_DEF 
FOR EACH ROW
BEGIN
:new.updated_dttm := systimestamp;
END;
/

create UNIQUE index ARCHIVE_PARAM_DEF_IDX on ARCHIVE_PARAM_DEF (PARAM_DEF_ID,PARAM_VALUE) TABLESPACE CBO_BASE_DT_TBS;

INSERT INTO ARCHIVE_PARAM_DEF   (PARAM_DEF_ID, PARAM_DESC , PARAM_TYPE  , PARAM_VALUE ) 
                        values  ('ARCHIVE_ORDER_CATEGORY_EQ','Order Categories which should be archived/purged','MULTIPLE',1);
INSERT INTO ARCHIVE_PARAM_DEF   (PARAM_DEF_ID, PARAM_DESC , PARAM_TYPE  , PARAM_VALUE ) 
                        VALUES  ('ARCHIVE_ORDER_CATEGORY_NE','Order Categories which should not be archived/purged','MULTIPLE',9999);

create table purge_criteria 
(no_of_days NUMBER(4) not null, 
purge_status NUMBER(10),
purge_type varchar2(20) not null,
purge_module varchar2(10) Not null)
 TABLESPACE CBO_STAT_DT_TBS; 

CREATE UNIQUE INDEX purge_criteria_IDX ON purge_criteria
(purge_module, PURGE_TYPE)
TABLESPACE CBO_BASE_DT_TBS;

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_ADJ_INV_STATUS' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_ADJ_INV_STATUS',
               'ARCHIVE',
               '',
               'Status of Adjustment Invoice which can be archived/purged',
               'Status of Adjustment Invoice which can be archived/purged',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '20',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);
COMMIT;

-- DBTicket SIF-2428 PLSQL

-- DBTicket SIF-2474 PLSQL

-- DBTicket DOM-17152 PLSQL

-- DBTicket TE-3006

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_FEDEX_TRAN_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_FEDEX_TRAN_WINDOW',
               'ARCHIVE',
               '',
               'Days after which Fedex transactions can be archived/purged',
               'Days after which Fedex transactions can be archived/purged',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

COMMIT;	

-- DBTicket SIF-2630 PLSQL

-- DBTicket TPE-37473 PLSQL

-- DBTicket TPE-39545 PLSQL

-- DBTicket DOM-20589 PLSQL

-- DBTicket DOM-22320
DELETE FROM COMPANY_PARAMETER WHERE PARAM_DEF_ID IN ('ARCHIVE_MOBILE_ACTIVITY_STATUS','ARCHIVE_MOBILE_ACTIVITY_WINDOW');

DELETE FROM PARAM_DEF WHERE PARAM_DEF_ID IN ('ARCHIVE_MOBILE_ACTIVITY_STATUS','ARCHIVE_MOBILE_ACTIVITY_WINDOW');

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_STORE_ACTIVITY_STATUS' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES (
                 'ARCHIVE_STORE_ACTIVITY_STATUS',
                 'ARCHIVE',
                 '',
                 'Minimum status of STORE_USER_ACTIVITY data which can be can be archived/purged',
                 'Minimum status of STORE_USER_ACTIVITY data which can be archived/purged',
                 '0',
                 '0',
                 '1',
                 'TEXTBOX',
                 '',
                 '0',
                 '',
                 '0',
                 '30',
                 '',
                 '',
                 '0',
                 '',
                 '',
                 '',
                 '',
                 SYSDATE,
                 SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_STORE_ACTIVITY_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES (
                 'ARCHIVE_STORE_ACTIVITY_WINDOW',
                 'ARCHIVE',
                 '',
                 'Days after which STORE_USER_ACTIVITY data can be archived/purged',
                 'Days after which STORE_USER_ACTIVITY data can be archived/purged',
                 '0',
                 '0',
                 '1',
                 'TEXTBOX',
                 '',
                 '0',
                 '',
                 '0',
                 '30',
                 '',
                 '',
                 '0',
                 '',
                 '',
                 '',
                 '',
                 SYSDATE,
                 SYSDATE);
	
COMMIT;

-- DBTicket DOM-21909

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_CUSTOMER_ORDER_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_CUSTOMER_ORDER_WINDOW',
               'ARCHIVE',
               '',
               'Days after which Customer Order should be archived/purged',
               'Days after which Customer Order should be archived/purged',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_SALES_ORDER_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_SALES_ORDER_WINDOW',
               'ARCHIVE',
               '',
               'Days after which Purchase Order should be archived/purged',
               'Days after which Purchase Order should be archived/purged',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '30',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

INSERT INTO ARCHIVE_PARAM_DEF   (PARAM_DEF_ID, PARAM_DESC , PARAM_TYPE  , PARAM_VALUE ) 
 VALUES  ('ARCHIVE_PO_STATUS','Minimum status of PO which can be archived/purged','MULTIPLE',1);
 
INSERT INTO ARCHIVE_PARAM_DEF   (PARAM_DEF_ID, PARAM_DESC , PARAM_TYPE  , PARAM_VALUE ) 
 VALUES  ('ARCHIVE_SALES_ORDER_STATUS','Minimum status of Sales Order which can be archived/purged','MULTIPLE',1); 

INSERT INTO ARCHIVE_PARAM_DEF   (PARAM_DEF_ID, PARAM_DESC , PARAM_TYPE  , PARAM_VALUE ) 
 VALUES  ('ARCHIVE_CUSTOMER_ORDER_STATUS','Minimum status of Customer Order which can be archived/purged','MULTIPLE',850);  
 
INSERT INTO ARCHIVE_PARAM_DEF   (PARAM_DEF_ID, PARAM_DESC , PARAM_TYPE  , PARAM_VALUE ) 
 VALUES  ('ARCHIVE_PAYMENT_STATUS','Payment status which can be archived/purged','MULTIPLE',30);
 
CREATE TABLE PAYMENT_TOKEN_ARCHIVE (Token_No VARCHAR2 (256))
TABLESPACE CBO_STAT_DT_TBS;

-- DBTicket TPE-40467 PLSQL

-- DBTicket DOM-22522 PLSQL
@DBScripts/Product/PLSQL_Objects/purge_other_logs_events.sql

-- DBTicket DOM-22543 PLSQL

-- DBTicket DOM-22647 PLSQL

-- DBTicket DOM-22666 PLSQL

-- DBTicket DOM-22827 PLSQL

-- DBTicket SIF-3635

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_FULFILLMENT_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_FULFILLMENT_WINDOW',
               'ARCHIVE',
               '',
               'Days after which Fulfillment should be purged',
               'Days after which Fulfillment should be purged',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '15',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

DELETE FROM COMPANY_PARAMETER WHERE PARAM_DEF_ID IN ('ARCHIVE_STORE_ACTIVITY_STATUS','ARCHIVE_STORE_ACTIVITY_WINDOW'); 
 
DELETE FROM PARAM_DEF WHERE PARAM_DEF_ID IN ('ARCHIVE_STORE_ACTIVITY_STATUS','ARCHIVE_STORE_ACTIVITY_WINDOW');

Insert into PARAM_DEF
   (PARAM_DEF_ID, PARAM_GROUP_ID, PARAM_NAME, DESCRIPTION, IS_DISABLED, IS_REQUIRED, DATA_TYPE, UI_TYPE, IS_MIN_INC, IS_MAX_INC, DFLT_VALUE, IS_LOCALIZABLE, CREATED_DTTM, LAST_UPDATED_DTTM, PARAM_LEVEL)
 Values
   ('ARCHIVE_STORE_ACTIVITY_STATUS', 'ARCHIVE', 'Minimum status of STORE_USER_ACTIVITY data which can be can be archived/purged', 'Minimum status of STORE_USER_ACTIVITY data which can be archived/purged', 
    0, 0, 1, 'TEXTBOX', 
    0, 0, '30', 0, SYSDATE, SYSDATE, 0);
	
Insert into PARAM_DEF
   (PARAM_DEF_ID, PARAM_GROUP_ID, PARAM_NAME, DESCRIPTION, IS_DISABLED, IS_REQUIRED, DATA_TYPE, UI_TYPE, IS_MIN_INC, IS_MAX_INC, DFLT_VALUE, IS_LOCALIZABLE, CREATED_DTTM, LAST_UPDATED_DTTM, PARAM_LEVEL)
 Values
   ('ARCHIVE_STORE_ACTIVITY_WINDOW', 'ARCHIVE', 'Days after which STORE_USER_ACTIVITY data can be archived/purged', 'Days after which STORE_USER_ACTIVITY data can be archived/purged', 
    0, 0, 1, 'TEXTBOX', 
    0, 0, '15', 0, SYSDATE, SYSDATE, 0);
	
COMMIT;	

-- DBTicket DOM-22217 PLSQL
@@DBScripts/Product/PLSQL_Objects/order_data_purge_pkg.sql

-- DBTicket EEM-6536 PLSQL

-- DBTicket TPE-41710 PLSQL

@@DBScripts/Product/PLSQL_Objects/ca_archive_pkg.sql

-- DBTicket DB-1262

Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ADM',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ADM_DETAIL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ADM_DETAIL_REASON',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ALERT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','APPT_SLOT_UTILIZATION',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','AP_ALERT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','AP_ALERT_ACTIVITY_DETAILS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','AP_ALERT_ARGS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','AP_ALERT_ESCALATION_DETAILS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','AP_ALERT_OBJECTS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ASN',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ASN_ATTRIBUTE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ASN_DETAIL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ASN_EVENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ASN_INFO',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ASN_NOTE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ASN_SEAL_NUMBER',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ASN_UNIT_DETAIL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_EMAIL_ADDRESS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_EMAIL_INFO',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_LINES_FOR_ALLOC',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_ORDERINVENTORYALLOCATION',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_POS_CHARGE_DETAIL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_POS_DISCOUNT_DETAIL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_POS_NOTE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_POS_PAYMENT_DETAIL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_POS_REASON_CODE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_POS_TAX_DETAIL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_POS_TRANSACTION',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_POS_TRANSACTION_DETAIL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_PT_INVOICE_MAPPING',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_SOURCINGAUDITDETAIL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','BOOKING_PURCHASE_ORDER',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CARRIER_CODE_RESPONSE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CLAIMS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CLAIM_COMMENTS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CLAIM_ITEMS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CLAIM_JOURNAL_ENTRIES',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CLAIM_PAYMENTS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CLAIM_REASON_CODE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','COGI',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','COGI_DETAIL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','COGI_POSTINGS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','COMB_LANE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','COMB_LANE_DTL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CONS_ORDER_PATH',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CWS_SCENARIO_ORDERS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CWS_STOP',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DAILY_UTILIZATION',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DEADHEAD_STOPS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DETENTION_COMMENTS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DETENTION_REQUEST',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DRIVER_BID',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DRIVER_BID_DRIVER_MAP',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DRIVER_BID_SEGMENT_MAP',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DRIVER_BID_TRACTOR_MAP',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DRIVER_SCHED_EXCEPTION',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DSP_COLLECTION_ACTIVITY',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DSP_DRIVER_LOG_BASE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DSP_DRIVER_LOG_COMP',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DSP_STATE_TAX_MILES',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DSP_TRANSACTION',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ERROR_LOG',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ERROR_LOG_ARGS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','FEDEX_LPN',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','FORECAST_LANE_MAPPING',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ILM_APPOINTMENTS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ILM_APPOINTMENTS_CHARGEBACK',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ILM_APPOINTMENT_EVENTS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ILM_APPOINTMENT_OBJECTS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ILM_APPT_CUSTOM_ATTRIBUTE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ILM_APPT_DOCKS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ILM_APPT_EQUIPMENTS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ILM_APPT_LOAD_CONFIG',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ILM_APPT_NOTES',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ILM_APPT_SIZES',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ILM_APPT_SLOTS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ILM_TASKS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ILM_TASK_EVENTS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ILM_TASK_HISTORY',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ILM_YARD_ACTIVITY',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ILM_YARD_AUDIT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','INSPECTION',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','INSPECTION_OBJECT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','INSPECTION_OBJECT_DEFECT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','INVITATION',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','INVOICE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','INVOICED_BKG_EQPMNT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','INVOICED_BKG_SHIPMENTS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','INVOICED_OBJECT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','INVOICED_OBJ_ACCESSORIAL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','INVOICE_ADJUSTMENTS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','INVOICE_ATTRIBUTE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','INVOICE_BOL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','INVOICE_EVENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','INVOICE_EVENT_MESSAGE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','INVOICE_NOTE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','INVOICE_ORDER_ACCOUNT_CODE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','INVOICE_ORDER_LINE_ITEM',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','INVOICE_SCHED_ACTION',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','INV_BKG_ACCESSORIAL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','INV_LN_ITEM_ACC_CODE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','LANE_ACCESSORIAL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','LANE_DTL_FORECAST',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','LPN',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','LPN_ACCESSORIAL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','LPN_ADDRESS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','LPN_ATTRIBUTE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','LPN_DETAIL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','LPN_EVENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','LPN_LOCK',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','LPN_MOVEMENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','LPN_NOTES',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','MANIFESTED_LPN',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','MANIFEST_HDR',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDERS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDERS_EXTN_TLM',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDERS_TEMPLATE_RUN',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_ACCESSORIAL_OPTION_GRP',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_ATTRIBUTE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_BASELINE_COST',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_EVENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_LINE_ITEM',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_LINE_ITEM_ATTRIBUTE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_LINE_ITEM_SIZE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_MASTER_ORDER',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_MOVEMENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_NOTE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_SPLIT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_SPLIT_SIZE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_SRL_NBR',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_WORK_TYPE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','PO_LINE_CLASSIFICATION_CODE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','PO_LINE_ITEMS_EXTN_EEM',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','PO_LINE_ITEM_REF_FIELDS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','PO_LINE_WMPROCESSINFO',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','PO_REF_FIELDS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','PO_WMPROCESSINFO',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','PURCHASE_ORDERS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','PURCHASE_ORDERS_ATTRIBUTE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','PURCHASE_ORDERS_EVENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','PURCHASE_ORDERS_EXTN_TLM',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','PURCHASE_ORDERS_LINE_ITEM',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','PURCHASE_ORDERS_NOTE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','PURCHASE_ORDER_BP',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','RATING_LANE_DTL_RATE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','REMIT_ADV',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','REMIT_ADV_DETAIL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','RESOURCE_OPTION',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','RLM_PACKAGES',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','RLM_RETURN_ORDERS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','RLM_RETURN_ORDERS_LINE_ITEM',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','RTS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','RTS_EVENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','RTS_LINE_ITEM',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','RTS_SIZES',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SAILING_SCHDL_BY_DATE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SAILING_SCHDL_BY_WEEK',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SAILING_SCHDL_BY_WEEK_DAY',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SEGMENT_ASSIGNMENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SEGMENT_ATTRIBUTE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SHIPMENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SHIPMENT_ACCESSORIAL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SHIPMENT_ATTRIBUTE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SHIPMENT_COMMODITY',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SHIPMENT_EVENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SHIPMENT_EXCL_ACCESSORIAL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SHIPMENT_EXTN_TLM',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SHIPMENT_LOAD_DETAIL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SHIPMENT_NOTE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SHIPMENT_SCHED_ACTION',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SHIPMENT_SEAL_NUMBER',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SHIPMENT_SIZE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SHIPMENT_SIZE_OVERRIDE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SHIPMENT_TEMPLATE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SHIPMENT_TRACKING',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SHP_RS_OPTION',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SPLITTER_ORDER_LI_EXCPTN',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','STOP',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','STOP_ACTION',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','STOP_ACTION_ORDER',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','STOP_ACTION_SHIPMENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','STOP_ADDITIONAL_SIZE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','STOP_COMMODITY',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','STOP_SIZE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','STOP_SIZE_TEMPLATE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SURGE_CAPACITY',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','TRACKING_MESSAGE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','TRACKING_MSG_DRV_TRIP_ACTVTY',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','TRANS_FORECAST',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','TRCK_MSG_COLL_ACT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','TRIP',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','TRIP_ATTRIBUTE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','BOOKING_DTL_EQPMNT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','BOOKING_EVENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_CHARGE_DETAIL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_PAYMENT_DETAIL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CONS_RUN_MONITOR',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_ALLOC_FAILED_DETAILS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_CO_NOTE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_DISCOUNT_DETAIL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_INVOICE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_INVOICE_LINE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_PAYMENT_TRANSACTION',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_PO_LINE_DOM_EXT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_TAX_DETAIL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_UNRELEASABLE_ALLOC',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','BOOKING',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','BOOKING_ACCESSORIAL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','BOOKING_DTL_EQUIP_INS_ORDERS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','BOOKING_ORDERS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','BOOKING_ORDER_LINE_ITEM',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','BOOKING_RTS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','BOOKING_RTS_LINE_ITEM',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','BOOKING_SHIPMENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CONS_AGGREGATION',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CONS_AGGREGATION_MAP',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CONS_RUN',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CONS_RUN_DTL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CONS_RUN_DTL_ABOVE4',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CONTINUOUS_MOVE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CUSTOM_ACCOUNT_CODE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DOCUMENT_MANAGEMENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DO_LINE_WMPROCESSINFO',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DO_WMPROCESSINFO',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','EEM_MANIFESTED_LPN',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','EEM_MANIFEST_HDR',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','EM_LINK_OBJECT_SCHED',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','EM_OBJECT_RULE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','EM_OBJECT_SCHEDULE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','EM_OBJECT_SCHEDULE_EVENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ISF_AUDIT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ISF_BOL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ISF_BOL_ATTR',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ISF_BOL_LINE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ISF_BOL_LINE_ATTR',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ISF_BOND_HOLDER',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ISF_COMMODITY',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ISF_FILER_IDENTIFICATION',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ISF_HEADER',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ISF_SHIPMENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','LETTER_OF_INSTRUCTION',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_ATTRIBUTE_TEMPLATE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_REASON_CODE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_SCHED_ACTION',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_SIZES',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_SPLIT_LINE_ITEM',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_SRL_NBR_TEMPLATE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_TEMPLATE_OD_PAIR',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','PO_NEGOTIATION_PROCESS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SPLITTER_ORDERS_SMALL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SPLITTER_RUN_STATS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','STOP_SCHED_ACTION',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','FEDEX_TRAN',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','PURCHASE_ORDER_LINE_COMPONENTS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ASN_DETAIL_ATTRIBUTE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DO_LINE_CLASSIFICATION_CODE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ARTIFACT_EXTERNAL_USERS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','PO_LINE_ITEM_ATTRIBUTE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','PURCHASE_ORDER_BP_CONTACT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','STORE_SHIPMENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','BOOKING_EQUIPMENT_INSTANCE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','BOOKING_ORDER_LINE_ITEM_SIZE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','BOOKING_RTS_LINE_ITEM_SIZE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DO_LINE_ALLOCATION',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','STORE_USER_ACTIVITY',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SEGMENT_SHIPMENT_EQUIPMENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ARTIFACT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DRIVER_SHIPMENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','POD_COMMANDS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','POD_UNIT_OF_DELIVERY',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','POD_INTERACTION_PARAMETER',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','POD_INTERACTION',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ALERT_NOTIFICATION_EVENTS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DOCK_DOOR_REF',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','LPN_CATCH_WEIGHT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORPHANED_ORDER_REASONS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','TRAILER_CONTENTS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','TRAILER_VISIT_DETAIL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','LPN_ADDRESS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','LPN_ACCESSORIAL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','LPN_EVENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','LPN_NOTES',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','LPN_LOCK',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DRIVER_BID_SEGMENT_MAP',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_LINE_ITEM_SIZE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_SPLIT_LINE_ITEM',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_LINE_ITEM',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','BOOKING_SHIPMENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CONS_ORDER_PATH',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ILM_APPT_SIZES',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CWS_SCENARIO_ORDERS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','BOOKING_ORDERS',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CONS_AGGREGATION_MAP',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_ATTRIBUTE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DO_LINE_WMPROCESSINFO',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DO_WMPROCESSINFO',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_SIZES',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','TRACKING_MSG_DRV_TRIP_ACTVTY',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_MASTER_ORDER',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_SPLIT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_SPLIT_SIZE',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDERS_TEMPLATE_RUN',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','INSPECTION_OBJECT_DEFECT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','INSPECTION_OBJECT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','EEM_MANIFESTED_LPN',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_EVENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','TRCK_MSG_COLL_ACT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','LPN_MOVEMENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DOCUMENT_MANAGEMENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','LPN_DETAIL',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_BASELINE_COST',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_MOVEMENT',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','TPE_STOP_EXTN',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DRIVER_EXTENSION',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','TRACKING_MESSAGE_COMMODITY',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','TRACKING_MSG_BKNG_RESPONSE',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_ACTIVITY_1',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_ACTIVITY_2',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_ACTIVITY_3',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_ACTIVITY_4',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_ACTIVITY_5',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_ACTIVITY_6',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_ACTIVITY_7',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_ACTIVITY_8',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_ACTIVITY_9',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_ACTIVITY_10',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_ACTIVITY_11',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_ACTIVITY_12',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_ACTIVITY_13',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_ACTIVITY_14',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_ACTIVITY_15',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_ACTIVITY_16',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_ACTIVITY_17',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_ACTIVITY_18',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_ACTIVITY_19',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_ACTIVITY_20',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_1',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_2',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_3',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_4',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_5',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_6',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_7',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_8',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_9',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_10',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_11',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_12',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_13',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_14',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_15',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_16',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_17',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_18',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_19',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_20',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_ASSOCIATED_AVAILABILITY',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_EXCLUSION_1',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_EXCLUSION_2',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_EXCLUSION_3',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_EXCLUSION_4',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_EXCLUSION_5',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_EXCLUSION_6',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_EXCLUSION_7',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_EXCLUSION_8',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_EXCLUSION_9',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_EXCLUSION_10',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_EXCLUSION_11',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_EXCLUSION_12',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_EXCLUSION_13',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_EXCLUSION_14',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_EXCLUSION_15',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_EXCLUSION_16',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_EXCLUSION_17',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_EXCLUSION_18',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_EXCLUSION_19',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAILABILITY_EXCLUSION_20',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_EVENT_STATUS_1',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_EVENT_STATUS_2',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_EVENT_STATUS_3',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_EVENT_STATUS_4',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_EVENT_STATUS_5',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_EVENT_STATUS_6',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_EVENT_STATUS_7',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_EVENT_STATUS_8',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_EVENT_STATUS_9',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_EVENT_STATUS_10',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_EVENT_STATUS_11',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_EVENT_STATUS_12',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_EVENT_STATUS_13',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_EVENT_STATUS_14',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_EVENT_STATUS_15',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_EVENT_STATUS_16',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_EVENT_STATUS_17',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_EVENT_STATUS_18',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_EVENT_STATUS_19',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_EVENT_STATUS_20',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_BY_INV_1',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_BY_INV_2',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_BY_INV_3',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_BY_INV_4',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_BY_INV_5',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_BY_INV_6',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_BY_INV_7',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_BY_INV_8',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_BY_INV_9',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_BY_INV_10',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_BY_INV_11',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_BY_INV_12',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_BY_INV_13',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_BY_INV_14',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_BY_INV_15',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_BY_INV_16',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_BY_INV_17',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_BY_INV_18',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_BY_INV_19',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_BY_INV_20',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_ALLOCATION',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_AVAIL_INVENTORY_EVENT_DETAIL',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_SEGMENTED',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_SEGMENT_PLAN',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_SEGMENTED_INV_LOG',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_PERPETUAL_INV_LOG',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_INV_EVENT_LOG',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_ONHAND_MOVEMENT',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_PERPETUAL',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','LPN_DETAIL_ATTRIBUTE',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','BKG_ANALYSIS_RUN_ORDS',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CWS_ORDER_MOVEMENT',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CWS_ORDER_SPLIT',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CWS_ORDER',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDERS_TEMPLATE',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_NOTE_TEMPLATE',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','TRACKING_MSG_BKG_RSP_EQMT_DTL',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','STOP_ADDITIONAL_SIZE_TEMPLATE',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SHP_RS_OPT_ACCESSORIAL',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SHP_RS_OPT_PF',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SHP_RS_OPT_EXCL_ACCL',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','RPOPT_RPLEGSET_SHPRSOPT',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CWS_ORIGINAL_MOVEMENT',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDER_REF_FIELDS',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDERS_PATH_WAYPOINT',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ORDERS_WM_FIELDS',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','DOCK_APPOINTMENT',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SHP_EXTRACT_HISTORY',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SHIPMENT_SKU_ACC_CODE',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SHIPMENT_ORDER_ACC_CODE',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','SHIPMENT_IN_YARD_EQUIP_MAP',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CM_LEG',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','FULFILLMENT_EVENT',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','WORK_BATCH_DTL_TEMPLATE_TAG',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','ACTIVITY_WORK_BATCH_DTL',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','WORK_BATCH_DTL',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','STORE_USER_ACTIVITY',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','MSG_BOARD_EXT_USERS',NULL,'Y','Product'); 
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','MSG_BOARD',NULL,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_ALLOC_COST_TRACE_CARTON',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_ALLOC_COST_TRACE',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_FACILITY_TRACE_DETAIL',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_FACILITY_TRACE',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_ALLOCATION_TRACE_DETAIL',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_CUSTOMER_PAYMENT_INFO',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_ORDERLINE_TIER_FACILITY_RANK',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_UNRELEASABLE_ALLOC',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_ORDERLINE_QUANTITY_STATUS',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','PO_LINE_ITEM_ATTRIBUTE',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_SRC_EXCL_RESULT',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','FEDEX_LPN',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CHARGEBACK_AUDIT',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_INVOICE_EVENT',NULL,'N','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','CHARGEBACK',NULL,'N','Product');

COMMIT;

-- DBTicket DB-1263 PLSQL

@@DBScripts/Product/PLSQL_Objects/ca_archive_pkg.sql

-- DBTicket DB-1681
ALTER INDEX ARCHIVE_PARAM_DEF_IDX REBUILD TABLESPACE CBO_BASE_IDX_TBS;

ALTER INDEX PURGE_CRITERIA_IDX  REBUILD TABLESPACE CBO_BASE_IDX_TBS;

ALTER INDEX PURGE_DAYS_IDX0   REBUILD TABLESPACE CBO_BASE_IDX_TBS;

-- DBTicket DB-1135 PLSQL
@@DBScripts/Product/PLSQL_Objects/order_data_purge_pkg.sql

-- DBTicket DB-1856

INSERT INTO ARCHIVE_PARAM_DEF (PARAM_DEF_ID,
                               PARAM_DESC,
                               PARAM_TYPE,
                               PARAM_VALUE)
     VALUES ('ARCHIVE_SITE_ID',
             'Sites which should be archived/purged',
             'MULTIPLE',
             -99999);

COMMIT;

-- DBTicket DB-1058

CREATE TABLE ARCH_PURGE_PLAN_TABLE
(
  SHIPMENT_ID         NUMBER(10),
  CMID                NUMBER(15),
  INV_ID              NUMBER(8),
  CLAIM_ID            NUMBER(12),
  ASN_ID              NUMBER(10),
  MANIFEST_ID         NUMBER(9),
  BOOKING_ID          NUMBER(8),
  SEGMENT_ID          NUMBER(8),
  BID_ID              NUMBER(8),
  TRIP_ID             NUMBER(12),
  APPOINTMENT_ID      NUMBER(9),
  STOP_ID             NUMBER(8),
  CONS_RUN_ID         NUMBER(12),
  ORDER_ID            NUMBER(10),
  LPN_ID              NUMBER(10),
  PURCHASE_ORDERS_ID  NUMBER(10),
  RTS_ID              NUMBER(10),
  PO_INVOICE          NUMBER(8),
  PO_APPOINTMENT      NUMBER(9),
  IS_ARCHIVABLE       NUMBER
)
TABLESPACE ARCH_DT_TBS;

-- DBTicket DB-3524 
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_COMMERCE_VIEW_STATS',null,'Y','Product');
Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_COMMERCE_STATS_BY_FTYPE_RS',null,'Y','Product');
insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','I_COMMERCE_VIEW_STATS_BY_EXCL',null,'Y','Product');

INSERT INTO PARAM_DEF (PARAM_DEF_ID,
                       PARAM_GROUP_ID,
                       PARAM_SUBGROUP_ID,
                       PARAM_NAME,
                       DESCRIPTION,
                       IS_DISABLED,
                       IS_REQUIRED,
                       DATA_TYPE,
                       UI_TYPE,
                       MIN_VALUE,
                       IS_MIN_INC,
                       MAX_VALUE,
                       IS_MAX_INC,
                       DFLT_VALUE,
                       DISPLAY_ORDER,
                       VALUE_GENERATOR_CLASS,
                       IS_LOCALIZABLE,
                       PARAM_CATEGORY,
                       CARR_DFLT_VALUE,
                       BP_DFLT_VALUE,
                       PARAM_VALIDATOR_CLASS,
                       CREATED_DTTM,
                       LAST_UPDATED_DTTM,
                       PARAM_LEVEL)
     VALUES ('ARCHIVE_COMMERCE_VIEW_WINDOW',
             'ARCHIVE',
             NULL,
             'Days after which commerce view data should be archived/purged',
             'Days after which commerce view data should be archived/purged',
             '0',
             '0',
             '1',
             'TEXTBOX',
             NULL,
             '0',
             NULL,
             '0',
             '365',
             NULL,
             NULL,
             '0',
             NULL,
             NULL,
             NULL,
             NULL,current_timestamp,current_timestamp,'0');
commit;

-- DBTicket DB-3610 PLSQL
@@DBScripts/Product/PLSQL_Objects/ca_archive_pkg.sql
@@DBScripts/Product/PLSQL_Objects/order_data_purge_pkg.sql

-- DBTicket DB-3597 PLSQL
@@DBScripts/Product/PLSQL_Objects/PURGE_ONLY_PROC.sql
@@DBScripts/Product/PLSQL_Objects/transient_purge.sql
@@DBScripts/Product/PLSQL_Objects/purge_other_logs_events.sql

-- DBTicket DB-3744

EXEC QUIET_DROP ('TABLE','ARCH_PURGE_PLAN_TABLE');

CREATE TABLE ARCH_PURGE_PLAN_TABLE
(
   SHIPMENT_ID          NUMBER (10),
   CMID                 NUMBER (15),
   INV_ID               NUMBER (8),
   CLAIM_ID             NUMBER (12),
   ASN_ID               NUMBER (10),
   MANIFEST_ID          NUMBER (9),
   BOOKING_ID           NUMBER (8),
   SEGMENT_ID           NUMBER (8),
   BID_ID               NUMBER (8),
   TRIP_ID              NUMBER (12),
   APPOINTMENT_ID       NUMBER (9),
   STOP_ID              NUMBER (8),
   CONS_RUN_ID          NUMBER (12),
   ORDER_ID             NUMBER (10),
   LPN_ID               NUMBER (10),
   PURCHASE_ORDERS_ID   NUMBER (10),
   RTS_ID               NUMBER (10),
   PO_INVOICE           NUMBER (8),
   PO_APPOINTMENT       NUMBER (9),
   CHARGEBACK_ID        NUMBER (9),
   INSPECTION_ID        NUMBER (9),
   ADM_ID               NUMBER (9),
   COGI_ID              NUMBER (9),
   RA_ID                NUMBER (9),
   MANIFESTED_LPN_ID    NUMBER (9),
   IS_ARCHIVABLE        NUMBER
)
TABLESPACE ARCH_DT_TBS;

CREATE TABLE ARCHIVEABLE_CONSRUN
(
   CONS_RUN_ID   NUMBER (10),
   IS_ARCHIVED   NUMBER (1) DEFAULT 0 NOT NULL
)
TABLESPACE CBO_BASE_DT_TBS;

CREATE TABLE ARCHIVEABLE_ISF
(
   ISF_HEADER_ID   NUMBER (10),
   IS_ARCHIVED     NUMBER (1) DEFAULT 0 NOT NULL
)
TABLESPACE CBO_BASE_DT_TBS;

CREATE TABLE ARCHIVEABLE_CYCLECOUNT
(
   CC_REQ_ID       NUMBER (10),
   CC_REQ_STR_ID   NUMBER (10)
)
TABLESPACE CBO_BASE_DT_TBS;

-- DBTicket DB-3835

Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','TRANSACTION_ARCHIVE',null,'Y','Product');

COMMIT;

-- DBTicket DB-4083

UPDATE om_sched_event
   SET event_exp_date = TO_DATE ('01-Jan-18', 'DD-MON-RR'),
       event_freq_per_day = 288
 WHERE event_type = (SELECT scheduler_event_id
                       FROM scheduler_event_type
                      WHERE scheduler_event_name = 'SEARCH_DELTA_INDEX');
COMMIT;					  
					  
-- DBTicket DB-5088

DECLARE
   v_sys_cpu           PLS_INTEGER := 0;
   v_parallel_thread   PLS_INTEGER := 0;
   v_sql               VARCHAR2 (1000);
BEGIN
   SELECT VALUE
     INTO v_sys_cpu
     FROM v$osstat
    WHERE stat_name = 'NUM_CPUS';

   v_parallel_thread := ROUND(v_sys_cpu / 2);

   v_sql :=
         'MERGE INTO PARAM_DEF P
     USING (SELECT ''ARCHIVE_PARALLEL_THREADS'' PARAM_DEF_ID,
                   ''ARCHIVE'' PARAM_GROUP_ID
              FROM DUAL) D
        ON (P.PARAM_DEF_ID = D.PARAM_DEF_ID
            AND P.PARAM_GROUP_ID = D.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               IS_MIN_INC,
               IS_MAX_INC,
               DFLT_VALUE,
               IS_LOCALIZABLE)
       VALUES 
	   (''ARCHIVE_PARALLEL_THREADS'', ''ARCHIVE'', ''Number of threads used for parallel execution'', ''Number of threads used for parallel execution'', 0, 0, 1, ''TEXTBOX'', 0, 0,'''
      || v_parallel_thread
      || ''',0)
WHEN MATCHED THEN
update set DFLT_VALUE = '
      || v_parallel_thread;

   EXECUTE IMMEDIATE v_sql;
   COMMIT;
END;
/

-- DBTicket DB-5565
MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_SAILING_SCHDL_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_SAILING_SCHDL_WINDOW',
               'ARCHIVE',
               '',
               'Days after which Sailing Schedule data should be archived/purged',
               'Days after which Sailing Schedule data should be archived/purged',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

COMMIT;	

-- DBTicket DB-5478

EXEC QUIET_DROP ('TABLE','ARCH_PURGE_PLAN_TABLE');

CREATE TABLE ARCH_PURGE_PLAN_TABLE
(
   SHIPMENT_ID          NUMBER (10),
   CMID                 NUMBER (15),
   INV_ID               NUMBER (8),
   CLAIM_ID             NUMBER (12),
   ASN_ID               NUMBER (10),
   MANIFEST_ID          NUMBER (9),
   BOOKING_ID           NUMBER (8),
   SEGMENT_ID           NUMBER (8),
   BID_ID               NUMBER (8),
   TRIP_ID              NUMBER (12),
   APPOINTMENT_ID       NUMBER (9),
   STOP_ID              NUMBER (8),
   CONS_RUN_ID          NUMBER (12),
   ORDER_ID             NUMBER (10),
   LPN_ID               NUMBER (10),
   PURCHASE_ORDERS_ID   NUMBER (10),
   RTS_ID               NUMBER (10),
   PO_INVOICE           NUMBER (8),
   PO_APPOINTMENT       NUMBER (9),
   CHARGEBACK_ID        NUMBER (9),
   INSPECTION_ID        NUMBER (9),
   ADM_ID               NUMBER (9),
   COGI_ID              NUMBER (9),
   RA_ID                NUMBER (9),
   MANIFESTED_LPN_ID    NUMBER (9),
   IS_ARCHIVABLE        NUMBER,
   REASON_DESC          VARCHAR2 (200)
)
TABLESPACE CBO_BASE_DT_TBS;

-- DBTicket DB-6218

Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_SOURCING_AUDIT_AVAIL_RULE',NULL,'N','Product');

Insert into MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME,TABLE_NAME,PURGE_CODE,ARCHIVE_FLAG,MODULE) values ('CA','A_SHIPPING_CHARGE',NULL,'Y','Product');

COMMIT;

-- DBTicket DB-6473

TRUNCATE TABLE manh_product_purging_tables;

Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_BY_INV_19', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_BY_INV_20', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_ALLOCATION', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_INVENTORY_EVENT_DETAIL', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_SEGMENTED', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_SEGMENT_PLAN', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_SEGMENTED_INV_LOG', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_PERPETUAL_INV_LOG', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_INV_EVENT_LOG', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_ONHAND_MOVEMENT', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_PERPETUAL', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'LPN_DETAIL_ATTRIBUTE', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'BKG_ANALYSIS_RUN_ORDS', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'CWS_ORDER_MOVEMENT', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'CWS_ORDER_SPLIT', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'CWS_ORDER', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'ORDERS_TEMPLATE', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'ORDER_NOTE_TEMPLATE', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'TRACKING_MSG_BKG_RSP_EQMT_DTL', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'STOP_ADDITIONAL_SIZE_TEMPLATE', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'SHP_RS_OPT_ACCESSORIAL', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'SHP_RS_OPT_PF', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'SHP_RS_OPT_EXCL_ACCL', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'RPOPT_RPLEGSET_SHPRSOPT', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'CWS_ORIGINAL_MOVEMENT', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'ORDER_REF_FIELDS', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'ORDERS_PATH_WAYPOINT', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'ORDERS_WM_FIELDS', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'DOCK_APPOINTMENT', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'SHP_EXTRACT_HISTORY', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'SHIPMENT_SKU_ACC_CODE', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'SHIPMENT_ORDER_ACC_CODE', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'SHIPMENT_IN_YARD_EQUIP_MAP', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'CM_LEG', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'FULFILLMENT_EVENT', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'WORK_BATCH_DTL_TEMPLATE_TAG', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'ACTIVITY_WORK_BATCH_DTL', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'WORK_BATCH_DTL', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'MSG_BOARD_EXT_USERS', 'Y', 'Product', 'ARCH_MSG_BOARD_EXT_USERS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'MSG_BOARD', 'Y', 'Product', 'ARCH_MSG_BOARD');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'A_ALLOC_COST_TRACE_CARTON', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'A_ALLOC_COST_TRACE', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'A_FACILITY_TRACE_DETAIL', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'A_FACILITY_TRACE', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'A_ALLOCATION_TRACE_DETAIL', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'A_CUSTOMER_PAYMENT_INFO', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'A_ORDERLINE_TIER_FACILITY_RANK', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'A_ORDERLINE_QUANTITY_STATUS', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'A_SRC_EXCL_RESULT', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'CHARGEBACK_AUDIT', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'A_INVOICE_EVENT', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'CHARGEBACK', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'I_COMMERCE_VIEW_STATS', 'Y', 'Product', 'ARCH_I_COMMERCE_VIEW_STATS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'I_COMMERCE_STATS_BY_FTYPE_RS', 'Y', 'Product', 'ARCH_I_COMM_STATS_BY_FTYPE_RS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'I_COMMERCE_VIEW_STATS_BY_EXCL', 'Y', 'Product', 'ARCH_I_COMM_VIEW_STATS_BY_EXCL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'TRANSACTION_ARCHIVE', 'Y', 'Product', 'ARCH_TRANSACTION_ARCHIVE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'A_SOURCING_AUDIT_AVAIL_RULE', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_SHIPPING_CHARGE', 'Y', 'Product', 'ARCH_A_SHIPPING_CHARGE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'COMB_LANE', 'Y', 'Product', 'ARCH_COMB_LANE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'CONS_ORDER_PATH', 'Y', 'Product', 'ARCH_CONS_ORDER_PATH');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'CWS_STOP', 'Y', 'Product', 'ARCH_CWS_STOP');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'DAILY_UTILIZATION', 'Y', 'Product', 'ARCH_DAILY_UTILIZATION');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'DETENTION_REQUEST', 'Y', 'Product', 'ARCH_DETENTION_REQUEST');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'DRIVER_BID_DRIVER_MAP', 'Y', 'Product', 'ARCH_DRIVER_BID_DRIVER_MAP');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'DRIVER_BID_TRACTOR_MAP', 'Y', 'Product', 'ARCH_DRIVER_BID_TRACTOR_MAP');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'DSP_COLLECTION_ACTIVITY', 'Y', 'Product', 'ARCH_DSP_COLLECTION_ACTIVITY');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'DSP_DRIVER_LOG_COMP', 'Y', 'Product', 'ARCH_DSP_DRIVER_LOG_COMP');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ERROR_LOG', 'Y', 'Product', 'ARCH_ERROR_LOG');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'FEDEX_LPN', 'Y', 'Product', 'ARCH_FEDEX_LPN');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'FORECAST_LANE_MAPPING', 'Y', 'Product', 'ARCH_FORECAST_LANE_MAPPING');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ILM_APPOINTMENT_OBJECTS', 'Y', 'Product', 'ARCH_ILM_APPOINTMENT_OBJECTS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ILM_APPT_DOCKS', 'Y', 'Product', 'ARCH_ILM_APPT_DOCKS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ILM_APPT_LOAD_CONFIG', 'Y', 'Product', 'ARCH_ILM_APPT_LOAD_CONFIG');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ILM_APPT_SIZES', 'Y', 'Product', 'ARCH_ILM_APPT_SIZES');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ILM_TASKS', 'Y', 'Product', 'ARCH_ILM_TASKS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ILM_TASK_HISTORY', 'Y', 'Product', 'ARCH_ILM_TASK_HISTORY');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ILM_YARD_AUDIT', 'Y', 'Product', 'ARCH_ILM_YARD_AUDIT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'INSPECTION_OBJECT', 'Y', 'Product', 'ARCH_INSPECTION_OBJECT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'INVITATION', 'Y', 'Product', 'ARCH_INVITATION');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'INVOICED_BKG_EQPMNT', 'Y', 'Product', 'ARCH_INVOICED_BKG_EQPMNT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'INVOICED_OBJECT', 'Y', 'Product', 'ARCH_INVOICED_OBJECT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'INVOICE_ADJUSTMENTS', 'Y', 'Product', 'ARCH_INVOICE_ADJUSTMENTS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'INVOICE_EVENT', 'Y', 'Product', 'ARCH_INVOICE_EVENT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'INVOICE_EVENT_MESSAGE', 'Y', 'Product', 'ARCH_INVOICE_EVENT_MESSAGE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'INVOICE_SCHED_ACTION', 'Y', 'Product', 'ARCH_INVOICE_SCHED_ACTION');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'INV_LN_ITEM_ACC_CODE', 'Y', 'Product', 'ARCH_INV_LN_ITEM_ACC_CODE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'LANE_DTL_FORECAST', 'Y', 'Product', 'ARCH_LANE_DTL_FORECAST');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'LPN_ATTRIBUTE', 'Y', 'Product', 'ARCH_LPN_ATTRIBUTE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'LPN_LOCK', 'Y', 'Product', 'ARCH_LPN_LOCK');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'LPN_NOTES', 'Y', 'Product', 'ARCH_LPN_NOTES');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'MANIFEST_HDR', 'Y', 'Product', 'ARCH_MANIFEST_HDR');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDERS_EXTN_TLM', 'Y', 'Product', 'ARCH_ORDERS_EXTN_TLM');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDER_ATTRIBUTE', 'Y', 'Product', 'ARCH_ORDER_ATTRIBUTE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDER_EVENT', 'Y', 'Product', 'ARCH_ORDER_EVENT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDER_LINE_ITEM_ATTRIBUTE', 'Y', 'Product', 'ARCH_ORDER_LINE_ITEM_ATTRIBUTE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDER_MASTER_ORDER', 'Y', 'Product', 'ARCH_ORDER_MASTER_ORDER');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDER_SPLIT', 'Y', 'Product', 'ARCH_ORDER_SPLIT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDER_SRL_NBR', 'Y', 'Product', 'ARCH_ORDER_SRL_NBR');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'PO_LINE_ITEMS_EXTN_EEM', 'Y', 'Product', 'ARCH_PO_LINE_ITEMS_EXTN_EEM');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'PO_LINE_WMPROCESSINFO', 'Y', 'Product', 'ARCH_PO_LINE_WMPROCESSINFO');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'PURCHASE_ORDERS', 'Y', 'Product', 'ARCH_PURCHASE_ORDERS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'PURCHASE_ORDERS_ATTRIBUTE', 'Y', 'Product', 'ARCH_PURCHASE_ORDERS_ATTRIBUTE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'PURCHASE_ORDERS_LINE_ITEM', 'Y', 'Product', 'ARCH_PURCHASE_ORDERS_LINE_ITEM');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'PURCHASE_ORDER_BP', 'Y', 'Product', 'ARCH_PURCHASE_ORDER_BP');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'REMIT_ADV', 'Y', 'Product', 'ARCH_REMIT_ADV');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'RESOURCE_OPTION', 'Y', 'Product', 'ARCH_RESOURCE_OPTION');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'RLM_RETURN_ORDERS', 'Y', 'Product', 'ARCH_RLM_RETURN_ORDERS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'RTS_LINE_ITEM', 'Y', 'Product', 'ARCH_RTS_LINE_ITEM');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SAILING_SCHDL_BY_DATE', 'Y', 'Product', 'ARCH_SAILING_SCHDL_BY_DATE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SAILING_SCHDL_BY_WEEK_DAY', 'Y', 'Product', 'ARCH_SAILING_SCHDL_BY_WEEK_DAY');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SHIPMENT', 'Y', 'Product', 'ARCH_SHIPMENT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SHIPMENT_ATTRIBUTE', 'Y', 'Product', 'ARCH_SHIPMENT_ATTRIBUTE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SHIPMENT_EVENT', 'Y', 'Product', 'ARCH_SHIPMENT_EVENT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SHIPMENT_EXCL_ACCESSORIAL', 'Y', 'Product', 'ARCH_SHIPMENT_EXCL_ACCESSORIAL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SHIPMENT_NOTE', 'Y', 'Product', 'ARCH_SHIPMENT_NOTE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SHIPMENT_SEAL_NUMBER', 'Y', 'Product', 'ARCH_SHIPMENT_SEAL_NUMBER');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SHIPMENT_SIZE_OVERRIDE', 'Y', 'Product', 'ARCH_SHIPMENT_SIZE_OVERRIDE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SHP_RS_OPTION', 'Y', 'Product', 'ARCH_SHP_RS_OPTION');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SPLITTER_ORDER_LI_EXCPTN', 'Y', 'Product', 'ARCH_SPLITTER_ORDER_LI_EXCPTN');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'STOP_ACTION_SHIPMENT', 'Y', 'Product', 'ARCH_STOP_ACTION_SHIPMENT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'STOP_COMMODITY', 'Y', 'Product', 'ARCH_STOP_COMMODITY');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'STOP_SIZE_TEMPLATE', 'Y', 'Product', 'ARCH_STOP_SIZE_TEMPLATE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'TRANS_FORECAST', 'Y', 'Product', 'ARCH_TRANS_FORECAST');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'TRIP', 'Y', 'Product', 'ARCH_TRIP');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'TRIP_ATTRIBUTE', 'Y', 'Product', 'ARCH_TRIP_ATTRIBUTE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'BOOKING_EVENT', 'Y', 'Product', 'ARCH_BOOKING_EVENT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_PAYMENT_DETAIL', 'Y', 'Product', 'ARCH_A_PAYMENT_DETAIL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_ALLOC_FAILED_DETAILS', 'Y', 'Product', 'ARCH_A_ALLOC_FAILED_DETAILS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_INVOICE', 'Y', 'Product', 'ARCH_A_INVOICE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ISF_SHIPMENT', 'Y', 'Product', 'ARCH_ISF_SHIPMENT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDER_ATTRIBUTE_TEMPLATE', 'Y', 'Product', 'ARCH_ORDER_ATTRIBUTE_TEMPLATE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDER_SCHED_ACTION', 'Y', 'Product', 'ARCH_ORDER_SCHED_ACTION');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDER_SRL_NBR_TEMPLATE', 'Y', 'Product', 'ARCH_ORDER_SRL_NBR_TEMPLATE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SPLITTER_ORDERS_SMALL', 'Y', 'Product', 'ARCH_SPLITTER_ORDERS_SMALL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'STOP_SCHED_ACTION', 'Y', 'Product', 'ARCH_STOP_SCHED_ACTION');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ASN_DETAIL_ATTRIBUTE', 'Y', 'Product', 'ARCH_ASN_DETAIL_ATTRIBUTE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'PO_LINE_ITEM_ATTRIBUTE', 'Y', 'Product', 'ARCH_PO_LINE_ITEM_ATTRIBUTE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'STORE_SHIPMENT', 'Y', 'Product', 'ARCH_STORE_SHIPMENT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'STORE_USER_ACTIVITY', 'Y', 'Product', 'ARCH_STORE_USER_ACTIVITY');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'POD_COMMANDS', 'Y', 'Product', 'ARCH_POD_COMMANDS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'POD_INTERACTION_PARAMETER', 'Y', 'Product', 'ARCH_POD_INTERACTION_PARAMETER');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ALERT_NOTIFICATION_EVENTS', 'Y', 'Product', 'ARCH_ALERT_NOTIFICATION_EVENTS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORPHANED_ORDER_REASONS', 'Y', 'Product', 'ARCH_ORPHANED_ORDER_REASONS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'TRAILER_VISIT_DETAIL', 'Y', 'Product', 'ARCH_TRAILER_VISIT_DETAIL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_ORDERINVENTORYALLOCATION', 'Y', 'Product', 'ARCH_A_ORDINVENTORYALLOCATION');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'INVOICE_ORDER_ACCOUNT_CODE', 'Y', 'Product', 'ARCH_INVOICE_ORD_ACCOUNT_CODE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'PO_LINE_CLASSIFICATION_CODE', 'Y', 'Product', 'ARCH_PO_LINE_CLASSIFICATION_CD');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'TRACKING_MSG_DRV_TRIP_ACTVTY', 'Y', 'Product', 'ARCH_TRACKING_MSG_DRV_TRIP_ACT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'BOOKING_DTL_EQUIP_INS_ORDERS', 'Y', 'Product', 'ARCH_BOOKING_DTL_EQUIP_INS_ORD');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'DO_LINE_CLASSIFICATION_CODE', 'Y', 'Product', 'ARCH_DO_LINE_CLASSIFICATION_CD');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'BOOKING_ORDER_LINE_ITEM_SIZE', 'Y', 'Product', 'ARCH_BOOKING_ORD_LINE_ITM_SIZE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SEGMENT_SHIPMENT_EQUIPMENT', 'Y', 'Product', 'ARCH_SEGMENT_SHIPMENT_EQUPT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ADM', 'Y', 'Product', 'ARCH_ADM');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ADM_DETAIL', 'Y', 'Product', 'ARCH_ADM_DETAIL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ADM_DETAIL_REASON', 'Y', 'Product', 'ARCH_ADM_DETAIL_REASON');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ALERT', 'Y', 'Product', 'ARCH_ALERT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'APPT_SLOT_UTILIZATION', 'Y', 'Product', 'ARCH_APPT_SLOT_UTILIZATION');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'AP_ALERT', 'Y', 'Product', 'ARCH_AP_ALERT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'AP_ALERT_ACTIVITY_DETAILS', 'Y', 'Product', 'ARCH_AP_ALERT_ACTIVITY_DETAILS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'AP_ALERT_ARGS', 'Y', 'Product', 'ARCH_AP_ALERT_ARGS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'AP_ALERT_ESCALATION_DETAILS', 'Y', 'Product', 'ARCH_AP_ALERT_ESCALATION_DTL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'AP_ALERT_OBJECTS', 'Y', 'Product', 'ARCH_AP_ALERT_OBJECTS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ASN', 'Y', 'Product', 'ARCH_ASN');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ASN_ATTRIBUTE', 'Y', 'Product', 'ARCH_ASN_ATTRIBUTE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ASN_DETAIL', 'Y', 'Product', 'ARCH_ASN_DETAIL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ASN_EVENT', 'Y', 'Product', 'ARCH_ASN_EVENT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ASN_INFO', 'Y', 'Product', 'ARCH_ASN_INFO');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ASN_NOTE', 'Y', 'Product', 'ARCH_ASN_NOTE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ASN_SEAL_NUMBER', 'Y', 'Product', 'ARCH_ASN_SEAL_NUMBER');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ASN_UNIT_DETAIL', 'Y', 'Product', 'ARCH_ASN_UNIT_DETAIL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_EMAIL_ADDRESS', 'Y', 'Product', 'ARCH_A_EMAIL_ADDRESS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_EMAIL_INFO', 'Y', 'Product', 'ARCH_A_EMAIL_INFO');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_LINES_FOR_ALLOC', 'Y', 'Product', 'ARCH_A_LINES_FOR_ALLOC');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_POS_CHARGE_DETAIL', 'Y', 'Product', 'ARCH_A_POS_CHARGE_DETAIL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_POS_DISCOUNT_DETAIL', 'Y', 'Product', 'ARCH_A_POS_DISCOUNT_DETAIL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_POS_NOTE', 'Y', 'Product', 'ARCH_A_POS_NOTE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_POS_PAYMENT_DETAIL', 'Y', 'Product', 'ARCH_A_POS_PAYMENT_DETAIL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_POS_REASON_CODE', 'Y', 'Product', 'ARCH_A_POS_REASON_CODE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_POS_TAX_DETAIL', 'Y', 'Product', 'ARCH_A_POS_TAX_DETAIL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_POS_TRANSACTION', 'Y', 'Product', 'ARCH_A_POS_TRANSACTION');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_POS_TRANSACTION_DETAIL', 'Y', 'Product', 'ARCH_A_POS_TRANSACTION_DETAIL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_PT_INVOICE_MAPPING', 'Y', 'Product', 'ARCH_A_PT_INVOICE_MAPPING');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_SOURCINGAUDITDETAIL', 'Y', 'Product', 'ARCH_A_SOURCINGAUDITDETAIL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'BOOKING_PURCHASE_ORDER', 'Y', 'Product', 'ARCH_BOOKING_PURCHASE_ORDER');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'CARRIER_CODE_RESPONSE', 'Y', 'Product', 'ARCH_CARRIER_CODE_RESPONSE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'CLAIMS', 'Y', 'Product', 'ARCH_CLAIMS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'CLAIM_COMMENTS', 'Y', 'Product', 'ARCH_CLAIM_COMMENTS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'CLAIM_ITEMS', 'Y', 'Product', 'ARCH_CLAIM_ITEMS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'CLAIM_JOURNAL_ENTRIES', 'Y', 'Product', 'ARCH_CLAIM_JOURNAL_ENTRIES');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'CLAIM_PAYMENTS', 'Y', 'Product', 'ARCH_CLAIM_PAYMENTS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'CLAIM_REASON_CODE', 'Y', 'Product', 'ARCH_CLAIM_REASON_CODE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'COGI', 'Y', 'Product', 'ARCH_COGI');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'COGI_DETAIL', 'Y', 'Product', 'ARCH_COGI_DETAIL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'COGI_POSTINGS', 'Y', 'Product', 'ARCH_COGI_POSTINGS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'COMB_LANE_DTL', 'Y', 'Product', 'ARCH_COMB_LANE_DTL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'CWS_SCENARIO_ORDERS', 'Y', 'Product', 'ARCH_CWS_SCENARIO_ORDERS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'DEADHEAD_STOPS', 'Y', 'Product', 'ARCH_DEADHEAD_STOPS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'DETENTION_COMMENTS', 'Y', 'Product', 'ARCH_DETENTION_COMMENTS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'DRIVER_BID', 'Y', 'Product', 'ARCH_DRIVER_BID');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'DRIVER_BID_SEGMENT_MAP', 'Y', 'Product', 'ARCH_DRIVER_BID_SEGMENT_MAP');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'DRIVER_SCHED_EXCEPTION', 'Y', 'Product', 'ARCH_DRIVER_SCHED_EXCEPTION');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'DSP_DRIVER_LOG_BASE', 'Y', 'Product', 'ARCH_DSP_DRIVER_LOG_BASE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'DSP_STATE_TAX_MILES', 'Y', 'Product', 'ARCH_DSP_STATE_TAX_MILES');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'DSP_TRANSACTION', 'Y', 'Product', 'ARCH_DSP_TRANSACTION');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ERROR_LOG_ARGS', 'Y', 'Product', 'ARCH_ERROR_LOG_ARGS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ILM_APPOINTMENTS', 'Y', 'Product', 'ARCH_ILM_APPOINTMENTS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ILM_APPOINTMENTS_CHARGEBACK', 'Y', 'Product', 'ARCH_ILM_APPT_CHARGEBACK');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ILM_APPOINTMENT_EVENTS', 'Y', 'Product', 'ARCH_ILM_APPOINTMENT_EVENTS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ILM_APPT_CUSTOM_ATTRIBUTE', 'Y', 'Product', 'ARCH_ILM_APPT_CUSTOM_ATTRIBUTE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ILM_APPT_EQUIPMENTS', 'Y', 'Product', 'ARCH_ILM_APPT_EQUIPMENTS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ILM_APPT_NOTES', 'Y', 'Product', 'ARCH_ILM_APPT_NOTES');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ILM_APPT_SLOTS', 'Y', 'Product', 'ARCH_ILM_APPT_SLOTS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ILM_TASK_EVENTS', 'Y', 'Product', 'ARCH_ILM_TASK_EVENTS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ILM_YARD_ACTIVITY', 'Y', 'Product', 'ARCH_ILM_YARD_ACTIVITY');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'INSPECTION', 'Y', 'Product', 'ARCH_INSPECTION');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'INSPECTION_OBJECT_DEFECT', 'Y', 'Product', 'ARCH_INSPECTION_OBJECT_DEFECT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'INVOICE', 'Y', 'Product', 'ARCH_INVOICE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'INVOICED_BKG_SHIPMENTS', 'Y', 'Product', 'ARCH_INVOICED_BKG_SHIPMENTS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'INVOICED_OBJ_ACCESSORIAL', 'Y', 'Product', 'ARCH_INVOICED_OBJ_ACCESSORIAL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'INVOICE_ATTRIBUTE', 'Y', 'Product', 'ARCH_INVOICE_ATTRIBUTE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'INVOICE_BOL', 'Y', 'Product', 'ARCH_INVOICE_BOL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'INVOICE_NOTE', 'Y', 'Product', 'ARCH_INVOICE_NOTE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'INVOICE_ORDER_LINE_ITEM', 'Y', 'Product', 'ARCH_INVOICE_ORDER_LINE_ITEM');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'INV_BKG_ACCESSORIAL', 'Y', 'Product', 'ARCH_INV_BKG_ACCESSORIAL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'LANE_ACCESSORIAL', 'Y', 'Product', 'ARCH_LANE_ACCESSORIAL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'LPN', 'Y', 'Product', 'ARCH_LPN');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'LPN_ADDRESS', 'Y', 'Product', 'ARCH_LPN_ADDRESS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'LPN_DETAIL', 'Y', 'Product', 'ARCH_LPN_DETAIL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'LPN_EVENT', 'Y', 'Product', 'ARCH_LPN_EVENT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'LPN_MOVEMENT', 'Y', 'Product', 'ARCH_LPN_MOVEMENT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'MANIFESTED_LPN', 'Y', 'Product', 'ARCH_MANIFESTED_LPN');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDERS', 'Y', 'Product', 'ARCH_ORDERS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDERS_TEMPLATE_RUN', 'Y', 'Product', 'ARCH_ORDERS_TEMPLATE_RUN');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDER_ACCESSORIAL_OPTION_GRP', 'Y', 'Product', 'ARCH_ORDER_ACCESSORIAL_OPT_GRP');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDER_BASELINE_COST', 'Y', 'Product', 'ARCH_ORDER_BASELINE_COST');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDER_LINE_ITEM', 'Y', 'Product', 'ARCH_ORDER_LINE_ITEM');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDER_LINE_ITEM_SIZE', 'Y', 'Product', 'ARCH_ORDER_LINE_ITEM_SIZE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDER_MOVEMENT', 'Y', 'Product', 'ARCH_ORDER_MOVEMENT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDER_NOTE', 'Y', 'Product', 'ARCH_ORDER_NOTE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDER_SPLIT_SIZE', 'Y', 'Product', 'ARCH_ORDER_SPLIT_SIZE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDER_WORK_TYPE', 'Y', 'Product', 'ARCH_ORDER_WORK_TYPE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'PO_LINE_ITEM_REF_FIELDS', 'Y', 'Product', 'ARCH_PO_LINE_ITEM_REF_FIELDS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'PO_REF_FIELDS', 'Y', 'Product', 'ARCH_PO_REF_FIELDS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'PO_WMPROCESSINFO', 'Y', 'Product', 'ARCH_PO_WMPROCESSINFO');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'PURCHASE_ORDERS_EVENT', 'Y', 'Product', 'ARCH_PURCHASE_ORDERS_EVENT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'PURCHASE_ORDERS_EXTN_TLM', 'Y', 'Product', 'ARCH_PURCHASE_ORDERS_EXTN_TLM');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'PURCHASE_ORDERS_NOTE', 'Y', 'Product', 'ARCH_PURCHASE_ORDERS_NOTE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'RATING_LANE_DTL_RATE', 'Y', 'Product', 'ARCH_RATING_LANE_DTL_RATE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'REMIT_ADV_DETAIL', 'Y', 'Product', 'ARCH_REMIT_ADV_DETAIL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'RLM_PACKAGES', 'Y', 'Product', 'ARCH_RLM_PACKAGES');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'RLM_RETURN_ORDERS_LINE_ITEM', 'Y', 'Product', 'ARCH_RLM_RETURN_ORD_LINE_ITEM');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'RTS', 'Y', 'Product', 'ARCH_RTS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'RTS_EVENT', 'Y', 'Product', 'ARCH_RTS_EVENT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'RTS_SIZES', 'Y', 'Product', 'ARCH_RTS_SIZES');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SAILING_SCHDL_BY_WEEK', 'Y', 'Product', 'ARCH_SAILING_SCHDL_BY_WEEK');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SEGMENT_ASSIGNMENT', 'Y', 'Product', 'ARCH_SEGMENT_ASSIGNMENT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SEGMENT_ATTRIBUTE', 'Y', 'Product', 'ARCH_SEGMENT_ATTRIBUTE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SHIPMENT_ACCESSORIAL', 'Y', 'Product', 'ARCH_SHIPMENT_ACCESSORIAL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SHIPMENT_COMMODITY', 'Y', 'Product', 'ARCH_SHIPMENT_COMMODITY');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SHIPMENT_EXTN_TLM', 'Y', 'Product', 'ARCH_SHIPMENT_EXTN_TLM');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SHIPMENT_LOAD_DETAIL', 'Y', 'Product', 'ARCH_SHIPMENT_LOAD_DETAIL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SHIPMENT_SCHED_ACTION', 'Y', 'Product', 'ARCH_SHIPMENT_SCHED_ACTION');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SHIPMENT_SIZE', 'Y', 'Product', 'ARCH_SHIPMENT_SIZE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SHIPMENT_TEMPLATE', 'Y', 'Product', 'ARCH_SHIPMENT_TEMPLATE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SHIPMENT_TRACKING', 'Y', 'Product', 'ARCH_SHIPMENT_TRACKING');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'STOP', 'Y', 'Product', 'ARCH_STOP');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'STOP_ACTION', 'Y', 'Product', 'ARCH_STOP_ACTION');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'STOP_ACTION_ORDER', 'Y', 'Product', 'ARCH_STOP_ACTION_ORDER');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'STOP_ADDITIONAL_SIZE', 'Y', 'Product', 'ARCH_STOP_ADDITIONAL_SIZE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'STOP_SIZE', 'Y', 'Product', 'ARCH_STOP_SIZE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SURGE_CAPACITY', 'Y', 'Product', 'ARCH_SURGE_CAPACITY');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'TRACKING_MESSAGE', 'Y', 'Product', 'ARCH_TRACKING_MESSAGE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'TRCK_MSG_COLL_ACT', 'Y', 'Product', 'ARCH_TRCK_MSG_COLL_ACT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'BOOKING_DTL_EQPMNT', 'Y', 'Product', 'ARCH_BOOKING_DTL_EQPMNT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_CHARGE_DETAIL', 'Y', 'Product', 'ARCH_A_CHARGE_DETAIL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'CONS_RUN_MONITOR', 'Y', 'Product', 'ARCH_CONS_RUN_MONITOR');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_CO_NOTE', 'Y', 'Product', 'ARCH_A_CO_NOTE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_DISCOUNT_DETAIL', 'Y', 'Product', 'ARCH_A_DISCOUNT_DETAIL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_INVOICE_LINE', 'Y', 'Product', 'ARCH_A_INVOICE_LINE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_PAYMENT_TRANSACTION', 'Y', 'Product', 'ARCH_A_PAYMENT_TRANSACTION');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_PO_LINE_DOM_EXT', 'Y', 'Product', 'ARCH_A_PO_LINE_DOM_EXT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_TAX_DETAIL', 'Y', 'Product', 'ARCH_A_TAX_DETAIL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'A_UNRELEASABLE_ALLOC', 'Y', 'Product', 'ARCH_A_UNRELEASABLE_ALLOC');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'BOOKING', 'Y', 'Product', 'ARCH_BOOKING');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'BOOKING_ACCESSORIAL', 'Y', 'Product', 'ARCH_BOOKING_ACCESSORIAL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'BOOKING_ORDERS', 'Y', 'Product', 'ARCH_BOOKING_ORDERS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'BOOKING_ORDER_LINE_ITEM', 'Y', 'Product', 'ARCH_BOOKING_ORDER_LINE_ITEM');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'BOOKING_RTS', 'Y', 'Product', 'ARCH_BOOKING_RTS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'BOOKING_RTS_LINE_ITEM', 'Y', 'Product', 'ARCH_BOOKING_RTS_LINE_ITEM');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'BOOKING_SHIPMENT', 'Y', 'Product', 'ARCH_BOOKING_SHIPMENT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'CONS_AGGREGATION', 'Y', 'Product', 'ARCH_CONS_AGGREGATION');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'CONS_AGGREGATION_MAP', 'Y', 'Product', 'ARCH_CONS_AGGREGATION_MAP');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'CONS_RUN', 'Y', 'Product', 'ARCH_CONS_RUN');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'CONS_RUN_DTL', 'Y', 'Product', 'ARCH_CONS_RUN_DTL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'CONS_RUN_DTL_ABOVE4', 'Y', 'Product', 'ARCH_CONS_RUN_DTL_ABOVE4');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'CONTINUOUS_MOVE', 'Y', 'Product', 'ARCH_CONTINUOUS_MOVE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'CUSTOM_ACCOUNT_CODE', 'Y', 'Product', 'ARCH_CUSTOM_ACCOUNT_CODE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'DOCUMENT_MANAGEMENT', 'Y', 'Product', 'ARCH_DOCUMENT_MANAGEMENT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'DO_LINE_WMPROCESSINFO', 'Y', 'Product', 'ARCH_DO_LINE_WMPROCESSINFO');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'DO_WMPROCESSINFO', 'Y', 'Product', 'ARCH_DO_WMPROCESSINFO');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'EEM_MANIFESTED_LPN', 'Y', 'Product', 'ARCH_EEM_MANIFESTED_LPN');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'EEM_MANIFEST_HDR', 'Y', 'Product', 'ARCH_EEM_MANIFEST_HDR');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'EM_LINK_OBJECT_SCHED', 'Y', 'Product', 'ARCH_EM_LINK_OBJECT_SCHED');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'EM_OBJECT_RULE', 'Y', 'Product', 'ARCH_EM_OBJECT_RULE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'EM_OBJECT_SCHEDULE', 'Y', 'Product', 'ARCH_EM_OBJECT_SCHEDULE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'EM_OBJECT_SCHEDULE_EVENT', 'Y', 'Product', 'ARCH_EM_OBJECT_SCHEDULE_EVENT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ISF_AUDIT', 'Y', 'Product', 'ARCH_ISF_AUDIT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ISF_BOL', 'Y', 'Product', 'ARCH_ISF_BOL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ISF_BOL_ATTR', 'Y', 'Product', 'ARCH_ISF_BOL_ATTR');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ISF_BOL_LINE', 'Y', 'Product', 'ARCH_ISF_BOL_LINE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ISF_BOL_LINE_ATTR', 'Y', 'Product', 'ARCH_ISF_BOL_LINE_ATTR');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ISF_BOND_HOLDER', 'Y', 'Product', 'ARCH_ISF_BOND_HOLDER');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ISF_COMMODITY', 'Y', 'Product', 'ARCH_ISF_COMMODITY');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ISF_FILER_IDENTIFICATION', 'Y', 'Product', 'ARCH_ISF_FILER_IDENTIFICATION');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ISF_HEADER', 'Y', 'Product', 'ARCH_ISF_HEADER');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'LETTER_OF_INSTRUCTION', 'Y', 'Product', 'ARCH_LETTER_OF_INSTRUCTION');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDER_REASON_CODE', 'Y', 'Product', 'ARCH_ORDER_REASON_CODE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDER_SIZES', 'Y', 'Product', 'ARCH_ORDER_SIZES');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDER_TEMPLATE_OD_PAIR', 'Y', 'Product', 'ARCH_ORDER_TEMPLATE_OD_PAIR');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'PO_NEGOTIATION_PROCESS', 'Y', 'Product', 'ARCH_PO_NEGOTIATION_PROCESS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'SPLITTER_RUN_STATS', 'Y', 'Product', 'ARCH_SPLITTER_RUN_STATS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'FEDEX_TRAN', 'Y', 'Product', 'ARCH_FEDEX_TRAN');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'PURCHASE_ORDER_LINE_COMPONENTS', 'Y', 'Product', 'ARCH_PURCHASE_ORDER_LINE_COMPT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ARTIFACT_EXTERNAL_USERS', 'Y', 'Product', 'ARCH_ARTIFACT_EXTERNAL_USERS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'PURCHASE_ORDER_BP_CONTACT', 'Y', 'Product', 'ARCH_PURCHASE_ORDER_BP_CONTACT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'BOOKING_EQUIPMENT_INSTANCE', 'Y', 'Product', 'ARCH_BOOKING_EQUPT_INSTANCE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'BOOKING_RTS_LINE_ITEM_SIZE', 'Y', 'Product', 'ARCH_BOOKING_RTS_LINE_ITM_SIZE');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'DO_LINE_ALLOCATION', 'Y', 'Product', 'ARCH_DO_LINE_ALLOCATION');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ARTIFACT', 'Y', 'Product', 'ARCH_ARTIFACT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'DRIVER_SHIPMENT', 'Y', 'Product', 'ARCH_DRIVER_SHIPMENT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'POD_UNIT_OF_DELIVERY', 'Y', 'Product', 'ARCH_POD_UNIT_OF_DELIVERY');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'POD_INTERACTION', 'Y', 'Product', 'ARCH_POD_INTERACTION');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'DOCK_DOOR_REF', 'Y', 'Product', 'ARCH_DOCK_DOOR_REF');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'LPN_CATCH_WEIGHT', 'Y', 'Product', 'ARCH_LPN_CATCH_WEIGHT');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'TRAILER_CONTENTS', 'Y', 'Product', 'ARCH_TRAILER_CONTENTS');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'LPN_ACCESSORIAL', 'Y', 'Product', 'ARCH_LPN_ACCESSORIAL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'ORDER_SPLIT_LINE_ITEM', 'Y', 'Product', 'ARCH_ORDER_SPLIT_LINE_ITEM');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'TPE_STOP_EXTN', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'DRIVER_EXTENSION', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'TRACKING_MESSAGE_COMMODITY', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'TRACKING_MSG_BKNG_RESPONSE', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_ACTIVITY_1', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_ACTIVITY_2', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_ACTIVITY_3', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_ACTIVITY_4', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_ACTIVITY_5', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_ACTIVITY_6', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_ACTIVITY_7', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_ACTIVITY_8', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_ACTIVITY_9', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_ACTIVITY_10', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_ACTIVITY_11', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_ACTIVITY_12', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_ACTIVITY_13', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_ACTIVITY_14', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_ACTIVITY_15', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_ACTIVITY_16', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_ACTIVITY_17', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_ACTIVITY_18', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_ACTIVITY_19', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_ACTIVITY_20', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_1', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_2', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_3', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_4', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_5', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_6', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_7', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_8', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_9', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_10', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_11', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_12', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_13', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_14', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_15', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_16', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_17', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_18', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_19', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_20', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_ASSOCIATED_AVAILABILITY', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_EXCLUSION_1', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_EXCLUSION_2', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_EXCLUSION_3', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_EXCLUSION_4', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_EXCLUSION_5', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_EXCLUSION_6', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_EXCLUSION_7', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_EXCLUSION_8', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_EXCLUSION_9', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_EXCLUSION_10', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_EXCLUSION_11', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_EXCLUSION_12', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_EXCLUSION_13', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_EXCLUSION_14', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_EXCLUSION_15', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_EXCLUSION_16', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_EXCLUSION_17', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_EXCLUSION_18', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_EXCLUSION_19', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAILABILITY_EXCLUSION_20', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_EVENT_STATUS_1', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_EVENT_STATUS_2', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_EVENT_STATUS_3', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_EVENT_STATUS_4', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_EVENT_STATUS_5', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_EVENT_STATUS_6', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_EVENT_STATUS_7', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_EVENT_STATUS_8', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_EVENT_STATUS_9', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_EVENT_STATUS_10', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_EVENT_STATUS_11', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_EVENT_STATUS_12', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_EVENT_STATUS_13', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_EVENT_STATUS_14', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_EVENT_STATUS_15', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_EVENT_STATUS_16', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_EVENT_STATUS_17', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_EVENT_STATUS_18', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_EVENT_STATUS_19', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_EVENT_STATUS_20', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_BY_INV_1', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_BY_INV_2', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_BY_INV_3', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_BY_INV_4', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_BY_INV_5', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_BY_INV_6', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_BY_INV_7', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_BY_INV_8', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_BY_INV_9', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_BY_INV_10', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_BY_INV_11', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_BY_INV_12', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_BY_INV_13', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_BY_INV_14', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_BY_INV_15', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_BY_INV_16', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_BY_INV_17', 'N', 'Product');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE)
 Values
   ('CA', 'I_AVAIL_BY_INV_18', 'N', 'Product');
COMMIT;

-- DBTicket DB-6674 PLSQL
@@DBScripts/Product/PLSQL_Objects/ca_archive_pkg.sql
@@DBScripts/Product/PLSQL_Objects/order_data_purge_pkg.sql

-- DBTicket DB-6796

Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'VIRTUAL_TRIP', 'Y', 'Product', 'ARCH_VIRTUAL_TRIP');

EXEC QUIET_DROP ('TABLE','ARCH_PURGE_PLAN_TABLE');

CREATE TABLE ARCH_PURGE_PLAN_TABLE
(
   SHIPMENT_ID          NUMBER (10),
   CMID                 NUMBER (15),
   INV_ID               NUMBER (8),
   CLAIM_ID             NUMBER (12),
   ASN_ID               NUMBER (10),
   MANIFEST_ID          NUMBER (9),
   BOOKING_ID           NUMBER (8),
   SEGMENT_ID           NUMBER (8),
   BID_ID               NUMBER (8),
   TRIP_ID              NUMBER (12),
   APPOINTMENT_ID       NUMBER (9),
   STOP_ID              NUMBER (8),
   CONS_RUN_ID          NUMBER (12),
   ORDER_ID             NUMBER (10),
   LPN_ID               NUMBER (10),
   PURCHASE_ORDERS_ID   NUMBER (10),
   RTS_ID               NUMBER (10),
   PO_INVOICE           NUMBER (8),
   PO_APPOINTMENT       NUMBER (9),
   CHARGEBACK_ID        NUMBER (9),
   INSPECTION_ID        NUMBER (9),
   ADM_ID               NUMBER (9),
   COGI_ID              NUMBER (9),
   RA_ID                NUMBER (9),
   MANIFESTED_LPN_ID    NUMBER (9),
   VIRTUAL_TRIP_ID      NUMBER (10),
   IS_ARCHIVABLE        NUMBER,
   REASON_DESC          VARCHAR2 (200)
)
TABLESPACE CBO_BASE_DT_TBS;

-- DBTicket DB-6835 PLSQL
@@DBScripts/Product/PLSQL_Objects/ca_archive_pkg.sql

-- DBTicket DB-6401

EXEC quiet_drop('TABLE','ARCHIVE_STATUS');

CREATE TABLE ARCHIVE_STATUS
(
   ARCH_BEGIN_DTTM   DATE DEFAULT SYSDATE NOT NULL,
   ARCH_END_DTTM     DATE,
   TC_COMPANY_ID     NUMBER (9),
   ARCH_RUN_STATUS   VARCHAR2 (40 CHAR) DEFAULT 'Running' NOT NULL
)
TABLESPACE CBO_BASE_DT_TBS;

EXEC quiet_drop('TABLE','ARCHIVE_RUN_LOG');

CREATE TABLE ARCHIVE_RUN_LOG
(
   ARCHIVE_DTTM     DATE DEFAULT SYSDATE,
   TC_COMPANY_ID    NUMBER (9),
   PURGE_GROUP      VARCHAR2 (10),
   PURGE_DAYS       NUMBER (10),
   PROCEDURE_NAME   VARCHAR2 (100 CHAR),
   RECORD_COUNT     NUMBER (20),
   RUN_TIME         NUMBER (13, 2),
   BUCKET_ID        NUMBER
)
TABLESPACE CBO_BASE_DT_TBS;

EXEC quiet_drop('TABLE','PURGE_CRITERIA');

CREATE TABLE PURGE_CRITERIA
(
   PURGE_MODULE   VARCHAR2 (10 CHAR) NOT NULL,
   PURGE_GROUP     VARCHAR2 (20 CHAR) NOT NULL,
   PURGE_STATUS     VARCHAR2 (100 CHAR),
   NO_OF_DAYS     NUMBER (4) NOT NULL
)
TABLESPACE CBO_STAT_DT_TBS;

CREATE UNIQUE INDEX PURGE_CRITERIA_IDX
   ON PURGE_CRITERIA (PURGE_MODULE, PURGE_GROUP)
   LOGGING
   TABLESPACE CBO_BASE_IDX_TBS;
   
INSERT INTO purge_criteria VALUES ('OLM','INVENTORY_BUSS',NULL,9999);
INSERT INTO purge_criteria VALUES ('OLM','INVENTORY_COMM',NULL,9999);
INSERT INTO purge_criteria VALUES ('OLM','INVENTORY_INV',NULL,9999);
INSERT INTO purge_criteria VALUES ('OLM','INVENTORY_STAG',NULL,9999);
INSERT INTO purge_criteria VALUES ('OLM','INVENTORY_LOG',NULL,9999);
INSERT INTO purge_criteria VALUES ('OLM','INVENTORY_COBASE',NULL,9999);
INSERT INTO purge_criteria VALUES ('OLM','EVENT',NULL,9999);
INSERT INTO purge_criteria VALUES ('OLM','LOG',NULL,9999);
INSERT INTO purge_criteria VALUES ('OLM','DWM',NULL,9999);
INSERT INTO purge_criteria VALUES ('OLM','STATS_SNAPSHOT',NULL,9999);

COMMIT;   

-- DBTicket DB-6776 PLSQL
@@DBScripts/Product/PLSQL_Objects/ca_purge_pkg.sql
@@DBScripts/Product/PLSQL_Objects/ca_archive_pkg.sql

-- DBTicket DB-7614
INSERT INTO MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE) VALUES ('CA', 'I_AVAIL_BY_INV_1_PRV', 'N', 'Product');
INSERT INTO MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE) VALUES ('CA', 'I_AVAIL_BY_INV_2_PRV', 'N', 'Product');
INSERT INTO MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE) VALUES ('CA', 'I_AVAIL_BY_INV_3_PRV', 'N', 'Product');
INSERT INTO MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE) VALUES ('CA', 'I_AVAIL_BY_INV_4_PRV', 'N', 'Product');
INSERT INTO MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE) VALUES ('CA', 'I_AVAIL_BY_INV_5_PRV', 'N', 'Product');
INSERT INTO MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE) VALUES ('CA', 'I_AVAIL_BY_INV_6_PRV', 'N', 'Product');
INSERT INTO MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE) VALUES ('CA', 'I_AVAIL_BY_INV_7_PRV', 'N', 'Product');
INSERT INTO MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE) VALUES ('CA', 'I_AVAIL_BY_INV_8_PRV', 'N', 'Product');
INSERT INTO MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE) VALUES ('CA', 'I_AVAIL_BY_INV_9_PRV', 'N', 'Product');
INSERT INTO MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE) VALUES ('CA', 'I_AVAIL_BY_INV_10_PRV', 'N', 'Product');
INSERT INTO MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE) VALUES ('CA', 'I_AVAIL_BY_INV_11_PRV', 'N', 'Product');
INSERT INTO MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE) VALUES ('CA', 'I_AVAIL_BY_INV_12_PRV', 'N', 'Product');
INSERT INTO MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE) VALUES ('CA', 'I_AVAIL_BY_INV_13_PRV', 'N', 'Product');
INSERT INTO MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE) VALUES ('CA', 'I_AVAIL_BY_INV_14_PRV', 'N', 'Product');
INSERT INTO MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE) VALUES ('CA', 'I_AVAIL_BY_INV_15_PRV', 'N', 'Product');
INSERT INTO MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE) VALUES ('CA', 'I_AVAIL_BY_INV_16_PRV', 'N', 'Product');
INSERT INTO MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE) VALUES ('CA', 'I_AVAIL_BY_INV_17_PRV', 'N', 'Product');
INSERT INTO MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE) VALUES ('CA', 'I_AVAIL_BY_INV_18_PRV', 'N', 'Product');
INSERT INTO MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE) VALUES ('CA', 'I_AVAIL_BY_INV_19_PRV', 'N', 'Product');
INSERT INTO MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE) VALUES ('CA', 'I_AVAIL_BY_INV_20_PRV', 'N', 'Product');
INSERT INTO MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE) VALUES ('CA', 'BOOKING_STATUS', 'Y', 'Product');
INSERT INTO MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE) VALUES ('CA', 'CARRIER_RGLANE_REJECT', 'N', 'Product');
INSERT INTO MANH_PRODUCT_PURGING_TABLES (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE) VALUES ('CA', 'I_EXCLUSION', 'N', 'Product');
commit;

-- DBTicket DB-8237 PLSQL
@@DBScripts/Product/PLSQL_Objects/order_data_purge_pkg.sql

-- DBTicket DB-9107
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'STATIC_SCHEDULE_CAP_USED', 'Y', 'Product', 'ARCH_STATIC_SCHEDULE_CAP_USED');

Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 Values
   ('CA', 'CONS_RUN_STATIC_ROUTE', 'Y', 'Product', 'ARCH_CONS_RUN_STATIC_ROUTE');

Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 VALUES
   ('CA', 'DOCK_APPOINTMENT_SCHED_ACTION', 'Y', 'Product', 'ARCH_DOCK_APPT_SCHED_ACTION');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 VALUES
   ('CA', 'YARD_AUDIT_TASK', 'Y', 'Product', 'ARCH_YARD_AUDIT_TASK');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 VALUES
   ('CA', 'YARD_RECURRING_TASK', 'Y', 'Product', 'ARCH_YARD_RECURRING_TASK');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 VALUES
   ('CA', 'STORE_FULFILLMENT_CONCURRENCY', 'Y', 'Product', 'ARCH_STORE_FULFILL_CONCURRENCY');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 VALUES
   ('CA', 'ORDER_REVENUE_ACCESSORIAL', 'Y', 'Product', 'ARCH_ORDER_REVENUE_ACCESSORIAL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 VALUES
   ('CA', 'SHIPMENT_REVENUE_ACCESSORIAL', 'Y', 'Product', 'ARCH_SHIP_REVENUE_ACCESSORIAL');
Insert into MANH_PRODUCT_PURGING_TABLES
   (PRODUCT_NAME, TABLE_NAME, ARCHIVE_FLAG, MODULE, ARCHIVE_TABLE_NAME)
 VALUES
   ('CA', 'CAPTURE_CONDITION_CODE', 'Y', 'Product', 'ARCH_CAPTURE_CONDITION_CODE');

COMMIT;   

-- DBTicket DB-9188 PLSQL
@@DBScripts/Product/PLSQL_Objects/ca_archive_pkg.sql

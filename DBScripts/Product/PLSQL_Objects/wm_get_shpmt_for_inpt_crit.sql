create or replace procedure wm_get_shpmt_for_inpt_criteria
(
    p_lpn_id              in lpn.lpn_id%type,
    p_order_id            in orders.order_id%type,
    p_order_split_id      in order_split.order_split_id%type,
    p_ship_via            in ship_via.ship_via%type,
    p_date                in date,
    p_user_id             in varchar2,
    p_out_shipment_id     out shipment.shipment_id%type
)
as
    v_ship_via            ship_via.ship_via%type;
    v_order_id            orders.order_id%type;
    v_order_split_id      order_split.order_split_id%type;
    v_sched_ship_dttm     date;
    v_date                date := trunc(p_date);
    v_stat_route_ind      number(1);
    v_tc_company_id       company.company_id%type;
    v_is_static_retail_rte_flow number(1) ;
    v_carrier_id          carrier_code.carrier_id%type;
    v_carrier_code        carrier_code.carrier_code%type;
    v_mot_id              ship_via.mot_id%type;
    v_service_level_id    ship_via.service_level_id%type;
    v_sv_tc_company_id    ship_via.tc_company_id%type;
begin
    if (p_lpn_id is null and p_order_id is null and p_order_split_id is null)
    then
        raise_application_error(-20050, 'Unsupported program parameters.');
    end if;

    if (p_lpn_id is not null)
    then
        select l.order_id, l.ship_via, l.sched_ship_dttm, l.shipment_id,
            l.order_split_id, l.tc_company_id, 
            case when l.static_route_id is not null then 1 else 0 end
        into v_order_id, v_ship_via, v_sched_ship_dttm, p_out_shipment_id,
            v_order_split_id, v_tc_company_id, v_stat_route_ind
        from lpn l
        where l.lpn_id = p_lpn_id;
        
        if (p_out_shipment_id is null and v_order_split_id is not null)
        then
            select os.line_haul_ship_via, o.sched_pickup_dttm, os.shipment_id
            into v_ship_via, v_sched_ship_dttm, p_out_shipment_id
            from order_split os
            join orders o on o.order_id = os.order_id
            where os.order_split_id = v_order_split_id;
        elsif (p_out_shipment_id is null)
        then
            select o.line_haul_ship_via, o.sched_pickup_dttm, o.shipment_id
            into v_ship_via, v_sched_ship_dttm, p_out_shipment_id
            from orders o
            where o.order_id = v_order_id;
        end if;
    elsif (p_order_id is not null)
    then
        select o.order_id, o.line_haul_ship_via, o.sched_pickup_dttm, 
            o.shipment_id, o.tc_company_id
        into v_order_id, v_ship_via, v_sched_ship_dttm, p_out_shipment_id, v_tc_company_id
        from orders o
-- check: has_split = 0?
        where o.order_id = p_order_id;

        select case when exists
        (
            select 1
            from lpn l
            where l.order_id = p_order_id and l.static_route_id is not null
        ) then 1 else 0 end
        into v_stat_route_ind
        from dual;           
    else
        select os.order_id, os.line_haul_ship_via, o.sched_pickup_dttm, os.shipment_id
        into v_order_id, v_ship_via, v_sched_ship_dttm, p_out_shipment_id
        from order_split os
        join orders o on o.order_id = os.order_id
        where os.order_split_id = p_order_split_id;
    end if;

    select case when exists
    (
        select 1
        from company_parameter cp
        where cp.param_def_id = 'assign_static_route'
            and cp.tc_company_id =  v_tc_company_id
            and cp.param_value in ('20', '30', '40', '50')
    ) then 1 else 0 end
    into v_is_static_retail_rte_flow
    from dual;
        
    select case when exists
    (
        select 1
        from shipment s
        where s.shipment_id = p_out_shipment_id and s.shipment_closed_indicator = 0 
		and not exists ( select 1 from manifest_hdr mh where s.manifest_id = mh.manifest_id
        and mh.manifest_status_Id < 90 and mh.sched_pickup_date != v_date )
    ) then p_out_shipment_id else null end
    into p_out_shipment_id
    from dual;

    if (p_out_shipment_id is null
        or coalesce(v_ship_via, ' ') != coalesce(p_ship_via, ' ')
         or coalesce(v_sched_ship_dttm, v_date) != v_date)
    then
        select cc.carrier_id, cc.carrier_code, sv.mot_id, sv.service_level_id, sv.tc_company_id
        into v_carrier_id, v_carrier_code, v_mot_id, v_service_level_id, v_sv_tc_company_id
        from ship_via sv
        join carrier_code cc on cc.carrier_id = sv.carrier_id
        where sv.ship_via = p_ship_via;
        -- find an existing shpmt that matches the order found above
            if (v_is_static_retail_rte_flow = 1)
            then        
                with giv as
                (
                    select o.d_facility_id, o.o_facility_id,
                        o.billing_method, o.d_address_1, o.d_address_2, o.d_address_3,
                        o.d_state_prov, o.d_postal_code, o.d_county, o.d_country_code, d_city
                    from orders o
                    where o.order_id = v_order_id
                )
            select max(s.shipment_id)
                into p_out_shipment_id
                from shipment s
                join giv on 1 = 1
                left join manifest_hdr mh on mh.manifest_id = s.manifest_id
                where s.scheduled_pickup_dttm = v_date and s.assigned_ship_via = p_ship_via
                    and s.shipment_closed_indicator = 0 and s.lpn_assignment_stopped = 'N'
                    and s.is_cancelled = 0 and s.has_import_error = 0
                    and s.creation_type in (20, 40) and s.wms_status_code < 55
                    and s.o_facility_number	= giv.o_facility_id
                    and s.tc_company_id = v_sv_tc_company_id
                    and coalesce(s.billing_method,-1)	= coalesce(giv.billing_method, -1)
                    and coalesce(mh.manifest_status_id, 10) = 10
                    and
                    (
                        s.d_facility_number = giv.d_facility_id
                        or
                        (
                            giv.d_facility_id is null
                            and exists
                            (
                                select 1
                                from stop st
                                where st.shipment_id = s.shipment_id and st.stop_seq = 2
                                    and coalesce(st.address_1, ' ') = coalesce(giv.d_address_1, ' ')
                                    and coalesce(st.address_2, ' ') = coalesce(giv.d_address_2, ' ')
                                    and coalesce(st.address_3, ' ') = coalesce(giv.d_address_3, ' ')
                                    and coalesce(st.city, ' ') = coalesce(giv.d_city, ' ')
                                    and coalesce(st.state_prov, ' ') = coalesce(giv.d_state_prov, ' ')
                                    and coalesce(st.postal_code, ' ') = coalesce(giv.d_postal_code, ' ')
                                    and coalesce(st.county, ' ') = coalesce(giv.d_county, ' ')
                                     and coalesce(st.country_code, ' ') = coalesce(giv.d_country_code, ' ')
                            )
                        )
                    )
                    and 
                    case 
                    when (v_stat_route_ind = 0) and 
                    not exists
                    (
                        select 1
                        from lpn l 
                        where l.shipment_id = s.shipment_id
                            and l.static_route_id is not null
                    ) then 1
                    when (v_stat_route_ind = 1) and 
                    not exists
                    (
                        select 1
                        from lpn l 
                        where l.shipment_id = s.shipment_id
                            and l.static_route_id is null
                    )then 1
                    end = 1;
            else
                with giv as
                (
                    select o.d_facility_id, o.o_facility_id,
                        o.billing_method, o.d_address_1, o.d_address_2, o.d_address_3,
                        o.d_state_prov, o.d_postal_code, o.d_county, o.d_country_code, d_city
                    from orders o
                    where o.order_id = v_order_id
                )
            select max(s.shipment_id)
                into p_out_shipment_id
                from shipment s
                join giv on 1 = 1
                left join manifest_hdr mh on mh.manifest_id = s.manifest_id
                where s.scheduled_pickup_dttm = v_date and s.assigned_ship_via = p_ship_via
                    and s.shipment_closed_indicator = 0 and s.lpn_assignment_stopped = 'N'
                    and s.is_cancelled = 0 and s.has_import_error = 0
                    and s.creation_type in (20, 40) and s.wms_status_code < 55
                    and s.o_facility_number	= giv.o_facility_id
                    and s.tc_company_id = v_sv_tc_company_id
                    and coalesce(s.billing_method,-1)	= coalesce(giv.billing_method, -1)
                    and coalesce(mh.manifest_status_id, 10) = 10
                    and
                    (
                        s.d_facility_number = giv.d_facility_id
                        or
                        (
                            giv.d_facility_id is null
                            and exists
                            (
                                select 1
                                from stop st
                                where st.shipment_id = s.shipment_id and st.stop_seq = 2
                                    and coalesce(st.address_1, ' ') = coalesce(giv.d_address_1, ' ')
                                    and coalesce(st.address_2, ' ') = coalesce(giv.d_address_2, ' ')
                                    and coalesce(st.address_3, ' ') = coalesce(giv.d_address_3, ' ')
                                    and coalesce(st.city, ' ') = coalesce(giv.d_city, ' ')
                                    and coalesce(st.state_prov, ' ') = coalesce(giv.d_state_prov, ' ')
                                    and coalesce(st.postal_code, ' ') = coalesce(giv.d_postal_code, ' ')
                                    and coalesce(st.county, ' ') = coalesce(giv.d_county, ' ')
                                     and coalesce(st.country_code, ' ') = coalesce(giv.d_country_code, ' ')
                            )
                        )
                    ); 
            end if;

        if (p_out_shipment_id is null)
        then
                wm_auton_find_or_create_shpmt(v_date, p_ship_via, v_order_id,p_user_id, 
				    v_is_static_retail_rte_flow, v_stat_route_ind,v_carrier_id, v_carrier_code,
                    v_mot_id, v_service_level_id, v_sv_tc_company_id, p_out_shipment_id);
        end if;
    end if;
end;
/
show errors;
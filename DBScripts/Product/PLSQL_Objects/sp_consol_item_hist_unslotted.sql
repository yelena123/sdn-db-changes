create or replace procedure sp_consol_item_hist_unslotted (
   v_sku_id                 in     number,
   p_whse                   IN     locn_hdr.whse%type,
   v_slot_ship_unit         in     number,
   v_hist_match             in     varchar2,
   v_mult_loc_grp           in     varchar2,
   v_no_of_slot_item_recs   in out number,
   v_min_slotitem_id        in     number,
   v_errcode                in out number)
as
   v_i_ih_nextrowid                number (10, 0);
   v_i_ih_currowid                 number (10, 0);
   v_i_ih_loopctl                  number (1, 0);
   v_wh_ti                         number (10, 0);
   v_ven_ti                        number (10, 0);
   v_ord_ti                        number (10, 0);
   v_wh_hi                         number (10, 0);
   v_ven_hi                        number (10, 0);
   v_ord_hi                        number (10, 0);
   v_inn_per_cs                    number (10, 0);
   v_each_per_cs                   number (10, 0);
   v_each_per_inn                  number (10, 0);
   v_item_ship_unit                number (5, 0);
   v_start_date                    timestamp (3);
   v_end_date                      timestamp (3);
   v_pallete_pattern               number (10, 0);
   v_mov_pallet                    number (17, 2);
   v_mov_case                      number (17, 2);
   v_mov_each                      number (17, 2);
   v_mov_inner                     number (17, 2);
   v_inv_pallet                    number (17, 2);
   v_inv_case                      number (17, 2);
   v_inv_inner                     number (17, 2);
   v_inv_each                      number (17, 2);
   v_nbr_of_picks                  number (17, 2);
   v_no_of_item_hist_recs          number (10, 0);
   v_hist_id                       number (19, 0);
   v_recs_updtd_tot                number (10, 0);
   v_recs_updtd_tmp                number (10, 0);
   v_bis_slotitem_good_to_delete   number (1, 0);
   v_strsqlst                      varchar2 (1500);
   v_rc_del                        number (10, 0);
   swv_rowcount                    number (10, 0);
   swv_no_of_slot_item_recs        number (10, 0);
   v_if_exists                     number (10, 0);
   v_if_exists2                    number (10, 0);
begin
   swv_no_of_slot_item_recs := v_no_of_slot_item_recs;
   v_bis_slotitem_good_to_delete := 0;
   v_errcode := 0;

   select wh_hi,
          ven_hi,
          ord_hi,
          wh_ti,
          ven_ti,
          ord_ti,
          inn_per_cs,
          each_per_cs,
          each_per_inn
     into v_wh_hi,
          v_ven_hi,
          v_ord_hi,
          v_wh_ti,
          v_ven_ti,
          v_ord_ti,
          v_inn_per_cs,
          v_each_per_cs,
          v_each_per_inn
     from so_item_master
    where sku_id = v_sku_id and whse_code = p_whse;

   if (v_wh_ti is null and v_ven_ti is null and v_ord_ti is null
       or v_wh_hi is null and v_ven_hi is null and v_ord_hi is null)
   then
      v_errcode := 0;
      dbms_output.put_line (
         'error: item info is missing in item master. consolidation for this sku is skipped');
      goto handle_error;
   end if;

   v_wh_hi := nvl (v_wh_hi, 1);
   v_ven_hi := nvl (v_ven_hi, 1);
   v_ord_hi := nvl (v_ord_hi, 1);
   v_wh_ti := nvl (v_wh_ti, 1);
   v_ven_ti := nvl (v_ven_ti, 1);
   v_ord_ti := nvl (v_ord_ti, 1);

   execute immediate ' delete tt_temp_item_history_smry ';

   insert into tt_temp_item_history_smry (ship_unit,
                                          pallete_pattern,
                                          nbr_of_picks,
                                          mov1,
                                          mov2,
                                          mov4,
                                          mov8,
                                          inv1,
                                          inv2,
                                          inv4,
                                          inv8,
                                          start_date,
                                          end_date)
        select t.ship_unit as ship_unit,
               t.pallete_pattern as pallete_pattern,
               sum (t.nbr_of_picks) as nbr_of_picks,
               sum (t.mov1) as mov_pallets,
               sum (t.mov2) as mov_cases,
               sum (t.mov4) as mov_inners,
               sum (t.mov8) as mov_eaches,
               sum (t.inv1) as inv_pallets,
               sum (t.inv2) as inv_cases,
               sum (t.inv4) as inv_inners,
               sum (t.inv8) as inv_eaches,
               start_date as start_date,
               end_date as end_date
          from (  select ih.ship_unit,
                         si.pallete_pattern,
                         sum (nvl (ih.nbr_of_picks, 0)) nbr_of_picks,
                         nvl (
                            case ih.ship_unit
                               when 1 then sum (nvl (ih.movement, 0))
                            end,
                            0)
                            as mov1,
                         nvl (
                            case ih.ship_unit
                               when 2 then sum (nvl (ih.movement, 0))
                            end,
                            0)
                            as mov2,
                         nvl (
                            case ih.ship_unit
                               when 4 then sum (nvl (ih.movement, 0))
                            end,
                            0)
                            as mov4,
                         nvl (
                            case ih.ship_unit
                               when 8 then sum (nvl (ih.movement, 0))
                            end,
                            0)
                            as mov8,
                         nvl (
                            case ih.ship_unit
                               when 1 then sum (nvl (ih.inventory, 0))
                            end,
                            0)
                            as inv1,
                         nvl (
                            case ih.ship_unit
                               when 2 then sum (nvl (ih.inventory, 0))
                            end,
                            0)
                            as inv2,
                         nvl (
                            case ih.ship_unit
                               when 4 then sum (nvl (ih.inventory, 0))
                            end,
                            0)
                            as inv4,
                         nvl (
                            case ih.ship_unit
                               when 8 then sum (nvl (ih.inventory, 0))
                            end,
                            0)
                            as inv8,
                         ih.start_date as start_date,
                         ih.end_date as end_date
                    from    item_history ih
                         inner join
                            slot_item si
                         on si.slotitem_id = ih.slotitem_id
                         left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                   where     si.sku_id = v_sku_id
                         and si.ship_unit = v_slot_ship_unit
                         and si.delete_mult <> 1
                         and nvl (si.hist_match, '0x') = v_hist_match
                         and nvl (si.mult_loc_grp, '0x') = v_mult_loc_grp
                         and si.slot_id is null
                         and si.slotitem_id <> v_min_slotitem_id
                group by ih.ship_unit,
                         si.pallete_pattern,
                         ih.start_date,
                         ih.end_date) t
      group by t.ship_unit,
               t.pallete_pattern,
               t.start_date,
               t.end_date
      order by t.start_date nulls first, t.end_date nulls first;

   v_i_ih_loopctl := 1;

   select min (i_ih_rowid)
     into v_i_ih_nextrowid
     from tt_temp_item_history_smry;

   if (nvl (v_i_ih_nextrowid, 0) = 0)
   then
      dbms_output.put_line (
         'info: no data in item history table to consolidate for the selected sku group.');
      v_errcode := 0;
      v_bis_slotitem_good_to_delete := 1;

      goto last_call;
   end if;



   select i_ih_rowid,
          ship_unit,
          pallete_pattern,
          nbr_of_picks,
          mov1,
          mov2,
          mov4,
          mov8,
          inv1,
          inv2,
          inv4,
          inv8,
          start_date,
          end_date
     into v_i_ih_currowid,
          v_item_ship_unit,
          v_pallete_pattern,
          v_nbr_of_picks,
          v_mov_pallet,
          v_mov_case,
          v_mov_inner,
          v_mov_each,
          v_inv_pallet,
          v_inv_case,
          v_inv_inner,
          v_inv_each,
          v_start_date,
          v_end_date
     from tt_temp_item_history_smry
    where i_ih_rowid = v_i_ih_nextrowid;


   while v_i_ih_loopctl = 1
   loop
      select count (*)
        into v_if_exists
        from    item_history ih
             inner join
                slot_item si
             on si.slotitem_id = ih.slotitem_id
             left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
       where     si.sku_id = v_sku_id
             and si.ship_unit = v_slot_ship_unit
             and si.delete_mult <> 1
             and nvl (si.hist_match, '0x') = v_hist_match
             and nvl (si.mult_loc_grp, '0x') = v_mult_loc_grp
             and si.slot_id is null
             and si.slotitem_id = v_min_slotitem_id
             and ih.start_date = v_start_date
             and ih.end_date = v_end_date
             and rownum <= 1;


      if v_if_exists > 0
      then
         dbms_output.put_line ('info: item history exists for sku:');

         v_recs_updtd_tot := 0;
         v_recs_updtd_tmp := 0;

         v_no_of_item_hist_recs := 1;

         BEGIN
         UPDATE item_history ih
         SET ih.nbr_of_picks = ih.nbr_of_picks + (v_nbr_of_picks /v_no_of_item_hist_recs),
             ih.mod_date_time = SYSTIMESTAMP, ih. mod_user = 'consoldtd'
         where ih.slotitem_id in ( select slotitem_id 
                                   from slot_item si  
                                   LEFT JOIN slot s ON s.slot_id = si.slot_id AND s.whse_code = p_whse
                                   where  si.sku_id = v_sku_id
                                       AND si.ship_unit = v_slot_ship_unit
                                       AND si.delete_mult <> 1
                                       AND NVL (si.hist_match, '0x') = v_hist_match
                                       AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                                       AND si.slot_id IS NULL
                                       AND si.slotitem_id = v_min_slotitem_id
                                       AND ih.start_date =v_start_date
                                       AND ih.end_date = v_end_date);        
            
         EXCEPTION
            WHEN OTHERS
            THEN
               v_errcode := SQLCODE;
               GOTO handle_error;
         END;

         swv_rowcount := sql%rowcount;
         v_recs_updtd_tmp := swv_rowcount;


         v_recs_updtd_tot := v_recs_updtd_tmp + v_recs_updtd_tot;


         update item_history ih_upd
            set (movement,
                 inventory,
                 mod_date_time,
                 mod_user) =
                   (select case
                              when v_item_ship_unit = 1
                              then
                                 ih.movement
                                 + (v_mov_pallet / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 2
                                   and v_pallete_pattern = 0
                              then
                                 ih.movement
                                 + ( (v_mov_case / (v_wh_hi * v_wh_ti))
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 2
                                   and v_pallete_pattern = 1
                              then
                                 ih.movement
                                 + ( (v_mov_case / (v_ord_hi * v_ord_ti))
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 2
                                   and v_pallete_pattern = 2
                              then
                                 ih.movement
                                 + ( (v_mov_case / (v_ven_hi * v_ven_ti))
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 4
                                   and v_pallete_pattern = 0
                              then
                                 ih.movement
                                 + ( (v_mov_inner
                                      / (v_wh_hi * v_wh_ti * v_inn_per_cs))
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 4
                                   and v_pallete_pattern = 1
                              then
                                 ih.movement
                                 + ( (v_mov_inner
                                      / (v_ord_hi * v_ord_ti * v_inn_per_cs))
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 4
                                   and v_pallete_pattern = 2
                              then
                                 ih.movement
                                 + ( (v_mov_inner
                                      / (v_ven_hi * v_ven_ti * v_inn_per_cs))
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 8
                                   and v_pallete_pattern = 0
                              then
                                 ih.movement
                                 + ( (v_mov_each
                                      / (v_wh_hi * v_wh_ti * v_each_per_cs))
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 8
                                   and v_pallete_pattern = 1
                              then
                                 ih.movement
                                 + ( (v_mov_each
                                      / (v_ord_hi * v_ord_ti * v_each_per_cs))
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 8
                                   and v_pallete_pattern = 2
                              then
                                 ih.movement
                                 + ( (v_mov_each
                                      / (v_ven_hi * v_ven_ti * v_each_per_cs))
                                    / v_no_of_item_hist_recs)
                           end,
                           case
                              when v_item_ship_unit = 1
                              then
                                 ih.inventory
                                 + (v_inv_pallet / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 2
                                   and v_pallete_pattern = 0
                              then
                                 ih.inventory
                                 + ( (v_inv_case / (v_wh_hi * v_wh_ti))
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 2
                                   and v_pallete_pattern = 1
                              then
                                 ih.inventory
                                 + ( (v_inv_case / (v_ord_hi * v_ord_ti))
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 2
                                   and v_pallete_pattern = 2
                              then
                                 ih.inventory
                                 + ( (v_inv_case / (v_ven_hi * v_ven_ti))
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 4
                                   and v_pallete_pattern = 0
                              then
                                 ih.inventory
                                 + ( (v_inv_inner
                                      / (v_wh_hi * v_wh_ti * v_inn_per_cs))
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 4
                                   and v_pallete_pattern = 1
                              then
                                 ih.inventory
                                 + ( (v_inv_inner
                                      / (v_ord_hi * v_ord_ti * v_inn_per_cs))
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 4
                                   and v_pallete_pattern = 2
                              then
                                 ih.inventory
                                 + ( (v_inv_inner
                                      / (v_ven_hi * v_ven_ti * v_inn_per_cs))
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 8
                                   and v_pallete_pattern = 0
                              then
                                 ih.inventory
                                 + ( (v_inv_each
                                      / (v_wh_hi * v_wh_ti * v_each_per_cs))
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 8
                                   and v_pallete_pattern = 1
                              then
                                 ih.inventory
                                 + ( (v_inv_each
                                      / (v_ord_hi * v_ord_ti * v_each_per_cs))
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 8
                                   and v_pallete_pattern = 2
                              then
                                 ih.inventory
                                 + ( (v_inv_each
                                      / (v_ven_hi * v_ven_ti * v_each_per_cs))
                                    / v_no_of_item_hist_recs)
                           end,
                           systimestamp,
                           'consoldtd'
                      from    item_history ih
                           inner join
                              slot_item si
                           on si.slotitem_id = ih.slotitem_id
                           left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                     where (    si.sku_id = v_sku_id
                            and si.ship_unit = v_slot_ship_unit
                            and si.delete_mult <> 1
                            and nvl (si.hist_match, '0x') = v_hist_match
                            and nvl (si.mult_loc_grp, '0x') = v_mult_loc_grp
                            and si.slot_id is null
                            and si.slotitem_id = v_min_slotitem_id
                            and ih.start_date = v_start_date
                            and ih.end_date = v_end_date
                            and ih.ship_unit = 1)
                           and ih.rowid = ih_upd.rowid)
          where rowid in
                   (select ih.rowid
                      from    item_history ih
                           inner join
                              slot_item si
                           on si.slotitem_id = ih.slotitem_id
                           left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                     where     si.sku_id = v_sku_id
                           and si.ship_unit = v_slot_ship_unit
                           and si.delete_mult <> 1
                           and nvl (si.hist_match, '0x') = v_hist_match
                           and nvl (si.mult_loc_grp, '0x') = v_mult_loc_grp
                           and si.slot_id is null
                           and si.slotitem_id = v_min_slotitem_id
                           and ih.start_date = v_start_date
                           and ih.end_date = v_end_date
                           and ih.ship_unit = 1);

         swv_rowcount := sql%rowcount;
         v_recs_updtd_tmp := swv_rowcount;
         v_recs_updtd_tot := v_recs_updtd_tmp + v_recs_updtd_tot;

         update item_history ih_upd
            set (movement,
                 inventory,
                 mod_date_time,
                 mod_user) =
                   (select case
                              when v_item_ship_unit = 1
                                   and v_pallete_pattern = 0
                              then
                                 ih.movement
                                 + ( (v_mov_pallet * v_wh_hi * v_wh_ti)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 1
                                   and v_pallete_pattern = 1
                              then
                                 ih.movement
                                 + ( (v_mov_pallet * v_ord_hi * v_ord_ti)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 1
                                   and v_pallete_pattern = 2
                              then
                                 ih.movement
                                 + ( (v_mov_pallet * v_ven_hi * v_ven_ti)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 2
                              then
                                 ih.movement
                                 + (v_mov_case / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 4
                              then
                                 ih.movement
                                 + ( (v_mov_inner / v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 8
                              then
                                 ih.movement
                                 + ( (v_mov_each / v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                           end,
                           case
                              when v_item_ship_unit = 1
                                   and v_pallete_pattern = 0
                              then
                                 ih.inventory
                                 + ( (v_inv_pallet * v_wh_hi * v_wh_ti)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 1
                                   and v_pallete_pattern = 1
                              then
                                 ih.inventory
                                 + ( (v_inv_pallet * v_ord_hi * v_ord_ti)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 1
                                   and v_pallete_pattern = 2
                              then
                                 ih.inventory
                                 + ( (v_inv_pallet * v_ven_hi * v_ven_ti)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 2
                              then
                                 ih.inventory
                                 + (v_inv_case / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 4
                              then
                                 ih.inventory
                                 + ( (v_inv_inner / v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 8
                              then
                                 ih.inventory
                                 + ( (v_inv_each / v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                           end,
                           systimestamp,
                           'consoldtd'
                      from    item_history ih
                           inner join
                              slot_item si
                           on si.slotitem_id = ih.slotitem_id
                           left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                     where (    si.sku_id = v_sku_id
                            and si.ship_unit = v_slot_ship_unit
                            and si.delete_mult <> 1
                            and nvl (si.hist_match, '0x') = v_hist_match
                            and nvl (si.mult_loc_grp, '0x') = v_mult_loc_grp
                            and si.slotitem_id = v_min_slotitem_id
                            and ih.start_date = v_start_date
                            and ih.end_date = v_end_date
                            and ih.ship_unit = 2)
                           and ih.rowid = ih_upd.rowid)
          where rowid in
                   (select ih.rowid
                      from    item_history ih
                           inner join
                              slot_item si
                           on si.slotitem_id = ih.slotitem_id
                           left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                     where     si.sku_id = v_sku_id
                           and si.ship_unit = v_slot_ship_unit
                           and si.delete_mult <> 1
                           and nvl (si.hist_match, '0x') = v_hist_match
                           and nvl (si.mult_loc_grp, '0x') = v_mult_loc_grp
                           and si.slotitem_id = v_min_slotitem_id
                           and ih.start_date = v_start_date
                           and ih.end_date = v_end_date
                           and ih.ship_unit = 2);

         swv_rowcount := sql%rowcount;
         v_recs_updtd_tmp := swv_rowcount;
         v_recs_updtd_tot := v_recs_updtd_tmp + v_recs_updtd_tot;

         update item_history ih_upd
            set (movement,
                 inventory,
                 mod_date_time,
                 mod_user) =
                   (select case
                              when v_item_ship_unit = 1
                                   and v_pallete_pattern = 0
                              then
                                 ih.movement
                                 + ( (  v_mov_pallet
                                      * v_wh_hi
                                      * v_wh_ti
                                      * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 1
                                   and v_pallete_pattern = 1
                              then
                                 ih.movement
                                 + ( (  v_mov_pallet
                                      * v_ord_hi
                                      * v_ord_ti
                                      * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 1
                                   and v_pallete_pattern = 2
                              then
                                 ih.movement
                                 + ( (  v_mov_pallet
                                      * v_ven_hi
                                      * v_ven_ti
                                      * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 2
                              then
                                 ih.movement
                                 + ( (v_mov_case * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 4
                              then
                                 ih.movement
                                 + (v_mov_inner / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 8
                              then
                                 ih.movement
                                 + ( (v_mov_each / v_each_per_inn)
                                    / v_no_of_item_hist_recs)
                           end,
                           case
                              when v_item_ship_unit = 1
                                   and v_pallete_pattern = 0
                              then
                                 ih.inventory
                                 + ( (  v_inv_pallet
                                      * v_wh_hi
                                      * v_wh_ti
                                      * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 1
                                   and v_pallete_pattern = 1
                              then
                                 ih.inventory
                                 + ( (  v_inv_pallet
                                      * v_ord_hi
                                      * v_ord_ti
                                      * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 1
                                   and v_pallete_pattern = 2
                              then
                                 ih.inventory
                                 + ( (  v_inv_pallet
                                      * v_ven_hi
                                      * v_ven_ti
                                      * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 2
                              then
                                 ih.inventory
                                 + ( (v_inv_case * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 4
                              then
                                 ih.inventory
                                 + (v_inv_inner / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 8
                              then
                                 ih.inventory
                                 + ( (v_inv_each / v_each_per_inn)
                                    / v_no_of_item_hist_recs)
                           end,
                           systimestamp,
                           'consoldtd'
                      from    item_history ih
                           inner join
                              slot_item si
                           on si.slotitem_id = ih.slotitem_id
                           left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                     where (    si.sku_id = v_sku_id
                            and si.ship_unit = v_slot_ship_unit
                            and si.delete_mult <> 1
                            and nvl (si.hist_match, '0x') = v_hist_match
                            and nvl (si.mult_loc_grp, '0x') = v_mult_loc_grp
                            and si.slotitem_id = v_min_slotitem_id
                            and ih.start_date = v_start_date
                            and ih.end_date = v_end_date
                            and ih.ship_unit = 4)
                           and ih.rowid = ih_upd.rowid)
          where rowid in
                   (select ih.rowid
                      from    item_history ih
                           inner join
                              slot_item si
                           on si.slotitem_id = ih.slotitem_id
                           left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                     where     si.sku_id = v_sku_id
                           and si.ship_unit = v_slot_ship_unit
                           and si.delete_mult <> 1
                           and nvl (si.hist_match, '0x') = v_hist_match
                           and nvl (si.mult_loc_grp, '0x') = v_mult_loc_grp
                           and si.slotitem_id = v_min_slotitem_id
                           and ih.start_date = v_start_date
                           and ih.end_date = v_end_date
                           and ih.ship_unit = 4);

         swv_rowcount := sql%rowcount;
         v_recs_updtd_tmp := swv_rowcount;
         v_recs_updtd_tot := v_recs_updtd_tmp + v_recs_updtd_tot;

         update item_history ih_upd
            set (movement,
                 inventory,
                 mod_date_time,
                 mod_user) =
                   (select case
                              when v_item_ship_unit = 1
                                   and v_pallete_pattern = 0
                              then
                                 ih.movement
                                 + ( (  v_mov_pallet
                                      * v_wh_hi
                                      * v_wh_ti
                                      * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 1
                                   and v_pallete_pattern = 1
                              then
                                 ih.movement
                                 + ( (  v_mov_pallet
                                      * v_ord_hi
                                      * v_ord_ti
                                      * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 1
                                   and v_pallete_pattern = 2
                              then
                                 ih.movement
                                 + ( (  v_mov_pallet
                                      * v_ven_hi
                                      * v_ven_ti
                                      * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 2
                              then
                                 ih.movement
                                 + ( (v_mov_case * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 4
                              then
                                 ih.movement
                                 + ( (v_mov_inner * v_each_per_inn)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 8
                              then
                                 ih.movement
                                 + (v_mov_each / v_no_of_item_hist_recs)
                           end,
                           case
                              when v_item_ship_unit = 1
                                   and v_pallete_pattern = 0
                              then
                                 ih.inventory
                                 + ( (  v_inv_pallet
                                      * v_wh_hi
                                      * v_wh_ti
                                      * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 1
                                   and v_pallete_pattern = 1
                              then
                                 ih.inventory
                                 + ( (  v_inv_pallet
                                      * v_ord_hi
                                      * v_ord_ti
                                      * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 1
                                   and v_pallete_pattern = 2
                              then
                                 ih.inventory
                                 + ( (  v_inv_pallet
                                      * v_ven_hi
                                      * v_ven_ti
                                      * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 2
                              then
                                 ih.inventory
                                 + ( (v_inv_case * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 4
                              then
                                 ih.inventory
                                 + ( (v_inv_inner * v_each_per_inn)
                                    / v_no_of_item_hist_recs)
                              when v_item_ship_unit = 8
                              then
                                 ih.inventory
                                 + (v_inv_each / v_no_of_item_hist_recs)
                           end,
                           systimestamp,
                           'consoldtd'
                      from    item_history ih
                           inner join
                              slot_item si
                           on si.slotitem_id = ih.slotitem_id
                           left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                     where (    si.sku_id = v_sku_id
                            and si.ship_unit = v_slot_ship_unit
                            and si.delete_mult <> 1
                            and nvl (si.hist_match, '0x') = v_hist_match
                            and nvl (si.mult_loc_grp, '0x') = v_mult_loc_grp
                            and si.slotitem_id = v_min_slotitem_id
                            and ih.start_date = v_start_date
                            and ih.end_date = v_end_date
                            and ih.ship_unit = 8)
                           and ih.rowid = ih_upd.rowid)
          where rowid in
                   (select ih.rowid
                      from    item_history ih
                           inner join
                              slot_item si
                           on si.slotitem_id = ih.slotitem_id
                           left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                     where     si.sku_id = v_sku_id
                           and si.ship_unit = v_slot_ship_unit
                           and si.delete_mult <> 1
                           and nvl (si.hist_match, '0x') = v_hist_match
                           and nvl (si.mult_loc_grp, '0x') = v_mult_loc_grp
                           and si.slotitem_id = v_min_slotitem_id
                           and ih.start_date = v_start_date
                           and ih.end_date = v_end_date
                           and ih.ship_unit = 8);

         swv_rowcount := sql%rowcount;
         v_recs_updtd_tmp := swv_rowcount;
         v_recs_updtd_tot := v_recs_updtd_tmp + v_recs_updtd_tot;

         if (v_recs_updtd_tot > 0)
         then
            v_strsqlst :=
               'delete from item_history '
               || ' where hist_id in  ( select hist_id from item_history ih'
               || ' inner join slot_item si on si.slotitem_id = ih.slotitem_id left join slot s on s.slot_id = si.slot_id and s.whse_code = '''
               || p_whse  
               || ''' where si.sku_id = '
               || v_sku_id
               || ' and si.ship_unit ='
               || v_slot_ship_unit
               || ' and si.delete_mult <> '
               || 1
               || ' and nvl(si.hist_match,''0x'') = '''
               || v_hist_match
               || ''' and nvl(si.mult_loc_grp,''0x'') = '''
               || v_mult_loc_grp
               || ''' and si.slot_id is null and si.slotitem_id  <> '
               || v_min_slotitem_id
               || ' and ih.start_date = '''
               || v_start_date
               || ''' and ih.end_date = '''
               || v_end_date
               || ''' and ih.ship_unit = '''
               || v_item_ship_unit
               || ''')';

            begin
               execute immediate v_strsqlst;
            exception
               when others
               then
                  v_errcode := sqlcode;
                  goto handle_error;
            end;

            v_bis_slotitem_good_to_delete := 1;
         else
            dbms_output.put_line (
               'error: oops!!! for some reasons none of the item history records are updated....rolling back transaction');
            v_errcode := -1;
            goto handle_error;
         end if;
      end if;

      v_i_ih_nextrowid := null;

      select nvl (min (i_ih_rowid), 0)
        into v_i_ih_nextrowid
        from tt_temp_item_history_smry
       where i_ih_rowid > v_i_ih_currowid;


      if nvl (v_i_ih_nextrowid, 0) = 0
      then
         exit;
      end if;


      select i_ih_rowid,
             ship_unit,
             pallete_pattern,
             nbr_of_picks,
             mov1,
             mov2,
             mov4,
             mov8,
             inv1,
             inv2,
             inv4,
             inv8,
             start_date,
             end_date
        into v_i_ih_currowid,
             v_item_ship_unit,
             v_pallete_pattern,
             v_nbr_of_picks,
             v_mov_pallet,
             v_mov_case,
             v_mov_inner,
             v_mov_each,
             v_inv_pallet,
             v_inv_case,
             v_inv_inner,
             v_inv_each,
             v_start_date,
             v_end_date
        from tt_temp_item_history_smry
       where i_ih_rowid = v_i_ih_nextrowid;
   end loop;

   execute immediate ' delete tt_temp_item_history_smry ';

   v_no_of_slot_item_recs := 1;

   insert into tt_temp_item_history_smry (ship_unit,
                                          pallete_pattern,
                                          nbr_of_picks,
                                          mov1,
                                          mov2,
                                          mov4,
                                          mov8,
                                          inv1,
                                          inv2,
                                          inv4,
                                          inv8,
                                          start_date,
                                          end_date)
        select distinct v_slot_ship_unit,
                        si.pallete_pattern,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        ih.start_date,
                        ih.end_date
          from    item_history ih
               inner join
                  slot_item si
               on si.slotitem_id = ih.slotitem_id
               left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
         where     si.sku_id = v_sku_id
               and si.ship_unit = v_slot_ship_unit
               and si.delete_mult <> 1
               and nvl (si.hist_match, '0x') = v_hist_match
               and nvl (si.mult_loc_grp, '0x') = v_mult_loc_grp
               and si.slotitem_id <> v_min_slotitem_id
               and si.slot_id is null
      order by ih.start_date nulls first, si.pallete_pattern nulls first;

   v_i_ih_loopctl := 1;

   select min (i_ih_rowid)
     into v_i_ih_nextrowid
     from tt_temp_item_history_smry;



   if (nvl (v_i_ih_nextrowid, 0) = 0)
   then
      dbms_output.put_line (
         'info: there is no data in item history table to consolidate by inserting the slot_items that do not have the history for the min. slot item id');
      goto last_call;
   end if;


   update tt_temp_item_history_smry tihs_upd
      set (mov1,
           mov2,
           mov4,
           mov8,
           inv1,
           inv2,
           inv4,
           inv8,
           nbr_of_picks) =
             (select tout.mov1,
                     tout.mov2,
                     tout.mov4,
                     tout.mov8,
                     tout.inv1,
                     tout.inv2,
                     tout.inv4,
                     tout.inv8,
                     tout.nbr_of_picks
                from    tt_temp_item_history_smry tihs
                     inner join
                        (  select sum (thist.mov1) mov1,
                                  sum (thist.mov2) mov2,
                                  sum (thist.mov4) mov4,
                                  sum (thist.mov8) mov8,
                                  sum (thist.inv1) inv1,
                                  sum (thist.inv2) inv2,
                                  sum (thist.inv4) inv4,
                                  sum (thist.inv8) inv8,
                                  thist.start_date,
                                  thist.end_date,
                                  thist.pallete_pattern,
                                  sum (thist.nbr_of_picks) nbr_of_picks
                             from (  select nvl (
                                               case ih.ship_unit
                                                  when 1
                                                  then
                                                     sum (nvl (ih.movement, 0))
                                               end,
                                               0)
                                               as mov1,
                                            nvl (
                                               case ih.ship_unit
                                                  when 2
                                                  then
                                                     sum (nvl (ih.movement, 0))
                                               end,
                                               0)
                                               as mov2,
                                            nvl (
                                               case ih.ship_unit
                                                  when 4
                                                  then
                                                     sum (nvl (ih.movement, 0))
                                               end,
                                               0)
                                               as mov4,
                                            nvl (
                                               case ih.ship_unit
                                                  when 8
                                                  then
                                                     sum (nvl (ih.movement, 0))
                                               end,
                                               0)
                                               as mov8,
                                            nvl (
                                               case ih.ship_unit
                                                  when 1
                                                  then
                                                     sum (nvl (ih.inventory, 0))
                                               end,
                                               0)
                                               as inv1,
                                            nvl (
                                               case ih.ship_unit
                                                  when 2
                                                  then
                                                     sum (nvl (ih.inventory, 0))
                                               end,
                                               0)
                                               as inv2,
                                            nvl (
                                               case ih.ship_unit
                                                  when 4
                                                  then
                                                     sum (nvl (ih.inventory, 0))
                                               end,
                                               0)
                                               as inv4,
                                            nvl (
                                               case ih.ship_unit
                                                  when 8
                                                  then
                                                     sum (nvl (ih.inventory, 0))
                                               end,
                                               0)
                                               as inv8,
                                            sum (nvl (ih.nbr_of_picks, 0))
                                            / v_no_of_slot_item_recs
                                               as nbr_of_picks,
                                            ih.start_date,
                                            ih.end_date,
                                            si.pallete_pattern
                                       from    item_history ih
                                            inner join
                                               slot_item si
                                            on si.slotitem_id = ih.slotitem_id
                                            left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                                      where     si.sku_id = v_sku_id
                                            and si.ship_unit = v_slot_ship_unit
                                            and si.delete_mult <> 1
                                            and nvl (si.hist_match, '0x') =
                                                   v_hist_match
                                            and nvl (si.mult_loc_grp, '0x') =
                                                   v_mult_loc_grp
                                            and si.slot_id is null
                                            and si.slotitem_id <>
                                                   v_min_slotitem_id
                                   --sqlways_eval# = '2008-08-07 00:00:00.000' and ih.end_date = '2008-08-13 00:00:00.000'
                                   --sqlways_eval# = 0
                                   group by ih.ship_unit,
                                            ih.start_date,
                                            ih.end_date,
                                            si.pallete_pattern) thist
                         group by thist.start_date,
                                  thist.end_date,
                                  thist.pallete_pattern) tout
                     on     tout.start_date = tihs.start_date
                        and tout.end_date = tihs.end_date
                        and tout.pallete_pattern = tihs.pallete_pattern
               where tihs.rowid = tihs_upd.rowid)
    where rowid in
             (select tihs.rowid
                from    tt_temp_item_history_smry tihs
                     inner join
                        (  select sum (thist.mov1) mov1,
                                  sum (thist.mov2) mov2,
                                  sum (thist.mov4) mov4,
                                  sum (thist.mov8) mov8,
                                  sum (thist.inv1) inv1,
                                  sum (thist.inv2) inv2,
                                  sum (thist.inv4) inv4,
                                  sum (thist.inv8) inv8,
                                  thist.start_date,
                                  thist.end_date,
                                  thist.pallete_pattern,
                                  sum (thist.nbr_of_picks) nbr_of_picks
                             from (  select nvl (
                                               case ih.ship_unit
                                                  when 1
                                                  then
                                                     sum (
                                                        nvl (ih.movement,
                                                             0))
                                               end,
                                               0)
                                               as mov1,
                                            nvl (
                                               case ih.ship_unit
                                                  when 2
                                                  then
                                                     sum (
                                                        nvl (ih.movement,
                                                             0))
                                               end,
                                               0)
                                               as mov2,
                                            nvl (
                                               case ih.ship_unit
                                                  when 4
                                                  then
                                                     sum (
                                                        nvl (ih.movement,
                                                             0))
                                               end,
                                               0)
                                               as mov4,
                                            nvl (
                                               case ih.ship_unit
                                                  when 8
                                                  then
                                                     sum (
                                                        nvl (ih.movement,
                                                             0))
                                               end,
                                               0)
                                               as mov8,
                                            nvl (
                                               case ih.ship_unit
                                                  when 1
                                                  then
                                                     sum (
                                                        nvl (ih.inventory,
                                                             0))
                                               end,
                                               0)
                                               as inv1,
                                            nvl (
                                               case ih.ship_unit
                                                  when 2
                                                  then
                                                     sum (
                                                        nvl (ih.inventory,
                                                             0))
                                               end,
                                               0)
                                               as inv2,
                                            nvl (
                                               case ih.ship_unit
                                                  when 4
                                                  then
                                                     sum (
                                                        nvl (ih.inventory,
                                                             0))
                                               end,
                                               0)
                                               as inv4,
                                            nvl (
                                               case ih.ship_unit
                                                  when 8
                                                  then
                                                     sum (
                                                        nvl (ih.inventory,
                                                             0))
                                               end,
                                               0)
                                               as inv8,
                                            sum (nvl (ih.nbr_of_picks, 0))
                                            / v_no_of_slot_item_recs
                                               as nbr_of_picks,
                                            ih.start_date,
                                            ih.end_date,
                                            si.pallete_pattern
                                       from    item_history ih
                                            inner join
                                               slot_item si
                                            on si.slotitem_id =
                                                  ih.slotitem_id
                                            left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                                      where si.sku_id = v_sku_id
                                            and si.ship_unit =
                                                   v_slot_ship_unit
                                            and si.delete_mult <> 1
                                            and nvl (si.hist_match, '0x') =
                                                   v_hist_match
                                            and nvl (si.mult_loc_grp, '0x') =
                                                   v_mult_loc_grp
                                            and si.slot_id is null
                                            and si.slotitem_id <>
                                                   v_min_slotitem_id
                                   group by ih.ship_unit,
                                            ih.start_date,
                                            ih.end_date,
                                            si.pallete_pattern) thist
                         group by thist.start_date,
                                  thist.end_date,
                                  thist.pallete_pattern) tout
                     on     tout.start_date = tihs.start_date
                        and tout.end_date = tihs.end_date
                        and tout.pallete_pattern = tihs.pallete_pattern);


   select i_ih_rowid,
          ship_unit,
          pallete_pattern,
          nbr_of_picks,
          mov1,
          mov2,
          mov4,
          mov8,
          inv1,
          inv2,
          inv4,
          inv8,
          start_date,
          end_date
     into v_i_ih_currowid,
          v_item_ship_unit,
          v_pallete_pattern,
          v_nbr_of_picks,
          v_mov_pallet,
          v_mov_case,
          v_mov_inner,
          v_mov_each,
          v_inv_pallet,
          v_inv_case,
          v_inv_inner,
          v_inv_each,
          v_start_date,
          v_end_date
     from tt_temp_item_history_smry
    where i_ih_rowid = v_i_ih_nextrowid;



   while v_i_ih_loopctl = 1
   loop
      select count (*)
        into v_if_exists2
        from    item_history ih
             inner join
                slot_item si
             on si.slotitem_id = ih.slotitem_id
             left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
       where     si.sku_id = v_sku_id
             and si.ship_unit = v_slot_ship_unit
             and si.delete_mult <> 1
             and nvl (si.hist_match, '0x') = v_hist_match
             and nvl (si.mult_loc_grp, '0x') = v_mult_loc_grp
             and si.slotitem_id = v_min_slotitem_id
             and si.slot_id is null
             and ih.start_date = v_start_date
             and ih.end_date = v_end_date;

      if v_if_exists2 = 0
      then
         execute immediate ('delete tt_temp_item_history_inst');


         select curr_nbr
           into v_hist_id
           from unique_ids
          where rec_type = 'item_history';

         v_hist_id := v_hist_id + 1;



         insert into tt_temp_item_history_inst (slotitem_id,
                                                start_date,
                                                end_date,
                                                ship_unit,
                                                nbr_of_picks,
                                                movement,
                                                inventory)
              select si.slotitem_id,
                     v_start_date,
                     v_end_date,
                     v_slot_ship_unit,
                     v_nbr_of_picks,
                     0,
                     0
                from slot_item si
                left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
               where     si.sku_id = v_sku_id
                     and si.ship_unit = v_slot_ship_unit
                     and si.delete_mult <> 1
                     and nvl (si.hist_match, '0x') = v_hist_match
                     and nvl (si.mult_loc_grp, '0x') = v_mult_loc_grp
                     and si.slotitem_id = v_min_slotitem_id
                     and si.slot_id is null
            order by si.slotitem_id nulls first;



         update tt_temp_item_history_inst
            set hist_id = v_hist_id,
                movement =
                   case
                      when v_slot_ship_unit = 8 and v_pallete_pattern = 0
                      then
                         ( (v_mov_pallet * v_wh_hi * v_wh_ti * v_each_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_mov_case * v_each_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_inner * v_each_per_inn)
                            / v_no_of_slot_item_recs)
                         + (v_mov_each / v_no_of_slot_item_recs)
                      when v_slot_ship_unit = 8 and v_pallete_pattern = 1
                      then
                         ( (  v_mov_pallet
                            * v_ord_hi
                            * v_ord_ti
                            * v_each_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_mov_case * v_each_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_inner * v_each_per_inn)
                            / v_no_of_slot_item_recs)
                         + (v_mov_each / v_no_of_slot_item_recs)
                      when v_slot_ship_unit = 8 and v_pallete_pattern = 2
                      then
                         ( (  v_mov_pallet
                            * v_ven_hi
                            * v_ven_ti
                            * v_each_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_mov_case * v_each_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_inner * v_each_per_inn)
                            / v_no_of_slot_item_recs)
                         + (v_mov_each / v_no_of_slot_item_recs)
                      when v_slot_ship_unit = 4 and v_pallete_pattern = 0
                      then
                         ( (v_mov_pallet * v_wh_hi * v_wh_ti * v_inn_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_mov_case * v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + (v_mov_inner / v_no_of_slot_item_recs)
                         + ( (v_mov_each / v_each_per_inn)
                            / v_no_of_slot_item_recs)
                      when v_slot_ship_unit = 4 and v_pallete_pattern = 1
                      then
                         ( (v_mov_pallet * v_ord_hi * v_ord_ti * v_inn_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_mov_case * v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + (v_mov_inner / v_no_of_slot_item_recs)
                         + ( (v_mov_each / v_each_per_inn)
                            / v_no_of_slot_item_recs)
                      when v_slot_ship_unit = 4 and v_pallete_pattern = 2
                      then
                         ( (v_mov_pallet * v_ven_hi * v_ven_ti * v_inn_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_mov_case * v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + (v_mov_inner / v_no_of_slot_item_recs)
                         + ( (v_mov_each / v_each_per_inn)
                            / v_no_of_slot_item_recs)
                      when v_slot_ship_unit = 2 and v_pallete_pattern = 0
                      then
                         ( (v_mov_pallet * v_wh_hi * v_wh_ti)
                          / v_no_of_slot_item_recs)
                         + (v_mov_case / v_no_of_slot_item_recs)
                         + ( (v_mov_inner / v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_each / v_each_per_cs)
                            / v_no_of_slot_item_recs)
                      when v_slot_ship_unit = 2 and v_pallete_pattern = 1
                      then
                         ( (v_mov_pallet * v_ord_hi * v_ord_ti)
                          / v_no_of_slot_item_recs)
                         + (v_mov_case / v_no_of_slot_item_recs)
                         + ( (v_mov_inner / v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_each / v_each_per_cs)
                            / v_no_of_slot_item_recs)
                      when v_slot_ship_unit = 2 and v_pallete_pattern = 2
                      then
                         ( (v_mov_pallet * v_ven_hi * v_ven_ti)
                          / v_no_of_slot_item_recs)
                         + (v_mov_case / v_no_of_slot_item_recs)
                         + ( (v_mov_inner / v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_each / v_each_per_cs)
                            / v_no_of_slot_item_recs)
                      when v_slot_ship_unit = 1 and v_pallete_pattern = 0
                      then
                         (v_mov_pallet / v_no_of_slot_item_recs)
                         + ( (v_mov_case / (v_wh_hi * v_wh_ti))
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_inner
                              / (v_wh_hi * v_wh_ti * v_inn_per_cs))
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_each
                              / (v_wh_hi * v_wh_ti * v_each_per_cs))
                            / v_no_of_slot_item_recs)
                      when v_slot_ship_unit = 1 and v_pallete_pattern = 1
                      then
                         (v_mov_pallet / v_no_of_slot_item_recs)
                         + ( (v_mov_case / (v_ord_hi * v_ord_ti))
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_inner
                              / (v_ord_hi * v_ord_ti * v_inn_per_cs))
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_each
                              / (v_ord_hi * v_ord_ti * v_each_per_cs))
                            / v_no_of_slot_item_recs)
                      when v_slot_ship_unit = 1 and v_pallete_pattern = 2
                      then
                         (v_mov_pallet / v_no_of_slot_item_recs)
                         + ( (v_mov_case / (v_ven_hi * v_ven_ti))
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_inner
                              / (v_ven_hi * v_ven_ti * v_inn_per_cs))
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_each
                              / (v_ven_hi * v_ven_ti * v_each_per_cs))
                            / v_no_of_slot_item_recs)
                   end,
                inventory =
                   case
                      when v_slot_ship_unit = 8 and v_pallete_pattern = 0
                      then
                         ( (v_inv_pallet * v_wh_hi * v_wh_ti * v_each_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_inv_case * v_each_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_inner * v_each_per_inn)
                            / v_no_of_slot_item_recs)
                         + (v_inv_each / v_no_of_slot_item_recs)
                      when v_slot_ship_unit = 8 and v_pallete_pattern = 1
                      then
                         ( (  v_inv_pallet
                            * v_ord_hi
                            * v_ord_ti
                            * v_each_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_inv_case * v_each_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_inner * v_each_per_inn)
                            / v_no_of_slot_item_recs)
                         + (v_inv_each / v_no_of_slot_item_recs)
                      when v_slot_ship_unit = 8 and v_pallete_pattern = 2
                      then
                         ( (  v_inv_pallet
                            * v_ven_hi
                            * v_ven_ti
                            * v_each_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_inv_case * v_each_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_inner * v_each_per_inn)
                            / v_no_of_slot_item_recs)
                         + (v_inv_each / v_no_of_slot_item_recs)
                      when v_slot_ship_unit = 4 and v_pallete_pattern = 0
                      then
                         ( (v_inv_pallet * v_wh_hi * v_wh_ti * v_inn_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_inv_case * v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + (v_inv_inner / v_no_of_slot_item_recs)
                         + ( (v_inv_each / v_each_per_inn)
                            / v_no_of_slot_item_recs)
                      when v_slot_ship_unit = 4 and v_pallete_pattern = 1
                      then
                         ( (v_inv_pallet * v_ord_hi * v_ord_ti * v_inn_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_inv_case * v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + (v_inv_inner / v_no_of_slot_item_recs)
                         + ( (v_inv_each / v_each_per_inn)
                            / v_no_of_slot_item_recs)
                      when v_slot_ship_unit = 4 and v_pallete_pattern = 2
                      then
                         ( (v_inv_pallet * v_ven_hi * v_ven_ti * v_inn_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_inv_case * v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + (v_inv_inner / v_no_of_slot_item_recs)
                         + ( (v_inv_each / v_each_per_inn)
                            / v_no_of_slot_item_recs)
                      when v_slot_ship_unit = 2 and v_pallete_pattern = 0
                      then
                         ( (v_inv_pallet * v_wh_hi * v_wh_ti)
                          / v_no_of_slot_item_recs)
                         + (v_inv_case / v_no_of_slot_item_recs)
                         + ( (v_inv_inner / v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_each / v_each_per_cs)
                            / v_no_of_slot_item_recs)
                      when v_slot_ship_unit = 2 and v_pallete_pattern = 1
                      then
                         ( (v_inv_pallet * v_ord_hi * v_ord_ti)
                          / v_no_of_slot_item_recs)
                         + (v_inv_case / v_no_of_slot_item_recs)
                         + ( (v_inv_inner / v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_each / v_each_per_cs)
                            / v_no_of_slot_item_recs)
                      when v_slot_ship_unit = 2 and v_pallete_pattern = 2
                      then
                         ( (v_inv_pallet * v_ven_hi * v_ven_ti)
                          / v_no_of_slot_item_recs)
                         + (v_inv_case / v_no_of_slot_item_recs)
                         + ( (v_inv_inner / v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_each / v_each_per_cs)
                            / v_no_of_slot_item_recs)
                      when v_slot_ship_unit = 1 and v_pallete_pattern = 0
                      then
                         (v_inv_pallet / v_no_of_slot_item_recs)
                         + ( (v_inv_case / (v_wh_hi * v_wh_ti))
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_inner
                              / (v_wh_hi * v_wh_ti * v_inn_per_cs))
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_each
                              / (v_wh_hi * v_wh_ti * v_each_per_cs))
                            / v_no_of_slot_item_recs)
                      when v_slot_ship_unit = 1 and v_pallete_pattern = 1
                      then
                         (v_inv_pallet / v_no_of_slot_item_recs)
                         + ( (v_inv_case / (v_ord_hi * v_ord_ti))
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_inner
                              / (v_ord_hi * v_ord_ti * v_inn_per_cs))
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_each
                              / (v_ord_hi * v_ord_ti * v_each_per_cs))
                            / v_no_of_slot_item_recs)
                      when v_slot_ship_unit = 1 and v_pallete_pattern = 2
                      then
                         (v_inv_pallet / v_no_of_slot_item_recs)
                         + ( (v_inv_case / (v_ven_hi * v_ven_ti))
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_inner
                              / (v_ven_hi * v_ven_ti * v_inn_per_cs))
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_each
                              / (v_ven_hi * v_ven_ti * v_each_per_cs))
                            / v_no_of_slot_item_recs)
                   end;


         v_strsqlst :=
            'insert into  item_history (hist_id,slotitem_id,start_date,end_date,ship_unit,movement,inventory,nbr_of_picks)
      select hist_id,slotitem_id,start_date,end_date,ship_unit,movement,inventory,nbr_of_picks from tt_temp_item_history_inst';


         begin
            execute immediate v_strsqlst;
         exception
            when others
            then
               v_errcode := sqlcode;
               goto handle_error;
         end;

         swv_rowcount := sql%rowcount;
         v_recs_updtd_tmp := swv_rowcount;


         v_recs_updtd_tmp := 0;

         update unique_ids
            set curr_nbr = v_hist_id
          where rec_type = 'item_history';

         v_strsqlst :=
            'delete from item_history '
            || ' where hist_id in  ( select hist_id from item_history ih'
            || ' inner join slot_item si on si.slotitem_id = ih.slotitem_id left join slot s on s.slot_id = si.slot_id and s.whse_code = '''
            || p_whse  
            || ''' where si.sku_id = '
            || v_sku_id
            || ' and si.ship_unit ='
            || v_slot_ship_unit
            || ' and si.delete_mult <> '
            || 1
            || ' and nvl(si.hist_match,''0x'') = '''
            || v_hist_match
            || ''' and nvl(si.mult_loc_grp,''0x'') = '''
            || v_mult_loc_grp
            || ''' and si.slot_id is null and si.slotitem_id <> '
            || v_min_slotitem_id
            || ' and ih.start_date = '''
            || v_start_date
            || ''' and ih.end_date = '''
            || v_end_date
            || ''')';

         begin
            execute immediate v_strsqlst;
         exception
            when others
            then
               v_errcode := sqlcode;
               goto handle_error;
         end;


         v_bis_slotitem_good_to_delete := 1;
      end if;

      v_i_ih_nextrowid := null;


      select nvl (min (i_ih_rowid), 0)
        into v_i_ih_nextrowid
        from tt_temp_item_history_smry
       where i_ih_rowid > v_i_ih_currowid;

      if nvl (v_i_ih_nextrowid, 0) = 0
      then
         exit;
      end if;


      select i_ih_rowid,
             ship_unit,
             pallete_pattern,
             nbr_of_picks,
             mov1,
             mov2,
             mov4,
             mov8,
             inv1,
             inv2,
             inv4,
             inv8,
             start_date,
             end_date
        into v_i_ih_currowid,
             v_item_ship_unit,
             v_pallete_pattern,
             v_nbr_of_picks,
             v_mov_pallet,
             v_mov_case,
             v_mov_inner,
             v_mov_each,
             v_inv_pallet,
             v_inv_case,
             v_inv_inner,
             v_inv_each,
             v_start_date,
             v_end_date
        from tt_temp_item_history_smry
       where i_ih_rowid = v_i_ih_nextrowid;
   end loop;

  <<last_call>>
   if (v_bis_slotitem_good_to_delete = 1)
   then
      delete from slot_item_score
            where slot_item_score.slotitem_id in
                     (select distinct si.slotitem_id
                        from slot_item si
                        left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                       where     si.sku_id = v_sku_id
                             and si.ship_unit = v_slot_ship_unit
                             and si.delete_mult <> 1
                             and nvl (si.hist_match, '0x') = v_hist_match
                             and nvl (si.mult_loc_grp, '0x') = v_mult_loc_grp
                             and si.slot_id is null
                             and si.slotitem_id <> v_min_slotitem_id);

      delete from hn_failure
            where hn_failure.slotitem_id in
                     (select distinct si.slotitem_id
                        from slot_item si
                        left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                       where     si.sku_id = v_sku_id
                             and si.ship_unit = v_slot_ship_unit
                             and si.delete_mult <> 1
                             and nvl (si.hist_match, '0x') = v_hist_match
                             and nvl (si.mult_loc_grp, '0x') = v_mult_loc_grp
                             and si.slot_id is null
                             and si.slotitem_id <> v_min_slotitem_id);

      delete from haves_needs
            where haves_needs.WHSE_CODE = p_whse
                AND haves_needs.slot_item_id in
                     (select distinct si.slotitem_id
                        from slot_item si
                       where     si.sku_id = v_sku_id
                             and si.ship_unit = v_slot_ship_unit
                             and si.delete_mult <> 1
                             and nvl (si.hist_match, '0x') = v_hist_match
                             and nvl (si.mult_loc_grp, '0x') = v_mult_loc_grp
                             and si.slot_id is null
                             and si.slotitem_id <> v_min_slotitem_id);

      delete from needs_results_dtl
            where needs_results_dtl.slotitem_id in
                     (select distinct si.slotitem_id
                        from slot_item si
                        left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                       where     si.sku_id = v_sku_id
                             and si.ship_unit = v_slot_ship_unit
                             and si.delete_mult <> 1
                             and nvl (si.hist_match, '0x') = v_hist_match
                             and nvl (si.mult_loc_grp, '0x') = v_mult_loc_grp
                             and si.slot_id is null
                             and si.slotitem_id <> v_min_slotitem_id);

      dbms_output.put_line ('    deleting records in sl_failure_reasons...');

      delete from sl_failure_reasons
            where sl_failure_reasons.WHSE_CODE = p_whse
                AND sl_failure_reasons.slotitem_id in
                     (select distinct si.slotitem_id
                        from slot_item si
                       where     si.sku_id = v_sku_id
                             and si.ship_unit = v_slot_ship_unit
                             and si.delete_mult <> 1
                             and nvl (si.hist_match, '0x') = v_hist_match
                             and nvl (si.mult_loc_grp, '0x') = v_mult_loc_grp
                             and si.slot_id is null
                             and si.slotitem_id <> v_min_slotitem_id);
                             
      DBMS_OUTPUT.put_line ('    deleting records in move_list/move_list_hdr...');
      
      DELETE FROM move_list
      WHERE move_hdr_id IN (SELECT move_hdr_id
                              FROM move_list_hdr
                             WHERE whse_code = p_whse);
                     
      DELETE FROM move_list_hdr
      WHERE whse_code = p_whse;
      
      dbms_output.put_line (
         '    finally deleting all the unslotted records (where slot_id is null) for selected group in slot_item recs ...');
      v_strsqlst :=
            'delete from slot_item '
         || ' where slotitem_id in  ( select slotitem_id from slot_item si left join slot s on s.slot_id = si.slot_id and s.whse_code = '''
         || p_whse
         || ''' where si.sku_id = '
         || v_sku_id
         || ' and si.ship_unit ='
         || v_slot_ship_unit
         || ' and si.delete_mult <>'
         || 1
         || ' and nvl(si.hist_match,''0x'') = '''
         || v_hist_match
         || ''' and nvl(si.mult_loc_grp,''0x'') = '''
         || v_mult_loc_grp
         || ''' and si.slot_id is null and si.slotitem_id <> '
         || v_min_slotitem_id
         || ')';



      begin
         execute immediate v_strsqlst;
      exception
         when others
         then
            v_errcode := sqlcode;
            goto handle_error;
      end;
   else
      dbms_output.put_line (
         'warning: slot item records are not deleted for this sku group since there is no change made in item_history');
   end if;

   v_errcode := 0;

  <<handle_error>>
   null;
end;
/
show errors;
CREATE OR REPLACE 
PROCEDURE LANEBID_A_U_PROC (N_LAST_UPDATED_SOURCE_TYPE   IN NUMBER,
                            N_LAST_UPDATED_SOURCE        IN VARCHAR2,
                            O_RFPID                         NUMBER,
                            O_LANEID                        NUMBER,
                            O_LANEEQUIPMENTTYPEID           NUMBER,
                            O_PACKAGEID                     NUMBER,
                            N_ROUND_NUM                     NUMBER,
                            O_OBCARRIERCODEID               NUMBER,
                            O_LANECAPACITY                  NUMBER,
                            N_LANECAPACITY                  NUMBER,
                            O_SURGECAPACITY                 NUMBER,
                            N_SURGECAPACITY                 NUMBER,
                            O_MINIMUMCHARGE                 NUMBER,
                            N_MINIMUMCHARGE                 NUMBER,
                            O_FLATCHARGE                    NUMBER,
                            N_FLATCHARGE                    NUMBER,
                            O_BASERATE                      NUMBER,
                            N_BASERATE                      NUMBER,
                            O_INCLUSIVERATE                 NUMBER,
                            N_INCLUSIVERATE                 NUMBER,
                            O_WEIGHTBREAKCHARGE1            NUMBER,
                            N_WEIGHTBREAKCHARGE1            NUMBER,
                            O_WEIGHTBREAKCHARGE2            NUMBER,
                            N_WEIGHTBREAKCHARGE2            NUMBER,
                            O_WEIGHTBREAKCHARGE3            NUMBER,
                            N_WEIGHTBREAKCHARGE3            NUMBER,
                            O_WEIGHTBREAKCHARGE4            NUMBER,
                            N_WEIGHTBREAKCHARGE4            NUMBER,
                            O_WEIGHTBREAKCHARGE5            NUMBER,
                            N_WEIGHTBREAKCHARGE5            NUMBER,
                            O_WEIGHTBREAKCHARGE6            NUMBER,
                            N_WEIGHTBREAKCHARGE6            NUMBER,
                            O_WEIGHTBREAKCHARGE7            NUMBER,
                            N_WEIGHTBREAKCHARGE7            NUMBER,
                            O_WEIGHTBREAKCHARGE8            NUMBER,
                            N_WEIGHTBREAKCHARGE8            NUMBER,
                            O_WEIGHTBREAKCHARGE9            NUMBER,
                            N_WEIGHTBREAKCHARGE9            NUMBER,
                            O_WEIGHTCAPACITY                NUMBER,
                            N_WEIGHTCAPACITY                NUMBER,
                            O_WEIGHTUOM                     NUMBER,
                            N_WEIGHTUOM                     NUMBER,
                            O_RATEPERUOM                    NUMBER,
                            N_RATEPERUOM                    NUMBER,
                            O_FLIGHTFREQUENCY               NUMBER,
                            N_FLIGHTFREQUENCY               NUMBER,
                            O_SAILINGALLOCATION             NUMBER,
                            N_SAILINGALLOCATION             NUMBER,
                            O_SAILINGFREQUENCY              NUMBER,
                            N_SAILINGFREQUENCY              NUMBER,
                            O_CHARGEBASIS                   NUMBER,
                            N_CHARGEBASIS                   NUMBER,
                            O_COMMENTS                      VARCHAR2,
                            N_COMMENTS                      VARCHAR2,
                            O_RATEPERSIZE                   NUMBER,
                            N_RATEPERSIZE                   NUMBER,
                            O_RATEPERDISTANCE               NUMBER,
                            N_RATEPERDISTANCE               NUMBER,
                            O_RATEDISCOUNT                  NUMBER,
                            N_RATEDISCOUNT                  NUMBER,
                            O_CURRENCYCODE                  VARCHAR2,
                            N_CURRENCYCODE                  VARCHAR2,
							O_TRANSITTIME					NUMBER,
							N_TRANSITTIME					NUMBER,
							O_DAILYCAPACITY					NUMBER,
							N_DAILYCAPACITY					NUMBER,
                            O_CUSTTEXT1                     VARCHAR2,
                            N_CUSTTEXT1                     VARCHAR2,
                            O_CUSTTEXT2                     VARCHAR2,
                            N_CUSTTEXT2                     VARCHAR2,
                            O_CUSTTEXT3                     VARCHAR2,
                            N_CUSTTEXT3                     VARCHAR2,
                            O_CUSTTEXT4                     VARCHAR2,
                            N_CUSTTEXT4                     VARCHAR2,
                            O_CUSTTEXT5                     VARCHAR2,
                            N_CUSTTEXT5                     VARCHAR2,
                            O_CUSTTEXT6                     VARCHAR2,
                            N_CUSTTEXT6                     VARCHAR2,
                            O_CUSTTEXT7                     VARCHAR2,
                            N_CUSTTEXT7                     VARCHAR2,
                            O_CUSTTEXT8                     VARCHAR2,
                            N_CUSTTEXT8                     VARCHAR2,
                            O_CUSTTEXT9                     VARCHAR2,
                            N_CUSTTEXT9                     VARCHAR2,
                            O_CUSTTEXT10                    VARCHAR2,
                            N_CUSTTEXT10                    VARCHAR2,
                            O_CUSTINT1                      NUMBER,
                            N_CUSTINT1                      NUMBER,
                            O_CUSTINT2                      NUMBER,
                            N_CUSTINT2                      NUMBER,
                            O_CUSTINT3                      NUMBER,
                            N_CUSTINT3                      NUMBER,
                            O_CUSTINT4                      NUMBER,
                            N_CUSTINT4                      NUMBER,
                            O_CUSTINT5                      NUMBER,
                            N_CUSTINT5                      NUMBER,
                            O_CUSTINT6                      NUMBER,
                            N_CUSTINT6                      NUMBER,
                            O_CUSTINT7                      NUMBER,
                            N_CUSTINT7                      NUMBER,
                            O_CUSTINT8                      NUMBER,
                            N_CUSTINT8                      NUMBER,
                            O_CUSTINT9                      NUMBER,
                            N_CUSTINT9                      NUMBER,
                            O_CUSTINT10                     NUMBER,
                            N_CUSTINT10                     NUMBER,
                            O_CUSTDOUBLE1                   NUMBER,
                            N_CUSTDOUBLE1                   NUMBER,
                            O_CUSTDOUBLE2                   NUMBER,
                            N_CUSTDOUBLE2                   NUMBER,
                            O_CUSTDOUBLE3                   NUMBER,
                            N_CUSTDOUBLE3                   NUMBER,
                            O_CUSTDOUBLE4                   NUMBER,
                            N_CUSTDOUBLE4                   NUMBER,
                            O_CUSTDOUBLE5                   NUMBER,
                            N_CUSTDOUBLE5                   NUMBER,
                            O_CUSTDOUBLE6                   NUMBER,
                            N_CUSTDOUBLE6                   NUMBER,
                            O_CUSTDOUBLE7                   NUMBER,
                            N_CUSTDOUBLE7                   NUMBER,
                            O_CUSTDOUBLE8                   NUMBER,
                            N_CUSTDOUBLE8                   NUMBER,
                            O_CUSTDOUBLE9                   NUMBER,
                            N_CUSTDOUBLE9                   NUMBER,
                            O_CUSTDOUBLE10                  NUMBER,
                            N_CUSTDOUBLE10                  NUMBER)
AS
   vOldValue     VARCHAR2 (500);
   vNewValue     VARCHAR2 (500);
   VCHGFLAG      NUMBER DEFAULT 0;
   TIME_NOW      DATE;
   VFIELD_NAME   VARCHAR2 (500) DEFAULT NULL;
BEGIN
   TIME_NOW := SYSDATE;

   IF (O_LANECAPACITY = N_LANECAPACITY)
      OR ( (O_LANECAPACITY IS NULL) AND (N_LANECAPACITY IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Lane Capacity',
                   TO_CHAR (O_LANECAPACITY),
                   TO_CHAR (N_LANECAPACITY),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_SURGECAPACITY = N_SURGECAPACITY)
      OR ( (O_SURGECAPACITY IS NULL) AND (N_SURGECAPACITY IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Surge Capacity',
                   TO_CHAR (O_SURGECAPACITY),
                   TO_CHAR (N_SURGECAPACITY),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_MINIMUMCHARGE = N_MINIMUMCHARGE)
      OR ( (O_MINIMUMCHARGE IS NULL) AND (N_MINIMUMCHARGE IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Minimum Charge',
                   TO_CHAR (O_MINIMUMCHARGE),
                   TO_CHAR (N_MINIMUMCHARGE),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_FLATCHARGE = N_FLATCHARGE)
      OR ( (O_FLATCHARGE IS NULL) AND (N_FLATCHARGE IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Flat Charge',
                   TO_CHAR (O_FLATCHARGE),
                   TO_CHAR (N_FLATCHARGE),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_BASERATE = N_BASERATE)
      OR ( (O_BASERATE IS NULL) AND (N_BASERATE IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Base Rate',
                   TO_CHAR (O_BASERATE),
                   TO_CHAR (N_BASERATE),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_INCLUSIVERATE = N_INCLUSIVERATE)
      OR ( (O_INCLUSIVERATE IS NULL) AND (N_INCLUSIVERATE IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Inclusive Rate',
                   TO_CHAR (O_INCLUSIVERATE),
                   TO_CHAR (N_INCLUSIVERATE),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_WEIGHTBREAKCHARGE1 = N_WEIGHTBREAKCHARGE1)
      OR ( (O_WEIGHTBREAKCHARGE1 IS NULL) AND (N_WEIGHTBREAKCHARGE1 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Weight Break Charge 1',
                   TO_CHAR (O_WEIGHTBREAKCHARGE1),
                   TO_CHAR (N_WEIGHTBREAKCHARGE1),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_WEIGHTBREAKCHARGE2 = N_WEIGHTBREAKCHARGE2)
      OR ( (O_WEIGHTBREAKCHARGE2 IS NULL) AND (N_WEIGHTBREAKCHARGE2 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Weight Break Charge 2',
                   TO_CHAR (O_WEIGHTBREAKCHARGE2),
                   TO_CHAR (N_WEIGHTBREAKCHARGE2),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_WEIGHTBREAKCHARGE3 = N_WEIGHTBREAKCHARGE3)
      OR ( (O_WEIGHTBREAKCHARGE3 IS NULL) AND (N_WEIGHTBREAKCHARGE3 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Weight Break Charge 3',
                   TO_CHAR (O_WEIGHTBREAKCHARGE3),
                   TO_CHAR (N_WEIGHTBREAKCHARGE3),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_WEIGHTBREAKCHARGE4 = N_WEIGHTBREAKCHARGE4)
      OR ( (O_WEIGHTBREAKCHARGE4 IS NULL) AND (N_WEIGHTBREAKCHARGE4 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Weight Break Charge 4',
                   TO_CHAR (O_WEIGHTBREAKCHARGE4),
                   TO_CHAR (N_WEIGHTBREAKCHARGE4),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_WEIGHTBREAKCHARGE5 = N_WEIGHTBREAKCHARGE5)
      OR ( (O_WEIGHTBREAKCHARGE5 IS NULL) AND (N_WEIGHTBREAKCHARGE5 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Weight Break Charge 5',
                   TO_CHAR (O_WEIGHTBREAKCHARGE5),
                   TO_CHAR (N_WEIGHTBREAKCHARGE5),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_WEIGHTBREAKCHARGE6 = N_WEIGHTBREAKCHARGE6)
      OR ( (O_WEIGHTBREAKCHARGE6 IS NULL) AND (N_WEIGHTBREAKCHARGE6 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Weight Break Charge 6',
                   TO_CHAR (O_WEIGHTBREAKCHARGE6),
                   TO_CHAR (N_WEIGHTBREAKCHARGE6),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_WEIGHTBREAKCHARGE7 = N_WEIGHTBREAKCHARGE7)
      OR ( (O_WEIGHTBREAKCHARGE7 IS NULL) AND (N_WEIGHTBREAKCHARGE7 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Weight Break Charge 7',
                   TO_CHAR (O_WEIGHTBREAKCHARGE7),
                   TO_CHAR (N_WEIGHTBREAKCHARGE7),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_WEIGHTBREAKCHARGE8 = N_WEIGHTBREAKCHARGE8)
      OR ( (O_WEIGHTBREAKCHARGE8 IS NULL) AND (N_WEIGHTBREAKCHARGE8 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Weight Break Charge 8',
                   TO_CHAR (O_WEIGHTBREAKCHARGE8),
                   TO_CHAR (N_WEIGHTBREAKCHARGE8),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_WEIGHTBREAKCHARGE9 = N_WEIGHTBREAKCHARGE9)
      OR ( (O_WEIGHTBREAKCHARGE9 IS NULL) AND (N_WEIGHTBREAKCHARGE9 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Weight Break Charge 9',
                   TO_CHAR (O_WEIGHTBREAKCHARGE9),
                   TO_CHAR (N_WEIGHTBREAKCHARGE9),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_WEIGHTCAPACITY = N_WEIGHTCAPACITY)
      OR ( (O_WEIGHTCAPACITY IS NULL) AND (N_WEIGHTCAPACITY IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Weight Capacity',
                   TO_CHAR (O_WEIGHTCAPACITY),
                   TO_CHAR (N_WEIGHTCAPACITY),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_WEIGHTUOM = N_WEIGHTUOM)
      OR ( (O_WEIGHTUOM IS NULL) AND (N_WEIGHTUOM IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Weight UOM',
                   TO_CHAR (O_WEIGHTUOM),
                   TO_CHAR (N_WEIGHTUOM),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_RATEPERUOM = N_RATEPERUOM)
      OR ( (O_RATEPERUOM IS NULL) AND (N_RATEPERUOM IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Distance UOM',
                   TO_CHAR (O_RATEPERUOM),
                   TO_CHAR (N_RATEPERUOM),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_FLIGHTFREQUENCY = N_FLIGHTFREQUENCY)
      OR ( (O_FLIGHTFREQUENCY IS NULL) AND (N_FLIGHTFREQUENCY IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Flight Frequency',
                   TO_CHAR (O_FLIGHTFREQUENCY),
                   TO_CHAR (N_FLIGHTFREQUENCY),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_SAILINGALLOCATION = N_SAILINGALLOCATION)
      OR ( (O_SAILINGALLOCATION IS NULL) AND (N_SAILINGALLOCATION IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Sailing Allocation',
                   TO_CHAR (O_SAILINGALLOCATION),
                   TO_CHAR (N_SAILINGALLOCATION),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_SAILINGFREQUENCY = N_SAILINGFREQUENCY)
      OR ( (O_SAILINGFREQUENCY IS NULL) AND (N_SAILINGFREQUENCY IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Sailing Frequency',
                   TO_CHAR (O_SAILINGFREQUENCY),
                   TO_CHAR (N_SAILINGFREQUENCY),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_CHARGEBASIS = N_CHARGEBASIS)
      OR ( (O_CHARGEBASIS IS NULL) AND (N_CHARGEBASIS IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Charge Basis',
                   TO_CHAR (O_CHARGEBASIS),
                   TO_CHAR (N_CHARGEBASIS),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (NVL (O_COMMENTS, '') <> NVL (N_COMMENTS, ''))
   THEN
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Comments',
                   SUBSTR (O_COMMENTS, 0, 99),
                   SUBSTR (N_COMMENTS, 0, 99),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_RATEPERSIZE = N_RATEPERSIZE)
      OR ( (O_RATEPERSIZE IS NULL) AND (N_RATEPERSIZE IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Rate Per Size',
                   TO_CHAR (O_RATEPERSIZE),
                   TO_CHAR (N_RATEPERSIZE),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_RATEPERDISTANCE = N_RATEPERDISTANCE)
      OR ( (O_RATEPERDISTANCE IS NULL) AND (N_RATEPERDISTANCE IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Rate Per Distance',
                   TO_CHAR (O_RATEPERDISTANCE),
                   TO_CHAR (N_RATEPERDISTANCE),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_RATEDISCOUNT = N_RATEDISCOUNT)
      OR ( (O_RATEDISCOUNT IS NULL) AND (N_RATEDISCOUNT IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   '% Discount',
                   TO_CHAR (O_RATEDISCOUNT),
                   TO_CHAR (N_RATEDISCOUNT),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;
   
    IF( O_TRANSITTIME = N_TRANSITTIME) or(( O_TRANSITTIME is NULL) and( N_TRANSITTIME is NULL)) THEN
			NULL;
		ELSE
			INSERT INTO  LANEBID_EVENT
					( LANEBID_EVENT_ID ,
					LANEID,
					LANEEQUIPMENTTYPEID,
					OBCARRIERCODEID,
					PACKAGEID,
					ROUND_NUM,
					RFPID,
					FIELD_NAME ,
					OLD_VALUE ,
					NEW_VALUE ,
					CREATED_SOURCE_TYPE ,
					CREATED_SOURCE ,
					CREATED_DTTM )
			 VALUES     ( LANEBID_EVENT_ID_SEQ.NEXTVAL ,
			 		O_LANEID,
			 		O_LANEEQUIPMENTTYPEID,
			 		O_OBCARRIERCODEID,
			 		O_PACKAGEID,
			 		N_ROUND_NUM,
			 		O_Rfpid,
					'Transit Time' ,
					TO_CHAR(O_TRANSITTIME) ,
					TO_CHAR(N_TRANSITTIME) ,
					N_LAST_UPDATED_SOURCE_TYPE ,
					N_LAST_UPDATED_SOURCE ,
					SYSDATE);

					Vchgflag  :=   1 ;
		End If;
    
    IF( O_DAILYCAPACITY = N_DAILYCAPACITY) or(( O_DAILYCAPACITY is NULL) and( N_DAILYCAPACITY is NULL)) THEN
			NULL;
		ELSE
			INSERT INTO  LANEBID_EVENT
					( LANEBID_EVENT_ID ,
					LANEID,
					LANEEQUIPMENTTYPEID,
					OBCARRIERCODEID,
					PACKAGEID,
					ROUND_NUM,
					RFPID,
					FIELD_NAME ,
					OLD_VALUE ,
					NEW_VALUE ,
					CREATED_SOURCE_TYPE ,
					CREATED_SOURCE ,
					CREATED_DTTM )
			 VALUES     ( LANEBID_EVENT_ID_SEQ.NEXTVAL ,
			 		O_LANEID,
			 		O_LANEEQUIPMENTTYPEID,
			 		O_OBCARRIERCODEID,
			 		O_PACKAGEID,
			 		N_ROUND_NUM,
			 		O_Rfpid,
					'Daily Capacity' ,
					TO_CHAR(O_DAILYCAPACITY) ,
					TO_CHAR(N_DAILYCAPACITY) ,
					N_LAST_UPDATED_SOURCE_TYPE ,
					N_LAST_UPDATED_SOURCE ,
					SYSDATE);

					Vchgflag  :=   1 ;
		END IF;

   IF (NVL (O_CURRENCYCODE, '') <> NVL (N_CURRENCYCODE, ''))
   THEN
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'Currency Code',
                   O_CURRENCYCODE,
                   N_CURRENCYCODE,
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (NVL (O_CUSTTEXT1, '') <> NVL (N_CUSTTEXT1, ''))
   THEN
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTTEXT1',
                   SUBSTR (O_CUSTTEXT1, 0, 99),
                   SUBSTR (N_CUSTTEXT1, 0, 99),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (NVL (O_CUSTTEXT2, '') <> NVL (N_CUSTTEXT2, ''))
   THEN
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTTEXT2',
                   SUBSTR (O_CUSTTEXT2, 0, 99),
                   SUBSTR (N_CUSTTEXT2, 0, 99),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (NVL (O_CUSTTEXT3, '') <> NVL (N_CUSTTEXT3, ''))
   THEN
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTTEXT3',
                   SUBSTR (O_CUSTTEXT3, 0, 99),
                   SUBSTR (N_CUSTTEXT3, 0, 99),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (NVL (O_CUSTTEXT4, '') <> NVL (N_CUSTTEXT4, ''))
   THEN
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTTEXT4',
                   SUBSTR (O_CUSTTEXT4, 0, 99),
                   SUBSTR (N_CUSTTEXT4, 0, 99),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (NVL (O_CUSTTEXT5, '') <> NVL (N_CUSTTEXT5, ''))
   THEN
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTTEXT5',
                   SUBSTR (O_CUSTTEXT5, 0, 99),
                   SUBSTR (N_CUSTTEXT5, 0, 99),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (NVL (O_CUSTTEXT6, '') <> NVL (N_CUSTTEXT6, ''))
   THEN
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTTEXT6',
                   SUBSTR (O_CUSTTEXT6, 0, 99),
                   SUBSTR (N_CUSTTEXT6, 0, 99),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (NVL (O_CUSTTEXT7, '') <> NVL (N_CUSTTEXT7, ''))
   THEN
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTTEXT7',
                   SUBSTR (O_CUSTTEXT7, 0, 99),
                   SUBSTR (N_CUSTTEXT7, 0, 99),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (NVL (O_CUSTTEXT8, '') <> NVL (N_CUSTTEXT8, ''))
   THEN
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTTEXT8',
                   SUBSTR (O_CUSTTEXT8, 0, 99),
                   SUBSTR (N_CUSTTEXT8, 0, 99),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (NVL (O_CUSTTEXT9, '') <> NVL (N_CUSTTEXT9, ''))
   THEN
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTTEXT9',
                   SUBSTR (O_CUSTTEXT9, 0, 99),
                   SUBSTR (N_CUSTTEXT9, 0, 99),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (NVL (O_CUSTTEXT10, '') <> NVL (N_CUSTTEXT10, ''))
   THEN
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTTEXT10',
                   SUBSTR (O_CUSTTEXT10, 0, 99),
                   SUBSTR (N_CUSTTEXT10, 0, 99),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_CUSTINT1 = N_CUSTINT1)
      OR ( (O_CUSTINT1 IS NULL) AND (N_CUSTINT1 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTINT1',
                   TO_CHAR (O_CUSTINT1),
                   TO_CHAR (N_CUSTINT1),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_CUSTINT2 = N_CUSTINT2)
      OR ( (O_CUSTINT2 IS NULL) AND (N_CUSTINT2 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTINT2',
                   TO_CHAR (O_CUSTINT2),
                   TO_CHAR (N_CUSTINT2),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_CUSTINT3 = N_CUSTINT3)
      OR ( (O_CUSTINT3 IS NULL) AND (N_CUSTINT3 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTINT3',
                   TO_CHAR (O_CUSTINT3),
                   TO_CHAR (N_CUSTINT3),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_CUSTINT4 = N_CUSTINT4)
      OR ( (O_CUSTINT4 IS NULL) AND (N_CUSTINT4 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTINT4',
                   TO_CHAR (O_CUSTINT4),
                   TO_CHAR (N_CUSTINT4),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_CUSTINT5 = N_CUSTINT5)
      OR ( (O_CUSTINT5 IS NULL) AND (N_CUSTINT5 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTINT5',
                   TO_CHAR (O_CUSTINT5),
                   TO_CHAR (N_CUSTINT5),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_CUSTINT6 = N_CUSTINT6)
      OR ( (O_CUSTINT6 IS NULL) AND (N_CUSTINT6 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTINT6',
                   TO_CHAR (O_CUSTINT6),
                   TO_CHAR (N_CUSTINT6),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_CUSTINT7 = N_CUSTINT7)
      OR ( (O_CUSTINT7 IS NULL) AND (N_CUSTINT7 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTINT7',
                   TO_CHAR (O_CUSTINT7),
                   TO_CHAR (N_CUSTINT7),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_CUSTINT8 = N_CUSTINT8)
      OR ( (O_CUSTINT8 IS NULL) AND (N_CUSTINT8 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTINT8',
                   TO_CHAR (O_CUSTINT8),
                   TO_CHAR (N_CUSTINT8),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_CUSTINT9 = N_CUSTINT9)
      OR ( (O_CUSTINT9 IS NULL) AND (N_CUSTINT9 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTINT9',
                   TO_CHAR (O_CUSTINT9),
                   TO_CHAR (N_CUSTINT9),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_CUSTINT10 = N_CUSTINT10)
      OR ( (O_CUSTINT10 IS NULL) AND (N_CUSTINT10 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTINT10',
                   TO_CHAR (O_CUSTINT10),
                   TO_CHAR (N_CUSTINT10),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_CUSTDOUBLE1 = N_CUSTDOUBLE1)
      OR ( (O_CUSTDOUBLE1 IS NULL) AND (N_CUSTDOUBLE1 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTDOUBLE1',
                   TO_CHAR (O_CUSTDOUBLE1),
                   TO_CHAR (N_CUSTDOUBLE1),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_CUSTDOUBLE2 = N_CUSTDOUBLE2)
      OR ( (O_CUSTDOUBLE2 IS NULL) AND (N_CUSTDOUBLE2 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTDOUBLE2',
                   TO_CHAR (O_CUSTDOUBLE2),
                   TO_CHAR (N_CUSTDOUBLE2),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_CUSTDOUBLE3 = N_CUSTDOUBLE3)
      OR ( (O_CUSTDOUBLE3 IS NULL) AND (N_CUSTDOUBLE3 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTDOUBLE3',
                   TO_CHAR (O_CUSTDOUBLE3),
                   TO_CHAR (N_CUSTDOUBLE3),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_CUSTDOUBLE4 = N_CUSTDOUBLE4)
      OR ( (O_CUSTDOUBLE4 IS NULL) AND (N_CUSTDOUBLE4 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTDOUBLE4',
                   TO_CHAR (O_CUSTDOUBLE4),
                   TO_CHAR (N_CUSTDOUBLE4),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_CUSTDOUBLE5 = N_CUSTDOUBLE5)
      OR ( (O_CUSTDOUBLE5 IS NULL) AND (N_CUSTDOUBLE5 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTDOUBLE5',
                   TO_CHAR (O_CUSTDOUBLE5),
                   TO_CHAR (N_CUSTDOUBLE5),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_CUSTDOUBLE6 = N_CUSTDOUBLE6)
      OR ( (O_CUSTDOUBLE6 IS NULL) AND (N_CUSTDOUBLE6 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTDOUBLE6',
                   TO_CHAR (O_CUSTDOUBLE6),
                   TO_CHAR (N_CUSTDOUBLE6),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_CUSTDOUBLE7 = N_CUSTDOUBLE7)
      OR ( (O_CUSTDOUBLE7 IS NULL) AND (N_CUSTDOUBLE7 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTDOUBLE7',
                   TO_CHAR (O_CUSTDOUBLE7),
                   TO_CHAR (N_CUSTDOUBLE7),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_CUSTDOUBLE8 = N_CUSTDOUBLE8)
      OR ( (O_CUSTDOUBLE8 IS NULL) AND (N_CUSTDOUBLE8 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTDOUBLE8',
                   TO_CHAR (O_CUSTDOUBLE8),
                   TO_CHAR (N_CUSTDOUBLE8),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_CUSTDOUBLE9 = N_CUSTDOUBLE9)
      OR ( (O_CUSTDOUBLE9 IS NULL) AND (N_CUSTDOUBLE9 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTDOUBLE9',
                   TO_CHAR (O_CUSTDOUBLE9),
                   TO_CHAR (N_CUSTDOUBLE9),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;

   IF (O_CUSTDOUBLE10 = N_CUSTDOUBLE10)
      OR ( (O_CUSTDOUBLE10 IS NULL) AND (N_CUSTDOUBLE10 IS NULL))
   THEN
      NULL;
   ELSE
      INSERT INTO LANEBID_EVENT (LANEBID_EVENT_ID,
                                 LANEID,
                                 LANEEQUIPMENTTYPEID,
                                 OBCARRIERCODEID,
                                 PACKAGEID,
                                 ROUND_NUM,
                                 RFPID,
                                 FIELD_NAME,
                                 OLD_VALUE,
                                 NEW_VALUE,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM)
           VALUES (LANEBID_EVENT_ID_SEQ.NEXTVAL,
                   O_LANEID,
                   O_LANEEQUIPMENTTYPEID,
                   O_OBCARRIERCODEID,
                   O_PACKAGEID,
                   N_ROUND_NUM,
                   O_RFPID,
                   'CUSTDOUBLE10',
                   TO_CHAR (O_CUSTDOUBLE10),
                   TO_CHAR (N_CUSTDOUBLE10),
                   N_LAST_UPDATED_SOURCE_TYPE,
                   N_LAST_UPDATED_SOURCE,
                   SYSDATE);

      vChgFlag := 1;
   END IF;
END;
/



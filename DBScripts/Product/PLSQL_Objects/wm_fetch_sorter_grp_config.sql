create or replace procedure wm_fetch_sorter_grp_config
(
    p_whse              in facility.whse%type,
    p_pack_wave_parm_id in pack_wave_parm_hdr.pack_wave_parm_id%type,
    p_start_lvl         in out pack_wave_parm_hdr.start_lvl%type,
    p_min_pw_seq_nbr    out pack_wave_parm_dtl.pack_wave_seq_nbr%type,
    p_num_sorter_grps   out number,
    p_sys_cd_flag_lvl   out number 
)
as
    type t_sorter_grp     is table of sorter_grp.sorter_grp%type;
    va_sorter_grp         t_sorter_grp;
begin
    begin
        -- check the start level sorter_grp here
        if (p_start_lvl = 1)
        then
            -- assumption! flag can be set at one level only;
-- todo! support for cd_sys_code
            with sys_cd_sg_list as
            (
                select code_id sorter_grp, 0 sys_cd_flag_lvl
                from sys_code 
                where rec_type = 'S' and code_type = '695' 
                union
                select code_id sorter_grp, 1 sys_cd_flag_lvl
                from whse_sys_code 
                where rec_type = 'S' and code_type = '695' and whse= p_whse
            )
            select pwpd.pack_wave_seq_nbr, sys_cd_sg_list.sys_cd_flag_lvl
            into p_min_pw_seq_nbr, p_sys_cd_flag_lvl
            from sys_cd_sg_list
            join pack_wave_parm_dtl pwpd on sys_cd_sg_list.sorter_grp = pwpd.sorter_grp
                and pwpd.pack_wave_parm_id = p_pack_wave_parm_id;
        elsif (p_start_lvl = 2)
        then
            -- check sorter grp at pick wave lvl
            with sg_pick_wv_list as
            (
                select min(pwpd.sorter_grp) keep (dense_rank first order by pwpd.pack_wave_parm_id desc, 
                    pwpd.pack_wave_seq_nbr desc) sorter_grp
                from pack_wave_parm_dtl pwpd 
                where pwpd.rec_type = 'A' and pwpd.pack_wave_nbr is not null and pwpd.stat_code <99
            )
            select pwpd.pack_wave_seq_nbr
            into p_min_pw_seq_nbr
            from sg_pick_wv_list
            join pack_wave_parm_dtl pwpd on sg_pick_wv_list.sorter_grp = pwpd.sorter_grp
                and pwpd.pack_wave_parm_id = p_pack_wave_parm_id;
        end if;
    exception
        when no_data_found then
            p_min_pw_seq_nbr := null;
            p_sys_cd_flag_lvl := null;
    end;
    -- use lowest sequence when min_pw_seq_nbr is not set or not in list
    p_start_lvl := case when (p_start_lvl = 1 or p_start_lvl = 2) and p_min_pw_seq_nbr is null then 0
        else p_start_lvl end;
        
    -- load sorter groups in configured sequence
    select pwpd.sorter_grp
    bulk collect into va_sorter_grp
    from pack_wave_parm_dtl pwpd
    where pwpd.pack_wave_parm_id = p_pack_wave_parm_id
        and pwpd.sorter_grp is not null
    order by pwpd.pack_wave_seq_nbr;

    if (va_sorter_grp.count = 0)
    then
        raise_application_error(-20050, 'No sorter groups configured.');
    else
        p_num_sorter_grps := va_sorter_grp.count; 
        wm_cs_log('sorter groups configured : ' || p_num_sorter_grps);
    end if;
end;
/
show errors;
create or replace procedure manh_merge_create_bulk_pkts
(
    p_whse		                  in facility.whse%type,
    p_user_id		                in ucl_user.user_name%type,
    p_pick_wave_nbr	            in wave_parm.wave_nbr%type
)
as
    v_use_locking               number(1) := 0;
    type t_d_facility_alias_id  is table of orders.d_facility_alias_id%type
        index by binary_integer;
    va_d_facility_alias_id      t_d_facility_alias_id;
    type t_d_facility_id        is table of orders.d_facility_id%type
        index by binary_integer;
    va_d_facility_id            t_d_facility_id;
    v_d_facility_alias_id       facility.name%type;
    v_max_log_lvl               number(1) := wm_get_curr_log_lvl();
    store_lock_busy             exception;
    pragma exception_init(store_lock_busy, -30006);
begin
    select case when exists
    (
	    select 1
	    from sys_code sc
	    where sc.rec_type = 'S' and sc.code_type = '520' and sc.code_id = '005'
	        and substr(sc.misc_flags, 2, 1) = 'Y'
    ) then 1 else 0 end
    into v_use_locking
    from dual;

    if (v_use_locking = 1)
    then
	    -- collect stores in the rtl pkt list for this wave
	    select ph.store_nbr, ph.d_facility_id
	    bulk collect into va_d_facility_alias_id, va_d_facility_id
	    from pkt_hdr ph
	    where ph.whse = p_whse and ph.stat_code < 40 and ph.rtl_pkt_flag = '1'
	        and coalesce(ph.major_minor_pkt, 'X') != 'P'
	        and exists
	        (
		        select 1
		        from pkt_dtl pd
		        where pd.pkt_ctrl_nbr = ph.pkt_ctrl_nbr
		            and pd.wave_nbr = p_pick_wave_nbr
		            and pd.rtl_to_be_distroed_qty > 0
	        );
      wm_cs_log('Distinct stores found ', sql%rowcount);

	    -- process per store
	    for i in 1..va_d_facility_alias_id.count
	    loop
	        begin
-- todo: configurable wait time or should we even need one at all?
		        select fa.facility_alias_id
		        into v_d_facility_alias_id
		        from facility_alias fa
		        where fa.facility_alias_id = va_d_facility_alias_id(i)
		            and fa.facility_id = va_d_facility_id(i)
		        for update wait 15;
	        exception
		        when store_lock_busy then
		            raise_application_error(-20050, 'Waited for facility_alias_id '
			        || va_d_facility_alias_id(i) || ', facility_id '
			        || va_d_facility_id(i));
		        when no_data_found then
		            raise_application_error(-20050, 'No data for facility_alias_id '
			        || va_d_facility_alias_id(i) || ', facility_id '
			        || va_d_facility_id(i));
	        end;

	        -- create PP for this wave, per store
	        insert into pkt_hdr
	        (
		        cd_master_id, pkt_ctrl_nbr, pkt_hdr_id, stat_code, whse,
		        major_minor_pkt, original_tc_order_id, rtl_pkt_flag, store_nbr,
		        create_date_time, mod_date_time, user_id, d_facility_id, sngl_unit_flag
	        )
	        select ph.cd_master_id, to_char(pkt_hdr_id_seq.nextval) pkt_ctrl_nbr,
		        pkt_hdr_id_seq.nextval pkt_hdr_id, 10 stat_code, p_whse,
		        'P' major_minor_pkt, ph.original_tc_order_id, '1' rtl_pkt_flag,
		        ph.store_nbr, sysdate create_date_time, sysdate mod_date_time,
		        p_user_id, ph.d_facility_id, ph.sngl_unit_flag
	        from pkt_hdr ph
	        where ph.whse = p_whse and ph.stat_code < 40
              and ph.rtl_pkt_flag = '1'
              and coalesce(ph.major_minor_pkt, 'X') != 'P'
              and ph.store_nbr = va_d_facility_alias_id(i)
              and ph.d_facility_id = va_d_facility_id(i)
              and exists
              (
                  select 1
                  from pkt_dtl pd
                  where pd.pkt_ctrl_nbr = ph.pkt_ctrl_nbr
                and pd.wave_nbr = p_pick_wave_nbr
                and pd.rtl_to_be_distroed_qty > 0
              )
              and not exists
              (
                  select 1
                  from pkt_hdr ph2
                  where ph2.original_tc_order_id = ph.original_tc_order_id
                and ph2.cd_master_id = ph.cd_master_id
                and ph2.whse = ph.whse and ph2.rtl_pkt_flag = '1'
                and ph2.major_minor_pkt = 'P'
              );
          if (v_max_log_lvl = 2)
          then
              wm_cs_log('Perpetual pkt created for store ' || v_d_facility_alias_id
                  || '? ' || sql%rowcount);
          end if;

	        commit;
	    end loop;
    else
    	-- create all PP for this wave for those that dont already exist
-- todo: complete the insert column list
	    insert into pkt_hdr
	    (
	        cd_master_id, pkt_ctrl_nbr, pkt_hdr_id, stat_code, whse,
	        major_minor_pkt, original_tc_order_id, rtl_pkt_flag, store_nbr,
	        create_date_time, mod_date_time, user_id, d_facility_id, sngl_unit_flag
	    )
	    select ph.cd_master_id, to_char(pkt_hdr_id_seq.nextval) pkt_ctrl_nbr,
	        pkt_hdr_id_seq.nextval pkt_hdr_id, 10 stat_code, p_whse,
	        'P' major_minor_pkt, ph.original_tc_order_id, '1' rtl_pkt_flag,
	        ph.store_nbr, sysdate create_date_time, sysdate mod_date_time,
	        p_user_id, ph.d_facility_id, ph.sngl_unit_flag
	    from pkt_hdr ph
	    where ph.whse = p_whse and ph.stat_code < 40 and ph.rtl_pkt_flag = '1'
	        and coalesce(ph.major_minor_pkt, 'X') != 'P'
	        and exists
	        (
		        select 1
		        from pkt_dtl pd
		        where pd.pkt_ctrl_nbr = ph.pkt_ctrl_nbr
		            and pd.wave_nbr = p_pick_wave_nbr
		            and pd.rtl_to_be_distroed_qty > 0
	        )
	        and not exists
	        (
		        select 1
		        from pkt_hdr ph2
		        where ph2.original_tc_order_id = ph.original_tc_order_id
		            and ph2.cd_master_id = ph.cd_master_id
		            and ph2.whse = ph.whse and ph2.rtl_pkt_flag = '1'
		            and ph2.major_minor_pkt = 'P'
	        );
      wm_cs_log('Perpetual pkt created for stores ' || sql%rowcount);
    end if;
end;
/

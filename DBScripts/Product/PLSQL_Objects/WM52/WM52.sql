insert into label 
select seq_label_id.nextval, 'AssignStagingLane', 'Assign Staging Lane', 'CBOGrid', sysdate, sysdate from dual
where not exists (select 1 from label where bundle_name='CBOGrid' and key='AssignStagingLane');

UPDATE label SET VALUE = 'Assign Staging Lane' WHERE Bundle_name = 'CBOGrid' AND KEY = 'AssignStagingLane';

commit; 

Insert into SYS_CODE_TYPE (REC_TYPE,CODE_TYPE,CODE_DESC,SHORT_DESC,MAX_CODE_ID_LEN,ALLOW_ADD,ALLOW_DEL,CHG_MISC_FLAG,CHG_DTL_DESC,WHSE_DEPNDT,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,WM_VERSION_ID,SYS_CODE_TYPE_ID,ORDER_BY,CREATED_DTTM,LAST_UPDATED_DTTM) 
SELECT 'C','052','EX52 On/Off Switch','EX52On/Off',3,'Y','Y','Y','Y','0',sysdate,sysdate,'WMADMIN1',2,SYS_CODE_TYPE_ID_SEQ.nextval,1,sysdate,sysdate 
from dual
Where Not exists (SELECT 1 FROM SYS_CODE_TYPE WHERE REC_TYPE = 'C' AND CODE_TYPE = '052');

Insert into SYS_CODE (REC_TYPE,CODE_TYPE,CODE_ID,CODE_DESC,SHORT_DESC,MISC_FLAGS,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,WM_VERSION_ID,SYS_CODE_ID,SYS_CODE_TYPE_ID,CREATED_DTTM,LAST_UPDATED_DTTM)
SELECT 'C','052','001','Enable/Disable','On/Off','N',sysdate,sysdate,'WMADMIN1',1,SYS_CODE_ID_SEQ.nextval,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'C' AND CODE_TYPE = '052'),sysdate,sysdate from dual
Where Not exists (SELECT 1 FROM SYS_CODE WHERE REC_TYPE = 'C' AND CODE_TYPE = '052' and CODE_ID = '001');


Insert into SYS_CODE_PARM (REC_TYPE,CODE_TYPE,FROM_POSN,TO_POSN,MISC_FLAG_DESC,LITRL,MANDT_FLAG,VALID_CODE,VALID_SYS_CODE_REC_TYPE,VALID_SYS_CODE_TYPE,VALID_FROM,VALID_TO,VALID_VALUES,EDIT_STYLE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,WM_VERSION_ID,SYS_CODE_PARM_ID,SYS_CODE_TYPE_ID,VALID_SYS_CODE_TYPE_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
SELECT 'C','052',1,1,'Ex52 On/Off Switch',null,'Y','F',null,null,null,null,null,'CKBX',sysdate,sysdate,'WMADMIN1',2,SYS_CODE_PARM_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'C' AND CODE_TYPE = '052'),null,sysdate,sysdate from dual
Where Not exists (SELECT 1 FROM SYS_CODE_PARM WHERE REC_TYPE = 'C' AND CODE_TYPE = '052');

commit;

-- Manually inserting the item for default layouts 
DECLARE
BEGIN
  FOR REC IN (select LP.xlayout_part_id  from 
		xlayout_part LP,
		xlayout_section LS,
		xlayout_component LC,
		xlayout L

		where L.component_name ='Cbo.component.shipment.ShipmentList' and L.layout_type =2 and L.xlayout_id = LC.xlayout_id
		and LC.xlayout_component_id = LS.xlayout_component_id and LC.name = 'Cbo.component.shipment.ShipmentListActions'
		and LS.xlayout_section_id = LP.xlayout_section_id and LS.name = 'Actions')
  LOOP

	DECLARE
    v_Count PLS_INTEGER := 0;
	BEGIN
	SELECT COUNT(*) INTO v_Count FROM  xlayout_item where xlayout_part_id = REC.xlayout_part_id and name = 'AssignStagingLane';
     IF V_COUNT = 0 THEN
          
           Insert into xlayout_item
            (XLAYOUT_ITEM_ID,XLAYOUT_PART_ID,POSITION,NESTED_XLAYOUT_COMP_ID,IS_VISIBLE,IS_EDITABLE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM, LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM,NAME) 
            values ( SEQ_XLAYOUT_ITEM_ID.nextval,REC.xlayout_part_id ,16,null,1,1,null,1,sysdate,null,1,sysdate,'AssignStagingLane' );
     END IF;
	EXCEPTION WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001, 'Error in inserting into xlayout_item' || CHR(10) || SQLERRM);
	END;
	END LOOP;
  COMMIT;
END;
/

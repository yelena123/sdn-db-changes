CREATE OR REPLACE PROCEDURE sp_del_slotitemscore_by_range 
(
    v_range_id IN NUMBER
)
AS
BEGIN
    BEGIN
        sp_get_range_ids (v_range_id, 'tt_RANGE_IDS_TABLE6');
    END;

    DELETE FROM slot_item_score
    WHERE slot_item_score_id IN ( SELECT sis.slot_item_score_id
                                  FROM slot_item_score sis
                                  INNER JOIN slot_item si ON sis.slotitem_id = si.slotitem_id
                                  INNER JOIN slot s ON s.slot_id = si.slot_id
                                  INNER JOIN tt_range_ids_table6 tr ON tr.range_id = s.my_range );

    DELETE FROM tt_range_ids_table6;
END;
/

CREATE OR REPLACE
PACKAGE RATING_LANE_VALIDATION_PKG
AS
   TYPE ref_curtype IS REF CURSOR;

   cvDelimiter   CONSTANT VARCHAR2 (1) := '~';

   PROCEDURE Import_Rating_Lanes (
      vTCCompanyId        IN COMPANY.COMPANY_ID%TYPE,
      vBatchId            IN IMPORT_RATING_LANE.BATCH_ID%TYPE,
      vBusinessUnit       IN IMPORT_RATING_LANE.BUSINESS_UNIT%TYPE,
      vOFacilityAliasId   IN IMPORT_RATING_LANE.O_FACILITY_ALIAS_ID%TYPE,
      vOCity              IN IMPORT_RATING_LANE.O_CITY%TYPE,
      vOStateProv         IN IMPORT_RATING_LANE.O_STATE_PROV%TYPE,
      vOCounty            IN IMPORT_RATING_LANE.O_COUNTY%TYPE,
      vOPostal            IN IMPORT_RATING_LANE.O_POSTAL_CODE%TYPE,
      vOCountry           IN IMPORT_RATING_LANE.O_COUNTRY_CODE%TYPE,
      vOZoneName          IN IMPORT_RATING_LANE.O_ZONE_NAME%TYPE,
      vDFacilityAliasId   IN IMPORT_RATING_LANE.D_FACILITY_ALIAS_ID%TYPE,
      vDCity              IN IMPORT_RATING_LANE.D_CITY%TYPE,
      vDStateProv         IN IMPORT_RATING_LANE.D_STATE_PROV%TYPE,
      vDCounty            IN IMPORT_RATING_LANE.D_COUNTY%TYPE,
      vDPostal            IN IMPORT_RATING_LANE.D_POSTAL_CODE%TYPE,
      vDCountry           IN IMPORT_RATING_LANE.D_COUNTRY_CODE%TYPE,
      vDZoneName          IN IMPORT_RATING_LANE.D_ZONE_NAME%TYPE,
      vLaneStatus         IN IMPORT_RATING_LANE.LANE_STATUS%TYPE,
      vOZoneId            IN IMPORT_RATING_LANE.O_ZONE_ID%TYPE,
      vDZoneId            IN IMPORT_RATING_LANE.D_ZONE_ID%TYPE);

   PROCEDURE IMPORT_RATING_LANE_DETAILS (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vBatchId               IN IMPORT_RATING_LANE.BATCH_ID%TYPE,
      vRatingLaneDtlSeq      IN IMPORT_RATING_LANE_DTL.RATING_LANE_DTL_SEQ%TYPE,
      vEffectiveDT           IN IMPORT_RATING_LANE_DTL.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN IMPORT_RATING_LANE_DTL.EXPIRATION_DT%TYPE,
      vCarrierCode           IN IMPORT_RATING_LANE_DTL.CARRIER_CODE%TYPE,
      vMot                   IN IMPORT_RATING_LANE_DTL.MOT%TYPE,
      vEquipment             IN IMPORT_RATING_LANE_DTL.EQUIPMENT_CODE%TYPE,
      vServiceLevel          IN IMPORT_RATING_LANE_DTL.SERVICE_LEVEL%TYPE,
      p_scndr_carrier_code   IN IMPORT_RATING_LANE_DTL.SCNDR_CARRIER_CODE%TYPE,
      vProtectionLevel       IN IMPORT_RATING_LANE_DTL.PROTECTION_LEVEL%TYPE,
      vIsBudgeted            IN IMPORT_RATING_LANE_DTL.IS_BUDGETED%TYPE,
      vOriginShipVia         IN IMPORT_RATING_LANE_DTL.O_SHIP_VIA%TYPE,
      vDestinationShipVia    IN IMPORT_RATING_LANE_DTL.D_SHIP_VIA%TYPE,
      vContractNumber        IN IMPORT_RATING_LANE_DTL.CONTRACT_NUMBER%TYPE,
      vCustomText1           IN IMPORT_RATING_LANE_DTL.CUSTOM_TEXT1%TYPE,
      vCustomText2           IN IMPORT_RATING_LANE_DTL.CUSTOM_TEXT2%TYPE,
      vCustomText3           IN IMPORT_RATING_LANE_DTL.CUSTOM_TEXT3%TYPE,
      vCustomText4           IN IMPORT_RATING_LANE_DTL.CUSTOM_TEXT4%TYPE,
      vCustomText5           IN IMPORT_RATING_LANE_DTL.CUSTOM_TEXT5%TYPE,
      vPackageId             IN IMPORT_RATING_LANE_DTL.PACKAGE_ID%TYPE,
      vPackageName           IN IMPORT_RATING_LANE_DTL.PACKAGE_NAME%TYPE,
      vRateDtlStatus         IN IMPORT_RATING_LANE_DTL.IMPORT_RATING_DTL_STATUS%TYPE,
      vImpRateDtlStatus      IN IMPORT_RATING_LANE_DTL.RATING_LANE_DTL_STATUS%TYPE);

   PROCEDURE IMPORT_RATING_LANE_RATES (
      vTCCompanyId          IN COMPANY.COMPANY_ID%TYPE,
      vBatchId              IN IMPORT_RATING_LANE.BATCH_ID%TYPE,
      vRatingLaneDtlId      IN IMPORT_RATING_LANE_DTL_RATE.RATING_LANE_DTL_SEQ%TYPE,
      vRldRateSeq           IN IMPORT_RATING_LANE_DTL_RATE.RLD_RATE_SEQ%TYPE,
      vMinSize              IN IMPORT_RATING_LANE_DTL_RATE.MINIMUM_SIZE%TYPE,
      vMaxSize              IN IMPORT_RATING_LANE_DTL_RATE.MAXIMUM_SIZE%TYPE,
      vSizeUOM              IN IMPORT_RATING_LANE_DTL_RATE.SIZE_UOM%TYPE,
      vRateCalcMethod       IN IMPORT_RATING_LANE_DTL_RATE.RATE_CALC_METHOD%TYPE,
      vRateUOM              IN IMPORT_RATING_LANE_DTL_RATE.RATE_UOM%TYPE,
      vRate                 IN IMPORT_RATING_LANE_DTL_RATE.RATE%TYPE,
      vMinRate              IN IMPORT_RATING_LANE_DTL_RATE.MINIMUM_RATE%TYPE,
      vCurrencyCode         IN IMPORT_RATING_LANE_DTL_RATE.CURRENCY_CODE%TYPE,
      vTariffId             IN IMPORT_RATING_LANE_DTL_RATE.TARIFF_ID%TYPE,
      vMinDistance          IN IMPORT_RATING_LANE_DTL_RATE.MIN_DISTANCE%TYPE,
      vMaxDistance          IN IMPORT_RATING_LANE_DTL_RATE.MAX_DISTANCE%TYPE,
      vDistanceUOM          IN IMPORT_RATING_LANE_DTL_RATE.DISTANCE_UOM%TYPE,
      vSupportsMSTL         IN IMPORT_RATING_LANE_DTL_RATE.SUPPORTS_MSTL%TYPE,
      vHasBH                IN IMPORT_RATING_LANE_DTL_RATE.HAS_BH%TYPE,
      vMinCommodity         IN IMPORT_RATING_LANE_DTL_RATE.MIN_COMMODITY%TYPE,
      vMaxCommodity         IN IMPORT_RATING_LANE_DTL_RATE.MAX_COMMODITY%TYPE,
      vHasRT                IN IMPORT_RATING_LANE_DTL_RATE.HAS_RT%TYPE,
      vParcelTier           IN IMPORT_RATING_LANE_DTL_RATE.PARCEL_TIER%TYPE,
      vCommodityCodeId      IN IMPORT_RATING_LANE_DTL_RATE.COMMODITY_CODE_ID%TYPE,
      vExcessWtRate         IN IMPORT_RATING_LANE_DTL_RATE.EXCESS_WT_RATE%TYPE,
      vPayeeCarrier         IN IMPORT_RATING_LANE_DTL_RATE.PAYEE_CARRIER_CODE%TYPE,
      vMaxCommodityCodeId   IN IMPORT_RATING_LANE_DTL_RATE.MAX_RANGE_COMMODITY_CODE_ID%TYPE);

   PROCEDURE IMPORT_LANE_ACCESSORIALS (
      vTCCompanyId             IN COMPANY.COMPANY_ID%TYPE,
      vBatchId                 IN IMPORT_RATING_LANE.BATCH_ID%TYPE,
      vRatingLaneDtlId         IN IMPORT_RATING_LANE_DTL_RATE.RATING_LANE_DTL_SEQ%TYPE,
      vAccessorialCode         IN IMPORT_LANE_ACCESSORIAL.ACCESSORIAL_CODE%TYPE,
      vRate                    IN IMPORT_LANE_ACCESSORIAL.RATE%TYPE,
      vMinRate                 IN IMPORT_LANE_ACCESSORIAL.MINIMUM_RATE%TYPE,
      vCurrencyCode            IN IMPORT_LANE_ACCESSORIAL.CURRENCY_CODE%TYPE,
      p_minimum_size           IN IMPORT_LANE_ACCESSORIAL.MINIMUM_SIZE%TYPE,
      p_maximum_size           IN IMPORT_LANE_ACCESSORIAL.MAXIMUM_SIZE%TYPE,
      p_size_uom               IN IMPORT_LANE_ACCESSORIAL.SIZE_UOM%TYPE,
      vIsApproved              IN IMPORT_LANE_ACCESSORIAL.IS_AUTO_APPROVE%TYPE,
      vEffectiveDT             IN IMPORT_LANE_ACCESSORIAL.EFFECTIVE_DT%TYPE,
      vExpirationDT            IN IMPORT_LANE_ACCESSORIAL.EXPIRATION_DT%TYPE,
      vPayeeCarrierCode        IN IMPORT_LANE_ACCESSORIAL.PAYEE_CARRIER_CODE%TYPE,
      vIsShipmentAccessorial   IN IMPORT_LANE_ACCESSORIAL.IS_SHIPMENT_ACCESSORIAL%TYPE);

   PROCEDURE TRANSFER_IMPORT_LANES (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vUser           IN rating_lane.LAST_UPDATED_SOURCE%TYPE,
      vFirstBatchId   IN rating_lane.LANE_ID%TYPE,
      vLastBatchId    IN rating_lane.LANE_ID%TYPE);

   PROCEDURE TRANSFER_IMPORT_LANE_DTL (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vUser           IN rating_lane.LAST_UPDATED_SOURCE%TYPE,
      vImportLaneId   IN rating_lane.LANE_ID%TYPE,
      vLaneId         IN rating_lane.LANE_ID%TYPE);

   PROCEDURE TRANSFER_IMP_LANE_ACCESSORIAL (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vUser                  IN rating_lane.LAST_UPDATED_SOURCE%TYPE,
      vImportedLaneId        IN rating_lane.LANE_ID%TYPE,
      vLaneId                IN rating_lane.LANE_ID%TYPE,
      vImpRatingLaneDtlSeq   IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vRatingLaneDtlSeq      IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vIsPendingAuth         IN rating_lane_dtl.IS_BUDGETED%TYPE,
      vDuplicateLaneDtlId    IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE);

   PROCEDURE TRANSFER_IMP_LANE_DTL_RATE (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vUser                  IN rating_lane_dtl.LAST_UPDATED_SOURCE%TYPE,
      vImportedLaneId        IN rating_lane.LANE_ID%TYPE,
      vLaneId                IN rating_lane.LANE_ID%TYPE,
      vImpRatingLaneDtlSeq   IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vRatingLaneDtlSeq      IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vIsPendingAuth         IN rating_lane_dtl.IS_BUDGETED%TYPE,
      vDuplicateLaneDtlId    IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE);

   PROCEDURE VALIDATE_IMPORT_LANES (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vUser           IN rating_lane.LAST_UPDATED_SOURCE%TYPE,
      vFirstBatchId   IN rating_lane.LANE_ID%TYPE,
      vLastBatchId    IN rating_lane.LANE_ID%TYPE);

   PROCEDURE VALIDATE_LANE (vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
                            vLaneId        IN rating_lane.LANE_ID%TYPE);

   PROCEDURE VALIDATE_LANE_UI (vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
                               vLaneId        IN rating_lane.LANE_ID%TYPE);

   PROCEDURE INITIALIZE_LANE_DTL_STATUS (
      vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
      vLaneId        IN rating_lane.LANE_ID%TYPE);

   PROCEDURE VALIDATE_LANE_DTL (vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
                                vLaneId        IN rating_lane.LANE_ID%TYPE);

   FUNCTION VALIDATE_LANE_DTL_RATE (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vLaneId         IN rating_lane.LANE_ID%TYPE,
      vRatingDtlSeq   IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_LANE_DTL_ACCESSORIAL (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vLaneId         IN rating_lane.LANE_ID%TYPE,
      vRatingDtlSeq   IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_DIMENSION (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vLaneId         IN rating_lane.LANE_ID%TYPE,
      vRatingDtlSeq   IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vMin            IN IMPORT_LANE_ACCESSORIAL.Min_Longest_Dimension%TYPE,
      vMax            IN IMPORT_LANE_ACCESSORIAL.Max_Longest_Dimension%TYPE,
      vMsg            IN VARCHAR2)
      RETURN NUMBER;

   PROCEDURE ADJUST_EFF_EXP_DT (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vUser                  IN COMB_LANE_DTL.LAST_UPDATED_SOURCE%TYPE,
      vLaneId                IN COMB_LANE.LANE_ID%TYPE,
      vRatingDtlSeq          IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vEffectiveDT           IN COMB_LANE_DTL.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN COMB_LANE_DTL.EXPIRATION_DT%TYPE,
      vCarrierCode           IN COMB_LANE_DTL.CARRIER_ID%TYPE,
      vMot                   IN COMB_LANE_DTL.MOT_ID%TYPE,
      vEquipment             IN COMB_LANE_DTL.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN COMB_LANE_DTL.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN COMB_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN COMB_LANE_DTL.PROTECTION_LEVEL_ID%TYPE,
      vOriginShipVia         IN COMB_LANE_DTL.O_SHIP_VIA%TYPE,
      vDestinationShipVia    IN COMB_LANE_DTL.D_SHIP_VIA%TYPE,
	  vContractNumber        IN COMB_LANE_DTL.CONTRACT_NUMBER%TYPE,
      vCustomText1           IN COMB_LANE_DTL.CUSTOM_TEXT1%TYPE,
      vCustomText2           IN COMB_LANE_DTL.CUSTOM_TEXT2%TYPE,
      vCustomText3           IN COMB_LANE_DTL.CUSTOM_TEXT3%TYPE,
      vCustomText4           IN COMB_LANE_DTL.CUSTOM_TEXT4%TYPE,
      vCustomText5           IN COMB_LANE_DTL.CUSTOM_TEXT5%TYPE,
      vDoNotDeleteRate       IN COMB_LANE.LANE_ID%TYPE,
	  vVOYAGE				 IN COMB_LANE_DTL.VOYAGE%TYPE );

   -- deprecated
   PROCEDURE ADJUST_EFF_EXP_DT (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vLaneId                IN COMB_LANE.LANE_ID%TYPE,
      vRatingDtlSeq          IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vEffectiveDT           IN COMB_LANE_DTL.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN COMB_LANE_DTL.EXPIRATION_DT%TYPE,
      vCarrierCode           IN COMB_LANE_DTL.CARRIER_ID%TYPE,
      vMot                   IN COMB_LANE_DTL.MOT_ID%TYPE,
      vEquipment             IN COMB_LANE_DTL.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN COMB_LANE_DTL.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN COMB_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN COMB_LANE_DTL.PROTECTION_LEVEL_ID%TYPE,
      vOriginShipVia         IN COMB_LANE_DTL.O_SHIP_VIA%TYPE,
      vDestinationShipVia    IN COMB_LANE_DTL.D_SHIP_VIA%TYPE);

   PROCEDURE ADJUST_EFF_EXP_DT (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vLaneId                IN COMB_LANE.LANE_ID%TYPE,
      vRatingDtlSeq          IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vRatingDtlSeqUpd       IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vEffectiveDT           IN COMB_LANE_DTL.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN COMB_LANE_DTL.EXPIRATION_DT%TYPE,
      vCarrierCode           IN COMB_LANE_DTL.CARRIER_ID%TYPE,
      vMot                   IN COMB_LANE_DTL.MOT_ID%TYPE,
      vEquipment             IN COMB_LANE_DTL.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN COMB_LANE_DTL.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN COMB_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN COMB_LANE_DTL.PROTECTION_LEVEL_ID%TYPE,
      vOriginShipVia         IN COMB_LANE_DTL.O_SHIP_VIA%TYPE,
      vDestinationShipVia    IN COMB_LANE_DTL.D_SHIP_VIA%TYPE);

   -- [Start] Merging CR MACR00592522
   PROCEDURE ADJUST_EFF_EXP_DT_UI (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vLaneId                IN COMB_LANE.LANE_ID%TYPE,
      vRatingDtlSeq          IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vRatingDtlSeqUpd       IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vEffectiveDT           IN COMB_LANE_DTL.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN COMB_LANE_DTL.EXPIRATION_DT%TYPE,
      vCarrierCode           IN COMB_LANE_DTL.CARRIER_ID%TYPE,
      vMot                   IN COMB_LANE_DTL.MOT_ID%TYPE,
      vEquipment             IN COMB_LANE_DTL.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN COMB_LANE_DTL.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN COMB_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN COMB_LANE_DTL.PROTECTION_LEVEL_ID%TYPE,
      vOriginShipVia         IN COMB_LANE_DTL.O_SHIP_VIA%TYPE,
      vDestinationShipVia    IN COMB_LANE_DTL.D_SHIP_VIA%TYPE,
      vCustomText1           IN COMB_LANE_DTL.CUSTOM_TEXT1%TYPE,
      vCustomText2           IN COMB_LANE_DTL.CUSTOM_TEXT2%TYPE,
      vCustomText3           IN COMB_LANE_DTL.CUSTOM_TEXT3%TYPE,
      vCustomText4           IN COMB_LANE_DTL.CUSTOM_TEXT4%TYPE,
      vCustomText5           IN COMB_LANE_DTL.CUSTOM_TEXT5%TYPE,
      vuser                  IN comb_lane_dtl.last_updated_source%TYPE,
	  vVoyage				 IN comb_lane_dtl.VOYAGE%TYPE,
	  NEW_SEQ_NUM            OUT NUMBER);

   PROCEDURE COPY_CARRIERS (
      vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
      vLaneId        IN COMB_LANE.LANE_ID%TYPE,
      vOldLaneId     IN COMB_LANE.LANE_ID%TYPE,
      vuser          IN comb_lane_dtl.last_updated_source%TYPE);

   -- [End] Merging CR MACR00592522

   FUNCTION MERGE_DRAFT (
      vTCCompanyId     IN COMPANY.COMPANY_ID%TYPE,
      vUser            IN COMB_LANE_DTL.LAST_UPDATED_SOURCE%TYPE,
      vLaneId          IN COMB_LANE.LANE_ID%TYPE,
      vLaneDtlSeq      IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vLaneDtlSeqUpd   IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE)
      RETURN INT;
	  
	FUNCTION REPLICATE_ROW (
      vTCCompanyId     IN COMPANY.COMPANY_ID%TYPE,
      vUser            IN COMB_LANE_DTL.LAST_UPDATED_SOURCE%TYPE,
      vLaneId          IN COMB_LANE.LANE_ID%TYPE,
      vOverlappedSeq   IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vRatingDtlSeq    IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vEffectiveDT     IN COMB_LANE_DTL.EFFECTIVE_DT%TYPE,
      vExpirationDT    IN COMB_LANE_DTL.EXPIRATION_DT%TYPE,
      vIsRating        IN COMB_LANE_DTL.IS_RATING%TYPE,
      vIsRouting       IN COMB_LANE_DTL.IS_ROUTING%TYPE,
      vIsSailing       IN COMB_LANE_DTL.IS_SAILING%TYPE)
	  RETURN INT;  
	  
	  FUNCTION ADJUST_EFF_EXP_DT_UI (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vLaneId                IN COMB_LANE.LANE_ID%TYPE,
      vRatingDtlSeq          IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vRatingDtlSeqUpd       IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vEffectiveDT           IN COMB_LANE_DTL.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN COMB_LANE_DTL.EXPIRATION_DT%TYPE,
      vCarrierCode           IN COMB_LANE_DTL.CARRIER_ID%TYPE,
      vMot                   IN COMB_LANE_DTL.MOT_ID%TYPE,
      vEquipment             IN COMB_LANE_DTL.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN COMB_LANE_DTL.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN COMB_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN COMB_LANE_DTL.PROTECTION_LEVEL_ID%TYPE,
      vOriginShipVia         IN COMB_LANE_DTL.O_SHIP_VIA%TYPE,
      vDestinationShipVia    IN COMB_LANE_DTL.D_SHIP_VIA%TYPE,
      vCustomText1           IN COMB_LANE_DTL.CUSTOM_TEXT1%TYPE,
      vCustomText2           IN COMB_LANE_DTL.CUSTOM_TEXT2%TYPE,
      vCustomText3           IN COMB_LANE_DTL.CUSTOM_TEXT3%TYPE,
      vCustomText4           IN COMB_LANE_DTL.CUSTOM_TEXT4%TYPE,
      vCustomText5           IN COMB_LANE_DTL.CUSTOM_TEXT5%TYPE,
      vuser                  IN comb_lane_dtl.last_updated_source%TYPE,
	  vVoyage				 IN COMB_LANE_DTL.VOYAGE%TYPE)
	  RETURN INT;

   PROCEDURE CREATE_NEW_ROW (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vUser           IN COMB_LANE_DTL.LAST_UPDATED_SOURCE%TYPE,
      vLaneId         IN COMB_LANE.LANE_ID%TYPE,
      vRatingDtlSeq   IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vEffectiveDT    IN COMB_LANE_DTL.EFFECTIVE_DT%TYPE,
      vExpirationDT   IN COMB_LANE_DTL.EXPIRATION_DT%TYPE);

   PROCEDURE REPLICATE_ROW (
      vTCCompanyId     IN COMPANY.COMPANY_ID%TYPE,
      vUser            IN COMB_LANE_DTL.LAST_UPDATED_SOURCE%TYPE,
      vLaneId          IN COMB_LANE.LANE_ID%TYPE,
      vOverlappedSeq   IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vRatingDtlSeq    IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vEffectiveDT     IN COMB_LANE_DTL.EFFECTIVE_DT%TYPE,
      vExpirationDT    IN COMB_LANE_DTL.EXPIRATION_DT%TYPE,
      vIsRating        IN COMB_LANE_DTL.IS_RATING%TYPE,
      vIsRouting       IN COMB_LANE_DTL.IS_ROUTING%TYPE,
      vIsSailing       IN COMB_LANE_DTL.IS_SAILING%TYPE,
	  TEMP_SEQ_NUM             OUT NUMBER);

   PROCEDURE DELETE_CHILD_ROW (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vLaneId         IN COMB_LANE.LANE_ID%TYPE,
      vRatingDtlSeq   IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE);

   PROCEDURE COPY_CHILD_ROW_NEW (
      vTCCompanyId       IN COMPANY.COMPANY_ID%TYPE,
      vLaneId            IN COMB_LANE.LANE_ID%TYPE,
      vOverlappedSeq     IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vRatingDtlSeq      IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vRatingDtlSeqNew   IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vIsRating          IN COMB_LANE_DTL.IS_RATING%TYPE,
      vIsRouting         IN COMB_LANE_DTL.IS_ROUTING%TYPE,
      vIsSailing         IN COMB_LANE_DTL.IS_SAILING%TYPE,
	  vuser              IN   comb_lane_dtl.last_updated_source%TYPE);  
	  
	PROCEDURE UPDATE_RATING_RATE_DATA (
	  vTCCompanyId       IN COMPANY.COMPANY_ID%TYPE,
      vLaneId            IN COMB_LANE.LANE_ID%TYPE,
      vRatingDtlSeqTemp     IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vRatingDtlSeqNew   IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE);
      
   PROCEDURE UPDATE_RATING_ACC_DATA (
	  vTCCompanyId       IN COMPANY.COMPANY_ID%TYPE,
      vLaneId            IN COMB_LANE.LANE_ID%TYPE,
      vRatingDtlSeqTemp     IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vRatingDtlSeqNew   IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE);		  
	  
   PROCEDURE COPY_CHILD_ROW (
      vTCCompanyId       IN COMPANY.COMPANY_ID%TYPE,
      vLaneId            IN COMB_LANE.LANE_ID%TYPE,
      vOverlappedSeq     IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vRatingDtlSeq      IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vRatingDtlSeqNew   IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vIsRating          IN COMB_LANE_DTL.IS_RATING%TYPE,
      vIsRouting         IN COMB_LANE_DTL.IS_ROUTING%TYPE,
      vIsSailing         IN COMB_LANE_DTL.IS_SAILING%TYPE,
	  vuser              IN   comb_lane_dtl.last_updated_source%TYPE);

   PROCEDURE REPLICATE_LANE_DETAIL (
	  laneDtlId             OUT NUMBER,
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vLaneId         IN COMB_LANE.LANE_ID%TYPE,
      vRatingDtlSeq   IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE);

   PROCEDURE ADJUST_IS_BUDGETED (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vLaneId                IN rating_lane.LANE_ID%TYPE,
      vRatingDtlSeq          IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vEffectiveDT           IN rating_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN rating_lane_dtl.EXPIRATION_DT%TYPE,
      vMot                   IN rating_lane_dtl.MOT_ID%TYPE,
      vEquipment             IN rating_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN rating_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN RATING_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN rating_lane_dtl.PROTECTION_LEVEL_ID%TYPE,
      vCustomText1           IN COMB_LANE_DTL.CUSTOM_TEXT1%TYPE,
      vCustomText2           IN COMB_LANE_DTL.CUSTOM_TEXT2%TYPE,
      vCustomText3           IN COMB_LANE_DTL.CUSTOM_TEXT3%TYPE,
      vCustomText4           IN COMB_LANE_DTL.CUSTOM_TEXT4%TYPE,
      vCustomText5           IN COMB_LANE_DTL.CUSTOM_TEXT5%TYPE);

   PROCEDURE INSERT_RATING_LANE_ERROR (
      vTCCompanyId         IN COMPANY.COMPANY_ID%TYPE,
      vLaneId              IN rating_lane.LANE_ID%TYPE,
      vErrorMsgDesc        IN RATING_LANE_ERRORS.ERROR_MSG_DESC%TYPE,
      p_error_msg_id       IN RATING_LANE_ERRORS.ERROR_MSG_ID%TYPE DEFAULT NULL,
      p_error_param_list   IN RATING_LANE_ERRORS.ERROR_PARAM_LIST%TYPE DEFAULT NULL);

   PROCEDURE INSERT_RATING_LANE_DTL_ERROR (
      vTCCompanyId         IN COMPANY.COMPANY_ID%TYPE,
      vLaneId              IN rating_lane.LANE_ID%TYPE,
      vRateLaneDtlSeq      IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vErrorMsgDesc        IN RATING_LANE_ERRORS.ERROR_MSG_DESC%TYPE,
      p_error_msg_id       IN RATING_LANE_ERRORS.ERROR_MSG_ID%TYPE DEFAULT NULL,
      p_error_param_list   IN RATING_LANE_ERRORS.ERROR_PARAM_LIST%TYPE DEFAULT NULL);

   PROCEDURE UPDATE_RATING_LANE_ERROR (
      vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
      vBatchId       IN RATING_LANE_ERRORS.BATCH_ID%TYPE);

   FUNCTION INSERT_RATING_LANE (
      vTCCompanyId      IN COMPANY.COMPANY_ID%TYPE,
      vUser             IN rating_lane.LAST_UPDATED_SOURCE%TYPE,
      vBusinessUnit     IN BUSINESS_UNIT.BUSINESS_UNIT%TYPE,
      vLaneHierarchy    IN rating_lane.LANE_HIERARCHY%TYPE,
      vOLocType         IN rating_lane.O_LOC_TYPE%TYPE,
      vOFacilityId      IN FACILITY.FACILITY_ID%TYPE,
      vOFacilityAlias   IN FACILITY_ALIAS.FACILITY_ALIAS_ID%TYPE,
      vOCity            IN rating_lane.O_CITY%TYPE,
      vOStateProv       IN STATE_PROV.STATE_PROV%TYPE,
      vOCounty          IN rating_lane.O_COUNTY%TYPE,
      vOPostal          IN POSTAL_CODE.POSTAL_CODE%TYPE,
      vOCountry         IN COUNTRY.COUNTRY_CODE%TYPE,
      vOZone            IN ZONE.ZONE_ID%TYPE,
      vDLocType         IN rating_lane.D_LOC_TYPE%TYPE,
      vDFacilityId      IN FACILITY.FACILITY_ID%TYPE,
      vDFacilityAlias   IN FACILITY_ALIAS.FACILITY_ALIAS_ID%TYPE,
      vDCity            IN rating_lane.D_CITY%TYPE,
      vDStateProv       IN STATE_PROV.STATE_PROV%TYPE,
      vDCounty          IN rating_lane.D_COUNTY%TYPE,
      vDPostal          IN POSTAL_CODE.POSTAL_CODE%TYPE,
      vDCountry         IN COUNTRY.COUNTRY_CODE%TYPE,
      vDZone            IN ZONE.ZONE_ID%TYPE,
      vBillingMethod    IN rating_lane.BILLING_METHOD%TYPE,
      vIncotermId       IN rating_lane.INCOTERM_ID%TYPE,
      vCustomerId       IN rating_lane.CUSTOMER_ID%TYPE,
      vRatingLaneId     IN rating_lane.LANE_ID%TYPE                    /*JMC*/
                                                   )
      RETURN NUMBER;

   FUNCTION CHECK_IS_BU_VALID (vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
                               vBUID          IN COMPANY.COMPANY_ID%TYPE)
      RETURN NUMBER;

   FUNCTION CHECK_IS_FAC_BU_VALID (vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
                                   vBUID          IN COMPANY.COMPANY_ID%TYPE)
      RETURN NUMBER;

   FUNCTION INSERT_RATING_LANE_DTL (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vUser                  IN rating_lane.LAST_UPDATED_SOURCE%TYPE,
      vRatingLaneId          IN rating_lane.LANE_ID%TYPE,
      vCarrierCode           IN rating_lane_dtl.CARRIER_ID%TYPE,
      vMot                   IN rating_lane_dtl.MOT_ID%TYPE,
      vEquipment             IN rating_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN rating_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN RATING_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN rating_lane_dtl.PROTECTION_LEVEL_ID%TYPE,
      vIsBudgeted            IN rating_lane_dtl.IS_BUDGETED%TYPE,
      vEffectiveDT           IN rating_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN rating_lane_dtl.EXPIRATION_DT%TYPE,
      vImpRateDtlStatus      IN IMPORT_RATING_LANE_DTL.IMPORT_RATING_DTL_STATUS%TYPE,
      vOriginShipVia         IN rating_lane_dtl.O_SHIP_VIA%TYPE,
      vDestinationShipVia    IN rating_lane_dtl.D_SHIP_VIA%TYPE,
      vContractNumber        IN rating_lane_dtl.CONTRACT_NUMBER%TYPE,
      vCustomText1           IN rating_lane_dtl.CUSTOM_TEXT1%TYPE,
      vCustomText2           IN rating_lane_dtl.CUSTOM_TEXT2%TYPE,
      vCustomText3           IN rating_lane_dtl.CUSTOM_TEXT3%TYPE,
      vCustomText4           IN rating_lane_dtl.CUSTOM_TEXT4%TYPE,
      vCustomText5           IN rating_lane_dtl.CUSTOM_TEXT5%TYPE,
      vPackageId             IN rating_lane_dtl.PACKAGE_ID%TYPE,
      vPackageName           IN rating_lane_dtl.PACKAGE_NAME%TYPE,
	  vVoyage				 IN rating_lane_dtl.VOYAGE%TYPE)
      RETURN NUMBER;

   PROCEDURE INSERT_RATING_LANE_DTL_RATE (
      vTCCompanyId             IN COMPANY.COMPANY_ID%TYPE,
      vRatingLaneId            IN rating_lane.LANE_ID%TYPE,
      vRatingLaneDtlId         IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vMinSize                 IN RATING_LANE_DTL_RATE.MINIMUM_SIZE%TYPE,
      vMaxSize                 IN RATING_LANE_DTL_RATE.MAXIMUM_SIZE%TYPE,
      vSizeUOM                 IN RATING_LANE_DTL_RATE.SIZE_UOM_ID%TYPE,
      vRateCalcMethod          IN RATING_LANE_DTL_RATE.RATE_CALC_METHOD%TYPE,
      vRateUOM                 IN RATING_LANE_DTL_RATE.RATE_UOM%TYPE,
      vRate                    IN RATING_LANE_DTL_RATE.RATE%TYPE,
      vMinRate                 IN RATING_LANE_DTL_RATE.MINIMUM_RATE%TYPE,
      vCurrencyCode            IN RATING_LANE_DTL_RATE.CURRENCY_CODE%TYPE,
      vTariffId                IN RATING_LANE_DTL_RATE.TARIFF_ID%TYPE,
      vMinDistance             IN RATING_LANE_DTL_RATE.MIN_DISTANCE%TYPE,
      vMaxDistance             IN RATING_LANE_DTL_RATE.MAX_DISTANCE%TYPE,
      vDistanceUOM             IN RATING_LANE_DTL_RATE.DISTANCE_UOM%TYPE,
      vSupportsMSTL            IN RATING_LANE_DTL_RATE.SUPPORTS_MSTL%TYPE,
      vHasBH                   IN RATING_LANE_DTL_RATE.HAS_BH%TYPE,
      vMinCommodity            IN RATING_LANE_DTL_RATE.MIN_COMMODITY%TYPE,
      vMaxCommodity            IN RATING_LANE_DTL_RATE.MAX_COMMODITY%TYPE,
      vHasRT                   IN RATING_LANE_DTL_RATE.HAS_RT%TYPE,
      vParcelTier              IN RATING_LANE_DTL_RATE.PARCEL_TIER%TYPE,
      vCommodityCodeId         IN RATING_LANE_DTL_RATE.COMMODITY_CODE_ID%TYPE,
      vExcessWtRate            IN RATING_LANE_DTL_RATE.EXCESS_WT_RATE%TYPE,
      vPayeeCarrier            IN RATING_LANE_DTL_RATE.PAYEE_CARRIER_ID%TYPE,
      vMaxCommodityCodeId      IN RATING_LANE_DTL_RATE.MAX_RANGE_COMMODITY_CODE_ID%TYPE,
      VIsSurgeRate             IN RATING_LANE_DTL_RATE.IS_SURGE_RATE%TYPE,
      vMinRateType             IN RATING_LANE_DTL_RATE.MINIMUM_RATE_TYPE%TYPE,
      vBaseRate                IN RATING_LANE_DTL_RATE.BASE_RATE%TYPE,
      vMaxRate                 IN RATING_LANE_DTL_RATE.MAXIMUM_RATE%TYPE,
      vCustomerMinCharge       IN RATING_LANE_DTL_RATE.CUSTOMER_MIN_CHARGE%TYPE,
      vCustomerBaseRate        IN RATING_LANE_DTL_RATE.CUSTOMER_BASE_RATE%TYPE,
      vCustomerSurchargeRate   IN RATING_LANE_DTL_RATE.CUSTOMER_SURCHARGE_RATE%TYPE);

   PROCEDURE INSERT_RATING_LANE_ACCESSORIAL (
      vTCCompanyId             IN COMPANY.COMPANY_ID%TYPE,
      vRatingLaneId            IN rating_lane.LANE_ID%TYPE,
      vRatingLaneDtlId         IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vAccessorialCode         IN LANE_ACCESSORIAL.ACCESSORIAL_ID%TYPE,
      vRate                    IN LANE_ACCESSORIAL.RATE%TYPE,
      vMinRate                 IN LANE_ACCESSORIAL.MINIMUM_RATE%TYPE,
      vCurrencyCode            IN LANE_ACCESSORIAL.CURRENCY_CODE%TYPE,
      p_minimum_size           IN LANE_ACCESSORIAL.MINIMUM_SIZE%TYPE,
      p_maximum_size           IN LANE_ACCESSORIAL.MAXIMUM_SIZE%TYPE,
      p_size_uom               IN LANE_ACCESSORIAL.SIZE_UOM_ID%TYPE,
      vIsApproved              IN LANE_ACCESSORIAL.IS_AUTO_APPROVE%TYPE,
      vEffectiveDT             IN LANE_ACCESSORIAL.EFFECTIVE_DT%TYPE,
      vExpirationDT            IN LANE_ACCESSORIAL.EXPIRATION_DT%TYPE,
      vPayeeCarrierCode        IN LANE_ACCESSORIAL.PAYEE_CARRIER_ID%TYPE,
      vIsShipmentAccessorial   IN LANE_ACCESSORIAL.IS_SHIPMENT_ACCESSORIAL%TYPE,
      vMinRateType             IN LANE_ACCESSORIAL.Min_Rate_Type%TYPE, -- MACR00159593 starts
      vMaximumRate             IN LANE_ACCESSORIAL.Maximum_Rate%TYPE,
      vPackageType             IN LANE_ACCESSORIAL.PACKAGE_TYPE_ID%TYPE,
      vCustomerRate            IN LANE_ACCESSORIAL.CUSTOMER_RATE%TYPE,
      vCustomerMinCharge       IN LANE_ACCESSORIAL.Customer_Min_Charge%TYPE,
      vCalculatedRate          IN LANE_ACCESSORIAL.Calculated_Rate%TYPE,
      vAmount                  IN LANE_ACCESSORIAL.Amount%TYPE,
      vBaseAmount              IN LANE_ACCESSORIAL.BASE_AMOUNT%TYPE,
      vBaseCharge              IN LANE_ACCESSORIAL.Base_Charge%TYPE,
      vRangeMinAmount          IN LANE_ACCESSORIAL.RANGE_MIN_AMOUNT%TYPE,
      vRangeMaxAmount          IN LANE_ACCESSORIAL.RANGE_MAX_AMOUNT%TYPE,
      vIncrement               IN LANE_ACCESSORIAL.Increment_Val%TYPE,
      vMot                     IN LANE_ACCESSORIAL.Mot_Id%TYPE,
      vEquipment               IN LANE_ACCESSORIAL.Equipment_Id%TYPE,
      vServiceLevel            IN LANE_ACCESSORIAL.Service_Level_Id%TYPE,
      vProtectionLevel         IN LANE_ACCESSORIAL.PROTECTION_LEVEL_ID%TYPE,
      vBillingMethod           IN LANE_ACCESSORIAL.BILLING_METHOD%TYPE,
      vMinLongestDim           IN LANE_ACCESSORIAL.Min_Longest_Dimension%TYPE,
      vMaxLongestDim           IN LANE_ACCESSORIAL.Max_Longest_Dimension%TYPE,
      vMinSecondLongestDim     IN LANE_ACCESSORIAL.Min_Second_Longest_Dimension%TYPE,
      vMaxSecondLongestDim     IN LANE_ACCESSORIAL.Max_Second_Longest_Dimension%TYPE,
      vMinThirdLongestDim      IN LANE_ACCESSORIAL.Min_Third_Longest_Dimension%TYPE,
      vMaxThirdLongestDim      IN LANE_ACCESSORIAL.Max_Third_Longest_Dimension%TYPE,
      vMinLengthGrith          IN LANE_ACCESSORIAL.Min_Length_Plus_Grith%TYPE,
      vMaxLengthGrith          IN LANE_ACCESSORIAL.Max_Length_Plus_Grith%TYPE,
      vMinWeight               IN LANE_ACCESSORIAL.Min_Weight%TYPE,
      vMaxWeight               IN LANE_ACCESSORIAL.Max_Weight%TYPE,
      vIsZoneStopOff           IN LANE_ACCESSORIAL.Is_Zone_Stopoff%TYPE,
      vZoneId                  IN LANE_ACCESSORIAL.Zone_Id%TYPE, -- MACR00159593 ends
                                                               /*  vLaneAccId         IN LANE_ACCESSORIAL.LANE_ACCESSORIAL_ID%TYPE */
	  vCommID				   IN LANE_ACCESSORIAL.COMMODITY_CODE_ID%TYPE, -- CMGT-615 Starts here
	  vMaxCommId			   IN LANE_ACCESSORIAL.MAX_RANGE_COMMODITY_CODE_ID%TYPE -- CMGT-615 Ends here
															   
      );

   PROCEDURE MERGE_DRAFT (
      vTCCompanyId     IN     COMPANY.COMPANY_ID%TYPE,
      vUser            IN     COMB_LANE_DTL.LAST_UPDATED_SOURCE%TYPE,
      vLaneId          IN     COMB_LANE.LANE_ID%TYPE,
      vLaneDtlSeq      IN     COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vLaneDtlSeqUpd   IN     COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      REC_CNT             OUT NUMBER);

   PROCEDURE CHECK_DUPLICATE_LANE_DTL (
      VLANEID               IN     COMB_LANE.LANE_ID%TYPE,
      CARRIERID             IN     COMB_LANE_DTL.CARRIER_ID%TYPE,
      MOTID                 IN     COMB_LANE_DTL.MOT_ID%TYPE,
      SLID                  IN     COMB_LANE_DTL.SERVICE_LEVEL_ID%TYPE,
      EQID                  IN     COMB_LANE_DTL.EQUIPMENT_ID%TYPE,
      PLID                  IN     COMB_LANE_DTL.PROTECTION_LEVEL_ID%TYPE,
      SCCARRIERID           IN     COMB_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      VORIGINSHIPVIA        IN     COMB_LANE_DTL.O_SHIP_VIA%TYPE,
      VDESTINATIONSHIPVIA   IN     COMB_LANE_DTL.D_SHIP_VIA%TYPE,
      VCUSTOMTEXT1          IN     COMB_LANE_DTL.CUSTOM_TEXT1%TYPE,
      VCUSTOMTEXT2          IN     COMB_LANE_DTL.CUSTOM_TEXT2%TYPE,
      VCUSTOMTEXT3          IN     COMB_LANE_DTL.CUSTOM_TEXT3%TYPE,
      VCUSTOMTEXT4          IN     COMB_LANE_DTL.CUSTOM_TEXT4%TYPE,
      VCUSTOMTEXT5          IN     COMB_LANE_DTL.CUSTOM_TEXT5%TYPE,
      VCOUNT                   OUT INTEGER,
	  vVOYAGE				IN		COMB_LANE_DTL.VOYAGE%TYPE);

   FUNCTION DELETE_IMP_RATING_LANES (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vImportLaneID   IN IMPORT_RATING_LANE.LANE_ID%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_HIERARCHY (
      vTCCompanyId       IN COMPANY.COMPANY_ID%TYPE,
      vLaneId            IN rating_lane.LANE_ID%TYPE,
      oFacilityId        IN rating_lane.O_FACILITY_ID%TYPE,
      oFacilityAliasId   IN rating_lane.O_FACILITY_ALIAS_ID%TYPE,
      oCity              IN rating_lane.O_CITY%TYPE,
      oState             IN rating_lane.O_STATE_PROV%TYPE,
      oCounty            IN rating_lane.O_COUNTY%TYPE,
      oPostal            IN rating_lane.O_POSTAL_CODE%TYPE,
      oCountry           IN rating_lane.O_COUNTRY_CODE%TYPE,
      oZoneId            IN rating_lane.O_ZONE_ID%TYPE,
      oZoneName          IN IMPORT_RATING_LANE.o_Zone_Name%TYPE,
      dFacilityId        IN rating_lane.D_FACILITY_ID%TYPE,
      dFacilityAliasId   IN rating_lane.D_FACILITY_ALIAS_ID%TYPE,
      dCity              IN rating_lane.D_CITY%TYPE,
      dState             IN rating_lane.D_STATE_PROV%TYPE,
      dCounty            IN rating_lane.D_COUNTY%TYPE,
      dPostal            IN rating_lane.D_POSTAL_CODE%TYPE,
      dCountry           IN rating_lane.D_COUNTRY_CODE%TYPE,
      dZoneId            IN rating_lane.D_ZONE_ID%TYPE,
      dZoneName          IN IMPORT_RATING_LANE.d_Zone_Name%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_LANE_BASE_DATA (
      vTCCompanyId         IN COMPANY.COMPANY_ID%TYPE,
      vLaneId              IN rating_lane.LANE_ID%TYPE,
      vBusinessUnit        IN BUSINESS_UNIT.BUSINESS_UNIT%TYPE,
      vOFacilityId         IN FACILITY.FACILITY_ID%TYPE,
      vOFacilityAlias      IN FACILITY_ALIAS.FACILITY_ALIAS_ID%TYPE,
      vOFacilityBU         IN BUSINESS_UNIT.BUSINESS_UNIT%TYPE,
      vOStateProv          IN STATE_PROV.STATE_PROV%TYPE,
      vOPostal             IN POSTAL_CODE.POSTAL_CODE%TYPE,
      vOCountry            IN COUNTRY.COUNTRY_CODE%TYPE,
      vOZoneId             IN ZONE.ZONE_ID%TYPE,
      vOZoneName           IN IMPORT_RATING_LANE.O_ZONE_NAME%TYPE,
      vDFacilityId         IN FACILITY.FACILITY_ID%TYPE,
      vDFacilityAlias      IN FACILITY_ALIAS.FACILITY_ALIAS_ID%TYPE,
      vDFacilityBU         IN BUSINESS_UNIT.BUSINESS_UNIT%TYPE,
      vDStateProv          IN STATE_PROV.STATE_PROV%TYPE,
      vDPostal             IN POSTAL_CODE.POSTAL_CODE%TYPE,
      vDCountry            IN COUNTRY.COUNTRY_CODE%TYPE,
      vDZoneId             IN ZONE.ZONE_ID%TYPE,
      vDZoneName           IN IMPORT_RATING_LANE.D_ZONE_NAME%TYPE,
      vCustomerId          IN rating_lane.CUSTOMER_ID%TYPE,
      vCustomerCode        IN IMPORT_RATING_LANE.CUSTOMER_CODE%TYPE,
      vCustomerCodeBU      IN IMPORT_RATING_LANE.CUSTOMER_CODE_BU%TYPE,
      vBillingMethod       IN rating_lane.BILLING_METHOD%TYPE,
      vBillingMethodCode   IN IMPORT_RATING_LANE.BILLING_METHOD_CODE%TYPE,
      vIncotermId          IN rating_lane.INCOTERM_ID%TYPE,
      vIncotermName        IN IMPORT_RATING_LANE.INCOTERM_NAME%TYPE,
      vIncotermNameBU      IN IMPORT_RATING_LANE.INCOTERM_NAME_BU%TYPE)
      RETURN NUMBER;

   FUNCTION GET_BUID_FROM_BUSINESS_UNIT (
      vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
      vBUColumn         VARCHAR2)
      RETURN NUMBER;

   FUNCTION VALIDATE_DETAIL_BASE_DATA (
      vTCCompanyId               IN COMPANY.COMPANY_ID%TYPE,
      vLaneId                    IN rating_lane.LANE_ID%TYPE,
      vRatingDtlSeq              IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vCarrierCode               IN IMPORT_RATING_LANE_DTL.CARRIER_CODE%TYPE,
      vCarrierCodeBUID           IN COMPANY.COMPANY_ID%TYPE,
      vMode                      IN IMPORT_RATING_LANE_DTL.MOT%TYPE,
      vModeBUID                  IN COMPANY.COMPANY_ID%TYPE,
      vServiceLevel              IN IMPORT_RATING_LANE_DTL.SERVICE_LEVEL%TYPE,
      vServiceLevelBUID          IN COMPANY.COMPANY_ID%TYPE,
      vEquipment                 IN IMPORT_RATING_LANE_DTL.EQUIPMENT_CODE%TYPE,
      vEquipmentBUID             IN COMPANY.COMPANY_ID%TYPE,
      vProtectionLevel           IN IMPORT_RATING_LANE_DTL.PROTECTION_LEVEL%TYPE,
      vProtectionLevelBUID       IN COMPANY.COMPANY_ID%TYPE,
      p_scndr_carrier_code       IN IMPORT_RATING_LANE_DTL.SCNDR_CARRIER_CODE%TYPE,
      p_scndr_carrier_codeBUID   IN COMPANY.COMPANY_ID%TYPE,
      vIsBudgeted                IN rating_lane_dtl.IS_BUDGETED%TYPE,
      vEffectiveDT               IN rating_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT              IN rating_lane_dtl.EXPIRATION_DT%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_TP_EQUIP_SERV_MODE (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vLaneId                IN rating_lane.LANE_ID%TYPE,
      vRatingDtlSeq          IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vCarrierCode           IN CARRIER_CODE.CARRIER_CODE%TYPE,
      vServiceLevel          IN SERVICE_LEVEL.SERVICE_LEVEL%TYPE,
      vEquipment             IN EQUIPMENT.EQUIPMENT_CODE%TYPE,
      vMode                  IN MOT.MOT%TYPE,
      p_scndr_carrier_code   IN CARRIER_CODE.CARRIER_CODE%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_FEASIBLE (
      vTCCompanyId               IN COMPANY.COMPANY_ID%TYPE,
      vLaneId                    IN rating_lane.LANE_ID%TYPE,
      vRatingDtlSeq              IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vCarrierCode               IN CARRIER_CODE.CARRIER_CODE%TYPE,
      vCarrierCodeBUID           IN COMPANY.COMPANY_ID%TYPE,
      vEquipment                 IN EQUIPMENT.EQUIPMENT_CODE%TYPE,
      vEquipmentBUID             IN COMPANY.COMPANY_ID%TYPE,
      vProtectionLevel           IN PROTECTION_LEVEL.PROTECTION_LEVEL%TYPE,
      vProtectionLevelBUID       IN COMPANY.COMPANY_ID%TYPE,
      p_scndr_carrier_code       IN CARRIER_CODE.CARRIER_CODE%TYPE,
      p_scndr_carrier_codeBUID   IN COMPANY.COMPANY_ID%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_FAC_CARR_FEASIBLE (
      vTCCompanyId               IN COMPANY.COMPANY_ID%TYPE,
      vLaneId                    IN rating_lane.LANE_ID%TYPE,
      vRatingDtlSeq              IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vCarrierCode               IN CARRIER_CODE.CARRIER_CODE%TYPE,
      vCarrierCodeBUID           IN COMPANY.COMPANY_ID%TYPE,
      p_scndr_carrier_code       IN CARRIER_CODE.CARRIER_CODE%TYPE,
      p_scndr_carrier_codeBUID   IN COMPANY.COMPANY_ID%TYPE)
      RETURN NUMBER;

   FUNCTION GET_BUDG_RESOURCE_WHERE_CLAUSE (
      vMot                   IN rating_lane_dtl.MOT_ID%TYPE,
      vEquipment             IN rating_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN rating_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN RATING_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN rating_lane_dtl.PROTECTION_LEVEL_ID%TYPE)
      RETURN VARCHAR2;

   FUNCTION GET_BUDG_RESOURCE_WHERE_CLAUSE (
      vMot                   IN rating_lane_dtl.MOT_ID%TYPE,
      vEquipment             IN rating_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN rating_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN RATING_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN rating_lane_dtl.PROTECTION_LEVEL_ID%TYPE,
      vCustomText1           IN COMB_LANE_DTL.CUSTOM_TEXT1%TYPE,
      vCustomText2           IN COMB_LANE_DTL.CUSTOM_TEXT2%TYPE,
      vCustomText3           IN COMB_LANE_DTL.CUSTOM_TEXT3%TYPE,
      vCustomText4           IN COMB_LANE_DTL.CUSTOM_TEXT4%TYPE,
      vCustomText5           IN COMB_LANE_DTL.CUSTOM_TEXT5%TYPE)
      RETURN VARCHAR2;

   FUNCTION GET_RESOURCE_WHERE_CLAUSE (
      vCarrierCode           IN rating_lane_dtl.CARRIER_ID%TYPE,
      vMot                   IN rating_lane_dtl.MOT_ID%TYPE,
      vEquipment             IN rating_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN rating_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN RATING_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN rating_lane_dtl.PROTECTION_LEVEL_ID%TYPE,
      vOriginShipVia         IN rating_lane_dtl.O_SHIP_VIA%TYPE,
      vDestinationShipVia    IN rating_lane_dtl.D_SHIP_VIA%TYPE,
      vCustomText1           IN COMB_LANE_DTL.CUSTOM_TEXT1%TYPE,
      vCustomText2           IN COMB_LANE_DTL.CUSTOM_TEXT2%TYPE,
      vCustomText3           IN COMB_LANE_DTL.CUSTOM_TEXT3%TYPE,
      vCustomText4           IN COMB_LANE_DTL.CUSTOM_TEXT4%TYPE,
      vCustomText5           IN COMB_LANE_DTL.CUSTOM_TEXT5%TYPE,
	  vVoyage				 IN COMB_LANE_DTL.VOYAGE%TYPE)
      RETURN VARCHAR2;

   FUNCTION GET_RESOURCE_WHERE_CLAUSE (
      vCarrierCode           IN rating_lane_dtl.CARRIER_ID%TYPE,
      vMot                   IN rating_lane_dtl.MOT_ID%TYPE,
      vEquipment             IN rating_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN rating_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN RATING_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN rating_lane_dtl.PROTECTION_LEVEL_ID%TYPE,
      vOriginShipVia         IN rating_lane_dtl.O_SHIP_VIA%TYPE,
      vDestinationShipVia    IN rating_lane_dtl.D_SHIP_VIA%TYPE)
      RETURN VARCHAR2;

   FUNCTION CHECK_DUP_LANE_DTL_NO_OVERLAP (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vRatingLaneId          IN rating_lane.LANE_ID%TYPE,
      vratinglanedtlseq      IN rating_lane_dtl.rating_lane_dtl_seq%TYPE,
      vCarrierCode           IN rating_lane_dtl.CARRIER_ID%TYPE,
      vMot                   IN rating_lane_dtl.MOT_ID%TYPE,
      vEquipment             IN rating_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN rating_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN RATING_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN rating_lane_dtl.PROTECTION_LEVEL_ID%TYPE,
      vEffectiveDT           IN rating_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN rating_lane_dtl.EXPIRATION_DT%TYPE,
      vOriginShipVia         IN rating_lane_dtl.O_SHIP_VIA%TYPE,
      vDestinationShipVia    IN rating_lane_dtl.D_SHIP_VIA%TYPE,
      vCustomText1           IN rating_lane_dtl.CUSTOM_TEXT1%TYPE,
      vCustomText2           IN rating_lane_dtl.CUSTOM_TEXT2%TYPE,
      vCustomText3           IN rating_lane_dtl.CUSTOM_TEXT3%TYPE,
      vCustomText4           IN rating_lane_dtl.CUSTOM_TEXT4%TYPE,
      vCustomText5           IN rating_lane_dtl.CUSTOM_TEXT5%TYPE,
      vPackageId             IN rating_lane_dtl.PACKAGE_ID%TYPE,
      vPackageName           IN rating_lane_dtl.PACKAGE_NAME%TYPE,
	  vVoyage				 IN rating_lane_dtl.VOYAGE%TYPE)
      RETURN NUMBER;

   PROCEDURE AUDIT_RATING_LANE_DTL_RATE (
      vTCCompanyId             IN COMPANY.COMPANY_ID%TYPE,
      vRatingLaneId            IN rating_lane.LANE_ID%TYPE,
      vRatingLaneDtlId         IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vMinSize                 IN RATING_LANE_DTL_RATE.MINIMUM_SIZE%TYPE,
      vMaxSize                 IN RATING_LANE_DTL_RATE.MAXIMUM_SIZE%TYPE,
      vSizeUOM                 IN RATING_LANE_DTL_RATE.SIZE_UOM_ID%TYPE,
      vRateCalcMethod          IN RATING_LANE_DTL_RATE.RATE_CALC_METHOD%TYPE,
      vRateUOM                 IN RATING_LANE_DTL_RATE.RATE_UOM%TYPE,
      vRate                    IN RATING_LANE_DTL_RATE.RATE%TYPE,
      vMinRate                 IN RATING_LANE_DTL_RATE.MINIMUM_RATE%TYPE,
      vCurrencyCode            IN RATING_LANE_DTL_RATE.CURRENCY_CODE%TYPE,
      vTariffId                IN RATING_LANE_DTL_RATE.TARIFF_ID%TYPE,
      vMinDistance             IN RATING_LANE_DTL_RATE.MIN_DISTANCE%TYPE,
      vMaxDistance             IN RATING_LANE_DTL_RATE.MAX_DISTANCE%TYPE,
      vDistanceUOM             IN RATING_LANE_DTL_RATE.DISTANCE_UOM%TYPE,
      vSupportsMSTL            IN RATING_LANE_DTL_RATE.SUPPORTS_MSTL%TYPE,
      vHasBH                   IN RATING_LANE_DTL_RATE.HAS_BH%TYPE,
      vMinCommodity            IN RATING_LANE_DTL_RATE.MIN_COMMODITY%TYPE,
      vMaxCommodity            IN RATING_LANE_DTL_RATE.MAX_COMMODITY%TYPE,
      vHasRT                   IN RATING_LANE_DTL_RATE.HAS_RT%TYPE,
      vParcelTier              IN RATING_LANE_DTL_RATE.PARCEL_TIER%TYPE,
      vCommodityCodeId         IN RATING_LANE_DTL_RATE.COMMODITY_CODE_ID%TYPE,
      vExcessWtRate            IN RATING_LANE_DTL_RATE.EXCESS_WT_RATE%TYPE,
      vPayeeCarrier            IN RATING_LANE_DTL_RATE.PAYEE_CARRIER_ID%TYPE,
      vMaxCommodityCodeId      IN RATING_LANE_DTL_RATE.MAX_RANGE_COMMODITY_CODE_ID%TYPE,
      VIsSurgeRate             IN RATING_LANE_DTL_RATE.IS_SURGE_RATE%TYPE,
      -- sysco merge
      vMinRateType             IN RATING_LANE_DTL_RATE.MINIMUM_RATE_TYPE%TYPE,
      vBaseRate                IN RATING_LANE_DTL_RATE.BASE_RATE%TYPE,
      vMaxRate                 IN RATING_LANE_DTL_RATE.MAXIMUM_RATE%TYPE,
      vCustomerMinCharge       IN RATING_LANE_DTL_RATE.CUSTOMER_MIN_CHARGE%TYPE,
      vCustomerBaseRate        IN RATING_LANE_DTL_RATE.CUSTOMER_BASE_RATE%TYPE,
      vCustomerSurchargeRate   IN RATING_LANE_DTL_RATE.CUSTOMER_SURCHARGE_RATE%TYPE,
      -- sysco merge
      vLastUpdatedSrcType      IN RATING_LANE_DTL_RATE.LAST_UPDATED_SRC_TYPE%TYPE,
      vLastUpdatedSrc          IN RATING_LANE_DTL_RATE.LAST_UPDATED_SRC%TYPE,
      vLastUpdatedDttm         IN RATING_LANE_DTL_RATE.LAST_UPDATED_DTTM%TYPE,
      vDuplicateLaneDtlId      IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE);

   FUNCTION CHECK_DUP_LANE_ACC (
      vTCCompanyId          IN COMPANY.COMPANY_ID%TYPE,
      vRatingLaneId         IN rating_lane.LANE_ID%TYPE,
      vDuplicateLaneDtlId   IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vAccessorialId        IN lane_accessorial.ACCESSORIAL_ID%TYPE,
      vEffectiveDT          IN rating_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT         IN rating_lane_dtl.EXPIRATION_DT%TYPE)
      RETURN NUMBER;

   PROCEDURE AUDIT_LANE_ACCESSORIAL (
      vTCCompanyId             IN COMPANY.COMPANY_ID%TYPE,
      vRatingLaneId            IN rating_lane.LANE_ID%TYPE,
      vRatingLaneDtlId         IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vAccessorialCode         IN LANE_ACCESSORIAL.ACCESSORIAL_ID%TYPE,
      vRate                    IN LANE_ACCESSORIAL.RATE%TYPE,
      vMinRate                 IN LANE_ACCESSORIAL.MINIMUM_RATE%TYPE,
      vCurrencyCode            IN LANE_ACCESSORIAL.CURRENCY_CODE%TYPE,
      p_minimum_size           IN LANE_ACCESSORIAL.MINIMUM_SIZE%TYPE,
      p_maximum_size           IN LANE_ACCESSORIAL.MAXIMUM_SIZE%TYPE,
      p_size_uom               IN LANE_ACCESSORIAL.SIZE_UOM_ID%TYPE,
      vIsApproved              IN LANE_ACCESSORIAL.IS_AUTO_APPROVE%TYPE,
      vEffectiveDT             IN LANE_ACCESSORIAL.EFFECTIVE_DT%TYPE,
      vExpirationDT            IN LANE_ACCESSORIAL.EXPIRATION_DT%TYPE,
      vPayeeCarrierCode        IN LANE_ACCESSORIAL.PAYEE_CARRIER_ID%TYPE,
      vIsShipmentAccessorial   IN LANE_ACCESSORIAL.IS_SHIPMENT_ACCESSORIAL%TYPE,
      vMinRateType                LANE_ACCESSORIAL.Min_Rate_Type%TYPE, -- MACR00159593 starts
      vMaximumRate                LANE_ACCESSORIAL.Maximum_Rate%TYPE,
      vPackageType                LANE_ACCESSORIAL.PACKAGE_TYPE_ID%TYPE,
      vCustomerRate               LANE_ACCESSORIAL.CUSTOMER_RATE%TYPE,
      vCustomerMinCharge          LANE_ACCESSORIAL.Customer_Min_Charge%TYPE,
      vCalculatedRate             LANE_ACCESSORIAL.Calculated_Rate%TYPE,
      vAmount                     LANE_ACCESSORIAL.Amount%TYPE,
      vBaseAmount                 LANE_ACCESSORIAL.BASE_AMOUNT%TYPE,
      vBaseCharge                 LANE_ACCESSORIAL.Base_Charge%TYPE,
      vRangeMinAmount             LANE_ACCESSORIAL.RANGE_MIN_AMOUNT%TYPE,
      vRangeMaxAmount             LANE_ACCESSORIAL.RANGE_MAX_AMOUNT%TYPE,
      vIncrement                  LANE_ACCESSORIAL.Increment_Val%TYPE,
      vMot                        LANE_ACCESSORIAL.Mot_Id%TYPE,
      vEquipment                  LANE_ACCESSORIAL.Equipment_Id%TYPE,
      vServiceLevel               LANE_ACCESSORIAL.Service_Level_Id%TYPE,
      vProtectionLevel            LANE_ACCESSORIAL.PROTECTION_LEVEL_ID%TYPE,
      vBillingMethod              LANE_ACCESSORIAL.Billing_Method%TYPE,
      vMinLongestDim              LANE_ACCESSORIAL.Min_Longest_Dimension%TYPE,
      vMaxLongestDim              LANE_ACCESSORIAL.Max_Longest_Dimension%TYPE,
      vMinSecondLongestDim        LANE_ACCESSORIAL.Min_Second_Longest_Dimension%TYPE,
      vMaxSecondLongestDim        LANE_ACCESSORIAL.Max_Second_Longest_Dimension%TYPE,
      vMinThirdLongestDim         LANE_ACCESSORIAL.Min_Third_Longest_Dimension%TYPE,
      vMaxThirdLongestDim         LANE_ACCESSORIAL.Max_Third_Longest_Dimension%TYPE,
      vMinLengthGrith             LANE_ACCESSORIAL.Min_Length_Plus_Grith%TYPE,
      vMaxLengthGrith             LANE_ACCESSORIAL.Max_Length_Plus_Grith%TYPE,
      vMinWeight                  LANE_ACCESSORIAL.Min_Weight%TYPE,
      vMaxWeight                  LANE_ACCESSORIAL.Max_Weight%TYPE,
      vIsZoneStopOff              LANE_ACCESSORIAL.Is_Zone_Stopoff%TYPE,
      vZoneId                     LANE_ACCESSORIAL.Zone_Id%TYPE, -- MACR00159593 ends
      vLastUpdatedSrcType      IN RATING_LANE_DTL_RATE.LAST_UPDATED_SRC_TYPE%TYPE,
      vLastUpdatedSrc          IN RATING_LANE_DTL_RATE.LAST_UPDATED_SRC%TYPE,
      vDuplicateLaneDtlId      IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE);


   FUNCTION VALIDATE_CUST_CARR_MOT_FEAS (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vRatingLaneId      IN rating_lane.LANE_ID%TYPE,
      vRatingLaneDtlId   IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vCarrierCode       IN carrier_code.CARRIER_CODE%TYPE,
      vMot               IN MOT.MOT%TYPE)
      RETURN NUMBER;
	  
   FUNCTION VALIDATE_ACC_FEASBLE_INCOTERMS (
	  vTCCompanyId		 IN COMPANY.COMPANY_ID%TYPE,
	  vRatingLaneId      IN RATING_LANE.LANE_ID%TYPE,
	  vRatingLaneDtlId   IN RATING_LANE_DTL.RATING_LANE_DTL_SEQ%TYPE,
	  vLaneAccId         IN LANE_ACCESSORIAL.LANE_ACCESSORIAL_ID%TYPE ) 
   RETURN NUMBER;
   
   -----------------------Added here for INfeasibility between SL and AOPTG------------------------------------------------------
 FUNCTION VAL_SL_FEASBLE_ACCOPTGROUP
(
   vTCCompanyId			IN COMPANY.COMPANY_ID%TYPE,
   vRatingLaneId    IN RATING_LANE.LANE_ID%TYPE,
   vRatingLaneDtlId   IN RATING_LANE_DTL.RATING_LANE_DTL_SEQ%TYPE,
   vAccId   IN    ACCESSORIAL_CODE.ACCESSORIAL_ID%TYPE,
   vServLevelId IN IMPORT_LANE_ACCESSORIAL.SERVICE_LEVEL_ID%TYPE
)
  RETURN NUMBER;

  
   PROCEDURE TRSFR_LANE_ACC_FEAS_INCOTERM (
	  vTCCompanyId			IN COMPANY.COMPANY_ID%TYPE,
      vImpRatingLaneId      IN RATING_LANE.LANE_ID%TYPE,
	  vImpRatingLaneDtlId   IN RATING_LANE_DTL.RATING_LANE_DTL_SEQ%TYPE,
	  vImpLaneAccId         IN LANE_ACCESSORIAL.LANE_ACCESSORIAL_ID%TYPE,
      vLaneAccId            IN LANE_ACCESSORIAL.LANE_ACCESSORIAL_ID%TYPE );
	  
   PROCEDURE VALIDATE_LANE_DTL_EPI (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vImportLaneId   IN RATING_LANE.LANE_ID%TYPE,
      vLaneId         IN RATING_LANE.LANE_ID%TYPE);
	  
	PROCEDURE REPLICATE_RG_SA_LANE_DETAIL (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vLaneId        IN comb_lane.LANE_ID%TYPE,
      vRatingLaneDtlSeq      IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vIsRouting      IN comb_lane_dtl.IS_ROUTING%TYPE,
      vIsSailing     IN comb_lane_dtl.IS_SAILING%TYPE);

END Rating_Lane_Validation_Pkg;
/



CREATE OR REPLACE 
PACKAGE BODY RATING_LANE_VALIDATION_PKG
-- -----------------------------------------------------------------------------------------
-- Change History /BUG FIXES
-- ID  DATE  AUTHOR DETAILS
-- n/a 08/27/2003 MB:  added Column Lists to RATING_LANE_DTL inserts (SCNDR_CARRIER_CODE support still required!)
-- n/a 08/29/2003 MB:  added SCNDR_CARRIER_CODE support to IMPORT_RATING_LANE_DETAILS
-- n/a 09/02/2003 MB:  added SCNDR_CARRIER_CODE support to INSERT_RATING_LANE_DTL
-- n/a 09/02/2003 MB:  added SCNDR_CARRIER_CODE support to TRANSFER_IMPORT_LANE_DTL
-- n/a 09/03/2003 MB:  added SCNDR_CARRIER_CODE support to ADJUST_EFF_EXP_DT, ADJUST_IS_BUDGETED
-- n/a 09/03/2003 MB:  added SCNDR_CARRIER_CODE support to VALIDATE_DETAIL_BASE_DATA,VALIDATE_TP_EQUIP_SERV_MODE,VALIDATE_FEASIBLE,VALIDATE_FAC_CARR_FEASIBLE
-- n/a 09/03/2003 MB:  added SCNDR_CARRIER_CODE support to GET_BUDG_RESOURCE_WHERE_CLAUSE,GET_RESOURCE_WHERE_CLAUSE
-- n/a 09/03/2003 MB:  added SCNDR_CARRIER_CODE support to VALIDATE_LANE_DTL,
-- n/a 09/09/2003 MB:  added p_error_msg_id,p_error_param_list to INSERT_RATING_LANE_ERROR, INSERT_RATING_LANE_DTL_ERROR (globalization - Fumin)
-- n/a 09/09/2003 MB:  error messages globalized (every call to INSERT_RATING_LANE_ERROR, INSERT_RATING_LANE_DTL_ERROR) (globalization - Fumin)
-- n/a 09/11/2003 MB  added MINIMUM_SIZE,MAXIMUM_SIZE,SIZE_UOM support to IMPORT_LANE_ACCESSORIALS (Deepak-Accessorial)
-- n/a 09/11/2003 MB  added MINIMUM_SIZE,MAXIMUM_SIZE,SIZE_UOM support to TRANSFER_IMP_LANE_ACCESSORIAL (Deepak-Accessorial)
-- n/a 09/11/2003 MB  added MINIMUM_SIZE,MAXIMUM_SIZE,SIZE_UOM support to INSERT_RATING_LANE_ACCESSORIAL (Deepak-Accessorial)
-- n/a 02/25/2004 MB  call to VALIDATE_TP_EQUIP_SERV_MODE from VALIDATE_LANE_DTL fixed
-- n/a  22/06/2004 RAM  added  Procedure COPY CARRIERS for updation of relavent carriers with the lane details changes when modified  SR1, 4R1 change, build92,  TT31182
--      07/15/2005      RS              modified by Priti Khare (CR#3354)
--      08/22/2005      RS              modified by Priti Khare (CR#2379) checking for Rate UOM to MI and KM for Rate Type "RADL"
--      11/22/2005      RS              modified by Mahesh Chandrashekhar (CR#8798)
--  11/24/2005  Ganesh   modified CARRIER_CODE to CARRIER_ID (CR #)
--      06/15/2005      RATING          added new  FUNCTION GET_BU_COMB_LANE_DTL (Ganesh Kadoor Prasad),
--      01/03/2006  Rajeev  DBCR 16475
--      01/05/2006  Rajeev  DBCR 16627
-- -----------------------------------------------------------------------------------------
AS
   nTime   NUMBER;

   PROCEDURE Import_Rating_Lanes (
      vTCCompanyId        IN COMPANY.COMPANY_ID%TYPE,
      vBatchId            IN IMPORT_RATING_LANE.BATCH_ID%TYPE,
      vBusinessUnit       IN IMPORT_RATING_LANE.BUSINESS_UNIT%TYPE,
      vOFacilityAliasId   IN IMPORT_RATING_LANE.O_FACILITY_ALIAS_ID%TYPE,
      vOCity              IN IMPORT_RATING_LANE.O_CITY%TYPE,
      vOStateProv         IN IMPORT_RATING_LANE.O_STATE_PROV%TYPE,
      vOCounty            IN IMPORT_RATING_LANE.O_COUNTY%TYPE,
      vOPostal            IN IMPORT_RATING_LANE.O_POSTAL_CODE%TYPE,
      vOCountry           IN IMPORT_RATING_LANE.O_COUNTRY_CODE%TYPE,
      vOZoneName          IN IMPORT_RATING_LANE.O_ZONE_NAME%TYPE,
      vDFacilityAliasId   IN IMPORT_RATING_LANE.D_FACILITY_ALIAS_ID%TYPE,
      vDCity              IN IMPORT_RATING_LANE.D_CITY%TYPE,
      vDStateProv         IN IMPORT_RATING_LANE.D_STATE_PROV%TYPE,
      vDCounty            IN IMPORT_RATING_LANE.D_COUNTY%TYPE,
      vDPostal            IN IMPORT_RATING_LANE.D_POSTAL_CODE%TYPE,
      vDCountry           IN IMPORT_RATING_LANE.D_COUNTRY_CODE%TYPE,
      vDZoneName          IN IMPORT_RATING_LANE.D_ZONE_NAME%TYPE,
      vLaneStatus         IN IMPORT_RATING_LANE.LANE_STATUS%TYPE,
      vOZoneId            IN IMPORT_RATING_LANE.O_ZONE_ID%TYPE,
      vDZoneId            IN IMPORT_RATING_LANE.D_ZONE_ID%TYPE)
   AS
      vLaneId                 IMPORT_RATING_LANE.LANE_ID%TYPE;
      vNewRatingLaneId        IMPORT_RATING_LANE.LANE_ID%TYPE;
      vImportLocationString   IMPORT_RATING_LANE.IMPORT_LOCATION_STRING%TYPE;
   BEGIN
      vImportLocationString :=
         Lane_Location_Pkg.fnGetImportLocationString (vBusinessUnit,
                                                      vOFacilityAliasId,
                                                      vOCity,
                                                      vOStateProv,
                                                      vOCounty,
                                                      vOPostal,
                                                      vOCountry,
                                                      vOZoneName,
                                                      vDFacilityAliasId,
                                                      vDCity,
                                                      vDStateProv,
                                                      vDCounty,
                                                      vDPostal,
                                                      vDCountry,
                                                      vDZoneName);

      SELECT LANE_ID
        INTO vLaneId
        FROM IMPORT_RATING_LANE
       WHERE TC_COMPANY_ID = vTCCompanyId
             AND IMPORT_LOCATION_STRING = vImportLocationString;

      UPDATE IMPORT_RATING_LANE
         SET BATCH_ID = vBatchId, LANE_STATUS = vLaneStatus
       WHERE LANE_ID = vLaneId AND TC_COMPANY_ID = vTCCompanyId;

      COMMIT;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         INSERT INTO IMPORT_RATING_LANE (TC_COMPANY_ID,
                                         LANE_ID,
                                         LANE_STATUS,
                                         BUSINESS_UNIT,
                                         O_FACILITY_ALIAS_ID,
                                         O_CITY,
                                         O_STATE_PROV,
                                         O_COUNTY,
                                         O_POSTAL_CODE,
                                         O_COUNTRY_CODE,
                                         O_ZONE_NAME,
                                         D_FACILITY_ALIAS_ID,
                                         D_CITY,
                                         D_STATE_PROV,
                                         D_COUNTY,
                                         D_POSTAL_CODE,
                                         D_COUNTRY_CODE,
                                         D_ZONE_NAME,
                                         CREATED_SOURCE_TYPE,
                                         CREATED_SOURCE,
                                         CREATED_DTTM,
                                         LAST_UPDATED_SOURCE_TYPE,
                                         LAST_UPDATED_SOURCE,
                                         LAST_UPDATED_DTTM,
                                         BATCH_ID,
                                         RATING_LANE_ID,
                                         IMPORT_LOCATION_STRING,
                                         O_ZONE_ID,
                                         D_ZONE_ID)
              VALUES (vTCCompanyId,
                      SEQ_IMP_RATING_LANE_ID.NEXTVAL,
                      vLaneStatus,
                      vBusinessUnit,
                      vOFacilityAliasId,
                      vOCity,
                      vOStateProv,
                      vOCounty,
                      vOPostal,
                      vOCountry,
                      vOZoneName,
                      vDFacilityAliasId,
                      vDCity,
                      vDStateProv,
                      vDCounty,
                      vDPostal,
                      vDCountry,
                      vDZoneName,
                      3,
                      'OMS',
                      SYSDATE,
                      3,
                      'OMS',
                      SYSDATE,
                      vBatchId,
                      vBatchId,
                      vImportLocationString,
                      vOZoneId,
                      vDZoneId);

         COMMIT;
   END Import_Rating_Lanes;

   PROCEDURE IMPORT_RATING_LANE_DETAILS (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vBatchId               IN IMPORT_RATING_LANE.BATCH_ID%TYPE,
      vRatingLaneDtlSeq      IN IMPORT_RATING_LANE_DTL.RATING_LANE_DTL_SEQ%TYPE,
      vEffectiveDT           IN IMPORT_RATING_LANE_DTL.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN IMPORT_RATING_LANE_DTL.EXPIRATION_DT%TYPE,
      vCarrierCode           IN IMPORT_RATING_LANE_DTL.CARRIER_CODE%TYPE,
      vMot                   IN IMPORT_RATING_LANE_DTL.MOT%TYPE,
      vEquipment             IN IMPORT_RATING_LANE_DTL.EQUIPMENT_CODE%TYPE,
      vServiceLevel          IN IMPORT_RATING_LANE_DTL.SERVICE_LEVEL%TYPE,
      p_scndr_carrier_code   IN IMPORT_RATING_LANE_DTL.SCNDR_CARRIER_CODE%TYPE,
      vProtectionLevel       IN IMPORT_RATING_LANE_DTL.PROTECTION_LEVEL%TYPE,
      vIsBudgeted            IN IMPORT_RATING_LANE_DTL.IS_BUDGETED%TYPE,
      vOriginShipVia         IN IMPORT_RATING_LANE_DTL.O_SHIP_VIA%TYPE,
      vDestinationShipVia    IN IMPORT_RATING_LANE_DTL.D_SHIP_VIA%TYPE,
      vContractNumber        IN IMPORT_RATING_LANE_DTL.CONTRACT_NUMBER%TYPE,
      vCustomText1           IN IMPORT_RATING_LANE_DTL.CUSTOM_TEXT1%TYPE,
      vCustomText2           IN IMPORT_RATING_LANE_DTL.CUSTOM_TEXT2%TYPE,
      vCustomText3           IN IMPORT_RATING_LANE_DTL.CUSTOM_TEXT3%TYPE,
      vCustomText4           IN IMPORT_RATING_LANE_DTL.CUSTOM_TEXT4%TYPE,
      vCustomText5           IN IMPORT_RATING_LANE_DTL.CUSTOM_TEXT5%TYPE,
      vPackageId             IN IMPORT_RATING_LANE_DTL.PACKAGE_ID%TYPE,
      vPackageName           IN IMPORT_RATING_LANE_DTL.PACKAGE_NAME%TYPE,
      vRateDtlStatus         IN IMPORT_RATING_LANE_DTL.IMPORT_RATING_DTL_STATUS%TYPE,
      vImpRateDtlStatus      IN IMPORT_RATING_LANE_DTL.RATING_LANE_DTL_STATUS%TYPE)
   IS
      vLaneId RATING_LANE.LANE_ID%TYPE;
   BEGIN
      SELECT LANE_ID
        INTO vLaneId
        FROM IMPORT_RATING_LANE
       WHERE TC_COMPANY_ID = vTCCompanyId AND BATCH_ID = vBatchID;

      INSERT INTO IMPORT_RATING_LANE_DTL (tc_company_id,
                                          lane_id,
                                          rating_lane_dtl_seq,
                                          effective_dt,
                                          expiration_dt,
                                          CARRIER_CODE,
                                          MOT,
                                          equipment_code,
                                          SERVICE_LEVEL,
                                          PROTECTION_LEVEL,
                                          last_updated_source_type,
                                          last_updated_source,
                                          last_updated_dttm,
                                          rating_lane_dtl_status,
                                          import_rating_dtl_status,
                                          is_budgeted,
                                          SCNDR_CARRIER_CODE,
                                          O_SHIP_VIA,
                                          D_SHIP_VIA,
                                          CONTRACT_NUMBER,
                                          CUSTOM_TEXT1,
                                          CUSTOM_TEXT2,
                                          CUSTOM_TEXT3,
                                          CUSTOM_TEXT4,
                                          CUSTOM_TEXT5,
                                          PACKAGE_ID,
                                          PACKAGE_NAME)
           VALUES (vTCCompanyId,
                   vLaneId,
                   vRatingLaneDtlSeq,
                   vEffectiveDT,
                   vExpirationDT,
                   vCarrierCode,
                   vMot,
                   vEquipment,
                   vServiceLevel,
                   vProtectionLevel,
                   3,
                   'OMS',
                   SYSDATE,
                   vRateDtlStatus,
                   vImpRateDtlStatus,
                   vIsBudgeted,
                   p_scndr_carrier_code,
                   vOriginShipVia,
                   vDestinationShipVia,
                   vContractNumber,
                   vCustomText1,
                   vCustomText2,
                   vCustomText3,
                   vCustomText4,
                   vCustomText5,
                   vPackageId,
                   vPackageName);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         INSERT INTO RATING_LANE_ERRORS (RATING_LANE_ERROR,
                                         TC_COMPANY_ID,
                                         LANE_ID,
                                         RATING_LANE_DTL_SEQ,
                                         ERROR_MSG_DESC,
                                         BATCH_ID)
              VALUES (
                        SEQ_RATING_LANE_ERROR.NEXTVAL,
                        vTCCompanyId,
                        0,
                        vRatingLaneDtlSeq,
                        'Duplicate lanes are not allowed within the same batch import. Some lane details, rates, accessorials have been lost.',
                        vBatchId);
   END IMPORT_RATING_LANE_DETAILS;

   PROCEDURE IMPORT_RATING_LANE_RATES (
      vTCCompanyId          IN COMPANY.COMPANY_ID%TYPE,
      vBatchId              IN IMPORT_RATING_LANE.BATCH_ID%TYPE,
      vRatingLaneDtlId      IN IMPORT_RATING_LANE_DTL_RATE.RATING_LANE_DTL_SEQ%TYPE,
      vRldRateSeq           IN IMPORT_RATING_LANE_DTL_RATE.RLD_RATE_SEQ%TYPE,
      vMinSize              IN IMPORT_RATING_LANE_DTL_RATE.MINIMUM_SIZE%TYPE,
      vMaxSize              IN IMPORT_RATING_LANE_DTL_RATE.MAXIMUM_SIZE%TYPE,
      vSizeUOM              IN IMPORT_RATING_LANE_DTL_RATE.SIZE_UOM%TYPE,
      vRateCalcMethod       IN IMPORT_RATING_LANE_DTL_RATE.RATE_CALC_METHOD%TYPE,
      vRateUOM              IN IMPORT_RATING_LANE_DTL_RATE.RATE_UOM%TYPE,
      vRate                 IN IMPORT_RATING_LANE_DTL_RATE.RATE%TYPE,
      vMinRate              IN IMPORT_RATING_LANE_DTL_RATE.MINIMUM_RATE%TYPE,
      vCurrencyCode         IN IMPORT_RATING_LANE_DTL_RATE.CURRENCY_CODE%TYPE,
      vTariffId             IN IMPORT_RATING_LANE_DTL_RATE.TARIFF_ID%TYPE,
      vMinDistance          IN IMPORT_RATING_LANE_DTL_RATE.MIN_DISTANCE%TYPE,
      vMaxDistance          IN IMPORT_RATING_LANE_DTL_RATE.MAX_DISTANCE%TYPE,
      vDistanceUOM          IN IMPORT_RATING_LANE_DTL_RATE.DISTANCE_UOM%TYPE,
      vSupportsMSTL         IN IMPORT_RATING_LANE_DTL_RATE.SUPPORTS_MSTL%TYPE,
      vHasBH                IN IMPORT_RATING_LANE_DTL_RATE.HAS_BH%TYPE,
      vMinCommodity         IN IMPORT_RATING_LANE_DTL_RATE.MIN_COMMODITY%TYPE,
      vMaxCommodity         IN IMPORT_RATING_LANE_DTL_RATE.MAX_COMMODITY%TYPE,
      vHasRT                IN IMPORT_RATING_LANE_DTL_RATE.HAS_RT%TYPE,
      vParcelTier           IN IMPORT_RATING_LANE_DTL_RATE.PARCEL_TIER%TYPE,
      vCommodityCodeId      IN IMPORT_RATING_LANE_DTL_RATE.COMMODITY_CODE_ID%TYPE,
      vExcessWtRate         IN IMPORT_RATING_LANE_DTL_RATE.EXCESS_WT_RATE%TYPE,
      vPayeeCarrier         IN IMPORT_RATING_LANE_DTL_RATE.PAYEE_CARRIER_CODE%TYPE,
      vMaxCommodityCodeId   IN IMPORT_RATING_LANE_DTL_RATE.MAX_RANGE_COMMODITY_CODE_ID%TYPE)
   IS
      vLaneId RATING_LANE.LANE_ID%TYPE;
   BEGIN
      SELECT LANE_ID
        INTO vLaneId
        FROM IMPORT_RATING_LANE
       WHERE TC_COMPANY_ID = vTCCompanyId AND BATCH_ID = vBatchID;

      INSERT INTO IMPORT_RATING_LANE_DTL_RATE (tc_company_id,
                                               lane_id,
                                               rating_lane_dtl_seq,
                                               rld_rate_seq,
                                               minimum_size,
                                               maximum_size,
                                               SIZE_UOM,
                                               RATE_CALC_METHOD,
                                               rate_uom,
                                               rate,
                                               minimum_rate,
                                               currency_code,
                                               tariff_id,
                                               min_distance,
                                               max_distance,
                                               DISTANCE_UOM,
                                               supports_mstl,
                                               has_bh,
                                               min_commodity,
                                               max_commodity,
                                               has_rt,
                                               parcel_tier,
                                               commodity_code_id,
                                               excess_wt_rate,
                                               payee_carrier_code,
                                               max_range_commodity_code_id)
           VALUES (vTCCompanyId,
                   vLaneId,
                   vRatingLaneDtlId,
                   vRldRateSeq,
                   vMinSize,
                   vMaxSize,
                   vSizeUOM,
                   vRateCalcMethod,
                   vRateUOM,
                   vRate,
                   vMinRate,
                   vCurrencyCode,
                   vTariffId,
                   vMinDistance,
                   vMaxDistance,
                   vDistanceUOM,
                   vSupportsMSTL,
                   vHasBH,
                   vMinCommodity,
                   vMaxCommodity,
                   vHasRT,
                   vParcelTier,
                   vCommodityCodeId,
                   vExcessWtRate,
                   vPayeeCarrier,
                   vMaxCommodityCodeId);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         NULL;                             -- already reported by lane details
   END IMPORT_RATING_LANE_RATES;

   PROCEDURE IMPORT_LANE_ACCESSORIALS (
      vTCCompanyId             IN COMPANY.COMPANY_ID%TYPE,
      vBatchId                 IN IMPORT_RATING_LANE.BATCH_ID%TYPE,
      vRatingLaneDtlId         IN IMPORT_RATING_LANE_DTL_RATE.RATING_LANE_DTL_SEQ%TYPE,
      vAccessorialCode         IN IMPORT_LANE_ACCESSORIAL.ACCESSORIAL_CODE%TYPE,
      vRate                    IN IMPORT_LANE_ACCESSORIAL.RATE%TYPE,
      vMinRate                 IN IMPORT_LANE_ACCESSORIAL.MINIMUM_RATE%TYPE,
      vCurrencyCode            IN IMPORT_LANE_ACCESSORIAL.CURRENCY_CODE%TYPE,
      p_minimum_size           IN IMPORT_LANE_ACCESSORIAL.MINIMUM_SIZE%TYPE,
      p_maximum_size           IN IMPORT_LANE_ACCESSORIAL.MAXIMUM_SIZE%TYPE,
      p_size_uom               IN IMPORT_LANE_ACCESSORIAL.SIZE_UOM%TYPE,
      vIsApproved              IN IMPORT_LANE_ACCESSORIAL.IS_AUTO_APPROVE%TYPE,
      vEffectiveDT             IN IMPORT_LANE_ACCESSORIAL.EFFECTIVE_DT%TYPE,
      vExpirationDT            IN IMPORT_LANE_ACCESSORIAL.EXPIRATION_DT%TYPE,
      vPayeeCarrierCode        IN IMPORT_LANE_ACCESSORIAL.PAYEE_CARRIER_CODE%TYPE,
      vIsShipmentAccessorial   IN IMPORT_LANE_ACCESSORIAL.IS_SHIPMENT_ACCESSORIAL%TYPE)
   IS
      vLaneId RATING_LANE.LANE_ID%TYPE;
   BEGIN
      SELECT LANE_ID
        INTO vLaneId
        FROM IMPORT_RATING_LANE
       WHERE TC_COMPANY_ID = vTCCompanyId AND BATCH_ID = vBatchID;

      INSERT INTO IMPORT_LANE_ACCESSORIAL (tc_company_id,
                                           lane_id,
                                           rating_lane_dtl_seq,
                                           ACCESSORIAL_CODE,
                                           rate,
                                           minimum_rate,
                                           currency_code,
                                           is_auto_approve,
                                           effective_dt,
                                           expiration_dt,
                                           payee_carrier_code,
                                           MINIMUM_SIZE,
                                           MAXIMUM_SIZE,
                                           SIZE_UOM_ID,
                                           IS_SHIPMENT_ACCESSORIAL)
           VALUES (vTCCompanyId,
                   vLaneId,
                   vRatingLaneDtlId,
                   vAccessorialCode,
                   vRate,
                   vMinRate,
                   vCurrencyCode,
                   vIsApproved,
                   vEffectiveDT,
                   vExpirationDT,
                   vPayeeCarrierCode,
                   p_minimum_size,
                   p_maximum_size,
                   p_size_uom,
                   vIsShipmentAccessorial);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         NULL;                             -- already reported by lane details
   END IMPORT_LANE_ACCESSORIALS;

   PROCEDURE TRANSFER_IMPORT_LANES (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vUser           IN rating_lane.LAST_UPDATED_SOURCE%TYPE,
      vFirstBatchId   IN rating_lane.LANE_ID%TYPE,
      vLastBatchId    IN rating_lane.LANE_ID%TYPE)
   AS
      vPassFail            NUMBER;
      vLaneId              RATING_LANE.LANE_ID%TYPE;

      CURSOR TRANSFER_LANE_CURSOR
      IS
         SELECT lane_id,
                BUSINESS_UNIT,
                lane_hierarchy,
                o_loc_type,
                o_facility_id,
                o_facility_alias_id,
                o_city,
                o_state_prov,
                o_county,
                o_postal_code,
                o_country_code,
                o_zone_id,
                d_loc_type,
                d_facility_id,
                d_facility_alias_id,
                d_city,
                d_state_prov,
                d_county,
                d_postal_code,
                d_country_code,
                d_zone_id,
                BILLING_METHOD,
                incoterm_id,
                customer_id,
                rating_lane_id                                         /*JMC*/
           FROM IMPORT_RATING_LANE
          WHERE     LANE_STATUS = 1
                AND tc_Company_Id = vTCCompanyId
                AND batch_id >= vFirstBatchId
                AND batch_id <= vLastBatchId;                           /*CR*/

      nInsertTime          NUMBER;
      nInsertTotalTime     NUMBER := 0;
      nUpdateTime          NUMBER;
      nUpdateTotalTime     NUMBER := 0;
      nXferDtlTime         NUMBER;
      nXferDtlTotalTime    NUMBER := 0;
      nPassFailTime        NUMBER;
      nPassFailTotalTime   NUMBER := 0;
   BEGIN
      FOR r IN TRANSFER_LANE_CURSOR
      LOOP
         nInsertTime := DBMS_UTILITY.GET_TIME ();
         vLaneId :=
            INSERT_RATING_LANE (vTCCompanyId,
                                vUser,
                                r.BUSINESS_UNIT,
                                r.Lane_Hierarchy,
                                r.O_Loc_Type,
                                r.O_Facility_Id,
                                r.O_Facility_Alias_Id,
                                r.O_City,
                                r.O_State_Prov,
                                r.O_County,
                                r.O_Postal_code,
                                r.O_Country_code,
                                r.O_Zone_id,
                                r.D_Loc_Type,
                                r.D_Facility_Id,
                                r.D_Facility_Alias_Id,
                                r.D_City,
                                r.D_State_Prov,
                                r.D_County,
                                r.D_Postal_code,
                                r.D_Country_code,
                                r.D_Zone_id,
                                r.BILLING_METHOD,
                                r.incoterm_id,
                                r.customer_id,
                                r.Rating_Lane_Id);                     /*JMC*/
         nInsertTotalTime :=
            nInsertTotalTime
            + ( (DBMS_UTILITY.GET_TIME () - nInsertTime) / 100);
         nUpdateTime := DBMS_UTILITY.GET_TIME ();

         UPDATE IMPORT_RATING_LANE
            SET RATING_LANE_ID = vLaneId
          WHERE LANE_ID = r.Lane_Id AND TC_COMPANY_ID = vTCCompanyId;

         COMMIT;
         nUpdateTotalTime :=
            nUpdateTotalTime
            + ( (DBMS_UTILITY.GET_TIME () - nUpdateTime) / 100);
         nXferDtlTime := DBMS_UTILITY.GET_TIME ();
		 
		 VALIDATE_LANE_DTL_EPI(vTCCompanyId,
                                r.Lane_Id,
                                  vLaneId);
		 
         TRANSFER_IMPORT_LANE_DTL (vTCCompanyId,
                                   vUser,
                                   r.Lane_Id,
                                   vLaneId);
         nXferDtlTotalTime :=
            nXferDtlTotalTime
            + ( (DBMS_UTILITY.GET_TIME () - nXferDtlTime) / 100);
         nPassFailTime := DBMS_UTILITY.GET_TIME ();
         vPassFail := DELETE_IMP_RATING_LANES (vTCCompanyId, r.Lane_Id);
         nPassFailTotalTime :=
            nPassFailTotalTime
            + ( (DBMS_UTILITY.GET_TIME () - nPassFailTime) / 100);
      END LOOP;
   END TRANSFER_IMPORT_LANES;

   PROCEDURE TRANSFER_IMPORT_LANE_DTL (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vUser           IN rating_lane.LAST_UPDATED_SOURCE%TYPE,
      vImportLaneId   IN rating_lane.LANE_ID%TYPE,
      vLaneId         IN rating_lane.LANE_ID%TYPE)
   AS
      vRatingLaneDtlSeq          rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE;

      vCarrierCodeBUID           COMPANY.COMPANY_ID%TYPE;
      vMotBUID                   COMPANY.COMPANY_ID%TYPE;
      vEquipmentBUID             COMPANY.COMPANY_ID%TYPE;
      vServiceLevelBUID          COMPANY.COMPANY_ID%TYPE;
      l_scndr_carrier_codeBUID   COMPANY.COMPANY_ID%TYPE;
      vProtectionLevelBUID       COMPANY.COMPANY_ID%TYPE;
      -- SYSCO Enhacement Merge
      vDuplicateLaneDtlId        rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE;
      vPendingAuth               VARCHAR (20);
      vIsPendingAuth             rating_lane_dtl.IS_BUDGETED%TYPE;
      vDoNotDeleteRate           NUMBER;
      vLastUpdatedSource         COMB_LANE_DTL.LAST_UPDATED_SOURCE%TYPE;
      vLastUpdatedSourceType     COMB_LANE_DTL.LAST_UPDATED_SOURCE_TYPE%TYPE;


      CURSOR TRANSFER_LANE_DTL_CURSOR
      IS
         SELECT rating_lane_dtl_seq,
                carrier_id,
                mot_id,
                equipment_id,
                service_level_id,
                SCNDR_CARRIER_ID,
                protection_level_id,
                is_budgeted,
                effective_dt,
                expiration_dt,
                import_rating_dtl_status,
                o_ship_via,
                d_ship_via,
                contract_number,
                custom_text1,
                custom_text2,
                custom_text3,
                custom_text4,
                custom_text5,
                package_id,
                package_name,
				voyage
           FROM IMPORT_RATING_LANE_DTL
          WHERE     tc_company_id = vTCCompanyId
                AND lane_id = vImportLaneId
                AND rating_lane_dtl_status = 1;
   BEGIN
      FOR r IN TRANSFER_LANE_DTL_CURSOR
      LOOP
         -- nTime := dbms_utility.get_time();
         -- SYSCO ENHC
         vDoNotDeleteRate := 0;
         vIsPendingAuth := 0;


         vpendingauth := NULL;

         BEGIN
            SELECT param_value
              INTO vpendingauth
              FROM company_parameter
             WHERE param_def_id = 'RATING_CHANGE_AUTHORIZATION_REQUIRED'
                   AND tc_company_id = vtccompanyid;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               SELECT dflt_value
                 INTO vpendingauth
                 FROM param_def
                WHERE param_def_id = 'RATING_CHANGE_AUTHORIZATION_REQUIRED';
         END;

         vRatingLaneDtlSeq :=
            INSERT_RATING_LANE_DTL (vTCCompanyId,
                                    vUser,
                                    vLaneId,
                                    r.carrier_id,
                                    r.mot_id,
                                    r.equipment_id,
                                    r.service_level_id,
                                    r.scndr_carrier_id,
                                    r.protection_level_id,
                                    r.Is_Budgeted,
                                    r.Effective_DT,
                                    r.Expiration_DT,
                                    r.import_rating_dtl_status,
                                    r.o_ship_via,
                                    r.d_ship_via,
                                    r.contract_number,
                                    r.custom_text1,
                                    r.custom_text2,
                                    r.custom_text3,
                                    r.custom_text4,
                                    r.custom_text5,
                                    r.package_id,
                                    r.package_name,
									r.voyage);



         vDuplicateLaneDtlId :=
            CHECK_DUP_LANE_DTL_NO_OVERLAP (vTCCompanyId,
                                           vLaneId,
                                           vRatingLaneDtlSeq,
                                           r.carrier_id,
                                           r.mot_id,
                                           r.equipment_id,
                                           r.service_level_id,
                                           r.scndr_carrier_id,
                                           r.protection_level_id,
                                           r.Effective_DT,
                                           r.Expiration_DT,
                                           r.o_ship_via,
                                           r.d_ship_via,
                                           r.custom_text1,
                                           r.custom_text2,
                                           r.custom_text3,
                                           r.custom_text4,
                                           r.custom_text5,
                                           r.package_id,
                                           r.package_name,
										   r.voyage);


         -- SYSCO ENHC
         IF (vpendingauth = '2' OR vpendingauth = '3')
         THEN
            vIsPendingAuth := 1;

            IF (vDuplicateLaneDtlId > 0)
            THEN
               vDoNotDeleteRate := 1;
            END IF;
         ELSE
            vIsPendingAuth := 0;
         END IF;

         --insert into TIME_IMPORT_LANE_DTL_KDM values ('XFER LANE DTL','INSERT RLD',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();
         -- If the sequence is zero the lane was imported with the expire setting
         -- but was not initially there in the first place

         vdonotdeleterate := 0;

         IF (vratinglanedtlseq > 0)
         THEN
            TRANSFER_IMP_LANE_ACCESSORIAL (vTCCompanyId,
                                           vUser,
                                           vImportLaneId,
                                           vLaneId,
                                           r.rating_lane_dtl_seq,
                                           vRatingLaneDtlSeq,
                                           vIsPendingAuth,
                                           vDuplicateLaneDtlId);
            --insert into TIME_IMPORT_LANE_DTL_KDM values ('XFER LANE DTL','INSERT ACCESS',dbms_utility.get_time() - nTime);
            -- nTime := dbms_utility.get_time();
            TRANSFER_IMP_LANE_DTL_RATE (vTCCompanyId,
                                        vUser,
                                        vImportLaneId,
                                        vLaneId,
                                        r.rating_lane_dtl_seq,
                                        vRatingLaneDtlSeq,
                                        vIsPendingAuth,
                                        vDuplicateLaneDtlId);

            ADJUST_EFF_EXP_DT (vTCCompanyId,
                               vUser,
                               vLaneId,
                               vRatingLaneDtlSeq,
                               r.Effective_DT,
                               r.Expiration_DT,
                               r.carrier_id,
                               r.mot_id,
                               r.equipment_id,
                               r.service_level_id,
                               r.scndr_carrier_id,
                               r.protection_level_id,
                               r.o_ship_via,
                               r.d_ship_via,
							   r.contract_number,
                               r.custom_text1,
                               r.custom_text2,
                               r.custom_text3,
                               r.custom_text4,
                               r.custom_text5,
                               vDoNotDeleteRate,
							   r.voyage);
            --insert into TIME_IMPORT_LANE_DTL_KDM values ('INSERT_RATING_LANE_DTL','ADJUST_EFF_EXP_DT',dbms_utility.get_time() - nTime);
            -- nTime := dbms_utility.get_time();

            ADJUST_IS_BUDGETED (vTCCompanyId,
                                vLaneId,
                                vRatingLaneDtlSeq,
                                r.Effective_DT,
                                r.Expiration_DT,
                                r.Mot_Id,
                                r.Equipment_Id,
                                r.service_Level_Id,
                                r.scndr_carrier_Id,
                                r.Protection_Level_Id,
                                r.custom_text1,
                                r.custom_text2,
                                r.custom_text3,
                                r.custom_text4,
                                r.custom_text5);
         END IF;

         DELETE FROM IMPORT_LANE_ACCESSORIAL
               WHERE     tc_company_id = vTCCompanyId
                     AND lane_id = vImportLaneId
                     AND rating_lane_dtl_seq = r.rating_lane_dtl_seq;

         DELETE FROM IMPORT_RATING_LANE_DTL_RATE
               WHERE     tc_company_id = vTCCompanyId
                     AND lane_id = vImportLaneId
                     AND rating_lane_dtl_seq = r.rating_lane_dtl_seq;

         --insert into TIME_IMPORT_LANE_DTL_KDM values ('XFER LANE DTL','INSERT RLD RATE',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();
         DELETE FROM IMPORT_RATING_LANE_DTL
               WHERE     TC_COMPANY_ID = vTCCompanyId
                     AND LANE_ID = vImportLaneId
                     AND RATING_LANE_DTL_SEQ = r.rating_lane_dtl_seq;

         --insert into TIME_IMPORT_LANE_DTL_KDM values ('XFER LANE DTL','DELETE RLD',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();

         COMMIT;
      END LOOP;
   END TRANSFER_IMPORT_LANE_DTL;

   PROCEDURE TRANSFER_IMP_LANE_ACCESSORIAL (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vUser                  IN rating_lane.LAST_UPDATED_SOURCE%TYPE,
      vImportedLaneId        IN rating_lane.LANE_ID%TYPE,
      vLaneId                IN rating_lane.LANE_ID%TYPE,
      vImpRatingLaneDtlSeq   IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vRatingLaneDtlSeq      IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vIsPendingAuth         IN rating_lane_dtl.IS_BUDGETED%TYPE,
      vDuplicateLaneDtlId    IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE)
   AS
      vAccessorialId           LANE_ACCESSORIAL.ACCESSORIAL_ID%TYPE;
      vRate                    LANE_ACCESSORIAL.RATE%TYPE;
      vMinimumRate             LANE_ACCESSORIAL.MINIMUM_RATE%TYPE;
      vCurrencyCode            LANE_ACCESSORIAL.CURRENCY_CODE%TYPE;
      vIsApproved              LANE_ACCESSORIAL.IS_AUTO_APPROVE%TYPE;
      vEffectiveDT             LANE_ACCESSORIAL.EFFECTIVE_DT%TYPE;
      vExpirationDT            LANE_ACCESSORIAL.EXPIRATION_DT%TYPE;
      vPayeeCarrierCode        LANE_ACCESSORIAL.PAYEE_CARRIER_ID%TYPE;
      l_minimum_size           LANE_ACCESSORIAL.MINIMUM_SIZE%TYPE;
      l_maximum_size           LANE_ACCESSORIAL.MAXIMUM_SIZE%TYPE;
      l_size_uom               LANE_ACCESSORIAL.SIZE_UOM_ID%TYPE;
      vIsShipmentAccessorial   LANE_ACCESSORIAL.IS_SHIPMENT_ACCESSORIAL%TYPE;
      vLaneAccId               LANE_ACCESSORIAL.Lane_Accessorial_Id%TYPE;
      vAccessorialBUID         COMPANY.COMPANY_ID%TYPE;
      vRootCompanyId           COMPANY.COMPANY_ID%TYPE;
      VPASSFAIL                NUMBER;
      VFAILURE                 NUMBER;
      VERRORMSGDESC            VARCHAR2 (255);
      vAccessorialTempID       LANE_ACCESSORIAL.ACCESSORIAL_ID%TYPE;
      vMinRateType             LANE_ACCESSORIAL.Min_Rate_Type%TYPE; -- MACR00159593 starts
      vMaximumRate             LANE_ACCESSORIAL.Maximum_Rate%TYPE;
      vPackageType             LANE_ACCESSORIAL.PACKAGE_TYPE_ID%TYPE;
      vCustomerRate            LANE_ACCESSORIAL.CUSTOMER_RATE%TYPE;
      vCustomerMinCharge       LANE_ACCESSORIAL.Customer_Min_Charge%TYPE;
      vCalculatedRate          LANE_ACCESSORIAL.Calculated_Rate%TYPE;
      vAmount                  LANE_ACCESSORIAL.Amount%TYPE;
      vBaseAmount              LANE_ACCESSORIAL.BASE_AMOUNT%TYPE;
      vBaseCharge              LANE_ACCESSORIAL.Base_Charge%TYPE;
      vRangeMinAmount          LANE_ACCESSORIAL.RANGE_MIN_AMOUNT%TYPE;
      vRangeMaxAmount          LANE_ACCESSORIAL.RANGE_MAX_AMOUNT%TYPE;
      vIncrement               LANE_ACCESSORIAL.Increment_Val%TYPE;
      vMot                     LANE_ACCESSORIAL.Mot_Id%TYPE;
      vEquipment               LANE_ACCESSORIAL.Equipment_Id%TYPE;
      vServiceLevel            LANE_ACCESSORIAL.Service_Level_Id%TYPE;
      vProtectionLevel         LANE_ACCESSORIAL.PROTECTION_LEVEL_ID%TYPE;
      vBillingMethod           LANE_ACCESSORIAL.Billing_Method%TYPE;
      vMinLongestDim           LANE_ACCESSORIAL.Min_Longest_Dimension%TYPE;
      vMaxLongestDim           LANE_ACCESSORIAL.Max_Longest_Dimension%TYPE;
      vMinSecondLongestDim     LANE_ACCESSORIAL.Min_Second_Longest_Dimension%TYPE;
      vMaxSecondLongestDim     LANE_ACCESSORIAL.Max_Second_Longest_Dimension%TYPE;
      vMinThirdLongestDim      LANE_ACCESSORIAL.Min_Third_Longest_Dimension%TYPE;
      vMaxThirdLongestDim      LANE_ACCESSORIAL.Max_Third_Longest_Dimension%TYPE;
      vMinLengthGrith          LANE_ACCESSORIAL.Min_Length_Plus_Grith%TYPE;
      vMaxLengthGrith          LANE_ACCESSORIAL.Max_Length_Plus_Grith%TYPE;
      vMinWeight               LANE_ACCESSORIAL.Min_Weight%TYPE;
      vMaxWeight               LANE_ACCESSORIAL.Max_Weight%TYPE;
      vIsZoneStopOff           LANE_ACCESSORIAL.Is_Zone_Stopoff%TYPE;
      vZoneId                  LANE_ACCESSORIAL.Zone_Id%TYPE; -- MACR00159593 ends
      vLastUpdatedSource       LANE_ACCESSORIAL.LAST_UPDATED_SRC%TYPE;
      vLastUpdatedSourceType   LANE_ACCESSORIAL.LAST_UPDATED_SRC_TYPE%TYPE;
	  maxRatingDtlId           NUMBER;
      maxAccLaneId             NUMBER;
      vImpLaneAccId         LANE_ACCESSORIAL.LANE_ACCESSORIAL_ID%TYPE;	
	  
	  acc_code_excluded_cur		IMP_EXCLUDED_LANE_ACC.ACCESSORIAL_CODE_EXCL%TYPE;
	  acc_code_excluded_bu_cur	IMP_EXCLUDED_LANE_ACC.ACCESSORIAL_CODE_EXCL_BU%TYPE;
	  vLaneAccessorialId		NUMBER;
	 -- CMGT-615 Starts here
	  vCommID				LANE_ACCESSORIAL.COMMODITY_CODE_ID%TYPE;
	  vMaxCommId			LANE_ACCESSORIAL.MAX_RANGE_COMMODITY_CODE_ID%TYPE;
	  vCommodityCode              VARCHAR2 (16);
      vMaxRangeCommodityCode      VARCHAR2 (16);
	  paramValueExists            NUMBER;
	  vUseRangeForCommodityCode   VARCHAR2 (5);
	  vsqlStatement               VARCHAR2 (4000);
	  vCount                 NUMBER;
	  vuomstr                import_lane_accessorial.size_uom%TYPE;
	  vRowId                 ROWID;
	  -- CMGT-615 Ends here
	  
      CURSOR TRANSFER_LANE_ACCESS_CURSOR
      IS
         SELECT lane_acc_id,
		        accessorial_id,
                rate,
                minimum_rate,
                currency_code,
                is_auto_approve,
                effective_dt,
                expiration_dt,
                payee_carrier_id,
                MINIMUM_SIZE,
                MAXIMUM_SIZE,
                SIZE_UOM_ID,
                IS_SHIPMENT_ACCESSORIAL,
                Min_Rate_Type,                          -- MACR00159593 starts
                Maximum_Rate,
                PACKAGE_TYPE_ID,
                CUSTOMER_RATE,
                Customer_Min_Charge,
                Calculated_Rate,
                Amount,
                BASE_AMOUNT,
                Base_Charge,
                RANGE_MIN_AMOUNT,
                RANGE_MAX_AMOUNT,
                Increment_Val,
                Mot_Id,
                Equipment_Id,
                Service_Level_Id,
                PROTECTION_LEVEL_ID,
                Billing_Method,
                Min_Longest_Dimension,
                Max_Longest_Dimension,
                Min_Second_Longest_Dimension,
                Max_Second_Longest_Dimension,
                Min_Third_Longest_Dimension,
                Max_Third_Longest_Dimension,
                Min_Length_Plus_Grith,
                Max_Length_Plus_Grith,
                Min_Weight,
                Max_Weight,
                Is_Zone_Stopoff,
                Zone_Id ,                                  -- MACR00159593 ends
				COMMODITY_CODE_ID, -- CMGT-615 Starts here
				 --  RLD_RATE_SEQ,
				--RATE_CALC_METHOD,
				MAX_RANGE_COMMODITY_CODE_ID,
				SIZE_UOM				-- CMGT-615 Ends here
           FROM IMPORT_LANE_ACCESSORIAL
          WHERE     tc_company_id = vTCCompanyId
                AND lane_id = vImportedLaneId
                AND rating_lane_dtl_seq = vImpRatingLaneDtlSeq;
   BEGIN
      OPEN TRANSFER_LANE_ACCESS_CURSOR;

     <<TRANSFER_LANE_ACCESS_LOOP>>
      LOOP
         FETCH TRANSFER_LANE_ACCESS_CURSOR
         INTO vImpLaneAccId,
			  vAccessorialId,
              vRate,
              vMinimumRate,
              vCurrencyCode,
              vIsApproved,
              vEffectiveDT,
              vExpirationDT,
              vPayeeCarrierCode,
              l_minimum_size,
              l_maximum_size,
              l_size_uom,
              vIsShipmentAccessorial,
              vMinRateType,                             -- MACR00159593 starts
              vMaximumRate,
              vPackageType,
              vCustomerRate,
              vCustomerMinCharge,
              vCalculatedRate,
              vAmount,
              vBaseAmount,
              vBaseCharge,
              vRangeMinAmount,
              vRangeMaxAmount,
              vIncrement,
              vMot,
              vEquipment,
              vServiceLevel,
              vProtectionLevel,
              vBillingMethod,
              vMinLongestDim,
              vMaxLongestDim,
              vMinSecondLongestDim,
              vMaxSecondLongestDim,
              vMinThirdLongestDim,
              vMaxThirdLongestDim,
              vMinLengthGrith,
              vMaxLengthGrith,
              vMinWeight,
              vMaxWeight,
              vIsZoneStopOff,
              vZoneId ,                                    -- MACR00159593 ends
			  vCommID, -- CMGT-615 Starts here
			  vMaxCommId,
			  VUOMSTR;-- CMGT-615 Ends here

         EXIT TRANSFER_LANE_ACCESS_LOOP WHEN TRANSFER_LANE_ACCESS_CURSOR%NOTFOUND;

         IF (vUser IS NOT NULL)
         THEN
            vLastUpdatedSource := vUser;
            vLastUpdatedSourceType := 1;
         ELSE
            vLastUpdatedSource := 'OMS';
            vLastUpdatedSourceType := 3;
         END IF;
		IF (vCommID IS NOT NUll AND vMaxCommId IS NOT NULL)
		THEN
			SELECT COMMODITY_CODE_ID
              INTO vCommID
              FROM COMMODITY_CODE
             WHERE DESCRIPTION_SHORT = vCommID and tc_company_id = vTCCompanyId;
			 SELECT COMMODITY_CODE_ID
              INTO vMaxCommId
              FROM COMMODITY_CODE
             WHERE DESCRIPTION_SHORT = vMaxCommId  and tc_company_id = vTCCompanyId;
			 
		END IF;

         IF (    vispendingauth > 0
             AND vduplicatelanedtlid > 0
             AND vratinglanedtlseq <> vduplicatelanedtlid)
         THEN
            AUDIT_LANE_ACCESSORIAL (vTCCompanyId,
                                    vLaneId,
                                    vRatingLaneDtlSeq,
                                    vAccessorialId,
                                    vRate,
                                    vMinimumRate,
                                    vCurrencyCode,
                                    l_minimum_size,
                                    l_maximum_size,
                                    l_size_uom,
                                    vIsApproved,
                                    vEffectiveDT,
                                    vExpirationDT,
                                    vPayeeCarrierCode,
                                    vIsShipmentAccessorial,
                                    vMinRateType,       -- MACR00159593 starts
                                    vMaximumRate,
                                    vPackageType,
                                    vCustomerRate,
                                    vCustomerMinCharge,
                                    vCalculatedRate,
                                    vAmount,
                                    vBaseAmount,
                                    vBaseCharge,
                                    vRangeMinAmount,
                                    vRangeMaxAmount,
                                    vIncrement,
                                    vMot,
                                    vEquipment,
                                    vServiceLevel,
                                    vProtectionLevel,
                                    vBillingMethod,
                                    vMinLongestDim,
                                    vMaxLongestDim,
                                    vMinSecondLongestDim,
                                    vMaxSecondLongestDim,
                                    vMinThirdLongestDim,
                                    vMaxThirdLongestDim,
                                    vMinLengthGrith,
                                    vMaxLengthGrith,
                                    vMinWeight,
                                    vMaxWeight,
                                    vIsZoneStopOff,
                                    vZoneId,              -- MACR00159593 ends
                                    vLastUpdatedSourceType,
                                    vLastUpdatedSource,
                                    vDuplicateLaneDtlId);
         ELSE
            INSERT_RATING_LANE_ACCESSORIAL (vTCCompanyId,
                                            vLaneId,
                                            vRatingLaneDtlSeq,
                                            vAccessorialId,
                                            vRate,
                                            vMinimumRate,
                                            vCurrencyCode,
                                            l_minimum_size,
                                            l_maximum_size,
                                            l_size_uom,
                                            vIsApproved,
                                            vEffectiveDT,
                                            vExpirationDT,
                                            vPayeeCarrierCode,
                                            vIsShipmentAccessorial,
                                            vMinRateType, -- MACR00159593 starts
                                            vMaximumRate,
                                            vPackageType,
                                            vCustomerRate,
                                            vCustomerMinCharge,
                                            vCalculatedRate,
                                            vAmount,
                                            vBaseAmount,
                                            vBaseCharge,
                                            vRangeMinAmount,
                                            vRangeMaxAmount,
                                            vIncrement,
                                            vMot,
                                            vEquipment,
                                            vServiceLevel,
                                            vProtectionLevel,
                                            vBillingMethod,
                                            vMinLongestDim,
                                            vMaxLongestDim,
                                            vMinSecondLongestDim,
                                            vMaxSecondLongestDim,
                                            vMinThirdLongestDim,
                                            vMaxThirdLongestDim,
                                            vMinLengthGrith,
                                            vMaxLengthGrith,
                                            vMinWeight,
                                            vMaxWeight,
                                            vIsZoneStopOff,
                                            vZoneId,       -- MACR00159593 ends
											vCommID,     -- CMGT-615 Starts here
											vMaxCommId		-- CMGT-615 Ends here	
                                                   );
         END IF;
		 
		 --MACR00716961 for Excluded Accessorials
		
				 
		FOR  EXCL_LANE_ACCESS_CURSOR
		IN (SELECT ACCESSORIAL_CODE_EXCL,ACCESSORIAL_CODE_EXCL_BU FROM IMP_EXCLUDED_LANE_ACC
			     WHERE tc_company_id = vTCCompanyId
       			 AND lane_id = vImportedLaneId
				 AND rating_lane_dtl_seq = vImpRatingLaneDtlSeq
				 AND ACCESSORIAL_ID = vAccessorialId)
		
		
				LOOP
					acc_code_excluded_cur := EXCL_LANE_ACCESS_CURSOR.ACCESSORIAL_CODE_EXCL;
					acc_code_excluded_bu_cur := EXCL_LANE_ACCESS_CURSOR.ACCESSORIAL_CODE_EXCL_BU;
			
					SELECT ROOTCOMPANYID INTO vRootCompanyId FROM IMPORT_RATING_LANE WHERE LANE_ID = vImportedLaneId;
					IF( acc_code_excluded_bu_cur IS NULL) THEN
						 vAccessorialBUID := vRootCompanyId;
					ELSE
						 vAccessorialBUID := Rating_Lane_Validation_Pkg.GET_BUID_FROM_BUSINESS_UNIT( vTCCompanyId, acc_code_excluded_cur);
					END IF;
			
			
					VPASSFAIL := Rating_Sub_Validation_Pkg.VALIDATE_ACCESSORIAL_CODE ( vAccessorialBUID , acc_code_excluded_cur);
					IF ( VPASSFAIL = 1) THEN
						 VFAILURE := 1;
						INSERT_RATING_LANE_DTL_ERROR(vTCCompanyId,vLaneId,vImpRatingLaneDtlSeq,acc_code_excluded_cur || ' is not a valid Excluded Accessorial Code',4720145,acc_code_excluded_cur);
					ELSE
						SELECT accessorial_id INTO vAccessorialTempID FROM accessorial_code WHERE accessorial_code = acc_code_excluded_cur AND tc_company_id = vAccessorialBUID;
				
						SELECT LANE_ACCESSORIAL_ID INTO vLaneAccessorialId FROM lane_accessorial WHERE tc_company_id = vTCCompanyId
						AND LANE_ID = vLaneId
						AND rating_lane_dtl_seq = vRatingLaneDtlSeq
						AND ACCESSORIAL_ID = vAccessorialId;
				
						INSERT INTO LANE_ACC_EXCLUDED_LIST(LANE_ACC_ID,EXCLUDED_ACC_ID) VALUES(vLaneAccessorialId, vAccessorialTempID);
			
					END IF;
				END LOOP;
		 
		 --MACR00716961 End
		 SELECT MAX(LANE_ACCESSORIAL_ID) INTO maxAccLaneId FROM LANE_ACCESSORIAL;
		 TRSFR_LANE_ACC_FEAS_INCOTERM(vTCCompanyId, vImportedLaneId, vImpRatingLaneDtlSeq,vImpLaneAccId, maxAccLaneId);
      END LOOP TRANSFER_LANE_ACCESS_LOOP;

      CLOSE TRANSFER_LANE_ACCESS_CURSOR;
   END TRANSFER_IMP_LANE_ACCESSORIAL;

   PROCEDURE TRANSFER_IMP_LANE_DTL_RATE (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vUser                  IN rating_lane_dtl.LAST_UPDATED_SOURCE%TYPE,
      vImportedLaneId        IN rating_lane.LANE_ID%TYPE,
      vLaneId                IN rating_lane.LANE_ID%TYPE,
      vImpRatingLaneDtlSeq   IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vRatingLaneDtlSeq      IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vIsPendingAuth         IN rating_lane_dtl.IS_BUDGETED%TYPE,
      vDuplicateLaneDtlId    IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE)
   AS
      vMinimumSize             IMPORT_RATING_LANE_DTL_RATE.MINIMUM_SIZE%TYPE;
      vMaximumSize             IMPORT_RATING_LANE_DTL_RATE.MAXIMUM_SIZE%TYPE;
      vSizeUOM                 IMPORT_RATING_LANE_DTL_RATE.SIZE_UOM_ID%TYPE;
      vRateCalcMethod          IMPORT_RATING_LANE_DTL_RATE.RATE_CALC_METHOD%TYPE;
      vRateUOM                 IMPORT_RATING_LANE_DTL_RATE.RATE_UOM%TYPE;
      vRate                    IMPORT_RATING_LANE_DTL_RATE.RATE%TYPE;
      vMinimumRate             IMPORT_RATING_LANE_DTL_RATE.MINIMUM_RATE%TYPE;
      vCurrencyCode            IMPORT_RATING_LANE_DTL_RATE.CURRENCY_CODE%TYPE;
      vTariffId                IMPORT_RATING_LANE_DTL_RATE.TARIFF_ID%TYPE;
      vMinDistance             IMPORT_RATING_LANE_DTL_RATE.MIN_DISTANCE%TYPE;
      vMaxDistance             IMPORT_RATING_LANE_DTL_RATE.MAX_DISTANCE%TYPE;
      vDistanceUOM             IMPORT_RATING_LANE_DTL_RATE.DISTANCE_UOM%TYPE;
      vSupportsMSTL            IMPORT_RATING_LANE_DTL_RATE.SUPPORTS_MSTL%TYPE;
      vHasBH                   IMPORT_RATING_LANE_DTL_RATE.HAS_BH%TYPE;
      vMinCommodity            IMPORT_RATING_LANE_DTL_RATE.MIN_COMMODITY%TYPE;
      vMaxCommodity            IMPORT_RATING_LANE_DTL_RATE.MAX_COMMODITY%TYPE;
      vHasRT                   IMPORT_RATING_LANE_DTL_RATE.HAS_RT%TYPE;
      vParcelTier              IMPORT_RATING_LANE_DTL_RATE.PARCEL_TIER%TYPE;
      vCommodityCodeId         IMPORT_RATING_LANE_DTL_RATE.COMMODITY_CODE_ID%TYPE;
      vExcessWtRate            IMPORT_RATING_LANE_DTL_RATE.EXCESS_WT_RATE%TYPE;
      vPayeeCarrier            IMPORT_RATING_LANE_DTL_RATE.PAYEE_CARRIER_ID%TYPE;
      vMaxCommodityCodeId      IMPORT_RATING_LANE_DTL_RATE.MAX_RANGE_COMMODITY_CODE_ID%TYPE;
      vIsSurgeRate             IMPORT_RATING_LANE_DTL_RATE.IS_SURGE_RATE%TYPE;
      vLastUpdatedSrcType      RATING_LANE_DTL_RATE.LAST_UPDATED_SRC_TYPE%TYPE;
      vLastUpdatedSrc          RATING_LANE_DTL_RATE.LAST_UPDATED_SRC%TYPE;
      vLastUpdatedDttm         RATING_LANE_DTL_RATE.LAST_UPDATED_DTTM%TYPE;
      vMinRateType             IMPORT_RATING_LANE_DTL_RATE.MIN_RATE_TYPE%TYPE;
      vBaseRate                IMPORT_RATING_LANE_DTL_RATE.BASE_RATE%TYPE;
      vMaxRate                 IMPORT_RATING_LANE_DTL_RATE.MAX_RATE%TYPE;
      vCustomerMinCharge       IMPORT_RATING_LANE_DTL_RATE.CUSTOMER_MIN_CHARGE%TYPE;
      vCustomerBaseRate        IMPORT_RATING_LANE_DTL_RATE.CUSTOMER_BASE_RATE%TYPE;
      vCustomerSurchargeRate   IMPORT_RATING_LANE_DTL_RATE.CUSTOMER_SURCHARGE_RATE%TYPE;
      vInsert                  INTEGER;

      CURSOR TRANSFER_DTL_RATE_CURSOR
      IS
         SELECT minimum_size,
                maximum_size,
                size_uom_id,
                RATE_CALC_METHOD,
                rate_uom,
                rate,
                minimum_rate,
                currency_code,
                tariff_id,
                min_distance,
                max_distance,
                DISTANCE_UOM,
                supports_mstl,
                has_bh,
                min_commodity,
                max_commodity,
                has_rt,
                parcel_tier,
                commodity_code_id,
                excess_wt_rate,
                payee_carrier_id,
                max_range_commodity_code_id,
                IS_SURGE_RATE,
                MIN_RATE_TYPE,
                BASE_RATE,
                MAX_RATE,
                CUSTOMER_MIN_CHARGE,
                CUSTOMER_BASE_RATE,
                CUSTOMER_SURCHARGE_RATE
           FROM IMPORT_RATING_LANE_DTL_RATE
          WHERE     tc_company_id = vTCCompanyId
                AND lane_id = vImportedLaneId
                AND rating_lane_dtl_seq = vImpRatingLaneDtlSeq;
   BEGIN
      vInsert := 0;

      OPEN TRANSFER_DTL_RATE_CURSOR;

     <<TRANSFER_DTL_RATE_LOOP>>
      LOOP
         FETCH TRANSFER_DTL_RATE_CURSOR
         INTO vMinimumSize,
              vMaximumSize,
              vSizeUOM,
              vRateCalcMethod,
              vRateUOM,
              vRate,
              vMinimumRate,
              vCurrencyCode,
              vTariffId,
              vMinDistance,
              vMaxDistance,
              vDistanceUOM,
              vSupportsMSTL,
              vHasBH,
              vMinCommodity,
              vMaxCommodity,
              vHasRT,
              vParcelTier,
              vCommodityCodeId,
              vExcessWtRate,
              vPayeeCarrier,
              vMaxCommodityCodeId,
              vIsSurgeRate,
              vMinRateType,
              vBaseRate,
              vMaxRate,
              vCustomerMinCharge,
              vCustomerBaseRate,
              vCustomerSurchargeRate;

         EXIT TRANSFER_DTL_RATE_LOOP WHEN TRANSFER_DTL_RATE_CURSOR%NOTFOUND;


         IF (vUser IS NOT NULL)
         THEN
            vLastUpdatedSrc := vUser;
            vLastUpdatedSrcType := 1;
         ELSE
            vLastUpdatedSrc := 'OMS';
            vLastUpdatedSrcType := 3;
         END IF;


         IF (    vIsPendingAuth = 1
             AND vDuplicateLaneDtlId > 0
             AND vRatingLaneDtlSeq <> vDuplicateLaneDtlId)
         THEN
            AUDIT_RATING_LANE_DTL_RATE (vTCCompanyId,
                                        vLaneId,
                                        vRatingLaneDtlSeq,
                                        vMinimumSize,
                                        vMaximumSize,
                                        vSizeUOM,
                                        vRateCalcMethod,
                                        vRateUOM,
                                        vRate,
                                        vMinimumRate,
                                        vCurrencyCode,
                                        vTariffId,
                                        vMinDistance,
                                        vMaxDistance,
                                        vDistanceUOM,
                                        vSupportsMSTL,
                                        vHasBH,
                                        vMinCommodity,
                                        vMaxCommodity,
                                        vHasRT,
                                        vParcelTier,
                                        vCommodityCodeId,
                                        vExcessWtRate,
                                        vPayeeCarrier,
                                        vMaxCommodityCodeId,
                                        VIsSurgeRate,
                                        vMinRateType,
                                        vBaseRate,
                                        vMaxRate,
                                        vCustomerMinCharge,
                                        vCustomerBaseRate,
                                        vCustomerSurchargeRate,
                                        vLastUpdatedSrcType,
                                        vLastUpdatedSrc,
                                        vLastUpdatedDttm,
                                        vDuplicateLaneDtlId);
         ELSE
            INSERT_RATING_LANE_DTL_RATE (vTCCompanyId,
                                         vLaneId,
                                         vRatingLaneDtlSeq,
                                         vMinimumSize,
                                         vMaximumSize,
                                         vSizeUOM,
                                         vRateCalcMethod,
                                         vRateUOM,
                                         vRate,
                                         vMinimumRate,
                                         vCurrencyCode,
                                         vTariffId,
                                         vMinDistance,
                                         vMaxDistance,
                                         vDistanceUOM,
                                         vSupportsMSTL,
                                         vHasBH,
                                         vMinCommodity,
                                         vMaxCommodity,
                                         vHasRT,
                                         vParcelTier,
                                         vCommodityCodeId,
                                         vExcessWtRate,
                                         vPayeeCarrier,
                                         vMaxCommodityCodeId,
                                         vIsSurgeRate,
                                         vMinRateType,
                                         vBaseRate,
                                         vMaxRate,
                                         vCustomerMinCharge,
                                         vCustomerBaseRate,
                                         vCustomerSurchargeRate);
         END IF;
      END LOOP TRANSFER_DTL_RATE_LOOP;

      CLOSE TRANSFER_DTL_RATE_CURSOR;
   END TRANSFER_IMP_LANE_DTL_RATE;


   PROCEDURE VALIDATE_IMPORT_LANES (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vUser           IN rating_lane.LAST_UPDATED_SOURCE%TYPE,
      vFirstBatchId   IN rating_lane.LANE_ID%TYPE,
      vLastBatchId    IN rating_lane.LANE_ID%TYPE)
   AS
      vLaneid   rating_lane.LANE_ID%TYPE;

      CURSOR IMPORTED_LANES_CURSOR
      IS
         SELECT LANE_ID
           FROM IMPORT_RATING_LANE
          WHERE     TC_COMPANY_ID = vTCCompanyId
                AND LANE_STATUS = 3
                AND BATCH_ID >= vFirstBatchId
                AND BATCH_ID <= vLastBatchId;
   BEGIN
      OPEN IMPORTED_LANES_CURSOR;

     <<IMPORTED_LANES_LOOP>>
      LOOP
         FETCH IMPORTED_LANES_CURSOR INTO vLaneId;

         EXIT IMPORTED_LANES_LOOP WHEN IMPORTED_LANES_CURSOR%NOTFOUND;
         VALIDATE_LANE (vTCCompanyId, vLaneId);
      END LOOP IMPORTED_LANES_LOOP;

      CLOSE IMPORTED_LANES_CURSOR;

      TRANSFER_IMPORT_LANES (vTCCompanyId,
                             vUser,
                             vFirstBatchId,
                             vLastBatchId);
   END VALIDATE_IMPORT_LANES;

   PROCEDURE VALIDATE_LANE_UI (vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
                               vLaneId        IN rating_lane.LANE_ID%TYPE)
   AS
      CURSOR lane_list
      IS
         SELECT tc_company_id, lane_id, batch_id
           FROM IMPORT_RATING_LANE
          WHERE tc_company_id = vTCCompanyId AND rating_lane_id = vLaneId;
   BEGIN
      FOR c_lane_list IN lane_list
      LOOP
         INITIALIZE_LANE_DTL_STATUS (c_lane_list.tc_company_id,
                                     c_lane_list.lane_id);

         DELETE FROM RATING_LANE_ERRORS
               WHERE TC_COMPANY_ID = c_lane_list.tc_company_id
                     AND LANE_ID = c_lane_list.lane_id;

         VALIDATE_LANE (c_lane_list.tc_company_id, c_lane_list.lane_id);
         TRANSFER_IMPORT_LANES (c_lane_list.tc_company_id,
                                NULL,
                                c_lane_list.batch_id,
                                c_lane_list.batch_id);
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE ('Procedure VALIDATE_LANE_UI: ' || SQLERRM);
         RAISE;
   END VALIDATE_LANE_UI;

   /* CR Note:
     Method added instead of removing the last line of this block (see end of validate_lane_dtl):
     UPDATE IMPORT_RATING_LANE_DTL
       SET RATING_LANE_DTL_STATUS = 1
       WHERE TC_COMPANY_ID = vTCCompanyId
       AND LANE_ID = vLaneId
       AND RATING_LANE_DTL_SEQ = vRatingLaneDtlSeq
       AND RATING_LANE_DTL_STATUS <> 4; -- CR Note: in Egg Nog, we should consider removing this last line...
     to avoid regression risks.
     Consider deleting both this method and the lane mentioned, in Egg Nog.
   */
   PROCEDURE INITIALIZE_LANE_DTL_STATUS (
      vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
      vLaneId        IN rating_lane.LANE_ID%TYPE)
   AS
   BEGIN
      UPDATE IMPORT_RATING_LANE_DTL
         SET RATING_LANE_DTL_STATUS = 3                         -- 3=imported.
       WHERE TC_COMPANY_ID = vTCCompanyId AND LANE_ID = vLaneId;

      COMMIT;
   END INITIALIZE_LANE_DTL_STATUS;

   PROCEDURE VALIDATE_LANE (vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
                            vLaneId        IN rating_lane.LANE_ID%TYPE)
   AS
      vBusinessUnit        BUSINESS_UNIT.BUSINESS_UNIT%TYPE;
      vOLocType            rating_lane.O_LOC_TYPE%TYPE;
      vOFacilityId         rating_lane.O_FACILITY_ID%TYPE;
      vOFacilityAliasId    rating_lane.O_FACILITY_ALIAS_ID%TYPE;
      vOFacilityBU         BUSINESS_UNIT.BUSINESS_UNIT%TYPE;
      vOCity               rating_lane.O_CITY%TYPE;
      vOStateProv          rating_lane.O_STATE_PROV%TYPE;
      vOCounty             rating_lane.O_COUNTY%TYPE;
      vOPostal             rating_lane.O_POSTAL_CODE%TYPE;
      vOCountry            rating_lane.O_COUNTRY_CODE%TYPE;
      vOZoneId             rating_lane.O_ZONE_ID%TYPE;
      vOZoneName           IMPORT_RATING_LANE.O_ZONE_NAME%TYPE;
      vDLocType            rating_lane.D_LOC_TYPE%TYPE;
      vDFacilityId         rating_lane.D_FACILITY_ID%TYPE;
      vDFacilityAliasId    rating_lane.D_FACILITY_ALIAS_ID%TYPE;
      vDFacilityBU         BUSINESS_UNIT.BUSINESS_UNIT%TYPE;
      vDCity               rating_lane.D_CITY%TYPE;
      vDStateProv          rating_lane.D_STATE_PROV%TYPE;
      vDCounty             rating_lane.D_COUNTY%TYPE;
      vDPostal             rating_lane.D_POSTAL_CODE%TYPE;
      vDCountry            rating_lane.D_COUNTRY_CODE%TYPE;
      vDZoneId             rating_lane.D_ZONE_ID%TYPE;
      vDZoneName           IMPORT_RATING_LANE.D_ZONE_NAME%TYPE;

      vCustomerId          rating_lane.CUSTOMER_ID%TYPE;
      vCustomerCode        IMPORT_RATING_LANE.CUSTOMER_CODE%TYPE;
      vCustomerCodeBU      IMPORT_RATING_LANE.CUSTOMER_CODE_BU%TYPE;
      vBillingMethod       rating_lane.BILLING_METHOD%TYPE;
      vBillingMethodCode   IMPORT_RATING_LANE.BILLING_METHOD_CODE%TYPE;
      vIncotermId          rating_lane.INCOTERM_ID%TYPE;
      vIncotermName        IMPORT_RATING_LANE.INCOTERM_NAME%TYPE;
      vIncotermNameBU      IMPORT_RATING_LANE.INCOTERM_NAME_BU%TYPE;

      vPassFail            NUMBER;
      vFailure             NUMBER;

      CURSOR LANE_CURSOR
      IS
         SELECT BUSINESS_UNIT,
                O_LOC_TYPE,
                O_FACILITY_ID,
                O_FACILITY_ALIAS_ID,
                O_FACILITY_BU,
                O_CITY,
                O_STATE_PROV,
                O_COUNTY,
                O_POSTAL_CODE,
                O_COUNTRY_CODE,
                O_ZONE_ID,
                O_ZONE_NAME,
                D_LOC_TYPE,
                D_FACILITY_ID,
                D_FACILITY_ALIAS_ID,
                D_FACILITY_BU,
                D_CITY,
                D_STATE_PROV,
                D_COUNTY,
                D_POSTAL_CODE,
                D_COUNTRY_CODE,
                D_ZONE_ID,
                D_ZONE_NAME,
                CUSTOMER_ID,
                CUSTOMER_CODE,
                CUSTOMER_CODE_BU,
                BILLING_METHOD,
                BILLING_METHOD_CODE,
                INCOTERM_ID,
                INCOTERM_NAME,
                INCOTERM_NAME_BU
           FROM IMPORT_RATING_LANE
          WHERE TC_COMPANY_ID = vTCCompanyId AND LANE_ID = vLaneId;
   BEGIN
      OPEN LANE_CURSOR;

     <<LANES_LOOP>>
      LOOP
         FETCH LANE_CURSOR
         INTO vBusinessUnit,
              vOLocType,
              vOFacilityId,
              vOFacilityAliasId,
              vOFacilityBU,
              vOCity,
              vOStateProv,
              vOCounty,
              vOPostal,
              vOCountry,
              vOZoneId,
              vOZoneName,
              vDLocType,
              vDFacilityId,
              vDFacilityAliasId,
              vDFacilityBU,
              vDCity,
              vDStateProv,
              vDCounty,
              vDPostal,
              vDCountry,
              vDZoneId,
              vDZoneName,
              vCustomerId,
              vCustomerCode,
              vCustomerCodeBU,
              vBillingMethod,
              vBillingMethodCode,
              vIncotermId,
              vIncotermName,
              vIncotermNameBU;

         EXIT LANES_LOOP WHEN LANE_CURSOR%NOTFOUND;

         /* moved to validate_lane_ui
         DELETE FROM RATING_LANE_ERRORS
          WHERE TC_COMPANY_ID = vTCCompanyId
          AND LANE_ID = vLaneId;
         */

         COMMIT;
         vFailure := 0;
         -- nTime := dbms_utility.get_time();
         vPassFail :=
            VALIDATE_LANE_BASE_DATA (vTCCompanyId,
                                     vLaneId,
                                     vBusinessUnit,
                                     vOFacilityId,
                                     vOFacilityAliasId,
                                     vOFacilityBU,
                                     vOStateProv,
                                     vOPostal,
                                     vOCountry,
                                     vOZoneId,
                                     vOZoneName,
                                     vDFacilityId,
                                     vDFacilityAliasId,
                                     vDFacilityBU,
                                     vDStateProv,
                                     vDPostal,
                                     vDCountry,
                                     vDZoneId,
                                     vDZoneName,
                                     vCustomerId,
                                     vCustomerCode,
                                     vCustomerCodeBU,
                                     vBillingMethod,
                                     vBillingMethodCode,
                                     vIncotermId,
                                     vIncotermName,
                                     vIncotermNameBU);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
         END IF;

         --insert into TIME_IMPORT_LANE_DTL_KDM values ('VALIDATE_LANE','VALIDATE_LANE_BASE_DATA',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();
         -- Validate Parent level of Lane
         vPassFail :=
            VALIDATE_HIERARCHY (vTCCompanyId,
                                vLaneId,
                                vOFacilityId,
                                vOFacilityAliasId,
                                vOCity,
                                vOStateProv,
                                vOCounty,
                                vOPostal,
                                vOCountry,
                                vOZoneId,
                                vOZoneName,
                                vDFacilityId,
                                vDFacilityAliasId,
                                vDCity,
                                vDStateProv,
                                vDCounty,
                                vDPostal,
                                vDCountry,
                                vDZoneId,
                                vDZoneName);

         --insert into TIME_IMPORT_LANE_DTL_KDM values ('VALIDATE_LANE','VALIDATE_HIERARCHY',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();
         IF (vPassFail = 1)
         THEN
            vFailure := 1;
         END IF;

         IF (vFailure = 1)
         THEN
            UPDATE IMPORT_RATING_LANE
               SET LANE_STATUS = 4,
                   LAST_UPDATED_DTTM = SYSDATE,
                   LAST_UPDATED_SOURCE_TYPE = 3,
                   LAST_UPDATED_SOURCE = 'OMS'
             WHERE TC_COMPANY_ID = vTCCompanyId AND LANE_ID = vLaneId;

            COMMIT;
         ELSE
            UPDATE IMPORT_RATING_LANE
               SET LANE_STATUS = 1,
                   LAST_UPDATED_DTTM = SYSDATE,
                   LAST_UPDATED_SOURCE_TYPE = 3,
                   LAST_UPDATED_SOURCE = 'OMS'
             WHERE TC_COMPANY_ID = vTCCompanyId AND LANE_ID = vLaneId;

            COMMIT;
         END IF;

         --insert into TIME_IMPORT_LANE_DTL_KDM values ('VALIDATE_LANE','UPDATE IMPORT_RATING_LANE',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();
         VALIDATE_LANE_DTL (vTCCompanyId, vLaneId);
      --insert into TIME_IMPORT_LANE_DTL_KDM values ('VALIDATE_LANE','VALIDATE_LANE_DTL',dbms_utility.get_time() - nTime);
      END LOOP LANES_LOOP;

      CLOSE LANE_CURSOR;
   END VALIDATE_LANE;

   PROCEDURE VALIDATE_LANE_DTL (vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
                                vLaneId        IN rating_lane.LANE_ID%TYPE)
   AS
      vCarrierCode               IMPORT_RATING_LANE_DTL.CARRIER_CODE%TYPE;
      vMode                      IMPORT_RATING_LANE_DTL.MOT%TYPE;
      vServiceLevel              IMPORT_RATING_LANE_DTL.SERVICE_LEVEL%TYPE;
      vEquipment                 IMPORT_RATING_LANE_DTL.EQUIPMENT_CODE%TYPE;
      vProtectionLevel           IMPORT_RATING_LANE_DTL.PROTECTION_LEVEL%TYPE;

      vCarrierCodeBU             IMPORT_RATING_LANE_DTL.TP_CODE_BU%TYPE;
      vModeBU                    IMPORT_RATING_LANE_DTL.MOT_BU%TYPE;
      vServiceLevelBU            IMPORT_RATING_LANE_DTL.SERVICE_LEVEL_BU%TYPE;
      vEquipmentBU               IMPORT_RATING_LANE_DTL.EQUIPMENT_BU%TYPE;
      vProtectionLevelBU         IMPORT_RATING_LANE_DTL.PROTECTION_LEVEL_BU%TYPE;

      vEffectiveDT               rating_lane_dtl.EFFECTIVE_DT%TYPE;
      vExpirationDT              rating_lane_dtl.EXPIRATION_DT%TYPE;
      vIsBudgeted                rating_lane_dtl.IS_BUDGETED%TYPE;
      vPassFail                  NUMBER;
      vFailure                   NUMBER;

      vCarrierCodeBUID           COMPANY.COMPANY_ID%TYPE;
      vModeBUID                  COMPANY.COMPANY_ID%TYPE;
      vServiceLevelBUID          COMPANY.COMPANY_ID%TYPE;
      vEquipmentBUID             COMPANY.COMPANY_ID%TYPE;
      vProtectionLevelBUID       COMPANY.COMPANY_ID%TYPE;
      l_scndr_carrier_codeBUID   COMPANY.COMPANY_ID%TYPE;
      vRootCompanyId             COMPANY.COMPANY_ID%TYPE;


      vOrigShipVia               IMPORT_RATING_LANE_DTL.O_SHIP_VIA%TYPE;
      vOrigShipViaBU             IMPORT_RATING_LANE_DTL.O_SHIP_VIA_BU%TYPE;
      vDestShipVia               IMPORT_RATING_LANE_DTL.D_SHIP_VIA%TYPE;
      vDestShipViaBU             IMPORT_RATING_LANE_DTL.D_SHIP_VIA_BU%TYPE;
      vOrigShipViaBUID           COMPANY.COMPANY_ID%TYPE;
      vDestShipViaBUID           COMPANY.COMPANY_ID%TYPE;


      vRatingLaneDtlSeq          rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE;
      l_scndr_carrier_code       IMPORT_RATING_LANE_DTL.SCNDR_CARRIER_CODE%TYPE;
      l_scndr_carrier_codeBU     IMPORT_RATING_LANE_DTL.SECONDARY_CARRIER_CODE_BU%TYPE;

      CURSOR LANE_DTL_CURSOR
      IS
         SELECT CARRIER_CODE,
                TP_CODE_BU,
                MOT,
                MOT_BU,
                SERVICE_LEVEL,
                SERVICE_LEVEL_BU,
                EQUIPMENT_CODE,
                EQUIPMENT_BU,
                PROTECTION_LEVEL,
                PROTECTION_LEVEL_BU,
                IS_BUDGETED,
                RATING_LANE_DTL_SEQ,
                EFFECTIVE_DT,
                EXPIRATION_DT,
                SCNDR_CARRIER_CODE,
                SECONDARY_CARRIER_CODE_BU,
                O_SHIP_VIA,
                O_SHIP_VIA_BU,
                D_SHIP_VIA,
                D_SHIP_VIA_BU
           FROM IMPORT_RATING_LANE_DTL
          WHERE TC_COMPANY_ID = vTCCompanyId AND LANE_ID = vLaneId;
   BEGIN
      OPEN LANE_DTL_CURSOR;

     <<LANES_DTL_LOOP>>
      LOOP
         vPassFail := 0;
         vFailure := 0;

         FETCH LANE_DTL_CURSOR
         INTO vCarrierCode,
              vCarrierCodeBU,
              vMode,
              vModeBU,
              vServiceLevel,
              vServiceLevelBU,
              vEquipment,
              vEquipmentBU,
              vProtectionLevel,
              vProtectionLevelBU,
              vIsBudgeted,
              vRatingLaneDtlSeq,
              vEffectiveDT,
              vExpirationDT,
              l_scndr_carrier_code,
              l_scndr_carrier_codeBU,
              vOrigShipVia,
              vOrigShipViaBU,
              vDestShipVia,
              vDestShipViaBU;

         EXIT LANES_DTL_LOOP WHEN LANE_DTL_CURSOR%NOTFOUND;

         -- nTime := dbms_utility.get_time();

         SELECT ROOTCOMPANYID
           INTO vRootCompanyId
           FROM IMPORT_RATING_LANE
          WHERE LANE_ID = vLaneId;

         IF (vCarrierCodeBU IS NULL)
         THEN
            vCarrierCodeBUID := vRootCompanyId;
         ELSE
            vCarrierCodeBUID :=
               Rating_Lane_Validation_Pkg.GET_BUID_FROM_BUSINESS_UNIT (
                  vRootCompanyId,
                  vCarrierCodeBU);
         END IF;

         IF (vModeBU IS NULL)
         THEN
            vModeBUID := vRootCompanyId;
         ELSE
            vModeBUID :=
               Rating_Lane_Validation_Pkg.GET_BUID_FROM_BUSINESS_UNIT (
                  vRootCompanyId,
                  vModeBU);
         END IF;

         IF (vServiceLevelBU IS NULL)
         THEN
            vServiceLevelBUID := vRootCompanyId;
         ELSE
            vServiceLevelBUID :=
               Rating_Lane_Validation_Pkg.GET_BUID_FROM_BUSINESS_UNIT (
                  vRootCompanyId,
                  vServiceLevelBU);
         END IF;

         IF (vEquipmentBU IS NULL)
         THEN
            vEquipmentBUID := vRootCompanyId;
         ELSE
            vEquipmentBUID :=
               Rating_Lane_Validation_Pkg.GET_BUID_FROM_BUSINESS_UNIT (
                  vRootCompanyId,
                  vEquipmentBU);
         END IF;

         IF (vProtectionLevelBU IS NULL)
         THEN
            vProtectionLevelBUID := vRootCompanyId;
         ELSE
            vProtectionLevelBUID :=
               Rating_Lane_Validation_Pkg.GET_BUID_FROM_BUSINESS_UNIT (
                  vRootCompanyId,
                  vProtectionLevelBU);
         END IF;

         IF (l_scndr_carrier_codeBU IS NULL)
         THEN
            l_scndr_carrier_codeBUID := vRootCompanyId;
         ELSE
            l_scndr_carrier_codeBUID :=
               Rating_Lane_Validation_Pkg.GET_BUID_FROM_BUSINESS_UNIT (
                  vRootCompanyId,
                  l_scndr_carrier_codeBU);
         END IF;

         vPassFail :=
            VALIDATE_DETAIL_BASE_DATA (vTCCompanyId,
                                       vLaneId,
                                       vRatingLaneDtlSeq,
                                       vCarrierCode,
                                       vCarrierCodeBUID,
                                       vMode,
                                       vModeBUID,
                                       vServiceLevel,
                                       vServiceLevelBUID,
                                       vEquipment,
                                       vEquipmentBUID,
                                       vProtectionLevel,
                                       vProtectionLevelBUID,
                                       l_scndr_carrier_code,
                                       l_scndr_carrier_codeBUID,
                                       vIsBudgeted,
                                       vEffectiveDT,
                                       vExpirationDT);

         --insert into TIME_IMPORT_LANE_DTL_KDM values ('VALIDATE_LANE_DTL','VALIDATE_DETAIL_BASE_DATA',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();
         IF (vPassFail = 1)
         THEN
            DBMS_OUTPUT.PUT_LINE ('VALIDATE_DETAIL_BASE_DATA failed');
            vFailure := 1;
         ELSE
            vPassFail :=
               VALIDATE_TP_EQUIP_SERV_MODE (vTCCompanyId,
                                            vLaneId,
                                            vRatingLaneDtlSeq,
                                            vCarrierCode,
                                            vServiceLevel,
                                            vEquipment,
                                            vMode,
                                            l_scndr_carrier_code);

            IF (vPassFail = 1)
            THEN
               DBMS_OUTPUT.PUT_LINE ('VALIDATE_TP_EQUIP_SERV_MODE failed');
               vFailure := 1;
            END IF;
         END IF;

         --insert into TIME_IMPORT_LANE_DTL_KDM values ('VALIDATE_LANE_DTL','VALIDATE_TP_EQUIP_SERV_MODE',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();

         IF (vEquipment IS NOT NULL)
         THEN
            vPassFail :=
               VALIDATE_FEASIBLE (vTCCompanyId,
                                  vLaneId,
                                  vRatingLaneDtlSeq,
                                  vCarrierCode,
                                  vCarrierCodeBUID,
                                  vEquipment,
                                  vEquipmentBUID,
                                  vProtectionLevel,
                                  vProtectionLevelBUID,
                                  l_scndr_carrier_code,
                                  l_scndr_carrier_codeBUID);

            IF (vPassFail = 1)
            THEN
               DBMS_OUTPUT.PUT_LINE ('VALIDATE_FEASIBLE failed');
               vFailure := 1;
            END IF;
         END IF;

         --insert into TIME_IMPORT_LANE_DTL_KDM values ('VALIDATE_LANE_DTL','VALIDATE_FEASIBLE',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();

         --already validated in VALIDATE_FEASIBLE
         vPassFail :=
            VALIDATE_FAC_CARR_FEASIBLE (vTCCompanyId,
                                        vLaneId,
                                        vRatingLaneDtlSeq,
                                        vCarrierCode,
                                        vCarrierCodeBUID,
                                        l_scndr_carrier_code,
                                        l_scndr_carrier_codeBUID);

         IF (vPassFail = 1)
         THEN
            DBMS_OUTPUT.PUT_LINE ('Validate Feasible Failed');
            vFailure := 1;
         END IF;

         --insert into TIME_IMPORT_LANE_DTL_KDM values ('VALIDATE_LANE_DTL','VALIDATE_FAC_CARR_FEASIBLE',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();
         vPassFail :=
            VALIDATE_LANE_DTL_ACCESSORIAL (vTCCompanyId,
                                           vLaneId,
                                           vRatingLaneDtlSeq);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            DBMS_OUTPUT.PUT_LINE ('VALIDATE_LANE_DTL_ACCESSORIAL failed');
         END IF;

         --insert into TIME_IMPORT_LANE_DTL_KDM values ('VALIDATE_LANE_DTL','VALIDATE_LANE_DTL_ACCESSORIAL',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();
         vPassFail :=
            VALIDATE_LANE_DTL_RATE (vTCCompanyId, vLaneId, vRatingLaneDtlSeq);

         IF (vPassFail = 1)
         THEN
            DBMS_OUTPUT.PUT_LINE ('VALIDATE_LANE_DTL_RATE failed');
            vFailure := 1;
         END IF;

         --insert into TIME_IMPORT_LANE_DTL_KDM values ('VALIDATE_LANE_DTL','VALIDATE_LANE_DTL_RATE',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();


         IF (vOrigShipViaBU IS NULL)
         THEN
            vOrigShipViaBUID := vRootCompanyId;
         ELSE
            vOrigShipViaBUID :=
               Rating_Lane_Validation_Pkg.GET_BUID_FROM_BUSINESS_UNIT (
                  vRootCompanyId,
                  vOrigShipViaBU);
         END IF;

         IF (vOrigShipVia IS NOT NULL)
         THEN
            vPassFail :=
               RATING_SUB_VALIDATION_PKG.VALIDATE_SHIP_VIA (vOrigShipViaBUID,
                                                            vOrigShipVia);

            IF (vPassFail = 1)
            THEN
               DBMS_OUTPUT.PUT_LINE ('VALIDATE_LANE_DTL_RATE failed');
               vFailure := 1;

               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingLaneDtlSeq,
                     'Origin Ship Through '
                  || vOrigShipVia
                  || ' is not a valid Port',
                  4000026,
                  vOrigShipVia);
            END IF;
         END IF;

         IF (vDestShipViaBU IS NULL)
         THEN
            vDestShipViaBUID := vRootCompanyId;
         ELSE
            vDestShipViaBUID :=
               Rating_Lane_Validation_Pkg.GET_BUID_FROM_BUSINESS_UNIT (
                  vRootCompanyId,
                  vDestShipViaBU);
         END IF;

         IF (vDestShipVia IS NOT NULL)
         THEN
            vPassFail :=
               RATING_SUB_VALIDATION_PKG.VALIDATE_SHIP_VIA (vDestShipViaBUID,
                                                            vDestShipVia);

            IF (vPassFail = 1)
            THEN
               DBMS_OUTPUT.PUT_LINE ('VALIDATE_LANE_DTL_RATE failed');
               vFailure := 1;

               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingLaneDtlSeq,
                     'Destination Ship Through '
                  || vDestShipVia
                  || ' is not a valid Port',
                  4000026,
                  vDestShipVia);
            END IF;
         END IF;



         vPassFail :=
            VALIDATE_CUST_CARR_MOT_FEAS (vTCCompanyId,
                                         vLaneId,
                                         vRatingLaneDtlSeq,
                                         vCarrierCode,
                                         vMode);

         IF (vPassFail = 1)
         THEN
            DBMS_OUTPUT.PUT_LINE ('VALIDATE_CUST_CARR_MOT_FEAS failed');
            vFailure := 1;
         END IF;



         IF (vFailure = 1)
         THEN
            UPDATE IMPORT_RATING_LANE_DTL
               SET RATING_LANE_DTL_STATUS = 4
             WHERE     TC_COMPANY_ID = vTCCompanyId
                   AND LANE_ID = vLaneId
                   AND RATING_LANE_DTL_SEQ = vRatingLaneDtlSeq;

            COMMIT;
         ELSE
            UPDATE IMPORT_RATING_LANE_DTL
               SET RATING_LANE_DTL_STATUS = 1
             WHERE     TC_COMPANY_ID = vTCCompanyId
                   AND LANE_ID = vLaneId
                   AND RATING_LANE_DTL_SEQ = vRatingLaneDtlSeq
                   AND RATING_LANE_DTL_STATUS <> 4; -- CR Note: in Egg Nog, we should consider removing this last line...

            COMMIT;
         END IF;
      --insert into TIME_IMPORT_LANE_DTL_KDM values ('VALIDATE_LANE_DTL','UPD IMPORT_RATING_LANE_DTL',dbms_utility.get_time() - nTime);
      -- nTime := dbms_utility.get_time();
      END LOOP LANES_DTL_LOOP;

      CLOSE LANE_DTL_CURSOR;
   END VALIDATE_LANE_DTL;

   FUNCTION VALIDATE_LANE_DTL_RATE (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vLaneId         IN rating_lane.LANE_ID%TYPE,
      vRatingDtlSeq   IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE)
      RETURN NUMBER
   IS
      CURSOR LANE_DTL_RATE_CURSOR
      IS
         SELECT RLD_RATE_SEQ,
                MINIMUM_SIZE,
                MAXIMUM_SIZE,
                SIZE_UOM,
                SIZE_UOM_BU,
                RATE_CALC_METHOD,
                RATE_UOM,
                CURRENCY_CODE,
                TARIFF_ID,
                MIN_DISTANCE,
                MAX_DISTANCE,
                DISTANCE_UOM,
                SUPPORTS_MSTL,
                HAS_BH,
                MIN_COMMODITY,
                MAX_COMMODITY,
                HAS_RT,
                PARCEL_TIER,
                PAYEE_CARRIER_CODE,
                PAYEE_CARRIER_BU,
                COMMODITY_CODE_ID,
                MAX_RANGE_COMMODITY_CODE_ID,
                CUSTOMER_BASE_RATE,
                CUSTOMER_SURCHARGE_RATE,
                ROWID
           FROM IMPORT_RATING_LANE_DTL_RATE
          WHERE     TC_COMPANY_ID = vTCCompanyId
                AND LANE_ID = vLaneId
                AND RATING_LANE_DTL_SEQ = vRatingDtlSeq;

      /* Horea 03/30/2002
       The returned rows have to be distinct for fetch
      so I selected rld_rate_seq too in order to avoid exception:
      ORA 01422: exact fetch returns more than requested number of rows
      */
      -- vRldRateSeq   NUMBER;
      -- vMinSize   FLOAT;
      -- vMaxSize   FLOAT;
      -- vSizeUOM   VARCHAR2(8);
      -- vRateCalcMethod  RATING_LANE_DTL_RATE.RATE_CALC_METHOD%TYPE;
      -- vRateUOM   RATING_LANE_DTL_RATE.RATE_UOM%TYPE;
      -- vCurrencyCode  RATING_LANE_DTL_RATE.CURRENCY_CODE%TYPE;
      -- vTariffId   RATING_LANE_DTL_RATE.TARIFF_ID%TYPE;
      -- vMinDistance   RATING_LANE_DTL_RATE.MIN_DISTANCE%TYPE;
      -- vMaxDistance   RATING_LANE_DTL_RATE.MAX_DISTANCE%TYPE;
      -- vDistanceUOM   RATING_LANE_DTL_RATE.DISTANCE_UOM%TYPE;
      -- vSupportsMSTL  RATING_LANE_DTL_RATE.SUPPORTS_MSTL%TYPE;
      -- vHasBH     RATING_LANE_DTL_RATE.HAS_BH%TYPE;
      -- vMinCommodity   RATING_LANE_DTL_RATE.MIN_COMMODITY%TYPE;
      -- vMaxCommodity   RATING_LANE_DTL_RATE.MAX_COMMODITY%TYPE;
      -- vHasRT     RATING_LANE_DTL_RATE.HAS_RT%TYPE;
      vResult                     NUMBER;
      vPassFail                   NUMBER;
      vFailure                    NUMBER;
      -- vRowId    ROWID;
      vsqlStatement               VARCHAR2 (4000);
      vCount                      NUMBER;
      nTime                       NUMBER;
      nTcId                       NUMBER;
      vRootCompanyId              NUMBER;
      vSizeUomId                  RATING_LANE_DTL_RATE.SIZE_UOM_ID%TYPE;
      vPayeeCarrierId             RATING_LANE_DTL_RATE.payee_carrier_id%TYPE;
      vUseRangeForCommodityCode   VARCHAR2 (5);
      vCommodityCode              VARCHAR2 (16);
      vMaxRangeCommodityCode      VARCHAR2 (16);
      RUOMstr                     VARCHAR2 (20);
      paramValueExists            NUMBER;
	  vExternalCarrCount          NUMBER;
   BEGIN
      FOR r IN LANE_DTL_RATE_CURSOR
      LOOP
         vPassFail := 0;

         SELECT ROOTCOMPANYID
           INTO vRootCompanyId
           FROM IMPORT_RATING_LANE
          WHERE LANE_ID = vLaneId;


         SELECT COUNT (PARAM_VALUE)
           INTO paramValueExists
           FROM COMPANY_PARAMETER
          WHERE TC_COMPANY_ID = vTCCompanyId
                AND PARAM_DEF_ID = 'use_range_for_commodity_codes';

         IF (paramValueExists > 0)
         THEN
            SELECT PARAM_VALUE
              INTO vUseRangeForCommodityCode
              FROM COMPANY_PARAMETER
             WHERE TC_COMPANY_ID = vTCCompanyId
                   AND PARAM_DEF_ID = 'use_range_for_commodity_codes';
         ELSE
            SELECT DFLT_VALUE
              INTO vUseRangeForCommodityCode
              FROM PARAM_DEF
             WHERE PARAM_DEF_ID = 'use_range_for_commodity_codes';
         END IF;

         IF (r.COMMODITY_CODE_ID IS NOT NULL)
         THEN
         BEGIN
            SELECT DESCRIPTION_SHORT
              INTO vCommodityCode
              FROM COMMODITY_CODE
             WHERE COMMODITY_CODE_ID = r.COMMODITY_CODE_ID;
              exception when no_data_found then
              vCommodityCode := 'NA';
              END;
        END IF;

         IF (r.MAX_RANGE_COMMODITY_CODE_ID IS NOT NULL)
         THEN
          BEGIN
            SELECT DESCRIPTION_SHORT
              INTO vMaxRangeCommodityCode
              FROM COMMODITY_CODE
             WHERE COMMODITY_CODE_ID = r.MAX_RANGE_COMMODITY_CODE_ID;
             exception when no_data_found then
              vMaxRangeCommodityCode := 'NA';
              END;
         END IF;
         
         IF (r.COMMODITY_CODE_ID IS NOT NULL)
         THEN
         begin 
    
            SELECT DESCRIPTION_SHORT
              INTO vCommodityCode
              FROM COMMODITY_CODE
             WHERE COMMODITY_CODE_ID = r.COMMODITY_CODE_ID;
             exception when no_data_found then
             vCommodityCode := r.COMMODITY_CODE_ID;
             vFailure := 1;
             INSERT_RATING_LANE_DTL_ERROR (
                     vTCCompanyId,
                     vLaneId,
                     vRatingDtlSeq,
                     r.COMMODITY_CODE_ID || ' is an invalid Commodity Code',
                     4000028,
                     vCommodityCode);
             end;
           
         END IF;
   
    
      IF (r.MAX_RANGE_COMMODITY_CODE_ID IS NOT NULL)
         THEN
         begin
            SELECT DESCRIPTION_SHORT
              INTO vMaxRangeCommodityCode
              FROM COMMODITY_CODE
             WHERE COMMODITY_CODE_ID = r.MAX_RANGE_COMMODITY_CODE_ID;
             exception when no_data_found then
             vMaxRangeCommodityCode := r.MAX_RANGE_COMMODITY_CODE_ID;
             vFailure := 1;
             INSERT_RATING_LANE_DTL_ERROR (
                     vTCCompanyId,
                     vLaneId,
                     vRatingDtlSeq,
                     r.MAX_RANGE_COMMODITY_CODE_ID || ' is an invalid Commodity Code',
                     4000028,
                     vMaxRangeCommodityCode);
             end;
         END IF;
		 

         IF (r.PAYEE_CARRIER_BU IS NULL)
         THEN
            nTcId := vRootCompanyId;
         ELSE
            nTcId :=
               Rating_Lane_Validation_Pkg.GET_BUID_FROM_BUSINESS_UNIT (
                  vRootCompanyId,
                  r.PAYEE_CARRIER_BU);
         END IF;

         IF (r.PAYEE_CARRIER_CODE IS NOT NULL)
         THEN
            -- verify that tariff exists in the database (TARIFF table)
            BEGIN
               SELECT 1
                 INTO vResult
                 FROM CARRIER_CODE
                WHERE     TC_COMPANY_ID = nTcId
                      AND CARRIER_CODE = r.PAYEE_CARRIER_CODE
                      AND is_payee = 1
                      AND CARRIER_CODE_STATUS = 0
					  AND MARK_FOR_DELETION = 0;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  vResult := NULL;
            END;

            IF (vResult IS NULL)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                     'Payee Carrier ('
                  || r.PAYEE_CARRIER_CODE
                  || ') is not valid',
                  4720174,
                  r.PAYEE_CARRIER_CODE);
            ELSE
               SELECT carrier_id
                 INTO vPayeeCarrierId
                 FROM CARRIER_CODE
                WHERE CARRIER_CODE = r.PAYEE_CARRIER_CODE
                      AND tc_company_id = nTcId
					  AND MARK_FOR_DELETION = 0;

               UPDATE IMPORT_RATING_LANE_DTL_RATE
                  SET payee_carrier_id = vPayeeCarrierId
                WHERE     payee_carrier_code = r.PAYEE_CARRIER_CODE
                      AND tc_company_id = vTCCompanyId
                      AND lane_id = vLaneId
                      AND RATING_LANE_DTL_SEQ = vRatingDtlSeq;
					  
			   SELECT COUNT(CARRIER_ID) INTO vExternalCarrCount FROM CARRIER_CODE WHERE CARRIER_ID = vPayeeCarrierId AND carrier_type_id = 24;
				IF (vExternalCarrCount > 0)
				THEN
					vFailure := 1;
					INSERT_RATING_LANE_DTL_ERROR (
						vTCCompanyId,
						vLaneId,
						vRatingDtlSeq,
						'External Parcel Carrier '||r.PAYEE_CARRIER_CODE||' cannot be used as a Payee Carrier',
						4720203,
						r.PAYEE_CARRIER_CODE);
				END IF;

               COMMIT;
            END IF;
         END IF;

         IF (r.CUSTOMER_BASE_RATE IS NOT NULL
             AND r.CUSTOMER_SURCHARGE_RATE IS NOT NULL)
         THEN
            vFailure := 1;
            INSERT_RATING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRatingDtlSeq,
               'Either of Customer Base Rate or Customer Surcharge Rate should be given for Customer Total Cost calculation.',
               4720191,
               NULL);
         END IF;

         IF (r.RATE_CALC_METHOD IS NULL)
         THEN
            vFailure := 1;
            INSERT_RATING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRatingDtlSeq,
               'Rate Calculation Method is required',
               4720114,
               NULL);
         ELSE                                   -- vRateCalcMethod IS NOT NULL
            -- nTime := dbms_utility.get_time();
            --BEGIN
            SELECT COUNT (*)
              INTO vResult
              FROM RATE_CALC_METHOD
             WHERE RATE_CALC_METHOD = r.RATE_CALC_METHOD;

            -- EXCEPTION
            --  WHEN NO_DATA_FOUND THEN
            --    vResult := NULL;
            --END;
            IF (vResult = 0)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                     'Rate Calculation Method ('
                  || r.RATE_CALC_METHOD
                  || ') is invalid',
                  4720115,
                  r.RATE_CALC_METHOD);
            END IF;
         --insert into TIME_IMPORT_LANE_DTL_KDM values ('val2','rcm',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();
         END IF;



         IF (r.RATE_UOM IS NOT NULL)
         THEN
            /* validate rateUOM here... */
            --BEGIN
            SELECT COUNT (*)
              INTO vResult
              FROM SIZE_UOM
             WHERE TO_CHAR (SIZE_UOM_ID) = r.RATE_UOM;

            --AND TC_COMPANY_ID = vTCCompanyId;
            -- EXCEPTION
            --  WHEN NO_DATA_FOUND THEN
            --    vResult := NULL;
            --END;
            IF ( (    UPPER (r.RATE_CALC_METHOD) = 'RPD'
                  AND UPPER (r.RATE_UOM) <> 'MI'
                  AND UPPER (r.RATE_UOM) <> 'KM'
                  AND vResult = 0)
                --AND vResult IS NULL)
                OR (UPPER (r.RATE_CALC_METHOD) = 'CWT' AND vResult = 0)
                --AND vResult IS NULL)
                OR (    UPPER (r.RATE_CALC_METHOD) = 'RADL'
                    AND UPPER (r.RATE_UOM) <> 'MI'
                    AND UPPER (r.RATE_UOM) <> 'KM'))
            THEN
               vFailure := 1;

               IF (vResult > 0)
               THEN
                  SELECT SIZE_UOM
                    INTO RUOMstr
                    FROM SIZE_UOM
                   WHERE TO_CHAR (SIZE_UOM_ID) = r.RATE_UOM;
               ELSE
                  RUOMstr := r.RATE_UOM;
               END IF;

               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                     'Rate UOM ('
                  || RUOMstr
                  || ') is invalid for rate type '
                  || UPPER (r.RATE_CALC_METHOD),
                  4720116,
                  RUOMstr || '<sep>' || UPPER (r.RATE_CALC_METHOD));
            END IF;
         -- else vRateUOM IS NULL
         ELSIF (   UPPER (r.RATE_CALC_METHOD) = 'RPD'
                OR UPPER (r.RATE_CALC_METHOD) = 'CWT'
                OR UPPER (r.RATE_CALC_METHOD) = 'RADL')
         THEN
            vFailure := 1;
            INSERT_RATING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRatingDtlSeq,
               'Rate UOM is Required when Rate Calculation Method is Rate/Unit (RPD) or Charge per Weight (CWT) or Radial Distance Rate (RADL).',
               4720117,
               NULL);
         END IF;

         --insert into TIME_IMPORT_LANE_DTL_KDM values ('val2','uom',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();
         IF (r.Currency_Code IS NULL)
         THEN
            vFailure := 1;
            INSERT_RATING_LANE_DTL_ERROR (vTCCompanyId,
                                          vLaneId,
                                          vRatingDtlSeq,
                                          'Currency Code is required',
                                          4720118,
                                          NULL);
         ELSE                                     -- vCurrencyCode IS NOT NULL
            --BEGIN
            SELECT COUNT (*)
              INTO vResult
              FROM CURRENCY
             WHERE CURRENCY_CODE = r.Currency_Code;

            -- EXCEPTION
            --  WHEN NO_DATA_FOUND THEN
            --    vResult := NULL;
            --END;
            IF (vResult = 0)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  'Currency Code (' || r.Currency_Code || ') is invalid',
                  4720119,
                  r.CURRENCY_CODE);
            END IF;
         END IF;

         --insert into TIME_IMPORT_LANE_DTL_KDM values ('val2','cur',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();
         IF (r.Tariff_Id IS NOT NULL)
         THEN
            -- verify that tariff exists in the database (TARIFF table)
            BEGIN
               SELECT 1
                 INTO vResult
                 FROM TARIFF_CODE
                WHERE TC_COMPANY_ID = vTCCompanyId
                      AND TARIFF_CODE = r.Tariff_Id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  vResult := NULL;
            END;

            IF (vResult IS NULL)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                     'Tariff Id ('
                  || r.Tariff_Id
                  || ') does not exist for your company',
                  4720120,
                  r.TARIFF_ID);
            ELSE
               UPDATE IMPORT_RATING_LANE_DTL_RATE
                  SET TARIFF_ID =
                         (SELECT TARIFF_CODE_ID
                            FROM TARIFF_CODE
                           WHERE TC_COMPANY_ID = vTCCompanyId
                                 AND TARIFF_CODE = r.Tariff_Id)
                WHERE     TC_COMPANY_ID = vTCCompanyId
                      AND LANE_ID = vLaneId
                      AND RATING_LANE_DTL_SEQ = vRatingDtlSeq
                      AND RLD_RATE_SEQ = r.RLD_RATE_SEQ;
            END IF;

            -- validate PARCEL_TIER
            IF (r.parcel_tier IS NOT NULL)
            THEN
               IF (r.parcel_tier < 1 OR r.parcel_tier > 8)
               THEN
                  vFailure := 1;
                  INSERT_RATING_LANE_DTL_ERROR (
                     vTCCompanyId,
                     vLaneId,
                     vRatingDtlSeq,
                     'Tier (' || r.parcel_tier
                     || ') is invalid. It should be a number between 1 and 8.',
                     4720121,
                     r.PARCEL_TIER);
               END IF;
            END IF;
         ELSE
            IF (r.RATE_CALC_METHOD = 'TRF')
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  'Tariff is Required when Rate Calculation Method is Tariff (TRF).',
                  4720122,
                  NULL);
            ELSE
               -- no need to report an error. Just clean the data.
               UPDATE IMPORT_RATING_LANE_DTL_RATE
                  SET PARCEL_TIER = NULL
                WHERE     TC_COMPANY_ID = vTCCompanyId
                      AND LANE_ID = vLaneId
                      AND RATING_LANE_DTL_SEQ = vRatingDtlSeq
                      AND RLD_RATE_SEQ = r.RLD_RATE_SEQ;
            END IF;
         END IF;

         --insert into TIME_IMPORT_LANE_DTL_KDM values ('val2','tar',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();
         IF (r.minimum_size IS NOT NULL AND r.maximum_size IS NOT NULL)
         THEN
            IF (r.minimum_size > r.maximum_size)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  'Minimum size is greater than the maximum size',
                  4720123,
                  NULL);
            END IF;

            IF (r.minimum_size = 0 AND r.maximum_size = 0)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  'Minimum and Maximum sizes cannot both be zero',
                  4720124,
                  NULL);
            END IF;
         END IF;

         --insert into TIME_IMPORT_LANE_DTL_KDM values ('val2','ms1',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();
         IF (r.minimum_size IS NOT NULL)
         THEN
            IF (r.maximum_size IS NULL)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  'Maximum size is required when Minimum size is given',
                  4720125,
                  NULL);
            END IF;

            IF (r.minimum_size < 0)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (vTCCompanyId,
                                             vLaneId,
                                             vRatingDtlSeq,
                                             'Minimum size is invalid',
                                             4720126,
                                             NULL);
            END IF;
         END IF;

         --insert into TIME_IMPORT_LANE_DTL_KDM values ('val2','ms2',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();
         IF (r.maximum_size IS NOT NULL)
         THEN
            IF (r.minimum_size IS NULL)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  'Minimum size is required when Maximum size is given',
                  4720127,
                  NULL);
            END IF;

            IF (r.maximum_size < 0)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (vTCCompanyId,
                                             vLaneId,
                                             vRatingDtlSeq,
                                             'Maximum size is invalid',
                                             4720128,
                                             NULL);
            END IF;
         END IF;

         --insert into TIME_IMPORT_LANE_DTL_KDM values ('val2','ms3',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();


         IF (r.SIZE_UOM_BU IS NULL)
         THEN
            nTcId := vRootCompanyId;
         ELSE
            nTcId :=
               Rating_Lane_Validation_Pkg.GET_BUID_FROM_BUSINESS_UNIT (
                  vRootCompanyId,
                  r.SIZE_UOM_BU);
         END IF;

         IF (R.SIZE_UOM IS NULL)
         THEN
            IF (r.minimum_size IS NOT NULL OR r.maximum_size IS NOT NULL)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  'Size UOM is required when sizes are given',
                  4720129,
                  NULL);
            END IF;
         ELSE
            vPassFail :=
               Rating_Sub_Validation_Pkg.VALIDATE_SIZE_UOM (nTcId,
                                                            r.SIZE_UOM);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  r.SIZE_UOM || ' is an invalid Size UOM',
                  4720130,
                  r.SIZE_UOM);
            ELSE
               SELECT SIZE_UOM_ID
                 INTO vSizeUomId
                 FROM SIZE_UOM
                WHERE TC_COMPANY_ID = nTcId AND SIZE_UOM = r.SIZE_UOM;

               UPDATE IMPORT_RATING_LANE_DTL_RATE
                  SET SIZE_UOM_ID = vSizeUomId
                WHERE     SIZE_UOM = r.SIZE_UOM
                      AND tc_company_id = vTCCompanyId
                      AND lane_id = vLaneId
                      AND RATING_LANE_DTL_SEQ = vRatingDtlSeq;

               COMMIT;
            END IF;
         END IF;

         --insert into TIME_IMPORT_LANE_DTL_KDM values ('val2','vsu',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();
         -- VALIDATE MIN/MAX DISTANCE AND DISTANCE UOM
         IF (r.Min_Distance IS NOT NULL AND r.Max_Distance IS NOT NULL)
         THEN
            IF (r.Min_Distance > r.Max_Distance)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  'Minimum Distance is greater then the maximum distance',
                  4720131,
                  NULL);
            END IF;

            IF (r.Min_Distance = 0 AND r.Max_Distance = 0)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  'Minimum and Maximum Distances cannot both be zero',
                  4720132,
                  NULL);
            END IF;
         END IF;

         --insert into TIME_IMPORT_LANE_DTL_KDM values ('val2','md1',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();
         IF (r.Min_Distance IS NOT NULL)
         THEN
            IF (r.Max_Distance IS NULL)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  'Maximum Distance is required when Minimum Distance is given',
                  4720133,
                  NULL);
            END IF;

            IF (r.Min_Distance < 0)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (vTCCompanyId,
                                             vLaneId,
                                             vRatingDtlSeq,
                                             'Minimum Distance is invalid',
                                             4720134,
                                             NULL);
            END IF;
         END IF;

         --insert into TIME_IMPORT_LANE_DTL_KDM values ('val2','md2',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();
         IF (r.Max_Distance IS NOT NULL)
         THEN
            IF (r.Min_Distance IS NULL)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  'Minimum Distance is required when Maximum Distance is given',
                  4720135,
                  NULL);
            END IF;

            IF (r.Max_Distance < 0)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (vTCCompanyId,
                                             vLaneId,
                                             vRatingDtlSeq,
                                             'Maximum Distance is invalid',
                                             4720136,
                                             NULL);
            END IF;
         END IF;

         --1875921743952379
         --insert into TIME_IMPORT_LANE_DTL_KDM values ('val2','md3',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();
         IF (r.DISTANCE_UOM IS NULL)
         THEN
            IF (r.Min_Distance IS NOT NULL OR r.Max_Distance IS NOT NULL)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  'Distance UOM is required when Minimum and/or Maximum distance entered',
                  4720137,
                  NULL);
            END IF;
         ELSE
            vPassFail :=
               Rating_Sub_Validation_Pkg.VALIDATE_DISTANCE_UOM (
                  r.DISTANCE_UOM);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  r.DISTANCE_UOM || ' is an invalid Distance UOM',
                  4720138,
                  r.DISTANCE_UOM);
            END IF;
         END IF;

         -- VALIDATE MIN/MAX COMMODITY
         IF (r.Min_Commodity IS NOT NULL)
         THEN
            -- Commodity Class Validation Moved to JAVA
            /* vPassFail := Rating_Sub_Validation_Pkg.VALIDATE_COMMODITY_CLASS(r.Min_Commodity);
             IF (vPassFail = 1) THEN
              vFailure := 1;
              INSERT_RATING_LANE_DTL_ERROR
              (
               vTCCompanyId,
               vLaneId,
               vRatingDtlSeq,
               r.Min_Commodity || ' is an invalid Commodity Class Code',
               4720139,
               r.MIN_COMMODITY
              );
             END IF;
            */
            IF (r.Max_Commodity
                   IS NULL)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  'Maximum Commodity Class Code is required when Minimum Commodity Class Code is Given. ',
                  4720140,
                  NULL);
            END IF;
         END IF;

         IF (r.Max_Commodity IS NOT NULL)
         THEN
            -- Commodity Class Validation Moved to JAVA
            /* vPassFail := Rating_Sub_Validation_Pkg.VALIDATE_COMMODITY_CLASS(r.Max_Commodity);
             IF (vPassFail = 1) THEN
              vFailure := 1;
              INSERT_RATING_LANE_DTL_ERROR
              (
               vTCCompanyId,
               vLaneId,
               vRatingDtlSeq,
               r.Max_Commodity || ' is an invalid Commodity Class Code',
               4720141,
               r.MAX_COMMODITY
              );
             END IF;
            */
            IF (r.Min_Commodity
                   IS NULL)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  'Minimum Commodity Class Code is required when Maximum Commodity Class Code is Given. ',
                  4720142,
                  NULL);
            END IF;
         END IF;

         IF (r.Min_Commodity IS NOT NULL AND r.Max_Commodity IS NOT NULL)
         THEN
            IF (r.Min_Commodity > r.Max_Commodity)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  'Minimum Commodity Class Code is greater then the maximum Commodity Class Code',
                  4720143,
                  NULL);
            END IF;
         END IF;

         -- CHECK FOR COMMODITY CODE RANGES IN CASE IF THE PARAMETER IS SET TRUE
         IF (vUseRangeForCommodityCode = 'true')
         THEN
            IF ( (r.COMMODITY_CODE_ID IS NOT NULL
                  AND r.MAX_RANGE_COMMODITY_CODE_ID IS NULL)
                OR (r.COMMODITY_CODE_ID IS NULL
                    AND r.MAX_RANGE_COMMODITY_CODE_ID IS NOT NULL))
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  'Use range for commodity codes parameter is set true. So enter both Commodity Code and Max Commodity Code values.',
                  3000031,
                  NULL);
            END IF;
         END IF;

         -- DATA IS VALID, CHECK FOR OVERLAPS
         IF (vFailure IS NULL OR vFailure <> 1)
         THEN
            vsqlStatement :=
                  'SELECT COUNT(*) FROM IMPORT_RATING_LANE_DTL_RATE irldr'
               || ' WHERE tc_company_id = :vTCCompanyId'     --|| vTCCompanyId
               || ' and lane_id = :vLaneId'                       --|| vLaneId
               || ' and rating_lane_dtl_seq = :vRatingDtlSeq' --|| vRatingDtlSeq
               || ' and rate_calc_method = :RateCalcMethod' --''' || r.Rate_Calc_Method || ''''
               || ' and supports_mstl = :supportsMSTL'    --|| r.Supports_MSTL
               || ' and has_bh = :hasBH'                         --|| r.Has_BH
               || ' and has_rt = :hasRT'                         --|| r.Has_RT
               || ' and ROWID != :vrowid'            --''' || r.RowId || '''';
                                         ;

            IF (r.minimum_size IS NULL OR r.maximum_size IS NULL)
            THEN
               vsqlStatement :=
                  vsqlStatement
                  || '  and minimum_size is null and maximum_size is null ';
            ELSE
               vsqlStatement :=
                     vsqlStatement
                  || ' and ( '
                  || '( '
                  || r.minimum_size
                  || ' >= minimum_size and  '
                  || r.minimum_size
                  || ' <= maximum_size) or  '
                  || '( '
                  || r.maximum_size
                  || ' >= minimum_size and  '
                  || r.maximum_size
                  || ' <= maximum_size) or  '
                  || '( minimum_size > '
                  || r.minimum_size
                  || ' and minimum_size < '
                  || r.maximum_size
                  || ' ) or  '
                  || '( maximum_size > '
                  || r.minimum_size
                  || ' and maximum_size < '
                  || r.maximum_size
                  || ' ) ) ';

               IF (r.SIZE_UOM IS NULL)
               THEN
                  vsqlStatement := vsqlStatement || ' and size_uom is null ';
               ELSE
                  vsqlStatement :=
                        vsqlStatement
                     || ' and size_uom = '''
                     || r.SIZE_UOM
                     || '''';
               END IF;
            END IF;

            IF (r.Min_Distance IS NULL OR r.Max_Distance IS NULL)
            THEN
               vsqlStatement :=
                  vsqlStatement
                  || '  and min_distance is null and max_distance is null ';
            ELSE
               vsqlStatement :=
                     vsqlStatement
                  || ' and ( '
                  || '( '
                  || r.Min_Distance
                  || ' >= min_distance and  '
                  || r.Min_Distance
                  || ' <= max_distance) or  '
                  || '( '
                  || r.Max_Distance
                  || ' >= min_distance and  '
                  || r.Max_Distance
                  || ' <= max_distance) or  '
                  || '( min_distance > '
                  || r.Min_Distance
                  || ' and min_distance < '
                  || r.Max_Distance
                  || ' ) or  '
                  || '( max_distance > '
                  || r.Min_Distance
                  || ' and max_distance < '
                  || r.Max_Distance
                  || ' ) ) ';

               IF (r.DISTANCE_UOM IS NULL)
               THEN
                  vsqlStatement :=
                     vsqlStatement || ' and distance_uom is null ';
               ELSE
                  vsqlStatement :=
                        vsqlStatement
                     || ' and distance_uom = '''
                     || r.DISTANCE_UOM
                     || '''';
               END IF;
            END IF;

            IF (   r.RATE_CALC_METHOD = 'RPD'
                OR r.RATE_CALC_METHOD = 'CWT'
                OR r.RATE_CALC_METHOD = 'WTM'
                OR r.RATE_CALC_METHOD = 'OVW'
                OR r.RATE_CALC_METHOD = 'TON'
                OR r.RATE_CALC_METHOD = 'RADL')
            THEN
               IF (r.Rate_UOM IS NULL)
               THEN
                  vsqlStatement := vsqlStatement || ' and rate_uom is null ';
               ELSE
                  vsqlStatement :=
                        vsqlStatement
                     || ' and rate_uom = '''
                     || r.Rate_UOM
                     || '''';
               END IF;
            END IF;

            IF (r.RATE_CALC_METHOD = 'TRF')
            THEN
               IF (r.Tariff_Id IS NULL)
               THEN
                  vsqlStatement := vsqlStatement || ' and tariff_id is null ';
               ELSE
                  vsqlStatement :=
                        vsqlStatement
                     || ' and tariff_id = '''
                     || r.Tariff_Id
                     || '''';
               END IF;
            END IF;

            IF (r.Min_Commodity IS NULL OR r.Max_Commodity IS NULL)
            THEN
               vsqlStatement :=
                  vsqlStatement
                  || '  and min_commodity is null and max_commodity is null ';
            ELSE
               vsqlStatement :=
                     vsqlStatement
                  || ' and ( '
                  || '( '
                  || r.Min_Commodity
                  || ' >= min_commodity and  '
                  || r.Min_Commodity
                  || ' <= max_commodity) or  '
                  || '( '
                  || r.Max_Commodity
                  || ' >= min_commodity and  '
                  || r.Max_Commodity
                  || ' <= max_commodity) or  '
                  || '( min_commodity > '
                  || r.Min_Commodity
                  || ' and min_commodity < '
                  || r.Max_Commodity
                  || ' ) or  '
                  || '( max_commodity > '
                  || r.Min_Commodity
                  || ' and max_commodity < '
                  || r.Max_Commodity
                  || ' ) ) ';
            END IF;

            --// Check for commodity codes, based on use_range_for_commodity_codes company parameter
            IF (vUseRangeForCommodityCode = 'true')
            THEN
               IF (r.COMMODITY_CODE_ID IS NOT NULL
                   AND r.MAX_RANGE_COMMODITY_CODE_ID IS NOT NULL)
               THEN
                  vsqlStatement :=
                     vsqlStatement
                     || ' and ((select description_short from commodity_code cc '
                     || ' where cc.commodity_code_id = irldr.commodity_code_id)  <= '''
                     || vMaxRangeCommodityCode
                     || ''') and ((select description_short from commodity_code cc where cc.commodity_code_id = irldr.max_range_commodity_code_id) >= '''
                     || vCommodityCode
                     || ''') ';
               ELSE
                  vsqlStatement :=
                     vsqlStatement
                     || ' and commodity_code_id is null and max_range_commodity_code_id is null ';
               END IF;
            ELSE
               IF (r.COMMODITY_CODE_ID IS NOT NULL)
               THEN
                  vsqlStatement :=
                        vsqlStatement
                     || ' and commodity_code_id = '
                     || r.COMMODITY_CODE_ID;
               ELSE
                  vsqlStatement :=
                     vsqlStatement
                     || ' and commodity_code_id is null and max_range_commodity_code_id is null ';
               END IF;
            END IF;

            EXECUTE IMMEDIATE vsqlStatement
               INTO vCount
               USING vTCCompanyId,
                     vLaneId,
                     vRatingDtlSeq,
                     r.RATE_CALC_METHOD,
                     r.Supports_MSTL,
                     r.Has_BH,
                     r.Has_RT,
                     r.ROWID;

            IF (vCount > 0)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  'Duplicate Rate found with the same rate information. Processing rate with sequence : '
                  || r.RLD_RATE_SEQ,
                  4720144,
                  r.RLD_RATE_SEQ);
            END IF;
         END IF;
      END LOOP;

      RETURN vFailure;
   END VALIDATE_LANE_DTL_RATE;

   FUNCTION VALIDATE_LANE_DTL_ACCESSORIAL (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vLaneId         IN rating_lane.LANE_ID%TYPE,
      vRatingDtlSeq   IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE)
      RETURN NUMBER
   IS
      vPassFail              NUMBER;
      vCount                 NUMBER;
      vAccessorialCode       ACCESSORIAL_CODE.ACCESSORIAL_CODE%TYPE;
      vAccessorialId         ACCESSORIAL_CODE.ACCESSORIAL_ID%TYPE;
      vAccessorialBU         IMPORT_LANE_ACCESSORIAL.ACCESSORIAL_BU%TYPE;
      vEffectiveDt           IMPORT_LANE_ACCESSORIAL.EFFECTIVE_DT%TYPE;
      vExpirationDt          IMPORT_LANE_ACCESSORIAL.EXPIRATION_DT%TYPE;
      vPayeeCarrierCode      IMPORT_LANE_ACCESSORIAL.PAYEE_CARRIER_CODE%TYPE;
      vPayeeCarrierId        IMPORT_LANE_ACCESSORIAL.PAYEE_CARRIER_ID%TYPE;
      vPayeeCarrierBU        IMPORT_LANE_ACCESSORIAL.PAYEE_CARRIER_BU%TYPE;
      -- MACR00159593 starts
      vMot                   IMPORT_LANE_ACCESSORIAL.MOT%TYPE;
      vEquipment             IMPORT_LANE_ACCESSORIAL.EQUIPMENT_CODE%TYPE;
      vServiceLevel          IMPORT_LANE_ACCESSORIAL.SERVICE_LEVEL%TYPE;
      vProtectionLevel       IMPORT_LANE_ACCESSORIAL.PROTECTION_LEVEL%TYPE;
      vMotId                 IMPORT_LANE_ACCESSORIAL.MOT_ID%TYPE;
      vEquipmentId           IMPORT_LANE_ACCESSORIAL.EQUIPMENT_ID%TYPE;
      vServiceLevelId        IMPORT_LANE_ACCESSORIAL.SERVICE_LEVEL_ID%TYPE;
      vProtectionLevelId     IMPORT_LANE_ACCESSORIAL.PROTECTION_LEVEL_ID%TYPE;
      -- MACR00159593 ends
      vFailure               NUMBER;
      vRowId                 ROWID;
      vRootCompanyId         NUMBER;
      vAccessorialBUID       NUMBER;
      vPayeeCarrierBUID      NUMBER;
      vuomstr                import_lane_accessorial.size_uom%TYPE;
      vuomid                 import_lane_accessorial.size_uom_id%TYPE;
      vcheckdupaccsql        VARCHAR2 (2000);
      vminqty                import_lane_accessorial.minimum_size%TYPE;
      vmaxqty                import_lane_accessorial.maximum_size%TYPE;
      vacctype               accessorial_code.accessorial_type%TYPE;

      -- MACR00159593 starts
      vModeBU                IMPORT_LANE_ACCESSORIAL.MOT_BU%TYPE;
      vServiceLevelBU        IMPORT_LANE_ACCESSORIAL.SERVICE_LEVEL_BU%TYPE;
      vEquipmentBU           IMPORT_LANE_ACCESSORIAL.EQUIPMENT_BU%TYPE;
      vProtectionLevelBU     IMPORT_LANE_ACCESSORIAL.PROTECTION_LEVEL_BU%TYPE;
      vModeBUID              COMPANY.COMPANY_ID%TYPE;
      vEquipmentBUID         COMPANY.COMPANY_ID%TYPE;
      vServiceLevelBUID      COMPANY.COMPANY_ID%TYPE;
      vProtectionLevelBUID   COMPANY.COMPANY_ID%TYPE;
      vZoneCode              IMPORT_LANE_ACCESSORIAL.Zone_Code%TYPE;
      vZoneId                IMPORT_LANE_ACCESSORIAL.ZONE_ID%TYPE;
      vBillingMethod         IMPORT_LANE_ACCESSORIAL.Billing_Method%TYPE;
      vBillingMethodCode     IMPORT_LANE_ACCESSORIAL.Billing_Method_Code%TYPE;
      vPackagetype           IMPORT_LANE_ACCESSORIAL.Package_Type%TYPE;
      vPackageTypeId         IMPORT_LANE_ACCESSORIAL.Package_Type_Id%TYPE;
      vPackageBU             IMPORT_LANE_ACCESSORIAL.Package_Type_BU%TYPE;
      vPackageBUID           COMPANY.COMPANY_ID%TYPE;
      vBMCount               NUMBER;
      vPCount                NUMBER;
      vMinLongestDim         LANE_ACCESSORIAL.Min_Longest_Dimension%TYPE;
      vMaxLongestDim         LANE_ACCESSORIAL.Max_Longest_Dimension%TYPE;
      vMinSecondLongestDim   LANE_ACCESSORIAL.Min_Second_Longest_Dimension%TYPE;
      vMaxSecondLongestDim   LANE_ACCESSORIAL.Max_Second_Longest_Dimension%TYPE;
      vMinThirdLongestDim    LANE_ACCESSORIAL.Min_Third_Longest_Dimension%TYPE;
      vMaxThirdLongestDim    LANE_ACCESSORIAL.Max_Third_Longest_Dimension%TYPE;
      vMinLengthGrith        LANE_ACCESSORIAL.Min_Length_Plus_Grith%TYPE;
      vMaxLengthGrith        LANE_ACCESSORIAL.Max_Length_Plus_Grith%TYPE;
      vMinWeight             LANE_ACCESSORIAL.Min_Weight%TYPE;
      vMaxWeight             LANE_ACCESSORIAL.Max_Weight%TYPE;
      vMinRate               LANE_ACCESSORIAL.MINIMUM_RATE%Type;
      -- MACR00159593 ends
      ISFUELACC              ACCESSORIAL_CODE.IS_FUEL_ACCESSORIAL%TYPE;
	  vImpLaneAccId         LANE_ACCESSORIAL.LANE_ACCESSORIAL_ID%TYPE;
		vCommId               LANE_ACCESSORIAL.COMMODITY_CODE_ID%TYPE;
		vCommMaxId            LANE_ACCESSORIAL.MAX_RANGE_COMMODITY_CODE_ID%TYPE;
		vCommodityCode              VARCHAR2 (16);
		vMaxRangeCommodityCode      VARCHAR2 (16);
		vUseRangeForCommodityCode   VARCHAR2 (5);
		paramValueExists            NUMBER;
		vExternalCarrCount          NUMBER;
      CURSOR LANE_ACCESS_CURSOR
      IS
         SELECT LANE_ACC_ID,
                ACCESSORIAL_CODE,
                ACCESSORIAL_BU,
                EFFECTIVE_DT,
                EXPIRATION_DT,
                PAYEE_CARRIER_CODE,
                PAYEE_CARRIER_BU,
                ROWID,
                MINIMUM_SIZE,
                MAXIMUM_SIZE,
                SIZE_UOM,
                SIZE_UOM_ID,
                MOT,
                MOT_BU,
                EQUIPMENT_CODE,
                EQUIPMENT_BU,
                SERVICE_LEVEL,
                SERVICE_LEVEL_BU,
                PROTECTION_LEVEL,
                PROTECTION_LEVEL_BU,
                ZONE_CODE,
                PACKAGE_TYPE,
                BILLING_METHOD_CODE,
                MIN_LONGEST_DIMENSION,
                MAX_LONGEST_DIMENSION,
                MIN_SECOND_LONGEST_DIMENSION,
                MAX_SECOND_LONGEST_DIMENSION,
                MIN_THIRD_LONGEST_DIMENSION,
                MAX_THIRD_LONGEST_DIMENSION,
                MIN_LENGTH_PLUS_GRITH,
                MAX_LENGTH_PLUS_GRITH,
                MIN_WEIGHT,
                MAX_WEIGHT,
				MINIMUM_RATE,
				COMMODITY_CODE_ID,
				MAX_RANGE_COMMODITY_CODE_ID
           FROM IMPORT_LANE_ACCESSORIAL
          WHERE     TC_COMPANY_ID = vTCCompanyId
                AND LANE_ID = vLaneId
                AND RATING_LANE_DTL_SEQ = vRatingDtlSeq;       -- MACR00159593

      c_ref                  ref_curtype;
   BEGIN
      vFailure := 0;

      OPEN LANE_ACCESS_CURSOR;

     <<LANE_ACCESS_LOOP>>
      LOOP
         vPassFail := 0;

         FETCH LANE_ACCESS_CURSOR
         INTO vImpLaneAccId,
              vAccessorialCode,
              vAccessorialBU,
              vEffectiveDt,
              vExpirationDt,
              vPayeeCarrierCode,
              vPayeeCarrierBU,
              vRowId,
              vminqty,
              vmaxqty,
              vuomstr,
              vuomid,
              vMot,
              vModeBU,
              vEquipment,
              vEquipmentBU,
              vServiceLevel,
              vServiceLevelBU,
              vProtectionLevel,
              vProtectionLevelBU,
              vZoneCode,
              vPackagetype,
              vBillingMethodCode,
              vMinLongestDim,
              vMaxLongestDim,
              vMinSecondLongestDim,
              vMaxSecondLongestDim,
              vMinThirdLongestDim,
              vMaxThirdLongestDim,
              vMinLengthGrith,
              vMaxLengthGrith,
              vMinWeight,
              vMaxWeight,
			  vMinRate,
			  vCommId,
			  vCommMaxId;

         EXIT LANE_ACCESS_LOOP WHEN LANE_ACCESS_CURSOR%NOTFOUND; -- MACR00159593

         SELECT ROOTCOMPANYID
           INTO vRootCompanyId
           FROM IMPORT_RATING_LANE
          WHERE LANE_ID = vLaneId;
		  
		  SELECT COUNT (PARAM_VALUE)
           INTO paramValueExists
           FROM COMPANY_PARAMETER
          WHERE TC_COMPANY_ID = vTCCompanyId
                AND PARAM_DEF_ID = 'use_range_for_commodity_codes';

         IF (paramValueExists > 0)
         THEN
            SELECT PARAM_VALUE
              INTO vUseRangeForCommodityCode
              FROM COMPANY_PARAMETER
             WHERE TC_COMPANY_ID = vTCCompanyId
                   AND PARAM_DEF_ID = 'use_range_for_commodity_codes';
         ELSE
            SELECT DFLT_VALUE
              INTO vUseRangeForCommodityCode
              FROM PARAM_DEF
             WHERE PARAM_DEF_ID = 'use_range_for_commodity_codes';
         END IF;
          
         
         IF (vUseRangeForCommodityCode = 'true')
         THEN
            IF ( (vCommId IS NOT NULL
                  AND vCommMaxId IS NULL)
                OR (vCommId IS NULL
                    AND vCommMaxId IS NOT NULL))
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  'Use range for commodity codes parameter is set true. So enter both Commodity Code and Max Commodity Code values.',
                  3000031,
                  NULL);
            END IF;
         END IF;

         IF (vAccessorialBU IS NULL)
         THEN
            vAccessorialBUID := vRootCompanyId;
         ELSE
            vAccessorialBUID :=
               Rating_Lane_Validation_Pkg.GET_BUID_FROM_BUSINESS_UNIT (
                  vRootCompanyId,
                  vAccessorialBU);
         END IF;
         vPassFail :=
            Rating_Sub_Validation_Pkg.VALIDATE_ACCESSORIAL_CODE (
               vAccessorialBUID,
               vAccessorialCode);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RATING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRatingDtlSeq,
               vAccessorialCode || ' is not a valid Accessorial Code',
               4720145,
               vAccessorialCode);
         ELSE
            SELECT accessorial_id
              INTO vAccessorialId
              FROM ACCESSORIAL_CODE
             WHERE ACCESSORIAL_CODE = vAccessorialCode
                   AND tc_company_id = vAccessorialBUID;

            UPDATE IMPORT_LANE_ACCESSORIAL
               SET accessorial_id = vAccessorialId
             WHERE     ACCESSORIAL_CODE = vAccessorialCode
                   AND tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND RATING_LANE_DTL_SEQ = vRatingDtlSeq;

            IF (vAccessorialId IS NOT NULL)
            THEN
               SELECT AC.IS_FUEL_ACCESSORIAL
                 INTO ISFUELACC
                 FROM ACCESSORIAL_CODE AC
                WHERE AC.ACCESSORIAL_ID = vAccessorialId;

               IF (ISFUELACC = 1)
               THEN
                  UPDATE IMPORT_LANE_ACCESSORIAL
                     SET RATE = 0.0
                   WHERE     ACCESSORIAL_CODE = vAccessorialCode
                         AND tc_company_id = vTCCompanyId
                         AND lane_id = vLaneId
                         AND RATING_LANE_DTL_SEQ = vRatingDtlSeq;
               END IF;
            END IF;

            COMMIT;
         END IF;

         IF (vEffectiveDt IS NULL)
         THEN
            vFailure := 1;
            INSERT_RATING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRatingDtlSeq,
               'Lane Accessorial Effective Date is required or value given is invalid',
               4720146,
               NULL);
         END IF;

         IF (vExpirationDt IS NULL)
         THEN
            vFailure := 1;
            INSERT_RATING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRatingDtlSeq,
               'Lane Accessorial Expiration Date is required or value given is invalid',
               4720147,
               NULL);
         END IF;

         IF (    vEffectiveDt IS NOT NULL
             AND vExpirationDt IS NOT NULL
             AND vEffectiveDt > vExpirationDt)
         THEN
            vFailure := 1;
            INSERT_RATING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRatingDtlSeq,
               'Expiration Date must be greater than Effective Date',
               4720148,
               NULL);
         END IF;


         IF (    vminqty IS NOT NULL
             AND vmaxqty IS NOT NULL
             AND vminqty > vmaxqty)
         THEN
            vfailure := 1;
            insert_rating_lane_dtl_error (
               vtccompanyid,
               vlaneid,
               vratingdtlseq,
               'Accessorial Minimum Size is greater than Maximum Size',
               3000018,
               NULL);
         END IF;

         IF (vuomstr IS NOT NULL AND vuomid IS NULL)
         THEN
            IF (vuomstr != 'STOPOFF')
            THEN
               vfailure := 1;
               insert_rating_lane_dtl_error (
                  vtccompanyid,
                  vlaneid,
                  vratingdtlseq,
                     'Invalid size UOM ('
                  || vuomstr
                  || ') for Accessorial Code ('
                  || vaccessorialcode
                  || ')',
                  4000031,
                  NULL);
            END IF;
         END IF;

         IF (vaccessorialid IS NOT NULL AND vuomstr IS NOT NULL)
         THEN
            SELECT accessorial_type
              INTO vacctype
              FROM accessorial_code
             WHERE accessorial_id = vaccessorialid;

            IF (vacctype = 'FC' AND vminqty IS NULL)
            THEN
               vfailure := 1;
               insert_rating_lane_dtl_error (
                  vtccompanyid,
                  vlaneid,
                  vratingdtlseq,
                  'Minimum Quantity is a required Field.',
                  9998916,
                  NULL);
            END IF;

            IF (vacctype = 'FC' AND vmaxqty IS NULL)
            THEN
               vfailure := 1;
               insert_rating_lane_dtl_error (
                  vtccompanyid,
                  vlaneid,
                  vratingdtlseq,
                  'Maximum Quantity is a required Field.',
                  9998916,
                  NULL);
            END IF;
         END IF;
--For validating rating lane when accType is not FC and there is no minimum rate
	 IF (vaccessorialid IS NOT NULL)
	  THEN
	   SELECT accessorial_type
              INTO vacctype
              FROM accessorial_code
             WHERE accessorial_id = vaccessorialid;

         IF (vacctype != 'FC' AND vMinRate IS NULL) THEN

             vfailure := 1;
               insert_rating_lane_dtl_error (
                  vtccompanyid,
                  vlaneid,
                  vratingdtlseq,
                  'Minimum Rate is a required Field.',
                  2820001,
                  'Minimum Rate');
           END IF;
	END IF;
--Ends
         IF (vPayeeCarrierBU IS NULL)
         THEN
            vPayeeCarrierBUID := vRootCompanyId;
         ELSE
            vPayeeCarrierBUID :=
               Rating_Lane_Validation_Pkg.GET_BUID_FROM_BUSINESS_UNIT (
                  vRootCompanyId,
                  vPayeeCarrierBU);
         END IF;

         vPayeeCarrierId := NULL;

         IF (vPayeeCarrierCode IS NOT NULL)
         THEN
            vPassFail :=
               Rating_Sub_Validation_Pkg.VALIDATE_CARRIER_CODE (
                  vPayeeCarrierBUID,
                  vPayeeCarrierCode);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  vPayeeCarrierCode || ' is an invalid Payee Carrier Code',
                  4720154,
                  vPayeeCarrierCode);
            ELSE
               vPassFail :=
                  Rating_Sub_Validation_Pkg.VALIDATE_CARRIER_STATUS (
                     vTCCompanyId,
                     vPayeeCarrierCode);

               IF (vPassFail = 1)
               THEN
                  vFailure := 1;
                  INSERT_RATING_LANE_DTL_ERROR (
                     vTCCompanyId,
                     vLaneId,
                     vRatingDtlSeq,
                     vPayeeCarrierCode
                     || ' is an inactive Payee Carrier Code',
                     4720155,
                     vPayeeCarrierCode);
               ELSE
                  SELECT carrier_id
                    INTO vPayeeCarrierId
                    FROM CARRIER_CODE
                   WHERE CARRIER_CODE = vPayeeCarrierCode
                         AND tc_company_id = vPayeeCarrierBUID
						 AND MARK_FOR_DELETION = 0;

                  UPDATE IMPORT_LANE_ACCESSORIAL
                     SET payee_carrier_id = vPayeeCarrierId
                   WHERE     payee_carrier_code = vPayeeCarrierCode
                         AND tc_company_id = vTCCompanyId
                         AND lane_id = vLaneId
                         AND RATING_LANE_DTL_SEQ = vRatingDtlSeq;
						 
				  SELECT COUNT(CARRIER_ID) INTO vExternalCarrCount FROM CARRIER_CODE WHERE CARRIER_ID = vPayeeCarrierId AND carrier_type_id = 24;
				  IF (vExternalCarrCount > 0)
				  THEN
						vFailure := 1;
						INSERT_RATING_LANE_DTL_ERROR (
							vTCCompanyId,
							vLaneId,
							vRatingDtlSeq,
							'External Parcel Carrier '||vPayeeCarrierCode||' cannot be used as a Payee Carrier',
							4720203,
							vPayeeCarrierCode);
				   END IF;

                  COMMIT;
               END IF;
            END IF;
         END IF;

         -- MACR00159593 - added for Mode,Eq, SL and PL starts

         IF (vModeBU IS NULL)
         THEN
            vModeBUID := vRootCompanyId;
         ELSE
            vModeBUID :=
               Rating_Lane_Validation_Pkg.GET_BUID_FROM_BUSINESS_UNIT (
                  vRootCompanyId,
                  vModeBU);
         END IF;

         IF (vServiceLevelBU IS NULL)
         THEN
            vServiceLevelBUID := vRootCompanyId;
         ELSE
            vServiceLevelBUID :=
               Rating_Lane_Validation_Pkg.GET_BUID_FROM_BUSINESS_UNIT (
                  vRootCompanyId,
                  vServiceLevelBU);
         END IF;

         IF (vEquipmentBU IS NULL)
         THEN
            vEquipmentBUID := vRootCompanyId;
         ELSE
            vEquipmentBUID :=
               Rating_Lane_Validation_Pkg.GET_BUID_FROM_BUSINESS_UNIT (
                  vRootCompanyId,
                  vEquipmentBU);
         END IF;

         IF (vProtectionLevelBU IS NULL)
         THEN
            vProtectionLevelBUID := vRootCompanyId;
         ELSE
            vProtectionLevelBUID :=
               Rating_Lane_Validation_Pkg.GET_BUID_FROM_BUSINESS_UNIT (
                  vRootCompanyId,
                  vProtectionLevelBU);
         END IF;

         IF (vPackageBU IS NULL)
         THEN
            vPackageBUID := vRootCompanyId;
         ELSE
            vPackageBUID :=
               Rating_Lane_Validation_Pkg.GET_BUID_FROM_BUSINESS_UNIT (
                  vRootCompanyId,
                  vPackageBU);
         END IF;

         IF (vMot IS NOT NULL)
         THEN
            vPassFail := 0;
            vPassFail := CHECK_IS_BU_VALID (vTCCompanyId, vModeBUID);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  vModeBU || ' is an invalid business unit',
                  4720100,
                  vModeBU);
            ELSE
               vPassFail :=
                  Rating_Sub_Validation_Pkg.VALIDATE_MODE (vModeBUID, vMot);

               IF (vPassFail = 1)
               THEN
                  vFailure := 1;
                  INSERT_RATING_LANE_DTL_ERROR (
                     vTCCompanyId,
                     vLaneId,
                     vRatingDtlSeq,
                     vMot || ' is an invalid Mode',
                     4720157,
                     vMot);
               ELSE
                  SELECT mot_id
                    INTO vMotId
                    FROM MOT
                   WHERE     MOT = vMot
                         AND MARK_FOR_DELETION = 0
                         AND tc_company_id = vModeBUID;

                  UPDATE IMPORT_LANE_ACCESSORIAL
                     SET mot_id = vMotId
                   WHERE     MOT = vMot
                         AND tc_company_id = vTCCompanyId
                         AND lane_id = vLaneId
                         AND RATING_LANE_DTL_SEQ = vRatingDtlSeq;
               END IF;
            END IF;
         END IF;

         IF (vServiceLevel IS NOT NULL)
         THEN
            vPassFail := 0;
            vPassFail := CHECK_IS_BU_VALID (vTCCompanyId, vServiceLevelBUID);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  vServiceLevelBU || ' is an invalid business unit',
                  4720100,
                  vServiceLevelBU);
            ELSE
               vPassFail :=
                  Rating_Sub_Validation_Pkg.VALIDATE_SERVICE_LEVEL (
                     vServiceLevelBUID,
                     vServiceLevel);

               IF (vPassFail = 1)
               THEN
                  vFailure := 1;
                  INSERT_RATING_LANE_DTL_ERROR (
                     vTCCompanyId,
                     vLaneId,
                     vRatingDtlSeq,
                     vServiceLevel || ' is an invalid Service Level',
                     4720158,
                     vServiceLevel);
               ELSE
                  SELECT service_level_id
                    INTO vServiceLevelId
                    FROM SERVICE_LEVEL
                   WHERE     SERVICE_LEVEL = vServiceLevel
                         AND MARK_FOR_DELETION = 0
                         AND tc_company_id = vServiceLevelBUID;

                  UPDATE IMPORT_LANE_ACCESSORIAL
                     SET service_level_id = vServiceLevelId
                   WHERE     SERVICE_LEVEL = vServiceLevel
                         AND tc_company_id = vTCCompanyId
                         AND lane_id = vLaneId
                         AND RATING_LANE_DTL_SEQ = vRatingDtlSeq;
               END IF;
            END IF;
         END IF;


         IF (vEquipment IS NOT NULL)
         THEN
            vPassFail := 0;
            vPassFail := CHECK_IS_BU_VALID (vTCCompanyId, vEquipmentBUID);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  vEquipmentBU || ' is an invalid business unit',
                  4720100,
                  vEquipmentBU);
            ELSE
               vPassFail :=
                  Rating_Sub_Validation_Pkg.VALIDATE_EQUIPMENT (
                     vEquipmentBUID,
                     vEquipment);

               IF (vPassFail = 1)
               THEN
                  vFailure := 1;
                  INSERT_RATING_LANE_DTL_ERROR (
                     vTCCompanyId,
                     vLaneId,
                     vRatingDtlSeq,
                     vEquipment || ' is an invalid Equipment Code',
                     4720159,
                     vEquipment);
               ELSE
                  SELECT equipment_id
                    INTO vEquipmentId
                    FROM EQUIPMENT
                   WHERE     equipment_code = vEquipment
                         AND MARK_FOR_DELETION = 0
                         AND tc_company_id = vEquipmentBUID;

                  UPDATE IMPORT_LANE_ACCESSORIAL
                     SET equipment_id = vEquipmentId
                   WHERE     equipment_code = vEquipment
                         AND tc_company_id = vTCCompanyId
                         AND lane_id = vLaneId
                         AND RATING_LANE_DTL_SEQ = vRatingDtlSeq;
               END IF;
            END IF;
         END IF;

         IF (vProtectionLevel IS NOT NULL)
         THEN
            vPassFail := 0;
            vPassFail :=
               CHECK_IS_BU_VALID (vTCCompanyId, vProtectionLevelBUID);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  vProtectionLevelBU || ' is an invalid business unit',
                  4720100,
                  vProtectionLevelBU);
            ELSE
               vPassFail :=
                  Rating_Sub_Validation_Pkg.VALIDATE_PROTECTION_LEVEL (
                     vProtectionLevelBUID,
                     vProtectionLevel);

               IF (vPassFail = 1)
               THEN
                  vFailure := 1;
                  INSERT_RATING_LANE_DTL_ERROR (
                     vTCCompanyId,
                     vLaneId,
                     vRatingDtlSeq,
                     vProtectionLevel || ' is an invalid Protection Level',
                     4720160,
                     vProtectionLevel);
               ELSE
                  SELECT protection_level_id
                    INTO vProtectionLevelId
                    FROM PROTECTION_LEVEL
                   WHERE     PROTECTION_LEVEL = vProtectionLevel
                         AND MARK_FOR_DELETION = 0
                         AND tc_company_id = vProtectionLevelBUID;

                  UPDATE IMPORT_LANE_ACCESSORIAL
                     SET protection_level_id = vProtectionLevelId
                   WHERE     PROTECTION_LEVEL = vProtectionLevel
                         AND tc_company_id = vTCCompanyId
                         AND lane_id = vLaneId
                         AND RATING_LANE_DTL_SEQ = vRatingDtlSeq;
               END IF;
            END IF;
         END IF;

         IF (vZoneCode IS NOT NULL)
         THEN
            vPassFail := 0;

            SELECT COUNT (Zone_Id)
              INTO vCount
              FROM ZONE
             WHERE ZONE_NAME = vZoneCode AND tc_company_id = vTCCompanyId;

            IF (vCount = 0)
            THEN
               vPassFail := 1;
            END IF;

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  vZoneCode || ' is an invalid zone id',
                  4720184,
                  vZoneCode);
            ELSE
               SELECT Zone_Id
                 INTO vZoneId
                 FROM ZONE
                WHERE ZONE_NAME = vZoneCode AND tc_company_id = vTCCompanyId;

               UPDATE IMPORT_LANE_ACCESSORIAL
                  SET Zone_Id = vZoneId
                WHERE     ZONE_CODE = vZoneCode
                      AND tc_company_id = vTCCompanyId
                      AND lane_id = vLaneId
                      AND RATING_LANE_DTL_SEQ = vRatingDtlSeq;
            END IF;
         END IF;

         IF (vBillingMethodCode IS NOT NULL)
         THEN
            vPassFail := 0;

            SELECT COUNT (1)
              INTO vBMCount
              FROM BILLING_METHOD
             WHERE DESCRIPTION = vBillingMethodCode;

            IF (vBMCount = 0)
            THEN
               vPassFail := 1;
            ELSE
               SELECT BILLING_METHOD
                 INTO vBillingMethod
                 FROM BILLING_METHOD
                WHERE DESCRIPTION = vBillingMethodCode;

               UPDATE IMPORT_LANE_ACCESSORIAL
                  SET BILLING_METHOD = vBillingMethod
                WHERE     TC_COMPANY_ID = vTCCompanyId
                      AND LANE_ID = vLaneId
                      AND RATING_LANE_DTL_SEQ = vRatingDtlSeq;
            END IF;

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  vBillingMethodCode || ' is an invalid Billing Method Code',
                  4720183,
                  vBillingMethodCode);
            END IF;
         END IF;

         IF (vPackageType IS NOT NULL)
         THEN
            vPassFail := 0;
            vPassFail := CHECK_IS_BU_VALID (vTCCompanyId, vPackageBUID);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  vPackageBU || ' is an invalid business unit',
                  4720100,
                  vPackageBU);
            ELSE
               SELECT COUNT (1)
                 INTO vPCount
                 FROM PACKAGE_TYPE
                WHERE DESCRIPTION = vPackagetype;

               IF (vPCount = 0)
               THEN
                  vPassFail := 1;
               END IF;

               IF (vPassFail = 1)
               THEN
                  vFailure := 1;
                  INSERT_RATING_LANE_DTL_ERROR (
                     vTCCompanyId,
                     vLaneId,
                     vRatingDtlSeq,
                     vPackageType || ' is an invalid Package Type',
                     4720187,
                     vPackageType);
               ELSE
                  SELECT package_type_id
                    INTO vPackageTypeId
                    FROM PACKAGE_TYPE
                   WHERE     DESCRIPTION = vPackagetype
                         AND MARK_FOR_DELETION = 0
                         AND tc_company_id = vPackageBUID;

                  UPDATE IMPORT_LANE_ACCESSORIAL
                     SET package_type_id = vPackageTypeId
                   WHERE     PACKAGE_TYPE = vPackagetype
                         AND tc_company_id = vTCCompanyId
                         AND lane_id = vLaneId
                         AND RATING_LANE_DTL_SEQ = vRatingDtlSeq;
               END IF;
            END IF;
         END IF;

         IF (vMinLongestDim IS NOT NULL OR vMaxLongestDim IS NOT NULL)
         THEN
            vPassFail :=
               VALIDATE_DIMENSION (vTCCompanyId,
                                   vLaneId,
                                   vRatingDtlSeq,
                                   vMinLongestDim,
                                   vMaxLongestDim,
                                   'Longest Dimension');

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               DBMS_OUTPUT.PUT_LINE ('VALIDATE_DIMENSION failed');
            END IF;
         END IF;

         IF (vMinSecondLongestDim IS NOT NULL
             OR vMaxSecondLongestDim IS NOT NULL)
         THEN
            vPassFail :=
               VALIDATE_DIMENSION (vTCCompanyId,
                                   vLaneId,
                                   vRatingDtlSeq,
                                   vMinSecondLongestDim,
                                   vMaxSecondLongestDim,
                                   'Second Longest Dimension');

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               DBMS_OUTPUT.PUT_LINE ('VALIDATE_DIMENSION failed');
            END IF;
         END IF;

         IF (vMinThirdLongestDim IS NOT NULL
             OR vMaxThirdLongestDim IS NOT NULL)
         THEN
            vPassFail :=
               VALIDATE_DIMENSION (vTCCompanyId,
                                   vLaneId,
                                   vRatingDtlSeq,
                                   vMinThirdLongestDim,
                                   vMaxThirdLongestDim,
                                   'Third Longest Dimension');

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               DBMS_OUTPUT.PUT_LINE ('VALIDATE_DIMENSION failed');
            END IF;
         END IF;

         IF (vMinLongestDim IS NOT NULL AND vMaxSecondLongestDim IS NOT NULL)
         THEN
            vPassFail := 0;

            IF (vMaxSecondLongestDim >= vMinLongestDim)
            THEN
               vPassFail := 1;

               IF (vPassFail = 1)
               THEN
                  vFailure := 1;
                  INSERT_RATING_LANE_DTL_ERROR (
                     vTCCompanyId,
                     vLaneId,
                     vRatingDtlSeq,
                        'Max. Second Longest Dimension'
                     || ' should be less than '
                     || 'Min. Longest Dimension',
                     4720190,
                     'Max. Second Longest Dimension'
                     || 'Min. Longest Dimension');
               END IF;
            END IF;
         END IF;

         IF (vMinSecondLongestDim IS NOT NULL
             AND vMaxThirdLongestDim IS NOT NULL)
         THEN
            vPassFail := 0;

            IF (vMaxThirdLongestDim >= vMinSecondLongestDim)
            THEN
               vPassFail := 1;
            END IF;

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                     'Max. Third Longest Dimension'
                  || ' should be less than '
                  || 'Min. Second Longest Dimension',
                  4720190,
                  'Max. Third Longest Dimension'
                  || 'Min. Second Longest Dimension');
            END IF;
         END IF;

         IF (vMinLengthGrith IS NOT NULL OR vMaxLengthGrith IS NOT NULL)
         THEN
            vPassFail :=
               VALIDATE_DIMENSION (vTCCompanyId,
                                   vLaneId,
                                   vRatingDtlSeq,
                                   vMinLengthGrith,
                                   vMaxLengthGrith,
                                   'Length plus Grith');

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               DBMS_OUTPUT.PUT_LINE ('VALIDATE_DIMENSION failed');
            END IF;
         END IF;

         IF (vMinWeight IS NOT NULL OR vMaxWeight IS NOT NULL)
         THEN
            vPassFail := 0;

            IF ( (vMinWeight IS NOT NULL AND vMaxWeight IS NULL)
                OR (vMinWeight IS NULL AND vMaxWeight IS NOT NULL))
            THEN
               vPassFail := 1;

               IF (vPassFail = 1)
               THEN
                  INSERT_RATING_LANE_DTL_ERROR (
                     vTCCompanyId,
                     vLaneId,
                     vRatingDtlSeq,
                     'Minimum value and Maximum value are required for'
                     || 'Weight',
                     4720188,
                     'Weight');
               END IF;
            END IF;

            IF ( (vMinWeight IS NOT NULL AND vMaxWeight IS NOT NULL)
                AND (vMinWeight > vMaxWeight))
            THEN
               vPassFail := 1;
            END IF;

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                     'Minimum weight'
                  || ' should be less than '
                  || 'Maximum Weight',
                  4720190,
                  'Minimum weight' || 'Maximum Weight');
            END IF;
         END IF;

         -- MACR00159593 ends

         IF (vFailure = 0)
         THEN
            vcheckdupaccsql :=
               'SELECT COUNT (*) FROM import_lane_accessorial WHERE tc_company_id = '
               || vtccompanyid
               || ' AND lane_id = '
               || vlaneid
               || ' AND rating_lane_dtl_seq = '
               || vratingdtlseq
               || ' AND ROWID != '''
               || vrowid
               || ''''
               || ' AND accessorial_code = '''
               || vaccessorialcode
               || ''''
               || ' AND ((effective_dt between  '''
               || veffectivedt
               || ''' and '''
               || vexpirationdt
               || ''')'
               || ' OR   (expiration_dt between '''
               || veffectivedt
               || ''' and '''
               || vexpirationdt
               || '''))'
               || ' AND expiration_dt >= effective_dt';

            IF (vpayeecarrierid IS NOT NULL)
            THEN
               vcheckdupaccsql :=
                     vcheckdupaccsql
                  || ' AND payee_carrier_id = '
                  || vpayeecarrierid;
            ELSE
               vcheckdupaccsql :=
                  vcheckdupaccsql || ' AND payee_carrier_id  IS NULL';
            END IF;
			
			IF (vUseRangeForCommodityCode = 'true')
            THEN
             IF (vCommId IS NOT NULL AND vCommMaxId IS NOT NULL)
            THEN
                  vcheckdupaccsql :=
                     vcheckdupaccsql
                  || ' AND ( '
                  || '( '
                  || vCommId
                  || ' >= COMMODITY_CODE_ID and '
                  || vCommId
                  || ' <= MAX_RANGE_COMMODITY_CODE_ID) OR '
                  || '( '
                  || vCommMaxId
                  || ' >= COMMODITY_CODE_ID and '
                  || vCommMaxId
                  || ' <= MAX_RANGE_COMMODITY_CODE_ID) OR '
                  || '( COMMODITY_CODE_ID > '
                  || vCommId
                  || ' and COMMODITY_CODE_ID < '
                  || vCommMaxId
                  || ')  OR '
                  || '( MAX_RANGE_COMMODITY_CODE_ID > '
                  || vCommId
                  || ' and MAX_RANGE_COMMODITY_CODE_ID < '
                  || vCommMaxId
                  || '))';
               ELSE
                  vcheckdupaccsql :=
                     vcheckdupaccsql
                     || ' and commodity_code_id is null and max_range_commodity_code_id is null ';
               END IF;
            ELSE
               IF (vCommId IS NOT NULL)
               THEN
                  vcheckdupaccsql :=
                        vcheckdupaccsql
                     || ' and commodity_code_id = '
                     || vCommId;
               ELSE
                  vcheckdupaccsql :=
                     vcheckdupaccsql
                     || ' and commodity_code_id is null and max_range_commodity_code_id is null ';
               END IF;
            END IF;

            IF (vminqty IS NOT NULL AND vmaxqty IS NOT NULL)
            THEN
               vcheckdupaccsql :=
                     vcheckdupaccsql
                  || ' AND ( '
                  || '( '
                  || vminqty
                  || ' >= minimum_size and '
                  || vminqty
                  || ' <= maximum_size) OR '
                  || '( '
                  || vmaxqty
                  || ' >= minimum_size and '
                  || vmaxqty
                  || ' <= maximum_size) OR '
                  || '( minimum_size > '
                  || vminqty
                  || ' and minimum_size < '
                  || vmaxqty
                  || ')  OR '
                  || '( maximum_size > '
                  || vminqty
                  || ' and maximum_size < '
                  || vmaxqty
                  || '))';
            ELSE
               vcheckdupaccsql :=
                  vcheckdupaccsql
                  || ' AND minimum_size is null and maximum_size is null';
            END IF;

            IF (vProtectionLevelId IS NOT NULL)
            THEN
               vcheckdupaccsql :=
                     vcheckdupaccsql
                  || ' AND protection_level_id = '
                  || vProtectionLevelId;
            ELSE
               vcheckdupaccsql :=
                  vcheckdupaccsql || ' AND protection_level_id  IS NULL';
            END IF;
            
            OPEN c_ref FOR vcheckdupaccsql;

            FETCH c_ref INTO vcount;

            CLOSE c_ref;


            IF (vCount >= 1)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  'Effective/Expiration Dates overlapping with other Lane Accessorials. Accessorial Code :'
                  || vAccessorialCode
                  || ' Effective Date :'
                  || vEffectiveDt
                  || ' Expiration Date :'
                  || vExpirationDt,
                  4720149,
                     vAccessorialCode
                  || '<sep>'
                  || vEffectiveDt
                  || '<sep>'
                  || vExpirationDt);
            END IF;
         END IF;
		 vPassFail:=VALIDATE_ACC_FEASBLE_INCOTERMS(vTCCompanyId, vLaneId, vRatingDtlSeq, vImpLaneAccId);
		 IF (vPassFail = 1) THEN
			vFailure:= 1;
			DBMS_OUTPUT.PUT_LINE ('VALIDATE_LANE_DTL_ACCESSORIAL failed');
		END IF;
-----------------------Added here for INfeasibility between SL and AOPTG
		IF (vAccessorialId IS NOT NULL)
		THEN
       vPassFail:=VAL_SL_FEASBLE_ACCOPTGROUP(vTCCompanyId, vLaneId, vRatingDtlSeq, vAccessorialId,vServiceLevelId);
       IF (vPassFail = 1) THEN
        vFailure:= 1;
        DBMS_OUTPUT.PUT_LINE ('VAL_SL_FEASBLE_ACCOPTGROUP failed');
       END IF;
    END IF;
		
-------------------------ended here----------------------------------------------------------
		
		
      END LOOP LANE_ACCESS_LOOP;

      CLOSE LANE_ACCESS_CURSOR;

      vPassFail := 0;

      SELECT COUNT (LA.TC_COMPANY_ID)
        INTO vCount
        FROM IMPORT_LANE_ACCESSORIAL LA, ACCESSORIAL_CODE AC
       WHERE (LA.TC_COMPANY_ID = AC.TC_COMPANY_ID
              AND LA.ACCESSORIAL_CODE = AC.ACCESSORIAL_CODE)
             AND LA.TC_COMPANY_ID = vTCCompanyId
             AND LA.LANE_ID = vLaneId
             AND LA.RATING_LANE_DTL_SEQ = vRatingDtlSeq
             AND AC.DATA_SOURCE = 'MANUAL'
             AND LA.IS_AUTO_APPROVE = 1;

      IF (vCount >= 1)
      THEN
         vFailure := 1;
         INSERT_RATING_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRatingDtlSeq,
            'Accessorial cannot have a source type of MANUAL and be set for Auto Approval',
            4720150,
            NULL);
      END IF;

      RETURN vFailure;
   END VALIDATE_LANE_DTL_ACCESSORIAL;

   FUNCTION VALIDATE_DIMENSION (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vLaneId         IN rating_lane.LANE_ID%TYPE,
      vRatingDtlSeq   IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vMin            IN IMPORT_LANE_ACCESSORIAL.Min_Longest_Dimension%TYPE,
      vMax            IN IMPORT_LANE_ACCESSORIAL.Max_Longest_Dimension%TYPE,
      vMsg            IN VARCHAR2)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
   BEGIN
      vPassFail := 0;

      IF ( (vMin IS NOT NULL AND vMax IS NULL)
          OR (vMax IS NOT NULL AND vMin IS NULL))
      THEN
         vPassFail := 1;

         IF (vPassFail = 1)
         THEN
            INSERT_RATING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRatingDtlSeq,
               ' Minimum value and Maximum value are required for ' || vMsg,
               4720188,
               vMsg);
         END IF;
      END IF;

      IF (vMin IS NOT NULL AND vMax IS NOT NULL)
      THEN
         IF (vMin > vMax)
         THEN
            vPassFail := 1;
         END IF;

         IF (vPassFail = 1)
         THEN
            INSERT_RATING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRatingDtlSeq,
               ' Minimum value should be less than Maximum value  for '
               || vMsg,
               4720189,
               vMsg);
         END IF;
      END IF;

      RETURN vPassFail;
   END VALIDATE_DIMENSION;

   PROCEDURE ADJUST_EFF_EXP_DT (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vLaneId                IN COMB_LANE.LANE_ID%TYPE,
      vRatingDtlSeq          IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vEffectiveDT           IN COMB_LANE_DTL.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN COMB_LANE_DTL.EXPIRATION_DT%TYPE,
      vCarrierCode           IN COMB_LANE_DTL.CARRIER_ID%TYPE,
      vMot                   IN COMB_LANE_DTL.MOT_ID%TYPE,
      vEquipment             IN COMB_LANE_DTL.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN COMB_LANE_DTL.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN COMB_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN COMB_LANE_DTL.PROTECTION_LEVEL_ID%TYPE,
      vOriginShipVia         IN COMB_LANE_DTL.O_SHIP_VIA%TYPE,
      vDestinationShipVia    IN COMB_LANE_DTL.D_SHIP_VIA%TYPE)
   IS
      eff_exp_sql                VARCHAR2 (2000);
      eff_exp_cursor             ref_curtype;
      vCurrentRatingLaneDtlSeq   COMB_LANE_DTL.lane_id%TYPE;
      vEffDT                     COMB_LANE_DTL.effective_dt%TYPE;
      vExpDT                     COMB_LANE_DTL.expiration_dt%TYPE;
      vExpDTTemp                 COMB_LANE_DTL.expiration_dt%TYPE;
      vIsRating                  COMB_LANE_DTL.IS_RATING%TYPE;
      vIsRouting                 COMB_LANE_DTL.IS_ROUTING%TYPE;
      vIsSailing                 COMB_LANE_DTL.IS_SAILING%TYPE;
      isFirstRow                 INT;
      isInsertAtEnd              INT;
      isChanged                  INT;
      vMotTemp                   COMB_LANE_DTL.MOT_ID%TYPE;
      vEquipmentTemp             COMB_LANE_DTL.EQUIPMENT_ID%TYPE;
      vServiceLevelTemp          COMB_LANE_DTL.SERVICE_LEVEL_ID%TYPE;
      vProtectionLevelTemp       COMB_LANE_DTL.PROTECTION_LEVEL_ID%TYPE;
      vIsBudgetedTemp            COMB_LANE_DTL.IS_BUDGETED%TYPE;
	  tempSeqNum                 INT;
   BEGIN
      eff_exp_sql :=
         ' SELECT lane_dtl_seq, effective_dt, expiration_dt, is_rating, is_routing, is_sailing '
         || ' FROM comb_lane_dtl '
         || ' WHERE tc_company_id = :vTCCompanyId'
         || ' AND lane_id = :vLaneId'
         || ' AND lane_dtl_seq != :vRatingDtlSeq'
         || ' AND lane_dtl_status = 0 '
         || ' AND effective_dt < expiration_dt '
         || ' AND expiration_dt >= :vEffectiveDT '
         || ' AND effective_dt <= :vExpirationDT ';

      vMotTemp := vMot;
      vEquipmentTemp := vEquipment;
      vServiceLevelTemp := vServiceLevel;
      vProtectionLevelTemp := vProtectionLevel;

       /*
IF( vMot = 'ALL' ) THEN
 vMotTemp := null;
END IF;

IF( vEquipment = 'ALL' ) THEN
 vEquipmentTemp := null;
END IF;

IF( vServiceLevel = 'ALL' ) THEN
 vServiceLevelTemp := null;
END IF;

IF( vProtectionLevel = 'ALL' ) THEN
 vProtectionLevelTemp := null;
END IF;
       */
      eff_exp_sql :=
         eff_exp_sql
         || GET_RESOURCE_WHERE_CLAUSE (vCarrierCode,
                                       vMotTemp,
                                       vEquipmentTemp,
                                       vServiceLevelTemp,
                                       p_scndr_carrier_code,
                                       vProtectionLevelTemp,
                                       vOriginShipVia,
                                       vDestinationShipVia);

      eff_exp_sql := eff_exp_sql || ' ORDER BY EFFECTIVE_DT';

      isInsertAtEnd := 1;
      isFirstRow := 0;
      isChanged := 0;

      SELECT IS_BUDGETED
        INTO vIsBudgetedTemp
        FROM COMB_LANE_DTL
       WHERE     TC_COMPANY_ID = vTCCompanyId
             AND LANE_ID = vLaneId
             AND LANE_DTL_SEQ = vRatingDtlSeq;

      OPEN eff_exp_cursor FOR eff_exp_sql
         USING vTCCompanyId,
               vLaneId,
               vRatingDtlSeq,
               vEffectiveDT,
               vExpirationDT;

      LOOP
         FETCH eff_exp_cursor
         INTO vCurrentRatingLaneDtlSeq,
              vEffDT,
              vExpDT,
              vIsRating,
              vIsRouting,
              vIsSailing;

         EXIT WHEN eff_exp_cursor%NOTFOUND;

         -- Start of Merge with other lane detail
         IF (vEffectiveDT <= vEffDt)
         THEN
            IF (isFirstRow = 0)
            THEN
               IF (vEffectiveDT <> vEffDt)
               THEN
                  -- create row from input effective date to effective date of current row
                  CREATE_NEW_ROW (vTCCompanyId,
                                  NULL,
                                  vLaneId,
                                  vRatingDtlSeq,
                                  vEffectiveDT,
                                  vEffDt - 1);
               END IF;
            ELSE
               IF (vExpDTTemp <> vEffDt)
               THEN
                  -- create row from input effective date to effective date of current row
                  CREATE_NEW_ROW (vTCCompanyId,
                                  NULL,
                                  vLaneId,
                                  vRatingDtlSeq,
                                  vExpDTTemp,
                                  vEffDt - 1);
               END IF;
            END IF;

            IF (vExpirationDT < vExpDT)
            THEN
               --replicate row from input expiration date to exp date of current row with rating flag turned off
               tempSeqNum :=REPLICATE_ROW (vTCCompanyId,
                              NULL,
                              vLaneId,
                              -1,
                              vCurrentRatingLaneDtlSeq,
                              vExpirationDT + 1,
                              vExpDT,
                              vIsRating,
                              vIsRouting,
                              vIsSailing);

               UPDATE COMB_LANE_DTL
                  SET is_rating = 1,
                      expiration_dt = vExpirationDT,
                      is_budgeted = vIsBudgetedTemp,
                      last_updated_source_type = 3,
                      last_updated_source = 'OMS',
                      last_updated_dttm = Getdate ()
                WHERE     tc_company_id = vTCCompanyId
                      AND lane_id = vLaneId
                      AND lane_dtl_seq = vCurrentRatingLaneDtlSeq;

               --COPY_CHILD_ROW( vTCCompanyId, vLaneId, vRatingDtlSeq, vCurrentRatingLaneDtlSeq,
               -- vCurrentRatingLaneDtlSeq, 1, 0, 0 );

               isInsertAtEnd := 1;
            ELSE
               UPDATE COMB_LANE_DTL
                  SET is_rating = 1,
                      is_budgeted = vIsBudgetedTemp,
                      last_updated_source_type = 3,
                      last_updated_source = 'OMS',
                      last_updated_dttm = Getdate ()
                WHERE     tc_company_id = vTCCompanyId
                      AND lane_id = vLaneId
                      AND lane_dtl_seq = vCurrentRatingLaneDtlSeq;

               --COPY_CHILD_ROW( vTCCompanyId, vLaneId, vRatingDtlSeq, vCurrentRatingLaneDtlSeq,
               -- vCurrentRatingLaneDtlSeq, 1, 0, 0 );

               isInsertAtEnd := 0;
            END IF;

            IF (vIsRating = 1)
            THEN
               DELETE_CHILD_ROW (vTCCompanyId,
                                 vLaneId,
                                 vCurrentRatingLaneDtlSeq);
            END IF;

            COPY_CHILD_ROW (vTCCompanyId,
                            vLaneId,
                            vRatingDtlSeq,
                            vCurrentRatingLaneDtlSeq,
                            vCurrentRatingLaneDtlSeq,
                            1,
                            0,
                            0,'OMS');
         ELSE
            UPDATE COMB_LANE_DTL
               SET expiration_dt = vEffectiveDT - 1,
                   last_updated_source_type = 3,
                   last_updated_source = 'OMS',
                   last_updated_dttm = Getdate ()
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND lane_dtl_seq = vCurrentRatingLaneDtlSeq;

            IF (vExpirationDT < vExpDT)
            THEN
               --replicate row from input effective date to input exp date with rating flag turned on
               tempSeqNum :=REPLICATE_ROW (vTCCompanyId,
                              NULL,
                              vLaneId,
                              vRatingDtlSeq,
                              vCurrentRatingLaneDtlSeq,
                              vEffectiveDT,
                              vExpirationDT,
                              1,
                              vIsRouting,
                              vIsSailing);
               --replicate row from input expiration date to exp date of current row with rating flag turned off
              tempSeqNum := REPLICATE_ROW (vTCCompanyId,
                              NULL,
                              vLaneId,
                              -1,
                              vCurrentRatingLaneDtlSeq,
                              vExpirationDT + 1,
                              vExpDT,
                              vIsRating,
                              vIsRouting,
                              vIsSailing);
               isInsertAtEnd := 1;
            ELSE
               --replicate row from input eff date to exp date of current row with rating flag turned on
               tempSeqNum :=REPLICATE_ROW (vTCCompanyId,
                              NULL,
                              vLaneId,
                              vRatingDtlSeq,
                              vCurrentRatingLaneDtlSeq,
                              vEffectiveDT,
                              vExpDT,
                              1,
                              vIsRouting,
                              vIsSailing);
               isInsertAtEnd := 0;
            END IF;
         END IF;

         isFirstRow := 1;
         vExpDTTemp := vExpDT + 1;
      END LOOP;

      IF (isInsertAtEnd = 0)
      THEN
         IF (vExpDT <> vExpirationDT)
         THEN
            -- create row from exp date of last row to input expiration date
            CREATE_NEW_ROW (vTCCompanyId,
                            NULL,
                            vLaneId,
                            vRatingDtlSeq,
                            vExpDT + 1,
                            vExpirationDT);
         END IF;
      END IF;

      IF (isFirstRow = 1)
      THEN
         --UPDATE comb_lane_dtl
         -- SET lane_dtl_status = 2,
         -- last_updated_source_type = 3,
         --  last_updated_source = 'OMS',
         --  last_updated_dttm = getdate()
         DELETE_CHILD_ROW (vTCCompanyId, vLaneId, vRatingDtlSeq);

         DELETE FROM COMB_LANE_DTL
               WHERE     tc_company_id = vTCCompanyId
                     AND lane_id = vLaneId
                     AND lane_dtl_seq = vRatingDtlSeq;
      END IF;

      COMMIT;

      CLOSE eff_exp_cursor;
   END ADJUST_EFF_EXP_DT;

   PROCEDURE ADJUST_EFF_EXP_DT (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vLaneId                IN COMB_LANE.LANE_ID%TYPE,
      vRatingDtlSeq          IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vRatingDtlSeqUpd       IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vEffectiveDT           IN COMB_LANE_DTL.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN COMB_LANE_DTL.EXPIRATION_DT%TYPE,
      vCarrierCode           IN COMB_LANE_DTL.CARRIER_ID%TYPE,
      vMot                   IN COMB_LANE_DTL.MOT_ID%TYPE,
      vEquipment             IN COMB_LANE_DTL.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN COMB_LANE_DTL.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN COMB_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN COMB_LANE_DTL.PROTECTION_LEVEL_ID%TYPE,
      vOriginShipVia         IN COMB_LANE_DTL.O_SHIP_VIA%TYPE,
      vDestinationShipVia    IN COMB_LANE_DTL.D_SHIP_VIA%TYPE)
   IS
      eff_exp_sql                VARCHAR2 (2000);
      eff_exp_cursor             ref_curtype;
      vCurrentRatingLaneDtlSeq   COMB_LANE_DTL.lane_id%TYPE;
      vEffDT                     COMB_LANE_DTL.effective_dt%TYPE;
      vExpDT                     COMB_LANE_DTL.expiration_dt%TYPE;
      vExpDTTemp                 COMB_LANE_DTL.expiration_dt%TYPE;
      vIsRating                  COMB_LANE_DTL.IS_RATING%TYPE;
      vIsRouting                 COMB_LANE_DTL.IS_ROUTING%TYPE;
      vIsSailing                 COMB_LANE_DTL.IS_SAILING%TYPE;
      isFirstRow                 INT;
      isInsertAtEnd              INT;
      isChanged                  INT;
      tempCount                  INT;
      vMotTemp                   COMB_LANE_DTL.MOT_ID%TYPE;
      vEquipmentTemp             COMB_LANE_DTL.EQUIPMENT_ID%TYPE;
      vServiceLevelTemp          COMB_LANE_DTL.SERVICE_LEVEL_ID%TYPE;
      vProtectionLevelTemp       COMB_LANE_DTL.PROTECTION_LEVEL_ID%TYPE;
      vIsBudgetedTemp            COMB_LANE_DTL.IS_BUDGETED%TYPE;
	  tempSeqNum                  INT;
   BEGIN
      eff_exp_sql :=
         ' SELECT lane_dtl_seq, effective_dt, expiration_dt, is_rating, is_routing, is_sailing '
         || ' FROM comb_lane_dtl '
         || ' WHERE tc_company_id = :vTCCompanyId'
         || ' AND lane_id = :vLaneId'
         || ' AND lane_dtl_seq != :vRatingDtlSeq'
         || ' AND HAS_SHIPPING_PARAM = 0 '
         || ' AND lane_dtl_status = 0 '
         || ' AND effective_dt < expiration_dt '
         || ' AND expiration_dt >= :vEffectiveDT '
         || ' AND effective_dt <= :vExpirationDT ';

      vMotTemp := vMot;
      vEquipmentTemp := vEquipment;
      vServiceLevelTemp := vServiceLevel;
      vProtectionLevelTemp := vProtectionLevel;
       /*
IF( vMot = 'ALL' ) THEN
 vMotTemp := null;
END IF;

IF( vEquipment = 'ALL' ) THEN
 vEquipmentTemp := null;
END IF;

IF( vServiceLevel = 'ALL' ) THEN
 vServiceLevelTemp := null;
END IF;

IF( vProtectionLevel = 'ALL' ) THEN
 vProtectionLevelTemp := null;
END IF;
       */
      tempCount := 0;

      IF (vRatingDtlSeqUpd IS NOT NULL)
      THEN
         --tempCount := MERGE_DRAFT( vTCCompanyId, vLaneId, vRatingDtlSeq, vRatingDtlSeqUpd );
         SELECT COUNT (*)
           INTO tempCount
           FROM COMB_LANE_DTL
          WHERE     TC_COMPANY_ID = vTCCompanyId
                AND LANE_ID = vLaneId
                AND LANE_DTL_SEQ = vRatingDtlSeqUpd
                AND IS_SAILING = 0
                AND IS_ROUTING = 0;

         IF (tempCount > 0)
         THEN
            eff_exp_sql :=
               eff_exp_sql || ' AND lane_dtl_seq != ' || vRatingDtlSeqUpd;
         END IF;
      END IF;

      eff_exp_sql :=
         eff_exp_sql
         || GET_RESOURCE_WHERE_CLAUSE (vCarrierCode,
                                       vMotTemp,
                                       vEquipmentTemp,
                                       vServiceLevelTemp,
                                       p_scndr_carrier_code,
                                       vProtectionLevelTemp,
                                       vOriginShipVia,
                                       vDestinationShipVia);

      eff_exp_sql := eff_exp_sql || ' ORDER BY EFFECTIVE_DT';

      isInsertAtEnd := 1;
      isFirstRow := 0;
      isChanged := 0;

      SELECT IS_BUDGETED
        INTO vIsBudgetedTemp
        FROM COMB_LANE_DTL
       WHERE     TC_COMPANY_ID = vTCCompanyId
             AND LANE_ID = vLaneId
             AND LANE_DTL_SEQ = vRatingDtlSeq;

      OPEN eff_exp_cursor FOR eff_exp_sql
         USING vTCCompanyId,
               vLaneId,
               vRatingDtlSeq,
               vEffectiveDT,
               vExpirationDT;

      LOOP
         FETCH eff_exp_cursor
         INTO vCurrentRatingLaneDtlSeq,
              vEffDT,
              vExpDT,
              vIsRating,
              vIsRouting,
              vIsSailing;

         EXIT WHEN eff_exp_cursor%NOTFOUND;

         -- Start of Merge with other lane detail
         IF (vEffectiveDT <= vEffDt)
         THEN
            IF (isFirstRow = 0)
            THEN
               IF (vEffectiveDT <> vEffDt)
               THEN
                  IF ( (tempCount > 0) AND (isChanged = 0))
                  THEN
                     isChanged := 1;
                     tempCount :=
                        MERGE_DRAFT (vTCCompanyId,
                                     NULL,
                                     vLaneId,
                                     vRatingDtlSeq,
                                     vRatingDtlSeqUpd);

                     -- Update the existing row
                     UPDATE COMB_LANE_DTL
                        SET effective_dt = vEffectiveDT,
                            expiration_dt = vEffDt - 1,
                            last_updated_source_type = 3,
                            last_updated_source = 'OMS',
                            last_updated_dttm = Getdate ()
                      WHERE     tc_company_id = vTCCompanyId
                            AND lane_id = vLaneId
                            AND lane_dtl_seq = vRatingDtlSeqUpd;
                  ELSE
                     -- create row from input effective date to effective date of current row
                     CREATE_NEW_ROW (vTCCompanyId,
                                     NULL,
                                     vLaneId,
                                     vRatingDtlSeq,
                                     vEffectiveDT,
                                     vEffDt - 1);
                  END IF;
               END IF;
            ELSE
               IF (vExpDTTemp <> vEffDt)
               THEN
                  IF ( (tempCount > 0) AND (isChanged = 0))
                  THEN
                     isChanged := 1;
                     tempCount :=
                        MERGE_DRAFT (vTCCompanyId,
                                     NULL,
                                     vLaneId,
                                     vRatingDtlSeq,
                                     vRatingDtlSeqUpd);

                     -- Update the existing row
                     UPDATE COMB_LANE_DTL
                        SET effective_dt = vExpDTTemp,
                            expiration_dt = vEffDt - 1,
                            last_updated_source_type = 3,
                            last_updated_source = 'OMS',
                            last_updated_dttm = Getdate ()
                      WHERE     tc_company_id = vTCCompanyId
                            AND lane_id = vLaneId
                            AND lane_dtl_seq = vRatingDtlSeqUpd;
                  ELSE
                     -- create row from input effective date to effective date of current row
                     CREATE_NEW_ROW (vTCCompanyId,
                                     NULL,
                                     vLaneId,
                                     vRatingDtlSeq,
                                     vExpDTTemp,
                                     vEffDt - 1);
                  END IF;
               END IF;
            END IF;

            IF (vExpirationDT < vExpDT)
            THEN
               --replicate row from input expiration date to exp date of current row with rating flag turned off
               tempSeqNum :=REPLICATE_ROW (vTCCompanyId,
                              NULL,
                              vLaneId,
                              -1,
                              vCurrentRatingLaneDtlSeq,
                              vExpirationDT + 1,
                              vExpDT,
                              vIsRating,
                              vIsRouting,
                              vIsSailing);

               UPDATE COMB_LANE_DTL
                  SET is_rating = 1,
                      expiration_dt = vExpirationDT,
                      is_budgeted = vIsBudgetedTemp,
                      last_updated_source_type = 3,
                      last_updated_source = 'OMS',
                      last_updated_dttm = Getdate ()
                WHERE     tc_company_id = vTCCompanyId
                      AND lane_id = vLaneId
                      AND lane_dtl_seq = vCurrentRatingLaneDtlSeq;

               --COPY_CHILD_ROW( vTCCompanyId, vLaneId, vRatingDtlSeq, vCurrentRatingLaneDtlSeq,
               -- vCurrentRatingLaneDtlSeq, 1, 0, 0 );

               isInsertAtEnd := 1;
            ELSE
               UPDATE COMB_LANE_DTL
                  SET is_rating = 1,
                      is_budgeted = vIsBudgetedTemp,
                      last_updated_source_type = 3,
                      last_updated_source = 'OMS',
                      last_updated_dttm = Getdate ()
                WHERE     tc_company_id = vTCCompanyId
                      AND lane_id = vLaneId
                      AND lane_dtl_seq = vCurrentRatingLaneDtlSeq;

               --COPY_CHILD_ROW( vTCCompanyId, vLaneId, vRatingDtlSeq, vCurrentRatingLaneDtlSeq,
               -- vCurrentRatingLaneDtlSeq, 1, 0, 0 );

               isInsertAtEnd := 0;
            END IF;

            IF (vIsRating = 1)
            THEN
               DELETE_CHILD_ROW (vTCCompanyId,
                                 vLaneId,
                                 vCurrentRatingLaneDtlSeq);
            END IF;

            COPY_CHILD_ROW (vTCCompanyId,
                            vLaneId,
                            vRatingDtlSeq,
                            vCurrentRatingLaneDtlSeq,
                            vCurrentRatingLaneDtlSeq,
                            1,
                            0,
                            0,'OMS');
         ELSE
            UPDATE COMB_LANE_DTL
               SET expiration_dt = vEffectiveDT - 1,
                   last_updated_source_type = 3,
                   last_updated_source = 'OMS',
                   last_updated_dttm = Getdate ()
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND lane_dtl_seq = vCurrentRatingLaneDtlSeq;

            IF (vExpirationDT < vExpDT)
            THEN
               --replicate row from input effective date to input exp date with rating flag turned on
               tempSeqNum :=REPLICATE_ROW (vTCCompanyId,
                              NULL,
                              vLaneId,
                              vRatingDtlSeq,
                              vCurrentRatingLaneDtlSeq,
                              vEffectiveDT,
                              vExpirationDT,
                              1,
                              vIsRouting,
                              vIsSailing);
               --replicate row from input expiration date to exp date of current row with rating flag turned off
               tempSeqNum :=REPLICATE_ROW (vTCCompanyId,
                              NULL,
                              vLaneId,
                              -1,
                              vCurrentRatingLaneDtlSeq,
                              vExpirationDT + 1,
                              vExpDT,
                              vIsRating,
                              vIsRouting,
                              vIsSailing);
               isInsertAtEnd := 1;
            ELSE
               --replicate row from input eff date to exp date of current row with rating flag turned on
               tempSeqNum :=REPLICATE_ROW (vTCCompanyId,
                              NULL,
                              vLaneId,
                              vRatingDtlSeq,
                              vCurrentRatingLaneDtlSeq,
                              vEffectiveDT,
                              vExpDT,
                              1,
                              vIsRouting,
                              vIsSailing);
               isInsertAtEnd := 0;
            END IF;
         END IF;

         isFirstRow := 1;
         vExpDTTemp := vExpDT + 1;
      END LOOP;

      IF (isInsertAtEnd = 0)
      THEN
         IF (vExpDT <> vExpirationDT)
         THEN
            IF ( (tempCount > 0) AND (isChanged = 0))
            THEN
               isChanged := 1;
               tempCount :=
                  MERGE_DRAFT (vTCCompanyId,
                               NULL,
                               vLaneId,
                               vRatingDtlSeq,
                               vRatingDtlSeqUpd);

               -- Update the existing row
               UPDATE COMB_LANE_DTL
                  SET effective_dt = vExpDT + 1,
                      expiration_dt = vExpirationDT,
                      last_updated_source_type = 3,
                      last_updated_source = 'OMS',
                      last_updated_dttm = Getdate ()
                WHERE     tc_company_id = vTCCompanyId
                      AND lane_id = vLaneId
                      AND lane_dtl_seq = vRatingDtlSeqUpd;
            ELSE
               -- create row from exp date of last row to input expiration date
               CREATE_NEW_ROW (vTCCompanyId,
                               NULL,
                               vLaneId,
                               vRatingDtlSeq,
                               vExpDT + 1,
                               vExpirationDT);
            END IF;
         END IF;
      END IF;

      IF (isFirstRow = 1)
      THEN
         --UPDATE comb_lane_dtl
         -- SET lane_dtl_status = 2,
         -- last_updated_source_type = 3,
         -- last_updated_source = 'OMS',
         --  last_updated_dttm = getdate()
         DELETE_CHILD_ROW (vTCCompanyId, vLaneId, vRatingDtlSeq);

         DELETE FROM COMB_LANE_DTL
               WHERE     tc_company_id = vTCCompanyId
                     AND lane_id = vLaneId
                     AND lane_dtl_seq = vRatingDtlSeq;
      END IF;

      --For Update flow
      IF (tempCount > 0)
      THEN
         -- If the draft is not merged with row which is getting updated
         IF ( (isChanged = 0) AND (isFirstRow = 1))
         THEN
            UPDATE COMB_LANE_DTL
               SET expiration_dt = effective_dt - 1,
                   last_updated_source_type = 3,
                   last_updated_source = 'OMS',
                   last_updated_dttm = Getdate ()
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND lane_dtl_seq = vRatingDtlSeqUpd;
         -- If the draft is not merged with any row
         ELSIF (isFirstRow = 0)
         THEN
            tempCount :=
               MERGE_DRAFT (vTCCompanyId,
                            NULL,
                            vLaneId,
                            vRatingDtlSeq,
                            vRatingDtlSeqUpd);
            DELETE_CHILD_ROW (vTCCompanyId, vLaneId, vRatingDtlSeq);

            DELETE FROM COMB_LANE_DTL
                  WHERE     tc_company_id = vTCCompanyId
                        AND lane_id = vLaneId
                        AND lane_dtl_seq = vRatingDtlSeq;
         END IF;
      END IF;

      COMMIT;

      CLOSE eff_exp_cursor;
   END ADJUST_EFF_EXP_DT;

   -- Procedure COPY CARRIERS for updation of relavent carriers with the lane details changes when modified  SR1, 4R1 change, build92,  TT31182 [BEGIN]
   PROCEDURE COPY_CARRIERS (
      vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
      vLaneId        IN COMB_LANE.LANE_ID%TYPE,
      vOldLaneId     IN COMB_LANE.LANE_ID%TYPE,
      vuser          IN comb_lane_dtl.last_updated_source%TYPE)
   IS
      select_sql               VARCHAR2 (2000);
      select_cursor            ref_curtype;
      vLaneDtlSeq              COMB_LANE_DTL.LANE_DTL_SEQ%TYPE;
      vLaneDtlSeqNew           COMB_LANE_DTL.LANE_DTL_SEQ%TYPE;
      vEffectiveDT             COMB_LANE_DTL.EFFECTIVE_DT%TYPE;
      vExpirationDT            COMB_LANE_DTL.EXPIRATION_DT%TYPE;
      vCarrierCode             COMB_LANE_DTL.CARRIER_ID%TYPE;
      vMot                     COMB_LANE_DTL.MOT_ID%TYPE;
      vEquipment               COMB_LANE_DTL.EQUIPMENT_ID%TYPE;
      vServiceLevel            COMB_LANE_DTL.SERVICE_LEVEL_ID%TYPE;
      p_scndr_carrier_code     COMB_LANE_DTL.SCNDR_CARRIER_ID%TYPE;
      vProtectionLevel         COMB_LANE_DTL.PROTECTION_LEVEL_Id%TYPE;
      vOriginShipVia           COMB_LANE_DTL.O_SHIP_VIA%TYPE;
      vDestinationShipVia      COMB_LANE_DTL.D_SHIP_VIA%TYPE;
      vLaneDtlStatus           COMB_LANE_DTL.LANE_DTL_STATUS%TYPE;
      vMasterLaneId            COMB_LANE_DTL.MASTER_LANE_ID%TYPE;

      vCustomText1             COMB_LANE_DTL.CUSTOM_TEXT1%TYPE;
      vCustomText2             COMB_LANE_DTL.CUSTOM_TEXT2%TYPE;
      vCustomText3             COMB_LANE_DTL.CUSTOM_TEXT3%TYPE;
      vCustomText4             COMB_LANE_DTL.CUSTOM_TEXT4%TYPE;
      vCustomText5             COMB_LANE_DTL.CUSTOM_TEXT5%TYPE;
      vlastupdatedsource       comb_lane_dtl.last_updated_source%TYPE;
      vlastupdatedsourcetype   comb_lane_dtl.last_updated_source_type%TYPE;
	  vVoyage				   comb_lane_dtl.VOYAGE%TYPE;
	  tempSeqNum               INT;
   BEGIN
      SELECT MASTER_LANE_ID
        INTO vMasterLaneId
        FROM COMB_LANE
       WHERE tc_company_id = vTCCompanyId AND lane_id = vLaneId;

      IF (vuser IS NOT NULL)
      THEN
         vlastupdatedsource := vuser;
         vlastupdatedsourcetype := 1;
      ELSE
         vlastupdatedsource := 'OMS';
         vlastupdatedsourcetype := 3;
      END IF;

	
      -- MACR00167063 fix, added comma after D_SHIP_VIA column in the below query
      select_sql :=
            ' SELECT LANE_DTL_SEQ, LANE_DTL_STATUS, '
         || ' EFFECTIVE_DT, EXPIRATION_DT, '
         || ' CARRIER_ID, MOT_ID, '
         || ' EQUIPMENT_ID, SERVICE_LEVEL_ID, '
         || ' PROTECTION_LEVEL_ID, SCNDR_CARRIER_ID, '
         || ' O_SHIP_VIA, D_SHIP_VIA, '
         || ' CUSTOM_TEXT1,CUSTOM_TEXT2,CUSTOM_TEXT3,CUSTOM_TEXT4,CUSTOM_TEXT5, VOYAGE '
         || ' FROM COMB_LANE_DTL '
         || ' WHERE tc_company_id = :vTCCompanyId'
         || ' AND lane_id = :vOldLaneId'
         || ' AND is_rating = 1'
         || ' AND lane_dtl_status < 2';

      OPEN select_cursor FOR select_sql USING vTCCompanyId, vOldLaneId;

      LOOP
         FETCH select_cursor
         INTO vLaneDtlSeq,
              vLaneDtlStatus,
              vEffectiveDT,
              vExpirationDT,
              vCarrierCode,
              vMot,
              vEquipment,
              vServiceLevel,
              vProtectionLevel,
              p_scndr_carrier_code,
              vOriginShipVia,
              vDestinationShipVia,
              vCustomText1,
              vCustomText2,
              vCustomText3,
              vCustomText4,
              vCustomText5,
			  vVoyage;

         EXIT WHEN select_cursor%NOTFOUND;

         SELECT NVL (MAX (LANE_DTL_SEQ), 0) + 1
           INTO vLaneDtlSeqNew
           FROM COMB_LANE_DTL
          WHERE TC_COMPANY_ID = vTCCompanyID AND LANE_ID = vLaneID;

         INSERT INTO COMB_LANE_DTL (TC_COMPANY_ID,
                                    LANE_ID,
                                    LANE_DTL_SEQ,
                                    EFFECTIVE_DT,
                                    EXPIRATION_DT,
                                    CARRIER_ID,
                                    IS_BUDGETED,
                                    MOT_ID,
                                    EQUIPMENT_ID,
                                    SERVICE_LEVEL_ID,
                                    PROTECTION_LEVEL_ID,
                                    SCNDR_CARRIER_ID,
                                    O_SHIP_VIA,
                                    D_SHIP_VIA,
                                    CONTRACT_NUMBER,
                                    CUSTOM_TEXT1,
                                    CUSTOM_TEXT2,
                                    CUSTOM_TEXT3,
                                    CUSTOM_TEXT4,
                                    CUSTOM_TEXT5,
									VOYAGE,
                                    LAST_UPDATED_SOURCE_TYPE,
                                    LAST_UPDATED_SOURCE,
                                    LAST_UPDATED_DTTM,
                                    LANE_DTL_STATUS,
                                    IS_RATING,
                                    MASTER_LANE_ID,
                                    REP_TP_FLAG,
				    REJECT_FURTHER_SHIPMENTS,
				    CARRIER_REJECT_PERIOD)
            SELECT TC_COMPANY_ID,
                   vLaneId,
                   vLaneDtlSeqNew,
                   EFFECTIVE_DT,
                   EXPIRATION_DT,
                   CARRIER_ID,
                   IS_BUDGETED,
                   MOT_ID,
                   EQUIPMENT_ID,
                   SERVICE_LEVEL_ID,
                   PROTECTION_LEVEL_ID,
                   SCNDR_CARRIER_ID,
                   O_SHIP_VIA,
                   D_SHIP_VIA,
                   CONTRACT_NUMBER,
                   CUSTOM_TEXT1,
                   CUSTOM_TEXT2,
                   CUSTOM_TEXT3,
                   CUSTOM_TEXT4,
                   CUSTOM_TEXT5,
				   VOYAGE,
                   vlastupdatedsourcetype,
                   vlastupdatedsource,
                   Getdate (),
                   1,
                   1,
                   vMasterLaneId,
                   1,
		   REJECT_FURTHER_SHIPMENTS,
		   CARRIER_REJECT_PERIOD 
              FROM COMB_LANE_DTL
             WHERE     TC_COMPANY_ID = vTCCompanyId
                   AND LANE_ID = vOldLaneId
                   AND LANE_DTL_SEQ = vLaneDtlSeq;

         INSERT INTO RATING_LANE_DTL_RATE (TC_COMPANY_ID,
                                           LANE_ID,
                                           RATING_LANE_DTL_SEQ,
                                           RLD_RATE_SEQ,
                                           MINIMUM_SIZE,
                                           MAXIMUM_SIZE,
                                           SIZE_UOM_ID,
                                           RATE_CALC_METHOD,
                                           RATE_UOM,
                                           RATE,
                                           MINIMUM_RATE,
                                           CURRENCY_CODE,
                                           TARIFF_CODE_ID, -- (8/8/2008 gakumar)
                                           MIN_DISTANCE,
                                           MAX_DISTANCE,
                                           DISTANCE_UOM,
                                           SUPPORTS_MSTL,
                                           HAS_BH,
                                           HAS_RT,
                                           MIN_COMMODITY,
                                           MAX_COMMODITY,
                                           COMMODITY_CODE_ID,
                                           EXCESS_WT_RATE,
                                           PAYEE_CARRIER_ID,
                                           MAX_RANGE_COMMODITY_CODE_ID,
                                           MINIMUM_RATE_TYPE,
                                           BASE_RATE,
                                           MAXIMUM_RATE,
                                           CUSTOMER_MIN_CHARGE,
                                           CUSTOMER_BASE_RATE,
                                           CUSTOMER_SURCHARGE_RATE)
            SELECT TC_COMPANY_ID,
                   vLaneId,
                   vLaneDtlSeqNew,
                   RLD_RATE_SEQ,
                   MINIMUM_SIZE,
                   MAXIMUM_SIZE,
                   SIZE_UOM_ID,
                   RATE_CALC_METHOD,
                   RATE_UOM,
                   RATE,
                   MINIMUM_RATE,
                   CURRENCY_CODE,
                   TARIFF_CODE_ID,                       -- (8/8/2008 gakumar)
                   MIN_DISTANCE,
                   MAX_DISTANCE,
                   DISTANCE_UOM,
                   SUPPORTS_MSTL,
                   HAS_BH,
                   HAS_RT,
                   MIN_COMMODITY,
                   MAX_COMMODITY,
                   COMMODITY_CODE_ID,
                   EXCESS_WT_RATE,
                   PAYEE_CARRIER_ID,
                   MAX_RANGE_COMMODITY_CODE_ID,
                   MINIMUM_RATE_TYPE,
                   BASE_RATE,
                   MAXIMUM_RATE,
                   CUSTOMER_MIN_CHARGE,
                   CUSTOMER_BASE_RATE,
                   CUSTOMER_SURCHARGE_RATE
              FROM RATING_LANE_DTL_RATE
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vOldLaneId
                   AND rating_lane_dtl_seq = vLaneDtlSeq;

         INSERT INTO LANE_ACCESSORIAL (TC_COMPANY_ID,
                                       LANE_ID,
                                       RATING_LANE_DTL_SEQ,
                                       ACCESSORIAL_ID,
                                       RATE,
                                       MINIMUM_RATE,
                                       CURRENCY_CODE,
                                       IS_AUTO_APPROVE,
                                       EFFECTIVE_DT,
                                       EXPIRATION_DT,
                                       LANE_ACCESSORIAL_ID,
                                       PAYEE_CARRIER_ID,
                                       MINIMUM_SIZE,
                                       MAXIMUM_SIZE,
                                       SIZE_UOM_ID,
                                       ZONE_ID,
                                       IS_SHIPMENT_ACCESSORIAL,
                                       INCOTERM_ID,
                                       Min_Rate_Type,
                                       Maximum_Rate,
                                       PACKAGE_TYPE_ID,
                                       CUSTOMER_RATE,
                                       Customer_Min_Charge,
                                       Calculated_Rate,
                                       Amount,
                                       BASE_AMOUNT,
                                       Base_Charge,
                                       RANGE_MIN_AMOUNT,
                                       RANGE_MAX_AMOUNT,
                                       Increment_Val,
                                       Mot_Id,
                                       Equipment_Id,
                                       Service_Level_Id,
                                       PROTECTION_LEVEL_ID,
                                       Billing_Method,
                                       Min_Longest_Dimension,
                                       Max_Longest_Dimension,
                                       Min_Second_Longest_Dimension,
                                       Max_Second_Longest_Dimension,
                                       Min_Third_Longest_Dimension,
                                       Max_Third_Longest_Dimension,
                                       Min_Length_Plus_Grith,
                                       Max_Length_Plus_Grith,
                                       Min_Weight,
                                       Max_Weight,
                                       Is_Zone_Stopoff,
                                       COMMODITY_CODE_ID,
                                       MAX_RANGE_COMMODITY_CODE_ID)
            SELECT TC_COMPANY_ID,
                   vLaneId,
                   vLaneDtlSeqNew,
                   ACCESSORIAL_ID,
                   RATE,
                   MINIMUM_RATE,
                   CURRENCY_CODE,
                   IS_AUTO_APPROVE,
                   EFFECTIVE_DT,
                   EXPIRATION_DT,
                   LANE_ACCESSORIAL_ID_SEQ.NEXTVAL,
                   PAYEE_CARRIER_ID,
                   MINIMUM_SIZE,
                   MAXIMUM_SIZE,
                   SIZE_UOM_ID,
                   ZONE_ID,
                   IS_SHIPMENT_ACCESSORIAL,
                   INCOTERM_ID,
                   Min_Rate_Type,
                   Maximum_Rate,
                   PACKAGE_TYPE_ID,
                   CUSTOMER_RATE,
                   Customer_Min_Charge,
                   Calculated_Rate,
                   Amount,
                   BASE_AMOUNT,
                   Base_Charge,
                   RANGE_MIN_AMOUNT,
                   RANGE_MAX_AMOUNT,
                   Increment_Val,
                   Mot_Id,
                   Equipment_Id,
                   Service_Level_Id,
                   PROTECTION_LEVEL_ID,
                   Billing_Method,
                   Min_Longest_Dimension,
                   Max_Longest_Dimension,
                   Min_Second_Longest_Dimension,
                   Max_Second_Longest_Dimension,
                   Min_Third_Longest_Dimension,
                   Max_Third_Longest_Dimension,
                   Min_Length_Plus_Grith,
                   Max_Length_Plus_Grith,
                   Min_Weight,
                   Max_Weight,
                   Is_Zone_Stopoff,
                   COMMODITY_CODE_ID,
                   MAX_RANGE_COMMODITY_CODE_ID
              FROM LANE_ACCESSORIAL
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vOldLaneId
                   AND rating_lane_dtl_seq = vLaneDtlSeq;

         IF (vLaneDtlStatus = 0)
         THEN
            tempSeqNum := ADJUST_EFF_EXP_DT_UI (vTCCompanyId,
                                  vLaneId,
                                  vLaneDtlSeqNew,
                                  NULL,
                                  vEffectiveDT,
                                  vExpirationDT,
                                  vCarrierCode,
                                  vMot,
                                  vEquipment,
                                  vServiceLevel,
                                  p_scndr_carrier_code,
                                  vProtectionLevel,
                                  vOriginShipVia,
                                  vDestinationShipVia,
                                  vCustomText1,
                                  vCustomText2,
                                  vCustomText3,
                                  vCustomText4,
                                  vCustomText5,
                                  vuser,
								  vVoyage);

            UPDATE COMB_LANE_DTL
               SET LANE_DTL_STATUS = 0, LAST_UPDATED_DTTM = Getdate ()
             WHERE     TC_COMPANY_ID = vTCCompanyId
                   AND LANE_ID = vLaneId
                   AND LANE_DTL_SEQ = vLaneDtlSeqNew
                   AND LANE_DTL_STATUS = 1;
         END IF;
      END LOOP;

      CLOSE select_cursor;
   END COPY_CARRIERS;

   -- SR1 , 4R1 change, build92,  TT31182 [END]

   FUNCTION MERGE_DRAFT (
      vTCCompanyId     IN COMPANY.COMPANY_ID%TYPE,
      vUser            IN COMB_LANE_DTL.LAST_UPDATED_SOURCE%TYPE,
      vLaneId          IN COMB_LANE.LANE_ID%TYPE,
      vLaneDtlSeq      IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vLaneDtlSeqUpd   IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE)
      RETURN INT
   IS
      tempCount                INT;
      vLastUpdatedSource       COMB_LANE_DTL.LAST_UPDATED_SOURCE%TYPE;
      vLastUpdatedSourceType   COMB_LANE_DTL.LAST_UPDATED_SOURCE_TYPE%TYPE;
   BEGIN
      tempCount := 0;

      SELECT COUNT (*)
        INTO tempCount
        FROM COMB_LANE_DTL
       WHERE     TC_COMPANY_ID = vTCCompanyId
             AND LANE_ID = vLaneId
             AND LANE_DTL_SEQ = vLaneDtlSeqUpd
             AND IS_SAILING = 0
             AND IS_ROUTING = 0;

      IF (tempCount > 0)
      THEN
         DELETE_CHILD_ROW (vTCCompanyId, vLaneId, vLaneDtlSeqUpd);
         COPY_CHILD_ROW (vTCCompanyId,
                         vLaneId,
                         -1,
                         vLaneDtlSeq,
                         vLaneDtlSeqUpd,
                         1,
                         0,
                         0,vUser);

         FOR CUR_DTL_UPD
            IN (SELECT EFFECTIVE_DT,
                       EXPIRATION_DT,
                       CARRIER_ID,
                       MOT_ID,
                       SERVICE_LEVEL_ID,
                       SCNDR_CARRIER_ID,
                       PROTECTION_LEVEL_ID,
                       EQUIPMENT_ID,
                       IS_BUDGETED,
                       O_SHIP_VIA,
                       D_SHIP_VIA,
                       CUSTOM_TEXT1,
                       CUSTOM_TEXT2,
                       CUSTOM_TEXT3,
                       CUSTOM_TEXT4,
                       CUSTOM_TEXT5,
                       VOYAGE,
		       REJECT_FURTHER_SHIPMENTS,
		       CARRIER_REJECT_PERIOD 
                  FROM COMB_LANE_DTL
                 WHERE     TC_COMPANY_ID = vTCCompanyId
                       AND LANE_ID = vLaneId
                       AND LANE_DTL_SEQ = vLaneDtlSeq)
         LOOP
            IF (vUser IS NOT NULL)
            THEN
               vLastUpdatedSource := vUser;
               vLastUpdatedSourceType := 1;
            ELSE
               vLastUpdatedSource := 'OMS';
               vLastUpdatedSourceType := 3;
            END IF;

            UPDATE COMB_LANE_DTL
               SET LAST_UPDATED_SOURCE_TYPE = vLastUpdatedSourceType,
                   LAST_UPDATED_SOURCE = vLastUpdatedSource,
                   LAST_UPDATED_DTTM = Getdate (),
                   EFFECTIVE_DT = CUR_DTL_UPD.EFFECTIVE_DT,
                   EXPIRATION_DT = CUR_DTL_UPD.EXPIRATION_DT,
                   CARRIER_ID = CUR_DTL_UPD.CARRIER_ID,
                   MOT_ID = CUR_DTL_UPD.MOT_ID,
                   SERVICE_LEVEL_ID = CUR_DTL_UPD.SERVICE_LEVEL_ID,
                   SCNDR_CARRIER_ID = CUR_DTL_UPD.SCNDR_CARRIER_ID,
                   PROTECTION_LEVEL_ID = CUR_DTL_UPD.PROTECTION_LEVEL_ID,
                   EQUIPMENT_ID = CUR_DTL_UPD.EQUIPMENT_ID,
                   IS_BUDGETED = CUR_DTL_UPD.IS_BUDGETED,
                   O_SHIP_VIA = CUR_DTL_UPD.O_SHIP_VIA,
                   D_SHIP_VIA = CUR_DTL_UPD.D_SHIP_VIA,
                   CUSTOM_TEXT1 = CUR_DTL_UPD.CUSTOM_TEXT1,
                   CUSTOM_TEXT2 = CUR_DTL_UPD.CUSTOM_TEXT2,
                   CUSTOM_TEXT3 = CUR_DTL_UPD.CUSTOM_TEXT3,
                   CUSTOM_TEXT4 = CUR_DTL_UPD.CUSTOM_TEXT4,
                   CUSTOM_TEXT5 = CUR_DTL_UPD.CUSTOM_TEXT5,
		   VOYAGE = CUR_DTL_UPD.VOYAGE,
		   REJECT_FURTHER_SHIPMENTS = CUR_DTL_UPD.REJECT_FURTHER_SHIPMENTS,
                   CARRIER_REJECT_PERIOD = CUR_DTL_UPD.CARRIER_REJECT_PERIOD 
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND lane_dtl_seq = vLaneDtlSeqUpd;
         END LOOP;
      END IF;

      RETURN tempCount;
   END MERGE_DRAFT;

   PROCEDURE CREATE_NEW_ROW (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vUser           IN COMB_LANE_DTL.LAST_UPDATED_SOURCE%TYPE,
      vLaneId         IN COMB_LANE.LANE_ID%TYPE,
      vRatingDtlSeq   IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vEffectiveDT    IN COMB_LANE_DTL.EFFECTIVE_DT%TYPE,
      vExpirationDT   IN COMB_LANE_DTL.EXPIRATION_DT%TYPE)
   IS
      vRatingDtlSeqNew         COMB_LANE_DTL.LANE_DTL_SEQ%TYPE;
      vLastUpdatedSource       COMB_LANE_DTL.LAST_UPDATED_SOURCE%TYPE;
      vLastUpdatedSourceType   COMB_LANE_DTL.LAST_UPDATED_SOURCE_TYPE%TYPE;
   BEGIN
      SELECT MAX (LANE_DTL_SEQ) + 1
        INTO vRatingDtlSeqNew
        FROM COMB_LANE_DTL
       WHERE TC_COMPANY_ID = vTCCompanyID AND LANE_ID = vLaneID;

      -- sysco merge
      IF (vUser IS NOT NULL)
      THEN
         vLastUpdatedSource := vUser;
         vLastUpdatedSourceType := 1;
      ELSE
         vLastUpdatedSource := 'OMS';
         vLastUpdatedSourceType := 3;
      END IF;

      INSERT INTO COMB_LANE_DTL (TC_COMPANY_ID,
                                 LANE_ID,
                                 LANE_DTL_SEQ,
                                 EFFECTIVE_DT,
                                 EXPIRATION_DT,
                                 CARRIER_ID,
                                 IS_BUDGETED,
                                 MOT_ID,
                                 EQUIPMENT_ID,
                                 SERVICE_LEVEL_ID,
                                 PROTECTION_LEVEL_Id,
                                 SCNDR_CARRIER_ID,
                                 O_SHIP_VIA,
                                 D_SHIP_VIA,
                                 CONTRACT_NUMBER,
                                 CUSTOM_TEXT1,
                                 CUSTOM_TEXT2,
                                 CUSTOM_TEXT3,
                                 CUSTOM_TEXT4,
                                 CUSTOM_TEXT5,
								 VOYAGE,
                                 LAST_UPDATED_SOURCE_TYPE,
                                 LAST_UPDATED_SOURCE,
                                 LAST_UPDATED_DTTM,
                                 LANE_DTL_STATUS,
                                 IS_RATING,
                                 MASTER_LANE_ID,
                                 REP_TP_FLAG,
				 REJECT_FURTHER_SHIPMENTS,
			         CARRIER_REJECT_PERIOD)
         SELECT TC_COMPANY_ID,
                LANE_ID,
                vRatingDtlSeqNew,
                vEffectiveDT,
                vExpirationDT,
                CARRIER_ID,
                IS_BUDGETED,
                MOT_ID,
                EQUIPMENT_ID,
                SERVICE_LEVEL_ID,
                PROTECTION_LEVEL_ID,
                SCNDR_CARRIER_ID,
                O_SHIP_VIA,
                D_SHIP_VIA,
                CONTRACT_NUMBER,
                CUSTOM_TEXT1,
                CUSTOM_TEXT2,
                CUSTOM_TEXT3,
                CUSTOM_TEXT4,
                CUSTOM_TEXT5,
				VOYAGE,
                vLastUpdatedSourceType,
                vLastUpdatedSource,
                Getdate (),
                0,
                1,
                MASTER_LANE_ID,
                1,
		REJECT_FURTHER_SHIPMENTS,
                CARRIER_REJECT_PERIOD 
           FROM COMB_LANE_DTL
          WHERE     TC_COMPANY_ID = vTCCompanyId
                AND LANE_ID = vLaneId
                AND LANE_DTL_SEQ = vRatingDtlSeq;

      COPY_CHILD_ROW_NEW (vTCCompanyId,
                      vLaneId,
                      vRatingDtlSeq,
                      vRatingDtlSeqNew,
                      vRatingDtlSeqNew,
                      1,
                      0,
                      0,vLastUpdatedSource);
   END CREATE_NEW_ROW;

  /* PROCEDURE REPLICATE_ROW (
      vTCCompanyId     IN COMPANY.COMPANY_ID%TYPE,
      vUser            IN COMB_LANE_DTL.LAST_UPDATED_SOURCE%TYPE,
      vLaneId          IN COMB_LANE.LANE_ID%TYPE,
      vOverlappedSeq   IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vRatingDtlSeq    IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vEffectiveDT     IN COMB_LANE_DTL.EFFECTIVE_DT%TYPE,
      vExpirationDT    IN COMB_LANE_DTL.EXPIRATION_DT%TYPE,
      vIsRating        IN COMB_LANE_DTL.IS_RATING%TYPE,
      vIsRouting       IN COMB_LANE_DTL.IS_ROUTING%TYPE,
      vIsSailing       IN COMB_LANE_DTL.IS_SAILING%TYPE)
   IS
      vRatingDtlSeqNew         COMB_LANE_DTL.LANE_DTL_SEQ%TYPE;
      vRatingDtlSeqTemp        COMB_LANE_DTL.LANE_DTL_SEQ%TYPE;
      vIsBudgetedTemp          COMB_LANE_DTL.IS_BUDGETED%TYPE;
      vLastUpdatedSource       COMB_LANE_DTL.LAST_UPDATED_SOURCE%TYPE;
      vLastUpdatedSourceType   COMB_LANE_DTL.LAST_UPDATED_SOURCE_TYPE%TYPE;
   BEGIN
      SELECT MAX (LANE_DTL_SEQ) + 1
        INTO vRatingDtlSeqNew
        FROM COMB_LANE_DTL
       WHERE TC_COMPANY_ID = vTCCompanyID AND LANE_ID = vLaneID;

      IF (vOverlappedSeq = -1)
      THEN
         vRatingDtlSeqTemp := vRatingDtlSeq;
      ELSE
         vRatingDtlSeqTemp := vOverlappedSeq;
      END IF;

      SELECT IS_BUDGETED
        INTO vIsBudgetedTemp
        FROM COMB_LANE_DTL
       WHERE     TC_COMPANY_ID = vTCCompanyId
             AND LANE_ID = vLaneId
             AND LANE_DTL_SEQ = vRatingDtlSeqTemp;

      -- syco merge
      IF (vUser IS NOT NULL)
      THEN
         vLastUpdatedSource := vUser;
         vLastUpdatedSourceType := 1;
      ELSE
         vLastUpdatedSource := 'OMS';
         vLastUpdatedSourceType := 3;
      END IF;

      INSERT INTO COMB_LANE_DTL (TC_COMPANY_ID,
                                 LANE_ID,
                                 LANE_DTL_SEQ,
                                 LANE_DTL_STATUS,
                                 IS_RATING,
                                 IS_ROUTING,
                                 IS_BUDGETED,
                                 IS_PREFERRED,
                                 REP_TP_FLAG,
                                 LAST_UPDATED_SOURCE_TYPE,
                                 LAST_UPDATED_SOURCE,
                                 LAST_UPDATED_DTTM,
                                 CARRIER_ID,
                                 MOT_ID,
                                 EQUIPMENT_ID,
                                 SERVICE_LEVEL_ID,
                                 SCNDR_CARRIER_ID,
                                 O_SHIP_VIA,
                                 D_SHIP_VIA,
                                 CONTRACT_NUMBER,
                                 CUSTOM_TEXT1,
                                 CUSTOM_TEXT2,
                                 CUSTOM_TEXT3,
                                 CUSTOM_TEXT4,
                                 CUSTOM_TEXT5,
								 VOYAGE,
                                 PROTECTION_LEVEL_ID,
                                 EFFECTIVE_DT,
                                 EXPIRATION_DT,
                                 TIER_ID,
                                 RANK,
                                 WEEKLY_CAPACITY,
                                 DAILY_CAPACITY,
                                 CAPACITY_SUN,
                                 CAPACITY_MON,
                                 CAPACITY_TUE,
                                 CAPACITY_WED,
                                 CAPACITY_THU,
                                 CAPACITY_FRI,
                                 CAPACITY_SAT,
                                 WEEKLY_COMMITMENT,
                                 DAILY_COMMITMENT,
                                 COMMITMENT_SUN,
                                 COMMITMENT_MON,
                                 COMMITMENT_TUE,
                                 COMMITMENT_WED,
                                 COMMITMENT_THU,
                                 COMMITMENT_FRI,
                                 COMMITMENT_SAT,
                                 WEEKLY_COMMIT_PCT,
                                 DAILY_COMMIT_PCT,
                                 COMMIT_PCT_SUN,
                                 COMMIT_PCT_MON,
                                 COMMIT_PCT_TUE,
                                 COMMIT_PCT_WED,
                                 COMMIT_PCT_THU,
                                 COMMIT_PCT_FRI,
                                 COMMIT_PCT_SAT,
                                 FIXED_TRANSIT_STANDARD_UOM,
                                 FIXED_TRANSIT_VALUE,
                                 IS_SAILING,
                                 SAILING_FREQUENCY_TYPE,
                                 SAILING_SCHEDULE_NAME,
                                 MASTER_LANE_ID,
                                 TT_TOLERANCE_FACTOR,
                                 SP_LPN_TYPE,
                                 SP_MIN_LPN_COUNT,
                                 SP_MAX_LPN_COUNT,
                                 SP_MIN_WEIGHT,
                                 SP_MAX_WEIGHT,
                                 SP_MIN_VOLUME,
                                 SP_MAX_VOLUME,
                                 SP_MIN_LINEAR_FEET,
                                 SP_MAX_LINEAR_FEET,
                                 SP_MIN_MONETARY_VALUE,
                                 SP_MAX_MONETARY_VALUE,
                                 SP_CURRENCY_CODE,
                                 HAS_SHIPPING_PARAM,
                                 CUBING_INDICATOR,
                                 OVERRIDE_CODE,
                                 PREFERENCE_BONUS_VALUE,
				 REJECT_FURTHER_SHIPMENTS,
                                 CARRIER_REJECT_PERIOD)
         SELECT TC_COMPANY_ID,
                LANE_ID,
                vRatingDtlSeqNew,
                0,
                vIsRating,
                IS_ROUTING,
                vIsBudgetedTemp,
                IS_PREFERRED,
                REP_TP_FLAG,
                vLastUpdatedSourceType,
                vLastUpdatedSource,
                Getdate (),
                CARRIER_ID,
                MOT_ID,
                EQUIPMENT_ID,
                SERVICE_LEVEL_ID,
                SCNDR_CARRIER_ID,
                O_SHIP_VIA,
                D_SHIP_VIA,
                CONTRACT_NUMBER,
                CUSTOM_TEXT1,
                CUSTOM_TEXT2,
                CUSTOM_TEXT3,
                CUSTOM_TEXT4,
                CUSTOM_TEXT5,
				VOYAGE,
                PROTECTION_LEVEL_ID,
                vEffectiveDT,
                vExpirationDT,
                TIER_ID,
                RANK,
                WEEKLY_CAPACITY,
                DAILY_CAPACITY,
                CAPACITY_SUN,
                CAPACITY_MON,
                CAPACITY_TUE,
                CAPACITY_WED,
                CAPACITY_THU,
                CAPACITY_FRI,
                CAPACITY_SAT,
                WEEKLY_COMMITMENT,
                DAILY_COMMITMENT,
                COMMITMENT_SUN,
                COMMITMENT_MON,
                COMMITMENT_TUE,
                COMMITMENT_WED,
                COMMITMENT_THU,
                COMMITMENT_FRI,
                COMMITMENT_SAT,
                WEEKLY_COMMIT_PCT,
                DAILY_COMMIT_PCT,
                COMMIT_PCT_SUN,
                COMMIT_PCT_MON,
                COMMIT_PCT_TUE,
                COMMIT_PCT_WED,
                COMMIT_PCT_THU,
                COMMIT_PCT_FRI,
                COMMIT_PCT_SAT,
                FIXED_TRANSIT_STANDARD_UOM,
                FIXED_TRANSIT_VALUE,
                IS_SAILING,
                SAILING_FREQUENCY_TYPE,
                SAILING_SCHEDULE_NAME,
                MASTER_LANE_ID,
                TT_TOLERANCE_FACTOR,
                SP_LPN_TYPE,
                SP_MIN_LPN_COUNT,
                SP_MAX_LPN_COUNT,
                SP_MIN_WEIGHT,
                SP_MAX_WEIGHT,
                SP_MIN_VOLUME,
                SP_MAX_VOLUME,
                SP_MIN_LINEAR_FEET,
                SP_MAX_LINEAR_FEET,
                SP_MIN_MONETARY_VALUE,
                SP_MAX_MONETARY_VALUE,
                SP_CURRENCY_CODE,
                HAS_SHIPPING_PARAM,
                CUBING_INDICATOR,
                OVERRIDE_CODE,
                PREFERENCE_BONUS_VALUE,
		REJECT_FURTHER_SHIPMENTS,
                CARRIER_REJECT_PERIOD 
           FROM COMB_LANE_DTL
          WHERE     TC_COMPANY_ID = vTCCompanyId
                AND LANE_ID = vLaneId
                AND LANE_DTL_SEQ = vRatingDtlSeq;

      COPY_CHILD_ROW (vTCCompanyId,
                      vLaneId,
                      vOverlappedSeq,
                      vRatingDtlSeq,
                      vRatingDtlSeqNew,
                      vIsRating,
                      vIsRouting,
                      vIsSailing,vLastUpdatedSource);
   END REPLICATE_ROW;*/
   
      PROCEDURE REPLICATE_ROW (
      vTCCompanyId     IN COMPANY.COMPANY_ID%TYPE,
      vUser            IN COMB_LANE_DTL.LAST_UPDATED_SOURCE%TYPE,
      vLaneId          IN COMB_LANE.LANE_ID%TYPE,
      vOverlappedSeq   IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vRatingDtlSeq    IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vEffectiveDT     IN COMB_LANE_DTL.EFFECTIVE_DT%TYPE,
      vExpirationDT    IN COMB_LANE_DTL.EXPIRATION_DT%TYPE,
      vIsRating        IN COMB_LANE_DTL.IS_RATING%TYPE,
      vIsRouting       IN COMB_LANE_DTL.IS_ROUTING%TYPE,
      vIsSailing       IN COMB_LANE_DTL.IS_SAILING%TYPE,
	  TEMP_SEQ_NUM             OUT NUMBER)
   AS
   BEGIN
      TEMP_SEQ_NUM :=
        REPLICATE_ROW (vTCCompanyId,
                              vUser,
                              vLaneId,
                              vOverlappedSeq,
                              vRatingDtlSeq,
                              vEffectiveDT,
                              vExpirationDT,
                              vIsRating,
                              vIsRouting,
                              vIsSailing);
   END REPLICATE_ROW;
   
   FUNCTION REPLICATE_ROW (
      vTCCompanyId     IN COMPANY.COMPANY_ID%TYPE,
      vUser            IN COMB_LANE_DTL.LAST_UPDATED_SOURCE%TYPE,
      vLaneId          IN COMB_LANE.LANE_ID%TYPE,
      vOverlappedSeq   IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vRatingDtlSeq    IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vEffectiveDT     IN COMB_LANE_DTL.EFFECTIVE_DT%TYPE,
      vExpirationDT    IN COMB_LANE_DTL.EXPIRATION_DT%TYPE,
      vIsRating        IN COMB_LANE_DTL.IS_RATING%TYPE,
      vIsRouting       IN COMB_LANE_DTL.IS_ROUTING%TYPE,
      vIsSailing       IN COMB_LANE_DTL.IS_SAILING%TYPE)
	  RETURN INT
   IS
      vRatingDtlSeqNew         INT;
      vRatingDtlSeqTemp        COMB_LANE_DTL.LANE_DTL_SEQ%TYPE;
      vIsBudgetedTemp          COMB_LANE_DTL.IS_BUDGETED%TYPE;
      vLastUpdatedSource       COMB_LANE_DTL.LAST_UPDATED_SOURCE%TYPE;
      vLastUpdatedSourceType   COMB_LANE_DTL.LAST_UPDATED_SOURCE_TYPE%TYPE;
	  
   BEGIN
      SELECT MAX (LANE_DTL_SEQ) + 1
        INTO vRatingDtlSeqNew
        FROM COMB_LANE_DTL
       WHERE TC_COMPANY_ID = vTCCompanyID AND LANE_ID = vLaneID;
	  
      IF (vOverlappedSeq = -1)
      THEN
         vRatingDtlSeqTemp := vRatingDtlSeq;
      ELSE
         vRatingDtlSeqTemp := vOverlappedSeq;
      END IF;

      SELECT IS_BUDGETED
        INTO vIsBudgetedTemp
        FROM COMB_LANE_DTL
       WHERE     TC_COMPANY_ID = vTCCompanyId
             AND LANE_ID = vLaneId
             AND LANE_DTL_SEQ = vRatingDtlSeqTemp;

      -- syco merge
      IF (vUser IS NOT NULL)
      THEN
         vLastUpdatedSource := vUser;
         vLastUpdatedSourceType := 1;
      ELSE
         vLastUpdatedSource := 'OMS';
         vLastUpdatedSourceType := 3;
      END IF;

      INSERT INTO COMB_LANE_DTL (TC_COMPANY_ID,
                                 LANE_ID,
                                 LANE_DTL_SEQ,
                                 LANE_DTL_STATUS,
                                 IS_RATING,
                                 IS_ROUTING,
                                 IS_BUDGETED,
                                 IS_PREFERRED,
                                 REP_TP_FLAG,
                                 LAST_UPDATED_SOURCE_TYPE,
                                 LAST_UPDATED_SOURCE,
                                 LAST_UPDATED_DTTM,
                                 CARRIER_ID,
                                 MOT_ID,
                                 EQUIPMENT_ID,
                                 SERVICE_LEVEL_ID,
                                 SCNDR_CARRIER_ID,
                                 O_SHIP_VIA,
                                 D_SHIP_VIA,
                                 CONTRACT_NUMBER,
                                 CUSTOM_TEXT1,
                                 CUSTOM_TEXT2,
                                 CUSTOM_TEXT3,
                                 CUSTOM_TEXT4,
                                 CUSTOM_TEXT5,
								 VOYAGE,
                                 PROTECTION_LEVEL_ID,
                                 EFFECTIVE_DT,
                                 EXPIRATION_DT,
                                 TIER_ID,
                                 RANK,
                                 WEEKLY_CAPACITY,
                                 DAILY_CAPACITY,
                                 CAPACITY_SUN,
                                 CAPACITY_MON,
                                 CAPACITY_TUE,
                                 CAPACITY_WED,
                                 CAPACITY_THU,
                                 CAPACITY_FRI,
                                 CAPACITY_SAT,
                                 WEEKLY_COMMITMENT,
                                 DAILY_COMMITMENT,
                                 COMMITMENT_SUN,
                                 COMMITMENT_MON,
                                 COMMITMENT_TUE,
                                 COMMITMENT_WED,
                                 COMMITMENT_THU,
                                 COMMITMENT_FRI,
                                 COMMITMENT_SAT,
                                 WEEKLY_COMMIT_PCT,
                                 DAILY_COMMIT_PCT,
                                 COMMIT_PCT_SUN,
                                 COMMIT_PCT_MON,
                                 COMMIT_PCT_TUE,
                                 COMMIT_PCT_WED,
                                 COMMIT_PCT_THU,
                                 COMMIT_PCT_FRI,
                                 COMMIT_PCT_SAT,
                                 FIXED_TRANSIT_STANDARD_UOM,
                                 FIXED_TRANSIT_VALUE,
                                 IS_SAILING,
                                 SAILING_FREQUENCY_TYPE,
                                 SAILING_SCHEDULE_NAME,
                                 MASTER_LANE_ID,
                                 TT_TOLERANCE_FACTOR,
                                 SP_LPN_TYPE,
                                 SP_MIN_LPN_COUNT,
                                 SP_MAX_LPN_COUNT,
                                 SP_MIN_WEIGHT,
                                 SP_MAX_WEIGHT,
                                 SP_MIN_VOLUME,
                                 SP_MAX_VOLUME,
                                 SP_MIN_LINEAR_FEET,
                                 SP_MAX_LINEAR_FEET,
                                 SP_MIN_MONETARY_VALUE,
                                 SP_MAX_MONETARY_VALUE,
                                 SP_CURRENCY_CODE,
                                 HAS_SHIPPING_PARAM,
                                 CUBING_INDICATOR,
                                 OVERRIDE_CODE,
                                 PREFERENCE_BONUS_VALUE,
				 REJECT_FURTHER_SHIPMENTS,
                                 CARRIER_REJECT_PERIOD)
         SELECT TC_COMPANY_ID,
                LANE_ID,
                vRatingDtlSeqNew,
                0,
                vIsRating,
                IS_ROUTING,
                vIsBudgetedTemp,
                IS_PREFERRED,
                REP_TP_FLAG,
                vLastUpdatedSourceType,
                vLastUpdatedSource,
                Getdate (),
                CARRIER_ID,
                MOT_ID,
                EQUIPMENT_ID,
                SERVICE_LEVEL_ID,
                SCNDR_CARRIER_ID,
                O_SHIP_VIA,
                D_SHIP_VIA,
                CONTRACT_NUMBER,
                CUSTOM_TEXT1,
                CUSTOM_TEXT2,
                CUSTOM_TEXT3,
                CUSTOM_TEXT4,
                CUSTOM_TEXT5,
				VOYAGE,
                PROTECTION_LEVEL_ID,
                vEffectiveDT,
                vExpirationDT,
                TIER_ID,
                RANK,
                WEEKLY_CAPACITY,
                DAILY_CAPACITY,
                CAPACITY_SUN,
                CAPACITY_MON,
                CAPACITY_TUE,
                CAPACITY_WED,
                CAPACITY_THU,
                CAPACITY_FRI,
                CAPACITY_SAT,
                WEEKLY_COMMITMENT,
                DAILY_COMMITMENT,
                COMMITMENT_SUN,
                COMMITMENT_MON,
                COMMITMENT_TUE,
                COMMITMENT_WED,
                COMMITMENT_THU,
                COMMITMENT_FRI,
                COMMITMENT_SAT,
                WEEKLY_COMMIT_PCT,
                DAILY_COMMIT_PCT,
                COMMIT_PCT_SUN,
                COMMIT_PCT_MON,
                COMMIT_PCT_TUE,
                COMMIT_PCT_WED,
                COMMIT_PCT_THU,
                COMMIT_PCT_FRI,
                COMMIT_PCT_SAT,
                FIXED_TRANSIT_STANDARD_UOM,
                FIXED_TRANSIT_VALUE,
                IS_SAILING,
                SAILING_FREQUENCY_TYPE,
                SAILING_SCHEDULE_NAME,
                MASTER_LANE_ID,
                TT_TOLERANCE_FACTOR,
                SP_LPN_TYPE,
                SP_MIN_LPN_COUNT,
                SP_MAX_LPN_COUNT,
                SP_MIN_WEIGHT,
                SP_MAX_WEIGHT,
                SP_MIN_VOLUME,
                SP_MAX_VOLUME,
                SP_MIN_LINEAR_FEET,
                SP_MAX_LINEAR_FEET,
                SP_MIN_MONETARY_VALUE,
                SP_MAX_MONETARY_VALUE,
                SP_CURRENCY_CODE,
                HAS_SHIPPING_PARAM,
                CUBING_INDICATOR,
                OVERRIDE_CODE,
                PREFERENCE_BONUS_VALUE,
		REJECT_FURTHER_SHIPMENTS,
                CARRIER_REJECT_PERIOD 
           FROM COMB_LANE_DTL
          WHERE     TC_COMPANY_ID = vTCCompanyId
                AND LANE_ID = vLaneId
                AND LANE_DTL_SEQ = vRatingDtlSeq;

      COPY_CHILD_ROW (vTCCompanyId,
                      vLaneId,
                      vOverlappedSeq,
                      vRatingDtlSeq,
                      vRatingDtlSeqNew,
                      vIsRating,
                      vIsRouting,
                      vIsSailing,vLastUpdatedSource);
					  
		 RETURN vRatingDtlSeqNew;			  
   END REPLICATE_ROW;

   PROCEDURE DELETE_CHILD_ROW (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vLaneId         IN COMB_LANE.LANE_ID%TYPE,
      vRatingDtlSeq   IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE)
   IS
      vRatingDtlSeqNew   COMB_LANE_DTL.LANE_DTL_SEQ%TYPE;
   BEGIN
      DELETE FROM RATING_LANE_DTL_RATE
            WHERE     tc_company_id = vTCCompanyId
                  AND lane_id = vLaneId
                  AND rating_lane_dtl_seq = vRatingDtlSeq;

      DELETE FROM LANE_ACC_EXCLUDED_LIST
            WHERE LANE_ACC_ID IN
                     (SELECT la.lane_accessorial_id
                        FROM LANE_ACCESSORIAL la
                       WHERE la.LANE_ID = vLaneId
                             AND LA.RATING_LANE_DTL_SEQ = vRatingDtlSeq);

      DELETE FROM LANE_ACCESSORIAL
            WHERE     tc_company_id = vTCCompanyId
                  AND lane_id = vLaneId
                  AND rating_lane_dtl_seq = vRatingDtlSeq;
   END DELETE_CHILD_ROW;

   PROCEDURE UPDATE_RATING_ACC_DATA (
	  vTCCompanyId       IN COMPANY.COMPANY_ID%TYPE,
      vLaneId            IN COMB_LANE.LANE_ID%TYPE,
      vRatingDtlSeqTemp     IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vRatingDtlSeqNew   IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE)
   AS
   BEGIN
		update LANE_ACCESSORIAL RATING_LANE_DTL_RATE set MINIMUM_SIZE = (select new_value FROM RATING_EVENT
				WHERE     rating_lane_id = vLaneId
			   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
			   AND is_pending_auth = 1
			   AND LANE_ACCESSORIAL_ID IS NOT NULL and field_name = 'Min Qty')
		 WHERE     tc_company_id = vTCCompanyId
			   AND lane_id = vLaneId
			   AND rating_lane_dtl_seq = vRatingDtlSeqNew;	
			   
		update LANE_ACCESSORIAL RATING_LANE_DTL_RATE set MAXIMUM_SIZE = (select new_value FROM RATING_EVENT
				WHERE     rating_lane_id = vLaneId
			   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
			   AND is_pending_auth = 1
			   AND LANE_ACCESSORIAL_ID IS NOT NULL and field_name = 'Max Qty')
		 WHERE     tc_company_id = vTCCompanyId
			   AND lane_id = vLaneId
			   AND rating_lane_dtl_seq = vRatingDtlSeqNew;
			   
		update LANE_ACCESSORIAL RATING_LANE_DTL_RATE set RATE = (select new_value FROM RATING_EVENT
				WHERE     rating_lane_id = vLaneId
			   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
			   AND is_pending_auth = 1
			   AND LANE_ACCESSORIAL_ID IS NOT NULL and field_name = 'Rate')
		 WHERE     tc_company_id = vTCCompanyId
			   AND lane_id = vLaneId
			   AND rating_lane_dtl_seq = vRatingDtlSeqNew;	
			   
		update LANE_ACCESSORIAL RATING_LANE_DTL_RATE set MINIMUM_RATE = (select new_value FROM RATING_EVENT
				WHERE     rating_lane_id = vLaneId
			   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
			   AND is_pending_auth = 1
			   AND LANE_ACCESSORIAL_ID IS NOT NULL and field_name = 'Min. Rate')
		 WHERE     tc_company_id = vTCCompanyId
			   AND lane_id = vLaneId
			   AND rating_lane_dtl_seq = vRatingDtlSeqNew;
		update LANE_ACCESSORIAL RATING_LANE_DTL_RATE set CURRENCY_CODE = (select max(new_value) FROM RATING_EVENT
				WHERE     rating_lane_id = vLaneId
			   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
			   AND LANE_ACCESSORIAL_ID IS NOT NULL and field_name in( 'Currency', 'CURRENCY_CODE'))
		 WHERE     tc_company_id = vTCCompanyId
			   AND lane_id = vLaneId
			   AND rating_lane_dtl_seq = vRatingDtlSeqNew;	
			   
		update LANE_ACCESSORIAL RATING_LANE_DTL_RATE set IS_AUTO_APPROVE = COALESCE((select new_value FROM RATING_EVENT
				WHERE     rating_lane_id = vLaneId
			   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
			   AND is_pending_auth = 1
			   AND LANE_ACCESSORIAL_ID IS NOT NULL and field_name = 'Is Auto Approve'), '0')
		 WHERE     tc_company_id = vTCCompanyId
			   AND lane_id = vLaneId
			   AND rating_lane_dtl_seq = vRatingDtlSeqNew;

		update LANE_ACCESSORIAL RATING_LANE_DTL_RATE set PAYEE_CARRIER_ID = (select new_value FROM RATING_EVENT
				WHERE     rating_lane_id = vLaneId
			   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
			   AND is_pending_auth = 1
			   AND LANE_ACCESSORIAL_ID IS NOT NULL and field_name = 'Payee')
		 WHERE     tc_company_id = vTCCompanyId
			   AND lane_id = vLaneId
			   AND rating_lane_dtl_seq = vRatingDtlSeqNew;	
			   
		update LANE_ACCESSORIAL RATING_LANE_DTL_RATE set size_uom_id = (select new_value FROM RATING_EVENT
				WHERE     rating_lane_id = vLaneId
			   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
			   AND is_pending_auth = 1
			   AND LANE_ACCESSORIAL_ID IS NOT NULL and field_name = 'UOM')
		 WHERE     tc_company_id = vTCCompanyId
			   AND lane_id = vLaneId
			   AND rating_lane_dtl_seq = vRatingDtlSeqNew;			   
   
   END UPDATE_RATING_ACC_DATA;
   
   PROCEDURE UPDATE_RATING_RATE_DATA (
	  vTCCompanyId       IN COMPANY.COMPANY_ID%TYPE,
      vLaneId            IN COMB_LANE.LANE_ID%TYPE,
      vRatingDtlSeqTemp     IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vRatingDtlSeqNew   IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE)
   AS
   BEGIN
			update RATING_LANE_DTL_RATE set RATE_UOM = (select new_value FROM RATING_EVENT
					WHERE     rating_lane_id = vLaneId
                   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NOT NULL and LANE_ACCESSORIAL_ID is null and field_name = 'Rate UOM')
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew;	
				   
			update RATING_LANE_DTL_RATE set TARIFF_ID = (select new_value FROM RATING_EVENT
					WHERE     rating_lane_id = vLaneId
                   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NOT NULL and LANE_ACCESSORIAL_ID is null and field_name = 'Tariff')
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew;				   
	
			update RATING_LANE_DTL_RATE set MINIMUM_SIZE = (select new_value FROM RATING_EVENT
					WHERE     rating_lane_id = vLaneId
                   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NOT NULL and LANE_ACCESSORIAL_ID is null and field_name = 'Min Qty')
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew;
				  
			update RATING_LANE_DTL_RATE set MAXIMUM_SIZE = (select new_value FROM RATING_EVENT
					WHERE     rating_lane_id = vLaneId
                   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NOT NULL and LANE_ACCESSORIAL_ID is null and field_name = 'Max Qty')
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew;	
				   
			update RATING_LANE_DTL_RATE set SIZE_UOM_ID = (select new_value FROM RATING_EVENT
					WHERE     rating_lane_id = vLaneId
                   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NOT NULL and LANE_ACCESSORIAL_ID is null and field_name = 'Qty Unit')
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew;
			
			update RATING_LANE_DTL_RATE set MIN_DISTANCE = (select new_value FROM RATING_EVENT
					WHERE     rating_lane_id = vLaneId
                   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NOT NULL and LANE_ACCESSORIAL_ID is null and field_name = 'Min Distance')
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew;
				  
			update RATING_LANE_DTL_RATE set MAX_DISTANCE = (select new_value FROM RATING_EVENT
					WHERE     rating_lane_id = vLaneId
                   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NOT NULL and LANE_ACCESSORIAL_ID is null and field_name = 'Max Distance')
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew;	
				   
			update RATING_LANE_DTL_RATE set DISTANCE_UOM = (select new_value FROM RATING_EVENT
					WHERE     rating_lane_id = vLaneId
                   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NOT NULL and LANE_ACCESSORIAL_ID is null and field_name = 'Distance Unit')
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew;				   
	
			update RATING_LANE_DTL_RATE set MIN_COMMODITY = (select new_value FROM RATING_EVENT
					WHERE     rating_lane_id = vLaneId
                   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NOT NULL and LANE_ACCESSORIAL_ID is null and field_name = 'Min Commodity')
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew;
				  
			update RATING_LANE_DTL_RATE set MAX_COMMODITY = (select new_value FROM RATING_EVENT
					WHERE     rating_lane_id = vLaneId
                   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NOT NULL and LANE_ACCESSORIAL_ID is null and field_name = 'Max Commodity')
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew;	
			
			update RATING_LANE_DTL_RATE set rate = (select new_value FROM RATING_EVENT
					WHERE     rating_lane_id = vLaneId
                   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NOT NULL and LANE_ACCESSORIAL_ID is null and field_name = 'Rate')
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew;
				   
			update RATING_LANE_DTL_RATE set MINIMUM_RATE = (select new_value FROM RATING_EVENT
					WHERE     rating_lane_id = vLaneId
                   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NOT NULL and LANE_ACCESSORIAL_ID is null and field_name = 'Min. Rate')
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew;			
   
			update RATING_LANE_DTL_RATE set CURRENCY_CODE = (select new_value FROM RATING_EVENT
					WHERE     rating_lane_id = vLaneId
                   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NOT NULL and LANE_ACCESSORIAL_ID is null and field_name = 'Currency')
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew;
				  
			update RATING_LANE_DTL_RATE set SUPPORTS_MSTL = (select new_value FROM RATING_EVENT
					WHERE     rating_lane_id = vLaneId
                   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NOT NULL and LANE_ACCESSORIAL_ID is null and field_name = 'Supports MSTL')
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew;	
				   
			update RATING_LANE_DTL_RATE set HAS_BH = (select new_value FROM RATING_EVENT
					WHERE     rating_lane_id = vLaneId
                   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NOT NULL and LANE_ACCESSORIAL_ID is null and field_name = 'Has Backhaul')
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew;				   
	
			update RATING_LANE_DTL_RATE set IS_SURGE_RATE = NVL((select new_value FROM RATING_EVENT
					WHERE     rating_lane_id = vLaneId
                   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NOT NULL and LANE_ACCESSORIAL_ID is null and field_name = 'Surge Rate'),0)
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew;
				  
			update RATING_LANE_DTL_RATE set minimum_rate_type = (select new_value FROM RATING_EVENT
					WHERE     rating_lane_id = vLaneId
                   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NOT NULL and LANE_ACCESSORIAL_ID is null and field_name = 'Min. Rate Type')
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew;	
				   
			update RATING_LANE_DTL_RATE set base_rate = (select new_value FROM RATING_EVENT
					WHERE     rating_lane_id = vLaneId
                   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NOT NULL and LANE_ACCESSORIAL_ID is null and field_name = 'Base Rate')
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew;
				   
			update RATING_LANE_DTL_RATE set maximum_rate = (select new_value FROM RATING_EVENT
					WHERE     rating_lane_id = vLaneId
                   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NOT NULL and LANE_ACCESSORIAL_ID is null and field_name = 'Max. Rate')
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew;				   
	
			update RATING_LANE_DTL_RATE set customer_min_charge = (select new_value FROM RATING_EVENT
					WHERE     rating_lane_id = vLaneId
                   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NOT NULL and LANE_ACCESSORIAL_ID is null and field_name = 'Cust. Min. Charge')
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew;
				  
			update RATING_LANE_DTL_RATE set customer_base_rate = (select new_value FROM RATING_EVENT
					WHERE     rating_lane_id = vLaneId
                   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NOT NULL and LANE_ACCESSORIAL_ID is null and field_name = 'Cust. Base Rate')
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew;	
				   
			update RATING_LANE_DTL_RATE set customer_surcharge_rate = (select new_value FROM RATING_EVENT
					WHERE     rating_lane_id = vLaneId
                   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NOT NULL and LANE_ACCESSORIAL_ID is null and field_name = 'Cust. Surcharge Rate')
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew;
				   
   END UPDATE_RATING_RATE_DATA;
   
   PROCEDURE COPY_CHILD_ROW_NEW (
      vTCCompanyId       IN COMPANY.COMPANY_ID%TYPE,
      vLaneId            IN COMB_LANE.LANE_ID%TYPE,
      vOverlappedSeq     IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vRatingDtlSeq      IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vRatingDtlSeqNew   IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vIsRating          IN COMB_LANE_DTL.IS_RATING%TYPE,
      vIsRouting         IN COMB_LANE_DTL.IS_ROUTING%TYPE,
      vIsSailing         IN COMB_LANE_DTL.IS_SAILING%TYPE,
	  vuser              IN   comb_lane_dtl.last_updated_source%TYPE)
   AS
      vRatingDtlSeqTemp        COMB_LANE_DTL.LANE_DTL_SEQ%TYPE;
      week_day_sql             VARCHAR2 (2000);
      vpendingauth             VARCHAR2 (20);
      week_day_cursor          ref_curtype;
      vWeekId                  SAILING_SCHDL_BY_WEEK.SAIL_SCHED_BY_WEEK_ID%TYPE;
      rldCount                 NUMBER;
      vcnt                     NUMBER;
      v_lane_acces_id_seq      lane_accessorial.lane_accessorial_id%TYPE;

      -- MACR00617605 starts
      vRateTemp                RATING_LANE_DTL_RATE.RATE%TYPE;
      -- MACR00617605 ends

     
      vlastupdatedsource       comb_lane_dtl.last_updated_source%TYPE;
      vlastupdatedsourcetype   comb_lane_dtl.last_updated_source_type%TYPE;

      ----------------------------------------------------
      CURSOR T1Cursor
      IS
         SELECT LANE_ID,
                RATING_LANE_DTL_SEQ,
                RLD_RATE_SEQ,
                TC_COMPANY_ID,
                CHANGE_ID,
                0 PENDING_APPROVAL,
                RATE,
                RATE_CALC_METHOD,
                SUPPORTS_MSTL,
                CURRENCY_CODE,
                RATE_UOM,
                LAST_UPDATED_SRC_TYPE,
                LAST_UPDATED_SRC,
                LAST_UPDATED_DTTM,
                AUTH_SRC_TYPE,
                AUTH_SRC,
                REASON_ID
           FROM RATING_LANE_DTL_RATE
          WHERE     TC_COMPANY_ID = vTCCompanyId
                AND LANE_ID = vLaneId
                AND RATING_LANE_DTL_SEQ = vRatingDtlSeq
                AND -- vRatingDtlSeqNew > vRatingDtlSeq AND
                    vIsRating = 1
                AND 0 =
                       (SELECT COUNT (*)
                          FROM RATING_EVENT
                         WHERE     TC_COMPANY_ID = vTCCompanyId
                               AND RATING_LANE_ID = vLaneId
                               AND RATING_LANE_DTL_ID = vRatingDtlSeqNew);
   BEGIN
      IF (vIsRating = 1)
      THEN
         IF (vOverlappedSeq = -1)
         THEN
            vRatingDtlSeqTemp := vRatingDtlSeq;
         ELSE
            vRatingDtlSeqTemp := vOverlappedSeq;
         END IF;
		 
		 IF (vuser IS NOT NULL)
            THEN
               vlastupdatedsource := vuser;
               vlastupdatedsourcetype := 1;
            ELSE
               vlastupdatedsource := 'OMS';
               vlastupdatedsourcetype := 3;
            END IF;

         INSERT INTO RATING_LANE_DTL_RATE (TC_COMPANY_ID,
                                           LANE_ID,
                                           RATING_LANE_DTL_SEQ,
                                           RLD_RATE_SEQ,
                                           MINIMUM_SIZE,
                                           MAXIMUM_SIZE,
                                           SIZE_UOM_ID,
                                           RATE_CALC_METHOD,
                                           RATE_UOM,
                                           RATE,
                                           MINIMUM_RATE,
                                           CURRENCY_CODE,
                                           TARIFF_CODE_ID, -- (8/8/2008 gakumar)
                                           MIN_DISTANCE,
                                           MAX_DISTANCE,
                                           DISTANCE_UOM,
                                           SUPPORTS_MSTL,
                                           HAS_BH,
                                           HAS_RT,
                                           MIN_COMMODITY,
                                           MAX_COMMODITY,
                                           COMMODITY_CODE_ID,
                                           EXCESS_WT_RATE,
                                           PAYEE_CARRIER_ID,
                                           MAX_RANGE_COMMODITY_CODE_ID,
                                           IS_SURGE_RATE,
                                           MINIMUM_RATE_TYPE,
                                           BASE_RATE,
                                           MAXIMUM_RATE,
                                           CUSTOMER_MIN_CHARGE,
                                           CUSTOMER_BASE_RATE,
                                           CUSTOMER_SURCHARGE_RATE)
            SELECT TC_COMPANY_ID,
                   LANE_ID,
                   vRatingDtlSeqNew,
                   RLD_RATE_SEQ,
                   MINIMUM_SIZE,
                   MAXIMUM_SIZE,
                   SIZE_UOM_ID,
                   RATE_CALC_METHOD,
                   RATE_UOM,
                   RATE,
                   MINIMUM_RATE,
                   CURRENCY_CODE,
                   TARIFF_CODE_ID,                       -- (8/8/2008 gakumar)
                   MIN_DISTANCE,
                   MAX_DISTANCE,
                   DISTANCE_UOM,
                   SUPPORTS_MSTL,
                   HAS_BH,
                   HAS_RT,
                   MIN_COMMODITY,
                   MAX_COMMODITY,
                   COMMODITY_CODE_ID,
                   EXCESS_WT_RATE,
                   PAYEE_CARRIER_ID,
                   MAX_RANGE_COMMODITY_CODE_ID,
                   IS_SURGE_RATE,
                   MINIMUM_RATE_TYPE,
                   BASE_RATE,
                   MAXIMUM_RATE,
                   CUSTOMER_MIN_CHARGE,
                   CUSTOMER_BASE_RATE,
                   CUSTOMER_SURCHARGE_RATE
              FROM RATING_LANE_DTL_RATE
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqTemp;

			-- update fields from rating_event table
     begin
      SELECT param_value
              INTO vpendingauth
              FROM company_parameter
             WHERE param_def_id = 'RATING_CHANGE_AUTHORIZATION_REQUIRED'
                   AND tc_company_id = vtccompanyid;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               SELECT dflt_value
                 INTO vpendingauth
                 FROM param_def
                WHERE param_def_id = 'RATING_CHANGE_AUTHORIZATION_REQUIRED';
         END;
			
		
         IF (vpendingauth >0)
			THEN
				UPDATE_RATING_RATE_DATA(vTCCompanyId, vLaneId, vRatingDtlSeqTemp, vRatingDtlSeqNew);
         END IF;
				   
         rldCount := 1;

         FOR r IN T1Cursor
         LOOP
         /* SELECT last_updated_source
              INTO vuser
              FROM COMB_LANE_DTL
             WHERE     tc_company_id = vtccompanyid
                   AND lane_id = vlaneid
                   AND lane_dtl_seq = vratingdtlseq
                   AND visrating = 1;*/

            

            -- MACR00617605 starts
			BEGIN
            SELECT RATE
              INTO vRateTemp
              FROM RATING_LANE_DTL_RATE
             WHERE     tc_company_id = r.TC_COMPANY_ID
                   AND lane_id = r.LANE_ID
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew
                   AND RLD_RATE_SEQ = r.RLD_RATE_SEQ;
			EXCEPTION
			WHEN NO_DATA_FOUND THEN
				vRateTemp := -1;
			END;

            Ins_Rating_Event (
               r.LANE_ID,
               vRatingDtlSeqNew,
               rldCount,
               r.TC_COMPANY_ID,
               r.CHANGE_ID,
               'Rate',
               NULL,
               vRateTemp,
               'Split',
               COALESCE (vlastupdatedsourcetype,r.LAST_UPDATED_SRC_TYPE),
               COALESCE (vlastupdatedsource,r.LAST_UPDATED_SRC ),
               r.AUTH_SRC_TYPE,
               r.AUTH_SRC,
               r.LAST_UPDATED_DTTM,
               NULL,
               r.REASON_ID);

            Ins_Rating_Event (
               r.LANE_ID,
               vRatingDtlSeqNew,
               rldCount,
               r.TC_COMPANY_ID,
               r.CHANGE_ID,
               'Rate Type',
               NULL,
               r.RATE_CALC_METHOD,
               'Split',
               COALESCE (vlastupdatedsourcetype,r.LAST_UPDATED_SRC_TYPE),
               COALESCE (vlastupdatedsource,r.LAST_UPDATED_SRC ),
               r.AUTH_SRC_TYPE,
               r.AUTH_SRC,
               r.LAST_UPDATED_DTTM,
               NULL,
               r.REASON_ID);

            Ins_Rating_Event (
               r.LANE_ID,
               vRatingDtlSeqNew,
               rldCount,
               r.TC_COMPANY_ID,
               r.CHANGE_ID,
               'Supports MSTL',
               NULL,
               r.SUPPORTS_MSTL,
               'Split',
              COALESCE (vlastupdatedsourcetype,r.LAST_UPDATED_SRC_TYPE),
               COALESCE (vlastupdatedsource,r.LAST_UPDATED_SRC ),
               r.AUTH_SRC_TYPE,
               r.AUTH_SRC,
               r.LAST_UPDATED_DTTM,
               NULL,
               r.REASON_ID);

            Ins_Rating_Event (
               r.LANE_ID,
               vRatingDtlSeqNew,
               rldCount,
               r.TC_COMPANY_ID,
               r.CHANGE_ID,
               'Currency',
               NULL,
               r.CURRENCY_CODE,
               'Split',
               COALESCE (vlastupdatedsourcetype,r.LAST_UPDATED_SRC_TYPE),
               COALESCE (vlastupdatedsource,r.LAST_UPDATED_SRC ),
               r.AUTH_SRC_TYPE,
               r.AUTH_SRC,
               r.LAST_UPDATED_DTTM,
               NULL,
               r.REASON_ID);

            Ins_Rating_Event (
               r.LANE_ID,
               vRatingDtlSeqNew,
               rldCount,
               r.TC_COMPANY_ID,
               r.CHANGE_ID,
               'Rate Unit',
               NULL,
               r.RATE_UOM,
               'Split',
               COALESCE (vlastupdatedsourcetype,r.LAST_UPDATED_SRC_TYPE),
               COALESCE (vlastupdatedsource,r.LAST_UPDATED_SRC ),
               r.AUTH_SRC_TYPE,
               r.AUTH_SRC,
               r.LAST_UPDATED_DTTM,
               NULL,
               r.REASON_ID);
            rldCount := rldCount + 1;
         -- MACR00617605 ends


         END LOOP;


         FOR l_rec
            IN (SELECT lane_accessorial_id,
                       tc_company_id,
                       lane_id,
                       accessorial_id,
                       rate,
                       minimum_rate,
                       currency_code,
                       is_auto_approve,
                       effective_dt,
                       expiration_dt,
                       payee_carrier_id,
                       minimum_size,
                       maximum_size,
                       size_uom_id,
                       zone_id,
                       is_shipment_accessorial,
                       incoterm_id,
                       Min_Rate_Type,
                       vratingdtlseqnew,
                       Maximum_Rate,
                       PACKAGE_TYPE_ID,
                       CUSTOMER_RATE,
                       Customer_Min_Charge,
                       Calculated_Rate,
                       Amount,
                       BASE_AMOUNT,
                       Base_Charge,
                       RANGE_MIN_AMOUNT,
                       RANGE_MAX_AMOUNT,
                       Increment_Val,
                       Mot_Id,
                       Equipment_Id,
                       Service_Level_Id,
                       PROTECTION_LEVEL_ID,
                       Billing_Method,
                       Min_Longest_Dimension,
                       Max_Longest_Dimension,
                       Min_Second_Longest_Dimension,
                       Max_Second_Longest_Dimension,
                       Min_Third_Longest_Dimension,
                       Max_Third_Longest_Dimension,
                       Min_Length_Plus_Grith,
                       Max_Length_Plus_Grith,
                       Min_Weight,
                       Max_Weight,
                       Is_Zone_Stopoff,
                       COMMODITY_CODE_ID,
                       MAX_RANGE_COMMODITY_CODE_ID
                  FROM lane_accessorial
                 WHERE     tc_company_id = vtccompanyid
                       AND lane_id = vlaneid
                       AND rating_lane_dtl_seq = vratingdtlseqtemp)
         LOOP
            INSERT INTO LANE_ACCESSORIAL (TC_COMPANY_ID,
                                          LANE_ID,
                                          RATING_LANE_DTL_SEQ,
                                          ACCESSORIAL_ID,
                                          RATE,
                                          MINIMUM_RATE,
                                          CURRENCY_CODE,
                                          IS_AUTO_APPROVE,
                                          EFFECTIVE_DT,
                                          EXPIRATION_DT,
                                          LANE_ACCESSORIAL_ID,
                                          PAYEE_CARRIER_ID,
                                          MINIMUM_SIZE,
                                          MAXIMUM_SIZE,
                                          SIZE_UOM_ID,
                                          ZONE_ID,
                                          IS_SHIPMENT_ACCESSORIAL,
                                          INCOTERM_ID,
                                          Min_Rate_Type, -- MACR00159593 starts
                                          Maximum_Rate,
                                          PACKAGE_TYPE_ID,
                                          CUSTOMER_RATE,
                                          Customer_Min_Charge,
                                          Calculated_Rate,
                                          Amount,
                                          BASE_AMOUNT,
                                          Base_Charge,
                                          RANGE_MIN_AMOUNT,
                                          RANGE_MAX_AMOUNT,
                                          Increment_Val,
                                          Mot_Id,
                                          Equipment_Id,
                                          Service_Level_Id,
                                          PROTECTION_LEVEL_ID,
                                          Billing_Method,
                                          Min_Longest_Dimension,
                                          Max_Longest_Dimension,
                                          Min_Second_Longest_Dimension,
                                          Max_Second_Longest_Dimension,
                                          Min_Third_Longest_Dimension,
                                          Max_Third_Longest_Dimension,
                                          Min_Length_Plus_Grith,
                                          Max_Length_Plus_Grith,
                                          Min_Weight,
                                          Max_Weight,
                                           Is_Zone_Stopoff, -- MACR00159593 ends
                                          COMMODITY_CODE_ID,
                                           MAX_RANGE_COMMODITY_CODE_ID)
                 VALUES (l_rec.TC_COMPANY_ID,
                         l_rec.LANE_ID,
                         l_rec.vratingdtlseqnew,
                         l_rec.ACCESSORIAL_ID,
                         l_rec.RATE,
                         l_rec.MINIMUM_RATE,
                         l_rec.CURRENCY_CODE,
                         l_rec.IS_AUTO_APPROVE,
                         l_rec.EFFECTIVE_DT,
                         l_rec.EXPIRATION_DT,
                         LANE_ACCESSORIAL_ID_SEQ.NEXTVAL,
                         l_rec.PAYEE_CARRIER_ID,
                         l_rec.MINIMUM_SIZE,
                         l_rec.MAXIMUM_SIZE,
                         l_rec.SIZE_UOM_ID,
                         l_rec.ZONE_ID,
                         l_rec.IS_SHIPMENT_ACCESSORIAL,
                         l_rec.INCOTERM_ID,
                         l_rec.Min_Rate_Type,
                         l_rec.Maximum_Rate,
                         l_rec.PACKAGE_TYPE_ID,
                         l_rec.CUSTOMER_RATE,
                         l_rec.Customer_Min_Charge,
                         l_rec.Calculated_Rate,
                         l_rec.Amount,
                         l_rec.BASE_AMOUNT,
                         l_rec.Base_Charge,
                         l_rec.RANGE_MIN_AMOUNT,
                         l_rec.RANGE_MAX_AMOUNT,
                         l_rec.Increment_Val,
                         l_rec.Mot_Id,
                         l_rec.Equipment_Id,
                         l_rec.Service_Level_Id,
                         l_rec.PROTECTION_LEVEL_ID,
                         l_rec.Billing_Method,
                         l_rec.Min_Longest_Dimension,
                         l_rec.Max_Longest_Dimension,
                         l_rec.Min_Second_Longest_Dimension,
                         l_rec.Max_Second_Longest_Dimension,
                         l_rec.Min_Third_Longest_Dimension,
                         l_rec.Max_Third_Longest_Dimension,
                         l_rec.Min_Length_Plus_Grith,
                         l_rec.Max_Length_Plus_Grith,
                         l_rec.Min_Weight,
                         l_rec.Max_Weight,
                         l_rec.Is_Zone_Stopoff,
                         l_rec.COMMODITY_CODE_ID,
                         l_rec.MAX_RANGE_COMMODITY_CODE_ID);

              -- update new values for newly created accessorial
              IF (vpendingauth >0)
                THEN
                  UPDATE_RATING_ACC_DATA(vTCCompanyId, vLaneId, vRatingDtlSeqTemp, vRatingDtlSeqNew);
              END IF;
              
            SELECT MAX (lane_accessorial_id)
              INTO v_lane_acces_id_seq
              FROM lane_accessorial
             WHERE     tc_company_id = vtccompanyid
                   AND lane_id = vlaneid
                   AND rating_lane_dtl_seq = vratingdtlseqnew;
				   
			INSERT INTO LANE_ACC_EXCLUDED_LIST (LANE_ACC_ID, EXCLUDED_ACC_ID)
			SELECT v_lane_acces_id_seq, EXCLUDED_ACC_ID
			FROM LANE_ACC_EXCLUDED_LIST
			WHERE LANE_ACC_ID = l_rec.lane_accessorial_id;
				   
            INSERT INTO lane_acc_feasible_incoterm (LANE_ACC_ID, INCOTERM_ID)
				SELECT v_lane_acces_id_seq, INCOTERM_ID
				FROM lane_acc_feasible_incoterm
				WHERE LANE_ACC_ID = l_rec.lane_accessorial_id;					
				   
            SELECT COUNT (*)
              INTO vcnt
              FROM rating_event
             WHERE     rating_lane_id = vlaneid
                   AND rating_lane_dtl_id = vratingdtlseqtemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NULL
                   AND lane_accessorial_id = l_rec.lane_accessorial_id;
         END LOOP;
      END IF;

      IF (vIsRouting = 1)
      THEN
         INSERT INTO SURGE_CAPACITY (SURGE_CAPACITY_ID,
                                     TC_COMPANY_ID,
                                     LANE_ID,
                                     RG_LANE_DTL_SEQ,
                                     EFFECTIVE_DT,
                                     EXPIRATION_DT,
                                     WEEKLY_SURGE_CAPACITY,
                                     DAILY_SURGE_CAPACITY,
                                     SURGE_CAPACITY_SUN,
                                     SURGE_CAPACITY_MON,
                                     SURGE_CAPACITY_TUE,
                                     SURGE_CAPACITY_WED,
                                     SURGE_CAPACITY_THU,
                                     SURGE_CAPACITY_FRI,
                                     SURGE_CAPACITY_SAT,
									 IS_SURGE)
            SELECT SEQ_SURGE_CAPACITY_ID.NEXTVAL,
                   TC_COMPANY_ID,
                   LANE_ID,
                   vRatingDtlSeqNew,
                   EFFECTIVE_DT,
                   EXPIRATION_DT,
                   WEEKLY_SURGE_CAPACITY,
                   DAILY_SURGE_CAPACITY,
                   SURGE_CAPACITY_SUN,
                   SURGE_CAPACITY_MON,
                   SURGE_CAPACITY_TUE,
                   SURGE_CAPACITY_WED,
                   SURGE_CAPACITY_THU,
                   SURGE_CAPACITY_FRI,
                   SURGE_CAPACITY_SAT,
				   IS_SURGE
              FROM SURGE_CAPACITY
             WHERE     TC_COMPANY_ID = vTCCompanyId
                   AND LANE_ID = vLaneId
                   AND RG_LANE_DTL_SEQ = vRatingDtlSeq;
      END IF;

      IF (vIsSailing = 1)
      THEN
         INSERT INTO SAILING_SCHDL_BY_DATE (TC_COMPANY_ID,
                                            LANE_ID,
                                            LANE_DTL_SEQ,
                                            SAIL_SCHED_BY_DATE_ID,
                                            DEPARTURE_DTTM,
                                            ARRIVAL_DTTM,
                                            CUTOFF_DTTM,
                                            PICKUP_DTTM,
                                            TRANSIT_TIME_VALUE,
                                            TRANSIT_TIME_STANDARD_UOM,
                                            RESOURCE_REF_EXTERNAL,
                                            RESOURCE_NAME_EXTERNAL,
                                            COMMENTS,
                                            DESCRIPTION)
            SELECT TC_COMPANY_ID,
                   LANE_ID,
                   vRatingDtlSeqNew,
                   SEQ_SAIL_SCHED_BY_DATE_ID.NEXTVAL,
                   DEPARTURE_DTTM,
                   ARRIVAL_DTTM,
                   CUTOFF_DTTM,
                   PICKUP_DTTM,
                   TRANSIT_TIME_VALUE,
                   TRANSIT_TIME_STANDARD_UOM,
                   RESOURCE_REF_EXTERNAL,
                   RESOURCE_NAME_EXTERNAL,
                   COMMENTS,
                   DESCRIPTION
              FROM SAILING_SCHDL_BY_DATE
             WHERE     TC_COMPANY_ID = vTCCompanyId
                   AND LANE_ID = vLaneId
                   AND LANE_DTL_SEQ = vRatingDtlSeq;

         week_day_sql :=
               ' SELECT SAIL_SCHED_BY_WEEK_ID '
            || ' FROM SAILING_SCHDL_BY_WEEK '
            || ' WHERE tc_company_id = :vTCCompanyId'
            || ' AND lane_id = :vLaneId'
            || ' AND lane_dtl_seq = :vRatingDtlSeq';

         OPEN week_day_cursor FOR week_day_sql
            USING vTCCompanyId, vLaneId, vRatingDtlSeq;

         LOOP
            FETCH week_day_cursor INTO vWeekId;

            EXIT WHEN week_day_cursor%NOTFOUND;

            INSERT INTO SAILING_SCHDL_BY_WEEK (TC_COMPANY_ID,
                                               LANE_ID,
                                               LANE_DTL_SEQ,
                                               SAIL_SCHED_BY_WEEK_ID,
                                               EFFECTIVE_DT,
                                               EXPIRATION_DT,
                                               RESOURCE_REF_EXTERNAL,
                                               RESOURCE_NAME_EXTERNAL,
                                               COMMENTS,
                                               DESCRIPTION)
               SELECT TC_COMPANY_ID,
                      LANE_ID,
                      vRatingDtlSeqNew,
                      SEQ_SAIL_SCHED_BY_WEEK_ID.NEXTVAL,
                      EFFECTIVE_DT,
                      EXPIRATION_DT,
                      RESOURCE_REF_EXTERNAL,
                      RESOURCE_NAME_EXTERNAL,
                      COMMENTS,
                      DESCRIPTION
                 FROM SAILING_SCHDL_BY_WEEK
                WHERE     TC_COMPANY_ID = vTCCompanyId
                      AND LANE_ID = vLaneId
                      AND LANE_DTL_SEQ = vRatingDtlSeq
                      AND SAIL_SCHED_BY_WEEK_ID = vWeekId;



            INSERT INTO SAILING_SCHDL_BY_WEEK_DAY (TC_COMPANY_ID,
                                                   LANE_ID,
                                                   LANE_DTL_SEQ,
                                                   SAIL_SCHED_BY_WEEK_ID,
                                                   SAIL_SCHED_BY_WEEK_DAY_ID,
                                                   DEPARTURE_DAYOFWEEK,
                                                   DEPARTURE_TIME,
                                                   ARRIVAL_DAYOFWEEK,
                                                   ARRIVAL_TIME,
                                                   CUTOFF_DAYOFWEEK,
                                                   CUTOFF_TIME,
                                                   PICKUP_DAYOFWEEK,
                                                   PICKUP_TIME,
                                                   TRANSIT_TIME_VALUE,
                                                   TRANSIT_TIME_STANDARD_UOM,
                                                   TRANSIT_TIME_WEEK)
               SELECT TC_COMPANY_ID,
                      LANE_ID,
                      vRatingDtlSeqNew,
                      SEQ_SAIL_SCHED_BY_WEEK_ID.CURRVAL,
                      SEQ_SAIL_SCHED_BY_WEEK_DAY_ID.NEXTVAL,
                      DEPARTURE_DAYOFWEEK,
                      DEPARTURE_TIME,
                      ARRIVAL_DAYOFWEEK,
                      ARRIVAL_TIME,
                      CUTOFF_DAYOFWEEK,
                      CUTOFF_TIME,
                      PICKUP_DAYOFWEEK,
                      PICKUP_TIME,
                      TRANSIT_TIME_VALUE,
                      TRANSIT_TIME_STANDARD_UOM,
                      TRANSIT_TIME_WEEK
                 FROM SAILING_SCHDL_BY_WEEK_DAY
                WHERE     TC_COMPANY_ID = vTCCompanyId
                      AND LANE_ID = vLaneId
                      AND LANE_DTL_SEQ = vRatingDtlSeq
                      AND SAIL_SCHED_BY_WEEK_ID = vWeekId;
         END LOOP;

         CLOSE week_day_cursor;
      END IF;
   END COPY_CHILD_ROW_NEW;
   
   PROCEDURE COPY_CHILD_ROW (
      vTCCompanyId       IN COMPANY.COMPANY_ID%TYPE,
      vLaneId            IN COMB_LANE.LANE_ID%TYPE,
      vOverlappedSeq     IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vRatingDtlSeq      IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vRatingDtlSeqNew   IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vIsRating          IN COMB_LANE_DTL.IS_RATING%TYPE,
      vIsRouting         IN COMB_LANE_DTL.IS_ROUTING%TYPE,
      vIsSailing         IN COMB_LANE_DTL.IS_SAILING%TYPE,
	  vuser              IN   comb_lane_dtl.last_updated_source%TYPE)
   AS
      vRatingDtlSeqTemp        COMB_LANE_DTL.LANE_DTL_SEQ%TYPE;
      week_day_sql             VARCHAR2 (2000);
      week_day_cursor          ref_curtype;
      vWeekId                  SAILING_SCHDL_BY_WEEK.SAIL_SCHED_BY_WEEK_ID%TYPE;
      rldCount                 NUMBER;
      vcnt                     NUMBER;
      v_lane_acces_id_seq      lane_accessorial.lane_accessorial_id%TYPE;

      -- MACR00617605 starts
      vRateTemp                RATING_LANE_DTL_RATE.RATE%TYPE;
      -- MACR00617605 ends

     
      vlastupdatedsource       comb_lane_dtl.last_updated_source%TYPE;
      vlastupdatedsourcetype   comb_lane_dtl.last_updated_source_type%TYPE;

      ----------------------------------------------------
      CURSOR T1Cursor
      IS
         SELECT LANE_ID,
                RATING_LANE_DTL_SEQ,
                RLD_RATE_SEQ,
                TC_COMPANY_ID,
                CHANGE_ID,
                0 PENDING_APPROVAL,
                RATE,
                RATE_CALC_METHOD,
                SUPPORTS_MSTL,
                CURRENCY_CODE,
                RATE_UOM,
                LAST_UPDATED_SRC_TYPE,
                LAST_UPDATED_SRC,
                LAST_UPDATED_DTTM,
                AUTH_SRC_TYPE,
                AUTH_SRC,
                REASON_ID
           FROM RATING_LANE_DTL_RATE
          WHERE     TC_COMPANY_ID = vTCCompanyId
                AND LANE_ID = vLaneId
                AND RATING_LANE_DTL_SEQ = vRatingDtlSeq
                AND -- vRatingDtlSeqNew > vRatingDtlSeq AND
                    vIsRating = 1
                AND 0 =
                       (SELECT COUNT (*)
                          FROM RATING_EVENT
                         WHERE     TC_COMPANY_ID = vTCCompanyId
                               AND RATING_LANE_ID = vLaneId
                               AND RATING_LANE_DTL_ID = vRatingDtlSeqNew);
   BEGIN
      IF (vIsRating = 1)
      THEN
         IF (vOverlappedSeq = -1)
         THEN
            vRatingDtlSeqTemp := vRatingDtlSeq;
         ELSE
            vRatingDtlSeqTemp := vOverlappedSeq;
         END IF;
		 
		 IF (vuser IS NOT NULL)
            THEN
               vlastupdatedsource := vuser;
               vlastupdatedsourcetype := 1;
            ELSE
               vlastupdatedsource := 'OMS';
               vlastupdatedsourcetype := 3;
            END IF;

         INSERT INTO RATING_LANE_DTL_RATE (TC_COMPANY_ID,
                                           LANE_ID,
                                           RATING_LANE_DTL_SEQ,
                                           RLD_RATE_SEQ,
                                           MINIMUM_SIZE,
                                           MAXIMUM_SIZE,
                                           SIZE_UOM_ID,
                                           RATE_CALC_METHOD,
                                           RATE_UOM,
                                           RATE,
                                           MINIMUM_RATE,
                                           CURRENCY_CODE,
                                           TARIFF_CODE_ID, -- (8/8/2008 gakumar)
                                           MIN_DISTANCE,
                                           MAX_DISTANCE,
                                           DISTANCE_UOM,
                                           SUPPORTS_MSTL,
                                           HAS_BH,
                                           HAS_RT,
                                           MIN_COMMODITY,
                                           MAX_COMMODITY,
                                           COMMODITY_CODE_ID,
                                           EXCESS_WT_RATE,
                                           PAYEE_CARRIER_ID,
                                           MAX_RANGE_COMMODITY_CODE_ID,
                                           IS_SURGE_RATE,
                                           MINIMUM_RATE_TYPE,
                                           BASE_RATE,
                                           MAXIMUM_RATE,
                                           CUSTOMER_MIN_CHARGE,
                                           CUSTOMER_BASE_RATE,
                                           CUSTOMER_SURCHARGE_RATE)
            SELECT TC_COMPANY_ID,
                   LANE_ID,
                   vRatingDtlSeqNew,
                   RLD_RATE_SEQ,
                   MINIMUM_SIZE,
                   MAXIMUM_SIZE,
                   SIZE_UOM_ID,
                   RATE_CALC_METHOD,
                   RATE_UOM,
                   RATE,
                   MINIMUM_RATE,
                   CURRENCY_CODE,
                   TARIFF_CODE_ID,                       -- (8/8/2008 gakumar)
                   MIN_DISTANCE,
                   MAX_DISTANCE,
                   DISTANCE_UOM,
                   SUPPORTS_MSTL,
                   HAS_BH,
                   HAS_RT,
                   MIN_COMMODITY,
                   MAX_COMMODITY,
                   COMMODITY_CODE_ID,
                   EXCESS_WT_RATE,
                   PAYEE_CARRIER_ID,
                   MAX_RANGE_COMMODITY_CODE_ID,
                   IS_SURGE_RATE,
                   MINIMUM_RATE_TYPE,
                   BASE_RATE,
                   MAXIMUM_RATE,
                   CUSTOMER_MIN_CHARGE,
                   CUSTOMER_BASE_RATE,
                   CUSTOMER_SURCHARGE_RATE
              FROM RATING_LANE_DTL_RATE
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vRatingDtlSeqTemp;

         /* insert into debug_log values(vTCCompanyId||':'||vLaneId||':'||vRatingDtlSeqTemp);
          insert into debug_log (COL1)
           SELECT  vRatingDtlSeqNew||'old :'||vRatingDtlSeq||'rating-'||vIsRating from dual;
         */
		 
         INSERT INTO RATING_EVENT (rating_event_id,
                                   rating_lane_id,
                                   rating_lane_dtl_id,
                                   rld_rate_id,
                                   tc_company_id,
                                   change_id,
                                   is_pending_auth,
                                   field_name,
                                   old_value,
                                   new_value,
                                   comments,
                                   last_updated_src_type,
                                   last_updated_src,
                                   last_updated_dttm,
                                   auth_src_type,
                                   auth_src,
                                   auth_dttm,
                                   lane_accessorial_id,
                                   reason_id,
                                   SPLIT)
            SELECT seq_rating_event_id.NEXTVAL,
                   rating_lane_id,
                   vRatingDtlSeqNew,
                   rld_rate_id,
                   tc_company_id,
                   change_id,
                   is_pending_auth,
                   field_name,
                   old_value,
                   new_value,
                   comments,
                   last_updated_src_type,
                   last_updated_src,
                   last_updated_dttm,
                   auth_src_type,
                   auth_src,
                   auth_dttm,
                   lane_accessorial_id,
                   reason_id,
                   SPLIT
              FROM RATING_EVENT
             WHERE     rating_lane_id = vLaneId
                   AND RATING_LANE_DTL_ID = vRatingDtlSeqTemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NOT NULL
                   AND lane_accessorial_id IS NULL;


         rldCount := 1;

         FOR r IN T1Cursor
         LOOP
         /* SELECT last_updated_source
              INTO vuser
              FROM COMB_LANE_DTL
             WHERE     tc_company_id = vtccompanyid
                   AND lane_id = vlaneid
                   AND lane_dtl_seq = vratingdtlseq
                   AND visrating = 1;*/

            

            -- MACR00617605 starts
			BEGIN
            SELECT RATE
              INTO vRateTemp
              FROM RATING_LANE_DTL_RATE
             WHERE     tc_company_id = r.TC_COMPANY_ID
                   AND lane_id = r.LANE_ID
                   AND rating_lane_dtl_seq = vRatingDtlSeqNew
                   AND RLD_RATE_SEQ = r.RLD_RATE_SEQ;
			EXCEPTION
			WHEN NO_DATA_FOUND THEN
				vRateTemp := -1;
			END;

            Ins_Rating_Event (
               r.LANE_ID,
               vRatingDtlSeqNew,
               rldCount,
               r.TC_COMPANY_ID,
               r.CHANGE_ID,
               'Rate',
               NULL,
               vRateTemp,
               'Split',
               COALESCE (vlastupdatedsourcetype,r.LAST_UPDATED_SRC_TYPE),
               COALESCE (vlastupdatedsource,r.LAST_UPDATED_SRC ),
               r.AUTH_SRC_TYPE,
               r.AUTH_SRC,
               r.LAST_UPDATED_DTTM,
               NULL,
               r.REASON_ID);

            Ins_Rating_Event (
               r.LANE_ID,
               vRatingDtlSeqNew,
               rldCount,
               r.TC_COMPANY_ID,
               r.CHANGE_ID,
               'Rate Type',
               NULL,
               r.RATE_CALC_METHOD,
               'Split',
               COALESCE (vlastupdatedsourcetype,r.LAST_UPDATED_SRC_TYPE),
               COALESCE (vlastupdatedsource,r.LAST_UPDATED_SRC ),
               r.AUTH_SRC_TYPE,
               r.AUTH_SRC,
               r.LAST_UPDATED_DTTM,
               NULL,
               r.REASON_ID);

            Ins_Rating_Event (
               r.LANE_ID,
               vRatingDtlSeqNew,
               rldCount,
               r.TC_COMPANY_ID,
               r.CHANGE_ID,
               'Supports MSTL',
               NULL,
               r.SUPPORTS_MSTL,
               'Split',
              COALESCE (vlastupdatedsourcetype,r.LAST_UPDATED_SRC_TYPE),
               COALESCE (vlastupdatedsource,r.LAST_UPDATED_SRC ),
               r.AUTH_SRC_TYPE,
               r.AUTH_SRC,
               r.LAST_UPDATED_DTTM,
               NULL,
               r.REASON_ID);

            Ins_Rating_Event (
               r.LANE_ID,
               vRatingDtlSeqNew,
               rldCount,
               r.TC_COMPANY_ID,
               r.CHANGE_ID,
               'Currency',
               NULL,
               r.CURRENCY_CODE,
               'Split',
               COALESCE (vlastupdatedsourcetype,r.LAST_UPDATED_SRC_TYPE),
               COALESCE (vlastupdatedsource,r.LAST_UPDATED_SRC ),
               r.AUTH_SRC_TYPE,
               r.AUTH_SRC,
               r.LAST_UPDATED_DTTM,
               NULL,
               r.REASON_ID);

            Ins_Rating_Event (
               r.LANE_ID,
               vRatingDtlSeqNew,
               rldCount,
               r.TC_COMPANY_ID,
               r.CHANGE_ID,
               'Rate Unit',
               NULL,
               r.RATE_UOM,
               'Split',
               COALESCE (vlastupdatedsourcetype,r.LAST_UPDATED_SRC_TYPE),
               COALESCE (vlastupdatedsource,r.LAST_UPDATED_SRC ),
               r.AUTH_SRC_TYPE,
               r.AUTH_SRC,
               r.LAST_UPDATED_DTTM,
               NULL,
               r.REASON_ID);
            rldCount := rldCount + 1;
         -- MACR00617605 ends


         END LOOP;


         FOR l_rec
            IN (SELECT lane_accessorial_id,
                       tc_company_id,
                       lane_id,
                       accessorial_id,
                       rate,
                       minimum_rate,
                       currency_code,
                       is_auto_approve,
                       effective_dt,
                       expiration_dt,
                       payee_carrier_id,
                       minimum_size,
                       maximum_size,
                       size_uom_id,
                       zone_id,
                       is_shipment_accessorial,
                       incoterm_id,
                       Min_Rate_Type,
                       vratingdtlseqnew,
                       Maximum_Rate,
                       PACKAGE_TYPE_ID,
                       CUSTOMER_RATE,
                       Customer_Min_Charge,
                       Calculated_Rate,
                       Amount,
                       BASE_AMOUNT,
                       Base_Charge,
                       RANGE_MIN_AMOUNT,
                       RANGE_MAX_AMOUNT,
                       Increment_Val,
                       Mot_Id,
                       Equipment_Id,
                       Service_Level_Id,
                       PROTECTION_LEVEL_ID,
                       Billing_Method,
                       Min_Longest_Dimension,
                       Max_Longest_Dimension,
                       Min_Second_Longest_Dimension,
                       Max_Second_Longest_Dimension,
                       Min_Third_Longest_Dimension,
                       Max_Third_Longest_Dimension,
                       Min_Length_Plus_Grith,
                       Max_Length_Plus_Grith,
                       Min_Weight,
                       Max_Weight,
                       Is_Zone_Stopoff,
                       COMMODITY_CODE_ID,
                       MAX_RANGE_COMMODITY_CODE_ID
                  FROM lane_accessorial
                 WHERE     tc_company_id = vtccompanyid
                       AND lane_id = vlaneid
                       AND rating_lane_dtl_seq = vratingdtlseqtemp)
         LOOP
            INSERT INTO LANE_ACCESSORIAL (TC_COMPANY_ID,
                                          LANE_ID,
                                          RATING_LANE_DTL_SEQ,
                                          ACCESSORIAL_ID,
                                          RATE,
                                          MINIMUM_RATE,
                                          CURRENCY_CODE,
                                          IS_AUTO_APPROVE,
                                          EFFECTIVE_DT,
                                          EXPIRATION_DT,
                                          LANE_ACCESSORIAL_ID,
                                          PAYEE_CARRIER_ID,
                                          MINIMUM_SIZE,
                                          MAXIMUM_SIZE,
                                          SIZE_UOM_ID,
                                          ZONE_ID,
                                          IS_SHIPMENT_ACCESSORIAL,
                                          INCOTERM_ID,
                                          Min_Rate_Type, -- MACR00159593 starts
                                          Maximum_Rate,
                                          PACKAGE_TYPE_ID,
                                          CUSTOMER_RATE,
                                          Customer_Min_Charge,
                                          Calculated_Rate,
                                          Amount,
                                          BASE_AMOUNT,
                                          Base_Charge,
                                          RANGE_MIN_AMOUNT,
                                          RANGE_MAX_AMOUNT,
                                          Increment_Val,
                                          Mot_Id,
                                          Equipment_Id,
                                          Service_Level_Id,
                                          PROTECTION_LEVEL_ID,
                                          Billing_Method,
                                          Min_Longest_Dimension,
                                          Max_Longest_Dimension,
                                          Min_Second_Longest_Dimension,
                                          Max_Second_Longest_Dimension,
                                          Min_Third_Longest_Dimension,
                                          Max_Third_Longest_Dimension,
                                          Min_Length_Plus_Grith,
                                          Max_Length_Plus_Grith,
                                          Min_Weight,
                                          Max_Weight,
                                           Is_Zone_Stopoff, -- MACR00159593 ends
                                          COMMODITY_CODE_ID,
                                           MAX_RANGE_COMMODITY_CODE_ID)
                 VALUES (l_rec.TC_COMPANY_ID,
                         l_rec.LANE_ID,
                         l_rec.vratingdtlseqnew,
                         l_rec.ACCESSORIAL_ID,
                         l_rec.RATE,
                         l_rec.MINIMUM_RATE,
                         l_rec.CURRENCY_CODE,
                         l_rec.IS_AUTO_APPROVE,
                         l_rec.EFFECTIVE_DT,
                         l_rec.EXPIRATION_DT,
                         LANE_ACCESSORIAL_ID_SEQ.NEXTVAL,
                         l_rec.PAYEE_CARRIER_ID,
                         l_rec.MINIMUM_SIZE,
                         l_rec.MAXIMUM_SIZE,
                         l_rec.SIZE_UOM_ID,
                         l_rec.ZONE_ID,
                         l_rec.IS_SHIPMENT_ACCESSORIAL,
                         l_rec.INCOTERM_ID,
                         l_rec.Min_Rate_Type,
                         l_rec.Maximum_Rate,
                         l_rec.PACKAGE_TYPE_ID,
                         l_rec.CUSTOMER_RATE,
                         l_rec.Customer_Min_Charge,
                         l_rec.Calculated_Rate,
                         l_rec.Amount,
                         l_rec.BASE_AMOUNT,
                         l_rec.Base_Charge,
                         l_rec.RANGE_MIN_AMOUNT,
                         l_rec.RANGE_MAX_AMOUNT,
                         l_rec.Increment_Val,
                         l_rec.Mot_Id,
                         l_rec.Equipment_Id,
                         l_rec.Service_Level_Id,
                         l_rec.PROTECTION_LEVEL_ID,
                         l_rec.Billing_Method,
                         l_rec.Min_Longest_Dimension,
                         l_rec.Max_Longest_Dimension,
                         l_rec.Min_Second_Longest_Dimension,
                         l_rec.Max_Second_Longest_Dimension,
                         l_rec.Min_Third_Longest_Dimension,
                         l_rec.Max_Third_Longest_Dimension,
                         l_rec.Min_Length_Plus_Grith,
                         l_rec.Max_Length_Plus_Grith,
                         l_rec.Min_Weight,
                         l_rec.Max_Weight,
                         l_rec.Is_Zone_Stopoff,
                         l_rec.COMMODITY_CODE_ID,
                         l_rec.MAX_RANGE_COMMODITY_CODE_ID);

						 
            SELECT MAX (lane_accessorial_id)
              INTO v_lane_acces_id_seq
              FROM lane_accessorial
             WHERE     tc_company_id = vtccompanyid
                   AND lane_id = vlaneid
                   AND rating_lane_dtl_seq = vratingdtlseqnew;
				   
			INSERT INTO LANE_ACC_EXCLUDED_LIST (LANE_ACC_ID, EXCLUDED_ACC_ID)
			SELECT v_lane_acces_id_seq, EXCLUDED_ACC_ID
			FROM LANE_ACC_EXCLUDED_LIST
			WHERE LANE_ACC_ID = l_rec.lane_accessorial_id;
				   
            INSERT INTO lane_acc_feasible_incoterm (LANE_ACC_ID, INCOTERM_ID)
				SELECT v_lane_acces_id_seq, INCOTERM_ID
				FROM lane_acc_feasible_incoterm
				WHERE LANE_ACC_ID = l_rec.lane_accessorial_id;					
				   
            SELECT COUNT (*)
              INTO vcnt
              FROM rating_event
             WHERE     rating_lane_id = vlaneid
                   AND rating_lane_dtl_id = vratingdtlseqtemp
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NULL
                   AND lane_accessorial_id = l_rec.lane_accessorial_id;

            INSERT INTO rating_event (rating_event_id,
                                      rating_lane_id,
                                      rating_lane_dtl_id,
                                      rld_rate_id,
                                      tc_company_id,
                                      change_id,
                                      is_pending_auth,
                                      field_name,
                                      old_value,
                                      new_value,
                                      comments,
                                      last_updated_src_type,
                                      last_updated_src,
                                      last_updated_dttm,
                                      auth_src_type,
                                      auth_src,
                                      auth_dttm,
                                      lane_accessorial_id,
                                      reason_id,
                                      SPLIT)
               SELECT seq_rating_event_id.NEXTVAL,
                      rating_lane_id,
                      vratingdtlseqnew,
                      rld_rate_id,
                      tc_company_id,
                      change_id,
                      is_pending_auth,
                      field_name,
                      old_value,
                      new_value,
                      comments,
                      last_updated_src_type,
                      last_updated_src,
                      last_updated_dttm,
                      auth_src_type,
                      auth_src,
                      auth_dttm,
                      v_lane_acces_id_seq,
                      reason_id,
                      SPLIT
                 FROM rating_event
                WHERE     rating_lane_id = vlaneid
                      AND rating_lane_dtl_id = vratingdtlseqtemp
                      AND is_pending_auth = 1
                      AND rld_rate_id IS NULL
                      AND lane_accessorial_id = l_rec.lane_accessorial_id;
         END LOOP;
      END IF;

      IF (vIsRouting = 1)
      THEN
         INSERT INTO SURGE_CAPACITY (SURGE_CAPACITY_ID,
                                     TC_COMPANY_ID,
                                     LANE_ID,
                                     RG_LANE_DTL_SEQ,
                                     EFFECTIVE_DT,
                                     EXPIRATION_DT,
                                     WEEKLY_SURGE_CAPACITY,
                                     DAILY_SURGE_CAPACITY,
                                     SURGE_CAPACITY_SUN,
                                     SURGE_CAPACITY_MON,
                                     SURGE_CAPACITY_TUE,
                                     SURGE_CAPACITY_WED,
                                     SURGE_CAPACITY_THU,
                                     SURGE_CAPACITY_FRI,
                                     SURGE_CAPACITY_SAT,
									 IS_SURGE)
            SELECT SEQ_SURGE_CAPACITY_ID.NEXTVAL,
                   TC_COMPANY_ID,
                   LANE_ID,
                   vRatingDtlSeqNew,
                   EFFECTIVE_DT,
                   EXPIRATION_DT,
                   WEEKLY_SURGE_CAPACITY,
                   DAILY_SURGE_CAPACITY,
                   SURGE_CAPACITY_SUN,
                   SURGE_CAPACITY_MON,
                   SURGE_CAPACITY_TUE,
                   SURGE_CAPACITY_WED,
                   SURGE_CAPACITY_THU,
                   SURGE_CAPACITY_FRI,
                   SURGE_CAPACITY_SAT,
				   IS_SURGE
              FROM SURGE_CAPACITY
             WHERE     TC_COMPANY_ID = vTCCompanyId
                   AND LANE_ID = vLaneId
                   AND RG_LANE_DTL_SEQ = vRatingDtlSeq;
      END IF;

      IF (vIsSailing = 1)
      THEN
         INSERT INTO SAILING_SCHDL_BY_DATE (TC_COMPANY_ID,
                                            LANE_ID,
                                            LANE_DTL_SEQ,
                                            SAIL_SCHED_BY_DATE_ID,
                                            DEPARTURE_DTTM,
                                            ARRIVAL_DTTM,
                                            CUTOFF_DTTM,
                                            PICKUP_DTTM,
                                            TRANSIT_TIME_VALUE,
                                            TRANSIT_TIME_STANDARD_UOM,
                                            RESOURCE_REF_EXTERNAL,
                                            RESOURCE_NAME_EXTERNAL,
                                            COMMENTS,
                                            DESCRIPTION)
            SELECT TC_COMPANY_ID,
                   LANE_ID,
                   vRatingDtlSeqNew,
                   SEQ_SAIL_SCHED_BY_DATE_ID.NEXTVAL,
                   DEPARTURE_DTTM,
                   ARRIVAL_DTTM,
                   CUTOFF_DTTM,
                   PICKUP_DTTM,
                   TRANSIT_TIME_VALUE,
                   TRANSIT_TIME_STANDARD_UOM,
                   RESOURCE_REF_EXTERNAL,
                   RESOURCE_NAME_EXTERNAL,
                   COMMENTS,
                   DESCRIPTION
              FROM SAILING_SCHDL_BY_DATE
             WHERE     TC_COMPANY_ID = vTCCompanyId
                   AND LANE_ID = vLaneId
                   AND LANE_DTL_SEQ = vRatingDtlSeq;

         week_day_sql :=
               ' SELECT SAIL_SCHED_BY_WEEK_ID '
            || ' FROM SAILING_SCHDL_BY_WEEK '
            || ' WHERE tc_company_id = :vTCCompanyId'
            || ' AND lane_id = :vLaneId'
            || ' AND lane_dtl_seq = :vRatingDtlSeq';

         OPEN week_day_cursor FOR week_day_sql
            USING vTCCompanyId, vLaneId, vRatingDtlSeq;

         LOOP
            FETCH week_day_cursor INTO vWeekId;

            EXIT WHEN week_day_cursor%NOTFOUND;

            INSERT INTO SAILING_SCHDL_BY_WEEK (TC_COMPANY_ID,
                                               LANE_ID,
                                               LANE_DTL_SEQ,
                                               SAIL_SCHED_BY_WEEK_ID,
                                               EFFECTIVE_DT,
                                               EXPIRATION_DT,
                                               RESOURCE_REF_EXTERNAL,
                                               RESOURCE_NAME_EXTERNAL,
                                               COMMENTS,
                                               DESCRIPTION)
               SELECT TC_COMPANY_ID,
                      LANE_ID,
                      vRatingDtlSeqNew,
                      SEQ_SAIL_SCHED_BY_WEEK_ID.NEXTVAL,
                      EFFECTIVE_DT,
                      EXPIRATION_DT,
                      RESOURCE_REF_EXTERNAL,
                      RESOURCE_NAME_EXTERNAL,
                      COMMENTS,
                      DESCRIPTION
                 FROM SAILING_SCHDL_BY_WEEK
                WHERE     TC_COMPANY_ID = vTCCompanyId
                      AND LANE_ID = vLaneId
                      AND LANE_DTL_SEQ = vRatingDtlSeq
                      AND SAIL_SCHED_BY_WEEK_ID = vWeekId;



            INSERT INTO SAILING_SCHDL_BY_WEEK_DAY (TC_COMPANY_ID,
                                                   LANE_ID,
                                                   LANE_DTL_SEQ,
                                                   SAIL_SCHED_BY_WEEK_ID,
                                                   SAIL_SCHED_BY_WEEK_DAY_ID,
                                                   DEPARTURE_DAYOFWEEK,
                                                   DEPARTURE_TIME,
                                                   ARRIVAL_DAYOFWEEK,
                                                   ARRIVAL_TIME,
                                                   CUTOFF_DAYOFWEEK,
                                                   CUTOFF_TIME,
                                                   PICKUP_DAYOFWEEK,
                                                   PICKUP_TIME,
                                                   TRANSIT_TIME_VALUE,
                                                   TRANSIT_TIME_STANDARD_UOM,
                                                   TRANSIT_TIME_WEEK)
               SELECT TC_COMPANY_ID,
                      LANE_ID,
                      vRatingDtlSeqNew,
                      SEQ_SAIL_SCHED_BY_WEEK_ID.CURRVAL,
                      SEQ_SAIL_SCHED_BY_WEEK_DAY_ID.NEXTVAL,
                      DEPARTURE_DAYOFWEEK,
                      DEPARTURE_TIME,
                      ARRIVAL_DAYOFWEEK,
                      ARRIVAL_TIME,
                      CUTOFF_DAYOFWEEK,
                      CUTOFF_TIME,
                      PICKUP_DAYOFWEEK,
                      PICKUP_TIME,
                      TRANSIT_TIME_VALUE,
                      TRANSIT_TIME_STANDARD_UOM,
                      TRANSIT_TIME_WEEK
                 FROM SAILING_SCHDL_BY_WEEK_DAY
                WHERE     TC_COMPANY_ID = vTCCompanyId
                      AND LANE_ID = vLaneId
                      AND LANE_DTL_SEQ = vRatingDtlSeq
                      AND SAIL_SCHED_BY_WEEK_ID = vWeekId;
         END LOOP;

         CLOSE week_day_cursor;
      END IF;
   END COPY_CHILD_ROW;

   PROCEDURE REPLICATE_LANE_DETAIL (
	  laneDtlId             OUT NUMBER,
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vLaneId         IN COMB_LANE.LANE_ID%TYPE,
      vRatingDtlSeq   IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE)
   IS
      vRatingDtlSeqNew   COMB_LANE_DTL.LANE_DTL_SEQ%TYPE;
   BEGIN
      SELECT MAX (LANE_DTL_SEQ) + 1
        INTO vRatingDtlSeqNew
        FROM COMB_LANE_DTL
       WHERE TC_COMPANY_ID = vTCCompanyID AND LANE_ID = vLaneID;
	   laneDtlId:= vRatingDtlSeqNew;
      INSERT INTO COMB_LANE_DTL (TC_COMPANY_ID,
                                 LANE_ID,
                                 LANE_DTL_SEQ,
                                 LANE_DTL_STATUS,
                                 IS_RATING,
                                 IS_ROUTING,
                                 IS_PREFERRED,
                                 REP_TP_FLAG,
                                 LAST_UPDATED_SOURCE_TYPE,
                                 LAST_UPDATED_SOURCE,
                                 LAST_UPDATED_DTTM,
                                 CARRIER_ID,
                                 MOT_ID,
                                 EQUIPMENT_ID,
                                 SERVICE_LEVEL_ID,
                                 SCNDR_CARRIER_ID,
                                 PROTECTION_LEVEL_ID,
                                 O_SHIP_VIA,
                                 D_SHIP_VIA,
                                 CONTRACT_NUMBER,
                                 CUSTOM_TEXT1,
                                 CUSTOM_TEXT2,
                                 CUSTOM_TEXT3,
                                 CUSTOM_TEXT4,
                                 CUSTOM_TEXT5,
								 VOYAGE,
                                 EFFECTIVE_DT,
                                 EXPIRATION_DT,
                                 TIER_ID,
                                 RANK,
                                 WEEKLY_CAPACITY,
                                 DAILY_CAPACITY,
                                 CAPACITY_SUN,
                                 CAPACITY_MON,
                                 CAPACITY_TUE,
                                 CAPACITY_WED,
                                 CAPACITY_THU,
                                 CAPACITY_FRI,
                                 CAPACITY_SAT,
                                 WEEKLY_COMMITMENT,
                                 DAILY_COMMITMENT,
                                 COMMITMENT_SUN,
                                 COMMITMENT_MON,
                                 COMMITMENT_TUE,
                                 COMMITMENT_WED,
                                 COMMITMENT_THU,
                                 COMMITMENT_FRI,
                                 COMMITMENT_SAT,
                                 WEEKLY_COMMIT_PCT,
                                 DAILY_COMMIT_PCT,
                                 COMMIT_PCT_SUN,
                                 COMMIT_PCT_MON,
                                 COMMIT_PCT_TUE,
                                 COMMIT_PCT_WED,
                                 COMMIT_PCT_THU,
                                 COMMIT_PCT_FRI,
                                 COMMIT_PCT_SAT,
                                 FIXED_TRANSIT_STANDARD_UOM,
                                 FIXED_TRANSIT_VALUE,
                                 IS_SAILING,
                                 SAILING_FREQUENCY_TYPE,
                                 SAILING_SCHEDULE_NAME,
                                 MASTER_LANE_ID,
                                 TT_TOLERANCE_FACTOR,
                                 SP_LPN_TYPE,
                                 SP_MIN_LPN_COUNT,
                                 SP_MAX_LPN_COUNT,
                                 SP_MIN_WEIGHT,
                                 SP_MAX_WEIGHT,
                                 SP_MIN_VOLUME,
                                 SP_MAX_VOLUME,
                                 SP_MIN_LINEAR_FEET,
                                 SP_MAX_LINEAR_FEET,
                                 SP_MIN_MONETARY_VALUE,
                                 SP_MAX_MONETARY_VALUE,
                                 SP_CURRENCY_CODE,
                                 HAS_SHIPPING_PARAM,
                                 CUBING_INDICATOR,
                                 OVERRIDE_CODE,
                                 PREFERENCE_BONUS_VALUE)
         SELECT TC_COMPANY_ID,
                LANE_ID,
                vRatingDtlSeqNew,
                0,
                0,
                IS_ROUTING,
                IS_PREFERRED,
                REP_TP_FLAG,
                3,
                'OMS',
                Getdate (),
                CARRIER_ID,
                MOT_ID,
                EQUIPMENT_ID,
                SERVICE_LEVEL_ID,
                SCNDR_CARRIER_ID,
                PROTECTION_LEVEL_ID,
                O_SHIP_VIA,
                D_SHIP_VIA,
                CONTRACT_NUMBER,
                CUSTOM_TEXT1,
                CUSTOM_TEXT2,
                CUSTOM_TEXT3,
                CUSTOM_TEXT4,
                CUSTOM_TEXT5,
				VOYAGE,
                EFFECTIVE_DT,
                EXPIRATION_DT,
                TIER_ID,
                RANK,
                WEEKLY_CAPACITY,
                DAILY_CAPACITY,
                CAPACITY_SUN,
                CAPACITY_MON,
                CAPACITY_TUE,
                CAPACITY_WED,
                CAPACITY_THU,
                CAPACITY_FRI,
                CAPACITY_SAT,
                WEEKLY_COMMITMENT,
                DAILY_COMMITMENT,
                COMMITMENT_SUN,
                COMMITMENT_MON,
                COMMITMENT_TUE,
                COMMITMENT_WED,
                COMMITMENT_THU,
                COMMITMENT_FRI,
                COMMITMENT_SAT,
                WEEKLY_COMMIT_PCT,
                DAILY_COMMIT_PCT,
                COMMIT_PCT_SUN,
                COMMIT_PCT_MON,
                COMMIT_PCT_TUE,
                COMMIT_PCT_WED,
                COMMIT_PCT_THU,
                COMMIT_PCT_FRI,
                COMMIT_PCT_SAT,
                FIXED_TRANSIT_STANDARD_UOM,
                FIXED_TRANSIT_VALUE,
                IS_SAILING,
                SAILING_FREQUENCY_TYPE,
                SAILING_SCHEDULE_NAME,
                MASTER_LANE_ID,
                TT_TOLERANCE_FACTOR,
                SP_LPN_TYPE,
                SP_MIN_LPN_COUNT,
                SP_MAX_LPN_COUNT,
                SP_MIN_WEIGHT,
                SP_MAX_WEIGHT,
                SP_MIN_VOLUME,
                SP_MAX_VOLUME,
                SP_MIN_LINEAR_FEET,
                SP_MAX_LINEAR_FEET,
                SP_MIN_MONETARY_VALUE,
                SP_MAX_MONETARY_VALUE,
                SP_CURRENCY_CODE,
                HAS_SHIPPING_PARAM,
                CUBING_INDICATOR,
                OVERRIDE_CODE,
                PREFERENCE_BONUS_VALUE
           FROM COMB_LANE_DTL
          WHERE     TC_COMPANY_ID = vTCCompanyId
                AND LANE_ID = vLaneId
                AND LANE_DTL_SEQ = vRatingDtlSeq;

      COPY_CHILD_ROW (vTCCompanyId,
                      vLaneId,
                      -1,
                      vRatingDtlSeq,
                      vRatingDtlSeqNew,
                      0,
                      1,
                      1,'OMS');
   END REPLICATE_LANE_DETAIL;


   PROCEDURE ADJUST_IS_BUDGETED (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vLaneId                IN rating_lane.LANE_ID%TYPE,
      vRatingDtlSeq          IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vEffectiveDT           IN rating_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN rating_lane_dtl.EXPIRATION_DT%TYPE,
      vMot                   IN rating_lane_dtl.MOT_ID%TYPE,
      vEquipment             IN rating_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN rating_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN RATING_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN rating_lane_dtl.PROTECTION_LEVEL_ID%TYPE,
      vCustomText1           IN COMB_LANE_DTL.CUSTOM_TEXT1%TYPE,
      vCustomText2           IN COMB_LANE_DTL.CUSTOM_TEXT2%TYPE,
      vCustomText3           IN COMB_LANE_DTL.CUSTOM_TEXT3%TYPE,
      vCustomText4           IN COMB_LANE_DTL.CUSTOM_TEXT4%TYPE,
      vCustomText5           IN COMB_LANE_DTL.CUSTOM_TEXT5%TYPE)
   IS
      adj_is_budg_sql   VARCHAR2 (2000);
   BEGIN
      adj_is_budg_sql :=
            ' UPDATE rating_lane_dtl '
         || ' SET is_budgeted = 0'
         || ' WHERE tc_company_id = :vTCCompanyId'
         ||                                                 --vTCCompanyId  ||
           ' AND lane_id = :vLaneId'
         ||                                                       --vLaneid ||
           ' AND rating_lane_dtl_seq != :vRatingDtlSeq'
         ||                                                 --vRatingDtlSeq ||
           ' AND rating_lane_dtl_status = 0 '
         || ' AND is_budgeted = 1 '
         || ' AND expiration_dt  >= :vEffectiveDT '
         ||                             --|| TO_CHAR(vEffectiveDT) || ''' ' ||
           ' AND effective_dt <= :vExpirationDT ' --'' || TO_CHAR(vExpirationDT) || ''' ' ;
                                                 ;
      adj_is_budg_sql :=
         adj_is_budg_sql
         || GET_BUDG_RESOURCE_WHERE_CLAUSE (vMot,
                                            vEquipment,
                                            vServiceLevel,
                                            p_scndr_carrier_code,
                                            vProtectionLevel,
                                            vCustomText1,
                                            vCustomText2,
                                            vCustomText3,
                                            vCustomText4,
                                            vCustomText5);

      EXECUTE IMMEDIATE adj_is_budg_sql
         USING vTCCompanyId,
               vLaneid,
               vRatingDtlSeq,
               vEffectiveDT,
               vExpirationDT;
   END ADJUST_IS_BUDGETED;

   PROCEDURE INSERT_RATING_LANE_ERROR (
      vTCCompanyId         IN COMPANY.COMPANY_ID%TYPE,
      vLaneId              IN rating_lane.LANE_ID%TYPE,
      vErrorMsgDesc        IN RATING_LANE_ERRORS.ERROR_MSG_DESC%TYPE,
      p_error_msg_id       IN RATING_LANE_ERRORS.ERROR_MSG_ID%TYPE DEFAULT NULL,
      p_error_param_list   IN RATING_LANE_ERRORS.ERROR_PARAM_LIST%TYPE DEFAULT NULL)
   AS
      vBatchId   RATING_LANE_ERRORS.BATCH_ID%TYPE;
   BEGIN
      SELECT BATCH_ID
        INTO vBatchId
        FROM IMPORT_RATING_LANE
       WHERE LANE_ID = vLaneId AND TC_COMPANY_ID = vTCCompanyId;

      INSERT INTO RATING_LANE_ERRORS (RATING_LANE_ERROR,
                                      TC_COMPANY_ID,
                                      LANE_ID,
                                      ERROR_MSG_DESC,
                                      BATCH_ID,
                                      ERROR_MSG_ID,
                                      ERROR_PARAM_LIST)
           VALUES (SEQ_RATING_LANE_ERROR.NEXTVAL,
                   vTCCompanyId,
                   vLaneId,
                   vErrorMsgDesc,
                   vBatchId,
                   p_error_msg_id,
                   p_error_param_list);

      COMMIT;
   END INSERT_RATING_LANE_ERROR;

   PROCEDURE UPDATE_RATING_LANE_ERROR (
      vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
      vBatchId       IN RATING_LANE_ERRORS.BATCH_ID%TYPE)
   AS
      vLaneId   rating_lane.LANE_ID%TYPE;
   BEGIN
      SELECT LANE_ID
        INTO vLaneId
        FROM IMPORT_RATING_LANE
       WHERE BATCH_ID = vBatchId AND TC_COMPANY_ID = vTCCompanyId;

      UPDATE RATING_LANE_ERRORS
         SET LANE_ID = vLaneId
       WHERE     BATCH_ID = vBatchId
             AND TC_COMPANY_ID = vTCCompanyId
             AND LANE_ID = 0;

      COMMIT;
   END UPDATE_RATING_LANE_ERROR;

   /*************************************
   *
   *  Returns the concatenated location string
   *  of the location type then the relevent
   *  data for the particular location type
   *
   *************************************/
   FUNCTION Fngetlocationstring (
      aLocType       IN VARCHAR2,
      aFacilityId    IN rating_lane.o_facility_id%TYPE,
      aCity          IN rating_lane.o_city%TYPE,
      aCounty        IN rating_lane.o_county%TYPE,
      aStateProv     IN rating_lane.o_state_prov%TYPE,
      aPostalCode    IN rating_lane.o_postal_code%TYPE,
      aCountryCode   IN rating_lane.o_country_code%TYPE,
      aZoneId        IN rating_lane.o_zone_id%TYPE)
      RETURN VARCHAR2
   IS
      vLocationString   VARCHAR2 (50);
      cvDelimiter       VARCHAR2 (2) := '~';
   BEGIN
      SELECT DECODE (
                aLocType,
                'CO', aLocType || cvDelimiter || aCountryCode,
                'FA', aLocType || cvDelimiter || TO_CHAR (aFacilityId),
                'CS',    aLocType
                      || cvDelimiter
                      || aCity
                      || cvDelimiter
                      || aStateProv
                      || cvDelimiter
                      || aCountryCode,
                'ST',    aLocType
                      || cvDelimiter
                      || aStateProv
                      || cvDelimiter
                      || aCountryCode,
                'P2',    aLocType
                      || cvDelimiter
                      || SUBSTR (aPostalCode, 1, 2)
                      || cvDelimiter
                      || aCountryCode,
                'P3',    aLocType
                      || cvDelimiter
                      || SUBSTR (aPostalCode, 1, 3)
                      || cvDelimiter
                      || aCountryCode,
                'P4',    aLocType
                      || cvDelimiter
                      || SUBSTR (aPostalCode, 1, 4)
                      || cvDelimiter
                      || aCountryCode,
                'P5',    aLocType
                      || cvDelimiter
                      || SUBSTR (aPostalCode, 1, 5)
                      || cvDelimiter
                      || aCountryCode,
                'P6',    aLocType
                      || cvDelimiter
                      || SUBSTR (aPostalCode, 1, 6)
                      || cvDelimiter
                      || aCountryCode,
                'ZN', aLocType || cvDelimiter || aZoneId,
                NULL)
        INTO vLocationString
        FROM DUAL;

      RETURN vLocationString;
   END;

   FUNCTION INSERT_RATING_LANE (
      vTCCompanyId      IN COMPANY.COMPANY_ID%TYPE,
      vUser             IN rating_lane.LAST_UPDATED_SOURCE%TYPE,
      vBusinessUnit     IN BUSINESS_UNIT.BUSINESS_UNIT%TYPE,
      vLaneHierarchy    IN rating_lane.LANE_HIERARCHY%TYPE,
      vOLocType         IN rating_lane.O_LOC_TYPE%TYPE,
      vOFacilityId      IN FACILITY.FACILITY_ID%TYPE,
      vOFacilityAlias   IN FACILITY_ALIAS.FACILITY_ALIAS_ID%TYPE,
      vOCity            IN rating_lane.O_CITY%TYPE,
      vOStateProv       IN STATE_PROV.STATE_PROV%TYPE,
      vOCounty          IN rating_lane.O_COUNTY%TYPE,
      vOPostal          IN POSTAL_CODE.POSTAL_CODE%TYPE,
      vOCountry         IN COUNTRY.COUNTRY_CODE%TYPE,
      vOZone            IN ZONE.ZONE_ID%TYPE,
      vDLocType         IN rating_lane.D_LOC_TYPE%TYPE,
      vDFacilityId      IN FACILITY.FACILITY_ID%TYPE,
      vDFacilityAlias   IN FACILITY_ALIAS.FACILITY_ALIAS_ID%TYPE,
      vDCity            IN rating_lane.D_CITY%TYPE,
      vDStateProv       IN STATE_PROV.STATE_PROV%TYPE,
      vDCounty          IN rating_lane.D_COUNTY%TYPE,
      vDPostal          IN POSTAL_CODE.POSTAL_CODE%TYPE,
      vDCountry         IN COUNTRY.COUNTRY_CODE%TYPE,
      vDZone            IN ZONE.ZONE_ID%TYPE,
      vBillingMethod    IN rating_lane.BILLING_METHOD%TYPE,
      vIncotermId       IN rating_lane.INCOTERM_ID%TYPE,
      vCustomerId       IN rating_lane.CUSTOMER_ID%TYPE,
      vRatingLaneId     IN rating_lane.LANE_ID%TYPE                    /*JMC*/
                                                   )
      RETURN NUMBER
   IS
      vLaneId                  RATING_LANE.LANE_ID%TYPE;
      vWhereClause             VARCHAR2 (2000);
      vsqlStatement            VARCHAR2 (2000);
      vOSearchLocation         RATING_LANE.O_SEARCH_LOCATION%TYPE;
      vDSearchLocation         RATING_LANE.D_SEARCH_LOCATION%TYPE;
      vLastUpdatedSource       RATING_LANE.LAST_UPDATED_SOURCE%TYPE;
      vLastUpdatedSourceType   RATING_LANE.LAST_UPDATED_SOURCE_TYPE%TYPE;
   BEGIN
      vOSearchLocation :=
         Fngetlocationstring (aLocType       => vOLocType,
                              aFacilityId    => vOFacilityId,
                              aCity          => replace(vOCity,chr(39),chr(34)),
                              aCounty        => vOCounty,
                              aStateProv     => vOStateProv,
                              aPostalCode    => vOPostal,
                              aCountryCode   => vOCountry,
                              aZoneId        => vOZone);
      vDSearchLocation :=
         Fngetlocationstring (aLocType       => vDLocType,
                              aFacilityId    => vDFacilityId,
                              aCity          => replace(vDCity,chr(39),chr(34)),
                              aCounty        => vDCounty,
                              aStateProv     => vDStateProv,
                              aPostalCode    => vDPostal,
                              aCountryCode   => vDCountry,
                              aZoneId        => vDZone);
      /* IF(vBusinessUnit IS NULL) THEN
        SELECT lane_id
        INTO vLaneId
        FROM
        (
         SELECT lane_id FROM COMB_LANE
         WHERE tc_company_id = vTcCompanyId
         AND LANE_STATUS=0
         AND RG_QUALIFIER IS NULL
               AND O_SEARCH_LOCATION = voSearchLocation
         AND D_SEARCH_LOCATION = vDSearchLocation
         ORDER BY IS_RATING DESC, is_routing DESC, is_sailing DESC
        )
        WHERE ROWNUM = 1;
       ELSE
        SELECT lane_id
        INTO vLaneId
        FROM
        (
         SELECT lane_id FROM COMB_LANE
         WHERE tc_company_id = vTcCompanyId
         AND LANE_STATUS=0
         AND RG_QUALIFIER IS NULL
               AND O_SEARCH_LOCATION = voSearchLocation
         AND D_SEARCH_LOCATION = vDSearchLocation
         ORDER BY IS_RATING DESC, is_routing DESC, is_sailing DESC
        )
        WHERE ROWNUM = 1;
       END IF;
      */


      vsqlStatement := 'SELECT lane_id FROM ( SELECT lane_id FROM COMB_LANE';

      vWhereClause :=
            ' WHERE tc_company_id = '
         || vTcCompanyId
         || ' AND LANE_STATUS = 0 '
         || ' AND RG_QUALIFIER IS NULL '
         || ' AND O_SEARCH_LOCATION = '''
         || voSearchLocation
         || ''''
         || ' AND D_SEARCH_LOCATION = '''
         || vDSearchLocation
         || '''';

      vWhereClause := vWhereClause || ' and CUSTOMER_ID ';

      IF (vCustomerId IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vCustomerId || '''';
      END IF;

      vWhereClause := vWhereClause || ' and BILLING_METHOD ';

      IF (vBillingMethod IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vBillingMethod || '''';
      END IF;

      vWhereClause := vWhereClause || ' and INCOTERM_ID ';

      IF (vIncotermId IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vIncotermId || '''';
      END IF;


      vsqlStatement := vsqlStatement || vWhereClause;
      vsqlStatement :=
         vsqlStatement
         || ' ORDER BY IS_RATING DESC, is_routing DESC, is_sailing DESC )';
      vsqlStatement := vsqlStatement || ' where rownum = 1 ';

      EXECUTE IMMEDIATE vsqlStatement INTO vLaneId;


      UPDATE COMB_LANE
         SET IS_RATING = 1, LAST_UPDATED_DTTM = SYSDATE
       WHERE     TC_COMPANY_ID = vTCCompanyId
             AND LANE_ID = vLaneId
             AND IS_RATING = 0;

      COMMIT;

      RETURN vLaneId;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         BEGIN
            -- syco merge
            IF (vUser IS NOT NULL)
            THEN
               vLastUpdatedSource := vUser;
               vLastUpdatedSourceType := 1;
            ELSE
               vLastUpdatedSource := 'OMS';
               vLastUpdatedSourceType := 3;
            END IF;

            /* JMC Removed select max value*/
            INSERT INTO COMB_LANE (TC_COMPANY_ID,
                                   LANE_ID,
                                   LANE_HIERARCHY,
                                   LANE_STATUS,
                                   IS_RATING,
                                   O_LOC_TYPE,
                                   O_FACILITY_ID,
                                   O_FACILITY_ALIAS_ID,
                                   O_CITY,
                                   O_STATE_PROV,
                                   O_COUNTY,
                                   O_POSTAL_CODE,
                                   O_COUNTRY_CODE,
                                   O_ZONE_ID,
                                   D_LOC_TYPE,
                                   D_FACILITY_ID,
                                   D_FACILITY_ALIAS_ID,
                                   D_CITY,
                                   D_STATE_PROV,
                                   D_COUNTY,
                                   D_POSTAL_CODE,
                                   D_COUNTRY_CODE,
                                   D_ZONE_ID,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_SOURCE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_DTTM,
                                   BILLING_METHOD,
                                   INCOTERM_ID,
                                   CUSTOMER_ID)
                 VALUES (vTCCompanyId,
                         vRatingLaneId,                                /*JMC*/
                         vLaneHierarchy,
                         0,
                         1,
                         vOLocType,
                         vOFacilityId,
                         vOFacilityAlias,
                         vOCity,
                         vOStateProv,
                         vOCounty,
                         vOPostal,
                         vOCountry,
                         vOZone,
                         vDLocType,
                         vDFacilityId,
                         vDFacilityAlias,
                         vDCity,
                         vDStateProv,
                         vDCounty,
                         vDPostal,
                         vDCountry,
                         vDZone,
                         vLastUpdatedSourceType,
                         vLastUpdatedSource,
                         SYSDATE,
                         vLastUpdatedSourceType,
                         vLastUpdatedSource,
                         SYSDATE,
                         vBillingMethod,
                         vIncotermId,
                         vCustomerId);

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               DBMS_OUTPUT.PUT_LINE (vRatingLaneId);
               DBMS_OUTPUT.PUT_LINE (voSearchLocation);
               DBMS_OUTPUT.PUT_LINE (vdSearchLocation);
               RAISE;
         END;

         RETURN vRatingLaneId;
   END INSERT_RATING_LANE;

   -- TALON DB-CR:68082 Merge
   -- max_lane_dtl_seq,rld_seq Data Type Changed
   FUNCTION INSERT_RATING_LANE_DTL (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vUser                  IN rating_lane.LAST_UPDATED_SOURCE%TYPE,
      vRatingLaneId          IN rating_lane.LANE_ID%TYPE,
      vCarrierCode           IN rating_lane_dtl.CARRIER_ID%TYPE,
      vMot                   IN rating_lane_dtl.MOT_ID%TYPE,
      vEquipment             IN rating_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN rating_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN RATING_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN rating_lane_dtl.PROTECTION_LEVEL_ID%TYPE,
      vIsBudgeted            IN rating_lane_dtl.IS_BUDGETED%TYPE,
      vEffectiveDT           IN rating_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN rating_lane_dtl.EXPIRATION_DT%TYPE,
      vImpRateDtlStatus      IN IMPORT_RATING_LANE_DTL.IMPORT_RATING_DTL_STATUS%TYPE,
      vOriginShipVia         IN rating_lane_dtl.O_SHIP_VIA%TYPE,
      vDestinationShipVia    IN rating_lane_dtl.D_SHIP_VIA%TYPE,
      vContractNumber        IN rating_lane_dtl.CONTRACT_NUMBER%TYPE,
      vCustomText1           IN rating_lane_dtl.CUSTOM_TEXT1%TYPE,
      vCustomText2           IN rating_lane_dtl.CUSTOM_TEXT2%TYPE,
      vCustomText3           IN rating_lane_dtl.CUSTOM_TEXT3%TYPE,
      vCustomText4           IN rating_lane_dtl.CUSTOM_TEXT4%TYPE,
      vCustomText5           IN rating_lane_dtl.CUSTOM_TEXT5%TYPE,
      vPackageId             IN rating_lane_dtl.PACKAGE_ID%TYPE,
      vPackageName           IN rating_lane_dtl.PACKAGE_NAME%TYPE,
	  vVoyage				 IN rating_lane_dtl.VOYAGE%TYPE)
      RETURN NUMBER
   IS
      max_lane_dtl_seq         NUMBER (12);
      rld_seq                  NUMBER (12);
      rating_dtl_seq_cursor    ref_curtype;
      expire_dtl_sql           VARCHAR (2000);
      vLastUpdatedSource       COMB_LANE_DTL.LAST_UPDATED_SOURCE%TYPE;
      vLastUpdatedSourceType   COMB_LANE_DTL.LAST_UPDATED_SOURCE_TYPE%TYPE;
   BEGIN
      /* vImpRateDtlStatus is not the Lane detail status. Possible values are :
       0 for "import as regular lane"
        and 1 for "expire this lane"
      */
      IF (vImpRateDtlStatus = 1)
      THEN
         expire_dtl_sql :=
               '/* insert_rating_lane_dtl */'
            || ' SELECT NVL(rating_lane_dtl_seq, 0) '
            || ' FROM RATING_LANE_DTL '
            || ' WHERE TC_COMPANY_ID = '
            || vTCCompanyId
            || ' AND LANE_ID = '
            || vRatingLaneId
            || ' AND EFFECTIVE_DT = '''
            || TO_CHAR (vEffectiveDT)
            || ''' '
            || ' AND EXPIRATION_DT = '''
            || TO_CHAR (vExpirationDT)
            || ''' ';

         expire_dtl_sql :=
            expire_dtl_sql
            || GET_RESOURCE_WHERE_CLAUSE (vCarrierCode,
                                          vMot,
                                          vEquipment,
                                          vServiceLevel,
                                          p_scndr_carrier_code,
                                          vProtectionLevel,
                                          vOriginShipVia,
                                          vDestinationShipVia
										  );
         rld_seq := 0;

         OPEN rating_dtl_seq_cursor FOR expire_dtl_sql;

         LOOP
            FETCH rating_dtl_seq_cursor INTO rld_seq;

            EXIT WHEN rating_dtl_seq_cursor%NOTFOUND;

            UPDATE RATING_LANE_DTL
               SET EXPIRATION_DT = EFFECTIVE_DT - 1
             WHERE     TC_COMPANY_ID = vTCCompanyId
                   AND LANE_ID = vRatingLaneId
                   AND RATING_LANE_DTL_SEQ = rld_seq;

            COMMIT;
         END LOOP;

         CLOSE rating_dtl_seq_cursor;

         RETURN rld_seq;
      ELSE
         -- nTime := dbms_utility.get_time();
         SELECT (NVL (MAX (lane_dtl_seq), 0) + 1)
           INTO max_lane_dtl_seq
           FROM COMB_LANE_DTL
          WHERE tc_company_id = vTCCompanyId AND lane_id = vRatingLaneId;

         --insert into TIME_IMPORT_LANE_DTL_KDM values ('INSERT_RATING_LANE_DTL','SELECT MAX',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();

         -- sysco merge
         IF (vUser IS NOT NULL)
         THEN
            vLastUpdatedSource := vUser;
            vLastUpdatedSourceType := 1;
         ELSE
            vLastUpdatedSource := 'OMS';
            vLastUpdatedSourceType := 3;
         END IF;

         INSERT INTO COMB_LANE_DTL (TC_COMPANY_ID,
                                    LANE_ID,
                                    LANE_DTL_SEQ,
                                    IS_RATING,
                                    CARRIER_ID,
                                    MOT_ID,
                                    EQUIPMENT_ID,
                                    SERVICE_LEVEL_ID,
                                    PROTECTION_LEVEL_ID,
                                    LAST_UPDATED_SOURCE_TYPE,
                                    LAST_UPDATED_SOURCE,
                                    LAST_UPDATED_DTTM,
                                    LANE_DTL_STATUS,
                                    EFFECTIVE_DT,
                                    EXPIRATION_DT,
                                    IS_BUDGETED,
                                    SCNDR_CARRIER_ID,
                                    O_SHIP_VIA,
                                    D_SHIP_VIA,
                                    CONTRACT_NUMBER,
                                    CUSTOM_TEXT1,
                                    CUSTOM_TEXT2,
                                    CUSTOM_TEXT3,
                                    CUSTOM_TEXT4,
                                    CUSTOM_TEXT5,
                                    PACKAGE_ID,
                                    PACKAGE_NAME,
									VOYAGE )
              VALUES (vTCCompanyId,
                      vRatingLaneId,
                      max_lane_dtl_seq,
                      1,
                      vCarrierCode,
                      vMot,
                      vEquipment,
                      vServiceLevel,
                      vProtectionLevel,
                      vLastUpdatedSourceType,
                      vLastUpdatedSource,
                      SYSDATE,
                      0,
                      vEffectiveDT,
                      vExpirationDT,
                      vIsBudgeted,
                      p_scndr_carrier_code,
                      vOriginShipVia,
                      vDestinationShipVia,
                      vContractNumber,
                      vCustomText1,
                      vCustomText2,
                      vCustomText3,
                      vCustomText4,
                      vCustomText5,
                      vPackageId,
                      vPackageName,
					  vVoyage);

         COMMIT;

         --insert into TIME_IMPORT_LANE_DTL_KDM values ('INSERT_RATING_LANE_DTL','insert RLD',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();


         --insert into TIME_IMPORT_LANE_DTL_KDM values ('INSERT_RATING_LANE_DTL','ADJUST_IS_BUDGETED',dbms_utility.get_time() - nTime);
         -- nTime := dbms_utility.get_time();
         RETURN max_lane_dtl_seq;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         --nInsertTime := dbms_utility.get_time();
         SELECT (NVL (MAX (lane_dtl_seq), 0) + 1)
           INTO max_lane_dtl_seq
           FROM COMB_LANE_DTL
          WHERE tc_company_id = vTCCompanyId AND lane_id = vRatingLaneId;

         ----insert into TIME_IMPORT_LANE_DTL_KDM values ('IRD3','sel',dbms_utility.get_time() - nInsertTime);
         --nInsertTime := dbms_utility.get_time();
         INSERT INTO COMB_LANE_DTL (TC_COMPANY_ID,
                                    LANE_ID,
                                    LANE_DTL_SEQ,
                                    IS_RATING,
                                    CARRIER_ID,
                                    MOT_ID,
                                    EQUIPMENT_ID,
                                    SERVICE_LEVEL_ID,
                                    PROTECTION_LEVEL_ID,
                                    LAST_UPDATED_SOURCE_TYPE,
                                    LAST_UPDATED_SOURCE,
                                    LAST_UPDATED_DTTM,
                                    LANE_DTL_STATUS,
                                    EFFECTIVE_DT,
                                    EXPIRATION_DT,
                                    IS_BUDGETED,
                                    SCNDR_CARRIER_ID,
                                    O_SHIP_VIA,
                                    D_SHIP_VIA,
                                    CONTRACT_NUMBER,
                                    CUSTOM_TEXT1,
                                    CUSTOM_TEXT2,
                                    CUSTOM_TEXT3,
                                    CUSTOM_TEXT4,
                                    CUSTOM_TEXT5,
                                    PACKAGE_ID,
                                    PACKAGE_NAME,
									VOYAGE )
              VALUES (vTCCompanyId,
                      vRatingLaneId,
                      max_lane_dtl_seq,
                      1,
                      vCarrierCode,
                      vMot,
                      vEquipment,
                      vServiceLevel,
                      vProtectionLevel,
                      vLastUpdatedSourceType,
                      vLastUpdatedSource,
                      SYSDATE,
                      0,
                      vEffectiveDT,
                      TRUNC (SYSDATE - 1),
                      vIsBudgeted,
                      p_scndr_carrier_code,
                      vOriginShipVia,
                      vDestinationShipVia,
                      vContractNumber,
                      vCustomText1,
                      vCustomText2,
                      vCustomText3,
                      vCustomText4,
                      vCustomText5,
                      vPackageId,
                      vPackageName,
					  vVoyage);

         COMMIT;
         ----insert into TIME_IMPORT_LANE_DTL_KDM values ('IRD3','ins',dbms_utility.get_time() - nInsertTime);
         --nInsertTime := dbms_utility.get_time();
         RETURN max_lane_dtl_seq;
   END INSERT_RATING_LANE_DTL;

   PROCEDURE INSERT_RATING_LANE_DTL_RATE (
      vTCCompanyId             IN COMPANY.COMPANY_ID%TYPE,
      vRatingLaneId            IN rating_lane.LANE_ID%TYPE,
      vRatingLaneDtlId         IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vMinSize                 IN RATING_LANE_DTL_RATE.MINIMUM_SIZE%TYPE,
      vMaxSize                 IN RATING_LANE_DTL_RATE.MAXIMUM_SIZE%TYPE,
      vSizeUOM                 IN RATING_LANE_DTL_RATE.SIZE_UOM_ID%TYPE,
      vRateCalcMethod          IN RATING_LANE_DTL_RATE.RATE_CALC_METHOD%TYPE,
      vRateUOM                 IN RATING_LANE_DTL_RATE.RATE_UOM%TYPE,
      vRate                    IN RATING_LANE_DTL_RATE.RATE%TYPE,
      vMinRate                 IN RATING_LANE_DTL_RATE.MINIMUM_RATE%TYPE,
      vCurrencyCode            IN RATING_LANE_DTL_RATE.CURRENCY_CODE%TYPE,
      vTariffId                IN RATING_LANE_DTL_RATE.TARIFF_ID%TYPE,
      vMinDistance             IN RATING_LANE_DTL_RATE.MIN_DISTANCE%TYPE,
      vMaxDistance             IN RATING_LANE_DTL_RATE.MAX_DISTANCE%TYPE,
      vDistanceUOM             IN RATING_LANE_DTL_RATE.DISTANCE_UOM%TYPE,
      vSupportsMSTL            IN RATING_LANE_DTL_RATE.SUPPORTS_MSTL%TYPE,
      vHasBH                   IN RATING_LANE_DTL_RATE.HAS_BH%TYPE,
      vMinCommodity            IN RATING_LANE_DTL_RATE.MIN_COMMODITY%TYPE,
      vMaxCommodity            IN RATING_LANE_DTL_RATE.MAX_COMMODITY%TYPE,
      vHasRT                   IN RATING_LANE_DTL_RATE.HAS_RT%TYPE,
      vParcelTier              IN RATING_LANE_DTL_RATE.PARCEL_TIER%TYPE,
      vCommodityCodeId         IN RATING_LANE_DTL_RATE.COMMODITY_CODE_ID%TYPE,
      vExcessWtRate            IN RATING_LANE_DTL_RATE.EXCESS_WT_RATE%TYPE,
      vPayeeCarrier            IN RATING_LANE_DTL_RATE.PAYEE_CARRIER_ID%TYPE,
      vMaxCommodityCodeId      IN RATING_LANE_DTL_RATE.MAX_RANGE_COMMODITY_CODE_ID%TYPE,
      VIsSurgeRate             IN RATING_LANE_DTL_RATE.IS_SURGE_RATE%TYPE,
      vMinRateType             IN RATING_LANE_DTL_RATE.MINIMUM_RATE_TYPE%TYPE,
      vBaseRate                IN RATING_LANE_DTL_RATE.BASE_RATE%TYPE,
      vMaxRate                 IN RATING_LANE_DTL_RATE.MAXIMUM_RATE%TYPE,
      vCustomerMinCharge       IN RATING_LANE_DTL_RATE.CUSTOMER_MIN_CHARGE%TYPE,
      vCustomerBaseRate        IN RATING_LANE_DTL_RATE.CUSTOMER_BASE_RATE%TYPE,
      vCustomerSurchargeRate   IN RATING_LANE_DTL_RATE.CUSTOMER_SURCHARGE_RATE%TYPE)
   AS
      max_lane_dtl_rate_seq   NUMBER (4);
      vTempExcessWtRate       RATING_LANE_DTL_RATE.EXCESS_WT_RATE%TYPE;
   BEGIN
      IF (vExcessWtRate IS NULL)
      THEN
         vTempExcessWtRate := 0.0;
      ELSE
         vTempExcessWtRate := vExcessWtRate;
      END IF;


      SELECT (NVL (MAX (rld_rate_seq), 0) + 1)
        INTO max_lane_dtl_rate_seq
        FROM RATING_LANE_DTL_RATE
       WHERE     tc_company_id = vTCCompanyId
             AND lane_id = vRatingLaneId
             AND rating_lane_dtl_seq = vRatingLaneDtlId;

      INSERT INTO RATING_LANE_DTL_RATE (TC_COMPANY_ID,
                                        LANE_ID,
                                        RATING_LANE_DTL_SEQ,
                                        RLD_RATE_SEQ,
                                        MINIMUM_SIZE,
                                        MAXIMUM_SIZE,
                                        SIZE_UOM_ID,
                                        RATE_CALC_METHOD,
                                        RATE_UOM,
                                        RATE,
                                        MINIMUM_RATE,
                                        CURRENCY_CODE,
                                        TARIFF_CODE_ID,
                                        MIN_DISTANCE,
                                        MAX_DISTANCE,
                                        DISTANCE_UOM,
                                        SUPPORTS_MSTL,
                                        HAS_BH,
                                        MIN_COMMODITY,
                                        MAX_COMMODITY,
                                        HAS_RT,
                                        PARCEL_TIER,
                                        COMMODITY_CODE_ID,
                                        EXCESS_WT_RATE,
                                        PAYEE_CARRIER_ID,
                                        MAX_RANGE_COMMODITY_CODE_ID,
                                        IS_SURGE_RATE,
                                        MINIMUM_RATE_TYPE,
                                        BASE_RATE,
                                        MAXIMUM_RATE,
                                        CUSTOMER_MIN_CHARGE,
                                        CUSTOMER_BASE_RATE,
                                        CUSTOMER_SURCHARGE_RATE)
           VALUES (vTCCompanyId,
                   vRatingLaneId,
                   vRatingLaneDtlId,
                   max_lane_dtl_rate_seq,
                   vMinSize,
                   vMaxSize,
                   vSizeUOM,
                   vRateCalcMethod,
                   vRateUOM,
                   vRate,
                   vMinRate,
                   vCurrencyCode,
                   vTariffId,
                   vMinDistance,
                   vMaxDistance,
                   vDistanceUOM,
                   vSupportsMSTL,
                   vHasBH,
                   vMinCommodity,
                   vMaxCommodity,
                   vHasRT,
                   vParcelTier,
                   vCommodityCodeId,
                   vTempExcessWtRate,
                   vPayeeCarrier,
                   vMaxCommodityCodeId,
                   VISSURGERATE,
                   vMinRateType,
                   vBaseRate,
                   vMaxRate,
                   vCustomerMinCharge,
                   vCustomerBaseRate,
                   vCustomerSurchargeRate);

      COMMIT;
   END INSERT_RATING_LANE_DTL_RATE;

   PROCEDURE INSERT_RATING_LANE_ACCESSORIAL (
      vTCCompanyId             IN COMPANY.COMPANY_ID%TYPE,
      vRatingLaneId            IN rating_lane.LANE_ID%TYPE,
      vRatingLaneDtlId         IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vAccessorialCode         IN LANE_ACCESSORIAL.ACCESSORIAL_ID%TYPE,
      vRate                    IN LANE_ACCESSORIAL.RATE%TYPE,
      vMinRate                 IN LANE_ACCESSORIAL.MINIMUM_RATE%TYPE,
      vCurrencyCode            IN LANE_ACCESSORIAL.CURRENCY_CODE%TYPE,
      p_minimum_size           IN LANE_ACCESSORIAL.MINIMUM_SIZE%TYPE,
      p_maximum_size           IN LANE_ACCESSORIAL.MAXIMUM_SIZE%TYPE,
      p_size_uom               IN LANE_ACCESSORIAL.SIZE_UOM_ID%TYPE,
      vIsApproved              IN LANE_ACCESSORIAL.IS_AUTO_APPROVE%TYPE,
      vEffectiveDT             IN LANE_ACCESSORIAL.EFFECTIVE_DT%TYPE,
      vExpirationDT            IN LANE_ACCESSORIAL.EXPIRATION_DT%TYPE,
      vPayeeCarrierCode        IN LANE_ACCESSORIAL.PAYEE_CARRIER_ID%TYPE,
      vIsShipmentAccessorial   IN LANE_ACCESSORIAL.IS_SHIPMENT_ACCESSORIAL%TYPE,
      vMinRateType             IN LANE_ACCESSORIAL.Min_Rate_Type%TYPE, -- MACR00159593 starts
      vMaximumRate             IN LANE_ACCESSORIAL.Maximum_Rate%TYPE,
      vPackageType             IN LANE_ACCESSORIAL.PACKAGE_TYPE_ID%TYPE,
      vCustomerRate            IN LANE_ACCESSORIAL.CUSTOMER_RATE%TYPE,
      vCustomerMinCharge       IN LANE_ACCESSORIAL.Customer_Min_Charge%TYPE,
      vCalculatedRate          IN LANE_ACCESSORIAL.Calculated_Rate%TYPE,
      vAmount                  IN LANE_ACCESSORIAL.Amount%TYPE,
      vBaseAmount              IN LANE_ACCESSORIAL.BASE_AMOUNT%TYPE,
      vBaseCharge              IN LANE_ACCESSORIAL.Base_Charge%TYPE,
      vRangeMinAmount          IN LANE_ACCESSORIAL.RANGE_MIN_AMOUNT%TYPE,
      vRangeMaxAmount          IN LANE_ACCESSORIAL.RANGE_MAX_AMOUNT%TYPE,
      vIncrement               IN LANE_ACCESSORIAL.Increment_Val%TYPE,
      vMot                     IN LANE_ACCESSORIAL.Mot_Id%TYPE,
      vEquipment               IN LANE_ACCESSORIAL.Equipment_Id%TYPE,
      vServiceLevel            IN LANE_ACCESSORIAL.Service_Level_Id%TYPE,
      vProtectionLevel         IN LANE_ACCESSORIAL.PROTECTION_LEVEL_ID%TYPE,
      vBillingMethod           IN LANE_ACCESSORIAL.Billing_Method%TYPE,
      vMinLongestDim           IN LANE_ACCESSORIAL.Min_Longest_Dimension%TYPE,
      vMaxLongestDim           IN LANE_ACCESSORIAL.Max_Longest_Dimension%TYPE,
      vMinSecondLongestDim     IN LANE_ACCESSORIAL.Min_Second_Longest_Dimension%TYPE,
      vMaxSecondLongestDim     IN LANE_ACCESSORIAL.Max_Second_Longest_Dimension%TYPE,
      vMinThirdLongestDim      IN LANE_ACCESSORIAL.Min_Third_Longest_Dimension%TYPE,
      vMaxThirdLongestDim      IN LANE_ACCESSORIAL.Max_Third_Longest_Dimension%TYPE,
      vMinLengthGrith          IN LANE_ACCESSORIAL.Min_Length_Plus_Grith%TYPE,
      vMaxLengthGrith          IN LANE_ACCESSORIAL.Max_Length_Plus_Grith%TYPE,
      vMinWeight               IN LANE_ACCESSORIAL.Min_Weight%TYPE,
      vMaxWeight               IN LANE_ACCESSORIAL.Max_Weight%TYPE,
      vIsZoneStopOff           IN LANE_ACCESSORIAL.Is_Zone_Stopoff%TYPE,
      vZoneId                  IN LANE_ACCESSORIAL.Zone_Id%TYPE, -- MACR00159593 ends
                                                               /* vLaneAccId         IN LANE_ACCESSORIAL.LANE_ACCESSORIAL_ID%TYPE */
		 -- CMGT-615 Starts here
	  vCommID				IN LANE_ACCESSORIAL.COMMODITY_CODE_ID%TYPE,
	  vMaxCommId			IN LANE_ACCESSORIAL.MAX_RANGE_COMMODITY_CODE_ID%TYPE
	  	  -- CMGT-615 Ends here
      )
   AS
   BEGIN
      INSERT INTO LANE_ACCESSORIAL (TC_COMPANY_ID,
                                    LANE_ID,
                                    RATING_LANE_DTL_SEQ,
                                    ACCESSORIAL_ID,
                                    RATE,
                                    MINIMUM_RATE,
                                    CURRENCY_CODE,
                                    IS_AUTO_APPROVE,
                                    EFFECTIVE_DT,
                                    EXPIRATION_DT,
                                    LANE_ACCESSORIAL_ID,
                                    PAYEE_CARRIER_ID,
                                    MINIMUM_SIZE,
                                    MAXIMUM_SIZE,
                                    SIZE_UOM_ID,
                                    IS_SHIPMENT_ACCESSORIAL,
                                    Min_Rate_Type,      -- MACR00159593 starts
                                    Maximum_Rate,
                                    PACKAGE_TYPE_ID,
                                    CUSTOMER_RATE,
                                    Customer_Min_Charge,
                                    Calculated_Rate,
                                    Amount,
                                    BASE_AMOUNT,
                                    Base_Charge,
                                    RANGE_MIN_AMOUNT,
                                    RANGE_MAX_AMOUNT,
                                    Increment_Val,
                                    Mot_Id,
                                    Equipment_Id,
                                    Service_Level_Id,
                                    PROTECTION_LEVEL_ID,
                                    Billing_Method,
                                    Min_Longest_Dimension,
                                    Max_Longest_Dimension,
                                    Min_Second_Longest_Dimension,
                                    Max_Second_Longest_Dimension,
                                    Min_Third_Longest_Dimension,
                                    Max_Third_Longest_Dimension,
                                    Min_Length_Plus_Grith,
                                    Max_Length_Plus_Grith,
                                    Min_Weight,
                                    Max_Weight,
                                    Is_Zone_Stopoff,
                                    Zone_Id ,              -- MACR00159593 ends
									commodity_code_id,   -- CMGT-615 Starts here
									max_range_commodity_code_id  -- CMGT-615 Ends here
                                           )
           VALUES (vTCCompanyId,
                   vRatingLaneId,
                   vRatingLaneDtlId,
                   vAccessorialCode,
                   vRate,
                   vMinRate,
                   vCurrencyCode,
                   vIsApproved,
                   vEffectiveDT,
                   vExpirationDT,
                   LANE_ACCESSORIAL_ID_SEQ.NEXTVAL,
                   vPayeeCarrierCode,
                   p_minimum_size,
                   p_maximum_size,
                   p_size_uom,
                   vIsShipmentAccessorial,
                   vMinRateType,                        -- MACR00159593 starts
                   vMaximumRate,
                   vPackageType,
                   vCustomerRate,
                   vCustomerMinCharge,
                   vCalculatedRate,
                   vAmount,
                   vBaseAmount,
                   vBaseCharge,
                   vRangeMinAmount,
                   vRangeMaxAmount,
                   vIncrement,
                   vMot,
                   vEquipment,
                   vServiceLevel,
                   vProtectionLevel,
                   vBillingMethod,
                   vMinLongestDim,
                   vMaxLongestDim,
                   vMinSecondLongestDim,
                   vMaxSecondLongestDim,
                   vMinThirdLongestDim,
                   vMaxThirdLongestDim,
                   vMinLengthGrith,
                   vMaxLengthGrith,
                   vMinWeight,
                   vMaxWeight,
                   vIsZoneStopOff,
                   vZoneId,                                -- MACR00159593 ends
				   vCommId,				-- CMGT-615 Starts here
				   vMaxCommId 				-- CMGT-615 Ends here
                          );

      COMMIT;
   END INSERT_RATING_LANE_ACCESSORIAL;

   PROCEDURE INSERT_RATING_LANE_DTL_ERROR (
      vTCCompanyId         IN COMPANY.COMPANY_ID%TYPE,
      vLaneId              IN rating_lane.LANE_ID%TYPE,
      vRateLaneDtlSeq      IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vErrorMsgDesc        IN RATING_LANE_ERRORS.ERROR_MSG_DESC%TYPE,
      p_error_msg_id       IN RATING_LANE_ERRORS.ERROR_MSG_ID%TYPE DEFAULT NULL,
      p_error_param_list   IN RATING_LANE_ERRORS.ERROR_PARAM_LIST%TYPE DEFAULT NULL)
   AS
      vBatchId   RATING_LANE_ERRORS.BATCH_ID%TYPE;
   BEGIN
    
--CMGT-731 START	
 begin
      SELECT BATCH_ID
        INTO vBatchId
        FROM IMPORT_RATING_LANE
       WHERE LANE_ID = vLaneId AND TC_COMPANY_ID = vTCCompanyId;
  exception 
  when no_data_found then
	vBatchId  := NULL;
  end;  
--CMGT-731 END
      INSERT INTO RATING_LANE_ERRORS (RATING_LANE_ERROR,
                                      TC_COMPANY_ID,
                                      LANE_ID,
                                      RATING_LANE_DTL_SEQ,
                                      ERROR_MSG_DESC,
                                      BATCH_ID,
                                      ERROR_MSG_ID,
                                      ERROR_PARAM_LIST)
           VALUES (SEQ_RATING_LANE_ERROR.NEXTVAL,
                   vTCCompanyId,
                   vLaneId,
                   vRateLaneDtlSeq,
                   vErrorMsgDesc,
                   vBatchId,
                   p_error_msg_id,
                   p_error_param_list);

      COMMIT;
   END INSERT_RATING_LANE_DTL_ERROR;

   FUNCTION DELETE_IMP_RATING_LANES (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vImportLaneID   IN IMPORT_RATING_LANE.LANE_ID%TYPE)
      RETURN NUMBER
   IS
   BEGIN
      DELETE FROM IMPORT_RATING_LANE
            WHERE TC_COMPANY_ID = vTCCompanyId AND LANE_ID = vImportLaneID
                  AND NOT EXISTS
                             (SELECT 1
                                FROM IMPORT_RATING_LANE_DTL
                               WHERE TC_COMPANY_ID = vTCCompanyId
                                     AND LANE_ID = vImportLaneID);

      COMMIT;
      RETURN 0;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 1;
   END DELETE_IMP_RATING_LANES;

   FUNCTION VALIDATE_LANE_BASE_DATA (
      vTCCompanyId         IN COMPANY.COMPANY_ID%TYPE,
      vLaneId              IN rating_lane.LANE_ID%TYPE,
      vBusinessUnit        IN BUSINESS_UNIT.BUSINESS_UNIT%TYPE,
      vOFacilityId         IN FACILITY.FACILITY_ID%TYPE,
      vOFacilityAlias      IN FACILITY_ALIAS.FACILITY_ALIAS_ID%TYPE,
      vOFacilityBU         IN BUSINESS_UNIT.BUSINESS_UNIT%TYPE,
      vOStateProv          IN STATE_PROV.STATE_PROV%TYPE,
      vOPostal             IN POSTAL_CODE.POSTAL_CODE%TYPE,
      vOCountry            IN COUNTRY.COUNTRY_CODE%TYPE,
      vOZoneId             IN ZONE.ZONE_ID%TYPE,
      vOZoneName           IN IMPORT_RATING_LANE.O_ZONE_NAME%TYPE,
      vDFacilityId         IN FACILITY.FACILITY_ID%TYPE,
      vDFacilityAlias      IN FACILITY_ALIAS.FACILITY_ALIAS_ID%TYPE,
      vDFacilityBU         IN BUSINESS_UNIT.BUSINESS_UNIT%TYPE,
      vDStateProv          IN STATE_PROV.STATE_PROV%TYPE,
      vDPostal             IN POSTAL_CODE.POSTAL_CODE%TYPE,
      vDCountry            IN COUNTRY.COUNTRY_CODE%TYPE,
      vDZoneId             IN ZONE.ZONE_ID%TYPE,
      vDZoneName           IN IMPORT_RATING_LANE.D_ZONE_NAME%TYPE,
      vCustomerId          IN rating_lane.CUSTOMER_ID%TYPE,
      vCustomerCode        IN IMPORT_RATING_LANE.CUSTOMER_CODE%TYPE,
      vCustomerCodeBU      IN IMPORT_RATING_LANE.CUSTOMER_CODE_BU%TYPE,
      vBillingMethod       IN rating_lane.BILLING_METHOD%TYPE,
      vBillingMethodCode   IN IMPORT_RATING_LANE.BILLING_METHOD_CODE%TYPE,
      vIncotermId          IN rating_lane.INCOTERM_ID%TYPE,
      vIncotermName        IN IMPORT_RATING_LANE.INCOTERM_NAME%TYPE,
      vIncotermNameBU      IN IMPORT_RATING_LANE.INCOTERM_NAME_BU%TYPE)
      RETURN NUMBER
   IS
      vPassFail            NUMBER;
      vFailure             NUMBER;
      vOTempFacility       NUMBER (8);
      vDTempFacility       NUMBER (8);
      vOFacilityBUID       NUMBER;
      vDFacilityBUID       NUMBER;
      vCustomerCodeBUID    NUMBER;
      vTempCustomerId      NUMBER;
      vIncotermNameBUID    NUMBER;
      vTempIncotermId      NUMBER;
      vTempBillingMethod   NUMBER;
      VTCID                NUMBER;
      vCount               NUMBER;
      vRootCompanyId       COMPANY.COMPANY_ID%TYPE;

      VERRORMSGDESC        VARCHAR2 (100);
      VERRORID             NUMBER;
      VERRORLIST           VARCHAR2 (10);
   BEGIN
      vFailure := 0;
      vCount := 0;
      vPassFail := 0;
      VTCID := NULL;

      IF (vBusinessUnit IS NOT NULL)
      THEN
         vPassFail := 0;
         /* SELECT COUNT(TC_COMPANY_ID) INTO vCount FROM BUSINESS_UNIT WHERE BU_ID = vTCCompanyId;
          IF(vCount > 0)THEN
          SELECT TC_COMPANY_ID INTO VTCID FROM BUSINESS_UNIT WHERE BU_ID = vTCCompanyId;
          END IF; */
         vPassFail :=
            Rating_Sub_Validation_Pkg.VALIDATE_BUSINESS_UNIT (vBusinessUnit);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RATING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vBusinessUnit || ' is an invalid Business Unit',
               4720100,
               vBusinessUnit);
         END IF;
      END IF;

      SELECT ROOTCOMPANYID
        INTO vRootCompanyId
        FROM IMPORT_RATING_LANE
       WHERE LANE_ID = vLaneId;

      IF (vOFacilityBU IS NULL)
      THEN
         vOFacilityBUID := vRootCompanyId;
      ELSE
         vOFacilityBUID :=
            Rating_Lane_Validation_Pkg.GET_BUID_FROM_BUSINESS_UNIT (
               vRootCompanyId,
               vOFacilityBU);
      END IF;


      vPassFail :=
         Rating_Lane_Validation_Pkg.CHECK_IS_FAC_BU_VALID (vTCCompanyId,
                                                           vOFacilityBUID);

      IF (vPassFail = 1)
      THEN
         vFailure := 1;
         INSERT_RATING_LANE_ERROR (
            vTCCompanyId,
            vLaneId,
            vOFacilityBU || ' is an invalid Business Unit',
            4720100,
            vBusinessUnit);
      END IF;



      IF (vOFacilityId IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail :=
            Rating_Sub_Validation_Pkg.VALIDATE_FACILITY (vOFacilityBUID,
                                                         vOFacilityId);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RATING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vOFacilityId || ' is an invalid Origin Facility',
               4720101,
               vOFacilityId);
         END IF;
      END IF;

      IF (vOFacilityAlias IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail :=
            Rating_Sub_Validation_Pkg.VALIDATE_FACILITY_ALIAS (
               vOFacilityBUID,
               vOFacilityAlias);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RATING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vOFacilityAlias || ' is an invalid Origin Facility Alias',
               4720102,
               vOFacilityAlias);
         ELSE
            IF (vOFacilityId IS NULL)
            THEN
               SELECT FACILITY_ID
                 INTO vOTempFacility
                 FROM FACILITY_ALIAS
                WHERE FACILITY_ALIAS_ID = vOFacilityAlias
                      AND TC_COMPANY_ID = vOFacilityBUID;

               UPDATE IMPORT_RATING_LANE
                  SET O_FACILITY_ID = vOTempFacility
                WHERE O_FACILITY_ALIAS_ID = vOFacilityAlias
                      AND TC_COMPANY_ID = vTCCompanyId;

               COMMIT;
            END IF;
         END IF;
      END IF;

      IF (vOStateProv IS NOT NULL AND vOCountry IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail :=
            Rating_Sub_Validation_Pkg.VALIDATE_STATE_PROV (vOStateProv,
                                                           vOCountry);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RATING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vOStateProv || ' is an invalid Origin State/Prov',
               4720103,
               vOStateProv);
         END IF;
      END IF;

      IF (vOCountry IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail := Rating_Sub_Validation_Pkg.VALIDATE_COUNTRY (vOCountry);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RATING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vOCountry || ' is an invalid Origin Country',
               4720104,
               vOCountry);
         END IF;
      END IF;

      IF (vOZoneName IS NOT NULL)
      THEN
         vPassFail := 0;

         IF (vOZoneId IS NULL)
         THEN
            vFailure := 1;

            INSERT_RATING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vOZoneName || ' is an invalid Origin Zone',
               4720105,
               vOZoneName);
         END IF;
      END IF;

      IF (vDFacilityBU IS NULL)
      THEN
         vDFacilityBUID := vRootCompanyId;
      ELSE
         vDFacilityBUID :=
            Rating_Lane_Validation_Pkg.GET_BUID_FROM_BUSINESS_UNIT (
               vRootCompanyId,
               vDFacilityBU);
      END IF;

      vPassFail :=
         Rating_Lane_Validation_Pkg.CHECK_IS_FAC_BU_VALID (vTCCompanyId,
                                                           vDFacilityBUID);

      IF (vPassFail = 1)
      THEN
         vFailure := 1;
         INSERT_RATING_LANE_ERROR (
            vTCCompanyId,
            vLaneId,
            vDFacilityBU || ' is an invalid Business Unit',
            4720100,
            vBusinessUnit);
      END IF;



      IF (vDFacilityId IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail :=
            Rating_Sub_Validation_Pkg.VALIDATE_FACILITY (vDFacilityBUID,
                                                         vDFacilityId);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RATING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vDFacilityId || ' is an invalid Destination Facility',
               4720106,
               vDFacilityId);
         END IF;
      END IF;

      IF (vDFacilityAlias IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail :=
            Rating_Sub_Validation_Pkg.VALIDATE_FACILITY_ALIAS (
               vDFacilityBUID,
               vDFacilityAlias);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RATING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vDFacilityAlias || ' is an invalid Destination Facility Alias',
               4720107,
               vDFacilityAlias);
         ELSE
            IF (vDFacilityId IS NULL)
            THEN
               SELECT FACILITY_ID
                 INTO vDTempFacility
                 FROM FACILITY_ALIAS
                WHERE FACILITY_ALIAS_ID = vDFacilityAlias
                      AND TC_COMPANY_ID = vDFacilityBUID;

               UPDATE IMPORT_RATING_LANE
                  SET D_FACILITY_ID = vDTempFacility
                WHERE D_FACILITY_ALIAS_ID = vDFacilityAlias
                      AND TC_COMPANY_ID = vTCCompanyId;

               COMMIT;
            END IF;
         END IF;
      END IF;

      IF (vDStateProv IS NOT NULL AND vDCountry IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail :=
            Rating_Sub_Validation_Pkg.VALIDATE_STATE_PROV (vDStateProv,
                                                           vDCountry);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RATING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vDStateProv || ' is an invalid Destination State/Prov',
               4720108,
               vDStateProv);
         END IF;
      END IF;

      IF (vDCountry IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail := Rating_Sub_Validation_Pkg.VALIDATE_COUNTRY (vDCountry);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RATING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vDCountry || ' is an invalid Destination Country',
               4720109,
               vDCountry);
            COMMIT;
         END IF;
      END IF;

      IF (vDZoneName IS NOT NULL)
      THEN
         vPassFail := 0;

         IF (vDZoneId IS NULL)
         THEN
            vFailure := 1;

            INSERT_RATING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vDZoneName || ' is an invalid Destnation Zone',
               4720110,
               vDZoneName);
         END IF;
      END IF;



      IF (vCustomerCodeBU IS NULL)
      THEN
         vCustomerCodeBUID := vRootCompanyId;
      ELSE
         vCustomerCodeBUID :=
            GET_BUID_FROM_BUSINESS_UNIT (vRootCompanyId, vCustomerCodeBU);
      END IF;

      VPASSFAIL := CHECK_IS_BU_VALID (vTCCompanyId, vCustomerCodeBUID);

      IF (VPASSFAIL = 1)
      THEN
         vFailure := 1;
         INSERT_RATING_LANE_ERROR (
            vTCCompanyId,
            vLaneId,
            vCustomerCodeBU || ' is an invalid business unit',
            4720100,
            vCustomerCodeBU);
      ELSE
         IF (vCustomerId IS NOT NULL)
         THEN
            VPASSFAIL := 0;
            VPASSFAIL :=
               Rating_Sub_Validation_Pkg.VALIDATE_CUSTOMER (
                  vCustomerCodeBUID,
                  vCustomerId);

            IF (VPASSFAIL = 1)
            THEN
               VFAILURE := 1;
               VERRORMSGDESC := VCUSTOMERID || ' is an invalid Customer Code';
               VERRORID := 4720181;
               VERRORLIST := vCustomerId;
               INSERT_RATING_LANE_ERROR (VTCCOMPANYID,
                                         VLANEID,
                                         VERRORMSGDESC,
                                         VERRORID,
                                         VERRORLIST);
            END IF;
         END IF;

         IF (vCustomerCode IS NOT NULL)
         THEN
            VPASSFAIL := 0;
            VPASSFAIL :=
               Rating_Sub_Validation_Pkg.VALIDATE_CUSTOMER_CODE (
                  vCustomerCodeBUID,
                  vCustomerCode);

            IF (VPASSFAIL = 1)
            THEN
               VFAILURE := 1;
               VERRORMSGDESC :=
                  VCUSTOMERCODE || ' is an invalid Customer Code';
               VERRORID := 4720181;
               VERRORLIST := vCustomerCode;
               INSERT_RATING_LANE_ERROR (VTCCOMPANYID,
                                         VLANEID,
                                         VERRORMSGDESC,
                                         VERRORID,
                                         VERRORLIST);
            ELSE
               IF (vCustomerId IS NULL)
               THEN
                  SELECT CUSTOMER_ID
                    INTO vTempCustomerId
                    FROM CUSTOMER
                   WHERE CUSTOMER_CODE = vCustomerCode
                         AND TC_COMPANY_ID = vCustomerCodeBUID;

                  UPDATE IMPORT_RATING_LANE
                     SET CUSTOMER_ID = vTempCustomerId
                   WHERE CUSTOMER_CODE = vCustomerCode
                         AND TC_COMPANY_ID = vTcCompanyId;
               END IF;
            END IF;
         END IF;
      END IF;

      IF (vIncotermNameBU IS NULL)
      THEN
         vIncotermNameBUID := vRootCompanyId;
      ELSE
         vIncotermNameBUID :=
            GET_BUID_FROM_BUSINESS_UNIT (vRootCompanyId, vIncotermNameBU);
      END IF;


      VPASSFAIL := CHECK_IS_BU_VALID (vTCCompanyId, vIncotermNameBUID);

      IF (VPASSFAIL = 1)
      THEN
         vFailure := 1;
         INSERT_RATING_LANE_ERROR (
            vTCCompanyId,
            vLaneId,
            vIncotermNameBU || ' is an invalid business unit',
            4720100,
            vIncotermNameBU);
      ELSE
         IF (vIncotermId IS NOT NULL)
         THEN
            VPASSFAIL := 0;
            VPASSFAIL :=
               Rating_Sub_Validation_Pkg.VALIDATE_INCOTERM (
                  vIncotermNameBUID,
                  vIncotermId);

            IF (VPASSFAIL = 1)
            THEN
               VFAILURE := 1;
               VERRORMSGDESC := VINCOTERMID || ' is an invalid Incoterm';
               VERRORID := 4720182;
               VERRORLIST := vIncotermId;
               INSERT_RATING_LANE_ERROR (VTCCOMPANYID,
                                         VLANEID,
                                         VERRORMSGDESC,
                                         VERRORID,
                                         VERRORLIST);
            END IF;
         END IF;

         IF (vIncotermName IS NOT NULL)
         THEN
            VPASSFAIL := 0;
            VPASSFAIL :=
               Rating_Sub_Validation_Pkg.VALIDATE_INCOTERM_NAME (
                  vIncotermNameBUID,
                  vIncotermName);

            IF (VPASSFAIL = 1)
            THEN
               VFAILURE := 1;
               VERRORMSGDESC := VINCOTERMNAME || ' is an invalid Incoterm';
               VERRORID := 4720182;
               VERRORLIST := vIncotermName;
               INSERT_RATING_LANE_ERROR (VTCCOMPANYID,
                                         VLANEID,
                                         VERRORMSGDESC,
                                         VERRORID,
                                         VERRORLIST);
            ELSE
               IF (vIncotermId IS NULL)
               THEN
                  SELECT INCOTERM_ID
                    INTO vTempIncotermId
                    FROM INCOTERM
                   WHERE INCOTERM_NAME = vIncotermName;

                  UPDATE IMPORT_RATING_LANE
                     SET INCOTERM_ID = vTempIncotermId
                   WHERE INCOTERM_NAME = vIncotermName
                         AND TC_COMPANY_ID = vTcCompanyId;
               END IF;
            END IF;
         END IF;
      END IF;

      IF (vBillingMethod IS NOT NULL)
      THEN
         SELECT COUNT (BILLING_METHOD)
           INTO vCount
           FROM BILLING_METHOD
          WHERE BILLING_METHOD = vBillingMethod;

         IF (vCount <= 0)
         THEN
            VFAILURE := 1;
            VERRORMSGDESC :=
               vBillingMethod || ' is an invalid Billing Method Code';
            VERRORID := 4720183;
            VERRORLIST := vBillingMethod;
            INSERT_RATING_LANE_ERROR (VTCCOMPANYID,
                                      VLANEID,
                                      VERRORMSGDESC,
                                      VERRORID,
                                      VERRORLIST);
         END IF;
      END IF;

      IF (vBillingMethodCode IS NOT NULL)
      THEN
         SELECT COUNT (BILLING_METHOD)
           INTO vCount
           FROM BILLING_METHOD
          WHERE DESCRIPTION = vBillingMethodCode;

         IF (vCount > 0)
         THEN
            SELECT BILLING_METHOD
              INTO vTempBillingMethod
              FROM BILLING_METHOD
             WHERE DESCRIPTION = vBillingMethodCode;

            UPDATE IMPORT_RATING_LANE
               SET BILLING_METHOD = vTempBillingMethod
             WHERE BILLING_METHOD_CODE = vBillingMethodCode;
         ELSE
            VFAILURE := 1;
            VERRORMSGDESC :=
               vBillingMethodCode || ' is an invalid Billing Method Code';
            VERRORID := 4720183;
            VERRORLIST := vBillingMethodCode;
            INSERT_RATING_LANE_ERROR (VTCCOMPANYID,
                                      VLANEID,
                                      VERRORMSGDESC,
                                      VERRORID,
                                      VERRORLIST);
         END IF;
      END IF;



      RETURN vFailure;
   END VALIDATE_LANE_BASE_DATA;

   FUNCTION GET_BUID_FROM_BUSINESS_UNIT (
      vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
      vBUColumn         VARCHAR2)
      RETURN NUMBER
   IS
      vBUId    NUMBER;
      vCount   NUMBER;
   BEGIN
      SELECT COUNT (COMPANY_ID)
        INTO vCount
        FROM COMPANY
       WHERE COMPANY_NAME = vBUColumn;

      IF (vCount > 0)
      THEN
         SELECT COMPANY_ID
           INTO vBUId
           FROM COMPANY
          WHERE COMPANY_NAME = vBUColumn;
      ELSE
         vBUId := -2; -- just to make sure that there is no company with this ID. thus can add an error message.
      END IF;


      RETURN vBUId;
   END GET_BUID_FROM_BUSINESS_UNIT;


   FUNCTION CHECK_IS_BU_VALID (vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
                               vBUID          IN COMPANY.COMPANY_ID%TYPE)
      RETURN NUMBER
   IS
      vParentCompanyId   NUMBER;
      vTcCompanyId1      NUMBER;
      vPassFail          NUMBER;
      vCount1            NUMBER;
   BEGIN
      vPassFail := 1;
      vParentCompanyId := 0;
      vTcCompanyId1 := vTCCompanyId;

      IF (vTcCompanyId1 = vBUID)
      THEN
         vPassFail := 0;
      END IF;

      WHILE (vParentCompanyId != -1)
      LOOP
         SELECT PARENT_COMPANY_ID
           INTO vParentCompanyId
           FROM COMPANY
          WHERE COMPANY_ID = vTcCompanyId1;



         vTcCompanyId1 := vParentCompanyId;

         IF (vTcCompanyId1 != -1 AND vTcCompanyId1 = vBUID)
         THEN
            vPassFail := 0;
         END IF;
      END LOOP;

      RETURN vPassFail;
   END CHECK_IS_BU_VALID;


   FUNCTION CHECK_IS_FAC_BU_VALID (vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
                                   vBUID          IN COMPANY.COMPANY_ID%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
      vCount      NUMBER;
   BEGIN
      vPassFail := 1;

      -- CHECK FOR CURRENT COMPANY
      IF (vTcCompanyId = vBUID)
      THEN
         vPassFail := 0;
      END IF;

      -- CHECK FOR PARENT COMPANY
      IF (vPassFail = 1)
      THEN
             SELECT COUNT (1)
               INTO vCount
               FROM company
              WHERE company_id = vBUID
         START WITH company_id = vTCCompanyId
         CONNECT BY PRIOR parent_company_id = company_id;

         IF (vCount > 0)
         THEN
            vPassFail := 0;
         END IF;
      END IF;

      -- CHECK FOR CHILD COMPANY LIST
      IF (vPassFail = 1)
      THEN
             SELECT COUNT (1)
               INTO vCount
               FROM company
              WHERE company_id = vBUID
         START WITH company_id = vTCCompanyId
         CONNECT BY PRIOR company_id = parent_company_id;

         IF (vCount > 0)
         THEN
            vPassFail := 0;
         END IF;
      END IF;

      RETURN vPassFail;
   END CHECK_IS_FAC_BU_VALID;



   --007
   FUNCTION VALIDATE_DETAIL_BASE_DATA (
      vTCCompanyId               IN COMPANY.COMPANY_ID%TYPE,
      vLaneId                    IN rating_lane.LANE_ID%TYPE,
      vRatingDtlSeq              IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vCarrierCode               IN IMPORT_RATING_LANE_DTL.CARRIER_CODE%TYPE,
      vCarrierCodeBUID           IN COMPANY.COMPANY_ID%TYPE,
      vMode                      IN IMPORT_RATING_LANE_DTL.MOT%TYPE,
      vModeBUID                  IN COMPANY.COMPANY_ID%TYPE,
      vServiceLevel              IN IMPORT_RATING_LANE_DTL.SERVICE_LEVEL%TYPE,
      vServiceLevelBUID          IN COMPANY.COMPANY_ID%TYPE,
      vEquipment                 IN IMPORT_RATING_LANE_DTL.EQUIPMENT_CODE%TYPE,
      vEquipmentBUID             IN COMPANY.COMPANY_ID%TYPE,
      vProtectionLevel           IN IMPORT_RATING_LANE_DTL.PROTECTION_LEVEL%TYPE,
      vProtectionLevelBUID       IN COMPANY.COMPANY_ID%TYPE,
      p_scndr_carrier_code       IN IMPORT_RATING_LANE_DTL.SCNDR_CARRIER_CODE%TYPE,
      p_scndr_carrier_codeBUID   IN COMPANY.COMPANY_ID%TYPE,
      vIsBudgeted                IN rating_lane_dtl.IS_BUDGETED%TYPE,
      vEffectiveDT               IN rating_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT              IN rating_lane_dtl.EXPIRATION_DT%TYPE)
      RETURN NUMBER
   IS
      vPassFail            NUMBER;
      vFailure             NUMBER;
      vCount               NUMBER;
      vCarrierId           rating_lane_dtl.CARRIER_ID%TYPE;
      vModeId              rating_lane_dtl.MOT_ID%TYPE;
      vEquipmentId         rating_lane_dtl.EQUIPMENT_ID%TYPE;
      vServiceLevelId      rating_lane_dtl.SERVICE_LEVEL_ID%TYPE;
      p_scndr_carrier_Id   rating_LANE_DTL.SCNDR_CARRIER_ID%TYPE;
      vProtectionLevelId   rating_lane_dtl.PROTECTION_LEVEL_ID%TYPE;
   BEGIN
      vPassFail := 0;
      vFailure := 0;
      vCarrierId := 0;
      vModeId := 0;
      vEquipmentId := 0;
      vServiceLevelId := 0;
      p_scndr_carrier_Id := 0;
      vProtectionLevelId := 0;

      IF (vEffectiveDt IS NULL)
      THEN
         vFailure := 1;
         INSERT_RATING_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRatingDtlSeq,
            'Lane Detail Effective Date is required or given value is invalid',
            4720151,
            NULL);
      END IF;

      IF (vExpirationDt IS NULL)
      THEN
         vFailure := 1;
         INSERT_RATING_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRatingDtlSeq,
            'Lane Detail Expiration Date is required or given value is invalid',
            4720152,
            NULL);
      END IF;

      IF (    vEffectiveDt IS NOT NULL
          AND vExpirationDt IS NOT NULL
          AND vEffectiveDt > vExpirationDt)
      THEN
         vFailure := 1;
         INSERT_RATING_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRatingDtlSeq,
            'Lane Detail Expiration Date must be greater than Effective Date',
            4720153,
            NULL);
      END IF;

      IF (vCarrierCode IS NOT NULL)
      THEN
         vPassFail := CHECK_IS_BU_VALID (vTCCompanyId, vCarrierCodeBUID);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RATING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRatingDtlSeq,
               vCarrierCode || ' is an invalid Carrier Code',
               4720154,
               vCarrierCode);
         ELSE
            vPassFail :=
               Rating_Sub_Validation_Pkg.VALIDATE_CARRIER_CODE (
                  vCarrierCodeBUID,
                  vCarrierCode);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  vCarrierCode || ' is an invalid Carrier Code',
                  4720154,
                  vCarrierCode);
            ELSE
               vPassFail :=
                  Rating_Sub_Validation_Pkg.VALIDATE_CARRIER_STATUS (
                     vCarrierCodeBUID,
                     vCarrierCode);

               IF (vPassFail = 1)
               THEN
                  vFailure := 1;
                  INSERT_RATING_LANE_DTL_ERROR (
                     vTCCompanyId,
                     vLaneId,
                     vRatingDtlSeq,
                     vCarrierCode || ' is an inactive Carrier Code',
                     4720155,
                     vCarrierCode);
               ELSE
                  IF (vCarrierId = 0)
                  THEN
                     SELECT carrier_id
                       INTO vCarrierId
                       FROM CARRIER_CODE
                      WHERE     CARRIER_CODE = vCarrierCode
                            AND MARK_FOR_DELETION = 0
                            AND tc_company_id = vCarrierCodeBUID;

                     UPDATE IMPORT_RATING_LANE_DTL
                        SET carrier_id = vCarrierId
                      WHERE     CARRIER_CODE = vCarrierCode
                            AND tc_company_id = vTCCompanyId
                            AND lane_id = vLaneId
                            AND RATING_LANE_DTL_SEQ = vRatingDtlSeq;
							
					 SELECT COUNT(CARRIER_ID) INTO vCount FROM CARRIER_CODE WHERE CARRIER_ID = vCarrierId AND carrier_type_id = 24;
					 IF (vCount > 0)
					 THEN
						vFailure := 1;
						INSERT_RATING_LANE_DTL_ERROR (
							vTCCompanyId,
							vLaneId,
							vRatingDtlSeq,
							'Rating lane detail cannot be created for External Parcel Carrier '||vCarrierCode,
							4720202,
							vCarrierCode);
					 END IF;
                  END IF;
               END IF;
            END IF;
         END IF;
      ELSIF (vIsBudgeted = 0)
      THEN
         vPassFail := 1;
         vFailure := 1;
         INSERT_RATING_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRatingDtlSeq,
            'Carrier Code can only be null if it is a Budgeted Carrier',
            4720156,
            NULL);
      END IF;



      IF (vMode IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail := CHECK_IS_BU_VALID (vTCCompanyId, vModeBUID);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RATING_LANE_DTL_ERROR (vTCCompanyId,
                                          vLaneId,
                                          vRatingDtlSeq,
                                          vMode || ' is an invalid Mode',
                                          4720157,
                                          vMode);
         ELSE
            vPassFail :=
               Rating_Sub_Validation_Pkg.VALIDATE_MODE (vModeBUID, vMode);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (vTCCompanyId,
                                             vLaneId,
                                             vRatingDtlSeq,
                                             vMode || ' is an invalid Mode',
                                             4720157,
                                             vMode);
            ELSE
               IF (vModeId = 0)
               THEN
                  SELECT mot_id
                    INTO vModeId
                    FROM MOT
                   WHERE     MOT = vMode
                         AND MARK_FOR_DELETION = 0
                         AND tc_company_id = vModeBUID;

                  UPDATE IMPORT_RATING_LANE_DTL
                     SET mot_id = vModeId
                   WHERE     MOT = vMode
                         AND tc_company_id = vTCCompanyId
                         AND lane_id = vLaneId
                         AND RATING_LANE_DTL_SEQ = vRatingDtlSeq;
               END IF;
            END IF;
         END IF;
      END IF;



      IF (vServiceLevel IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail := CHECK_IS_BU_VALID (vTCCompanyId, vServiceLevelBUID);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RATING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRatingDtlSeq,
               vServiceLevel || ' is an invalid Service Level',
               4720158,
               vServiceLevel);
         ELSE
            vPassFail :=
               Rating_Sub_Validation_Pkg.VALIDATE_SERVICE_LEVEL (
                  vServiceLevelBUID,
                  vServiceLevel);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  vServiceLevel || ' is an invalid Service Level',
                  4720158,
                  vServiceLevel);
            ELSE
               IF (vServiceLevelId = 0)
               THEN
                  SELECT service_level_id
                    INTO vServiceLevelId
                    FROM SERVICE_LEVEL
                   WHERE     SERVICE_LEVEL = vServiceLevel
                         AND MARK_FOR_DELETION = 0
                         AND tc_company_id = vServiceLevelBUID;

                  UPDATE IMPORT_RATING_LANE_DTL
                     SET service_level_id = vServiceLevelId
                   WHERE     SERVICE_LEVEL = vServiceLevel
                         AND tc_company_id = vTCCompanyId
                         AND lane_id = vLaneId
                         AND RATING_LANE_DTL_SEQ = vRatingDtlSeq;
               END IF;
            END IF;
         END IF;
      END IF;


      IF (vEquipment IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail := CHECK_IS_BU_VALID (vTCCompanyId, vEquipmentBUID);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RATING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRatingDtlSeq,
               vEquipment || ' is an invalid Equipment Code',
               4720159,
               vEquipment);
         ELSE
            vPassFail :=
               Rating_Sub_Validation_Pkg.VALIDATE_EQUIPMENT (vEquipmentBUID,
                                                             vEquipment);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  vEquipment || ' is an invalid Equipment Code',
                  4720159,
                  vEquipment);
            ELSE
               IF (vEquipmentId = 0)
               THEN
                  SELECT equipment_id
                    INTO vEquipmentId
                    FROM EQUIPMENT
                   WHERE     equipment_code = vEquipment
                         AND MARK_FOR_DELETION = 0
                         AND tc_company_id = vEquipmentBUID;

                  UPDATE IMPORT_RATING_LANE_DTL
                     SET equipment_id = vEquipmentId
                   WHERE     equipment_code = vEquipment
                         AND tc_company_id = vTCCompanyId
                         AND lane_id = vLaneId
                         AND RATING_LANE_DTL_SEQ = vRatingDtlSeq;
               END IF;
            END IF;
         END IF;
      END IF;

      IF (vProtectionLevel IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail := CHECK_IS_BU_VALID (vTCCompanyId, vProtectionLevelBUID);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RATING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRatingDtlSeq,
               vProtectionLevel || ' is an invalid Protection Level',
               4720160,
               vProtectionLevel);
         ELSE
            vPassFail :=
               Rating_Sub_Validation_Pkg.VALIDATE_PROTECTION_LEVEL (
                  vProtectionLevelBUID,
                  vProtectionLevel);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  vProtectionLevel || ' is an invalid Protection Level',
                  4720160,
                  vProtectionLevel);
            ELSE
               IF (vProtectionLevelId = 0)
               THEN
                  SELECT protection_level_id
                    INTO vProtectionLevelId
                    FROM PROTECTION_LEVEL
                   WHERE     PROTECTION_LEVEL = vProtectionLevel
                         AND MARK_FOR_DELETION = 0
                         AND tc_company_id = vProtectionLevelBUID;

                  UPDATE IMPORT_RATING_LANE_DTL
                     SET protection_level_id = vProtectionLevelId
                   WHERE     PROTECTION_LEVEL = vProtectionLevel
                         AND tc_company_id = vTCCompanyId
                         AND lane_id = vLaneId
                         AND RATING_LANE_DTL_SEQ = vRatingDtlSeq;
               END IF;
            END IF;
         END IF;
      END IF;

      IF (p_scndr_carrier_code IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail :=
            CHECK_IS_BU_VALID (vTCCompanyId, p_scndr_carrier_codeBUID);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RATING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRatingDtlSeq,
               p_scndr_carrier_code
               || ' is an invalid Secondary Carrier Code',
               4720161,
               p_scndr_carrier_code);
         ELSE
            vPassFail :=
               Rating_Sub_Validation_Pkg.VALIDATE_CARRIER_CODE (
                  p_scndr_carrier_codeBUID,
                  p_scndr_carrier_code);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                  p_scndr_carrier_code
                  || ' is an invalid Secondary Carrier Code',
                  4720161,
                  p_scndr_carrier_code);
            ELSE
               vPassFail := 0;
               vPassFail :=
                  Rating_Sub_Validation_Pkg.VALIDATE_CARRIER_STATUS (
                     p_scndr_carrier_codeBUID,
                     p_scndr_carrier_code);

               IF (vPassFail = 1)
               THEN
                  vFailure := 1;
                  INSERT_RATING_LANE_DTL_ERROR (
                     vTCCompanyId,
                     vLaneId,
                     vRatingDtlSeq,
                     p_scndr_carrier_code
                     || ' is an inactive Secondary Carrier Code',
                     4720162,
                     p_scndr_carrier_code);
               ELSE
                  IF (p_scndr_carrier_Id = 0)
                  THEN
                     SELECT CARRIER_ID
                       INTO p_scndr_carrier_Id
                       FROM CARRIER_CODE
                      WHERE     CARRIER_CODE = p_scndr_carrier_code
                            AND MARK_FOR_DELETION = 0
                            AND tc_company_id = p_scndr_carrier_codeBUID;

                     UPDATE IMPORT_RATING_LANE_DTL
                        SET SCNDR_CARRIER_ID = p_scndr_carrier_Id
                      WHERE     SCNDR_CARRIER_CODE = p_scndr_carrier_code
                            AND tc_company_id = vTCCompanyId
                            AND lane_id = vLaneId
                            AND RATING_LANE_DTL_SEQ = vRatingDtlSeq;
                  END IF;
               END IF;
            END IF;
         END IF;
      END IF;

      RETURN vFailure;
   END VALIDATE_DETAIL_BASE_DATA;



   FUNCTION VALIDATE_HIERARCHY (
      vTCCompanyId       IN COMPANY.COMPANY_ID%TYPE,
      vLaneId            IN rating_lane.LANE_ID%TYPE,
      oFacilityId        IN rating_lane.O_FACILITY_ID%TYPE,
      oFacilityAliasId   IN rating_lane.O_FACILITY_ALIAS_ID%TYPE,
      oCity              IN rating_lane.O_CITY%TYPE,
      oState             IN rating_lane.O_STATE_PROV%TYPE,
      oCounty            IN rating_lane.O_COUNTY%TYPE,
      oPostal            IN rating_lane.O_POSTAL_CODE%TYPE,
      oCountry           IN rating_lane.O_COUNTRY_CODE%TYPE,
      oZoneId            IN rating_lane.O_ZONE_ID%TYPE,
      oZoneName          IN IMPORT_RATING_LANE.o_Zone_Name%TYPE,
      dFacilityId        IN rating_lane.D_FACILITY_ID%TYPE,
      dFacilityAliasId   IN rating_lane.D_FACILITY_ALIAS_ID%TYPE,
      dCity              IN rating_lane.D_CITY%TYPE,
      dState             IN rating_lane.D_STATE_PROV%TYPE,
      dCounty            IN rating_lane.D_COUNTY%TYPE,
      dPostal            IN rating_lane.D_POSTAL_CODE%TYPE,
      dCountry           IN rating_lane.D_COUNTRY_CODE%TYPE,
      dZoneId            IN rating_lane.D_ZONE_ID%TYPE,
      dZoneName          IN IMPORT_RATING_LANE.d_Zone_Name%TYPE)
      RETURN NUMBER
   IS
      vPassFail        NUMBER;
      vDPassFail       NUMBER;
      vState           NUMBER;
      vOLocType        rating_lane.O_LOC_TYPE%TYPE;
      vOLocTypeLH      VARCHAR2 (5);
      vDLocType        rating_lane.D_LOC_TYPE%TYPE;
      vDLocTypeLH      VARCHAR2 (5);
      vOLocValue       RATING_LANE_HIERARCHY.ORIGIN_VALUE%TYPE;
      vDLocValue       RATING_LANE_HIERARCHY.DEST_VALUE%TYPE;
      vLHValue         NUMBER;
      vOLocError       ERROR_MESSAGE.ERROR_MSG_ID%TYPE;
      vDLocError       ERROR_MESSAGE.ERROR_MSG_ID%TYPE;
      vHierarchy       NUMBER;
      vOPostal         VARCHAR2 (5);
      vDPostal         VARCHAR2 (5);
      vAttributeType   NUMBER;
      vErrorMsg        VARCHAR2 (1000);
   BEGIN
      vPassFail := 0;
      vDPassFail := 0;

      IF (oState IS NOT NULL AND oCountry IS NOT NULL)
         AND (    oCity IS NULL
              AND oPostal IS NULL
              AND oCounty IS NULL
              AND oFacilityAliasId IS NULL
              AND oZoneId IS NULL)
      THEN
         vOLocType := 'ST';
         vOLocTypeLH := 'ST';
      ELSIF (oCity IS NOT NULL AND oCountry IS NOT NULL)
            AND (    oPostal IS NULL
                 AND oFacilityAliasId IS NULL
                 AND oZoneId IS NULL)
      THEN
         SELECT HAS_STATE_PROV
           INTO vState
           FROM COUNTRY
          WHERE COUNTRY_CODE = oCountry;

         IF (vState = 1)
         THEN
            IF (oState IS NOT NULL)
            THEN
               vOLocType := 'CS';
               vOLocTypeLH := 'CS';
            ELSE                       /* oState is null but it is required */
               vPassFail := 1;
            END IF;
         ELSIF (oState IS NOT NULL)
         THEN
            vPassFail := 1;
         ELSE
            vOLocType := 'CS';
            vOLocTypeLH := 'CS';
         END IF;
      ELSIF (oPostal IS NOT NULL AND oCountry IS NOT NULL)
            AND (    oCity IS NULL
                 AND oState IS NULL
                 AND oCounty IS NULL
                 AND oFacilityAliasId IS NULL
                 AND oZoneId IS NULL)
      THEN
         SELECT DECODE (LENGTH (oPostal),
                        2, 'P2',
                        3, 'P3',
                        4, 'P4',
                        5, 'P5',
                        6, 'P6',
                        'Other')
           INTO vOPostal
           FROM DUAL;

         IF (vOPostal != 'Other')
         THEN
            vOLocType := vOPostal;
            vOLocTypeLH := vOPostal;
         ELSE
            vPassFail := 1;
         END IF;
      ELSIF (oZoneId IS NOT NULL)
            AND (    oCity IS NULL
                 AND oState IS NULL
                 AND oPostal IS NULL
                 AND oCounty IS NULL
                 AND oCountry IS NULL
                 AND oFacilityAliasId IS NULL)
      THEN
         vOLocType := 'ZN';

         SELECT COUNT (ATTRIBUTE_TYPE)
           INTO vAttributeType
           FROM ZONE
          WHERE TC_COMPANY_ID = vTCCompanyId AND ZONE_ID = oZoneId;

         IF (vAttributeType > 0)
         THEN
            SELECT DECODE (ATTRIBUTE_TYPE,
                           'ST', 'ZST',
                           'FC', 'ZFA',
                           'CT', 'ZCO',
                           'Z2', 'Z2',
                           'Z3', 'Z3',
                           'Z4', 'Z4',
                           'Z5', 'Z5',
                           'Z6', 'Z6',
                           'PR', 'PR',
                           'CI', 'ZCS',
                           'Other')
              INTO vOLocTypeLH
              FROM ZONE
             WHERE TC_COMPANY_ID = vTCCompanyId AND ZONE_ID = oZoneId;

            IF (vOLocTypeLH = 'Other')
            THEN
               vPassFail := 1;
            END IF;
         ELSE
            vPassFail := 1;
         END IF;
      ELSIF (oFacilityAliasId IS NOT NULL)
            AND (    oCity IS NULL
                 AND oState IS NULL
                 AND oPostal IS NULL
                 AND oCounty IS NULL
                 AND oCountry IS NULL
                 AND oZoneId IS NULL)
      THEN
         vOLocType := 'FA';
         vOLocTypeLH := 'FA';
      ELSIF (oCountry IS NOT NULL)
            AND (    oCity IS NULL
                 AND oState IS NULL
                 AND oPostal IS NULL
                 AND oCounty IS NULL
                 AND oFacilityAliasId IS NULL
                 AND oZoneId IS NULL)
      THEN
         vOLocType := 'CO';
         vOLocTypeLH := 'CO';
      ELSE
         vOLocError := 320000;
         vPassFail := 1;
      END IF;

      /*In case we missed an else case, we should check if the loc types have been determined*/
      IF (vOLocTypeLH IS NULL OR vOLocType IS NULL)
      THEN
         vPassFail := 1;
      END IF;

      IF (vPassFail = 1)
      THEN
         INSERT_RATING_LANE_ERROR (vTCCompanyId,
                                   vLaneId,
                                   'Origin Definition is invalid',
                                   4720111,
                                   NULL);
         COMMIT;
      END IF;

      -- Destination validation
      IF (dState IS NOT NULL AND dCountry IS NOT NULL)
         AND (    dCity IS NULL
              AND dPostal IS NULL
              AND dCounty IS NULL
              AND dFacilityAliasId IS NULL
              AND dZoneId IS NULL)
      THEN
         vDLocType := 'ST';
         vDLocTypeLH := 'ST';
      ELSIF (dCity IS NOT NULL AND dCountry IS NOT NULL)
            AND (    dPostal IS NULL
                 AND dFacilityAliasId IS NULL
                 AND dZoneId IS NULL)
      THEN
         SELECT HAS_STATE_PROV
           INTO vState
           FROM COUNTRY
          WHERE COUNTRY_CODE = dCountry;

         IF (vState = 1)
         THEN
            IF (dState IS NOT NULL)
            THEN
               vDLocType := 'CS';
               vDLocTypeLH := 'CS';
            ELSE                       /* dState is null but it is required */
               vDPassFail := 1;
            END IF;
         ELSIF (dState IS NOT NULL)
         THEN
            vDPassFail := 1;
         ELSE
            vDLocType := 'CS';
            vDLocTypeLH := 'CS';
         END IF;
      ELSIF (dPostal IS NOT NULL AND dCountry IS NOT NULL)
            AND (    dCity IS NULL
                 AND dState IS NULL
                 AND dCounty IS NULL
                 AND dFacilityAliasId IS NULL
                 AND dZoneId IS NULL)
      THEN
         SELECT DECODE (LENGTH (dPostal),
                        2, 'P2',
                        3, 'P3',
                        4, 'P4',
                        5, 'P5',
                        6, 'P6',
                        'Other')
           INTO vDPostal
           FROM DUAL;

         IF (vDPostal != 'Other')
         THEN
            vDLocType := vDPostal;
            vDLocTypeLH := vDPostal;
         ELSE
            vDPassFail := 1;
         END IF;
      ELSIF (dZoneId IS NOT NULL)
            AND (    dCity IS NULL
                 AND dState IS NULL
                 AND dPostal IS NULL
                 AND dCounty IS NULL
                 AND dCountry IS NULL
                 AND dFacilityAliasId IS NULL)
      THEN
         --DBMS_OUTPUT.PUT_LINE( 'DESTINATION IS ZONE !!!' || dZone || 'done' );
         vDLocType := 'ZN';

         SELECT COUNT (ATTRIBUTE_TYPE)
           INTO vAttributeType
           FROM ZONE
          WHERE TC_COMPANY_ID = vTCCompanyId AND ZONE_ID = dZoneId;

         IF (vAttributeType > 0)
         THEN
            SELECT DECODE (ATTRIBUTE_TYPE,
                           'ST', 'ZST',
                           'FC', 'ZFA',
                           'CT', 'ZCO',
                           'Z2', 'Z2',
                           'Z3', 'Z3',
                           'Z4', 'Z4',
                           'Z5', 'Z5',
                           'Z6', 'Z6',
                           'PR', 'PR',
                           'CI', 'ZCS',
                           'Other')
              INTO vDLocTypeLH
              FROM ZONE
             WHERE TC_COMPANY_ID = vTCCompanyId AND ZONE_ID = dZoneId;

            IF (vDLocTypeLH = 'Other')
            THEN
               vDPassFail := 1;
            END IF;
         ELSE
            vDPassFail := 1;
         END IF;
      ELSIF (dFacilityAliasId IS NOT NULL)
            AND (    dCity IS NULL
                 AND dState IS NULL
                 AND dPostal IS NULL
                 AND dCounty IS NULL
                 AND dCountry IS NULL
                 AND dZoneId IS NULL)
      THEN
         vDLocType := 'FA';
         vDLocTypeLH := 'FA';
      ELSIF (dCountry IS NOT NULL)
            AND (    dCity IS NULL
                 AND dState IS NULL
                 AND dPostal IS NULL
                 AND dCounty IS NULL
                 AND dFacilityAliasId IS NULL
                 AND dZoneId IS NULL)
      THEN
         vDLocType := 'CO';
         vDLocTypeLH := 'CO';
      ELSE
         vDLocError := 320000;
         vDPassFail := 1;
      END IF;

      /*In case we missed an else case, we should check if the loc types have been determined*/
      IF (vDLocTypeLH IS NULL OR vDLocType IS NULL)
      THEN
         vDPassFail := 1;
      END IF;

      IF (vDPassFail = 1)
      THEN
         INSERT_RATING_LANE_ERROR (vTCCompanyId,
                                   vLaneId,
                                   'Destination Definition is invalid',
                                   4720112,
                                   NULL);
         COMMIT;
      END IF;

      IF (vDPassFail = 1)
      THEN
         vPassFail := 1;
      END IF;

      IF (vPassFail = 0)
      THEN
         -- calculate lane hierarchy
         BEGIN
            SELECT ORIGIN_VALUE
              INTO vOLocValue
              FROM RATING_LANE_HIERARCHY
             WHERE TC_COMPANY_ID = vTCCompanyId
                   AND LOCATION_TYPE = vOLocTypeLH;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               vOLocValue := NULL;
         END;

         BEGIN
            SELECT DEST_VALUE
              INTO vDLocValue
              FROM RATING_LANE_HIERARCHY
             WHERE TC_COMPANY_ID = vTCCompanyId
                   AND LOCATION_TYPE = vDLocTypeLH;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               vDLocValue := NULL;
         END;

         IF (vOLocValue IS NOT NULL AND vDLocValue IS NOT NULL)
         THEN
            vLHValue := vOLocValue + vDLocValue;
         ELSE
            -- calculate default hierarchy
            SELECT DECODE (vOLocTypeLH,
                           'FA', 100,
                           'ZFA', 200,
                           'P6', 300,
                           'Z6', 400,
                           'P5', 500,
                           'Z5', 600,
                           'CS', 700,
                           'ZCS', 750,
                           'P4', 800,
                           'Z4', 900,
                           'P3', 1000,
                           'Z3', 1100,
                           'P2', 1200,
                           'Z2', 1300,
                           'ST', 1400,
                           'ZST', 1500,
                           'CO', 1600,
                           'ZCO', 1700,
                           'PR', 1800,
                           0)
              INTO vOLocValue
              FROM DUAL;

            SELECT DECODE (vDLocTypeLH,
                           'FA', 101,
                           'ZFA', 202,
                           'P6', 303,
                           'Z6', 404,
                           'P5', 505,
                           'Z5', 606,
                           'CS', 707,
                           'ZCS', 757,
                           'P4', 808,
                           'Z4', 909,
                           'P3', 1010,
                           'Z3', 1111,
                           'P2', 1212,
                           'Z2', 1313,
                           'ST', 1414,
                           'ZST', 1515,
                           'CO', 1616,
                           'ZCO', 1717,
                           'PR', 1818,
                           0)
              INTO vDLocValue
              FROM DUAL;

            IF (vOLocValue = 0 OR vDLocValue = 0)
            THEN
               vPassFail := 1;
               vLHValue := 0;
            ELSE
               vLHValue := vOLocValue + vDLocValue;
               vLHValue := vLHValue + 4000 + 4100 + 4200;
            END IF;
         END IF;

         -- update the entry in the database
         UPDATE IMPORT_RATING_LANE
            SET LANE_HIERARCHY = vLHValue,
                O_LOC_TYPE = vOLocType,
                D_LOC_TYPE = vDLocType
          WHERE TC_COMPANY_ID = vTCCompanyId AND LANE_ID = vLaneId;

         IF (vPassFail = 1)
         THEN
            INSERT_RATING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               'An error prevented the Lane Hierarchy value from being calculated.',
               4720113,
               NULL);
         END IF;

         COMMIT;
      END IF;

      RETURN vPassFail;
   END VALIDATE_HIERARCHY;

   FUNCTION VALIDATE_TP_EQUIP_SERV_MODE (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vLaneId                IN rating_lane.LANE_ID%TYPE,
      vRatingDtlSeq          IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vCarrierCode           IN CARRIER_CODE.CARRIER_CODE%TYPE,
      vServiceLevel          IN SERVICE_LEVEL.SERVICE_LEVEL%TYPE,
      vEquipment             IN EQUIPMENT.EQUIPMENT_CODE%TYPE,
      vMode                  IN MOT.MOT%TYPE,
      p_scndr_carrier_code   IN CARRIER_CODE.CARRIER_CODE%TYPE)
      RETURN NUMBER
   IS
      vPassFail            NUMBER;
      vCount               NUMBER;
      vCarrierId           CARRIER_CODE.CARRIER_ID%TYPE;
      p_Scndr_Carrier_Id   CARRIER_CODE.CARRIER_ID%TYPE;
	  FEACARR             VARCHAR2 (100);
   BEGIN
      /*
      Horea 03/30/2002
      initialize vPassFail
      */
      vPassFail := 0;

      /*
          if the carrier code is null (budgeted carrier) no validation is needed here
      */
      IF (vCarrierCode IS NOT NULL)
      THEN
	  
	  FEACARR := VCARRIERCODE;
              IF (P_SCNDR_CARRIER_CODE IS NOT NULL) THEN
                  FEACARR := VCARRIERCODE || ' or ' || P_SCNDR_CARRIER_CODE;
              END IF;
	  
         SELECT carrier_id, SCNDR_CARRIER_ID
           INTO vCarrierId, p_Scndr_Carrier_Id
           FROM IMPORT_RATING_LANE_DTL
          WHERE lane_id = vLaneId AND RATING_LANE_DTL_SEQ = vRatingDtlSeq;


         IF (vEquipment IS NOT NULL)
         THEN
            SELECT COUNT (1)
              INTO vCount
              FROM TP_COMPANY_EQUIPMENT TCE, CARRIER_CODE cc, EQUIPMENT eqp
             WHERE                          --TCE.TC_COMPANY_ID = vTCCompanyID
                       --and
                       TCE.carrier_id = cc.carrier_id
                   AND cc.CARRIER_ID IN (vCarrierId, p_Scndr_Carrier_Id)
                   AND eqp.EQUIPMENT_CODE = vEquipment
                   AND eqp.equipment_id = tce.equipment_id;

            IF (vCount < 1)
            THEN
               vPassFail := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                     '('
                  || vCarrierCode
                  || ' or '
                  || p_scndr_carrier_code
                  || ') and '
                  || vEquipment
                  || ' is not feasible',
                  2460218,
                     FEACARR
                  || '<sep>'
                  || vEquipment);
            END IF;
         END IF;

         IF (vServiceLevel IS NOT NULL)
         THEN
            SELECT COUNT (1)
              INTO vCount
              FROM TP_COMPANY_SERVICE_LEVEL TCS,
                   CARRIER_CODE cc,
                   SERVICE_LEVEL sl
             WHERE                          --TCS.TC_COMPANY_ID = vTCCompanyID
                       --and
                       cc.CARRIER_ID IN (vCarrierId, p_Scndr_Carrier_Id)
                   AND TCS.carrier_id = cc.carrier_id
                   AND sl.SERVICE_LEVEL = vServiceLevel
                   AND TCS.service_level_id = sl.service_level_id;

            IF (vCount < 1)
            THEN
               vPassFail := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                     '('
                  || vCarrierCode
                  || ' or '
                  || p_scndr_carrier_code
                  || ') and '
                  || vServiceLevel
                  || ' is not feasible',
                  2460219,
                     FEACARR
                  || '<sep>'
                  || vServiceLevel);
            END IF;
         END IF;

         IF (vMode IS NOT NULL)
         THEN
            SELECT COUNT (1)
              INTO vCount
              FROM CARRIER_CODE_MOT ccm, CARRIER_CODE cc, MOT mt
             WHERE                          --ccm.TC_COMPANY_ID = vTCCompanyID
                       --and
                       cc.CARRIER_ID IN (vCarrierId, p_Scndr_Carrier_Id)
                   AND ccm.carrier_id = cc.carrier_id
                   AND mt.MOT = vMode
                   AND ccm.mot_id = mt.mot_id;

            IF (vCount < 1)
            THEN
               vPassFail := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRatingDtlSeq,
                     '('
                  || vCarrierCode
                  || ' or '
                  || p_scndr_carrier_code
                  || ') and '
                  || vMode
                  || ' is not feasible',
                  2460220,
                     FEACARR
                  || '<sep>'
                  || vMode);
            END IF;
         END IF;
      END IF;

      RETURN vPassFail;
   END VALIDATE_TP_EQUIP_SERV_MODE;

   FUNCTION VALIDATE_FEASIBLE (
      vTCCompanyId               IN COMPANY.COMPANY_ID%TYPE,
      vLaneId                    IN rating_lane.LANE_ID%TYPE,
      vRatingDtlSeq              IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vCarrierCode               IN CARRIER_CODE.CARRIER_CODE%TYPE,
      vCarrierCodeBUID           IN COMPANY.COMPANY_ID%TYPE,
      vEquipment                 IN EQUIPMENT.EQUIPMENT_CODE%TYPE,
      vEquipmentBUID             IN COMPANY.COMPANY_ID%TYPE,
      vProtectionLevel           IN PROTECTION_LEVEL.PROTECTION_LEVEL%TYPE,
      vProtectionLevelBUID       IN COMPANY.COMPANY_ID%TYPE,
      p_scndr_carrier_code       IN CARRIER_CODE.CARRIER_CODE%TYPE,
      p_scndr_carrier_codeBUID   IN COMPANY.COMPANY_ID%TYPE)
      RETURN NUMBER
   IS
      vCount               NUMBER;
      vPassFail            NUMBER;
      vCarrierId           NUMBER;
      vEquipmentId         NUMBER;
      vProtectionLevelId   NUMBER;
      p_scndr_carrierId    NUMBER;
      vBUId                NUMBER;
      errorMsg             VARCHAR2 (1000);
   BEGIN
      vCount := 0;
      vPassFail := 0;

      IF (vProtectionLevel IS NOT NULL)
      THEN
         SELECT COUNT (protection_Level_Id)
           INTO vCount
           FROM PROTECTION_LEVEL
          WHERE tc_company_id = vProtectionLevelBUID
                AND PROTECTION_LEVEL = vProtectionLevel;
      END IF;

      IF (vCount > 0)
      THEN
         SELECT protection_Level_Id
           INTO vProtectionLevelId
           FROM PROTECTION_LEVEL
          WHERE tc_company_id = vProtectionLevelBUID
                AND PROTECTION_LEVEL = vProtectionLevel;
      END IF;

      vCount := 0;

      IF (vEquipment IS NOT NULL)
      THEN
         SELECT COUNT (equipment_Id)
           INTO vCount
           FROM EQUIPMENT
          WHERE tc_company_id = vEquipmentBUID
                AND equipment_code = vEquipment;
      END IF;

      IF (vCount > 0)
      THEN
         SELECT equipment_Id
           INTO vEquipmentId
           FROM EQUIPMENT
          WHERE tc_company_id = vEquipmentBUID
                AND equipment_code = vEquipment;
      END IF;

      vCount := 0;

      IF (vCarrierCode IS NOT NULL)
      THEN
         SELECT COUNT (carrier_id)
           INTO vCount
           FROM CARRIER_CODE
          WHERE tc_company_id = vCarrierCodeBUID
                AND CARRIER_CODE = vCarrierCode
				AND MARK_FOR_DELETION = 0;
      END IF;

      IF (vCount > 0)
      THEN
         SELECT carrier_id
           INTO vCarrierId
           FROM CARRIER_CODE
          WHERE tc_company_id = vCarrierCodeBUID
                AND CARRIER_CODE = vCarrierCode
				AND MARK_FOR_DELETION = 0;
      END IF;

      vCount := 0;

      IF (p_scndr_carrier_code IS NOT NULL)
      THEN
         SELECT COUNT (carrier_id)
           INTO vCount
           FROM CARRIER_CODE
          WHERE tc_company_id = p_scndr_carrier_codeBUID
                AND CARRIER_CODE = p_scndr_carrier_code
				AND MARK_FOR_DELETION = 0;
      END IF;

      IF (vCount > 0)
      THEN
         SELECT carrier_id
           INTO p_scndr_carrierId
           FROM CARRIER_CODE
          WHERE tc_company_id = p_scndr_carrier_codeBUID
                AND CARRIER_CODE = p_scndr_carrier_code
				AND MARK_FOR_DELETION = 0;
      END IF;

      IF (vProtectionLevel IS NOT NULL)
      THEN
         SELECT COUNT (INFEASIBILITY_ID)
           INTO vCount
           FROM INFEASIBILITY
          WHERE     INFEASIBILITY_TYPE = 'PLEQ'
                AND TC_COMPANY_ID = vTCCompanyId
                AND value1 = TO_CHAR (vProtectionLevelId)
                AND VALUE2 = TO_CHAR (vEquipmentId);

         IF (vCount > 0)
         THEN
            vCount := 0;

            SELECT COUNT (INFEASIBILITY_ID)
              INTO vCount
              FROM INFEASIBILITY
             WHERE     INFEASIBILITY_TYPE = 'FAPLEQ'
                   AND TC_COMPANY_ID = vTCCompanyId
                   AND VALUE1 = (SELECT O_FACILITY_ID
                                   FROM IMPORT_RATING_LANE
                                  WHERE LANE_ID = vLaneId)
                   AND VALUE2 = TO_CHAR (vProtectionLevelId)
                   AND VALUE3 = TO_CHAR (vEquipmentId);

            IF (vCount < 1)
            THEN
               vPassFail := 1;
            END IF;
         ELSE     -- CR: if no data found in the infeasible table, it is valid
            vPassFail := 0;
         END IF;

         IF (vPassFail = 1)
         THEN
            INSERT_RATING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRatingDtlSeq,
                  vProtectionLevel
               || ' is infeasible'
               || ' with equipment '
               || vEquipment,
               2460223,
               vProtectionLevel || '<sep>' || vEquipment);
         END IF;
      END IF;

      SELECT COUNT (INFEASIBILITY_ID)
        INTO vCount
        FROM INFEASIBILITY
       WHERE     INFEASIBILITY_TYPE = 'FAEQ'
             AND VALUE1 = (SELECT O_FACILITY_ID
                             FROM IMPORT_RATING_LANE
                            WHERE LANE_ID = vLaneId)
             AND VALUE2 = TO_CHAR (vEquipmentId)
             AND TC_COMPANY_ID = vTCCompanyId;

      IF (vCount > 0)
      THEN
         vPassFail := 1;
         INSERT_RATING_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRatingDtlSeq,
            'Origin Facility is infeasible with equipment ' || vEquipment,
            4720167,
            vEquipment);
      END IF;

      SELECT COUNT (INFEASIBILITY_ID)
        INTO vCount
        FROM INFEASIBILITY
       WHERE     INFEASIBILITY_TYPE = 'FAEQ'
             AND VALUE1 = (SELECT D_FACILITY_ID
                             FROM IMPORT_RATING_LANE
                            WHERE LANE_ID = vLaneId)
             AND VALUE2 = TO_CHAR (vEquipmentId)
             AND TC_COMPANY_ID = vTCCompanyId;

      IF (vCount > 0)
      THEN
         vPassFail := 1;
         INSERT_RATING_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRatingDtlSeq,
            'Destination Facility is infeasible with equipment '
            || vEquipment,
            4720168,
            vEquipment);
      END IF;

      -- SELECT COUNT(INFEASIBILITY_ID) INTO vCount
      --  FROM INFEASIBILITY
      --  WHERE INFEASIBILITY_TYPE = 'FATP'
      --  AND VALUE1 = ( SELECT FACILITY_ID
      --       FROM FACILITY_ALIAS
      --       WHERE FACILITY_ALIAS_ID = ( SELECT O_FACILITY_ALIAS_ID
      --               FROM IMPORT_RATING_LANE
      --               WHERE LANE_ID = vLaneId
      --               AND TC_COMPANY_ID = vTCCompanyID)
      --       AND TC_COMPANY_ID = vTCCompanyId)
      --  AND VALUE2 IN (TO_CHAR(vCarrierId),TO_CHAR(p_scndr_carrierId));

      --IF(vCount > 0) THEN
      --  vPassFail := 1;



      --  INSERT_RATING_LANE_DTL_ERROR
      --  (
      --   vTCCompanyId,
      --   vLaneId,
      --   vRatingDtlSeq,
      --     errorMsg,
      --   4720169,
      --   vCarrierCode || '<sep>' || p_scndr_carrier_code
      --  );
      -- END IF;
      -- SELECT COUNT(INFEASIBILITY_ID) INTO vCount
      --  FROM INFEASIBILITY
      --  WHERE INFEASIBILITY_TYPE = 'FATP'
      --  AND VALUE1 = ( SELECT FACILITY_ID
      --       FROM FACILITY_ALIAS
      --       WHERE FACILITY_ALIAS_ID = ( SELECT D_FACILITY_ALIAS_ID
      --               FROM IMPORT_RATING_LANE
      --               WHERE LANE_ID = vLaneId
      --               AND TC_COMPANY_ID = vTCCompanyID)
      --       AND TC_COMPANY_ID = vTCCompanyId)
      --  AND VALUE2 IN (TO_CHAR(vCarrierId),TO_CHAR(p_scndr_carrierId));

      --IF(vCount > 0) THEN
      --  vPassFail := 1;


      --  INSERT_RATING_LANE_DTL_ERROR
      --  (
      --   vTCCompanyId,
      --   vLaneId,
      --   vRatingDtlSeq,
      --        errorMsg,
      --   4720170,
      --   vCarrierCode || '<sep>' || p_scndr_carrier_code
      --  );
      -- END IF;
      RETURN vPassFail;
   END VALIDATE_FEASIBLE;

   FUNCTION VALIDATE_FAC_CARR_FEASIBLE (
      vTCCompanyId               IN COMPANY.COMPANY_ID%TYPE,
      vLaneId                    IN rating_lane.LANE_ID%TYPE,
      vRatingDtlSeq              IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vCarrierCode               IN CARRIER_CODE.CARRIER_CODE%TYPE,
      vCarrierCodeBUID           IN COMPANY.COMPANY_ID%TYPE,
      p_scndr_carrier_code       IN CARRIER_CODE.CARRIER_CODE%TYPE,
      p_scndr_carrier_codeBUID   IN COMPANY.COMPANY_ID%TYPE)
      RETURN NUMBER
   IS
      vCount              NUMBER;
      vPassFail           NUMBER;
      vCarrierId          NUMBER;
      p_scndr_carrierId   NUMBER;
      vBUId               NUMBER;
      errorMsg            VARCHAR2 (1000);
   BEGIN
      vCount := 0;
      vPassFail := 0;
      vCarrierId := NULL;
      p_scndr_carrierId := NULL;


      SELECT COUNT (*)
        INTO vCount
        FROM CARRIER_CODE
       WHERE tc_company_id = vCarrierCodeBUID AND CARRIER_CODE = vCarrierCode
				AND MARK_FOR_DELETION = 0;

      IF (vCount > 0)
      THEN
         SELECT carrier_id
           INTO vCarrierId
           FROM CARRIER_CODE
          WHERE tc_company_id = vCarrierCodeBUID
                AND CARRIER_CODE = vCarrierCode
				AND MARK_FOR_DELETION = 0;
      END IF;


      SELECT COUNT (*)
        INTO vCount
        FROM CARRIER_CODE
       WHERE tc_company_id = p_scndr_carrier_codeBUID
             AND CARRIER_CODE = p_scndr_carrier_code
				AND MARK_FOR_DELETION = 0;

      IF (vCount > 0)
      THEN
         SELECT carrier_id
           INTO p_scndr_carrierId
           FROM CARRIER_CODE
          WHERE tc_company_id = p_scndr_carrier_codeBUID
                AND CARRIER_CODE = p_scndr_carrier_code
				AND MARK_FOR_DELETION = 0;
      END IF;

      SELECT COUNT (INFEASIBILITY_ID)
        INTO vCount
        FROM INFEASIBILITY
       WHERE INFEASIBILITY_TYPE = 'FATP'
             AND VALUE1 = (SELECT O_FACILITY_ID
                             FROM IMPORT_RATING_LANE
                            WHERE LANE_ID = vLaneId)
             AND value2 IN
                    (TO_CHAR (vCarrierId), TO_CHAR (p_scndr_carrierId))
             AND TC_COMPANY_ID = vTCCompanyId;



      IF (vCount > 0)
      THEN
         IF (p_scndr_carrier_code != NULL)
         THEN
            errorMsg :=
                  'Origin Facility is infeasible with carriers '
               || vCarrierCode
               || ' or '
               || p_scndr_carrier_code;
         ELSE
            errorMsg :=
               'Origin Facility is infeasible with carrier ' || vCarrierCode;
         END IF;

         vPassFail := 1;
		 
		IF (p_scndr_carrier_code != NULL)
         THEN
            INSERT_RATING_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRatingDtlSeq,
            errorMsg,
            4720048,
            vCarrierCode || '<sep>' || p_scndr_carrier_code);
         ELSE
            INSERT_RATING_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRatingDtlSeq,
            errorMsg,
            2460221,
            vCarrierCode);
         END IF;
      END IF;

      SELECT COUNT (INFEASIBILITY_ID)
        INTO vCount
        FROM INFEASIBILITY
       WHERE INFEASIBILITY_TYPE = 'FATP'
             AND VALUE1 = (SELECT D_FACILITY_ID
                             FROM IMPORT_RATING_LANE
                            WHERE LANE_ID = vLaneId)
             AND value2 IN
                    (TO_CHAR (vCarrierId), TO_CHAR (p_scndr_carrierId))
             AND TC_COMPANY_ID = vTCCompanyId;

      IF (vCount > 0)
      THEN
         IF (p_scndr_carrier_code != NULL)
         THEN
            errorMsg :=
                  'Destination Facility is infeasible with carriers '
               || vCarrierCode
               || ' or '
               || p_scndr_carrier_code;
         ELSE
            errorMsg :=
               'Destination Facility is infeasible with carrier '
               || vCarrierCode;
         END IF;

         vPassFail := 1;
		 
		 IF (p_scndr_carrier_code != NULL)
         THEN
            INSERT_RATING_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRatingDtlSeq,
            errorMsg,
            4720049,
            vCarrierCode || '<sep>' || p_scndr_carrier_code);
         ELSE
            INSERT_RATING_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRatingDtlSeq,
            errorMsg,
            2460222,
            vCarrierCode);
         END IF;

      END IF;

      RETURN vPassFail;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Procedure VALIDATE_FAC_CARR_FEASIBLE: ' || SQLERRM);
         RAISE;
   END VALIDATE_FAC_CARR_FEASIBLE;

   FUNCTION GET_BUDG_RESOURCE_WHERE_CLAUSE (
      vMot                   IN rating_lane_dtl.MOT_ID%TYPE,
      vEquipment             IN rating_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN rating_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN RATING_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN rating_lane_dtl.PROTECTION_LEVEL_ID%TYPE)
      RETURN VARCHAR2
   IS
      resource_sql   VARCHAR2 (2000);
   BEGIN
      resource_sql := '';

      IF (vMot IS NOT NULL)
      THEN
         resource_sql := resource_sql || ' AND mot_id = ''' || vMot || ''' ';
      ELSE
         resource_sql := resource_sql || ' AND mot_id IS Null ';
      END IF;

      IF (vEquipment IS NOT NULL)
      THEN
         resource_sql :=
            resource_sql || ' AND equipment_id = ''' || vEquipment || ''' ';
      ELSE
         resource_sql := resource_sql || ' AND equipment_id IS Null ';
      END IF;

      IF (vServiceLevel IS NOT NULL)
      THEN
         resource_sql :=
               resource_sql
            || ' AND service_level_id = '''
            || vServiceLevel
            || ''' ';
      ELSE
         resource_sql := resource_sql || ' AND service_level_id IS Null ';
      END IF;

      IF (vProtectionLevel IS NOT NULL)
      THEN
         resource_sql :=
               resource_sql
            || ' AND protection_level_id = '''
            || vProtectionLevel
            || ''' ';
      ELSE
         resource_sql := resource_sql || ' AND protection_level_id IS Null ';
      END IF;

      IF (p_scndr_carrier_code IS NOT NULL)
      THEN
         resource_sql :=
               resource_sql
            || ' and SCNDR_CARRIER_ID = '''
            || p_scndr_carrier_code
            || ''' ';
      ELSE
         resource_sql := resource_sql || ' and SCNDR_CARRIER_ID is NULL ';
      END IF;

      RETURN resource_sql;
   END GET_BUDG_RESOURCE_WHERE_CLAUSE;

   FUNCTION GET_BUDG_RESOURCE_WHERE_CLAUSE (
      vMot                   IN rating_lane_dtl.MOT_ID%TYPE,
      vEquipment             IN rating_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN rating_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN RATING_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN rating_lane_dtl.PROTECTION_LEVEL_ID%TYPE,
      vCustomText1           IN COMB_LANE_DTL.CUSTOM_TEXT1%TYPE,
      vCustomText2           IN COMB_LANE_DTL.CUSTOM_TEXT2%TYPE,
      vCustomText3           IN COMB_LANE_DTL.CUSTOM_TEXT3%TYPE,
      vCustomText4           IN COMB_LANE_DTL.CUSTOM_TEXT4%TYPE,
      vCustomText5           IN COMB_LANE_DTL.CUSTOM_TEXT5%TYPE)
      RETURN VARCHAR2
   IS
      resource_sql   VARCHAR2 (2000);
   BEGIN
      resource_sql := '';

      IF (vMot IS NOT NULL)
      THEN
         resource_sql := resource_sql || ' AND mot_id = ''' || vMot || ''' ';
      ELSE
         resource_sql := resource_sql || ' AND mot_id IS Null ';
      END IF;

      IF (vEquipment IS NOT NULL)
      THEN
         resource_sql :=
            resource_sql || ' AND equipment_id = ''' || vEquipment || ''' ';
      ELSE
         resource_sql := resource_sql || ' AND equipment_id IS Null ';
      END IF;

      IF (vServiceLevel IS NOT NULL)
      THEN
         resource_sql :=
               resource_sql
            || ' AND service_level_id = '''
            || vServiceLevel
            || ''' ';
      ELSE
         resource_sql := resource_sql || ' AND service_level_id IS Null ';
      END IF;

      IF (vProtectionLevel IS NOT NULL)
      THEN
         resource_sql :=
               resource_sql
            || ' AND protection_level_id = '''
            || vProtectionLevel
            || ''' ';
      ELSE
         resource_sql := resource_sql || ' AND protection_level_id IS Null ';
      END IF;

      IF (p_scndr_carrier_code IS NOT NULL)
      THEN
         resource_sql :=
               resource_sql
            || ' and SCNDR_CARRIER_ID = '''
            || p_scndr_carrier_code
            || ''' ';
      ELSE
         resource_sql := resource_sql || ' and SCNDR_CARRIER_ID is NULL ';
      END IF;

      IF (VCUSTOMTEXT1 IS NOT NULL)
      THEN
         resource_sql :=
            resource_sql || ' AND CUSTOM_TEXT1 = ''' || VCUSTOMTEXT1 || ''' ';
      ELSE
         resource_sql := resource_sql || ' AND  CUSTOM_TEXT1 IS Null ';
      END IF;

      IF (VCUSTOMTEXT2 IS NOT NULL)
      THEN
         resource_sql :=
            resource_sql || ' AND CUSTOM_TEXT2 = ''' || VCUSTOMTEXT2 || ''' ';
      ELSE
         resource_sql := resource_sql || ' AND  CUSTOM_TEXT2 IS Null ';
      END IF;

      IF (VCUSTOMTEXT3 IS NOT NULL)
      THEN
         resource_sql :=
            resource_sql || ' AND CUSTOM_TEXT3 = ''' || VCUSTOMTEXT3 || ''' ';
      ELSE
         resource_sql := resource_sql || ' AND  CUSTOM_TEXT3 IS Null ';
      END IF;

      IF (VCUSTOMTEXT4 IS NOT NULL)
      THEN
         resource_sql :=
            resource_sql || ' AND CUSTOM_TEXT4 = ''' || VCUSTOMTEXT4 || ''' ';
      ELSE
         resource_sql := resource_sql || ' AND  CUSTOM_TEXT4 IS Null ';
      END IF;

      IF (VCUSTOMTEXT5 IS NOT NULL)
      THEN
         resource_sql :=
            resource_sql || ' AND CUSTOM_TEXT5 = ''' || VCUSTOMTEXT5 || ''' ';
      ELSE
         resource_sql := resource_sql || ' AND  CUSTOM_TEXT5 IS Null ';
      END IF;

      RETURN resource_sql;
   END GET_BUDG_RESOURCE_WHERE_CLAUSE;

   FUNCTION GET_RESOURCE_WHERE_CLAUSE (
      vCarrierCode           IN rating_lane_dtl.CARRIER_ID%TYPE,
      vMot                   IN rating_lane_dtl.MOT_ID%TYPE,
      vEquipment             IN rating_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN rating_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN RATING_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN rating_lane_dtl.PROTECTION_LEVEL_ID%TYPE,
      vOriginShipVia         IN rating_lane_dtl.O_SHIP_VIA%TYPE,
      vDestinationShipVia    IN rating_lane_dtl.D_SHIP_VIA%TYPE)
      RETURN VARCHAR2
   IS
      resource_sql   VARCHAR2 (2000);
   BEGIN
      resource_sql := '';

      IF (vCarrierCode IS NOT NULL)
      THEN
         resource_sql :=
            resource_sql || ' AND carrier_id = ''' || vCarrierCode || ''' ';
      ELSE
         resource_sql := resource_sql || ' AND carrier_id IS Null ';
      END IF;

      IF (vOriginShipVia IS NOT NULL)
      THEN
         resource_sql :=
            resource_sql || ' AND o_ship_via = ''' || vOriginShipVia || ''' ';
      ELSE
         resource_sql := resource_sql || ' AND o_ship_via IS Null ';
      END IF;

      IF (vDestinationShipVia IS NOT NULL)
      THEN
         resource_sql :=
               resource_sql
            || ' AND d_ship_via = '''
            || vDestinationShipVia
            || ''' ';
      ELSE
         resource_sql := resource_sql || ' AND d_ship_via IS Null ';
      END IF;

      resource_sql :=
         resource_sql
         || GET_BUDG_RESOURCE_WHERE_CLAUSE (vMot,
                                            vEquipment,
                                            vServiceLevel,
                                            p_scndr_carrier_code,
                                            vProtectionLevel);
      RETURN resource_sql;
   END GET_RESOURCE_WHERE_CLAUSE;

   FUNCTION GET_RESOURCE_WHERE_CLAUSE (
      vCarrierCode           IN rating_lane_dtl.CARRIER_ID%TYPE,
      vMot                   IN rating_lane_dtl.MOT_ID%TYPE,
      vEquipment             IN rating_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN rating_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN RATING_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN rating_lane_dtl.PROTECTION_LEVEL_ID%TYPE,
      vOriginShipVia         IN rating_lane_dtl.O_SHIP_VIA%TYPE,
      vDestinationShipVia    IN rating_lane_dtl.D_SHIP_VIA%TYPE,
      vCustomText1           IN COMB_LANE_DTL.CUSTOM_TEXT1%TYPE,
      vCustomText2           IN COMB_LANE_DTL.CUSTOM_TEXT2%TYPE,
      vCustomText3           IN COMB_LANE_DTL.CUSTOM_TEXT3%TYPE,
      vCustomText4           IN COMB_LANE_DTL.CUSTOM_TEXT4%TYPE,
      vCustomText5           IN COMB_LANE_DTL.CUSTOM_TEXT5%TYPE,
	  vVoyage				 IN COMB_LANE_DTL.VOYAGE%TYPE)
      RETURN VARCHAR2
   IS
      resource_sql   VARCHAR2 (2000);
   BEGIN
      resource_sql := '';

      IF (vCarrierCode IS NOT NULL)
      THEN
         resource_sql :=
            resource_sql || ' AND carrier_id = ''' || vCarrierCode || ''' ';
      ELSE
         resource_sql := resource_sql || ' AND carrier_id IS Null ';
      END IF;

      IF (vOriginShipVia IS NOT NULL)
      THEN
         resource_sql :=
            resource_sql || ' AND o_ship_via = ''' || vOriginShipVia || ''' ';
      ELSE
         resource_sql := resource_sql || ' AND o_ship_via IS Null ';
      END IF;

      IF (vDestinationShipVia IS NOT NULL)
      THEN
         resource_sql :=
               resource_sql
            || ' AND d_ship_via = '''
            || vDestinationShipVia
            || ''' ';
      ELSE
         resource_sql := resource_sql || ' AND d_ship_via IS Null ';
      END IF;

	IF (vVoyage IS NOT NULL)
	THEN
		resource_sql := resource_sql || ' AND voyage = '''|| vVoyage || ''' ';
	ELSE
		resource_sql := resource_sql || ' AND voyage IS Null ';
	END IF;

      resource_sql :=
         resource_sql
         || GET_BUDG_RESOURCE_WHERE_CLAUSE (vMot,
                                            vEquipment,
                                            vServiceLevel,
                                            p_scndr_carrier_code,
                                            vProtectionLevel,
                                            vCustomText1,
                                            vCustomText2,
                                            vCustomText3,
                                            vCustomText4,
                                            vCustomText5);
      RETURN resource_sql;
   END GET_RESOURCE_WHERE_CLAUSE;

   PROCEDURE MERGE_DRAFT (
      vTCCompanyId     IN     COMPANY.COMPANY_ID%TYPE,
      vUser            IN     COMB_LANE_DTL.LAST_UPDATED_SOURCE%TYPE,
      vLaneId          IN     COMB_LANE.LANE_ID%TYPE,
      vLaneDtlSeq      IN     COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vLaneDtlSeqUpd   IN     COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      REC_CNT             OUT NUMBER)
   AS
   BEGIN
      REC_CNT :=
         MERGE_DRAFT (vTCCompanyId,
                      vUser,
                      vLaneId,
                      vLaneDtlSeq,
                      vLaneDtlSeqUpd);
   END MERGE_DRAFT;


   PROCEDURE ADJUST_EFF_EXP_DT (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vUser                  IN COMB_LANE_DTL.LAST_UPDATED_SOURCE%TYPE,
      vLaneId                IN COMB_LANE.LANE_ID%TYPE,
      vRatingDtlSeq          IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vEffectiveDT           IN COMB_LANE_DTL.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN COMB_LANE_DTL.EXPIRATION_DT%TYPE,
      vCarrierCode           IN COMB_LANE_DTL.CARRIER_ID%TYPE,
      vMot                   IN COMB_LANE_DTL.MOT_ID%TYPE,
      vEquipment             IN COMB_LANE_DTL.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN COMB_LANE_DTL.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN COMB_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN COMB_LANE_DTL.PROTECTION_LEVEL_ID%TYPE,
      vOriginShipVia         IN COMB_LANE_DTL.O_SHIP_VIA%TYPE,
      vDestinationShipVia    IN COMB_LANE_DTL.D_SHIP_VIA%TYPE,
	  vContractNumber        IN COMB_LANE_DTL.CONTRACT_NUMBER%TYPE,
      vCustomText1           IN COMB_LANE_DTL.CUSTOM_TEXT1%TYPE,
      vCustomText2           IN COMB_LANE_DTL.CUSTOM_TEXT2%TYPE,
      vCustomText3           IN COMB_LANE_DTL.CUSTOM_TEXT3%TYPE,
      vCustomText4           IN COMB_LANE_DTL.CUSTOM_TEXT4%TYPE,
      vCustomText5           IN COMB_LANE_DTL.CUSTOM_TEXT5%TYPE,
      vDoNotDeleteRate       IN COMB_LANE.LANE_ID%TYPE,
	  vVoyage				 IN COMB_LANE_DTL.VOYAGE%TYPE)
   IS
      eff_exp_sql                VARCHAR2 (2000);
      eff_exp_cursor             ref_curtype;
      vCurrentRatingLaneDtlSeq   COMB_LANE_DTL.lane_id%TYPE;
      vEffDT                     COMB_LANE_DTL.effective_dt%TYPE;
      vExpDT                     COMB_LANE_DTL.expiration_dt%TYPE;
      vExpDTTemp                 COMB_LANE_DTL.expiration_dt%TYPE;
      vIsRating                  COMB_LANE_DTL.IS_RATING%TYPE;
      vIsRouting                 COMB_LANE_DTL.IS_ROUTING%TYPE;
      vIsSailing                 COMB_LANE_DTL.IS_SAILING%TYPE;
      isFirstRow                 INT;
      isInsertAtEnd              INT;
      isChanged                  INT;
      vMotTemp                   COMB_LANE_DTL.MOT_ID%TYPE;
      vEquipmentTemp             COMB_LANE_DTL.EQUIPMENT_ID%TYPE;
      vServiceLevelTemp          COMB_LANE_DTL.SERVICE_LEVEL_ID%TYPE;
      vProtectionLevelTemp       COMB_LANE_DTL.PROTECTION_LEVEL_ID%TYPE;
      vIsBudgetedTemp            COMB_LANE_DTL.IS_BUDGETED%TYPE;
      vLastUpdatedSource         COMB_LANE_DTL.LAST_UPDATED_SOURCE%TYPE;
      vLastUpdatedSourceType     COMB_LANE_DTL.LAST_UPDATED_SOURCE_TYPE%TYPE;
	  tempSeqNum                  INT;
   BEGIN
      eff_exp_sql :=
         ' SELECT lane_dtl_seq, effective_dt, expiration_dt, is_rating, is_routing, is_sailing '
         || ' FROM comb_lane_dtl '
         || ' WHERE tc_company_id = :vTCCompanyId'
         || ' AND lane_id = :vLaneId'
         || ' AND lane_dtl_seq != :vRatingDtlSeq'
         || ' AND lane_dtl_status = 0 '
         || ' AND effective_dt <= expiration_dt '
         || ' AND expiration_dt >= :vEffectiveDT '
         || ' AND effective_dt <= :vExpirationDT ';

      vMotTemp := vMot;
      vEquipmentTemp := vEquipment;
      vServiceLevelTemp := vServiceLevel;
      vProtectionLevelTemp := vProtectionLevel;

       /*
IF( vMot = 'ALL' ) THEN
 vMotTemp := null;
END IF;

IF( vEquipment = 'ALL' ) THEN
 vEquipmentTemp := null;
END IF;

IF( vServiceLevel = 'ALL' ) THEN
 vServiceLevelTemp := null;
END IF;

IF( vProtectionLevel = 'ALL' ) THEN
 vProtectionLevelTemp := null;
END IF;
       */
      eff_exp_sql :=
         eff_exp_sql
         || GET_RESOURCE_WHERE_CLAUSE (vCarrierCode,
                                       vMotTemp,
                                       vEquipmentTemp,
                                       vServiceLevelTemp,
                                       p_scndr_carrier_code,
                                       vProtectionLevelTemp,
                                       vOriginShipVia,
                                       vDestinationShipVia,
                                       vCustomText1,
                                       vCustomText2,
                                       vCustomText3,
                                       vCustomText4,
                                       vCustomText5,
									   vVoyage);

      eff_exp_sql := eff_exp_sql || ' ORDER BY EFFECTIVE_DT';

      isInsertAtEnd := 1;
      isFirstRow := 0;
      isChanged := 0;

      -- SYSCO Merge
      IF (vUser IS NOT NULL)
      THEN
         vLastUpdatedSource := vUser;
         vLastUpdatedSourceType := 1;
      ELSE
         vLastUpdatedSource := 'OMS';
         vLastUpdatedSourceType := 3;
      END IF;

      SELECT IS_BUDGETED
        INTO vIsBudgetedTemp
        FROM COMB_LANE_DTL
       WHERE     TC_COMPANY_ID = vTCCompanyId
             AND LANE_ID = vLaneId
             AND LANE_DTL_SEQ = vRatingDtlSeq;

      OPEN eff_exp_cursor FOR eff_exp_sql
         USING vTCCompanyId,
               vLaneId,
               vRatingDtlSeq,
               vEffectiveDT,
               vExpirationDT;

      LOOP
         FETCH eff_exp_cursor
         INTO vCurrentRatingLaneDtlSeq,
              vEffDT,
              vExpDT,
              vIsRating,
              vIsRouting,
              vIsSailing;

         EXIT WHEN eff_exp_cursor%NOTFOUND;

         -- Start of Merge with other lane detail
         IF (vEffectiveDT <= vEffDt)
         THEN
            IF (isFirstRow = 0)
            THEN
               IF (vEffectiveDT <> vEffDt)
               THEN
                  -- create row from input effective date to effective date of current row
                  CREATE_NEW_ROW (vTCCompanyId,
                                  vUser,
                                  vLaneId,
                                  vRatingDtlSeq,
                                  vEffectiveDT,
                                  vEffDt - 1);
               END IF;
            ELSE
               IF (vExpDTTemp <> vEffDt)
               THEN
                  -- create row from input effective date to effective date of current row
                  CREATE_NEW_ROW (vTCCompanyId,
                                  vUser,
                                  vLaneId,
                                  vRatingDtlSeq,
                                  vExpDTTemp,
                                  vEffDt - 1);
               END IF;
            END IF;

            IF (vExpirationDT < vExpDT)
            THEN
               --replicate row from input expiration date to exp date of current row with rating flag turned off
               tempSeqNum := REPLICATE_ROW (vTCCompanyId,
                              vUser,
                              vLaneId,
                              -1,
                              vCurrentRatingLaneDtlSeq,
                              vExpirationDT + 1,
                              vExpDT,
                              vIsRating,
                              vIsRouting,
                              vIsSailing);

               UPDATE COMB_LANE_DTL
                  SET is_rating = 1,
                      expiration_dt = vExpirationDT,
                      is_budgeted = vIsBudgetedTemp,
                      last_updated_source_type = 3,
                      last_updated_source = 'OMS',
                      last_updated_dttm = Getdate (),
					  CONTRACT_NUMBER = vContractNumber 
                WHERE     tc_company_id = vTCCompanyId
                      AND lane_id = vLaneId
                      AND lane_dtl_seq = vCurrentRatingLaneDtlSeq;

               --COPY_CHILD_ROW( vTCCompanyId, vLaneId, vRatingDtlSeq, vCurrentRatingLaneDtlSeq,
               -- vCurrentRatingLaneDtlSeq, 1, 0, 0 );

               isInsertAtEnd := 1;
            ELSE
               UPDATE COMB_LANE_DTL
                  SET is_rating = 1,
                      is_budgeted = vIsBudgetedTemp,
                      last_updated_source_type = vLastUpdatedSourceType,
                      last_updated_source = vLastUpdatedSource,
                      last_updated_dttm = Getdate (),
					  CONTRACT_NUMBER = vContractNumber 
                WHERE     tc_company_id = vTCCompanyId
                      AND lane_id = vLaneId
                      AND lane_dtl_seq = vCurrentRatingLaneDtlSeq;

               --COPY_CHILD_ROW( vTCCompanyId, vLaneId, vRatingDtlSeq, vCurrentRatingLaneDtlSeq,
               -- vCurrentRatingLaneDtlSeq, 1, 0, 0 );

               isInsertAtEnd := 0;
            END IF;

            IF (vIsRating = 1)
            THEN
               IF (vDoNotDeleteRate = 0)
               THEN
                  DELETE_CHILD_ROW (vTCCompanyId,
                                    vLaneId,
                                    vCurrentRatingLaneDtlSeq);
               END IF;
            END IF;

            COPY_CHILD_ROW (vTCCompanyId,
                            vLaneId,
                            vRatingDtlSeq,
                            vCurrentRatingLaneDtlSeq,
                            vCurrentRatingLaneDtlSeq,
                            1,
                            0,
                            0,vLastUpdatedSource);
         ELSE
            UPDATE COMB_LANE_DTL
               SET expiration_dt = vEffectiveDT - 1,
                   last_updated_source_type = vLastUpdatedSourceType,
                   last_updated_source = vLastUpdatedSource,
                   last_updated_dttm = Getdate ()
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND lane_dtl_seq = vCurrentRatingLaneDtlSeq;

            IF (vExpirationDT < vExpDT)
            THEN
               --replicate row from input effective date to input exp date with rating flag turned on
               tempSeqNum := REPLICATE_ROW (vTCCompanyId,
                              vUser,
                              vLaneId,
                              vRatingDtlSeq,
                              vCurrentRatingLaneDtlSeq,
                              vEffectiveDT,
                              vExpirationDT,
                              1,
                              vIsRouting,
                              vIsSailing);
               --replicate row from input expiration date to exp date of current row with rating flag turned off
               tempSeqNum := REPLICATE_ROW (vTCCompanyId,
                              vUser,
                              vLaneId,
                              -1,
                              vCurrentRatingLaneDtlSeq,
                              vExpirationDT + 1,
                              vExpDT,
                              vIsRating,
                              vIsRouting,
                              vIsSailing);
               isInsertAtEnd := 1;
            ELSE
               --replicate row from input eff date to exp date of current row with rating flag turned on
              tempSeqNum := REPLICATE_ROW (vTCCompanyId,
                              vUser,
                              vLaneId,
                              vRatingDtlSeq,
                              vCurrentRatingLaneDtlSeq,
                              vEffectiveDT,
                              vExpDT,
                              1,
                              vIsRouting,
                              vIsSailing);
               isInsertAtEnd := 0;
            END IF;
         END IF;

         isFirstRow := 1;
         vExpDTTemp := vExpDT + 1;
      END LOOP;

      IF (isInsertAtEnd = 0)
      THEN
         IF (vExpDT <> vExpirationDT)
         THEN
            -- create row from exp date of last row to input expiration date
            CREATE_NEW_ROW (vTCCompanyId,
                            vUser,
                            vLaneId,
                            vRatingDtlSeq,
                            vExpDT + 1,
                            vExpirationDT);
         END IF;
      END IF;

      IF (isFirstRow = 1)
      THEN
         --UPDATE comb_lane_dtl
         -- SET lane_dtl_status = 2,
         -- last_updated_source_type = 3,
         --  last_updated_source = 'OMS',
         --  last_updated_dttm = getdate()
         IF (vDoNotDeleteRate = 0)
         THEN
            DELETE_CHILD_ROW (vTCCompanyId, vLaneId, vRatingDtlSeq);
         END IF;

         DELETE FROM COMB_LANE_DTL
               WHERE     tc_company_id = vTCCompanyId
                     AND lane_id = vLaneId
                     AND lane_dtl_seq = vRatingDtlSeq;

         DELETE FROM RATING_EVENT
               WHERE RATING_LANE_ID = vLaneId
                     AND RATING_LANE_DTL_ID = vRatingDtlSeq;
      END IF;

      COMMIT;

      CLOSE eff_exp_cursor;
   END ADJUST_EFF_EXP_DT;

 
   
   FUNCTION ADJUST_EFF_EXP_DT_UI (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vLaneId                IN COMB_LANE.LANE_ID%TYPE,
      vRatingDtlSeq          IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vRatingDtlSeqUpd       IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vEffectiveDT           IN COMB_LANE_DTL.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN COMB_LANE_DTL.EXPIRATION_DT%TYPE,
      vCarrierCode           IN COMB_LANE_DTL.CARRIER_ID%TYPE,
      vMot                   IN COMB_LANE_DTL.MOT_ID%TYPE,
      vEquipment             IN COMB_LANE_DTL.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN COMB_LANE_DTL.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN COMB_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN COMB_LANE_DTL.PROTECTION_LEVEL_ID%TYPE,
      vOriginShipVia         IN COMB_LANE_DTL.O_SHIP_VIA%TYPE,
      vDestinationShipVia    IN COMB_LANE_DTL.D_SHIP_VIA%TYPE,
      vCustomText1           IN COMB_LANE_DTL.CUSTOM_TEXT1%TYPE,
      vCustomText2           IN COMB_LANE_DTL.CUSTOM_TEXT2%TYPE,
      vCustomText3           IN COMB_LANE_DTL.CUSTOM_TEXT3%TYPE,
      vCustomText4           IN COMB_LANE_DTL.CUSTOM_TEXT4%TYPE,
      vCustomText5           IN COMB_LANE_DTL.CUSTOM_TEXT5%TYPE,
      vuser                  IN comb_lane_dtl.last_updated_source%TYPE,
	  vVoyage				 IN COMB_LANE_DTL.VOYAGE%TYPE)
	  RETURN INT
   IS
      eff_exp_sql                VARCHAR2 (2000);
      eff_exp_cursor             ref_curtype;
      vCurrentRatingLaneDtlSeq   COMB_LANE_DTL.lane_id%TYPE;
      vEffDT                     COMB_LANE_DTL.effective_dt%TYPE;
      vExpDT                     COMB_LANE_DTL.expiration_dt%TYPE;
      vExpDTTemp                 COMB_LANE_DTL.expiration_dt%TYPE;
      vIsRating                  COMB_LANE_DTL.IS_RATING%TYPE;
      vIsRouting                 COMB_LANE_DTL.IS_ROUTING%TYPE;
      vIsSailing                 COMB_LANE_DTL.IS_SAILING%TYPE;
      isFirstRow                 INT;
      isInsertAtEnd              INT;
      isChanged                  INT;
      tempCount                  INT;
      vMotTemp                   COMB_LANE_DTL.MOT_ID%TYPE;
      vEquipmentTemp             COMB_LANE_DTL.EQUIPMENT_ID%TYPE;
      vServiceLevelTemp          COMB_LANE_DTL.SERVICE_LEVEL_ID%TYPE;
      vProtectionLevelTemp       COMB_LANE_DTL.PROTECTION_LEVEL_ID%TYPE;
      vIsBudgetedTemp            COMB_LANE_DTL.IS_BUDGETED%TYPE;
      vlastupdatedsource         comb_lane_dtl.last_updated_source%TYPE;
      vlastupdatedsourcetype     comb_lane_dtl.last_updated_source_type%TYPE;
	  tempSeqNum                 INT;     
   BEGIN
      IF (vuser IS NOT NULL)
      THEN
         vlastupdatedsource := vuser;
         vlastupdatedsourcetype := 1;
      ELSE
         vlastupdatedsource := 'OMS';
         vlastupdatedsourcetype := 3;
      END IF;
	  

      eff_exp_sql :=
         ' SELECT lane_dtl_seq, effective_dt, expiration_dt, is_rating, is_routing, is_sailing '
         || ' FROM comb_lane_dtl '
         || ' WHERE tc_company_id = :vTCCompanyId'
         || ' AND lane_id = :vLaneId'
         || ' AND lane_dtl_seq != :vRatingDtlSeq'
         || ' AND lane_dtl_status = 0 '
         || ' AND effective_dt <= expiration_dt '
         || ' AND expiration_dt >= :vEffectiveDT '
         || ' AND effective_dt <= :vExpirationDT ';

      vMotTemp := vMot;
      vEquipmentTemp := vEquipment;
      vServiceLevelTemp := vServiceLevel;
      vProtectionLevelTemp := vProtectionLevel;
       /*
IF( vMot = 'ALL' ) THEN
 vMotTemp := null;
END IF;

IF( vEquipment = 'ALL' ) THEN
 vEquipmentTemp := null;
END IF;

IF( vServiceLevel = 'ALL' ) THEN
 vServiceLevelTemp := null;
END IF;

IF( vProtectionLevel = 'ALL' ) THEN
 vProtectionLevelTemp := null;
END IF;
       */
      tempCount := 0;

      IF (vRatingDtlSeqUpd IS NOT NULL)
      THEN
         --tempCount := MERGE_DRAFT( vTCCompanyId, vLaneId, vRatingDtlSeq, vRatingDtlSeqUpd );
         SELECT COUNT (*)
           INTO tempCount
           FROM COMB_LANE_DTL
          WHERE     TC_COMPANY_ID = vTCCompanyId
                AND LANE_ID = vLaneId
                AND LANE_DTL_SEQ = vRatingDtlSeqUpd
                AND IS_SAILING = 0
                AND IS_ROUTING = 0;

         IF (tempCount > 0)
         THEN
            eff_exp_sql :=
               eff_exp_sql || ' AND lane_dtl_seq != ' || vRatingDtlSeqUpd;
         END IF;
      END IF;

      eff_exp_sql :=
         eff_exp_sql
         || GET_RESOURCE_WHERE_CLAUSE (vCarrierCode,
                                       vMotTemp,
                                       vEquipmentTemp,
                                       vServiceLevelTemp,
                                       p_scndr_carrier_code,
                                       vProtectionLevelTemp,
                                       vOriginShipVia,
                                       vDestinationShipVia,
                                       vCustomText1,
                                       vCustomText2,
                                       vCustomText3,
                                       vCustomText4,
                                       vCustomText5,
									   vVoyage);

      eff_exp_sql := eff_exp_sql || ' ORDER BY EFFECTIVE_DT';

      isInsertAtEnd := 1;
      isFirstRow := 0;
      isChanged := 0;

      SELECT IS_BUDGETED
        INTO vIsBudgetedTemp
        FROM COMB_LANE_DTL
       WHERE     TC_COMPANY_ID = vTCCompanyId
             AND LANE_ID = vLaneId
             AND LANE_DTL_SEQ = vRatingDtlSeq;

      OPEN eff_exp_cursor FOR eff_exp_sql
         USING vTCCompanyId,
               vLaneId,
               vRatingDtlSeq,
               vEffectiveDT,
               vExpirationDT;

      LOOP
         FETCH eff_exp_cursor
         INTO vCurrentRatingLaneDtlSeq,
              vEffDT,
              vExpDT,
              vIsRating,
              vIsRouting,
              vIsSailing;

         EXIT WHEN eff_exp_cursor%NOTFOUND;

         -- Start of Merge with other lane detail
         IF (vEffectiveDT <= vEffDt)
         THEN
            IF (isFirstRow = 0)
            THEN
               IF (vEffectiveDT <> vEffDt)
               THEN
                  IF ( (tempCount > 0) AND (isChanged = 0))
                  THEN
                     isChanged := 1;
                     tempCount :=
                        MERGE_DRAFT (vTCCompanyId,
                                     vuser,
                                     vLaneId,
                                     vRatingDtlSeq,
                                     vRatingDtlSeqUpd);

                     -- Update the existing row
                     UPDATE COMB_LANE_DTL
                        SET effective_dt = vEffectiveDT,
                            expiration_dt = vEffDt - 1,
                            last_updated_source_type = vlastupdatedsourcetype,
                            last_updated_source = vlastupdatedsource,
                            last_updated_dttm = Getdate ()
                      WHERE     tc_company_id = vTCCompanyId
                            AND lane_id = vLaneId
                            AND lane_dtl_seq = vRatingDtlSeqUpd;
                  ELSE
                     -- create row from input effective date to effective date of current row
                     CREATE_NEW_ROW (vTCCompanyId,
                                     vuser,
                                     vLaneId,
                                     vRatingDtlSeq,
                                     vEffectiveDT,
                                     vEffDt - 1);
                  END IF;
               END IF;
            ELSE
               IF (vExpDTTemp <> vEffDt)
               THEN
                  IF ( (tempCount > 0) AND (isChanged = 0))
                  THEN
                     isChanged := 1;
                     tempCount :=
                        MERGE_DRAFT (vTCCompanyId,
                                     vuser,
                                     vLaneId,
                                     vRatingDtlSeq,
                                     vRatingDtlSeqUpd);

                     -- Update the existing row
                     UPDATE COMB_LANE_DTL
                        SET effective_dt = vExpDTTemp,
                            expiration_dt = vEffDt - 1,
                            last_updated_source_type = vlastupdatedsourcetype,
                            last_updated_source = vlastupdatedsource,
                            last_updated_dttm = Getdate ()
                      WHERE     tc_company_id = vTCCompanyId
                            AND lane_id = vLaneId
                            AND lane_dtl_seq = vRatingDtlSeqUpd;
                  ELSE
                     -- create row from input effective date to effective date of current row
                     CREATE_NEW_ROW (vTCCompanyId,
                                     vuser,
                                     vLaneId,
                                     vRatingDtlSeq,
                                     vExpDTTemp,
                                     vEffDt - 1);
                  END IF;
               END IF;
            END IF;

            IF (vExpirationDT < vExpDT)
            THEN
               --replicate row from input expiration date to exp date of current row with rating flag turned off
              tempSeqNum := REPLICATE_ROW (vTCCompanyId,
                              vuser,
                              vLaneId,
                              -1,
                              vCurrentRatingLaneDtlSeq,
                              vExpirationDT + 1,
                              vExpDT,
                              vIsRating,
                              vIsRouting,
                              vIsSailing);
							 

               UPDATE COMB_LANE_DTL
                  SET is_rating = 1,
                      expiration_dt = vExpirationDT,
                      is_budgeted = vIsBudgetedTemp,
                      last_updated_source_type = vlastupdatedsourcetype,
                      last_updated_source = vlastupdatedsource,
                      last_updated_dttm = Getdate ()
                WHERE     tc_company_id = vTCCompanyId
                      AND lane_id = vLaneId
                      AND lane_dtl_seq = vCurrentRatingLaneDtlSeq;

               --COPY_CHILD_ROW( vTCCompanyId, vLaneId, vRatingDtlSeq, vCurrentRatingLaneDtlSeq,
               -- vCurrentRatingLaneDtlSeq, 1, 0, 0 );

               isInsertAtEnd := 1;
            ELSE
               UPDATE COMB_LANE_DTL
                  SET is_rating = 1,
                      is_budgeted = vIsBudgetedTemp,
                      last_updated_source_type = vlastupdatedsourcetype,
                      last_updated_source = vlastupdatedsource,
                      last_updated_dttm = Getdate ()
                WHERE     tc_company_id = vTCCompanyId
                      AND lane_id = vLaneId
                      AND lane_dtl_seq = vCurrentRatingLaneDtlSeq;

               --COPY_CHILD_ROW( vTCCompanyId, vLaneId, vRatingDtlSeq, vCurrentRatingLaneDtlSeq,
               -- vCurrentRatingLaneDtlSeq, 1, 0, 0 );

               isInsertAtEnd := 0;
            END IF;

            IF (vIsRating = 1)
            THEN
               DELETE_CHILD_ROW (vTCCompanyId,
                                 vLaneId,
                                 vCurrentRatingLaneDtlSeq);
            END IF;

            COPY_CHILD_ROW (vTCCompanyId,
                            vLaneId,
                            vRatingDtlSeq,
                            vCurrentRatingLaneDtlSeq,
                            vCurrentRatingLaneDtlSeq,
                            1,
                            0,
                            0,vLastUpdatedSource);
         ELSE
            UPDATE COMB_LANE_DTL
               SET expiration_dt = vEffectiveDT - 1,
                   last_updated_source_type = vlastupdatedsourcetype,
                   last_updated_source = vlastupdatedsource,
                   last_updated_dttm = Getdate ()
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND lane_dtl_seq = vCurrentRatingLaneDtlSeq;

            IF (vExpirationDT < vExpDT)
            THEN
               --replicate row from input effective date to input exp date with rating flag turned on
              tempSeqNum := REPLICATE_ROW (vTCCompanyId,
                              vuser,
                              vLaneId,
                              vRatingDtlSeq,
                              vCurrentRatingLaneDtlSeq,
                              vEffectiveDT,
                              vExpirationDT,
                              1,
                              vIsRouting,
                              vIsSailing);
							
               --replicate row from input expiration date to exp date of current row with rating flag turned off
              tempSeqNum := REPLICATE_ROW (vTCCompanyId,
                              vuser,
                              vLaneId,
                              -1,
                              vCurrentRatingLaneDtlSeq,
                              vExpirationDT + 1,
                              vExpDT,
                              vIsRating,
                              vIsRouting,
                              vIsSailing);
							 
               isInsertAtEnd := 1;
            ELSE
               --replicate row from input eff date to exp date of current row with rating flag turned on
              tempSeqNum := REPLICATE_ROW (vTCCompanyId,
                              vuser,
                              vLaneId,
                              vRatingDtlSeq,
                              vCurrentRatingLaneDtlSeq,
                              vEffectiveDT,
                              vExpDT,
                              1,
                              vIsRouting,
                              vIsSailing);
							 
               isInsertAtEnd := 0;
            END IF;
         END IF;

         isFirstRow := 1;
         vExpDTTemp := vExpDT + 1;
      END LOOP;

      IF (isInsertAtEnd = 0)
      THEN
         IF (vExpDT <> vExpirationDT)
         THEN
            IF ( (tempCount > 0) AND (isChanged = 0))
            THEN
               isChanged := 1;
               tempCount :=
                  MERGE_DRAFT (vTCCompanyId,
                               vuser,
                               vLaneId,
                               vRatingDtlSeq,
                               vRatingDtlSeqUpd);

               -- Update the existing row
               UPDATE COMB_LANE_DTL
                  SET effective_dt = vExpDT + 1,
                      expiration_dt = vExpirationDT,
                      last_updated_source_type = vlastupdatedsourcetype,
                      last_updated_source = vlastupdatedsource,
                      last_updated_dttm = Getdate ()
                WHERE     tc_company_id = vTCCompanyId
                      AND lane_id = vLaneId
                      AND lane_dtl_seq = vRatingDtlSeqUpd;
            ELSE
               -- create row from exp date of last row to input expiration date
               CREATE_NEW_ROW (vTCCompanyId,
                               vuser,
                               vLaneId,
                               vRatingDtlSeq,
                               vExpDT + 1,
                               vExpirationDT);
            END IF;
         END IF;
      END IF;

      IF (isFirstRow = 1)
      THEN
         --UPDATE comb_lane_dtl
         -- SET lane_dtl_status = 2,
         -- last_updated_source_type = 3,
         -- last_updated_source = 'OMS',
         --  last_updated_dttm = getdate()
         DELETE_CHILD_ROW (vTCCompanyId, vLaneId, vRatingDtlSeq);

         DELETE FROM COMB_LANE_DTL
               WHERE     tc_company_id = vTCCompanyId
                     AND lane_id = vLaneId
                     AND lane_dtl_seq = vRatingDtlSeq;

         DELETE FROM RATING_EVENT
               WHERE RATING_LANE_ID = vLaneId
                     AND RATING_LANE_DTL_ID = vRatingDtlSeq;
      END IF;

      --For Update flow
      IF (tempCount > 0)
      THEN
         -- If the draft is not merged with row which is getting updated
         IF ( (isChanged = 0) AND (isFirstRow = 1))
         THEN
            UPDATE COMB_LANE_DTL
               SET expiration_dt = effective_dt - 1,
                   last_updated_source_type = vlastupdatedsourcetype,
                   last_updated_source = vlastupdatedsource,
                   last_updated_dttm = Getdate ()
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND lane_dtl_seq = vRatingDtlSeqUpd;
         -- If the draft is not merged with any row
         ELSIF (isFirstRow = 0)
         THEN
            tempCount :=
               MERGE_DRAFT (vTCCompanyId,
                            vuser,
                            vLaneId,
                            vRatingDtlSeq,
                            vRatingDtlSeqUpd);
            DELETE_CHILD_ROW (vTCCompanyId, vLaneId, vRatingDtlSeq);

            DELETE FROM COMB_LANE_DTL
                  WHERE     tc_company_id = vTCCompanyId
                        AND lane_id = vLaneId
                        AND lane_dtl_seq = vRatingDtlSeq;

            DELETE FROM RATING_EVENT
                  WHERE RATING_LANE_ID = vLaneId
                        AND RATING_LANE_DTL_ID = vRatingDtlSeq;
         END IF;
      END IF;

      COMMIT;

      CLOSE eff_exp_cursor;
	  RETURN tempSeqNum;
   END ADJUST_EFF_EXP_DT_UI;
   
   
   
   PROCEDURE ADJUST_EFF_EXP_DT_UI (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vLaneId                IN COMB_LANE.LANE_ID%TYPE,
      vRatingDtlSeq          IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vRatingDtlSeqUpd       IN COMB_LANE_DTL.LANE_DTL_SEQ%TYPE,
      vEffectiveDT           IN COMB_LANE_DTL.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN COMB_LANE_DTL.EXPIRATION_DT%TYPE,
      vCarrierCode           IN COMB_LANE_DTL.CARRIER_ID%TYPE,
      vMot                   IN COMB_LANE_DTL.MOT_ID%TYPE,
      vEquipment             IN COMB_LANE_DTL.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN COMB_LANE_DTL.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN COMB_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN COMB_LANE_DTL.PROTECTION_LEVEL_ID%TYPE,
      vOriginShipVia         IN COMB_LANE_DTL.O_SHIP_VIA%TYPE,
      vDestinationShipVia    IN COMB_LANE_DTL.D_SHIP_VIA%TYPE,
      vCustomText1           IN COMB_LANE_DTL.CUSTOM_TEXT1%TYPE,
      vCustomText2           IN COMB_LANE_DTL.CUSTOM_TEXT2%TYPE,
      vCustomText3           IN COMB_LANE_DTL.CUSTOM_TEXT3%TYPE,
      vCustomText4           IN COMB_LANE_DTL.CUSTOM_TEXT4%TYPE,
      vCustomText5           IN COMB_LANE_DTL.CUSTOM_TEXT5%TYPE,
      vuser                  IN comb_lane_dtl.last_updated_source%TYPE,
	  vVoyage				 IN COMB_LANE_DTL.VOYAGE%TYPE,
	  NEW_SEQ_NUM             OUT NUMBER)
   AS
   BEGIN
      NEW_SEQ_NUM :=
         ADJUST_EFF_EXP_DT_UI (vTCCompanyId  ,        
      vLaneId  ,             
      vRatingDtlSeq ,         
      vRatingDtlSeqUpd ,      
      vEffectiveDT ,          
      vExpirationDT  ,       
      vCarrierCode ,          
      vMot    ,               
      vEquipment  ,           
      vServiceLevel  ,       
      p_scndr_carrier_code   ,
      vProtectionLevel  ,     
      vOriginShipVia   ,     
      vDestinationShipVia  ,  
      vCustomText1    ,      
      vCustomText2  ,        
      vCustomText3  ,         
      vCustomText4   ,       
      vCustomText5  ,        
      vuser  ,                
	  vVoyage);
	  
   END ADJUST_EFF_EXP_DT_UI;


   PROCEDURE CHECK_DUPLICATE_LANE_DTL (
      VLANEID               IN     COMB_LANE.LANE_ID%TYPE,
      CARRIERID             IN     COMB_LANE_DTL.CARRIER_ID%TYPE,
      MOTID                 IN     COMB_LANE_DTL.MOT_ID%TYPE,
      SLID                  IN     COMB_LANE_DTL.SERVICE_LEVEL_ID%TYPE,
      EQID                  IN     COMB_LANE_DTL.EQUIPMENT_ID%TYPE,
      PLID                  IN     COMB_LANE_DTL.PROTECTION_LEVEL_ID%TYPE,
      SCCARRIERID           IN     COMB_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      VORIGINSHIPVIA        IN     COMB_LANE_DTL.O_SHIP_VIA%TYPE,
      VDESTINATIONSHIPVIA   IN     COMB_LANE_DTL.D_SHIP_VIA%TYPE,
      VCUSTOMTEXT1          IN     COMB_LANE_DTL.CUSTOM_TEXT1%TYPE,
      VCUSTOMTEXT2          IN     COMB_LANE_DTL.CUSTOM_TEXT2%TYPE,
      VCUSTOMTEXT3          IN     COMB_LANE_DTL.CUSTOM_TEXT3%TYPE,
      VCUSTOMTEXT4          IN     COMB_LANE_DTL.CUSTOM_TEXT4%TYPE,
      VCUSTOMTEXT5          IN     COMB_LANE_DTL.CUSTOM_TEXT5%TYPE,
      VCOUNT                   OUT INTEGER,
	  vVoyage				IN     COMB_LANE_DTL.VOYAGE%TYPE)
   IS
      -- vCount INTEGER;
      vMotTemp               COMB_LANE_DTL.MOT_ID%TYPE;
      vEquipmentTemp         COMB_LANE_DTL.EQUIPMENT_ID%TYPE;
      vServiceLevelTemp      COMB_LANE_DTL.SERVICE_LEVEL_ID%TYPE;
      vProtectionLevelTemp   COMB_LANE_DTL.PROTECTION_LEVEL_ID%TYPE;
      vSecCarrierIdTemp      COMB_LANE_DTL.SCNDR_CARRIER_ID%TYPE;

      vEffDT                 COMB_LANE_DTL.EFFECTIVE_DT%TYPE;
      vExpDT                 COMB_LANE_DTL.EXPIRATION_DT%TYPE;
      vLaneDtlSeq            COMB_LANE_DTL.LANE_DTL_SEQ%TYPE;

      EFF_EXP_SQL            VARCHAR2 (2000);
      OVRLAP_EFF_EXP_SQL     VARCHAR2 (2000);

      eff_exp_cursor         ref_curtype;
      OVRLAP_EFF_CURSOR      ref_curtype;
   BEGIN
      vCount := 0;

      eff_exp_sql :=
            ' SELECT LANE_DTL_SEQ, EFFECTIVE_DT, EXPIRATION_DT '
         || ' FROM comb_lane_dtl '
         || ' WHERE lane_id = :vLaneId'
         || ' AND IS_RATING = 1'
         || ' AND HAS_SHIPPING_PARAM = 0 '
         || ' AND lane_dtl_status = 0 '
         || ' AND effective_dt <= expiration_dt ';

      vMotTemp := MOTID;
      vEquipmentTemp := EQID;
      vServiceLevelTemp := SLID;
      vProtectionLevelTemp := PLID;
      vSecCarrierIdTemp := SCCARRIERID;

      EFF_EXP_SQL :=
         (COALESCE (EFF_EXP_SQL, '')
          || COALESCE (GET_RESOURCE_WHERE_CLAUSE (CARRIERID,
                                                  VMOTTEMP,
                                                  VEQUIPMENTTEMP,
                                                  VSERVICELEVELTEMP,
                                                  vSecCarrierIdTemp,
                                                  VPROTECTIONLEVELTEMP,
                                                  vOriginShipVia,
                                                  vDestinationShipVia,
                                                  VCUSTOMTEXT1,
                                                  VCUSTOMTEXT2,
                                                  VCUSTOMTEXT3,
                                                  VCUSTOMTEXT4,
                                                  VCUSTOMTEXT5,
												  vVoyage),
                       ''));

      eff_exp_sql := eff_exp_sql || ' ORDER BY EFFECTIVE_DT';

      OVRLAP_EFF_EXP_SQL :=
            ' SELECT COUNT(*) '
         || ' FROM comb_lane_dtl '
         || ' WHERE lane_id = :vLaneId'
         || ' AND LANE_DTL_SEQ != :vLaneDtlSeq'
         || ' AND HAS_SHIPPING_PARAM = 0 '
         || ' AND IS_RATING = 1 '
         || ' AND lane_dtl_status = 0 '
         || ' AND effective_dt <= expiration_dt '
         || ' AND expiration_dt >= :vEffectDt '
         || ' AND effective_dt <=  :vExpirDt ';

      OVRLAP_EFF_EXP_SQL :=
         (COALESCE (OVRLAP_EFF_EXP_SQL, '')
          || COALESCE (GET_RESOURCE_WHERE_CLAUSE (CARRIERID,
                                                  VMOTTEMP,
                                                  VEQUIPMENTTEMP,
                                                  VSERVICELEVELTEMP,
                                                  vSecCarrierIdTemp,
                                                  VPROTECTIONLEVELTEMP,
                                                  vOriginShipVia,
                                                  vDestinationShipVia,
                                                  VCUSTOMTEXT1,
                                                  VCUSTOMTEXT2,
                                                  VCUSTOMTEXT3,
                                                  VCUSTOMTEXT4,
                                                  VCUSTOMTEXT5,
												  vVoyage),
                       ''));

      --INSERT INTO DEBUG_SQL(SQLTEXT) VALUES(eff_exp_sql);
      OPEN eff_exp_cursor FOR eff_exp_sql USING VLANEID;

     <<LOOP_LABEL>>
      LOOP
         FETCH eff_exp_cursor
         INTO vLaneDtlSeq, vEffDT, vExpDT;

         EXIT LOOP_LABEL WHEN eff_exp_cursor%NOTFOUND;

         --  INSERT INTO DEBUG_SQL(SQLTEXT) VALUES(TO_CHAR(vLaneDtlSeq));

         OPEN OVRLAP_EFF_CURSOR FOR OVRLAP_EFF_EXP_SQL
            USING VLANEID,
                  vLaneDtlSeq,
                  vEffDT,
                  vExpDT;

         FETCH OVRLAP_EFF_CURSOR INTO vCount;

         EXIT WHEN OVRLAP_EFF_CURSOR%NOTFOUND;

         CLOSE OVRLAP_EFF_CURSOR;

         IF (vCount > 0)
         THEN
            INSERT INTO DEBUG_SQL (SQLTEXT)
                 VALUES ('FOUND DUPLICATE');

            EXIT LOOP_LABEL;
         END IF;
      END LOOP LOOP_LABEL;

      CLOSE eff_exp_cursor;
   END;

   -- (gakumar) SYSCO ENHC
   FUNCTION CHECK_DUP_LANE_DTL_NO_OVERLAP (
      vTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      vRatingLaneId          IN rating_lane.LANE_ID%TYPE,
      vratinglanedtlseq      IN rating_lane_dtl.rating_lane_dtl_seq%TYPE,
      vCarrierCode           IN rating_lane_dtl.CARRIER_ID%TYPE,
      vMot                   IN rating_lane_dtl.MOT_ID%TYPE,
      vEquipment             IN rating_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN rating_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN RATING_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN rating_lane_dtl.PROTECTION_LEVEL_ID%TYPE,
      vEffectiveDT           IN rating_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN rating_lane_dtl.EXPIRATION_DT%TYPE,
      vOriginShipVia         IN rating_lane_dtl.O_SHIP_VIA%TYPE,
      vDestinationShipVia    IN rating_lane_dtl.D_SHIP_VIA%TYPE,
      vCustomText1           IN rating_lane_dtl.CUSTOM_TEXT1%TYPE,
      vCustomText2           IN rating_lane_dtl.CUSTOM_TEXT2%TYPE,
      vCustomText3           IN rating_lane_dtl.CUSTOM_TEXT3%TYPE,
      vCustomText4           IN rating_lane_dtl.CUSTOM_TEXT4%TYPE,
      vCustomText5           IN rating_lane_dtl.CUSTOM_TEXT5%TYPE,
      vPackageId             IN rating_lane_dtl.PACKAGE_ID%TYPE,
      vPackageName           IN rating_lane_dtl.PACKAGE_NAME%TYPE,
	  vVoyage				 IN rating_lane_dtl.VOYAGE%TYPE)
      RETURN NUMBER
   IS
      rld_seq                 NUMBER (12);
      rating_dtl_seq_cursor   ref_curtype;
      expire_dtl_sql          VARCHAR (2000);
   BEGIN
      expire_dtl_sql :=
            ' SELECT NVL(rating_lane_dtl_seq, 0) '
         || ' FROM RATING_LANE_DTL '
         || ' WHERE TC_COMPANY_ID = '
         || vTCCompanyId
         || ' AND LANE_ID = '
         || vRatingLaneId
         || ' AND RATING_LANE_DTL_SEQ !='
         || vratinglanedtlseq
         || ' AND EXPIRATION_DT >= '''
         || TO_CHAR (veffectivedt)
         || ''''
         || ' AND EFFECTIVE_DT  <= '''
         || TO_CHAR (vexpirationdt)
         || ''''
         || ' AND EXPIRATION_DT >= EFFECTIVE_DT';

      expire_dtl_sql :=
         (COALESCE (expire_dtl_sql, '')
          || COALESCE (GET_RESOURCE_WHERE_CLAUSE (vCarrierCode,
                                                  vMot,
                                                  vEquipment,
                                                  vServiceLevel,
                                                  p_scndr_carrier_code,
                                                  vProtectionLevel,
                                                  vOriginShipVia,
                                                  vDestinationShipVia,
                                                  VCUSTOMTEXT1,
                                                  VCUSTOMTEXT2,
                                                  VCUSTOMTEXT3,
                                                  VCUSTOMTEXT4,
                                                  VCUSTOMTEXT5,
												  vVoyage),
                       ''));

      expire_dtl_sql := expire_dtl_sql || 'ORDER BY EFFECTIVE_DT';

      rld_seq := 0;

      OPEN rating_dtl_seq_cursor FOR expire_dtl_sql;

      LOOP
         FETCH rating_dtl_seq_cursor INTO rld_seq;

         EXIT WHEN rating_dtl_seq_cursor%NOTFOUND;
      END LOOP;

      CLOSE rating_dtl_seq_cursor;

      RETURN rld_seq;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN 0;
   END CHECK_DUP_LANE_DTL_NO_OVERLAP;

   PROCEDURE AUDIT_RATING_LANE_DTL_RATE (
      vTCCompanyId             IN COMPANY.COMPANY_ID%TYPE,
      vRatingLaneId            IN rating_lane.LANE_ID%TYPE,
      vRatingLaneDtlId         IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vMinSize                 IN RATING_LANE_DTL_RATE.MINIMUM_SIZE%TYPE,
      vMaxSize                 IN RATING_LANE_DTL_RATE.MAXIMUM_SIZE%TYPE,
      vSizeUOM                 IN RATING_LANE_DTL_RATE.SIZE_UOM_ID%TYPE,
      vRateCalcMethod          IN RATING_LANE_DTL_RATE.RATE_CALC_METHOD%TYPE,
      vRateUOM                 IN RATING_LANE_DTL_RATE.RATE_UOM%TYPE,
      vRate                    IN RATING_LANE_DTL_RATE.RATE%TYPE,
      vMinRate                 IN RATING_LANE_DTL_RATE.MINIMUM_RATE%TYPE,
      vCurrencyCode            IN RATING_LANE_DTL_RATE.CURRENCY_CODE%TYPE,
      vTariffId                IN RATING_LANE_DTL_RATE.TARIFF_ID%TYPE,
      vMinDistance             IN RATING_LANE_DTL_RATE.MIN_DISTANCE%TYPE,
      vMaxDistance             IN RATING_LANE_DTL_RATE.MAX_DISTANCE%TYPE,
      vDistanceUOM             IN RATING_LANE_DTL_RATE.DISTANCE_UOM%TYPE,
      vSupportsMSTL            IN RATING_LANE_DTL_RATE.SUPPORTS_MSTL%TYPE,
      vHasBH                   IN RATING_LANE_DTL_RATE.HAS_BH%TYPE,
      vMinCommodity            IN RATING_LANE_DTL_RATE.MIN_COMMODITY%TYPE,
      vMaxCommodity            IN RATING_LANE_DTL_RATE.MAX_COMMODITY%TYPE,
      vHasRT                   IN RATING_LANE_DTL_RATE.HAS_RT%TYPE,
      vParcelTier              IN RATING_LANE_DTL_RATE.PARCEL_TIER%TYPE,
      vCommodityCodeId         IN RATING_LANE_DTL_RATE.COMMODITY_CODE_ID%TYPE,
      vExcessWtRate            IN RATING_LANE_DTL_RATE.EXCESS_WT_RATE%TYPE,
      vPayeeCarrier            IN RATING_LANE_DTL_RATE.PAYEE_CARRIER_ID%TYPE,
      vMaxCommodityCodeId      IN RATING_LANE_DTL_RATE.MAX_RANGE_COMMODITY_CODE_ID%TYPE,
      VIsSurgeRate             IN RATING_LANE_DTL_RATE.IS_SURGE_RATE%TYPE,
      -- sysco merge
      vMinRateType             IN RATING_LANE_DTL_RATE.MINIMUM_RATE_TYPE%TYPE,
      vBaseRate                IN RATING_LANE_DTL_RATE.BASE_RATE%TYPE,
      vMaxRate                 IN RATING_LANE_DTL_RATE.MAXIMUM_RATE%TYPE,
      vCustomerMinCharge       IN RATING_LANE_DTL_RATE.CUSTOMER_MIN_CHARGE%TYPE,
      vCustomerBaseRate        IN RATING_LANE_DTL_RATE.CUSTOMER_BASE_RATE%TYPE,
      vCustomerSurchargeRate   IN RATING_LANE_DTL_RATE.CUSTOMER_SURCHARGE_RATE%TYPE,
      -- sysco merge
      vLastUpdatedSrcType      IN RATING_LANE_DTL_RATE.LAST_UPDATED_SRC_TYPE%TYPE,
      vLastUpdatedSrc          IN RATING_LANE_DTL_RATE.LAST_UPDATED_SRC%TYPE,
      vLastUpdatedDttm         IN RATING_LANE_DTL_RATE.LAST_UPDATED_DTTM%TYPE,
      vDuplicateLaneDtlId      IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE)
   AS
      rldCount              NUMBER;
      vDupRateExists        NUMBER;
      dup_dtl_rate_sql      VARCHAR (2000);
      vduplicaterateseq     rating_lane_dtl_rate.rld_rate_seq%TYPE;
      dup_dtl_rate_cursor   ref_curtype;

      CURSOR AUDIT_LANE_RATE_CURSOR
      IS
         SELECT RLD_RATE_SEQ,
                RATE_CALC_METHOD,
                RATE_UOM,
                CURRENCY_CODE,
                TARIFF_ID,
                DISTANCE_UOM,
                SUPPORTS_MSTL,
                HAS_BH,
                COMMODITY_CODE_ID,
                PAYEE_CARRIER_ID,
                SIZE_UOM_ID,
                CHANGE_ID,
                IS_SURGE_RATE,
                EXCESS_WT_RATE,
                MAX_COMMODITY,
                MAX_DISTANCE,
                MAXIMUM_SIZE,
                MIN_COMMODITY,
                MIN_DISTANCE,
                MINIMUM_RATE,
                MINIMUM_SIZE,
                RATE,
                has_rt,
                parcel_tier,
                MINIMUM_RATE_TYPE,
                BASE_RATE,
                MAXIMUM_RATE,
                CUSTOMER_MIN_CHARGE,
                CUSTOMER_BASE_RATE,
                CUSTOMER_SURCHARGE_RATE
           FROM RATING_LANE_DTL_RATE
          WHERE     tc_Company_Id = vTCCompanyId
                AND lane_id = vRatingLaneId
                AND rating_lane_dtl_seq = vduplicatelanedtlid
                AND rld_rate_seq = vduplicaterateseq;
   BEGIN
      vduplicaterateseq := 0;
      dup_dtl_rate_sql :=
            ' select NVL(rld_rate_seq,0) from rating_lane_dtl_rate '
         || ' where lane_id = '
         || vratinglaneid
         || ' and rating_lane_dtl_seq = '
         || vduplicatelanedtlid
         || ' and rate_calc_method = '''
         || vratecalcmethod
         || ''''
         || ' and supports_mstl = '
         || vsupportsmstl
         || ' and has_bh = '
         || vhasbh
         || ' and has_rt = '
         || vhasrt
         || ' and is_surge_rate = '
         || vissurgerate;

      IF (vrateuom IS NOT NULL)
      THEN
         dup_dtl_rate_sql :=
            dup_dtl_rate_sql || ' and rate_uom = ''' || vrateuom || '''';
      ELSE
         dup_dtl_rate_sql := dup_dtl_rate_sql || ' and rate_uom is null';
      END IF;

      IF (vtariffid IS NOT NULL)
      THEN
         dup_dtl_rate_sql :=
            dup_dtl_rate_sql || ' and tariff_id =  ''' || vtariffid || '''';
      ELSE
         dup_dtl_rate_sql := dup_dtl_rate_sql || ' and tariff_id is null';
      END IF;

      IF (vminsize IS NOT NULL AND vmaxsize IS NOT NULL)
      THEN
         dup_dtl_rate_sql :=
               dup_dtl_rate_sql
            || ' and ( '
            || '( '
            || vminsize
            || ' >= minimum_size and '
            || vminsize
            || ' <= maximum_size) or '
            || '( '
            || vmaxsize
            || ' >= minimum_size and '
            || vmaxsize
            || ' <= maximum_size) or '
            || '( minimum_size > '
            || vminsize
            || ' and minimum_size < '
            || vmaxsize
            || ')  or '
            || '( maximum_size > '
            || vminsize
            || ' and maximum_size < '
            || vmaxsize
            || '))';

         IF (vsizeuom IS NOT NULL)
         THEN
            dup_dtl_rate_sql :=
               dup_dtl_rate_sql || ' and size_uom_id = ' || vsizeuom;
         ELSE
            dup_dtl_rate_sql := dup_dtl_rate_sql || ' and size_uom_id is null';
         END IF;
      ELSE
         dup_dtl_rate_sql :=
            dup_dtl_rate_sql
            || ' and minimum_size is null and maximum_size is null';
      END IF;

      IF (vmindistance IS NOT NULL AND vmaxdistance IS NOT NULL)
      THEN
         dup_dtl_rate_sql :=
               dup_dtl_rate_sql
            || ' and ( '
            || '( '
            || vmindistance
            || ' >= min_distance and '
            || vmindistance
            || ' <= max_distance) or '
            || '( '
            || vmaxdistance
            || ' >= min_distance and '
            || vmaxdistance
            || ' <= max_distance) or '
            || '( min_distance > '
            || vmindistance
            || ' and min_distance < '
            || vmaxdistance
            || ')  or '
            || '( max_distance > '
            || vmindistance
            || ' and max_distance < '
            || vmaxdistance
            || '))';

         IF (vdistanceuom IS NOT NULL)
         THEN
            dup_dtl_rate_sql :=
                  dup_dtl_rate_sql
               || ' and distance_uom = '''
               || vdistanceuom
               || '''';
         ELSE
            dup_dtl_rate_sql :=
               dup_dtl_rate_sql || ' and distance_uom is null';
         END IF;
      ELSE
         dup_dtl_rate_sql :=
            dup_dtl_rate_sql
            || ' and min_distance is null and max_distance is null';
      END IF;

      IF (vmincommodity IS NOT NULL AND vmaxcommodity IS NOT NULL)
      THEN
         dup_dtl_rate_sql :=
               dup_dtl_rate_sql
            || ' and ( '
            || '( '
            || vmincommodity
            || ' >= min_commodity and '
            || vmaxcommodity
            || ' <= max_commodity) or '
            || '( '
            || vmaxcommodity
            || ' >= min_commodity and '
            || vmaxdistance
            || ' <= max_commodity) or '
            || '( min_commodity > '
            || vmincommodity
            || ' and min_commodity < '
            || vmaxcommodity
            || ')  or '
            || '( max_commodity > '
            || vmincommodity
            || ' and max_commodity < '
            || vmaxcommodity
            || '))';
      ELSE
         dup_dtl_rate_sql :=
            dup_dtl_rate_sql
            || ' and min_commodity is null and max_commodity is null';
      END IF;

      OPEN dup_dtl_rate_cursor FOR dup_dtl_rate_sql;

      FETCH dup_dtl_rate_cursor INTO vduplicaterateseq;

      CLOSE dup_dtl_rate_cursor;

      IF (vduplicaterateseq > 0)
      THEN
         FOR r IN AUDIT_LANE_RATE_CURSOR
         LOOP
            vDupRateExists := 0;


            rldCount := 0;

            SELECT MAX (CHANGE_ID)
              INTO rldCount
              FROM RATING_EVENT
             WHERE     tc_Company_Id = vTCCompanyId
                   AND RATING_LANE_ID = vRatingLaneId
                   AND RATING_LANE_DTL_ID = vratinglanedtlid
                   AND is_pending_auth = 1;



            IF (rldCount > 0)
            THEN
               rldCount := rldCount + 1;
            ELSE
               rldCount := 1;
            END IF;


            IF (vRateUOM <> r.RATE_UOM)
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    r.RLD_RATE_SEQ,
                                    vTCCompanyId,
                                    rldCount,
                                    'Rate UOM',
                                    r.RATE_UOM,
                                    vRateUOM,
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    NULL);
            END IF;

            IF (vTariffId <> r.TARIFF_ID)
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    r.RLD_RATE_SEQ,
                                    vTCCompanyId,
                                    rldCount,
                                    'Tariff',
                                    TO_CHAR (r.TARIFF_ID),
                                    TO_CHAR (vTariffId),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    NULL);
            END IF;

            IF (NVL (r.MINIMUM_SIZE, 0) <> NVL (vMinSize, 0))
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    r.RLD_RATE_SEQ,
                                    vTCCompanyId,
                                    rldCount,
                                    'Min Qty',
                                    TO_CHAR (r.MINIMUM_SIZE),
                                    TO_CHAR (vMinSize),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    NULL);
            END IF;

            IF (NVL (r.MAXIMUM_SIZE, 0) <> NVL (vMaxSize, 0))
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    r.RLD_RATE_SEQ,
                                    vTCCompanyId,
                                    rldCount,
                                    'Max Qty',
                                    TO_CHAR (r.MAXIMUM_SIZE),
                                    TO_CHAR (vMaxSize),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    NULL);
            END IF;

            IF (r.SIZE_UOM_ID <> vSizeUOM)
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    r.RLD_RATE_SEQ,
                                    vTCCompanyId,
                                    rldCount,
                                    'Qty Unit',
                                    TO_CHAR (r.SIZE_UOM_ID),
                                    TO_CHAR (vSizeUOM),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    NULL);
            END IF;

            IF (NVL (r.MIN_DISTANCE, 0) <> NVL (vMinDistance, 0))
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    r.RLD_RATE_SEQ,
                                    vTCCompanyId,
                                    rldCount,
                                    'Min Distance',
                                    TO_CHAR (r.MIN_DISTANCE),
                                    TO_CHAR (vMinDistance),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    NULL);
            END IF;

            IF (NVL (r.MAX_DISTANCE, 0) <> NVL (vMaxDistance, 0))
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    r.RLD_RATE_SEQ,
                                    vTCCompanyId,
                                    rldCount,
                                    'Max Distance',
                                    TO_CHAR (r.MAX_DISTANCE),
                                    TO_CHAR (vMaxDistance),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    NULL);
            END IF;

            IF (r.DISTANCE_UOM <> vDistanceUOM)
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    r.RLD_RATE_SEQ,
                                    vTCCompanyId,
                                    rldCount,
                                    'Distance Unit',
                                    TO_CHAR (r.DISTANCE_UOM),
                                    TO_CHAR (vDistanceUOM),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    NULL);
            END IF;

            IF (NVL (r.MIN_COMMODITY, 0) <> NVL (vMinCommodity, 0))
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    r.RLD_RATE_SEQ,
                                    vTCCompanyId,
                                    rldCount,
                                    'Min Commodity',
                                    TO_CHAR (r.MIN_COMMODITY),
                                    TO_CHAR (vMinCommodity),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    NULL);
            END IF;

            IF (NVL (r.MAX_COMMODITY, 0) <> NVL (vMaxCommodity, 0))
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    r.RLD_RATE_SEQ,
                                    vTCCompanyId,
                                    rldCount,
                                    'Max Commodity',
                                    TO_CHAR (r.MAX_COMMODITY),
                                    TO_CHAR (vMaxCommodity),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    NULL);
            END IF;

            IF (NVL (r.RATE, 0) <> NVL (vRate, 0))
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    r.RLD_RATE_SEQ,
                                    vTCCompanyId,
                                    rldCount,
                                    'Rate',
                                    TO_CHAR (r.RATE),
                                    TO_CHAR (vRate),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    NULL);
            END IF;

            IF (NVL (r.MINIMUM_RATE, 0) <> NVL (vMinRate, 0))
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    r.RLD_RATE_SEQ,
                                    vTCCompanyId,
                                    rldCount,
                                    'Min. Rate',
                                    TO_CHAR (r.MINIMUM_RATE),
                                    TO_CHAR (vMinRate),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    NULL);
            END IF;

            IF (r.CURRENCY_CODE <> vCurrencyCode)
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    r.RLD_RATE_SEQ,
                                    vTCCompanyId,
                                    rldCount,
                                    'Currency',
                                    TO_CHAR (r.CURRENCY_CODE),
                                    TO_CHAR (vCurrencyCode),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    NULL);
            END IF;

            IF (r.SUPPORTS_MSTL <> vSupportsMSTL)
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    r.RLD_RATE_SEQ,
                                    vTCCompanyId,
                                    rldCount,
                                    'Supports MSTL',
                                    TO_CHAR (r.SUPPORTS_MSTL),
                                    TO_CHAR (vSupportsMSTL),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    NULL);
            END IF;

            IF (r.HAS_BH <> vHasBH)
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    r.RLD_RATE_SEQ,
                                    vTCCompanyId,
                                    rldCount,
                                    'Has Backhaul',
                                    TO_CHAR (r.HAS_BH),
                                    TO_CHAR (vHasBH),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    NULL);
            END IF;

            IF (r.IS_SURGE_RATE <> VIsSurgeRate)
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    r.RLD_RATE_SEQ,
                                    vTCCompanyId,
                                    rldCount,
                                    'Surge Rate',
                                    TO_CHAR (r.IS_SURGE_RATE),
                                    TO_CHAR (VIsSurgeRate),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    NULL);
            END IF;

            -- SYSCO
            IF (r.minimum_rate_type <> vMinRateType)
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    r.RLD_RATE_SEQ,
                                    vTCCompanyId,
                                    rldCount,
                                    'Min. Rate Type',
                                    TO_CHAR (r.minimum_rate_type),
                                    TO_CHAR (vMinRateType),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    NULL);
            END IF;

            IF (NVL (r.base_rate, 0) <> NVL (vBaseRate, 0))
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    r.RLD_RATE_SEQ,
                                    vTCCompanyId,
                                    rldCount,
                                    'Base Rate',
                                    TO_CHAR (r.base_rate),
                                    TO_CHAR (vBaseRate),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    NULL);
            END IF;

            IF (NVL (r.maximum_rate, 0) <> NVL (vMaxRate, 0))
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    r.RLD_RATE_SEQ,
                                    vTCCompanyId,
                                    rldCount,
                                    'Max. Rate',
                                    TO_CHAR (r.maximum_rate),
                                    TO_CHAR (vMaxRate),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    NULL);
            END IF;

            IF (NVL (r.customer_min_charge, 0) <> NVL (vCustomerMinCharge, 0))
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    r.RLD_RATE_SEQ,
                                    vTCCompanyId,
                                    rldCount,
                                    'Cust. Min. Charge',
                                    TO_CHAR (r.customer_min_charge),
                                    TO_CHAR (vCustomerMinCharge),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    NULL);
            END IF;

            IF (NVL (r.customer_base_rate, 0) <> NVL (vCustomerBaseRate, 0))
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    r.RLD_RATE_SEQ,
                                    vTCCompanyId,
                                    rldCount,
                                    'Cust. Base Rate',
                                    TO_CHAR (r.customer_base_rate),
                                    TO_CHAR (vCustomerBaseRate),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    NULL);
            END IF;

            IF (NVL (r.customer_surcharge_rate, 0) <>
                   NVL (vCustomerSurchargeRate, 0))
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    r.RLD_RATE_SEQ,
                                    vTCCompanyId,
                                    rldCount,
                                    'Cust. Surcharge Rate',
                                    TO_CHAR (r.customer_surcharge_rate),
                                    TO_CHAR (vCustomerSurchargeRate),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    NULL);
            END IF;

            -- SYSCO
            rldCount := rldCount + 1;

            INSERT_RATING_LANE_DTL_RATE (vtccompanyid,
                                         vratinglaneid,
                                         vratinglanedtlid,
                                         r.minimum_size,
                                         r.maximum_size,
                                         r.size_uom_id,
                                         r.rate_calc_method,
                                         r.rate_uom,
                                         r.rate,
                                         r.minimum_rate,
                                         r.currency_code,
                                         r.tariff_id,
                                         r.min_distance,
                                         r.max_distance,
                                         r.distance_uom,
                                         r.supports_mstl,
                                         r.has_bh,
                                         r.min_commodity,
                                         r.max_commodity,
                                         r.has_rt,
                                         r.parcel_tier,
                                         r.commodity_code_id,
                                         r.excess_wt_rate,
                                         r.payee_carrier_id,
                                         r.commodity_code_id,
                                         r.is_surge_rate,
                                         r.MINIMUM_RATE_TYPE,
                                         r.BASE_RATE,
                                         r.MAXIMUM_RATE,
                                         r.CUSTOMER_MIN_CHARGE,
                                         r.CUSTOMER_BASE_RATE,
                                         r.CUSTOMER_SURCHARGE_RATE);

            COMMIT;
         END LOOP;
      ELSE
         INSERT_RATING_LANE_DTL_RATE (vtccompanyid,
                                      vratinglaneid,
                                      vratinglanedtlid,
                                      vMinSize,
                                      vMaxSize,
                                      vSizeUOM,
                                      vRateCalcMethod,
                                      vRateUOM,
                                      vRate,
                                      vMinRate,
                                      vCurrencyCode,
                                      vTariffId,
                                      vMinDistance,
                                      vMaxDistance,
                                      vDistanceUOM,
                                      vSupportsMSTL,
                                      vHasBH,
                                      vMinCommodity,
                                      vMaxCommodity,
                                      vHasRT,
                                      vParcelTier,
                                      vCommodityCodeId,
                                      vExcessWtRate,
                                      vPayeeCarrier,
                                      vMaxCommodityCodeId,
                                      vIsSurgeRate,
                                      vMinRateType,
                                      vBaseRate,
                                      vMaxRate,
                                      vCustomerMinCharge,
                                      vCustomerBaseRate,
                                      vCustomerSurchargeRate);
      END IF;

      COMMIT;
   END AUDIT_RATING_LANE_DTL_RATE;

   PROCEDURE AUDIT_LANE_ACCESSORIAL (
      vTCCompanyId             IN COMPANY.COMPANY_ID%TYPE,
      vRatingLaneId            IN rating_lane.LANE_ID%TYPE,
      vRatingLaneDtlId         IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vAccessorialCode         IN LANE_ACCESSORIAL.ACCESSORIAL_ID%TYPE,
      vRate                    IN LANE_ACCESSORIAL.RATE%TYPE,
      vMinRate                 IN LANE_ACCESSORIAL.MINIMUM_RATE%TYPE,
      vCurrencyCode            IN LANE_ACCESSORIAL.CURRENCY_CODE%TYPE,
      p_minimum_size           IN LANE_ACCESSORIAL.MINIMUM_SIZE%TYPE,
      p_maximum_size           IN LANE_ACCESSORIAL.MAXIMUM_SIZE%TYPE,
      p_size_uom               IN LANE_ACCESSORIAL.SIZE_UOM_ID%TYPE,
      vIsApproved              IN LANE_ACCESSORIAL.IS_AUTO_APPROVE%TYPE,
      vEffectiveDT             IN LANE_ACCESSORIAL.EFFECTIVE_DT%TYPE,
      vExpirationDT            IN LANE_ACCESSORIAL.EXPIRATION_DT%TYPE,
      vPayeeCarrierCode        IN LANE_ACCESSORIAL.PAYEE_CARRIER_ID%TYPE,
      vIsShipmentAccessorial   IN LANE_ACCESSORIAL.IS_SHIPMENT_ACCESSORIAL%TYPE,
      vMinRateType                LANE_ACCESSORIAL.Min_Rate_Type%TYPE, -- MACR00159593 starts
      vMaximumRate                LANE_ACCESSORIAL.Maximum_Rate%TYPE,
      vPackageType                LANE_ACCESSORIAL.PACKAGE_TYPE_ID%TYPE,
      vCustomerRate               LANE_ACCESSORIAL.CUSTOMER_RATE%TYPE,
      vCustomerMinCharge          LANE_ACCESSORIAL.Customer_Min_Charge%TYPE,
      vCalculatedRate             LANE_ACCESSORIAL.Calculated_Rate%TYPE,
      vAmount                     LANE_ACCESSORIAL.Amount%TYPE,
      vBaseAmount                 LANE_ACCESSORIAL.BASE_AMOUNT%TYPE,
      vBaseCharge                 LANE_ACCESSORIAL.Base_Charge%TYPE,
      vRangeMinAmount             LANE_ACCESSORIAL.RANGE_MIN_AMOUNT%TYPE,
      vRangeMaxAmount             LANE_ACCESSORIAL.RANGE_MAX_AMOUNT%TYPE,
      vIncrement                  LANE_ACCESSORIAL.Increment_Val%TYPE,
      vMot                        LANE_ACCESSORIAL.Mot_Id%TYPE,
      vEquipment                  LANE_ACCESSORIAL.Equipment_Id%TYPE,
      vServiceLevel               LANE_ACCESSORIAL.Service_Level_Id%TYPE,
      vProtectionLevel            LANE_ACCESSORIAL.PROTECTION_LEVEL_ID%TYPE,
      vBillingMethod              LANE_ACCESSORIAL.Billing_Method%TYPE,
      vMinLongestDim              LANE_ACCESSORIAL.Min_Longest_Dimension%TYPE,
      vMaxLongestDim              LANE_ACCESSORIAL.Max_Longest_Dimension%TYPE,
      vMinSecondLongestDim        LANE_ACCESSORIAL.Min_Second_Longest_Dimension%TYPE,
      vMaxSecondLongestDim        LANE_ACCESSORIAL.Max_Second_Longest_Dimension%TYPE,
      vMinThirdLongestDim         LANE_ACCESSORIAL.Min_Third_Longest_Dimension%TYPE,
      vMaxThirdLongestDim         LANE_ACCESSORIAL.Max_Third_Longest_Dimension%TYPE,
      vMinLengthGrith             LANE_ACCESSORIAL.Min_Length_Plus_Grith%TYPE,
      vMaxLengthGrith             LANE_ACCESSORIAL.Max_Length_Plus_Grith%TYPE,
      vMinWeight                  LANE_ACCESSORIAL.Min_Weight%TYPE,
      vMaxWeight                  LANE_ACCESSORIAL.Max_Weight%TYPE,
      vIsZoneStopOff              LANE_ACCESSORIAL.Is_Zone_Stopoff%TYPE,
      vZoneId                     LANE_ACCESSORIAL.Zone_Id%TYPE, -- MACR00159593 ends
      vLastUpdatedSrcType      IN RATING_LANE_DTL_RATE.LAST_UPDATED_SRC_TYPE%TYPE,
      vLastUpdatedSrc          IN RATING_LANE_DTL_RATE.LAST_UPDATED_SRC%TYPE,
      vDuplicateLaneDtlId      IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE)
   AS
      rldAccCount                   NUMBER;
      dup_dtl_acc_sql               VARCHAR2 (2000);
      vduplicatelaneaccessorialid   lane_accessorial.lane_accessorial_id%TYPE;
      v_lane_acces_id_seq           lane_accessorial.lane_accessorial_id%TYPE;
      dup_dtl_acc_cursor            ref_curtype;
	  -- CMGT-615 Starts here
	  vCommID				LANE_ACCESSORIAL.COMMODITY_CODE_ID%TYPE;
	  vMaxCommId			LANE_ACCESSORIAL.MAX_RANGE_COMMODITY_CODE_ID%TYPE;
	   -- CMGT-615 Ends here

      CURSOR AUDIT_ACC_CURSOR
      IS
         SELECT TC_COMPANY_ID,
                LANE_ID,
                RATING_LANE_DTL_SEQ,
                ACCESSORIAL_ID,
                RATE,
                MINIMUM_RATE,
                CURRENCY_CODE,
                IS_AUTO_APPROVE,
                EFFECTIVE_DT,
                EXPIRATION_DT,
                LANE_ACCESSORIAL_ID,
                PAYEE_CARRIER_ID,
                MINIMUM_SIZE,
                MAXIMUM_SIZE,
                SIZE_UOM_ID,
                IS_SHIPMENT_ACCESSORIAL,
                INCOTERM_ID,
                MIN_RATE_TYPE,
                MAXIMUM_RATE,
                PACKAGE_TYPE_ID,
                CUSTOMER_RATE,
                CUSTOMER_MIN_CHARGE,
                CALCULATED_RATE,
                AMOUNT,
                BASE_AMOUNT,
                BASE_CHARGE,
                RANGE_MIN_AMOUNT,
                RANGE_MAX_AMOUNT,
                INCREMENT_VAL,
                MOT_ID,
                EQUIPMENT_ID,
                SERVICE_LEVEL_ID,
                PROTECTION_LEVEL_ID,
                BILLING_METHOD,
                MIN_LONGEST_DIMENSION,
                MAX_LONGEST_DIMENSION,
                MIN_SECOND_LONGEST_DIMENSION,
                MAX_SECOND_LONGEST_DIMENSION,
                MIN_THIRD_LONGEST_DIMENSION,
                MAX_THIRD_LONGEST_DIMENSION,
                MIN_LENGTH_PLUS_GRITH,
                MAX_LENGTH_PLUS_GRITH,
                MIN_WEIGHT,
                MAX_WEIGHT,
                IS_ZONE_STOPOFF,
                ZONE_ID
           FROM LANE_ACCESSORIAL lane_acc
          WHERE lane_acc.lane_id = vratinglaneid
                AND lane_acc.rating_lane_dtl_seq = vduplicatelanedtlid
                AND lane_acc.lane_accessorial_id =
                       vduplicatelaneaccessorialid;
   BEGIN
      vduplicatelaneaccessorialid := 0;
      dup_dtl_acc_sql :=
         'SELECT NVL(lane_accessorial_id,0) FROM lane_accessorial WHERE tc_company_id = '
         || vtccompanyid
         || ' AND lane_id = '
         || vratinglaneid
         || ' AND rating_lane_dtl_seq = '
         || vduplicatelanedtlid
         || ' AND accessorial_id = '
         || vaccessorialcode
         || ' AND ((effective_dt between  '''
         || veffectivedt
         || ''' and '''
         || vexpirationdt
         || ''')'
         || ' OR   (expiration_dt between '''
         || veffectivedt
         || ''' and '''
         || vexpirationdt
         || '''))'
         || ' AND expiration_dt >= effective_dt';

      IF (vpayeecarriercode IS NOT NULL)
      THEN
         dup_dtl_acc_sql :=
               dup_dtl_acc_sql
            || ' AND payee_carrier_id = '
            || vpayeecarriercode;
      ELSE
         dup_dtl_acc_sql :=
            dup_dtl_acc_sql || ' AND payee_carrier_id  IS NULL';
      END IF;

      IF (p_minimum_size IS NOT NULL AND p_maximum_size IS NOT NULL)
      THEN
         dup_dtl_acc_sql :=
               dup_dtl_acc_sql
            || ' AND ( '
            || '( '
            || p_minimum_size
            || ' >= minimum_size and '
            || p_minimum_size
            || ' <= maximum_size) OR '
            || '( '
            || p_maximum_size
            || ' >= minimum_size and '
            || p_maximum_size
            || ' <= maximum_size) OR '
            || '( minimum_size > '
            || p_minimum_size
            || ' and minimum_size < '
            || p_maximum_size
            || ')  OR '
            || '( maximum_size > '
            || p_minimum_size
            || ' and maximum_size < '
            || p_maximum_size
            || '))';
      ELSE
         dup_dtl_acc_sql :=
            dup_dtl_acc_sql
            || ' AND minimum_size is null and maximum_size is null';
      END IF;

      OPEN dup_dtl_acc_cursor FOR dup_dtl_acc_sql;

      FETCH dup_dtl_acc_cursor INTO vduplicatelaneaccessorialid;

      CLOSE dup_dtl_acc_cursor;

      IF (vduplicatelaneaccessorialid > 0)
      THEN
         FOR p IN AUDIT_ACC_CURSOR
         LOOP
            rldAccCount := 0;

            SELECT MAX (CHANGE_ID)
              INTO rldAccCount
              FROM RATING_EVENT
             WHERE     tc_Company_Id = vTCCompanyId
                   AND RATING_LANE_ID = vRatingLaneId
                   AND RATING_LANE_DTL_ID = vratinglanedtlid
                   AND LANE_ACCESSORIAL_ID IS NOT NULL
                   AND is_pending_auth = 1;

            IF (rldAccCount > 0)
            THEN
               rldAccCount := rldAccCount + 1;
            ELSE
               rldAccCount := 1;
            END IF;

            INSERT_RATING_LANE_ACCESSORIAL (vtccompanyid,
                                            vratinglaneid,
                                            vratinglanedtlid,
                                            p.accessorial_id,
                                            p.rate,
                                            p.minimum_rate,
                                            p.currency_code,
                                            p.minimum_size,
                                            p.maximum_size,
                                            p.size_uom_id,
                                            p.is_auto_approve,
                                            p.effective_dt,
                                            p.expiration_dt,
                                            p.payee_carrier_id,
                                            p.is_shipment_accessorial,
                                            p.MIN_RATE_TYPE,
                                            p.MAXIMUM_RATE,
                                            p.PACKAGE_TYPE_ID,
                                            p.CUSTOMER_RATE,
                                            p.CUSTOMER_MIN_CHARGE,
                                            p.CALCULATED_RATE,
                                            p.AMOUNT,
                                            p.BASE_AMOUNT,
                                            p.BASE_CHARGE,
                                            p.RANGE_MIN_AMOUNT,
                                            p.RANGE_MAX_AMOUNT,
                                            p.INCREMENT_VAL,
                                            p.MOT_ID,
                                            p.EQUIPMENT_ID,
                                            p.SERVICE_LEVEL_ID,
                                            p.PROTECTION_LEVEL_ID,
                                            p.BILLING_METHOD,
                                            p.MIN_LONGEST_DIMENSION,
                                            p.MAX_LONGEST_DIMENSION,
                                            p.MIN_SECOND_LONGEST_DIMENSION,
                                            p.MAX_SECOND_LONGEST_DIMENSION,
                                            p.MIN_THIRD_LONGEST_DIMENSION,
                                            p.MAX_THIRD_LONGEST_DIMENSION,
                                            p.MIN_LENGTH_PLUS_GRITH,
                                            p.MAX_LENGTH_PLUS_GRITH,
                                            p.MIN_WEIGHT,
                                            p.MAX_WEIGHT,
                                            p.IS_ZONE_STOPOFF,
                                            TO_CHAR (p.ZONE_ID),
											vCommID,     -- CMGT-615 Starts here
											vMaxCommId);	-- CMGT-615 Ends here	


            SELECT MAX (lane_accessorial_id)
              INTO v_lane_acces_id_seq
              FROM lane_accessorial
             WHERE lane_id = vratinglaneid
                   AND rating_lane_dtl_seq = vratinglanedtlid;


            IF (NVL (p.MINIMUM_SIZE, 0) <> NVL (p_minimum_size, 0))
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    NULL,
                                    vTCCompanyId,
                                    rldAccCount,
                                    'Min Qty',
                                    TO_CHAR (p.MINIMUM_SIZE),
                                    TO_CHAR (p_minimum_size),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    v_lane_acces_id_seq);
            END IF;

            IF (NVL (p.MAXIMUM_SIZE, 0) <> NVL (p_maximum_size, 0))
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    NULL,
                                    vTCCompanyId,
                                    rldAccCount,
                                    'Max Qty',
                                    TO_CHAR (p.MAXIMUM_SIZE),
                                    TO_CHAR (p_maximum_size),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    v_lane_acces_id_seq);
            END IF;


            IF (NVL (p.RATE, 0) <> NVL (vRate, 0))
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    NULL,
                                    vTCCompanyId,
                                    rldAccCount,
                                    'Rate',
                                    TO_CHAR (p.RATE),
                                    TO_CHAR (vRate),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    v_lane_acces_id_seq);
            END IF;

            IF (NVL (p.MINIMUM_RATE, 0) <> NVL (vMinRate, 0))
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    NULL,
                                    vTCCompanyId,
                                    rldAccCount,
                                    'Min. Rate',
                                    TO_CHAR (p.MINIMUM_RATE),
                                    TO_CHAR (vMinRate),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    v_lane_acces_id_seq);
            END IF;

            IF (p.CURRENCY_CODE <> vCurrencyCode)
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    NULL,
                                    vTCCompanyId,
                                    rldAccCount,
                                    'Currency',
                                    TO_CHAR (p.CURRENCY_CODE),
                                    TO_CHAR (vCurrencyCode),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    v_lane_acces_id_seq);
            END IF;

            IF (p.IS_AUTO_APPROVE <> vIsApproved)
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    NULL,
                                    vTCCompanyId,
                                    rldAccCount,
                                    'Is Auto Approve',
                                    TO_CHAR (p.IS_AUTO_APPROVE),
                                    TO_CHAR (vIsApproved),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    v_lane_acces_id_seq);
            END IF;
            
            IF (p.PAYEE_CARRIER_ID <> vPayeeCarrierCode)
            THEN
               Insert_Rating_Event (vRatingLaneId,
                                    vratinglanedtlid,
                                    NULL,
                                    vTCCompanyId,
                                    rldAccCount,
                                    'Payee',
                                    TO_CHAR (p.PAYEE_CARRIER_ID),
                                    TO_CHAR (vPayeeCarrierCode),
                                    NULL,
                                    vLastUpdatedSrcType,
                                    vLastUpdatedSrc,
                                    v_lane_acces_id_seq);
            END IF;

            IF (p.size_uom_id <> p_size_uom)
            THEN
               insert_rating_event (vratinglaneid,
                                    vratinglanedtlid,
                                    NULL,
                                    vtccompanyid,
                                    rldacccount,
                                    'UOM',
                                    TO_CHAR (p.size_uom_id),
                                    TO_CHAR (p_size_uom),
                                    NULL,
                                    vlastupdatedsrctype,
                                    vlastupdatedsrc,
                                    v_lane_acces_id_seq);
            END IF;

            /**[gakumar] TODO **/
            rldAccCount := rldAccCount + 1;
            COMMIT;
         END LOOP;
      ELSE
         INSERT_RATING_LANE_ACCESSORIAL (vTCCompanyId,
                                         vratinglaneid,
                                         vratinglanedtlid,
                                         vaccessorialcode,
                                         vRate,
                                         vMinRate,
                                         vCurrencyCode,
                                         p_minimum_size,
                                         p_maximum_size,
                                         p_size_uom,
                                         vIsApproved,
                                         vEffectiveDT,
                                         vExpirationDT,
                                         vPayeeCarrierCode,
                                         vIsShipmentAccessorial,
                                         vMinRateType,  -- MACR00159593 starts
                                         vMaximumRate,
                                         vPackageType,
                                         vCustomerRate,
                                         vCustomerMinCharge,
                                         vCalculatedRate,
                                         vAmount,
                                         vBaseAmount,
                                         vBaseCharge,
                                         vRangeMinAmount,
                                         vRangeMaxAmount,
                                         vIncrement,
                                         vMot,
                                         vEquipment,
                                         vServiceLevel,
                                         vProtectionLevel,
                                         vBillingMethod,
                                         vMinLongestDim,
                                         vMaxLongestDim,
                                         vMinSecondLongestDim,
                                         vMaxSecondLongestDim,
                                         vMinThirdLongestDim,
                                         vMaxThirdLongestDim,
                                         vMinLengthGrith,
                                         vMaxLengthGrith,
                                         vMinWeight,
                                         vMaxWeight,
                                         vIsZoneStopOff,
                                          vZoneId ,         -- MACR00159593 ends
                                          vCommID,     -- CMGT-615 Starts here
										vMaxCommId	 );	-- CMGT-615 Ends here 
      END IF;
   END AUDIT_LANE_ACCESSORIAL;


   FUNCTION CHECK_DUP_LANE_ACC (
      vTCCompanyId          IN COMPANY.COMPANY_ID%TYPE,
      vRatingLaneId         IN rating_lane.LANE_ID%TYPE,
      vDuplicateLaneDtlId   IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vAccessorialId        IN lane_accessorial.ACCESSORIAL_ID%TYPE,
      vEffectiveDT          IN rating_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT         IN rating_lane_dtl.EXPIRATION_DT%TYPE)
      RETURN NUMBER
   IS
      acc_id          NUMBER (12);
      acc_id_cursor   ref_curtype;
      acc_sql         VARCHAR (2000);
   BEGIN
      acc_sql :=
            ' SELECT LANE_ACCESSORIAL_ID '
         || ' FROM LANE_ACCESSORIAL '
         || ' WHERE TC_COMPANY_ID = '
         || vTCCompanyId
         || ' AND LANE_ID = '
         || vRatingLaneId
         || ' AND RATING_LANE_DTL_SEQ = '
         || vDuplicateLaneDtlId
         || ' AND ACCESSORIAL_ID = '
         || vAccessorialId
         || ' AND EFFECTIVE_DT = '''
         || TO_CHAR (vEffectiveDT)
         || ''' '
         || ' AND EXPIRATION_DT = '''
         || TO_CHAR (vExpirationDT)
         || ''' ';


      acc_id := 0;

      OPEN acc_id_cursor FOR acc_sql;

      LOOP
         FETCH acc_id_cursor INTO acc_id;

         EXIT WHEN acc_id_cursor%NOTFOUND;
      END LOOP;

      CLOSE acc_id_cursor;

      RETURN acc_id;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN 0;
   END CHECK_DUP_LANE_ACC;



   FUNCTION VALIDATE_CUST_CARR_MOT_FEAS (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vRatingLaneId      IN rating_lane.LANE_ID%TYPE,
      vRatingLaneDtlId   IN rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE,
      vCarrierCode       IN carrier_code.CARRIER_CODE%TYPE,
      vMot               IN MOT.MOT%TYPE)
      RETURN NUMBER
   IS
      vCount       NUMBER;
      vPassFail    NUMBER;
      vCustCode    IMPORT_RATING_LANE.CUSTOMER_CODE%TYPE;
      vCustId      IMPORT_RATING_LANE.CUSTOMER_ID%TYPE;
      vCarrierId   IMPORT_RATING_LANE_DTL.CARRIER_ID%TYPE;
      vMotId       IMPORT_RATING_LANE_DTL.MOT_ID%TYPE;
   BEGIN
      vPassFail := 0;

      SELECT CUSTOMER_CODE, CUSTOMER_ID
        INTO vCustCode, vCustId
        FROM IMPORT_RATING_LANE
       WHERE LANE_ID = vRatingLaneId;

      SELECT CARRIER_ID, MOT_ID
        INTO vCarrierId, vMotId
        FROM IMPORT_RATING_LANE_DTL
       WHERE LANE_ID = vRatingLaneId
             AND RATING_LANE_DTL_SEQ = vRatingLaneDtlId;

      IF (vCustCode IS NOT NULL)
      THEN
         IF (vCarrierCode IS NOT NULL)
         THEN
            SELECT COUNT (INFEASIBILITY_ID)
              INTO vCount
              FROM INFEASIBILITY
             WHERE     INFEASIBILITY_TYPE = 'CUCA'
                   AND VALUE1 = (SELECT CUSTOMER_ID
                                   FROM IMPORT_RATING_LANE
                                  WHERE LANE_ID = vRatingLaneId)
                   AND VALUE2 = TO_CHAR (vCarrierId)
                   AND TC_COMPANY_ID = vTCCompanyId;

            IF (vCount > 0)
            THEN
               vPassFail := 1;


               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vRatingLaneId,
                  vRatingLaneDtlId,
                  vCustCode || ' is infeasible with ' || vCarrierCode,
                  4000036,
                  vCustCode || '<sep>' || vCarrierCode);
            END IF;
         END IF;


         IF (vMot IS NOT NULL)
         THEN
            SELECT COUNT (INFEASIBILITY_ID)
              INTO vCount
              FROM INFEASIBILITY
             WHERE     INFEASIBILITY_TYPE = 'CUMO'
                   AND VALUE1 = (SELECT CUSTOMER_ID
                                   FROM IMPORT_RATING_LANE
                                  WHERE LANE_ID = vRatingLaneId)
                   AND VALUE2 = TO_CHAR (vMotId)
                   AND TC_COMPANY_ID = vTCCompanyId;

            IF (vCount > 0)
            THEN
               vPassFail := 1;

               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vRatingLaneId,
                  vRatingLaneDtlId,
                  vCustCode || ' is infeasible with ' || vMot,
                  4000036,
                  vCustCode || '<sep>' || vMot);
            END IF;
         END IF;
      END IF;

      RETURN vPassFail;
   END VALIDATE_CUST_CARR_MOT_FEAS;
   
       FUNCTION VALIDATE_ACC_FEASBLE_INCOTERMS
(
   vTCCompanyId			IN COMPANY.COMPANY_ID%TYPE,
   vRatingLaneId    IN RATING_LANE.LANE_ID%TYPE,
   vRatingLaneDtlId   IN RATING_LANE_DTL.RATING_LANE_DTL_SEQ%TYPE,
   vLaneAccId      IN LANE_ACCESSORIAL.LANE_ACCESSORIAL_ID%TYPE
)
  RETURN NUMBER
  IS
  vPassFail			NUMBER;
  vCount        NUMBER;
  vIncotermName IMP_LANE_ACC_FEASIBLE_INCOTERM.INCOTERM_NAME%TYPE;
  vIncotermId IMP_LANE_ACC_FEASIBLE_INCOTERM.INCOTERM_ID%TYPE;
  vAccId IMP_LANE_ACC_FEASIBLE_INCOTERM.LANE_ACC_ID%TYPE;
  
  CURSOR LANE_ACC_INCOTERM_CURSOR IS
			SELECT INCOTERM_NAME
			FROM IMP_LANE_ACC_FEASIBLE_INCOTERM
			WHERE RATING_LANE_ID = vRatingLaneId			
			AND RATING_LANE_DTL_SEQ = vRatingLaneDtlId
      AND LANE_ACC_ID = vLaneAccId;
      
  BEGIN
      OPEN LANE_ACC_INCOTERM_CURSOR;
      <<LANE_ACC_INCOTERM_LOOP>>
      LOOP
      vPassFail := 0;
      FETCH LANE_ACC_INCOTERM_CURSOR INTO vIncotermName;
      EXIT LANE_ACC_INCOTERM_LOOP WHEN LANE_ACC_INCOTERM_CURSOR%NOTFOUND;
          
          SELECT COUNT(INCOTERM_NAME) INTO vCount FROM INCOTERM WHERE INCOTERM_NAME = vIncotermName;
         
          IF(vCount = 0) THEN
				     vPassFail := 1;
				     INSERT_RATING_LANE_DTL_ERROR( vTCCompanyId,
			       vRatingLaneId,
			       vRatingLaneDtlId,
			       ' Lane Accessorial incoterm name is not correct',
			       4720193,
			       NULL
			      );
          ELSE
          SELECT INCOTERM_ID INTO vIncotermId FROM INCOTERM WHERE INCOTERM_NAME = vIncotermName;
          UPDATE IMP_LANE_ACC_FEASIBLE_INCOTERM SET INCOTERM_ID = vIncotermId WHERE
                    RATING_LANE_ID = vRatingLaneId			
                    AND RATING_LANE_DTL_SEQ = vRatingLaneDtlId
                    AND LANE_ACC_ID = vLaneAccId
                    AND INCOTERM_NAME = vIncotermName;
 		      END IF;
          
      END LOOP LANE_ACC_INCOTERM_LOOP;
      return vPassFail;
END VALIDATE_ACC_FEASBLE_INCOTERMS;


-----------------------Added here for INfeasibility between SL and AOPTG------------------------------------------------------
 FUNCTION VAL_SL_FEASBLE_ACCOPTGROUP
(
   vTCCompanyId			IN COMPANY.COMPANY_ID%TYPE,
   vRatingLaneId    IN RATING_LANE.LANE_ID%TYPE,
   vRatingLaneDtlId   IN RATING_LANE_DTL.RATING_LANE_DTL_SEQ%TYPE,
   vAccId   IN    ACCESSORIAL_CODE.ACCESSORIAL_ID%TYPE,
   vServLevelId IN IMPORT_LANE_ACCESSORIAL.SERVICE_LEVEL_ID%TYPE
)
  RETURN NUMBER
  IS
  vPassFail			NUMBER;
  vCount        NUMBER;
  vaccOptionGroup  VARCHAR2 (4);
  
  BEGIN
  vCount := 0;
  vaccOptionGroup := NULL;
   vPassFail := 0;
  --CMGT-731 START
	BEGIN
      SELECT ACCESSORIAL_OPTION_GROUP_ID
      INTO vaccOptionGroup
      FROM ACCESSORIAL_CODE WHERE ACCESSORIAL_ID=vAccId  AND TC_COMPANY_ID=vTCCompanyId ;
  exception 
  when no_data_found then
     vaccoptiongroup := NULL;
  END;
	--CMGT-731 END
	SELECT COUNT (INFEASIBILITY_ID)
        INTO vCount
        FROM INFEASIBILITY
       WHERE INFEASIBILITY_TYPE = 'SLOG'
             AND VALUE1 = vServLevelId
             AND value2 = vaccOptionGroup
             AND TC_COMPANY_ID = vTCCompanyId;
			 
IF (vaccOptionGroup IS NOT NULL)
      THEN	
	 IF (vCount > 0)
        THEN
               vPassFail := 1;
               INSERT_RATING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vRatingLaneId,
                  vRatingLaneDtlId,
                  vServLevelId || ' is infeasible with ' || vaccOptionGroup,
                  4000051,
                  vServLevelId || '<sep>' || vaccOptionGroup);
      END IF;
END IF;	  
	  
  
	return vPassFail;
  
  END VAL_SL_FEASBLE_ACCOPTGROUP;

-------------------------ended here-------------------------------------------------------------
   



PROCEDURE TRSFR_LANE_ACC_FEAS_INCOTERM
(	
   vTCCompanyId			IN COMPANY.COMPANY_ID%TYPE,
   vImpRatingLaneId    IN RATING_LANE.LANE_ID%TYPE,
   vImpRatingLaneDtlId   IN RATING_LANE_DTL.RATING_LANE_DTL_SEQ%TYPE,
   vImpLaneAccId        IN LANE_ACCESSORIAL.LANE_ACCESSORIAL_ID%TYPE,
   vLaneAccId      IN LANE_ACCESSORIAL.LANE_ACCESSORIAL_ID%TYPE
) AS  
  vIncotermId IMP_LANE_ACC_FEASIBLE_INCOTERM.INCOTERM_ID%TYPE;
  
  CURSOR INCOTERM_CURSOR IS
			SELECT INCOTERM_ID
			FROM IMP_LANE_ACC_FEASIBLE_INCOTERM
			WHERE RATING_LANE_ID = vImpRatingLaneId			
			AND RATING_LANE_DTL_SEQ = vImpRatingLaneDtlId
      AND LANE_ACC_ID = vImpLaneAccId
      AND INCOTERM_ID IS NOT NULL;
      
  BEGIN      
      OPEN INCOTERM_CURSOR;
      
      <<INCOTERM_LOOP>>
      LOOP
      FETCH INCOTERM_CURSOR INTO vIncotermId;
      EXIT INCOTERM_LOOP WHEN INCOTERM_CURSOR%NOTFOUND;          
          INSERT INTO LANE_ACC_FEASIBLE_INCOTERM VALUES(vIncotermId, vLaneAccId);          
      END LOOP INCOTERM_LOOP;
END TRSFR_LANE_ACC_FEAS_INCOTERM;

 PROCEDURE VALIDATE_LANE_DTL_EPI (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vImportLaneId   IN RATING_LANE.LANE_ID%TYPE,
      vLaneId         IN RATING_LANE.LANE_ID%TYPE)
   AS
      vRatingLaneDtlSeq          rating_lane_dtl.RATING_LANE_DTL_SEQ%TYPE;
      vCarrierId        		 rating_lane_dtl.CARRIER_ID%TYPE;
	  vUseEPI					 COMB_LANE.USE_EPI%TYPE;
      CURSOR VALIDATE_LANE_DTL_CURSOR
      IS
         SELECT RL.rating_lane_dtl_seq,
                RL.carrier_id
           FROM IMPORT_RATING_LANE_DTL RL, CARRIER_CODE CC
          WHERE     RL.tc_company_id = vTCCompanyId
                AND RL.lane_id = vImportLaneId AND CC.CARRIER_ID=RL.CARRIER_ID AND CC.CARRIER_TYPE_ID IS NOT NULL AND CC.CARRIER_TYPE_ID != 24 
                AND RL.RATING_LANE_DTL_STATUS = 1 and RL.IMPORT_RATING_DTL_STATUS=0 AND RL.EXPIRATION_DT>RL.EFFECTIVE_DT AND RL.EXPIRATION_DT>GETDATE();
   BEGIN
      
	  BEGIN	  
		SELECT USE_EPI INTO vUseEPI FROM COMB_LANE where LANE_ID = vLaneId;
	  EXCEPTION
            WHEN NO_DATA_FOUND
			THEN
				vUseEPI:= 0;
	  END;
	  
	  IF (vUseEPI=1)
	  THEN
			FOR r IN VALIDATE_LANE_DTL_CURSOR
			LOOP
				vCarrierId := r.carrier_id;
				vRatingLaneDtlSeq := r.rating_lane_dtl_seq;
				
				INSERT_RATING_LANE_DTL_ERROR (
							vTCCompanyId,
							vImportLaneId,
							vRatingLaneDtlSeq,
							'Lane detail for native parcel carrier cannot be created when external parcel integration is enabled on the lane',
							4720201,
							NULL);
							
				UPDATE IMPORT_RATING_LANE_DTL
					SET RATING_LANE_DTL_STATUS = 4
					WHERE     TC_COMPANY_ID = vTCCompanyId
					AND LANE_ID = vImportLaneId
					AND RATING_LANE_DTL_SEQ = vRatingLaneDtlSeq;                  

			END LOOP;
	  END IF;	  
	  
	  
 END VALIDATE_LANE_DTL_EPI;
 
 PROCEDURE REPLICATE_RG_SA_LANE_DETAIL (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vLaneId        IN comb_lane.LANE_ID%TYPE,
      vRatingLaneDtlSeq      IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vIsRouting      IN comb_lane_dtl.IS_ROUTING%TYPE,
      vIsSailing     IN comb_lane_dtl.IS_SAILING%TYPE)
   IS
      vRatingDtlSeqNew   comb_lane_dtl.LANE_DTL_SEQ%TYPE;
   BEGIN
      SELECT MAX (LANE_DTL_SEQ) + 1
        INTO vRatingDtlSeqNew
        FROM COMB_LANE_DTL
       WHERE LANE_ID = vLaneID;

      INSERT INTO COMB_LANE_DTL (TC_COMPANY_ID,
                                 LANE_ID,
                                 LANE_DTL_SEQ,
                                 LANE_DTL_STATUS,
                                 IS_RATING,
                                 IS_ROUTING,
                                 IS_BUDGETED,
                                 LAST_UPDATED_SOURCE_TYPE,
                                 LAST_UPDATED_SOURCE,
                                 LAST_UPDATED_DTTM,
                                 CARRIER_ID,
                                 MOT_ID,
                                 EQUIPMENT_ID,
                                 SERVICE_LEVEL_ID,
                                 SCNDR_CARRIER_ID,
                                 PROTECTION_LEVEL_ID,
                                 EFFECTIVE_DT,
                                 EXPIRATION_DT,
                                 FIXED_TRANSIT_STANDARD_UOM,
                                 FIXED_TRANSIT_VALUE,
                                 IS_SAILING,
                                 SAILING_FREQUENCY_TYPE,
                                 SAILING_SCHEDULE_NAME,
                                 MASTER_LANE_ID,
                                 O_SHIP_VIA,
                                 D_SHIP_VIA,
                                 CONTRACT_NUMBER,
                                 CUSTOM_TEXT1,
                                 CUSTOM_TEXT2,
                                 CUSTOM_TEXT3,
                                 CUSTOM_TEXT4,
                                 CUSTOM_TEXT5,
                                 TT_TOLERANCE_FACTOR)
         SELECT TC_COMPANY_ID,
                LANE_ID,
                vRatingDtlSeqNew,
                0,
                0,
                IS_ROUTING,
                IS_BUDGETED,
                3,
                'OMS',
                GETDATE (),
                CARRIER_ID,
                MOT_ID,
                EQUIPMENT_ID,
                SERVICE_LEVEL_ID,
                SCNDR_CARRIER_ID,
                PROTECTION_LEVEL_ID,
                EFFECTIVE_DT,
                EXPIRATION_DT,
                FIXED_TRANSIT_STANDARD_UOM,
                FIXED_TRANSIT_VALUE,
                IS_SAILING,
                SAILING_FREQUENCY_TYPE,
                SAILING_SCHEDULE_NAME,
                MASTER_LANE_ID,
                O_SHIP_VIA,
                D_SHIP_VIA,
                CONTRACT_NUMBER,
                CUSTOM_TEXT1,
                CUSTOM_TEXT2,
                CUSTOM_TEXT3,
                CUSTOM_TEXT4,
                CUSTOM_TEXT5,
                TT_TOLERANCE_FACTOR
           FROM COMB_LANE_DTL
          WHERE LANE_ID = vLaneId AND LANE_DTL_SEQ = vRatingLaneDtlSeq;

      COPY_CHILD_ROW (vTCCompanyId,
                      vLaneId,
                      -1,
                      vRatingLaneDtlSeq,
                      vRatingDtlSeqNew,
                      0,
                      vIsRouting,
                      vIsSailing,
					  'OMS');
   END REPLICATE_RG_SA_LANE_DETAIL;

END Rating_Lane_Validation_Pkg;
/
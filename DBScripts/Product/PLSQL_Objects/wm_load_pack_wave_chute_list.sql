create or replace procedure wm_load_pack_wave_chute_list
(
    p_whse                    in facility.whse%type,
    p_chute_util              in number,
    p_max_pack_wave           in number default null,
    p_pack_wave_parm_id       in pack_wave_parm_hdr.pack_wave_parm_id%type default null,
    p_start_lvl               in out pack_wave_parm_hdr.start_lvl%type,
    p_sys_cd_flag_lvl         out number
)
as
    v_num_sorter_grps         number(4);
    v_min_pw_seq_nbr          pack_wave_parm_dtl.pack_wave_seq_nbr%type;
begin
    -- get sorter group based on start_lvl setting
    wm_fetch_sorter_grp_config(p_whse, p_pack_wave_parm_id, p_start_lvl, v_min_pw_seq_nbr, 
        v_num_sorter_grps, p_sys_cd_flag_lvl);
        
    -- load chutes for all possible pack waves by cycling through sorters;
    -- initialize remaining caps with max caps configured in chute_master
    insert into tmp_pack_wave_selected_chutes
    (
        chute_id, chute_type, pack_wave_seq_nbr, sorter_grp, sorter_seq_nbr, chute_row_id, 
        remaining_vol, remaining_units, remaining_cartons, remaining_wt, remaining_orders, 
        critcl_dim_1, critcl_dim_2, critcl_dim_3
    )
    with sg_list as
    (
        select pwpd.sorter_grp, row_number() over(order by 
            case 
                when p_start_lvl = '0' then pwpd.pack_wave_seq_nbr 
                when p_start_lvl in ('1', '2') then 
                    case 
                        when pwpd.pack_wave_seq_nbr > v_min_pw_seq_nbr 
                            then pwpd.pack_wave_seq_nbr - (v_num_sorter_grps - v_min_pw_seq_nbr) 
                        when pwpd.pack_wave_seq_nbr <= v_min_pw_seq_nbr 
                            then pwpd.pack_wave_seq_nbr + (v_num_sorter_grps - v_min_pw_seq_nbr) 
                    end 
            end) seq
        from pack_wave_parm_dtl pwpd
        where pwpd.pack_wave_parm_id = p_pack_wave_parm_id and pwpd.sorter_grp is not null
    ),
    row_gen as
    (
        select rownum pack_wave_seq_nbr
        from dual
        connect by rownum <= p_max_pack_wave
    )
    select cm.chute_id, cm.chute_type, rg.pack_wave_seq_nbr, sg.sorter_grp, 
        sg.sorter_seq_nbr, rownum, coalesce(nullif(trunc(cm.max_vol * p_chute_util),0),999999999) remaining_vol,
        coalesce(nullif(trunc(cm.max_units * p_chute_util), 0), 999999999) remaining_units,
        coalesce(nullif(trunc(cm.max_carton * p_chute_util), 0), 9999999) remaining_cartons,
        coalesce(nullif(trunc(cm.max_wt * p_chute_util),0),999999999) remaining_wt,
        coalesce(nullif(trunc(cm.max_pkts * p_chute_util), 0), 9999999) remaining_orders,
        case when cm.len >= cm.width and cm.len >= cm.ht then cm.len
            when cm.width >= cm.len and cm.width >= cm.ht then cm.width
            else cm.ht
        end critcl_dim_1,
        case when cm.len >= cm.width and cm.len <= cm.ht then cm.len
            when cm.width >= cm.len and cm.width <= cm.ht then cm.width
            else cm.ht
        end critcl_dim_2,
        case when cm.len <= cm.width and cm.len <= cm.ht then cm.len
            when cm.width <= cm.len and cm.width <= cm.ht then cm.width
            else cm.ht
        end critcl_dim_3
    from chute_master cm
    join sorter_grp sg on sg.sorter = cm.sorter
    join sg_list sl on sl.sorter_grp = sg.sorter_grp
    join row_gen rg on mod(rg.pack_wave_seq_nbr, v_num_sorter_grps)
        = mod(sl.seq, v_num_sorter_grps)
    where cm.whse = p_whse and cm.stat_code = 0;
    wm_cs_log('Loaded chute data ' || sql%rowcount);
end;
/
show errors;
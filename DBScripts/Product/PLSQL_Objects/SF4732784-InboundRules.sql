delete from ui_menu_item_permission where permission = 'WMAUIINBDRLS' and ui_menu_item_id in 
( select ui_menu_item_id from ui_menu_item where link in ( '#{inboundRuleEventBackingBean.addAction}', 'return checkDeleteEvent();','return checkDeletes();', 'return checkDetail()', 'return checkCopy();'));

delete from UI_MENU_ITEM where ui_menu_screen_id in ( select ui_menu_screen_id from ui_menu_screen where screen_id = '44000029') and link in ( '#{inboundRuleEventBackingBean.addAction}', 'return checkDeleteEvent();','return checkDeletes();', 'return checkDetail()', 'return checkCopy();');

delete from ui_menu_screen where screen_id = '44000029' and menu_id = '1';


Insert INTO UI_MENU_SCREEN (UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID)
SELECT UI_MENU_SCREEN_SEQ.nextval, '1','44000029','SYSTEM',null,null
from dual
Where Not exists (SELECT 1 FROM UI_MENU_SCREEN WHERE screen_id = '44000029' and menu_id = '1');

DECLARE
    v_Count PLS_INTEGER := 0;
	v_ScreenId PLS_INTEGER := 0;
	BEGIN
	SELECT COUNT(*) INTO v_Count FROM UI_MENU_SCREEN WHERE screen_id = '44000029' and menu_id = '1';
     IF V_COUNT = 1 THEN
           SELECT ui_menu_screen_id INTO v_ScreenId FROM UI_MENU_SCREEN WHERE screen_id = '44000029' and menu_id = '1';
		   
Insert INTO UI_MENU_ITEM (UI_MENU_ITEM_ID, UI_MENU_SCREEN_ID, MENU_ID,IS_DISPLAY,DISPLAY_TEXT,LINK,LINK_TYPE,LOCN_FLAG, ITEM_SEQ_NBR,  UI_MENU_COMP_ID, IS_MORE, AJAX_ACTION, AJAX_RERENDER, AJAX_ONCOMPLETE, RENDERED, IMAGE_URL)
SELECT UI_MENU_ITEM_SEQ.nextval,v_ScreenId,'1','0','Add','#{inboundRuleEventBackingBean.addAction}','JSFA','7','1','dataForm:page-content_footer-panel','0',null,null,null,'TRUE',null
from dual
Where Not exists (SELECT 1 FROM UI_MENU_ITEM WHERE link = '#{inboundRuleEventBackingBean.addAction}' and ui_menu_screen_id = v_ScreenId);

Insert INTO UI_MENU_ITEM (UI_MENU_ITEM_ID, UI_MENU_SCREEN_ID, MENU_ID,IS_DISPLAY,DISPLAY_TEXT,LINK,LINK_TYPE,LOCN_FLAG, ITEM_SEQ_NBR,  UI_MENU_COMP_ID, IS_MORE, AJAX_ACTION, AJAX_RERENDER, AJAX_ONCOMPLETE, RENDERED, IMAGE_URL)
SELECT UI_MENU_ITEM_SEQ.nextval,v_ScreenId,'1','0','DeleteRules','return checkDeleteEvent();','JS','7','3','dataForm:page-content_footer-panel','0',null,null,null,'TRUE',null
from dual
Where Not exists (SELECT 1 FROM UI_MENU_ITEM WHERE link = 'return checkDeleteEvent();' and ui_menu_screen_id = v_ScreenId);

Insert INTO UI_MENU_ITEM (UI_MENU_ITEM_ID, UI_MENU_SCREEN_ID, MENU_ID,IS_DISPLAY,DISPLAY_TEXT,LINK,LINK_TYPE,LOCN_FLAG, ITEM_SEQ_NBR,  UI_MENU_COMP_ID, IS_MORE, AJAX_ACTION, AJAX_RERENDER, AJAX_ONCOMPLETE, RENDERED, IMAGE_URL)
SELECT UI_MENU_ITEM_SEQ.nextval,v_ScreenId,'1','0','DeleteEvent','return checkDeletes();','JS','7','2','dataForm:page-content_footer-panel','0',null,null,null,'TRUE',null
from dual
Where Not exists (SELECT 1 FROM UI_MENU_ITEM WHERE link = 'return checkDeletes();' and ui_menu_screen_id = v_ScreenId);

Insert INTO UI_MENU_ITEM (UI_MENU_ITEM_ID, UI_MENU_SCREEN_ID, MENU_ID,IS_DISPLAY,DISPLAY_TEXT,LINK,LINK_TYPE,LOCN_FLAG, ITEM_SEQ_NBR,  UI_MENU_COMP_ID, IS_MORE, AJAX_ACTION, AJAX_RERENDER, AJAX_ONCOMPLETE, RENDERED, IMAGE_URL)
SELECT UI_MENU_ITEM_SEQ.nextval,v_ScreenId,'1','0','View','return checkDetail();','JS','7','1','dataForm:page-content_footer-panel','0',null,null,null,'TRUE',null
from dual
Where Not exists (SELECT 1 FROM UI_MENU_ITEM WHERE link = 'return checkDetail();' and ui_menu_screen_id = v_ScreenId);

Insert INTO UI_MENU_ITEM (UI_MENU_ITEM_ID, UI_MENU_SCREEN_ID, MENU_ID,IS_DISPLAY,DISPLAY_TEXT,LINK,LINK_TYPE,LOCN_FLAG, ITEM_SEQ_NBR,  UI_MENU_COMP_ID, IS_MORE, AJAX_ACTION, AJAX_RERENDER, AJAX_ONCOMPLETE, RENDERED, IMAGE_URL)
SELECT UI_MENU_ITEM_SEQ.nextval,v_ScreenId,'1','1','Copy','return checkCopy();','JS','7','5','dataForm:page-content_footer-panel','0',null,null,null,'TRUE',null
from dual
Where Not exists (SELECT 1 FROM UI_MENU_ITEM WHERE link = 'return checkCopy();' and ui_menu_screen_id = v_ScreenId);




INSERT INTO UI_MENU_ITEM_PERMISSION ( UI_MENU_ITEM_PERMISSION_ID,UI_MENU_ITEM_ID,PERMISSION) 
SELECT  UI_MENU_ITEM_PERM_SEQ.nextval,(SELECT UI_MENU_ITEM_ID FROM UI_MENU_ITEM WHERE link = '#{inboundRuleEventBackingBean.addAction}' and ui_menu_screen_id = v_ScreenId),'WMAUIINBDRLS'
from dual 
where not exists (select 1 from ui_menu_item_permission where ui_menu_item_id in (SELECT UI_MENU_ITEM_ID FROM UI_MENU_ITEM WHERE link = '#{inboundRuleEventBackingBean.addAction}' and ui_menu_screen_id = v_ScreenId));

INSERT INTO UI_MENU_ITEM_PERMISSION ( UI_MENU_ITEM_PERMISSION_ID,UI_MENU_ITEM_ID,PERMISSION) 
SELECT  UI_MENU_ITEM_PERM_SEQ.nextval,(SELECT UI_MENU_ITEM_ID FROM UI_MENU_ITEM WHERE link = 'return checkDeleteEvent();' and ui_menu_screen_id = v_ScreenId),'WMAUIINBDRLS'
from dual 
where not exists (select 1 from ui_menu_item_permission where ui_menu_item_id in (SELECT UI_MENU_ITEM_ID FROM UI_MENU_ITEM WHERE link = 'return checkDeleteEvent();' and ui_menu_screen_id = v_ScreenId));

INSERT INTO UI_MENU_ITEM_PERMISSION ( UI_MENU_ITEM_PERMISSION_ID,UI_MENU_ITEM_ID,PERMISSION) 
SELECT  UI_MENU_ITEM_PERM_SEQ.nextval,(SELECT UI_MENU_ITEM_ID FROM UI_MENU_ITEM WHERE link = 'return checkDeletes();' and ui_menu_screen_id = v_ScreenId),'WMAUIINBDRLS'
from dual 
where not exists (select 1 from ui_menu_item_permission where ui_menu_item_id in (SELECT UI_MENU_ITEM_ID FROM UI_MENU_ITEM WHERE link = 'return checkDeletes();' and ui_menu_screen_id = v_ScreenId));

INSERT INTO UI_MENU_ITEM_PERMISSION ( UI_MENU_ITEM_PERMISSION_ID,UI_MENU_ITEM_ID,PERMISSION) 
SELECT  UI_MENU_ITEM_PERM_SEQ.nextval,(SELECT UI_MENU_ITEM_ID FROM UI_MENU_ITEM WHERE link = 'return checkDetail();' and ui_menu_screen_id = v_ScreenId),'WMAUIINBDRLS'
from dual 
where not exists (select 1 from ui_menu_item_permission where ui_menu_item_id in (SELECT UI_MENU_ITEM_ID FROM UI_MENU_ITEM WHERE link = 'return checkDetail();' and ui_menu_screen_id = v_ScreenId));

INSERT INTO UI_MENU_ITEM_PERMISSION ( UI_MENU_ITEM_PERMISSION_ID,UI_MENU_ITEM_ID,PERMISSION) 
SELECT  UI_MENU_ITEM_PERM_SEQ.nextval,(SELECT UI_MENU_ITEM_ID FROM UI_MENU_ITEM WHERE link = 'return checkCopy();' and ui_menu_screen_id = v_ScreenId),'WMAUIINBDRLS'
from dual 
where not exists (select 1 from ui_menu_item_permission where ui_menu_item_id in (SELECT UI_MENU_ITEM_ID FROM UI_MENU_ITEM WHERE link = 'return checkCopy();' and ui_menu_screen_id = v_ScreenId));

commit;

     END IF;
	EXCEPTION WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001, 'Error in inserting into UI_MENU_ITEM/PERMISSION ' || CHR(10) || SQLERRM);
	END;

/

CREATE OR REPLACE procedure ui_purge_h
(
    p_rowset                out types.cursortype,
    p_purge_config_ids      in varchar2,
    p_user_id               in varchar2 default 'PURGE'
)
as
    v_msg              varchar2(1000);
    p_retval           number;
begin
    wm_archive_pkg.ui_purge(p_purge_config_ids, p_user_id);
    open p_rowset for
        select p_retval as p_retval from dual;
exception
    when others then
        v_msg := (sqlerrm(sqlcode));
        exception_msg_log_insert(v_msg);
end ui_purge_h;

/

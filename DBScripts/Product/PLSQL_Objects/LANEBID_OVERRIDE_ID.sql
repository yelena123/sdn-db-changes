CREATE OR REPLACE TRIGGER LANEBID_OVERRIDE_ID
   BEFORE INSERT
   ON lanebid
   REFERENCING NEW AS newvalue
   FOR EACH ROW
DECLARE
-- local variables here
BEGIN
   :newvalue.override_id := override_id_seq.nextval;
END lanebid_override_id;
/



CREATE OR REPLACE
PROCEDURE Xmllaneawardupload (I_RFPID INTEGER, I_ROUND_NUM INTEGER)
IS
   T_ALLOWPACKAGES    INTEGER;
   RATINGROUTING      INTEGER;
   NEWSCEN            INTEGER;
   TC                 INTEGER;
   --PACKAGEFAIL DECIMAL(13,4)    ;
   BASESCENARIOID     DECIMAL (13, 4);
   PV_FIELD1          INTEGER;
   PV_FIELD2          INTEGER;
   PV_FIELD3          VARCHAR (50);
   PV_FIELD4          INTEGER;

   C_UPLOAD_TYPE_ID   INTEGER;                       -- used for xmlerror type
   C_ERRORS           INTEGER; -- used to check if there were errors in uploading lanes
BEGIN
   C_UPLOAD_TYPE_ID := 4;

   SELECT MAX (SCENARIOID)
     INTO BASESCENARIOID
     FROM SCENARIO
    WHERE RFPID = I_RFPID AND ROUND_NUM = I_ROUND_NUM AND ISBASESCENARIO = 1;

   SELECT ALLOWPACKAGES
     INTO T_ALLOWPACKAGES
     FROM RFP
    WHERE RFPID = I_RFPID AND ROUND_NUM = I_ROUND_NUM;

   SELECT TCCOMPANYID
     INTO TC
     FROM RFP
    WHERE RFPID = I_RFPID AND ROUND_NUM = I_ROUND_NUM;

   UPDATE XMLLANEAWARD
      SET OBCARRIERCODEID =
             (SELECT OBCARRIERCODEID
                FROM OBCARRIERCODE CC, COMPANY C
               WHERE     C.COMPANY_ID = CC.TPCOMPANYID
                     AND C.COMPANY_NAME = XMLLANEAWARD.COMPANYNAME
                     AND CC.CARRIERCODE IS NULL)
    WHERE XMLLANEAWARD.CARRIERCODE IS NULL;

   UPDATE XMLLANEAWARD
      SET OBCARRIERCODEID =
             (SELECT OBCARRIERCODEID
                FROM OBCARRIERCODE CC, COMPANY C
               WHERE C.COMPANY_ID = CC.TPCOMPANYID
                     AND C.COMPANY_NAME = XMLLANEAWARD.COMPANYNAME
                     AND NVL (XMLLANEAWARD.CARRIERCODE, '|||') =
                            NVL (CC.CARRIERCODE, '|||')
                     AND CC.TCCOMPANYID = TC)
    WHERE XMLLANEAWARD.CARRIERCODE IS NOT NULL;

   SELECT ALLOWPACKAGES, EXPORTTORATINGROUTING
     INTO T_ALLOWPACKAGES, RATINGROUTING
     FROM RFP
    WHERE RFPID = I_RFPID AND ROUND_NUM = I_ROUND_NUM;

   UPDATE XMLLANEAWARD
      SET PACKAGEID =
             (SELECT PACKAGEID
                FROM PACKAGE P
               WHERE RFPID = I_RFPID
                     AND NVL (OBCARRIERCODEID, -1) =
                            CASE
                               WHEN OBCARRIERCODEID IS NULL THEN -1
                               ELSE XMLLANEAWARD.OBCARRIERCODEID
                            END
                     AND XMLLANEAWARD.PACKAGENAME = P.PACKAGENAME
                     AND P.ROUND_NUM <= I_ROUND_NUM)
    WHERE XMLLANEAWARD.RFPID = I_RFPID
          AND EXISTS
                 (SELECT PACKAGEID
                    FROM PACKAGE P
                   WHERE RFPID = I_RFPID
                         AND NVL (OBCARRIERCODEID, -1) =
                                CASE
                                   WHEN OBCARRIERCODEID IS NULL THEN -1
                                   ELSE XMLLANEAWARD.OBCARRIERCODEID
                                END
                         AND XMLLANEAWARD.PACKAGENAME = P.PACKAGENAME
                         AND P.ROUND_NUM <= I_ROUND_NUM);

   INSERT INTO XMLERROR (UPLOAD_TYPE_ID,
                         RFPID,
                         LANEID,
                         LANEEQUIPMENTTYPEID,
                         OBCARRIERCODEID,
                         PACKAGENAME,
                         ERRORDESC)
      SELECT C_UPLOAD_TYPE_ID,
             RFPID,
             LANEID,
             LANEEQUIPMENTTYPEID,
             OBCARRIERCODEID,
             PACKAGENAME,
             'bad package'
        FROM XMLLANEAWARD XLA
       WHERE     XLA.RFPID = I_RFPID
             AND NVL (LENGTH (PACKAGENAME), 0) > 0
             AND XLA.PACKAGEID = 0;

   INSERT INTO XMLERROR (UPLOAD_TYPE_ID,
                         RFPID,
                         LANEID,
                         LANEEQUIPMENTTYPEID,
                         OBCARRIERCODEID,
                         PACKAGENAME,
                         ERRORDESC)
      SELECT C_UPLOAD_TYPE_ID,
             RFPID,
             LANEID,
             LANEEQUIPMENTTYPEID,
             OBCARRIERCODEID,
             PACKAGENAME,
             'bad company'
        FROM XMLLANEAWARD XLA
       WHERE XLA.RFPID = I_RFPID
             AND XLA.PACKAGEID =
                    CASE T_ALLOWPACKAGES WHEN 1 THEN XLA.PACKAGEID ELSE 0 END
             AND XLA.OBCARRIERCODEID IS NULL;

   INSERT INTO XMLERROR (UPLOAD_TYPE_ID,
                         RFPID,
                         LANEID,
                         ERRORDESC)
      SELECT C_UPLOAD_TYPE_ID,
             RFPID,
             LANEID,
             'bad lane'
        FROM XMLLANEAWARD XLA
       WHERE XLA.RFPID = I_RFPID AND PACKAGEID = 0
             AND NOT EXISTS
                        (SELECT 1
                           FROM LANE L
                          WHERE     L.RFPID = XLA.RFPID
                                AND L.LANEID = XLA.LANEID
                                AND NVL (ISCANCELLED, 0) = 0
                                AND L.ROUND_NUM =
                                       (SELECT MAX (ML.ROUND_NUM)
                                          FROM LANE ML
                                         WHERE ML.RFPID = L.RFPID
                                               AND ML.LANEID = L.LANEID
                                               AND ML.ROUND_NUM <=
                                                      I_ROUND_NUM));

   INSERT INTO XMLERROR (UPLOAD_TYPE_ID,
                         RFPID,
                         LANEID,
                         ERRORDESC)
      SELECT C_UPLOAD_TYPE_ID,
             RFPID,
             LANEID,
             'bad line item'
        FROM XMLLANEAWARD XLA
       WHERE XLA.RFPID = I_RFPID AND PACKAGEID = 0
             AND NOT EXISTS
                        (SELECT 1
                           FROM LANEEQUIPMENTTYPE
                          WHERE     LANEEQUIPMENTTYPE.RFPID = XLA.RFPID
                                AND LANEEQUIPMENTTYPE.LANEID = XLA.LANEID
                                AND NVL (LANEEQUIPMENTTYPE.EQUIPMENTTYPE_ID,
                                         -1) = NVL (XLA.EQUIPMENTTYPE_ID, -1)
                                AND NVL (LANEEQUIPMENTTYPE.SERVICETYPE_ID,
                                         -1) = NVL (XLA.SERVICETYPE_ID, -1)
                                AND NVL (
                                       LANEEQUIPMENTTYPE.PROTECTIONLEVEL_ID,
                                       -1) = NVL (XLA.PROTECTIONLEVEL_ID, -1)
                                AND NVL (LANEEQUIPMENTTYPE.COMMODITYCODE_ID,
                                         -1) = NVL (XLA.COMMODITYCODE_ID, -1)
                                AND NVL (LANEEQUIPMENTTYPE.MOT, '|||') =
                                       NVL (XLA.MOT, '|||')
                                AND LANEEQUIPMENTTYPE.ROUND_NUM <=
                                       I_ROUND_NUM);

   INSERT INTO XMLERROR (UPLOAD_TYPE_ID,
                         RFPID,
                         LANEID,
                         ERRORDESC)
      SELECT C_UPLOAD_TYPE_ID,
             RFPID,
             LANEID,
             'bad lanebid'
        FROM XMLLANEAWARD XLA
       WHERE XLA.RFPID = I_RFPID AND PACKAGEID = 0
             AND NOT EXISTS
                        (SELECT 1
                           FROM SCENARIOADJUSTMENT SA, SCENARIO S
                          WHERE     SA.RFPID = XLA.RFPID
                                AND S.RFPID = SA.RFPID
                                AND S.SCENARIOID = SA.SCENARIOID
                                AND S.ROUND_NUM = I_ROUND_NUM
                                AND SA.LANEID = XLA.LANEID
                                AND SA.PACKAGEID = XLA.PACKAGEID
                                AND SA.OBCARRIERCODEID = XLA.OBCARRIERCODEID
                                AND NVL (SA.EQUIPMENTTYPE_ID, -1) =
                                       NVL (XLA.EQUIPMENTTYPE_ID, -1)
                                AND NVL (SA.SERVICETYPE_ID, -1) =
                                       NVL (XLA.SERVICETYPE_ID, -1)
                                AND NVL (SA.PROTECTIONLEVEL_ID, -1) =
                                       NVL (XLA.PROTECTIONLEVEL_ID, -1)
                                AND NVL (SA.COMMODITYCODE_ID, -1) =
                                       NVL (XLA.COMMODITYCODE_ID, -1)
                                AND NVL (SA.MOT, '|||') =
                                       NVL (XLA.MOT, '|||'));

   INSERT INTO XMLERROR (UPLOAD_TYPE_ID,
                         RFPID,
                         LANEID,
                         LANEEQUIPMENTTYPEID,
                         OBCARRIERCODEID,
                         ERRORDESC)
        SELECT C_UPLOAD_TYPE_ID,
               RFPID,
               LANEID,
               MIN (LANEEQUIPMENTTYPEID),
               OBCARRIERCODEID,
               'duplicate line item'
          FROM XMLLANEAWARD XLA
         WHERE XLA.RFPID = I_RFPID AND PACKAGEID = 0 AND HISTORICALAWARD = 0
      GROUP BY RFPID,
               LANEID,
               OBCARRIERCODEID,
               PACKAGEID,
               EQUIPMENTTYPE_ID,
               SERVICETYPE_ID,
               PROTECTIONLEVEL_ID,
               MOT,
               COMMODITYCODE_ID
        HAVING COUNT (*) > 1;

   INSERT INTO XMLERROR (UPLOAD_TYPE_ID,
                         RFPID,
                         LANEID,
                         LANEEQUIPMENTTYPEID,
                         OBCARRIERCODEID,
                         ERRORDESC)
      SELECT C_UPLOAD_TYPE_ID,
             RFPID,
             LANEID,
             LANEEQUIPMENTTYPEID,
             OBCARRIERCODEID,
             'missing lane id'
        FROM XMLLANEAWARD XLA
       WHERE     XLA.RFPID = I_RFPID
             AND LANEID IS NULL
             AND NVL (PACKAGEID, 0) = 0;

   IF T_ALLOWPACKAGES = 1
   THEN
      INSERT INTO XMLERROR (UPLOAD_TYPE_ID,
                            RFPID,
                            LANEID,
                            LANEEQUIPMENTTYPEID,
                            OBCARRIERCODEID,
                            PACKAGENAME,
                            ERRORDESC)
         SELECT C_UPLOAD_TYPE_ID,
                RFPID,
                LANEID,
                LANEEQUIPMENTTYPEID,
                OBCARRIERCODEID,
                PACKAGENAME,
                'bad package'
           FROM XMLLANEAWARD XLA
          WHERE XLA.RFPID = I_RFPID AND PACKAGEID > 0
                AND NOT EXISTS
                           (SELECT 1
                              FROM PACKAGE P
                             WHERE P.RFPID = I_RFPID
                                   AND P.PACKAGEID = XLA.PACKAGEID);
   END IF;

   IF RATINGROUTING = 1
   THEN
      INSERT INTO XMLERROR (UPLOAD_TYPE_ID,
                            RFPID,
                            LANEID,
                            LANEEQUIPMENTTYPEID,
                            OBCARRIERCODEID,
                            PACKAGENAME,
                            ERRORDESC)
         SELECT C_UPLOAD_TYPE_ID,
                XLA.RFPID,
                XLA.LANEID,
                XLA.LANEEQUIPMENTTYPEID,
                XLA.OBCARRIERCODEID,
                XLA.PACKAGENAME,
                'award more than carrier lane capacity'
           FROM XMLLANEAWARD XLA, LANEBID LB, RFP rfp
          WHERE     XLA.RFPID = I_RFPID
                AND LB.RFPID = XLA.RFPID
				And rfp.RFPID = Xla.Rfpid
				AND rfp.AWARDTYPE = 0
                AND LB.LANEID = XLA.LANEID
                AND LB.PACKAGEID = XLA.PACKAGEID
                AND LB.OBCARRIERCODEID = XLA.OBCARRIERCODEID
                AND LB.HISTORICALAWARD = XLA.HISTORICALAWARD
                AND NVL (LB.EQUIPMENTTYPE_ID, -1) =
                       NVL (XLA.EQUIPMENTTYPE_ID, -1)
                AND NVL (LB.SERVICETYPE_ID, -1) =
                       NVL (XLA.SERVICETYPE_ID, -1)
                AND NVL (LB.PROTECTIONLEVEL_ID, -1) =
                       NVL (XLA.PROTECTIONLEVEL_ID, -1)
                AND NVL (LB.COMMODITYCODE_ID, -1) =
                       NVL (XLA.COMMODITYCODE_ID, -1)
                AND NVL (LB.MOT, '|||') = NVL (XLA.MOT, '|||')
                AND LB.LANECAPACITY < NVL (XLA.AWARDAMOUNT, 0)
                AND LB.ROUND_NUM =
                       (SELECT MAX (MLB.ROUND_NUM)
                          FROM LANEBID MLB
                         WHERE     MLB.RFPID = LB.RFPID
                               AND MLB.LANEID = LB.LANEID
                               AND MLB.OBCARRIERCODEID = LB.OBCARRIERCODEID
                               AND MLB.LANEEQUIPMENTTYPEID =
                                      LB.LANEEQUIPMENTTYPEID
                               AND NVL (MLB.PACKAGEID, -1) =
                                      NVL (LB.PACKAGEID, -1)
                               AND MLB.ROUND_NUM <= I_ROUND_NUM
                               AND MLB.HISTORICALAWARD = LB.HISTORICALAWARD);
   END IF;

   SELECT COUNT (*) CNT
     INTO C_ERRORS
     FROM XMLERROR
    WHERE RFPID = I_RFPID AND UPLOAD_TYPE_ID = C_UPLOAD_TYPE_ID;

   -- if there were no errors

   IF C_ERRORS = 0
   THEN
      SELECT MAX (SCENARIOID) + 1
        INTO NEWSCEN
        FROM SCENARIO
       WHERE RFPID = I_RFPID;

      NEWSCEN := Getnextscenarioid (I_RFPID);

      INSERT INTO SCENARIO (RFPID,
                            SCENARIOID,
                            NAME,
                            DESCRIPTION,
                            ISFINAL,
                            STATUS,
                            ROUND_NUM,
                            ISBASESCENARIO)
           VALUES (I_RFPID,
                   NEWSCEN,
                   'Uploaded OptiBid Scenario',
                   'Uploaded OptiBid Scenario',
                   0,
                   'P',
                   I_ROUND_NUM,
                   0);

      Duplicatescenario (I_RFPID, BASESCENARIOID, NEWSCEN);

      UPDATE SCENARIOADJUSTMENT
         SET MANUALAWARDAMOUNT =
                (SELECT AWARDAMOUNT
                   FROM XMLLANEAWARD XLA
                  WHERE XLA.RFPID = SCENARIOADJUSTMENT.RFPID
                        AND XLA.LANEID = SCENARIOADJUSTMENT.LANEID
                        AND XLA.HISTORICALAWARD =
                               SCENARIOADJUSTMENT.HISTORICALAWARD
                        AND XLA.OBCARRIERCODEID =
                               SCENARIOADJUSTMENT.OBCARRIERCODEID
                        AND NVL (XLA.PACKAGEID, 0) =
                               NVL (SCENARIOADJUSTMENT.PACKAGEID, 0)
                        AND NVL (SCENARIOADJUSTMENT.EQUIPMENTTYPE_ID, -1) =
                               NVL (XLA.EQUIPMENTTYPE_ID, -1)
                        AND NVL (SCENARIOADJUSTMENT.SERVICETYPE_ID, -1) =
                               NVL (XLA.SERVICETYPE_ID, -1)
                        AND NVL (SCENARIOADJUSTMENT.PROTECTIONLEVEL_ID, -1) =
                               NVL (XLA.PROTECTIONLEVEL_ID, -1)
                        AND NVL (SCENARIOADJUSTMENT.COMMODITYCODE_ID, -1) =
                               NVL (XLA.COMMODITYCODE_ID, -1)
                        AND NVL (SCENARIOADJUSTMENT.MOT, '|||') =
                               NVL (XLA.MOT, '|||')),
             AUTOAWARDAMOUNT = 0,
             PACKAGEAWARDAMOUNT = 0,
             NEEDSAUTOAWARD = 1
       WHERE     SCENARIOADJUSTMENT.RFPID = I_RFPID
             AND PACKAGEID = 0
             AND SCENARIOID = NEWSCEN
             AND NOT EXISTS
                        (SELECT 1
                           FROM XMLERROR XE
                          WHERE XE.RFPID = SCENARIOADJUSTMENT.RFPID
                                AND XE.LANEID = SCENARIOADJUSTMENT.LANEID
                                AND XE.OBCARRIERCODEID =
                                       SCENARIOADJUSTMENT.OBCARRIERCODEID
                                AND XE.ERRORDESC = 'duplicate line item');

      UPDATE SCENARIOADJUSTMENT
         SET PACKAGEAWARDAMOUNT = 0
       WHERE RFPID = I_RFPID AND SCENARIOID = NEWSCEN AND PACKAGEID <> 0;

      UPDATE SCENARIOADJUSTMENT
         SET HISTORICALCOST =
                (SELECT HISTORICALCOST
                   FROM XMLHISTORICALCOST XHC
                  WHERE XHC.RFPID = SCENARIOADJUSTMENT.RFPID
                        AND XHC.LANEID = SCENARIOADJUSTMENT.LANEID)
       WHERE SCENARIOADJUSTMENT.RFPID = I_RFPID AND SCENARIOID = NEWSCEN
             AND EXISTS
                    (SELECT 1
                       FROM XMLHISTORICALCOST XHC
                      WHERE XHC.RFPID = SCENARIOADJUSTMENT.RFPID
                            AND XHC.LANEID = SCENARIOADJUSTMENT.LANEID);

      -- completed

      UPDATE LANE
         SET HISTORICALCOST =
                (SELECT HISTORICALCOST
                   FROM XMLHISTORICALCOST XHC
                  WHERE XHC.RFPID = LANE.RFPID AND XHC.LANEID = LANE.LANEID)
       WHERE LANE.RFPID = I_RFPID
             AND EXISTS
                    (SELECT 1
                       FROM XMLHISTORICALCOST XHC
                      WHERE XHC.RFPID = LANE.RFPID
                            AND XHC.LANEID = LANE.LANEID);

      IF T_ALLOWPACKAGES = 1
      THEN
         -- my for each

         FOR CURSOR_XMLLANEAWARD
            IN (SELECT OBCARRIERCODEID,
                       PACKAGEID,
                       PACKAGENAME,
                       HISTORICALAWARD
                  FROM XMLLANEAWARD XLA
                 WHERE     RFPID = I_RFPID
                       AND PACKAGEID > 0
                       AND OBCARRIERCODEID IS NOT NULL
                       AND NOT EXISTS
                              (SELECT 1
                                 FROM XMLERROR XE
                                WHERE XE.PACKAGEID = XLA.PACKAGEID))
         LOOP
            PV_FIELD1 := CURSOR_XMLLANEAWARD.OBCARRIERCODEID;
            PV_FIELD2 := CURSOR_XMLLANEAWARD.PACKAGEID;
            PV_FIELD3 := CURSOR_XMLLANEAWARD.PACKAGENAME;
            PV_FIELD4 := CURSOR_XMLLANEAWARD.HISTORICALAWARD;

            Awardpackage (PV_FIELD2,
                          NEWSCEN,
                          PV_FIELD1,
                          PV_FIELD4);
         END LOOP;
      END IF;
	
   	 CreateManualAwardConstraints(I_RFPID, I_ROUND_NUM, NEWSCEN);	
      Calculatescenariostats (I_RFPID, NEWSCEN);

      UPDATE SCENARIO
         SET STATUS = 'U'
       WHERE     RFPID = I_RFPID
             AND SCENARIOID = NEWSCEN
             AND ROUND_NUM = I_ROUND_NUM;
   END IF;
END Xmllaneawardupload;
/
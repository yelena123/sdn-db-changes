create or replace procedure manh_mhe_get_event_msg
(
    p_event_ids_csv         in  varchar2,
    p_retry_limit           in  event_message.nbr_of_retry%type,
    p_time_limit            in  number, -- minutes
    p_event_message_id      out event_message.event_message_id%type,
    p_event_id              out event_message.event_id%type,
    p_ek_wave_nbr           out event_message.ek_wave_nbr%type,
    p_ek_ilpn_nbr           out event_message.ek_ilpn_nbr%type,
    p_ek_olpn_nbr           out event_message.ek_olpn_nbr%type,
    p_ek_iplt_nbr           out event_message.ek_iplt_nbr%type,
    p_ek_oplt_nbr           out event_message.ek_oplt_nbr%type,
    p_ek_order_nbr          out event_message.ek_order_nbr%type,
    p_ek_item_id            out event_message.ek_item_id%type,
    p_ek_seal_nbr           out event_message.ek_seal_nbr%type,
    p_ek_shipment_nbr       out event_message.ek_shipment_nbr%type,
    p_ek_asn_nbr            out event_message.ek_asn_nbr%type,
    p_ek_trailer_nbr        out event_message.ek_trailer_nbr%type,
    p_ek_task_id            out event_message.ek_task_id%type,
    p_ek_pick_locn_id       out event_message.ek_pick_locn_id%type,
    p_ek_dest_locn_id       out event_message.ek_dest_locn_id%type,
    p_ek_pull_locn_id       out event_message.ek_pull_locn_id%type,
    p_whse                  out event_message.whse%type,
    p_user_id               out event_message.user_id%type
)
as
    v_select_all_event_ids number(1) := case when length(p_event_ids_csv) = 0
        then  1 else 0 end;
begin
    if (v_select_all_event_ids = 1)
    then
        select event_message_id, event_id, ek_wave_nbr, ek_ilpn_nbr, ek_olpn_nbr,
            ek_iplt_nbr, ek_oplt_nbr, ek_order_nbr, ek_item_id, ek_seal_nbr,
            ek_shipment_nbr, ek_asn_nbr, ek_trailer_nbr, ek_task_id,
            ek_pick_locn_id, ek_dest_locn_id, ek_pull_locn_id, whse, user_id
        into p_event_message_id, p_event_id, p_ek_wave_nbr, p_ek_ilpn_nbr,
            p_ek_olpn_nbr,  p_ek_iplt_nbr, p_ek_oplt_nbr, p_ek_order_nbr,
            p_ek_item_id, p_ek_seal_nbr, p_ek_shipment_nbr, p_ek_asn_nbr,
            p_ek_trailer_nbr, p_ek_task_id, p_ek_pick_locn_id, p_ek_dest_locn_id,
            p_ek_pull_locn_id, p_whse, p_user_id
        from event_message em
        join
        (
            select min(em1.event_message_id) keep(dense_rank first
                order by em1.stat_code, em1.event_message_id) first_event_message_id
            from event_message em1
            where em1.stat_code between 20 and 22
        )iv on iv.first_event_message_id = em.event_message_id
        for update of em.stat_code;
    else
        select /*+ ordered */ event_message_id, event_id, ek_wave_nbr, ek_ilpn_nbr, ek_olpn_nbr,
            ek_iplt_nbr, ek_oplt_nbr, ek_order_nbr, ek_item_id, ek_seal_nbr,
            ek_shipment_nbr, ek_asn_nbr, ek_trailer_nbr, ek_task_id,
            ek_pick_locn_id, ek_dest_locn_id, ek_pull_locn_id, whse, user_id
        into p_event_message_id, p_event_id, p_ek_wave_nbr, p_ek_ilpn_nbr,
            p_ek_olpn_nbr,  p_ek_iplt_nbr, p_ek_oplt_nbr, p_ek_order_nbr,
            p_ek_item_id, p_ek_seal_nbr, p_ek_shipment_nbr, p_ek_asn_nbr,
            p_ek_trailer_nbr, p_ek_task_id, p_ek_pick_locn_id, p_ek_dest_locn_id,
            p_ek_pull_locn_id, p_whse, p_user_id
        from
        (
            with giv as
            (
                select /*+ dynamic_sampling(t, 2) */ t.column_value event_id
                from table(cl_parse_endpoint_name_csv(p_event_ids_csv)) t
            )
            select /*+ ordered use_nl(giv em1) index(em1 PE_EVNT_IDX) */ min(em1.event_message_id) keep(dense_rank first
                order by em1.stat_code, em1.event_message_id) first_event_message_id
            from giv
            join event_message em1 on em1.event_id = giv.event_id
            where em1.stat_code between 20 and 22
        ) iv
        join event_message em on em.event_message_id = iv.first_event_message_id
        for update of em.stat_code;
    end if;

    update event_message em
    set em.stat_code = 40
    where em.event_message_id = p_event_message_id;

    commit;
exception
    when no_data_found then
        begin
            if (v_select_all_event_ids = 1)
            then
                select event_message_id, event_id, ek_wave_nbr, ek_ilpn_nbr,
                    ek_olpn_nbr, ek_iplt_nbr, ek_oplt_nbr, ek_order_nbr, ek_item_id,
                    ek_seal_nbr, ek_shipment_nbr, ek_asn_nbr, ek_trailer_nbr,
                    ek_task_id, ek_pick_locn_id, ek_dest_locn_id, ek_pull_locn_id,
                    whse, user_id
                into p_event_message_id, p_event_id, p_ek_wave_nbr, p_ek_ilpn_nbr,
                    p_ek_olpn_nbr,  p_ek_iplt_nbr, p_ek_oplt_nbr, p_ek_order_nbr,
                    p_ek_item_id, p_ek_seal_nbr, p_ek_shipment_nbr, p_ek_asn_nbr,
                    p_ek_trailer_nbr, p_ek_task_id, p_ek_pick_locn_id,
                    p_ek_dest_locn_id, p_ek_pull_locn_id, p_whse, p_user_id
                from event_message em
                join
                    (
                        select min(em1.event_message_id) keep(dense_rank first
                            order by em1.event_message_id) first_event_message_id
                        from event_message em1
                        where em1.stat_code = 96
                            and em1.nbr_of_retry  < p_retry_limit
                            and em1.mod_date_time >= sysdate - p_time_limit/(24 * 60)
                    ) iv on iv.first_event_message_id = em.event_message_id
                for update of em.stat_code;
            else
                select event_message_id, event_id, ek_wave_nbr, ek_ilpn_nbr,
                    ek_olpn_nbr, ek_iplt_nbr, ek_oplt_nbr, ek_order_nbr, ek_item_id,
                    ek_seal_nbr, ek_shipment_nbr, ek_asn_nbr, ek_trailer_nbr,
                    ek_task_id, ek_pick_locn_id, ek_dest_locn_id, ek_pull_locn_id,
                    whse, user_id
                into p_event_message_id, p_event_id, p_ek_wave_nbr, p_ek_ilpn_nbr,
                    p_ek_olpn_nbr,  p_ek_iplt_nbr, p_ek_oplt_nbr, p_ek_order_nbr,
                    p_ek_item_id, p_ek_seal_nbr, p_ek_shipment_nbr, p_ek_asn_nbr,
                    p_ek_trailer_nbr, p_ek_task_id, p_ek_pick_locn_id,
                    p_ek_dest_locn_id, p_ek_pull_locn_id, p_whse, p_user_id
                from event_message em
                join
                (
                    select min(em1.event_message_id) keep(dense_rank first
                        order by em1.event_message_id) first_event_message_id
                    from event_message em1
                    where em1.stat_code = 96
                        and em1.nbr_of_retry  < p_retry_limit
                        and em1.mod_date_time >= sysdate - p_time_limit/(24 * 60)
                        and exists
                        (
                            select 1
                            from table(cl_parse_endpoint_name_csv(p_event_ids_csv)) t
                            where t.column_value = em1.event_id
                        )
                ) iv on iv.first_event_message_id = em.event_message_id
                for update of em.stat_code;
            end if;

            update event_message em
            set em.stat_code = 40
            where em.event_message_id = p_event_message_id;

            commit;
        exception
            when no_data_found then
                p_event_message_id := -1;
                return;
        end;
end;
/
show errors;

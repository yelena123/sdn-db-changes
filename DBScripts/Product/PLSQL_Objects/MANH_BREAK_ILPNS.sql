CREATE OR REPLACE PROCEDURE MANH_BREAK_ILPNS (
   p_tc_lpn_id          IN LPN.TC_LPN_ID%TYPE,
   p_tc_company_id      IN DISPOSITION_TYPE.TC_COMPANY_ID%TYPE,
   p_login_user_id      IN VARCHAR2,
   p_disposition_type   IN DISPOSITION_TYPE.DISPOSITION_TYPE%TYPE,
   p_next_up_number     IN VARCHAR2,
   p_debug_flag         IN NUMBER,
   p_whse               IN DISPOSITION_TYPE.WHSE%TYPE,
   p_facility_id        in facility.facility_id%type
)
IS
   stmt                         VARCHAR2 (1000);
   v_error_details              VARCHAR2 (2000);
   v_lpn_count                  NUMBER;
   v_remaining_qty              NUMBER;
   v_lpn_qty                    NUMBER;
   v_ilpn_id                    LPN.LPN_ID%TYPE;
   v_lpn_detail_id              LPN_DETAIL.LPN_DETAIL_ID%TYPE;
   v_tc_ilpn_id                 LPN.TC_LPN_ID%TYPE;
   v_item_id                    lpn_detail.item_id%type;
   v_num_ilpns_created          number(9);
   v_allow_shippable_inners     WHSE_PARAMETERS.ALLOW_SHIPPABLE_INNERS%TYPE;
   v_process_shippable_inners   DISPOSITION_TYPE.PROCESS_SHIPPABLE_INNERS%TYPE;
   v_break_lpn                  DISPOSITION_TYPE.BREAK_LPN_FLAG%TYPE;
   v_disp_method                DISPOSITION_TYPE.DISPOSITION_METHOD%TYPE;
   v_alloc_uom                  VARCHAR2 (10);
   v_uom_qty                    NUMBER;
   v_pfx_field                  nxt_up_cnt.pfx_field%TYPE;
   v_start_tc_lpn_id            NUMBER (10) := 0;
   v_nxt_up_curr_nbr            number(10) := 0;
   v_nxt_up_nbr_len             NUMBER (10) := 0;
   v_wm_invn_id                 WM_INVENTORY.WM_INVENTORY_ID%TYPE;
   v_unit_weight                ITEM_CBO.UNIT_WEIGHT%TYPE;
   v_unit_volume                ITEM_CBO.UNIT_VOLUME%TYPE;
   
   CURSOR CURSOR_LPN_DETAIL
   IS
      SELECT ---------------------------------LPN RECORDS----------------------------------------
            L.TC_LPN_ID AS TC_LPN_ID,
             L.LPN_ID AS LPN_ID,
             L.TC_COMPANY_ID,
             L.LPN_MONETARY_VALUE,
             L.C_FACILITY_ID,
             L.O_FACILITY_ID,
             L.O_FACILITY_ALIAS_ID,
             L.MANIFEST_NBR,
             L.SHIP_VIA,
             L.MASTER_BOL_NBR,
             L.BOL_NBR,
             L.INIT_SHIP_VIA,
             L.PATH_ID,
             L.REPRINT_COUNT,
             L.FREIGHT_CHARGE,
             L.LENGTH,
             L.WIDTH,
             L.HEIGHT,
             L.TC_SHIPMENT_ID,
             L.SHIPMENT_ID,
             L.TOTAL_LPN_QTY,
             L.ESTIMATED_VOLUME,
             L.TC_ASN_ID,
             coalesce(L.ASN_ID, A.ASN_ID) as ASN_ID,
             L.TC_PURCHASE_ORDERS_ID as LPN_TC_PURCHASE_ORDERS_ID,
             L.PURCHASE_ORDERS_ID as LPN_PURCHASE_ORDERS_ID,
             L.WEIGHT,
             L.ACTUAL_VOLUME,
             L.LPN_LABEL_TYPE,
             L.HIBERNATE_VERSION,
             L.CREATED_SOURCE,
             L.C_FACILITY_ALIAS_ID,
             L.PICK_SUB_LOCN_ID,
             L.INTERNAL_ORDER_ID,
             L.TRAILER_STOP_SEQ_NBR,
             L.WAVE_NBR,
             L.WAVE_SEQ_NBR,
             L.WAVE_STAT_CODE,
             coalesce(L.STAGE_INDICATOR, 0) STAGE_INDICATOR,
             L.PICK_DELIVERY_DURATION,
             L.LPN_BREAK_ATTR,
             L.SEQ_RULE_PRIORITY,
             L.NBR_OF_ZONES,
             L.PALLET_X_OF_Y,
             L.LPN_NBR_X_OF_Y,
             L.LOAD_SEQUENCE,
             L.SELECTION_RULE_ID,
             L.SINGLE_LINE_LPN,
             L.LPN_FACILITY_STAT_UPDATED_DTTM,
             L.ESTIMATED_WEIGHT,
             L.D_FACILITY_ID,
             L.D_FACILITY_ALIAS_ID,
             L.CUBE_UOM,
             L.WEIGHT_UOM_ID_BASE,
             L.VOLUME_UOM_ID_BASE,
             L.DISPOSITION_TYPE,
             L.VOCO_INTRN_REVERSE_ID,
             L.CONTAINER_TYPE,
             L.PLANNED_TC_ASN_ID,
             L.PHYSICAL_ENTITY_CODE,
             L.PROCESS_IMMEDIATE_NEEDS,             
             L.RCVD_DTTM,
             L.MANUFACTURED_DTTM,
             L.SHIP_BY_DATE,
             L.EXPIRATION_DATE,
             L.LPN_CREATION_CODE,
             L.PACKER_USERID,
             L.TC_ORDER_ID,
             L.ORDER_ID,
             L.RECEIVED_TC_SHIPMENT_ID,
             ---------------------------------LPN_DETAIL RECORDS----------------------------------
             LD.LPN_DETAIL_ID AS LPN_DETAIL_ID,
             LD.ITEM_ID AS ITEM_ID,
             LD.BUSINESS_PARTNER_ID,
             LD.GTIN,
             LD.INCUBATION_DATE,
             LD.SELL_BY_DTTM,
             LD.CONSUMPTION_PRIORITY_DTTM,
             LD.CNTRY_OF_ORGN,
             LD.INVENTORY_TYPE,
             LD.PRODUCT_STATUS,
             LD.ITEM_ATTR_1,
             LD.ITEM_ATTR_2,
             LD.ITEM_ATTR_3,
             LD.ITEM_ATTR_4,
             LD.ITEM_ATTR_5,
             LD.ITEM_NAME,
             LD.ASN_DTL_ID,
             LD.ASSORT_NBR,
             LD.CUT_NBR,
             LD.PURCHASE_ORDERS_ID,
             LD.TC_PURCHASE_ORDERS_ID,
             LD.PURCHASE_ORDERS_LINE_ID,
             LD.TC_PURCHASE_ORDERS_LINE_ID,
             LD.INSTRTN_CODE_1,
             LD.INSTRTN_CODE_2,
             LD.INSTRTN_CODE_3,
             LD.INSTRTN_CODE_4,
             LD.INSTRTN_CODE_5,
             LD.VENDOR_ITEM_NBR,
             LD.MANUFACTURED_PLANT,
             LD.BATCH_NBR,
             LD.ASSIGNED_QTY,
             LD.PREPACK_GROUP_CODE,
             LD.PACK_CODE,
             LD.STD_PACK_QTY,
             LD.STD_SUB_PACK_QTY,
             LD.STD_BUNDLE_QTY,
             LD.VOLUME_UOM_ID,
             LD.WEIGHT_UOM_ID,
             LD.QTY_UOM_ID,
             LD.SIZE_VALUE,
             LD.QTY_CONV_FACTOR,
             LD.QTY_UOM_ID_BASE,
             LD.INTERNAL_ORDER_DTL_ID,
             LD.TC_ORDER_LINE_ID,
             LD.LPN_DETAIL_STATUS,
             LD.DISTRIBUTION_ORDER_DTL_ID,             
             ---------------------------------WM_INVENTORY_RECORDS------------------------
             WM.TRANSITIONAL_INVENTORY_TYPE,
             WM.LAST_UPDATED_SOURCE_TYPE,
             WM.TO_BE_FILLED_QTY,
             WM.WM_VERSION_ID,
             WM.CREATED_SOURCE_TYPE,
             WM.TO_BE_CONSOLIDATED_QTY,
             WM.LOCATION_DTL_ID,
             WM.PACK_QTY,
             WM.SUB_PACK_QTY,
             WM.LOCN_CLASS,
             WM.ALLOCATABLE,
             AD.STD_PACK_QTY AS AD_STD_PACK_QTY,
             AD.STD_SUB_PACK_QTY AS AD_STD_SUB_PACK_QTY,
             AD.STD_CASE_QTY
        FROM LPN_DETAIL LD
             INNER JOIN LPN L
                ON     LD.LPN_ID = L.LPN_ID
                   AND L.TC_LPN_ID = p_tc_lpn_id
                   AND LD.TC_COMPANY_ID = p_tc_company_id 
                           AND L.INBOUND_OUTBOUND_INDICATOR='I' 
                   and l.c_facility_id = p_facility_id
             LEFT OUTER JOIN WM_INVENTORY WM
                ON     WM.LPN_ID = LD.LPN_ID
                   AND WM.LPN_DETAIL_ID = LD.LPN_DETAIL_ID
                   AND WM.TC_COMPANY_ID = LD.TC_COMPANY_ID
                   AND WM.MARKED_FOR_DELETE = 'N'
             LEFT OUTER JOIN ASN A
              ON     A.TC_ASN_ID = L.TC_ASN_ID                     
                 AND A.TC_COMPANY_ID = LD.TC_COMPANY_ID
            LEFT OUTER JOIN ASN_DETAIL AD
              ON     AD.ASN_DETAIL_ID = LD.ASN_DTL_ID 
                AND AD.ASN_ID = A.ASN_ID 
                AND AD.SKU_ID = LD.ITEM_ID                
                and ad.tc_company_id = ld.tc_company_id
        where ld.size_value > 0;         
BEGIN
    
   wm_log (
         'BREAK LPN : TC_LPN_ID: '
      || p_tc_lpn_id
      || ' :: '
      || 'beginning breaking',
      1,
      'INVMGMT');

   SELECT CASE
             WHEN EXISTS
                     (SELECT '1'
                        FROM WHSE_PARAMETERS WP, WHSE_MASTER W
                       WHERE     W.WHSE = p_whse
                             AND WP.WHSE_MASTER_ID = W.WHSE_MASTER_ID
                             AND WP.ALLOW_SHIPPABLE_INNERS = '1')
             THEN
                '1'
             ELSE
                '0'
          END
     INTO v_allow_shippable_inners
     FROM DUAL;

   wm_log (
         'BREAK LPN : TC_LPN_ID: '
      || p_tc_lpn_id
      || ' :: '
      || 'v_allow_shippable_inners:'
      || v_allow_shippable_inners,
      1,
      'INVMGMT');
   wm_log (
         'BREAK LPN : TC_LPN_ID: '
      || p_tc_lpn_id
      || ' :: '
      || 'Starting the Breaking of LPNs.....',
      1,
      'INVMGMT');

   SELECT BREAK_LPN_FLAG,
          PROCESS_SHIPPABLE_INNERS,
          DISPOSITION_METHOD
     INTO v_break_lpn,
          v_process_shippable_inners,
          v_disp_method
     FROM DISPOSITION_TYPE
    WHERE     DISPOSITION_TYPE = p_disposition_type
          AND WHSE = p_whse
          AND TC_COMPANY_ID = p_tc_company_id;

   wm_log (
         'BREAK LPN : TC_LPN_ID: '
      || p_tc_lpn_id
      || ' :: '
      || 'Configuration.....BREAK_LPN_FLAG-->'
      || v_break_lpn      
      || 'PROCESS_SHIPPABLE_INNERS-->'
      || v_process_shippable_inners,
      1,
      'INVMGMT');

      
   FOR lpn_detail_rec IN CURSOR_LPN_DETAIL
   LOOP
      wm_log (
            'BREAK LPN : TC_LPN_ID: '
         || p_tc_lpn_id
         || ' :: '
         || 'Processing lpn_detail:'
         || lpn_detail_rec.lpn_detail_id,
         1,
         'INVMGMT');
      v_item_id := lpn_detail_rec.ITEM_ID;
      wm_log (
            'BREAK LPN : TC_LPN_ID: '
         || p_tc_lpn_id
         || ' :: '
         || 'item id for lpn_detail:'
         || v_item_id,
         1,
         'INVMGMT');

      -- if shippable inner is allowed then fetch the quantity from the item table
      IF v_break_lpn = '1'
         AND (v_allow_shippable_inners = '1'
              OR v_allow_shippable_inners = '2')
         AND v_process_shippable_inners = '1'
      THEN
        v_uom_qty := 0;
         --WM-27145:Use allocation UOM if minimum shippable inner uom is not defined on the item    
         SELECT COALESCE (ic.min_ship_inner_uom_id, (SELECT du.allocation_uom_id              
              FROM disposition_type du
             WHERE du.disposition_type = p_disposition_type 
                   AND du.whse=p_whse AND du.tc_company_id  = p_tc_company_id))
           INTO v_alloc_uom
           FROM item_cbo ic
          WHERE ic.item_id = v_item_id;
                   
      --if shippable inner is not allowed then get the value from lpn_detail
      ELSIF v_break_lpn = '1'
      THEN      
      v_uom_qty := 0;
         SELECT du.allocation_uom_id
              INTO v_alloc_uom
              FROM disposition_type du
             WHERE du.disposition_type = p_disposition_type 
               AND du.whse=p_whse AND du.tc_company_id  = p_tc_company_id;
      END IF;  
      
      IF v_alloc_uom = 'K'
            THEN
                v_uom_qty := COALESCE(lpn_detail_rec.STD_PACK_QTY, lpn_detail_rec.AD_STD_PACK_QTY, 0);            
        ELSIF v_alloc_uom = 'S'
            THEN
                v_uom_qty := COALESCE(lpn_detail_rec.STD_SUB_PACK_QTY, lpn_detail_rec.AD_STD_SUB_PACK_QTY, 0);            
        ELSIF v_alloc_uom = 'C'
            THEN
                v_uom_qty := COALESCE(lpn_detail_rec.STD_CASE_QTY, 0);
        END IF;
        
        IF v_uom_qty = 0
         then
         SELECT COALESCE(nullif(ipc.quantity, 0), 1)
           INTO v_uom_qty 
           FROM item_cbo ic, item_package_cbo ipc, size_uom su,STANDARD_UOM suom
          WHERE     ic.item_id = v_item_id
                AND ic.item_id = ipc.item_id
                AND su.STANDARD_UOM = suom.STANDARD_UOM
                AND suom.ABBREVIATION = v_alloc_uom
                AND su.size_uom_id = ipc.package_uom_id
                AND ipc.mark_for_deletion = 0
                AND ipc.is_std = 1
                AND ROWNUM < 2;
            END IF;    
            
      wm_log (
            'BREAK LPN : TC_LPN_ID: '
         || p_tc_lpn_id
         || ' :: '
         || 'Uom Qty'
         || v_uom_qty,
         1,
         'INVMGMT');
      v_lpn_qty := lpn_detail_rec.size_value;
      v_remaining_qty := MOD (v_lpn_qty, v_uom_qty);
      v_lpn_count := floor (v_lpn_qty / v_uom_qty);
      wm_log (
            'BREAK LPN : TC_LPN_ID: '
         || p_tc_lpn_id
         || ' :: '
         || 'Total Child LPNs to be created with Uom Qty is : '
         || v_lpn_count
         || ' Toal non-standard quantity for which another LPN is created is :'
         || v_remaining_qty,
         1,
         'INVMGMT');

      if (v_lpn_count > 0 or v_remaining_qty > 0)
      then
          wm_gen_lpn_nbrs (p_login_user_id, p_whse, lpn_detail_rec.tc_company_id, 
              lpn_detail_rec.o_facility_id, p_next_up_number, 
              v_lpn_count + case when v_remaining_qty > 0 then 1 else 0 end, v_pfx_field, 
              v_start_tc_lpn_id, v_nxt_up_nbr_len);
          v_nxt_up_curr_nbr := v_start_tc_lpn_id - 1;
      end if;
      v_num_ilpns_created := 0;
      --------------------------------------------------CREATE LPNs and LPN Details for the LPN count calculated above--------------------------------------------------
      WHILE v_num_ilpns_created < v_lpn_count
      LOOP
         ----------------CREATE iLPN-------------
         select lpn_id_seq.nextval into v_ilpn_id from dual;
         v_nxt_up_curr_nbr := v_nxt_up_curr_nbr + 1;
         v_tc_ilpn_id :=
            COALESCE (v_pfx_field, '')
            || LPAD (TO_CHAR (v_nxt_up_curr_nbr), v_nxt_up_nbr_len, '0');

         /*SELECT UNIT_VOLUME,UNIT_WEIGHT INTO v_unit_volume,v_unit_weight FROM ITEM_CBO  WHERE ITEM_ID = lpn_detail_rec.ITEM_ID AND COMPANY_ID= p_tc_company_id;*/

         INSERT INTO lpn (lpn_id,
                          tc_lpn_id,
                          tc_company_id,
                          lpn_type,
                          lpn_monetary_value,
                          c_facility_id,
                          o_facility_id,
                          o_facility_alias_id,
                          lpn_status,
                          lpn_facility_status,
                          tc_reference_lpn_id,                
                          tc_asn_id,
                          asn_id,
                          tc_purchase_orders_id,
                          purchase_orders_id,
                          manifest_nbr,
                          ship_via,
                          master_bol_nbr,
                          bol_nbr,
                          init_ship_via,
                          path_id,
                          reprint_count,
                          freight_charge,
                          LENGTH,
                          width,
                          height,
                          tc_shipment_id,
                          shipment_id,
                          total_lpn_qty,
                          estimated_volume,
                          weight,
                          actual_volume,
                          lpn_label_type,
                          hibernate_version,
                          created_source,
                          created_dttm,
                          c_facility_alias_id,
                          pick_sub_locn_id,
                          inbound_outbound_indicator,
                          internal_order_id,
                          trailer_stop_seq_nbr,
                          wave_nbr,
                          wave_seq_nbr,
                          wave_stat_code,
                          transitional_inventory_type,
                          stage_indicator,
                          pick_delivery_duration,
                          lpn_break_attr,
                          seq_rule_priority,
                          nbr_of_zones,
                          pallet_x_of_y,
                          lpn_nbr_x_of_y,
                          load_sequence,
                          selection_rule_id,
                          single_line_lpn,
                          item_id,
                          lpn_facility_stat_updated_dttm,
                          order_id,
                          estimated_weight,
                          d_facility_id,
                          d_facility_alias_id,
                          lpn_creation_code,
                          cube_uom,
                          qty_uom_id_base,
                          weight_uom_id_base,
                          volume_uom_id_base,
                          disposition_type,
                          voco_intrn_reverse_id,
                          CONTAINER_TYPE,
                          PLANNED_TC_ASN_ID,
                          PHYSICAL_ENTITY_CODE,
                          PROCESS_IMMEDIATE_NEEDS,
                          ITEM_NAME,
                          RCVD_DTTM ,
                          PACKER_USERID,
                          BUSINESS_PARTNER_ID,
                          TC_ORDER_ID,
                          RECEIVED_TC_SHIPMENT_ID)
            SELECT v_ilpn_id,
                   v_tc_ilpn_id,
                   lpn_detail_rec.tc_company_id,
                   1,
                   lpn_detail_rec.lpn_monetary_value,
                   lpn_detail_rec.c_facility_id,
                   lpn_detail_rec.o_facility_id,
                   lpn_detail_rec.o_facility_alias_id,
                   50,
                   10,
                   p_tc_lpn_id,                 
                   lpn_detail_rec.tc_asn_id,
                   lpn_detail_rec.asn_id,
                   lpn_detail_rec.lpn_tc_purchase_orders_id,
                   lpn_detail_rec.lpn_purchase_orders_id,
                   lpn_detail_rec.manifest_nbr,
                   lpn_detail_rec.ship_via,
                   lpn_detail_rec.master_bol_nbr,
                   lpn_detail_rec.bol_nbr,
                   lpn_detail_rec.init_ship_via,
                   lpn_detail_rec.path_id,
                   lpn_detail_rec.reprint_count,
                   lpn_detail_rec.freight_charge,
                   lpn_detail_rec.LENGTH,
                   lpn_detail_rec.width,
                   lpn_detail_rec.height,
                   lpn_detail_rec.tc_shipment_id,
                   lpn_detail_rec.shipment_id,
                   v_uom_qty,
                   (
                   Case 
                    When (lpn_detail_rec.Estimated_volume Is NULL) 
                          Then NULL
                          Else (lpn_detail_rec.Estimated_volume/lpn_detail_rec.size_value) * v_uom_qty
                  end),
                   (
                   Case 
                    When (lpn_detail_rec.weight Is NULL) 
                          Then NULL
                          Else (lpn_detail_rec.weight/lpn_detail_rec.size_value) * v_uom_qty
                  end),
                   (
                   Case 
                    When (lpn_detail_rec.actual_volume Is NULL) 
                          Then NULL
                          Else (lpn_detail_rec.actual_volume/lpn_detail_rec.size_value) * v_uom_qty
                  end),
                   lpn_detail_rec.lpn_label_type,
                   lpn_detail_rec.hibernate_version,
                   lpn_detail_rec.created_source,
                   SYSTIMESTAMP,
                   lpn_detail_rec.c_facility_alias_id,
                   lpn_detail_rec.pick_sub_locn_id,
                   'I',
                   lpn_detail_rec.internal_order_id,
                   lpn_detail_rec.trailer_stop_seq_nbr,
                   lpn_detail_rec.wave_nbr,
                   lpn_detail_rec.wave_seq_nbr,
                   lpn_detail_rec.wave_stat_code,
                   lpn_detail_rec.transitional_inventory_type,
                   lpn_detail_rec.stage_indicator,
                   lpn_detail_rec.pick_delivery_duration,
                   lpn_detail_rec.lpn_break_attr,
                   lpn_detail_rec.seq_rule_priority,
                   lpn_detail_rec.nbr_of_zones,
                   lpn_detail_rec.pallet_x_of_y,
                   lpn_detail_rec.lpn_nbr_x_of_y,
                   lpn_detail_rec.load_sequence,
                   lpn_detail_rec.selection_rule_id,
                   lpn_detail_rec.single_line_lpn,
                   lpn_detail_rec.item_id,
                   lpn_detail_rec.lpn_facility_stat_updated_dttm,
                   NULL,
                    (
                   Case 
                    When (lpn_detail_rec.Estimated_Weight Is NULL) 
                          Then NULL
                          Else (lpn_detail_rec.Estimated_Weight/lpn_detail_rec.size_value) * v_uom_qty
                  end),
                   lpn_detail_rec.d_facility_id,
                   lpn_detail_rec.d_facility_alias_id,
                   lpn_detail_rec.lpn_creation_code,
                   lpn_detail_rec.cube_uom,
                   lpn_detail_rec.qty_uom_id_base,
                   lpn_detail_rec.weight_uom_id_base,
                   lpn_detail_rec.volume_uom_id_base,
                   lpn_detail_rec.disposition_type,
                   lpn_detail_rec.voco_intrn_reverse_id,
                   lpn_detail_rec.CONTAINER_TYPE,
                   lpn_detail_rec.PLANNED_TC_ASN_ID,
                   lpn_detail_rec.PHYSICAL_ENTITY_CODE,
                   lpn_detail_rec.PROCESS_IMMEDIATE_NEEDS,
                   lpn_detail_rec.ITEM_NAME,
                   lpn_detail_rec.RCVD_DTTM,
                   lpn_detail_rec.PACKER_USERID,
                   lpn_detail_rec.BUSINESS_PARTNER_ID,
                   NULL,
                   lpn_detail_rec.RECEIVED_TC_SHIPMENT_ID
              FROM DUAL;

         wm_log (
               'BREAK LPN : TC_LPN_ID: '
            || p_tc_lpn_id
            || ' :: '
            || v_tc_ilpn_id,
            1,
            'INVMGMT');

         SELECT lpn_detail_id_seq.NEXTVAL INTO v_lpn_detail_id FROM DUAL;

         INSERT INTO lpn_detail (tc_company_id,
                                 lpn_id,
                                 lpn_detail_id,
                                 lpn_detail_status,
                                 distribution_order_dtl_id,
                                 shipped_qty,
                                 received_qty,
                                 initial_qty,
                                 business_partner_id,
                                 item_id,
                                 gtin,
                                 std_pack_qty,
                                 std_sub_pack_qty,
                                 std_bundle_qty,
                                 incubation_date,
                                 expiration_date,
                                 ship_by_date,
                                 sell_by_dttm,
                                 consumption_priority_dttm,
                                 manufactured_dttm,
                                 cntry_of_orgn,
                                 inventory_type,
                                 product_status,
                                 item_attr_1,
                                 item_attr_2,
                                 item_attr_3,
                                 item_attr_4,
                                 item_attr_5,
                                 asn_dtl_id,
                                 size_value,
                                 qty_uom_id,
                                 weight_uom_id,
                                 volume_uom_id,
                                 assort_nbr,
                                 cut_nbr,
                                 purchase_orders_id,
                                 tc_purchase_orders_id,
                                 purchase_orders_line_id,
                                 tc_purchase_orders_line_id,
                                 hibernate_version,
                                 instrtn_code_1,
                                 instrtn_code_2,
                                 instrtn_code_3,
                                 instrtn_code_4,
                                 instrtn_code_5,
                                 created_source_type,
                                 created_source,
                                 created_dttm,
                                 last_updated_source_type,
                                 last_updated_source,
                                 last_updated_dttm,
                                 vendor_item_nbr,
                                 manufactured_plant,
                                 batch_nbr,
                                 assigned_qty,
                                 prepack_group_code,
                                 pack_code,
                                 QTY_CONV_FACTOR,
                                 ITEM_NAME,
                                 INTERNAL_ORDER_DTL_ID,
                                 TC_ORDER_LINE_ID,
                                 QTY_UOM_ID_BASE)
            SELECT lpn_detail_rec.tc_company_id,
                   v_ilpn_id,
                   v_lpn_detail_id,
                   lpn_detail_rec.LPN_DETAIL_STATUS,
                   NULL,
                   0,
                   v_uom_qty AS received_qty,
                   v_uom_qty AS initial_qty,
                   lpn_detail_rec.business_partner_id,
                   v_item_id,
                   lpn_detail_rec.gtin,
                   lpn_detail_rec.std_pack_qty,
                   lpn_detail_rec.std_sub_pack_qty,
                   lpn_detail_rec.std_bundle_qty,
                   lpn_detail_rec.incubation_date,
                   lpn_detail_rec.expiration_date,
                   lpn_detail_rec.ship_by_date,
                   lpn_detail_rec.sell_by_dttm,
                   lpn_detail_rec.consumption_priority_dttm,
                   lpn_detail_rec.manufactured_dttm,
                   lpn_detail_rec.cntry_of_orgn,
                   lpn_detail_rec.inventory_type,
                   lpn_detail_rec.product_status,
                   lpn_detail_rec.item_attr_1,
                   lpn_detail_rec.item_attr_2,
                   lpn_detail_rec.item_attr_3,
                   lpn_detail_rec.item_attr_4,
                   lpn_detail_rec.item_attr_5,
                   lpn_detail_rec.asn_dtl_id,
                   v_uom_qty AS size_value,
                   lpn_detail_rec.qty_uom_id,
                   lpn_detail_rec.weight_uom_id,
                   lpn_detail_rec.volume_uom_id,
                   lpn_detail_rec.assort_nbr,
                   lpn_detail_rec.cut_nbr,
                   lpn_detail_rec.purchase_orders_id,
                   lpn_detail_rec.tc_purchase_orders_id,
                   lpn_detail_rec.purchase_orders_line_id,
                   lpn_detail_rec.tc_purchase_orders_line_id,
                   lpn_detail_rec.hibernate_version,
                   lpn_detail_rec.instrtn_code_1,
                   lpn_detail_rec.instrtn_code_2,
                   lpn_detail_rec.instrtn_code_3,
                   lpn_detail_rec.instrtn_code_4,
                   lpn_detail_rec.instrtn_code_5,
                   5,
                   p_login_user_id,
                   SYSTIMESTAMP,
                   5,
                   p_login_user_id,
                   SYSTIMESTAMP,
                   lpn_detail_rec.vendor_item_nbr,
                   lpn_detail_rec.manufactured_plant,
                   lpn_detail_rec.batch_nbr,
                   lpn_detail_rec.assigned_qty,
                   lpn_detail_rec.prepack_group_code,
                   lpn_detail_rec.pack_code,
                   lpn_detail_rec.QTY_CONV_FACTOR,
                   lpn_detail_rec.ITEM_NAME,
                   lpn_detail_rec.INTERNAL_ORDER_DTL_ID,
                   lpn_detail_rec.TC_ORDER_LINE_ID,
                   lpn_detail_rec.QTY_UOM_ID_BASE 
              FROM DUAL;

         SELECT wm_inventory_id_seq.NEXTVAL INTO v_wm_invn_id FROM DUAL;

         INSERT INTO WM_INVENTORY (TC_COMPANY_ID,
                                   TC_LPN_ID,
                                   ALLOCATABLE,
                                   CREATED_SOURCE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM,
                                   INBOUND_OUTBOUND_INDICATOR,
                                   C_FACILITY_ID,
                                   LPN_ID,
                                   WM_INVENTORY_ID,
                                   ON_HAND_QTY,
                                   WM_ALLOCATED_QTY,
                                   WM_VERSION_ID,
                                   CREATED_SOURCE_TYPE,
                                   ITEM_ID,
                                   MARKED_FOR_DELETE,
                                   LPN_DETAIL_ID,
                                   PACK_QTY,
                                   INVENTORY_TYPE,
                                   PRODUCT_STATUS,
                                   CNTRY_OF_ORGN,
                                   ITEM_ATTR_1,
                                   ITEM_ATTR_2,
                                   ITEM_ATTR_3,
                                   ITEM_ATTR_4,
                                   ITEM_ATTR_5,
                                   BATCH_NBR,
                                   SUB_PACK_QTY)
            SELECT lpn_detail_rec.tc_company_id,
                   v_tc_ilpn_id,
                   lpn_detail_rec.ALLOCATABLE,
                   p_login_user_id,
                   SYSTIMESTAMP,
                   1,
                   SYSTIMESTAMP,
                   'I',
                   lpn_detail_rec.C_FACILITY_ID,
                   v_ilpn_id,
                   v_wm_invn_id,
                   v_uom_qty,
                   v_uom_qty,
                   1,
                   1,
                   lpn_detail_rec.item_id,
                   'N',
                   v_lpn_detail_id,
                   lpn_detail_rec.PACK_QTY,
                   lpn_detail_rec.INVENTORY_TYPE,
                   lpn_detail_rec.PRODUCT_STATUS,
                   lpn_detail_rec.CNTRY_OF_ORGN,
                   lpn_detail_rec.ITEM_ATTR_1,
                   lpn_detail_rec.ITEM_ATTR_2,
                   lpn_detail_rec.ITEM_ATTR_3,
                   lpn_detail_rec.ITEM_ATTR_4,
                   lpn_detail_rec.ITEM_ATTR_5,
                   lpn_detail_rec.BATCH_NBR,
                   lpn_detail_rec.STD_SUB_PACK_QTY
                   
              FROM DUAL;

         wm_log (
               'BREAK LPN : TC_LPN_ID: '
            || p_tc_lpn_id
            || ' :: '
            || 'created inventory record for child lpn:'
            || v_tc_ilpn_id,
            1,
            'INVMGMT');

         v_num_ilpns_created := v_num_ilpns_created + 1;
      END LOOP;

      ---------------------------------------------------------------------------CREATE LPNS FOR  REMAINING NON-STD QTY--------------------------------------------------------------
      IF v_remaining_qty > 0
      THEN
          select lpn_id_seq.nextval into v_ilpn_id from dual;
          v_nxt_up_curr_nbr := v_nxt_up_curr_nbr + 1;
          v_tc_ilpn_id :=
            COALESCE (v_pfx_field, '')
            || LPAD (TO_CHAR (v_nxt_up_curr_nbr), v_nxt_up_nbr_len, '0');

         SELECT UNIT_VOLUME,UNIT_WEIGHT INTO v_unit_volume,v_unit_weight FROM ITEM_CBO  WHERE ITEM_ID = lpn_detail_rec.ITEM_ID AND COMPANY_ID= p_tc_company_id;
         INSERT INTO lpn (lpn_id,
                          tc_lpn_id,
                          tc_company_id,
                          lpn_type,
                          lpn_monetary_value,
                          c_facility_id,
                          o_facility_id,
                          o_facility_alias_id,
                          lpn_status,
                          lpn_facility_status,
                          tc_reference_lpn_id,
                          tc_asn_id,
                          asn_id,
                          tc_purchase_orders_id,
                          purchase_orders_id,
                          manifest_nbr,
                          ship_via,
                          master_bol_nbr,
                          bol_nbr,
                          init_ship_via,
                          path_id,
                          reprint_count,
                          freight_charge,
                          LENGTH,
                          width,
                          height,
                          tc_shipment_id,
                          shipment_id,
                          total_lpn_qty,
                          estimated_volume,
                          weight,
                          actual_volume,
                          lpn_label_type,
                          hibernate_version,
                          created_source,
                          created_dttm,
                          c_facility_alias_id,
                          pick_sub_locn_id,
                          inbound_outbound_indicator,
                          internal_order_id,
                          trailer_stop_seq_nbr,
                          wave_nbr,
                          wave_seq_nbr,
                          wave_stat_code,
                          transitional_inventory_type,
                          stage_indicator,
                          pick_delivery_duration,
                          lpn_break_attr,
                          seq_rule_priority,
                          nbr_of_zones,
                          pallet_x_of_y,
                          lpn_nbr_x_of_y,
                          load_sequence,
                          selection_rule_id,
                          single_line_lpn,
                          item_id,
                          lpn_facility_stat_updated_dttm,
                          order_id,
                          estimated_weight,
                          d_facility_id,
                          d_facility_alias_id,
                          lpn_creation_code,
                          cube_uom,
                          qty_uom_id_base,
                          weight_uom_id_base,
                          volume_uom_id_base,
                          disposition_type,
                          voco_intrn_reverse_id,
                          CONTAINER_TYPE,
                          PLANNED_TC_ASN_ID,
                          PHYSICAL_ENTITY_CODE,
                          PROCESS_IMMEDIATE_NEEDS,
                          ITEM_NAME,
                          RCVD_DTTM ,
                          PACKER_USERID,
                          BUSINESS_PARTNER_ID,
                          tc_order_id,
                          RECEIVED_TC_SHIPMENT_ID)
            SELECT v_ilpn_id,
                   v_tc_ilpn_id,
                   lpn_detail_rec.tc_company_id,
                   1,
                   lpn_detail_rec.lpn_monetary_value,
                   lpn_detail_rec.c_facility_id,
                   lpn_detail_rec.o_facility_id,
                   lpn_detail_rec.o_facility_alias_id,
                   45,
                   10,
                   p_tc_lpn_id,
                   lpn_detail_rec.tc_asn_id,
                   lpn_detail_rec.asn_id,
                   lpn_detail_rec.lpn_tc_purchase_orders_id,
                   lpn_detail_rec.lpn_purchase_orders_id,
                   lpn_detail_rec.manifest_nbr,
                   lpn_detail_rec.ship_via,
                   lpn_detail_rec.master_bol_nbr,
                   lpn_detail_rec.bol_nbr,
                   lpn_detail_rec.init_ship_via,
                   lpn_detail_rec.path_id,
                   lpn_detail_rec.reprint_count,
                   lpn_detail_rec.freight_charge,
                   lpn_detail_rec.LENGTH,
                   lpn_detail_rec.width,
                   lpn_detail_rec.height,
                   lpn_detail_rec.tc_shipment_id,
                   lpn_detail_rec.shipment_id,
                   v_remaining_qty,
                   (
                   Case 
                    When (lpn_detail_rec.Estimated_volume Is NULL) 
                          Then NULL
                          Else (lpn_detail_rec.Estimated_volume/lpn_detail_rec.size_value) * v_remaining_qty
                  end),
                   (
                   Case 
                    When (lpn_detail_rec.weight Is NULL) 
                          Then NULL
                          Else (lpn_detail_rec.weight/lpn_detail_rec.size_value) * v_remaining_qty
                  end),
                   (
                   Case 
                    When (lpn_detail_rec.actual_volume Is NULL) 
                          Then NULL
                          Else (lpn_detail_rec.actual_volume/lpn_detail_rec.size_value) * v_remaining_qty
                  end),                  
                   lpn_detail_rec.lpn_label_type,
                   lpn_detail_rec.hibernate_version,
                   lpn_detail_rec.created_source,
                   SYSTIMESTAMP,
                   lpn_detail_rec.c_facility_alias_id,
                   lpn_detail_rec.pick_sub_locn_id,
                   'I',
                   lpn_detail_rec.internal_order_id,
                   lpn_detail_rec.trailer_stop_seq_nbr,
                   lpn_detail_rec.wave_nbr,
                   lpn_detail_rec.wave_seq_nbr,
                   lpn_detail_rec.wave_stat_code,
                   lpn_detail_rec.transitional_inventory_type,
                   lpn_detail_rec.stage_indicator,
                   lpn_detail_rec.pick_delivery_duration,
                   lpn_detail_rec.lpn_break_attr,
                   lpn_detail_rec.seq_rule_priority,
                   lpn_detail_rec.nbr_of_zones,
                   lpn_detail_rec.pallet_x_of_y,
                   lpn_detail_rec.lpn_nbr_x_of_y,
                   lpn_detail_rec.load_sequence,
                   lpn_detail_rec.selection_rule_id,
                   lpn_detail_rec.single_line_lpn,
                   lpn_detail_rec.item_id,
                   lpn_detail_rec.lpn_facility_stat_updated_dttm,
                   NULL,
                   (
                   Case 
                    When (lpn_detail_rec.Estimated_Weight Is NULL) 
                          Then NULL
                          Else (lpn_detail_rec.Estimated_Weight/lpn_detail_rec.size_value) * v_remaining_qty
                  end),
                   lpn_detail_rec.d_facility_id,
                   lpn_detail_rec.d_facility_alias_id,
                   lpn_detail_rec.lpn_creation_code,
                   lpn_detail_rec.cube_uom,
                   lpn_detail_rec.qty_uom_id_base,
                   lpn_detail_rec.weight_uom_id_base,
                   lpn_detail_rec.volume_uom_id_base,
                   lpn_detail_rec.disposition_type,
                   lpn_detail_rec.voco_intrn_reverse_id,
                   lpn_detail_rec.CONTAINER_TYPE,
                   lpn_detail_rec.PLANNED_TC_ASN_ID,
                   lpn_detail_rec.PHYSICAL_ENTITY_CODE,
                   lpn_detail_rec.PROCESS_IMMEDIATE_NEEDS,
                   lpn_detail_rec.ITEM_NAME,
                   lpn_detail_rec.RCVD_DTTM,
                   lpn_detail_rec.PACKER_USERID,
                   lpn_detail_rec.BUSINESS_PARTNER_ID,
                   NULL,
                   lpn_detail_rec.RECEIVED_TC_SHIPMENT_ID
              FROM DUAL;

         wm_log (
               'BREAK LPN : TC_LPN_ID: '
            || p_tc_lpn_id
            || ' :: '
            || 'created child lpn'
            || v_tc_ilpn_id,
            1,
            'INVMGMT');

         ----------------CREATE ILPN DETAIL-------------
         SELECT lpn_detail_id_seq.NEXTVAL INTO v_lpn_detail_id FROM DUAL;

         INSERT INTO lpn_detail (tc_company_id,
                                 lpn_id,
                                 lpn_detail_id,
                                 lpn_detail_status,
                                 distribution_order_dtl_id,
                                 shipped_qty,
                                 received_qty,
                                 initial_qty,
                                 business_partner_id,
                                 item_id,
                                 gtin,
                                 std_pack_qty,
                                 std_sub_pack_qty,
                                 std_bundle_qty,
                                 incubation_date,
                                 expiration_date,
                                 ship_by_date,
                                 sell_by_dttm,
                                 consumption_priority_dttm,
                                 manufactured_dttm,
                                 cntry_of_orgn,
                                 inventory_type,
                                 product_status,
                                 item_attr_1,
                                 item_attr_2,
                                 item_attr_3,
                                 item_attr_4,
                                 item_attr_5,
                                 asn_dtl_id,
                                 size_value,
                                 qty_uom_id,
                                 weight_uom_id,
                                 volume_uom_id,
                                 assort_nbr,
                                 cut_nbr,
                                 purchase_orders_id,
                                 tc_purchase_orders_id,
                                 purchase_orders_line_id,
                                 tc_purchase_orders_line_id,
                                 hibernate_version,
                                 instrtn_code_1,
                                 instrtn_code_2,
                                 instrtn_code_3,
                                 instrtn_code_4,
                                 instrtn_code_5,
                                 created_source_type,
                                 created_source,
                                 created_dttm,
                                 last_updated_source_type,
                                 last_updated_source,
                                 last_updated_dttm,
                                 vendor_item_nbr,
                                 manufactured_plant,
                                 batch_nbr,
                                 assigned_qty,
                                 prepack_group_code,
                                 pack_code,
                                 QTY_CONV_FACTOR,
                                 ITEM_NAME,
                                 INTERNAL_ORDER_DTL_ID,
                                 TC_ORDER_LINE_ID,
                                 QTY_UOM_ID_BASE)
            SELECT lpn_detail_rec.tc_company_id,
                   v_ilpn_id,
                   v_lpn_detail_id,
                   lpn_detail_rec.LPN_DETAIL_STATUS,
                   NULL,
                   0,
                   v_remaining_qty AS received_qty,
                   v_remaining_qty AS initial_qty,
                   lpn_detail_rec.business_partner_id,
                   v_item_id,
                   lpn_detail_rec.gtin,
                   lpn_detail_rec.std_pack_qty,
                   lpn_detail_rec.std_sub_pack_qty,
                   lpn_detail_rec.std_bundle_qty,
                   lpn_detail_rec.incubation_date,
                   lpn_detail_rec.expiration_date,
                   lpn_detail_rec.ship_by_date,
                   lpn_detail_rec.sell_by_dttm,
                   lpn_detail_rec.consumption_priority_dttm,
                   lpn_detail_rec.manufactured_dttm,
                   lpn_detail_rec.cntry_of_orgn,
                   lpn_detail_rec.inventory_type,
                   lpn_detail_rec.product_status,
                   lpn_detail_rec.item_attr_1,
                   lpn_detail_rec.item_attr_2,
                   lpn_detail_rec.item_attr_3,
                   lpn_detail_rec.item_attr_4,
                   lpn_detail_rec.item_attr_5,
                   lpn_detail_rec.asn_dtl_id,
                   v_remaining_qty AS size_value,
                   lpn_detail_rec.qty_uom_id,
                   lpn_detail_rec.weight_uom_id,
                   lpn_detail_rec.volume_uom_id,
                   lpn_detail_rec.assort_nbr,
                   lpn_detail_rec.cut_nbr,
                   lpn_detail_rec.purchase_orders_id,
                   lpn_detail_rec.tc_purchase_orders_id,
                   lpn_detail_rec.purchase_orders_line_id,
                   lpn_detail_rec.tc_purchase_orders_line_id,
                   lpn_detail_rec.hibernate_version,
                   lpn_detail_rec.instrtn_code_1,
                   lpn_detail_rec.instrtn_code_2,
                   lpn_detail_rec.instrtn_code_3,
                   lpn_detail_rec.instrtn_code_4,
                   lpn_detail_rec.instrtn_code_5,
                   5,
                   p_login_user_id,
                   SYSTIMESTAMP,
                   5,
                   p_login_user_id,
                   SYSTIMESTAMP,
                   lpn_detail_rec.vendor_item_nbr,
                   lpn_detail_rec.manufactured_plant,
                   lpn_detail_rec.batch_nbr,
                   lpn_detail_rec.assigned_qty,
                   lpn_detail_rec.prepack_group_code,
                   lpn_detail_rec.pack_code,
                   lpn_detail_rec.QTY_CONV_FACTOR,
                   lpn_detail_rec.ITEM_NAME,
                   lpn_detail_rec.INTERNAL_ORDER_DTL_ID,
                   lpn_detail_rec.TC_ORDER_LINE_ID,
                   lpn_detail_rec.QTY_UOM_ID_BASE 
              FROM DUAL;

         SELECT wm_inventory_id_seq.NEXTVAL INTO v_wm_invn_id FROM DUAL;

         INSERT INTO WM_INVENTORY (TC_COMPANY_ID,
                                   TC_LPN_ID,                   --LOCATION_ID,
                                   ALLOCATABLE,
                                   CREATED_SOURCE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM,
                                   INBOUND_OUTBOUND_INDICATOR,
                                   C_FACILITY_ID,
                                   LPN_ID,
                                   WM_INVENTORY_ID,
                                   ON_HAND_QTY,
                                   WM_ALLOCATED_QTY,
                                   WM_VERSION_ID,
                                   CREATED_SOURCE_TYPE,
                                   ITEM_ID,
                                   MARKED_FOR_DELETE,
                                   LPN_DETAIL_ID,
                                   PACK_QTY,
                                   INVENTORY_TYPE,
                                   PRODUCT_STATUS,
                                   CNTRY_OF_ORGN,
                                   ITEM_ATTR_1,
                                   ITEM_ATTR_2,
                                   ITEM_ATTR_3,
                                   ITEM_ATTR_4,
                                   ITEM_ATTR_5,
                                   BATCH_NBR,
                                   SUB_PACK_QTY)
            SELECT lpn_detail_rec.tc_company_id,
                   v_tc_ilpn_id,                --lpn_detail_rec.PICK_LOCN_ID,
                   lpn_detail_rec.ALLOCATABLE,
                   p_login_user_id,
                   SYSTIMESTAMP,
                   1,
                   SYSTIMESTAMP,
                   'I',
                   lpn_detail_rec.C_FACILITY_ID,
                   v_ilpn_id,
                   v_wm_invn_id,
                   v_remaining_qty,
                   v_remaining_qty,
                   1,
                   1,
                   lpn_detail_rec.item_id,
                   'N',
                   v_lpn_detail_id,
                   lpn_detail_rec.PACK_QTY,
                   lpn_detail_rec.INVENTORY_TYPE,
                   lpn_detail_rec.PRODUCT_STATUS,
                   lpn_detail_rec.CNTRY_OF_ORGN,
                   lpn_detail_rec.ITEM_ATTR_1,
                   lpn_detail_rec.ITEM_ATTR_2,
                   lpn_detail_rec.ITEM_ATTR_3,
                   lpn_detail_rec.ITEM_ATTR_4,
                   lpn_detail_rec.ITEM_ATTR_5,
                   lpn_detail_rec.BATCH_NBR,
                   lpn_detail_rec.STD_SUB_PACK_QTY 
              FROM DUAL;

         wm_log (
               'BREAK LPN : TC_LPN_ID: '
            || p_tc_lpn_id
            || ' :: '
            || 'LPN for non-standard Quantity is created with LPN_ID:'
            || v_ilpn_id,
            1,
            'INVMGMT');
          v_num_ilpns_created := v_num_ilpns_created + 1;
      END IF;
   END LOOP;
END;
/
show errors;

create or replace procedure manh_pack_x_dock_lpn 
(
    p_case_nbr_list   in     varchar2,        -- list of lpn's separated by ","
    p_tc_asn_id       in     varchar2,
    p_mode            in     number,              -- 1 = asn mode, 2 = lpn mode
    p_plt_nbr         in     varchar2,
    p_whse            in     varchar2,
    p_facility_id     in     number,
    p_tc_company_id   in     number,
    p_login_user_id   in     varchar2,
    p_debug_flag      in     number,
    p_rc              out    number
)
as
    v_task_id              number(10) := 0;
    v_curr_date_time       date := sysdate;
    v_pallet_lpn_id        number(10) := 0;
    v_sum_on_hand_qty      number(13, 5) := 0;
    v_pakd_qty             number(13, 5) := 0;
    v_child_pakd_qty       number(13, 5) := 0;
    v_case_nbr             varchar2(50) := null;
    v_orig_case_nbr_list   varchar2(3000) := null;
    v_act_trk_tran_nbr     varchar2(30) := NULL;
    v_prev_module_name     wm_utils.t_app_context_data;
    v_prev_action_name     wm_utils.t_app_context_data;	

    -- get all the aggregated lines from temporary tables in a loop 
    -- and loop through all the original order lines.
    cursor agg_line is
        select t.line_item_id,
            (sum (t.qty_alloc) - sum (t.qty_pulld)) as pakd_qty
        from tmp_pack_mop_lpn_dtls t
        group by t.line_item_id;

    v_agg_line    agg_line%rowtype;

    -- loop through each original order line item using agg line 
    -- and update units_pakd
    cursor orig_line is
        select oli.line_item_id, (oli.allocated_qty - (coalesce (oli.units_pakd, 0)
            + coalesce (oli.user_canceled_qty, 0))) as avlbl_qty
        from order_line_item oli
        where oli.reference_line_item_id = v_agg_line.line_item_id
            and oli.do_dtl_status in (120, 130, 140)
        order by oli.priority asc, oli.order_id asc, oli.line_item_id desc ;

    v_orig_line    orig_line%rowtype;
begin
    p_rc := 0;
    v_orig_case_nbr_list := p_case_nbr_list;
	
    wm_utils.get_context_info(v_prev_module_name, v_prev_action_name);
    wm_utils.set_context_info(p_module_name => 'THD', p_action_name => 'PACK_X_DOCK_LPN',
        p_client_id => p_tc_asn_id||p_plt_nbr);

    -- if mode is "1", if it auto-receiving mode. (asn mode)
    -- if mode is "2", a list of lpn's concatenated by "," is sent (list of lpn mode)
    if (p_mode = 1)
    then
        wm_log ('manh_pack_mop_lpn called for asn: ' || p_tc_asn_id, p_debug_flag);
    elsif (p_mode = 2)
    then
        wm_log ('manh_pack_mop_lpn called for ilpn;s : ' || v_orig_case_nbr_list,
            p_debug_flag);
    else
        wm_log ('manh_pack_mop_lpn called with invalid mode', p_debug_flag);
    end if;

    -- load task_dtl and alloc_invn_dtl into tmp_pack_mop_lpn_dtls
    if (p_mode = 1)
    then
        -- load the cases based on the asn_id passed from the caller
        insert into tmp_pack_mop_lpn_dtls 
        (
            cntr_nbr, carton_nbr, carton_seq_nbr, dest_locn_id, tc_order_id, 
            line_item_id, pkt_ctrl_nbr, pkt_seq_nbr, qty_alloc, qty_pulld,  
            item_id, alloc_invn_dtl_id, task_genrtn_ref_nbr, inventory_type,
            product_status, cntry_of_orgn, batch_nbr, item_attr_1,
            item_attr_2, item_attr_3, item_attr_4, item_attr_5
        )
        select td.cntr_nbr, td.carton_nbr, td.carton_seq_nbr, td.dest_locn_id,
            td.tc_order_id, td.line_item_id, td.pkt_ctrl_nbr, td.pkt_seq_nbr,
            td.qty_alloc, td.qty_pulld, td.item_id, td.alloc_invn_dtl_id,
            td.task_genrtn_ref_nbr, td.invn_type, td.prod_stat, td.cntry_of_orgn,
            td.batch_nbr, td.sku_attr_1, td.sku_attr_2, td.sku_attr_3,
            td.sku_attr_4, td.sku_attr_5
        from task_dtl td
        join task_hdr th on th.task_id = td.task_id and th.whse = p_whse
        where td.stat_code < 90 and td.cd_master_id = p_tc_company_id
            and td.task_cmpl_ref_code = 6 and td.task_cmpl_ref_nbr = td.carton_nbr
            and exists
            (
                select 1
                from lpn l
                where td.cntr_nbr = l.tc_lpn_id
                    and l.tc_asn_id = p_tc_asn_id
                    and l.inbound_outbound_indicator = 'I'
                    and l.tc_company_id = p_tc_company_id
                    and l.lpn_facility_status = 50
                    and l.disposition_type is not null
                    and l.c_facility_id = p_facility_id
            )
        union
        select aid.cntr_nbr, aid.carton_nbr, aid.carton_seq_nbr, aid.dest_locn_id,
            aid.tc_order_id, aid.line_item_id, aid.pkt_ctrl_nbr, aid.pkt_seq_nbr,
            aid.qty_alloc, aid.qty_pulld, aid.item_id, aid.alloc_invn_dtl_id,
            aid.task_genrtn_ref_nbr, aid.invn_type, aid.prod_stat,
            aid.cntry_of_orgn, aid.batch_nbr, aid.sku_attr_1, aid.sku_attr_2,
            aid.sku_attr_3, aid.sku_attr_4, aid.sku_attr_5 
        from alloc_invn_dtl aid
        where aid.stat_code < 90 and aid.cd_master_id = p_tc_company_id
            and aid.task_cmpl_ref_code = 6
            and aid.task_cmpl_ref_nbr = aid.carton_nbr
            and aid.whse = p_whse
            and exists
            (
                select 1
                from lpn l
                where aid.cntr_nbr = l.tc_lpn_id
                    and l.tc_asn_id = p_tc_asn_id
                    and l.inbound_outbound_indicator = 'I'
                    and l.tc_company_id = p_tc_company_id
							      and l.lpn_facility_status = 50 
                    and l.disposition_type is not null
                    and l.c_facility_id = p_facility_id
            )
        order by tc_order_id desc;
        wm_log ('tmp_pack_mop_lpn_dtls insert ' || sql%rowcount, p_debug_flag);
    elsif (p_mode = 2)
    then
        -- load the cases based on the list of case nbr passed from the 
        -- caller which are separated by ","
        while v_orig_case_nbr_list is not null
        loop
        begin
            v_case_nbr := null;

            -- get the next case_nbr to process
            if instr (v_orig_case_nbr_list, ',') > 0
            then
                v_case_nbr := substr (v_orig_case_nbr_list, 1, instr(v_orig_case_nbr_list, ',') - 1);
                -- remove it from the list
                v_orig_case_nbr_list := substr (v_orig_case_nbr_list,
                    instr (v_orig_case_nbr_list, ',') + 1);
            else
                v_case_nbr := v_orig_case_nbr_list;
                v_orig_case_nbr_list := null;
            end if;

            if v_case_nbr is not null
            then
                insert into tmp_pack_mop_lpn_dtls 
                (
                    cntr_nbr, carton_nbr, carton_seq_nbr, dest_locn_id,
                    tc_order_id, line_item_id, pkt_ctrl_nbr, pkt_seq_nbr,
                    qty_alloc, qty_pulld, item_id, alloc_invn_dtl_id,
                    task_genrtn_ref_nbr, inventory_type, product_status,
                    cntry_of_orgn, batch_nbr, item_attr_1, item_attr_2,
                    item_attr_3, item_attr_4, item_attr_5
                )
                select td.cntr_nbr, td.carton_nbr, td.carton_seq_nbr, td.dest_locn_id,
                    td.tc_order_id, td.line_item_id, td.pkt_ctrl_nbr, td.pkt_seq_nbr,
                    td.qty_alloc, td.qty_pulld, td.item_id, td.alloc_invn_dtl_id,
                    td.task_genrtn_ref_nbr, td.invn_type, td.prod_stat,
                    td.cntry_of_orgn, td.batch_nbr, td.sku_attr_1, td.sku_attr_2,
                    td.sku_attr_3, td.sku_attr_4, td.sku_attr_5
                from task_dtl td
                join task_hdr th on th.task_id = td.task_id and th.whse = p_whse
                where td.stat_code < 90 and td.cd_master_id = p_tc_company_id
                    and td.task_cmpl_ref_code = 6
                    and td.task_cmpl_ref_nbr = td.carton_nbr
                    and exists
                    (
                        select 1
                        from lpn l
                        where td.cntr_nbr = l.tc_lpn_id
                            and l.tc_lpn_id = v_case_nbr
                            and l.inbound_outbound_indicator = 'I'
                            and l.tc_company_id = p_tc_company_id
                            and l.c_facility_id = p_facility_id
                    )
                union
                select aid.cntr_nbr, aid.carton_nbr, aid.carton_seq_nbr,
                    aid.dest_locn_id, aid.tc_order_id, aid.line_item_id,
                    aid.pkt_ctrl_nbr, aid.pkt_seq_nbr, aid.qty_alloc,
                    aid.qty_pulld, aid.item_id, aid.alloc_invn_dtl_id,
                    aid.task_genrtn_ref_nbr, aid.invn_type, aid.prod_stat,
                    aid.cntry_of_orgn, aid.batch_nbr, aid.sku_attr_1,
                    aid.sku_attr_2, aid.sku_attr_3, aid.sku_attr_4,
                    aid.sku_attr_5
                from alloc_invn_dtl aid
                where aid.stat_code < 90 and aid.cd_master_id = p_tc_company_id
                    and aid.task_cmpl_ref_code = 6
                    and aid.task_cmpl_ref_nbr = aid.carton_nbr
                    and aid.whse = p_whse
                    and exists
                    (
                        select 1
                        from lpn l
                        where aid.cntr_nbr = l.tc_lpn_id
                            and l.tc_lpn_id = v_case_nbr
                            and l.inbound_outbound_indicator = 'I'
                            and l.tc_company_id = p_tc_company_id
                            and l.c_facility_id = p_facility_id
                    )
                order by tc_order_id desc;
                wm_log ('tmp_pack_mop_lpn_dtls insert ' || sql%rowcount, p_debug_flag);
            end if;
        end;
        end loop;
    end if;

    -- ilpn wm_inventory updates
    merge into wm_inventory wi
    using 
    (
        select t.cntr_nbr, t.item_id, t.inventory_type, t.product_status,
            t.cntry_of_orgn, t.batch_nbr, t.item_attr_1, t.item_attr_2,
            t.item_attr_3, t.item_attr_4, t.item_attr_5,
            (sum (t.qty_alloc) - sum (t.qty_pulld)) as pakd_qty
        from tmp_pack_mop_lpn_dtls t
        group by t.cntr_nbr, t.item_id, t.inventory_type, t.product_status,
            t.cntry_of_orgn, t.batch_nbr, t.item_attr_1, t.item_attr_2,
            t.item_attr_3, t.item_attr_4, t.item_attr_5
    ) td on ( td.cntr_nbr = wi.tc_lpn_id and wi.inbound_outbound_indicator = 'I'
        and wi.tc_company_id = p_tc_company_id
        and wi.c_facility_id = p_facility_id
        and td.item_id = wi.item_id
        and (td.inventory_type = '*' or wi.inventory_type = '*' 
            or coalesce(td.inventory_type, '0') = coalesce(wi.inventory_type, '0'))
               and (td.product_status = '*' or wi.product_status = '*'
            or coalesce(td.product_status, '0') = coalesce(wi.product_status, '0'))
        and (td.cntry_of_orgn = '*' or wi.cntry_of_orgn = '*'
            or coalesce(td.cntry_of_orgn, '0') = coalesce(wi.cntry_of_orgn, '0'))
        and (td.batch_nbr = '*' or wi.batch_nbr = '*'
            or coalesce(td.batch_nbr, '0') = coalesce(wi.batch_nbr, '0'))
        and (td.item_attr_1 = '*' or wi.item_attr_1 = '*'
            or coalesce(td.item_attr_1, '0') = coalesce(wi.item_attr_1, '0'))
        and (td.item_attr_2 = '*' or wi.item_attr_2 = '*'
            or coalesce(td.item_attr_2, '0') = coalesce(wi.item_attr_2, '0'))
        and (td.item_attr_3 = '*' or wi.item_attr_3 = '*'
            or coalesce(td.item_attr_3, '0') = coalesce(wi.item_attr_3, '0'))
        and (td.item_attr_4 = '*' or wi.item_attr_4 = '*'
            or coalesce(td.item_attr_4, '0') = coalesce(wi.item_attr_4, '0'))
        and (td.item_attr_5 = '*' or wi.item_attr_5 = '*'
            or coalesce(td.item_attr_5, '0') = coalesce(wi.item_attr_5, '0')))
    when matched then
    update set
        wi.last_updated_source = p_login_user_id,
        wi.last_updated_dttm = v_curr_date_time,
        wi.wm_version_id = (coalesce (wi.wm_version_id, 0) + 1),
        wi.on_hand_qty =
            (case
                when (wi.on_hand_qty - pakd_qty) <= 0 then 0
                else (wi.on_hand_qty - pakd_qty)
             end),
        wi.wm_allocated_qty =
            (case
                when (wi.wm_allocated_qty - pakd_qty) <= 0 then 0
                else (wi.wm_allocated_qty - pakd_qty)
             end);
    wm_log ('ilpn wm_inventory update ' || sql%rowcount, p_debug_flag);


    -- ilpn lpn_detail update
    merge into lpn_detail ld
    using 
    (  
        select l.lpn_id, t.item_id, t.inventory_type, t.product_status,
            t.cntry_of_orgn, t.batch_nbr, t.item_attr_1, t.item_attr_2,
            t.item_attr_3, t.item_attr_4, t.item_attr_5,
            (sum (t.qty_alloc) - sum (t.qty_pulld)) as pakd_qty
        from tmp_pack_mop_lpn_dtls t, lpn l
        where t.cntr_nbr = l.tc_lpn_id
            and l.inbound_outbound_indicator = 'I'
            and l.tc_company_id = p_tc_company_id
            and l.c_facility_id = p_facility_id
        group by l.lpn_id, t.item_id, t.inventory_type, t.product_status,
            t.cntry_of_orgn, t.batch_nbr, t.item_attr_1, t.item_attr_2,
            t.item_attr_3, t.item_attr_4, t.item_attr_5
    ) td
    on (td.lpn_id = ld.lpn_id and td.item_id = ld.item_id
        and (td.inventory_type = '*' or ld.inventory_type = '*'
            or coalesce(td.inventory_type, '0') = coalesce(ld.inventory_type, '0'))
        and (td.product_status = '*' or ld.product_status = '*'
            or coalesce(td.product_status, '0') = coalesce (ld.product_status, '0'))
        and (td.cntry_of_orgn = '*' or ld.cntry_of_orgn = '*'
            or coalesce(td.cntry_of_orgn, '0') = coalesce(ld.cntry_of_orgn, '0'))
        and (td.batch_nbr = '*' or ld.batch_nbr = '*'
            or coalesce(td.batch_nbr, '0') = coalesce(ld.batch_nbr, '0'))
        and (td.item_attr_1 = '*' or ld.item_attr_1 = '*'
            or coalesce(td.item_attr_1, '0') = coalesce(ld.item_attr_1, '0'))
        and (td.item_attr_2 = '*' or ld.item_attr_2 = '*'
            or coalesce(td.item_attr_2, '0') = coalesce(ld.item_attr_2, '0'))
        and (td.item_attr_3 = '*' or ld.item_attr_3 = '*'
            or coalesce(td.item_attr_3, '0') = coalesce(ld.item_attr_3, '0'))
        and (td.item_attr_4 = '*' or ld.item_attr_4 = '*'
            or coalesce(td.item_attr_4, '0') = coalesce(ld.item_attr_4, '0'))
        and (td.item_attr_5 = '*' or ld.item_attr_5 = '*'
            or coalesce(td.item_attr_5, '0') = coalesce(ld.item_attr_5, '0')))
    when matched then
    update set
        ld.hibernate_version = (coalesce (ld.hibernate_version, 0) + 1),
        ld.last_updated_source = p_login_user_id,
        ld.last_updated_dttm = v_curr_date_time,
        ld.size_value =
            (case
                when (ld.size_value - pakd_qty) <= 0 then 0
                else (ld.size_value - pakd_qty)
             end);
    wm_log ('ilpn lpn_detail update ' || sql%rowcount, p_debug_flag);

    -- final ilpn update
    merge into lpn l
    using 
    (  
        select wi.lpn_id, coalesce (sum (wi.on_hand_qty), 0) as v_sum_on_hand_qty
        from tmp_pack_mop_lpn_dtls t, wm_inventory wi
        where t.cntr_nbr = wi.tc_lpn_id
            and wi.inbound_outbound_indicator = 'I'
            and wi.tc_company_id = p_tc_company_id
            and wi.c_facility_id = p_facility_id
        group by wi.lpn_id 
        having v_sum_on_hand_qty <= 0
    ) wmi on (wmi.lpn_id = l.lpn_id)
   when matched then
   update set
       l.hibernate_version = (coalesce (l.hibernate_version, 0) + 1),
       l.last_updated_source = p_login_user_id,
       l.last_updated_dttm = v_curr_date_time,
       l.parent_lpn_id = null,
       l.tc_parent_lpn_id = null,
       l.lpn_status = 70,
       l.lpn_facility_status = 95,
       l.lpn_facility_stat_updated_dttm = v_curr_date_time,
       l.weight = 0;
    wm_log ('ilpn lpn update ' || sql%rowcount, p_debug_flag);


    -- delete empty ilpn wm_inventory
    delete from wm_inventory wmi
    where wmi.wm_inventory_id in
    (
        select wi.wm_inventory_id
        from wm_inventory wi, tmp_pack_mop_lpn_dtls td
        where td.cntr_nbr = wi.tc_lpn_id
            and wi.inbound_outbound_indicator = 'I'
            and wi.tc_company_id = p_tc_company_id
            and wi.c_facility_id = p_facility_id
            and wi.on_hand_qty <= 0 and td.item_id = wi.item_id
            and (td.inventory_type = '*' or wi.inventory_type = '*'
                or coalesce (td.inventory_type, '0') = coalesce (wi.inventory_type, '0'))
            and (td.product_status = '*' or wi.product_status = '*'
                or coalesce (td.product_status, '0') = coalesce (wi.product_status, '0'))
            and (td.cntry_of_orgn = '*' or wi.cntry_of_orgn = '*'
                or coalesce (td.cntry_of_orgn, '0') = coalesce (wi.cntry_of_orgn, '0'))
            and (td.batch_nbr = '*' or wi.batch_nbr = '*' 
                or coalesce (td.batch_nbr, '0') = coalesce (wi.batch_nbr, '0'))
            and (td.item_attr_1 = '*' or wi.item_attr_1 = '*'
                or coalesce (td.item_attr_1, '0') = coalesce (wi.item_attr_1, '0'))
            and (td.item_attr_2 = '*' or wi.item_attr_2 = '*'
                or coalesce (td.item_attr_2, '0') = coalesce (wi.item_attr_2, '0'))
            and (td.item_attr_3 = '*' or wi.item_attr_3 = '*'
                or coalesce (td.item_attr_3, '0') = coalesce (wi.item_attr_3, '0'))
            and (td.item_attr_4 = '*' or wi.item_attr_4 = '*'
                or coalesce (td.item_attr_4, '0') = coalesce (wi.item_attr_4, '0'))
            and (td.item_attr_5 = '*' or wi.item_attr_5 = '*'
                or coalesce (td.item_attr_5, '0') = coalesce (wi.item_attr_5, '0'))
    );
    wm_log ('ilpn wm_inventory delete ' || sql%rowcount, p_debug_flag);

    -- olpn wm_inventory updates
    merge into wm_inventory wi
    using 
    (  
        select t.carton_nbr, t.carton_seq_nbr,
            (sum (t.qty_alloc) - sum (t.qty_pulld)) as pakd_qty
        from tmp_pack_mop_lpn_dtls t
        group by t.carton_nbr, t.carton_seq_nbr
    ) td on ( td.carton_nbr = wi.tc_lpn_id and td.carton_seq_nbr = wi.lpn_detail_id
        and wi.inbound_outbound_indicator = 'O' 
        and wi.tc_company_id = p_tc_company_id
        and wi.c_facility_id = p_facility_id)
   when matched then
   update set wi.last_updated_source = p_login_user_id,
       wi.last_updated_dttm = v_curr_date_time,
       wi.location_id = null,
       wi.locn_class = null,
       wi.location_dtl_id = null,
       wi.wm_version_id = (coalesce (wi.wm_version_id, 0) + 1),
       wi.on_hand_qty = (wi.on_hand_qty + pakd_qty),
       wi.wm_allocated_qty = (wi.wm_allocated_qty + pakd_qty);
    wm_log ('olpn wm_inventory update ' || sql%rowcount, p_debug_flag);


    -- olpn lpn_detail update
    merge into lpn_detail ld
    using 
    (  
        select l.lpn_id, t.carton_seq_nbr, t.item_id, l.consumption_priority_dttm,
            t.inventory_type, t.product_status, t.cntry_of_orgn, t.batch_nbr,
            t.item_attr_1, t.item_attr_2, t.item_attr_3, t.item_attr_4,
            t.item_attr_5, (sum (t.qty_alloc) - sum (t.qty_pulld)) as pakd_qty
        from tmp_pack_mop_lpn_dtls t, lpn l
        where t.carton_nbr = l.tc_lpn_id
            and l.inbound_outbound_indicator = 'O'
            and l.tc_company_id = p_tc_company_id
            and l.c_facility_id = p_facility_id
        group by l.lpn_id, t.carton_seq_nbr, t.item_id, l.consumption_priority_dttm,
            t.inventory_type, t.product_status, t.cntry_of_orgn, t.batch_nbr,
            t.item_attr_1, t.item_attr_2, t.item_attr_3, t.item_attr_4,
            t.item_attr_5
    ) td on (td.lpn_id = ld.lpn_id and td.carton_seq_nbr = ld.lpn_detail_id)
   when matched then
   update set
       ld.hibernate_version = (coalesce (ld.hibernate_version, 0) + 1),
       ld.last_updated_source = p_login_user_id,
       ld.last_updated_dttm = v_curr_date_time,
       ld.size_value = (ld.size_value + pakd_qty),
       ld.consumption_priority_dttm = td.consumption_priority_dttm,
       ld.vendor_item_nbr =
           (
               select ild.vendor_item_nbr
               from lpn_detail ild, lpn il
               where ild.lpn_id = il.lpn_id
                   and il.lpn_id = td.lpn_id
                   and il.inbound_outbound_indicator = 'I'
                   and il.c_facility_id = p_facility_id
                   and il.tc_company_id = p_tc_company_id
                   and il.lpn_type = 1
                   and ild.item_id = td.item_id
                   and (td.inventory_type = '*' or ild.inventory_type = '*'
                       or coalesce (td.inventory_type, '0') =
                           coalesce (ild.inventory_type, '0'))
                   and (td.product_status = '*' or ild.product_status = '*'
                       or coalesce (td.product_status, '0') =
                           coalesce (ild.product_status, '0'))
                   and (td.cntry_of_orgn = '*' or ild.cntry_of_orgn = '*'
                       or coalesce (td.cntry_of_orgn, '0') =
                           coalesce (ild.cntry_of_orgn, '0'))
                   and (td.batch_nbr = '*' or ild.batch_nbr = '*'
                       or coalesce (td.batch_nbr, '0') =
                           coalesce (ild.batch_nbr, '0'))
                   and (td.item_attr_1 = '*' or ild.item_attr_1 = '*'
                       or coalesce (td.item_attr_1, '0') =
                           coalesce (ild.item_attr_1, '0'))
                   and (td.item_attr_2 = '*' or ild.item_attr_2 = '*'
                       or coalesce (td.item_attr_2, '0') =
                           coalesce (ild.item_attr_2, '0'))
                   and (td.item_attr_3 = '*' or ild.item_attr_3 = '*'
                       or coalesce (td.item_attr_3, '0') =
                           coalesce (ild.item_attr_3, '0'))
                   and (td.item_attr_4 = '*' or ild.item_attr_4 = '*'
                       or coalesce (td.item_attr_4, '0') =
                           coalesce (ild.item_attr_4, '0'))
                   and (td.item_attr_5 = '*' or ild.item_attr_5 = '*'
                       or coalesce (td.item_attr_5, '0') =
                           coalesce (ild.item_attr_5, '0'))
            ),
       ld.lpn_detail_status =
           (
               case
                   when (ld.initial_qty - (ld.size_value + pakd_qty)) <= 0
                   then 90 else 0
               end
           );
    wm_log ('olpn lpn_detail update ' || sql%rowcount, p_debug_flag);

    -- ob pallet
    if (p_plt_nbr is not null)
    then
        -- insert ob pallet if not found --
        select coalesce (min (lpn.lpn_id), 0)
        into v_pallet_lpn_id
        from lpn
        where lpn.tc_lpn_id = p_plt_nbr
            and lpn.inbound_outbound_indicator = 'O'
            and lpn.c_facility_id = p_facility_id
            and lpn.tc_company_id = p_tc_company_id
            and lpn.lpn_type = 2;

        if (v_pallet_lpn_id <= 0)
        then
            select lpn_id_seq.nextval into v_pallet_lpn_id from dual;

            insert into lpn 
            (
                lpn.weight, lpn.actual_volume, lpn.hibernate_version,
                lpn.created_source, lpn.created_dttm, lpn.inbound_outbound_indicator,
                lpn.voco_intrn_reverse_id, lpn.lpn_id, lpn.tc_lpn_id,
                lpn.tc_company_id, lpn.lpn_type, lpn.tier_qty, lpn.c_facility_id,
                lpn.lpn_status, lpn.lpn_facility_status, lpn.single_line_lpn,
                lpn.estimated_weight
            )
            values 
            (
                0, 0, 1, p_login_user_id, v_curr_date_time, 'O', reverse(p_plt_nbr),
                v_pallet_lpn_id, p_plt_nbr, p_tc_company_id, 2, 0, p_facility_id,
                10, 0, 'Y', 0
            );

            wm_log ('ob pallet lpn insert ' || sql%rowcount, p_debug_flag);
        end if;
    end if;

    -- final olpn update
    merge into lpn l
    using 
    (  
        select lpn.lpn_id, s.static_route_id, s.shipment_id, s.tc_shipment_id,
            o.original_assigned_ship_via, os.line_haul_ship_via,
            o.bill_of_lading_number
        from tmp_pack_mop_lpn_dtls t
        inner join lpn lpn
            on ( t.carton_nbr = lpn.tc_lpn_id and lpn.inbound_outbound_indicator = 'O' 
                and lpn.c_facility_id = p_facility_id
                and lpn.tc_company_id = p_tc_company_id
                and lpn.lpn_type = 1)
        inner join orders o
            on lpn.order_id = o.order_id
        left join order_split os
            on lpn.order_split_id = os.order_split_id
                and lpn.order_id = os.order_id
                and o.order_id = os.order_id
                and os.is_cancelled = 0
        left join shipment s
            on (os.shipment_id = s.shipment_id
                and s.shipment_closed_indicator = 0
                and s.shipment_status < 80 )
        group by lpn.lpn_id, s.static_route_id, s.shipment_id, s.tc_shipment_id,
            o.original_assigned_ship_via, os.line_haul_ship_via, 
            o.bill_of_lading_number
    ) td on (l.lpn_id = td.lpn_id)
   when matched then
   update set
       l.hibernate_version = (coalesce (l.hibernate_version, 0) + 1),
       l.last_updated_source = p_login_user_id,
       l.last_updated_dttm = v_curr_date_time,
       l.total_lpn_qty =
           ( 
               select coalesce (sum (wi.on_hand_qty), 0)
               from wm_inventory wi
               where wi.inbound_outbound_indicator = 'O'
                   and wi.tc_company_id = p_tc_company_id
                   and wi.tc_lpn_id = l.tc_lpn_id
           ),
       l.packer_userid = p_login_user_id,
       l.curr_sub_locn_id = null,
       l.lpn_status = 15,
       l.lpn_facility_status = 20,
       l.lpn_facility_stat_updated_dttm = v_curr_date_time,
       l.parent_lpn_id =
           (case when v_pallet_lpn_id > 0 then v_pallet_lpn_id else null end),
       l.tc_parent_lpn_id = p_plt_nbr,
       l.static_route_id = (case when coalesce(td.static_route_id,0) > 0 
           then td.static_route_id else l.static_route_id end),
       l.shipment_id = (case when coalesce(td.shipment_id,0) > 0 
           then td.shipment_id else l.shipment_id end),
       l.init_ship_via = (case when td.original_assigned_ship_via is not null 
           then td.original_assigned_ship_via else l.init_ship_via end),
       l.ship_via = (case when td.line_haul_ship_via is not null 
           then td.line_haul_ship_via else l.ship_via end),
       l.bol_nbr = (case when td.bill_of_lading_number is not null 
           then td.bill_of_lading_number else l.bol_nbr end),
       l.master_bol_nbr = (case when td.bill_of_lading_number is not null 
           then td.bill_of_lading_number else l.master_bol_nbr end);
    wm_log ('olpn lpn update ' || sql%rowcount, p_debug_flag);

    -- update order split line item if there are any
    merge into order_split_line_item osli
    using 
    (  
        select l.order_id, t.line_item_id, l.order_split_id,
            (sum (t.qty_alloc) - sum (t.qty_pulld)) as pakd_qty
        from tmp_pack_mop_lpn_dtls t
        join lpn l on ( l.inbound_outbound_indicator = 'O'
            and l.c_facility_id = p_facility_id
            and l.tc_company_id = p_tc_company_id
            and l.lpn_type = 1
            and l.tc_lpn_id = t.carton_nbr)
        group by l.order_id, t.line_item_id, l.order_split_id
    ) iv on ( iv.order_split_id = osli.order_split_id
        and iv.order_id = osli.order_id
        and iv.line_item_id = osli.line_item_id)
   when matched then
   update set 
       osli.last_updated_dttm = v_curr_date_time,
       osli.last_updated_source = p_login_user_id,
       osli.units_packed = ((coalesce (osli.units_packed, 0) + iv.pakd_qty));
    wm_log ('osli update ' || sql%rowcount, p_debug_flag);

    -- aggrgate order line update
    merge into order_line_item oli
    using 
    (  
        select line_item_id, (sum (t.qty_alloc) - sum (t.qty_pulld)) as pakd_qty
        from tmp_pack_mop_lpn_dtls t
        group by t.line_item_id
    ) td on (td.line_item_id = oli.line_item_id)
    when matched then
    update set
        oli.last_updated_source = p_login_user_id,
        oli.last_updated_dttm = v_curr_date_time,
        oli.hibernate_version = (coalesce (oli.hibernate_version, 0) + 1),
        oli.units_pakd = ( (coalesce (oli.units_pakd, 0) + td.pakd_qty)),
        oli.do_dtl_status =
            (case
                when oli.order_qty - oli.allocated_qty <= 0
                then
                   case
                   when oli.order_qty - (coalesce(oli.units_pakd, 0) + td.pakd_qty) <= 0
                      then 150 
                   else 140
                   end
                else 120
             end);
    wm_log ('aggregate lines update ' || sql%rowcount, p_debug_flag);

    -- original order line update
    open agg_line;

    loop
        fetch agg_line into v_agg_line;

        exit when agg_line%notfound;
        v_pakd_qty := v_agg_line.pakd_qty;
        open orig_line;
        loop
            fetch orig_line into v_orig_line;
            exit when (orig_line%notfound or (v_pakd_qty <= 0));

            v_child_pakd_qty :=
                case
                   when v_orig_line.avlbl_qty < v_pakd_qty
                       then v_orig_line.avlbl_qty
                   else v_pakd_qty
                end;

            update order_line_item oli
            set oli.last_updated_source = p_login_user_id,
                oli.last_updated_dttm = v_curr_date_time,
                oli.hibernate_version =
                    (coalesce (oli.hibernate_version, 0) + 1),
                oli.units_pakd =
                    ((coalesce (oli.units_pakd, 0)) + v_child_pakd_qty),
                oli.do_dtl_status =
                    (case
                        when oli.order_qty - oli.allocated_qty <= 0
                        then
                            case
                            when oli.order_qty - (coalesce(oli.units_pakd, 0) + v_child_pakd_qty) <= 0
                                then 150
                            else 140
                            end
                         else 120
                    end)
            where oli.line_item_id = v_orig_line.line_item_id;

            v_pakd_qty := v_pakd_qty - v_child_pakd_qty;
        end loop;

        close orig_line;
    end loop;

    close agg_line;

    -- pkt_dtl update
    merge into pkt_dtl pd
    using 
    (  
        select t.pkt_ctrl_nbr, t.pkt_seq_nbr,
            (sum (t.qty_alloc) - sum (t.qty_pulld)) as pakd_qty
        from tmp_pack_mop_lpn_dtls t
        group by t.pkt_ctrl_nbr, t.pkt_seq_nbr
    ) td on (td.pkt_ctrl_nbr = pd.pkt_ctrl_nbr and td.pkt_seq_nbr = pd.pkt_seq_nbr)
    when matched then
    update set
        pd.user_id = p_login_user_id,
        pd.mod_date_time = v_curr_date_time,
        pd.wm_version_id = (coalesce (pd.wm_version_id, 0) + 1),
        pd.units_pakd = ( (coalesce (pd.units_pakd, 0) + td.pakd_qty)),
        pd.verf_as_pakd = ( (coalesce (pd.verf_as_pakd, 0) + td.pakd_qty)),
        pd.stat_code = 40;
    wm_log ('pkt_dtl update ' || sql%rowcount, p_debug_flag);

    -- task dtl update
    update task_dtl td
    set td.qty_pulld = td.qty_alloc,
        td.stat_code = 90,
        td.mod_date_time = v_curr_date_time,
        td.user_id = p_login_user_id,
        td.wm_version_id = (coalesce (td.wm_version_id, 0) + 1)
    where td.alloc_invn_dtl_id in (select alloc_invn_dtl_id from tmp_pack_mop_lpn_dtls)
        and td.cd_master_id = p_tc_company_id;
    wm_log ('task_dtl update ' || sql%rowcount, p_debug_flag);

    -- alloc_invn_dtl update
    update alloc_invn_dtl aid
    set aid.qty_pulld = aid.qty_alloc,
        aid.stat_code = 90,
        aid.mod_date_time = v_curr_date_time,
        aid.user_id = p_login_user_id,
        aid.wm_version_id = (coalesce (aid.wm_version_id, 0) + 1)
    where aid.stat_code < 90
        and aid.alloc_invn_dtl_id in
            (select alloc_invn_dtl_id from tmp_pack_mop_lpn_dtls)
        and aid.cd_master_id = p_tc_company_id;
    wm_log ('alloc_invn_dtl update ' || sql%rowcount, p_debug_flag);

    -- task header update 
    update task_hdr th
    set th.mod_date_time = v_curr_date_time,
        th.user_id = p_login_user_id,
        th.curr_user_id = null,
        th.wm_version_id = (coalesce (th.wm_version_id, null) + 1),
        th.stat_code =
            (case
                when exists
                (
                    select 1
                    from task_dtl
                    where task_dtl.task_id = th.task_id
                        and stat_code < 90
                )
                then th.stat_code else 90
             end)
    where th.task_id in
        (
            select task_id
            from task_dtl, tmp_pack_mop_lpn_dtls
            where task_dtl.alloc_invn_dtl_id = tmp_pack_mop_lpn_dtls.alloc_invn_dtl_id)
            and th.whse = p_whse;
    wm_log ('task_hdr update ' || sql%rowcount, p_debug_flag);
    
    select decode(wp.trk_prod_flag, 'Y', prod_trkg_tran_id_seq.nextval, null)
      into v_act_trk_tran_nbr
      from whse_parameters wp
      join facility f on f.facility_id = wp.whse_master_id
    where f.facility_id = p_facility_id;
  
    if (v_act_trk_tran_nbr is not null)
    then
        insert into prod_trkg_tran
        (
            tran_type, tran_code, whse, from_locn, to_locn, wkstn_id, sam, begin_date,
            end_date, stat_code, module_name, menu_optn_name, create_date_time,
            mod_date_time, user_id, plt_id, cd_master_id, wm_version_id, tran_nbr,
            prod_trkg_tran_id, seq_nbr, cntr_nbr, nbr_units,
            old_stat_code, new_stat_code, item_id, tc_order_id, line_item_id
        )
        select '500', '001', p_whse, null, null, up.prtr_reqstr, 0,
            v_curr_date_time, sysdate, 0, 'Receiving', 'Initiate ASN', sysdate, sysdate,
            p_login_user_id, p_plt_nbr, p_tc_company_id, 1, v_act_trk_tran_nbr,
            prod_trkg_tran_id_seq.nextval, rownum, tmp.carton_nbr,
            tmp.qty_alloc, 0, 20, tmp.item_id,
            tmp.tc_order_id, tmp.line_item_id
        from tmp_pack_mop_lpn_dtls tmp
        join user_profile up on up.login_user_id = p_login_user_id;
        WM_LOG('prod_trkg_tran insert: ' || SQL%ROWCOUNT, P_DEBUG_FLAG);
    end if;

    p_rc := 1;
    commit;
	
    wm_utils.set_context_info(p_module_name => v_prev_module_name,
        p_action_name => v_prev_action_name);
end;
/

show errors;
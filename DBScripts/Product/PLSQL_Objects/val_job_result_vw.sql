create or replace force view val_job_result_vw
(
    val_txn_id, job_txn_id, create_date_time, job_name, passed, msg, sql_ident, sql_text,
    pgm_id, bind_subs_sql_text, flow_name
)
as
select vrh.parent_txn_id val_txn_id, vrh.txn_id job_txn_id, vrh.create_date_time, vjh.job_name, 
    vrh.last_run_passed passed, vrh.msg, vs.ident sql_ident, vs.sql_text, vrh.pgm_id,
    wm_val_get_bind_subs_sql(vjd.val_job_dtl_id, vrh.txn_id, vs.sql_text) bind_subs_sql_text,
    vrh.app_hook flow_name
from val_result_hist vrh
join val_job_dtl vjd on vjd.val_job_dtl_id = vrh.val_job_dtl_id 
join val_job_hdr vjh on vjh.val_job_hdr_id = vjd.val_job_hdr_id
join val_sql vs on vs.sql_id = vjd.sql_id
/

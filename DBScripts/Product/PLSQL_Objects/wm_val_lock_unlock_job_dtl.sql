create or replace procedure wm_val_lock_unlock_job_dtl
(
    p_job_name  in val_job_hdr.job_name%type,
    p_ident     in val_sql.ident%type,
    p_lock      in val_sql.is_locked%type
)
as
    v_vjd_id    val_job_dtl.val_job_dtl_id%type;
begin
    update val_job_dtl vjd
    set vjd.is_locked = p_lock, vjd.mod_date_time = sysdate
    where exists
        (
            select 1
            from val_job_hdr vjh
            where vjh.val_job_hdr_id = vjd.val_job_hdr_id
                and vjh.job_name = p_job_name
        )
        and exists
        (
            select 1
            from val_sql vs
            where vs.sql_id = vjd.sql_id and vs.ident = p_ident
        )
    returning vjd.val_job_dtl_id into v_vjd_id;
    dbms_output.put_line(case when p_lock = 1 then 'Locked' else 'Unlocked' end 
        || ' job dtl ' || v_vjd_id || '(sql: ' || p_ident || ')');
    
    commit;
end;
/
show errors;

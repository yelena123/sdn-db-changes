create or replace PROCEDURE C_WM01_ALLOC_TRAILER_POS
(
  p_wave_nbr    IN wave_parm.wave_nbr%type,
  p_whse        IN wave_parm.whse%type,
  p_rc                  out number
)
as

	/*CURSOR C1 IS
	SELECT DISTINCT AID.TC_ORDER_ID, ALLOC_INVN_DTL_ID,QTY_ALLOC,AID.ITEM_ID,AID.LINE_ITEM_ID,STORAGE_TYPE from ALLOC_INVN_DTL AID , C_LOAD_DIAGRAM_ITEM_DETAIL CLD--, C_STORAGE_TYPE CLS
	WHERE AID.TASK_GENRTN_REF_NBR = p_wave_nbr AND AID.MISC_ALPHA_FIELD_1 IS Null AND stat_code = 0
	AND CLD.line_item_id = AID.line_item_id
	AND CLD.TC_ORDER_ID = AID.TC_ORDER_ID
	Order By TC_ORDER_ID,STORAGE_TYPE,ITEM_ID Desc;*/
  CURSOR C1 IS
  SELECT DISTINCT AID.TC_ORDER_ID, ALLOC_INVN_DTL_ID,QTY_ALLOC,AID.ITEM_ID,AID.LINE_ITEM_ID from ALLOC_INVN_DTL AID--, C_STORAGE_TYPE CLS
	WHERE AID.TASK_GENRTN_REF_NBR = p_wave_nbr AND AID.MISC_ALPHA_FIELD_1 IS Null AND stat_code = 0
	--AND CLD.line_item_id = AID.line_item_id
	--AND CLD.TC_ORDER_ID = AID.TC_ORDER_ID
	Order By TC_ORDER_ID,ITEM_ID Desc;

	REC1        	VARCHAR2(10);
	REC2        	VARCHAR2(10);
	v_remQtyOnPosn 	NUMBER(13,5);
	v_qtyAllocd 	NUMBER(13,5);
  v_alloCreated 	NUMBER(13,5);--added for accurate updates on CLDIT table when there is a shortage
	v_firstCrtnNumber  VARCHAR2(20);
	v_lstCrtnNumber  VARCHAR2(20);
	v_runningCarton_nbr Number;
	v_totalNbrCnts Number;
	v_currentOrder VARCHAR2(20);
	v_perOrdctnNbr VARCHAR2(20);
	v_cartonNbr VARCHAR2(20);
	v_prevCartonNbr VARCHAR2(20);
	v_prevTrlrPosn VARCHAR2(20);
	--v_currLpnDtlId Number;
	v_prevItemId Number;
	v_prevStorageType Number;
	v_allocationSearched Number;
	v_load_diagramIDs Number;
	v_nextCrtnNumber  VARCHAR2(20);
  v_clditProcessedQtyPerAID  Number;
  v_currAidAllocQty Number;
  v_originalQtyAllocd NUMBER(13,5);



BEGIN
 p_rc := 0;
 v_runningCarton_nbr :=0;
 v_totalNbrCnts := 0;
 v_perOrdctnNbr:= 'NULL';
 v_prevTrlrPosn := 'NULL';
 v_prevItemId := 0;
 v_prevStorageType :=0;
 v_clditProcessedQtyPerAID  :=0;
 v_currAidAllocQty :=0;

	BEGIN

		C_WM01_ITEM_SUBSTITUTION(p_wave_nbr,p_whse,p_rc);
		custom_load_diagram.exception_msg_log_insert('C_WM01_ITEM_SUBSTITUTION SP call for Wave Nbr :' || p_wave_nbr, 'C_WM01_ITEM_SUBSTITUTION', '','INFO');

	EXCEPTION WHEN OTHERS THEN
		custom_load_diagram.exception_msg_log_insert('Exception in C_WM01_ITEM_SUBSTITUTION function for Wave Nbr :' || p_wave_nbr ||
															   SQLCODE || SQLERRM,
															   'C_WM01_ITEM_SUBSTITUTION',
															   '',
															   'ERROR');

		p_rc := -1;
	END;

Begin


-- get the count of LPNs to be created  ---

INSERT INTO C_REYE_TMP_ALLOC
SELECT CLD.TC_ORDER_ID, CLD.LINE_ITEM_ID,  TRAILER_POSITION, NUM_CASES, CLD.STORAGE_TYPE, NVL(CLS.ONE_OLPN_PER_ITEM,'N') PER_ITEM, NVL(CLS.ONE_OLPN_PER_ORDER,'N') PER_ORDER
FROM C_LOAD_DIAGRAM_ITEM_DETAIL  CLD , ORDERS ORD, ORDER_LINE_ITEM OLI , C_LOAD_DIAGRAM_SUMMARY CLDS , C_STORAGE_TYPE CLS
WHERE  OLI.WAVE_NBR = p_wave_nbr
AND OLI.ORDER_ID = ORD.ORDER_ID
--AND ORD.TC_SHIPMENT_ID = CLDS.TC_SHIPMENT_ID
AND CLD.LOAD_DIAGRAM_ID = CLDS.LOAD_DIAGRAM_ID
AND OLI.line_item_id = CLD.line_item_id
AND ORD.TC_ORDER_ID = CLD.TC_ORDER_ID
AND CLS.STORAGE_TYPE_ID = CLD.STORAGE_TYPE;


Select NVL(Sum(NUM_CASES),0) Into v_runningCarton_nbr
From C_REYE_TMP_ALLOC Where PER_ITEM = 'Y';

v_totalNbrCnts := v_runningCarton_nbr + v_totalNbrCnts;


Select NVL(Sum(ABC.ORDERS),0)  into v_runningCarton_nbr
From (Select Count(Distinct TC_ORDER_ID) As ORDERS
FROM C_REYE_TMP_ALLOC
WHERE  PER_ORDER = 'Y'
--Group By LINE_ITEM_ID
) ABC;

v_totalNbrCnts := v_runningCarton_nbr + v_totalNbrCnts;

--'201206280268'


Select NVL(Sum(ABC.ORDERS),0)  into v_runningCarton_nbr
From (Select Count(Distinct TC_ORDER_ID) As ORDERS
FROM C_REYE_TMP_ALLOC
WHERE  PER_ITEM = 'N' AND PER_ORDER = 'N'
Group By trailer_position,tC_ORDER_ID,STORAGE_TYPE ) ABC;

v_totalNbrCnts := v_runningCarton_nbr + v_totalNbrCnts;

MANH_GET_NXTUP('*','P14',v_totalNbrCnts+1,p_rc,v_firstCrtnNumber,v_lstCrtnNumber);

v_runningCarton_nbr := 0;
v_allocationSearched := 0;
v_load_diagramIDs := 0;


FOR REC1 IN C1 LOOP

	v_allocationSearched := v_allocationSearched+1; --storing count for error hANDling

	v_qtyAllocd := REC1.QTY_ALLOC; --storing QTY_ALLOC into a variable
	v_originalQtyAllocd := REC1.QTY_ALLOC;

  v_currAidAllocQty := v_qtyAllocd;
  v_clditProcessedQtyPerAID := 0;
	<<INNER_LOOP>> FOR REC2 In
		(SELECT TRAILER_POSITION, NUM_CASES, ALLOCATED_CASES, LOAD_DIAGRAM_ITEM_DETAIL_ID,TC_ORDER_ID,STORAGE_TYPE,NVL(CLS.ONE_OLPN_PER_ITEM,'N') PER_ITEM ,
		NVL(CLS.ONE_OLPN_PER_ORDER,'N') PER_ORDER
		FROM C_LOAD_DIAGRAM_ITEM_DETAIL  CLDIT ,C_STORAGE_TYPE CLS
		WHERE CLDIT.ITEM_ID = REC1.ITEM_ID
		AND CLDIT.TC_ORDER_ID = REC1.TC_ORDER_ID
		AND CLDIT.LINE_ITEM_ID = REC1.LINE_ITEM_ID
		AND CLS.STORAGE_TYPE_ID = CLDIT.STORAGE_TYPE
		AND ALLOCATED_CASES < NUM_CASES
		order by CLDIT.CREATED_DTTM desc)

	Loop

		v_load_diagramIDs := v_load_diagramIDs+1; --storing count for error hANDling

    if REC2.ALLOCATED_CASES < 0 then  -- preventive check
    		v_remQtyOnPosn := (REC2.NUM_CASES );

    else
        v_remQtyOnPosn := (REC2.NUM_CASES - REC2.ALLOCATED_CASES);
    end if;

        v_clditProcessedQtyPerAID := v_clditProcessedQtyPerAID + v_remQtyOnPosn; -- Issue 695
		--  DBMS_OUTPUT.PUT_LINE('Order ' ||REC1.TC_ORDER_ID  || ' QTY Alloc ' || REC1.QTY_ALLOC || ' Item ' || REC1.ITEM_ID);
		--  DBMS_OUTPUT.PUT_LINE('Order ' ||REC2.TC_ORDER_ID  || ' QTY Alloc ' || REC2.NUM_CASES || ' NUM_CASES ' || REC2.NUM_CASES);

		IF v_qtyAllocd <= v_remQtyOnPosn  THEN -- IF AID.QTY_ALLOC = CLD.NUM_CASES - CLD.ALLOCATED_CASES OR LESSER

			IF REC2.PER_ORDER = 'Y' Then

				BEGIN

					SELECT DISTINCT NVL(CARTON_NBR,0) into v_prevCartonNbr from ALLOC_INVN_DTL AID
					where TC_ORDER_ID = REC2.TC_ORDER_ID AND MISC_ALPHA_FIELD_3 = REC2.STORAGE_TYPE
					AND AID.TASK_GENRTN_REF_NBR = P_WAVE_NBR;

					exception when others Then
					v_prevCartonNbr := 0;
				END;

				If v_prevCartonNbr = 0 Then
				v_runningCarton_nbr := v_runningCarton_nbr  + 1;
				v_prevCartonNbr := UCCFormat( v_firstCrtnNumber + v_runningCarton_nbr ,p_whse);
				End If;

				UPDATE ALLOC_INVN_DTL AID Set
				CARTON_NBR = v_prevCartonNbr,
				TASK_CMPL_REF_NBR = v_prevCartonNbr,
				MISC_ALPHA_FIELD_1 = REC2.TRAILER_POSITION, -- issues undo wave
				MISC_ALPHA_FIELD_3 = REC2.STORAGE_TYPE,  -- issues 391 temporarily using the field for storing STORAGE_TYPE
				carton_seq_nbr = lpn_detail_id_seq.nextval
				WHERE AID.ALLOC_INVN_DTL_ID = REC1.ALLOC_INVN_DTL_ID;

				UPDATE C_LOAD_DIAGRAM_ITEM_DETAIL CLDIT
				SET CLDIT.ALLOCATED_CASES = CLDIT.ALLOCATED_CASES + v_qtyAllocd
				WHERE CLDIT.LOAD_DIAGRAM_ITEM_DETAIL_ID = REC2.LOAD_DIAGRAM_ITEM_DETAIL_ID;

				Exit INNER_LOOP;
			End If;
			IF REC2.PER_ITEM  = 'Y' Then
        v_alloCreated:=0;--added for accurate updates on CLDIT table when there is a shortage
				For I In 2 .. v_remQtyOnPosn
				Loop
				if v_qtyAllocd<=1 then
			        Exit ;
			        end if;
					v_cartonNbr := UCCFormat( v_firstCrtnNumber + v_runningCarton_nbr + I,p_whse);

					INSERT INTO ALLOC_INVN_DTL (ALLOC_INVN_DTL_ID, ALLOC_UOM_QTY,QTY_ALLOC,MISC_ALPHA_FIELD_1,
									WHSE,INVN_TYPE,PROD_STAT,BATCH_NBR,SKU_ATTR_1,SKU_ATTR_2,SKU_ATTR_3,SKU_ATTR_4,SKU_ATTR_5,CNTRY_OF_ORGN,ALLOC_INVN_CODE,CNTR_NBR,TRANS_INVN_TYPE,PULL_LOCN_ID,INVN_NEED_TYPE,TASK_TYPE,TASK_PRTY,
									TASK_BATCH,ALLOC_UOM,QTY_PULLD,FULL_CNTR_ALLOCD,ORIG_REQMT,DEST_LOCN_ID,TASK_GENRTN_REF_CODE,TASK_GENRTN_REF_NBR,TASK_CMPL_REF_CODE,TASK_CMPL_REF_NBR,ERLST_START_DATE_TIME,
									LTST_START_DATE_TIME,LTST_CMPL_DATE_TIME,NEED_ID,STAT_CODE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PKT_CTRL_NBR,REQD_INVN_TYPE,REQD_PROD_STAT,REQD_BATCH_NBR,REQD_SKU_ATTR_1,REQD_SKU_ATTR_2,REQD_SKU_ATTR_3,
									REQD_SKU_ATTR_4,REQD_SKU_ATTR_5,REQD_CNTRY_OF_ORGN,PKT_SEQ_NBR,CARTON_NBR,CARTON_SEQ_NBR,PIKR_NBR,PULL_LOCN_SEQ_NBR,DEST_LOCN_SEQ_NBR,TASK_CMPL_REF_NBR_SEQ,SUBSTITUTION_FLAG,MISC_ALPHA_FIELD_2,
									MISC_ALPHA_FIELD_3,CD_MASTER_ID,RESEQ_ALLOC,ITEM_ID,WM_VERSION_ID,TC_ORDER_ID,LINE_ITEM_ID)

					SELECT ALLOC_INVN_DTL_ID_SEQ.NEXTVAL,ALLOC_UOM_QTY,1,REC2.TRAILER_POSITION,
									WHSE,INVN_TYPE,PROD_STAT,BATCH_NBR,SKU_ATTR_1,SKU_ATTR_2,SKU_ATTR_3,SKU_ATTR_4,SKU_ATTR_5,CNTRY_OF_ORGN,ALLOC_INVN_CODE,CNTR_NBR,TRANS_INVN_TYPE,PULL_LOCN_ID,INVN_NEED_TYPE,TASK_TYPE,TASK_PRTY,
									TASK_BATCH,ALLOC_UOM,QTY_PULLD,'N',ORIG_REQMT,DEST_LOCN_ID,TASK_GENRTN_REF_CODE,TASK_GENRTN_REF_NBR,TASK_CMPL_REF_CODE,v_cartonNbr,ERLST_START_DATE_TIME,
									LTST_START_DATE_TIME,LTST_CMPL_DATE_TIME,NEED_ID,STAT_CODE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PKT_CTRL_NBR,REQD_INVN_TYPE,REQD_PROD_STAT,REQD_BATCH_NBR,REQD_SKU_ATTR_1,REQD_SKU_ATTR_2,REQD_SKU_ATTR_3,
									REQD_SKU_ATTR_4,REQD_SKU_ATTR_5,REQD_CNTRY_OF_ORGN,PKT_SEQ_NBR,v_cartonNbr,lpn_detail_id_seq.nextval,PIKR_NBR,PULL_LOCN_SEQ_NBR,DEST_LOCN_SEQ_NBR,TASK_CMPL_REF_NBR_SEQ,SUBSTITUTION_FLAG,MISC_ALPHA_FIELD_2,
									REC2.STORAGE_TYPE,CD_MASTER_ID,RESEQ_ALLOC,ITEM_ID,WM_VERSION_ID,TC_ORDER_ID,LINE_ITEM_ID
					FROM ALLOC_INVN_DTL
					WHERE ALLOC_INVN_DTL_ID = REC1.ALLOC_INVN_DTL_ID;
				v_qtyAllocd := v_qtyAllocd - 1;  -- decrementing the QTY_ALLOC
        v_alloCreated:=v_alloCreated + 1;
				End Loop;

				v_cartonNbr := UCCFormat( v_firstCrtnNumber + v_runningCarton_nbr + 1,p_whse);
				UPDATE ALLOC_INVN_DTL AID
				SET AID.MISC_ALPHA_FIELD_1 = REC2.TRAILER_POSITION,
				MISC_ALPHA_FIELD_3 = REC2.STORAGE_TYPE,  -- issues 391 temporarily using the field for storing STORAGE_TYPE
				AID.QTY_ALLOC = 1,
				CARTON_NBR = v_cartonNbr,
				TASK_CMPL_REF_NBR = v_cartonNbr,
				carton_seq_nbr = lpn_detail_id_seq.nextval
				WHERE AID.ALLOC_INVN_DTL_ID = REC1.ALLOC_INVN_DTL_ID;
        --
        v_qtyAllocd := v_qtyAllocd - 1;
        v_alloCreated:=v_alloCreated + 1;

				v_runningCarton_nbr := v_runningCarton_nbr + v_remQtyOnPosn;

        Update C_LOAD_DIAGRAM_ITEM_DETAIL CLDIT
				SET CLDIT.ALLOCATED_CASES = CLDIT.ALLOCATED_CASES + v_alloCreated--changed for accurate updates on CLDIT table when there is a shortage
				WHERE CLDIT.LOAD_DIAGRAM_ITEM_DETAIL_ID = REC2.LOAD_DIAGRAM_ITEM_DETAIL_ID;
        --after modifying the wave allocation if there is no allocated qty end the process. 
        if v_qtyAllocd<=0 then
			        Exit INNER_LOOP;
        end if;

			ELSE

				BEGIN

					SELECT DISTINCT NVL(CARTON_NBR,0) into v_prevCartonNbr from ALLOC_INVN_DTL AID
					where AID.MISC_ALPHA_FIELD_1 = REC2.TRAILER_POSITION
					AND AID.MISC_ALPHA_FIELD_3 = REC2.STORAGE_TYPE
					AND TC_ORDER_ID = REC2.TC_ORDER_ID AND AID.TASK_GENRTN_REF_NBR = P_WAVE_NBR;


					EXCEPTION WHEN OTHERS THEN
					v_prevCartonNbr := 0;
				END;

				If v_prevCartonNbr = 0 Then
					v_runningCarton_nbr := v_runningCarton_nbr  + 1;
					v_prevCartonNbr := UCCFormat( v_firstCrtnNumber + v_runningCarton_nbr ,p_whse);
				End If;

				UPDATE ALLOC_INVN_DTL AID
				SET AID.MISC_ALPHA_FIELD_1 = REC2.TRAILER_POSITION,
				MISC_ALPHA_FIELD_3 = REC2.STORAGE_TYPE,  -- issues 391 temporarily using the field for storing STORAGE_TYPE
				AID.QTY_ALLOC = v_qtyAllocd,
				CARTON_NBR = v_prevCartonNbr,
				CARTON_SEQ_NBR = lpn_detail_id_seq.nextval,
				TASK_CMPL_REF_NBR = v_prevCartonNbr
				WHERE AID.ALLOC_INVN_DTL_ID = REC1.ALLOC_INVN_DTL_ID;

			--DBMS_OUTPUT.PUT_LINE('CARTON_NBR ' ||v_prevCartonNbr || ' MISC_ALPHA_FIELD_1 ' || REC2.TRAILER_POSITION );

				Update C_LOAD_DIAGRAM_ITEM_DETAIL CLDIT
				SET CLDIT.ALLOCATED_CASES = CLDIT.ALLOCATED_CASES + v_qtyAllocd
				WHERE CLDIT.LOAD_DIAGRAM_ITEM_DETAIL_ID = REC2.LOAD_DIAGRAM_ITEM_DETAIL_ID;

				v_prevTrlrPosn  := REC2.TRAILER_POSITION;
				v_prevItemId := REC1.ITEM_ID;
				v_prevStorageType :=  REC2.STORAGE_TYPE;
				v_currentOrder := REC1.TC_ORDER_ID;

				EXIT INNER_LOOP;
			End If;

		Else	-- IF AID.QTY_ALLOC > CLD.NUM_CASES - CLD.ALLOCATED_CASES

			IF REC2.PER_ORDER = 'Y' Then

				BEGIN
					SELECT DISTINCT NVL(CARTON_NBR,0) into v_prevCartonNbr from ALLOC_INVN_DTL AID
					where TC_ORDER_ID = REC2.TC_ORDER_ID AND MISC_ALPHA_FIELD_3 = REC2.STORAGE_TYPE
					AND AID.TASK_GENRTN_REF_NBR = P_WAVE_NBR;


					EXCEPTION WHEN OTHERS THEN
						v_prevCartonNbr := 0;
				END;

				If v_prevCartonNbr = 0 Then
					v_runningCarton_nbr := v_runningCarton_nbr  + 1;
					v_prevCartonNbr := UCCFormat( v_firstCrtnNumber + v_runningCarton_nbr ,p_whse);
				End If;

				UPDATE ALLOC_INVN_DTL AID
				SET CARTON_NBR = v_prevCartonNbr,
				TASK_CMPL_REF_NBR = v_prevCartonNbr,
				MISC_ALPHA_FIELD_1 = REC2.TRAILER_POSITION, -- issues undo wave
				MISC_ALPHA_FIELD_3 = REC2.STORAGE_TYPE, -- issues 391 temporarily using the field for storing STORAGE_TYPE
				carton_seq_nbr = lpn_detail_id_seq.nextval
				WHERE AID.ALLOC_INVN_DTL_ID = REC1.ALLOC_INVN_DTL_ID;

				UPDATE C_LOAD_DIAGRAM_ITEM_DETAIL CLDIT
				SET CLDIT.ALLOCATED_CASES = CLDIT.ALLOCATED_CASES + v_qtyAllocd
				WHERE CLDIT.LOAD_DIAGRAM_ITEM_DETAIL_ID = REC2.LOAD_DIAGRAM_ITEM_DETAIL_ID;

				Exit INNER_LOOP;
			End If;

			If REC2.PER_ITEM  = 'Y' Then
        v_alloCreated:=0;--added for accurate updates on CLDIT table when there is a shortage
				For I In 1 .. v_remQtyOnPosn
				Loop
				if v_qtyAllocd<=0 then
        			Exit ;
        			end if;

				INSERT INTO ALLOC_INVN_DTL (ALLOC_INVN_DTL_ID, ALLOC_UOM_QTY,QTY_ALLOC,MISC_ALPHA_FIELD_1,
													WHSE,INVN_TYPE,PROD_STAT,BATCH_NBR,SKU_ATTR_1,SKU_ATTR_2,SKU_ATTR_3,SKU_ATTR_4,SKU_ATTR_5,CNTRY_OF_ORGN,ALLOC_INVN_CODE,CNTR_NBR,TRANS_INVN_TYPE,PULL_LOCN_ID,INVN_NEED_TYPE,TASK_TYPE,TASK_PRTY,
													TASK_BATCH,ALLOC_UOM,QTY_PULLD,FULL_CNTR_ALLOCD,ORIG_REQMT,DEST_LOCN_ID,TASK_GENRTN_REF_CODE,TASK_GENRTN_REF_NBR,TASK_CMPL_REF_CODE,TASK_CMPL_REF_NBR,ERLST_START_DATE_TIME,
													LTST_START_DATE_TIME,LTST_CMPL_DATE_TIME,NEED_ID,STAT_CODE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PKT_CTRL_NBR,REQD_INVN_TYPE,REQD_PROD_STAT,REQD_BATCH_NBR,REQD_SKU_ATTR_1,REQD_SKU_ATTR_2,REQD_SKU_ATTR_3,
													REQD_SKU_ATTR_4,REQD_SKU_ATTR_5,REQD_CNTRY_OF_ORGN,PKT_SEQ_NBR,CARTON_NBR,CARTON_SEQ_NBR,PIKR_NBR,PULL_LOCN_SEQ_NBR,DEST_LOCN_SEQ_NBR,TASK_CMPL_REF_NBR_SEQ,SUBSTITUTION_FLAG,MISC_ALPHA_FIELD_2,
													MISC_ALPHA_FIELD_3,CD_MASTER_ID,RESEQ_ALLOC,ITEM_ID,WM_VERSION_ID,TC_ORDER_ID,LINE_ITEM_ID)

					SELECT ALLOC_INVN_DTL_ID_SEQ.NEXTVAL,ALLOC_UOM_QTY,1,REC2.TRAILER_POSITION,
													WHSE,INVN_TYPE,PROD_STAT,BATCH_NBR,SKU_ATTR_1,SKU_ATTR_2,SKU_ATTR_3,SKU_ATTR_4,SKU_ATTR_5,CNTRY_OF_ORGN,ALLOC_INVN_CODE,CNTR_NBR,TRANS_INVN_TYPE,PULL_LOCN_ID,INVN_NEED_TYPE,TASK_TYPE,TASK_PRTY,
													TASK_BATCH,ALLOC_UOM,QTY_PULLD,'N',ORIG_REQMT,DEST_LOCN_ID,TASK_GENRTN_REF_CODE,TASK_GENRTN_REF_NBR,TASK_CMPL_REF_CODE,UCCFormat( v_firstCrtnNumber + v_runningCarton_nbr + I,p_whse),ERLST_START_DATE_TIME,
													LTST_START_DATE_TIME,LTST_CMPL_DATE_TIME,NEED_ID,STAT_CODE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PKT_CTRL_NBR,REQD_INVN_TYPE,REQD_PROD_STAT,REQD_BATCH_NBR,REQD_SKU_ATTR_1,REQD_SKU_ATTR_2,REQD_SKU_ATTR_3,
													REQD_SKU_ATTR_4,REQD_SKU_ATTR_5,REQD_CNTRY_OF_ORGN,PKT_SEQ_NBR,UCCFormat( v_firstCrtnNumber + v_runningCarton_nbr + I,p_whse),lpn_detail_id_seq.nextval,PIKR_NBR,PULL_LOCN_SEQ_NBR,DEST_LOCN_SEQ_NBR,TASK_CMPL_REF_NBR_SEQ,SUBSTITUTION_FLAG,MISC_ALPHA_FIELD_2,
													REC2.STORAGE_TYPE,CD_MASTER_ID,RESEQ_ALLOC,ITEM_ID,WM_VERSION_ID,TC_ORDER_ID,LINE_ITEM_ID
					FROM ALLOC_INVN_DTL
					WHERE ALLOC_INVN_DTL_ID = REC1.ALLOC_INVN_DTL_ID;
				v_qtyAllocd := v_qtyAllocd - 1;  -- decrementing the QTY_ALLOC
        v_alloCreated:=v_alloCreated +1;
				End Loop;
				v_runningCarton_nbr := v_runningCarton_nbr + v_remQtyOnPosn;

			Else
				BEGIN

					SELECT DISTINCT NVL(CARTON_NBR,0) into v_prevCartonNbr from ALLOC_INVN_DTL AID
					where AID.MISC_ALPHA_FIELD_1 = REC2.TRAILER_POSITION
					AND AID.MISC_ALPHA_FIELD_3 = REC2.STORAGE_TYPE
					AND TC_ORDER_ID = REC2.TC_ORDER_ID AND AID.TASK_GENRTN_REF_NBR = P_WAVE_NBR;

					exception when others Then
					v_prevCartonNbr := 0;
				END;

				If v_prevCartonNbr = 0 Then
					v_runningCarton_nbr := v_runningCarton_nbr  + 1;
					v_prevCartonNbr := UCCFormat( v_firstCrtnNumber + v_runningCarton_nbr ,p_whse);
				End If;

				INSERT INTO ALLOC_INVN_DTL (ALLOC_INVN_DTL_ID, ALLOC_UOM_QTY,QTY_ALLOC,MISC_ALPHA_FIELD_1,
										WHSE,INVN_TYPE,PROD_STAT,BATCH_NBR,SKU_ATTR_1,SKU_ATTR_2,SKU_ATTR_3,SKU_ATTR_4,SKU_ATTR_5,CNTRY_OF_ORGN,ALLOC_INVN_CODE,CNTR_NBR,TRANS_INVN_TYPE,PULL_LOCN_ID,INVN_NEED_TYPE,TASK_TYPE,TASK_PRTY,
										TASK_BATCH,ALLOC_UOM,QTY_PULLD,FULL_CNTR_ALLOCD,ORIG_REQMT,DEST_LOCN_ID,TASK_GENRTN_REF_CODE,TASK_GENRTN_REF_NBR,TASK_CMPL_REF_CODE,TASK_CMPL_REF_NBR,ERLST_START_DATE_TIME,
										LTST_START_DATE_TIME,LTST_CMPL_DATE_TIME,NEED_ID,STAT_CODE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PKT_CTRL_NBR,REQD_INVN_TYPE,REQD_PROD_STAT,REQD_BATCH_NBR,REQD_SKU_ATTR_1,REQD_SKU_ATTR_2,REQD_SKU_ATTR_3,
										REQD_SKU_ATTR_4,REQD_SKU_ATTR_5,REQD_CNTRY_OF_ORGN,PKT_SEQ_NBR,CARTON_NBR,CARTON_SEQ_NBR,PIKR_NBR,PULL_LOCN_SEQ_NBR,DEST_LOCN_SEQ_NBR,TASK_CMPL_REF_NBR_SEQ,SUBSTITUTION_FLAG,MISC_ALPHA_FIELD_2,
										MISC_ALPHA_FIELD_3,CD_MASTER_ID,RESEQ_ALLOC,ITEM_ID,WM_VERSION_ID,TC_ORDER_ID,LINE_ITEM_ID)

				SELECT ALLOC_INVN_DTL_ID_SEQ.NEXTVAL,ALLOC_UOM_QTY,v_remQtyOnPosn,REC2.TRAILER_POSITION,
										WHSE,INVN_TYPE,PROD_STAT,BATCH_NBR,SKU_ATTR_1,SKU_ATTR_2,SKU_ATTR_3,SKU_ATTR_4,SKU_ATTR_5,CNTRY_OF_ORGN,ALLOC_INVN_CODE,CNTR_NBR,TRANS_INVN_TYPE,PULL_LOCN_ID,INVN_NEED_TYPE,TASK_TYPE,TASK_PRTY,
										TASK_BATCH,ALLOC_UOM,QTY_PULLD,'N',ORIG_REQMT,DEST_LOCN_ID,TASK_GENRTN_REF_CODE,TASK_GENRTN_REF_NBR,TASK_CMPL_REF_CODE,v_prevCartonNbr,ERLST_START_DATE_TIME,
										LTST_START_DATE_TIME,LTST_CMPL_DATE_TIME,NEED_ID,STAT_CODE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PKT_CTRL_NBR,REQD_INVN_TYPE,REQD_PROD_STAT,REQD_BATCH_NBR,REQD_SKU_ATTR_1,REQD_SKU_ATTR_2,REQD_SKU_ATTR_3,
										REQD_SKU_ATTR_4,REQD_SKU_ATTR_5,REQD_CNTRY_OF_ORGN,PKT_SEQ_NBR,v_prevCartonNbr,lpn_detail_id_seq.nextval,PIKR_NBR,PULL_LOCN_SEQ_NBR,DEST_LOCN_SEQ_NBR,TASK_CMPL_REF_NBR_SEQ,SUBSTITUTION_FLAG,MISC_ALPHA_FIELD_2,
										REC2.STORAGE_TYPE,CD_MASTER_ID,RESEQ_ALLOC,ITEM_ID,WM_VERSION_ID,TC_ORDER_ID,LINE_ITEM_ID
				FROM ALLOC_INVN_DTL
				WHERE ALLOC_INVN_DTL_ID = REC1.ALLOC_INVN_DTL_ID;
				 v_qtyAllocd := v_qtyAllocd - v_remQtyOnPosn;  -- decrementing the QTY_ALLOC
        v_alloCreated:=v_remQtyOnPosn; --Allocations created here is as v_remQtyOnPosn as per design changing the location of this statement in code to bringin shrtage related logic 
			--  DBMS_OUTPUT.PUT_LINE('Order ' ||REC1.ALLOC_INVN_DTL_ID || ' QTY Alloc ' || v_remQtyOnPosn || ' Item ' || ALLOC_INVN_DTL_ID_SEQ.Currval);
			--	DBMS_OUTPUT.PUT_LINE('CARTON_NBR ' ||v_prevCartonNbr || ' MISC_ALPHA_FIELD_1 ' || REC2.TRAILER_POSITION );

			End IF;

      		UPDATE C_LOAD_DIAGRAM_ITEM_DETAIL CLDIT
	    	SET CLDIT.ALLOCATED_CASES = CLDIT.ALLOCATED_CASES + v_alloCreated--changed for accurate updates on CLDIT table when there is a shortage
	      	WHERE CLDIT.LOAD_DIAGRAM_ITEM_DETAIL_ID = REC2.LOAD_DIAGRAM_ITEM_DETAIL_ID;

		END IF;

		v_prevTrlrPosn  := REC2.TRAILER_POSITION;
		v_prevItemId := REC1.ITEM_ID;
		v_currentOrder := REC1.TC_ORDER_ID;
		v_prevStorageType :=  REC2.STORAGE_TYPE;

		custom_load_diagram.exception_msg_log_insert(' C_WM01_ALLOC_TRAILER_POS function,  Custom details processed :  ' || v_load_diagramIDs ||
                                                           SQLCODE || SQLERRM,
                                                           'C_WM01_ALLOC_TRAILER_POS',
                                                           '',
                                                           'ERROR');
	END LOOP INNER_LOOP;
  if v_currAidAllocQty !=  v_clditProcessedQtyPerAID  then  -- issue fix for 695
     		UPDATE ALLOC_INVN_DTL AID
				SET AID.QTY_ALLOC = v_qtyAllocd
				WHERE AID.ALLOC_INVN_DTL_ID = REC1.ALLOC_INVN_DTL_ID;
  end if;
  
   if v_originalQtyAllocd !=  v_qtyAllocd  then  -- 
     		UPDATE ALLOC_INVN_DTL AID
				SET AID.FULL_CNTR_ALLOCD = 'N'
				WHERE AID.ALLOC_INVN_DTL_ID = REC1.ALLOC_INVN_DTL_ID AND AID.FULL_CNTR_ALLOCD = 'Y';
  end if; 

	custom_load_diagram.exception_msg_log_insert(' C_WM01_ALLOC_TRAILER_POS function, Allocations Searched :  ' || v_allocationSearched ||
                                                           SQLCODE || SQLERRM,
                                                           'C_WM01_ALLOC_TRAILER_POS',
                                                           '',
                                                           'ERROR');
	IF(v_runningCarton_nbr > v_totalNbrCnts) THEN

		custom_load_diagram.exception_msg_log_insert( p_wave_nbr ||'C_WM01_ALLOC_TRAILER_POS function, Created more cartons than expected.' || p_wave_nbr || '  Expected ---> ' || v_totalNbrCnts ||'  Created ----->' || v_runningCarton_nbr ,'C_WM01_ALLOC_TRAILER_POS', '', 'ERROR');
		v_totalNbrCnts :=  v_runningCarton_nbr - v_totalNbrCnts;
		MANH_GET_NXTUP('*','P14',v_totalNbrCnts+1,p_rc,v_nextCrtnNumber,v_lstCrtnNumber);

	END IF;

END LOOP;
END;


-- UPDATE ALLOC_INVN_DTL AID
-- SET MISC_ALPHA_FIELD_2 =
-- (
-- SELECT DISTINCT SHPT.PROD_SCHED_REF_NUMBER FROM SHIPMENT SHPT , ORDERS ORDS
-- WHERE ((ORDS.TC_SHIPMENT_ID  = SHPT.TC_SHIPMENT_ID) OR (EXISTS ( SELECT 1 FROM LPN L WHERE L.ORDER_ID=ORDS.ORDER_ID AND SHPT.TC_SHIPMENT_ID=L.TC_SHIPMENT_ID)))
-- AND ORDS.TC_ORDER_ID  = AID.TC_ORDER_ID
-- AND AID.CARTON_NBR IS NOT null
-- )
-- Where AID.Task_Genrtn_Ref_Nbr = p_wave_nbr
-- AND AID.CARTON_NBR IS NOT null;

--SF case 4528050 WM-138184: Chase wave performance issue
DECLARE CURSOR CREFNBR
IS
  (SELECT DISTINCT AID.ALLOC_INVN_DTL_ID,
    SHPT.PROD_SCHED_REF_NUMBER
  FROM SHIPMENT SHPT,
    ORDERS ORDS,
    ALLOC_INVN_DTL AID
  WHERE ORDS.SHIPMENT_ID   = SHPT.SHIPMENT_ID
  AND ORDS.TC_ORDER_ID        = AID.TC_ORDER_ID
  AND AID.CARTON_NBR         IS NOT NULL
  AND AID.TASK_GENRTN_REF_NBR = p_wave_nbr
UNION
SELECT DISTINCT AID.ALLOC_INVN_DTL_ID,
  SHPT.PROD_SCHED_REF_NUMBER
FROM SHIPMENT SHPT,
  ORDERS ORDS,
  ALLOC_INVN_DTL AID
WHERE EXISTS
  (SELECT 1
  FROM LPN L
  WHERE L.ORDER_ID            = ORDS.ORDER_ID
  AND SHPT.SHIPMENT_ID     = L.SHIPMENT_ID
  AND AID.TASK_GENRTN_REF_NBR = L.WAVE_NBR
  )
AND ORDS.TC_ORDER_ID        = AID.TC_ORDER_ID
AND AID.CARTON_NBR         IS NOT NULL
AND AID.TASK_GENRTN_REF_NBR = p_wave_nbr);
BEGIN
  FOR REC1 IN CREFNBR
  LOOP
    UPDATE ALLOC_INVN_DTL
    SET MISC_ALPHA_FIELD_2  = REC1.PROD_SCHED_REF_NUMBER
    WHERE ALLOC_INVN_DTL_ID = REC1.ALLOC_INVN_DTL_ID;
   COMMIT;
  END LOOP;
END;

COMMIT;

EXCEPTION WHEN OTHERS THEN

custom_load_diagram.exception_msg_log_insert('Exception in C_WM01_ALLOC_TRAILER_POS function for Wave Nbr :' || p_wave_nbr ||
                                                           SQLCODE || SQLERRM,
                                                           'C_WM01_ALLOC_TRAILER_POS',
                                                           '',
                                                           'ERROR');
 p_rc := -1;

END;
/
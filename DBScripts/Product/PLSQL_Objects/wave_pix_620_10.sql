create or replace procedure wave_pix_620_10
(
    p_ship_wave_nbr     in  varchar2,
    p_whse              in  varchar2,
    p_cd_master_id      in  number,
    p_user_id           in  varchar2,
    p_rc                out number
)
is
    v_xml_group_id      varchar2(10)   default '0';
    v_proc_stat_code    varchar2(2);
    v_facility_id       number(14);
    v_tran_nbr          number(9);
begin    
    p_rc := 0;
        
    select facility_id 
        into v_facility_id
    from facility
        where whse = p_whse  
        and mark_for_deletion = 0
        and rownum < 2;

    manh_get_pix_config_data('620', '10', '01', p_cd_master_id, v_proc_stat_code, v_xml_group_id);     
                         
    insert into tmp_wave_pix_620_10_01
    (
        tc_lpn_id, tc_parent_lpn_id, invn_lock_code, id
    )
    select ll.tc_lpn_id, l.tc_parent_lpn_id, ll.inventory_lock_code, 
        case when tc_parent_lpn_id is null then
            row_number() over(partition by l.tc_lpn_id order by
            to_char(nvl(trim(substr(sc.misc_flags, 3, 2)), '99')))
        else row_number() over(partition by l.tc_parent_lpn_id order by
            to_char(nvl(trim(substr(sc.misc_flags, 3, 2)), '99')))
        end id
    from lpn_lock ll
    join lpn l on l.lpn_id = ll.lpn_id
    join sys_code sc on sc.code_id = ll.inventory_lock_code
    join
    (
        select aid.cntr_nbr, aid.cd_master_id
        from alloc_invn_dtl aid
            where aid.whse = p_whse
            and aid.task_genrtn_ref_nbr = p_ship_wave_nbr
            and aid.task_genrtn_ref_code in ('1', '44') 
            and aid.stat_code <= 91
    ) t on (t.cntr_nbr in (l.tc_lpn_id, l.tc_parent_lpn_id) and t.cd_master_id = l.tc_company_id)
    where l.c_facility_id = v_facility_id and l.inbound_outbound_indicator = 'I'
        and l.lpn_facility_status in (45, 50) and sc.rec_type = 'B'
        and sc.code_type = '527'
        and nvl(substr(sc.misc_flags, 2, 1), 'N') = 'N';
    wm_log('Collected lpn lock data for 620 01 05 ' || sql%rowcount,
        p_ref_code_1 => 'PIX', p_ref_value_1 => p_ship_wave_nbr);
        
    select pix_tran_id_seq.nextval 
    into v_tran_nbr
    from dual;
    insert into pix_tran
    (
        pix_tran_id,tran_type, tran_code, actn_code, tran_nbr, pix_seq_nbr,
        proc_stat_code, whse, tc_company_id, create_date_time, 
        mod_date_time, user_id, item_id, sys_user_id, invn_adjmt_type,
        invn_adjmt_qty, uom, style, style_sfx, season, season_yr, qual,
        sec_dim, color, color_sfx, size_desc, xml_group_id, invn_type, 
        prod_stat, cntry_of_orgn, batch_nbr, sku_attr_1, sku_attr_2, 
        sku_attr_3, sku_attr_4, sku_attr_5, ref_code_id_1, ref_field_1,
        size_range_code,size_rel_posn_in_table, company_code, facility_id,
        item_name
    )
    select v_tran_nbr,'620' tran_type, '10' tran_code,
        '01' actn_code,v_tran_nbr, 
        rownum pix_seq_nbr,
        cast(v_proc_stat_code as int), p_whse, aid.cd_master_id, current_timestamp, 
        current_timestamp, p_user_id, ic.item_id, p_user_id , 
        'A' invn_adjmt_type, sum(aid.qty_alloc) invn_adjmt_qty, 
        aid.alloc_uom, ic.item_style, ic.item_style_sfx, ic.item_season,
        ic.item_season_year, ic.item_quality, ic.item_second_dim, 
        ic.item_color, ic.item_color_sfx, ic.item_size_desc, v_xml_group_id,
        aid.invn_type, aid.prod_stat, aid.cntry_of_orgn, aid.batch_nbr, 
        aid.sku_attr_1, aid.sku_attr_2, aid.sku_attr_3, aid.sku_attr_4, 
        aid.sku_attr_5, '25' ref_code_id_1, t.invn_lock_code ref_field_1,
        iw.size_range_code, iw.size_rel_posn_in_table, c.company_code, 
        v_facility_id, ic.item_name
    from alloc_invn_dtl aid
    join invn_need_type i on i.invn_need_type = aid.invn_need_type
        and i.whse = aid.whse and i.cd_master_id = aid.cd_master_id
    join item_cbo ic on ic.item_id = aid.item_id 
    join item_wms iw on iw.item_id = ic.item_id 
    join tmp_wave_pix_620_10_01 t
        on (t.tc_lpn_id = aid.cntr_nbr or t.tc_parent_lpn_id = aid.cntr_nbr)
    join company c on c.company_id = ic.company_id     
    where aid.whse = p_whse
        and aid.task_genrtn_ref_nbr = p_ship_wave_nbr
        and aid.task_genrtn_ref_code in ('1', '44')
        and aid.stat_code <=91 and t.id = 1
        and (i.invn_need_type in (53, 54) or i.task_cmpl_corr_upd = 6)
    group by aid.cntry_of_orgn ,c.company_id , aid.item_id, aid.alloc_uom, 
        aid.sku_attr_1, aid.sku_attr_2, aid.sku_attr_3, aid.sku_attr_4, 
        aid.sku_attr_5, ic.item_season, ic.item_season_year, 
        ic.item_style, ic.item_style_sfx, ic.item_color, ic.item_color_sfx,
        ic.item_second_dim, ic.item_quality, ic.item_size_desc, ic.item_id,      
        iw.size_range_code, iw.size_rel_posn_in_table , aid.invn_type, 
        aid.prod_stat, aid.batch_nbr ,aid.cd_master_id, c.company_code, 
        t.invn_lock_code, ic.item_name;
    wm_log('Released 620 10 01 ' || sql%rowcount, p_ref_code_1 => 'PIX',
            p_ref_value_1 => p_ship_wave_nbr);
exception
    when no_data_found then
        return;
end wave_pix_620_10;
/
show errors;

create or replace procedure manh_wave_load_orders_for_rule
(
    p_wave_by_rule          in varchar2,
    p_list_of_shpmts        in varchar2,
    p_list_of_orders        in varchar2,
    p_facility_id           in whse_master.whse_master_id%type,
    p_perf_pickng_wave      in ship_wave_parm.perf_pickng_wave%type,
    p_perf_rte              in ship_wave_parm.perf_rte%type,
    p_preview_wave_flag     in number,
    p_wave_type_indic       in wave_parm.wave_type_indic%type,
    p_tc_company_id         in wave_parm.tc_company_id%type,
    p_pull_all_swc          in wave_parm.pull_all_swc%type,
    p_rule_id               in wave_rule_parm.rule_id%type,
    p_rule_type             in rule_hdr.rule_type%type,
    p_max_qty_per_rule      in wave_rule_parm.units_capcty%type,
    p_max_lines_per_rule    in wave_rule_parm.max_order_lines%type,
    p_is_wave_by_shpmt      in varchar2,
    p_max_orders_per_rule   in wave_rule_parm.max_orders%type,
    p_user_id               in ucl_user.user_name%type,
    p_whse                  in facility.whse%type,
    p_ship_wave_nbr         in ship_wave_parm.ship_wave_nbr%type,
    p_pick_wave_nbr         in ship_wave_parm.pick_wave_nbr%type,
    p_chase_wave            in wave_parm.chase_wave%type
)
as
    v_order_selection_sql   varchar2(32600);
    v_do_type               orders.do_type%type 
        := case when p_wave_type_indic = '1' then 20 else 10 end;
    v_line_item_count       number(9) := 0;
    v_failure_msg           msg_log.msg%type;
    v_lang_id               language.language_suffix%type;
begin
    manh_wave_build_selection_rule(p_wave_by_rule, p_list_of_shpmts,
        p_list_of_orders, p_facility_id, p_perf_pickng_wave, p_perf_rte,
        p_preview_wave_flag, p_wave_type_indic, p_tc_company_id, p_pull_all_swc,
        p_rule_id, p_rule_type, p_is_wave_by_shpmt, p_chase_wave,v_order_selection_sql);

    -- get the list of orders to be processed in this wave
    begin
        if (p_tc_company_id is not null)
        then
            execute immediate v_order_selection_sql using p_user_id, p_facility_id, p_facility_id, 
                p_tc_company_id;
        else
            execute immediate v_order_selection_sql using p_user_id, p_facility_id, p_facility_id;
        end if;
        v_line_item_count := sql%rowcount;
        wm_cs_log('Loaded ' || sql%rowcount || ' OLI''s for rule ' || p_rule_id, p_sql_log_level => 1);
    exception
        when others then
-- todo: log this error
            raise;
    end;
    
    select la.language_suffix
    into v_lang_id
    from ucl_user uu
    join locale l on l.locale_id = uu.locale_id
    join language la on la.language_id = l.language_id
    where uu.user_name = p_user_id;

    if (p_wave_by_rule = 'Y' and p_perf_pickng_wave = '1')
    then
        -- trim returned capacities here since they are rule selection lvl parms
        if (p_max_lines_per_rule < 999999999)
        then    
            delete from tmp_wave_selected_orders t
            where t.id > p_max_lines_per_rule;
        end if;

        if (p_max_orders_per_rule < 999999999)
        then
            delete from tmp_wave_selected_orders t
            where exists
            (
                select 1
                from
                (
                    select iv.order_id,
                        row_number() over(order by iv.min_id) order_pos_no_holes
                    from
                    (
                        select t1.order_id, min(t1.id) min_id
                        from tmp_wave_selected_orders t1
                        group by t1.order_id
                    ) iv
                ) iv2
                where iv2.order_id = t.order_id
                    and iv2.order_pos_no_holes > p_max_orders_per_rule
            );
            wm_cs_log('Dropped ' || sql%rowcount || ' OLI''s due to max qty per rule being '
                || p_max_qty_per_rule, p_sql_log_level => 1);
        end if;

        if (p_max_qty_per_rule < 999999999)
        then
            delete from tmp_wave_selected_orders t
            where t.id >=
            (
                select min(t2.id)
                from
                (
                    select t1.id, sum(t1.need_qty) over(order by t1.id) rng_sum
                    from tmp_wave_selected_orders t1
                ) t2
                where t2.rng_sum > p_max_qty_per_rule
            );
            wm_cs_log('Dropped ' || sql%rowcount || ' OLI''s due to max lines per rule being '
                || p_max_lines_per_rule, p_sql_log_level => 1);
        end if;
    end if;

    if (p_pull_all_swc = 'Y' and p_perf_pickng_wave = '1'
        and p_wave_by_rule = 'Y' and p_is_wave_by_shpmt != 'Y')
    then
-- todo: swc with vas flow has gap; need to avoid pulling non-vas lines in
-- other orders with same swc
        -- insert additional lines with the same swc as those already
        -- selected; per CV, swc's are always together - all are either
        -- fully allocated (or) none are allocated at all (110)
        insert into tmp_wave_selected_orders
        (
            order_id, line_item_id, is_swc_pull, order_qty, allocated_qty,
            pre_pack_flag, ship_group_id, item_id, ppack_grp_code, assort_nbr,
            invn_type, prod_stat, cntry_of_orgn, batch_nbr, item_attr_1, 
            item_attr_2, item_attr_3, item_attr_4, item_attr_5, sku_sub_code_id,
            sku_sub_code_value, do_dtl_status, batch_requirement_type, id,
            perform_vas, need_qty
        )
        select oli.order_id, oli.line_item_id, 1 is_swc_pull, oli.order_qty,
            oli.allocated_qty,
            case when oli.ppack_qty = 0
                and oli.assort_nbr is not null 
                and oli.ppack_grp_code is not null
                then 1 else 0 end pre_pack_flag,
            o.ship_group_id, oli.item_id, oli.ppack_grp_code, oli.assort_nbr,
            oli.invn_type, oli.prod_stat, oli.cntry_of_orgn, oli.batch_nbr,
            oli.item_attr_1, oli.item_attr_2, oli.item_attr_3, oli.item_attr_4,
            oli.item_attr_5, oli.sku_sub_code_id, oli.sku_sub_code_value, 
            oli.do_dtl_status, coalesce(oli.batch_requirement_type, '2'),
            (v_line_item_count + rownum) id,
            case when oli.vas_process_type = '1' then
                case when o.wm_order_status = 20 then 2 else 1 end
            else 0 end perform_vas,
            (oli.order_qty - oli.allocated_qty) need_qty
        from order_line_item oli
        join orders o on o.order_id = oli.order_id
        join 
        (
            select distinct t.ship_group_id
            from tmp_wave_selected_orders t
        ) t1 on t1.ship_group_id = o.ship_group_id
        where o.o_facility_id = p_facility_id and o.is_original_order = 1
            and o.do_status = 110 and o.do_type = v_do_type
            and oli.allocation_source = 10 and oli.do_dtl_status = 110
            and (case when p_tc_company_id is null then o.tc_company_id
                else p_tc_company_id end) = o.tc_company_id
            and coalesce(oli.fulfillment_type, '1') != '2'
            and not exists
            (
                select 1
                from tmp_wave_selected_orders t2
                where t2.order_id = oli.order_id 
                    and t2.line_item_id = oli.line_item_id
            )
            and (o.wm_order_status = 20 or not(oli.vas_process_type = '1'
                and oli.pick_locn_id is not null)) and o.has_import_error = 0;

        wm_cs_log('Pulled in more OLI''s for SWC proc ' || sql%rowcount, p_sql_log_level => 1);
    end if; -- swc inclusion
end;
/

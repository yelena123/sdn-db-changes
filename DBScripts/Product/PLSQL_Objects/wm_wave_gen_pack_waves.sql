create or replace procedure wm_wave_gen_pack_waves
(
    p_user_id           in ucl_user.user_name%type,
    p_facility_id       in facility.facility_id%type,
    p_ship_wave_parm_id in ship_wave_parm.ship_wave_parm_id%type
)
as
    v_incl_actv_flag      pack_wave_parm_hdr.incl_actv_flag%type := 'N';
    v_incl_resv_flag      pack_wave_parm_hdr.incl_resv_flag%type := 'N';
    v_incl_case_pick_flag pack_wave_parm_hdr.incl_case_pick_flag%type := 'N';
    v_incl_mxd_flag       pack_wave_parm_hdr.inc_mxd_flag%type := 'N';
    v_pack_wave_parm_id   pack_wave_parm_hdr.pack_wave_parm_id%type := 0;
    v_max_pack_wave       pack_wave_parm_hdr.max_pack_wave%type := 999;
    v_ship_wave_nbr       ship_wave_parm.ship_wave_nbr%type;
    v_pick_wave_nbr       ship_wave_parm.pick_wave_nbr%type;
    v_wave_parm_id        wave_parm.wave_parm_id%type := 0;
    v_is_olpn_present     number(1) := 0;
    v_chute_util          number(5, 2) := 0;
    v_dflt_cat            pack_wave_parm_hdr.chute_assign_type%type;
    v_force_cat_flag      pack_wave_parm_hdr.force_cat_flag%type;
    v_chk_critcl_dim_flag pack_wave_parm_hdr.chk_critcl_dim_flag%type := 'N';
    v_whse                facility.whse%type;
    v_bal_alg             pack_wave_parm_hdr.bal_alg%type;
    v_code_id             sys_code.code_id%type := '010';
    v_max_log_lvl         number(1);
    v_tc_company_id       wave_parm.tc_company_id%type;
    v_cat_event_id        wave_parm.chute_assign_type_event_id%type;
    v_prev_module_name    wm_utils.t_app_context_data;
    v_prev_action_name    wm_utils.t_app_context_data;
    v_start_lvl           pack_wave_parm_hdr.start_lvl%type := '0';
    v_sys_cd_flag_lvl     number(1);
begin
    select f.whse
    into v_whse
    from facility f
    where f.facility_id = p_facility_id;

    wm_validate_pack_wave_config(p_ship_wave_parm_id, v_incl_actv_flag,
        v_incl_resv_flag, v_incl_case_pick_flag, v_incl_mxd_flag,
        v_pack_wave_parm_id, v_max_pack_wave, v_ship_wave_nbr, v_pick_wave_nbr, 
        v_wave_parm_id, v_chute_util, v_force_cat_flag, v_dflt_cat, 
        v_chk_critcl_dim_flag, v_bal_alg, v_cat_event_id, v_start_lvl);
 
    select to_number(coalesce(trim(substr(sc.misc_flags, 8, 1)), '0'))
    into v_max_log_lvl
    from sys_code sc 
    where sc.rec_type = 'S' and sc.code_type = '520' and sc.code_id = v_code_id;

    select coalesce(wp.tc_company_id, wm_utils.wm_get_user_bu(p_user_id))
    into v_tc_company_id
    from wave_parm wp
    where wp.wave_nbr = v_pick_wave_nbr and wp.whse = v_whse;

    delete from tmp_log_parm;
    insert into tmp_log_parm
    (
        log_lvl, pgm_id, module, msg_id, ref_code_1, ref_value_1, user_id, whse,
        cd_master_id
    )
    values
    (
        v_max_log_lvl, 'wm_wave_gen_pack_waves', 'WAVE', '1094', v_code_id,
        to_char(p_ship_wave_parm_id), p_user_id, v_whse, v_tc_company_id
    );  
    
    wm_utils.get_context_info(v_prev_module_name, v_prev_action_name);
    wm_utils.set_context_info(p_module_name => 'WAVE', p_action_name => 'PACK WAVE',
        p_client_id => v_ship_wave_nbr);

    wm_cs_log('Beginning Pack wave', p_sql_log_level => 0);
       
    wm_load_pw_lpn_data(p_facility_id, v_pick_wave_nbr, v_incl_actv_flag,
        v_incl_resv_flag, v_incl_case_pick_flag, v_incl_mxd_flag, 
        v_chk_critcl_dim_flag);
    
    wm_find_chute_assign_type(v_whse, v_dflt_cat, v_force_cat_flag, v_cat_event_id);

    -- some cartons may not have been assigned CAT's and are deleted before
    -- getting here
    select case when exists
    (
        select 1
        from tmp_pack_wave_selected_lpns
    ) then 1 else 0 end
    into v_is_olpn_present
    from dual;
    
    if (v_is_olpn_present = 0)
    then
        wm_cs_log('No oLpns fed to pack wave!', p_sql_log_level => 0);
        return;
    end if;

    wm_create_pack_waves(v_whse, v_ship_wave_nbr, v_max_pack_wave, p_user_id,
        v_wave_parm_id, v_pack_wave_parm_id, v_chute_util, v_bal_alg, 
        v_start_lvl, v_sys_cd_flag_lvl);

    wm_complete_pack_wave(v_whse, v_pick_wave_nbr, p_user_id, v_pack_wave_parm_id, 
        v_sys_cd_flag_lvl);

    commit;
    wm_cs_log('Completed Pack wave', p_sql_log_level => 0);
	
    wm_utils.set_context_info(p_module_name => v_prev_module_name,
        p_action_name => v_prev_action_name);
end;
/
show errors;
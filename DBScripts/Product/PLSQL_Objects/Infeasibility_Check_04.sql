CREATE OR REPLACE
PROCEDURE Infeasibility_Check_04 (OP_RETVAL            OUT NUMBER,
                                  IP_INFEASIBILITYID       NUMBER,
                                  IP_TCCOMPANYID           NUMBER,
                                  IP_RFPID                 NUMBER,
                                  IP_SCENARIOID            NUMBER)
IS
   c1_override_id        NUMBER;
   c1_carrierName        VARCHAR2 (60);
   c2_roundnum           NUMBER (6);
   c1_scn_roundnum       NUMBER (6);
   c1_systemcapacity	 NUMBER(24,3);
   C2_Systemcapacity	 Number(24,3);
   C3_Systemcapacity     Number(24,3);
   v_override_id         VARCHAR2 (100);
   v_fails_on            VARCHAR2 (1028);
   v_error_description   VARCHAR2 (2056);
   v_comments            VARCHAR2 (255);
   v_override_id_int     NUMBER;
   v_PASS                NUMBER;
   v_FAIL                NUMBER;
   v_retVal              NUMBER;
   sql_error_code        NUMBER;
   ISAMCODE              NUMBER;
BEGIN
   v_PASS := 0;
   v_FAIL := 1;
   v_retVal := v_PASS;
   op_retVal := v_PASS;
   v_fails_on := '-1';

   FOR c1
      IN (  SELECT DISTINCT
                   obcarriercodeid, bid_id, MAX (min_value) max_min_val
              FROM ob200override_adv
             WHERE     rfpid = ip_rfpId
                   AND scenarioid = ip_scenarioId
                   AND SOURCE = 'O'
                   AND level_of_detail = 'L'
                   AND (TYPE = 'F' OR (TYPE = 'O' AND max_value = -1))
                   AND obcarriercodeid IS NOT NULL
                   AND bid_id IS NOT NULL
                   AND max_value = -1
                   AND min_value IS NOT NULL
                   AND min_value <> -1
                   AND enable_flag = 1
                   AND historicalaward = 0
                   AND value_type_id = 0
          GROUP BY obcarriercodeid, bid_id)
   LOOP
      SELECT round_num
        INTO c1_scn_roundnum
        FROM scenario
       WHERE rfpid = ip_rfpId AND scenarioid = ip_scenarioId;

      SELECT MAX (round_num)
        INTO c2_roundnum
        FROM capacitybidrfp
       WHERE     rfpid = ip_rfpId
             AND obcarriercodeid = c1.obcarriercodeid
             AND round_num <= c1_scn_roundnum
             AND weeklycapacity IS NOT NULL;

      IF (c2_roundnum IS NOT NULL) THEN
			SELECT  weeklycapacity
			INTO c1_systemcapacity
			FROM capacitybidrfp
			WHERE rfpid = ip_rfpId
			AND obcarriercodeid = c1.obcarriercodeid
			AND round_num = c2_roundnum;
		ELSE
			c1_systemcapacity := NULL;
		End If;
   Select  Min(Max_Value) Into C2_Systemcapacity From Ob200override_Adv  
   Where Rfpid=Ip_Rfpid And Scenarioid=Ip_Scenarioid And Obcarriercodeid=C1.Obcarriercodeid 
    And Level_Of_Detail='C' And Type='O' And Historicalaward = 0 And Enable_Flag=1 And Is_Valid=1 And Value_Type_Id=0;
   If (C1_Systemcapacity Is Not Null) And (C2_Systemcapacity Is Not Null)Then
    IF (C1_Systemcapacity < C2_Systemcapacity) Then
    C3_Systemcapacity := C1_Systemcapacity;
    Else
    C3_Systemcapacity := C2_Systemcapacity;
    End If;
    Else IF(C1_Systemcapacity Is Not Null) THEN
    C3_Systemcapacity := C1_Systemcapacity;
    Else
    C3_Systemcapacity := C2_Systemcapacity;
    End IF;
    END IF;
		IF ((C3_Systemcapacity IS NOT NULL)
		AND (C3_Systemcapacity <> -1)) THEN
			IF c1.max_min_val > C3_Systemcapacity THEN
			
            v_retVal := v_FAIL;
            op_retVal := v_FAIL;

            SELECT override_id, comments
              INTO c1_override_id, v_comments
              FROM ob200override_adv
             WHERE     rfpid = ip_rfpId
                   AND scenarioid = ip_scenarioId
                   AND SOURCE = 'O'
                   AND level_of_detail = 'L'
                   AND (TYPE = 'F' OR (TYPE = 'O' AND max_value = -1))
                   AND bid_id = c1.bid_id
                   AND obcarriercodeid = c1.obcarriercodeid
                   AND enable_flag = 1
                   AND max_value = -1
                   AND min_value = c1.max_min_val
                   AND value_type_id = 0
                   AND override_id =
                          (SELECT MAX (o.override_id)
                             FROM ob200override_adv o
                            WHERE     o.rfpid = ip_rfpId
                                  AND o.scenarioid = ip_scenarioId
                                  AND o.SOURCE = 'O'
                                  AND o.level_of_detail = 'L'
                                  AND (o.TYPE = 'F'
                                       OR (o.TYPE = 'O' AND o.max_value = -1))
                                  AND o.bid_id = c1.bid_id
                                  AND o.obcarriercodeid = c1.obcarriercodeid
                                  AND o.enable_flag = 1
                                  AND o.max_value = -1
                                  AND o.value_type_id = 0
                                  AND o.min_value = c1.max_min_val);

            v_override_id := TO_CHAR (c1_override_id);

            SELECT c.company_name
              INTO c1_carrierName
              FROM obcarriercode o, COMPANY c
             WHERE o.obcarriercodeid = c1.obcarriercodeid
                   AND o.tpcompanyid = c.company_id;

            IF (c1.obcarriercodeid IS NULL)
            THEN
               c1_carrierName := TO_CHAR (c1.obcarriercodeid);
            END IF;

            v_error_description :=
                  'Carrier ('
               || c1_carrierName
               || ') System Capacity ('
               || TO_CHAR (C3_Systemcapacity)
               || ' lds/wk) <  max(min_val) constraint ('
               || TO_CHAR (c1.max_min_val)
               || ' lds/wk) on lane ('
               || TO_CHAR (c1.bid_id)
               || ')';
            v_fails_on :=
                  'Carrier ('
               || c1_carrierName
               || ') System Capacity ('
               || TO_CHAR (C3_Systemcapacity)
               || ' lds/wk)';

            INSERT INTO INFEASIBILITY_CHECK_RESULTS (rfp_id,
                                                     scenario_id,
                                                     infeasibility_id,
                                                     override_id,
                                                     comments,
                                                     fails_on,
                                                     error_description)
                 VALUES (ip_rfpId,
                         ip_scenarioId,
                         ip_infeasibilityId,
                         v_override_id,
                         v_comments,
                         v_fails_on,
                         v_error_description);
         END IF;
      END IF;
   END LOOP;


   -- FOR Spend Force

   FOR c1
      IN (  SELECT DISTINCT
                   o.obcarriercodeid,
                   o.bid_id,
                   MAX (o.min_value / b.effective_cost) max_min_val
              FROM ob200override_adv o, ob200lane_bid_view b
             WHERE     o.rfpid = ip_rfpId
                   AND o.scenarioid = ip_scenarioId
                   AND o.rfpid = b.rfpid
                   AND o.scenarioid = b.scenarioId
                   AND o.SOURCE = 'O'
                   AND o.level_of_detail = 'L'
                   AND (o.TYPE = 'F' OR (o.TYPE = 'U' AND o.max_value = -1))
                   AND o.obcarriercodeid IS NOT NULL
                   AND o.bid_id IS NOT NULL
                   AND o.max_value = -1
                   AND o.min_value IS NOT NULL
                   AND o.min_value <> -1
                   AND o.enable_flag = 1
                   AND o.historicalaward = 0
                   AND o.value_type_id = 2
          GROUP BY o.obcarriercodeid, o.bid_id)
   LOOP
      SELECT round_num
        INTO c1_scn_roundnum
        FROM scenario
       WHERE rfpid = ip_rfpId AND scenarioid = ip_scenarioId;

      SELECT MAX (round_num)
        INTO c2_roundnum
        FROM capacitybidrfp
       WHERE     rfpid = ip_rfpId
             AND obcarriercodeid = c1.obcarriercodeid
             AND round_num <= c1_scn_roundnum
             AND weeklycapacity IS NOT NULL;

      IF (c2_roundnum IS NOT NULL)
      THEN
         SELECT weeklycapacity
           INTO c2_systemcapacity
           FROM capacitybidrfp
          WHERE     rfpid = ip_rfpId
                AND obcarriercodeid = c1.obcarriercodeid
                AND round_num = c2_roundnum;
      ELSE
         c2_systemcapacity := NULL;
      END IF;

      IF ( (c2_systemcapacity IS NOT NULL) AND (c2_systemcapacity <> -1))
      THEN
         IF c1.max_min_val > c2_systemcapacity
         THEN
            v_retVal := v_FAIL;
            op_retVal := v_FAIL;

            SELECT override_id, comments
              INTO c1_override_id, v_comments
              FROM ob200override_adv
             WHERE     rfpid = ip_rfpId
                   AND scenarioid = ip_scenarioId
                   AND SOURCE = 'O'
                   AND level_of_detail = 'L'
                   AND (TYPE = 'F' OR (TYPE = 'U' AND max_value = -1))
                   AND bid_id = c1.bid_id
                   AND obcarriercodeid = c1.obcarriercodeid
                   AND enable_flag = 1
                   AND max_value = -1
                   --AND min_value = c1.max_min_val
                   AND value_type_id = 2
                   AND override_id =
                          (SELECT MAX (o.override_id)
                             FROM ob200override_adv o
                            WHERE     o.rfpid = ip_rfpId
                                  AND o.scenarioid = ip_scenarioId
                                  AND o.SOURCE = 'O'
                                  AND o.level_of_detail = 'L'
                                  AND (o.TYPE = 'F'
                                       OR (o.TYPE = 'U' AND o.max_value = -1))
                                  AND o.bid_id = c1.bid_id
                                  AND o.obcarriercodeid = c1.obcarriercodeid
                                  AND o.enable_flag = 1
                                  AND o.max_value = -1
                                  AND o.value_type_id = 2);

            -- AND o.min_value = c1.max_min_val);
            v_override_id := TO_CHAR (c1_override_id);

            SELECT c.company_name
              INTO c1_carrierName
              FROM obcarriercode o, COMPANY c
             WHERE o.obcarriercodeid = c1.obcarriercodeid
                   AND o.tpcompanyid = c.company_id;

            IF (c1.obcarriercodeid IS NULL)
            THEN
               c1_carrierName := TO_CHAR (c1.obcarriercodeid);
            END IF;

            v_error_description :=
                  'Carrier ('
               || c1_carrierName
               || ') System Capacity ('
               || TO_CHAR (c2_systemcapacity)
               || ' lds/wk) <  Award Volume created by MIN spend force ('
               || TO_CHAR (c1.max_min_val)
               || ' lds/wk) on lane ('
               || TO_CHAR (c1.bid_id)
               || ')';
            v_fails_on :=
                  'Carrier ('
               || c1_carrierName
               || ') System Capacity ('
               || TO_CHAR (c2_systemcapacity)
               || ' lds/wk)';

            INSERT INTO INFEASIBILITY_CHECK_RESULTS (rfp_id,
                                                     scenario_id,
                                                     infeasibility_id,
                                                     override_id,
                                                     comments,
                                                     fails_on,
                                                     error_description)
                 VALUES (ip_rfpId,
                         ip_scenarioId,
                         ip_infeasibilityId,
                         v_override_id,
                         v_comments,
                         v_fails_on,
                         v_error_description);
         END IF;
      END IF;
   END LOOP;
END Infeasibility_Check_04;
/

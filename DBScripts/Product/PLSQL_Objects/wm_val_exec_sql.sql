create or replace procedure wm_val_exec_sql
(
    p_sql_id          in val_job_dtl.sql_id%type,
    p_val_job_dtl_id  in val_job_dtl.val_job_dtl_id%type default null,
    p_bind_val_csv    in varchar2 default null,
    p_result_csv      out varchar2
)
as
    v_cursor          int := dbms_sql.open_cursor;
    v_bind_var_list   val_sql.bind_var_list%type;
    v_sql_text        val_sql.sql_text%type;
begin
-- todo: remove vjd flow
    if ((p_val_job_dtl_id is null and p_bind_val_csv is null)
        or (p_val_job_dtl_id is not null and p_bind_val_csv is not null))
    then
        raise_application_error(-20050, 'Will marshall sql with a job dtl or a bind value list; not both');
    end if;

    select vs.bind_var_list, regexp_replace(vs.sql_text, '/\*\+[ ]*HINT[ ]*\*/',
        '/*+ ' || regexp_replace(vs.sql_hints, '[\/*]', '') || '*/', 1, 1, 'i') sql_text
    into v_bind_var_list, v_sql_text
    from val_sql vs
    where vs.sql_id = p_sql_id;
        
    wm_val_exec_sql_intrnl(v_cursor, p_sql_id, v_sql_text, v_bind_var_list, p_val_job_dtl_id, 
        p_bind_val_csv, p_txn_id => null, p_result_csv => p_result_csv);

    if (dbms_sql.is_open(v_cursor) = true)
    then
        dbms_sql.close_cursor(v_cursor);
    end if;
end;
/
show errors;

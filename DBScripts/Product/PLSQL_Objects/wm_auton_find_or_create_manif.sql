create or replace procedure wm_auton_find_or_create_manif
(
    p_carrier_id          in carrier_code.carrier_id%type,
    p_manifest_type_id    in manifest_type.manifest_type_id%type,
    p_date                in date,
    p_ship_via            in ship_via.ship_via%type,
    p_shipper_ac_num      in varchar2,
    p_usps_permit_number  in varchar2,
    p_facility_id         in facility.facility_id%type,
    p_tc_company_id       in ship_via.tc_company_id%type,
    p_user_id             in varchar2,
    p_manifest_id         out manifest_hdr.manifest_id%type
)
as
    pragma autonomous_transaction;
    v_tc_company_id          ship_via.tc_company_id%type;
    v_nxt_up_nbr             varchar2(3) := ' ';
    v_lock_time_out          number(3) := 120;
    v_manifest_id            number(9);
    v_tc_manifest_id         varchar2(50);
    v_dummy_whse             nxt_up_cnt.whse%type;
    v_dummy_rec_type_id      nxt_up_cnt.rec_type_id%type;
    v_pfx_field        nxt_up_cnt.pfx_field%type;
    v_pfx_len          nxt_up_cnt.pfx_len%type;
    v_dummy_start_nbr        nxt_up_cnt.start_nbr%type;
    v_dummy_end_nbr          nxt_up_cnt.end_nbr%type;
    v_nbr_len          nxt_up_cnt.nbr_len%type;
    v_dummy_incr_value       nxt_up_cnt.incr_value%type;
    v_dummy_nxt_start_nbr    nxt_up_cnt.nxt_start_nbr%type;
    v_dummy_nxt_end_nbr      nxt_up_cnt.nxt_end_nbr%type;
    v_dummy_chk_digit_type   nxt_up_cnt.chk_digit_type%type;
    v_dummy_chk_digit_len    nxt_up_cnt.chk_digit_len%type;
    v_dummy_repeat_range     nxt_up_cnt.repeat_range%type;
    v_dummy_cd_master_id     nxt_up_cnt.cd_master_id%type;
    v_dummy_nxt_up_cnt_id    nxt_up_cnt.nxt_up_cnt_id%type;
    v_dummy_pfx_type	        nxt_up_cnt.pfx_type%type;
    v_dummy_pfx_order        nxt_up_cnt.rev_order%type;    
    v_dummy_rc               number(1) := 0;
begin
    select mt.tc_company_id, mt.tc_manifest_id_record_type
    into v_tc_company_id, v_nxt_up_nbr
    from manifest_type mt
    where mt.manifest_type_id = p_manifest_type_id
    for update;

    -- search again
    select iv.manifest_id
    into p_manifest_id
    from 
    (
        select mh.manifest_id
        from manifest_hdr mh
        where mh.carrier_id = p_carrier_id
            and mh.manifest_type_id = p_manifest_type_id
            and trunc(mh.sched_pickup_date) = p_date
            and mh.manifest_status_id = 10
			      and mh.o_facility_id = p_facility_id
        order by mh.sched_pickup_date
    ) iv
    where rownum < 2;

    commit;
exception
    when no_data_found then
        p_manifest_id := manifest_hdr_id_seq.nextval;
        wm_auton_get_nxt_up_cnt_id (p_facility_id, p_tc_company_id, v_nxt_up_nbr, v_lock_time_out, 'SP',
            1, v_dummy_whse, v_dummy_rec_type_id, v_pfx_field,
            v_pfx_len, v_dummy_start_nbr, v_dummy_end_nbr, v_manifest_id,
            v_nbr_len, v_dummy_incr_value, v_dummy_nxt_start_nbr,
            v_dummy_nxt_end_nbr, v_dummy_chk_digit_type, v_dummy_chk_digit_len,
            v_dummy_repeat_range, v_dummy_cd_master_id, v_dummy_nxt_up_cnt_id,
            v_dummy_pfx_type, v_dummy_pfx_order, v_dummy_rc);

        v_tc_manifest_id := lpad(to_char(v_manifest_id), v_nbr_len, '0');
            
        if (v_pfx_len > 0)
        then
            v_tc_manifest_id := v_pfx_field || v_tc_manifest_id;          
        end if;
        

        insert into manifest_hdr
        (
            manifest_status_id, sched_pickup_date, manifest_id, tc_company_id,
            tc_manifest_id, o_facility_alias_id, o_facility_id, manifest_type_id,
            shipper_ac_num, usps_permit_number, carrier_id, created_source_type,
            created_source, created_dttm, last_updated_source_type, 
            last_updated_source, last_updated_dttm
        )
        values
        (
            10, p_date, p_manifest_id, p_tc_company_id, to_char(v_tc_manifest_id),
            (
                select fa.facility_alias_id from facility_alias fa 
                where fa.facility_id = p_facility_id and fa.is_primary = 1
            ),
            p_facility_id, p_manifest_type_id, p_shipper_ac_num, 
            p_usps_permit_number, p_carrier_id, 5, p_user_id, sysdate, 5, 
            p_user_id, sysdate
        );

        commit;
end;
/
show errors;

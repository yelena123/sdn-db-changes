create or replace procedure wm_eval_lpn_attrs_for_cubing
(
    p_user_id                    in ucl_user.user_name%type,
    p_facility_id                in facility.facility_id%type,
    p_ship_wave_nbr              in ship_wave_parm.ship_wave_nbr%type,
    p_need_id                    in alloc_invn_dtl.need_id%type default null
)
as
    v_update_sql                varchar2(32600) default ' ' ;
    v_carton_rule               varchar2(4000) default ' ' ;
    v_max_log_lvl               number(1) := 0;
    v_code_id                   sys_code.code_id%type := 'CUB';
    v_ref_value_1               msg_log.ref_value_1%type := p_ship_wave_nbr ;
    v_lpn_brk_attr_event_id     wave_parm.lpn_brk_attrib_event_id%type;
    v_crtn_type_event_id        wave_parm.carton_type_event_id%type;
    v_tc_company_id             wave_parm.tc_company_id%type;
    v_whse                      facility.whse%type;
    v_wp_carton_type            wave_parm.carton_type%type;
    v_pick_wave_nbr             ship_wave_parm.pick_wave_nbr%type;
    v_prev_module_name          wm_utils.t_app_context_data;
    v_prev_action_name          wm_utils.t_app_context_data;
begin
    select swp.whse, wp.carton_type_event_id, wp.lpn_brk_attrib_event_id, coalesce(wp.tc_company_id, wm_utils.wm_get_user_bu(p_user_id)),
        wp.carton_type, swp.pick_wave_nbr
    into v_whse, v_crtn_type_event_id, v_lpn_brk_attr_event_id, v_tc_company_id, v_wp_carton_type,
        v_pick_wave_nbr
    from ship_wave_parm swp
    join wave_parm wp on wp.wave_parm_id = swp.wave_parm_id
    where swp.ship_wave_nbr = p_ship_wave_nbr
        and swp.whse =
        (
            select f.whse
            from facility f
            where f.facility_id = p_facility_id
        );

    select to_number(coalesce(trim(substr(sc.misc_flags, 8, 1)), '0'))
    into v_max_log_lvl
    from sys_code sc 
    where sc.rec_type = 'S' and sc.code_type = '520' and sc.code_id = v_code_id;

    delete from tmp_log_parm;
    insert into tmp_log_parm
    (
        log_lvl, pgm_id, module, msg_id, ref_code_1, ref_value_1, user_id, whse,
        cd_master_id
    )
    values
    (
        v_max_log_lvl, 'wm_eval_lpn_attrs_for_cubing', 'WAVE', '1094', v_code_id, v_ref_value_1, 
        p_user_id, v_whse, v_tc_company_id
    ); 
    
    if (v_crtn_type_event_id is null and v_lpn_brk_attr_event_id is null)
    then
        wm_cs_log('No event type selected for CNT or LBA rules ');
        return;
    end if;
    
    wm_utils.get_context_info(v_prev_module_name, v_prev_action_name);
    wm_utils.set_context_info(p_module_name => 'WAVE', p_action_name => 'LPN ATTRS DETERMINATION',
        p_client_id => p_ship_wave_nbr);
		
    wm_cs_log('Starting lpn type/break attr determination');
    insert into tmp_oli_cntr_type
    (
        alloc_invn_dtl_id, tc_order_id, apply_lpntype_for_order, tc_company_id, carton_type,
        carton_brk_attr
    )
    select aid.alloc_invn_dtl_id, o.tc_order_id, o.apply_lpntype_for_order, o.tc_company_id, 
        decode(o.apply_lpntype_for_order, 1, null, 
            coalesce(pd.carton_type, iw.carton_type, v_wp_carton_type)) carton_type, 
        coalesce(pd.carton_break_attr, ifmw.carton_break_attr) carton_brk_attr
    from alloc_invn_dtl aid
    join pkt_dtl pd on pd.pkt_ctrl_nbr = aid.pkt_ctrl_nbr and pd.pkt_seq_nbr = aid.pkt_seq_nbr
        and pd.rtl_to_be_distroed_qty = 0
    join orders o on aid.tc_order_id = o.tc_order_id and o.tc_company_id = aid.cd_master_id
    join item_wms iw on iw.item_id = aid.item_id
    join item_facility_mapping_wms ifmw on ifmw.item_id = iw.item_id
        and ifmw.facility_id = p_facility_id
    where aid.whse = v_whse and aid.task_genrtn_ref_nbr = v_pick_wave_nbr and aid.stat_code < 90
        and coalesce(aid.need_id, ' ') = coalesce(p_need_id, aid.need_id, ' ');
    wm_cs_log('AID data collected ' || sql%rowcount);

    for rule_rec in
    (
        select rsd.rule_id, rh.rule_type,
            decode(rh.rule_type, 'CNT', 't.carton_type', 't.carton_brk_attr') col_name,
            decode(rh.rule_type, 'CNT', erp.carton_type, erp.lpn_brk_attrib) lpn_attr
        from rule_sel_dtl rsd 
        join rule_hdr rh on rsd.rule_id = rh.rule_id and rh.stat_code = 0
        join event_rule_parm erp on erp.rule_id = rh.rule_id
        join event_parm ep on ep.event_parm_id = erp.event_parm_id
        where (ep.event_type ='CNT' and ep.event_parm_id = v_crtn_type_event_id
            and erp.carton_type is not null)
            or (ep.event_type ='LBA' and ep.event_parm_id = v_lpn_brk_attr_event_id
                and erp.lpn_brk_attrib is not null)
        order by ep.event_type asc, erp.rule_prty asc, rsd.rule_id asc
    )
    loop
        select manh_get_rule(rule_rec.rule_id, 0, rule_rec.rule_type)
        into v_carton_rule
        from dual;

        if (v_carton_rule is not null)
        then
            v_update_sql := 
                   ' merge into tmp_oli_cntr_type t '
                || ' using'
                || ' ('
                || '    select alloc_invn_dtl.alloc_invn_dtl_id, alloc_invn_dtl.tc_order_id,'
                || '      alloc_invn_dtl.cd_master_id'
                || case when rule_rec.rule_type = 'CNT' then
                   '      , row_number() over(partition by alloc_invn_dtl.tc_order_id, alloc_invn_dtl.cd_master_id'
                || '          order by alloc_invn_dtl.alloc_invn_dtl_id) rn'
                   else ' ' end
                || '    from alloc_invn_dtl'
                || '    where alloc_invn_dtl.task_genrtn_ref_nbr = :pick_wave_nbr '
                || '        and alloc_invn_dtl.whse = :whse and alloc_invn_dtl.stat_code < 90'
                || case when p_need_id is null then ' ' 
                    else ' and alloc_invn_dtl.need_id = :need_id' end
                || '        and ' || v_carton_rule
                || ' ) iv on '
                || case when rule_rec.rule_type = 'CNT' then
                   '    ((t.apply_lpntype_for_order = 0 and iv.alloc_invn_dtl_id = t.alloc_invn_dtl_id)'
                   || '   or (t.apply_lpntype_for_order = 1 and iv.tc_order_id = t.tc_order_id'
                   || '     and iv.cd_master_id = t.tc_company_id and iv.rn = 1))'
                   else ' (iv.alloc_invn_dtl_id = t.alloc_invn_dtl_id)' end
                || ' when matched then'
                || ' update set ' || rule_rec.col_name || ' = :attr '
                || ' outer_clause ' || rule_rec.col_name || ' is null';

            wm_add_joins_for_cube_attrib(p_facility_id, v_update_sql);
            v_update_sql := replace(v_update_sql, 'outer_clause', 'where');
            if (instr(v_update_sql, 'ORDER_NOTE.', 1, 1) > 0)
            then
                v_update_sql := replace(v_update_sql, 'select ', 'select distinct ');
            end if;

            if (p_need_id is null)
            then
                execute immediate v_update_sql using v_pick_wave_nbr, v_whse, rule_rec.lpn_attr;
            else
                execute immediate v_update_sql using v_pick_wave_nbr, v_whse, p_need_id,
                    rule_rec.lpn_attr;
            end if;
            wm_cs_log('Updated ' || rule_rec.lpn_attr || ' on AIDs matching rule ' 
                || rule_rec.rule_id || ' for event_type ' || rule_rec.rule_type || ': ' 
                || sql%rowcount); 
        end if;        
    end loop;

    merge into alloc_invn_dtl aid
    using
    (
         select t.alloc_invn_dtl_id, t.carton_type, t.carton_brk_attr
         from tmp_oli_cntr_type t
    ) iv
    on (iv.alloc_invn_dtl_id = aid.alloc_invn_dtl_id)
    when matched then
    update set aid.carton_type = iv.carton_type, aid.carton_break_attr = iv.carton_brk_attr,
        aid.user_id = p_user_id, aid.mod_date_time = sysdate;
    wm_cs_log('lpn type, brk attr updated on aid ' || sql%rowcount);   

    commit;
    wm_cs_log('cntr_type and lpn_brk_attr setting complete ');
	
    wm_utils.set_context_info(p_module_name => v_prev_module_name,
        p_action_name => v_prev_action_name);
end;
/
show errors;

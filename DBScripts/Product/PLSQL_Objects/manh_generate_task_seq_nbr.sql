create or replace procedure manh_generate_task_seq_nbr
(
    p_facility_id   in facility.facility_id%type,
    p_rule_id       in rule_hdr.rule_id%type
)
as
    v_task_seq_nbr_gen_sql varchar2(18000);
    v_sort_rule varchar2(1000) ;
    v_rule_type rule_hdr.rule_type%type := 'TS';
begin  
    -- construct order-by clause from task_prt_sort_dtl
    for rec in
    (
        select ' ' || ',' || tpsd.tbl_name || '.' || tpsd.colm_name || ' ' 
            || case tpsd.sort_seq when 'A' then 'asc' else 'desc' end rule_cond
        from rule_hdr rh
        join task_prt_sort_dtl tpsd on tpsd.rule_id = rh.rule_id
        where rh.rule_id = p_rule_id and rh.rule_type = v_rule_type
        order by tpsd.sort_seq_nbr 
    )
    loop
        v_sort_rule := v_sort_rule || rec.rule_cond;
    end loop;

    if (v_sort_rule is null)
    then
        -- the sequencing rules for selection still apply
        v_sort_rule := ' partition by t.task_id order by t.id ';
    else
        -- use these new resequencing rules to generate seq_nbr's for task_dtl's
        v_sort_rule := ' partition by t.task_id'
            || ' order by ' || substr(v_sort_rule, 3, length(v_sort_rule));
    end if;

    -- account for multiple rows in locn_grp
    v_task_seq_nbr_gen_sql 
        := ' insert into tmp_task_creation_task_seq_nbr'
        || ' ('
        || '    id, task_seq_nbr'
        || ' )'
        || ' select iv.id, min(iv.task_seq_nbr) task_seq_nbr'
        || ' from'
        || ' ('
        || '    select t.id, '
        || '    row_number() over(' || v_sort_rule || ') task_seq_nbr'
        || '    from tmp_task_creation_selected_aid t'
        || '    where 1 = 1 ';

    -- add appropriate join conditions where needed
    -- check: spec does not specify which tables to use; using the list of tables
    -- specified in the 6r10 prod db; this list cannot change without a mod
    -- todo: all data in the tmp aid table must be given a seq_nbr here; so we need
    -- to ensure that all the data in the tmp aid table is inserted above? should
    -- we left join on pkt_hdr, carton_hdr?
    if (instr(v_task_seq_nbr_gen_sql, 'ALLOC_INVN_DTL', 1, 1) > 0)
    then
        v_task_seq_nbr_gen_sql := replace(v_task_seq_nbr_gen_sql, 'where ',
            ' join alloc_invn_dtl on alloc_invn_dtl.alloc_invn_dtl_id = '
            || ' t.alloc_invn_dtl_id where ');
    end if;

    if (instr(v_task_seq_nbr_gen_sql, 'ORDERS', 1, 1) > 0
        or instr(v_task_seq_nbr_gen_sql, 'ORDER_LINE_ITEM', 1, 1) > 0)
    then
        v_task_seq_nbr_gen_sql := replace(v_task_seq_nbr_gen_sql, 'where ',
            ' join orders on orders.tc_order_id = t.tc_order_id '
            || ' and orders.tc_company_id = t.cd_master_id where ');
    end if;
    if (instr(v_task_seq_nbr_gen_sql, 'ORDER_LINE_ITEM', 1, 1) > 0)
    then
        v_task_seq_nbr_gen_sql := replace(v_task_seq_nbr_gen_sql, 'where ',
            ' join order_line_item on order_line_item.order_id = orders.order_id '
            || ' and order_line_item.line_item_id = t.line_item_id where ');
    end if;
    if (instr(v_task_seq_nbr_gen_sql, 'ALIAS_OLPN', 1, 1) > 0)
    then
        v_task_seq_nbr_gen_sql := replace(v_task_seq_nbr_gen_sql, 'where ',
            ' join lpn olpn on olpn.tc_lpn_id = t.carton_nbr and olpn.tc_company_id = t.cd_master_id '
            || ' and olpn.inbound_outbound_indicator = ''O'' and olpn.c_facility_id = ' || p_facility_id || ' where ');
        v_task_seq_nbr_gen_sql := replace(v_task_seq_nbr_gen_sql, 'ALIAS_OLPN',
            'olpn');
    end if;
    if (instr(v_task_seq_nbr_gen_sql, 'ALIAS_IBPALLET', 1, 1) > 0)
    then
        v_task_seq_nbr_gen_sql := replace(v_task_seq_nbr_gen_sql, 'where ',
            ' join lpn plt on plt.tc_lpn_id = t.cntr_nbr and plt.tc_company_id = t.cd_master_id '
            || ' and plt.inbound_outbound_indicator = ''I'' and plt.lpn_type = 2 '
            || ' and plt.c_facility_id = '|| p_facility_id || ' where ');
        v_task_seq_nbr_gen_sql := replace(v_task_seq_nbr_gen_sql, 'ALIAS_IBPALLET',
            'plt');
    end if;
    if (instr(v_task_seq_nbr_gen_sql, 'ALIAS_DLH', 1, 1) > 0)
    then
        v_task_seq_nbr_gen_sql := replace(v_task_seq_nbr_gen_sql, 'where ',
            ' join locn_hdr dlh on dlh.locn_id = t.dest_locn_id where ');
        v_task_seq_nbr_gen_sql := replace(v_task_seq_nbr_gen_sql, 'ALIAS_DLH',
            'dlh');
    end if;
    
    if (instr(v_task_seq_nbr_gen_sql, 'ALIAS_DPL', 1, 1) > 0)
    then
        v_task_seq_nbr_gen_sql := replace(v_task_seq_nbr_gen_sql, 'where ',
            ' join pick_locn_hdr dpl on dpl.locn_id = t.dest_locn_id where ');
        v_task_seq_nbr_gen_sql := replace(v_task_seq_nbr_gen_sql, 'ALIAS_DPL',
            'dpl');
    end if;
    
    if (instr(v_task_seq_nbr_gen_sql, 'ALIAS_DRL', 1, 1) > 0)
    then
        v_task_seq_nbr_gen_sql := replace(v_task_seq_nbr_gen_sql, 'where ',
            ' join resv_locn_hdr drl on drl.locn_id = t.dest_locn_id where ');
        v_task_seq_nbr_gen_sql := replace(v_task_seq_nbr_gen_sql, 'ALIAS_DRL',
            'drl');
    end if;

    if (instr(v_task_seq_nbr_gen_sql, 'LPN.', 1, 1) > 0)
    then	
        v_task_seq_nbr_gen_sql := replace(v_task_seq_nbr_gen_sql, 'where ',
            ' join lpn on lpn.tc_lpn_id = t.cntr_nbr and lpn.tc_company_id = t.cd_master_id '
            || ' and lpn.inbound_outbound_indicator = ''I'' and lpn.lpn_type = 1 '
            || ' and lpn.c_facility_id = ' || p_facility_id || ' where ');
    end if;

    if (instr(v_task_seq_nbr_gen_sql, 'ITEM_CBO', 1, 1) > 0)
    then
        v_task_seq_nbr_gen_sql := replace(v_task_seq_nbr_gen_sql, 'where ',
            ' join item_cbo on item_cbo.item_id = t.item_id where ');
    end if;
    if (instr(v_task_seq_nbr_gen_sql, 'ITEM_WMS', 1, 1) > 0)
    then
        v_task_seq_nbr_gen_sql := replace(v_task_seq_nbr_gen_sql, 'where ',
            'join item_wms on item_wms.item_id = t.item_id '
            || ' where ');
    end if;

    if (instr(v_task_seq_nbr_gen_sql, 'ITEM_FACILITY_MAPPING_WMS', 1 ,1) > 0)
    then
        v_task_seq_nbr_gen_sql := replace(v_task_seq_nbr_gen_sql, 'where ',
          ' join item_facility_mapping_wms on item_facility_mapping_wms.item_id '
          || ' = t.item_id '
          || ' and item_facility_mapping_wms.facility_id = ' 
          || to_char(p_facility_id) || ''
          || ' where ');
    end if;

    if (instr(v_task_seq_nbr_gen_sql, 'LOCN_HDR', 1, 1) > 0)
    then
        v_task_seq_nbr_gen_sql := replace(v_task_seq_nbr_gen_sql, 'where ',
            ' left join locn_hdr on locn_hdr.locn_id = t.pull_locn_id where ');
    end if;
	
    if (instr(v_task_seq_nbr_gen_sql, 'PICK_LOCN_HDR', 1, 1) > 0)
    then
        v_task_seq_nbr_gen_sql := replace(v_task_seq_nbr_gen_sql, 'where ',
            ' left join pick_locn_hdr on pick_locn_hdr.locn_id = t.pull_locn_id where ');
    end if;
    
    if (instr(v_task_seq_nbr_gen_sql, 'RESV_LOCN_HDR', 1, 1) > 0)
    then
        v_task_seq_nbr_gen_sql := replace(v_task_seq_nbr_gen_sql, 'where ',
            ' left join resv_locn_hdr on resv_locn_hdr.locn_id = t.pull_locn_id where ');
    end if; 
    
    if (instr(v_task_seq_nbr_gen_sql, 'LOCN_GRP', 1, 1) > 0)
    then
        v_task_seq_nbr_gen_sql := replace(v_task_seq_nbr_gen_sql, 'where ',
            ' left join locn_grp on locn_grp.locn_id = t.pull_locn_id where ');
    end if;

    v_task_seq_nbr_gen_sql := v_task_seq_nbr_gen_sql
        || ' ) iv'
        || ' group by iv.id';

    -- generate task_seq_nbr's according to the order by criteria configured
    execute immediate v_task_seq_nbr_gen_sql;
    wm_cs_log('Calculated primary task sequences ' || sql%rowcount);

    -- put the generated numbers back into the main tmp table
    update tmp_task_creation_selected_aid t
    set t.task_seq_nbr = 
    (
        select t1.task_seq_nbr
        from tmp_task_creation_task_seq_nbr t1
        where t1.id = t.id 
    );
    wm_cs_log('Updated primary task sequences ' || sql%rowcount);
end;
/

create or replace
PROCEDURE Infeasibility_Check_03
 (OP_RETVAL OUT NUMBER,
   IP_INFEASIBILITYID NUMBER,
   IP_TCCOMPANYID NUMBER,
   IP_RFPID NUMBER,
   IP_SCENARIOID NUMBER
 ) IS
 	 c1_override_id	NUMBER;
 	 c1_carrierName	VARCHAR2(60);
 	 c2_lane_roundnum_i	NUMBER(6);
 	 c2_lane_roundnum_o	NUMBER(6);
 	 c2_facilitycode_i	VARCHAR2(32);
 	 c2_facilitycode_o	VARCHAR2(32);
 	 c2_facilitycapacity_i	NUMBER(24,3);
 	 c2_facilitycapacity_o	NUMBER(24,3);
   c2_areacode_i	VARCHAR2(32);
 	 c2_areacode_o	VARCHAR2(32);
 	 c2_areacapacity_i	NUMBER(24,3);
 	 c2_areacapacity_o	NUMBER(24,3);
 	 c2_cbf_roundnum_i	NUMBER(6);
 	 c2_cbf_roundnum_o	NUMBER(6);
 	 c1_scn_roundnum	NUMBER(6);
 	 v_comments	VARCHAR2(255);
 	 v_override_id	VARCHAR2(100);
 	 v_fails_on	VARCHAR2(1028);
 	 v_error_description	VARCHAR2(2056);
 	 obcarriercodeid  	NUMBER;
	 bid_id		  	NUMBER;
 	 max_min_val	 	NUMBER(24,3);
 	 v_PASS	NUMBER;
 	 v_FAIL	NUMBER;
 	 v_retVal	NUMBER;
 	 v_OB	NUMBER;
 	 v_IB	NUMBER;
   v_OBA	NUMBER;
 	 v_IBA	NUMBER;
 	 v_NOBID	NUMBER;
	 sql_error_code NUMBER;
	 ISAMCODE NUMBER;
BEGIN
	v_PASS := 0;
	v_FAIL := 1;
	v_retVal := v_PASS;
	op_retVal := v_retVal;
	v_fails_on := '-1';
-- For a given carrier and lane combination
	FOR c1 IN ( SELECT DISTINCT obcarriercodeid ,
	bid_id ,
	MAX( min_value) max_min_val
	FROM ob200override_adv
	WHERE rfpid = ip_rfpId
	AND scenarioid = ip_scenarioId
	AND SOURCE = 'O'
	AND level_of_detail = 'L'
	AND (TYPE = 'F' OR (TYPE = 'O' AND max_value = -1))
	AND obcarriercodeid IS NOT NULL
	AND bid_id IS NOT NULL
	AND max_value = -1
	AND min_value IS NOT NULL
	AND min_value <> -1
	AND enable_flag = 1
	AND historicalaward = 0
	AND value_type_id = 0
	GROUP BY obcarriercodeid,
	bid_id) LOOP
		v_OB := 0;
		v_IB := 0;
		v_NOBID := 0;
		v_retVal := v_PASS;
		SELECT  round_num
		INTO c1_scn_roundnum
		FROM scenario
		WHERE rfpid = ip_rfpId
		AND scenarioid = ip_scenarioId;
		SELECT  c.company_name
		INTO c1_carrierName
		FROM obcarriercode o,
		COMPANY c
		WHERE o.obcarriercodeid = c1.obcarriercodeid
		AND o.tpcompanyid = c.company_id;
		IF (c1.obcarriercodeid IS NULL) THEN
			c1_carrierName := TO_CHAR(c1.obcarriercodeid);
		END IF;
		SELECT  MAX( round_num)
		INTO c2_lane_roundnum_o
		FROM lane
		WHERE rfpid = ip_rfpId
		AND laneid = c1.bid_id
		AND originfacilitycode IS NOT NULL
		AND round_num <= c1_scn_roundnum;
		IF (c2_lane_roundnum_o IS NOT NULL) THEN
			SELECT  originfacilitycode
			INTO c2_facilitycode_o
			FROM lane
			WHERE rfpid = ip_rfpId
			AND laneid = c1.bid_id
			AND originfacilitycode IS NOT NULL
			AND round_num = c2_lane_roundnum_o;
		ELSE
			c2_facilitycode_o := NULL;
		END IF;
		SELECT  MAX( round_num)
		INTO c2_lane_roundnum_i
		FROM lane
		WHERE rfpid = ip_rfpId
		AND laneid = c1.bid_id
		AND destinationfacilitycode IS NOT NULL
		AND round_num <= c1_scn_roundnum;
		IF (c2_lane_roundnum_i IS NOT NULL) THEN
			SELECT  destinationfacilitycode
			INTO c2_facilitycode_i
			FROM lane
			WHERE rfpid = ip_rfpId
			AND laneid = c1.bid_id
			AND destinationfacilitycode IS NOT NULL
			AND round_num = c2_lane_roundnum_i;
		ELSE
			c2_facilitycode_i := NULL;
		END IF;
		IF (c2_facilitycode_o IS NOT NULL) THEN
			SELECT  MAX( round_num)
			INTO c2_cbf_roundnum_o
			FROM capacitybidfacility
			WHERE rfpid = ip_rfpId
			AND obcarriercodeid = c1.obcarriercodeid
			AND facilitycode = c2_facilitycode_o
			AND DIRECTION = 'OUT'
			AND round_num <= c1_scn_roundnum
			AND weeklycapacity IS NOT NULL;
			c2_facilitycapacity_o := NULL;
			IF (c2_cbf_roundnum_o IS NOT NULL) THEN
				SELECT  weeklycapacity
				INTO c2_facilitycapacity_o
				FROM capacitybidfacility
				WHERE rfpid = ip_rfpId
				AND obcarriercodeid = c1.obcarriercodeid
				AND facilitycode = c2_facilitycode_o
				AND DIRECTION = 'OUT'
				AND round_num = c2_cbf_roundnum_o;
			END IF;
			IF (c2_facilitycapacity_o IS NOT NULL) THEN
				IF (c1.max_min_val > c2_facilitycapacity_o) THEN
					v_retVal := v_FAIL;
					op_retVal := v_retVal;
					v_OB := 1;
				END IF;
			END IF;
		END IF;
		IF (c2_facilitycode_i IS NOT NULL) THEN
			SELECT  MAX( round_num)
			INTO c2_cbf_roundnum_i
			FROM capacitybidfacility
			WHERE rfpid = ip_rfpId
			AND obcarriercodeid = c1.obcarriercodeid
			AND facilitycode = c2_facilitycode_i
			AND DIRECTION = 'IN'
			AND round_num <= c1_scn_roundnum
			AND weeklycapacity IS NOT NULL;
			c2_facilitycapacity_i := NULL;
			IF (c2_cbf_roundnum_i IS NOT NULL) THEN
				SELECT  weeklycapacity
				INTO c2_facilitycapacity_i
				FROM capacitybidfacility
				WHERE rfpid = ip_rfpId
				AND obcarriercodeid = c1.obcarriercodeid
				AND facilitycode = c2_facilitycode_i
				AND DIRECTION = 'IN'
				AND round_num = c2_cbf_roundnum_i;
			END IF;
			IF (c2_facilitycapacity_i IS NOT NULL) THEN
				IF (c1.max_min_val > c2_facilitycapacity_i) THEN
					v_retVal := v_FAIL;
					op_retVal := v_retVal;
					v_IB := 1;
				END IF;
			END IF;
		END IF;
		IF (v_retVal = v_FAIL) THEN
			SELECT  override_id ,
			comments
			INTO c1_override_id,
			v_comments
			FROM ob200override_adv
			WHERE rfpid = ip_rfpId
			AND scenarioid = ip_scenarioId
			AND SOURCE = 'O'
			AND level_of_detail = 'L'
			AND (TYPE = 'F' OR (TYPE = 'O' AND max_value = -1))
			AND bid_id = c1.bid_id
			AND obcarriercodeid = c1.obcarriercodeid
			AND enable_flag = 1
			AND max_value = -1
			AND min_value = c1.max_min_val
			AND value_type_id = 0
			AND override_id = (SELECT  MAX( o.override_id)
			FROM ob200override_adv o
			WHERE o.rfpid = ip_rfpId
			AND o.scenarioid = ip_scenarioId
			AND o.SOURCE = 'O'
			AND o.level_of_detail = 'L'
			AND (o.TYPE = 'F' OR (o.TYPE = 'O' AND o.max_value = -1))
			AND o.bid_id = c1.bid_id
			AND o.obcarriercodeid = c1.obcarriercodeid
			AND o.enable_flag = 1
			AND o.max_value = -1
			AND o.value_type_id = 0
			AND o.min_value = c1.max_min_val);
			v_override_id := TO_CHAR(c1_override_id);
			IF (v_comments IS NULL) THEN
				v_comments := ' ';
			END IF;
			IF ((v_OB = 1)
			AND (v_IB = 1)) THEN
				v_fails_on := 'Lane ('||TO_CHAR(c1.bid_id)||') Carrier ('||c1_carrierName||') conflicts with both Inbound facility ('||c2_facilitycode_i||') Capacity ('||TO_CHAR(c2_facilitycapacity_i)||' lds/wk) and Outbound facility ('||c2_facilitycode_o||' lds/wk) Capacity ('||TO_CHAR(c2_facilitycapacity_o)||' lds/wk)';
				v_error_description := 'Lane ('||TO_CHAR(c1.bid_id)||') Carrier ('||c1_carrierName||') OB Facility ('||c2_facilitycode_o||') Capacity ('||TO_CHAR(c2_facilitycapacity_o)||' lds/wk) < lane max(min_value constraint) value ('||TO_CHAR(c1.max_min_val)||' lds/wk) and IB Facility ('||c2_facilitycode_i||') Capacity ('||TO_CHAR(c2_facilitycapacity_i)||' lds/wk) < lane max(min_value constraint) value('||TO_CHAR(c1.max_min_val)||' lds/wk)';
			ELSIF (v_OB = 1) THEN
				v_fails_on := 'Lane ('||TO_CHAR(c1.bid_id)||') Carrier ('||c1_carrierName||') conflicts with  outbound facility ('||c2_facilitycode_o||') Capacity ('||TO_CHAR(c2_facilitycapacity_o)||' lds/wk)';
				v_error_description := 'Lane ('||TO_CHAR(c1.bid_id)||') Carrier ('||c1_carrierName||') OB Facility ('||c2_facilitycode_o||') Capacity ('||TO_CHAR(c2_facilitycapacity_o)||' lds/wk) < lane max(min_value constraint) value ('||TO_CHAR(c1.max_min_val)||' lds/wk)';
			ELSIF (v_IB = 1) THEN
				v_fails_on := 'Lane ('||TO_CHAR(c1.bid_id)||') Carrier ('||c1_carrierName||') conflicts with  inbound facility ('||c2_facilitycode_i||') Capacity ('||TO_CHAR(c2_facilitycapacity_i)||' lds/wk)';
				v_error_description := 'Lane ('||TO_CHAR(c1.bid_id)||') Carrier ('||c1_carrierName||') IB Facility ('||c2_facilitycode_i||') Capacity ('||TO_CHAR(c2_facilitycapacity_i)||' lds/wk) < lane max(min_value constraint) value ('||TO_CHAR(c1.max_min_val)||' lds/wk)';
			END IF;
			INSERT INTO INFEASIBILITY_CHECK_RESULTS (rfp_id,scenario_id,infeasibility_id,override_id,comments,fails_on,error_description) VALUES ( ip_rfpId,ip_scenarioId,ip_infeasibilityId,v_override_id,v_comments,v_fails_on,v_error_description );
		END IF;
	END LOOP;
  
  -- AREA  INF
  
 FOR c1 IN ( SELECT DISTINCT obcarriercodeid ,
	bid_id ,
	MAX( min_value) max_min_val
	FROM ob200override_adv
	WHERE rfpid = ip_rfpId
	AND scenarioid = ip_scenarioId
	AND SOURCE = 'O'
	AND level_of_detail = 'L'
	AND (TYPE = 'F' OR (TYPE = 'O' AND max_value = -1))
	AND obcarriercodeid IS NOT NULL
	AND bid_id IS NOT NULL
	AND max_value = -1
	AND min_value IS NOT NULL
	AND min_value <> -1
	AND enable_flag = 1
	AND historicalaward = 0
	AND value_type_id = 0
	GROUP BY obcarriercodeid,
	bid_id) LOOP
		v_OBA := 0;
		v_IBA := 0;
		v_NOBID := 0;
		v_retVal := v_PASS;
		SELECT  round_num
		INTO c1_scn_roundnum
		FROM scenario
		WHERE rfpid = ip_rfpId
		AND scenarioid = ip_scenarioId;
		SELECT  c.company_name
		INTO c1_carrierName
		FROM obcarriercode o,
		COMPANY c
		WHERE o.obcarriercodeid = c1.obcarriercodeid
		AND o.tpcompanyid = c.company_id;
		IF (c1.obcarriercodeid IS NULL) THEN
			c1_carrierName := TO_CHAR(c1.obcarriercodeid);
		END IF;
		SELECT  MAX( round_num)
		INTO c2_lane_roundnum_o
		FROM lane
		WHERE rfpid = ip_rfpId
		AND laneid = c1.bid_id
		AND originfacilitycode IS NOT NULL
		AND round_num <= c1_scn_roundnum;
		IF (c2_lane_roundnum_o IS NOT NULL) THEN
			SELECT  origincapacityareacode
			INTO c2_areacode_o
			FROM lane
			WHERE rfpid = ip_rfpId
			AND laneid = c1.bid_id
			--AND origincapacityareacode IS NOT NULL
			AND round_num = c2_lane_roundnum_o;
		ELSE
			c2_areacode_o := NULL;
		END IF;
		SELECT  MAX( round_num)
		INTO c2_lane_roundnum_i
		FROM lane
		WHERE rfpid = ip_rfpId
		AND laneid = c1.bid_id
		AND destinationcapacityareacode IS NOT NULL
		AND round_num <= c1_scn_roundnum;
		IF (c2_lane_roundnum_i IS NOT NULL) THEN
			SELECT  destinationcapacityareacode
			INTO c2_areacode_i
			FROM lane
			WHERE rfpid = ip_rfpId
			AND laneid = c1.bid_id
			AND destinationcapacityareacode IS NOT NULL
			AND round_num = c2_lane_roundnum_i;
		ELSE
			c2_areacode_i := NULL;
		END IF;
		IF (c2_areacode_o IS NOT NULL) THEN
			SELECT  MAX( round_num)
			INTO c2_cbf_roundnum_o
			FROM capacitybidfacility
			WHERE rfpid = ip_rfpId
			AND obcarriercodeid = c1.obcarriercodeid
			AND areacode = c2_areacode_o
			AND DIRECTION = 'OUT'
			AND round_num <= c1_scn_roundnum
			AND weeklycapacity IS NOT NULL;
			c2_facilitycapacity_o := NULL;
			IF (c2_cbf_roundnum_o IS NOT NULL) THEN
				SELECT  weeklycapacity
				INTO c2_areacapacity_o
				FROM capacitybidfacility
				WHERE rfpid = ip_rfpId
				AND obcarriercodeid = c1.obcarriercodeid
				AND areacode = c2_areacode_o
				AND DIRECTION = 'OUT'
				AND round_num = c2_cbf_roundnum_o;
			END IF;
			IF (c2_areacapacity_o IS NOT NULL) THEN
				IF (c1.max_min_val > c2_areacapacity_o) THEN
					v_retVal := v_FAIL;
					op_retVal := v_retVal;
					v_OBA := 1;
				END IF;
			END IF;
		END IF;
		IF (c2_areacode_i IS NOT NULL) THEN
			SELECT  MAX( round_num)
			INTO c2_cbf_roundnum_i
			FROM capacitybidfacility
			WHERE rfpid = ip_rfpId
			AND obcarriercodeid = c1.obcarriercodeid
			AND areacode = c2_areacode_i
			AND DIRECTION = 'IN'
			AND round_num <= c1_scn_roundnum
			AND weeklycapacity IS NOT NULL;
			c2_areacapacity_i := NULL;
			IF (c2_cbf_roundnum_i IS NOT NULL) THEN
				SELECT  weeklycapacity
				INTO c2_areacapacity_i
				FROM capacitybidfacility
				WHERE rfpid = ip_rfpId
				AND obcarriercodeid = c1.obcarriercodeid
				AND areacode = c2_areacode_i
				AND DIRECTION = 'IN'
				AND round_num = c2_cbf_roundnum_i;
			END IF;
			IF (c2_areacapacity_i IS NOT NULL) THEN
				IF (c1.max_min_val > c2_areacapacity_i) THEN
					v_retVal := v_FAIL;
					op_retVal := v_retVal;
					v_IBA := 1;
				END IF;
			END IF;
		END IF;
		IF (v_retVal = v_FAIL) THEN
			SELECT  override_id ,
			comments
			INTO c1_override_id,
			v_comments
			FROM ob200override_adv
			WHERE rfpid = ip_rfpId
			AND scenarioid = ip_scenarioId
			AND SOURCE = 'O'
			AND level_of_detail = 'L'
			AND (TYPE = 'F' OR (TYPE = 'O' AND max_value = -1))
			AND bid_id = c1.bid_id
			AND obcarriercodeid = c1.obcarriercodeid
			AND enable_flag = 1
			AND max_value = -1
			AND min_value = c1.max_min_val
			AND value_type_id = 0
			AND override_id = (SELECT  MAX( o.override_id)
			FROM ob200override_adv o
			WHERE o.rfpid = ip_rfpId
			AND o.scenarioid = ip_scenarioId
			AND o.SOURCE = 'O'
			AND o.level_of_detail = 'L'
			AND (o.TYPE = 'F' OR (o.TYPE = 'O' AND o.max_value = -1))
			AND o.bid_id = c1.bid_id
			AND o.obcarriercodeid = c1.obcarriercodeid
			AND o.enable_flag = 1
			AND o.max_value = -1
			AND o.value_type_id = 0
			AND o.min_value = c1.max_min_val);
			v_override_id := TO_CHAR(c1_override_id);
			IF (v_comments IS NULL) THEN
				v_comments := ' ';
			END IF;
			IF ((v_OBA = 1)
			AND (v_IBA = 1)) THEN
				v_fails_on := 'Lane ('||TO_CHAR(c1.bid_id)||') Carrier ('||c1_carrierName||') conflicts with both Inbound area ('||c2_areacode_i||') Capacity ('||TO_CHAR(c2_areacapacity_i)||' lds/wk) and Outbound area ('||c2_areacode_o||' lds/wk) Capacity ('||TO_CHAR(c2_areacapacity_o)||' lds/wk)';
				v_error_description := 'Lane ('||TO_CHAR(c1.bid_id)||') Carrier ('||c1_carrierName||') OB Area ('||c2_areacode_o||') Capacity ('||TO_CHAR(c2_areacapacity_o)||' lds/wk) < lane max(min_value constraint) value ('||TO_CHAR(c1.max_min_val)||' lds/wk) and IB Area ('||c2_areacode_i||') Capacity ('||TO_CHAR(c2_areacapacity_i)||' lds/wk) < lane max(min_value constraint) value('||TO_CHAR(c1.max_min_val)||' lds/wk)';
			ELSIF (v_OBA = 1) THEN
				v_fails_on := 'Lane ('||TO_CHAR(c1.bid_id)||') Carrier ('||c1_carrierName||') conflicts with  outbound area ('||c2_areacode_o||') Capacity ('||TO_CHAR(c2_areacapacity_o)||' lds/wk)';
				v_error_description := 'Lane ('||TO_CHAR(c1.bid_id)||') Carrier ('||c1_carrierName||') OB Area ('||c2_areacode_o||') Capacity ('||TO_CHAR(c2_areacapacity_o)||' lds/wk) < lane max(min_value constraint) value ('||TO_CHAR(c1.max_min_val)||' lds/wk)';
			ELSIF (v_IBA = 1) THEN
				v_fails_on := 'Lane ('||TO_CHAR(c1.bid_id)||') Carrier ('||c1_carrierName||') conflicts with  inbound area ('||c2_areacode_i||') Capacity ('||TO_CHAR(c2_areacapacity_i)||' lds/wk)';
				v_error_description := 'Lane ('||TO_CHAR(c1.bid_id)||') Carrier ('||c1_carrierName||') IB Area ('||c2_areacode_i||') Capacity ('||TO_CHAR(c2_areacapacity_i)||' lds/wk) < lane max(min_value constraint) value ('||TO_CHAR(c1.max_min_val)||' lds/wk)';
			END IF;
			INSERT INTO INFEASIBILITY_CHECK_RESULTS (rfp_id,scenario_id,infeasibility_id,override_id,comments,fails_on,error_description) VALUES ( ip_rfpId,ip_scenarioId,ip_infeasibilityId,v_override_id,v_comments,v_fails_on,v_error_description );
		END IF;
	END LOOP; 

END Infeasibility_Check_03;
/
create or replace
PROCEDURE PRC_MENU_COUNT_WINMOBILE AS
BEGIN

  execute immediate 'TRUNCATE TABLE MENU_COUNT';

-- Fullfilment->Customer Pickups->Pick [USER count only]
-- Level 4(Insert) START
   insert into menu_count ( created_dttm , last_updated_dttm , menu_count_id , company_id , permission_code , menu_count , last_updated_source , facility_id )
(
-- Level 3 (Sequence) START
select sysdate , sysdate , SEQ_MENU_COUNT_ID.nextval , tc_company_id , 'DO_CUST_ORDER_PICKUPS' , sum ,  usr ,  o_facility_id   from (
-- Level 2 (Sum) START
select  tc_company_id ,  o_facility_id , usr , count(1) sum from  (
-- Level 1(Decode) START
select  distinct orders.order_id , orders.tc_company_id , 'DO_CUST_ORDER_PICKUPS', null ,  orders.o_facility_id ,  decode(orders.last_updated_source_type,2,'EXTERNAL',1,orders.last_updated_source ) usr
from orders orders , order_line_item oli  where
  orders.order_id = oli.order_id and
  orders.o_facility_id  is not null and
  orders.do_type = 20 and
  orders.delivery_options = '01' and
  orders.do_status in ( 100 , 110 ) and
  oli.allocation_source ='10' and
  oli.do_dtl_status!=200 and
  orders.has_import_error=0 and
  orders.last_updated_source_type in ( '1' , '2' ) and
  oli.do_dtl_status!=200
  -- Level 1(Decode) END
  ) group by tc_company_id , o_facility_id , usr
  -- Level 2(Sum) END
  )
  -- Level 3(Sequence) END
  )
  -- Level 4(Insert) END
  ;

-- Fulfilment->Ship to customer->Pick
-- SHIP2CUST_PICK
-- LEVEL 4(Insert) START
insert into menu_count ( created_dttm , last_updated_dttm ,menu_count_id , company_id , permission_code , menu_count , last_updated_source , facility_id )
(
-- LEVEL 3(Sequence) START
select sysdate , sysdate ,  SEQ_MENU_COUNT_ID.nextval , tc_company_id , 'DO_SHIP_TO_CUST_PICK' , sum , usr , o_facility_id from
  (
-- Level 2(Sum) START
(
select  tc_company_id ,  o_facility_id , usr , count(1) sum from  (
-- Level 1(Decode) START
select distinct orders.order_id , orders.tc_company_id , 'DO_SHIP_TO_CUST_PICK' ,  null ,  orders.o_facility_id , decode(orders.last_updated_source_type,2,'EXTERNAL',1,orders.last_updated_source ) usr  from orders orders , order_line_item oli  where

orders.order_id = oli.order_id and

  orders.o_facility_id is not null and
  orders.do_type = 20 and
  orders.do_status in ( 100 , 110) and

  (
     (orders.delivery_options = '02' and orders.destination_action = '01' )
   or(orders.delivery_options = '03')
   ) and

oli.allocation_source ='10' and
orders.has_import_error=0 and
oli.do_dtl_status!=200 and

    orders.last_updated_source_type in ( '1' , '2' )

-- Level 1(Decode) END
    )
   group by tc_company_id , o_facility_id , usr
   )
-- Level 2(Sum) END
   )
-- Level 3(Sequence) END
   );
-- Level 4(Insert) END

-- Fulfilment->Ship to customer->Pick/Pack
--SHIP2CUST_PICK_OR_PACK
-- level 4 (insert) start
insert into menu_count ( created_dttm , last_updated_dttm , menu_count_id , company_id , permission_code , menu_count , user_id , facility_id )
-- level 3 ( sequence) start
select sysdate , sysdate , SEQ_MENU_COUNT_ID.nextval , inner_select.tc_company_id , 'DO_CUST_ORDER' , inner_select.s2c_pick_pack_count , null , inner_select.o_facility_id
  from (
  -- level 2(count) start
   select tc_company_id , 'DO_CUST_ORDER' , count(o_facility_id) s2c_pick_pack_count , null ,  o_facility_id from
  -- level 1(data) start
  (
   select distinct orders.order_id , orders.tc_company_id , 'DO_CUST_ORDER' ,  null ,  orders.o_facility_id from orders orders , order_line_item oli  where
   orders.o_facility_id is not null and
  orders.do_status in  ( 110,130,140,185 ) and
  orders.do_type = 20  and

(
orders.delivery_options = '03' or
(orders.delivery_options = '02' and orders.destination_action = '01'  )
) and
  orders.has_import_error=0 and
  orders.order_id =  oli.order_id and
  oli.allocation_source ='10' and
  oli.do_dtl_status!=200

  )
  -- level 1(data) end
  group by tc_company_id , o_facility_id
  -- level 2(count) end
   )
inner_select
-- level 3(sequence) end
-- level 4 (insert) end
;


-- Fulfilment->Ship to location->XFER-Pick
-- XFER_PICK
-- Level 4(Insert) START
  insert into menu_count ( created_dttm , last_updated_dttm , menu_count_id , company_id , permission_code , menu_count , last_updated_source , facility_id )
  (
  -- Level 3 (Sequence) START
  select sysdate , sysdate , SEQ_MENU_COUNT_ID.nextval , tc_company_id , 'DO_SHIP_TO_LOCN_PICK' , sum ,  usr ,  o_facility_id from
  (
  -- Level 2(Count) START
  (
select  tc_company_id ,  o_facility_id , usr , count(1) sum from  (
  -- Level 1(Decode) START
  select distinct orders.order_id ,  orders.tc_company_id , 'DO_SHIP_TO_LOCN_PICK' ,  null ,  orders.o_facility_id , decode(orders.last_updated_source_type,2,'EXTERNAL',1,orders.last_updated_source ) usr from orders orders , order_line_item oli where

  orders.o_facility_id is not null and
  orders.do_status in ( 100 , 110 ) and
  orders.do_type = 60 and
  orders.has_import_error = 0  and
  orders.order_id =  oli.order_id and
  oli.allocation_source ='10' and
  oli.do_dtl_status!=200 and
  orders.last_updated_source_type in ( '1' , '2' )
   -- Level 1(Decode) END
  )
  group by tc_company_id , o_facility_id  , usr
  )
  -- Level 2(Count) END
  )
-- Level 3(Sequence) END
   )
   -- Level 4(Insert) END
   ;

-- Fulfilment->Ship to location->XFER-Pick/Pack
-- XFER_PICK_PACK
-- level 4 ( insert) start
insert into menu_count ( created_dttm , last_updated_dttm , menu_count_id , company_id , permission_code , menu_count , user_id , facility_id )
-- level 3 ( sequence) start
select sysdate , sysdate , SEQ_MENU_COUNT_ID.nextval , inner_select.tc_company_id , 'DO_TRANSFER' , inner_select.xfer_pick_pack , null , inner_select.o_facility_id
  from (
   -- level 2 ( count) start
  select tc_company_id , 'DO_TRANSFER' , count(o_facility_id) xfer_pick_pack , null ,  o_facility_id from
  -- level 1 ( data) start
 (
  select distinct orders.order_id , orders.tc_company_id , 'DO_TRANSFER' ,  null ,  orders.o_facility_id from orders orders , order_line_item oli  where

   orders.o_facility_id is not null and
   orders.do_status in (110,130,140,185) and
   orders.do_type = 60    and
   (
   (orders.delivery_options = '03')  or
   (orders.delivery_options = '02' and orders.destination_action = '01')
   )   and

   orders.has_import_error=0 and
   orders.order_id =  oli.order_id and
   oli.allocation_source ='10' and
   oli.do_dtl_status!=200 and
   oli.order_id=orders.order_id
    )
    -- level 1 ( data) end
   group by tc_company_id ,  o_facility_id
     -- level 2 ( count) end
     )
inner_select
-- level 3 ( sequence) end
-- level 4 ( insert) end
;

-- 'RTNS_PICK_PACK
-- Fulfilment->Ship to location->Returns->Pick/Pack
-- level 4 ( insert) end
insert into menu_count ( created_dttm , last_updated_dttm , menu_count_id , company_id , permission_code , menu_count , user_id , facility_id )
-- level 3 ( sequence) start
select sysdate , sysdate , SEQ_MENU_COUNT_ID.nextval , inner_select.tc_company_id , 'DO_RETURNS' , inner_select.xfer_pick_pack , null , inner_select.o_facility_id
  from
  (
  -- level 2(count) start
   select tc_company_id , 'DO_RETURNS' , count(o_facility_id) xfer_pick_pack ,  o_facility_id from
-- level 1 (list) start
   (
  select distinct  orders.order_id ,  orders.tc_company_id , 'DO_RETURNS' , null ,  orders.o_facility_id from orders orders , order_line_item oli
  where

orders.o_facility_id is not null and
orders.do_status in ( 110 , 130 , 140 , 185  ) and
orders.do_type = 40 and

orders.has_import_error=0 and
orders.order_id =  oli.order_id and
oli.allocation_source ='10' and
oli.do_dtl_status!=200
)
-- level 1(list) end
group by tc_company_id , o_facility_id

-- level 2( count) end
  )  inner_select
  -- level 3 ( sequence) end
  -- level 4 ( insert) end


  ;


  --INVENTORY-> CYCLE COUNT
  -- CYCLE_COUNT
	  INSERT INTO MENU_COUNT ( CREATED_DTTM , LAST_UPDATED_DTTM , MENU_COUNT_ID , COMPANY_ID , PERMISSION_CODE , MENU_COUNT , USER_ID , FACILITY_ID )  (SELECT SYSDATE ,
	  SYSDATE ,
	  SEQ_MENU_COUNT_ID.nextval ,
        COUNT_INNER_SELECT.TC_COMPANY_ID ,
        'CYCLE_COUNT' ,
        COUNT_INNER_SELECT.CYCCNT_COUNT ,
        COUNT_INNER_SELECT.ASSIGNED_USER_ID ,
        COUNT_INNER_SELECT.FACILITY_ID
      FROM
        (SELECT TC_COMPANY_ID ,
          FACILITY_ID ,
          'CYCLE_COUNT' ,
          COUNT(ASSIGNED_USER_ID) CYCCNT_COUNT ,
          ASSIGNED_USER_ID ,
          NULL
        FROM
          (
          -- LEVEL 2 START ( FILTER DATA RANK = 1)
          SELECT TC_COMPANY_ID,
            FACILITY_ID,
            CYCLE_COUNT_REQUEST_ID,
            TC_CYCLE_COUNT_REQUEST_ID,
            CYCLE_COUNT_REQUEST_NAME,
            CYCLE_COUNT_REQUEST_STORE_ID,
            SCHEDULED_DTTM,
            COMPLETE_BY_DTTM,
            EARLIEST_START_BY_DTTM,
            CYCLE_COUNT_REQUEST_STATUS,
            ASSIGNED_USER_ID
          FROM
            (
            -- LEVEL 1 START ( JOIN CCR , CCRS , CCI , CCIA )
            SELECT CCR.TC_COMPANY_ID ,
              CCRS.FACILITY_ID,
              CCR.CYCLE_COUNT_REQUEST_ID,
              CCR.TC_CYCLE_COUNT_REQUEST_ID,
              CCR.CYCLE_COUNT_REQUEST_NAME,
              CCRS.CYCLE_COUNT_REQUEST_STORE_ID,
              CCRS.SCHEDULED_DTTM,
              CCRS.COMPLETE_BY_DTTM,
              CCRS.EARLIEST_START_BY_DTTM,
              CCRS.CYCLE_COUNT_REQUEST_STATUS,
              NVL(CCIA.ASSIGNED_USER_ID,-1) ASSIGNED_USER_ID,
              DECODE(CCIA.CYCLE_COUNT_ASSIGN_STATUS,30,30,NULL) ASSIGN_STATUS,
              RANK() OVER (PARTITION BY CCR.TC_CYCLE_COUNT_REQUEST_ID ORDER BY CCIA.ASSIGNED_USER_ID ) DATARANK
            FROM CYCLE_COUNT_REQUEST CCR
            LEFT JOIN CYCLE_COUNT_REQUEST_STORE CCRS
            ON CCRS.CYCLE_COUNT_REQUEST_ID = CCR.CYCLE_COUNT_REQUEST_ID
            LEFT JOIN CYCLE_COUNT_ITEMS CCI
            ON CCI.CYCLE_COUNT_REQUEST_STORE_ID = CCRS.CYCLE_COUNT_REQUEST_STORE_ID
            LEFT JOIN CYCLE_COUNT_ITEM_ASSIGNMENT CCIA
            ON CCIA.CYCLE_COUNT_ITEM_ID          = CCI.CYCLE_COUNT_ITEM_ID
            WHERE CCR.IS_CANCELLED               = 0
            AND CCRS.CYCLE_COUNT_REQUEST_STATUS != 40
            AND CCRS.CYCLE_COUNT_REQUEST_STATUS !=30
            AND CCI.CYCLE_COUNT_ITEM_STATUS!     =40
            AND CCI.CYCLE_COUNT_ITEM_STATUS!     =60
            AND CCI.CYCLE_COUNT_ITEM_STATUS!     =70

            AND ( ( CCRS.SCHEDULED_DTTM         >= TRUNC(SYSDATE)
            AND CCRS.SCHEDULED_DTTM              < TRUNC(SYSDATE + 1) )
            OR ( CCRS.SCHEDULED_DTTM            IS NULL
            AND CCRS.EARLIEST_START_BY_DTTM      < TRUNC(SYSDATE + 1)
            AND CCRS.COMPLETE_BY_DTTM           >= TRUNC(SYSDATE) ) )

            GROUP BY CCR.TC_COMPANY_ID ,
              CCRS.FACILITY_ID ,
              CCR.CYCLE_COUNT_REQUEST_ID,
              CCR.TC_CYCLE_COUNT_REQUEST_ID,
              CCR.CYCLE_COUNT_REQUEST_NAME,
              CCRS.CYCLE_COUNT_REQUEST_STORE_ID,
              CCRS.SCHEDULED_DTTM,
              CCRS.COMPLETE_BY_DTTM,
              CCRS.EARLIEST_START_BY_DTTM,
              CCRS.CYCLE_COUNT_REQUEST_STATUS,
              CCIA.ASSIGNED_USER_ID,
              CCIA.CYCLE_COUNT_ASSIGN_STATUS
              -- LEVEL 1 END ( JOIN CCR , CCRS , CCI , CCIA )
            )
          WHERE DATARANK     = 1
          AND ASSIGN_STATUS IS NULL
          GROUP BY TC_COMPANY_ID,
            FACILITY_ID,
            CYCLE_COUNT_REQUEST_ID,
            TC_CYCLE_COUNT_REQUEST_ID,
            CYCLE_COUNT_REQUEST_NAME,
            CYCLE_COUNT_REQUEST_STORE_ID,
            SCHEDULED_DTTM,
            COMPLETE_BY_DTTM,
            EARLIEST_START_BY_DTTM,
            CYCLE_COUNT_REQUEST_STATUS,
            ASSIGNED_USER_ID
          ORDER BY COMPLETE_BY_DTTM ASC
            -- LEVEL 2 END ( FILTER DATA RANK = 1)
          )
        GROUP BY TC_COMPANY_ID ,
          FACILITY_ID ,
          ASSIGNED_USER_ID
          -- LEVEL 3 END  ( GROUP AND COUNT PER USER)
        ) COUNT_INNER_SELECT
        -- LEVEL 4 END  ( MENU COUNT SEQUENCE )
    );


-- CYCLE COUNT. MANAGER LEVEL COUNT. USER_ID AS -2 INDICATES MANAGER AND USER_ID AS -1 INDICATES UNASSIGNED.
  INSERT
  INTO MENU_COUNT
    (
      CREATED_DTTM ,
      LAST_UPDATED_DTTM ,
      MENU_COUNT_ID ,
      COMPANY_ID ,
      PERMISSION_CODE ,
      MENU_COUNT ,
      USER_ID ,
      FACILITY_ID
    )
    (      -- LEVEL 4 START  ( MENU COUNT SEQUENCE )
      SELECT SYSDATE ,
        SYSDATE ,
        SEQ_MENU_COUNT_ID.nextval ,
        COUNT_INNER_SELECT.TC_COMPANY_ID ,
        'CYCLE_COUNT' ,
        COUNT_INNER_SELECT.CYCCNT_COUNT ,
        -2 , -- MANAGER
        COUNT_INNER_SELECT.FACILITY_ID
      FROM
        (
        -- LEVEL 3 START  ( GROUP AND COUNT PER USER)
        SELECT TC_COMPANY_ID ,
          FACILITY_ID ,
          'CYCLE_COUNT' ,
          COUNT(ASSIGNED_USER_ID) CYCCNT_COUNT ,
          -2 ,
          NULL
        FROM
          (
          -- LEVEL 2 START ( FILTER DATA RANK = 1)
          SELECT TC_COMPANY_ID,
            FACILITY_ID,
            CYCLE_COUNT_REQUEST_ID,
            TC_CYCLE_COUNT_REQUEST_ID,
            CYCLE_COUNT_REQUEST_NAME,
            CYCLE_COUNT_REQUEST_STORE_ID,
            SCHEDULED_DTTM,
            COMPLETE_BY_DTTM,
            EARLIEST_START_BY_DTTM,
            CYCLE_COUNT_REQUEST_STATUS,
            ASSIGNED_USER_ID
          FROM
            (
            -- LEVEL 1 START ( JOIN CCR , CCRS , CCI , CCIA )
            SELECT CCR.TC_COMPANY_ID ,
              CCRS.FACILITY_ID,
              CCR.CYCLE_COUNT_REQUEST_ID,
              CCR.TC_CYCLE_COUNT_REQUEST_ID,
              CCR.CYCLE_COUNT_REQUEST_NAME,
              CCRS.CYCLE_COUNT_REQUEST_STORE_ID,
              CCRS.SCHEDULED_DTTM,
              CCRS.COMPLETE_BY_DTTM,
              CCRS.EARLIEST_START_BY_DTTM,
              CCRS.CYCLE_COUNT_REQUEST_STATUS,
              NVL(CCIA.ASSIGNED_USER_ID,-1) ASSIGNED_USER_ID,
              DECODE(CCIA.CYCLE_COUNT_ASSIGN_STATUS,30,30,NULL) ASSIGN_STATUS,
              RANK() OVER (PARTITION BY CCR.TC_CYCLE_COUNT_REQUEST_ID ORDER BY CCIA.ASSIGNED_USER_ID ) DATARANK
            FROM CYCLE_COUNT_REQUEST CCR
            LEFT JOIN CYCLE_COUNT_REQUEST_STORE CCRS
            ON CCRS.CYCLE_COUNT_REQUEST_ID = CCR.CYCLE_COUNT_REQUEST_ID
            LEFT JOIN CYCLE_COUNT_ITEMS CCI
            ON CCI.CYCLE_COUNT_REQUEST_STORE_ID = CCRS.CYCLE_COUNT_REQUEST_STORE_ID
            LEFT JOIN CYCLE_COUNT_ITEM_ASSIGNMENT CCIA
            ON CCIA.CYCLE_COUNT_ITEM_ID          = CCI.CYCLE_COUNT_ITEM_ID
            WHERE CCR.IS_CANCELLED               = 0
            AND CCRS.CYCLE_COUNT_REQUEST_STATUS != 40
            AND CCRS.CYCLE_COUNT_REQUEST_STATUS !=30
            AND CCI.CYCLE_COUNT_ITEM_STATUS!     =40
            AND CCI.CYCLE_COUNT_ITEM_STATUS!     =60
            AND CCI.CYCLE_COUNT_ITEM_STATUS!     =70

            AND ( ( CCRS.SCHEDULED_DTTM         >= TRUNC(SYSDATE)
            AND CCRS.SCHEDULED_DTTM              < TRUNC(SYSDATE + 1) )
            OR ( CCRS.SCHEDULED_DTTM            IS NULL
            AND CCRS.EARLIEST_START_BY_DTTM      < TRUNC(SYSDATE + 1)
            AND CCRS.COMPLETE_BY_DTTM           >= TRUNC(SYSDATE) ) )

            GROUP BY CCR.TC_COMPANY_ID ,
              CCRS.FACILITY_ID ,
              CCR.CYCLE_COUNT_REQUEST_ID,
              CCR.TC_CYCLE_COUNT_REQUEST_ID,
              CCR.CYCLE_COUNT_REQUEST_NAME,
              CCRS.CYCLE_COUNT_REQUEST_STORE_ID,
              CCRS.SCHEDULED_DTTM,
              CCRS.COMPLETE_BY_DTTM,
              CCRS.EARLIEST_START_BY_DTTM,
              CCRS.CYCLE_COUNT_REQUEST_STATUS,
              CCIA.ASSIGNED_USER_ID,
              CCIA.CYCLE_COUNT_ASSIGN_STATUS
              -- LEVEL 1 END ( JOIN CCR , CCRS , CCI , CCIA )
            )
          WHERE DATARANK     = 1
          AND ASSIGN_STATUS IS NULL
          GROUP BY TC_COMPANY_ID,
            FACILITY_ID,
            CYCLE_COUNT_REQUEST_ID,
            TC_CYCLE_COUNT_REQUEST_ID,
            CYCLE_COUNT_REQUEST_NAME,
            CYCLE_COUNT_REQUEST_STORE_ID,
            SCHEDULED_DTTM,
            COMPLETE_BY_DTTM,
            EARLIEST_START_BY_DTTM,
            CYCLE_COUNT_REQUEST_STATUS,
            ASSIGNED_USER_ID
          ORDER BY COMPLETE_BY_DTTM ASC
            -- LEVEL 2 END ( FILTER DATA RANK = 1)
          )
        GROUP BY TC_COMPANY_ID ,
          FACILITY_ID
          -- LEVEL 3 END  ( GROUP AND COUNT PER USER)
        ) COUNT_INNER_SELECT
        -- LEVEL 4 END  ( MENU COUNT SEQUENCE )
)
;

  --Adds the EXTERNAL counts to the user level counts
  update menu_count m1 set m1.menu_count =
( nvl(m1.menu_count,0) +
nvl((select nvl(m2.menu_count,0) from menu_count m2 where m2.company_id = m1.company_id and m1.facility_id=m2.facility_id and  m2.PERMISSION_CODE = m1.PERMISSION_CODE and m2.LAST_UPDATED_SOURCE = 'EXTERNAL'),0)
)
where m1.PERMISSION_CODE in (  'DO_CUST_ORDER_PICKUPS' , 'DO_SHIP_TO_CUST_PICK' , 'DO_SHIP_TO_LOCN_PICK'  )  and m1.LAST_UPDATED_SOURCE <> 'EXTERNAL';


insert into menu_count ( created_dttm , last_updated_dttm , MENU_COUNT_ID , company_id , permission_code , menu_count , user_id , last_updated_source , facility_id )
select sysdate , sysdate , SEQ_MENU_COUNT_ID.nextval , inner_select.tc_company_id , 'Pullback' , inner_select.menu_count , null , usr , inner_select.o_facility_id
  from (
   -- level 2 ( count) start
  select tc_company_id , 'Pullback' , count(o_facility_id) menu_count , null , usr , o_facility_id from
  -- level 1 ( data) start
(
  select distinct orders.order_id , orders.tc_company_id , 'Pullback' ,  null , null usr,  
  orders.o_facility_id from orders orders , order_line_item oli, company_parameter cp
  where
   orders.o_facility_id is not null and
   orders.do_status in (110) and
   orders.tc_company_id = cp.tc_company_id and
   UPPER(orders.order_type) = UPPER(cp.param_value) and
   cp.param_def_id = 'PULLBACK_ORDER_TYPE' and
   orders.do_type = 40  and
   orders.has_import_error=0  and
   orders.order_id =  oli.order_id and
   oli.allocation_source ='10' and
   oli.do_dtl_status!=200 and
   oli.do_dtl_status = 110 and
   oli.order_id=orders.order_id and
   oli.item_id is not null
    )
    -- level 1 ( data) end
   group by tc_company_id ,  o_facility_id , usr
     -- level 2 ( count) end
     )
inner_select
-- level 3 ( sequence) end
-- level 4 ( insert) end
;

insert into menu_count ( created_dttm , last_updated_dttm , MENU_COUNT_ID , company_id , permission_code , menu_count , user_id , last_updated_source , facility_id )
select sysdate , sysdate , SEQ_MENU_COUNT_ID.nextval , inner_select.tc_company_id , 'Store to Store' , inner_select.menu_count , null , usr , inner_select.o_facility_id
  from (
   -- level 2 ( count) start
  select tc_company_id , 'Store to Store' , count(o_facility_id) menu_count , null , usr , o_facility_id from
  -- level 1 ( data) start
(
  select distinct orders.order_id , orders.tc_company_id , 'Store to Store' ,  null , null usr,  
  orders.o_facility_id from orders orders , order_line_item oli, company_parameter cp
  where
   orders.o_facility_id is not null and
   orders.do_status in (110) and
   orders.tc_company_id = cp.tc_company_id and
   UPPER(orders.order_type) = UPPER(cp.param_value) and
   cp.param_def_id = 'STORE_TRANSFER_ORDER_TYPE' and
   orders.do_type = 40  and
   orders.has_import_error=0  and
   orders.order_id =  oli.order_id and
   oli.allocation_source ='10' and
   oli.do_dtl_status!=200 and
   oli.do_dtl_status = 110 and
   oli.order_id=orders.order_id and
   oli.item_id is not null
    )
    -- level 1 ( data) end
   group by tc_company_id ,  o_facility_id , usr
     -- level 2 ( count) end
     )
inner_select
-- level 3 ( sequence) end
-- level 4 ( insert) end
;


--Adds the no-user assigned cycle counts to the user level counts ( user_id = -1)
--UPDATE menu_count m1
--SET m1.menu_count = ( NVL(m1.menu_count,0) + NVL(
--  (SELECT NVL(m2.menu_count,0)
--  FROM menu_count m2
--  WHERE m2.company_id    = m1.company_id
--  AND m2.PERMISSION_CODE = m1.PERMISSION_CODE
-- AND m2.user_id         = -1
--  ),0) )
--WHERE m1.PERMISSION_CODE   IN ( 'CYCLE_COUNT' )
--AND m1.user_id           <> -1 ;


commit;

END PRC_MENU_COUNT_WINMOBILE;
/

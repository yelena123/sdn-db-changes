create or replace procedure wm_invoice_orders_in_list
(
    p_invc_batch_nbr    in number,
    p_user_id           in user_profile.user_id%type,
    p_tc_company_id     in facility.tc_company_id%type,
    p_pre_bill_flag     in number
)
as
    va_order_list           t_num;
    v_invc_canceled_orders  number(1) := 0;    
begin
    select case when exists
    (
        select 1
        from company_parameter cp
        where cp.param_def_id = 'SHIP_CONFIRM_CANCELLED_ORDERS'
            and cp.param_value = 'true' and cp.param_group_id = 'COM_WM'
            and (cp.tc_company_id = p_tc_company_id or
                cp.tc_company_id = ( select c.parent_company_id from company c where c.company_id = p_tc_company_id))
    ) then 1 else 0 end
    into v_invc_canceled_orders
    from dual;
    
    if (p_pre_bill_flag in (0, 2))
    then
        -- avoid lpn->OLI load for order level PB
        -- save orig oli data as this gets overwritten next by this point, all the ld's have been 
        -- split in the retail flow
        insert into tmp_invc_orig_oli
        (
            order_id, line_item_id, reference_line_item_id, units_pakd, parent_order_id, 
            packing_short_qty
        )
		--@CUSTBASE_PSO_REYE_BASE_MONO_START 
		--Fix for SF 4113518 start
        /*select oli.order_id, oli.line_item_id, oli.reference_line_item_id, iv.units_pakd, 
            o.parent_order_id, coalesce(oli.received_qty, 0)
        from
        (
            select ld.distribution_order_dtl_id, sum(ld.size_value) units_pakd
            from lpn_detail ld
            where ld.size_value > 0
                and exists
                (
                    select 1
                    from tmp_invc_lpn_list t
                    where t.lpn_id = ld.lpn_id
                )
            group by ld.distribution_order_dtl_id
        ) iv
        join order_line_item oli on oli.line_item_id = iv.distribution_order_dtl_id
        join orders o on o.order_id = oli.order_id
        where o.is_original_order = 1*/
		
		select oli.order_id, oli.line_item_id, oli.reference_line_item_id,
             (select sum(size_value) from lpn_detail,tmp_invc_lpn_list
              where lpn_detail.lpn_id = tmp_invc_lpn_list.lpn_id 
             and lpn_detail.distribution_order_dtl_id = oli.line_item_id and lpn_detail.size_value >= 0 )
             , o.parent_order_id, coalesce(oli.received_qty, 0)
             
        from order_line_item oli
        join orders o on o.order_id = oli.order_id
        where o.is_original_order = 1
        and o.order_id in (select order_id from tmp_invc_lpn_list)
		--Fix for SF 4113518 end
		--@CUSTBASE_PSO_REYE_BASE_MONO_END
        log errors (to_char(sysdate));
        wm_cs_log('Collected orig OLI''s derived from lpn_detail ' || sql%rowcount);        
    end if;

    if (p_pre_bill_flag = 0)
    then
        -- update agg oli via shipped lpns; packing shorted qty comes via orig oli
        merge into order_line_item oli
        using
        (
            select t.reference_line_item_id, sum(t.units_pakd) units_pakd
            from tmp_invc_orig_oli t
            group by t.reference_line_item_id
        ) iv on (iv.reference_line_item_id = oli.line_item_id)
        when matched then
        update set oli.last_updated_source = p_user_id, oli.last_updated_dttm = sysdate,
            oli.shipped_qty = coalesce(oli.shipped_qty, 0) + iv.units_pakd,
            oli.do_dtl_status = decode(oli.order_qty, 
                coalesce(oli.shipped_qty, 0) + iv.units_pakd, 190, oli.do_dtl_status)
        log errors (to_char(sysdate));
        wm_cs_log('Agg OLI''s updated ' || sql%rowcount);
        
        -- todo: subs, ppack ratios
        -- orig oli qty sync from shipped lpns
        merge into order_line_item oli
        using
        (
            select t.order_id, t.line_item_id, t.units_pakd
            from tmp_invc_orig_oli t
        ) iv on (iv.order_id = oli.order_id and iv.line_item_id = oli.line_item_id)
        when matched then
        update set oli.last_updated_source = p_user_id, oli.last_updated_dttm = sysdate,
            oli.shipped_qty = coalesce(oli.shipped_qty, 0) + iv.units_pakd,
            oli.received_qty = 0,
            oli.do_dtl_status = decode(oli.order_qty, 
                coalesce(oli.shipped_qty, 0) + iv.units_pakd, 190, oli.do_dtl_status)
        log errors (to_char(sysdate));
        wm_cs_log('Orig OLI''s updated ' || sql%rowcount);

        -- cache the old order status for 601 pix
        insert into tmp_wave_old_order_status
        (
            order_id, do_status
        )
        select o.order_id, o.do_status
        from orders o
        join
        (
            select t.order_id
            from tmp_invc_orig_oli t
            union
            select t.order_id
            from tmp_invc_order_list t
        ) iv on (iv.order_id = o.order_id)
        where o.is_original_order = 1;
        wm_cs_log('Cache the orders for 601 pix' || sql%rowcount);

        -- rollup orig do_status for retail and DTC flows; this is for those that
        -- had something shipped or those that were recently shorted
        -- back ord flag for cancelled orders needs to be 'C'
        merge into orders o1
        using
        (
            -- orig orders that are getting shipped or shorted
            -- when < 200, join with oli to calc final do_status and back ord qty
            -- per Shashank, when shorted, only the orig order is passed to the proc
            with giv as
            (
                select o.order_id, o.do_status,
                    coalesce(sum(oli.units_pakd), 0) sum_units_pakd,
                    sum(oli.adjusted_order_qty) sum_adjusted_order_qty,
                    coalesce(min(oli.do_dtl_status), o.do_status) min_dtl_status,
                    coalesce(max(nullif(oli.do_dtl_status, 200)), o.do_status) max_dtl_status,
                    min(case when o.parent_order_id is null then 1 else 0 end) perform_lpn_join
                from orders o
                join
                (
                    select t.order_id
                    from tmp_invc_orig_oli t
                    union
                    select t.order_id
                    from tmp_invc_order_list t
                ) iv on (iv.order_id = o.order_id)
                left join order_line_item oli on oli.order_id = o.order_id and o.do_status < 200
                where o.is_original_order = 1
                group by o.order_id, o.do_status
            )
            select iv1.order_id, iv1.sum_units_pakd, iv1.sum_adjusted_order_qty,
                (
                    case
                    when iv1.do_status_tmp between 150 and 180 and iv1.min_lpn_status is not null then
                        case
                        when iv1.min_lpn_status in (20, 30) and iv1.cannot_stage = 0 then 165
                        else decode(iv1.min_lpn_status, 10, 140, 15, 140, 20, 150, 30, 160, 40, 170, 50, 180, 90, 190, 200)
                        end
                    else iv1.do_status_tmp
                    end
                ) new_do_status
            from
            (
                select giv.order_id, giv.sum_units_pakd, giv.sum_adjusted_order_qty,
                    giv.min_dtl_status, giv.max_dtl_status,
                    min(l.lpn_facility_status) min_lpn_status,
                    min
                    (
                        case
                        when giv.do_status = 200 then 200
                        when giv.min_dtl_status in (110, 112) and giv.min_dtl_status != giv.max_dtl_status then 120
                        when giv.min_dtl_status = 130 and giv.max_dtl_status > 130 then 140
                        else coalesce(giv.min_dtl_status, 200)
                        end
                    ) do_status_tmp,
                    max
                    (
                        case
                        when l.lpn_facility_status < 50
                            and
                            (
                                coalesce(l.stage_indicator, 0) != 10
                                or coalesce(lh.locn_class, ' ') not in ('P', 'S', 'O')
                            ) then 1
                        else 0
                        end
                    ) cannot_stage
                from giv
                left join lpn l on l.order_id = giv.order_id and l.inbound_outbound_indicator = 'O'
                    and
                    (
                        case
                        when giv.do_status = 200 then 200
                        when giv.min_dtl_status in (110, 112) and giv.min_dtl_status != giv.max_dtl_status then 120
                        when giv.min_dtl_status = 130 and giv.max_dtl_status > 130 then 140
                        else coalesce(giv.min_dtl_status, 200)
                        end
                    ) between 150 and 180
                left join locn_hdr lh on lh.locn_id = l.curr_sub_locn_id
                where giv.perform_lpn_join = 1
                group by giv.order_id, giv.sum_units_pakd, giv.sum_adjusted_order_qty,
                    giv.min_dtl_status, giv.max_dtl_status
            ) iv1
            union all
            select giv.order_id, giv.sum_units_pakd, giv.sum_adjusted_order_qty,
                case
                when giv.do_status = 200 then 200
                when giv.min_dtl_status in (110, 112) and giv.min_dtl_status != giv.max_dtl_status then 120
                when giv.min_dtl_status = 130 and giv.max_dtl_status > 130 then 140
                else coalesce(giv.min_dtl_status, 200)
                end new_do_status
            from giv
            where giv.perform_lpn_join = 0
        ) iv2 on (iv2.order_id = o1.order_id)
        when matched then
        update set o1.last_updated_source = p_user_id, o1.last_updated_dttm = sysdate,
            o1.actual_shipped_dttm = (case when o1.do_status < 190 then sysdate
                else o1.actual_shipped_dttm end),
            o1.is_back_ordered =
            (
                case
                when o1.do_status = 200 then 'C'
                when iv2.sum_units_pakd < iv2.sum_adjusted_order_qty then '2'
                else '1'
                end
            ),
            o1.do_status = iv2.new_do_status,
            o1.prtl_ship_conf_status =
            (
                case
                when iv2.new_do_status < 190 and coalesce(o1.prtl_ship_conf_status, 0) in (0, 1) then 1
                when iv2.new_do_status = 190 and coalesce(o1.prtl_ship_conf_status, 0) = 1 then 2
                when iv2.new_do_status = 200 then 3
                when iv2.new_do_status = 190 and coalesce(o1.prtl_ship_conf_status, 0) = 0 then 5
                else 4
                end
            )
        log errors (to_char(sysdate));
        wm_cs_log('Orig orders updated ' || sql%rowcount);

        -- derive agg order status using all associated orig orders that had
        -- something shipped; no need to account for shorts here
        merge into orders agg
        using
        (
            with order_list as
            (
                select o.parent_order_id, min(o.do_status) min_orig_status
                from orders o
                where exists
                (
                    select 1
                    from tmp_invc_orig_oli t
                    where t.parent_order_id = o.parent_order_id
                )
                group by o.parent_order_id
            )
            select o.parent_order_id, o.min_orig_status, null min_lpn_stat
            from order_list o
            where o.min_orig_status != 150
            union all
            select o.parent_order_id, 150 min_orig_status,
                min(l.lpn_facility_status) min_lpn_stat
            from order_list o
            join lpn l on l.order_id = o.parent_order_id
            where o.min_orig_status = 150
            group by o.parent_order_id
        ) iv on (iv.parent_order_id = agg.order_id)
        when matched then
        update set agg.last_updated_source = p_user_id, agg.last_updated_dttm = sysdate,
            agg.do_status =
            (
                case
                when agg.do_status > 130 and iv.min_orig_status = 130 then 140
                when iv.min_orig_status = 150
                    then decode(iv.min_lpn_stat, 10, 140, 15, 140, 30, 160, 40, 170, 50, 180, 90, agg.do_status, 150)
                else iv.min_orig_status
                end
            )
        log errors (to_char(sysdate));
        wm_cs_log('Agg orders status update ' || sql%rowcount);

        select iv.order_id
        bulk collect into va_order_list
        from
        (
            select t.order_id
            from tmp_invc_orig_oli t
            union
            select t.order_id
            from tmp_invc_order_list t
        ) iv;
        wm_cs_log('Collected orders for stale shpmt check ' || sql%rowcount);

        wm_unplan_pakd_orders_in_list(p_user_id, va_order_list);
    else
        -- update orders to pre billed
        update orders o
        set o.pre_bill_status = '1', o.last_updated_dttm = sysdate,
            o.last_updated_source = p_user_id
        where exists
        (
            select 1
            from tmp_invc_order_list t
            where t.order_id = o.order_id
        );
        wm_cs_log('Orders updated to pre billed ' || sql%rowcount);
    end if;

    insert into outpt_order_line_item_size
    (
        batch_ctrl_nbr, created_dttm, created_source, created_source_type,
        invc_batch_nbr, last_updated_dttm, last_updated_source, proc_dttm,
        last_updated_source_type, line_item_id, order_id, size_value,
        shipped_size_value, received_size_value, size_uom, proc_stat_code,
        outpt_order_line_item_size_id, tc_order_id, tc_company_id, version_nbr
    )
    select p_invc_batch_nbr, sysdate, p_user_id, 1, p_invc_batch_nbr, sysdate,
        p_user_id, sysdate, 1, olis.line_item_id, olis.order_id, olis.size_value,
        olis.shipped_size_value, olis.received_size_value, su.size_uom, 0,
        outptorderlineitemsize_id_seq.nextval, o.tc_order_id,
        olis.tc_company_id, 1 version_nbr
    from order_line_item_size olis
    join tmp_invc_orig_oli t on t.order_id = olis.order_id
        and t.line_item_id = olis.line_item_id
    join orders o on o.order_id = olis.order_id
        and (v_invc_canceled_orders = 1 or o.do_status != 200)
    join size_uom su on su.size_uom_id = olis.size_uom_id
    log errors into err$_outpt_order_line_item_siz(to_char(sysdate));
    wm_cs_log('Outpt OLIS insert ' || sql%rowcount);
        
    -- when the line is closed, wave shorts are collected into UCQ in the outpt table
    insert into outpt_order_line_item
    (
        actual_cost, actual_cost_currency_code, actual_shipped_dttm, aisle, area,
        assort_nbr, batch_ctrl_nbr, batch_nbr, bay,
        cntry_of_orgn, commodity_class, created_dttm, created_source, created_source_type,
        customer_item, exp_info_code, gtin, invc_batch_nbr, invn_type, item_attr_1,
        item_attr_2, item_attr_3, item_attr_4, item_attr_5, item_color, item_color_sfx,
        item_id, item_name, item_quality, item_season, item_season_year, item_second_dim,
        item_size_desc, item_style, item_style_sfx,
        line_item_id, line_type, lvl, manufacturing_dttm,
        order_qty_uom, orig_item_color,
        orig_item_color_sfx, orig_item_id, orig_item_quality, orig_item_season,
        orig_item_season_year, orig_item_second_dim, orig_item_size_desc, orig_item_style,
        orig_item_style_sfx, orig_order_qty, orig_order_qty_uom,
        outpt_order_line_item_id, parent_line_item_id, pickup_end_dttm, pickup_reference_number,
        pickup_start_dttm,  planned_quantity_uom,
        planned_ship_date,posn, ppack_grp_code, ppack_qty, price,
        price_tkt_type, proc_stat_code, product_status, purchase_order_line_number,
        reason_code, retail_price, shelf_days, shipped_qty, tc_order_line_id,
        tc_company_id, tc_order_id, tc_purchase_orders_id,
        unit_vol, unit_wt, uom, version_nbr, zone, order_qty, planned_quantity,
        user_canceled_qty, orig_order_line_item_id, last_updated_dttm,
        last_updated_source, last_updated_source_type, proc_dttm,
        ext_purchase_order, purchase_order_number, back_ord_qty, temp_zone
    )
    select oli.actual_cost, oli.actual_cost_currency_code, oli.actual_shipped_dttm,
        lh.aisle, lh.area,
         oli.assort_nbr, p_invc_batch_nbr, oli.batch_nbr,
        lh.bay,
        oli.cntry_of_orgn, oli.commodity_class, sysdate,
        p_user_id, 1, oli.customer_item, oli.exp_info_code,
        oli.sku_gtin, p_invc_batch_nbr, oli.invn_type, oli.item_attr_1, oli.item_attr_2,
        oli.item_attr_3, oli.item_attr_4, oli.item_attr_5, ic.item_color, ic.item_color_sfx,
        oli.item_id, ic.item_name, ic.item_quality, ic.item_season, ic.item_season_year,
        ic.item_second_dim, ic.item_size_desc, ic.item_style, ic.item_style_sfx,
        oli.line_item_id, oli.line_type,
        lh.lvl,
        oli.manufacturing_dttm,
        su.size_uom,
        case when oli2.line_item_id is null then ic.item_color else ic2.item_color end,
        case when oli2.line_item_id is null then ic.item_color_sfx else ic2.item_color_sfx end,
        case when oli2.line_item_id is null then ic.item_id else ic2.item_id end,
        case when oli2.line_item_id is null then ic.item_quality else ic2.item_quality end,
        case when oli2.line_item_id is null then ic.item_season else ic2.item_season end,
        case when oli2.line_item_id is null then ic.item_season_year else ic2.item_season_year end,
        case when oli2.line_item_id is null then ic.item_second_dim else ic2.item_second_dim end,
        case when oli2.line_item_id is null then ic.item_size_desc else ic2.item_size_desc end,
        case when oli2.line_item_id is null then ic.item_style else ic2.item_style end,
        case when oli2.line_item_id is null then ic.item_style_sfx else ic2.item_style_sfx end,
        oli.orig_order_qty,
        su.size_uom,
        outpt_order_line_item_id_seq.nextval,
        oli.parent_line_item_id, oli.pickup_end_dttm, oli.pickup_reference_number,
        oli.pickup_start_dttm,
        su.size_uom,
        oli.planned_ship_date,
        lh.posn,
        oli.ppack_grp_code, oli.ppack_qty, oli.price, oli.price_tkt_type,
        0, oli.prod_stat, oli.purchase_order_line_number, oli.reason_code,
        oli.retail_price, oli.shelf_days,
        decode(p_pre_bill_flag, 1, oli.units_pakd, iv.units_pakd) shipped_qty,
        case when oli.is_chase_created_line = 0 
             then coalesce(oli2.tc_order_line_id, oli.tc_order_line_id)
             else coalesce(oli3.tc_order_line_id, oli2.tc_order_line_id)
             end tc_order_line_id, o.tc_company_id,
        o.tc_order_id, oli.purchase_order_number, ic.unit_volume, ic.unit_weight,
        su.size_uom,
        1,
        lh.zone,
        oli.order_qty,
        oli.allocated_qty planned_quantity,
        case when p_pre_bill_flag in (1, 2) then oli.user_canceled_qty
            else (case when o.do_status < 190 or oli.adjusted_order_qty < oli.order_qty
                then 0 else oli.adjusted_order_qty - oli.order_qty end) end,
        oli.substituted_parent_line_id orig_order_line_item_id, sysdate,
        p_user_id, 1, sysdate, oli. ext_purchase_order, oli.purchase_order_number,
        iv.packing_short_qty + (case when o.do_status < 190 then 0 else coalesce(oli.received_qty,0) end) back_ord_qty, iw.temp_zone 
    from order_line_item oli
    join item_cbo ic on ic.item_id = oli.item_id
    join orders o on o.order_id = oli.order_id and o.is_original_order = 1
        and (v_invc_canceled_orders = 1 or o.do_status != 200)
    join item_wms iw on iw.item_id = oli.item_id	
    left join locn_hdr lh on lh.locn_id = oli.pick_locn_id
    left join size_uom su on su.size_uom_id = oli.qty_uom_id_base
    left join order_line_item oli2 on oli2.line_item_id = oli.parent_line_item_id
    left join order_line_item oli3 on oli3.line_item_id = oli2.parent_line_item_id
    left join item_cbo ic2 on ic2.item_id = oli2.item_id
    join
    (
        -- all line level data that has had something invoiced right now
        select t.order_id, t.line_item_id, t.units_pakd, t.packing_short_qty, 0 rn
        from tmp_invc_orig_oli t
        -- collect all orders already >= 190 prior to invoicing
        union all
        select t.order_id, null line_item_id, 0 units_pakd,  0 packing_short_qty, null rn
        from tmp_invc_order_list t
        where t.do_status in (190, 200) or p_pre_bill_flag = 1
        or
            (
                p_pre_bill_flag = 2
                and not exists
                (
                    select 1
                    from tmp_invc_orig_oli t2
                    where t2.order_id = t.order_id
                )
            )
        -- we may need to report shots for orders that may have just been moved to 190
        union all
        select oli3.order_id, oli3.line_item_id, 0 units_pakd, 0 packing_short_qty,1 rn
        from order_line_item oli3
        where oli3.adjusted_order_qty > oli3.order_qty
            and exists
            (
                select 1
                from tmp_invc_orig_oli t
                where t.order_id = oli3.order_id
            )
            and not exists
            (
                select 1
                from tmp_invc_orig_oli t
                where t.order_id = oli3.order_id and t.line_item_id = oli3.line_item_id
            )
    ) iv on iv.order_id = oli.order_id
        and
        (
            iv.line_item_id = oli.line_item_id and (iv.rn = 0 or o.do_status >= 190)
            or (iv.line_item_id is null and (oli.adjusted_order_qty > oli.order_qty or p_pre_bill_flag in (1, 2)))
        )
    log errors (to_char(sysdate));
    wm_cs_log('Outpt_order_line_item insert '  || sql%rowcount);

    insert into outpt_orders
    (
        acct_rcvbl_acct_nbr, acct_rcvbl_code, addr_code,
        batch_ctrl_nbr, billing_method, bill_acct_nbr, bill_facility_alias_id,
        bill_facility_id, bill_of_lading_number, bill_to_address_1, bill_to_address_2,
        bill_to_address_3, bill_to_city, bill_to_contact_name,
        bill_to_country_code, bill_to_county, bill_to_email, bill_to_facility_name,
        bill_to_fax_number, bill_to_name, bill_to_phone_number, bill_to_postal_code,
        bill_to_state_prov, bill_to_title, bol_break_attr,  business_partner,
        created_dttm, created_source, created_source_type, currency,
        d_address_1, d_address_2, d_address_3, d_city, d_contact, d_country_code,
        d_county, d_dock_door_id, d_dock_id, d_email, d_facility_alias_id, d_facility_id,
        d_facility_name, d_fax_number, d_name, d_phone_number, d_postal_code,
        d_state_prov, delivery_start_dttm, distribution_ship_via, do_type,
        dsg_ship_via, original_ship_via, ext_purchase_order,
        freight_class, freight_forwarder_acct_nbr, global_locn_nbr,
        incoterm_facility_alias_id, incoterm_facility_id, incoterm_id, incoterm_loc_ava_dttm,
        incoterm_loc_ava_time_zone_id,  insur_chrg, invc_batch_nbr,
        is_back_ordered,
        major_minor_order, major_order_ctrl_nbr, major_order_grp_attr,
        merchandizing_department,
        monetary_value, movement_option, mv_currency, non_machineable, o_address_1,
        o_address_2, o_address_3, o_city, o_contact, o_country_code, o_county,
        o_dock_door_id, o_dock_id, o_email, o_facility_alias_id, o_facility_id,
        o_facility_name,  o_fax_number, o_phone_number, o_postal_code, o_state_prov,
        order_weight, outpt_orders_id,
        original_assigned_ship_via, origin_ship_thru_facility_id, origin_ship_thru_fac_alias_id,
        pickup_start_dttm, planned_ship_date,
        pre_pack_flag, priority, proc_stat_code,
        rte_id,
        shipment_id, ship_date,
        ship_group_id, ship_group_sequence, shpng_chrg, order_status, do_status, store_nbr,
        tc_company_id, tc_order_id, tc_shipment_id, total_nbr_of_lpn,
        total_nbr_of_plt, total_nbr_of_units, version_nbr, line_haul_ship_via,
        plan_d_facility_alias_id, plan_d_facility_id, plan_leg_d_alias_id, plan_leg_d_id,
        plan_leg_o_alias_id, plan_leg_o_id, plan_o_facility_alias_id, plan_o_facility_id,
        pre_bill_status, prtl_ship_conf_flag, prtl_ship_conf_status,
        tax_id, last_updated_dttm, last_updated_source,
        last_updated_source_type, proc_dttm, sched_delivery_dttm, ref_shipment_nbr,
        ref_stop_seq, purchase_order_nbr, distro_number,
        order_date_dttm, pickup_end_dttm, handling_charges, dc_ctr_nbr, manifest_id,
        spl_instr_code_1, spl_instr_code_2, spl_instr_code_3, spl_instr_code_4,
        spl_instr_code_5, spl_instr_code_6, spl_instr_code_7, spl_instr_code_8,
        spl_instr_code_9, spl_instr_code_10, po_type_attr, allow_pre_billing, mark_for, bol_type
    )
    with order_weights as
    (
        select l.order_id,
            count(*) num_lpns, 
            sum(l.insur_charge) insur_charge, sum(l.total_lpn_qty) total_lpn_qty,
            sum(coalesce(l.weight, l.estimated_weight)) order_weight,
            sum(case when (cc.carrier_type_id < 10 or cc.carrier_type_id = 23) then l.actual_charge else 0 end)
            + sum(case when (cc.carrier_type_id < 10 or cc.carrier_type_id = 23) then 0
                else coalesce(l.weight, l.estimated_weight, 0) * s.total_cost end)
              / coalesce(nullif(first_value(sum(coalesce(l.weight, l.estimated_weight)))
                  over(partition by s.shipment_id order by l.order_id nulls first), 0), 1) shpng_chrg
        from tmp_invc_lpn_list t
        join lpn l on l.lpn_id = t.lpn_id
        left join shipment s on s.shipment_id = l.shipment_id
        left join carrier_code cc on cc.carrier_id = s.assigned_carrier_id
        group by rollup(s.shipment_id, l.order_id)
        having grouping(s.shipment_id) = 0
    ),
    orders_with_shpd_qty as
    (
        select t.order_id, t.parent_order_id, sum(t.units_pakd) units_pakd,
            decode(t.parent_order_id, null, null,
                row_number() over(partition by t.parent_order_id order by t.order_id)) rn
        from tmp_invc_orig_oli t
        group by t.order_id, t.parent_order_id
        union all
        select misc_orders.order_id, null parent_order_id, null units_pakd, null rn
        from tmp_invc_order_list misc_orders
        where misc_orders.do_status < 190
            and not exists
            (
                select 1
                from order_line_item oli
                where oli.order_id = misc_orders.order_id
            )
    )
    select o.acct_rcvbl_acct_nbr, o.acct_rcvbl_code, o.addr_code,
        p_invc_batch_nbr, o.billing_method, o.bill_acct_nbr, o.bill_facility_alias_id,
        o.bill_facility_id, o.bill_of_lading_number, o.bill_to_address_1, o.bill_to_address_2,
        o.bill_to_address_3, o.bill_to_city, o.bill_to_contact_name,
        o.bill_to_country_code, o.bill_to_county, o.bill_to_email, o.bill_to_facility_name,
        o.bill_to_fax_number, o.bill_to_name, o.bill_to_phone_number, o.bill_to_postal_code,
        o.bill_to_state_prov, o.bill_to_title, o.bol_break_attr,  o.business_partner_id,
        sysdate, p_user_id, 1, o.cod_currency_code, o.d_address_1, o.d_address_2,
        o.d_address_3, o.d_city, o.d_contact, o.d_country_code, o.d_county, o.d_dock_door_id,
        o.d_dock_id, o.d_email, o.d_facility_alias_id, o.d_facility_id, o.d_facility_name,
        o.d_fax_number, o.d_name, o.d_phone_number, o.d_postal_code, o.d_state_prov,
        o.delivery_start_dttm, o.distribution_ship_via, o.do_type, s.ship_via_id,
        o.dsg_ship_via, o.ext_purchase_order, o.freight_class,
        o.freight_forwarder_acct_nbr, o.global_locn_nbr, o.incoterm_facility_alias_id,
        o.incoterm_facility_id, o.incoterm_id,
        o.incoterm_loc_ava_dttm, o.incoterm_loc_ava_time_zone_id,
        iv.insur_charge,
        p_invc_batch_nbr, o.is_back_ordered,
        o.major_minor_order, coalesce(o.major_order_ctrl_nbr, to_char(o.parent_order_id)),
        o.major_order_grp_attr,
        md.merchandizing_department, o.monetary_value, o.movement_option,
        o.mv_currency_code, o.non_machineable, o.o_address_1, o.o_address_2, o.o_address_3, o.o_city,
        o.o_contact, o.o_country_code, o.o_county, o.o_dock_door_id, o.o_dock_id, o.o_email,
        o.o_facility_alias_id, o.o_facility_id, o.o_facility_name, o.o_fax_number,
        o.o_phone_number, o.o_postal_code, o.o_state_prov,
        iv.order_weight, outpt_orders_id_seq.nextval,
        o.original_assigned_ship_via,
        o.origin_ship_thru_facility_id, o.origin_ship_thru_fac_alias_id,
        o.pickup_start_dttm, o.plan_due_dttm, o.pre_pack_flag, o.priority,
        0, o.dsg_static_route_id,
        o.shipment_id, o.actual_shipped_dttm, o.ship_group_id,
        o.ship_group_sequence, iv.shpng_chrg, o.order_status, o.do_status, o.store_nbr,
        o.tc_company_id, o.tc_order_id, o.tc_shipment_id,
        iv.num_lpns total_nbr_of_lpn,
        o.total_nbr_of_plt, iv.units_pakd total_nbr_of_units,
        1, o. line_haul_ship_via, o.plan_d_facility_alias_id, o.plan_d_facility_id,
        o.plan_leg_d_alias_id, o.plan_d_facility_id, o.plan_leg_o_alias_id, o.plan_o_facility_id,
        o.plan_o_facility_alias_id, o.plan_o_facility_id,
        case when o.pre_bill_status = '1' and p_pre_bill_flag = 0 then 2 else to_number(coalesce(o.pre_bill_status, '0')) end pre_bill_status,
        o.prtl_ship_conf_flag, coalesce(o.prtl_ship_conf_status, 0),
        o.tax_id, sysdate, p_user_id, 1, sysdate,
        o.sched_delivery_dttm, o.ref_shipment_nbr, o.ref_stop_seq, o.purchase_order_number,
        o.distro_number, o.order_date_dttm, o.pickup_end_dttm, iv.shpng_chrg,
        o.dc_ctr_nbr, o.manifest_nbr, o.spl_instr_code_1, o.spl_instr_code_2,
        o.spl_instr_code_3, o.spl_instr_code_4, o.spl_instr_code_5,
        o.spl_instr_code_6, o.spl_instr_code_7, o.spl_instr_code_8,
        o.spl_instr_code_9, o.spl_instr_code_10, o.po_type_attr, o.allow_pre_billing, o.mark_for, o.bol_type
    from orders o
    join
    (
        select giv_o.order_id, coalesce(sum(case when giv_o.rn > 1 then 0 else giv_ow.insur_charge end), 0) insur_charge,
            coalesce(sum(case when giv_o.rn > 1 then 0 else giv_ow.num_lpns end), 0) num_lpns,
            coalesce(sum(case when giv_o.rn > 1 then 0 else giv_ow.order_weight end), 0) order_weight,
            coalesce(sum(case when giv_o.rn > 1 then 0 else giv_ow.shpng_chrg end), 0) shpng_chrg,
            coalesce(min(giv_o.units_pakd), sum(giv_ow.total_lpn_qty), 0) units_pakd
        from orders_with_shpd_qty giv_o
        join order_weights giv_ow on giv_ow.order_id = giv_o.order_id
        where p_pre_bill_flag = 0
        group by giv_o.order_id
        union all
        select giv_o.order_id, coalesce(sum(case when giv_o.rn > 1 then 0 else giv_ow.insur_charge end), 0) insur_charge,
            coalesce(sum(case when giv_o.rn > 1 then 0 else giv_ow.num_lpns end), 0) num_lpns,
            coalesce(sum(case when giv_o.rn > 1 then 0 else giv_ow.order_weight end), 0) order_weight,
            coalesce(sum(case when giv_o.rn > 1 then 0 else giv_ow.shpng_chrg end), 0) shpng_chrg,
            coalesce(min(giv_o.units_pakd), sum(giv_ow.total_lpn_qty), 0) units_pakd
        from orders_with_shpd_qty giv_o
        join order_weights giv_ow on giv_ow.order_id = giv_o.parent_order_id
        where p_pre_bill_flag = 0
        group by giv_o.order_id
        union all
        select shorted_orders.order_id, 0 insur_charge, 0 num_lpns, 0 order_weight,
            0 shpng_chrg, 0 units_pakd
        from tmp_invc_order_list shorted_orders
        where shorted_orders.do_status >= 190
        union all
        select pb_orders.order_id, 0 insur_charge, 0 num_lpns, 0 order_weight,
            0 shpng_chrg, sum(oli.units_pakd) units_pakd
        from tmp_invc_order_list pb_orders
        join order_line_item oli on oli.order_id = pb_orders.order_id
        where p_pre_bill_flag in (1,2)
        group by pb_orders.order_id
    ) iv on iv.order_id = o.order_id
    left join merchandizing_department md on md.merchandizing_department_id = o.merchandizing_department_id
    left join ship_via s on o.dsg_ship_via = s.ship_via and o.tc_company_id = s.tc_company_id
    where o.is_original_order = 1
        and (v_invc_canceled_orders = 1 or o.do_status != 200)
    log errors (to_char(sysdate));
    wm_cs_log('Outpt_orders insert ' || sql%rowcount);

    if (p_pre_bill_flag = 0)
    then
        merge into orders o
        using
        (
            select oo.tc_order_id, oo.tc_company_id, oo.shpng_chrg, oo.pre_bill_status
            from outpt_orders oo
            where oo.invc_batch_nbr = p_invc_batch_nbr and oo.do_status = 190
        ) iv on (iv.tc_order_id = o.tc_order_id and iv.tc_company_id = o.tc_company_id)
        when matched then
        update set o.shpng_chrg = iv.shpng_chrg, o.pre_bill_status = iv.pre_bill_status, 
            o.last_updated_dttm = sysdate, o.last_updated_source = p_user_id
        log errors (to_char(sysdate));
        wm_cs_log('Shipping charge update ' || sql%rowcount);
    end if;
end;
/
show errors;

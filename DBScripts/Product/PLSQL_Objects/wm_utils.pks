create or replace package wm_utils
is  
    subtype t_app_context_data is varchar2(64);
    type    ta_bu_cnfg         is table of varchar2(4000) index by varchar2(32000);
    
    function wm_get_user_bu
    (
        p_user_id in ucl_user.user_name%type
    )
    return number;

    procedure set_context_info
    (
        p_module_name in varchar2,
        p_action_name in varchar2,
        p_client_id   in varchar2 default null
    );

    procedure get_context_info
    (
        p_module_name out varchar2,
        p_action_name out varchar2
    );
end;
/

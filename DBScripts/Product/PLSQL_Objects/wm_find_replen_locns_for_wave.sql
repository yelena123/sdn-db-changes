create or replace procedure wm_find_replen_locns_for_wave
(
    p_whse        in facility.whse%type,
    p_locn_class  in locn_hdr.locn_class%type,
    p_wave_nbr    in ship_wave_parm.pick_wave_nbr%type,
    p_cancel_qty  in pkt_dtl.cancel_qty%type,
    p_pdz_csv     in varchar2,
    p_result_set  out types.cursortype
)
as
begin
    open p_result_set for
    with waved_items as
    (
        select distinct pd.item_id
        from pkt_dtl pd
        where pd.wave_nbr = p_wave_nbr and pd.wave_stat_code in (20, 30) 
            and pd.cancel_qty > p_cancel_qty
    )
    select pld.locn_id, pld.locn_seq_nbr, lh.pick_detrm_zone, pld.item_id, pld.max_invn_qty,
        pld.ltst_pick_assign_date_time
    from waved_items
    join pick_locn_dtl pld on pld.item_id = waved_items.item_id and pld.trig_repl_for_sku = 'Y'
        and pld.ltst_sku_assign = 'Y' and pld.pikng_lock_code is null and pld.max_invn_qty > 0
    join locn_hdr lh on lh.locn_id = pld.locn_id and lh.whse = p_whse 
        and lh.locn_class = p_locn_class
        and lh.pick_detrm_zone in
        (
            select t.column_value
            from table(cl_parse_endpoint_name_csv(p_pdz_csv)) t
        )
    join wm_inventory wi on wi.location_dtl_id = pld.pick_locn_dtl_id
    group by pld.locn_id, pld.locn_seq_nbr, lh.pick_detrm_zone, pld.item_id, pld.max_invn_qty,
        pld.ltst_pick_assign_date_time
    having sum(wi.on_hand_qty) + sum(wi.to_be_filled_qty) - sum(wi.wm_allocated_qty) <= 0;
end;
/
show errors;

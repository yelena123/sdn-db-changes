create or replace procedure manh_get_pix_ref_code_col_list
(
    p_tran_type                in pix_tran.tran_type%type,
    p_tran_code                in pix_tran.tran_code%type,
    p_actn_code                in pix_tran.actn_code%type,
    p_cd_master_id             in cd_master.cd_master_id%type,
    p_ref_codes_ins_merge_list out varchar2,
    p_ref_codes_select_list    out varchar2,
    p_is_merge_stmt            in number default 0
)
as
    v_ref_code_seq_str varchar2(2);
begin
    p_ref_codes_ins_merge_list := null;
    p_ref_codes_select_list := null;
    -- build the insert-into/merge-update and select-from portions of the ref code list
    for rec in
    (
        select iv.tbl_name, iv.colm_name, iv.ref_code_id, iv.pix_tran_code_seq,
            iv.rank
        from
        (
            select rcl.tbl_name, rcl.colm_name, ptcd.ref_code_id,
                ptcd.pix_tran_code_seq, dense_rank() over(order by
                ptc.cd_master_id nulls last, ptc.actn_code desc nulls last) rank
            from rule_colm_list rcl
            join pix_ref_code_master prcm on prcm.rule_colm_id = rcl.rule_colm_id
            join pix_tran_code_dtl ptcd on ptcd.ref_code_id = prcm.ref_code_id
            join pix_tran_code ptc on ptc.pix_tran_code_id = ptcd.pix_tran_code_id
            where ptc.tran_type = p_tran_type and ptc.tran_code = p_tran_code
                and (coalesce(ptc.actn_code, ' ') = coalesce(p_actn_code, ' ')
                    or ptc.actn_code = '*' or p_actn_code = '*')
                and coalesce(ptc.cd_master_id, p_cd_master_id, -1) = coalesce(p_cd_master_id, -1)
                and rcl.tbl_name not in ('USER_ENTERED', 'TXN')
        ) iv
        where iv.rank < 2
    )
    loop
        v_ref_code_seq_str := to_char(rec.pix_tran_code_seq);
        
        if (p_is_merge_stmt = 1)
        then
            p_ref_codes_select_list := p_ref_codes_select_list
                || ' , ''' || rec.ref_code_id || ''''
                || ' ref_code_id_' || v_ref_code_seq_str
                || ', ' || rec.tbl_name || '.' || rec.colm_name
                || ' ref_field_' || v_ref_code_seq_str;
            
            p_ref_codes_ins_merge_list := p_ref_codes_ins_merge_list
                || ',  ref_code_id_' || v_ref_code_seq_str
                || ' = iv.ref_code_id_' || v_ref_code_seq_str
                || ', ref_field_' || v_ref_code_seq_str
                || ' = iv.ref_field_' || v_ref_code_seq_str;
        else
            p_ref_codes_ins_merge_list := p_ref_codes_ins_merge_list
                || ' , ref_code_id_' || v_ref_code_seq_str
                || ', ref_field_' || v_ref_code_seq_str;
  
            p_ref_codes_select_list := p_ref_codes_select_list
                || ' , ''' || rec.ref_code_id || ''''
                || ', ' || rec.tbl_name || '.' || rec.colm_name;
        end if;
    end loop;
end;
/
show errors;

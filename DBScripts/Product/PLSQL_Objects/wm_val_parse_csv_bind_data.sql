create or replace function wm_val_parse_csv_bind_data
(
    p_bind_val_csv  in varchar2
)
return ta_bind_obj pipelined
as
    v_delim         varchar2(1) := ',';
begin
    for token_rec in
    (
        select substr(p_bind_val_csv, iv.start_pos, iv.delim_pos - iv.start_pos) bind_var,
            substr(p_bind_val_csv, iv.delim_pos + 1, 
                decode(iv.end_pos, 0, length(p_bind_val_csv) + 1, iv.end_pos) - iv.delim_pos - 1) bind_val
        from
        (
            select decode(level, 1, 1, instr(p_bind_val_csv, v_delim, 1, level-1) + 1) start_pos, 
                instr(p_bind_val_csv, v_delim, 1, level) delim_pos, 
                instr(p_bind_val_csv, v_delim, 1, level+1) end_pos, level lvl
            from dual
            where mod(level, 2) > 0
            connect by instr(p_bind_val_csv, v_delim, 1, level) > 0
        ) iv
        where iv.delim_pos > iv.start_pos
        order by iv.lvl
    )
    loop
        pipe row(t_bind_obj(token_rec.bind_var, token_rec.bind_val));
    end loop;

    return;
end;
/
show errors;

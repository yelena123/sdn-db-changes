create or replace procedure manh_wave_complete_selection
(
    p_facility_id       in facility.facility_id%type,
    p_user_id           in ucl_user.user_name%type,
    p_whse              in facility.whse%type,
    p_preview_wave_flag in number,
    p_ship_wave_nbr     in ship_wave_parm.ship_wave_nbr%type,
    p_pick_wave_nbr     in ship_wave_parm.pick_wave_nbr%type,
    p_perf_rte          in ship_wave_parm.perf_rte%type,
    p_sku_cnstr         in wave_parm.sku_cnstr%type,
    p_ship_via          in wave_parm.ship_via%type,
    p_user_bu           in company.company_id%type,
    p_ship_date_time    in wave_parm.ship_date_time%type,
    p_debug_level       in number,
    p_repl_wave_flag    in wave_parm.repl_wave%type
)
as
    v_is_pix_configured     number(1) := 0;
    v_delv_date_time wave_parm.ship_date_time%type := p_ship_date_time + 10;
    v_lang_id               language.language_suffix%type;
    v_success_msg           msg_log.msg%type;
    v_failure_msg           msg_log.msg%type;
    va_order_list           t_num;
begin
    insert into tmp_wave_old_order_status
    (
        order_id, parent_order_id, do_status 
    )
    select o.order_id, o.parent_order_id, o.do_status
    from orders o
    where o.o_facility_id = p_facility_id
        and exists
        (
            select 1
            from order_line_item oli
            where oli.order_id = o.order_id and oli.wave_nbr = p_pick_wave_nbr
        );
    wm_cs_log('Orders collected for completion ' || sql%rowcount);

    if (p_preview_wave_flag = 1)
    then
        update orders o
        set o.do_status = 115, o.last_updated_source = p_user_id, o.last_updated_dttm = sysdate
        where o.o_facility_id = p_facility_id
            and exists
            (
                select 1
                from order_line_item oli
                where oli.ship_wave_nbr = p_ship_wave_nbr
                    and oli.do_dtl_status = 115 and oli.order_id = o.order_id
            );
        wm_cs_log('Updated orders to PW status ' || sql%rowcount);
    else
        -- if a vas pending line exists and we've waved its order, then none of
        -- the non-vas lines should have been waved
        -- FTA will hold locks on one row at a time, so we block until its
        -- update is complete, but this is a problem if another process performs
        -- a bulk update on more than one of the rows updated below
        update orders o
        set (o.last_updated_source, o.last_updated_dttm, o.rte_wave_nbr,
            o.wm_order_status, o.do_status, o.is_cancelled, o.dsg_ship_via,
            o.dsg_carrier_id, o.dsg_mot_id, o.dsg_service_level_id,
            o.prtl_ship_conf_status,o.pickup_start_dttm, o.pickup_end_dttm,
            o.delivery_start_dttm,o.delivery_end_dttm) =
        (
            select p_user_id last_updated_source, sysdate last_updated_dttm,
                case when coalesce(min(oli.do_dtl_status), 200) < 200
                    and p_perf_rte = '1' and o.order_status = 5
                    then p_ship_wave_nbr else o.rte_wave_nbr end rte_wave_nbr,
                case when coalesce(min(oli.do_dtl_status), 200) < 200
                    and coalesce(o.wm_order_status, 0) = 0 and min(nullif(oli.vas_process_type,'0')) = '1'
                    then 10 else o.wm_order_status end wm_order_status,
                case
                    when min(oli.do_dtl_status) in (110, 112)
                        and min(oli.do_dtl_status) != max(oli.do_dtl_status)
                        then 120
                    when min(oli.do_dtl_status) = 130
                        and max(oli.do_dtl_status) > 130 then 140
                    else coalesce(min(oli.do_dtl_status), 200)
                end do_status,
                case when coalesce(min(oli.do_dtl_status), 200) = 200 then 1
                    else 0 end is_cancelled,
                case when coalesce(min(oli.do_dtl_status), 200) < 200
                    and p_ship_via is not null then p_ship_via
                    else o.dsg_ship_via end dsg_ship_via,
                case when coalesce(min(oli.do_dtl_status), 200) < 200
                    and p_ship_via is not null then null
                    else o.dsg_carrier_id end dsg_carrier_id,
                case when coalesce(min(oli.do_dtl_status), 200) < 200
                    and p_ship_via is not null then null
                    else o.dsg_mot_id end dsg_mot_id,
                case when coalesce(min(oli.do_dtl_status), 200) < 200
                    and p_ship_via is not null then null
                    else o.dsg_service_level_id end dsg_service_level_id,	
                case when coalesce(min(oli.do_dtl_status), 200) = 200 then 3
                    else o.prtl_ship_conf_status end prtl_ship_conf_status,
                case when coalesce(min(oli.do_dtl_status), 200) < 200
                    and p_ship_date_time is not null then p_ship_date_time
                    else o.pickup_start_dttm end pickup_start_dttm,
                case when coalesce(min(oli.do_dtl_status), 200) < 200
                    and p_ship_date_time is not null then p_ship_date_time
                    else o.pickup_end_dttm end pickup_end_dttm,
                case when coalesce(min(oli.do_dtl_status), 200) < 200
                    and p_ship_date_time is not null then p_ship_date_time
                    else o.delivery_start_dttm end delivery_start_dttm,
                case when coalesce(min(oli.do_dtl_status), 200) < 200
                    and p_ship_date_time is not null then v_delv_date_time
                    else o.delivery_end_dttm end delivery_end_dttm
            from order_line_item oli
            where oli.order_id = o.order_id and oli.do_dtl_status < 200
        )
        where exists
        (
            select 1
            from tmp_wave_old_order_status t
            where t.order_id = o.order_id
        )
        returning o.order_id
        bulk collect into va_order_list;
        wm_cs_log('Updated orig order data ' || sql%rowcount, p_sql_log_level => 1);

        -- determine if packed orders can be moved to beyond in-packed, based on lpn
        merge into orders o
        using
        (
            select t.order_id, min(l.lpn_facility_status) min_lpn_status,
                max
                (
                    case
                    when l.lpn_facility_status < 50
                        and
                        (
                            coalesce(l.stage_indicator, 0) != 10
                            or coalesce(lh.locn_class, ' ') not in ('P', 'S', 'O')
                        ) then 1
                    else 0
                    end
                ) cannot_stage
            from tmp_wave_old_order_status t
            join orders o on o.order_id = t.order_id and o.do_status = 150
            join lpn l on l.order_id in (t.order_id, t.parent_order_id)
            left join locn_hdr lh on lh.locn_id = l.curr_sub_locn_id
            group by t.order_id
        ) iv on (iv.order_id = o.order_id)
        when matched then
        update set o.do_status =
        (
            case
            when iv.min_lpn_status in (20, 30) and iv.cannot_stage = 0 then 165
            else decode(iv.min_lpn_status, 20, 150, 30, 160, 50, 180, 170)
            end
        );
        wm_cs_log('DO status revised for orders ' || sql%rowcount);
        if (p_sku_cnstr = '2') 
        then
            -- if there is a shortage during proc then we need to recalculate the aggregate status
            -- which are tied to original order and those aggregate are in 120 status only
            merge into orders agg
            using
            (
                select orig.parent_order_id, min(orig.do_status) min_do_status
                from orders orig
                join tmp_wave_old_order_status t on t.parent_order_id = orig.parent_order_id
                    and t.parent_order_id is not null
                where orig.do_status <= 190
                group by orig.parent_order_id
            ) iv on (iv.parent_order_id = agg.order_id)
            when matched then
            update set agg.do_status = (
                case 
                    when agg.do_status > 130 and iv.min_do_status = 130 then 140 
                    when agg.do_status > 130 and iv.min_do_status = 150 
                        and exists
                        (
                            select 1 
                            from lpn l 
                            where l.order_id = agg.order_id and l.lpn_facility_status < 20
                        ) then 140 
			              else iv.min_do_status end);
        else
            --- Only for chase wave and only in scenario where aggregate order are >= 150 and < 190
            merge into orders agg
            using
            (
                select orig.parent_order_id, min(orig.do_status) min_do_status
                from orders orig
                join tmp_wave_old_order_status t on t.parent_order_id = orig.parent_order_id
                    and t.parent_order_id is not null
                where orig.do_status between 150 and 190
                group by orig.parent_order_id
            ) iv on (iv.parent_order_id = agg.order_id)
            when matched then
            update set agg.do_status = (
                case 
                    when agg.do_status > 130 and iv.min_do_status = 130 then 140 
                    when agg.do_status > 130 and iv.min_do_status = 150 
                        and exists
                        (
                            select 1 
                            from lpn l 
                            where l.order_id = agg.order_id and l.lpn_facility_status < 20
                        ) then 140 
			              else iv.min_do_status end);
        end if;
        wm_cs_log('Agg order status update ' || sql%rowcount);
    
        wm_unplan_pakd_orders_in_list(p_user_id, va_order_list);
    end if;

    -- the do status informational pix can also be turned off in S501
    select case when exists
    (
        select 1
        from sys_code sc
        where sc.rec_type = 'S' and sc.code_type = '501' and sc.code_id >= '115' 
            and substr(sc.misc_flags, 1, 1) = '1'
    ) then 1 else 0 end
    into v_is_pix_configured
    from dual;
    
    if (v_is_pix_configured = 1 and p_repl_wave_flag != '1')
    then
        manh_wave_do_status_change_pix(p_facility_id, p_user_id, p_whse, p_pick_wave_nbr);
    end if;

    if (p_preview_wave_flag = 1)
    then
        return;
        -- no more pre wave dml below
    end if;

    -- notes for split lines only
    insert into order_note
    (
        order_id, line_item_id, note_type, note, internal_only,
        is_vendor_comment, note_code, note_seq
    )
    select oli.order_id, oli.line_item_id, ot.note_type, ot.note,
        ot.internal_only, ot.is_vendor_comment, ot.note_code,
        row_number() over(partition by oli.order_id, oli.line_item_id
        order by ot.note_seq) note_seq
    from order_line_item oli
    join tmp_wave_old_order_status t on t.order_id = oli.order_id
    join order_note ot on ot.order_id = oli.order_id and ot.line_item_id = oli.parent_line_item_id
    where oli.wave_nbr = p_pick_wave_nbr and oli.do_dtl_status = 130;
    wm_cs_log('Created notes for split lines ' || sql%rowcount);

    select la.language_suffix
    into v_lang_id
    from ucl_user uu
    join locale l on l.locale_id = uu.locale_id
    join language la on la.language_id = l.language_id
    where uu.user_name = p_user_id;

    v_success_msg := get_msg_info('INVMGMT', '9128', v_lang_id);
    v_failure_msg := get_msg_info('INVMGMT', '9127', v_lang_id);
    insert into msg_log
    (
        msg_log_id, module, pgm_id, log_date_time, create_date_time, msg_id,
        mod_date_time, msg, user_id, whse, cd_master_id, ref_code_1, ref_value_1,
        ref_code_2, ref_value_2, ref_code_4, ref_value_4, ref_value_3
    )
    select msg_log_id_seq.nextval, 'INVMGMT', 'Selection SP', sysdate, sysdate,
        case when sc.code_id = '45' then '9128' else '9127' end msg_id, sysdate,
        case when sc.code_id = '45' then replace(replace(v_success_msg, '{0}', oli.tc_order_line_id), '{1}', o.tc_order_id)
            else replace(replace(replace(v_failure_msg, '{0}', oli.tc_order_line_id), '{1}', o.tc_order_id), '{2}', sc.code_desc) end msg,
        p_user_id, p_whse, p_user_bu, '19', p_ship_wave_nbr, '06', p_pick_wave_nbr, '4', 'D', '2'

    from order_line_item oli
    join orders o on o.order_id = oli.order_id
    join sys_code sc on sc.code_id = oli.reason_code
    where oli.ship_wave_nbr = p_ship_wave_nbr and o.o_facility_id = p_facility_id
        and sc.rec_type ='S' and sc.code_type = '809'
        and (oli.reason_code != '45' or p_debug_level = 3);
    wm_cs_log('SA failure/high level logs ' || sql%rowcount);
end;
/
show errors;

create or replace force view val_job_sql_vw
(
    ident, sql_id, val_job_dtl_id, job_name, disabled
)
as
select vs.ident, vs.sql_id, vjd.val_job_dtl_id, vjh.job_name,
    case when vjd.is_locked = 1 or vs.is_locked = 1 then 1 else 0 end disabled
from val_sql vs
join val_job_dtl vjd on vjd.sql_id = vs.sql_id
join val_job_hdr vjh on vjh.val_job_hdr_id = vjd.val_job_hdr_id
/

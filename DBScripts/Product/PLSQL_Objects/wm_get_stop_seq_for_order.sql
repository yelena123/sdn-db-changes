create or replace procedure wm_get_stop_seq_for_order
(
    p_facility_id        in   facility.facility_id%type,
    p_company_id         in   company.company_id%type,
    p_order_id           in   orders.order_id%type,
    p_shpmt_id           in   shipment.shipment_id%type,
    p_stop_seq           out  stop.stop_seq%type
)
as
begin
    select s.stop_seq 
    into p_stop_seq
    from stop s
    join
    (
        select coalesce(o.plan_d_facility_id, o.zone_skip_hub_location_id, o.d_facility_id, -1) fac_id, 
            coalesce(lower(o.d_address_1), '?') d_address_1, 
            coalesce(lower(o.d_address_2), '?') d_address_2, 
            coalesce(lower(o.d_address_3), '?') d_address_3, 
            coalesce(lower(o.d_city), '?') d_city, 
            coalesce(lower(o.d_state_prov), '?') d_state_prov, 
            coalesce(lower(o.d_county), '?') d_county, 
            coalesce(lower(o.d_postal_code), '?') d_postal_code, 
            coalesce(lower(o.d_country_code), '?') d_country_code
        from orders o
        where o.order_id = p_order_id
    ) o on o.fac_id = s.facility_id
        or (s.facility_id is null and o.d_address_1 = coalesce(lower(s.address_1), '?')
            and o.d_address_2 = coalesce(lower(s.address_2), '?')
            and o.d_address_3 = coalesce(lower(s.address_3), '?')
            and o.d_city = coalesce(lower(s.city), '?')
            and o.d_state_prov = coalesce(lower(s.state_prov), '?')
            and o.d_postal_code = coalesce(lower(s.postal_code), '?')
            and o.d_county = coalesce(lower(s.county), '?')
            and o.d_country_code = coalesce(lower(s.country_code), '?'))
    where s.shipment_id = p_shpmt_id and s.tc_company_id = p_company_id and s.stop_seq > 1;
    exception
        when no_data_found then
            p_stop_seq := 0;
end;
/
show errors;
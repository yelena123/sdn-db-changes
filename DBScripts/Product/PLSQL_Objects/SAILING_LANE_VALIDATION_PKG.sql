CREATE OR REPLACE
PACKAGE SAILING_LANE_VALIDATION_PKG
AS
   TYPE ref_curtype IS REF CURSOR;

   PROCEDURE ADJUST_EFF_EXP_DT (
      vTCCompanyId           IN company.COMPANY_ID%TYPE,
      vLaneId                IN comb_lane.LANE_ID%TYPE,
      vLaneDtlSeq            IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vEffectiveDT           IN comb_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN comb_lane_dtl.EXPIRATION_DT%TYPE,
      vCarrierCode           IN comb_lane_dtl.CARRIER_ID%TYPE,
      vMot                   IN comb_lane_dtl.MOT_ID%TYPE,
      vServiceLevel          IN comb_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN comb_LANE_DTL.SCNDR_CARRIER_ID%TYPE);

   PROCEDURE ADJUST_EFF_EXP_DT (
      vTCCompanyId           IN company.COMPANY_ID%TYPE,
      vLaneId                IN comb_lane.LANE_ID%TYPE,
      vLaneDtlSeq            IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vLaneDtlSeqUpd         IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vEffectiveDT           IN comb_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN comb_lane_dtl.EXPIRATION_DT%TYPE,
      vCarrierCode           IN comb_lane_dtl.CARRIER_ID%TYPE,
      vMot                   IN comb_lane_dtl.MOT_ID%TYPE,
      vServiceLevel          IN comb_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN comb_LANE_DTL.SCNDR_CARRIER_ID%TYPE);

   /* n/a  22/06/2004 RAM  added  Procedure COPY CARRIERS for updation of relavent carriers with the lane details changes when modified  SR1 , 4R1 change, build92,  TT31182 */

   PROCEDURE COPY_CARRIERS (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                            vLaneId        IN comb_lane.LANE_ID%TYPE,
                            vOldLaneId     IN comb_lane.LANE_ID%TYPE);

   FUNCTION MERGE_DRAFT (vTCCompanyId     IN company.COMPANY_ID%TYPE,
                         vLaneId          IN comb_lane.LANE_ID%TYPE,
                         vLaneDtlSeq      IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
                         vLaneDtlSeqUpd   IN comb_lane_dtl.LANE_DTL_SEQ%TYPE)
      RETURN INT;

   PROCEDURE CREATE_NEW_ROW (
      vTCCompanyId    IN company.COMPANY_ID%TYPE,
      vLaneId         IN comb_lane.LANE_ID%TYPE,
      vLaneDtlSeq     IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vEffectiveDT    IN comb_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT   IN comb_lane_dtl.EXPIRATION_DT%TYPE);

   PROCEDURE REPLICATE_ROW (
      vTCCompanyId     IN company.COMPANY_ID%TYPE,
      vLaneId          IN comb_lane.LANE_ID%TYPE,
      vOverlappedSeq   IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vLaneDtlSeq      IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vEffectiveDT     IN comb_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT    IN comb_lane_dtl.EXPIRATION_DT%TYPE,
      vIsRating        IN comb_lane_dtl.IS_RATING%TYPE,
      vIsRouting       IN comb_lane_dtl.IS_ROUTING%TYPE,
      vIsSailing       IN comb_lane_dtl.IS_SAILING%TYPE);

   PROCEDURE DELETE_CHILD_ROW (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vLaneId        IN comb_lane.LANE_ID%TYPE,
      vLaneDtlSeq    IN comb_lane_dtl.LANE_DTL_SEQ%TYPE);

   PROCEDURE COPY_CHILD_ROW (
      vTCCompanyId     IN company.COMPANY_ID%TYPE,
      vLaneId          IN comb_lane.LANE_ID%TYPE,
      vOverlappedSeq   IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vLaneDtlSeq      IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vLaneDtlSeqNew   IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vIsRating        IN comb_lane_dtl.IS_RATING%TYPE,
      vIsRouting       IN comb_lane_dtl.IS_ROUTING%TYPE,
      vIsSailing       IN comb_lane_dtl.IS_SAILING%TYPE);

   PROCEDURE REPLICATE_LANE_DETAIL (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vLaneId        IN comb_lane.LANE_ID%TYPE,
      vLaneDtlSeq    IN comb_lane_dtl.LANE_DTL_SEQ%TYPE);

   PROCEDURE MERGE_DRAFT (
      vTCCompanyId     IN     company.COMPANY_ID%TYPE,
      vLaneId          IN     comb_lane.LANE_ID%TYPE,
      vLaneDtlSeq      IN     comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vLaneDtlSeqUpd   IN     comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      REC_CNT             OUT NUMBER);

   PROCEDURE IMPORT_SAILING_LANES (
      vRootCompanyId      IN import_sailing_lane.TC_COMPANY_ID%TYPE,
      vTCCompanyId        IN import_sailing_lane.TC_COMPANY_ID%TYPE,
      vBusinessUnit       IN import_sailing_lane.BUSINESS_UNIT%TYPE,
      vOFacilityAliasId   IN import_sailing_lane.O_FACILITY_ALIAS_ID%TYPE,
      vOFacilityBU        IN import_sailing_lane.O_FACILITY_BU%TYPE,
      vOCity              IN import_sailing_lane.O_CITY%TYPE,
      vOStateProv         IN import_sailing_lane.O_STATE_PROV%TYPE,
      vOCounty            IN import_sailing_lane.O_COUNTY%TYPE,
      vOPostal            IN import_sailing_lane.O_POSTAL_CODE%TYPE,
      vOCountry           IN import_sailing_lane.O_COUNTRY_CODE%TYPE,
      vOZoneId            IN import_sailing_lane.O_ZONE_ID%TYPE,
      vOZoneName          IN import_sailing_lane.O_ZONE_NAME%TYPE,
      vDFacilityAliasId   IN import_sailing_lane.D_FACILITY_ALIAS_ID%TYPE,
      vDFacilityBU        IN import_sailing_lane.D_FACILITY_BU%TYPE,
      vDCity              IN import_sailing_lane.D_CITY%TYPE,
      vDStateProv         IN import_sailing_lane.D_STATE_PROV%TYPE,
      vDCounty            IN import_sailing_lane.D_COUNTY%TYPE,
      vDPostal            IN import_sailing_lane.D_POSTAL_CODE%TYPE,
      vDCountry           IN import_sailing_lane.D_COUNTRY_CODE%TYPE,
      vDZoneId            IN import_sailing_lane.D_ZONE_ID%TYPE,
      vDZoneName          IN import_sailing_lane.D_ZONE_NAME%TYPE,
      vBatchId            IN import_sailing_lane.BATCH_ID%TYPE,
      vLaneStatus         IN import_sailing_lane.LANE_STATUS%TYPE);

   PROCEDURE IMPORT_SAILING_LANE_DETAILS (
      vTCCompanyId              IN import_sailing_lane.TC_COMPANY_ID%TYPE,
      vTPCode                   IN import_sailing_lane_dtl.CARRIER_CODE%TYPE,
      vTPCodeBU                 IN import_sailing_lane_dtl.TP_CODE_BU%TYPE,
      vBatchId                  IN import_sailing_lane.BATCH_ID%TYPE,
      vSailingLaneDtlSeq        IN import_sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE,
      vMot                      IN import_sailing_lane_dtl.MOT%TYPE,
      vMotBU                    IN import_sailing_lane_dtl.MOT_BU%TYPE,
      vServiceLevel             IN import_sailing_lane_dtl.SERVICE_LEVEL%TYPE,
      vServiceLevelBU           IN import_sailing_lane_dtl.SERVICE_LEVEL_BU%TYPE,
      vEffectiveDt              IN import_sailing_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDt             IN import_sailing_lane_dtl.EXPIRATION_DT%TYPE,
      vFixedTransitValue        IN import_sailing_lane_dtl.FIXED_TRANSIT_VALUE%TYPE,
      vFixedTransitUOM          IN import_sailing_lane_dtl.FIXED_TRANSIT_STANDARD_UOM%TYPE,
      vSecondaryCarrierCode     IN import_sailing_lane_dtl.SCNDR_CARRIER_CODE%TYPE,
      vSecondaryCarrierCodeBU   IN import_sailing_lane_dtl.SCNDR_CARRIER_CODE_BU%TYPE,
      vSailingScheduleName      IN import_sailing_lane_dtl.SAILING_SCHEDULE_NAME%TYPE,
      vSailingFrequencyType     IN import_sailing_lane_dtl.SAILING_FREQUENCY_TYPE%TYPE);

   PROCEDURE IMPORT_SAILING_SCHDLS_BY_DATE (
      vTCCompanyId          IN import_sailing_lane.TC_COMPANY_ID%TYPE,
      vBatchId              IN import_sailing_lane.BATCH_ID%TYPE,
      vLaneDtlSeq           IN import_sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE,
      vSailSchedByDateSeq   IN import_sailing_schdl_by_date.SAIL_SCHED_BY_DATE_ID%TYPE,
      vVesselName           IN import_sailing_schdl_by_date.RESOURCE_REF_EXTERNAL%TYPE,
      vVoyageNumber         IN import_sailing_schdl_by_date.RESOURCE_NAME_EXTERNAL%TYPE,
      vCutoffDttm           IN import_sailing_schdl_by_date.CUTOFF_DTTM%TYPE,
      vArrivalDttm          IN import_sailing_schdl_by_date.ARRIVAL_DTTM%TYPE,
      vDepartureDttm        IN import_sailing_schdl_by_date.DEPARTURE_DTTM%TYPE,
      vAvailableDttm        IN import_sailing_schdl_by_date.PICKUP_DTTM%TYPE,
      vTransitTime          IN import_sailing_schdl_by_date.TRANSIT_TIME_VALUE%TYPE,
      vTransitTimeUOM       IN import_sailing_schdl_by_date.TRANSIT_TIME_STANDARD_UOM%TYPE,
      vComments             IN import_sailing_schdl_by_date.COMMENTS%TYPE,
      vRestrictions         IN import_sailing_schdl_by_date.DESCRIPTION%TYPE);

   PROCEDURE IMPORT_SAILING_SCHDLS_BY_WK (
      vTCCompanyId          IN import_sailing_lane.TC_COMPANY_ID%TYPE,
      vBatchId              IN import_sailing_lane.BATCH_ID%TYPE,
      vLaneDtlSeq           IN import_sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE,
      vSailSchedByWeekSeq   IN import_sailing_schdl_by_wk.SAIL_SCHED_BY_WEEK_ID%TYPE,
      vVesselName           IN import_sailing_schdl_by_wk.RESOURCE_REF_EXTERNAL%TYPE,
      vVoyageNumber         IN import_sailing_schdl_by_wk.RESOURCE_NAME_EXTERNAL%TYPE,
      vComments             IN import_sailing_schdl_by_wk.COMMENTS%TYPE,
      vRestrictions         IN import_sailing_schdl_by_wk.DESCRIPTION%TYPE,
      vEffectiveDt          IN import_sailing_schdl_by_wk.EFFECTIVE_DT%TYPE,
      vExpirationDt         IN import_sailing_schdl_by_wk.EXPIRATION_DT%TYPE);

   PROCEDURE IMPORT_SAILING_SCHDLS_BY_DAY (
      vTCCompanyId            IN import_sailing_lane.TC_COMPANY_ID%TYPE,
      vBatchId                IN import_sailing_lane.BATCH_ID%TYPE,
      vLaneDtlSeq             IN import_sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE,
      vSailSchedByWeekSeq     IN import_sailing_schdl_by_wk.SAIL_SCHED_BY_WEEK_ID%TYPE,
      vSailSchedByWeekDayId   IN import_sailing_schdl_wk_day.SAIL_SCHED_BY_WEEK_DAY_ID%TYPE,
      vCutoffTime             IN import_sailing_schdl_wk_day.CUTOFF_TIME%TYPE,
      vArrivalTime            IN import_sailing_schdl_wk_day.ARRIVAL_TIME%TYPE,
      vDepartureTime          IN import_sailing_schdl_wk_day.DEPARTURE_TIME%TYPE,
      vAvailableTime          IN import_sailing_schdl_wk_day.PICKUP_TIME%TYPE,
      vCuttOffDay             IN import_sailing_schdl_wk_day.CUTOFF_DAYOFWEEK%TYPE,
      vArrivalDay             IN import_sailing_schdl_wk_day.ARRIVAL_DAYOFWEEK%TYPE,
      vDepartureDay           IN import_sailing_schdl_wk_day.DEPARTURE_DAYOFWEEK%TYPE,
      vAvailableDay           IN import_sailing_schdl_wk_day.PICKUP_DAYOFWEEK%TYPE,
      vTransitTime            IN import_sailing_schdl_wk_day.TRANSIT_TIME_VALUE%TYPE,
      vTransitTimeUOM         IN import_sailing_schdl_wk_day.TRANSIT_TIME_STANDARD_UOM%TYPE,
      vDuration               IN import_sailing_schdl_wk_day.DURATION%TYPE);

   PROCEDURE TRANSFER_IMPORT_SAILING_LANES (
      vTCCompanyId    IN company.COMPANY_ID%TYPE,
      vFirstBatchId   IN sailing_lane.LANE_ID%TYPE,
      vLastBatchId    IN sailing_lane.LANE_ID%TYPE);

   FUNCTION DELETE_IMPORT_SAILING_LANES (
      vTCCompanyId    IN company.COMPANY_ID%TYPE,
      vImportLaneID   IN import_sailing_lane.LANE_ID%TYPE)
      RETURN NUMBER;

   FUNCTION INSERT_SAILING_LANE (
      vTCCompanyId      IN company.COMPANY_ID%TYPE,
      vImportLaneId     IN import_sailing_lane.LANE_ID%TYPE,
      /*  vBusinessUnit    IN business_unit.BUSINESS_UNIT%TYPE,*/
      vLaneHierarchy    IN sailing_lane.LANE_HIERARCHY%TYPE,
      vOLocType         IN sailing_lane.O_LOC_TYPE%TYPE,
      vOFacilityId      IN facility.FACILITY_ID%TYPE,
      vOFacilityAlias   IN facility_alias.FACILITY_ALIAS_ID%TYPE,
      vOFacilityBU      IN import_sailing_lane.O_FACILITY_BU%TYPE,
      vOCity            IN sailing_lane.O_CITY%TYPE,
      vOStateProv       IN state_prov.STATE_PROV%TYPE,
      vOCounty          IN sailing_lane.O_COUNTY%TYPE,
      vOPostal          IN postal_code.POSTAL_CODE%TYPE,
      vOCountry         IN country.COUNTRY_CODE%TYPE,
      vOZone            IN zone.ZONE_ID%TYPE,
      vDLocType         IN sailing_lane.D_LOC_TYPE%TYPE,
      vDFacilityId      IN facility.FACILITY_ID%TYPE,
      vDFacilityAlias   IN facility_alias.FACILITY_ALIAS_ID%TYPE,
      vDFacilityBU      IN import_sailing_lane.D_FACILITY_BU%TYPE,
      vDCity            IN sailing_lane.D_CITY%TYPE,
      vDStateProv       IN state_prov.STATE_PROV%TYPE,
      vDCounty          IN sailing_lane.D_COUNTY%TYPE,
      vDPostal          IN postal_code.POSTAL_CODE%TYPE,
      vDCountry         IN country.COUNTRY_CODE%TYPE,
      vDZone            IN zone.ZONE_ID%TYPE,
      vSailingLaneId    IN sailing_lane.lane_id%TYPE,
      vBatchId          IN import_sailing_lane.BATCH_ID%TYPE)
      RETURN NUMBER;

   FUNCTION GET_BUID_FROM_BUSINESS_UNIT (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vBUColumn         VARCHAR2)
      RETURN NUMBER;

   PROCEDURE TRANSFER_IMP_SAILING_LANE_DTL (
      vTCCompanyId    IN company.COMPANY_ID%TYPE,
      vImportLaneId   IN sailing_lane.LANE_ID%TYPE,
      vLaneId         IN sailing_lane.LANE_ID%TYPE);

   FUNCTION INSERT_SAILING_LANE_DTL (
      vTCCompanyId               IN company.COMPANY_ID%TYPE,
      vSailingLaneId             IN sailing_lane_dtl.LANE_ID%TYPE,
      vCarrierCode               IN sailing_lane_dtl.CARRIER_ID%TYPE,
      vMot                       IN sailing_lane_dtl.MOT_ID%TYPE,
      vServiceLevel              IN sailing_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code       IN sailing_lane_dtl.SCNDR_CARRIER_ID%TYPE,
      vEffectiveDT               IN sailing_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT              IN sailing_lane_dtl.EXPIRATION_DT%TYPE,
      vImpSailingDtlStatus       IN import_sailing_lane_dtl.IMPORT_SAILING_DTL_STATUS%TYPE,
      vFixedTransitSTandardUOM   IN import_sailing_lane_dtl.FIXED_TRANSIT_STANDARD_UOM%TYPE,
      vFixedTransitValue         IN import_sailing_lane_dtl.FIXED_TRANSIT_VALUE%TYPE,
      vSailingFrequencyType      IN import_sailing_lane_dtl.SAILING_FREQUENCY_TYPE%TYPE,
      vSailingScheduleName       IN import_sailing_lane_dtl.SAILING_SCHEDULE_NAME%TYPE)
      RETURN NUMBER;

   PROCEDURE TRANSFER_SAILING_SCH_BY_DT (
      vTCCompanyId            IN company.COMPANY_ID%TYPE,
      vImportLaneId           IN SAILING_SCHDL_BY_DATE.LANE_ID%TYPE,
      vLaneId                 IN SAILING_SCHDL_BY_DATE.LANE_ID%TYPE,
      vImpSailingLaneDtlSeq   IN SAILING_SCHDL_BY_DATE.LANE_DTL_SEQ%TYPE,
      vSailingLaneDtlSeq      IN SAILING_SCHDL_BY_DATE.LANE_DTL_SEQ%TYPE);

   FUNCTION INSERT_SAILING_SCH_BY_WK (
      vTCCompanyId        IN company.COMPANY_ID%TYPE,
      vSailingLaneId      IN SAILING_SCHDL_BY_WEEK.LANE_ID%TYPE,
      vSailingLaneDtlId   IN SAILING_SCHDL_BY_WEEK.LANE_DTL_SEQ%TYPE,
      vVesselName         IN SAILING_SCHDL_BY_WEEK.RESOURCE_NAME_EXTERNAL%TYPE,
      vVoyageNumber       IN SAILING_SCHDL_BY_WEEK.RESOURCE_REF_EXTERNAL%TYPE,
      vComments           IN SAILING_SCHDL_BY_WEEK.COMMENTS%TYPE,
      vRestrictions       IN SAILING_SCHDL_BY_WEEK.DESCRIPTION%TYPE,
      vEffectiveDt        IN SAILING_SCHDL_BY_WEEK.EFFECTIVE_DT%TYPE,
      vExpirationDt       IN SAILING_SCHDL_BY_WEEK.EXPIRATION_DT%TYPE)
      RETURN NUMBER;

   PROCEDURE TRANSFER_SAILING_SCH_BY_WK (
      vTCCompanyId            IN company.COMPANY_ID%TYPE,
      vImportLaneID           IN sailing_lane.LANE_ID%TYPE,
      vLaneId                 IN sailing_lane.LANE_ID%TYPE,
      vImpSailingLaneDtlSeq   IN sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE,
      vSailingLaneDtlSeq      IN sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE);

   PROCEDURE INSERT_SAILING_SCH_BY_DT (
      vTCCompanyId             IN company.COMPANY_ID%TYPE,
      vSailingLaneId           IN SAILING_SCHDL_BY_DATE.LANE_ID%TYPE,
      vSailingLaneDtlId        IN SAILING_SCHDL_BY_DATE.LANE_DTL_SEQ%TYPE,
      vDepatureDttm            IN SAILING_SCHDL_BY_DATE.DEPARTURE_DTTM%TYPE,
      vArrivalDttm             IN SAILING_SCHDL_BY_DATE.ARRIVAL_DTTM%TYPE,
      vCutoffDttm              IN SAILING_SCHDL_BY_DATE.CUTOFF_DTTM%TYPE,
      vPickupDttm              IN SAILING_SCHDL_BY_DATE.PICKUP_DTTM%TYPE,
      vTransitTimeValue        IN SAILING_SCHDL_BY_DATE.TRANSIT_TIME_VALUE%TYPE,
      vTransitTimeStandarUOM   IN SAILING_SCHDL_BY_DATE.TRANSIT_TIME_STANDARD_UOM%TYPE,
      vResourceRefExternal     IN SAILING_SCHDL_BY_DATE.RESOURCE_REF_EXTERNAL%TYPE,
      vResourceNameExternal    IN SAILING_SCHDL_BY_DATE.RESOURCE_NAME_EXTERNAL%TYPE,
      vComments                IN SAILING_SCHDL_BY_DATE.COMMENTS%TYPE,
      vDescription             IN SAILING_SCHDL_BY_DATE.DESCRIPTION%TYPE);

   FUNCTION GET_RESOURCE_WHERE_CLAUSE (
      vCarrierCode           IN sailing_lane_dtl.CARRIER_ID%TYPE,
      vMot                   IN sailing_lane_dtl.MOT_ID%TYPE,
      vServiceLevel          IN sailing_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN sailing_LANE_DTL.SCNDR_CARRIER_ID%TYPE)
      RETURN VARCHAR2;

   FUNCTION GET_BUDG_RESOURCE_WHERE_CLAUSE (
      vMot                   IN sailing_lane_dtl.MOT_ID%TYPE,
      vServiceLevel          IN sailing_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN sailing_lane_dtl.SCNDR_CARRIER_ID%TYPE)
      RETURN VARCHAR2;

   PROCEDURE INSERT_SAILING_SCH_BY_WK_DAY (
      vTCCompanyId        IN company.COMPANY_ID%TYPE,
      vSailingLaneId      IN SAILING_SCHDL_BY_WEEK_DAY.LANE_ID%TYPE,
      vSailingLaneDtlId   IN SAILING_SCHDL_BY_WEEK_DAY.LANE_DTL_SEQ%TYPE,
      vSailingDtlWkSeq    IN SAILING_SCHDL_BY_WEEK_DAY.SAIL_SCHED_BY_WEEK_ID%TYPE,
      vCutoffTime         IN SAILING_SCHDL_BY_WEEK_DAY.CUTOFF_TIME%TYPE,
      vArrivalTime        IN SAILING_SCHDL_BY_WEEK_DAY.ARRIVAL_TIME%TYPE,
      vDepartureTime      IN SAILING_SCHDL_BY_WEEK_DAY.DEPARTURE_TIME%TYPE,
      vAvailableTime      IN SAILING_SCHDL_BY_WEEK_DAY.PICKUP_TIME%TYPE,
      vCuttOffDay         IN SAILING_SCHDL_BY_WEEK_DAY.CUTOFF_DAYOFWEEK%TYPE,
      vArrivalDay         IN SAILING_SCHDL_BY_WEEK_DAY.ARRIVAL_DAYOFWEEK%TYPE,
      vDepartureDay       IN SAILING_SCHDL_BY_WEEK_DAY.DEPARTURE_DAYOFWEEK%TYPE,
      vAvailableDay       IN SAILING_SCHDL_BY_WEEK_DAY.PICKUP_DAYOFWEEK%TYPE,
      vTransitTime        IN SAILING_SCHDL_BY_WEEK_DAY.TRANSIT_TIME_VALUE%TYPE,
      vTransitTimeUOM     IN SAILING_SCHDL_BY_WEEK_DAY.TRANSIT_TIME_STANDARD_UOM%TYPE,
      vDuration           IN SAILING_SCHDL_BY_WEEK_DAY.TRANSIT_TIME_WEEK%TYPE);

   PROCEDURE TRANSFER_SAILING_SCH_WK_DAY (
      vTCCompanyId            IN company.COMPANY_ID%TYPE,
      vImportLaneID           IN sailing_lane.LANE_ID%TYPE,
      vLaneId                 IN sailing_lane.LANE_ID%TYPE,
      vImpSailingLaneDtlSeq   IN sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE,
      vSailingLaneDtlSeq      IN sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE,
      vImpSailingDtlWkSeq     IN sailing_schdl_by_week.SAIL_SCHED_BY_WEEK_ID%TYPE,
      vSailingDtlWkSeq        IN sailing_schdl_by_week.SAIL_SCHED_BY_WEEK_ID%TYPE);

   PROCEDURE VALIDATE_IMPORT_SAILING_LANES (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vFirstBatchId   IN sailing_lane.LANE_ID%TYPE,
      vLastBatchId    IN sailing_lane.LANE_ID%TYPE);

   PROCEDURE VALIDATE_SAILING_LANE (
      vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
      vLaneId        IN SAILING_LANE.LANE_ID%TYPE);

   FUNCTION VALIDATE_LANE_BASE_DATA (
      vTCCompanyId      IN company.COMPANY_ID%TYPE,
      vLaneId           IN sailing_lane.LANE_ID%TYPE,
      vBusinessUnit     IN business_unit.BUSINESS_UNIT%TYPE,
      vOFacilityId      IN facility.FACILITY_ID%TYPE,
      vOFacilityAlias   IN facility_alias.FACILITY_ALIAS_ID%TYPE,
      vOFacilityBU      IN business_unit.BUSINESS_UNIT%TYPE,
      vOStateProv       IN state_prov.STATE_PROV%TYPE,
      vOPostal          IN postal_code.POSTAL_CODE%TYPE,
      vOCountry         IN country.COUNTRY_CODE%TYPE,
      vOZoneId          IN import_sailing_lane.O_ZONE_ID%TYPE,
      vOZoneName        IN import_sailing_lane.O_ZONE_NAME%TYPE,
      vDFacilityId      IN facility.FACILITY_ID%TYPE,
      vDFacilityAlias   IN facility_alias.FACILITY_ALIAS_ID%TYPE,
      vDFacilityBU      IN business_unit.BUSINESS_UNIT%TYPE,
      vDStateProv       IN state_prov.STATE_PROV%TYPE,
      vDPostal          IN postal_code.POSTAL_CODE%TYPE,
      vDCountry         IN country.COUNTRY_CODE%TYPE,
      vDZoneId          IN import_sailing_lane.D_ZONE_ID%TYPE,
      vDZoneName        IN import_sailing_lane.D_ZONE_NAME%TYPE)
      RETURN NUMBER;

   FUNCTION CHECK_IS_BU_VALID (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                               vBUID          IN company.COMPANY_ID%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_HIERARCHY (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vLaneId            IN sailing_lane.LANE_ID%TYPE,
      oFacilityId        IN sailing_lane.O_FACILITY_ID%TYPE,
      oFacilityAliasId   IN sailing_lane.O_FACILITY_ALIAS_ID%TYPE,
      oCity              IN sailing_lane.O_CITY%TYPE,
      oState             IN sailing_lane.O_STATE_PROV%TYPE,
      oCounty            IN sailing_lane.O_COUNTY%TYPE,
      oPostal            IN sailing_lane.O_POSTAL_CODE%TYPE,
      oCountry           IN sailing_lane.O_COUNTRY_CODE%TYPE,
      oZoneId            IN import_sailing_lane.O_ZONE_ID%TYPE,
      oZoneName          IN import_sailing_lane.O_ZONE_NAME%TYPE,
      dFacilityId        IN sailing_lane.D_FACILITY_ID%TYPE,
      dFacilityAliasId   IN sailing_lane.D_FACILITY_ALIAS_ID%TYPE,
      dState             IN sailing_lane.D_STATE_PROV%TYPE,
      dCity              IN sailing_lane.D_CITY%TYPE,
      dCounty            IN sailing_lane.D_COUNTY%TYPE,
      dCountry           IN sailing_lane.D_COUNTRY_CODE%TYPE,
      dPostal            IN sailing_lane.D_POSTAL_CODE%TYPE,
      dZoneId            IN import_sailing_lane.D_ZONE_ID%TYPE,
      dZoneName          IN import_sailing_lane.D_ZONE_NAME%TYPE)
      RETURN NUMBER;

   PROCEDURE INSERT_SAILING_LANE_ERROR (
      vTCCompanyId         IN company.COMPANY_ID%TYPE,
      vLaneId              IN sailing_lane.LANE_ID%TYPE,
      vErrorMsgDesc        IN sailing_lane_errors.ERROR_MSG_DESC%TYPE,
      p_error_msg_id       IN SAILING_LANE_ERRORS.ERROR_MSG_ID%TYPE DEFAULT NULL,
      p_error_param_list   IN SAILING_LANE_ERRORS.ERROR_PARAM_LIST%TYPE DEFAULT NULL);

   PROCEDURE INSERT_SAILING_LANE_DTL_ERROR (
      vTCCompanyId         IN company.COMPANY_ID%TYPE,
      vLaneId              IN sailing_lane.LANE_ID%TYPE,
      vSailingLaneDtlSeq   IN sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE,
      vErrorMsgDesc        IN sailing_lane_errors.ERROR_MSG_DESC%TYPE,
      p_error_msg_id       IN SAILING_LANE_ERRORS.ERROR_MSG_ID%TYPE DEFAULT NULL,
      p_error_param_list   IN SAILING_LANE_ERRORS.ERROR_PARAM_LIST%TYPE DEFAULT NULL);

   FUNCTION GET_PARENT_BU_IDS (vTCCompanyId IN company.COMPANY_ID%TYPE)
      RETURN VARCHAR2;

   PROCEDURE VALIDATE_SAILING_LANE_DTL (
      vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
      vLaneId        IN sailing_lane.lane_id%TYPE);

   FUNCTION VALIDATE_TP_SERV_MODE (
      vTCCompanyId           IN company.COMPANY_ID%TYPE,
      vLaneId                IN sailing_lane.LANE_ID%TYPE,
      vSailingDtlSeq         IN sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE,
      vCarrierCode           IN carrier_code.CARRIER_CODE%TYPE,
      vServiceLevel          IN service_level.SERVICE_LEVEL%TYPE,
      vMode                  IN mot.MOT%TYPE,
      p_scndr_carrier_code   IN CARRIER_CODE.CARRIER_CODE%TYPE,
      vCarrierCodeId         IN carrier_code.CARRIER_ID%TYPE,
      vServiceLevelId        IN service_level.SERVICE_LEVEL_ID%TYPE,
      vModeId                IN mot.MOT_ID%TYPE,
      p_scndr_carrier_Id     IN CARRIER_CODE.CARRIER_ID%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_DETAIL_BASE_DATA (
      vTCCompanyId               IN company.COMPANY_ID%TYPE,
      vLaneId                    IN sailing_lane.LANE_ID%TYPE,
      vSailingDtlSeq             IN sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE,
      vCarrierCode               IN IMPORT_SAILING_LANE_DTL.CARRIER_CODE%TYPE,
      vCarrierCodeBUID           IN company.COMPANY_ID%TYPE,
      vMode                      IN IMPORT_SAILING_LANE_DTL.MOT%TYPE,
      vModeBUID                  IN company.COMPANY_ID%TYPE,
      vServiceLevel              IN IMPORT_SAILING_LANE_DTL.SERVICE_LEVEL%TYPE,
      vServiceLevelBUID          IN company.COMPANY_ID%TYPE,
      p_scndr_carrier_code       IN CARRIER_CODE.CARRIER_CODE%TYPE,
      p_scndr_carrier_codeBUID   IN company.COMPANY_ID%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_SAILING_SCHDL_WEEKLY (
      vTCCompanyId         IN COMPANY.COMPANY_ID%TYPE,
      vLaneId              IN sailing_lane.lane_id%TYPE,
      vSailingLaneDtlSeq   IN sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE/* SL  05/22/02  */
      )
      RETURN NUMBER;

   FUNCTION VALIDATE_SAILING_SCHDL_BY_DATE (
      vTCCompanyId         IN COMPANY.COMPANY_ID%TYPE,
      vLaneId              IN sailing_lane.lane_id%TYPE,
      vSailingLaneDtlSeq   IN sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE/* SL  05/22/02  */
      )
      RETURN NUMBER;

   FUNCTION VALIDATE_SAILING_SCHDL_WK_DAY (
      vTCCompanyId         IN COMPANY.COMPANY_ID%TYPE,
      vLaneId              IN sailing_lane.lane_id%TYPE,
      vSailingLaneDtlSeq   IN sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE,
      vSailSchedByWeekId   IN sailing_schdl_by_week.SAIL_SCHED_BY_WEEK_ID%TYPE,
      vVesselName          IN sailing_schdl_by_week.RESOURCE_REF_EXTERNAL%TYPE,
      vVoyageNumber        IN sailing_schdl_by_week.RESOURCE_NAME_EXTERNAL%TYPE,
      /* SL  05/22/02  */
      vEffectiveDt         IN sailing_schdl_by_week.EFFECTIVE_DT%TYPE,
      vExpirationDt        IN sailing_schdl_by_week.EXPIRATION_DT%TYPE)
      RETURN NUMBER;
	  
	  
   PROCEDURE VALIDATE_LANE_UI(
      VTCCOMPANYID  IN COMPANY.COMPANY_ID%TYPE,
      VSAILINGLANEID  IN SAILING_LANE.LANE_ID%TYPE);
	  
   PROCEDURE VALIDATE_LANE_DTL_EPI (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vImportLaneId   IN SAILING_LANE.LANE_ID%TYPE,
      vLaneId         IN SAILING_LANE.LANE_ID%TYPE);
	
   PROCEDURE REPLICATE_RG_RA_LANE_DETAIL (
     vTCCompanyId   IN company.COMPANY_ID%TYPE,
    vLaneId        IN comb_lane.LANE_ID%TYPE,
    vSailingLaneDtlSeq      IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
    vIsRouting      IN comb_lane_dtl.IS_ROUTING%TYPE,
    vIsRating     IN comb_lane_dtl.IS_RATING%TYPE);
	
END SAILING_LANE_VALIDATION_PKG;	  
/


CREATE OR REPLACE
PACKAGE BODY SAILING_LANE_VALIDATION_PKG
AS
   PROCEDURE ADJUST_EFF_EXP_DT (
      vTCCompanyId           IN company.COMPANY_ID%TYPE,
      vLaneId                IN comb_lane.LANE_ID%TYPE,
      vLaneDtlSeq            IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vEffectiveDT           IN comb_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN comb_lane_dtl.EXPIRATION_DT%TYPE,
      vCarrierCode           IN comb_lane_dtl.CARRIER_ID%TYPE,
      vMot                   IN comb_lane_dtl.MOT_ID%TYPE,
      vServiceLevel          IN comb_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN comb_LANE_DTL.SCNDR_CARRIER_ID%TYPE)
   IS
      eff_exp_sql          VARCHAR2 (2000);
      eff_exp_cursor       ref_curtype;
      vCurrentLaneDtlSeq   comb_lane_dtl.lane_id%TYPE;
      vEffDT               comb_lane_dtl.effective_dt%TYPE;
      vExpDT               comb_lane_dtl.expiration_dt%TYPE;
      vExpDTTemp           comb_lane_dtl.expiration_dt%TYPE;
      vIsRating            comb_lane_dtl.IS_RATING%TYPE;
      vIsRouting           comb_lane_dtl.IS_ROUTING%TYPE;
      vIsSailing           comb_lane_dtl.IS_SAILING%TYPE;
      isFirstRow           INT;
      isInsertAtEnd        INT;
      isChanged            INT;
      vMotTemp             comb_lane_dtl.MOT_ID%TYPE;
      vServiceLevelTemp    comb_lane_dtl.SERVICE_LEVEL_ID%TYPE;
      vTransitUOMTemp      comb_lane_dtl.FIXED_TRANSIT_STANDARD_UOM%TYPE;
      vTransitValueTemp    comb_lane_dtl.FIXED_TRANSIT_VALUE%TYPE;
      vFrequencyTypeTemp   comb_lane_dtl.SAILING_FREQUENCY_TYPE%TYPE;
      vScheduleNameTemp    comb_lane_dtl.SAILING_SCHEDULE_NAME%TYPE;
   BEGIN
      eff_exp_sql :=
         ' SELECT lane_dtl_seq, effective_dt, expiration_dt, is_rating, is_routing, is_sailing '
         || ' FROM comb_lane_dtl '
         || ' WHERE tc_company_id = :vTCCompanyId'
         || ' AND lane_id = :vLaneId'
         || ' AND lane_dtl_seq != :vLaneDtlSeq'
         || ' AND lane_dtl_status = 0 '
         || ' AND effective_dt <= expiration_dt '
         || ' AND expiration_dt >= :vEffectiveDT '
         || ' AND effective_dt <= :vExpirationDT '
         || ' AND equipment_id is null'
         || ' AND protection_level_id is null'
		 || ' AND CUSTOM_TEXT1 IS NULL '
         || ' AND CUSTOM_TEXT2 IS NULL '
         || ' AND CUSTOM_TEXT3 IS NULL '
         || ' AND CUSTOM_TEXT4 IS NULL '
         || ' AND CUSTOM_TEXT5 IS NULL ';

      vMotTemp := vMot;
      vServiceLevelTemp := vServiceLevel;

      IF (vMot = -1)
      THEN
         vMotTemp := NULL;
      END IF;

      IF (vServiceLevel = -1)
      THEN
         vServiceLevelTemp := NULL;
      END IF;

      eff_exp_sql :=
         eff_exp_sql
         || GET_RESOURCE_WHERE_CLAUSE (vCarrierCode,
                                       vMotTemp,
                                       vServiceLevelTemp,
                                       p_scndr_carrier_code);

      eff_exp_sql := eff_exp_sql || ' ORDER BY EFFECTIVE_DT';

      isInsertAtEnd := 1;
      isFirstRow := 0;
      isChanged := 0;

      SELECT FIXED_TRANSIT_STANDARD_UOM,
             FIXED_TRANSIT_VALUE,
             SAILING_FREQUENCY_TYPE,
             SAILING_SCHEDULE_NAME
        INTO vTransitUOMTemp,
             vTransitValueTemp,
             vFrequencyTypeTemp,
             vScheduleNameTemp
        FROM COMB_LANE_DTL
       WHERE     TC_COMPANY_ID = vTCCompanyId
             AND LANE_ID = vLaneId
             AND LANE_DTL_SEQ = vLaneDtlSeq;

      OPEN eff_exp_cursor FOR eff_exp_sql
         USING vTCCompanyId,
               vLaneId,
               vLaneDtlSeq,
               vEffectiveDT,
               vExpirationDT;

      LOOP
         FETCH eff_exp_cursor
         INTO vCurrentLaneDtlSeq,
              vEffDT,
              vExpDT,
              vIsRating,
              vIsRouting,
              vIsSailing;

         EXIT WHEN eff_exp_cursor%NOTFOUND;

         -- Start of Merge with other lane detail
         IF (vEffectiveDT <= vEffDt)
         THEN
            IF (isFirstRow = 0)
            THEN
               IF (vEffectiveDT <> vEffDt)
               THEN
                  -- create row from input effective date to effective date of current row
                  CREATE_NEW_ROW (vTCCompanyId,
                                  vLaneId,
                                  vLaneDtlSeq,
                                  vEffectiveDT,
                                  vEffDt - 1);
               END IF;
            ELSE
               IF ( vExpDTTemp <> vEffDt and vExpDTTemp > vEffDt) 
               THEN
                  -- create row from prev expiration date to effective date of current row
                  CREATE_NEW_ROW (vTCCompanyId,
                                  vLaneId,
                                  vLaneDtlSeq,
                                  vExpDTTemp,
                                  vEffDt - 1);
               END IF;
            END IF;

            IF (vExpirationDT < vExpDT)
            THEN
               --replicate row from input expiration date to exp date of current row with rating flag turned off
               REPLICATE_ROW (vTCCompanyId,
                              vLaneId,
                              -1,
                              vCurrentLaneDtlSeq,
                              vExpirationDT + 1,
                              vExpDT,
                              vIsRating,
                              vIsRouting,
                              vIsSailing);

               UPDATE comb_lane_dtl
                  SET is_sailing = 1,
                      expiration_dt = vExpirationDT,
                      fixed_transit_standard_uom = vTransitUOMTemp,
                      fixed_transit_value = vTransitValueTemp,
                      sailing_frequency_type = vFrequencyTypeTemp,
                      sailing_schedule_name = vScheduleNameTemp,
                      last_updated_source_type = 3,
                      last_updated_source = 'OMS',
                      last_updated_dttm = getdate ()
                WHERE     tc_company_id = vTCCompanyId
                      AND lane_id = vLaneId
                      AND lane_dtl_seq = vCurrentLaneDtlSeq;

               --COPY_CHILD_ROW( vTCCompanyId, vLaneId, vLaneDtlSeq, vCurrentLaneDtlSeq,
               -- vCurrentLaneDtlSeq, 0, 0, 1 );

               isInsertAtEnd := 1;
            ELSE
               UPDATE comb_lane_dtl
                  SET is_sailing = 1,
                      fixed_transit_standard_uom = vTransitUOMTemp,
                      fixed_transit_value = vTransitValueTemp,
                      sailing_frequency_type = vFrequencyTypeTemp,
                      sailing_schedule_name = vScheduleNameTemp,
                      last_updated_source_type = 3,
                      last_updated_source = 'OMS',
                      last_updated_dttm = getdate ()
                WHERE     tc_company_id = vTCCompanyId
                      AND lane_id = vLaneId
                      AND lane_dtl_seq = vCurrentLaneDtlSeq;

               --COPY_CHILD_ROW( vTCCompanyId, vLaneId, vLaneDtlSeq, vCurrentLaneDtlSeq,
               --   vCurrentLaneDtlSeq, 0, 0, 1 );

               isInsertAtEnd := 0;
            END IF;

            IF (vIsSailing = 1)
            THEN
               DELETE_CHILD_ROW (vTCCompanyId, vLaneId, vCurrentLaneDtlSeq);
            END IF;

            COPY_CHILD_ROW (vTCCompanyId,
                            vLaneId,
                            vLaneDtlSeq,
                            vCurrentLaneDtlSeq,
                            vCurrentLaneDtlSeq,
                            0,
                            0,
                            1);
         ELSE
            UPDATE comb_lane_dtl
               SET expiration_dt = vEffectiveDT - 1,
                   last_updated_source_type = 3,
                   last_updated_source = 'OMS',
                   last_updated_dttm = getdate ()
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND lane_dtl_seq = vCurrentLaneDtlSeq;

            IF (vExpirationDT < vExpDT)
            THEN
               --replicate row from input effective date to input exp date with rating flag turned on
               REPLICATE_ROW (vTCCompanyId,
                              vLaneId,
                              vLaneDtlSeq,
                              vCurrentLaneDtlSeq,
                              vEffectiveDT,
                              vExpirationDT,
                              vIsRating,
                              vIsRouting,
                              1);
               --replicate row from input expiration date to exp date of current row with rating flag turned off
               REPLICATE_ROW (vTCCompanyId,
                              vLaneId,
                              -1,
                              vCurrentLaneDtlSeq,
                              vExpirationDT + 1,
                              vExpDT,
                              vIsRating,
                              vIsRouting,
                              vIsSailing);
               isInsertAtEnd := 1;
            ELSE
               --replicate row from input eff date to exp date of current row with rating flag turned on
               REPLICATE_ROW (vTCCompanyId,
                              vLaneId,
                              vLaneDtlSeq,
                              vCurrentLaneDtlSeq,
                              vEffectiveDT,
                              vExpDT,
                              vIsRating,
                              vIsRouting,
                              1);
               isInsertAtEnd := 0;
            END IF;
         END IF;

         isFirstRow := 1;
         vExpDTTemp := vExpDT + 1;
      END LOOP;

      IF (isInsertAtEnd = 0)
      THEN
         IF (vExpDT <> vExpirationDT)
         THEN
            -- create row from exp date of last row to input expiration date
            CREATE_NEW_ROW (vTCCompanyId,
                            vLaneId,
                            vLaneDtlSeq,
                            vExpDT + 1,
                            vExpirationDT);
         END IF;
      END IF;

      IF (isFirstRow = 1)
      THEN
         --UPDATE comb_lane_dtl
         --SET lane_dtl_status = 2,
         --last_updated_source_type = 3,
         -- last_updated_source = 'OMS',
         --last_updated_dttm = getdate()

         DELETE_CHILD_ROW (vTCCompanyId, vLaneId, vLaneDtlSeq);

         DELETE FROM comb_lane_dtl
               WHERE     tc_company_id = vTCCompanyId
                     AND lane_id = vLaneId
                     AND lane_dtl_seq = vLaneDtlSeq;
      END IF;

      COMMIT;

      CLOSE eff_exp_cursor;
   END ADJUST_EFF_EXP_DT;

   --Added for update of carriers
   PROCEDURE ADJUST_EFF_EXP_DT (
      vTCCompanyId           IN company.COMPANY_ID%TYPE,
      vLaneId                IN comb_lane.LANE_ID%TYPE,
      vLaneDtlSeq            IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vLaneDtlSeqUpd         IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vEffectiveDT           IN comb_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN comb_lane_dtl.EXPIRATION_DT%TYPE,
      vCarrierCode           IN comb_lane_dtl.CARRIER_ID%TYPE,
      vMot                   IN comb_lane_dtl.MOT_ID%TYPE,
      vServiceLevel          IN comb_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN comb_LANE_DTL.SCNDR_CARRIER_ID%TYPE)
   IS
      eff_exp_sql          VARCHAR2 (2000);
      eff_exp_cursor       ref_curtype;
      vCurrentLaneDtlSeq   comb_lane_dtl.lane_id%TYPE;
      vEffDT               comb_lane_dtl.effective_dt%TYPE;
      vExpDT               comb_lane_dtl.expiration_dt%TYPE;
      vExpDTTemp           comb_lane_dtl.expiration_dt%TYPE;
      vIsRating            comb_lane_dtl.IS_RATING%TYPE;
      vIsRouting           comb_lane_dtl.IS_ROUTING%TYPE;
      vIsSailing           comb_lane_dtl.IS_SAILING%TYPE;
      isFirstRow           INT;
      isInsertAtEnd        INT;
      isChanged            INT;
      tempCount            INT;
      vMotTemp             comb_lane_dtl.MOT_ID%TYPE;
      vServiceLevelTemp    comb_lane_dtl.SERVICE_LEVEL_ID%TYPE;
      vTransitUOMTemp      comb_lane_dtl.FIXED_TRANSIT_STANDARD_UOM%TYPE;
      vTransitValueTemp    comb_lane_dtl.FIXED_TRANSIT_VALUE%TYPE;
      vFrequencyTypeTemp   comb_lane_dtl.SAILING_FREQUENCY_TYPE%TYPE;
      vScheduleNameTemp    comb_lane_dtl.SAILING_SCHEDULE_NAME%TYPE;
   BEGIN
      eff_exp_sql :=
         ' SELECT lane_dtl_seq, effective_dt, expiration_dt, is_rating, is_routing, is_sailing '
         || ' FROM comb_lane_dtl '
         || ' WHERE tc_company_id = :vTCCompanyId'
         || ' AND lane_id = :vLaneId'
         || ' AND lane_dtl_seq != :vLaneDtlSeq'
         || ' AND lane_dtl_status = 0 '
         || ' AND HAS_SHIPPING_PARAM = 0 '
         || ' AND effective_dt < expiration_dt '
         || ' AND expiration_dt >= :vEffectiveDT '
         || ' AND effective_dt <= :vExpirationDT '
         || ' AND equipment_id is null'
         || ' AND protection_level_id is null'
		 || ' AND CUSTOM_TEXT1 IS NULL '
         || ' AND CUSTOM_TEXT2 IS NULL '
         || ' AND CUSTOM_TEXT3 IS NULL '
         || ' AND CUSTOM_TEXT4 IS NULL '
         || ' AND CUSTOM_TEXT5 IS NULL ';

      vMotTemp := vMot;
      vServiceLevelTemp := vServiceLevel;

      IF (vMot = -1)
      THEN
         vMotTemp := NULL;
      END IF;

      IF (vServiceLevel = -1)
      THEN
         vServiceLevelTemp := NULL;
      END IF;

      tempCount := 0;

      IF (vLaneDtlSeqUpd IS NOT NULL)
      THEN
         --tempCount := MERGE_DRAFT( vTCCompanyId, vLaneId, vLaneDtlSeq, vLaneDtlSeqUpd );
         SELECT COUNT (*)
           INTO tempCount
           FROM COMB_LANE_DTL
          WHERE     TC_COMPANY_ID = vTCCompanyId
                AND LANE_ID = vLaneId
                AND LANE_DTL_SEQ = vLaneDtlSeqUpd
                AND IS_RATING = 0
                AND IS_ROUTING = 0;

         IF (tempCount > 0)
         THEN
            eff_exp_sql :=
               eff_exp_sql || ' AND lane_dtl_seq != ' || vLaneDtlSeqUpd;
         END IF;
      END IF;

      eff_exp_sql :=
         eff_exp_sql
         || GET_RESOURCE_WHERE_CLAUSE (vCarrierCode,
                                       vMotTemp,
                                       vServiceLevelTemp,
                                       p_scndr_carrier_code);

      eff_exp_sql := eff_exp_sql || ' ORDER BY EFFECTIVE_DT';

      isInsertAtEnd := 1;
      isFirstRow := 0;
      isChanged := 0;

      SELECT FIXED_TRANSIT_STANDARD_UOM,
             FIXED_TRANSIT_VALUE,
             SAILING_FREQUENCY_TYPE,
             SAILING_SCHEDULE_NAME
        INTO vTransitUOMTemp,
             vTransitValueTemp,
             vFrequencyTypeTemp,
             vScheduleNameTemp
        FROM COMB_LANE_DTL
       WHERE     TC_COMPANY_ID = vTCCompanyId
             AND LANE_ID = vLaneId
             AND LANE_DTL_SEQ = vLaneDtlSeq;

      OPEN eff_exp_cursor FOR eff_exp_sql
         USING vTCCompanyId,
               vLaneId,
               vLaneDtlSeq,
               vEffectiveDT,
               vExpirationDT;

      LOOP
         FETCH eff_exp_cursor
         INTO vCurrentLaneDtlSeq,
              vEffDT,
              vExpDT,
              vIsRating,
              vIsRouting,
              vIsSailing;

         EXIT WHEN eff_exp_cursor%NOTFOUND;

         -- Start of Merge with other lane detail
         IF (vEffectiveDT <= vEffDt)
         THEN
            IF (isFirstRow = 0)
            THEN
               IF (vEffectiveDT <> vEffDt)
               THEN
                  IF ( (tempCount > 0) AND (isChanged = 0))
                  THEN
                     isChanged := 1;
                     tempCount :=
                        MERGE_DRAFT (vTCCompanyId,
                                     vLaneId,
                                     vLaneDtlSeq,
                                     vLaneDtlSeqUpd);

                     -- Update the existing row
                     UPDATE comb_lane_dtl
                        SET effective_dt = vEffectiveDT,
                            expiration_dt = vEffDt - 1,
                            last_updated_source_type = 3,
                            last_updated_source = 'OMS',
                            last_updated_dttm = getdate ()
                      WHERE     tc_company_id = vTCCompanyId
                            AND lane_id = vLaneId
                            AND lane_dtl_seq = vLaneDtlSeqUpd;
                  ELSE
                     -- create row from input effective date to effective date of current row
                     CREATE_NEW_ROW (vTCCompanyId,
                                     vLaneId,
                                     vLaneDtlSeq,
                                     vEffectiveDT,
                                     vEffDt - 1);
                  END IF;
               END IF;
            ELSE
               IF (vExpDTTemp <> vEffDt)
               THEN
                  IF ( (tempCount > 0) AND (isChanged = 0))
                  THEN
                     isChanged := 1;
                     tempCount :=
                        MERGE_DRAFT (vTCCompanyId,
                                     vLaneId,
                                     vLaneDtlSeq,
                                     vLaneDtlSeqUpd);

                     -- Update the existing row
                     UPDATE comb_lane_dtl
                        SET effective_dt = vExpDTTemp,
                            expiration_dt = vEffDt - 1,
                            last_updated_source_type = 3,
                            last_updated_source = 'OMS',
                            last_updated_dttm = getdate ()
                      WHERE     tc_company_id = vTCCompanyId
                            AND lane_id = vLaneId
                            AND lane_dtl_seq = vLaneDtlSeqUpd;
                  ELSE
                     -- create row from prev expiration date to effective date of current row
                     CREATE_NEW_ROW (vTCCompanyId,
                                     vLaneId,
                                     vLaneDtlSeq,
                                     vExpDTTemp,
                                     vEffDt - 1);
                  END IF;
               END IF;
            END IF;

            IF (vExpirationDT < vExpDT)
            THEN
               --replicate row from input expiration date to exp date of current row with rating flag turned off
               REPLICATE_ROW (vTCCompanyId,
                              vLaneId,
                              -1,
                              vCurrentLaneDtlSeq,
                              vExpirationDT + 1,
                              vExpDT,
                              vIsRating,
                              vIsRouting,
                              vIsSailing);

               UPDATE comb_lane_dtl
                  SET is_sailing = 1,
                      expiration_dt = vExpirationDT,
                      fixed_transit_standard_uom = vTransitUOMTemp,
                      fixed_transit_value = vTransitValueTemp,
                      sailing_frequency_type = vFrequencyTypeTemp,
                      sailing_schedule_name = vScheduleNameTemp,
                      last_updated_source_type = 3,
                      last_updated_source = 'OMS',
                      last_updated_dttm = getdate ()
                WHERE     tc_company_id = vTCCompanyId
                      AND lane_id = vLaneId
                      AND lane_dtl_seq = vCurrentLaneDtlSeq;

               --COPY_CHILD_ROW( vTCCompanyId, vLaneId, vLaneDtlSeq, vCurrentLaneDtlSeq,
               -- vCurrentLaneDtlSeq, 0, 0, 1 );

               isInsertAtEnd := 1;
            ELSE
               UPDATE comb_lane_dtl
                  SET is_sailing = 1,
                      fixed_transit_standard_uom = vTransitUOMTemp,
                      fixed_transit_value = vTransitValueTemp,
                      sailing_frequency_type = vFrequencyTypeTemp,
                      sailing_schedule_name = vScheduleNameTemp,
                      last_updated_source_type = 3,
                      last_updated_source = 'OMS',
                      last_updated_dttm = getdate ()
                WHERE     tc_company_id = vTCCompanyId
                      AND lane_id = vLaneId
                      AND lane_dtl_seq = vCurrentLaneDtlSeq;

               --COPY_CHILD_ROW( vTCCompanyId, vLaneId, vLaneDtlSeq, vCurrentLaneDtlSeq,
               --   vCurrentLaneDtlSeq, 0, 0, 1 );

               isInsertAtEnd := 0;
            END IF;

            IF (vIsSailing = 1)
            THEN
               DELETE_CHILD_ROW (vTCCompanyId, vLaneId, vCurrentLaneDtlSeq);
            END IF;

            COPY_CHILD_ROW (vTCCompanyId,
                            vLaneId,
                            vLaneDtlSeq,
                            vCurrentLaneDtlSeq,
                            vCurrentLaneDtlSeq,
                            0,
                            0,
                            1);
         ELSE
            UPDATE comb_lane_dtl
               SET expiration_dt = vEffectiveDT - 1,
                   last_updated_source_type = 3,
                   last_updated_source = 'OMS',
                   last_updated_dttm = getdate ()
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND lane_dtl_seq = vCurrentLaneDtlSeq;

            IF (vExpirationDT < vExpDT)
            THEN
               --replicate row from input effective date to input exp date with rating flag turned on
               REPLICATE_ROW (vTCCompanyId,
                              vLaneId,
                              vLaneDtlSeq,
                              vCurrentLaneDtlSeq,
                              vEffectiveDT,
                              vExpirationDT,
                              vIsRating,
                              vIsRouting,
                              1);
               --replicate row from input expiration date to exp date of current row with rating flag turned off
               REPLICATE_ROW (vTCCompanyId,
                              vLaneId,
                              -1,
                              vCurrentLaneDtlSeq,
                              vExpirationDT + 1,
                              vExpDT,
                              vIsRating,
                              vIsRouting,
                              vIsSailing);
               isInsertAtEnd := 1;
            ELSE
               --replicate row from input eff date to exp date of current row with rating flag turned on
               REPLICATE_ROW (vTCCompanyId,
                              vLaneId,
                              vLaneDtlSeq,
                              vCurrentLaneDtlSeq,
                              vEffectiveDT,
                              vExpDT,
                              vIsRating,
                              vIsRouting,
                              1);
               isInsertAtEnd := 0;
            END IF;
         END IF;

         isFirstRow := 1;
         vExpDTTemp := vExpDT + 1;
      END LOOP;

      IF (isInsertAtEnd = 0)
      THEN
         IF (vExpDT <> vExpirationDT)
         THEN
            IF ( (tempCount > 0) AND (isChanged = 0))
            THEN
               isChanged := 1;
               tempCount :=
                  MERGE_DRAFT (vTCCompanyId,
                               vLaneId,
                               vLaneDtlSeq,
                               vLaneDtlSeqUpd);

               -- Update the existing row
               UPDATE comb_lane_dtl
                  SET effective_dt = vExpDT + 1,
                      expiration_dt = vExpirationDT,
                      last_updated_source_type = 3,
                      last_updated_source = 'OMS',
                      last_updated_dttm = getdate ()
                WHERE     tc_company_id = vTCCompanyId
                      AND lane_id = vLaneId
                      AND lane_dtl_seq = vLaneDtlSeqUpd;
            ELSE
               -- create row from exp date of last row to input expiration date
               CREATE_NEW_ROW (vTCCompanyId,
                               vLaneId,
                               vLaneDtlSeq,
                               vExpDT + 1,
                               vExpirationDT);
            END IF;
         END IF;
      END IF;

      IF (isFirstRow = 1)
      THEN
         --UPDATE comb_lane_dtl
         --SET lane_dtl_status = 2,
         --  last_updated_source_type = 3,
         --    last_updated_source = 'OMS',
         -- last_updated_dttm = getdate()

         DELETE_CHILD_ROW (vTCCompanyId, vLaneId, vLaneDtlSeq);

         DELETE FROM comb_lane_dtl
               WHERE     tc_company_id = vTCCompanyId
                     AND lane_id = vLaneId
                     AND lane_dtl_seq = vLaneDtlSeq;
      END IF;

      --For Update flow
      IF (tempCount > 0)
      THEN
         -- If the draft is not merged with row which is getting updated
         IF ( (isChanged = 0) AND (isFirstRow = 1))
         THEN
            UPDATE comb_lane_dtl
               SET expiration_dt = effective_dt - 1,
                   last_updated_source_type = 3,
                   last_updated_source = 'OMS',
                   last_updated_dttm = getdate ()
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND lane_dtl_seq = vLaneDtlSeqUpd;
         -- If the draft is not merged with any row
         ELSIF (isFirstRow = 0)
         THEN
            tempCount :=
               MERGE_DRAFT (vTCCompanyId,
                            vLaneId,
                            vLaneDtlSeq,
                            vLaneDtlSeqUpd);
            DELETE_CHILD_ROW (vTCCompanyId, vLaneId, vLaneDtlSeq);

            DELETE FROM comb_lane_dtl
                  WHERE     tc_company_id = vTCCompanyId
                        AND lane_id = vLaneId
                        AND lane_dtl_seq = vLaneDtlSeq;
         END IF;
      END IF;

      COMMIT;

      CLOSE eff_exp_cursor;
   END ADJUST_EFF_EXP_DT;

   -- SR1, 4R1 change, build92,  TT31182 [BEGIN]
   PROCEDURE COPY_CARRIERS (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                            vLaneId        IN comb_lane.LANE_ID%TYPE,
                            vOldLaneId     IN comb_lane.LANE_ID%TYPE)
   IS
      select_sql             VARCHAR2 (2000);
      select_cursor          ref_curtype;
      vLaneDtlSeq            comb_lane_dtl.LANE_DTL_SEQ%TYPE;
      vLaneDtlSeqNew         comb_lane_dtl.LANE_DTL_SEQ%TYPE;
      vEffectiveDT           comb_lane_dtl.EFFECTIVE_DT%TYPE;
      vExpirationDT          comb_lane_dtl.EXPIRATION_DT%TYPE;
      vCarrierCode           comb_lane_dtl.CARRIER_ID%TYPE;
      vMot                   comb_lane_dtl.MOT_ID%TYPE;
      vServiceLevel          comb_lane_dtl.SERVICE_LEVEL_ID%TYPE;
      p_scndr_carrier_code   comb_lane_dtl.SCNDR_CARRIER_ID%TYPE;
      vLaneDtlStatus         comb_lane_dtl.LANE_DTL_STATUS%TYPE;
      vMasterLaneId          comb_lane_dtl.MASTER_LANE_ID%TYPE;
      week_day_sql           VARCHAR2 (2000);
      week_day_cursor        ref_curtype;
      vWeekId                SAILING_SCHDL_BY_WEEK.SAIL_SCHED_BY_WEEK_ID%TYPE;
   BEGIN
      SELECT MASTER_LANE_ID
        INTO vMasterLaneId
        FROM comb_lane
       WHERE tc_company_id = vTCCompanyId AND lane_id = vLaneId;

      select_sql :=
            ' SELECT LANE_DTL_SEQ, LANE_DTL_STATUS, '
         || ' EFFECTIVE_DT, EXPIRATION_DT, '
         || ' CARRIER_ID, MOT_ID, '
         || ' SERVICE_LEVEL_ID, SCNDR_CARRIER_ID '
         || ' FROM COMB_LANE_DTL '
         || ' WHERE tc_company_id = :vTCCompanyId'
         || ' AND lane_id = :vOldLaneId'
         || ' AND is_sailing = 1'
         || ' AND lane_dtl_status < 2';

      OPEN select_cursor FOR select_sql USING vTCCompanyId, vOldLaneId;

      LOOP
         FETCH select_cursor
         INTO vLaneDtlSeq,
              vLaneDtlStatus,
              vEffectiveDT,
              vExpirationDT,
              vCarrierCode,
              vMot,
              vServiceLevel,
              p_scndr_carrier_code;

         EXIT WHEN select_cursor%NOTFOUND;

         SELECT NVL (MAX (LANE_DTL_SEQ), 0) + 1
           INTO vLaneDtlSeqNew
           FROM COMB_LANE_DTL
          WHERE TC_COMPANY_ID = vTCCompanyID AND LANE_ID = vLaneID;

         INSERT INTO COMB_LANE_DTL (TC_COMPANY_ID,
                                    LANE_ID,
                                    LANE_DTL_SEQ,
                                    EFFECTIVE_DT,
                                    EXPIRATION_DT,
                                    CARRIER_ID,
                                    SCNDR_CARRIER_ID,
                                    MOT_ID,
                                    SERVICE_LEVEL_ID,
                                    FIXED_TRANSIT_VALUE,
                                    FIXED_TRANSIT_STANDARD_UOM,
                                    SAILING_SCHEDULE_NAME,
                                    SAILING_FREQUENCY_TYPE,
                                    LAST_UPDATED_SOURCE_TYPE,
                                    LAST_UPDATED_SOURCE,
                                    LAST_UPDATED_DTTM,
                                    LANE_DTL_STATUS,
                                    IS_SAILING,
                                    MASTER_LANE_ID,
                                    REP_TP_FLAG,
				    REJECT_FURTHER_SHIPMENTS,
                                    CARRIER_REJECT_PERIOD)
            SELECT TC_COMPANY_ID,
                   vLaneId,
                   vLaneDtlSeqNew,
                   EFFECTIVE_DT,
                   EXPIRATION_DT,
                   CARRIER_ID,
                   SCNDR_CARRIER_ID,
                   MOT_ID,
                   SERVICE_LEVEL_ID,
                   FIXED_TRANSIT_VALUE,
                   FIXED_TRANSIT_STANDARD_UOM,
                   SAILING_SCHEDULE_NAME,
                   SAILING_FREQUENCY_TYPE,
                   3,
                   'OMS',
                   GETDATE (),
                   1,
                   1,
                   vMasterLaneId,
                   1,
		   REJECT_FURTHER_SHIPMENTS,
                   CARRIER_REJECT_PERIOD 
              FROM COMB_LANE_DTL
             WHERE     TC_COMPANY_ID = vTCCompanyId
                   AND LANE_ID = vOldLaneId
                   AND LANE_DTL_SEQ = vLaneDtlSeq;

         INSERT INTO SAILING_SCHDL_BY_DATE (TC_COMPANY_ID,
                                            LANE_ID,
                                            LANE_DTL_SEQ,
                                            SAIL_SCHED_BY_DATE_ID,
                                            DEPARTURE_DTTM,
                                            ARRIVAL_DTTM,
                                            CUTOFF_DTTM,
                                            PICKUP_DTTM,
                                            TRANSIT_TIME_VALUE,
                                            TRANSIT_TIME_STANDARD_UOM,
                                            RESOURCE_REF_EXTERNAL,
                                            RESOURCE_NAME_EXTERNAL,
                                            COMMENTS,
                                            DESCRIPTION)
            SELECT TC_COMPANY_ID,
                   vLaneId,
                   vLaneDtlSeqNew,
                   SEQ_SAIL_SCHED_BY_DATE_ID.NEXTVAL,
                   DEPARTURE_DTTM,
                   ARRIVAL_DTTM,
                   CUTOFF_DTTM,
                   PICKUP_DTTM,
                   TRANSIT_TIME_VALUE,
                   TRANSIT_TIME_STANDARD_UOM,
                   RESOURCE_REF_EXTERNAL,
                   RESOURCE_NAME_EXTERNAL,
                   COMMENTS,
                   DESCRIPTION
              FROM SAILING_SCHDL_BY_DATE
             WHERE     TC_COMPANY_ID = vTCCompanyId
                   AND LANE_ID = vOldLaneId
                   AND LANE_DTL_SEQ = vLaneDtlSeq;

         week_day_sql :=
               ' SELECT SAIL_SCHED_BY_WEEK_ID '
            || ' FROM SAILING_SCHDL_BY_WEEK '
            || ' WHERE tc_company_id = :vTCCompanyId'
            || ' AND lane_id = :vOldLaneId'
            || ' AND lane_dtl_seq = :vLaneDtlSeq';

         OPEN week_day_cursor FOR week_day_sql
            USING vTCCompanyId, vOldLaneId, vLaneDtlSeq;

         LOOP
            FETCH week_day_cursor INTO vWeekId;

            EXIT WHEN week_day_cursor%NOTFOUND;

            INSERT INTO SAILING_SCHDL_BY_WEEK (TC_COMPANY_ID,
                                               LANE_ID,
                                               LANE_DTL_SEQ,
                                               SAIL_SCHED_BY_WEEK_ID,
                                               EFFECTIVE_DT,
                                               EXPIRATION_DT,
                                               RESOURCE_REF_EXTERNAL,
                                               RESOURCE_NAME_EXTERNAL,
                                               COMMENTS,
                                               DESCRIPTION)
               SELECT TC_COMPANY_ID,
                      vLaneId,
                      vLaneDtlSeqNew,
                      SEQ_SAIL_SCHED_BY_WEEK_ID.NEXTVAL,
                      EFFECTIVE_DT,
                      EXPIRATION_DT,
                      RESOURCE_REF_EXTERNAL,
                      RESOURCE_NAME_EXTERNAL,
                      COMMENTS,
                      DESCRIPTION
                 FROM SAILING_SCHDL_BY_WEEK
                WHERE     TC_COMPANY_ID = vTCCompanyId
                      AND LANE_ID = vOldLaneId
                      AND LANE_DTL_SEQ = vLaneDtlSeq
                      AND SAIL_SCHED_BY_WEEK_ID = vWeekId;

            INSERT INTO SAILING_SCHDL_BY_WEEK_DAY (TC_COMPANY_ID,
                                                   LANE_ID,
                                                   LANE_DTL_SEQ,
                                                   SAIL_SCHED_BY_WEEK_ID,
                                                   SAIL_SCHED_BY_WEEK_DAY_ID,
                                                   DEPARTURE_DAYOFWEEK,
                                                   DEPARTURE_TIME,
                                                   ARRIVAL_DAYOFWEEK,
                                                   ARRIVAL_TIME,
                                                   CUTOFF_DAYOFWEEK,
                                                   CUTOFF_TIME,
                                                   PICKUP_DAYOFWEEK,
                                                   PICKUP_TIME,
                                                   TRANSIT_TIME_VALUE,
                                                   TRANSIT_TIME_STANDARD_UOM,
                                                   TRANSIT_TIME_WEEK)
               SELECT TC_COMPANY_ID,
                      vLaneId,
                      vLaneDtlSeqNew,
                      SEQ_SAIL_SCHED_BY_WEEK_ID.CURRVAL,
                      SEQ_SAIL_SCHED_BY_WEEK_DAY_ID.NEXTVAL,
                      DEPARTURE_DAYOFWEEK,
                      DEPARTURE_TIME,
                      ARRIVAL_DAYOFWEEK,
                      ARRIVAL_TIME,
                      CUTOFF_DAYOFWEEK,
                      CUTOFF_TIME,
                      PICKUP_DAYOFWEEK,
                      PICKUP_TIME,
                      TRANSIT_TIME_VALUE,
                      TRANSIT_TIME_STANDARD_UOM,
                      TRANSIT_TIME_WEEK
                 FROM SAILING_SCHDL_BY_WEEK_DAY
                WHERE     TC_COMPANY_ID = vTCCompanyId
                      AND LANE_ID = vOldLaneId
                      AND LANE_DTL_SEQ = vLaneDtlSeq
                      AND SAIL_SCHED_BY_WEEK_ID = vWeekId;
         END LOOP;

         CLOSE week_day_cursor;

         IF (vLaneDtlStatus = 0)
         THEN
            ADJUST_EFF_EXP_DT (vTCCompanyId,
                               vLaneId,
                               vLaneDtlSeqNew,
                               NULL,
                               vEffectiveDT,
                               vExpirationDT,
                               vCarrierCode,
                               vMot,
                               vServiceLevel,
                               p_scndr_carrier_code);

            UPDATE COMB_LANE_DTL
               SET LANE_DTL_STATUS = 0, LAST_UPDATED_DTTM = GETDATE ()
             WHERE     TC_COMPANY_ID = vTCCompanyId
                   AND LANE_ID = vLaneId
                   AND LANE_DTL_SEQ = vLaneDtlSeqNew
                   AND LANE_DTL_STATUS = 1;
         END IF;
      END LOOP;

      CLOSE select_cursor;
   END COPY_CARRIERS;

   -- SR1, 4R1 change, build92,  TT31182 [END]
   FUNCTION MERGE_DRAFT (vTCCompanyId     IN company.COMPANY_ID%TYPE,
                         vLaneId          IN comb_lane.LANE_ID%TYPE,
                         vLaneDtlSeq      IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
                         vLaneDtlSeqUpd   IN comb_lane_dtl.LANE_DTL_SEQ%TYPE)
      RETURN INT
   IS
      tempCount   INT;
   BEGIN
      tempCount := 0;

      SELECT COUNT (*)
        INTO tempCount
        FROM COMB_LANE_DTL
       WHERE     TC_COMPANY_ID = vTCCompanyId
             AND LANE_ID = vLaneId
             AND LANE_DTL_SEQ = vLaneDtlSeqUpd
             AND IS_RATING = 0
             AND IS_ROUTING = 0;

      IF (tempCount > 0)
      THEN
         DELETE_CHILD_ROW (vTCCompanyId, vLaneId, vLaneDtlSeqUpd);
         COPY_CHILD_ROW (vTCCompanyId,
                         vLaneId,
                         -1,
                         vLaneDtlSeq,
                         vLaneDtlSeqUpd,
                         0,
                         0,
                         1);

         FOR CUR_DTL_UPD
            IN (SELECT EFFECTIVE_DT,
                       EXPIRATION_DT,
                       CARRIER_ID,
                       MOT_ID,
                       SERVICE_LEVEL_ID,
                       SCNDR_CARRIER_ID,
                       FIXED_TRANSIT_STANDARD_UOM,
                       FIXED_TRANSIT_VALUE,
                       SAILING_FREQUENCY_TYPE,
                       SAILING_SCHEDULE_NAME,
		       REJECT_FURTHER_SHIPMENTS,
                       CARRIER_REJECT_PERIOD 
                  FROM COMB_LANE_DTL
                 WHERE     TC_COMPANY_ID = vTCCompanyId
                       AND LANE_ID = vLaneId
                       AND LANE_DTL_SEQ = vLaneDtlSeq)
         LOOP
            UPDATE comb_lane_dtl
               SET LAST_UPDATED_SOURCE_TYPE = 3,
                   LAST_UPDATED_SOURCE = 'OMS',
                   LAST_UPDATED_DTTM = getdate (),
                   EFFECTIVE_DT = CUR_DTL_UPD.EFFECTIVE_DT,
                   EXPIRATION_DT = CUR_DTL_UPD.EXPIRATION_DT,
                   CARRIER_ID = CUR_DTL_UPD.CARRIER_ID,
                   MOT_ID = CUR_DTL_UPD.MOT_ID,
                   SERVICE_LEVEL_ID = CUR_DTL_UPD.SERVICE_LEVEL_ID,
                   SCNDR_CARRIER_ID = CUR_DTL_UPD.SCNDR_CARRIER_ID,
                   FIXED_TRANSIT_STANDARD_UOM =
                      CUR_DTL_UPD.FIXED_TRANSIT_STANDARD_UOM,
                   FIXED_TRANSIT_VALUE = CUR_DTL_UPD.FIXED_TRANSIT_VALUE,
                   SAILING_FREQUENCY_TYPE = CUR_DTL_UPD.SAILING_FREQUENCY_TYPE,
                   SAILING_SCHEDULE_NAME = CUR_DTL_UPD.SAILING_SCHEDULE_NAME,
		   REJECT_FURTHER_SHIPMENTS = CUR_DTL_UPD.REJECT_FURTHER_SHIPMENTS,
                   CARRIER_REJECT_PERIOD = CUR_DTL_UPD.CARRIER_REJECT_PERIOD 
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND lane_dtl_seq = vLaneDtlSeqUpd;
         END LOOP;
      END IF;

      RETURN tempCount;
   END MERGE_DRAFT;

   PROCEDURE CREATE_NEW_ROW (
      vTCCompanyId    IN company.COMPANY_ID%TYPE,
      vLaneId         IN comb_lane.LANE_ID%TYPE,
      vLaneDtlSeq     IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vEffectiveDT    IN comb_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT   IN comb_lane_dtl.EXPIRATION_DT%TYPE)
   IS
      vLaneDtlSeqNew   comb_lane_dtl.LANE_DTL_SEQ%TYPE;
   BEGIN
      SELECT MAX (LANE_DTL_SEQ) + 1
        INTO vLaneDtlSeqNew
        FROM COMB_LANE_DTL
       WHERE TC_COMPANY_ID = vTCCompanyID AND LANE_ID = vLaneID;

      INSERT INTO COMB_LANE_DTL (TC_COMPANY_ID,
                                 LANE_ID,
                                 LANE_DTL_SEQ,
                                 EFFECTIVE_DT,
                                 EXPIRATION_DT,
                                 CARRIER_ID,
                                 SCNDR_CARRIER_ID,
                                 MOT_ID,
                                 SERVICE_LEVEL_ID,
                                 FIXED_TRANSIT_VALUE,
                                 FIXED_TRANSIT_STANDARD_UOM,
                                 SAILING_SCHEDULE_NAME,
                                 SAILING_FREQUENCY_TYPE,
                                 LAST_UPDATED_SOURCE_TYPE,
                                 LAST_UPDATED_SOURCE,
                                 LAST_UPDATED_DTTM,
                                 LANE_DTL_STATUS,
                                 IS_SAILING,
                                 MASTER_LANE_ID,
                                 REP_TP_FLAG,
				 REJECT_FURTHER_SHIPMENTS,
                                 CARRIER_REJECT_PERIOD)
         SELECT TC_COMPANY_ID,
                LANE_ID,
                vLaneDtlSeqNew,
                vEffectiveDT,
                vExpirationDT,
                CARRIER_ID,
                SCNDR_CARRIER_ID,
                MOT_ID,
                SERVICE_LEVEL_ID,
                FIXED_TRANSIT_VALUE,
                FIXED_TRANSIT_STANDARD_UOM,
                SAILING_SCHEDULE_NAME,
                SAILING_FREQUENCY_TYPE,
                3,
                'OMS',
                GETDATE (),
                0,
                1,
                MASTER_LANE_ID,
                1,
		REJECT_FURTHER_SHIPMENTS,
                CARRIER_REJECT_PERIOD 
           FROM COMB_LANE_DTL
          WHERE     TC_COMPANY_ID = vTCCompanyId
                AND LANE_ID = vLaneId
                AND LANE_DTL_SEQ = vLaneDtlSeq;

      COPY_CHILD_ROW (vTCCompanyId,
                      vLaneId,
                      vLaneDtlSeq,
                      vLaneDtlSeqNew,
                      vLaneDtlSeqNew,
                      0,
                      0,
                      1);
   END CREATE_NEW_ROW;

   PROCEDURE REPLICATE_ROW (
      vTCCompanyId     IN company.COMPANY_ID%TYPE,
      vLaneId          IN comb_lane.LANE_ID%TYPE,
      vOverlappedSeq   IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vLaneDtlSeq      IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vEffectiveDT     IN comb_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT    IN comb_lane_dtl.EXPIRATION_DT%TYPE,
      vIsRating        IN comb_lane_dtl.IS_RATING%TYPE,
      vIsRouting       IN comb_lane_dtl.IS_ROUTING%TYPE,
      vIsSailing       IN comb_lane_dtl.IS_SAILING%TYPE)
   IS
      vLaneDtlSeqNew       comb_lane_dtl.LANE_DTL_SEQ%TYPE;
      vLaneDtlSeqTemp      comb_lane_dtl.LANE_DTL_SEQ%TYPE;
      vTransitUOMTemp      comb_lane_dtl.FIXED_TRANSIT_STANDARD_UOM%TYPE;
      vTransitValueTemp    comb_lane_dtl.FIXED_TRANSIT_VALUE%TYPE;
      vFrequencyTypeTemp   comb_lane_dtl.SAILING_FREQUENCY_TYPE%TYPE;
      vScheduleNameTemp    comb_lane_dtl.SAILING_SCHEDULE_NAME%TYPE;
   BEGIN
      SELECT MAX (LANE_DTL_SEQ) + 1
        INTO vLaneDtlSeqNew
        FROM COMB_LANE_DTL
       WHERE TC_COMPANY_ID = vTCCompanyID AND LANE_ID = vLaneID;

      IF (vOverlappedSeq = -1)
      THEN
         vLaneDtlSeqTemp := vLaneDtlSeq;
      ELSE
         vLaneDtlSeqTemp := vOverlappedSeq;
      END IF;

      SELECT FIXED_TRANSIT_STANDARD_UOM,
             FIXED_TRANSIT_VALUE,
             SAILING_FREQUENCY_TYPE,
             SAILING_SCHEDULE_NAME
        INTO vTransitUOMTemp,
             vTransitValueTemp,
             vFrequencyTypeTemp,
             vScheduleNameTemp
        FROM COMB_LANE_DTL
       WHERE     TC_COMPANY_ID = vTCCompanyId
             AND LANE_ID = vLaneId
             AND LANE_DTL_SEQ = vLaneDtlSeqTemp;

      INSERT INTO COMB_LANE_DTL (TC_COMPANY_ID,
                                 LANE_ID,
                                 LANE_DTL_SEQ,
                                 LANE_DTL_STATUS,
                                 IS_RATING,
                                 IS_ROUTING,
                                 IS_BUDGETED,
                                 IS_PREFERRED,
                                 REP_TP_FLAG,
                                 LAST_UPDATED_SOURCE_TYPE,
                                 LAST_UPDATED_SOURCE,
                                 LAST_UPDATED_DTTM,
                                 CARRIER_ID,
                                 MOT_ID,
                                 EQUIPMENT_ID,
                                 SERVICE_LEVEL_ID,
                                 SCNDR_CARRIER_ID,
                                 O_SHIP_VIA,
                                 D_SHIP_VIA,
                                 CONTRACT_NUMBER,
                                 CUSTOM_TEXT1,
                                 CUSTOM_TEXT2,
                                 CUSTOM_TEXT3,
                                 CUSTOM_TEXT4,
                                 CUSTOM_TEXT5,
                                 PROTECTION_LEVEL_ID,
                                 EFFECTIVE_DT,
                                 EXPIRATION_DT,
                                 TIER_ID,
                                 RANK,
                                 WEEKLY_CAPACITY,
                                 DAILY_CAPACITY,
                                 CAPACITY_SUN,
                                 CAPACITY_MON,
                                 CAPACITY_TUE,
                                 CAPACITY_WED,
                                 CAPACITY_THU,
                                 CAPACITY_FRI,
                                 CAPACITY_SAT,
                                 WEEKLY_COMMITMENT,
                                 DAILY_COMMITMENT,
                                 COMMITMENT_SUN,
                                 COMMITMENT_MON,
                                 COMMITMENT_TUE,
                                 COMMITMENT_WED,
                                 COMMITMENT_THU,
                                 COMMITMENT_FRI,
                                 COMMITMENT_SAT,
                                 WEEKLY_COMMIT_PCT,
                                 DAILY_COMMIT_PCT,
                                 COMMIT_PCT_SUN,
                                 COMMIT_PCT_MON,
                                 COMMIT_PCT_TUE,
                                 COMMIT_PCT_WED,
                                 COMMIT_PCT_THU,
                                 COMMIT_PCT_FRI,
                                 COMMIT_PCT_SAT,
                                 FIXED_TRANSIT_STANDARD_UOM,
                                 FIXED_TRANSIT_VALUE,
                                 IS_SAILING,
                                 SAILING_FREQUENCY_TYPE,
                                 SAILING_SCHEDULE_NAME,
                                 MASTER_LANE_ID,
                                 TT_TOLERANCE_FACTOR,
                                 SP_LPN_TYPE,
                                 SP_MIN_LPN_COUNT,
                                 SP_MAX_LPN_COUNT,
                                 SP_MIN_WEIGHT,
                                 SP_MAX_WEIGHT,
                                 SP_MIN_VOLUME,
                                 SP_MAX_VOLUME,
                                 SP_MIN_LINEAR_FEET,
                                 SP_MAX_LINEAR_FEET,
                                 SP_MIN_MONETARY_VALUE,
                                 SP_MAX_MONETARY_VALUE,
                                 SP_CURRENCY_CODE,
                                 HAS_SHIPPING_PARAM,
                                 CUBING_INDICATOR,
                                 OVERRIDE_CODE,
                                 PREFERENCE_BONUS_VALUE,
				 REJECT_FURTHER_SHIPMENTS,
                                 CARRIER_REJECT_PERIOD)
         SELECT TC_COMPANY_ID,
                LANE_ID,
                vLaneDtlSeqNew,
                0,
                IS_RATING,
                IS_ROUTING,
                IS_BUDGETED,
                IS_PREFERRED,
                REP_TP_FLAG,
                3,
                'OMS',
                GETDATE (),
                CARRIER_ID,
                MOT_ID,
                EQUIPMENT_ID,
                SERVICE_LEVEL_ID,
                SCNDR_CARRIER_ID,
                O_SHIP_VIA,
                D_SHIP_VIA,
                CONTRACT_NUMBER,
                CUSTOM_TEXT1,
                CUSTOM_TEXT2,
                CUSTOM_TEXT3,
                CUSTOM_TEXT4,
                CUSTOM_TEXT5,
                PROTECTION_LEVEL_ID,
                vEffectiveDT,
                vExpirationDT,
                TIER_ID,
                RANK,
                WEEKLY_CAPACITY,
                DAILY_CAPACITY,
                CAPACITY_SUN,
                CAPACITY_MON,
                CAPACITY_TUE,
                CAPACITY_WED,
                CAPACITY_THU,
                CAPACITY_FRI,
                CAPACITY_SAT,
                WEEKLY_COMMITMENT,
                DAILY_COMMITMENT,
                COMMITMENT_SUN,
                COMMITMENT_MON,
                COMMITMENT_TUE,
                COMMITMENT_WED,
                COMMITMENT_THU,
                COMMITMENT_FRI,
                COMMITMENT_SAT,
                WEEKLY_COMMIT_PCT,
                DAILY_COMMIT_PCT,
                COMMIT_PCT_SUN,
                COMMIT_PCT_MON,
                COMMIT_PCT_TUE,
                COMMIT_PCT_WED,
                COMMIT_PCT_THU,
                COMMIT_PCT_FRI,
                COMMIT_PCT_SAT,
                vTransitUOMTemp,
                vTransitValueTemp,
                vIsSailing,
                vFrequencyTypeTemp,
                vScheduleNameTemp,
                MASTER_LANE_ID,
                TT_TOLERANCE_FACTOR,
                SP_LPN_TYPE,
                SP_MIN_LPN_COUNT,
                SP_MAX_LPN_COUNT,
                SP_MIN_WEIGHT,
                SP_MAX_WEIGHT,
                SP_MIN_VOLUME,
                SP_MAX_VOLUME,
                SP_MIN_LINEAR_FEET,
                SP_MAX_LINEAR_FEET,
                SP_MIN_MONETARY_VALUE,
                SP_MAX_MONETARY_VALUE,
                SP_CURRENCY_CODE,
                HAS_SHIPPING_PARAM,
                CUBING_INDICATOR,
                OVERRIDE_CODE,
                PREFERENCE_BONUS_VALUE,
		REJECT_FURTHER_SHIPMENTS,
                CARRIER_REJECT_PERIOD 
           FROM COMB_LANE_DTL
          WHERE     TC_COMPANY_ID = vTCCompanyId
                AND LANE_ID = vLaneId
                AND LANE_DTL_SEQ = vLaneDtlSeq;

      COPY_CHILD_ROW (vTCCompanyId,
                      vLaneId,
                      vOverlappedSeq,
                      vLaneDtlSeq,
                      vLaneDtlSeqNew,
                      vIsRating,
                      vIsRouting,
                      vIsSailing);
   END REPLICATE_ROW;

   PROCEDURE DELETE_CHILD_ROW (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vLaneId        IN comb_lane.LANE_ID%TYPE,
      vLaneDtlSeq    IN comb_lane_dtl.LANE_DTL_SEQ%TYPE)
   IS
      vLaneDtlSeqNew   comb_lane_dtl.LANE_DTL_SEQ%TYPE;
   BEGIN
      DELETE FROM SAILING_SCHDL_BY_DATE
            WHERE     tc_company_id = vTCCompanyId
                  AND lane_id = vLaneId
                  AND lane_dtl_seq = vLaneDtlSeq;

      DELETE FROM SAILING_SCHDL_BY_WEEK_DAY
            WHERE     tc_company_id = vTCCompanyId
                  AND lane_id = vLaneId
                  AND lane_dtl_seq = vLaneDtlSeq;

      DELETE FROM SAILING_SCHDL_BY_WEEK
            WHERE     tc_company_id = vTCCompanyId
                  AND lane_id = vLaneId
                  AND lane_dtl_seq = vLaneDtlSeq;
   END DELETE_CHILD_ROW;

   PROCEDURE COPY_CHILD_ROW (
      vTCCompanyId     IN company.COMPANY_ID%TYPE,
      vLaneId          IN comb_lane.LANE_ID%TYPE,
      vOverlappedSeq   IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vLaneDtlSeq      IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vLaneDtlSeqNew   IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vIsRating        IN comb_lane_dtl.IS_RATING%TYPE,
      vIsRouting       IN comb_lane_dtl.IS_ROUTING%TYPE,
      vIsSailing       IN comb_lane_dtl.IS_SAILING%TYPE)
   IS
      vLaneDtlSeqTemp   comb_lane_dtl.LANE_DTL_SEQ%TYPE;
      week_day_sql      VARCHAR2 (2000);
      week_day_cursor   ref_curtype;
      vWeekId           SAILING_SCHDL_BY_WEEK.SAIL_SCHED_BY_WEEK_ID%TYPE;
   BEGIN
      IF (vIsRating = 1)
      THEN
         INSERT INTO rating_lane_dtl_rate (TC_COMPANY_ID,
                                           LANE_ID,
                                           RATING_LANE_DTL_SEQ,
                                           RLD_RATE_SEQ,
                                           MINIMUM_SIZE,
                                           MAXIMUM_SIZE,
                                           SIZE_UOM_ID,
                                           RATE_CALC_METHOD,
                                           RATE_UOM,
                                           RATE,
                                           MINIMUM_RATE,
                                           CURRENCY_CODE,
                                           TARIFF_CODE_ID,
                                           MIN_DISTANCE,
                                           MAX_DISTANCE,
                                           DISTANCE_UOM,
                                           SUPPORTS_MSTL,
                                           HAS_BH,
                                           HAS_RT,
                                           MIN_COMMODITY,
                                           MAX_COMMODITY,
                                           COMMODITY_CODE_ID,
                                           EXCESS_WT_RATE,
                                           PAYEE_CARRIER_ID,
                                           MAX_RANGE_COMMODITY_CODE_ID)
            SELECT TC_COMPANY_ID,
                   LANE_ID,
                   vLaneDtlSeqNew,
                   RLD_RATE_SEQ,
                   MINIMUM_SIZE,
                   MAXIMUM_SIZE,
                   SIZE_UOM_ID,
                   RATE_CALC_METHOD,
                   RATE_UOM,
                   RATE,
                   MINIMUM_RATE,
                   CURRENCY_CODE,
                   TARIFF_CODE_ID,
                   MIN_DISTANCE,
                   MAX_DISTANCE,
                   DISTANCE_UOM,
                   SUPPORTS_MSTL,
                   HAS_BH,
                   HAS_RT,
                   MIN_COMMODITY,
                   MAX_COMMODITY,
                   COMMODITY_CODE_ID,
                   EXCESS_WT_RATE,
                   PAYEE_CARRIER_ID,
                   MAX_RANGE_COMMODITY_CODE_ID
              FROM RATING_LANE_DTL_RATE
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vLaneDtlSeq;

         INSERT INTO LANE_ACCESSORIAL (TC_COMPANY_ID,
                                       LANE_ID,
                                       RATING_LANE_DTL_SEQ,
                                       ACCESSORIAL_ID,
                                       RATE,
                                       MINIMUM_RATE,
                                       CURRENCY_CODE,
                                       IS_AUTO_APPROVE,
                                       EFFECTIVE_DT,
                                       EXPIRATION_DT,
                                       LANE_ACCESSORIAL_ID,
                                       PAYEE_CARRIER_ID,
                                       MINIMUM_SIZE,
                                       MAXIMUM_SIZE,
                                       SIZE_UOM_ID,
                                       ZONE_ID,
                                       IS_SHIPMENT_ACCESSORIAL)
            SELECT TC_COMPANY_ID,
                   LANE_ID,
                   vLaneDtlSeqNew,
                   ACCESSORIAL_ID,
                   RATE,
                   MINIMUM_RATE,
                   CURRENCY_CODE,
                   IS_AUTO_APPROVE,
                   EFFECTIVE_DT,
                   EXPIRATION_DT,
                   LANE_ACCESSORIAL_ID_SEQ.NEXTVAL,
                   PAYEE_CARRIER_ID,
                   MINIMUM_SIZE,
                   MAXIMUM_SIZE,
                   SIZE_UOM_ID,
                   ZONE_ID,
                   IS_SHIPMENT_ACCESSORIAL
              FROM LANE_ACCESSORIAL
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vLaneId
                   AND rating_lane_dtl_seq = vLaneDtlSeq;
      END IF;

      IF (vIsRouting = 1)
      THEN
         INSERT INTO SURGE_CAPACITY (SURGE_CAPACITY_ID,
                                     TC_COMPANY_ID,
                                     LANE_ID,
                                     RG_LANE_DTL_SEQ,
                                     EFFECTIVE_DT,
                                     EXPIRATION_DT,
                                     WEEKLY_SURGE_CAPACITY,
                                     DAILY_SURGE_CAPACITY,
                                     SURGE_CAPACITY_SUN,
                                     SURGE_CAPACITY_MON,
                                     SURGE_CAPACITY_TUE,
                                     SURGE_CAPACITY_WED,
                                     SURGE_CAPACITY_THU,
                                     SURGE_CAPACITY_FRI,
                                     SURGE_CAPACITY_SAT)
            SELECT SEQ_SURGE_CAPACITY_ID.NEXTVAL,
                   TC_COMPANY_ID,
                   LANE_ID,
                   vLaneDtlSeqNew,
                   EFFECTIVE_DT,
                   EXPIRATION_DT,
                   WEEKLY_SURGE_CAPACITY,
                   DAILY_SURGE_CAPACITY,
                   SURGE_CAPACITY_SUN,
                   SURGE_CAPACITY_MON,
                   SURGE_CAPACITY_TUE,
                   SURGE_CAPACITY_WED,
                   SURGE_CAPACITY_THU,
                   SURGE_CAPACITY_FRI,
                   SURGE_CAPACITY_SAT
              FROM SURGE_CAPACITY
             WHERE     TC_COMPANY_ID = vTCCompanyId
                   AND LANE_ID = vLaneId
                   AND RG_LANE_DTL_SEQ = vLaneDtlSeq;
      END IF;

      IF (vIsSailing = 1)
      THEN
         IF (vOverlappedSeq = -1)
         THEN
            vLaneDtlSeqTemp := vLaneDtlSeq;
         ELSE
            vLaneDtlSeqTemp := vOverlappedSeq;
         END IF;

         INSERT INTO SAILING_SCHDL_BY_DATE (TC_COMPANY_ID,
                                            LANE_ID,
                                            LANE_DTL_SEQ,
                                            SAIL_SCHED_BY_DATE_ID,
                                            DEPARTURE_DTTM,
                                            ARRIVAL_DTTM,
                                            CUTOFF_DTTM,
                                            PICKUP_DTTM,
                                            TRANSIT_TIME_VALUE,
                                            TRANSIT_TIME_STANDARD_UOM,
                                            RESOURCE_REF_EXTERNAL,
                                            RESOURCE_NAME_EXTERNAL,
                                            COMMENTS,
                                            DESCRIPTION)
            SELECT TC_COMPANY_ID,
                   LANE_ID,
                   vLaneDtlSeqNew,
                   SEQ_SAIL_SCHED_BY_DATE_ID.NEXTVAL,
                   DEPARTURE_DTTM,
                   ARRIVAL_DTTM,
                   CUTOFF_DTTM,
                   PICKUP_DTTM,
                   TRANSIT_TIME_VALUE,
                   TRANSIT_TIME_STANDARD_UOM,
                   RESOURCE_REF_EXTERNAL,
                   RESOURCE_NAME_EXTERNAL,
                   COMMENTS,
                   DESCRIPTION
              FROM SAILING_SCHDL_BY_DATE
             WHERE     TC_COMPANY_ID = vTCCompanyId
                   AND LANE_ID = vLaneId
                   AND LANE_DTL_SEQ = vLaneDtlSeqTemp;

         week_day_sql :=
               ' SELECT SAIL_SCHED_BY_WEEK_ID '
            || ' FROM SAILING_SCHDL_BY_WEEK '
            || ' WHERE tc_company_id = :vTCCompanyId'
            || ' AND lane_id = :vLaneId'
            || ' AND lane_dtl_seq = :vLaneDtlSeqTemp';

         OPEN week_day_cursor FOR week_day_sql
            USING vTCCompanyId, vLaneId, vLaneDtlSeqTemp;

         LOOP
            FETCH week_day_cursor INTO vWeekId;

            EXIT WHEN week_day_cursor%NOTFOUND;

            -- select SEQ_SAIL_SCHED_BY_WEEK_ID.NEXTVAL into vWeekId from dual;

            INSERT INTO SAILING_SCHDL_BY_WEEK (TC_COMPANY_ID,
                                               LANE_ID,
                                               LANE_DTL_SEQ,
                                               SAIL_SCHED_BY_WEEK_ID,
                                               EFFECTIVE_DT,
                                               EXPIRATION_DT,
                                               RESOURCE_REF_EXTERNAL,
                                               RESOURCE_NAME_EXTERNAL,
                                               COMMENTS,
                                               DESCRIPTION)
               SELECT TC_COMPANY_ID,
                      LANE_ID,
                      vLaneDtlSeqNew,
                      SEQ_SAIL_SCHED_BY_WEEK_ID.NEXTVAL--        , vWeekId
                      ,
                      EFFECTIVE_DT,
                      EXPIRATION_DT,
                      RESOURCE_REF_EXTERNAL,
                      RESOURCE_NAME_EXTERNAL,
                      COMMENTS,
                      DESCRIPTION
                 FROM SAILING_SCHDL_BY_WEEK
                WHERE     TC_COMPANY_ID = vTCCompanyId
                      AND LANE_ID = vLaneId
                      AND LANE_DTL_SEQ = vLaneDtlSeqTemp
                      AND SAIL_SCHED_BY_WEEK_ID = vWeekId;

            INSERT INTO SAILING_SCHDL_BY_WEEK_DAY (TC_COMPANY_ID,
                                                   LANE_ID,
                                                   LANE_DTL_SEQ,
                                                   SAIL_SCHED_BY_WEEK_ID,
                                                   SAIL_SCHED_BY_WEEK_DAY_ID,
                                                   DEPARTURE_DAYOFWEEK,
                                                   DEPARTURE_TIME,
                                                   ARRIVAL_DAYOFWEEK,
                                                   ARRIVAL_TIME,
                                                   CUTOFF_DAYOFWEEK,
                                                   CUTOFF_TIME,
                                                   PICKUP_DAYOFWEEK,
                                                   PICKUP_TIME,
                                                   TRANSIT_TIME_VALUE,
                                                   TRANSIT_TIME_STANDARD_UOM,
                                                   TRANSIT_TIME_WEEK)
               SELECT TC_COMPANY_ID,
                      LANE_ID,
                      vLaneDtlSeqNew,
                      SEQ_SAIL_SCHED_BY_WEEK_ID.CURRVAL--        , vWeekId
                      ,
                      SEQ_SAIL_SCHED_BY_WEEK_DAY_ID.NEXTVAL,
                      DEPARTURE_DAYOFWEEK,
                      DEPARTURE_TIME,
                      ARRIVAL_DAYOFWEEK,
                      ARRIVAL_TIME,
                      CUTOFF_DAYOFWEEK,
                      CUTOFF_TIME,
                      PICKUP_DAYOFWEEK,
                      PICKUP_TIME,
                      TRANSIT_TIME_VALUE,
                      TRANSIT_TIME_STANDARD_UOM,
                      TRANSIT_TIME_WEEK
                 FROM SAILING_SCHDL_BY_WEEK_DAY
                WHERE     TC_COMPANY_ID = vTCCompanyId
                      AND LANE_ID = vLaneId
                      AND LANE_DTL_SEQ = vLaneDtlSeqTemp
                      AND SAIL_SCHED_BY_WEEK_ID = vWeekId;
         END LOOP;

         CLOSE week_day_cursor;
      END IF;
   END COPY_CHILD_ROW;

   PROCEDURE REPLICATE_LANE_DETAIL (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vLaneId        IN comb_lane.LANE_ID%TYPE,
      vLaneDtlSeq    IN comb_lane_dtl.LANE_DTL_SEQ%TYPE)
   IS
      vLaneDtlSeqNew   comb_lane_dtl.LANE_DTL_SEQ%TYPE;
   BEGIN
      SELECT MAX (LANE_DTL_SEQ) + 1
        INTO vLaneDtlSeqNew
        FROM COMB_LANE_DTL
       WHERE TC_COMPANY_ID = vTCCompanyID AND LANE_ID = vLaneID;

      INSERT INTO COMB_LANE_DTL (TC_COMPANY_ID,
                                 LANE_ID,
                                 LANE_DTL_SEQ,
                                 LANE_DTL_STATUS,
                                 IS_RATING,
                                 IS_ROUTING,
                                 IS_BUDGETED,
                                 IS_PREFERRED,
                                 REP_TP_FLAG,
                                 LAST_UPDATED_SOURCE_TYPE,
                                 LAST_UPDATED_SOURCE,
                                 LAST_UPDATED_DTTM,
                                 CARRIER_ID,
                                 MOT_ID,
                                 EQUIPMENT_ID,
                                 SERVICE_LEVEL_ID,
                                 SCNDR_CARRIER_ID,
                                 PROTECTION_LEVEL_ID,
                                 EFFECTIVE_DT,
                                 EXPIRATION_DT,
                                 TIER_ID,
                                 RANK,
                                 WEEKLY_CAPACITY,
                                 DAILY_CAPACITY,
                                 CAPACITY_SUN,
                                 CAPACITY_MON,
                                 CAPACITY_TUE,
                                 CAPACITY_WED,
                                 CAPACITY_THU,
                                 CAPACITY_FRI,
                                 CAPACITY_SAT,
                                 WEEKLY_COMMITMENT,
                                 DAILY_COMMITMENT,
                                 COMMITMENT_SUN,
                                 COMMITMENT_MON,
                                 COMMITMENT_TUE,
                                 COMMITMENT_WED,
                                 COMMITMENT_THU,
                                 COMMITMENT_FRI,
                                 COMMITMENT_SAT,
                                 WEEKLY_COMMIT_PCT,
                                 DAILY_COMMIT_PCT,
                                 COMMIT_PCT_SUN,
                                 COMMIT_PCT_MON,
                                 COMMIT_PCT_TUE,
                                 COMMIT_PCT_WED,
                                 COMMIT_PCT_THU,
                                 COMMIT_PCT_FRI,
                                 COMMIT_PCT_SAT,
                                 IS_SAILING,
                                 MASTER_LANE_ID,
                                 O_SHIP_VIA,
                                 D_SHIP_VIA,
                                 CONTRACT_NUMBER,
                                 CUSTOM_TEXT1,
                                 CUSTOM_TEXT2,
                                 CUSTOM_TEXT3,
                                 CUSTOM_TEXT4,
                                 CUSTOM_TEXT5,
                                 TT_TOLERANCE_FACTOR,
                                 SP_LPN_TYPE,
                                 SP_MIN_LPN_COUNT,
                                 SP_MAX_LPN_COUNT,
                                 SP_MIN_WEIGHT,
                                 SP_MAX_WEIGHT,
                                 SP_MIN_VOLUME,
                                 SP_MAX_VOLUME,
                                 SP_MIN_LINEAR_FEET,
                                 SP_MAX_LINEAR_FEET,
                                 SP_MIN_MONETARY_VALUE,
                                 SP_MAX_MONETARY_VALUE,
                                 SP_CURRENCY_CODE,
                                 HAS_SHIPPING_PARAM,
                                 CUBING_INDICATOR,
                                 OVERRIDE_CODE,
                                 PREFERENCE_BONUS_VALUE)
         SELECT TC_COMPANY_ID,
                LANE_ID,
                vLaneDtlSeqNew,
                0,
                IS_RATING,
                IS_ROUTING,
                IS_BUDGETED,
                IS_PREFERRED,
                REP_TP_FLAG,
                3,
                'OMS',
                GETDATE (),
                CARRIER_ID,
                MOT_ID,
                EQUIPMENT_ID,
                SERVICE_LEVEL_ID,
                SCNDR_CARRIER_ID,
                PROTECTION_LEVEL_ID,
                EFFECTIVE_DT,
                EXPIRATION_DT,
                TIER_ID,
                RANK,
                WEEKLY_CAPACITY,
                DAILY_CAPACITY,
                CAPACITY_SUN,
                CAPACITY_MON,
                CAPACITY_TUE,
                CAPACITY_WED,
                CAPACITY_THU,
                CAPACITY_FRI,
                CAPACITY_SAT,
                WEEKLY_COMMITMENT,
                DAILY_COMMITMENT,
                COMMITMENT_SUN,
                COMMITMENT_MON,
                COMMITMENT_TUE,
                COMMITMENT_WED,
                COMMITMENT_THU,
                COMMITMENT_FRI,
                COMMITMENT_SAT,
                WEEKLY_COMMIT_PCT,
                DAILY_COMMIT_PCT,
                COMMIT_PCT_SUN,
                COMMIT_PCT_MON,
                COMMIT_PCT_TUE,
                COMMIT_PCT_WED,
                COMMIT_PCT_THU,
                COMMIT_PCT_FRI,
                COMMIT_PCT_SAT,
                0,
                MASTER_LANE_ID,
                O_SHIP_VIA,
                D_SHIP_VIA,
                CONTRACT_NUMBER,
                CUSTOM_TEXT1,
                CUSTOM_TEXT2,
                CUSTOM_TEXT3,
                CUSTOM_TEXT4,
                CUSTOM_TEXT5,
                TT_TOLERANCE_FACTOR,
                SP_LPN_TYPE,
                SP_MIN_LPN_COUNT,
                SP_MAX_LPN_COUNT,
                SP_MIN_WEIGHT,
                SP_MAX_WEIGHT,
                SP_MIN_VOLUME,
                SP_MAX_VOLUME,
                SP_MIN_LINEAR_FEET,
                SP_MAX_LINEAR_FEET,
                SP_MIN_MONETARY_VALUE,
                SP_MAX_MONETARY_VALUE,
                SP_CURRENCY_CODE,
                HAS_SHIPPING_PARAM,
                CUBING_INDICATOR,
                OVERRIDE_CODE,
                PREFERENCE_BONUS_VALUE
           FROM COMB_LANE_DTL
          WHERE     TC_COMPANY_ID = vTCCompanyId
                AND LANE_ID = vLaneId
                AND LANE_DTL_SEQ = vLaneDtlSeq;

      COPY_CHILD_ROW (vTCCompanyId,
                      vLaneId,
                      -1,
                      vLaneDtlSeq,
                      vLaneDtlSeqNew,
                      1,
                      1,
                      0);
   END REPLICATE_LANE_DETAIL;

   PROCEDURE MERGE_DRAFT (
      vTCCompanyId     IN     company.COMPANY_ID%TYPE,
      vLaneId          IN     comb_lane.LANE_ID%TYPE,
      vLaneDtlSeq      IN     comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vLaneDtlSeqUpd   IN     comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      REC_CNT             OUT NUMBER)
   AS
   BEGIN
      REC_CNT :=
         MERGE_DRAFT (vTCCompanyId,
                      vLaneId,
                      vLaneDtlSeq,
                      vLaneDtlSeqUpd);
   END MERGE_DRAFT;

   PROCEDURE IMPORT_SAILING_LANES (
      vRootCompanyId      IN import_sailing_lane.TC_COMPANY_ID%TYPE,
      vTCCompanyId        IN import_sailing_lane.TC_COMPANY_ID%TYPE,
      vBusinessUnit       IN import_sailing_lane.BUSINESS_UNIT%TYPE,
      vOFacilityAliasId   IN import_sailing_lane.O_FACILITY_ALIAS_ID%TYPE,
      vOFacilityBU        IN import_sailing_lane.O_FACILITY_BU%TYPE,
      vOCity              IN import_sailing_lane.O_CITY%TYPE,
      vOStateProv         IN import_sailing_lane.O_STATE_PROV%TYPE,
      vOCounty            IN import_sailing_lane.O_COUNTY%TYPE,
      vOPostal            IN import_sailing_lane.O_POSTAL_CODE%TYPE,
      vOCountry           IN import_sailing_lane.O_COUNTRY_CODE%TYPE,
      vOZoneId            IN import_sailing_lane.O_ZONE_ID%TYPE,
      vOZoneName          IN import_sailing_lane.O_ZONE_NAME%TYPE,
      vDFacilityAliasId   IN import_sailing_lane.D_FACILITY_ALIAS_ID%TYPE,
      vDFacilityBU        IN import_sailing_lane.D_FACILITY_BU%TYPE,
      vDCity              IN import_sailing_lane.D_CITY%TYPE,
      vDStateProv         IN import_sailing_lane.D_STATE_PROV%TYPE,
      vDCounty            IN import_sailing_lane.D_COUNTY%TYPE,
      vDPostal            IN import_sailing_lane.D_POSTAL_CODE%TYPE,
      vDCountry           IN import_sailing_lane.D_COUNTRY_CODE%TYPE,
      vDZoneId            IN import_sailing_lane.D_ZONE_ID%TYPE,
      vDZoneName          IN import_sailing_lane.D_ZONE_NAME%TYPE,
      vBatchId            IN import_sailing_lane.BATCH_ID%TYPE,
      vLaneStatus         IN import_sailing_lane.LANE_STATUS%TYPE)
   AS
      vLaneId                 NUMBER (8);
      vImportLocationString   IMPORT_SAILING_LANE.IMPORT_LOCATION_STRING%TYPE;
   BEGIN
      vImportLocationString :=
         LANE_LOCATION_PKG.fnGetImportLocationString (vBusinessUnit,
                                                      vOFacilityAliasId,
                                                      vOCity,
                                                      vOStateProv,
                                                      vOCounty,
                                                      vOPostal,
                                                      vOCountry,
                                                      vOZoneId,
                                                      vDFacilityAliasId,
                                                      vDCity,
                                                      vDStateProv,
                                                      vDCounty,
                                                      vDPostal,
                                                      vDCountry,
                                                      vDZoneId);

      SELECT LANE_ID
        INTO vLaneId
        FROM IMPORT_SAILING_LANE
       WHERE     TC_COMPANY_ID = vTCCompanyId
             AND IMPORT_LOCATION_STRING = vImportLocationString
             AND LANE_STATUS = 1;

      UPDATE IMPORT_SAILING_LANE
         SET BATCH_ID = vBatchId, LANE_STATUS = vLaneStatus
       WHERE LANE_ID = vLaneId AND TC_COMPANY_ID = vTCCompanyId;

      COMMIT;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         INSERT INTO IMPORT_SAILING_LANE (ROOTCOMPANYID,
                                          TC_COMPANY_ID,
                                          LANE_ID,
                                          BUSINESS_UNIT,
                                          O_FACILITY_ALIAS_ID,
                                          O_FACILITY_BU,
                                          O_CITY,
                                          O_STATE_PROV,
                                          O_COUNTY,
                                          O_POSTAL_CODE,
                                          O_COUNTRY_CODE,
                                          O_ZONE_ID,
                                          O_ZONE_NAME,
                                          D_FACILITY_ALIAS_ID,
                                          D_FACILITY_BU,
                                          D_CITY,
                                          D_STATE_PROV,
                                          D_COUNTY,
                                          D_POSTAL_CODE,
                                          D_COUNTRY_CODE,
                                          D_ZONE_ID,
                                          D_ZONE_NAME,
                                          BATCH_ID,
                                          SAILING_LANE_ID,
                                          LANE_STATUS,
                                          IMPORT_LOCATION_STRING)
              VALUES (vRootCompanyId,
                      vTCCompanyId,
                      SEQ_IMP_SAILING_LANE_ID.NEXTVAL,
                      vBusinessUnit,
                      vOFacilityAliasId,
                      vOFacilityBU,
                      vOCity,
                      vOStateProv,
                      vOCounty,
                      vOPostal,
                      vOCountry,
                      vOZoneId,
                      vOZoneName,
                      vDFacilityAliasId,
                      vDFacilityBU,
                      vDCity,
                      vDStateProv,
                      vDCounty,
                      vDPostal,
                      vDCountry,
                      vDZoneId,
                      vDZoneName,
                      vBatchId,
                      vBatchId,
                      vLaneStatus,
                      vImportLocationString);

         COMMIT;
   END IMPORT_SAILING_LANES;

   PROCEDURE IMPORT_SAILING_LANE_DETAILS (
      vTCCompanyId              IN import_sailing_lane.TC_COMPANY_ID%TYPE,
      vTPCode                   IN import_sailing_lane_dtl.CARRIER_CODE%TYPE,
      vTPCodeBU                 IN import_sailing_lane_dtl.TP_CODE_BU%TYPE,
      vBatchId                  IN import_sailing_lane.BATCH_ID%TYPE,
      vSailingLaneDtlSeq        IN import_sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE,
      vMot                      IN import_sailing_lane_dtl.MOT%TYPE,
      vMotBU                    IN import_sailing_lane_dtl.MOT_BU%TYPE,
      vServiceLevel             IN import_sailing_lane_dtl.SERVICE_LEVEL%TYPE,
      vServiceLevelBU           IN import_sailing_lane_dtl.SERVICE_LEVEL_BU%TYPE,
      vEffectiveDt              IN import_sailing_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDt             IN import_sailing_lane_dtl.EXPIRATION_DT%TYPE,
      vFixedTransitValue        IN import_sailing_lane_dtl.FIXED_TRANSIT_VALUE%TYPE,
      vFixedTransitUOM          IN import_sailing_lane_dtl.FIXED_TRANSIT_STANDARD_UOM%TYPE,
      vSecondaryCarrierCode     IN import_sailing_lane_dtl.SCNDR_CARRIER_CODE%TYPE,
      vSecondaryCarrierCodeBU   IN import_sailing_lane_dtl.SCNDR_CARRIER_CODE_BU%TYPE,
      vSailingScheduleName      IN import_sailing_lane_dtl.SAILING_SCHEDULE_NAME%TYPE,
      vSailingFrequencyType     IN import_sailing_lane_dtl.SAILING_FREQUENCY_TYPE%TYPE)
   IS
      vLaneId   NUMBER;
   BEGIN
      SELECT LANE_ID
        INTO vLaneId
        FROM IMPORT_SAILING_LANE
       WHERE TC_COMPANY_ID = vTCCompanyId AND BATCH_ID = vBatchID;

      /*    EXCEPTION

              WHEN NO_DATA_FOUND THEN
              DBMS_OUTPUT.PUT_LINE('Calling INSERT_SAILING_LANE....');*/

      INSERT INTO IMPORT_SAILING_LANE_DTL (TC_COMPANY_ID,
                                           CARRIER_CODE,
                                           TP_CODE_BU,
                                           LANE_ID,
                                           SAILING_LANE_DTL_SEQ,
                                           MOT,
                                           MOT_BU,
                                           SERVICE_LEVEL,
                                           SERVICE_LEVEL_BU,
                                           EFFECTIVE_DT,
                                           EXPIRATION_DT,
                                           FIXED_TRANSIT_VALUE,
                                           FIXED_TRANSIT_STANDARD_UOM,
                                           SCNDR_CARRIER_CODE,
                                           SCNDR_CARRIER_CODE_BU,
                                           SAILING_SCHEDULE_NAME,
                                           SAILING_FREQUENCY_TYPE,
                                           LANE_DTL_STATUS)
           VALUES (vTCCompanyId,
                   vTPCode,
                   vTPCodeBU,
                   vLaneId,
                   vSailingLaneDtlSeq,
                   vMot,
                   vMotBU,
                   vServiceLevel,
                   vServiceLevelBU,
                   vEffectiveDt,
                   vExpirationDt,
                   vFixedTransitValue,
                   vFixedTransitUOM,
                   vSecondaryCarrierCode,
                   vSecondaryCarrierCodeBU,
                   vSailingScheduleName,
                   vSailingFrequencyType,
                   3);

      COMMIT;
   END IMPORT_SAILING_LANE_DETAILS;

   PROCEDURE IMPORT_SAILING_SCHDLS_BY_DATE (
      vTCCompanyId          IN import_sailing_lane.TC_COMPANY_ID%TYPE,
      vBatchId              IN import_sailing_lane.BATCH_ID%TYPE,
      vLaneDtlSeq           IN import_sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE,
      vSailSchedByDateSeq   IN import_sailing_schdl_by_date.SAIL_SCHED_BY_DATE_ID%TYPE,
      vVesselName           IN import_sailing_schdl_by_date.RESOURCE_REF_EXTERNAL%TYPE,
      vVoyageNumber         IN import_sailing_schdl_by_date.RESOURCE_NAME_EXTERNAL%TYPE,
      vCutoffDttm           IN import_sailing_schdl_by_date.CUTOFF_DTTM%TYPE,
      vArrivalDttm          IN import_sailing_schdl_by_date.ARRIVAL_DTTM%TYPE,
      vDepartureDttm        IN import_sailing_schdl_by_date.DEPARTURE_DTTM%TYPE,
      vAvailableDttm        IN import_sailing_schdl_by_date.PICKUP_DTTM%TYPE,
      vTransitTime          IN import_sailing_schdl_by_date.TRANSIT_TIME_VALUE%TYPE,
      vTransitTimeUOM       IN import_sailing_schdl_by_date.TRANSIT_TIME_STANDARD_UOM%TYPE,
      vComments             IN import_sailing_schdl_by_date.COMMENTS%TYPE,
      vRestrictions         IN import_sailing_schdl_by_date.DESCRIPTION%TYPE)
   IS
      vLaneId   NUMBER;
   BEGIN
      SELECT LANE_ID
        INTO vLaneId
        FROM IMPORT_SAILING_LANE
       WHERE TC_COMPANY_ID = vTCCompanyId AND BATCH_ID = vBatchID;

      INSERT INTO IMPORT_SAILING_SCHDL_BY_DATE (TC_COMPANY_ID,
                                                LANE_ID,
                                                LANE_DTL_SEQ,
                                                SAIL_SCHED_BY_DATE_ID,
                                                RESOURCE_REF_EXTERNAL,
                                                RESOURCE_NAME_EXTERNAL,
                                                CUTOFF_DTTM,
                                                ARRIVAL_DTTM,
                                                DEPARTURE_DTTM,
                                                PICKUP_DTTM,
                                                TRANSIT_TIME_VALUE,
                                                TRANSIT_TIME_STANDARD_UOM,
                                                COMMENTS,
                                                DESCRIPTION)
           VALUES (vTCCompanyId,
                   vLaneId,
                   vLaneDtlSeq,
                   vSailSchedByDateSeq,
                   vVoyageNumber,
                   vVesselName,
                   vCutoffDttm,
                   vArrivalDttm,
                   vDepartureDttm,
                   vAvailableDttm,
                   vTransitTime,
                   vTransitTimeUOM,
                   vComments,
                   vRestrictions);

      COMMIT;
   END IMPORT_SAILING_SCHDLS_BY_DATE;

   PROCEDURE IMPORT_SAILING_SCHDLS_BY_WK (
      vTCCompanyId          IN import_sailing_lane.TC_COMPANY_ID%TYPE,
      vBatchId              IN import_sailing_lane.BATCH_ID%TYPE,
      vLaneDtlSeq           IN import_sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE,
      vSailSchedByWeekSeq   IN import_sailing_schdl_by_wk.SAIL_SCHED_BY_WEEK_ID%TYPE,
      vVesselName           IN import_sailing_schdl_by_wk.RESOURCE_REF_EXTERNAL%TYPE,
      vVoyageNumber         IN import_sailing_schdl_by_wk.RESOURCE_NAME_EXTERNAL%TYPE,
      vComments             IN import_sailing_schdl_by_wk.COMMENTS%TYPE,
      vRestrictions         IN import_sailing_schdl_by_wk.DESCRIPTION%TYPE,
      vEffectiveDt          IN import_sailing_schdl_by_wk.EFFECTIVE_DT%TYPE,
      vExpirationDt         IN import_sailing_schdl_by_wk.EXPIRATION_DT%TYPE)
   IS
      vLaneId   NUMBER;
   BEGIN
      SELECT LANE_ID
        INTO vLaneId
        FROM IMPORT_SAILING_LANE
       WHERE TC_COMPANY_ID = vTCCompanyId AND BATCH_ID = vBatchID;

      INSERT INTO IMPORT_SAILING_SCHDL_BY_WK (TC_COMPANY_ID,
                                              LANE_ID,
                                              SAILING_LANE_DTL_SEQ,
                                              SAIL_SCHED_BY_WEEK_ID,
                                              RESOURCE_REF_EXTERNAL,
                                              RESOURCE_NAME_EXTERNAL,
                                              COMMENTS,
                                              DESCRIPTION,
                                              EFFECTIVE_DT,
                                              EXPIRATION_DT)
           VALUES (vTCCompanyId,
                   vLaneId,
                   vLaneDtlSeq,
                   vSailSchedByWeekSeq,
                   vVoyageNumber,
                   vVesselName,
                   vComments,
                   vRestrictions,
                   vEffectiveDt,
                   vExpirationDt);

      COMMIT;
   END IMPORT_SAILING_SCHDLS_BY_WK;

   PROCEDURE IMPORT_SAILING_SCHDLS_BY_DAY (
      vTCCompanyId            IN import_sailing_lane.TC_COMPANY_ID%TYPE,
      vBatchId                IN import_sailing_lane.BATCH_ID%TYPE,
      vLaneDtlSeq             IN import_sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE,
      vSailSchedByWeekSeq     IN import_sailing_schdl_by_wk.SAIL_SCHED_BY_WEEK_ID%TYPE,
      vSailSchedByWeekDayId   IN import_sailing_schdl_wk_day.SAIL_SCHED_BY_WEEK_DAY_ID%TYPE,
      vCutoffTime             IN import_sailing_schdl_wk_day.CUTOFF_TIME%TYPE,
      vArrivalTime            IN import_sailing_schdl_wk_day.ARRIVAL_TIME%TYPE,
      vDepartureTime          IN import_sailing_schdl_wk_day.DEPARTURE_TIME%TYPE,
      vAvailableTime          IN import_sailing_schdl_wk_day.PICKUP_TIME%TYPE,
      vCuttOffDay             IN import_sailing_schdl_wk_day.CUTOFF_DAYOFWEEK%TYPE,
      vArrivalDay             IN import_sailing_schdl_wk_day.ARRIVAL_DAYOFWEEK%TYPE,
      vDepartureDay           IN import_sailing_schdl_wk_day.DEPARTURE_DAYOFWEEK%TYPE,
      vAvailableDay           IN import_sailing_schdl_wk_day.PICKUP_DAYOFWEEK%TYPE,
      vTransitTime            IN import_sailing_schdl_wk_day.TRANSIT_TIME_VALUE%TYPE,
      vTransitTimeUOM         IN import_sailing_schdl_wk_day.TRANSIT_TIME_STANDARD_UOM%TYPE,
      vDuration               IN import_sailing_schdl_wk_day.DURATION%TYPE)
   IS
      vLaneId   NUMBER;
   BEGIN
      SELECT LANE_ID
        INTO vLaneId
        FROM IMPORT_SAILING_LANE
       WHERE TC_COMPANY_ID = vTCCompanyId AND BATCH_ID = vBatchID;

      INSERT INTO IMPORT_SAILING_SCHDL_WK_DAY (TC_COMPANY_ID,
                                               LANE_ID,
                                               SAILING_LANE_DTL_SEQ,
                                               SAIL_SCHED_BY_WEEK_ID,
                                               SAIL_SCHED_BY_WEEK_DAY_ID,
                                               CUTOFF_TIME,
                                               ARRIVAL_TIME,
                                               DEPARTURE_TIME,
                                               PICKUP_TIME,
                                               CUTOFF_DAYOFWEEK,
                                               ARRIVAL_DAYOFWEEK,
                                               DEPARTURE_DAYOFWEEK,
                                               PICKUP_DAYOFWEEK,
                                               TRANSIT_TIME_VALUE,
                                               TRANSIT_TIME_STANDARD_UOM,
                                               DURATION)
           VALUES (vTCCompanyId,
                   vLaneId,
                   vLaneDtlSeq,
                   vSailSchedByWeekSeq,
                   vSailSchedByWeekDayId,
                   vCutoffTime,
                   vArrivalTime,
                   vDepartureTime,
                   vAvailableTime,
                   vCuttOffDay,
                   vArrivalDay,
                   vDepartureDay,
                   vAvailableDay,
                   vTransitTime,
                   vTransitTimeUOM,
                   vDuration);

      COMMIT;
   END IMPORT_SAILING_SCHDLS_BY_DAY;

   PROCEDURE TRANSFER_IMPORT_SAILING_LANES (
      vTCCompanyId    IN company.COMPANY_ID%TYPE,
      vFirstBatchId   IN sailing_lane.LANE_ID%TYPE,
      vLastBatchId    IN sailing_lane.LANE_ID%TYPE)
   AS
      vBusinessUnit       BUSINESS_UNIT.BUSINESS_UNIT%TYPE;
      vOLocType           sailing_lane.O_LOC_TYPE%TYPE;
      vLaneHierarchy      sailing_lane.LANE_HIERARCHY%TYPE;
      vOFacilityId        sailing_lane.O_FACILITY_ID%TYPE;
      vOFacilityAliasId   sailing_lane.O_FACILITY_ALIAS_ID%TYPE;
      vOFacilityBU        import_sailing_lane.O_FACILITY_BU%TYPE;
      vOCity              sailing_lane.O_CITY%TYPE;
      vOStateProv         sailing_lane.O_STATE_PROV%TYPE;
      vOCounty            sailing_lane.O_COUNTY%TYPE;
      vOPostal            sailing_lane.O_POSTAL_CODE%TYPE;
      vOCountry           sailing_lane.O_COUNTRY_CODE%TYPE;
      vOZone              sailing_lane.O_ZONE_ID%TYPE;
      vDLocType           sailing_lane.D_LOC_TYPE%TYPE;
      vDFacilityId        sailing_lane.D_FACILITY_ID%TYPE;
      vDFacilityAliasId   sailing_lane.D_FACILITY_ALIAS_ID%TYPE;
      vDFacilityBU        import_sailing_lane.D_FACILITY_BU%TYPE;
      vDCity              sailing_lane.D_CITY%TYPE;
      vDStateProv         sailing_lane.D_STATE_PROV%TYPE;
      vDCounty            sailing_lane.D_COUNTY%TYPE;
      vDPostal            sailing_lane.D_POSTAL_CODE%TYPE;
      vDCountry           sailing_lane.D_COUNTRY_CODE%TYPE;
      vDZone              sailing_lane.D_ZONE_ID%TYPE;
      vImportLaneId       sailing_lane.LANE_ID%TYPE;
      vLaneID             sailing_lane.LANE_ID%TYPE;
      vPassFail           NUMBER;
      vSailingLaneId      SAILING_LANE.LANE_ID%TYPE;
      vBatchId            import_sailing_lane.batch_id%TYPE;
      vOZone1             sailing_lane.O_ZONE_ID%TYPE;
      vDZone1             sailing_lane.D_ZONE_ID%TYPE;

      CURSOR TRANSFER_LANE_CURSOR
      IS
         SELECT lane_id,
                /*     business_unit, */
                lane_hierarchy,
                o_loc_type,
                o_facility_id,
                o_facility_alias_id,
                o_facility_bu,
                o_city,
                o_state_prov,
                o_county,
                o_postal_code,
                o_country_code,
                o_zone_id,
                d_loc_type,
                d_facility_id,
                d_facility_alias_id,
                d_facility_bu,
                d_city,
                d_state_prov,
                d_county,
                d_postal_code,
                d_country_code,
                d_zone_id,
                sailing_lane_id,
                batch_id
           FROM IMPORT_SAILING_LANE isl
          WHERE     LANE_STATUS = 1
                AND tc_company_id = vTCCompanyId
                AND batch_id >= vFirstBatchId
                AND batch_id <= vLastBatchId;
   BEGIN
      OPEN TRANSFER_LANE_CURSOR;

     <<TRANSFER_LANE_LOOP>>
      LOOP
         FETCH TRANSFER_LANE_CURSOR
         INTO vImportLaneId,
              /*    vBusinessUnit,*/
              vLaneHierarchy,
              vOLocType,
              vOFacilityId,
              vOFacilityAliasId,
              vOFacilityBU,
              vOCity,
              vOStateProv,
              vOCounty,
              vOPostal,
              vOCountry,
              vOZone,
              vDLocType,
              vDFacilityId,
              vDFacilityAliasId,
              vDFacilityBU,
              vDCity,
              vDStateProv,
              vDCounty,
              vDPostal,
              vDCountry,
              vDZone,
              vSailingLaneId,
              vBatchId;

         DBMS_OUTPUT.PUT_LINE (
            'SAILING_LANE_ID: ' || TO_CHAR (vSailingLaneId));

         EXIT TRANSFER_LANE_LOOP WHEN TRANSFER_LANE_CURSOR%NOTFOUND;
         vOZone1 := NULL;
         vDZone1 := NULL;

         IF (vOZone IS NOT NULL AND LENGTH (TRIM (vOZone)) > 0)
         THEN
            vOZone1 := TO_NUMBER (vOZone);
         END IF;

         IF (vDZone IS NOT NULL AND LENGTH (TRIM (vDZone)) > 0)
         THEN
            vDZone1 := TO_NUMBER (vDZone);
         END IF;

         DBMS_OUTPUT.PUT_LINE ('Calling INSERT_SAILING_LANE....');

         vLaneId :=
            INSERT_SAILING_LANE (vTCCompanyId,
                                 vImportLaneId,
                                 /*   vBusinessUnit, */
                                 vLaneHierarchy,
                                 vOLocType,
                                 vOFacilityId,
                                 vOFacilityAliasId,
                                 vOFacilityBU,
                                 vOCity,
                                 vOStateProv,
                                 vOCounty,
                                 vOPostal,
                                 vOCountry,
                                 vOZone1,
                                 vDLocType,
                                 vDFacilityId,
                                 vDFacilityAliasId,
                                 vDFacilityBU,
                                 vDCity,
                                 vDStateProv,
                                 vDCounty,
                                 vDPostal,
                                 vDCountry,
                                 vDZone1,
                                 vSailingLaneId,
                                 vBatchId);

         IF (vLaneId <> -999)
         THEN
            DBMS_OUTPUT.PUT_LINE (
               'LANE_ID INSERTED IN SAILING_LANE TABLE ' || TO_CHAR (vLaneId));

            UPDATE IMPORT_SAILING_LANE
               SET SAILING_LANE_ID = vLaneId
             WHERE LANE_ID = vImportLaneID;

            COMMIT;
			
			VALIDATE_LANE_DTL_EPI(vTCCompanyId,
                                           vImportLaneID,
                                           vLaneId);

            TRANSFER_IMP_SAILING_LANE_DTL (vTCCompanyId,
                                           vImportLaneID,
                                           vLaneId);

            vPassFail :=
               DELETE_IMPORT_SAILING_LANES (vTCCompanyId, vImportLaneID);
         END IF;
      END LOOP TRANSFER_LANE_LOOP;

      CLOSE TRANSFER_LANE_CURSOR;
   END TRANSFER_IMPORT_SAILING_LANES;

   FUNCTION DELETE_IMPORT_SAILING_LANES (
      vTCCompanyId    IN company.COMPANY_ID%TYPE,
      vImportLaneID   IN import_sailing_lane.LANE_ID%TYPE)
      RETURN NUMBER
   IS
   BEGIN
      DELETE FROM IMPORT_SAILING_LANE
            WHERE TC_COMPANY_ID = vTCCompanyId AND LANE_ID = vImportLaneID
                  AND NOT EXISTS
                             (SELECT 1
                                FROM IMPORT_SAILING_LANE_DTL
                               WHERE TC_COMPANY_ID = vTCCompanyId
                                     AND LANE_ID = vImportLaneID);

      COMMIT;
      RETURN 0;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Procedure DELETE_IMP_SAILING_LANES: ' || SQLERRM);

         RETURN 1;
   END DELETE_IMPORT_SAILING_LANES;

   -----------------------------------------------------------------------------------

   FUNCTION INSERT_SAILING_LANE (
      vTCCompanyId      IN company.COMPANY_ID%TYPE,
      vImportLaneId     IN import_sailing_lane.LANE_ID%TYPE,
      /* vBusinessUnit    IN business_unit.BUSINESS_UNIT%TYPE, */
      vLaneHierarchy    IN sailing_lane.LANE_HIERARCHY%TYPE,
      vOLocType         IN sailing_lane.O_LOC_TYPE%TYPE,
      vOFacilityId      IN facility.FACILITY_ID%TYPE,
      vOFacilityAlias   IN facility_alias.FACILITY_ALIAS_ID%TYPE,
      vOFacilityBU      IN import_sailing_lane.O_FACILITY_BU%TYPE,
      vOCity            IN sailing_lane.O_CITY%TYPE,
      vOStateProv       IN state_prov.STATE_PROV%TYPE,
      vOCounty          IN sailing_lane.O_COUNTY%TYPE,
      vOPostal          IN postal_code.POSTAL_CODE%TYPE,
      vOCountry         IN country.COUNTRY_CODE%TYPE,
      vOZone            IN zone.ZONE_ID%TYPE,
      vDLocType         IN sailing_lane.D_LOC_TYPE%TYPE,
      vDFacilityId      IN facility.FACILITY_ID%TYPE,
      vDFacilityAlias   IN facility_alias.FACILITY_ALIAS_ID%TYPE,
      vDFacilityBU      IN import_sailing_lane.D_FACILITY_BU%TYPE,
      vDCity            IN sailing_lane.D_CITY%TYPE,
      vDStateProv       IN state_prov.STATE_PROV%TYPE,
      vDCounty          IN sailing_lane.D_COUNTY%TYPE,
      vDPostal          IN postal_code.POSTAL_CODE%TYPE,
      vDCountry         IN country.COUNTRY_CODE%TYPE,
      vDZone            IN zone.ZONE_ID%TYPE,
      vSailingLaneId    IN sailing_lane.lane_id%TYPE,
      vBatchId          IN import_sailing_lane.BATCH_ID%TYPE)
      RETURN NUMBER
   IS
      vRealLaneId              sailing_lane.lane_id%TYPE;
      l_real_lane_is_sailing   comb_lane.is_sailing%TYPE;
      vOFacilityBUID           company.COMPANY_ID%TYPE;
      vDFacilityBUID           company.COMPANY_ID%TYPE;
      vRootCompanyId           company.COMPANY_ID%TYPE;

      vWhereClause             VARCHAR2 (2000);
      vsqlStatement            VARCHAR2 (2000);
      vCount                   NUMBER;
   BEGIN
      SELECT COUNT (ROOTCOMPANYID)
        INTO vCount
        FROM IMPORT_SAILING_LANE
       WHERE LANE_ID = vImportLaneId;

      IF (vCount > 0)
      THEN
         SELECT ROOTCOMPANYID
           INTO vRootCompanyId
           FROM IMPORT_SAILING_LANE
          WHERE LANE_ID = vImportLaneId;
      END IF;

      IF (vOFacilityBU IS NULL)
      THEN
         vOFacilityBUID := vRootCompanyId;
      ELSE
         vOFacilityBUID :=
            GET_BUID_FROM_BUSINESS_UNIT (vRootCompanyId, vOFacilityBU);
      END IF;

      IF (vDFacilityBU IS NULL)
      THEN
         vDFacilityBUID := vRootCompanyId;
      ELSE
         vDFacilityBUID :=
            GET_BUID_FROM_BUSINESS_UNIT (vRootCompanyId, vDFacilityBU);
      END IF;

      vsqlStatement :=
         '/* INSERT_SAILING_LANE - SEARCH FOR EXISTING SAILING LANE */ ';
      vsqlStatement := vsqlStatement || 'SELECT lane_id,  is_sailing FROM ( ';
      vsqlStatement :=
         vsqlStatement || 'SELECT lane_id,  is_sailing FROM COMB_LANE';
      vWhereClause := ' WHERE tc_company_id = ' || vTCCompanyId;
      vWhereClause := vWhereClause || ' and lane_status = 0 ';
      /*  vWhereClause := vWhereClause || ' and business_unit ';
        IF(vBusinessUnit IS NULL) THEN
         vWhereClause := vWhereClause || 'is null';
           ELSE
         vWhereClause := vWhereClause || '= ''' || vBusinessUnit || '''';
        END IF; */
      vWhereClause := vWhereClause || ' and o_facility_id ';

      IF (vOFacilityId IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause :=
            vWhereClause
            || '= ( select facility_id from facility_alias where tc_company_id = '
            || vOFacilityBUID
            || ' ';
         vWhereClause :=
               vWhereClause
            || 'and facility_alias_id = '''
            || vOFacilityAlias
            || ''' )';
      END IF;

      vWhereClause := vWhereClause || ' and o_city ';

      IF (vOCity IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause :=
            vWhereClause || '= ''' || REPLACE (vOCity, '''', '''''') || '''';
      END IF;

      vWhereClause := vWhereClause || ' and o_state_prov ';

      IF (vOStateProv IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vOStateProv || '''';
      END IF;

      vWhereClause := vWhereClause || ' and o_county ';

      IF (vOCounty IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vOCounty || '''';
      END IF;

      vWhereClause := vWhereClause || ' and o_postal_code ';

      IF (vOPostal IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vOPostal || '''';
      END IF;

      vWhereClause := vWhereClause || ' and o_country_code ';

      IF (vOCountry IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vOCountry || '''';
      END IF;

      vWhereClause := vWhereClause || ' and o_zone_id ';

      IF (vOZone IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vOZone || '''';
      END IF;

      vWhereClause := vWhereClause || ' and d_facility_id ';

      IF (vDFacilityId IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause :=
            vWhereClause
            || '= ( select facility_id from facility_alias where tc_company_id = '
            || vDFacilityBUID
            || ' ';
         vWhereClause :=
               vWhereClause
            || 'and facility_alias_id = '''
            || vDFacilityAlias
            || ''' )';
      END IF;

      vWhereClause := vWhereClause || ' and d_city ';

      IF (vDCity IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause :=
            vWhereClause || '= ''' || REPLACE (vDCity, '''', '''''') || '''';
      END IF;

      vWhereClause := vWhereClause || ' and d_state_prov ';

      IF (vDStateProv IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vDStateProv || '''';
      END IF;

      vWhereClause := vWhereClause || ' and d_county ';

      IF (vDCounty IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vDCounty || '''';
      END IF;

      vWhereClause := vWhereClause || ' and d_postal_code ';

      IF (vDPostal IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vDPostal || '''';
      END IF;

      vWhereClause := vWhereClause || ' and d_country_code ';

      IF (vDCountry IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vDCountry || '''';
      END IF;

      vWhereClause := vWhereClause || ' and d_zone_id ';

      IF (vDZone IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vDZone || '''';
      END IF;

      vWhereClause := vWhereClause || ' and lane_status = 0 ';

      vsqlStatement := vsqlStatement || vWhereClause;
      vsqlStatement :=
         vsqlStatement
         || 'ORDER BY IS_SAILING desc, is_rating desc, is_ROUTING desc ) ';
      vsqlStatement := vsqlStatement || 'where rownum = 1 ';

      EXECUTE IMMEDIATE vsqlStatement
         INTO vRealLaneId, l_real_lane_is_sailing;

      UPDATE COMB_LANE
         SET IS_SAILING = 1
       WHERE     TC_COMPANY_ID = vTCCompanyId
             AND LANE_ID = vRealLaneId
             AND IS_SAILING = 0;

      COMMIT;

      RETURN vRealLaneId;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         BEGIN
            DBMS_OUTPUT.PUT_LINE (
               'INSERTING NEW RECORD INTO SAILING_LANE TABLE');

            DBMS_OUTPUT.PUT_LINE (
                  'SAILING LANE: - COMPANY_ID: '
               || TO_CHAR (vTCCompanyId)
               || ' Lane Id: '
               || TO_CHAR (vSailingLaneId));

            INSERT INTO COMB_LANE (TC_COMPANY_ID,
                                   LANE_ID,
                                   IS_SAILING,
                                   LANE_HIERARCHY,
                                   LANE_STATUS,
                                   O_LOC_TYPE,
                                   O_FACILITY_ID,
                                   O_FACILITY_ALIAS_ID,
                                   O_CITY,
                                   O_STATE_PROV,
                                   O_COUNTY,
                                   O_POSTAL_CODE,
                                   O_COUNTRY_CODE,
                                   O_ZONE_ID,
                                   D_LOC_TYPE,
                                   D_FACILITY_ID,
                                   D_FACILITY_ALIAS_ID,
                                   D_CITY,
                                   D_STATE_PROV,
                                   D_COUNTY,
                                   D_POSTAL_CODE,
                                   D_COUNTRY_CODE,
                                   D_ZONE_ID,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_SOURCE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_DTTM)
                 VALUES (vTCCompanyId,
                         vBatchId,
                         1,
                         vLaneHierarchy,
                         0,
                         vOLocType,
                         vOFacilityId,
                         vOFacilityAlias,
                         vOCity,
                         vOStateProv,
                         vOCounty,
                         vOPostal,
                         vOCountry,
                         vOZone,
                         vDLocType,
                         vDFacilityId,
                         vDFacilityAlias,
                         vDCity,
                         vDStateProv,
                         vDCounty,
                         vDPostal,
                         vDCountry,
                         vDZone,
                         3,
                         'OMS',
                         SYSDATE,
                         3,
                         'OMS',
                         SYSDATE);

            COMMIT;

            RETURN vBatchId;
         EXCEPTION
            WHEN OTHERS
            THEN
               RAISE;
         END;
   END INSERT_SAILING_LANE;

   ------------------------------------------------------------------------------

   FUNCTION GET_BUID_FROM_BUSINESS_UNIT (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vBUColumn         VARCHAR2)
      RETURN NUMBER
   IS
      vBUId    NUMBER;
      vCount   NUMBER;
   BEGIN
      SELECT COUNT (COMPANY_ID)
        INTO vCount
        FROM COMPANY
       WHERE COMPANY_NAME = vBUColumn;   -- and TC_COMPANY_ID = vTCCompanyId;

      IF (vCount > 0)
      THEN
         SELECT COMPANY_ID
           INTO vBUId
           FROM COMPANY
          WHERE COMPANY_NAME = vBUColumn; -- and TC_COMPANY_ID = vTCCompanyId;
      ELSE
         vBUId := vTCCompanyId;
      END IF;

      RETURN vBUId;
   END GET_BUID_FROM_BUSINESS_UNIT;

   ---------------------------------------------------------

   PROCEDURE TRANSFER_IMP_SAILING_LANE_DTL (
      vTCCompanyId    IN company.COMPANY_ID%TYPE,
      vImportLaneId   IN sailing_lane.LANE_ID%TYPE,
      vLaneId         IN sailing_lane.LANE_ID%TYPE)
   AS
      s_SailingLaneDtlSeq         sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE;
      s_ImpSailingLaneDtlSeq      sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE;
      --s_CarrierCode              import_sailing_lane_dtl.CARRIER_CODE%TYPE;
      --s_Mot                     import_sailing_lane_dtl.MOT%TYPE;
      --s_ServiceLevel            import_sailing_lane_dtl.SERVICE_LEVEL%TYPE;
      --s_scndr_carrier_code       import_sailing_lane_dtl.SCNDR_CARRIER_CODE%type;
      s_EffectiveDT               sailing_lane_dtl.EFFECTIVE_DT%TYPE;
      s_ExpirationDT              sailing_lane_dtl.EXPIRATION_DT%TYPE;
      s_ImpSailingDtlStatus       import_sailing_lane_dtl.IMPORT_SAILING_DTL_STATUS%TYPE;
      s_Carrier_Id                sailing_lane_dtl.CARRIER_ID%TYPE;
      s_Mot_Id                    sailing_lane_dtl.MOT_ID%TYPE;
      s_service_Level_Id          sailing_lane_dtl.SERVICE_LEVEL_ID%TYPE;
      s_scndr_carrier_Id          sailing_lane_dtl.SCNDR_CARRIER_ID%TYPE;
      s_fixedTransitStandardUOM   import_sailing_lane_dtl.FIXED_TRANSIT_STANDARD_UOM%TYPE;
      s_fixedTransitValue         import_sailing_lane_dtl.FIXED_TRANSIT_VALUE%TYPE;
      s_sailingFrequencyType      import_sailing_lane_dtl.SAILING_FREQUENCY_TYPE%TYPE;
      s_sailingScheduleName       import_sailing_lane_dtl.SAILING_SCHEDULE_NAME%TYPE;

      CURSOR TRANSFER_LANE_DTL_CURSOR
      IS
         SELECT sailing_lane_dtl_seq,
                carrier_id,
                mot_id,
                service_level_id,
                SCNDR_CARRIER_ID,
                effective_dt,
                expiration_dt,
                FIXED_TRANSIT_STANDARD_UOM,
                FIXED_TRANSIT_VALUE,
                SAILING_FREQUENCY_TYPE,
                SAILING_SCHEDULE_NAME,
                import_sailing_dtl_status
           FROM IMPORT_SAILING_LANE_DTL ISLD
          WHERE     tc_company_id = vTCCompanyId
                AND lane_id = vImportLaneId
                AND isld.LANE_DTL_STATUS = 1;
   BEGIN
      OPEN TRANSFER_LANE_DTL_CURSOR;

     <<TRANSFER_LANE_DTL_LOOP>>
      LOOP
         FETCH TRANSFER_LANE_DTL_CURSOR
         INTO s_ImpSailingLaneDtlSeq,
              s_Carrier_Id,
              s_Mot_Id,
              s_Service_Level_Id,
              s_scndr_carrier_Id,
              s_EffectiveDT,
              s_ExpirationDT,
              s_fixedTransitStandardUOM,
              s_fixedTransitValue,
              s_sailingFrequencyType,
              s_sailingScheduleName,
              s_ImpSailingDtlStatus;

         EXIT TRANSFER_LANE_DTL_LOOP WHEN TRANSFER_LANE_DTL_CURSOR%NOTFOUND;

         s_SailingLaneDtlSeq :=
            INSERT_SAILING_LANE_DTL (vTCCompanyId,
                                     vLaneId,
                                     s_Carrier_Id,
                                     s_Mot_Id,
                                     s_service_Level_Id,
                                     s_scndr_carrier_Id,
                                     s_EffectiveDT,
                                     s_ExpirationDT,
                                     s_ImpSailingDtlStatus,
                                     s_fixedTransitStandardUOM,
                                     s_fixedTransitValue,
                                     s_sailingFrequencyType,
                                     s_sailingScheduleName);

         --   DBMS_OUTPUT.PUT_LINE('Before Transfer into SAILING_SCH_BY_DT');

         IF (s_sailingFrequencyType = 4)
         THEN
            TRANSFER_SAILING_SCH_BY_DT (vTCCompanyId,
                                        vImportLaneID,
                                        vLaneId,
                                        s_ImpSailingLaneDtlSeq,
                                        s_SailingLaneDtlSeq);
         END IF;

         IF (s_sailingFrequencyType = 8)
         THEN
            TRANSFER_SAILING_SCH_BY_WK (vTCCompanyId,
                                        vImportLaneID,
                                        vLaneId,
                                        s_ImpSailingLaneDtlSeq,
                                        s_SailingLaneDtlSeq);
         END IF;

         /*9900 */
         DELETE FROM IMPORT_SAILING_LANE_DTL ISLD
               WHERE     TC_COMPANY_ID = vTCCompanyId
                     AND LANE_ID = vImportLaneId
                     AND SAILING_LANE_DTL_SEQ = s_ImpSailingLaneDtlSeq
                     AND isld.IMPORT_SAILING_DTL_STATUS != 4;

         /*9901 Adjust call for spliting lanes */
         ADJUST_EFF_EXP_DT (vTCCompanyId,
                            vLaneId,
                            s_SailingLaneDtlSeq,
                            s_EffectiveDT,
                            s_ExpirationDT,
                            s_Carrier_Id,
                            s_Mot_Id,
                            s_Service_Level_Id,
                            s_scndr_carrier_Id);

         COMMIT;
      END LOOP TRANSFER_LANE_DTL_LOOP;

      CLOSE TRANSFER_LANE_DTL_CURSOR;
   END TRANSFER_IMP_SAILING_LANE_DTL;

   ------------------------------------------------------------------------------

   FUNCTION INSERT_SAILING_LANE_DTL (
      vTCCompanyId               IN company.COMPANY_ID%TYPE,
      vSailingLaneId             IN sailing_lane_dtl.LANE_ID%TYPE,
      vCarrierCode               IN sailing_lane_dtl.CARRIER_ID%TYPE,
      vMot                       IN sailing_lane_dtl.MOT_ID%TYPE,
      vServiceLevel              IN sailing_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code       IN sailing_lane_dtl.SCNDR_CARRIER_ID%TYPE,
      vEffectiveDT               IN sailing_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT              IN sailing_lane_dtl.EXPIRATION_DT%TYPE,
      vImpSailingDtlStatus       IN import_sailing_lane_dtl.IMPORT_SAILING_DTL_STATUS%TYPE,
      vFixedTransitSTandardUOM   IN import_sailing_lane_dtl.FIXED_TRANSIT_STANDARD_UOM%TYPE,
      vFixedTransitValue         IN import_sailing_lane_dtl.FIXED_TRANSIT_VALUE%TYPE,
      vSailingFrequencyType      IN import_sailing_lane_dtl.SAILING_FREQUENCY_TYPE%TYPE,
      vSailingScheduleName       IN import_sailing_lane_dtl.SAILING_SCHEDULE_NAME%TYPE)
      RETURN NUMBER
   IS
      max_lane_dtl_seq         sailing_lane_dtl.sailing_lane_dtl_seq%TYPE;
      sld_seq                  sailing_lane_dtl.sailing_lane_dtl_seq%TYPE;
      sailing_dtl_seq_cursor   ref_curtype;
      expire_dtl_sql           VARCHAR2 (2000);
   BEGIN
      --  vImpSailingDtlStatus is not the lane detail status. Possible values are:
      --  0 for Import as regular lane
      --  1 for expire this lane

      IF (vImpSailingDtlStatus = 1)
      THEN
         expire_dtl_sql :=
               ' SELECT NVL(sailing_lane_dtl_seq, 0) '
            || ' FROM COMB_LANE_DTL '
            || ' WHERE TC_COMPANY_ID = '
            || vTCCompanyId
            || ' AND LANE_ID = '
            || vSailingLaneId
	    || ' AND IS_SAILING = 1'
            || ' AND EFFECTIVE_DT = '''
            || TO_CHAR (vEffectiveDT)
            || ''' '
            || ' AND EXPIRATION_DT = '''
            || TO_CHAR (vExpirationDT)
            || ''' ';

         expire_dtl_sql :=
            expire_dtl_sql
            || GET_RESOURCE_WHERE_CLAUSE (vCarrierCode,
                                          vMot,
                                          vServiceLevel,
                                          p_scndr_carrier_code);

         sld_seq := 0;

         OPEN sailing_dtl_seq_cursor FOR expire_dtl_sql;

         LOOP
            FETCH sailing_dtl_seq_cursor INTO sld_seq;

            EXIT WHEN sailing_dtl_seq_cursor%NOTFOUND;

            UPDATE SAILING_LANE_DTL
               SET expiration_dt = effective_dt - 1,
                   last_updated_dttm = SYSDATE
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vSailingLaneId
                   AND sailing_lane_dtl_seq = sld_seq;
         END LOOP;

         CLOSE sailing_dtl_seq_cursor;

         COMMIT;

         RETURN sld_seq;
      ELSE
         SELECT (NVL (MAX (lane_dtl_seq), 0) + 1)
           INTO max_lane_dtl_seq
           FROM comb_lane_dtl
          WHERE tc_company_id = vTCCompanyId AND lane_id = vSailingLaneId;

         DBMS_OUTPUT.PUT_LINE (
            'INSERTING NEW RECORD INTO SAILING_LANE_DTL TABLE');

         DBMS_OUTPUT.PUT_LINE (
               'COMAPNY_ID: '
            || TO_CHAR (vTCCompanyId)
            || ' Lane Id: '
            || TO_CHAR (vSailingLaneId));

         INSERT INTO COMB_LANE_DTL cbd (TC_COMPANY_ID,
                                        LANE_ID,
                                        LANE_DTL_SEQ,
                                        IS_SAILING,
                                        CARRIER_ID,
                                        MOT_ID,
                                        SERVICE_LEVEL_ID,
                                        LAST_UPDATED_SOURCE_TYPE,
                                        LAST_UPDATED_SOURCE,
                                        LAST_UPDATED_DTTM,
                                        LANE_DTL_STATUS,
                                        EFFECTIVE_DT,
                                        EXPIRATION_DT,
                                        SCNDR_CARRIER_ID,
                                        FIXED_TRANSIT_STANDARD_UOM,
                                        FIXED_TRANSIT_VALUE,
                                        SAILING_FREQUENCY_TYPE,
                                        SAILING_SCHEDULE_NAME)
              VALUES (vTCCompanyId,
                      vSailingLaneId,
                      max_lane_dtl_seq,
                      1,
                      vCarrierCode,
                      vMot,
                      vServiceLevel,
                      3,
                      'OMS',
                      SYSDATE,
                      0,
                      vEffectiveDT,
                      vExpirationDT,
                      p_scndr_carrier_code,
                      vFixedTransitSTandardUOM,
                      vFixedTransitValue,
                      vSailingFrequencyType,
                      vSailingScheduleName);

         COMMIT;

         RETURN max_lane_dtl_seq;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         SELECT (NVL (MAX (lane_dtl_seq), 0) + 1)
           INTO max_lane_dtl_seq
           FROM comb_lane_dtl
          WHERE tc_company_id = vTCCompanyId AND lane_id = vSailingLaneId;

         INSERT INTO COMB_LANE_DTL (TC_COMPANY_ID,
                                    LANE_ID,
                                    LANE_DTL_SEQ,
                                    IS_SAILING,
                                    CARRIER_ID,
                                    MOT_ID,
                                    SERVICE_LEVEL_ID,
                                    LAST_UPDATED_SOURCE_TYPE,
                                    LAST_UPDATED_SOURCE,
                                    LAST_UPDATED_DTTM,
                                    LANE_DTL_STATUS,
                                    EFFECTIVE_DT,
                                    EXPIRATION_DT,
                                    SCNDR_CARRIER_ID)
              VALUES (vTCCompanyId,
                      vSailingLaneId,
                      max_lane_dtl_seq,
                      1,
                      vCarrierCode,
                      vMot,
                      vServiceLevel,
                      3,
                      'OMS',
                      SYSDATE,
                      0,
                      vEffectiveDT,
                      TRUNC (SYSDATE - 1),
                      p_scndr_carrier_code);

         COMMIT;

         RETURN max_lane_dtl_seq;
   END INSERT_SAILING_LANE_DTL;

   ------------------------------------------------------------------------------
   PROCEDURE TRANSFER_SAILING_SCH_BY_DT (
      vTCCompanyId            IN company.COMPANY_ID%TYPE,
      vImportLaneId           IN SAILING_SCHDL_BY_DATE.LANE_ID%TYPE,
      vLaneId                 IN SAILING_SCHDL_BY_DATE.LANE_ID%TYPE,
      vImpSailingLaneDtlSeq   IN SAILING_SCHDL_BY_DATE.LANE_DTL_SEQ%TYPE,
      vSailingLaneDtlSeq      IN SAILING_SCHDL_BY_DATE.LANE_DTL_SEQ%TYPE)
   AS
      vDepatureDttm            SAILING_SCHDL_BY_DATE.DEPARTURE_DTTM%TYPE;
      vArrivalDttm             SAILING_SCHDL_BY_DATE.ARRIVAL_DTTM%TYPE;
      vCutoffDttm              SAILING_SCHDL_BY_DATE.CUTOFF_DTTM%TYPE;
      vPickupDttm              SAILING_SCHDL_BY_DATE.PICKUP_DTTM%TYPE;
      vTransitTimeValue        SAILING_SCHDL_BY_DATE.TRANSIT_TIME_VALUE%TYPE;
      vTransitTimeStandarUOM   SAILING_SCHDL_BY_DATE.TRANSIT_TIME_STANDARD_UOM%TYPE;
      vResourceRefExternal     SAILING_SCHDL_BY_DATE.RESOURCE_REF_EXTERNAL%TYPE;
      vResourceNameExternal    SAILING_SCHDL_BY_DATE.RESOURCE_NAME_EXTERNAL%TYPE;
      vComments                SAILING_SCHDL_BY_DATE.COMMENTS%TYPE;
      vDescription             SAILING_SCHDL_BY_DATE.DESCRIPTION%TYPE;

      CURSOR IMPORTED_SCH_BY_DATE_CURSOR
      IS
         SELECT DEPARTURE_DTTM,
                ARRIVAL_DTTM,
                CUTOFF_DTTM,
                PICKUP_DTTM,
                TRANSIT_TIME_VALUE,
                TRANSIT_TIME_STANDARD_UOM,
                RESOURCE_REF_EXTERNAL,
                RESOURCE_NAME_EXTERNAL,
                COMMENTS,
                DESCRIPTION
           FROM IMPORT_SAILING_SCHDL_BY_DATE ISLD
          WHERE     TC_COMPANY_ID = vTCCompanyId
                AND LANE_ID = vImportLaneId
                AND LANE_DTL_SEQ = vImpSailingLaneDtlSeq;
   BEGIN
      OPEN IMPORTED_SCH_BY_DATE_CURSOR;

     <<IMPORTED_SCH_BY_DATE_LOOP>>
      LOOP
         FETCH IMPORTED_SCH_BY_DATE_CURSOR
         INTO vDepatureDttm,
              vArrivalDttm,
              vCutoffDttm,
              vPickupDttm,
              vTransitTimeValue,
              vTransitTimeStandarUOM,
              vResourceRefExternal,
              vResourceNameExternal,
              vComments,
              vDescription;

         EXIT IMPORTED_SCH_BY_DATE_LOOP WHEN IMPORTED_SCH_BY_DATE_CURSOR%NOTFOUND;

         INSERT_SAILING_SCH_BY_DT (vTCCompanyId,
                                   vLaneId,
                                   vSailingLaneDtlSeq,
                                   vDepatureDttm,
                                   vArrivalDttm,
                                   vCutoffDttm,
                                   vPickupDttm,
                                   vTransitTimeValue,
                                   vTransitTimeStandarUOM,
                                   vResourceRefExternal,
                                   vResourceNameExternal,
                                   vComments,
                                   vDescription);
      END LOOP IMPORTED_SCH_BY_DATE_LOOP;

      CLOSE IMPORTED_SCH_BY_DATE_CURSOR;

      /* 9900 */
      DELETE FROM IMPORT_SAILING_SCHDL_BY_DATE
            WHERE     tc_company_id = vTCCompanyId
                  AND lane_id = vImportLaneId
                  AND LANE_DTL_SEQ = vImpSailingLaneDtlSeq;
   END TRANSFER_SAILING_SCH_BY_DT;

   -------------------------------------------------------------------------------------------------
   PROCEDURE INSERT_SAILING_SCH_BY_DT (
      vTCCompanyId             IN company.COMPANY_ID%TYPE,
      vSailingLaneId           IN SAILING_SCHDL_BY_DATE.LANE_ID%TYPE,
      vSailingLaneDtlId        IN SAILING_SCHDL_BY_DATE.LANE_DTL_SEQ%TYPE,
      vDepatureDttm            IN SAILING_SCHDL_BY_DATE.DEPARTURE_DTTM%TYPE,
      vArrivalDttm             IN SAILING_SCHDL_BY_DATE.ARRIVAL_DTTM%TYPE,
      vCutoffDttm              IN SAILING_SCHDL_BY_DATE.CUTOFF_DTTM%TYPE,
      vPickupDttm              IN SAILING_SCHDL_BY_DATE.PICKUP_DTTM%TYPE,
      vTransitTimeValue        IN SAILING_SCHDL_BY_DATE.TRANSIT_TIME_VALUE%TYPE,
      vTransitTimeStandarUOM   IN SAILING_SCHDL_BY_DATE.TRANSIT_TIME_STANDARD_UOM%TYPE,
      vResourceRefExternal     IN SAILING_SCHDL_BY_DATE.RESOURCE_REF_EXTERNAL%TYPE,
      vResourceNameExternal    IN SAILING_SCHDL_BY_DATE.RESOURCE_NAME_EXTERNAL%TYPE,
      vComments                IN SAILING_SCHDL_BY_DATE.COMMENTS%TYPE,
      vDescription             IN SAILING_SCHDL_BY_DATE.DESCRIPTION%TYPE)
   AS
   BEGIN
      INSERT INTO SAILING_SCHDL_BY_DATE (TC_COMPANY_ID,
                                         LANE_ID,
                                         LANE_DTL_SEQ,
                                         SAIL_SCHED_BY_DATE_ID,
                                         DEPARTURE_DTTM,
                                         ARRIVAL_DTTM,
                                         CUTOFF_DTTM,
                                         PICKUP_DTTM,
                                         TRANSIT_TIME_VALUE,
                                         TRANSIT_TIME_STANDARD_UOM,
                                         RESOURCE_REF_EXTERNAL,
                                         RESOURCE_NAME_EXTERNAL,
                                         COMMENTS,
                                         DESCRIPTION)
           VALUES (vTCCompanyId,
                   vSailingLaneId,
                   vSailingLaneDtlId,
                   SEQ_SAIL_SCHED_BY_DATE_ID.NEXTVAL,
                   vDepatureDttm,
                   vArrivalDttm,
                   vCutoffDttm,
                   vPickupDttm,
                   vTransitTimeValue,
                   vTransitTimeStandarUOM,
                   vResourceRefExternal,
                   vResourceNameExternal,
                   vComments,
                   vDescription);

      COMMIT;
   END INSERT_SAILING_SCH_BY_DT;

   --------------------------------------------------------------------------------------------------

   PROCEDURE TRANSFER_SAILING_SCH_BY_WK (
      vTCCompanyId            IN company.COMPANY_ID%TYPE,
      vImportLaneID           IN sailing_lane.LANE_ID%TYPE,
      vLaneId                 IN sailing_lane.LANE_ID%TYPE,
      vImpSailingLaneDtlSeq   IN sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE,
      vSailingLaneDtlSeq      IN sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE)
   AS
      vSailSchedByWeekSeq        sailing_schdl_by_week.SAIL_SCHED_BY_WEEK_ID%TYPE;
      vVesselName                sailing_schdl_by_week.RESOURCE_REF_EXTERNAL%TYPE;
      vVoyageNumber              sailing_schdl_by_week.RESOURCE_NAME_EXTERNAL%TYPE;
      vComments                  sailing_schdl_by_week.COMMENTS%TYPE;
      vRestrictions              sailing_schdl_by_week.DESCRIPTION%TYPE;
      vEffectiveDt               sailing_schdl_by_week.EFFECTIVE_DT%TYPE;
      vExpirationDt              sailing_schdl_by_week.EXPIRATION_DT%TYPE;
      vImpSailingSchedByWeekId   SAILING_SCHDL_BY_WEEK_DAY.SAIL_SCHED_BY_WEEK_DAY_ID%TYPE;

      CURSOR TRANSFER_SAIL_SCHDL_WK_CURSOR
      IS
         SELECT SAIL_SCHED_BY_WEEK_ID,
                RESOURCE_REF_EXTERNAL,
                RESOURCE_NAME_EXTERNAL,
                COMMENTS,
                DESCRIPTION,
                EFFECTIVE_DT,
                EXPIRATION_DT
           FROM IMPORT_SAILING_SCHDL_BY_WK
          WHERE     tc_company_id = vTCCompanyId
                AND lane_id = vImportLaneId
                AND SAILING_LANE_DTL_SEQ = vImpSailingLaneDtlSeq;
   BEGIN
      OPEN TRANSFER_SAIL_SCHDL_WK_CURSOR;

     <<TRANSFER_LANE_DTL_WK_LOOP>>
      LOOP
         FETCH TRANSFER_SAIL_SCHDL_WK_CURSOR
         INTO vImpSailingSchedByWeekId,
              vVoyageNumber,
			  vVesselName,
              vComments,
              vRestrictions,
              vEffectiveDt,
              vExpirationDt;

         EXIT TRANSFER_LANE_DTL_WK_LOOP WHEN TRANSFER_SAIL_SCHDL_WK_CURSOR%NOTFOUND;

         vSailSchedByWeekSeq :=
            INSERT_SAILING_SCH_BY_WK (vTCCompanyId,
                                      vLaneId,
                                      vSailingLaneDtlSeq,
                                      vVesselName,
                                      vVoyageNumber,
                                      vComments,
                                      vRestrictions,
                                      vEffectiveDt,
                                      vExpirationDt);

         TRANSFER_SAILING_SCH_WK_DAY (vTCCompanyId,
                                      vImportLaneID,
                                      vLaneId,
                                      vImpSailingLaneDtlSeq,
                                      vSailingLaneDtlSeq,
                                      vImpSailingSchedByWeekId,
                                      vSailSchedByWeekSeq);
         COMMIT;
      END LOOP TRANSFER_LANE_DTL_WK_LOOP;

      CLOSE TRANSFER_SAIL_SCHDL_WK_CURSOR;

      /*9900 */
      DELETE FROM IMPORT_SAILING_SCHDL_BY_WK iwk
            WHERE     tc_company_id = vTCCompanyId
                  AND lane_id = vImportLaneId
                  AND iwk.sailing_lane_dtl_seq = vImpSailingLaneDtlSeq;
   END TRANSFER_SAILING_SCH_BY_WK;

   --------------------------------------------------------------------------------------------------
   FUNCTION INSERT_SAILING_SCH_BY_WK (
      vTCCompanyId        IN company.COMPANY_ID%TYPE,
      vSailingLaneId      IN SAILING_SCHDL_BY_WEEK.LANE_ID%TYPE,
      vSailingLaneDtlId   IN SAILING_SCHDL_BY_WEEK.LANE_DTL_SEQ%TYPE,
      vVesselName         IN SAILING_SCHDL_BY_WEEK.RESOURCE_NAME_EXTERNAL%TYPE,
      vVoyageNumber       IN SAILING_SCHDL_BY_WEEK.RESOURCE_REF_EXTERNAL%TYPE,
      vComments           IN SAILING_SCHDL_BY_WEEK.COMMENTS%TYPE,
      vRestrictions       IN SAILING_SCHDL_BY_WEEK.DESCRIPTION%TYPE,
      vEffectiveDt        IN SAILING_SCHDL_BY_WEEK.EFFECTIVE_DT%TYPE,
      vExpirationDt       IN SAILING_SCHDL_BY_WEEK.EXPIRATION_DT%TYPE)
      RETURN NUMBER
   AS
      vSailingSchedByWeekId   SAILING_SCHDL_BY_WEEK.SAIL_SCHED_BY_WEEK_ID%TYPE;
   BEGIN
      SELECT SEQ_SAIL_SCHED_BY_WEEK_ID.NEXTVAL
        INTO vSailingSchedByWeekId
        FROM DUAL;

      INSERT INTO SAILING_SCHDL_BY_WEEK (TC_COMPANY_ID,
                                         LANE_ID,
                                         LANE_DTL_SEQ,
                                         SAIL_SCHED_BY_WEEK_ID,
                                         EFFECTIVE_DT,
                                         EXPIRATION_DT,
                                         RESOURCE_REF_EXTERNAL,
                                         RESOURCE_NAME_EXTERNAL,
                                         COMMENTS,
                                         DESCRIPTION)
           VALUES (vTCCompanyId,
                   vSailingLaneId,
                   vSailingLaneDtlId,
                   vSailingSchedByWeekId,
                   vEffectiveDt,
                   vExpirationDt,
                   vVoyageNumber,
                   vVesselName,
                   vComments,
                   vRestrictions);

      COMMIT;
      RETURN vSailingSchedByWeekId;
   END INSERT_SAILING_SCH_BY_WK;

   --------------------------------------------------------------------------------------------------
   FUNCTION GET_RESOURCE_WHERE_CLAUSE (
      vCarrierCode           IN sailing_lane_dtl.CARRIER_ID%TYPE,
      vMot                   IN sailing_lane_dtl.MOT_ID%TYPE,
      vServiceLevel          IN sailing_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN sailing_LANE_DTL.SCNDR_CARRIER_ID%TYPE)
      RETURN VARCHAR2
   IS
      resource_sql   VARCHAR2 (2000);
   BEGIN
      resource_sql := '';

      IF (vCarrierCode IS NOT NULL)
      THEN
         resource_sql := resource_sql || ' AND carrier_id = ' || vCarrierCode;
      ELSE
         resource_sql := resource_sql || ' AND carrier_id IS Null ';
      END IF;

      resource_sql :=
         resource_sql
         || GET_BUDG_RESOURCE_WHERE_CLAUSE (vMot,
                                            vServiceLevel,
                                            p_scndr_carrier_code);

      RETURN resource_sql;
   END GET_RESOURCE_WHERE_CLAUSE;

   --------------------------------------------------------------------------------------------------
   FUNCTION GET_BUDG_RESOURCE_WHERE_CLAUSE (
      vMot                   IN sailing_lane_dtl.MOT_ID%TYPE,
      vServiceLevel          IN sailing_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN sailing_lane_dtl.SCNDR_CARRIER_ID%TYPE)
      RETURN VARCHAR2
   IS
      resource_sql   VARCHAR2 (2000);
   BEGIN
      resource_sql := '';

      IF (vMot IS NOT NULL)
      THEN
         resource_sql := resource_sql || ' AND mot_id = ' || vMot;
      ELSE
         resource_sql := resource_sql || ' AND mot_id IS Null ';
      END IF;

      IF (vServiceLevel IS NOT NULL)
      THEN
         resource_sql :=
            resource_sql || ' AND service_level_id = ' || vServiceLevel;
      ELSE
         resource_sql := resource_sql || ' AND service_level_id IS Null ';
      END IF;

      IF (p_scndr_carrier_code IS NOT NULL)
      THEN
         resource_sql :=
               resource_sql
            || ' and SCNDR_CARRIER_ID = '
            || p_scndr_carrier_code;
      ELSE
         resource_sql := resource_sql || ' and SCNDR_CARRIER_ID is NULL ';
      END IF;
      
      resource_sql := resource_sql || ' AND O_SHIP_VIA IS Null AND D_SHIP_VIA IS Null AND VOYAGE IS NULL AND EQUIPMENT_ID IS NULL AND PROTECTION_LEVEL_ID IS NULL';

      RETURN resource_sql;
   END GET_BUDG_RESOURCE_WHERE_CLAUSE;

   --------------------------------------------------------------------------------------------------
   PROCEDURE TRANSFER_SAILING_SCH_WK_DAY (
      vTCCompanyId            IN company.COMPANY_ID%TYPE,
      vImportLaneID           IN sailing_lane.LANE_ID%TYPE,
      vLaneId                 IN sailing_lane.LANE_ID%TYPE,
      vImpSailingLaneDtlSeq   IN sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE,
      vSailingLaneDtlSeq      IN sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE,
      vImpSailingDtlWkSeq     IN sailing_schdl_by_week.SAIL_SCHED_BY_WEEK_ID%TYPE,
      vSailingDtlWkSeq        IN sailing_schdl_by_week.SAIL_SCHED_BY_WEEK_ID%TYPE)
   AS
      vSailSchedByWeekDayId   sailing_schdl_by_week_day.SAIL_SCHED_BY_WEEK_DAY_ID%TYPE;
      vCutoffTime             sailing_schdl_by_week_day.CUTOFF_TIME%TYPE;
      vArrivalTime            sailing_schdl_by_week_day.ARRIVAL_TIME%TYPE;
      vDepartureTime          sailing_schdl_by_week_day.DEPARTURE_TIME%TYPE;
      vAvailableTime          sailing_schdl_by_week_day.PICKUP_TIME%TYPE;
      vCuttOffDay             sailing_schdl_by_week_day.CUTOFF_DAYOFWEEK%TYPE;
      vArrivalDay             sailing_schdl_by_week_day.ARRIVAL_DAYOFWEEK%TYPE;
      vDepartureDay           sailing_schdl_by_week_day.DEPARTURE_DAYOFWEEK%TYPE;
      vAvailableDay           sailing_schdl_by_week_day.PICKUP_DAYOFWEEK%TYPE;
      vTransitTime            sailing_schdl_by_week_day.TRANSIT_TIME_VALUE%TYPE;
      vTransitTimeUOM         sailing_schdl_by_week_day.TRANSIT_TIME_STANDARD_UOM%TYPE;
      vDuration               sailing_schdl_by_week_day.TRANSIT_TIME_WEEK%TYPE;

      CURSOR TRANSFER_SAIL_SCH_WK_D_CURSOR
      IS
         SELECT SAIL_SCHED_BY_WEEK_DAY_ID,
                CUTOFF_TIME,
                ARRIVAL_TIME,
                DEPARTURE_TIME,
                PICKUP_TIME,
                CUTOFF_DAYOFWEEK,
                ARRIVAL_DAYOFWEEK,
                DEPARTURE_DAYOFWEEK,
                PICKUP_DAYOFWEEK,
                TRANSIT_TIME_VALUE,
                TRANSIT_TIME_STANDARD_UOM,
                DURATION
           FROM IMPORT_SAILING_SCHDL_WK_DAY
          WHERE     tc_company_id = vTCCompanyId
                AND lane_id = vImportLaneId
                AND SAILING_LANE_DTL_SEQ = vImpSailingLaneDtlSeq
                AND SAIL_SCHED_BY_WEEK_ID = vImpSailingDtlWkSeq;
   BEGIN
      OPEN TRANSFER_SAIL_SCH_WK_D_CURSOR;

     <<TRANSFER_LANE_DTL_WK_DAY_LOOP>>
      LOOP
         FETCH TRANSFER_SAIL_SCH_WK_D_CURSOR
         INTO vSailSchedByWeekDayId,
              vCutoffTime,
              vArrivalTime,
              vDepartureTime,
              vAvailableTime,
              vCuttOffDay,
              vArrivalDay,
              vDepartureDay,
              vAvailableDay,
              vTransitTime,
              vTransitTimeUOM,
              vDuration;

         EXIT TRANSFER_LANE_DTL_WK_DAY_LOOP WHEN TRANSFER_SAIL_SCH_WK_D_CURSOR%NOTFOUND;

         INSERT_SAILING_SCH_BY_WK_DAY (vTCCompanyId,
                                       vLaneId,
                                       vSailingLaneDtlSeq,
                                       vSailingDtlWkSeq,
                                       vCutoffTime,
                                       vArrivalTime,
                                       vDepartureTime,
                                       vAvailableTime,
                                       vCuttOffDay,
                                       vArrivalDay,
                                       vDepartureDay,
                                       vAvailableDay,
                                       vTransitTime,
                                       vTransitTimeUOM,
                                       vDuration);
         COMMIT;
      END LOOP TRANSFER_LANE_DTL_WK_DAY_LOOP;

      CLOSE TRANSFER_SAIL_SCH_WK_D_CURSOR;

      /*9900 */
      DELETE FROM IMPORT_SAILING_SCHDL_WK_DAY iwkd
            WHERE     tc_company_id = vTCCompanyId
                  AND lane_id = vImportLaneId
                  AND iwkd.sailing_lane_dtl_seq = vImpSailingLaneDtlSeq;
   END TRANSFER_SAILING_SCH_WK_DAY;

   --------------------------------------------------------------------------------------------------
   PROCEDURE INSERT_SAILING_SCH_BY_WK_DAY (
      vTCCompanyId        IN company.COMPANY_ID%TYPE,
      vSailingLaneId      IN SAILING_SCHDL_BY_WEEK_DAY.LANE_ID%TYPE,
      vSailingLaneDtlId   IN SAILING_SCHDL_BY_WEEK_DAY.LANE_DTL_SEQ%TYPE,
      vSailingDtlWkSeq    IN SAILING_SCHDL_BY_WEEK_DAY.SAIL_SCHED_BY_WEEK_ID%TYPE,
      vCutoffTime         IN SAILING_SCHDL_BY_WEEK_DAY.CUTOFF_TIME%TYPE,
      vArrivalTime        IN SAILING_SCHDL_BY_WEEK_DAY.ARRIVAL_TIME%TYPE,
      vDepartureTime      IN SAILING_SCHDL_BY_WEEK_DAY.DEPARTURE_TIME%TYPE,
      vAvailableTime      IN SAILING_SCHDL_BY_WEEK_DAY.PICKUP_TIME%TYPE,
      vCuttOffDay         IN SAILING_SCHDL_BY_WEEK_DAY.CUTOFF_DAYOFWEEK%TYPE,
      vArrivalDay         IN SAILING_SCHDL_BY_WEEK_DAY.ARRIVAL_DAYOFWEEK%TYPE,
      vDepartureDay       IN SAILING_SCHDL_BY_WEEK_DAY.DEPARTURE_DAYOFWEEK%TYPE,
      vAvailableDay       IN SAILING_SCHDL_BY_WEEK_DAY.PICKUP_DAYOFWEEK%TYPE,
      vTransitTime        IN SAILING_SCHDL_BY_WEEK_DAY.TRANSIT_TIME_VALUE%TYPE,
      vTransitTimeUOM     IN SAILING_SCHDL_BY_WEEK_DAY.TRANSIT_TIME_STANDARD_UOM%TYPE,
      vDuration           IN SAILING_SCHDL_BY_WEEK_DAY.TRANSIT_TIME_WEEK%TYPE)
   AS
   /*   vTransitTimeDays  NUMBER;
        vTempCuttOffTime  DATE;
        vTempCuttOffDay   NUMBER;
        vTempDepartureTime DATE;
        vTempArrivalDay   NUMBER;
        vTempArrivalTime  DATE;
        vTempAvailableDay NUMBER;
        vTempAvailableTime DATE;
        vTempDuration     NUMBER;
        vTempDay         NUMBER;
      */
   BEGIN
      INSERT INTO SAILING_SCHDL_BY_WEEK_DAY (LANE_ID,
                                             LANE_DTL_SEQ,
                                             SAIL_SCHED_BY_WEEK_ID,
                                             SAIL_SCHED_BY_WEEK_DAY_ID,
                                             TC_COMPANY_ID,
                                             DEPARTURE_DAYOFWEEK,
                                             DEPARTURE_TIME,
                                             ARRIVAL_DAYOFWEEK,
                                             ARRIVAL_TIME,
                                             CUTOFF_DAYOFWEEK,
                                             CUTOFF_TIME,
                                             PICKUP_DAYOFWEEK,
                                             PICKUP_TIME,
                                             TRANSIT_TIME_VALUE,
                                             TRANSIT_TIME_STANDARD_UOM,
                                             TRANSIT_TIME_WEEK)
           VALUES (vSailingLaneId,
                   vSailingLaneDtlId,
                   vSailingDtlWkSeq,
                   SEQ_SAIL_SCHED_BY_WEEK_DAY_ID.NEXTVAL,
                   vTCCompanyId,
                   vDepartureDay,
                   vDepartureTime,
                   vArrivalDay,
                   vArrivalTime,
                   vCuttOffDay,
                   vCutoffTime,
                   vAvailableDay,
                   vAvailableTime,
                   vTransitTime,
                   vTransitTimeUOM,
                   vDuration);

      COMMIT;
   END INSERT_SAILING_SCH_BY_WK_DAY;

   --------------------------------------------------------------------------------------------------
   -- validate PROCEDURES
   ---------------------------------------------------------------------------------------------
   PROCEDURE VALIDATE_IMPORT_SAILING_LANES (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vFirstBatchId   IN sailing_lane.LANE_ID%TYPE,
      vLastBatchId    IN sailing_lane.LANE_ID%TYPE)
   AS
      vLaneId   comb_lane.LANE_ID%TYPE;
	  vBatchId		    INTEGER;

      CURSOR IMPORTED_LANES_CURSOR
      IS
         SELECT LANE_ID,BATCH_ID
           FROM IMPORT_SAILING_LANE
          WHERE     TC_COMPANY_ID = vTCCompanyId
                AND LANE_STATUS = 3
                AND BATCH_ID >= vFirstBatchId
                AND BATCH_ID <= vLastBatchId;
   BEGIN
      OPEN IMPORTED_LANES_CURSOR;

     <<IMPORTED_LANES_LOOP>>
      LOOP
         FETCH IMPORTED_LANES_CURSOR into vLaneId,vBatchId;
		 UPDATE SAILING_LANE_ERRORS SET LANE_ID = vLaneId WHERE BATCH_ID = vBatchId;

         EXIT IMPORTED_LANES_LOOP WHEN IMPORTED_LANES_CURSOR%NOTFOUND;

         VALIDATE_SAILING_LANE (vTCCompanyId, vLaneId);
      END LOOP IMPORTED_LANES_LOOP;

      CLOSE IMPORTED_LANES_CURSOR;

      TRANSFER_IMPORT_SAILING_LANES (vTCCompanyId,
                                     vFirstBatchId,
                                     vLastBatchId);
   END VALIDATE_IMPORT_SAILING_LANES;

   -----------------------------------------------------------------------------------------
   PROCEDURE VALIDATE_SAILING_LANE (
      vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
      vLaneId        IN SAILING_LANE.LANE_ID%TYPE)
   AS
      vBusinessUnit       BUSINESS_UNIT.BUSINESS_UNIT%TYPE;
      vOLocType           sailing_lane.O_LOC_TYPE%TYPE;
      vOFacilityId        sailing_lane.O_FACILITY_ID%TYPE;
      vOFacilityAliasId   sailing_lane.O_FACILITY_ALIAS_ID%TYPE;
      vOFacilityBU        BUSINESS_UNIT.BUSINESS_UNIT%TYPE;
      vOCity              sailing_lane.O_CITY%TYPE;
      vOStateProv         sailing_lane.O_STATE_PROV%TYPE;
      vOCounty            sailing_lane.O_COUNTY%TYPE;
      vOPostal            sailing_lane.O_POSTAL_CODE%TYPE;
      vOCountry           sailing_lane.O_COUNTRY_CODE%TYPE;
      vOZoneId            import_sailing_lane.O_ZONE_ID%TYPE;
      vOZoneName          import_sailing_lane.O_ZONE_NAME%TYPE;
      vDLocType           sailing_lane.D_LOC_TYPE%TYPE;
      vDFacilityId        sailing_lane.D_FACILITY_ID%TYPE;
      vDFacilityAliasId   sailing_lane.D_FACILITY_ALIAS_ID%TYPE;
      vDFacilityBU        BUSINESS_UNIT.BUSINESS_UNIT%TYPE;
      vDCity              sailing_lane.D_CITY%TYPE;
      vDStateProv         sailing_lane.D_STATE_PROV%TYPE;
      vDCounty            sailing_lane.D_COUNTY%TYPE;
      vDPostal            sailing_lane.D_POSTAL_CODE%TYPE;
      vDCountry           sailing_lane.D_COUNTRY_CODE%TYPE;
      vDZoneId            import_sailing_lane.D_ZONE_ID%TYPE;
      vDZoneName          import_sailing_lane.D_ZONE_NAME%TYPE;
      vPassFail           NUMBER;
      vFailure            NUMBER;
      vCount              NUMBER;

      CURSOR LANE_CURSOR
      IS
         SELECT BUSINESS_UNIT,
                O_LOC_TYPE,
                O_FACILITY_ID,
                O_FACILITY_ALIAS_ID,
                O_FACILITY_BU,
                O_CITY,
                O_STATE_PROV,
                O_COUNTY,
                O_POSTAL_CODE,
                O_COUNTRY_CODE,
                O_ZONE_ID,
                O_ZONE_NAME,
                D_LOC_TYPE,
                D_FACILITY_ID,
                D_FACILITY_ALIAS_ID,
                D_FACILITY_BU,
                D_CITY,
                D_STATE_PROV,
                D_COUNTY,
                D_POSTAL_CODE,
                DECODE (D_COUNTRY_CODE, '', NULL, D_COUNTRY_CODE)
                   D_COUNTRY_CODE,
                D_ZONE_ID,
                D_ZONE_NAME
           FROM IMPORT_SAILING_LANE
          WHERE TC_COMPANY_ID = vTCCompanyId AND LANE_ID = vLaneId;
   BEGIN
      OPEN LANE_CURSOR;

     <<LANES_LOOP>>
      LOOP
         FETCH LANE_CURSOR
         INTO vBusinessUnit,
              vOLocType,
              vOFacilityId,
              vOFacilityAliasId,
              vOFacilityBU,
              vOCity,
              vOStateProv,
              vOCounty,
              vOPostal,
              vOCountry,
              vOZoneId,
              vOZoneName,
              vDLocType,
              vDFacilityId,
              vDFacilityAliasId,
              vDFacilityBU,
              vDCity,
              vDStateProv,
              vDCounty,
              vDPostal,
              vDCountry,
              vDZoneId,
              vDZoneName;

         EXIT LANES_LOOP WHEN LANE_CURSOR%NOTFOUND;

         COMMIT;

         /*  IF(
              vOCity is not null or
              vOStateProv is not null or
              vOCounty is not null or
              vOPostal is not null or
              vOCountry is not null or
              vOZone is not null or
              vDCity is not null or
              vDStateProv is not null or
              vDCounty is not null or
              vDPostal is not null or
              vDCountry is not null or
              vDZone is not null
              ) THEN

            vFailure := 1;

                INSERT_SAILING_LANE_ERROR(
                                           vTCCompanyId,
                                           vLaneId,
                                      'Sailing schedules can be created only for facility to facility lanes ',
                                           9920074,
                                           NULL
                                           );

            END IF;*/

         vPassFail :=
            VALIDATE_LANE_BASE_DATA (vTCCompanyId,
                                     vLaneId,
                                     vBusinessUnit,
                                     vOFacilityId,
                                     vOFacilityAliasId,
                                     vOFacilityBU,
                                     vOStateProv,
                                     vOPostal,
                                     vOCountry,
                                     vOZoneId,
                                     vOZoneName,
                                     vDFacilityId,
                                     vDFacilityAliasId,
                                     vDFacilityBU,
                                     vDStateProv,
                                     vDPostal,
                                     vDCountry,
                                     vDZoneId,
                                     vDZoneName);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
         END IF;

         vPassFail :=
            VALIDATE_HIERARCHY (vTCCompanyId       => vTCCompanyId,
                                vLaneId            => vLaneId,
                                oFacilityId        => vOFacilityId,
                                oFacilityAliasId   => vOFacilityAliasId,
                                oCity              => vOCity,
                                oState             => vOStateProv,
                                oCounty            => vOCounty,
                                oPostal            => vOPostal,
                                oCountry           => vOCountry,
                                oZoneId            => vOZoneId,
                                oZoneName          => vOZoneName,
                                dFacilityId        => vDFacilityId,
                                dFacilityAliasId   => vDFacilityAliasId,
                                dState             => vDStateProv,
                                dCity              => vDCity,
                                dCounty            => vDCounty,
                                dCountry           => vDCountry,
                                dPostal            => vDPostal,
                                dZoneId            => vDZoneId,
                                dZoneName          => vDZoneName);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
         END IF;

         IF (vFailure = 1)
         THEN
            UPDATE IMPORT_SAILING_LANE
               SET LANE_STATUS = 4,
                   LAST_UPDATED_DTTM = SYSDATE,
                   LAST_UPDATED_SOURCE_TYPE = 3,
                   LAST_UPDATED_SOURCE = 'OMS'
             WHERE TC_COMPANY_ID = vTCCompanyId AND LANE_ID = vLaneId;

            COMMIT;
         ELSE
            UPDATE IMPORT_SAILING_LANE
               SET LANE_STATUS = 1,
                   LAST_UPDATED_DTTM = SYSDATE,
                   LAST_UPDATED_SOURCE_TYPE = 3,
                   LAST_UPDATED_SOURCE = 'OMS'
             WHERE TC_COMPANY_ID = vTCCompanyId AND LANE_ID = vLaneId;

            COMMIT;
         END IF;

         VALIDATE_SAILING_LANE_DTL (vTcCompanyId, vLaneId);
      END LOOP LANES_LOOP;

      CLOSE LANE_CURSOR;
   END VALIDATE_SAILING_LANE;

   --------------------------------------------------------------------------------------
   FUNCTION VALIDATE_LANE_BASE_DATA (
      vTCCompanyId      IN company.COMPANY_ID%TYPE,
      vLaneId           IN sailing_lane.LANE_ID%TYPE,
      vBusinessUnit     IN business_unit.BUSINESS_UNIT%TYPE,
      vOFacilityId      IN facility.FACILITY_ID%TYPE,
      vOFacilityAlias   IN facility_alias.FACILITY_ALIAS_ID%TYPE,
      vOFacilityBU      IN business_unit.BUSINESS_UNIT%TYPE,
      vOStateProv       IN state_prov.STATE_PROV%TYPE,
      vOPostal          IN postal_code.POSTAL_CODE%TYPE,
      vOCountry         IN country.COUNTRY_CODE%TYPE,
      vOZoneId          IN import_sailing_lane.O_ZONE_ID%TYPE,
      vOZoneName        IN import_sailing_lane.O_ZONE_NAME%TYPE,
      vDFacilityId      IN facility.FACILITY_ID%TYPE,
      vDFacilityAlias   IN facility_alias.FACILITY_ALIAS_ID%TYPE,
      vDFacilityBU      IN business_unit.BUSINESS_UNIT%TYPE,
      vDStateProv       IN state_prov.STATE_PROV%TYPE,
      vDPostal          IN postal_code.POSTAL_CODE%TYPE,
      vDCountry         IN country.COUNTRY_CODE%TYPE,
      vDZoneId          IN import_sailing_lane.D_ZONE_ID%TYPE,
      vDZoneName        IN import_sailing_lane.D_ZONE_NAME%TYPE)
      RETURN NUMBER
   IS
      vPassFail        NUMBER;
      vFailure         NUMBER;
      vOTempFacility   NUMBER (8);
      vDTempFacility   NUMBER (8);
      vOFacilityBUID   NUMBER;
      vDFacilityBUID   NUMBER;
      VTCID            NUMBER;
      vCount           NUMBER;

      vRootCompanyId   COMPANY.COMPANY_ID%TYPE;
   BEGIN
      vFailure := 0;
      vCount := 0;
      VTCID := NULL;

      /* IF(vOCity is not null or vOStateProv is not null or vOCounty is not null
         or vOPostal is not null or vOCountry is not null or vOZone is not null or
         vDCity is not null or vDStateProv is not null or vDCounty is not null or
         vDPostal is not null or vDCountry is not null or vDZone is not null ) THEN

         vFailure := 1;

             INSERT_SAILING_LANE__ERROR(vTCCompanyId, vLaneId, vSailingLaneDtlSeq,
     'Sailing schedules can be created only for facility to facility lanes ',9920074,NULL);

         END IF; */

      IF (vBusinessUnit IS NOT NULL)
      THEN
         vPassFail := 0;

         SELECT COUNT (PARENT_COMPANY_ID)
           INTO vCount
           FROM COMPANY
          WHERE COMPANY_ID = vTCCompanyId;

         IF (vCount > 0)
         THEN
            SELECT PARENT_COMPANY_ID
              INTO VTCID
              FROM COMPANY
             WHERE COMPANY_ID = vTCCompanyId;
         END IF;

         vPassFail :=
            SAILING_SUB_VALIDATION_PKG.VALIDATE_BUSINESS_UNIT (VTCID,
                                                               vBusinessUnit);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_SAILING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vBusinessUnit || ' is an invalid Business Unit',
               4720007,
               vBusinessUnit);
         END IF;
      END IF;

      SELECT ROOTCOMPANYID
        INTO vRootCompanyId
        FROM IMPORT_SAILING_LANE
       WHERE LANE_ID = vLaneId;

      IF (vOFacilityBU IS NULL)
      THEN
         vOFacilityBUID := vRootCompanyId;
      ELSE
         vOFacilityBUID :=
            GET_BUID_FROM_BUSINESS_UNIT (vRootCompanyId, vOFacilityBU);
      END IF;

      IF (vOFacilityId IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail :=
            SAILING_SUB_VALIDATION_PKG.VALIDATE_FACILITY (vOFacilityBUID,
                                                          vOFacilityId);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_SAILING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vOFacilityId || ' is an invalid Origin Facility',
               4720008,
               vOFacilityId);
         END IF;
      END IF;

      IF (vOFacilityAlias IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail :=
            SAILING_SUB_VALIDATION_PKG.VALIDATE_FACILITY_ALIAS (
               vOFacilityBUID,
               vOFacilityAlias);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_SAILING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vOFacilityAlias || ' is an invalid Origin Facility Alias',
               4720009,
               vOFacilityId);
         ELSE
            IF (vOFacilityId IS NULL)
            THEN
               SELECT FACILITY_ID
                 INTO vOTempFacility
                 FROM FACILITY_ALIAS
                WHERE FACILITY_ALIAS_ID = vOFacilityAlias
                      AND TC_COMPANY_ID = vOFacilityBUID;

               UPDATE IMPORT_SAILING_LANE
                  SET O_FACILITY_ID = vOTempFacility
                WHERE O_FACILITY_ALIAS_ID = vOFacilityAlias
                      AND TC_COMPANY_ID = vTCCompanyId;

               COMMIT;
            END IF;
         END IF;
      END IF;

      IF (vOStateProv IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail :=
            SAILING_SUB_VALIDATION_PKG.VALIDATE_STATE_PROV (vOStateProv);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_SAILING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vOStateProv || ' is an invalid Origin State/Prov',
               4720010,
               vOStateProv);
         END IF;
      END IF;

      IF (vOCountry IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail := SAILING_SUB_VALIDATION_PKG.VALIDATE_COUNTRY (vOCountry);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_SAILING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vOCountry || ' is an invalid Origin Country',
               4720011,
               vOCountry);
         END IF;
      END IF;

      IF (vOZoneName IS NOT NULL)
      THEN
         IF (vOZoneId IS NULL)
         THEN
            vFailure := 1;
            INSERT_SAILING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vOZoneName || ' is an invalid Origin Zone',
               4720012,
               vOZoneName);
         END IF;
      END IF;

      IF (vDFacilityBU IS NULL)
      THEN
         vDFacilityBUID := vRootCompanyId;
      ELSE
         vDFacilityBUID :=
            GET_BUID_FROM_BUSINESS_UNIT (vRootCompanyId, vDFacilityBU);
      END IF;

      IF (vDFacilityId IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail :=
            SAILING_SUB_VALIDATION_PKG.VALIDATE_FACILITY (vDFacilityBUID,
                                                          vDFacilityId);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_SAILING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vDFacilityId || ' is an invalid Destination Facility',
               4720013,
               vDFacilityId);
         END IF;
      END IF;

      IF (vDFacilityAlias IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail :=
            SAILING_SUB_VALIDATION_PKG.VALIDATE_FACILITY_ALIAS (
               vDFacilityBUID,
               vDFacilityAlias);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_SAILING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vDFacilityAlias || ' is an invalid Destination Facility Alias',
               4720014,
               vDFacilityAlias);
         ELSE
            IF (vDFacilityId IS NULL)
            THEN
               SELECT FACILITY_ID
                 INTO vDTempFacility
                 FROM FACILITY_ALIAS
                WHERE FACILITY_ALIAS_ID = vDFacilityAlias
                      AND TC_COMPANY_ID = vDFacilityBUID;

               UPDATE IMPORT_SAILING_LANE
                  SET D_FACILITY_ID = vDTempFacility
                WHERE D_FACILITY_ALIAS_ID = vDFacilityAlias
                      AND TC_COMPANY_ID = vTCCompanyId;

               COMMIT;
            END IF;
         END IF;
      END IF;

      IF (vDStateProv IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail :=
            SAILING_SUB_VALIDATION_PKG.VALIDATE_STATE_PROV (vDStateProv);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_SAILING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vDStateProv || ' is an invalid Destination State/Prov',
               4720015,
               vDStateProv);
         END IF;
      END IF;

      IF (vDCountry IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail := SAILING_SUB_VALIDATION_PKG.VALIDATE_COUNTRY (vDCountry);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_SAILING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vDCountry || ' is an invalid Destination Country',
               4720016,
               vDCountry);
            COMMIT;
         END IF;
      END IF;

      IF (vDZoneName IS NOT NULL)
      THEN
         IF (vDZoneId IS NULL)
         THEN
            vFailure := 1;
            INSERT_SAILING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vDZoneName || ' is an invalid Destination Zone',
               4720017,
               vDZoneName);
         END IF;
      END IF;

      RETURN vFailure;
   END VALIDATE_LANE_BASE_DATA;

   --------------------------------------------------------------------------------------

   FUNCTION VALIDATE_HIERARCHY (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vLaneId            IN sailing_lane.LANE_ID%TYPE,
      oFacilityId        IN sailing_lane.O_FACILITY_ID%TYPE,
      oFacilityAliasId   IN sailing_lane.O_FACILITY_ALIAS_ID%TYPE,
      oCity              IN sailing_lane.O_CITY%TYPE,
      oState             IN sailing_lane.O_STATE_PROV%TYPE,
      oCounty            IN sailing_lane.O_COUNTY%TYPE,
      oPostal            IN sailing_lane.O_POSTAL_CODE%TYPE,
      oCountry           IN sailing_lane.O_COUNTRY_CODE%TYPE,
      oZoneId            IN import_sailing_lane.O_ZONE_ID%TYPE,
      oZoneName          IN import_sailing_lane.O_ZONE_NAME%TYPE,
      dFacilityId        IN sailing_lane.D_FACILITY_ID%TYPE,
      dFacilityAliasId   IN sailing_lane.D_FACILITY_ALIAS_ID%TYPE,
      dState             IN sailing_lane.D_STATE_PROV%TYPE,
      dCity              IN sailing_lane.D_CITY%TYPE,
      dCounty            IN sailing_lane.D_COUNTY%TYPE,
      dCountry           IN sailing_lane.D_COUNTRY_CODE%TYPE,
      dPostal            IN sailing_lane.D_POSTAL_CODE%TYPE,
      dZoneId            IN import_sailing_lane.D_ZONE_ID%TYPE,
      dZoneName          IN import_sailing_lane.D_ZONE_NAME%TYPE)
      RETURN NUMBER
   IS
      vPassFail        NUMBER;
      vDPassFail       NUMBER;
      vState           NUMBER;
      vOLocType        sailing_lane.O_LOC_TYPE%TYPE;
      vOLocTypeLH      VARCHAR2 (5);
      vDLocType        sailing_lane.D_LOC_TYPE%TYPE;
      vDLocTypeLH      VARCHAR2 (5);
      vOLocValue       sailing_lane_hierarchy.ORIGIN_VALUE%TYPE;
      vDLocValue       sailing_lane_hierarchy.DEST_VALUE%TYPE;
      vLHValue         NUMBER;
      vOLocError       error_message.ERROR_MSG_ID%TYPE;
      vDLocError       error_message.ERROR_MSG_ID%TYPE;
      vHierarchy       NUMBER;
      vOPostal         VARCHAR2 (5);
      vDPostal         VARCHAR2 (5);
      vAttributeType   NUMBER;
      vErrorMsg        VARCHAR2 (1000);
   BEGIN
      vPassFail := 0;

      vDPassFail := 0;

      IF (oState IS NOT NULL AND oCountry IS NOT NULL)
         AND (    oCity IS NULL
              AND oPostal IS NULL
              AND oCounty IS NULL
              AND oFacilityAliasId IS NULL
              AND oZoneId IS NULL)
      THEN
         vOLocType := 'ST';
         vOLocTypeLH := 'ST';
      ELSIF (oCity IS NOT NULL AND oCountry IS NOT NULL)
            AND (    oPostal IS NULL
                 AND oFacilityAliasId IS NULL
                 AND oZoneId IS NULL)
      THEN
         SELECT HAS_STATE_PROV
           INTO vState
           FROM COUNTRY
          WHERE COUNTRY_CODE = oCountry;

         IF (vState = 1)
         THEN
            IF (oState IS NOT NULL)
            THEN
               vOLocType := 'CS';
               vOLocTypeLH := 'CS';
            ELSE                       /* oState is null but it is required */
               vPassFail := 1;
            END IF;
         ELSIF (oState IS NOT NULL)
         THEN
            vPassFail := 1;
         ELSE
            vOLocType := 'CS';
            vOLocTypeLH := 'CS';
         END IF;
      ELSIF (oPostal IS NOT NULL AND oCountry IS NOT NULL)
            AND (    oCity IS NULL
                 AND oState IS NULL
                 AND oCounty IS NULL
                 AND oFacilityAliasId IS NULL
                 AND oZoneId IS NULL)
      THEN
         SELECT DECODE (LENGTH (oPostal),
                        2, 'P2',
                        3, 'P3',
                        4, 'P4',
                        5, 'P5',
                        6, 'P6',
                        'Other')
           INTO vOPostal
           FROM DUAL;

         IF (vOPostal != 'Other')
         THEN
            vOLocType := vOPostal;
            vOLocTypeLH := vOPostal;
         ELSE
            vPassFail := 1;
         END IF;
      ELSIF (oZoneId IS NOT NULL)
            AND (    oCity IS NULL
                 AND oState IS NULL
                 AND oPostal IS NULL
                 AND oCounty IS NULL
                 AND oCountry IS NULL
                 AND oFacilityAliasId IS NULL)
      THEN
         vOLocType := 'ZN';

         SELECT COUNT (ATTRIBUTE_TYPE)
           INTO vAttributeType
           FROM ZONE
          WHERE TC_COMPANY_ID = vTCCompanyId AND ZONE_ID = oZoneId;

         IF (vAttributeType > 0)
         THEN
            SELECT DECODE (ATTRIBUTE_TYPE,
                           'ST', 'ZST',
                           'FC', 'ZFA',
                           'CT', 'ZCO',
                           'Z2', 'Z2',
                           'Z3', 'Z3',
                           'Z4', 'Z4',
                           'Z5', 'Z5',
                           'Z6', 'Z6',
                           'PR', 'PR',
                           'CI', 'ZCS',
                           'Other')
              INTO vOLocTypeLH
              FROM ZONE
             WHERE TC_COMPANY_ID = vTCCompanyId AND ZONE_ID = oZoneId;

            IF (vOLocTypeLH = 'Other')
            THEN
               vPassFail := 1;
            END IF;
         ELSE
            vPassFail := 1;
         END IF;
      ELSIF (oFacilityAliasId IS NOT NULL)
            AND (    oCity IS NULL
                 AND oState IS NULL
                 AND oPostal IS NULL
                 AND oCounty IS NULL
                 AND oCountry IS NULL
                 AND oZoneId IS NULL)
      THEN
         vOLocType := 'FA';
         vOLocTypeLH := 'FA';
      ELSIF (oCountry IS NOT NULL)
            AND (    oCity IS NULL
                 AND oState IS NULL
                 AND oPostal IS NULL
                 AND oCounty IS NULL
                 AND oFacilityAliasId IS NULL
                 AND oZoneId IS NULL)
      THEN
         vOLocType := 'CO';
         vOLocTypeLH := 'CO';
      ELSE
         vOLocError := 320000;
         vPassFail := 1;
      END IF;

      /*In case we missed an else case, we should check if the loc types have been determined*/
      IF (vOLocTypeLH IS NULL OR vOLocType IS NULL)
      THEN
         vPassFail := 1;
      END IF;

      IF (vPassFail = 1)
      THEN
         INSERT_SAILING_LANE_ERROR (vTCCompanyId,
                                    vLaneId,
                                    'Origin Definition is invalid',
                                    4720018,
                                    NULL);
         COMMIT;
      END IF;

      -- Destination validation

      IF (dState IS NOT NULL AND dCountry IS NOT NULL)
         AND (    dCity IS NULL
              AND dPostal IS NULL
              AND dCounty IS NULL
              AND dFacilityAliasId IS NULL
              AND dZoneId IS NULL)
      THEN
         vDLocType := 'ST';
         vDLocTypeLH := 'ST';
      ELSIF (dCity IS NOT NULL AND dCountry IS NOT NULL)
            AND (    dPostal IS NULL
                 AND dFacilityAliasId IS NULL
                 AND dZoneId IS NULL)
      THEN
         SELECT HAS_STATE_PROV
           INTO vState
           FROM COUNTRY
          WHERE COUNTRY_CODE = dCountry;

         IF (vState = 1)
         THEN
            IF (dState IS NOT NULL)
            THEN
               vDLocType := 'CS';
               vDLocTypeLH := 'CS';
            ELSE                       /* dState is null but it is required */
               vDPassFail := 1;
            END IF;
         ELSIF (dState IS NOT NULL)
         THEN
            vDPassFail := 1;
         ELSE
            vDLocType := 'CS';
            vDLocTypeLH := 'CS';
         END IF;
      ELSIF (dPostal IS NOT NULL AND dCountry IS NOT NULL)
            AND (    dCity IS NULL
                 AND dState IS NULL
                 AND dCounty IS NULL
                 AND dFacilityAliasId IS NULL
                 AND dZoneId IS NULL)
      THEN
         SELECT DECODE (LENGTH (dPostal),
                        2, 'P2',
                        3, 'P3',
                        4, 'P4',
                        5, 'P5',
                        6, 'P6',
                        'Other')
           INTO vDPostal
           FROM DUAL;

         IF (vDPostal != 'Other')
         THEN
            vDLocType := vDPostal;
            vDLocTypeLH := vDPostal;
         ELSE
            vDPassFail := 1;
         END IF;
      ELSIF (dZoneId IS NOT NULL)
            AND (    dCity IS NULL
                 AND dState IS NULL
                 AND dPostal IS NULL
                 AND dCounty IS NULL
                 AND dCountry IS NULL
                 AND dFacilityAliasId IS NULL)
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'DESTINATION IS ZONE !!!' || dZoneId || 'done');
         vDLocType := 'ZN';

         SELECT COUNT (ATTRIBUTE_TYPE)
           INTO vAttributeType
           FROM ZONE
          WHERE TC_COMPANY_ID = vTCCompanyId AND ZONE_ID = dZoneId;

         IF (vAttributeType > 0)
         THEN
            SELECT DECODE (ATTRIBUTE_TYPE,
                           'ST', 'ZST',
                           'FC', 'ZFA',
                           'CT', 'ZCO',
                           'Z2', 'Z2',
                           'Z3', 'Z3',
                           'Z4', 'Z4',
                           'Z5', 'Z5',
                           'Z6', 'Z6',
                           'PR', 'PR',
                           'CI', 'ZCS',
                           'Other')
              INTO vDLocTypeLH
              FROM ZONE
             WHERE TC_COMPANY_ID = vTCCompanyId AND ZONE_ID = dZoneId;

            IF (vDLocTypeLH = 'Other')
            THEN
               vDPassFail := 1;
            END IF;
         ELSE
            vDPassFail := 1;
         END IF;
      ELSIF (dFacilityAliasId IS NOT NULL)
            AND (    dCity IS NULL
                 AND dState IS NULL
                 AND dPostal IS NULL
                 AND dCounty IS NULL
                 AND dCountry IS NULL
                 AND dZoneId IS NULL)
      THEN
         vDLocType := 'FA';
         vDLocTypeLH := 'FA';
      ELSIF (dCountry IS NOT NULL)
            AND (    dCity IS NULL
                 AND dState IS NULL
                 AND dPostal IS NULL
                 AND dCounty IS NULL
                 AND dFacilityAliasId IS NULL
                 AND dZoneId IS NULL)
      THEN
         vDLocType := 'CO';
         vDLocTypeLH := 'CO';
      ELSE
         vDLocError := 320000;
         vDPassFail := 1;
      END IF;

      /*In case we missed an else case, we should check if the loc types have been determined*/
      IF (vDLocTypeLH IS NULL OR vDLocType IS NULL)
      THEN
         vDPassFail := 1;
      END IF;

      IF (vDPassFail = 1)
      THEN
         INSERT_SAILING_LANE_ERROR (vTCCompanyId,
                                    vLaneId,
                                    'Destination Definition is invalid',
                                    4720019,
                                    NULL);
         COMMIT;
      END IF;

      IF (vDPassFail = 1)
      THEN
         vPassFail := 1;
      END IF;

      IF (vPassFail = 0)
      THEN
         -- calculate lane hierarchy
         BEGIN
            SELECT ORIGIN_VALUE
              INTO vOLocValue
              FROM SAILING_LANE_HIERARCHY
             WHERE TC_COMPANY_ID = vTCCompanyId
                   AND LOCATION_TYPE = vOLocTypeLH;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               vOLocValue := NULL;
         END;

         BEGIN
            SELECT DEST_VALUE
              INTO vDLocValue
              FROM SAILING_LANE_HIERARCHY
             WHERE TC_COMPANY_ID = vTCCompanyId
                   AND LOCATION_TYPE = vDLocTypeLH;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               vDLocValue := NULL;
         END;

         IF (vOLocValue IS NOT NULL AND vDLocValue IS NOT NULL)
         THEN
            vLHValue := vOLocValue + vDLocValue;
         ELSE
            -- calculate default hierarchy
            SELECT DECODE (vOLocTypeLH,
                           'FA', 100,
                           'ZFA', 200,
                           'P6', 300,
                           'Z6', 400,
                           'P5', 500,
                           'Z5', 600,
                           'CS', 700,
                           'ZCS', 750,
                           'P4', 800,
                           'Z4', 900,
                           'P3', 1000,
                           'Z3', 1100,
                           'P2', 1200,
                           'Z2', 1300,
                           'ST', 1400,
                           'ZST', 1500,
                           'CO', 1600,
                           'ZCO', 1700,
                           'PR', 1800,
                           0)
              INTO vOLocValue
              FROM DUAL;

            SELECT DECODE (vDLocTypeLH,
                           'FA', 101,
                           'ZFA', 202,
                           'P6', 303,
                           'Z6', 404,
                           'P5', 505,
                           'Z5', 606,
                           'CS', 707,
                           'ZCS', 757,
                           'P4', 808,
                           'Z4', 909,
                           'P3', 1010,
                           'Z3', 1111,
                           'P2', 1212,
                           'Z2', 1313,
                           'ST', 1414,
                           'ZST', 1515,
                           'CO', 1616,
                           'ZCO', 1717,
                           'PR', 1818,
                           0)
              INTO vDLocValue
              FROM DUAL;

            IF (vOLocValue = 0 OR vDLocValue = 0)
            THEN
               vPassFail := 1;
               vLHValue := 0;
            ELSE
               vLHValue := vOLocValue + vDLocValue;
               vLHValue := vLHValue + 4000 + 4100 + 4200;
            END IF;
         END IF;

         -- update the entry in the database

         UPDATE IMPORT_SAILING_LANE
            SET LANE_HIERARCHY = vLHValue,
                O_LOC_TYPE = vOLocType,
                D_LOC_TYPE = vDLocType
          WHERE TC_COMPANY_ID = vTCCompanyId AND LANE_ID = vLaneId;

         IF (vPassFail = 1)
         THEN
            INSERT_SAILING_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               'An error prevented the Lane Hierarchy value from being calculated.',
               4720020,
               NULL);
         END IF;

         COMMIT;
      END IF;

      RETURN vPassFail;
   END VALIDATE_HIERARCHY;

   --------------------------------------------------------------------------------------
   PROCEDURE INSERT_SAILING_LANE_ERROR (
      vTCCompanyId         IN company.COMPANY_ID%TYPE,
      vLaneId              IN sailing_lane.LANE_ID%TYPE,
      vErrorMsgDesc        IN sailing_lane_errors.ERROR_MSG_DESC%TYPE,
      p_error_msg_id       IN SAILING_LANE_ERRORS.ERROR_MSG_ID%TYPE DEFAULT NULL,
      p_error_param_list   IN SAILING_LANE_ERRORS.ERROR_PARAM_LIST%TYPE DEFAULT NULL)
   AS
      vBatchId   sailing_lane_errors.BATCH_ID%TYPE;
   BEGIN
      SELECT BATCH_ID
        INTO vBatchId
        FROM IMPORT_SAILING_LANE
       WHERE LANE_ID = vLaneId AND TC_COMPANY_ID = vTCCompanyId;

      INSERT INTO SAILING_LANE_ERRORS (SAILING_LANE_ERROR,
                                       TC_COMPANY_ID,
                                       LANE_ID,
                                       ERROR_MSG_DESC,
                                       BATCH_ID,
                                       ERROR_MSG_ID,
                                       ERROR_PARAM_LIST)
           VALUES (SEQ_SAILING_LANE_ERROR.NEXTVAL,
                   vTCCompanyId,
                   vLaneId,
                   vErrorMsgDesc,
                   vBatchId,
                   p_error_msg_id,
                   p_error_param_list);

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Procedure INSERT_SAILING_LANE_ERROR: ' || SQLERRM);
         RAISE;
   END INSERT_SAILING_LANE_ERROR;

   --------------------------------------------------------------------------------------
   PROCEDURE INSERT_SAILING_LANE_DTL_ERROR (
      vTCCompanyId         IN company.COMPANY_ID%TYPE,
      vLaneId              IN sailing_lane.LANE_ID%TYPE,
      vSailingLaneDtlSeq   IN sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE,
      vErrorMsgDesc        IN sailing_lane_errors.ERROR_MSG_DESC%TYPE,
      p_error_msg_id       IN SAILING_LANE_ERRORS.ERROR_MSG_ID%TYPE DEFAULT NULL,
      p_error_param_list   IN SAILING_LANE_ERRORS.ERROR_PARAM_LIST%TYPE DEFAULT NULL)
   AS
      vBatchId   sailing_lane_errors.BATCH_ID%TYPE;
   BEGIN
      SELECT BATCH_ID
        INTO vBatchId
        FROM IMPORT_SAILING_LANE
       WHERE LANE_ID = vLaneId AND TC_COMPANY_ID = vTCCompanyId;

      INSERT INTO SAILING_LANE_ERRORS (SAILING_LANE_ERROR,
                                       TC_COMPANY_ID,
                                       LANE_ID,
                                       SAILING_LANE_DTL_SEQ,
                                       ERROR_MSG_DESC,
                                       BATCH_ID,
                                       ERROR_MSG_ID,
                                       ERROR_PARAM_LIST)
           VALUES (SEQ_SAILING_LANE_ERROR.NEXTVAL,
                   vTCCompanyId,
                   vLaneId,
                   vSailingLaneDtlSeq,
                   vErrorMsgDesc,
                   vBatchId,
                   p_error_msg_id,
                   p_error_param_list);

      COMMIT;
   END INSERT_SAILING_LANE_DTL_ERROR;

   -------------------------------------------------------------------------------------
   FUNCTION GET_PARENT_BU_IDS (vTCCompanyId IN company.COMPANY_ID%TYPE)
      RETURN VARCHAR2
   IS
      vBUList            VARCHAR2 (10000);
      vParentCompanyId   company.COMPANY_ID%TYPE;
   BEGIN
      vBUList := '';
      vParentCompanyId := NULL;

      IF vTCCompanyId IS NULL
      THEN
         RETURN vBUList;
      END IF;

      BEGIN
         SELECT PARENT_COMPANY_ID
           INTO vParentCompanyId
           FROM COMPANY
          WHERE COMPANY_ID = vTcCompanyId;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            vParentCompanyId := NULL;
      END;

      IF (vParentCompanyId IS NULL)
      THEN
         vBUList := TO_CHAR (vTcCompanyId);
      ELSE
         vBUList :=
               TO_CHAR (vTcCompanyId)
            || ','
            || GET_PARENT_BU_IDS (vParentCompanyId);
      END IF;

      RETURN vBUList;
   END GET_PARENT_BU_IDS;

   ------------------------------------------------------------------------------------------
   FUNCTION VALIDATE_FAC_CARR_FEASIBLE (
      vTCCompanyId               IN company.COMPANY_ID%TYPE,
      vLaneId                    IN sailing_lane.LANE_ID%TYPE,
      vSailingDtlSeq             IN sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE,
      vCarrierCode               IN carrier_code.CARRIER_CODE%TYPE,
      vCarrierCodeBUID           IN company.COMPANY_ID%TYPE,
      p_scndr_carrier_code       IN CARRIER_CODE.CARRIER_CODE%TYPE,
      p_scndr_carrier_codeBUID   IN company.COMPANY_ID%TYPE)
      RETURN NUMBER
   IS
      vCount              NUMBER;
      vPassFail           NUMBER;
      vCarrierId          NUMBER;
      p_scndr_carrierId   NUMBER;
      vBUId               NUMBER;
      vInfeasibilitySQL   VARCHAR2 (20000);
      vBuList             VARCHAR2 (10000);
      vErrStr             VARCHAR2 (100);
   BEGIN
      SELECT COUNT (*)
        INTO vCount
        FROM carrier_code
       WHERE tc_company_id = vCarrierCodeBUID AND carrier_code = vCarrierCode;

      IF (vCount > 0)
      THEN
         SELECT carrier_id
           INTO vCarrierId
           FROM carrier_code
          WHERE tc_company_id = vCarrierCodeBUID
                AND carrier_code = vCarrierCode;
      END IF;

      SELECT COUNT (*)
        INTO vCount
        FROM carrier_code
       WHERE tc_company_id = p_scndr_carrier_codeBUID
             AND carrier_code = p_scndr_carrier_code;

      IF (vCount > 0)
      THEN
         SELECT carrier_id
           INTO p_scndr_carrierId
           FROM carrier_code
          WHERE tc_company_id = p_scndr_carrier_codeBUID
                AND carrier_code = p_scndr_carrier_code;
      END IF;

      -- Get the Parent BU IDS as comma separated string
      /*      vBUList := GET_PARENT_BU_IDS(vTCCompanyId);
            IF vBUList IS NOT NULL THEN
                BEGIN
                vInfeasibilitySQL := 'SELECT count(INFEASIBILITY_ID) ';
                vInfeasibilitySQL :=  vInfeasibilitySQL || ' FROM INFEASIBILITY ';
                vInfeasibilitySQL :=  vInfeasibilitySQL || ' WHERE INFEASIBILITY_TYPE = ''FATP'' ';
                vInfeasibilitySQL :=  vInfeasibilitySQL || ' AND VALUE1 = (SELECT FACILITY_ID ';
                vInfeasibilitySQL :=  vInfeasibilitySQL || ' FROM FACILITY_ALIAS ';
                vInfeasibilitySQL :=  vInfeasibilitySQL || ' WHERE FACILITY_ALIAS_ID = (SELECT O_FACILITY_ALIAS_ID ';
                vInfeasibilitySQL :=  vInfeasibilitySQL || ' FROM IMPORT_SAILING_LANE ';
                vInfeasibilitySQL :=  vInfeasibilitySQL || ' WHERE LANE_ID = '|| vLaneId || ' ';
                vInfeasibilitySQL :=  vInfeasibilitySQL || ' AND TC_COMPANY_ID = '|| vTCCompanyId || '  ) ';
                vInfeasibilitySQL :=  vInfeasibilitySQL || ' AND TC_COMPANY_ID IN ('|| vBUList || ')) ';
                vInfeasibilitySQL :=  vInfeasibilitySQL || ' AND value2 IN (''' || to_char(vCarrierId) || '''';
                IF p_scndr_carrierId IS NOT NULL THEN
                   vInfeasibilitySQL :=  vInfeasibilitySQL || ' , ''' || to_char(p_scndr_carrierId) || '''';
                END IF;
                vInfeasibilitySQL :=  vInfeasibilitySQL || ' ) ';
                execute immediate vInfeasibilitySQL into vCount;
                END;
            ELSE */
      SELECT COUNT (INFEASIBILITY_ID)
        INTO vCount
        FROM INFEASIBILITY
       WHERE INFEASIBILITY_TYPE = 'FATP'
             AND VALUE1 = (SELECT O_FACILITY_ID
                             FROM IMPORT_SAILING_LANE
                            WHERE LANE_ID = vLaneId)
             AND VALUE2 IN
                    (TO_CHAR (vCarrierId), TO_CHAR (p_scndr_carrierId))
             AND TC_COMPANY_ID = vTCCompanyId;

      --  END IF;
      IF (vCount > 0)
      THEN
         vPassFail := 1;

         IF (p_scndr_carrier_code IS NULL)
         THEN
            vErrStr := vCarrierCode;
         ELSE
            vErrStr := vCarrierCode || ' or ' || p_scndr_carrier_code;
         END IF;

         INSERT_SAILING_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vSailingDtlSeq,
            'Origin Facility is infeasible with carrier ' || vErrStr,
            4720048,
            vCarrierCode || '<sep>' || p_scndr_carrier_code);
      END IF;

      /*    IF vBUList IS NOT NULL THEN
              vInfeasibilitySQL := 'SELECT count(INFEASIBILITY_ID) ';
              vInfeasibilitySQL :=  vInfeasibilitySQL || ' FROM INFEASIBILITY ';
              vInfeasibilitySQL :=  vInfeasibilitySQL || ' WHERE INFEASIBILITY_TYPE = ''FATP'' ';
              vInfeasibilitySQL :=  vInfeasibilitySQL || ' AND VALUE1 = (SELECT FACILITY_ID ';
              vInfeasibilitySQL :=  vInfeasibilitySQL || ' FROM FACILITY_ALIAS ';
              vInfeasibilitySQL :=  vInfeasibilitySQL || ' WHERE FACILITY_ALIAS_ID = (SELECT D_FACILITY_ALIAS_ID ';
              vInfeasibilitySQL :=  vInfeasibilitySQL || ' FROM IMPORT_SAILING_LANE ';
              vInfeasibilitySQL :=  vInfeasibilitySQL || ' WHERE LANE_ID = '|| vLaneId || ' ';
              vInfeasibilitySQL :=  vInfeasibilitySQL || ' AND TC_COMPANY_ID = '|| vTCCompanyId || '  ) ';
              vInfeasibilitySQL :=  vInfeasibilitySQL || ' AND TC_COMPANY_ID IN ('|| vBUList || ')) ';
              vInfeasibilitySQL :=  vInfeasibilitySQL || ' AND value2 IN (''' || to_char(vCarrierId) || '''';
              IF p_scndr_carrierId IS NOT NULL THEN
                 vInfeasibilitySQL :=  vInfeasibilitySQL || ' , ''' || to_char(p_scndr_carrierId) || '''';
              END IF;
              vInfeasibilitySQL :=  vInfeasibilitySQL || ' ) ';
              execute immediate vInfeasibilitySQL into vCount;
          ELSE */
      SELECT COUNT (INFEASIBILITY_ID)
        INTO vCount
        FROM INFEASIBILITY
       WHERE INFEASIBILITY_TYPE = 'FATP'
             AND VALUE1 = (SELECT D_FACILITY_ID
                             FROM IMPORT_SAILING_LANE
                            WHERE LANE_ID = vLaneId)
             AND VALUE2 IN
                    (TO_CHAR (vCarrierId), TO_CHAR (p_scndr_carrierId))
             AND TC_COMPANY_ID = vTCCompanyId;

      --  END IF;

      IF (vCount > 0)
      THEN
         vPassFail := 1;

         IF (p_scndr_carrier_code IS NULL)
         THEN
            vErrStr := vCarrierCode;
         ELSE
            vErrStr := vCarrierCode || ' or ' || p_scndr_carrier_code;
         END IF;

         INSERT_SAILING_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vSailingDtlSeq,
            'Destination Facility is infeasible with carrier ' || vErrStr,
            4720049,
            vCarrierCode || '<sep>' || p_scndr_carrier_code);
      END IF;

      RETURN vPassFail;
   END VALIDATE_FAC_CARR_FEASIBLE;

   --------------------------------------------------------------------------------------------------------------
   FUNCTION VALIDATE_TP_SERV_MODE (
      vTCCompanyId           IN company.COMPANY_ID%TYPE,
      vLaneId                IN sailing_lane.LANE_ID%TYPE,
      vSailingDtlSeq         IN sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE,
      vCarrierCode           IN carrier_code.CARRIER_CODE%TYPE,
      vServiceLevel          IN service_level.SERVICE_LEVEL%TYPE,
      vMode                  IN mot.MOT%TYPE,
      p_scndr_carrier_code   IN CARRIER_CODE.CARRIER_CODE%TYPE,
      vCarrierCodeId         IN carrier_code.CARRIER_ID%TYPE,
      vServiceLevelId        IN service_level.SERVICE_LEVEL_ID%TYPE,
      vModeId                IN mot.MOT_ID%TYPE,
      p_scndr_carrier_Id     IN CARRIER_CODE.CARRIER_ID%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
      vCount      NUMBER;
   BEGIN
      /*

         S.Lal  05/21/02

         If the carrier code is null (budgeted carrier) then no Validation is required.

      */

      vPassFail := 0;

      IF (vCarrierCode IS NOT NULL)
      THEN
         IF (vServiceLevel IS NOT NULL)
         THEN
            SELECT COUNT (1)
              INTO vCount
              FROM TP_COMPANY_SERVICE_LEVEL
             WHERE CARRIER_ID IN (vCarrierCodeId, p_scndr_carrier_Id)
                   AND SERVICE_LEVEL_ID = vServiceLevelId;

            IF (vCount < 1)
            THEN
               vPassFail := 1;

               INSERT_SAILING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vSailingDtlSeq,
                     '('
                  || vCarrierCode
                  || ' or '
                  || p_scndr_carrier_code
                  || ') and '
                  || vServiceLevel
                  || ' is not feasible (Carrier_2Carrier/ServiceLevel)',
                  4720041,
                     vCarrierCode
                  || '<sep>'
                  || p_scndr_carrier_code
                  || '<sep>'
                  || vServiceLevel);
            END IF;
         END IF;

         IF (vMode IS NOT NULL)
         THEN
            SELECT COUNT (1)
              INTO vCount
              FROM CARRIER_CODE_MOT
             WHERE CARRIER_ID IN (vCarrierCodeId, p_scndr_carrier_Id)
                   AND MOT_ID = vModeId;

            IF (vCount < 1)
            THEN
               vPassFail := 1;

               INSERT_SAILING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vSailingDtlSeq,
                     '('
                  || vCarrierCode
                  || ' or '
                  || p_scndr_carrier_code
                  || ') and '
                  || vMode
                  || ' is not feasible (Carrier_2Carrier/MOT)',
                  4720042,
                     vCarrierCode
                  || '<sep>'
                  || p_scndr_carrier_code
                  || '<sep>'
                  || vMode);
            END IF;
         END IF;
      END IF;

      RETURN vPassFail;
   END VALIDATE_TP_SERV_MODE;

   ---------------------------------------------------------------------------------------------------------------
   PROCEDURE VALIDATE_SAILING_LANE_DTL (
      vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
      vLaneId        IN sailing_lane.lane_id%TYPE)
   AS
      vCarrierCode               IMPORT_SAILING_LANE_DTL.CARRIER_CODE%TYPE;
      vMode                      IMPORT_SAILING_LANE_DTL.MOT%TYPE;
      vServiceLevel              IMPORT_SAILING_LANE_DTL.SERVICE_LEVEL%TYPE;
      vCarrierCodeBU             IMPORT_SAILING_LANE_DTL.TP_CODE_BU%TYPE;
      vModeBU                    IMPORT_SAILING_LANE_DTL.MOT_BU%TYPE;
      vServiceLevelBU            IMPORT_SAILING_LANE_DTL.SERVICE_LEVEL_BU%TYPE;
      vPassFail                  NUMBER;
      vFailure                   NUMBER;
      vSailingLaneDtlSeq         sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE;
      vEffectiveDt               sailing_lane_dtl.effective_dt%TYPE;
      vExpirationDt              sailing_lane_dtl.expiration_dt%TYPE;
      l_scndr_carrier_code       IMPORT_SAILING_LANE_DTL.SCNDR_CARRIER_CODE%TYPE;
      l_scndr_carrier_codeBU     IMPORT_SAILING_LANE_DTL.SCNDR_CARRIER_CODE_BU%TYPE;
      vCarrierCodeBUID           COMPANY.COMPANY_ID%TYPE;
      vModeBUID                  COMPANY.COMPANY_ID%TYPE;
      vServiceLevelBUID          COMPANY.COMPANY_ID%TYPE;
      l_scndr_carrier_codeBUID   COMPANY.COMPANY_ID%TYPE;
      vErrorCount                PLS_INTEGER;                --[TT #45901 fix]
      vRootCompanyId             COMPANY.COMPANY_ID%TYPE;
      vTrasitTime                IMPORT_SAILING_LANE_DTL.Fixed_Transit_Value%TYPE;
      vTransitTimeUom            IMPORT_SAILING_LANE_DTL.Fixed_Transit_Standard_Uom%TYPE;
      vSailingFrequencyType      IMPORT_SAILING_LANE_DTL.Sailing_Frequency_Type%TYPE;
      vSailingScheduleName       IMPORT_SAILING_LANE_DTL.Sailing_Schedule_Name%TYPE;

      vTempOFacilityAliasId      import_sailing_lane.O_FACILITY_ALIAS_ID%TYPE;
      vTempOCity                 import_sailing_lane.O_CITY%TYPE;
      vTempOStateProv            import_sailing_lane.O_STATE_PROV%TYPE;
      vTempOCounty               import_sailing_lane.O_COUNTY%TYPE;
      vTempOPostal               import_sailing_lane.O_POSTAL_CODE%TYPE;
      vTempOCountry              import_sailing_lane.O_COUNTRY_CODE%TYPE;
      vTempOZone                 import_sailing_lane.O_ZONE_ID%TYPE;
      vTempDFacilityAliasId      import_sailing_lane.D_FACILITY_ALIAS_ID%TYPE;
      vTempDCity                 import_sailing_lane.D_CITY%TYPE;
      vTempDStateProv            import_sailing_lane.D_STATE_PROV%TYPE;
      vTempDCounty               import_sailing_lane.D_COUNTY%TYPE;
      vTempDPostal               import_sailing_lane.D_POSTAL_CODE%TYPE;
      vTempDCountry              import_sailing_lane.D_COUNTRY_CODE%TYPE;
      vTempDZone                 import_sailing_lane.D_ZONE_ID%TYPE;

      CURSOR LANE_DTL_CURSOR
      IS
         SELECT CARRIER_CODE,
                TP_CODE_BU,
                MOT,
                MOT_BU,
                SERVICE_LEVEL,
                SERVICE_LEVEL_BU,
                SAILING_LANE_DTL_SEQ,
                EFFECTIVE_DT,
                EXPIRATION_DT,
                SCNDR_CARRIER_CODE,
                SCNDR_CARRIER_CODE_BU,
                FIXED_TRANSIT_VALUE,
                FIXED_TRANSIT_STANDARD_UOM,
                SAILING_FREQUENCY_TYPE,
                SAILING_SCHEDULE_NAME
           FROM IMPORT_SAILING_LANE_DTL
          WHERE     TC_COMPANY_ID = vTCCompanyId
                AND LANE_ID = vLaneId
                AND LANE_DTL_STATUS = 3;
   --AND LANE_DTL_STATUS in (3, 4);
   BEGIN
      OPEN LANE_DTL_CURSOR;

     <<LANE_DTL_LOOP>>
      LOOP
         FETCH LANE_DTL_CURSOR
         INTO vCarrierCode,
              vCarrierCodeBU,
              vMode,
              vModeBU,
              vServiceLevel,
              vServiceLevelBU,
              vSailingLaneDtlSeq,
              vEffectiveDt,
              vExpirationDt,
              l_scndr_carrier_code,
              l_scndr_carrier_codeBU,
              vTrasitTime,
              vTransitTimeUom,
              vSailingFrequencyType,
              vSailingScheduleName;

         EXIT LANE_DTL_LOOP WHEN LANE_DTL_CURSOR%NOTFOUND;

         vPassFail := 0;
         vFailure := 0;

         SELECT O_FACILITY_ALIAS_ID, D_FACILITY_ALIAS_ID
           INTO vTempOFacilityAliasId, vTempDFacilityAliasId
           FROM IMPORT_SAILING_LANE
          WHERE LANE_ID = vLaneId AND TC_COMPANY_ID = vTCCompanyId;

         SELECT ROOTCOMPANYID
           INTO vRootCompanyId
           FROM IMPORT_SAILING_LANE
          WHERE LANE_ID = vLaneId;

         IF (vCarrierCodeBU IS NULL)
         THEN
            vCarrierCodeBUID := vRootCompanyId;
         ELSE
            vCarrierCodeBUID :=
               GET_BUID_FROM_BUSINESS_UNIT (vRootCompanyId, vCarrierCodeBU);
         END IF;

         IF (vModeBU IS NULL)
         THEN
            vModeBUID := vRootCompanyId;
         ELSE
            vModeBUID := GET_BUID_FROM_BUSINESS_UNIT (vRootCompanyId, vModeBU);
         END IF;

         IF (vServiceLevelBU IS NULL)
         THEN
            vServiceLevelBUID := vRootCompanyId;
         ELSE
            vServiceLevelBUID :=
               GET_BUID_FROM_BUSINESS_UNIT (vRootCompanyId, vServiceLevelBU);
         END IF;

         IF (l_scndr_carrier_codeBU IS NULL)
         THEN
            l_scndr_carrier_codeBUID := vRootCompanyId;
         ELSE
            l_scndr_carrier_codeBUID :=
               GET_BUID_FROM_BUSINESS_UNIT (vRootCompanyId,
                                            l_scndr_carrier_codeBU);
         END IF;

         IF (   vTempOFacilityAliasId IS NULL
             OR LTRIM (RTRIM (vTempOFacilityAliasId)) = ''
             OR vTempDFacilityAliasId IS NULL
             OR LTRIM (RTRIM (vTempDFacilityAliasId)) = '')
         THEN
            IF ( (vSailingScheduleName IS NOT NULL
                  AND LTRIM (RTRIM (vSailingScheduleName)) <> '')
                OR vSailingFrequencyType > 0)
            THEN
               vFailure := 1;
               INSERT_SAILING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vSailingLaneDtlSeq,
                  'Sailing schedules can be created only for facility to facility lanes ',
                  9920074,
                  NULL);
            END IF;
         END IF;

         IF (vEffectiveDt IS NULL)
         THEN
            vFailure := 1;

            INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingLaneDtlSeq,
               'Effective date is null in transit lane detail',
               9920058,
               NULL);
         END IF;

         IF (vExpirationDt IS NULL)
         THEN
            vFailure := 1;

            INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingLaneDtlSeq,
               'Expiration date is null in transit lane detail',
               9920059,
               NULL);
         END IF;

         IF (vCarrierCode IS NULL)
         THEN
            vFailure := 1;

            INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingLaneDtlSeq,
               'TP Code is null in transit lane detail',
               9920061,
               NULL);
         END IF;

         IF (vMode IS NULL)
         THEN
            vFailure := 1;

            INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingLaneDtlSeq,
               'Mode cannot be blank in the transit lane detail',
               9920060,
               NULL);
         END IF;

         IF (vTrasitTime >= 1 AND vSailingFrequencyType >= 1)
         THEN
            vFailure := 1;

            INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingLaneDtlSeq,
               'Either Fixed Transit Time or Sailing Frequency Type can be provided in transit lane detail',
               9920062,
               NULL);
         END IF;

         IF (vTrasitTime < 1 AND vSailingFrequencyType >= 1)
         THEN
            IF (vSailingFrequencyType != 8 AND vSailingFrequencyType != 4)
            THEN
               vFailure := 1;

               INSERT_SAILING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vSailingLaneDtlSeq,
                  'Sailing Frequency Type cannot be other than Date or Weekly in transit lane detail ',
                  9920063,
                  NULL);
            END IF;
         END IF;
		 
		 IF(vTrasitTime IS NULL AND vTrasitTime = 0)
         THEN
           vfailure := 1;
           INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingLaneDtlSeq,
               'Please enter either sailing information or transit time details',
               1700073,
               NULL);
         END IF;


         --satya
         IF (vTrasitTime IS NOT NULL AND vTrasitTime < 0)
         THEN
            vfailure := 1;
            INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingLaneDtlSeq,
               'Transit Time cannot have a negative value.',
               9920075,
               NULL);
         END IF;

         IF (vTransitTimeUom IS NOT NULL AND vSailingFrequencyType IS NULL)
         THEN
            IF (vTransitTimeUom != 166 AND vTransitTimeUom != 404)
            THEN
               vFailure := 1;

               INSERT_SAILING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vSailingLaneDtlSeq,
                  'Transit Time Uom cannot be other than Business Hour or Business Day in transit lane detail ',
                  9920064,
                  NULL);
            END IF;
         END IF;

         vPassFail :=
            VALIDATE_DETAIL_BASE_DATA (vTCCompanyId,
                                       vLaneId,
                                       vSailingLaneDtlSeq,
                                       vCarrierCode,
                                       vCarrierCodeBUID,
                                       vMode,
                                       vModeBUID,
                                       vServiceLevel,
                                       vServiceLevelBUID,
                                       l_scndr_carrier_code,
                                       l_scndr_carrier_codeBUID);

         IF (vPassFail = 1)
         THEN
            DBMS_OUTPUT.put_line ('Validate Detail Base Data Failed');
            vFailure := 1;
         /* ELSE
              vPassFail :=  VALIDATE_TP_SERV_MODE
             (
              vTCCompanyId,
               vLaneId,
              vSailingLaneDtlSeq,
              vCarrierCode,
              vServiceLevel,
              vMode,
              l_scndr_carrier_code
             );
           IF (vPassFail = 1) THEN
            dbms_output.put_line('Validate TP SERV Mode Failed');
            vFailure := 1;
           END IF;
         */

         END IF;

         vPassFail :=
            VALIDATE_FAC_CARR_FEASIBLE (vTCCompanyId,
                                        vLaneId,
                                        vSailingLaneDtlSeq,
                                        vCarrierCode,
                                        vCarrierCodeBUID,
                                        l_scndr_carrier_code,
                                        l_scndr_carrier_codeBUID);

         IF (vPassFail = 1)
         THEN
            DBMS_OUTPUT.put_line ('Validate Feasible Failed');
            vFailure := 1;
         END IF;

         /*
             Check Whether the effective date for the detail records is greater
             than the expiration record. If it is true, insert into the
             error log table
         */

         IF vEffectiveDt > vExpirationDt
         THEN
            vFailure := 1;

            INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingLaneDtlSeq,
               'Effective Date is greater than the Expiration Date',
               4720034,
               NULL);
         END IF;

         IF (vSailingFrequencyType = 8)
         THEN
            vPassFail :=
               VALIDATE_SAILING_SCHDL_WEEKLY (vTCCompanyId,
                                              vLaneId,
                                              vSailingLaneDtlSeq);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
            END IF;
         END IF;

         IF (vSailingFrequencyType = 4)
         THEN
            vPassFail :=
               VALIDATE_SAILING_SCHDL_BY_DATE (vTCCompanyId,
                                               vLaneId,
                                               vSailingLaneDtlSeq);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
            END IF;
         END IF;

         --[TT #45901 fix]
         BEGIN
            SELECT COUNT (*)
              INTO vErrorCount
              FROM SAILING_LANE_ERRORS
             WHERE     tc_company_id = vTCCompanyId
                   AND LANE_ID = vLaneId
                   AND SAILING_LANE_DTL_SEQ = vSailingLaneDtlSeq
                   AND BATCH_ID =
                          (SELECT BATCH_ID
                             FROM IMPORT_SAILING_LANE
                            WHERE LANE_ID = vLaneId
                                  AND TC_COMPANY_ID = vTCCompanyId);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               vErrorCount := 0;
         END;

         -- If lane has errors in this batch then do not set lane status as 1.
         IF (vFailure = 1 OR vErrorCount > 0)
         THEN
            UPDATE IMPORT_SAILING_LANE_DTL
               SET lane_dtl_status = 4, import_sailing_dtl_status = 4
             WHERE     TC_COMPANY_ID = vTCCompanyId
                   AND LANE_ID = vLaneId
                   AND SAILING_LANE_DTL_SEQ = vSailingLaneDtlSeq;

            /*9900 - doubtful
                --Updating the Parent Lane to
                --Invalid When the Lane details are
                --Invalid
                UPDATE IMPORT_SAILING_LANE
                 SET lane_status = 4
                 WHERE TC_COMPANY_ID = vTCCompanyId
                 AND LANE_ID = vLaneId;
            */
            COMMIT;
         ELSE
            UPDATE IMPORT_SAILING_LANE_DTL
               SET lane_dtl_status = 1, import_sailing_dtl_status = 0
             WHERE     TC_COMPANY_ID = vTCCompanyId
                   AND LANE_ID = vLaneId
                   AND SAILING_LANE_DTL_SEQ = vSailingLaneDtlSeq;

            COMMIT;
         END IF;
      END LOOP LANE_DTL_LOOP;

      CLOSE LANE_DTL_CURSOR;
   END VALIDATE_SAILING_LANE_DTL;

   --------------------------------------------------------------------------------------

   FUNCTION CHECK_IS_BU_VALID (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                               vBUID          IN company.COMPANY_ID%TYPE)
      RETURN NUMBER
   IS
      vParentCompanyId   NUMBER;
      vTcCompanyId1      NUMBER;
      vPassFail          NUMBER;
      vCount1            NUMBER;
   BEGIN
      vPassFail := 1;
      vParentCompanyId := 0;
      vTcCompanyId1 := vTCCompanyId;

      IF (vTcCompanyId1 = vBUID)
      THEN
         vPassFail := 0;
      END IF;

      WHILE (vParentCompanyId != -1)
      LOOP
         SELECT PARENT_COMPANY_ID
           INTO vParentCompanyId
           FROM COMPANY
          WHERE COMPANY_ID = vTcCompanyId1;

         vTcCompanyId1 := vParentCompanyId;

         IF (vTcCompanyId1 != -1 AND vTcCompanyId1 = vBUID)
         THEN
            vPassFail := 0;
         END IF;
      END LOOP;

      RETURN vPassFail;
   END CHECK_IS_BU_VALID;

   --------------------------------------------------------------------------------------

   FUNCTION VALIDATE_DETAIL_BASE_DATA (
      vTCCompanyId               IN company.COMPANY_ID%TYPE,
      vLaneId                    IN sailing_lane.LANE_ID%TYPE,
      vSailingDtlSeq             IN sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE,
      vCarrierCode               IN IMPORT_SAILING_LANE_DTL.CARRIER_CODE%TYPE,
      vCarrierCodeBUID           IN company.COMPANY_ID%TYPE,
      vMode                      IN IMPORT_SAILING_LANE_DTL.MOT%TYPE,
      vModeBUID                  IN company.COMPANY_ID%TYPE,
      vServiceLevel              IN IMPORT_SAILING_LANE_DTL.SERVICE_LEVEL%TYPE,
      vServiceLevelBUID          IN company.COMPANY_ID%TYPE,
      p_scndr_carrier_code       IN CARRIER_CODE.CARRIER_CODE%TYPE,
      p_scndr_carrier_codeBUID   IN company.COMPANY_ID%TYPE)
      RETURN NUMBER
   IS
      vPassFail            NUMBER;
      vFailure             NUMBER;
      vBUId                NUMBER;
	  vCount			   NUMBER;
      vCarrierId           sailing_lane_dtl.CARRIER_ID%TYPE;
      vModeId              sailing_lane_dtl.MOT_ID%TYPE;
      vServiceLevelId      sailing_lane_dtl.SERVICE_LEVEL_ID%TYPE;
      p_scndr_carrier_Id   sailing_lane_dtl.SCNDR_CARRIER_ID%TYPE;
   BEGIN
      vPassFail := 0;
      vFailure := 0;
      vCarrierId := 0;
      vModeId := 0;
      vServiceLevelId := 0;
      p_scndr_carrier_Id := 0;

      vPassFail := CHECK_IS_BU_VALID (vTCCompanyId, vCarrierCodeBUID);

      IF (vPassFail = 1)
      THEN
         vFailure := 1;
         INSERT_SAILING_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vSailingDtlSeq,
            vCarrierCode || ' is an invalid Carrier Code',
            4720035,
            vCarrierCode);
      ELSE
         vPassFail :=
            SAILING_SUB_VALIDATION_PKG.VALIDATE_CARRIER_CODE (
               vCarrierCodeBUID,
               vCarrierCode);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingDtlSeq,
               vCarrierCode || ' is an invalid Carrier Code',
               4720035,
               vCarrierCode);
         ELSE
            IF (vCarrierId = 0)
            THEN
               SELECT carrier_id
                 INTO vCarrierId
                 FROM carrier_code
                WHERE carrier_code = vCarrierCode
                      AND tc_company_id = vCarrierCodeBUID;

               UPDATE IMPORT_SAILING_LANE_DTL
                  SET carrier_id = vCarrierId
                WHERE     carrier_code = vCarrierCode
                      AND tc_company_id = vTCCompanyId
                      AND lane_id = vLaneId
                      AND SAILING_LANE_DTL_SEQ = vSailingDtlSeq;
					  
			   SELECT COUNT(CARRIER_ID) INTO vCount FROM CARRIER_CODE WHERE CARRIER_ID = vCarrierId AND carrier_type_id = 24;
			   IF (vCount > 0)
			   THEN
				  vFailure := 1;
				  INSERT_SAILING_LANE_DTL_ERROR (
					  vTCCompanyId,
					  vLaneId,
					  vSailingDtlSeq,
					  'Transit lane detail cannot be created for External Parcel Carrier '||vCarrierCode,
					  4720204,
					  vCarrierCode);
			   END IF;
            END IF;
         END IF;
      END IF;

      IF (vMode IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail := CHECK_IS_BU_VALID (vTCCompanyId, vModeBUID);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_SAILING_LANE_DTL_ERROR (vTCCompanyId,
                                           vLaneId,
                                           vSailingDtlSeq,
                                           vMode || ' is an invalid Mode',
                                           4720036,
                                           vMode);
         ELSE
            vPassFail :=
               SAILING_SUB_VALIDATION_PKG.VALIDATE_MODE (vModeBUID, vMode);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_SAILING_LANE_DTL_ERROR (vTCCompanyId,
                                              vLaneId,
                                              vSailingDtlSeq,
                                              vMode || ' is an invalid Mode',
                                              4720036,
                                              vMode);
            ELSE
               IF (vModeId = 0)
               THEN
                  SELECT mot_id
                    INTO vModeId
                    FROM mot
                   WHERE mot = vMode AND tc_company_id = vModeBUID;

                  UPDATE IMPORT_SAILING_LANE_DTL
                     SET mot_id = vModeId
                   WHERE     mot = vMode
                         AND tc_company_id = vTCCompanyId
                         AND lane_id = vLaneId
                         AND SAILING_LANE_DTL_SEQ = vSailingDtlSeq;
               END IF;
            END IF;
         END IF;
      END IF;

      IF (vServiceLevel IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail := CHECK_IS_BU_VALID (vTCCompanyId, vServiceLevelBUID);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingDtlSeq,
               vServiceLevel || ' is an invalid Service Level',
               4720037,
               vServiceLevel);
         ELSE
            vPassFail :=
               SAILING_SUB_VALIDATION_PKG.VALIDATE_SERVICE_LEVEL (
                  vServiceLevelBUID,
                  vServiceLevel);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_SAILING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vSailingDtlSeq,
                  vServiceLevel || ' is an invalid Service Level',
                  4720037,
                  vServiceLevel);
            ELSE
               IF (vServiceLevelId = 0)
               THEN
                  SELECT service_level_id
                    INTO vServiceLevelId
                    FROM service_level
                   WHERE service_level = vServiceLevel
                         AND tc_company_id = vServiceLevelBUID;

                  UPDATE IMPORT_SAILING_LANE_DTL
                     SET service_level_id = vServiceLevelId
                   WHERE     service_level = vServiceLevel
                         AND tc_company_id = vTCCompanyId
                         AND lane_id = vLaneId
                         AND SAILING_LANE_DTL_SEQ = vSailingDtlSeq;
               END IF;
            END IF;
         END IF;
      END IF;

      IF (p_scndr_carrier_code IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail := CHECK_IS_BU_VALID (vTCCompanyId, vServiceLevelBUID);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingDtlSeq,
               p_scndr_carrier_code
               || ' is an invalid Secondary Carrier Code',
               4720066,
               p_scndr_carrier_code);
         ELSE
            vPassFail :=
               SAILING_SUB_VALIDATION_PKG.VALIDATE_CARRIER_CODE (
                  p_scndr_carrier_codeBUID,
                  p_scndr_carrier_code);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_SAILING_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vSailingDtlSeq,
                  p_scndr_carrier_code
                  || ' is an invalid Secondary Carrier Code',
                  4720066,
                  p_scndr_carrier_code);
            ELSE
               IF (p_scndr_carrier_Id = 0)
               THEN
                  SELECT CARRIER_ID
                    INTO p_scndr_carrier_Id
                    FROM carrier_code
                   WHERE carrier_code = p_scndr_carrier_code
                         AND tc_company_id = p_scndr_carrier_codeBUID;

                  UPDATE IMPORT_SAILING_LANE_DTL
                     SET SCNDR_CARRIER_ID = p_scndr_carrier_Id
                   WHERE     SCNDR_CARRIER_CODE = p_scndr_carrier_code
                         AND tc_company_id = vTCCompanyId
                         AND lane_id = vLaneId
                         AND SAILING_LANE_DTL_SEQ = vSailingDtlSeq;
               END IF;
            END IF;
         END IF;
      END IF;

      IF (vFailure <> 1)
      THEN
         vPassFail :=
            VALIDATE_TP_SERV_MODE (vTCCompanyId,
                                   vLaneId,
                                   vSailingDtlSeq,
                                   vCarrierCode,
                                   vServiceLevel,
                                   vMode,
                                   p_scndr_carrier_code,
                                   vCarrierId,
                                   vServiceLevelId,
                                   vModeId,
                                   p_scndr_carrier_Id);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            DBMS_OUTPUT.put_line ('Validate TP SERV Mode Failed');
         END IF;
      END IF;

      RETURN vFailure;
   END VALIDATE_DETAIL_BASE_DATA;

   FUNCTION VALIDATE_SAILING_SCHDL_WEEKLY (
      vTCCompanyId         IN COMPANY.COMPANY_ID%TYPE,
      vLaneId              IN sailing_lane.lane_id%TYPE,
      vSailingLaneDtlSeq   IN sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE/* SL  05/22/02  */
      )
      RETURN NUMBER
   AS
      vSailSchedByWeekSeq   sailing_schdl_by_week.SAIL_SCHED_BY_WEEK_ID%TYPE;
      vVesselName           sailing_schdl_by_week.RESOURCE_REF_EXTERNAL%TYPE;
      vVoyageNumber         sailing_schdl_by_week.RESOURCE_NAME_EXTERNAL%TYPE;
      vEffectiveDt          sailing_schdl_by_week.EFFECTIVE_DT%TYPE;
      vExpirationDt         sailing_schdl_by_week.EXPIRATION_DT%TYPE;
      vFailure              NUMBER;

      CURSOR LANE_DTL_BY_WK_CURSOR
      IS
         SELECT I1.SAIL_SCHED_BY_WEEK_ID,
                I1.RESOURCE_NAME_EXTERNAL,
                I1.RESOURCE_REF_EXTERNAL,
                I1.EFFECTIVE_DT,
                I1.EXPIRATION_DT
           FROM IMPORT_SAILING_SCHDL_BY_WK I1, IMPORT_SAILING_LANE_DTL I2
          WHERE     I1.TC_COMPANY_ID = vTCCompanyId
                AND I1.TC_COMPANY_ID = I2.TC_COMPANY_ID
                AND I1.LANE_ID = I2.LANE_ID
                AND I1.SAILING_LANE_DTL_SEQ = I2.SAILING_LANE_DTL_SEQ
                AND I2.LANE_DTL_STATUS = 3
                AND I1.LANE_ID = vLaneId
                AND I1.SAILING_LANE_DTL_SEQ = vSailingLaneDtlSeq
                AND I2.LANE_DTL_STATUS = 3;
   --AND LANE_DTL_STATUS in (3, 4);
   BEGIN
      OPEN LANE_DTL_BY_WK_CURSOR;

     <<LANE_DTL_WK_LOOP>>
      LOOP
         FETCH LANE_DTL_BY_WK_CURSOR
         INTO vSailSchedByWeekSeq,
              vVesselName,
              vVoyageNumber,
              vEffectiveDt,
              vExpirationDt;

         EXIT LANE_DTL_WK_LOOP WHEN LANE_DTL_BY_WK_CURSOR%NOTFOUND;

         vFailure := 0;

   /*      IF (vVesselName IS NULL)
         THEN
            vFailure := 1;

            INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingLaneDtlSeq,
               'Vessel name is null in weekly schedule',
               9920050,
               NULL);
         END IF;
	*/
         IF (vVoyageNumber IS NULL)
         THEN
            vFailure := 1;

            INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingLaneDtlSeq,
               'Voyage number is null in weekly schedule',
               9920051,
               NULL);
         END IF;

         IF (vEffectiveDt IS NULL)
         THEN
            vFailure := 1;

            INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingLaneDtlSeq,
               'Effective Date is null in weekly schedule',
               9920052,
               NULL);
         END IF;

         IF (vExpirationDt IS NULL)
         THEN
            vFailure := 1;

            INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingLaneDtlSeq,
               'Expiration Date is null in weekly schedule',
               9920053,
               NULL);
         END IF;

         IF vEffectiveDt > vExpirationDt
         THEN
            vFailure := 1;

            INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingLaneDtlSeq,
               'Effective Date is greater than the Expiration Date in weekly schedule',
               9920065,
               NULL);
         END IF;

         vFailure :=
            VALIDATE_SAILING_SCHDL_WK_DAY (vTCCompanyId,
                                           vLaneId,
                                           vSailingLaneDtlSeq,
                                           vSailSchedByWeekSeq,
                                           vVesselName,
                                           vVoyageNumber,
                                           vEffectiveDt,
                                           vExpirationDt);
      END LOOP LANE_DTL_WK_LOOP;

      CLOSE LANE_DTL_BY_WK_CURSOR;

      RETURN vFailure;
   END VALIDATE_SAILING_SCHDL_WEEKLY;

   FUNCTION VALIDATE_SAILING_SCHDL_BY_DATE (
      vTCCompanyId         IN COMPANY.COMPANY_ID%TYPE,
      vLaneId              IN sailing_lane.lane_id%TYPE,
      vSailingLaneDtlSeq   IN sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE/* SL  05/22/02  */
      )
      RETURN NUMBER
   AS
      vSailSchedByDateid   SAILING_SCHDL_BY_DATE.SAIL_SCHED_BY_DATE_ID%TYPE;
      vVesselName          SAILING_SCHDL_BY_DATE.RESOURCE_REF_EXTERNAL%TYPE;
      vVoyageNumber        SAILING_SCHDL_BY_DATE.RESOURCE_NAME_EXTERNAL%TYPE;
      vDepartureDttm       SAILING_SCHDL_BY_DATE.DEPARTURE_DTTM%TYPE;
      vCuttoffDttm         SAILING_SCHDL_BY_DATE.CUTOFF_DTTM%TYPE;
      vArrivalDttm         SAILING_SCHDL_BY_DATE.ARRIVAL_DTTM%TYPE;
      vPickupDttm          SAILING_SCHDL_BY_DATE.PICKUP_DTTM%TYPE;
      vTransitTime         SAILING_SCHDL_BY_DATE.TRANSIT_TIME_VALUE%TYPE;
      vTransitTimeUOM      SAILING_SCHDL_BY_DATE.TRANSIT_TIME_STANDARD_UOM%TYPE;
      vFailure             NUMBER;
      vCountDup            NUMBER;
      vTempCuttoffDttm     DATE;
      vTempArrivalDttm     DATE;
      vTempPickupDttm      DATE;
      vTempTransitTime     SAILING_SCHDL_BY_DATE.TRANSIT_TIME_VALUE%TYPE;
      vDays                NUMBER;

      CURSOR LANE_DTL_BY_DATE_CURSOR
      IS
         SELECT I1.SAIL_SCHED_BY_DATE_ID,
                I1.RESOURCE_REF_EXTERNAL,
                I1.RESOURCE_NAME_EXTERNAL,
                I1.DEPARTURE_DTTM,
                I1.CUTOFF_DTTM,
                I1.ARRIVAL_DTTM,
                I1.PICKUP_DTTM,
                I1.Transit_Time_Value,
                I1.Transit_Time_Standard_Uom
           FROM IMPORT_SAILING_SCHDL_BY_DATE I1, IMPORT_SAILING_LANE_DTL I2
          WHERE     I1.TC_COMPANY_ID = vTCCompanyId
                AND I1.TC_COMPANY_ID = I2.TC_COMPANY_ID
                AND I1.LANE_ID = vLaneId
                AND I1.LANE_ID = I2.LANE_ID
                AND I1.LANE_DTL_SEQ = vSailingLaneDtlSeq
                AND I1.LANE_DTL_SEQ = I2.SAILING_LANE_DTL_SEQ
                AND I2.LANE_DTL_STATUS = 3;
   --AND LANE_DTL_STATUS = 3;
   --AND LANE_DTL_STATUS in (3, 4);
   BEGIN
      OPEN LANE_DTL_BY_DATE_CURSOR;

     <<LANE_DTL_DATE_LOOP>>
      LOOP
         FETCH LANE_DTL_BY_DATE_CURSOR
         INTO vSailSchedByDateid,
              vVoyageNumber,
              vVesselName,
              vDepartureDttm,
              vCuttoffDttm,
              vArrivalDttm,
              vPickupDttm,
              vTransitTime,
              vTransitTimeUOM;

         EXIT LANE_DTL_DATE_LOOP WHEN LANE_DTL_BY_DATE_CURSOR%NOTFOUND;

         vFailure := 0;

   /*      IF (vVesselName IS NULL OR LTRIM (RTRIM (vVesselName)) = '')
         THEN
            vFailure := 1;

            INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingLaneDtlSeq,
               'Vessel name is null in date schedule',
               9920054,
               NULL);
         END IF;
	*/
         IF (vVoyageNumber IS NULL OR LTRIM (RTRIM (vVoyageNumber)) = '')
         THEN
            vFailure := 1;

            INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingLaneDtlSeq,
               'Voyage number is null in date schedule',
               9920055,
               NULL);
         END IF;

         IF (vDepartureDttm IS NULL)
         THEN
            vFailure := 1;

            INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingLaneDtlSeq,
               'Departure Date is null in date schedule',
               9920056,
               NULL);
         END IF;

         IF (vTransitTimeUOM IS NULL OR vTransitTimeUOM = 0)
         THEN
            vFailure := 1;

            INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingLaneDtlSeq,
               'The Transit Time UOM cannot be null in Schedule By Date',
               9920072,
               NULL);
         END IF;

         IF (vTransitTime IS NULL AND vArrivalDttm IS NULL)
         THEN
            vfailure := 1;
            INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingLaneDtlSeq,
               'Either Transit Time or Arrival information must be provided in Schedule By Date',
               9920073,
               NULL);
         END IF;

         

         --satya
       

         --satya

         /*Populating missing DTTM */
         IF (vCuttoffDttm IS NOT NULL)
         THEN
           
            vTempCuttoffDttm := vCuttoffDttm;
         END IF;

         IF (vArrivalDttm IS NOT NULL)
         THEN
            vTempArrivalDttm := vArrivalDttm;
         END IF;

        

         IF (vPickupDttm IS NOT NULL)
         THEN
            
            vTempPickupDttm := vPickupDttm;
         END IF;

         IF (vTransitTime IS NOT NULL)
         THEN
            vTempTransitTime := vTransitTime;
         END IF;

        

         UPDATE IMPORT_SAILING_SCHDL_BY_DATE WDT
            SET WDT.Transit_Time_Value = vTempTransitTime,
                WDT.Cutoff_Dttm = vTempCuttoffDttm,
                WDT.Arrival_Dttm = vTempArrivalDttm,
                WDT.Departure_Dttm = vDepartureDttm,
                WDT.Pickup_Dttm = vTempPickupDttm
          WHERE     WDT.TC_COMPANY_ID = vTCCompanyId
                AND WDT.LANE_ID = vLaneId
                AND WDT.LANE_DTL_SEQ = vSailingLaneDtlSeq
                AND WDT.SAIL_SCHED_BY_DATE_ID = vSailSchedByDateid;

         COMMIT;

         /* Check for Duplicate Schedule */
         SELECT COUNT (*)
           INTO vCountDup
           FROM import_sailing_schdl_by_date ISD
          WHERE     ISD.TC_COMPANY_ID = vTCCompanyId
                AND ISD.LANE_ID = vLaneId
                AND ISD.LANE_DTL_SEQ = vSailingLaneDtlSeq
                AND ISD.RESOURCE_REF_EXTERNAL = vVoyageNumber
                AND ISD.RESOURCE_NAME_EXTERNAL = vVesselName
                AND ISD.DEPARTURE_DTTM = vDepartureDttm
                AND ISD.CUTOFF_DTTM = vCuttoffDttm
                AND ISD.ARRIVAL_DTTM = vArrivalDttm
                AND ISD.PICKUP_DTTM = vPickupDttm;

         IF (vCountDup > 1)
         THEN
            vFailure := 1;

            INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingLaneDtlSeq,
               'Duplicate date schedule for Transit Lane detail',
               9920071,
               NULL);
         END IF;
      END LOOP LANE_DTL_DATE_LOOP;

      CLOSE LANE_DTL_BY_DATE_CURSOR;

      RETURN vFailure;
   END VALIDATE_SAILING_SCHDL_BY_DATE;

   FUNCTION VALIDATE_SAILING_SCHDL_WK_DAY (
      vTCCompanyId         IN COMPANY.COMPANY_ID%TYPE,
      vLaneId              IN sailing_lane.lane_id%TYPE,
      vSailingLaneDtlSeq   IN sailing_lane_dtl.SAILING_LANE_DTL_SEQ%TYPE,
      vSailSchedByWeekId   IN sailing_schdl_by_week.SAIL_SCHED_BY_WEEK_ID%TYPE,
      vVesselName          IN sailing_schdl_by_week.RESOURCE_REF_EXTERNAL%TYPE,
      vVoyageNumber        IN sailing_schdl_by_week.RESOURCE_NAME_EXTERNAL%TYPE,
      /* SL  05/22/02  */
      vEffectiveDt         IN sailing_schdl_by_week.EFFECTIVE_DT%TYPE,
      vExpirationDt        IN sailing_schdl_by_week.EXPIRATION_DT%TYPE)
      RETURN NUMBER
   AS
      vSailSchedByWeekSeq    sailing_schdl_by_week.SAIL_SCHED_BY_WEEK_ID%TYPE;
      vSailingSchedWkDayId   SAILING_SCHDL_BY_WEEK_DAY.SAIL_SCHED_BY_WEEK_DAY_ID%TYPE;
      vCutoffTime            SAILING_SCHDL_BY_WEEK_DAY.CUTOFF_TIME%TYPE;
      vArrivalTime           SAILING_SCHDL_BY_WEEK_DAY.ARRIVAL_TIME%TYPE;
      vDepartureTime         SAILING_SCHDL_BY_WEEK_DAY.DEPARTURE_TIME%TYPE;
      vAvailableTime         SAILING_SCHDL_BY_WEEK_DAY.PICKUP_TIME%TYPE;
      vCuttOffDay            SAILING_SCHDL_BY_WEEK_DAY.CUTOFF_DAYOFWEEK%TYPE;
      vArrivalDay            SAILING_SCHDL_BY_WEEK_DAY.ARRIVAL_DAYOFWEEK%TYPE;
      vDepartureDay          SAILING_SCHDL_BY_WEEK_DAY.DEPARTURE_DAYOFWEEK%TYPE;
      vAvailableDay          SAILING_SCHDL_BY_WEEK_DAY.PICKUP_DAYOFWEEK%TYPE;
      vTransitTime           SAILING_SCHDL_BY_WEEK_DAY.TRANSIT_TIME_VALUE%TYPE;
      vTransitTimeUOM        SAILING_SCHDL_BY_WEEK_DAY.TRANSIT_TIME_STANDARD_UOM%TYPE;
      vDuration              SAILING_SCHDL_BY_WEEK_DAY.TRANSIT_TIME_WEEK%TYPE;

      vFailure               NUMBER;

      vTransitTimeDays       NUMBER;
      vTempTransitTime       SAILING_SCHDL_BY_WEEK_DAY.TRANSIT_TIME_VALUE%TYPE;
      vTempCuttOffTime       DATE;
      vTempCuttOffDay        NUMBER;
      vTempDepartureTime     DATE;
      vTempArrivalDay        NUMBER;
      vTempArrivalTime       DATE;
      vTempAvailableDay      NUMBER;
      vTempAvailableTime     DATE;
      vTempDuration          NUMBER;
      vTempDay               NUMBER;
      vCOUNT                 NUMBER;
      vHour                  NUMBER;
      vDays                  NUMBER;

      CURSOR LANE_DTL_BY_WK_DAY_CUR
      IS
         SELECT I1.SAIL_SCHED_BY_WEEK_DAY_ID,
                I1.CUTOFF_TIME,
                I1.ARRIVAL_TIME,
                I1.DEPARTURE_TIME,
                I1.PICKUP_TIME,
                I1.CUTOFF_DAYOFWEEK,
                I1.ARRIVAL_DAYOFWEEK,
                I1.DEPARTURE_DAYOFWEEK,
                I1.PICKUP_DAYOFWEEK,
                I1.TRANSIT_TIME_VALUE,
                I1.TRANSIT_TIME_STANDARD_UOM,
                I1.DURATION
           FROM IMPORT_SAILING_SCHDL_WK_DAY I1, IMPORT_SAILING_LANE_DTL I2
          WHERE     I1.TC_COMPANY_ID = vTCCompanyId
                AND I1.TC_COMPANY_ID = I2.TC_COMPANY_ID
                AND I1.LANE_ID = vLaneId
                AND I1.LANE_ID = I2.LANE_ID
                AND I1.SAILING_LANE_DTL_SEQ = vSailingLaneDtlSeq
                AND I1.SAILING_LANE_DTL_SEQ = I2.SAILING_LANE_DTL_SEQ
                AND I2.LANE_DTL_STATUS = 3
                AND I1.SAIL_SCHED_BY_WEEK_ID = vSailSchedByWeekId;
   --AND LANE_DTL_STATUS in (3, 4);
   BEGIN
      OPEN LANE_DTL_BY_WK_DAY_CUR;

     <<LANE_DTL_WK_DAY_LOOP>>
      LOOP
         FETCH LANE_DTL_BY_WK_DAY_CUR
         INTO vSailingSchedWkDayId,
              vCutoffTime,
              vArrivalTime,
              vDepartureTime,
              vAvailableTime,
              vCuttOffDay,
              vArrivalDay,
              vDepartureDay,
              vAvailableDay,
              vTransitTime,
              vTransitTimeUOM,
              vDuration;

         EXIT LANE_DTL_WK_DAY_LOOP WHEN LANE_DTL_BY_WK_DAY_CUR%NOTFOUND;

         vFailure := 0;

         IF (vDepartureDay = 0)
         THEN
            vFailure := 1;

            INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingLaneDtlSeq,
               'The Departure Day cannot be null in Schedule Line Item',
               9920069,
               NULL);
         END IF;

         --satya
         

         /* Populating  the Time fields */
         IF (vCutoffTime IS NOT NULL)
         THEN
            
            vTempCuttOffTime := vCutoffTime;
         END IF;

         IF (vDepartureTime IS NOT NULL)
         THEN
            
            vTempDepartureTime := vDepartureTime;
         END IF;

         IF (vArrivalTime IS NOT NULL)
         THEN
          
            vTempArrivalTime := vArrivalTime;
         END IF;

         IF (vAvailableTime IS NOT NULL)
         THEN
            
            vTempAvailableTime := vAvailableTime;
         END IF;

         /*Populating the Days fields */
         IF (vCuttOffDay IS  NOT NULL)
         THEN
            vTempCuttOffDay := vCuttOffDay;
         END IF;

         IF (vArrivalDay IS NOT NULL)
         THEN
            vTempArrivalDay := vArrivalDay;
         END IF;

         /*Populating Duration */
         vTempDuration := 0;

         IF (vDuration IS NOT NULL)
         THEN
            vTempDuration := vDuration;
         END IF;

         

         /* -- mrkumar
         This computation is not needed, as per UC1148. According to UC, if both the arrival day and the duration are not given, system should throw an error and no computation
         should follow thereafter

                   IF(vTransitTime IS NULL AND vArrivalDay IS NULL AND vDuration IS NOT NULL)THEN

                                  vTransitTimeDays:= vDuration * 7;
                      vDays:=vDepartureDay + vTransitTimeDays Mod 7;
                      if(vDays <= 7)then
                       vTempArrivalDay:=vDays;
                      end if;
                      if (vDays =8)then
                      vTempArrivalDay:=1;
                      end if;
                       if (vDays =9)then
                      vTempArrivalDay:=2;
                      end if;
                       if (vDays =10)then
                      vTempArrivalDay:=3;
                      end if;
                       if (vDays =11)then
                      vTempArrivalDay:=4;
                      end if;
                       if (vDays =12)then
                      vTempArrivalDay:=5;
                      end if;
                        if (vDays =13)then
                      vTempArrivalDay:=6;
                      end if;
                     vDays:=0;
                   END IF;
         */

         IF (vAvailableDay IS NOT NULL)
         THEN
            vTempAvailableDay := vAvailableDay;
         END IF;

         /* Populating Transit Time */
         IF (vTransitTime IS NOT NULL)
         THEN
            vTempTransitTime := vTransitTime;
         END IF;

         
         /* -- mrkumar
         This computation is not needed, as per UC1148. According to UC, if both the arrival day and the duration are not given, system should throw an error and no computation
         should follow thereafter

            IF(vTransitTime IS NULL AND vDuration is Null AND vArrivalDay IS NOT NULL)then

                vDays:=vTempArrivalDay-vDepartureDay;
                  IF(vDays=-6)THEN
                  vDays:=1;
                  END IF;
                  IF(vDays=-5)THEN
                  vDays:=2;
                  END IF;
                  IF(vDays=-4)THEN
                  vDays:=3;
                  END IF;
                  IF(vDays=-3)THEN
                  vDays:=4;
                  END IF;
                  IF(vDays=-2)THEN
                  vDays:=5;
                  END IF;
                  IF(vDays=-1)THEN
                  vDays:=6;
                  END IF;

                if(vTransitTimeUOM=404)then

                 vTempTransitTime:=vDays;
                end if;

                 vHour:= trunc(((86400*(vTempArrivalTime-vTempDepartureTime))/60)/60)-
                     24*(trunc((((86400*(vTempArrivalTime-vTempDepartureTime))/60)/60)/24));

                 if(vHour=0)then
                 vHour:=1;
                 end if;

                 IF(vHour > 0 AND vDays>0 AND vTransitTimeUOM=166  )THEN

                   vTempTransitTime:= floor(vDays*24+vHour);
                 END IF;

            end if;

         */

         UPDATE IMPORT_SAILING_SCHDL_WK_DAY WKD
            SET WKD.TRANSIT_TIME_VALUE = vTempTransitTime,
                WKD.CUTOFF_TIME = vTempCuttOffTime,
                WKD.CUTOFF_DAYOFWEEK = vTempCuttOffDay,
                WKD.DEPARTURE_TIME = vTempDepartureTime,
                WKD.ARRIVAL_DAYOFWEEK = vTempArrivalDay,
                WKD.ARRIVAL_TIME = vTempArrivalTime,
                WKD.PICKUP_DAYOFWEEK = vTempAvailableDay,
                WKD.PICKUP_TIME = vTempAvailableTime,
                WKD.DURATION = vTempDuration
          WHERE     WKD.TC_COMPANY_ID = vTCCompanyId
                AND WKD.LANE_ID = vLaneId
                AND WKD.SAILING_LANE_DTL_SEQ = vSailingLaneDtlSeq
                AND WKD.SAIL_SCHED_BY_WEEK_ID = vSailSchedByWeekId
                AND WKD.SAIL_SCHED_BY_WEEK_DAY_ID = vSailingSchedWkDayId;

         COMMIT;

         /* CHECK FOR DUPLICATE SCHEDULE */
         SELECT COUNT (*)
           INTO vCOUNT
           FROM IMPORT_SAILING_SCHDL_BY_WK IW,
                IMPORT_SAILING_SCHDL_WK_DAY IWD
          WHERE     IW.TC_COMPANY_ID = IWD.TC_COMPANY_ID
                AND IW.LANE_ID = IWD.LANE_ID
                AND IW.SAILING_LANE_DTL_SEQ = IWD.SAILING_LANE_DTL_SEQ
                AND IW.SAIL_SCHED_BY_WEEK_ID = IWD.SAIL_SCHED_BY_WEEK_ID
                AND IW.LANE_ID = vLaneId
                AND IW.SAILING_LANE_DTL_SEQ = vSailingLaneDtlSeq
                AND IW.RESOURCE_NAME_EXTERNAL = vVesselName
                AND IW.RESOURCE_REF_EXTERNAL = vVoyageNumber
                AND IW.EFFECTIVE_DT = vEffectiveDt
                AND IW.EXPIRATION_DT = vExpirationDt
                AND IWD.DEPARTURE_DAYOFWEEK = vDepartureDay
                AND IWD.CUTOFF_DAYOFWEEK = vTempCuttOffDay
                AND IWD.ARRIVAL_DAYOFWEEK = vTempArrivalDay
                AND IWD.PICKUP_DAYOFWEEK = vTempAvailableDay
                AND IWD.DEPARTURE_TIME = vTempDepartureTime
                AND IWD.CUTOFF_TIME = vTempCuttOffTime
                AND IWD.PICKUP_TIME = vTempAvailableTime
                AND IWD.ARRIVAL_TIME = vTempArrivalTime;

         IF (vCOUNT > 1)
         THEN
            vFailure := 1;

            INSERT_SAILING_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vSailingLaneDtlSeq,
               'Duplicate weekly schedule for Transit Lane detail',
               9920068,
               NULL);
         END IF;
      END LOOP LANE_DTL_WK_DAY_LOOP;

      CLOSE LANE_DTL_BY_WK_DAY_CUR;

      RETURN vFailure;
   END VALIDATE_SAILING_SCHDL_WK_DAY;



PROCEDURE VALIDATE_LANE_UI (VTCCOMPANYID   IN COMPANY.COMPANY_ID%TYPE,VSAILINGLANEID  IN SAILING_LANE.LANE_ID%TYPE)
AS
  CURSOR LANE_LIST IS SELECT TC_COMPANY_ID,LANE_ID, BATCH_ID FROM IMPORT_SAILING_LANE WHERE SAILING_LANE_ID = VSAILINGLANEID;
BEGIN
  FOR C_LANE_LIST IN LANE_LIST
  LOOP
	  UPDATE IMPORT_SAILING_LANE_DTL SET LANE_DTL_STATUS = 3 WHERE TC_COMPANY_ID = VTCCOMPANYID AND LANE_ID = VSAILINGLANEID;
	  DELETE FROM SAILING_LANE_ERRORS WHERE TC_COMPANY_ID = C_LANE_LIST.TC_COMPANY_ID AND LANE_ID = C_LANE_LIST.LANE_ID;
	  VALIDATE_SAILING_LANE (C_LANE_LIST.TC_COMPANY_ID, C_LANE_LIST.LANE_ID);
	  TRANSFER_IMPORT_SAILING_LANES(C_LANE_LIST.TC_COMPANY_ID,C_LANE_LIST.BATCH_ID,C_LANE_LIST.BATCH_ID);
  END LOOP;
EXCEPTION
  WHEN OTHERS
  THEN
	 DBMS_OUTPUT.PUT_LINE ('PROCEDURE VALIDATE_LANE_UI: ' || SQLERRM);
	 RAISE;
END VALIDATE_LANE_UI;

PROCEDURE VALIDATE_LANE_DTL_EPI (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vImportLaneId   IN SAILING_LANE.LANE_ID%TYPE,
      vLaneId         IN SAILING_LANE.LANE_ID%TYPE)
   AS
      s_SailingLaneDtlSeq         SAILING_LANE_DTL.SAILING_LANE_DTL_SEQ%TYPE;
      s_Carrier_Id        		  SAILING_LANE_DTL.CARRIER_ID%TYPE;
	  vUseEPI					 COMB_LANE.USE_EPI%TYPE;
      CURSOR VALIDATE_LANE_DTL_CURSOR
      IS
         SELECT SL.sailing_lane_dtl_seq,
                SL.carrier_id
           FROM IMPORT_SAILING_LANE_DTL SL, CARRIER_CODE CC
          WHERE     SL.tc_company_id = vTCCompanyId
                AND SL.lane_id = vImportLaneId AND CC.CARRIER_ID=SL.CARRIER_ID AND CC.CARRIER_TYPE_ID IS NOT NULL AND CC.CARRIER_TYPE_ID != 24 
                AND SL.LANE_DTL_STATUS = 1 and SL.IMPORT_SAILING_DTL_STATUS=0 AND SL.EXPIRATION_DT>SL.EFFECTIVE_DT AND SL.EXPIRATION_DT>GETDATE();
   BEGIN
      
	  BEGIN	  
		SELECT USE_EPI INTO vUseEPI FROM COMB_LANE where LANE_ID = vLaneId;
	  EXCEPTION
            WHEN NO_DATA_FOUND
			THEN
				vUseEPI:= 0;
	  END;
	  
	  IF (vUseEPI=1)
	  THEN
			FOR s IN VALIDATE_LANE_DTL_CURSOR
			LOOP
				s_SailingLaneDtlSeq := s.sailing_lane_dtl_seq;
				s_Carrier_Id := s.carrier_id;
				
				INSERT_SAILING_LANE_DTL_ERROR (
							vTCCompanyId,
							vImportLaneId,
							s_SailingLaneDtlSeq,
							'Lane detail for native parcel carrier cannot be created when external parcel integration is enabled on the lane',
							4720201,
							NULL);
							
				UPDATE IMPORT_SAILING_LANE_DTL
					SET lane_dtl_status = 4, import_sailing_dtl_status = 4 
					WHERE     TC_COMPANY_ID = vTCCompanyId
					AND LANE_ID = vImportLaneId
					AND SAILING_LANE_DTL_SEQ = s_SailingLaneDtlSeq;                  

				COMMIT;
			END LOOP;
	  END IF;	  
	  
   END VALIDATE_LANE_DTL_EPI;

PROCEDURE REPLICATE_RG_RA_LANE_DETAIL(
	vTCCompanyId   IN company.COMPANY_ID%TYPE,
    vLaneId        IN comb_lane.LANE_ID%TYPE,
    vSailingLaneDtlSeq      IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
    vIsRouting      IN comb_lane_dtl.IS_ROUTING%TYPE,
    vIsRating     IN comb_lane_dtl.IS_RATING%TYPE)
	
	IS
      vSalingDtlSeqNew   comb_lane_dtl.LANE_DTL_SEQ%TYPE;
	 BEGIN
		SELECT MAX (LANE_DTL_SEQ) + 1
        INTO vSalingDtlSeqNew
        FROM COMB_LANE_DTL
       WHERE LANE_ID = vLaneID;

      INSERT INTO COMB_LANE_DTL (TC_COMPANY_ID,
                                 LANE_ID,
                                 LANE_DTL_SEQ,
                                 LANE_DTL_STATUS,
                                 IS_RATING,
                                 IS_ROUTING,
                                 IS_BUDGETED,
                                 LAST_UPDATED_SOURCE_TYPE,
                                 LAST_UPDATED_SOURCE,
                                 LAST_UPDATED_DTTM,
                                 CARRIER_ID,
                                 MOT_ID,
                                 EQUIPMENT_ID,
                                 SERVICE_LEVEL_ID,
                                 SCNDR_CARRIER_ID,
                                 PROTECTION_LEVEL_ID,
                                 EFFECTIVE_DT,
                                 EXPIRATION_DT,
                                 FIXED_TRANSIT_STANDARD_UOM,
                                 FIXED_TRANSIT_VALUE,
                                 IS_SAILING,
                                 SAILING_FREQUENCY_TYPE,
                                 SAILING_SCHEDULE_NAME,
                                 MASTER_LANE_ID,
                                 O_SHIP_VIA,
                                 D_SHIP_VIA,
                                 CONTRACT_NUMBER,
                                 CUSTOM_TEXT1,
                                 CUSTOM_TEXT2,
                                 CUSTOM_TEXT3,
                                 CUSTOM_TEXT4,
                                 CUSTOM_TEXT5,
                                 TT_TOLERANCE_FACTOR)
         SELECT TC_COMPANY_ID,
                LANE_ID,
                vSalingDtlSeqNew,
                0,
                IS_RATING,
                IS_ROUTING,
                IS_BUDGETED,
                3,
                'OMS',
                GETDATE (),
                CARRIER_ID,
                MOT_ID,
                EQUIPMENT_ID,
                SERVICE_LEVEL_ID,
                SCNDR_CARRIER_ID,
                PROTECTION_LEVEL_ID,
                EFFECTIVE_DT,
                EXPIRATION_DT,
                FIXED_TRANSIT_STANDARD_UOM,
                FIXED_TRANSIT_VALUE,
                0,
                SAILING_FREQUENCY_TYPE,
                SAILING_SCHEDULE_NAME,
                MASTER_LANE_ID,
                O_SHIP_VIA,
                D_SHIP_VIA,
                CONTRACT_NUMBER,
                CUSTOM_TEXT1,
                CUSTOM_TEXT2,
                CUSTOM_TEXT3,
                CUSTOM_TEXT4,
                CUSTOM_TEXT5,
                TT_TOLERANCE_FACTOR
           FROM COMB_LANE_DTL
          WHERE LANE_ID = vLaneId AND LANE_DTL_SEQ = vSailingLaneDtlSeq;

      COPY_CHILD_ROW (vTCCompanyId,
                      vLaneId,
                      -1,
                      vSailingLaneDtlSeq,
                      vSalingDtlSeqNew,
                      vIsRating,
                      vIsRouting,
                      0);
	 END REPLICATE_RG_RA_LANE_DETAIL;   
   
END SAILING_LANE_VALIDATION_PKG;
/



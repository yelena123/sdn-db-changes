create or replace procedure wm_add_lpn_to_shpmt
(
    p_user_id             in user_profile.user_id%type,
    p_tc_company_id       in shipment.tc_company_id%type,
    p_shipment_id         in shipment.shipment_id%type,
    p_om_shipment_id      in shipment.shipment_id%type,
    p_static_route_id     in static_route.static_route_id%type,
    p_lpn_id              in lpn.lpn_id%type,
    p_order_id            in orders.order_id%type,
    p_order_split_id      in order_split.order_split_id%type,
    p_cons_run_id         in cons_run.cons_run_id%type,
    p_caller_id           in varchar2 default null
)
as
    v_use_locking           number(1) := 0;
    v_exec_pre_cleanup      varchar2(1);
    v_max_log_lvl           number(1) := 0;
    v_commit_freq           number(5) := 99999;
    v_code_id               sys_code.code_id%type := 'ADD';
    v_ref_value_1           msg_log.ref_value_1%type
        := 'lpn:' || to_char(p_lpn_id) || '|o_id:' || to_char(p_order_id) 
        || '|os_id:' || to_char(p_order_split_id) || '|cr_id:' 
        || to_char(p_cons_run_id) || '|s_id:' || to_char(p_shipment_id)
        || '|om_s_id:' || to_char(p_om_shipment_id) || '|sr_id:'
        || to_char(p_static_route_id);
begin
    if (p_shipment_id is null)
    then
        raise_application_error(-20050, 'Shipment_id not passed for assignment');
    end if;

    select /*+ result_cache */ to_number(coalesce(trim(substr(sc.misc_flags, 3, 5)), '99999')),
        case when substr(sc.misc_flags, 2, 1) = 'Y' then 1 else 0 end,
        to_number(coalesce(trim(substr(sc.misc_flags, 8, 1)), '0')),
        coalesce(substr(sc.misc_flags, 9, 1), 'N')
    into v_commit_freq, v_use_locking, v_max_log_lvl, v_exec_pre_cleanup
    from sys_code sc 
    where sc.rec_type = 'S' and sc.code_type = '520' and sc.code_id = v_code_id;
    
    v_commit_freq := case v_commit_freq when 0 then 99999 else v_commit_freq end;
    if (p_caller_id = 'MLP')
    then
        v_use_locking := 0;
    end if;

    delete from tmp_order_splits_scratch;
    delete from tmp_lpn_list;
    delete from tmp_store_master;
    delete from tmp_order_splits_master;
    delete from tmp_log_parm;
    insert into tmp_log_parm
    (
        log_lvl, pgm_id, module, msg_id, ref_code_1, ref_value_1, user_id
    )
    values
    (
        v_max_log_lvl, 'wm_add_to_shpmt_wrapper', 'WAVE', '1094', v_code_id,
        v_ref_value_1, p_user_id
    );

    if (v_exec_pre_cleanup = 'Y')
    then
        wm_cs_log('Beginning cleanup');
        wm_pre_add_cleanup(p_mode => '_assign_lpn_');
    end if;

    wm_cs_log('Beginning lpn assignment');
    
    wm_add_to_shpmt_wrapper(p_user_id, p_tc_company_id, v_use_locking, v_commit_freq, p_caller_id, 
        p_trk_prod_flag => null, p_shipment_id => p_shipment_id, 
        p_om_shipment_id => p_om_shipment_id, p_static_route_id => p_static_route_id, 
        p_lpn_id => p_lpn_id, p_order_id => p_order_id, p_order_split_id => p_order_split_id, 
        p_cons_run_id => p_cons_run_id, p_load_split_data => 1, p_mode => '_assign_lpn_');

    wm_cs_log('Completed assignment of lpn to shpmt');
end;
/

show errors;

--SEQUENCE Update for MESSAGE_MASTER
CALL sequpdt('MESSAGE_MASTER','MESSAGE_MASTER_ID','SEQ_MESSAGE_MASTER_ID');

-- Error message for Invalid Date/Format ----
merge INTO message_master a USING
(SELECT '1401' msg_id,
  'Invalid Date/Format' msg,
  'ErrorMessage' bundle_name
FROM dual
) b ON (a.msg_id = b.msg_id AND a.bundle_name = b.bundle_name AND a.msg = b.msg)
WHEN matched THEN
  UPDATE
  SET KEY      ='111201401' ,
    ils_module = 'wm' ,
    msg_module = 'PREREC' ,
    log_flag   = 'Y' 
WHEN NOT matched THEN
  INSERT
    (
      message_master_id,
      msg_id,
      KEY,
      ils_module,
      msg_module,
      msg,
      bundle_name,
      msg_class,
      msg_type,
      ovride_role,
      log_flag
    )
    VALUES
    (
      seq_message_master_id.nextval,
      '1401',
      '111201401',
      'wm',
      'PREREC',
      'Invalid Date/Format',
      'ErrorMessage',
      'USER',
      'ERROR',
      NULL,
      'Y'
    );
	
-- Error message for Date entered does not match the Lot ----
merge INTO message_master a USING
(SELECT '1402' msg_id,
  'Date entered does not match the Lot' msg,
  'ErrorMessage' bundle_name
FROM dual
) b ON (a.msg_id = b.msg_id AND a.bundle_name = b.bundle_name AND a.msg = b.msg)
WHEN matched THEN
  UPDATE
  SET KEY      ='111201402' ,
    ils_module = 'wm' ,
    msg_module = 'PREREC' ,
    log_flag   = 'Y' 
WHEN NOT matched THEN
  INSERT
    (
      message_master_id,
      msg_id,
      KEY,
      ils_module,
      msg_module,
      msg,
      bundle_name,
      msg_class,
      msg_type,
      ovride_role,
      log_flag
    )
    VALUES
    (
      seq_message_master_id.nextval,
      '1402',
      '111201402',
      'wm',
      'PREREC',
      'Date entered does not match the Lot',
      'ErrorMessage',
      'USER',
      'ERROR',
      NULL,
      'Y'
    );
	
commit;
CREATE OR REPLACE procedure wm_get_manifest_for_shpmt
(
    p_shipment_id         in shipment.shipment_id%type,
    p_manifest_type_id    in manifest_hdr.manifest_type_id%type,
    p_ship_via            in ship_via.ship_via%type,
    p_date                in date,
    p_shipper_ac_num      in varchar2,
    p_usps_permit_number  in varchar2,
    p_user_id             in varchar2,
    p_out_manifest_id     out manifest_hdr.manifest_id%type
)
as
    v_carrier_id          carrier_code.carrier_id%type;
    v_find_manifest       number(1) := 0;
    v_ship_via            ship_via.ship_via%type;
    v_tc_company_id       ship_via.tc_company_id%type;
    v_facility_id         shipment.o_facility_number%type;
    v_date                date := trunc(p_date);
begin
    select s.manifest_id, s.assigned_ship_via, s.tc_company_id, s.o_facility_number
    into p_out_manifest_id, v_ship_via, v_tc_company_id, v_facility_id
    from shipment s
    where s.shipment_id = p_shipment_id;

    select sv.carrier_id
    into v_carrier_id
    from ship_via sv, facility f
    where sv.ship_via = p_ship_via 
    --and sv.tc_company_id = f.tc_company_id 
    and f.facility_id = v_facility_id
    and sv.marked_for_deletion = 0;

    select case when p_ship_via = v_ship_via
        and exists
        (
            select 1
            from manifest_hdr mh
            where mh.manifest_id = p_out_manifest_id
                and mh.sched_pickup_date = v_date                
         ) then 0 else 1 end
    into v_find_manifest
    from dual;

    if (v_find_manifest = 1)
    then
        -- search
        begin
            select iv.manifest_id
            into p_out_manifest_id
            from
            (
                select mh.manifest_id
                from manifest_hdr mh
                where mh.carrier_id = v_carrier_id
                    and mh.manifest_type_id = p_manifest_type_id
                    and mh.manifest_status_id = 10
                    and mh.sched_pickup_date = v_date					          
                order by mh.sched_pickup_date
            ) iv
            where rownum < 2;
        exception
            when no_data_found then
                wm_auton_find_or_create_manif(v_carrier_id, p_manifest_type_id,
                    v_date, p_ship_via, p_shipper_ac_num, p_usps_permit_number,
                    v_facility_id, v_tc_company_id, p_user_id, p_out_manifest_id);
        end;
    end if;
end;
/
show errors;
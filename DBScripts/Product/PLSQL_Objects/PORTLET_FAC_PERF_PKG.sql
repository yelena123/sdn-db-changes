CREATE OR REPLACE PACKAGE BODY PORTLET_FAC_PERF_PKG
IS
   PROCEDURE PORTLET_FAC_PERF_PROC (
      ENDIDX         IN     NUMBER,
      STARTIDX       IN     NUMBER,
      PLOGINID       IN     VIEW_EMP_LATEST_INFO.EMP_ID%TYPE,
      PROLLINGDAYS   IN     NUMBER,
      TOTAL             OUT NUMBER,
      OUTREF            OUT AN_CURSOR)
   AS
      TYPE r_cursor IS REF CURSOR;

      APPCURSOR          R_CURSOR;
      EPSTR              VARCHAR2 (50);
      OSSTR              VARCHAR2 (50);
      EFFSTR             VARCHAR2 (50);
      UTILSTR            VARCHAR2 (50);
      --DAYSTR varchar2(10);
      WHSE               VIEW_EMP_LATEST_INFO.WHSE%TYPE;
      CLOCK_IN_DATE      E_CONSOL_PERF_SMRY.CLOCK_IN_DATE%TYPE;
      FULL_QUERY         VARCHAR (30000);
      QUERY_STR          VARCHAR (30000);
      COUNT_QUERY        VARCHAR2 (4000);
      COL_FORMULA_1      VARCHAR (1024);
      COL_FORMULA_2      VARCHAR (1024);
      COL_FORMULA_3      VARCHAR (1024);
      COL_FORMULA_4      VARCHAR (1024);
      COL_FORMULA_5      VARCHAR (1024);
      COL_FORMULA_6      VARCHAR (1024);
      COL_FORMULA_7      VARCHAR (1024);
      COL_FORMULA_8      VARCHAR (1024);
      COL_FORMULA_9      VARCHAR (1024);
      COL_FORMULA_10     VARCHAR (1024);
      COL_FORMULA_11     VARCHAR (1024);
      COL_FORMULA_12     VARCHAR (1024);
      COL_FORMULA_13     VARCHAR (1024);
      COL_FORMULA_14     VARCHAR (1024);
      COL_FORMULA_15     VARCHAR (1024);
      COL_VALUE_17       NUMBER (22, 4);
      COL_VALUE_18       NUMBER (22, 4);
      COL_VALUE_19       NUMBER (22, 4);
      ----------------Inflated EP%---------------------
      EP_NUMERATOR       VARCHAR (300);
      EP_DENOMINATOR     VARCHAR (300);
      EFF_NUMERATOR      VARCHAR (300);
      EFF_DENOMINATOR    VARCHAR (300);
      UTIL_NUMERATOR     VARCHAR (300);
      UTIL_DENOMINATOR   VARCHAR (300);
      OS_NUMERATOR       VARCHAR (300);
      OS_DENOMINATOR     VARCHAR (300);
      TOTAL_TIME         VARCHAR (300);
      formula_name       VARCHAR (100);
      formula_value      VARCHAR (512);
      ERROR_MSG_STR      VARCHAR (4000);
      NUM_RECS           INTEGER := 0;
   ----------------Inflated EP%---------------------
   BEGIN
      SELECT REPORT_COLUMN_FORMULA_1,
             REPORT_COLUMN_FORMULA_2,
             REPORT_COLUMN_FORMULA_3,
             REPORT_COLUMN_FORMULA_4,
             REPORT_COLUMN_FORMULA_5,
             REPORT_COLUMN_FORMULA_6,
             REPORT_COLUMN_FORMULA_7,
             REPORT_COLUMN_FORMULA_8,
             REPORT_COLUMN_FORMULA_9,
             REPORT_COLUMN_FORMULA_10,
             REPORT_COLUMN_FORMULA_11,
             REPORT_COLUMN_FORMULA_12,
             REPORT_COLUMN_FORMULA_13,
             REPORT_COLUMN_FORMULA_14,
             REPORT_COLUMN_FORMULA_15,
             OVERALL_EP_PERCENT,
             OVERALL_QA_PERCENT,
             OVERALL_SAFE_PERCENT
        INTO COL_FORMULA_1,
             COL_FORMULA_2,
             COL_FORMULA_3,
             COL_FORMULA_4,
             COL_FORMULA_5,
             COL_FORMULA_6,
             COL_FORMULA_7,
             COL_FORMULA_8,
             COL_FORMULA_9,
             COL_FORMULA_10,
             COL_FORMULA_11,
             COL_FORMULA_12,
             COL_FORMULA_13,
             COL_FORMULA_14,
             COL_FORMULA_15,
             COL_VALUE_17,
             COL_VALUE_18,
             COL_VALUE_19
        FROM WHSE_LABOR_PARAMETERS
       WHERE FACILITY_ID =
                (SELECT FACILITY_ID
                   FROM FACILITY F
                  WHERE F.INBOUND_REGION_ID =
                           (SELECT parameter_value
                              FROM user_default
                             WHERE     parameter_name =
                                          'USER_DEFAULT_REGION_ID'
                                   AND ucl_user_id = PLOGINID));

      IF COL_FORMULA_1 IS NULL
      THEN
         COL_FORMULA_1 := 'OSDL';
      END IF;

      IF COL_FORMULA_2 IS NULL
      THEN
         COL_FORMULA_2 := 'OSIL';
      END IF;

      IF COL_FORMULA_3 IS NULL
      THEN
         COL_FORMULA_3 := 'UDIL';
      END IF;

      IF COL_FORMULA_4 IS NULL
      THEN
         COL_FORMULA_4 := 'NSDL';
      END IF;

      IF COL_FORMULA_5 IS NULL
      THEN
         COL_FORMULA_5 := 'PAID_BRK+UNPAID_BRK';
      END IF;

      IF COL_FORMULA_6 IS NULL
      THEN
         COL_FORMULA_6 := 'UIL';
      END IF;

      ----------------Inflated EP%---------------------
      IF COL_FORMULA_7 IS NOT NULL
      THEN
         EP_NUMERATOR := COL_FORMULA_7;
      END IF;

      IF COL_FORMULA_8 IS NOT NULL
      THEN
         EP_DENOMINATOR := COL_FORMULA_8;
      END IF;

      IF COL_FORMULA_13 IS NOT NULL
      THEN
         EFF_NUMERATOR := COL_FORMULA_13;
      END IF;

      IF COL_FORMULA_14 IS NOT NULL
      THEN
         EFF_DENOMINATOR := COL_FORMULA_14;
      END IF;

      IF COL_FORMULA_9 IS NOT NULL
      THEN
         UTIL_NUMERATOR := COL_FORMULA_9;
      END IF;

      IF COL_FORMULA_10 IS NOT NULL
      THEN
         UTIL_DENOMINATOR := COL_FORMULA_10;
      END IF;

      IF COL_FORMULA_11 IS NOT NULL
      THEN
         OS_NUMERATOR := COL_FORMULA_11;
      END IF;

      IF COL_FORMULA_12 IS NOT NULL
      THEN
         OS_DENOMINATOR := COL_FORMULA_12;
      END IF;

      IF COL_FORMULA_15 IS NOT NULL
      THEN
         TOTAL_TIME := COL_FORMULA_15;
      END IF;

      IF EP_NUMERATOR IS NULL
      THEN
         EP_NUMERATOR := 'TOTAL_PAM';
      END IF;

      IF EP_DENOMINATOR IS NULL
      THEN
         EP_DENOMINATOR :=
            'EP_TOTAL_TIME - PAID_BRK - UNPAID_BRK - ADJ_OSDL - ADJ_OSIL - NSDL - SIL - UDIL  + NSDL_BRK_OVRLP + UDIL_BRK_OVRLP';
      END IF;

      IF EFF_NUMERATOR IS NULL
      THEN
         EFF_NUMERATOR := 'TOTAL_PAM';
      END IF;

      IF EFF_DENOMINATOR IS NULL
      THEN
         EFF_DENOMINATOR := 'TOTAL_TIME';
      END IF;

      IF UTIL_NUMERATOR IS NULL
      THEN
         UTIL_NUMERATOR := 'NSDL+OSDL';
      END IF;

      IF UTIL_DENOMINATOR IS NULL
      THEN
         UTIL_DENOMINATOR := 'TOTAL_TIME';
      END IF;

      IF OS_NUMERATOR IS NULL
      THEN
         OS_NUMERATOR := 'OSIL+OSDL';
      END IF;

      IF OS_DENOMINATOR IS NULL
      THEN
         OS_DENOMINATOR := 'TOTAL_TIME';
      END IF;

      IF TOTAL_TIME IS NULL
      THEN
         TOTAL_TIME := 'NSDL+OSDL';
      END IF;

      IF COL_VALUE_19 IS NULL
      THEN
         COL_VALUE_19 := '0';
      END IF;

      EPSTR := 'Employee Performance';
      OSSTR := 'On Standard';
      EFFSTR := 'Efficiency';
      UTILSTR := 'Utilization';

      --DAYSTR := 'DAY';
      DBMS_OUTPUT.put_line (PLOGINID);
      DBMS_OUTPUT.put_line (PROLLINGDAYS);

      SELECT EMP.WHSE,
             FN_DATEADD ('DAY',
                         -PROLLINGDAYS,
                         SYSDATE,
                         EMP.WHSE)
        INTO WHSE, CLOCK_IN_DATE
        FROM E_CONSOL_PERF_SMRY ECPF
             INNER JOIN VIEW_EMP_LATEST_INFO EMP ON ECPF.WHSE = EMP.WHSE
       WHERE EMP.EMP_ID = PLOGINID AND ROWNUM < 2;

      -- dbms_output.put_line('str');

      -- SAMPLE QUERY:
      --select 'EP %' as XAXIS, 100*(SUM(CPS.TOTAL_PAM)/SUM(CPS.TOTAL_TIME)) as YAXIS from E_CONSOL_PERF_SMRY CPS inner join VIEW_EMP_LATEST_INFO EMP on CPS.WHSE = EMP.WHSE
      --where EMP.EMP_ID = 4 and CPS.CLOCK_IN_DATE >= FN_DATEADD('DAY', -7, sysdate, '01')
      --union all
      --select 'OS %' as XAXIS, 100*(SUM(CPS.TOTAL_PAM)/SUM(CPS.TOTAL_TIME)) as YAXIS from E_CONSOL_PERF_SMRY CPS inner join VIEW_EMP_LATEST_INFO EMP on CPS.WHSE = EMP.WHSE
      --where EMP.EMP_ID = 4 and CPS.CLOCK_IN_DATE >= FN_DATEADD('DAY', -7, sysdate, '01')
      QUERY_STR :=
            'SELECT '
         || ''''
         || EPSTR
         || ''' as XAXIS, case when SUM($g)=0.0 then 0.0
                                                            when SUM($h)=0.0 then 0.0
                                                            else ROUND(100*(SUM($g)/SUM($h)),2) end AS YAXIS FROM E_CONSOL_PERF_SMRY ECPF
          where ECPF.WHSE = '
         || ''''
         || WHSE
         || ''''
         || ' and ECPF.CLOCK_IN_DATE >= CLOCK_IN_DATE and ECPF.CLOCK_IN_STATUS NOT IN (90, 99)
         union all
          SELECT '
         || ''''
         || UTILSTR
         || ''' as XAXIS, case when SUM($k)=0.0 then 0.0
                                                            when SUM($l)=0.0 then 0.0
                                                            else ROUND(100*(SUM($k)/SUM($l)),2) end AS YAXIS FROM E_CONSOL_PERF_SMRY ECPF
          where ECPF.WHSE = '
         || ''''
         || WHSE
         || ''''
         || ' and ECPF.CLOCK_IN_DATE >= CLOCK_IN_DATE and ECPF.CLOCK_IN_STATUS NOT IN (90, 99)
          union all
          SELECT '
         || ''''
         || EFFSTR
         || ''' as XAXIS, case when SUM($i)=0.0 then 0.0
                                                          when SUM($j)=0.0 then 0.0
                                                          else ROUND(100*(SUM($i)/SUM($j)),2) end AS YAXIS FROM E_CONSOL_PERF_SMRY ECPF
          where ECPF.WHSE = '
         || ''''
         || WHSE
         || ''''
         || ' and ECPF.CLOCK_IN_DATE >= CLOCK_IN_DATE and ECPF.CLOCK_IN_STATUS NOT IN (90, 99)
          union all
          SELECT '
         || ''''
         || OSSTR
         || ''' as XAXIS, case when SUM($m)=0.0 then 0.0
                                                          when SUM($n)=0.0 then 0.0
                                                          else ROUND(100*(SUM($m)/SUM($n)),2) end AS YAXIS FROM E_CONSOL_PERF_SMRY ECPF
          where ECPF.WHSE = '
         || ''''
         || WHSE
         || ''''
         || ' and ECPF.CLOCK_IN_DATE >= CLOCK_IN_DATE and ECPF.CLOCK_IN_STATUS NOT IN (90, 99)';
      --ROUND($p,5) as  EPP
      QUERY_STR := REPLACE (QUERY_STR, '$a', COL_FORMULA_1);
      QUERY_STR := REPLACE (QUERY_STR, '$b', COL_FORMULA_2);
      QUERY_STR := REPLACE (QUERY_STR, '$c', COL_FORMULA_3);
      QUERY_STR := REPLACE (QUERY_STR, '$d', COL_FORMULA_4);
      QUERY_STR := REPLACE (QUERY_STR, '$e', COL_FORMULA_5);
      QUERY_STR := REPLACE (QUERY_STR, '$f', COL_FORMULA_6);
      -----------------Inflated EP%----------------------
      QUERY_STR := REPLACE (QUERY_STR, '$g', EP_NUMERATOR);
      QUERY_STR := REPLACE (QUERY_STR, '$h', EP_DENOMINATOR);
      QUERY_STR := REPLACE (QUERY_STR, '$i', EFF_NUMERATOR);
      QUERY_STR := REPLACE (QUERY_STR, '$j', EFF_DENOMINATOR);
      QUERY_STR := REPLACE (QUERY_STR, '$k', UTIL_NUMERATOR);
      QUERY_STR := REPLACE (QUERY_STR, '$l', UTIL_DENOMINATOR);
      QUERY_STR := REPLACE (QUERY_STR, '$m', OS_NUMERATOR);
      QUERY_STR := REPLACE (QUERY_STR, '$n', OS_DENOMINATOR);
      QUERY_STR := REPLACE (QUERY_STR, '$p', COL_VALUE_17);
      QUERY_STR := REPLACE (QUERY_STR, '$q', COL_VALUE_18);
      QUERY_STR := REPLACE (QUERY_STR, '$r', COL_VALUE_19);
      QUERY_STR := REPLACE (QUERY_STR, '$r', COL_VALUE_19);
      QUERY_STR := REPLACE (QUERY_STR, 'EP_TOTAL_TIME', TOTAL_TIME);
      QUERY_STR := REPLACE (QUERY_STR, 'OSDL_PAID_BRK', 'PAID_OVERLAP_OSDL');
      QUERY_STR :=
         REPLACE (QUERY_STR, 'OSDL_UNPAID_BRK', 'UNPAID_OVERLAP_OSDL');
      QUERY_STR := REPLACE (QUERY_STR, 'NSDL_PAID_BRK', 'PAID_OVERLAP_NSDL');
      QUERY_STR :=
         REPLACE (QUERY_STR, 'NSDL_UNPAID_BRK', 'UNPAID_OVERLAP_NSDL');
      QUERY_STR := REPLACE (QUERY_STR, 'OSIL_PAID_BRK', 'PAID_OVERLAP_OSIL');
      QUERY_STR :=
         REPLACE (QUERY_STR, 'OSIL_UNPAID_BRK', 'UNPAID_OVERLAP_OSIL');
      QUERY_STR := REPLACE (QUERY_STR, 'UDIL_PAID_BRK', 'PAID_OVERLAP_UDIL');
      QUERY_STR :=
         REPLACE (QUERY_STR, 'UDIL_UNPAID_BRK', 'UNPAID_OVERLAP_UDIL');
      QUERY_STR :=
         REPLACE (QUERY_STR,
                  'OSDL_BRK_OVRLP',
                  '(PAID_OVERLAP_OSDL + UNPAID_OVERLAP_OSDL)');
      QUERY_STR :=
         REPLACE (QUERY_STR,
                  'NSDL_BRK_OVRLP',
                  '(PAID_OVERLAP_NSDL + UNPAID_OVERLAP_NSDL)');
      QUERY_STR :=
         REPLACE (QUERY_STR,
                  'OSIL_BRK_OVRLP',
                  '(PAID_OVERLAP_OSIL + UNPAID_OVERLAP_OSIL)');
      QUERY_STR :=
         REPLACE (QUERY_STR,
                  'UDIL_BRK_OVRLP',
                  '(PAID_OVERLAP_UDIL + UNPAID_OVERLAP_UDIL)');
      -----------------Inflated EP%----------------------
      FULL_QUERY :=
            'select XAXIS,YAXIS from ( select rownum R, MAIN_SUBQUERY.* from ('
         || QUERY_STR
         || ') main_subquery  WHERE rownum <= '
         || ENDIDX
         || ' ) second_subquery where R >= '
         || STARTIDX;

      --dbms_output.put_line(FULL_QUERY);

      OPEN OUTREF FOR FULL_QUERY;

      COUNT_QUERY := 'select 1 from dual';

      OPEN appCursor FOR COUNT_QUERY;

      FETCH APPCURSOR INTO TOTAL;

      CLOSE APPCURSOR;
   END PORTLET_FAC_PERF_PROC;
END PORTLET_FAC_PERF_PKG;
/
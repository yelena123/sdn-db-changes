create or replace procedure wm_auton_get_nxt_up_cnt_id
(
    p_facility_id       in       nxt_up_cnt.facility_id%type,
    p_csv_companyid     in       varchar2,
    p_rectypeid         in       nxt_up_cnt.rec_type_id%type,
    p_lock_time_out     in       pls_integer default 40,
    p_userid            in       nxt_up_cnt.user_id%type,
    p_nbr_nxtupnbrs     in       number default 1,
    po_whse             out      nxt_up_cnt.whse%type,
    po_rec_type_id      out      nxt_up_cnt.rec_type_id%type,
    po_pfx_field        out      nxt_up_cnt.pfx_field%type,
    po_pfx_len          out      nxt_up_cnt.pfx_len%type,
    po_start_nbr        out      nxt_up_cnt.start_nbr%type,
    po_end_nbr          out      nxt_up_cnt.end_nbr%type,
    po_curr_nbr         out      nxt_up_cnt.curr_nbr%type,
    po_nbr_len          out      nxt_up_cnt.nbr_len%type,
    po_incr_value       out      nxt_up_cnt.incr_value%type,
    po_nxt_start_nbr    out      nxt_up_cnt.nxt_start_nbr%type,
    po_nxt_end_nbr      out      nxt_up_cnt.nxt_end_nbr%type,
    po_chk_digit_type   out      nxt_up_cnt.chk_digit_type%type,
    po_chk_digit_len    out      nxt_up_cnt.chk_digit_len%type,
    po_repeat_range     out      nxt_up_cnt.repeat_range%type,
    po_cd_master_id     out      nxt_up_cnt.cd_master_id%type,
    po_nxt_up_cnt_id    out      nxt_up_cnt.nxt_up_cnt_id%type,
    po_pfx_type	        out      nxt_up_cnt.pfx_type%type,
    po_rev_pfx_order    out      nxt_up_cnt.rev_order%type,
    p_rc                out      number
)
as 
    pragma autonomous_transaction;
begin

    manh_get_nxt_up_cnt_id (p_facility_id, p_csv_companyid, p_rectypeid, p_lock_time_out, p_userid,
            p_nbr_nxtupnbrs, po_whse, po_rec_type_id, po_pfx_field,
            po_pfx_len, po_start_nbr, po_end_nbr, po_curr_nbr,
            po_nbr_len, po_incr_value, po_nxt_start_nbr,
            po_nxt_end_nbr, po_chk_digit_type, po_chk_digit_len,
            po_repeat_range, po_cd_master_id, po_nxt_up_cnt_id,
            po_pfx_type, po_rev_pfx_order, p_rc);
                
end;
/

show errors ;
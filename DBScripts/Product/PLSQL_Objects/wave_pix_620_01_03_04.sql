create or replace procedure wave_pix_620_01_03_04
(
    p_ship_wave_nbr    in  varchar2,
    p_whse             in  varchar2,
    p_facility_id      in  facility.facility_id%type,
    p_user_name        in  ucl_user.user_name%type,
    p_zero             in  number
)
as
    v_proc_stat_code                varchar2(2);
    v_xml_group_attr                varchar2(10);
    v_pix620_tmp                    varchar2(4000);
    v_pix620                        varchar2(4000);
    e_pix620                        varchar2(4000);
    v_ins_ref_col_string            varchar2(1000);
    v_sel_ref_col_string            varchar2(1000);
    va_bu_cnfg                      wm_utils.ta_bu_cnfg;
    v_cnfg_str                      varchar2(32000);
    v_bu_inlist_clause              varchar2(1000);
begin
    v_pix620_tmp 
        := ' insert into pix_tran '                               
        || ' ('
        || '   item_name, pix_tran_id, tran_type, tran_code, tran_nbr, sys_user_id, '     
        || '   pix_seq_nbr, proc_stat_code, whse, season, season_yr, style,' 
        || '   style_sfx, color, color_sfx, sec_dim, qual, size_desc,'  
        || '   size_range_code,size_rel_posn_in_table,invn_type, prod_stat,' 
        || '   batch_nbr, sku_attr_1, sku_attr_2, sku_attr_3, sku_attr_4,'  
        || '   sku_attr_5, cntry_of_orgn, invn_adjmt_qty, invn_adjmt_type,'  
        || '   wt_adjmt_qty, wt_adjmt_type, uom, actn_code, create_date_time, '    
        || '   mod_date_time, user_id, wm_version_id, item_id, facility_id, '         
        || '   tc_company_id, company_code, xml_group_id ';

    for pix_rec in
    (
        select '620' tran_type, '01' tran_code, '04' actn_code from dual
        union all
        select '620' tran_type, '03' tran_code, '04' actn_code from dual
    )
    loop
        --correct input param mappings
        wm_merge_pix_cnfg_for_elgbl_bu(p_ucl_user_name => p_user_name,
            p_facility_id => p_facility_id, p_tran_type => pix_rec.tran_type, 
            p_tran_code => pix_rec.tran_code, p_actn_code => pix_rec.actn_code,
            pa_bu_cnfg => va_bu_cnfg);

        v_cnfg_str := va_bu_cnfg.first;
        while (v_cnfg_str is not null)
        loop
            v_bu_inlist_clause := ' and orders.tc_company_id in (' || va_bu_cnfg(v_cnfg_str) || ')';
            wm_extract_pix_cnfg_data(v_cnfg_str, v_proc_stat_code, v_xml_group_attr,
                v_ins_ref_col_string, v_sel_ref_col_string);
                
            v_pix620 := v_pix620_tmp || v_ins_ref_col_string;
            e_pix620 := v_pix620                                      
                ||' ) '
                ||'select item_cbo.item_name, pix_tran_id_seq.nextval,'                                                     
                ||'    ''620'', :tran_code, pix_tran_id_seq.nextval, :user_id,'
                ||'    rownum, :proc_stat_code, :whse, '   
                ||'    item_cbo.item_season, item_cbo.item_season_year,' 
                ||'    item_cbo.item_style, item_cbo.item_style_sfx,'             
                ||'    item_cbo.item_color, item_cbo.item_color_sfx,'             
                ||'    item_cbo.item_second_dim, item_cbo.item_quality,'
                ||'    item_cbo.item_size_desc, item_wms.size_range_code,'
                ||'    item_wms.size_rel_posn_in_table, order_line_item.invn_type,'
                ||'    order_line_item.prod_stat,order_line_item.batch_nbr, '      
                ||'    order_line_item.item_attr_1, order_line_item.item_attr_2,'
                ||'    order_line_item.item_attr_3, order_line_item.item_attr_4,' 
                ||'    order_line_item.item_attr_5, order_line_item.cntry_of_orgn,'
                ||'    order_line_item.allocated_qty, ''A'', '
                ||'    order_line_item.allocated_qty * coalesce(order_line_item.unit_wt, '
                ||'    item_cbo.unit_weight, 0), ''A'','
                ||'    size_uom.size_uom,'
                ||'    :actn_code, current_timestamp, current_timestamp, '
                ||'    :user_id, 1, order_line_item.item_id, :facility_id,'
                ||'    order_line_item.tc_company_id, company.company_code, '
                || ( case when v_xml_group_attr is null then 'null' 
                         else '''' || v_xml_group_attr ||''' ' end ) 
                || replace(v_sel_ref_col_string,'ORDER_LINE_ITEM.TC_ORDER_LINE_ID',
                   'prnt.tc_order_line_id') 
                || ' '   
                ||'from orders '                                          
                ||'join order_line_item on order_line_item.order_id = orders.order_id'
                ||'    and order_line_item.allocated_qty > ' || to_char(p_zero)
                ||'    and orders.is_original_order = 1 '
                ||'    and coalesce(orders.wm_order_status,0) != 12 '
                ||'    and orders.o_facility_id = :facility_id '   
                ||'join order_line_item prnt on prnt.order_id=orders.order_id '
                ||'    and coalesce(order_line_item.substituted_parent_line_id,'
                ||'        order_line_item.line_item_id) = prnt.line_item_id '           
                ||'join item_cbo on item_cbo.item_id = order_line_item.item_id '             
                ||'join item_wms on item_wms.item_id = item_cbo.item_id '             
                ||'join size_uom on size_uom.size_uom_id = order_line_item.qty_uom_id_base '
                ||'join company on company.company_id = orders.tc_company_id '           
                ||'    and order_line_item.ship_wave_nbr = :ship_wave_nbr '
                ||'    and coalesce(order_line_item.fulfillment_type, ''1'')= :fulfillment_type ' 
                ||'where order_line_item.do_dtl_status = 130 '
                || v_bu_inlist_clause;
        
            if ( instr(e_pix620, 'TASK_DTL.', 1, 1)  > 0 or instr(e_pix620, 'TASK_HDR.', 1, 1) > 0)
            then
                e_pix620 := replace(e_pix620, 'where ',
                    ' left join task_dtl on task_dtl.line_item_id = order_line_item.line_item_id where ');
            end if;
            
            if instr(e_pix620, 'TASK_HDR.', 1, 1) > 0
            then
                e_pix620 := replace(e_pix620, 'where ',
                    ' left join task_hdr on task_hdr.task_id = task_dtl.task_id where ');
            end if;
        
            execute immediate e_pix620 using pix_rec.tran_code, p_user_name, 
                v_proc_stat_code, p_whse, pix_rec.actn_code, p_user_name, 
                p_facility_id, p_facility_id,p_ship_wave_nbr, 
                case pix_rec.tran_code when '01' then '1' else '3' end;
            wm_log('Released 620 ' || pix_rec.tran_code ||' '||  pix_rec.actn_code 
                ||' '|| sql%rowcount, p_ref_code_1 => 'PIX', p_ref_value_1 => p_ship_wave_nbr);
           
            -- move along to the sql for the next set of BU's
            v_cnfg_str := va_bu_cnfg.next(v_cnfg_str);
        end loop;
    end loop;
end;
/
show errors;

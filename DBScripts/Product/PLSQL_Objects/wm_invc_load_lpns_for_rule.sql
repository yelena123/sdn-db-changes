create or replace procedure wm_invc_load_lpns_for_rule
(
    p_whse              in varchar2,
    p_invc_parm_id      in invc_parm.invc_parm_id%type,
    p_check_manifest    in number
)
as
    v_select_rule       varchar2(4000);
    v_rule_sql          varchar2(32600);
    v_facility_id       facility.facility_id%type;
begin
    select f.facility_id
    into v_facility_id
    from facility f
    where f.whse = p_whse;

    for rule_rec in
    (
        select rh.rule_id
        from rule_hdr rh
        where rh.rule_type = 'IS' and rh.stat_code = 0
            and exists
            (
                select 1
                from invc_rule_parm irp
                where irp.rule_id = rh.rule_id and irp.invc_parm_id = p_invc_parm_id
            )
    )
    loop
        select manh_get_rule(rule_rec.rule_id, 0, 'IS')
        into v_select_rule
        from dual;

        if (v_select_rule is not null)
        then
            v_rule_sql :=
                '    insert into tmp_invc_lpn_mastr_list '
                || ' ('
                || '    lpn_id, order_id '
                || ' )'
                || ' select distinct lpn.lpn_id, lpn.order_id '
                || ' from lpn  '
                || ' where lpn.c_facility_id = :facility_id and lpn.lpn_type = 1'
                || '    and lpn.inbound_outbound_indicator = ''O'''
                || '    and lpn.lpn_facility_status >= 20 and lpn.lpn_facility_status < 90'
                || '    and lpn.order_id is not null' ;

            v_rule_sql := v_rule_sql || ' and ' || v_select_rule;

            wm_add_joins_for_batch_invc(v_rule_sql);

            v_rule_sql := v_rule_sql
                || '    and not exists'
                || '    ('
                || '        select 1'
                || '        from tmp_invc_lpn_mastr_list t'
                || '        where t.lpn_id = lpn.lpn_id'
                || '    )'
                || ' log errors (to_char(sysdate))';

            execute immediate v_rule_sql using v_facility_id;
            wm_cs_log('Lpns matching rule ' || rule_rec.rule_id || ' are ' || sql%rowcount);
        end if;
    end loop;
end;
/

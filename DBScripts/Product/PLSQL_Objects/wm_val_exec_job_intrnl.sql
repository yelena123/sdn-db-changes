create or replace procedure wm_val_exec_job_intrnl
(
    p_user_id      in user_profile.user_id%type,
    p_job_name     in val_job_hdr.job_name%type,
    p_cursor       in number
)
as
    v_dummy_csv    varchar2(1);
    udef_excp      exception;
    pragma         exception_init(udef_excp, -20050);
begin
    for sql_rec in
    (
        select vjd.val_job_dtl_id, vjd.sql_id, vs.bind_var_list, vs.ident,
            regexp_replace(vs.sql_text, '/\*\+[ ]*HINT[ ]*\*/',
                '/*+ ' || regexp_replace(vs.sql_hints, '[\/*]', '') || '*/', 1, 1, 'i') sql_text
        from val_job_hdr vjh
        join val_job_dtl vjd on vjd.val_job_hdr_id = vjh.val_job_hdr_id and vjd.is_locked = 0
        join val_sql vs on vs.sql_id = vjd.sql_id and vs.is_locked = 0
        where vjh.job_name = p_job_name and vjh.is_locked = 0
        order by vjd.val_job_dtl_id
    )
    loop
        begin
            wm_val_log('--->' || sql_rec.ident || ': ' || substr(sql_rec.sql_text, 1, 485));
            wm_val_exec_sql_intrnl(p_cursor, sql_rec.sql_id, sql_rec.sql_text, sql_rec.bind_var_list,
                sql_rec.val_job_dtl_id, p_bind_val_csv => null, p_txn_id => wm_val_get_txn_id(), 
                p_result_csv => v_dummy_csv);
        exception
            when udef_excp then
                wm_val_add_job_result(sql_rec.val_job_dtl_id, p_msg => sqlerrm,
                    p_last_run_passed => 0);
            when others then
                wm_val_log(sqlerrm, p_sql_log_lvl => 0);
                raise;
        end;
    end loop;
end;
/
show errors;

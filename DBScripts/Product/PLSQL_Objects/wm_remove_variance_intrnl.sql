create or replace procedure wm_remove_variance_intrnl
(
    p_user_id             in user_profile.user_id%type,
    p_is_cm               in number,
    p_is_cs_or_cm         in number,
    p_caller_id           in varchar2,
    p_act_trk_tran_nbr    in prod_trkg_tran.tran_nbr%type,
    p_act_trk_rec_count   in out number
)
as
    v_shipment_ref_code varchar2(3) := '41';
    v_order_ref_code    varchar2(3) := '01';
    v_lpn_ref_code      varchar2(3) := '02';
begin
    -- this picks up orphaned lpns too; if any are NON splits, it may also pick 
    -- up lpns dumped off as variance from other unassign or close processes
    if (p_is_cs_or_cm = 1)
    then
        insert into tmp_variance_lpns
        (
            lpn_id
        )
        select l.lpn_id
        from lpn l
        where l.lpn_facility_status < 40
            and exists
            (
                select 1
                from tmp_order_splits_scratch t
                where t.order_id = l.order_id 
                    and coalesce(t.order_split_id, -1) = coalesce(l.order_split_id, -1)
            );
        wm_cs_log('Collected variance lpns on locked splits ' || sql%rowcount);
    else
        insert into tmp_variance_lpns
        (
            lpn_id
        )
        select l.lpn_id
        from lpn l
        where l.lpn_facility_status < 40
            and exists
            (
                select 1
                from tmp_lpn_list t
                where t.lpn_id = l.lpn_id
            )
            and exists
            (
                select 1
                from tmp_order_splits_scratch t
                where t.order_id = l.order_id 
                    and coalesce(t.order_split_id, -1) = coalesce(l.order_split_id, -1)
            )
            and
            (
                l.shipment_id is not null
                or exists
                (
                    select 1
                    from order_split os
                    where os.order_split_id = l.order_split_id and os.is_cancelled = 0
                        and os.shipment_id is not null
                )
                or exists
                (
                    select 1
                    from orders o
                    where o.order_id = l.order_id and o.has_split = 0
                        and o.shipment_id is not null
                )
            );
        wm_cs_log('Collected lpns for unassignment ' || sql%rowcount);
        
        delete from tmp_order_splits_scratch t
        where not exists
        (
            select 1
            from lpn l
            where l.order_id = t.order_id
                and coalesce(l.order_split_id, -1) = coalesce(t.order_split_id, -1)
                and exists
                (
                    select 1
                    from tmp_variance_lpns t2 
                    where t2.lpn_id = l.lpn_id
                )
        );
        wm_cs_log('Removed splits no longer tied to lpns eligible for unassignment ' || sql%rowcount);
    end if;

    insert into tmp_oli_splits
    (
        order_id, line_item_id, order_split_id, order_qty, allocated_qty, 
        oli_split_wt, oli_split_vol, removed_lpn_qty, removed_lpn_wt, 
        removed_lpn_vol, retained_lpn_qty, retained_lpn_wt, retained_lpn_vol,
        removed_units_pakd
    )
    select giv.order_id, giv.line_item_id, giv.order_split_id, min(giv.order_qty),
        min(giv.allocated_qty), min(giv.oli_split_wt), min(giv.oli_split_vol),
        coalesce(sum(
            case when t.lpn_id is not null then
                case when l.lpn_facility_status < 20 then ld.initial_qty else ld.size_value end 
            else 0 
            end
        ), 0) removed_lpn_qty,
        coalesce(sum(
            case when t.lpn_id is not null and ld.weight_uom_id_base > 0 then 
                coalesce(nullif(ld.weight, 0), ld.estimated_weight) 
            else 0 
            end
        ), 0) removed_lpn_wt,
        coalesce(sum(
            case when t.lpn_id is not null and ld.volume_uom_id_base > 0 then ld.estimated_volume
            else 0 
            end
        ), 0) removed_lpn_vol,
        coalesce(sum(
            case when t.lpn_id is null and l.lpn_id is not null then
                case when l.lpn_facility_status < 20 then ld.initial_qty else ld.size_value end
            else 0
            end
        ), 0) retained_lpn_qty,
        coalesce(sum(
            case when t.lpn_id is null and l.lpn_id is not null
                and ld.weight_uom_id_base > 0 then
                    coalesce(nullif(ld.weight, 0), ld.estimated_weight)
            else 0
            end
        ), 0) retained_lpn_wt,
        coalesce(sum(
            case when t.lpn_id is null and l.lpn_id is not null
                and ld.volume_uom_id_base > 0 then ld.estimated_volume
            else 0
            end
        ), 0) retained_lpn_vol,
        coalesce(sum(
            case when t.lpn_id is not null then ld.size_value else 0 end
        ), 0) removed_units_pakd
    from
    (
        select oli.order_id, oli.line_item_id, null order_split_id,
            oli.order_qty, oli.allocated_qty,
            case when oli.weight_uom_id_base > 0
                then coalesce(oli.planned_weight, 0) else 0 end oli_split_wt,
            case when oli.volume_uom_id_base > 0
                then coalesce(oli.planned_volume, 0) else 0 end oli_split_vol
        from tmp_order_splits_scratch t
        join order_line_item oli on oli.order_id = t.order_id
        where t.order_split_id is null
        union all
        select osli.order_id, osli.line_item_id, osli.order_split_id,
            osli.order_qty, osli.allocated_qty, osli.planned_weight oli_split_wt,
            osli.planned_volume oli_split_vol
        from tmp_order_splits_scratch t
        join order_split_line_item osli on osli.order_split_id = t.order_split_id
    ) giv
    left join lpn_detail ld on ld.distribution_order_dtl_id = giv.line_item_id
    left join lpn l on l.lpn_id = ld.lpn_id and l.order_id = giv.order_id
        and coalesce(l.order_split_id, -1) = coalesce(giv.order_split_id, -1)
        and l.lpn_facility_status < 90
    left join tmp_variance_lpns t on t.lpn_id = l.lpn_id
    group by giv.order_id, giv.line_item_id, giv.order_split_id;
    wm_cs_log('Computed line/split level qty data ' || sql%rowcount);

    -- all splits collected in the unassign flow already have a variance as they
    -- are tied to those to-be-removed lpns
    if (p_is_cs_or_cm = 1)
    then
        -- remove splits without a variance on at least one associated line
        -- (and) if they're not unsplit agg orders that are partially allocated
        -- (and) if they're not (misc/non-misc) orders with variance lpns
        delete from tmp_order_splits_scratch t
        where (t.order_id, coalesce(t.order_split_id, -1)) not in
            (
                select l.order_id, coalesce(l.order_split_id, -1)
                from lpn l
                join tmp_variance_lpns t2 on t2.lpn_id = l.lpn_id
            )
            and not exists
            (
                select 1
                from tmp_oli_splits t1
                where t1.order_id = t.order_id 
                    and coalesce(t1.order_split_id, -1) = coalesce(t.order_split_id, -1)
                    and t1.order_qty - t1.retained_lpn_qty > 0
            )
            and not exists
            (
                select 1
                from orders o
                where o.order_id = t.order_id and o.has_split = 0
                    and o.do_status = 120 and o.is_original_order = 0
            );
        wm_cs_log('Removed splits with no variance ' || sql%rowcount, p_sql_log_level => 2);

        delete from tmp_oli_splits t
        where not exists
        (
            select 1
            from tmp_order_splits_scratch t1
            where t1.order_id = t.order_id 
                and coalesce(t1.order_split_id, -1) = coalesce(t.order_split_id, -1)
        );
        wm_cs_log('Removed lines for splits with no variance ' || sql%rowcount, p_sql_log_level => 2);
    end if;

    -- collect splits that are to be completely removed - having full qty 
    -- and lpn variance
    insert into tmp_unplan_splits
    (
        order_id, order_split_id
    )
    select t1.order_id, t1.order_split_id
    from tmp_order_splits_scratch t1
    where not exists
        (
            select 1
            from tmp_oli_splits t2
            where t2.order_id = t1.order_id 
                and coalesce(t2.order_split_id, -1) = coalesce(t1.order_split_id, -1)
                and decode(p_is_cs_or_cm, 1, t2.retained_lpn_qty, t2.order_qty - t2.removed_lpn_qty) > 0
        )
        and not exists
        (
            select 1
            from lpn l
            where l.order_id = t1.order_id
                and coalesce(l.order_split_id, -1) = coalesce(t1.order_split_id, -1)
                and l.shipment_id is not null
                and not exists
                (
                    select 1
                    from tmp_variance_lpns t3
                    where t3.lpn_id = l.lpn_id
                )
        );
    wm_cs_log('Collected splits to be unplanned ' || sql%rowcount);

    if (p_is_cs_or_cm = 0)
    then
        -- we dont need fully unplanned splits any more for unassignment flows
        delete from tmp_order_splits_scratch t1
        where exists
        (
            select 1
            from tmp_unplan_splits t2
            where t2.order_id = t1.order_id 
                and coalesce(t2.order_split_id, -1) = coalesce(t1.order_split_id, -1)
        );
        
        delete from tmp_oli_splits t1
        where exists
        (
            select 1
            from tmp_unplan_splits t2
            where t2.order_id = t1.order_id 
                and coalesce(t2.order_split_id, -1) = coalesce(t1.order_split_id, -1)
        );
    end if;

    -- subtract removed qty from planned splits; delete osli if zeroed; at this
    -- point, splits in the unassign flow all have a partial variance
    merge into order_split_line_item osli
    using
    (
        -- do not do this for previously unsplit orders
        select /*+ full(t) parallel */ t.order_id, t.line_item_id, 
            t.order_split_id, t.removed_lpn_qty, t.removed_units_pakd,
            decode(p_is_cs_or_cm, 1, t.order_qty - t.retained_lpn_qty, t.removed_lpn_qty) removed_qty,
            decode(p_is_cs_or_cm, 1, t.oli_split_wt - t.retained_lpn_wt, t.removed_lpn_wt) removed_wt,
            decode(p_is_cs_or_cm, 1, t.oli_split_vol - t.retained_lpn_vol, t.removed_lpn_vol) removed_vol,
            decode(p_is_cs_or_cm, 1, t.allocated_qty - t.retained_lpn_qty, t.removed_lpn_qty) removed_allocated_qty
        from tmp_oli_splits t
        where t.order_split_id is not null
            and decode(p_is_cs_or_cm, 1, t.order_qty - t.retained_lpn_qty, t.removed_lpn_qty) > 0
    ) iv on (iv.order_id = osli.order_id and iv.line_item_id = osli.line_item_id
        and osli.order_split_id = iv.order_split_id)
    when matched then
    update set osli.order_qty = osli.order_qty - iv.removed_qty,
        osli.planned_weight = osli.planned_weight - iv.removed_wt,
        osli.planned_volume = osli.planned_volume - iv.removed_vol,
        osli.units_packed = coalesce(osli.units_packed, 0) - iv.removed_units_pakd,
        osli.allocated_qty = osli.allocated_qty - iv.removed_allocated_qty,
        osli.last_updated_source = p_user_id, osli.last_updated_dttm = sysdate
    delete where osli.order_qty <= 0;
    wm_cs_log('OSLI variance reduction ' || sql%rowcount);

    -- in CS, this can cancel existing splits and nullify shpmt on them; that's
    -- why the osli qty reduction above has to occur prior to this
    wm_unplan_splits_intrnl(p_user_id, p_is_cs_or_cm => p_is_cs_or_cm);

    -- every split at this point has qty being removed; create unplanned splits
    -- create a new unplanned split if a) the order is already split or 
    -- b) an unsplit order is not being completely unassigned the distinct may 
    -- be necessary for unassign or CM flows
    insert into order_split
    (
        shipment_id, created_dttm, created_source, last_updated_dttm, 
        last_updated_source, order_split_status, order_split_id, tc_company_id, 
        order_id, order_split_seq, created_source_type, last_updated_source_type,
        original_assigned_ship_via
    )
    select null, sysdate, p_user_id, sysdate, p_user_id, 5,
        seq_order_split_id.nextval, o.tc_company_id, iv.order_id, 
        (
            select coalesce(max(os1.order_split_seq), 0) + 1 
            from order_split os1 
            where os1.order_id = iv.order_id
        ) order_split_seq, 
        5 created_source_type, 5 last_updated_source_type, 
        decode(p_is_cs_or_cm, 1, 'NON', null)
    from
    (
        -- cs/cm - look for NON splits
        select distinct t1.order_id
        from tmp_order_splits_scratch t1
        where p_is_cs_or_cm = 1
            and
            (
                t1.order_split_id is not null
                or not exists
                (
                    select 1
                    from tmp_unplan_splits t2
                    where t2.order_id = t1.order_id
                )
            )
            and not exists
            (
                select 1
                from order_split os
                left join shipment s on s.shipment_id = os.shipment_id
                where os.order_id = t1.order_id and os.is_cancelled = 0
                    and os.original_assigned_ship_via = 'NON'
                    and (os.shipment_id is null or s.shipment_closed_indicator = 0)
            )
        -- unassign lpn - do not look for NON splits
        union all
        select distinct t1.order_id
        from tmp_order_splits_scratch t1
        where p_is_cs_or_cm = 0
            and
            (
                t1.order_split_id is not null
                or not exists
                (
                    select 1
                    from tmp_unplan_splits t2
                    where t2.order_id = t1.order_id
                )
            )
            and not exists
            (
                select 1
                from order_split os 
                where os.order_id = t1.order_id and os.is_cancelled = 0
                    and os.shipment_id is null
            )
    ) iv
	join orders o on o.order_id = iv.order_id;
    
    wm_cs_log('Created unplanned ' || case p_is_cs_or_cm when 1 then 'NON' else ' ' end || ' splits ' || sql%rowcount);

    -- create a planned split for previously unsplit orders for lpns that are 
    -- retained on the shpmt; dont do this if the order is being unplanned; this
    -- occurs in both cs and unassign flows
    insert into order_split
    (
        shipment_id, created_dttm, created_source, last_updated_dttm, last_updated_source,
        order_split_status, order_split_id, tc_company_id, order_id,
        order_split_seq, created_source_type, last_updated_source_type,
        assigned_service_level_id, assigned_mot_id, assigned_carrier_id, 
        line_haul_ship_via, distribution_ship_via, original_assigned_ship_via
    )
    select t1.shipment_id, sysdate, p_user_id, sysdate, p_user_id, 10,
        seq_order_split_id.nextval order_split_id, o.tc_company_id, t1.order_id,
        2 order_split_seq, 5, 5, o.assigned_service_level_id, o.assigned_mot_id,
        o.assigned_carrier_id, o.line_haul_ship_via, o.distribution_ship_via,
        decode(p_is_cs_or_cm, 0, 'NON', null)
    from tmp_order_splits_scratch t1
    join orders o on o.order_id = t1.order_id
    where t1.order_split_id is null
        and not exists
        (
            select 1
            from tmp_unplan_splits t2
            where t2.order_id = t1.order_id
        );
    wm_cs_log('Created planned splits for previously unsplit orders ' || sql%rowcount);

if (p_act_trk_tran_nbr is not null)
    then
        insert into prod_trkg_tran
        (
            prod_trkg_tran_id, tran_type, tran_code, tran_nbr, seq_nbr, whse,
            menu_optn_name, user_id, cd_master_id, ref_code_id_1, ref_code_id_2, 
            ref_code_id_3, ref_field_1, ref_field_2, ref_field_3, mod_date_time,
            create_date_time, module_name, begin_date, end_date
        ) 
        select prod_trkg_tran_id_seq.nextval, '800', '013', p_act_trk_tran_nbr,
            p_act_trk_rec_count + rownum seq_nbr, f.whse, 'CS', p_user_id, 
            l.tc_company_id, v_shipment_ref_code, v_order_ref_code, v_lpn_ref_code,
            l.tc_shipment_id ref_field_1, l.tc_order_id ref_field_2, 
            l.tc_lpn_id ref_field_3, sysdate, sysdate, 'Shipping', l.last_updated_dttm, sysdate
        from tmp_variance_lpns t
        join lpn l on l.lpn_id = t.lpn_id
        join facility f on f.facility_id = l.c_facility_id;
        p_act_trk_rec_count := p_act_trk_rec_count + sql%rowcount;
    end if;

    -- we cant determine that the ship_via was only assigned during routing and 
    -- not at any other time, so we nullify it anyway
    -- assign removed lpns to unplanned splits; if removal of an unsplit order
    -- leads to the entire order being removed, we still need to nullify shpmt
    -- data on the lpn - hence the left join
    -- in CM or unassign flows, if multiple splits for the same order exist, the
    -- variance on all of them is consolidated under a single unplanned split
    merge into lpn l
    using
    (
        select t.order_id, min(os.order_split_id) order_split_id
        from tmp_order_splits_scratch t
        left join order_split os on os.order_id = t.order_id
            and os.is_cancelled = 0 and os.original_assigned_ship_via = 'NON'
        left join shipment s on s.shipment_id = os.shipment_id
        where p_is_cs_or_cm = 1
            and (os.shipment_id is null or s.shipment_closed_indicator = 0)
        group by t.order_id
        union all
        select t.order_id, min(os.order_split_id) order_split_id
        from tmp_order_splits_scratch t
        left join order_split os on os.order_id = t.order_id
            and os.is_cancelled = 0 and os.shipment_id is null
        where p_is_cs_or_cm = 0
        group by t.order_id
    ) iv on (iv.order_id = l.order_id)
    when matched then
    update set l.tc_shipment_id = null, l.shipment_id = null, l.plan_load_id = null,
        l.ship_via = decode(p_is_cm, 1, l.ship_via, null),
        l.tracking_nbr = decode(p_is_cm, 1, l.tracking_nbr, null), 
        l.alt_tracking_nbr = decode(p_is_cm, 1, l.alt_tracking_nbr, null),
        l.last_updated_dttm = sysdate, l.last_updated_source = p_user_id,
        l.last_updated_source_type = 1, l.init_ship_via = null,
        l.distribution_leg_carrier_id = null, l.distribution_leg_mode_id = null,
        l.distribution_lev_svce_level_id = null, l.order_split_id = iv.order_split_id
    where exists
    (
        select 1
        from tmp_variance_lpns t
        where t.lpn_id = l.lpn_id
    );
    wm_cs_log('Assigned variance lpns to unplanned splits ' || sql%rowcount);

    -- if order gets split now, assign retained lpns of unsplit orders to newly
    -- created planned splits
    merge into lpn l
    using
    (
        -- get all that have a combination of retained and removed qty
        select t1.order_id, planned_os.order_split_id 
        from tmp_order_splits_scratch t1
        join order_split planned_os on planned_os.order_id = t1.order_id
            and planned_os.shipment_id = t1.shipment_id 
            and planned_os.is_cancelled = 0
        where t1.order_split_id is null
            and not exists
            (
                select 1
                from tmp_unplan_splits t2
                where t2.order_id = t1.order_id
            )
    ) iv on (iv.order_id = l.order_id)
    when matched then
    update set l.last_updated_dttm = sysdate, l.last_updated_source = p_user_id,
        l.order_split_id = iv.order_split_id
    where l.lpn_facility_status < 90
        and not exists
        (
            select 1
            from tmp_variance_lpns t
            where t.lpn_id = l.lpn_id
        );
    wm_cs_log('Assigned loaded lpns to planned splits for previously unsplit orders ' || sql%rowcount);

    -- lpns should have the correct split on them at this point
-- todo: wt/vol needs to be prorated via oli/osli qty
    merge into order_split_line_item osli
    using
    (
        -- 1. removed qty on lines going to unplanned split; non-cubed qty goes
        -- to unplanned split for close shpmt, stays for unassign flows
        -- need to group by because there could have been multiple planned
        -- splits to begin with
        -- when multiple unplanned splits exist, lpns were assigned above to the 
        -- one with the least os_id, so we need to create osli qty similarly
        -- the assumption here is that, in the meantime, a newer os_id isnt 
        -- introduced elsewhere that has a lower id than this minimum (unlikely)
        select unplanned_os.order_split_id, t.order_id, t.line_item_id,
            decode(p_is_cs_or_cm, 1, sum(t.oli_split_vol - t.retained_lpn_vol), sum(t.removed_lpn_vol)) vol,
            decode(p_is_cs_or_cm, 1, sum(t.oli_split_wt - t.retained_lpn_wt), sum(t.removed_lpn_wt)) wt,
            decode(p_is_cs_or_cm, 1, sum(t.order_qty - t.retained_lpn_qty), sum(t.removed_lpn_qty)) order_qty,
            decode(p_is_cs_or_cm, 1, sum(t.allocated_qty - t.retained_lpn_qty), sum(t.removed_lpn_qty)) allocated_qty,
            sum(t.removed_units_pakd) units_pakd,
            min(oli.weight_uom_id_base) weight_uom_id_base, 
            min(oli.volume_uom_id_base) volume_uom_id_base,
            min(oli.qty_uom_id_base) qty_uom_id_base, min(oli.wave_nbr) wave_nbr
        from tmp_oli_splits t
        join
        (
            select t2.order_id, min(os.order_split_id) order_split_id
            from tmp_order_splits_scratch t2
            join order_split os on os.order_id = t2.order_id
                and os.is_cancelled = 0 and os.original_assigned_ship_via = 'NON'
            left join shipment s on s.shipment_id = os.shipment_id
            where p_is_cs_or_cm = 1
                and (os.shipment_id is null or s.shipment_closed_indicator = 0)
            group by t2.order_id
            union all
            select t2.order_id, min(os.order_split_id) order_split_id
            from tmp_order_splits_scratch t2
            join order_split os on os.order_id = t2.order_id
                and os.is_cancelled = 0 and os.shipment_id is null
            where p_is_cs_or_cm = 0
            group by t2.order_id
        ) unplanned_os on unplanned_os.order_id = t.order_id
        join order_line_item oli on oli.line_item_id = t.line_item_id
        where decode(p_is_cs_or_cm, 1, t.order_qty - t.retained_lpn_qty, t.removed_lpn_qty) > 0
        group by unplanned_os.order_split_id, t.order_id, t.line_item_id
        union all
        -- 2. retained qty on touched lines going to planned splits for a 
        -- previously unsplit order
        -- 3. qty on other untouched lines on the same order with no variance;
        -- create osli for them as well
        select planned_os.order_split_id, t.order_id, t.line_item_id,
            decode(p_is_cs_or_cm, 1, t.retained_lpn_vol, t.oli_split_vol - t.removed_lpn_vol) vol, 
            decode(p_is_cs_or_cm, 1, t.retained_lpn_wt, t.oli_split_wt - t.removed_lpn_wt) wt,
            decode(p_is_cs_or_cm, 1, t.retained_lpn_qty, t.order_qty - t.removed_lpn_qty) order_qty, 
            decode(p_is_cs_or_cm, 1, t.retained_lpn_qty, t.allocated_qty - t.removed_lpn_qty) allocated_qty,
            t.retained_lpn_qty units_pakd, oli.weight_uom_id_base, oli.volume_uom_id_base, 
            oli.qty_uom_id_base, oli.wave_nbr
        from tmp_oli_splits t
        join tmp_order_splits_scratch t2 on t2.order_id = t.order_id
            and t2.order_split_id is null
        join order_split planned_os on planned_os.order_id = t2.order_id 
            and planned_os.shipment_id = t2.shipment_id
            and planned_os.is_cancelled = 0
        join order_line_item oli on oli.line_item_id = t.line_item_id
        where decode(p_is_cs_or_cm, 1, t.retained_lpn_qty, t.order_qty - t.removed_lpn_qty) > 0
    ) iv on (iv.order_id = osli.order_id and iv.line_item_id = osli.line_item_id
        and iv.order_split_id = osli.order_split_id)
    when matched then
    update set osli.planned_weight = coalesce(osli.planned_weight, 0) + iv.wt, 
        osli.planned_volume = coalesce(osli.planned_volume, 0) + iv.vol, 
        osli.order_qty = osli.order_qty + iv.order_qty, 
        osli.allocated_qty = osli.allocated_qty + iv.allocated_qty,
        osli.units_packed = coalesce(osli.units_packed, 0) + iv.units_pakd, 
        osli.last_updated_dttm = sysdate, osli.last_updated_source = p_user_id
    when not matched then
    insert
    (
        order_split_line_item_id, order_split_id, order_id, line_item_id,
        weight_uom_id_base, volume_uom_id_base, qty_uom_id_base, planned_weight,
        planned_volume, order_qty, allocated_qty, units_packed, wave_nbr,
        created_dttm, last_updated_dttm, created_source, last_updated_source
    )
    values
    (
        seq_order_split_line_item_id.nextval, iv.order_split_id, iv.order_id,
        iv.line_item_id, iv.weight_uom_id_base, iv.volume_uom_id_base, 
        iv.qty_uom_id_base, iv.wt, iv.vol, iv.order_qty, iv.allocated_qty, 
        iv.units_pakd, iv.wave_nbr, sysdate, sysdate, p_user_id, p_user_id
    );
    wm_cs_log('OSLI merge ' || sql%rowcount);

    -- for previously unsplit orders that are split now
    merge into stop_action_order sao
    using
    (
        select t.order_id, planned_os.shipment_id, planned_os.order_split_id
        from tmp_order_splits_scratch t
        join order_split planned_os on planned_os.order_id = t.order_id
            and planned_os.shipment_id = t.shipment_id
            and planned_os.is_cancelled = 0
        where t.order_split_id is null
    ) iv on (iv.order_id = sao.order_id and sao.shipment_id = iv.shipment_id)
    when matched then
    update set sao.order_split_id = iv.order_split_id;
    wm_cs_log('SAO merge ' || sql%rowcount);

    merge into order_movement om
    using
    (
        select t.order_id, planned_os.shipment_id, planned_os.order_split_id
        from tmp_order_splits_scratch t
        join order_split planned_os on planned_os.order_id = t.order_id
            and planned_os.shipment_id = t.shipment_id 
            and planned_os.is_cancelled = 0
        where t.order_split_id is null
    ) iv on (iv.order_id = om.order_id and om.shipment_id = iv.shipment_id)
    when matched then
    update set om.order_split_id = iv.order_split_id;
    wm_cs_log('Order_movement merge ' || sql%rowcount);

-- todo: in base, delete os/osli for orders having only one unplanned split remaining; has_split should reset below normally

    -- nullify fields; all orders should either be split or completely removed
    -- from shpmt, so doing this for all of them is ok
-- todo: swap two fields for 2012; override billing
    merge into orders o
    using
    (
        select t.order_id, coalesce(min(os.order_split_status), 5) order_split_status,
            case when min(os.order_split_status) is not null then 1 else 0 end has_split
        from tmp_order_splits_scratch t
        left join order_split os on os.order_id = t.order_id and os.is_cancelled = 0
        group by t.order_id
    ) iv on (iv.order_id = o.order_id)
    when matched then
    update set o.actual_cost = null, o.actual_cost_currency_code = null,
        o.assigned_carrier_id = null, o.assigned_mot_id = null,
        o.assigned_service_level_id = null, o.assigned_equipment_id = null,
        o.baseline_cost = null, o.baseline_cost_currency_code = null,
        o.distribution_ship_via = null, o.dynamic_routing_reqd = null,
        o.last_updated_dttm = sysdate, o.last_updated_source = p_user_id,
        o.last_updated_source_type = 1, o.line_haul_ship_via = null,
        o.shipment_id = null, o.tc_shipment_id = null,
        o.zone_skip_hub_location_id = null, o.order_loading_seq = null,
        o.path_id = null, o.path_set_id = null, o.plan_d_facility_alias_id = (case when o.path_set_id is not null then null else o.plan_d_facility_alias_id end),
        o.plan_d_facility_id = (case when o.path_set_id is not null then null else o.plan_d_facility_id end), o.has_split = iv.has_split,
        o.order_status = iv.order_split_status
    where o.order_status != iv.order_split_status or o.shipment_id is not null
        or o.has_split != iv.has_split;
    wm_cs_log('Orders fully unassigned or split ' || sql%rowcount);
end;
/

show errors;

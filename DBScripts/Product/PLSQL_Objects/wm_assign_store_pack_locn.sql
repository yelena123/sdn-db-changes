create or replace procedure wm_assign_store_pack_locn
(
    p_user_id        in  ucl_user.user_name%type,
    p_whse           in  facility.whse%type,
    p_facility_id    in  facility.facility_id%type,
    p_lang_id        in  message_display.to_lang%type,
    p_pick_wave_nbr  in  wave_parm.wave_nbr%type
)
as
    v_force_wpt                   wave_parm.force_wpt%type;
    v_wp_wave_proc_type           wave_parm.wave_proc_type%type;
    v_wp_proc_bulk_wave           wave_proc_type.proc_bulk_wave%type;
    v_pick_locn_assign_type       wave_parm.pick_locn_assign_type%TYPE;
    v_force_pick_locn_assign_type wave_parm.force_pick_locn_assign_type%TYPE;
    v_error_msg                   msg_log.msg%type;
    v_dummy_rc                    number(1);
    v_dummy_debug_lvl             number(1) := 0;
    v_tc_company_id               wave_parm.tc_company_id%type := 0;
    v_max_log_lvl                 number(1) := 0;
    v_lang_id                     language.language_suffix%type;
    v_is_pix_configured           number(1) := 0;
    v_prev_module_name            wm_utils.t_app_context_data;
    v_prev_action_name            wm_utils.t_app_context_data;
begin
     select coalesce(wp.force_wpt, 'N'), nullif(wpt.wave_proc_type, 0),
        coalesce(wpt.proc_bulk_wave, 'N'), coalesce(wp.tc_company_id, wm_utils.wm_get_user_bu(p_user_id)),
        wp.pick_locn_assign_type, wp.force_pick_locn_assign_type
    into v_force_wpt, v_wp_wave_proc_type, v_wp_proc_bulk_wave, v_tc_company_id,
        v_pick_locn_assign_type, v_force_pick_locn_assign_type
    from ship_wave_parm swp
    join wave_parm wp on wp.wave_parm_id = swp.wave_parm_id
    left join wave_proc_type wpt on wpt.wave_proc_type = wp.wave_proc_type
        and wpt.whse = wp.whse
     where wp.wave_nbr = p_pick_wave_nbr and wp.whse = p_whse;

    select la.language_suffix
    into v_lang_id
    from ucl_user uu
    join locale l on l.locale_id = uu.locale_id
    join language la on la.language_id = l.language_id
    where uu.user_name = p_user_id;

    if (v_force_wpt = 'Y')
    then
        if(v_wp_wave_proc_type is null)
        then
            raise_application_error(-20050, 'Cannot force an invalid WPT');
        end if;

        if (v_wp_proc_bulk_wave = 'N')
        then
            -- the forced wpt is not configured for bulk processing
            return;
        end if;
    end if;

    select to_number(coalesce(trim(substr(sc.misc_flags, 8, 1)), '0'))
    into v_max_log_lvl
    from sys_code sc
    where sc.rec_type = 'S' and sc.code_type = '520' and sc.code_id = 'SPA';

    delete from tmp_log_parm;
    insert into tmp_log_parm
    (
        log_lvl, pgm_id, module, msg_id, ref_code_1, ref_value_1, user_id, whse,
        cd_master_id
    )
    values
    (
        v_max_log_lvl, 'wm_assign_store_pack_locn', 'WAVE', '1094', 'SPA',
        p_pick_wave_nbr, p_user_id, p_whse, v_tc_company_id
    );
    
    wm_utils.get_context_info(v_prev_module_name, v_prev_action_name);
    wm_utils.set_context_info(p_module_name => 'WAVE', p_action_name => 'SPLA',
        p_client_id => p_pick_wave_nbr);
		
    wm_cs_log('Beginning SPLA', p_sql_log_level => 0);

    v_error_msg := get_msg_info('WAVE', '1094', p_lang_id);
    insert all
    when (wave_proc_type is null)
    then
        into msg_log
        (
            msg_log_id, module, msg_id, pgm_id, ref_value_1, msg,
            create_date_time, mod_date_time, user_id, whse, log_date_time
        )
         values
        (
            msg_log_id_seq.nextval, 'WAVE', '1094', 'wm_assign_store_pack_locn',
            p_pick_wave_nbr, replace(v_error_msg, '{0}', to_char(wave_proc_type)),
            sysdate, sysdate, p_user_id, p_whse, sysdate
        )
    when (wave_proc_type is not null)
    then
        into tmp_spla_selected_orders
        (
            order_id, line_item_id, d_facility_alias_id, wave_proc_type,
            merch_type, merch_group, d_facility_id, store_dept,
            pick_locn_assign_type, attr_group_num
        )
        values
        (
            order_id, line_item_id, d_facility_alias_id, wave_proc_type,
            merch_type, merch_group, d_facility_id, store_dept,
            pick_locn_assign_type, attr_group_num
        )
    when (wave_proc_type is not null and rn = 1)
    then
        into tmp_spla_attr_groups
        (
            pick_locn_assign_type, d_facility_id, d_facility_alias_id,
            merch_type, merch_group, store_dept, attr_group_num
        )
        values
        (
            pick_locn_assign_type, d_facility_id, d_facility_alias_id,
            merch_type, merch_group, store_dept, attr_group_num
        )
    select iv.order_id, iv.line_item_id, iv.d_facility_alias_id, iv.merch_type, iv.merch_group,
        iv.store_dept, iv.pick_locn_assign_type, iv.d_facility_id, wpt.wave_proc_type,
        dense_rank() over(order by iv.pick_locn_assign_type, iv.d_facility_id,
            iv.d_facility_alias_id, iv.merch_type, iv.merch_group, iv.store_dept) attr_group_num,
        row_number() over(partition by iv.pick_locn_assign_type, iv.d_facility_id,
            iv.d_facility_alias_id, iv.merch_type, iv.merch_group, iv.store_dept 
            order by decode(wpt.wave_proc_type, null, 1, 0), iv.line_item_id) rn
    from
    (
        select oli.order_id, oli.line_item_id, o.d_facility_alias_id,
            case when v_force_wpt = 'Y' then v_wp_wave_proc_type
                else coalesce(nullif(oli.wave_proc_type, 0),
                nullif(ifmw.dflt_wave_proc_type, 0), v_wp_wave_proc_type) end wpt,
            coalesce(oli.merch_type, iw.merch_type) merch_type,
            coalesce(oli.merch_grp, iw.merch_group) merch_group,
            coalesce(oli.store_dept, iw.store_dept) store_dept,
            case when v_force_pick_locn_assign_type in ('Y', 'y')
                and v_pick_locn_assign_type is not null then v_pick_locn_assign_type
                else coalesce(oli.pick_locn_assign_type, ifmw.pick_locn_assign_type,
                    v_pick_locn_assign_type)
                end pick_locn_assign_type,
            o.d_facility_id
        from orders o
        join order_line_item oli on oli.order_id = o.order_id
        join item_wms iw on iw.item_id = oli.item_id
        join item_facility_mapping_wms ifmw on ifmw.item_id = iw.item_id
            and ifmw.facility_id = o.o_facility_id
        where o.o_facility_id = p_facility_id and oli.wave_nbr = p_pick_wave_nbr
            and oli.do_dtl_status in (120, 130)
    ) iv
    left join wave_proc_type wpt on wpt.wave_proc_type = iv.wpt
        and wpt.proc_bulk_wave = 'Y' and wpt.whse = p_whse;
    wm_cs_log('Collected data for SPLA ' || sql%rowcount);

    -- assign existing locns if available
    merge into tmp_spla_attr_groups t1
    using
    (
        select t.attr_group_num, min(splh.locn_id)
            keep(dense_rank first order by (lh.locn_pick_seq)) locn_id
        from tmp_spla_attr_groups t
        join store_pack_locn_hdr splh on splh.store_nbr = t.d_facility_alias_id
            -- and splh.d_facility_id = t.d_facility_id
        join locn_hdr lh on lh.locn_id = splh.locn_id and lh.whse = p_whse
        join store_pack_locn_dtl spld on spld.locn_id = splh.locn_id
        where (coalesce(spld.merch_type, ' ') = coalesce(t.merch_type, ' ')
            or spld.merch_type = '*' or t.merch_type = '*')
            and (coalesce(spld.merch_group, ' ') = coalesce(t.merch_group, ' ')
                or spld.merch_group = '*' or t.merch_group = '*')
            and (coalesce(spld.store_dept, ' ') = coalesce(t.store_dept, ' ')
                or spld.store_dept = '*' or t.store_dept = '*')
        group by t.attr_group_num
    ) iv on (iv.attr_group_num = t1.attr_group_num and iv.locn_id is not null)
    when matched then
    update set t1.locn_id = iv.locn_id;
    wm_cs_log('Existing locns found ' || sql%rowcount);

    -- new massaging code to handle data post existing location match
    merge into tmp_spla_selected_orders t
    using
    (
        select iv1.order_id, iv1.line_item_id, iv1.merch_type, iv1.merch_group, iv1.store_dept, 
            first_value(iv1.old_attr_group_num) over (partition by iv1.new_attr_grp_num) attr_group_num
        from 
        (
            select t1.attr_group_num old_attr_group_num, t1.pick_locn_assign_type, 
                t1.d_facility_id, t1.d_facility_alias_id, t1.order_id, t1.line_item_id,
                decode(f.assign_merch_type, '2', t1.merch_type, '*') merch_type, 
                decode(f.assign_merch_group, '2', t1.merch_group, '*') merch_group, 
                decode(f.assign_store_dept, '2', t1.store_dept, '*') store_dept,
                dense_rank() over(order by t1.pick_locn_assign_type, t1.d_facility_id,
                    t1.d_facility_alias_id, decode(f.assign_merch_type, '2', t1.merch_type, '*'), 
                    decode(f.assign_merch_group, '2', t1.merch_group, '*'), 
                    decode(f.assign_store_dept, '2', t1.store_dept, '*')) new_attr_grp_num
            from tmp_spla_selected_orders t1
            join facility f on f.facility_id = t1.d_facility_id
            where exists
            (
                select 1
                from tmp_spla_attr_groups t2
                where t2.attr_group_num = t1.attr_group_num
                    and t2.locn_id is null
            )
        ) iv1
    )iv on (iv.order_id = t.order_id and iv.line_item_id = t.line_item_id)
    when matched then
    update set
        t.attr_group_num = iv.attr_group_num, t.merch_type = iv.merch_type, 
        t.merch_group = iv.merch_group, t.store_dept = iv.store_dept;
    wm_cs_log('New groups based on facility merch attr enforcement ' || sql%rowcount);

    -- delete the attr group numbers that have been merged based on the assign_* flag
    delete from tmp_spla_attr_groups t
    where not exists
    (
        select 1
        from tmp_spla_selected_orders t1
        where t1.attr_group_num = t.attr_group_num
    );
    wm_cs_log('older groupings cleaned up ' || sql%rowcount);
    
    merge into tmp_spla_attr_groups t2
    using
    (
        select distinct t.attr_group_num, t.d_facility_id, t.d_facility_alias_id, t.pick_locn_assign_type,
            t.merch_type, t.merch_group, t.store_dept
        from tmp_spla_selected_orders t
        where exists
        (
            select 1
            from tmp_spla_attr_groups t1
            where t1.attr_group_num = t.attr_group_num
        )
    )iv on ( iv.attr_group_num = t2.attr_group_num)
    when matched then
    update set
        t2.merch_type = iv.merch_type, 
        t2.merch_group = iv.merch_group, t2.store_dept = iv.store_dept;
    wm_cs_log('existing groupings updated ' || sql%rowcount);

    --insert new attr_group numbers
    insert into tmp_spla_attr_groups
    (
        attr_group_num, d_facility_id, d_facility_alias_id, pick_locn_assign_type,
        merch_type, merch_group, store_dept
    )
    select distinct t.attr_group_num, t.d_facility_id, t.d_facility_alias_id, t.pick_locn_assign_type,
        t.merch_type, t.merch_group, t.store_dept
    from tmp_spla_selected_orders t
    where not exists
    (
        select 1
        from tmp_spla_attr_groups t1
        where t1.attr_group_num = t.attr_group_num
    );
    wm_cs_log('New groupings added ' || sql%rowcount);

    -- find as many unassigned locations as required by each distinct plat
    for plat_rec in
    (
         select t.pick_locn_assign_type, count(*) num_locns_per_plat
        from tmp_spla_attr_groups t
        where t.locn_id is null and t.pick_locn_assign_type is not null
        group by t.pick_locn_assign_type
    )
    loop
        merge into tmp_spla_attr_groups t
        using
        (
            with free_locns as
            (
                select iv.locn_id, iv.rank
                from
                (
                    select splh.locn_id, row_number()
                        over(order by plat.prty, lh.locn_pick_seq) rank
                    from pick_locn_assign_prty plat
                    join store_pack_locn_hdr splh on splh.store_nbr is null
                        and splh.assign_zone = plat.pick_locn_assign_zone
                    join locn_hdr lh on lh.locn_id = splh.locn_id
                    where plat.pick_locn_assign_type = plat_rec.pick_locn_assign_type
                        and plat.whse = p_whse
                        and not exists
                        (
                            select 1
                            from tmp_spla_attr_groups t
                            where t.locn_id = splh.locn_id
                        )
                ) iv
                where iv.rank <= plat_rec.num_locns_per_plat
            ),
            unassigned_attr_groups as
            (
                select t.attr_group_num,
                    row_number() over(order by t.attr_group_num) rank
                from tmp_spla_attr_groups t
                where t.pick_locn_assign_type = plat_rec.pick_locn_assign_type
                    and t.locn_id is null
            )
            select uag.attr_group_num, fl.locn_id
            from unassigned_attr_groups uag
            join free_locns fl on fl.rank = uag.rank
        ) iv on (iv.attr_group_num = t.attr_group_num)
        when matched then
        update set t.locn_id = iv.locn_id;

        if (v_max_log_lvl >= 2)
        then
            wm_cs_log('Updated locn_id on ' || sql%rowcount || ' attr groups for PLAT '
                || plat_rec.pick_locn_assign_type, p_sql_log_level => 2);
        end if;
    end loop;

    -- update splh with stores that appropriated those locns
    merge into store_pack_locn_hdr splh
    using
    (
        select t.locn_id, t.d_facility_alias_id
        from tmp_spla_attr_groups t
    ) iv on (iv.locn_id = splh.locn_id)
    when matched then
    update set splh.store_nbr = iv.d_facility_alias_id, splh.user_id = p_user_id,
        splh.mod_date_time = sysdate
    where splh.store_nbr is null;
    wm_cs_log('SPLH updated ' || sql%rowcount);

    -- wildcard individual merch attrs here if configured appropriately
    insert into store_pack_locn_dtl
    (
        locn_id, seq_nbr, create_date_time, mod_date_time, user_id, store_dept, 
        store_pack_locn_hdr_id, merch_type, store_pack_locn_dtl_id, merch_group
    )
    select t.locn_id, store_pack_locn_dtl_id_seq.nextval, sysdate, sysdate, p_user_id, 
        t.store_dept, splh.store_pack_locn_hdr_id, t.merch_type, 
        store_pack_locn_dtl_id_seq.nextval, t.merch_group
    from tmp_spla_attr_groups t
    join store_pack_locn_hdr splh on splh.locn_id = t.locn_id
    where not exists
    (
        select 1
        from store_pack_locn_dtl spld
        where spld.store_pack_locn_hdr_id = splh.store_pack_locn_hdr_id
    );
    wm_cs_log('Created SPLD ' || sql%rowcount);

    -- error out those with no locns
    v_error_msg := get_msg_info('WAVE', '1061', v_lang_id);
    insert into msg_log
    (
        msg_log_id, module, msg_id, pgm_id, ref_value_1, msg,
        create_date_time, mod_date_time, user_id, whse, log_date_time
    )
    select msg_log_id_seq.nextval, 'WAVE', '1061', 'wm_assign_store_pack_locn', p_pick_wave_nbr ref_value_1, replace(v_error_msg, '%DISTRO', to_char(t2.order_id)),
           sysdate, sysdate, p_user_id, p_whse, sysdate
    from tmp_spla_attr_groups t1
    join tmp_spla_selected_orders t2 on t2.attr_group_num = t1.attr_group_num
    where t1.locn_id is null;
    wm_cs_log('Unassigned lines logged ' || sql%rowcount, p_sql_log_level => 0);

    -- the do status informational pix can also be turned off in S501
    select case when exists
    (
        select 1
        from sys_code sc
        where sc.rec_type = 'S' and sc.code_type = '501'
            and sc.code_id >= '120' and substr(sc.misc_flags, 1, 1) = '1'
    ) then 1 else 0 end
    into v_is_pix_configured
    from dual;

    if (v_is_pix_configured = 1)
    then
        -- we're forced to cache the status because configurable do status
        -- pix drives off of orders after it is updated with the new status
        insert into tmp_wave_old_order_status
        (
            order_id, do_status
        )
        select o.order_id, o.do_status
        from orders o
        where o.o_facility_id = p_facility_id and o.is_original_order = 1
            and exists
            (
                select 1
                from tmp_spla_selected_orders t
                where t.order_id = o.order_id
                and t.locn_id is null
            );
        wm_cs_log('Old order status cached ' || sql%rowcount, p_sql_log_level => 2);
    end if;

    -- deselect unassigned orders
    for oli_rec in
    (
         select t2.order_id, t2.line_item_id, o.is_original_order
        from tmp_spla_attr_groups t1
        join tmp_spla_selected_orders t2 on t2.attr_group_num = t1.attr_group_num
        join orders o on o.order_id = t2.order_id
        where t1.locn_id is null
    )
    loop
        -- this is a rare occurrence so the logic is left loop-based
         manh_deselect_do(v_dummy_rc, p_user_id, p_facility_id, v_tc_company_id,
            oli_rec.order_id, oli_rec.line_item_id, oli_rec.is_original_order,
            p_pick_wave_nbr, v_dummy_debug_lvl);
    end loop;
    wm_cs_log('Deselections complete');

    -- update originals back to the status before Selection
    update orders o
    set (o.last_updated_source, o.last_updated_dttm, o.do_status) =
    (
        select p_user_id last_updated_source, sysdate last_updated_dttm,
            case
                when min(oli.do_dtl_status) in (110, 112)
                    and min(oli.do_dtl_status) != max(oli.do_dtl_status)
                    then 120
                when min(oli.do_dtl_status) = 130
                    and max(oli.do_dtl_status) > 130 then 140
                else coalesce(min(oli.do_dtl_status), 200)
            end do_status
        from order_line_item oli
        where oli.order_id = o.order_id and oli.do_dtl_status < 200
    )
    where o.o_facility_id = p_facility_id
        and exists
        (
            select 1
            from tmp_spla_selected_orders t
            where t.order_id = o.order_id
            and t.locn_id is null
        );
    wm_cs_log('Orig orders updated ' || sql%rowcount);

    -- update agg status based on the original order status set above
    merge into orders agg
    using
    (
        select orig.parent_order_id, min(orig.do_status) do_status
        from orders orig
        where orig.o_facility_id = p_facility_id
            and orig.parent_order_id is not null
            and orig.is_original_order = 1
            and exists
            (
                select 1
                from tmp_spla_selected_orders t
                where t.order_id = orig.order_id and t.locn_id is null
            )
        group by parent_order_id
    ) iv on (iv.parent_order_id = agg.order_id)
    when matched then
    update set agg.do_status = iv.do_status, agg.last_updated_source = p_user_id,
        agg.last_updated_dttm = sysdate
    where agg.do_status != iv.do_status;
    wm_cs_log('Agg orders updated ' || sql%rowcount);

    -- determine if packed orders can be moved to beyond in-packed, based on lpn
    merge into orders o
    using
    (
        select l.order_id, min(l.lpn_facility_status) min_lpn_status,
            max
            (
                case
                when l.lpn_facility_status < 50
                    and
                    (
                        coalesce(l.stage_indicator, 0) != 10
                        or coalesce(lh.locn_class, ' ') not in ('P', 'S', 'O')
                    ) then 1
                else 0
                end
            ) cannot_stage
        from lpn l
        left join locn_hdr lh on lh.locn_id = l.curr_sub_locn_id
        where exists
        (
            select 1
            from orders o
            where o.order_id = l.order_id and o.o_facility_id = p_facility_id
                and o.do_status = 150
                and exists
                (
                    select 1
                    from tmp_spla_selected_orders t
                    where t.parent_order_id = o.order_id
                    and t.locn_id is null
                )
        )
        group by l.order_id
    ) iv on (iv.order_id = o.order_id)
    when matched then
    update set o.do_status =
    (
        case
        when iv.min_lpn_status in (20, 30) and iv.cannot_stage = 0 then 165
        else
            case iv.min_lpn_status
                when 20 then 150 when 30 then 160 when 50 then 180 else 170
            end
        end
    );
    wm_cs_log('Orig orders status revised ' || sql%rowcount);

    -- update orig orders based on the status set above.
    merge into orders orig
    using
    (
        select order_id, do_status
        from orders agg
        where agg.o_facility_id = p_facility_id
            and agg.do_status between 150 and 180
            and agg.parent_order_id is null
            and agg.is_original_order = 0
            and exists
            (
                select 1
                from tmp_spla_selected_orders t
                where t.parent_order_id = agg.order_id
                and t.locn_id is null
            )
    ) iv on (iv.order_id = orig.parent_order_id)
    when matched then
    update set orig.do_status = iv.do_status
    where orig.do_status between 150 and 180 and orig.do_status != iv.do_status;
    wm_cs_log('Agg orders status revised ' || sql%rowcount);

    if (v_is_pix_configured = 1)
    then
        manh_wave_do_status_change_pix(p_facility_id, p_user_id, p_whse,
            p_pick_wave_nbr);
    end if;

    merge into order_line_item oli
    using
    (
        select t2.order_id, t2.line_item_id, t1.locn_id
        from tmp_spla_attr_groups t1
        join tmp_spla_selected_orders t2 on t2.attr_group_num = t1.attr_group_num
    ) iv on (iv.order_id = oli.order_id and iv.line_item_id = oli.line_item_id)
    when matched then
    update set oli.reason_code = decode(iv.locn_id, null, '9', oli.reason_code),
        oli.pick_locn_id = iv.locn_id, oli.last_updated_dttm = sysdate,
        oli.last_updated_source = p_user_id;
    wm_cs_log('OLI updated with store pack locns ' || sql%rowcount);

    commit;
    wm_cs_log('Completed SPLA', p_sql_log_level => 0);
	
    wm_utils.set_context_info(p_module_name => v_prev_module_name,
        p_action_name => v_prev_action_name);
end;
/
show errors;
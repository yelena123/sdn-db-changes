create or replace procedure wm_cleanup_unused_sp_locns
(
    p_whse                in facility.whse%type,
    p_user_id             in user_profile.user_id%type
)
as
    type t_splh_id is table of store_pack_locn_hdr.store_pack_locn_hdr_id%type
        index by binary_integer;
    va_splh_id            t_splh_id;
begin
    select splh.store_pack_locn_hdr_id
    bulk collect into va_splh_id
    from store_pack_locn_hdr splh
    join locn_hdr lh on lh.locn_id = splh.locn_id and lh.whse = p_whse
    where splh.store_nbr is not null and splh.assign_zone is not null
        and not exists
        (
            select 1
            from lpn l
            where l.curr_sub_locn_id = splh.locn_id and l.lpn_facility_status < 90
        )
        and not exists
        (
            select 1
            from alloc_invn_dtl aid
            where aid.dest_locn_id = splh.locn_id and aid.stat_code < 90
        )
        and not exists 
        (
            select 1
            from task_dtl td
            where td.dest_locn_id = splh.locn_id and td.stat_code < 90
        )
        and not exists 
        ( 
            select 1 
            from pkt_dtl pd
            where pd.pick_locn_id = splh.locn_id and pd.rtl_to_be_distroed_qty > 0.0
                and exists
                (
                    select 1
                    from pkt_hdr ph
                    where ph.pkt_hdr_id = pd.pkt_hdr_id and ph.major_minor_pkt = 'P'
                )
        );

    forall i in 1..va_splh_id.count
    update store_pack_locn_hdr splh
    set splh.store_nbr = null, splh.mod_date_time = sysdate,
        splh.user_id = p_user_id
    where splh.store_pack_locn_hdr_id = va_splh_id(i);

    forall i in 1..va_splh_id.count
    delete from store_pack_locn_dtl spld
    where spld.store_pack_locn_hdr_id = va_splh_id(i);
    
    commit;    
end;
/
show errors;

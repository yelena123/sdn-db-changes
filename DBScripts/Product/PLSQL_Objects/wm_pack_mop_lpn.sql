create or replace procedure wm_pack_mop_lpn
(
    p_cntr_nbr          in task_dtl.cntr_nbr%type,
    p_locn_id 			    in task_dtl.pull_locn_id%type,    
    p_pallet_nbr        in lpn.tc_lpn_id%type,
    p_facility_id       in facility.facility_id%type,
    p_tc_company_id     in wave_parm.tc_company_id%type,
    p_user_id           in user_profile.user_id%type,
    p_auto_weigh        in number default 0,
    p_raise_mhe_event   in number default 0,
    p_els_activity_code in char default null
)
as
    v_whse                  facility.whse%type;
    v_current_date          date := sysdate;
    v_act_trk_tran_nbr      prod_trkg_tran.tran_nbr%type;
    v_max_log_lvl           number(1) := 0;
    v_error_msg             msg_log.msg%type;
    v_mode_id               sys_code.code_id%type := 'PML';    
    v_ref_value_1           msg_log.ref_value_1%type 
        := case when p_locn_id is null then to_char(p_cntr_nbr) 
            else to_char(p_locn_id) end;
    v_prev_module_name      wm_utils.t_app_context_data;
    v_prev_action_name      wm_utils.t_app_context_data;
    v_event_enabled         interface_config.event_enabled%type;    
begin
    select f.whse
    into v_whse
    from facility f
    where f.facility_id = p_facility_id;

    select to_number(coalesce(trim(substr(sc.misc_flags, 8, 1)), '0'))
    into v_max_log_lvl
    from sys_code sc 
    where sc.rec_type = 'S' and sc.code_type = '520' and sc.code_id = v_mode_id;
    
    delete from tmp_log_parm;
    insert into tmp_log_parm
    (
        log_lvl, pgm_id, module, msg_id, ref_code_1, ref_value_1, user_id,
        whse, cd_master_id
    )
    values
    (
        v_max_log_lvl, 'wm_pack_mop_lpn', 'WAVE', '1094', v_mode_id,
        v_ref_value_1, p_user_id , v_whse, p_tc_company_id
    );
    
    select decode(wp.trk_prod_flag, 'Y', prod_trkg_tran_id_seq.nextval, null)
    into v_act_trk_tran_nbr
    from whse_parameters wp
    join facility f on f.facility_id = wp.whse_master_id
    where f.facility_id = p_facility_id;
    
    wm_utils.get_context_info(v_prev_module_name, v_prev_action_name);
    wm_utils.set_context_info(p_module_name => 'PACKING', p_action_name => 'MOP LPN PACKING',
        p_client_id => p_pallet_nbr);
		
    wm_cs_log('Start MOP lpn Packing');
    
    --collect all the allocations
    if (p_cntr_nbr is not null)
    then
        insert into tmp_pack_lpn_alloc_dtl
        (
            order_id, line_item_id, cd_master_id, cntr_nbr, carton_nbr, carton_seq_nbr, 
            pull_locn_id, item_id, invn_type, prod_stat, batch_nbr, sku_attr_1,   
            sku_attr_2, sku_attr_3, sku_attr_4, sku_attr_5, cntry_of_orgn,   
            invn_need_type, alloc_uom, qty_alloc, qty_pulld, stat_code, 
            task_id, task_genrtn_ref_nbr 
        )
        select o.order_id, iv.line_item_id, iv.cd_master_id, iv.cntr_nbr,  
            iv.carton_nbr, iv.carton_seq_nbr, iv.pull_locn_id, iv.item_id, iv.invn_type,   
            iv.prod_stat, iv.batch_nbr, iv.sku_attr_1, iv.sku_attr_2, iv.sku_attr_3,  
            iv.sku_attr_4, iv.sku_attr_5, iv.cntry_of_orgn, iv.invn_need_type, 
            iv.alloc_uom, iv.qty_alloc, iv.qty_pulld, iv.stat_code, 
            iv.task_id, iv.task_genrtn_ref_nbr
        from
        (
            select aid.tc_order_id, aid.line_item_id, aid.cd_master_id, aid.cntr_nbr, 
                aid.carton_nbr, aid.carton_seq_nbr, aid.pull_locn_id, aid.item_id,  
                aid.invn_type, aid.prod_stat, aid.batch_nbr, aid.sku_attr_1, 
                aid.sku_attr_2, aid.sku_attr_3, aid.sku_attr_4, aid.sku_attr_5, 
                aid.cntry_of_orgn, aid.invn_need_type, aid.alloc_uom, aid.qty_alloc, 
                aid.qty_pulld, aid.stat_code, null task_id, aid.task_genrtn_ref_nbr
            from alloc_invn_dtl aid
            where aid.cntr_nbr = p_cntr_nbr and aid.stat_code <90 
                and aid.cd_master_id = p_tc_company_id and aid.task_cmpl_ref_code = 6
                and aid.task_cmpl_ref_nbr = aid.carton_nbr and aid.whse = v_whse
                and not exists
                (
                    select 1
                    from alloc_invn_dtl aid2
                    where aid2.cntr_nbr != p_cntr_nbr
                       and aid2.stat_code < 90 and aid2.carton_nbr = aid.carton_nbr
                       and aid.whse = v_whse
                )
            union all
            select td.tc_order_id, td.line_item_id, td.cd_master_id, td.cntr_nbr, 
                td.carton_nbr, td.carton_seq_nbr, td.pull_locn_id, td.item_id, 
                td.invn_type, td.prod_stat, td.batch_nbr, td.sku_attr_1, 
                td.sku_attr_2, td.sku_attr_3, td.sku_attr_4, td.sku_attr_5, 
                td.cntry_of_orgn, td.invn_need_type, td.alloc_uom, td.qty_alloc, 
                td.qty_pulld, td.stat_code, td.task_id, td.task_genrtn_ref_nbr
            from task_dtl td
            where td.cntr_nbr = p_cntr_nbr and td.stat_code < 90 
                and td.cd_master_id = p_tc_company_id and td.task_cmpl_ref_code = 6
                and td.task_cmpl_ref_nbr = td.carton_nbr
                and not exists 
                (
                    select 1
                    from task_dtl td2
                    where td2.cntr_nbr != p_cntr_nbr
                        and td.stat_code < 90 and td2.carton_nbr = td.carton_nbr
                )
                and exists
                (
                     select 1
                     from task_hdr th
                     where td.task_id = th.task_id and th.task_hdr_id = td.task_hdr_id
                         and th.whse = v_whse
                )
                     
        ) iv
        join orders o on o.tc_order_id = iv.tc_order_id and o.tc_company_id = iv.cd_master_id
            and o.o_facility_id = p_facility_id;
    else
        insert into tmp_pack_lpn_alloc_dtl
        (
            order_id, line_item_id, cd_master_id, cntr_nbr, carton_nbr, carton_seq_nbr, 
            pull_locn_id, item_id, invn_type, prod_stat, batch_nbr, sku_attr_1,   
            sku_attr_2, sku_attr_3, sku_attr_4, sku_attr_5, cntry_of_orgn,  
            invn_need_type, alloc_uom, qty_alloc, qty_pulld, stat_code, 
            task_id, task_genrtn_ref_nbr 
        )
        select o.order_id, iv.line_item_id, iv.cd_master_id, iv.cntr_nbr,  
            iv.carton_nbr, iv.carton_seq_nbr, iv.pull_locn_id, iv.item_id,   
            iv.invn_type, iv.prod_stat, iv.batch_nbr, iv.sku_attr_1,   
            iv.sku_attr_2, iv.sku_attr_3, iv.sku_attr_4, iv.sku_attr_5, 
            iv.cntry_of_orgn, iv.invn_need_type, iv.alloc_uom, iv.qty_alloc, 
            iv.qty_pulld, iv.stat_code, iv.task_id, iv.task_genrtn_ref_nbr 
        from
        (
            select aid.tc_order_id, aid.line_item_id, aid.cd_master_id, aid.cntr_nbr, 
                aid.carton_nbr, aid.carton_seq_nbr, aid.pull_locn_id, aid.item_id,  
                aid.invn_type, aid.prod_stat, aid.batch_nbr, aid.sku_attr_1, 
                aid.sku_attr_2, aid.sku_attr_3, aid.sku_attr_4, aid.sku_attr_5, 
                aid.cntry_of_orgn, aid.invn_need_type, aid.alloc_uom, aid.qty_alloc, 
                aid.qty_pulld, aid.stat_code, null task_id, aid.task_genrtn_ref_nbr
            from alloc_invn_dtl aid
            where aid.pull_locn_id = p_locn_id and aid.stat_code <90 
                and aid.cd_master_id = p_tc_company_id and aid.task_cmpl_ref_code = 6
                and aid.task_cmpl_ref_nbr = aid.carton_nbr
                and aid.whse = v_whse
                and not exists
                (
                    select 1
                    from alloc_invn_dtl aid2
                    where aid2.pull_locn_id != p_locn_id
                       and aid2.stat_code < 90 and aid2.carton_nbr = aid.carton_nbr
                       and aid.whse = v_whse
                )
            union all
            select td.tc_order_id, td.line_item_id, td.cd_master_id, td.cntr_nbr, 
                td.carton_nbr, td.carton_seq_nbr, td.pull_locn_id, td.item_id,  
                td.invn_type, td.prod_stat, td.batch_nbr, td.sku_attr_1, 
                td.sku_attr_2, td.sku_attr_3, td.sku_attr_4, td.sku_attr_5, 
                td.cntry_of_orgn, td.invn_need_type, td.alloc_uom, td.qty_alloc, 
                td.qty_pulld, td.stat_code, td.task_id, td.task_genrtn_ref_nbr
            from task_dtl td
            where td.pull_locn_id = p_locn_id and td.stat_code < 90 
                and td.cd_master_id = p_tc_company_id and td.task_cmpl_ref_code = 6
                and td.task_cmpl_ref_nbr = td.carton_nbr
                and not exists 
                (
                    select 1
                    from task_dtl td2
                    where td2.pull_locn_id != p_locn_id
                        and td.stat_code < 90 and td2.carton_nbr = td.carton_nbr
                )
                and exists
                (
                     select 1
                     from task_hdr th
                     where td.task_id = th.task_id and th.task_hdr_id = td.task_hdr_id
                         and th.whse = v_whse
                )
        ) iv
        join orders o on o.tc_order_id = iv.tc_order_id 
            and o.tc_company_id = iv.cd_master_id
            and o.o_facility_id = p_facility_id;  
    end if;
    wm_cs_log('Allocations/tasks collected: ' || sql%rowcount );

    -- adjust qty on Agg line 
    merge into order_line_item oli
    using
    (
        select t.line_item_id, sum(t.qty_alloc - t.qty_pulld) qty_to_pack
        from tmp_pack_lpn_alloc_dtl t
        group by t.line_item_id
    ) iv on (iv.line_item_id = oli.line_item_id)
    when matched then
    update set oli.last_updated_source = p_user_id, oli.last_updated_dttm = v_current_date,
        oli.hibernate_version = (coalesce(oli.hibernate_version,0)+1),
        oli.units_pakd = coalesce(oli.units_pakd, 0) + iv.qty_to_pack,
        oli.do_dtl_status =
        (
            case
                when oli.order_qty - oli.allocated_qty <= 0 then
                    case
                        when oli.order_qty - (coalesce(oli.units_pakd, 0) + iv.qty_to_pack)
                            <= 0 then 150
                        else 140
                    end
                else 120
            end
        );
    wm_cs_log('aggregate lines updated: ' || sql%rowcount);   

    -- adjust qty on orig lines
    merge into order_line_item oli
    using
    (
        with agg as
        (
            select t.order_id, t.line_item_id, sum(t.qty_alloc - t.qty_pulld) qty_to_pack
            from tmp_pack_lpn_alloc_dtl t
            group by t.order_id, t.line_item_id
        )
        select iv2.line_item_id, iv2.reference_line_item_id,
            (
                case 
                    when iv2.rng_to_pack_qty <= iv2.agg_qty_to_pack then iv2.to_pack_qty
                    else iv2.agg_qty_to_pack - iv2.lag_rng_to_pack_qty 
                 end
            ) qty_pakd_per_line
        from
        (
            select iv1.line_item_id, iv1.reference_line_item_id,
                iv1.to_pack_qty, agg.qty_to_pack agg_qty_to_pack, iv1.rng_to_pack_qty,
                lag(iv1.rng_to_pack_qty, 1, 0) over(partition by iv1.reference_line_item_id order by iv1.rng_to_pack_qty) lag_rng_to_pack_qty
            from
            (
                select orig.line_item_id, orig.reference_line_item_id,
                    orig.allocated_qty - coalesce(orig.units_pakd, 0) - coalesce(orig.user_canceled_qty, 0) to_pack_qty,
                    sum(orig.allocated_qty - coalesce(orig.units_pakd, 0) - coalesce(orig.user_canceled_qty, 0))
                    over(partition by orig.reference_line_item_id order by orig.priority desc, orig.order_id asc, orig.line_item_id desc) rng_to_pack_qty
                from agg
                join order_line_item orig on orig.reference_order_id = agg.order_id
                    and orig.reference_line_item_id = agg.line_item_id
                where (orig.do_dtl_status between 120 and 140)
                    and orig.allocated_qty - coalesce(orig.units_pakd, 0) - coalesce(orig.user_canceled_qty, 0) > 0 
            ) iv1
            join agg on agg.line_item_id = iv1.reference_line_item_id
        ) iv2 where iv2.lag_rng_to_pack_qty < iv2.agg_qty_to_pack
    ) iv on (iv.line_item_id = oli.line_item_id)
    when matched then
    update set oli.last_updated_source = p_user_id, oli.last_updated_dttm = v_current_date, 
        oli.units_pakd = coalesce(oli.units_pakd, 0) + iv.qty_pakd_per_line,
        oli.do_dtl_status =
        (
            case
                when oli.order_qty - oli.allocated_qty <= 0 then
                    case
                        when oli.order_qty - (coalesce(oli.units_pakd, 0) + iv.qty_pakd_per_line) <= 0 
                            then 150
                        else 140
                    end
                else 120
            end
        );
    wm_cs_log('orginal lines adjusted: ' || sql%rowcount);

    merge into order_split_line_item osli
    using
    (
        with agg as
        (
            select t.order_id, t.line_item_id, sum(t.qty_alloc - t.qty_pulld) qty_to_pack
            from tmp_pack_lpn_alloc_dtl t
            group by t.order_id, t.line_item_id
        )
        select iv2.order_split_line_item_id,
            (
                case 
                    when iv2.rng_to_pack_qty <= iv2.agg_qty_to_pack then iv2.to_pack_qty
                    else iv2.agg_qty_to_pack - iv2.lag_rng_to_pack_qty 
                end
            ) qty_pakd_per_split_line 
        from
        (
            select iv1.order_split_line_item_id, iv1.agg_qty_to_pack, iv1.to_pack_qty, 
                iv1.rng_to_pack_qty,
                lag(iv1.rng_to_pack_qty, 1, 0) over(partition by iv1.line_item_id 
                    order by iv1.rng_to_pack_qty) lag_rng_to_pack_qty
            from
            (
                select osli.order_split_line_item_id, agg.qty_to_pack agg_qty_to_pack, 
                    agg.line_item_id, osli.order_qty - coalesce(osli.shipped_qty, 0) to_pack_qty,
                    sum(osli.order_qty - coalesce(osli.shipped_qty, 0))
                        over(partition by agg.line_item_id
                        order by (case when os.shipment_id is null then 0 else 1 end), osli.order_split_line_item_id) rng_to_pack_qty
                from agg
                join order_split_line_item osli on osli.order_id = agg.order_id
                    and osli.line_item_id = agg.line_item_id
                join order_split os on os.order_split_id = osli.order_split_id and os.is_cancelled = 0
-- check: shpmt closed indic?
                where osli.order_qty - coalesce(osli.shipped_qty, 0) > 0
            ) iv1
        ) iv2 where iv2.lag_rng_to_pack_qty < iv2.agg_qty_to_pack
    ) iv on (iv.order_split_line_item_id = osli.order_split_line_item_id)
    when matched then
    update set osli.units_packed = coalesce(osli.units_packed, 0) + iv.qty_pakd_per_split_line,
        osli.last_updated_dttm = v_current_date, osli.last_updated_source = p_user_id;
    wm_cs_log('order split lines adjusted: ' || sql%rowcount);

    -- pkt_dtl updates
    merge into pkt_dtl pd
    using
    (
        select t.order_id, t.line_item_id, sum(t.qty_alloc - t.qty_pulld) pakd_qty
        from tmp_pack_lpn_alloc_dtl t
        group by t.order_id, t.line_item_id
    ) iv on (iv.line_item_id = pd.reference_line_item_id 
        and iv.order_id = pd.reference_order_id)
    when matched then
    update set pd.user_id = p_user_id, pd.mod_date_time = v_current_date,
        pd.units_pakd = coalesce(pd.units_pakd, 0) + iv.pakd_qty,
        pd.verf_as_pakd = coalesce(pd.verf_as_pakd, 0) + iv.pakd_qty,
        pd.wm_version_id = (coalesce(pd.wm_version_id,0)+1),
        pd.stat_code = 40;
    wm_cs_log('pkt_dtls updated: ' || sql%rowcount );
    
    -- iLPN updates
    
    -- assumption! for active and case pick cntr_nbr is null and there is 1 record for 1 locn
    -- assumption! for reserve the combination of tc_lpn_id and location_id is unique
    merge into wm_inventory wi
    using
    (
         select t.cntr_nbr, t.pull_locn_id, t.item_id, t.invn_type, t.prod_stat, 
             t.cntry_of_orgn, t.batch_nbr, t.sku_attr_1, t.sku_attr_2, 
             t.sku_attr_3, t.sku_attr_4, t.sku_attr_5, t.cd_master_id,
             sum(t.qty_alloc - t.qty_pulld) as to_pack_qty
         from tmp_pack_lpn_alloc_dtl t
         group by t.cntr_nbr, t.pull_locn_id, t.item_id, t.invn_type, 
             t.prod_stat, t.cntry_of_orgn, t.batch_nbr, t.sku_attr_1, 
             t.sku_attr_2, t.sku_attr_3, t.sku_attr_4, t.sku_attr_5,
			 t.cd_master_id
    )iv on (coalesce(iv.cntr_nbr, '0') = coalesce(wi.tc_lpn_id, '0')  
        and iv.cd_master_id = wi.tc_company_id
        and wi.c_facility_id = p_facility_id
        and wi.inbound_outbound_indicator = 'I' 
        and iv.item_id = wi.item_id
        and (iv.invn_type = '*' or wi.inventory_type = '*'
            or coalesce(iv.invn_type, '0') = coalesce(wi.inventory_type, '0'))
        and (iv.prod_stat = '*' or wi.product_status = '*'
            or coalesce(iv.prod_stat, '0') = coalesce(wi.product_status, '0'))
        and (iv.batch_nbr = '*' or wi.batch_nbr = '*'
            or coalesce(iv.batch_nbr, '0') = coalesce(wi.batch_nbr, '0'))
        and (iv.cntry_of_orgn = '*' or wi.cntry_of_orgn = '*'
            or coalesce(iv.cntry_of_orgn, '0') = coalesce(wi.cntry_of_orgn, '0'))
        and (iv.sku_attr_1 = '*' or wi.item_attr_1 = '*'
            or coalesce(iv.sku_attr_1, '0') = coalesce(wi.item_attr_1, '0'))
        and (iv.sku_attr_2 = '*' or wi.item_attr_2 = '*'
            or coalesce(iv.sku_attr_2, '0') = coalesce(wi.item_attr_2, '0'))
        and (iv.sku_attr_3 = '*' or wi.item_attr_3 = '*'
            or coalesce(iv.sku_attr_3, '0') = coalesce(wi.item_attr_3, '0'))
        and (iv.sku_attr_4 = '*' or wi.item_attr_4 = '*'
            or coalesce(iv.sku_attr_4, '0') = coalesce(wi.item_attr_4, '0'))
        and (iv.sku_attr_5 = '*' or wi.item_attr_5 = '*'
            or coalesce(iv.sku_attr_5, '0') = coalesce(wi.item_attr_5, '0')))
    when matched then
    update set wi.last_updated_source = p_user_id, wi.last_updated_dttm = v_current_date,
        wi.wm_version_id = (coalesce(wi.wm_version_id,0)+1),
        wi.on_hand_qty =
            (case
                when (wi.on_hand_qty - iv.to_pack_qty) <= 0 then 0
                else (wi.on_hand_qty - iv.to_pack_qty)
             end),
        wi.wm_allocated_qty =
            (case
                when (wi.wm_allocated_qty - iv.to_pack_qty) <= 0 then 0
                else (wi.wm_allocated_qty - iv.to_pack_qty)
             end)
    delete where wi.on_hand_qty <=0 ; 
    wm_cs_log('case wm_inventory details updated: ' || sql%rowcount );

    merge into lpn_detail ibld
    using
    (
        select l.lpn_id, t.item_id, t.invn_type, t.prod_stat, t.cntry_of_orgn, 
             t.batch_nbr, t.sku_attr_1, t.sku_attr_2, t.sku_attr_3, t.sku_attr_4, 
             t.sku_attr_5, sum(t.qty_alloc - t.qty_pulld) qty_to_pack
        from tmp_pack_lpn_alloc_dtl t
        join lpn l on (t.cntr_nbr = l.tc_lpn_id and l.inbound_outbound_indicator = 'I' 
            and l.tc_company_id = p_tc_company_id and l.c_facility_id = p_facility_id)
        where t.cntr_nbr is not null
        group by l.lpn_id, t.item_id, t.invn_type, t.prod_stat, t.cntry_of_orgn, 
            t.batch_nbr, t.sku_attr_1, t.sku_attr_2, t.sku_attr_3, t.sku_attr_4, 
            t.sku_attr_5
    ) iv on (iv.lpn_id = ibld.lpn_id and ibld.item_id = iv.item_id
        and (iv.invn_type = '*' or ibld.inventory_type = '*'
            or coalesce(iv.invn_type, '0') = coalesce(ibld.inventory_type, '0'))
        and (iv.prod_stat = '*' or ibld.product_status = '*'
            or coalesce(iv.prod_stat, '0') = coalesce(ibld.product_status, '0'))
        and (iv.batch_nbr = '*' or ibld.batch_nbr = '*'
            or coalesce(iv.batch_nbr, '0') = coalesce(ibld.batch_nbr, '0'))
        and (iv.cntry_of_orgn = '*' or ibld.cntry_of_orgn = '*'
            or coalesce(iv.cntry_of_orgn, '0') = coalesce(ibld.cntry_of_orgn, '0'))
        and (iv.sku_attr_1 = '*' or ibld.item_attr_1 = '*'
            or coalesce(iv.sku_attr_1, '0') = coalesce(ibld.item_attr_1, '0'))
        and (iv.sku_attr_2 = '*' or ibld.item_attr_2 = '*'
            or coalesce(iv.sku_attr_2, '0') = coalesce(ibld.item_attr_2, '0'))
        and (iv.sku_attr_3 = '*' or ibld.item_attr_3 = '*'
            or coalesce(iv.sku_attr_3, '0') = coalesce(ibld.item_attr_3, '0'))
        and (iv.sku_attr_4 = '*' or ibld.item_attr_4 = '*'
            or coalesce(iv.sku_attr_4, '0') = coalesce(ibld.item_attr_4, '0'))
        and (iv.sku_attr_5 = '*' or ibld.item_attr_5 = '*'
            or coalesce(iv.sku_attr_5, '0') = coalesce(ibld.item_attr_5, '0')))
    when matched then
    update set ibld.hibernate_version = (coalesce(ibld.hibernate_version, 0)+1), 
        ibld.last_updated_source = p_user_id,
        ibld.last_updated_dttm = v_current_date,
        ibld.size_value =
            (
              case
                when (ibld.size_value - iv.qty_to_pack) <= 0 then 0
                else (ibld.size_value - iv.qty_to_pack)
              end
            );
    wm_cs_log('iLPN details adjusted: ' || sql%rowcount);
        
    -- oLPN updates
    merge into wm_inventory wi
    using
    (
        select t.carton_nbr, t.carton_seq_nbr, sum(t.qty_alloc - t.qty_pulld) to_pack_qty
        from tmp_pack_lpn_alloc_dtl t
        group by t.carton_nbr, t.carton_seq_nbr
    ) iv on (iv.carton_nbr = wi.tc_lpn_id and iv.carton_seq_nbr = wi.lpn_detail_id
        and wi.c_facility_id = p_facility_id and wi.tc_company_id = p_tc_company_id
        and wi.inbound_outbound_indicator = 'O')
    when matched then
    update set wi.on_hand_qty = wi.on_hand_qty + iv.to_pack_qty,
        wi.wm_allocated_qty = wi.wm_allocated_qty + iv.to_pack_qty,
        wi.wm_version_id = (coalesce(wi.wm_version_id,0)+1),
        wi.location_id = null, wi.locn_class = null, wi.location_dtl_id = null,
        wi.last_updated_source = p_user_id, wi.last_updated_dttm = v_current_date;
    wm_cs_log('related carton inventory details updated: ' || sql%rowcount);

    if (p_pallet_nbr is not null)
    then
        merge into lpn pllt
        using
        (
        
            select p_pallet_nbr tc_lpn_id from dual
            
        ) iv on (iv.tc_lpn_id = pllt.tc_lpn_id and pllt.inbound_outbound_indicator = 'O'
            and pllt.tc_company_id = p_tc_company_id and pllt.c_facility_id = p_facility_id
            and pllt.lpn_type = 2)
        when not matched then
        insert 
        (
            weight, actual_volume, hibernate_version, created_source, created_dttm,
            inbound_outbound_indicator, voco_intrn_reverse_id, lpn_id, tc_lpn_id, 
            tc_company_id, lpn_type, tier_qty, c_facility_id, lpn_status, 
            lpn_facility_status, single_line_lpn, estimated_weight 
        )
        values
        (
            0, 0, 1, p_user_id, v_current_date, 'O', reverse(p_pallet_nbr), 
            lpn_id_seq.nextval, p_pallet_nbr, p_tc_company_id, 2, 0, 
            p_facility_id, 10, 0, 'Y', 0
        );
        wm_cs_log('outbound Pallet entry created: ' || sql%rowcount);
    end if;

    merge into lpn_detail obld
    using
    (
        with tmp_lpn as
        (
            select t.carton_nbr, t.carton_seq_nbr, t.item_id, t.cntr_nbr,   
                t.pull_locn_id, sum(t.qty_alloc - t.qty_pulld) qty_to_pack
            from tmp_pack_lpn_alloc_dtl t
            group by t.carton_nbr, t.carton_seq_nbr, t.item_id, t.cntr_nbr, t.pull_locn_id
        )
        select ol.lpn_id, tmp_lpn.carton_seq_nbr lpn_detail_id, tmp_lpn.item_id,  
            il.consumption_priority_dttm, tmp_lpn.qty_to_pack, vndr.vendor_item_nbr
        from tmp_lpn
        join lpn ol on (tmp_lpn.carton_nbr = ol.tc_lpn_id and ol.inbound_outbound_indicator = 'O'
            and ol.tc_company_id = p_tc_company_id and ol.c_facility_id = p_facility_id)
        left join lpn il on (il.inbound_outbound_indicator = 'I' 
            and il.tc_lpn_id = tmp_lpn.cntr_nbr 
            and il.tc_company_id = p_tc_company_id 
            and il.c_facility_id = p_facility_id)
        left join lpn_detail vndr on (vndr.lpn_id = il.lpn_id 
            and vndr.item_id = tmp_lpn.item_id)
    ) iv on (iv.lpn_id = obld.lpn_id and iv.lpn_detail_id = obld.lpn_detail_id)
    when matched then
    update set obld.hibernate_version = (coalesce(obld.hibernate_version, 0)+1), 
        obld.last_updated_source = p_user_id,
        obld.last_updated_dttm = v_current_date,
        obld.size_value = coalesce(obld.size_value, 0) + iv.qty_to_pack,
        obld.consumption_priority_dttm = coalesce(iv.consumption_priority_dttm, obld.consumption_priority_dttm),
        obld.vendor_item_nbr = coalesce(iv.vendor_item_nbr, obld.vendor_item_nbr),
        obld.lpn_detail_status = 
            (
                case 
                    when (obld.initial_qty - (obld.size_value + iv.qty_to_pack)) <= 0 then 90
                    else 0
                end
            );
    wm_cs_log('oLPN details adjusted: ' || sql%rowcount);

    -- iLPN updates
    -- happen only for INT2
    update lpn ilpn
    set ilpn.hibernate_version = (coalesce(ilpn.hibernate_version,0)+1),
        ilpn.last_updated_source = p_user_id, ilpn.last_updated_dttm = v_current_date,
        ilpn.parent_lpn_id = null, ilpn.tc_parent_lpn_id = null, ilpn.lpn_status = 70, 
        ilpn.lpn_facility_status = 95, ilpn.lpn_facility_stat_updated_dttm = v_current_date,
        ilpn.weight = 0, ilpn.prev_sub_locn_id = ilpn.curr_sub_locn_id, ilpn.curr_sub_locn_id = null
    where ilpn.c_facility_id = p_facility_id and ilpn.tc_company_id = p_tc_company_id 
        and ilpn.inbound_outbound_indicator = 'I'
        and exists
        (
            select 1
            from tmp_pack_lpn_alloc_dtl t
            where t.cntr_nbr = ilpn.tc_lpn_id
        )
        and not exists
        (
            select 1
            from wm_inventory wi
            where wi.lpn_id = ilpn.lpn_id and wi.on_hand_qty > 0
        );

    -- task_dtl updates
    update task_dtl td
    set td.qty_pulld = td.qty_alloc, td.stat_code = 90,
        td.mod_date_time = v_current_date, td.user_id = p_user_id
    where exists
    (
        select 1
        from tmp_pack_lpn_alloc_dtl t
        where t.task_id = td.task_id
            and coalesce(t.cntr_nbr, '0') = coalesce(td.cntr_nbr, '0')
            and coalesce(t.pull_locn_id, '0') = coalesce(td.pull_locn_id, '0')
            and t.task_id is not null
    );
    wm_cs_log('task_dtls completed: ' || sql%rowcount);

    -- task_hdr updates
    update task_hdr th
    set th.stat_code = 90, th.mod_date_time = v_current_date, 
        th.user_id = p_user_id
    where th.whse = v_whse 
    and exists
    (
        select 1
        from tmp_pack_lpn_alloc_dtl t1
        where t1.task_id = th.task_id and t1.task_id is not null
    )
    and not exists
    (
        select 1
        from task_dtl td
        join tmp_pack_lpn_alloc_dtl t2 on t2.task_id = td.task_id and t2.task_id is not null
        where td.task_id = th.task_id
            and td.stat_code < 90
    );
    wm_cs_log('task_hdr updated: ' || sql%rowcount);

    -- update alloc_invn_dtl
    update alloc_invn_dtl aid
    set aid.qty_pulld = aid.qty_alloc, aid.stat_code = 90, 
        aid.mod_date_time = v_current_date, aid.user_id = p_user_id
    where aid.whse = v_whse 
        and (aid.cntr_nbr = p_cntr_nbr or aid.pull_locn_id = p_locn_id)
    and exists
    (
        select 1
        from tmp_pack_lpn_alloc_dtl t
        where t.line_item_id = aid.line_item_id
            and t.task_id is null
    );
    wm_cs_log('alloc_invn_dtl completed: ' || sql%rowcount);

    -- oLPN update
    merge into lpn l
    using 
    ( 
        select lpn.lpn_id, pllt.lpn_id pallet_id,
            coalesce(sum(wi.on_hand_qty),0) totat_lpn_qty
        from lpn               
        join orders o on lpn.order_id = o.order_id
        join wm_inventory wi on wi.tc_lpn_id = lpn.tc_lpn_id
            and wi.inbound_outbound_indicator = 'O' and wi.c_facility_id = p_facility_id
            and wi.tc_company_id = lpn.tc_company_id
        left join lpn pllt on pllt.tc_lpn_id = p_pallet_nbr
            and pllt.tc_company_id = o.tc_company_id
            and pllt.inbound_outbound_indicator = 'O' 
            and pllt.c_facility_id = p_facility_id
        where lpn.lpn_type = 1 and lpn.tc_company_id = p_tc_company_id
            and lpn.c_facility_id = p_facility_id
            and exists
            (
                select 1
                from tmp_pack_lpn_alloc_dtl t
                where t.carton_nbr = lpn.tc_lpn_id
            )
        group by lpn.lpn_id, pllt.lpn_id
    ) iv on (l.lpn_id = iv.lpn_id)
    when matched then
    update set l.hibernate_version = (coalesce(l.hibernate_version,0)+1),
        l.last_updated_source = p_user_id, l.last_updated_dttm = v_current_date,
        l.total_lpn_qty = iv.totat_lpn_qty,
        l.packer_userid = p_user_id,
        l.curr_sub_locn_id = null,
        l.lpn_status =
            (
                 case
                      when exists
                           (
                             select 1 
                             from task_dtl td 
                             where td.task_cmpl_ref_code = 6 
                                 and td.task_cmpl_ref_nbr = l.tc_lpn_id 
                                 and td.stat_code < 90
                             union
                             select 1 
                             from alloc_invn_dtl aid 
                             where aid.task_cmpl_ref_code = 6 
                                 and aid.task_cmpl_ref_nbr = l.tc_lpn_id 
                                 and aid.stat_code < 90
                           ) then 10
                      else 15
                 end
            ),
        l.lpn_facility_status =
            (
                 case
                      when not exists
                           (
                             select 1 
                             from task_dtl td 
                             where td.task_cmpl_ref_code = 6 
                                 and td.task_cmpl_ref_nbr = l.tc_lpn_id 
                                 and td.stat_code < 90
                             union
                             select 1 
                             from alloc_invn_dtl aid 
                             where aid.task_cmpl_ref_code = 6 
                                 and aid.task_cmpl_ref_nbr = l.tc_lpn_id 
                                 and aid.stat_code < 90
                           ) then case when p_auto_weigh = 1 then 30 else 20 end
                      else 15
                 end
            ),
       l.weight =
           (
                 case
                      when not exists
                           (
                             select 1 
                             from task_dtl td 
                             where td.task_cmpl_ref_code = 6 
                                 and td.task_cmpl_ref_nbr = l.tc_lpn_id 
                                 and td.stat_code < 90
                             union
                             select 1 
                             from alloc_invn_dtl aid 
                             where aid.task_cmpl_ref_code = 6 
                                 and aid.task_cmpl_ref_nbr = l.tc_lpn_id 
                                 and aid.stat_code < 90
                           ) and p_auto_weigh = 1 then l.estimated_weight else l.weight
                 end
            ),
       l.lpn_facility_stat_updated_dttm = v_current_date,
       l.parent_lpn_id = iv.pallet_id,
       l.tc_parent_lpn_id = p_pallet_nbr; 
    wm_cs_log('oLPN hdr updated: ' || sql%rowcount);     
    
    -- Productivity tracking
    if (v_act_trk_tran_nbr is not null)
    then
        insert into prod_trkg_tran
        (
            tran_type, tran_code, whse, from_locn, to_locn, wkstn_id, sam, begin_date, 
            end_date, stat_code, module_name, menu_optn_name, create_date_time,
            mod_date_time, user_id, plt_id, cd_master_id, wm_version_id, tran_nbr, 
            prod_trkg_tran_id, seq_nbr, task_id, cntr_nbr, wave_nbr, nbr_units, 
            old_stat_code, new_stat_code, item_id, tc_order_id, line_item_id
        )
        select '500', '001', v_whse, tmp.pull_locn_id, null, up.prtr_reqstr, 0, 
            v_current_date, sysdate, 0, 'Packing', 'Pck Cubed Dir', sysdate, sysdate, 
            p_user_id, p_pallet_nbr, p_tc_company_id, 1, v_act_trk_tran_nbr, 
            prod_trkg_tran_id_seq.nextval, rownum, tmp.task_id, tmp.carton_nbr,
            tmp.task_genrtn_ref_nbr, tmp.qty_alloc, 0, 20, tmp.item_id, 
            o.tc_order_id, tmp.line_item_id
        from tmp_pack_lpn_alloc_dtl tmp
        join orders o on o.order_id = tmp.order_id
        join user_profile up on up.login_user_id = p_user_id 
        where not exists
        (
            select 1 
            from task_dtl td 
            where td.task_cmpl_ref_code = 6 
                and td.task_cmpl_ref_nbr = tmp.carton_nbr 
                and td.stat_code < 90
            union
            select 1 
            from alloc_invn_dtl aid 
            where aid.task_cmpl_ref_code = 6 
                and aid.task_cmpl_ref_nbr = tmp.carton_nbr
                and aid.stat_code < 90
        );
        wm_cs_log('prod_trkg_tran insert: ' || sql%rowcount);
    end if;
    
    if (p_raise_mhe_event = 1) 
    then
        select event_enabled 
        into v_event_enabled
        from interface_config 
        where event_id = 6180 and interface = 'MHE'; 
        
        if (v_event_enabled = 1) 
        then 
            insert into event_message
            (
                event_message_id, event_id, whse, nbr_of_retry, stat_code, 
                error_seq_nbr, create_date_time, mod_date_time, user_id,
                ek_olpn_nbr, cd_master_id, wm_version_id, els_actvty_code
            )
            select event_message_id_seq.nextval, 6180, v_whse, 0, 20, 0, 
            v_current_date, v_current_date, p_user_id, l.tc_lpn_id, 
            p_tc_company_id, 1, p_els_activity_code 
            from lpn l
            where l.lpn_facility_status in (20, 30)
                and exists
                (
                    select 1
                    from tmp_pack_lpn_alloc_dtl t
                    where t.carton_nbr = l.tc_lpn_id
                );
            wm_cs_log('MHE events created : '|| sql%rowcount);
        end if;
    end if;
    
    wm_cs_log('MOP LPN packing complete');
    wm_utils.set_context_info(p_module_name => v_prev_module_name,
        p_action_name => v_prev_action_name);
    commit;
exception
    when others then
        rollback;
        v_error_msg := substr(sqlerrm, 1, 512);
        wm_cs_log('Pack MOP lpn: ' || v_error_msg);
        raise_application_error(-20050, 'Exception '|| v_error_msg);    
end;
/
show errors;

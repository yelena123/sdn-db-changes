CREATE OR REPLACE PROCEDURE PRC_E_SIMULATION_GATHER_STATS( n_stats_flag number )
IS
    v_user   USER_USERS.USERNAME%TYPE;
	type pa_tablename is table of all_tables.table_name%type index by binary_integer;
	type pa_owner is table of user_users.username%type index by binary_integer;
	va_tablename pa_tablename;
	va_owner pa_owner;
BEGIN
   
    SELECT user  INTO  v_user FROM DUAL ;
	
if n_stats_flag =0  then 	

 dbms_stats.gather_table_stats ( ownname => v_user, tabname => 'E_SIMULATION_COND', method_opt =>  'for all indexed columns size auto' ,estimate_percent => 100,granularity => 'ALL',cascade => true,degree => 4 );
 
elsif n_stats_flag = 1 THEN

	begin

        
        select distinct nvl(table_owner,user) , upper(substr(t.table_name , 1, decode(instr(t.table_name,'|'),0,length(t.table_name), instr(t.table_name,'|')-1)))  
        bulk collect into va_owner , va_tablename
        from e_simulation_tables t, user_synonyms a
        where upper(substr(t.table_name , 1, decode(instr(t.table_name,'|'),0,length(t.table_name), instr(t.table_name,'|')-1))) = a.table_name(+)
        order by 2 ;

        FOR I IN 1..va_tablename.count
        LOOP
                BEGIN
                        dbms_stats.gather_table_stats ( ownname => va_owner(i),
                                                        tabname => va_tablename(i),
                                                        method_opt =>  'for all columns size auto' ,
                                                        granularity => 'ALL',
                                                        cascade => true,
                                                        degree => 4 );

                        dbms_output.put_line(sysdate || ': stats done for table' || va_owner(i) ||'.'|| va_tablename(i));

                EXCEPTION
                WHEN OTHERS THEN
                NULL;
                END;
        END LOOP;

	end ;
end if ;	
		

EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (SQLERRM);
END prc_e_simulation_gather_stats;
/

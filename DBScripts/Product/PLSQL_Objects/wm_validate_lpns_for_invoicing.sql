create or replace procedure wm_validate_lpns_for_invoicing
(
    p_user_id               in user_profile.user_id%type,
    p_lang_id               in varchar2,
    p_whse                  in facility.whse%type,
    p_check_manifest        in number
)
as
    v_log_and_abort   number(1) := 0;
    v_lc_info_msg     message_master.msg%type;
    v_lc_err_msg      message_master.msg%type;
    v_open_manif_msg  message_master.msg%type;
    v_vas_info_msg    message_master.msg%type;
    v_pgm_id          varchar2(30) := 'wm_validate_lpns_for_invoicing';
    type ta_tc_lpn_id is table of lpn.tc_lpn_id%type index by binary_integer;
    va_tc_lpn_id      ta_tc_lpn_id;
begin
    v_lc_info_msg := get_msg_info('SHIPPING', '1206', p_lang_id);
    v_lc_err_msg := get_msg_info('SHIPPING', '1205', p_lang_id);
    v_open_manif_msg := get_msg_info('SHIPPING', '1365', p_lang_id);
    v_vas_info_msg := get_msg_info('OUTBGEN', '1230', p_lang_id);

    select min(coalesce(wop.invc_mode, 0))
    into v_log_and_abort
    from whse_outbd_parm wop
    where wop.whse = p_whse;

    -- lpns tied to open manifests
    if (p_check_manifest = 1)
    then
        insert into msg_log 
        (
            msg_log_id, module, msg_id, pgm_id, create_date_time, mod_date_time, 
            user_id, log_date_time, msg
        )
        select msg_log_id_seq.nextval, 'SHIPPING', '1365', v_pgm_id, sysdate, 
            sysdate, p_user_id, sysdate, 
            replace(replace(v_open_manif_msg, '%CTN', tc_lpn_id), '%MST', t.manifest_nbr)
        from tmp_invc_lpn_list t
        where exists
        (
            select 1
            from manifest_hdr mh 
            where mh.tc_manifest_id = t.manifest_nbr and mh.manifest_status_id < 90
        );
        
        if (sql%rowcount > 0)
        then
            if (v_log_and_abort = 1)
            then
                commit;
                raise_application_error(-20050, 'Lpns tied to open manifests found.');
            end if;
        
            delete from tmp_invc_lpn_list t
            where exists
            (
                select 1
                from manifest_hdr mh 
                where mh.tc_manifest_id = t.manifest_nbr and mh.manifest_status_id < 90
            );
            wm_cs_log('Dropped lpns tied to open manifests ' || sql%rowcount);
        end if;
    end if;
    
    -- lpns with lock codes that cannot be invoiced
    insert into msg_log 
    (
        msg_log_id, module, msg_id, pgm_id, create_date_time, mod_date_time, 
        user_id, log_date_time, msg
    )
    select msg_log_id_seq.nextval, 'SHIPPING', '1205', v_pgm_id, sysdate, 
        sysdate, p_user_id, sysdate, 
        replace(replace(v_lc_err_msg, '%LPN', t.tc_lpn_id), '%LCK', ll.inventory_lock_code)
    from tmp_invc_lpn_list t
    join lpn_lock ll on ll.lpn_id = t.lpn_id
    where exists
    (
        select 1
        from sys_code sc
        where sc.rec_type = 'B' and sc.code_type = '528'
            and substr(sc.misc_flags, 5, 1) = '2'
            and sc.code_id = ll.inventory_lock_code
    );
    
    if (sql%rowcount > 0)
    then
        if (v_log_and_abort = 1)
        then
            commit;
            raise_application_error(-20050, 'Lpns tied to lock codes that prevent invoicing.');
        end if;
        
        delete from tmp_invc_lpn_list t
        where exists
        (
            select 1
            from lpn_lock ll 
            where ll.lpn_id = t.lpn_id
                and exists
                (
                    select 1
                    from sys_code sc
                    where sc.rec_type = 'B' and sc.code_type = '528'
                        and substr(sc.misc_flags, 5, 1) = '2'
                        and sc.code_id = ll.inventory_lock_code
                )
        );
        wm_cs_log('Dropped locked lpns ' || sql%rowcount);
    end if;

    delete from tmp_invc_lpn_list t
    where t.lpn_id in
    (
        select t.lpn_id
        from tmp_invc_lpn_list t
        join vas_carton vc on vc.carton_nbr = t.tc_lpn_id and vc.cmpl_qty < vc.reqd_qty
        union all
        select t.lpn_id
        from tmp_invc_lpn_list t
        join vas_pkt vp on vp.order_id = t.order_id and vp.cmpl_qty < vp.reqd_qty
    )
    returning t.tc_lpn_id
    bulk collect into va_tc_lpn_id;
    wm_cs_log('Dropped incomplete VAS lpns  ' || va_tc_lpn_id.count);

    -- note: the msg seed data is incomplete - it actually does not accomodate lpn subs; assume {0}
    forall i in 1..va_tc_lpn_id.count
    insert into msg_log 
    (
        msg_log_id, module, msg_id, pgm_id, create_date_time, mod_date_time, user_id, log_date_time,
        msg
    )
    values
    (
        msg_log_id_seq.nextval, 'OUTBGEN', '1230', v_pgm_id, sysdate, sysdate, p_user_id, sysdate, 
        replace(v_vas_info_msg, '{0}', va_tc_lpn_id(i))
    );
    
    -- warnings for lock codes that allow invoicing
    insert into msg_log 
    (
        msg_log_id, module, msg_id, pgm_id, create_date_time, mod_date_time, 
        user_id, log_date_time, msg
    )
    select msg_log_id_seq.nextval, 'SHIPPING', '1206', v_pgm_id, sysdate, 
        sysdate, p_user_id, sysdate, 
        replace(replace(v_lc_info_msg, '%LPN', t.tc_lpn_id), '%LCK', ll.inventory_lock_code)
    from tmp_invc_lpn_list t
    join lpn_lock ll on ll.lpn_id = t.lpn_id
    where exists
    (
        select 1
        from sys_code sc
        where sc.rec_type = 'B' and sc.code_type = '528'
            and substr(sc.misc_flags, 5, 1) = '1'
            and sc.code_id = ll.inventory_lock_code
    );
end;
/
show errors;

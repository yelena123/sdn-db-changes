CREATE OR REPLACE
PROCEDURE SHIPMENT_A_U_PROC_TLM (O_TC_SHIPMENT_ID              IN VARCHAR2,
                                 O_SHIPMENT_ID                 IN NUMBER,
                                 N_SHIPMENT_ID                 IN NUMBER,
                                 O_O_FACILITY_ID               IN VARCHAR2,
                                 N_O_FACILITY_ID               IN VARCHAR2,
                                 O_O_ADDRESS                   IN VARCHAR2,
                                 O_O_CITY                      IN VARCHAR2,
                                 O_O_STATE_PROV                IN VARCHAR2,
                                 O_O_POSTAL_CODE               IN VARCHAR2,
                                 O_O_COUNTY                    IN VARCHAR2,
                                 O_O_COUNTRY_CODE              IN VARCHAR2,
                                 N_O_ADDRESS                   IN VARCHAR2,
                                 N_O_CITY                      IN VARCHAR2,
                                 N_O_STATE_PROV                IN VARCHAR2,
                                 N_O_POSTAL_CODE               IN VARCHAR2,
                                 N_O_COUNTY                    IN VARCHAR2,
                                 N_O_COUNTRY_CODE              IN VARCHAR2,
                                 O_D_FACILITY_ID               IN VARCHAR2,
                                 N_D_FACILITY_ID               IN VARCHAR2,
                                 O_D_ADDRESS                   IN VARCHAR2,
                                 O_D_CITY                      IN VARCHAR2,
                                 O_D_STATE_PROV                IN VARCHAR2,
                                 O_D_POSTAL_CODE               IN VARCHAR2,
                                 O_D_COUNTY                    IN VARCHAR2,
                                 O_D_COUNTRY_CODE              IN VARCHAR2,
                                 N_D_ADDRESS                   IN VARCHAR2,
                                 N_D_CITY                      IN VARCHAR2,
                                 N_D_STATE_PROV                IN VARCHAR2,
                                 N_D_POSTAL_CODE               IN VARCHAR2,
                                 N_D_COUNTY                    IN VARCHAR2,
                                 N_D_COUNTRY_CODE              IN VARCHAR2,
                                 O_PICKUP_END_DTTM             IN DATE,
                                 O_PICKUP_START_DTTM           IN DATE,
                                 O_DELIVERY_END_DTTM           IN DATE,
                                 O_DELIVERY_START_DTTM         IN DATE,
                                 N_PICKUP_END_DTTM             IN DATE,
                                 N_PICKUP_START_DTTM           IN DATE,
                                 N_DELIVERY_END_DTTM           IN DATE,
                                 N_DELIVERY_START_DTTM         IN DATE,
                                 O_SHIPMENT_STATUS             IN NUMBER,
                                 N_SHIPMENT_STATUS             IN NUMBER,
                                 O_DSG_EQUIPMENT_ID            IN NUMBER,
                                 N_DSG_EQUIPMENT_ID            IN NUMBER,
                                 O_DSG_VOYAGE_FLIGHT           IN VARCHAR2,
                                 N_DSG_VOYAGE_FLIGHT           IN VARCHAR2,
                                 O_TRAILER_NUMBER              IN VARCHAR2,
                                 N_TRAILER_NUMBER              IN VARCHAR2,
                                 O_BK_RESOURCE_NAME_EXTERNAL   IN VARCHAR2,
                                 N_BK_RESOURCE_NAME_EXTERNAL   IN VARCHAR2,
                                 O_SEAL_NUMBER                 IN VARCHAR2,
                                 N_SEAL_NUMBER                 IN VARCHAR2,
                                 O_PLANNED_WEIGHT              IN NUMBER,
                                 N_PLANNED_WEIGHT              IN NUMBER,
                                 O_PLANNED_VOLUME              IN NUMBER,
                                 N_PLANNED_VOLUME              IN NUMBER,
                                 O_SIZE1_VALUE                 IN NUMBER,
                                 N_SIZE1_VALUE                 IN NUMBER,
                                 N_LAST_UPDATED_SOURCE_TYPE    IN NUMBER,
                                 N_LAST_UPDATED_SOURCE         IN VARCHAR2,
                                 N_TENDER_RESP_DEADLINE_DTTM   IN DATE)
AS
   TENDERSTATUS      VARCHAR2 (50);
   SHIP_INV_STATUS   NUMBER DEFAULT 0;

   l_tc_booking_id   BOOKING.BOOKING_ID%TYPE DEFAULT 0;
   vOldValue         BOOKING_EVENT.OLD_VALUE%TYPE;
   vNewValue         BOOKING_EVENT.NEW_VALUE%TYPE;
BEGIN
   --Common code for both updates
   BEGIN
      SELECT BOOKING_ID
        INTO l_tc_booking_id
        FROM BOOKING_SHIPMENT
       WHERE SHIPMENT_ID = O_SHIPMENT_ID;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_tc_booking_id := 0;
   END;

   IF (l_tc_booking_id != 0)
   THEN
      UPDATE SHIPMENT_EXTN_TLM
         SET tender_Status = NULL
       WHERE SHIPMENT_ID = N_SHIPMENT_ID;
   ELSE
      IF (O_SHIPMENT_STATUS <> N_SHIPMENT_STATUS)
      THEN
         IF (N_SHIPMENT_STATUS = 50)
         THEN
            UPDATE SHIPMENT_EXTN_TLM
               SET tender_Status = NULL
             WHERE SHIPMENT_ID = N_SHIPMENT_ID;
         END IF;

         IF (N_SHIPMENT_STATUS = 55)
         THEN
            SELECT TENDER_RESPONSE.DESCRIPTION
              INTO TENDERSTATUS
              FROM RESPONSE_REQUEST, TENDER_RESPONSE
             WHERE TENDER_RESPONSE.TENDER_RESPONSE =
                      RESPONSE_REQUEST.TENDER_RESPONSE
                   AND SHIPMENT_ID = N_SHIPMENT_ID
                   AND RESPONSE_REQUEST_ID IN
                          (SELECT MAX (RESPONSE_REQUEST_ID)
                             FROM RESPONSE_REQUEST
                            WHERE SHIPMENT_ID = N_SHIPMENT_ID);

            UPDATE SHIPMENT_EXTN_TLM
               SET tender_Status = TENDERSTATUS
             WHERE SHIPMENT_ID = N_SHIPMENT_ID;
         END IF;

         IF (O_SHIPMENT_STATUS >= 50 AND N_SHIPMENT_STATUS < 50)
         THEN
            IF (N_SHIPMENT_STATUS = 40 OR N_SHIPMENT_STATUS = 20)
            THEN
               SELECT invitation_Status
                 INTO SHIP_INV_STATUS
                 FROM Response_Request
                WHERE Shipment_Id = N_SHIPMENT_ID
                      AND Response_Request_Id IN
                             (SELECT MAX (Response_Request_Id)
                                FROM RESPONSE_REQUEST
                               WHERE SHIPMENT_ID = N_SHIPMENT_ID);

               IF (SHIP_INV_STATUS = 40)
               THEN
				SELECT TENDER_RESPONSE.DESCRIPTION 
				INTO TENDERSTATUS 
				FROM  TENDER_RESPONSE 
				WHERE TENDER_RESPONSE.TENDER_RESPONSE= 30;
               ELSE
                  SELECT TENDER_RESPONSE.DESCRIPTION
                    INTO TENDERSTATUS
                    FROM RESPONSE_REQUEST, TENDER_RESPONSE
                   WHERE TENDER_RESPONSE.TENDER_RESPONSE =
                            RESPONSE_REQUEST.TENDER_RESPONSE
                         AND SHIPMENT_ID = N_SHIPMENT_ID
                         AND RESPONSE_REQUEST_ID IN
                                (SELECT MAX (RESPONSE_REQUEST_ID)
                                   FROM RESPONSE_REQUEST
                                  WHERE SHIPMENT_ID = N_SHIPMENT_ID);
               END IF;

               UPDATE SHIPMENT_EXTN_TLM
                  SET tender_Status = TENDERSTATUS
                WHERE SHIPMENT_ID = N_SHIPMENT_ID;
            ELSE
               UPDATE SHIPMENT_EXTN_TLM
                  SET tender_Status = NULL
                WHERE SHIPMENT_ID = N_SHIPMENT_ID;
            END IF;
         END IF;

         IF (    N_SHIPMENT_STATUS <> 50
             AND N_SHIPMENT_STATUS <> 40
             AND N_SHIPMENT_STATUS <> 20
             AND N_SHIPMENT_STATUS <> 55)
         THEN
            UPDATE SHIPMENT_EXTN_TLM
               SET tender_Status = NULL
             WHERE SHIPMENT_ID = N_SHIPMENT_ID;
         END IF;
      END IF;
   END IF;

   --Booking Shipment Audit Trial

   IF (l_tc_booking_id != 0)
   THEN
      --Origin Facility
      IF (NVL (O_O_FACILITY_ID, 0) != NVL (N_O_FACILITY_ID, 0))
      THEN
         INS_BOOKING_EVENT (
            l_tc_booking_id,
            'Shipment[' || O_TC_SHIPMENT_ID || ']:Origin Facility',
            O_O_FACILITY_ID,
            N_O_FACILITY_ID,
            N_LAST_UPDATED_SOURCE_TYPE,
            N_LAST_UPDATED_SOURCE);
      END IF;

      --Origin Address Field
      IF    (NVL (O_O_ADDRESS, ' ') != NVL (N_O_ADDRESS, ' '))
         OR (NVL (O_O_CITY, ' ') != NVL (N_O_CITY, ' '))
         OR (NVL (O_O_STATE_PROV, ' ') != NVL (N_O_STATE_PROV, ' '))
         OR (NVL (O_O_POSTAL_CODE, ' ') != NVL (N_O_POSTAL_CODE, ' '))
         OR (NVL (O_O_COUNTRY_CODE, ' ') != NVL (N_O_COUNTRY_CODE, ' '))
         OR (NVL (O_O_COUNTY, ' ') != NVL (N_O_COUNTY, ' '))
      THEN
         vOldValue :=
               'Addr: '
            || NVL (O_O_ADDRESS, ' ')
            || '  City/ST/ZIP: '
            || NVL (O_O_CITY, ' ')
            || ', '
            || NVL (O_O_STATE_PROV, ' ')
            || ' '
            || NVL (O_O_POSTAL_CODE, ' ')
            || 'County: '
            || NVL (O_O_COUNTY, ' ')
            || '  Ctry: '
            || NVL (O_O_COUNTRY_CODE, ' ');
         vNewValue :=
               'Addr: '
            || NVL (N_O_ADDRESS, ' ')
            || '  City/ST/ZIP: '
            || NVL (N_O_CITY, ' ')
            || ', '
            || NVL (N_O_STATE_PROV, ' ')
            || ' '
            || NVL (N_O_POSTAL_CODE, ' ')
            || 'County: '
            || NVL (N_O_COUNTY, ' ')
            || '  Ctry: '
            || NVL (N_O_COUNTRY_CODE, ' ');

         INS_BOOKING_EVENT (
            l_tc_booking_id,
            'Shipment[' || O_TC_SHIPMENT_ID || ']:Origin Address',
            vOldValue,
            vNewValue,
            N_LAST_UPDATED_SOURCE_TYPE,
            N_LAST_UPDATED_SOURCE);
      END IF;

      -- Destination Facility
      IF (NVL (O_D_FACILITY_ID, 0) != NVL (N_D_FACILITY_ID, 0))
      THEN
         INS_BOOKING_EVENT (
            l_tc_booking_id,
            'Shipment[' || O_TC_SHIPMENT_ID || ']:Destination Facility',
            O_D_FACILITY_ID,
            N_D_FACILITY_ID,
            N_LAST_UPDATED_SOURCE_TYPE,
            N_LAST_UPDATED_SOURCE);
      END IF;

      --Destination Address Field
      IF    (NVL (O_D_ADDRESS, ' ') != NVL (N_D_ADDRESS, ' '))
         OR (NVL (O_D_CITY, ' ') != NVL (N_D_CITY, ' '))
         OR (NVL (O_D_STATE_PROV, ' ') != NVL (N_D_STATE_PROV, ' '))
         OR (NVL (O_D_POSTAL_CODE, ' ') != NVL (N_D_POSTAL_CODE, ' '))
         OR (NVL (O_D_COUNTRY_CODE, ' ') != NVL (N_D_COUNTRY_CODE, ' '))
         OR (NVL (O_D_COUNTY, ' ') != NVL (N_D_COUNTY, ' '))
      THEN
         vOldValue :=
               'Addr: '
            || NVL (O_D_ADDRESS, ' ')
            || '  City/ST/ZIP: '
            || NVL (O_D_CITY, ' ')
            || ', '
            || NVL (O_D_STATE_PROV, ' ')
            || ' '
            || NVL (O_D_POSTAL_CODE, ' ')
            || 'County: '
            || NVL (O_D_COUNTY, ' ')
            || '  Ctry: '
            || NVL (O_D_COUNTRY_CODE, ' ');
         vNewValue :=
               'Addr: '
            || NVL (N_D_ADDRESS, ' ')
            || '  City/ST/ZIP: '
            || NVL (N_D_CITY, ' ')
            || ', '
            || NVL (N_D_STATE_PROV, ' ')
            || ' '
            || NVL (N_D_POSTAL_CODE, ' ')
            || 'County: '
            || NVL (N_D_COUNTY, ' ')
            || '  Ctry: '
            || NVL (N_D_COUNTRY_CODE, ' ');

         INS_BOOKING_EVENT (
            l_tc_booking_id,
            'Shipment[' || O_TC_SHIPMENT_ID || ']:Destination Address',
            vOldValue,
            vNewValue,
            N_LAST_UPDATED_SOURCE_TYPE,
            N_LAST_UPDATED_SOURCE);
      END IF;

      --Pickup Start
      IF ( (O_PICKUP_START_DTTM != N_PICKUP_START_DTTM)
          OR (O_PICKUP_START_DTTM IS NULL AND N_PICKUP_START_DTTM IS NOT NULL)
          OR (O_PICKUP_START_DTTM IS NOT NULL AND N_PICKUP_START_DTTM IS NULL))
      THEN
         INS_BOOKING_EVENT (
            l_tc_booking_id,
            'Shipment[' || O_TC_SHIPMENT_ID || ']:Pickup Start',
            TO_CHAR (O_PICKUP_START_DTTM, 'DD-MON-YYYY HH:MI:SS AM'),
            TO_CHAR (N_PICKUP_START_DTTM, 'DD-MON-YYYY HH:MI:SS AM'),
            N_LAST_UPDATED_SOURCE_TYPE,
            N_LAST_UPDATED_SOURCE);
      END IF;

      --Pickup End
      IF (   (O_PICKUP_END_DTTM != N_PICKUP_END_DTTM)
          OR (O_PICKUP_END_DTTM IS NULL AND N_PICKUP_END_DTTM IS NOT NULL)
          OR (O_PICKUP_END_DTTM IS NOT NULL AND N_PICKUP_END_DTTM IS NULL))
      THEN
         INS_BOOKING_EVENT (
            l_tc_booking_id,
            'Shipment[' || O_TC_SHIPMENT_ID || ']:Pickup End',
            TO_CHAR (O_PICKUP_END_DTTM, 'DD-MON-YYYY HH:MI:SS AM'),
            TO_CHAR (N_PICKUP_END_DTTM, 'DD-MON-YYYY HH:MI:SS AM'),
            N_LAST_UPDATED_SOURCE_TYPE,
            N_LAST_UPDATED_SOURCE);
      END IF;

      --Delivery Start
      IF ( (O_DELIVERY_START_DTTM != N_DELIVERY_START_DTTM)
          OR (O_DELIVERY_START_DTTM IS NULL
              AND N_DELIVERY_START_DTTM IS NOT NULL)
          OR (O_DELIVERY_START_DTTM IS NOT NULL
              AND N_DELIVERY_START_DTTM IS NULL))
      THEN
         INS_BOOKING_EVENT (
            l_tc_booking_id,
            'Shipment[' || O_TC_SHIPMENT_ID || ']:Delivery Start',
            TO_CHAR (O_DELIVERY_START_DTTM, 'DD-MON-YYYY HH:MI:SS AM'),
            TO_CHAR (N_DELIVERY_START_DTTM, 'DD-MON-YYYY HH:MI:SS AM'),
            N_LAST_UPDATED_SOURCE_TYPE,
            N_LAST_UPDATED_SOURCE);
      END IF;

      --Delivery End
      IF ( (O_DELIVERY_END_DTTM != N_DELIVERY_END_DTTM)
          OR (O_DELIVERY_END_DTTM IS NULL AND N_DELIVERY_END_DTTM IS NOT NULL)
          OR (O_DELIVERY_END_DTTM IS NOT NULL AND N_DELIVERY_END_DTTM IS NULL))
      THEN
         INS_BOOKING_EVENT (
            l_tc_booking_id,
            'Shipment[' || O_TC_SHIPMENT_ID || ']:Delivery End',
            TO_CHAR (O_DELIVERY_END_DTTM, 'DD-MON-YYYY HH:MI:SS AM'),
            TO_CHAR (N_DELIVERY_END_DTTM, 'DD-MON-YYYY HH:MI:SS AM'),
            N_LAST_UPDATED_SOURCE_TYPE,
            N_LAST_UPDATED_SOURCE);
      END IF;

      --  Shipment Status
      IF (O_SHIPMENT_STATUS != N_SHIPMENT_STATUS)
      THEN
         IF (O_SHIPMENT_STATUS IS NULL)
         THEN
            vOldValue := NULL;
         ELSE
            SELECT DESCRIPTION
              INTO vOldValue
              FROM SHIPMENT_STATUS
             WHERE SHIPMENT_STATUS = O_SHIPMENT_STATUS;
         END IF;

         IF (N_SHIPMENT_STATUS IS NULL)
         THEN
            vNewValue := NULL;
         ELSE
            SELECT DESCRIPTION
              INTO vNewValue
              FROM SHIPMENT_STATUS
             WHERE SHIPMENT_STATUS = N_SHIPMENT_STATUS;
         END IF;

         INS_BOOKING_EVENT (l_tc_booking_id,
                            'Shipment[' || O_TC_SHIPMENT_ID || ']:Status',
                            vOldValue,
                            vNewValue,
                            N_LAST_UPDATED_SOURCE_TYPE,
                            N_LAST_UPDATED_SOURCE);
      END IF;

      -- Equipment Code
      IF (O_DSG_EQUIPMENT_ID != N_DSG_EQUIPMENT_ID)
      THEN
         IF (O_DSG_EQUIPMENT_ID IS NULL)
         THEN
            vOldValue := NULL;
         ELSE
            SELECT EQUIPMENT_CODE
              INTO vOldValue
              FROM EQUIPMENT
             WHERE EQUIPMENT_ID = O_DSG_EQUIPMENT_ID;
         END IF;

         IF (N_DSG_EQUIPMENT_ID IS NULL)
         THEN
            vNewValue := NULL;
         ELSE
            SELECT EQUIPMENT_CODE
              INTO vNewValue
              FROM EQUIPMENT
             WHERE EQUIPMENT_ID = N_DSG_EQUIPMENT_ID;
         END IF;

         INS_BOOKING_EVENT (
            l_tc_booking_id,
            'Shipment[' || O_TC_SHIPMENT_ID || ']:Designated Equipment',
            vOldValue,
            vNewValue,
            N_LAST_UPDATED_SOURCE_TYPE,
            N_LAST_UPDATED_SOURCE);
      END IF;

      --Voyage/Flight
      IF ( (NVL (O_DSG_VOYAGE_FLIGHT, 0) != NVL (N_DSG_VOYAGE_FLIGHT, 0))
          OR (O_DSG_VOYAGE_FLIGHT IS NULL AND N_DSG_VOYAGE_FLIGHT IS NOT NULL)
          OR (O_DSG_VOYAGE_FLIGHT IS NOT NULL AND N_DSG_VOYAGE_FLIGHT IS NULL))
      THEN
         INS_BOOKING_EVENT (
            l_tc_booking_id,
            'Shipment[' || O_TC_SHIPMENT_ID || ']:Voyage/Flight',
            O_DSG_VOYAGE_FLIGHT,
            N_DSG_VOYAGE_FLIGHT,
            N_LAST_UPDATED_SOURCE_TYPE,
            N_LAST_UPDATED_SOURCE);
      END IF;

      --Container Id
      IF (   (NVL (O_TRAILER_NUMBER, 0) != NVL (N_TRAILER_NUMBER, 0))
          OR (O_TRAILER_NUMBER IS NULL AND N_TRAILER_NUMBER IS NOT NULL)
          OR (O_TRAILER_NUMBER IS NOT NULL AND N_TRAILER_NUMBER IS NULL))
      THEN
         INS_BOOKING_EVENT (
            l_tc_booking_id,
            'Shipment[' || O_TC_SHIPMENT_ID || ']:Container Id',
            O_TRAILER_NUMBER,
            N_TRAILER_NUMBER,
            N_LAST_UPDATED_SOURCE_TYPE,
            N_LAST_UPDATED_SOURCE);
      END IF;

      --Vessel Name
      IF ( (NVL (O_BK_RESOURCE_NAME_EXTERNAL, 0) !=
               NVL (N_BK_RESOURCE_NAME_EXTERNAL, 0))
          OR (O_BK_RESOURCE_NAME_EXTERNAL IS NULL
              AND N_BK_RESOURCE_NAME_EXTERNAL IS NOT NULL)
          OR (O_BK_RESOURCE_NAME_EXTERNAL IS NOT NULL
              AND N_BK_RESOURCE_NAME_EXTERNAL IS NULL))
      THEN
         INS_BOOKING_EVENT (
            l_tc_booking_id,
            'Shipment[' || O_TC_SHIPMENT_ID || ']:Vessel Name',
            O_BK_RESOURCE_NAME_EXTERNAL,
            N_BK_RESOURCE_NAME_EXTERNAL,
            N_LAST_UPDATED_SOURCE_TYPE,
            N_LAST_UPDATED_SOURCE);
      END IF;

      --Seal Number
      IF (   (NVL (O_SEAL_NUMBER, 0) != NVL (N_SEAL_NUMBER, 0))
          OR (O_SEAL_NUMBER IS NULL AND N_SEAL_NUMBER IS NOT NULL)
          OR (O_SEAL_NUMBER IS NOT NULL AND N_SEAL_NUMBER IS NULL))
      THEN
         INS_BOOKING_EVENT (
            l_tc_booking_id,
            'Shipment[' || O_TC_SHIPMENT_ID || ']:Seal Number',
            O_SEAL_NUMBER,
            N_SEAL_NUMBER,
            N_LAST_UPDATED_SOURCE_TYPE,
            N_LAST_UPDATED_SOURCE);
      END IF;

      --Planned Weight
      IF (   (NVL (O_PLANNED_WEIGHT, 0) != NVL (N_PLANNED_WEIGHT, 0))
          OR (O_PLANNED_WEIGHT IS NULL AND N_PLANNED_WEIGHT IS NOT NULL)
          OR (O_PLANNED_WEIGHT IS NOT NULL AND N_PLANNED_WEIGHT IS NULL))
      THEN
         INS_BOOKING_EVENT (
            l_tc_booking_id,
            'Shipment[' || O_TC_SHIPMENT_ID || ']:Planned Weight',
            O_PLANNED_WEIGHT,
            N_PLANNED_WEIGHT,
            N_LAST_UPDATED_SOURCE_TYPE,
            N_LAST_UPDATED_SOURCE);
      END IF;

      --Planned Volume
      IF (   (NVL (O_PLANNED_VOLUME, 0) != NVL (N_PLANNED_VOLUME, 0))
          OR (O_PLANNED_VOLUME IS NULL AND N_PLANNED_VOLUME IS NOT NULL)
          OR (O_PLANNED_VOLUME IS NOT NULL AND N_PLANNED_VOLUME IS NULL))
      THEN
         INS_BOOKING_EVENT (
            l_tc_booking_id,
            'Shipment[' || O_TC_SHIPMENT_ID || ']:Planned Volume',
            O_PLANNED_VOLUME,
            N_PLANNED_VOLUME,
            N_LAST_UPDATED_SOURCE_TYPE,
            N_LAST_UPDATED_SOURCE);
      END IF;

      --Size
      IF (   (NVL (O_SIZE1_VALUE, 0) != NVL (N_SIZE1_VALUE, 0))
          OR (O_SIZE1_VALUE IS NULL AND N_SIZE1_VALUE IS NOT NULL)
          OR (O_SIZE1_VALUE IS NOT NULL AND N_SIZE1_VALUE IS NULL))
      THEN
         INS_BOOKING_EVENT (l_tc_booking_id,
                            'Shipment[' || O_TC_SHIPMENT_ID || ']:Size',
                            O_SIZE1_VALUE,
                            N_SIZE1_VALUE,
                            N_LAST_UPDATED_SOURCE_TYPE,
                            N_LAST_UPDATED_SOURCE);
      END IF;
   END IF;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      NULL;
END;
/

create or replace procedure wm_add_joins_for_cat_assign
(
    p_rule_sql    in out varchar2,
    p_whse        in whse_master.whse%type
)
as
begin
    if (instr(p_rule_sql, 'ORDERS.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join orders on orders.order_id = lpn.order_id '
            || ' and orders.tc_company_id = lpn.tc_company_id '
            || ' where ');
    end if;

    if (instr(p_rule_sql, 'ALLOC_INVN_DTL.', 1, 1) > 0)
        or (instr(p_rule_sql, 'ALIAS_IBPALLET.', 1, 1) > 0)
        or (instr(p_rule_sql, 'ALIAS_DLH.', 1, 1) > 0)
        or (instr(p_rule_sql, 'LOCN_HDR.', 1, 1) > 0)
        or (instr(p_rule_sql, 'RESV_LOCN_HDR.', 1, 1) > 0)
        or (instr(p_rule_sql, 'LOCN_GRP.', 1, 1) > 0)
        or (instr(p_rule_sql, 'VAS_CARTON.', 1, 1) > 0)
        or (instr(p_rule_sql, 'VAS_PKT.', 1, 1) > 0) 
        or (instr(p_rule_sql, 'PICK_LOCN_HDR.', 1, 1) > 0)
        or (instr(p_rule_sql, 'PICK_LOCN_DTL.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join alloc_invn_dtl on alloc_invn_dtl.carton_nbr = lpn.tc_lpn_id '
            || ' and alloc_invn_dtl.carton_seq_nbr = lpn_detail.lpn_detail_id '
            || ' and alloc_invn_dtl.whse = ''' || p_whse || ''''
            || ' and alloc_invn_dtl.cd_master_id  = lpn.tc_company_id where ');
    end if;
    
    if (instr(p_rule_sql, 'VAS_CARTON.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join vas_carton on vas_carton.carton_nbr = alloc_invn_dtl.carton_nbr '
            || ' where ');
    end if;
    
    if (instr(p_rule_sql, 'VAS_PKT.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join vas_pkt on vas_pkt.pkt_seq_nbr = alloc_invn_dtl.pkt_seq_nbr '
            || ' and vas_pkt.pkt_ctrl_nbr = alloc_invn_dtl.pkt_ctrl_nbr '
            || ' where ');
    end if;  

    if (instr(p_rule_sql, 'ORDER_LINE_ITEM.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join order_line_item  on order_line_item.order_id = lpn.order_id '
            || ' and order_line_item.line_item_id = lpn_detail.distribution_order_dtl_id '
            || ' and order_line_item.tc_company_id = lpn.tc_company_id '
            || ' where ');
    end if;
    
    if (instr(p_rule_sql, 'ITEM_CBO.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join item_cbo on item_cbo.item_id = lpn_detail.item_id '
            || ' where ');
    end if;

    if (instr(p_rule_sql, 'ITEM_WMS.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join item_wms on item_wms.item_id = lpn_detail.item_id '
            || ' where ');
    end if;

    if (instr(p_rule_sql, 'ITEM_FACILITY_MAPPING_WMS.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join item_facility_mapping_wms on item_facility_mapping_wms.item_id = lpn_detail.item_id'
            || '    and item_facility_mapping_wms.facility_id = lpn.c_facility_id where ');
    end if;
    
    if (instr(p_rule_sql, 'ORDER_NOTE.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join order_note on order_note.order_id = lpn.order_id '
            || ' and order_note.line_item_id = lpn_detail.distribution_order_dtl_id '
            || ' where ');
    end if;

    if (instr(p_rule_sql, 'ALIAS_IBPALLET.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join lpn pllt on pllt.inbound_outbound_indicator = ''I'' and pllt.lpn_type = 2 '
            || ' and alloc_invn_dtl.cntr_nbr = pllt.tc_lpn_id where ');
        p_rule_sql := replace(p_rule_sql, 'ALIAS_IBPALLET.','pllt.');
    end if;

    if (instr(p_rule_sql, 'ALIAS_DLH.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join locn_hdr dlh on dlh.locn_id = alloc_invn_dtl.dest_locn_id where ');

        p_rule_sql := replace(p_rule_sql, 'ALIAS_DLH.','dlh.');
    end if;
    
    if (instr(p_rule_sql, 'LOCN_HDR.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join locn_hdr on locn_hdr.locn_id = alloc_invn_dtl.pull_locn_id '
            || ' where ');
    end if;

    if (instr(p_rule_sql, 'RESV_LOCN_HDR.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join resv_locn_hdr on resv_locn_hdr.locn_id = alloc_invn_dtl.pull_locn_id '
            || ' where ');
    end if;      

    if (instr(p_rule_sql, 'LOCN_GRP.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' left join locn_grp on ((locn_grp.locn_id '
            || ' = alloc_invn_dtl.pull_locn_id and locn_grp.grp_type != 51) '
            || ' or (locn_grp.locn_id = alloc_invn_dtl.dest_locn_id '
            || ' and locn_grp.grp_type = 51 )) '
            || ' where ');
    end if;
           
    if instr(p_rule_sql, 'PICK_LOCN_DTL.', 1, 1) > 0
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join pick_locn_dtl on pick_locn_dtl.locn_id = alloc_invn_dtl.pull_locn_id '
            || ' and pick_locn_dtl.item_id = lpn_detail.item_id '
            || ' where ');
    end if;
           
    if (instr(p_rule_sql, 'PICK_LOCN_HDR.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join pick_locn_hdr on pick_locn_hdr.locn_id = alloc_invn_dtl.pull_locn_id '
            || ' where ');
    end if;   
end;
/

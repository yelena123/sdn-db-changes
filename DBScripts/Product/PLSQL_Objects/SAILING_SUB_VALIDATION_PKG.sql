CREATE OR REPLACE 
PACKAGE SAILING_SUB_VALIDATION_PKG
AS
   FUNCTION VALIDATE_BUSINESS_UNIT (
      vTCCompanyId      IN company.COMPANY_ID%TYPE,
      vBusinessUnitId   IN business_unit.BUSINESS_UNIT%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_FACILITY (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                               vFacilityId    IN facility.FACILITY_ID%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_FACILITY_ALIAS (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vFacilityAliasId   IN facility_alias.FACILITY_ALIAS_ID%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_ZONE (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                           vZone          IN zone.ZONE_ID%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_STATE_PROV (vStateProv IN state_prov.STATE_PROV%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_POSTAL_CODE (
      vPostalCode IN postal_code.POSTAL_CODE%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_COUNTRY (vCountry IN country.COUNTRY_CODE%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_CARRIER_CODE (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vCarrierCode   IN carrier_code.CARRIER_CODE%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_MODE (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                           vMode          IN mot.MOT%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_SERVICE_LEVEL (
      vTCCompanyId    IN company.COMPANY_ID%TYPE,
      vServiceLevel   IN service_level.SERVICE_LEVEL%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_EQUIPMENT (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vEquipment     IN equipment.EQUIPMENT_CODE%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_PROTECTION_LEVEL (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vProtectionLevel   IN protection_level.PROTECTION_LEVEL%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_SIZE_UOM (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                               vSizeUom       IN SIZE_UOM.SIZE_UOM%TYPE)
      RETURN NUMBER;
END SAILING_SUB_VALIDATION_PKG;
/

CREATE OR REPLACE 
PACKAGE BODY SAILING_SUB_VALIDATION_PKG
AS
   FUNCTION VALIDATE_BUSINESS_UNIT (
      vTCCompanyId      IN company.COMPANY_ID%TYPE,
      vBusinessUnitId   IN business_unit.BUSINESS_UNIT%TYPE)
      RETURN NUMBER
   IS
      vPassFail            NUMBER;
      vBusinessUnitCount   NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (COMPANY_NAME)
        INTO vBusinessUnitCount
        FROM COMPANY
       WHERE PARENT_COMPANY_ID = vTCCompanyId AND COMPANY_NAME = vBusinessUnitId;

      IF (vBusinessUnitCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_BUSINESS_UNIT;

   FUNCTION VALIDATE_FACILITY (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                               vFacilityId    IN facility.FACILITY_ID%TYPE)
      RETURN NUMBER
   IS
      vPassFail        NUMBER;
      vFacilityCount   NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (FACILITY_ID)
        INTO vFacilityCount
        FROM FACILITY
       WHERE TC_COMPANY_ID = vTCCompanyId AND FACILITY_ID = vFacilityId;

      IF (vFacilityCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_FACILITY;

   FUNCTION VALIDATE_FACILITY_ALIAS (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vFacilityAliasId   IN facility_alias.FACILITY_ALIAS_ID%TYPE)
      RETURN NUMBER
   IS
      vPassFail             NUMBER;
      vFacilityAliasCount   NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (FACILITY_ALIAS_ID)
        INTO vFacilityAliasCount
        FROM FACILITY_ALIAS
       WHERE TC_COMPANY_ID = vTCCompanyId
             AND FACILITY_ALIAS_ID = vFacilityAliasId;

      IF (vFacilityAliasCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_FACILITY_ALIAS;

   FUNCTION VALIDATE_ZONE (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                           vZone          IN zone.ZONE_ID%TYPE)
      RETURN NUMBER
   IS
      vPassFail    NUMBER;
      vZoneCount   NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (ZONE_ID)
        INTO vZoneCount
        FROM ZONE
       WHERE TC_COMPANY_ID = vTCCompanyId AND ZONE_ID = vZone;

      IF (vZoneCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_ZONE;

   FUNCTION VALIDATE_STATE_PROV (vStateProv IN state_prov.STATE_PROV%TYPE)
      RETURN NUMBER
   IS
      vPassFail     NUMBER;
      vStateCount   NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (STATE_PROV)
        INTO vStateCount
        FROM STATE_PROV
       WHERE STATE_PROV = vStateProv;

      IF (vStateCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_STATE_PROV;

   FUNCTION VALIDATE_POSTAL_CODE (
      vPostalCode IN postal_code.POSTAL_CODE%TYPE)
      RETURN NUMBER
   IS
      vPassFail      NUMBER;
      vPostalCount   NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (POSTAL_CODE)
        INTO vPostalCount
        FROM POSTAL_CODE
       WHERE POSTAL_CODE = vPostalCode;

      IF (vPostalCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_POSTAL_CODE;

   FUNCTION VALIDATE_COUNTRY (vCountry IN country.COUNTRY_CODE%TYPE)
      RETURN NUMBER
   IS
      vPassFail       NUMBER;
      vCountryCount   NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (COUNTRY_CODE)
        INTO vCountryCount
        FROM COUNTRY
       WHERE COUNTRY_CODE = vCountry;

      IF (vCountryCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_COUNTRY;

   FUNCTION VALIDATE_CARRIER_CODE (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vCarrierCode   IN carrier_code.CARRIER_CODE%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
      vCount      NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (CARRIER_CODE)
        INTO vCount
        FROM CARRIER_CODE
       WHERE TC_COMPANY_ID = vTCCompanyId AND CARRIER_CODE = vCarrierCode;

      IF (vCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_CARRIER_CODE;

   FUNCTION VALIDATE_MODE (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                           vMode          IN mot.MOT%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
      vCount      NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (MOT)
        INTO vCount
        FROM MOT
       WHERE     TC_COMPANY_ID = vTCCompanyId
             AND MOT = vMode
             AND MARK_FOR_DELETION = 0;

      IF (vCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_MODE;

   FUNCTION VALIDATE_SERVICE_LEVEL (
      vTCCompanyId    IN company.COMPANY_ID%TYPE,
      vServiceLevel   IN service_level.SERVICE_LEVEL%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
      vCount      NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (SERVICE_LEVEL)
        INTO vCount
        FROM SERVICE_LEVEL
       WHERE TC_COMPANY_ID = vTCCompanyId AND SERVICE_LEVEL = vServiceLevel;

      IF (vCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_SERVICE_LEVEL;

   FUNCTION VALIDATE_EQUIPMENT (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vEquipment     IN equipment.EQUIPMENT_CODE%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
      vCount      NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (EQUIPMENT_CODE)
        INTO vCount
        FROM EQUIPMENT
       WHERE TC_COMPANY_ID = vTCCompanyId AND EQUIPMENT_CODE = vEquipment;

      IF (vCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_EQUIPMENT;

   FUNCTION VALIDATE_PROTECTION_LEVEL (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vProtectionLevel   IN protection_level.PROTECTION_LEVEL%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
      vCount      NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (PROTECTION_LEVEL)
        INTO vCount
        FROM PROTECTION_LEVEL
       WHERE TC_COMPANY_ID = vTCCompanyId
             AND PROTECTION_LEVEL = vProtectionLevel;

      IF (vCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_PROTECTION_LEVEL;

   FUNCTION VALIDATE_SIZE_UOM (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                               vSizeUom       IN SIZE_UOM.SIZE_UOM%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
      vCount      NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (SIZE_UOM)
        INTO vCount
        FROM SIZE_UOM
       WHERE TC_COMPANY_ID = vTCCompanyId AND SIZE_UOM = vSizeUom;

      IF (vCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_SIZE_UOM;
END SAILING_SUB_VALIDATION_PKG;
/



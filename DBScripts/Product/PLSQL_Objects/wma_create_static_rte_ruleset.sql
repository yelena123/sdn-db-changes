create or replace procedure wma_create_static_rte_ruleset
(
    p_tc_company_id            in lema_rule.tc_company_id%type,
    p_identity                 in lema_rule.identity%type,
    p_name                     in lema_rule.name%type,                        
    p_domaingroupid            in lema_rule.domaingroupid%type,
    p_expression               in lema_rule.expression%type,
    p_status                   in lema_rule.status%type,
    p_version                  in lema_rule.version%type
)
as
begin
    insert into lema_ruleset
    (
        tc_company_id, identity, name, description, domaingroupid, expression, 
        status, version, createdby, createddate, modifieddate
    )
    select p_tc_company_id, lema_ruleset_s.nextval, p_name, p_name, 
        p_domaingroupid, p_expression, p_status, p_version, 'WMAutomator', 
        sysdate, sysdate
    from dual
    where not exists
    (
        select 1
        from lema_ruleset lr
        where lr.tc_company_id = p_tc_company_id and lr.name = p_name
            and lr.domaingroupid = p_domaingroupid
    );  

    insert into lema_rule
    (
        tc_company_id, identity, name, description, domaingroupid, expression, 
        status, version, createdby, createddate, modifieddate
    )
    values 
    (
        p_tc_company_id, p_identity, p_name, p_name, p_domaingroupid, 
        p_expression, p_status, p_version, 'WMAutomator', sysdate, sysdate
    );
end;
/
show errors;

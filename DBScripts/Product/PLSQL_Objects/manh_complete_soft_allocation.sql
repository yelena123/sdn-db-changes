create or replace procedure manh_complete_soft_allocation
(
    p_ship_wave_nbr     in order_line_item.ship_wave_nbr%type,
    p_pick_wave_nbr     in order_line_item.wave_nbr%type,
    p_sku_cnstr         in wave_parm.sku_cnstr%type,
    p_user_id           in ucl_user.user_name%type,
    p_preview_wave_flag in number,
    p_repl_wave         in varchar2,
    p_use_locking       in number,
    p_zero              in number,
    p_scale             in number, 
    p_chase_wave        in wave_parm.chase_wave%type
)
as
    v_unselected_status             number(4) := 110;
    v_partially_allocated_status    number(4) := 120;
    v_fully_allocated_status        number(4) := 130;
    v_partially_packed_status       number(4) := 140;
    v_fully_packed_status           number(4) := 150;
    v_shipped_status                number(4) := 190;
    v_cancelled_status              number(4) := 200;
    type ta_line_item_id            is table of order_line_item.line_item_id%type index by binary_integer;
    va_line_item_id                 ta_line_item_id;
begin
    if (p_preview_wave_flag = 1)
    then
        -- these lines are reset later if a convert to wave is kicked off
        v_partially_allocated_status := 115;
        v_fully_allocated_status := 115;
        v_cancelled_status := 200;
        v_partially_packed_status := 115;
        v_fully_packed_status := 115;
        v_shipped_status := 115;
    end if;

    if (p_use_locking = 1)
    then
        -- a cosmetic update to lock rows that have not been changed - this is
        -- necessary since the functionality below is insert and then update;
        -- FTA commits immediately so we optimistically block; if FTA has
        -- touched a row we want, the predicate (alloc qty) will have changed
        merge into order_line_item oli
        using
        (
            select t.order_id, t.line_item_id
            from tmp_wave_selected_orders t
            join orders o on o.order_id = t.order_id
            join order_line_item oli on oli.order_id = t.order_id
                and oli.line_item_id = t.line_item_id
            where abs(oli.allocated_qty - t.allocated_qty) <= p_zero and o.has_import_error = 0
                and abs(oli.order_qty - t.order_qty) <= p_zero
                and oli.do_dtl_status = t.do_dtl_status
        ) iv on (iv.order_id = oli.order_id and iv.line_item_id = oli.line_item_id)
        when matched then
        update set oli.last_updated_dttm = sysdate;

        -- purge any changed rows from cache while unchanged ones are X-locked
        delete from tmp_wave_selected_orders t
        where exists
        (
            select 1
            from order_line_item oli
            join orders o on o.order_id = oli.order_id
            where oli.order_id = t.order_id and oli.line_item_id = t.line_item_id
                and (abs(oli.allocated_qty - t.allocated_qty) > p_zero or o.has_import_error = 1
                    or abs(oli.order_qty - t.order_qty) > p_zero 
                    or oli.do_dtl_status != t.do_dtl_status)
        )
        returning t.line_item_id
        bulk collect into va_line_item_id;

-- todo: group rejections here for item/order
        -- swc's will not be partially allocated or touched by FTA

        forall i in 1..va_line_item_id.count
        delete from tmp_ord_dtl_sku_invn t
        where t.line_item_id = va_line_item_id(i);
    end if;
    
    if p_chase_wave in ('1','2')
    then
       -- if chase_wave is enabled create a new chase line based on the allocable qty
       -- instead of a regular spilt as below.
       manh_chase_line_creation(p_ship_wave_nbr,p_pick_wave_nbr,p_zero,p_user_id);     
    else
       -- insert new line items for all splits
       -- per CV, it is ok to copy the parent grp code/assort nbr onto the
       -- component oli; is_split_line on soft allocation cache is:
       -- 0 = not split; the orig line is allocated
       -- 1 = split; orig line is partially allocated
       -- 2 = split; substituted item
       -- 3 = split; prepack component item
       -- 4 = split; wave shorted qty is represented
       -- 5 = split; external shorted qty is represented
       insert into order_line_item
       (
           order_id, parent_line_item_id, ppack_qty, orig_order_qty, adjusted_order_qty, order_qty,
           allocated_qty, do_dtl_status, created_source, created_dttm, item_id,
           invn_type, prod_stat, cntry_of_orgn, batch_nbr, item_attr_1,
           item_attr_2, item_attr_3, item_attr_4, item_attr_5, ship_wave_nbr,
           wave_nbr, is_hazmat,
           is_stackable, has_errors,
           created_source_type, substituted_parent_line_id, line_item_id,
           tc_order_line_id, actual_cost, actual_cost_currency_code,
           actual_shipped_dttm, allocation_source, allocation_source_id,
           allocation_source_line_id, alloc_line_id, alloc_type,
           assort_nbr, batch_requirement_type, budg_cost, budg_cost_currency_code,
           chute_assign_type, commodity_class,
           commodity_code_id, critcl_dim_1, critcl_dim_2, critcl_dim_3,
           cube_multiple_qty, customer_item,custom_tag, delivery_end_dttm,
           delivery_reference_number, delivery_start_dttm,
           event_code, exp_info_code, ext_sys_line_item_id, fulfillment_type,
           hibernate_version, internal_order_id, internal_order_seq_nbr,
           is_cancelled, is_emergency, item_name,
           last_updated_dttm, last_updated_source, last_updated_source_type,
           line_type, lpn_brk_attrib, lpn_size, lpn_type,
           manufacturing_dttm, master_order_id, merchandizing_department_id,
           merch_grp, merch_type, minor_order_nbr, mo_line_item_id, received_qty,
           shipped_qty, qty_uom_id_base, mv_currency_code,
           mv_size_uom_id, order_consol_attr,
           order_line_id, qty_uom_id, orig_budg_cost, package_type_id, pack_rate,
           pack_zone, pallet_type, partl_fill,
           pickup_end_dttm, pickup_reference_number, pickup_start_dttm,
           pick_locn_assign_type,
           planned_ship_date, ppack_grp_code, price,
           price_tkt_type, priority, product_class_id,
           protection_level_id,
           purchase_order_line_number, reason_code,
           ref_field1, ref_field2, ref_field3, ref_field4, ref_field5, 
           ref_field6, ref_field7, ref_field8, ref_field9, ref_field10, 
           repl_proc_type, repl_wave_nbr, repl_wave_run, retail_price,
           rtl_to_be_distroed_qty, rts_id, rts_line_item_id,
           serial_number_required_flag, shelf_days, single_unit_flag,
           sku_break_attr, sku_gtin, sku_sub_code_id, sku_sub_code_value,
           stack_diameter_standard_uom, stack_diameter_value,
           stack_height_standard_uom, stack_height_value,
           stack_length_standard_uom, stack_length_value, stack_rank,
           stack_width_standard_uom, stack_width_value, std_bundle_qty,
           std_lpn_qty, std_lpn_vol, std_lpn_wt, std_pack_qty, std_pallet_qty,
           std_sub_pack_qty, store_dept, tc_company_id,
           total_monetary_value, unit_cost, unit_monetary_value, unit_price_amount,
           unit_tax_amount, unit_vol, unit_wt, un_number_id, user_canceled_qty,
           vas_process_type, wave_proc_type,
           qty_conv_factor, received_weight,
           planned_weight, shipped_weight, weight_uom_id_base, weight_uom_id,
           planned_volume, received_volume, shipped_volume, volume_uom_id_base,
           volume_uom_id, size1_uom_id, size1_value, received_size1, shipped_size1,
           size2_uom_id, size2_value, received_size2, shipped_size2,
           units_pakd, description,  ext_purchase_order, ref_num1, ref_num2, 
		   ref_num3, ref_num4, freight_revenue_currency_code,ref_num5, purchase_order_number
       )
       select t.order_id, t.line_item_id parent_line_item_id,
           case when t.is_split_line = 3 then t.qty_soft_alloc
               / round(t2.need_qty - t.rng_shortage, p_scale) 
               else oli.ppack_qty end ppack_qty,
		     oli.orig_order_qty orig_order_qty, 
           case when t.is_split_line in (4, 5) then t.rng_shortage 
               else t.qty_soft_alloc end adjusted_order_qty, 
           t.qty_soft_alloc order_qty, t.qty_soft_alloc allocated_qty, 
           case when t.is_split_line in (4, 5) then v_cancelled_status
               else v_fully_allocated_status end do_dtl_status,
           p_user_id created_source, sysdate created_dttm, t.item_id, t.invn_type,
           t.prod_stat, t.cntry_of_orgn, t.batch_nbr, t.item_attr_1, t.item_attr_2,
           t.item_attr_3, t.item_attr_4, t.item_attr_5, decode(t.is_split_line, 5, null, p_ship_wave_nbr),
           decode(t.is_split_line, 5, null, p_pick_wave_nbr) wave_nbr, oli.is_hazmat,
           oli.is_stackable, oli.has_errors,
           oli.created_source_type,
           t.line_item_id substituted_parent_line_id,
           seq_line_item_id.nextval line_item_id,
           to_char(seq_line_item_id.nextval) tc_order_line_id,
           oli.actual_cost, oli.actual_cost_currency_code, oli.actual_shipped_dttm,
           oli.allocation_source, oli.allocation_source_id,
           oli.allocation_source_line_id, oli.alloc_line_id, oli.alloc_type,
           oli.assort_nbr, oli.batch_requirement_type, oli.budg_cost,
           oli.budg_cost_currency_code,
           oli.chute_assign_type, oli.commodity_class, oli.commodity_code_id,
           oli.critcl_dim_1, oli.critcl_dim_2, oli.critcl_dim_3,
           oli.cube_multiple_qty, oli.customer_item, oli.custom_tag,
           oli.delivery_end_dttm, oli.delivery_reference_number,
           oli.delivery_start_dttm, oli.event_code,
           oli.exp_info_code, oli.ext_sys_line_item_id, oli.fulfillment_type,
           oli.hibernate_version, oli.internal_order_id,
           oli.internal_order_seq_nbr,
           case when t.is_split_line in (4, 5) then '1' else '0' end is_cancelled,
           oli.is_emergency,
           ic.item_name, sysdate last_updated_dttm,
           p_user_id last_updated_source, oli.last_updated_source_type,
           oli.line_type, oli.lpn_brk_attrib, oli.lpn_size,
           oli.lpn_type, oli.manufacturing_dttm, oli.master_order_id,
           oli.merchandizing_department_id, oli.merch_grp, oli.merch_type,
           oli.minor_order_nbr, oli.mo_line_item_id, 0 received_qty,
           0 shipped_qty, oli.qty_uom_id_base,
           oli.mv_currency_code, oli.mv_size_uom_id,
           oli.order_consol_attr, oli.order_line_id,
           oli.qty_uom_id, oli.orig_budg_cost, oli.package_type_id, oli.pack_rate,
           oli.pack_zone, oli.pallet_type, oli.partl_fill,
           oli.pickup_end_dttm, oli.pickup_reference_number,
           oli.pickup_start_dttm, oli.pick_locn_assign_type,
           oli.planned_ship_date, oli.ppack_grp_code,
           oli.price, oli.price_tkt_type, oli.priority, oli.product_class_id,
           oli.protection_level_id,
           oli.purchase_order_line_number,
           case t.is_split_line when 4 then '1' when 5 then '55' else '45' end reason_code,
           oli.ref_field1, oli.ref_field2, oli.ref_field3, oli.ref_field4, 
           oli.ref_field5, oli.ref_field6, oli.ref_field7, oli.ref_field8, 
           oli.ref_field9, oli.ref_field10, oli.repl_proc_type, 
           case when p_repl_wave = '1' and t.is_split_line != 4
               then p_pick_wave_nbr else null end repl_wave_nbr, oli.repl_wave_run,
           oli.retail_price, oli.rtl_to_be_distroed_qty,
           oli.rts_id, oli.rts_line_item_id,
           oli.serial_number_required_flag, oli.shelf_days, oli.single_unit_flag,
           oli.sku_break_attr, oli.sku_gtin, oli.sku_sub_code_id,
           oli.sku_sub_code_value, oli.stack_diameter_standard_uom,
           oli.stack_diameter_value, oli.stack_height_standard_uom,
           oli.stack_height_value, oli.stack_length_standard_uom,
           oli.stack_length_value, oli.stack_rank, oli.stack_width_standard_uom,
           oli.stack_width_value,
           case when t.is_split_line = 1 then oli.std_bundle_qty else 0 end,
           case when t.is_split_line = 1 then oli.std_lpn_qty else 0 end,
           case when t.is_split_line = 1 then oli.std_lpn_vol else 0 end,
           case when t.is_split_line = 1 then oli.std_lpn_wt else 0 end,
           case when t.is_split_line = 1 then oli.std_pack_qty else 0 end,
           case when t.is_split_line = 1 then oli.std_pallet_qty else 0 end,
           case when t.is_split_line = 1 then oli.std_sub_pack_qty else 0 end,
           oli.store_dept, oli.tc_company_id,
           oli.total_monetary_value, oli.unit_cost, oli.unit_monetary_value,
           oli.unit_price_amount, oli.unit_tax_amount, oli.unit_vol, oli.unit_wt,
           oli.un_number_id, 0 user_canceled_qty, oli.vas_process_type,
           oli.wave_proc_type,
           oli.qty_conv_factor,
           oli.received_weight,
           case when oli.weight_uom_id_base is null then null
               when t.is_split_line = 1
                   then oli.planned_weight * t.qty_soft_alloc/oli.order_qty
               else ic.unit_weight * t.qty_soft_alloc end planned_weight,
           oli.shipped_weight, oli.weight_uom_id_base, oli.weight_uom_id,
           case when oli.volume_uom_id_base is null then null
               when t.is_split_line = 1
                   then oli.planned_volume * t.qty_soft_alloc/oli.order_qty
               else ic.unit_volume * t.qty_soft_alloc end planned_volume,
           oli.received_volume, oli.shipped_volume,
           oli.volume_uom_id_base, oli.volume_uom_id, oli.size1_uom_id,
           oli.size1_value * t.qty_soft_alloc/oli.order_qty size1_value,
           oli.received_size1, oli.shipped_size1, oli.size2_uom_id,
           oli.size2_value * t.qty_soft_alloc/oli.order_qty size2_value,
           oli.received_size2, oli.shipped_size2,
           0 units_pakd, ic.description, oli.ext_purchase_order, oli.ref_num1, oli.ref_num2, 
		   oli.ref_num3, oli.ref_num4, oli.freight_revenue_currency_code,oli.ref_num5, oli.purchase_order_number
       from 
       (
        select t.line_item_id, t.item_id, t.invn_type, t.prod_stat, 
            t.cntry_of_orgn, t.batch_nbr, t.item_attr_1, t.item_attr_2, 
            t.item_attr_3, t.item_attr_4, t.item_attr_5, t.qty_soft_alloc, 
            t.order_id, t.rng_shortage, t.is_flushed, t.is_split_line
        from tmp_ord_dtl_sku_invn t
        union all
        select oli.line_item_id, oli.item_id, oli.invn_type, oli.prod_stat, 
            oli.cntry_of_orgn, oli.batch_nbr, oli.item_attr_1, oli.item_attr_2, 
            oli.item_attr_3, oli.item_attr_4, oli.item_attr_5, 0, oli.order_id, 
            oli.adjusted_order_qty - oli.order_qty, 0, 5
        from tmp_wave_selected_orders t
        join order_line_item oli on oli.order_id = t.order_id
            and oli.line_item_id = t.line_item_id
        where oli.adjusted_order_qty - oli.order_qty > 0
       )t
       join order_line_item oli on oli.order_id = t.order_id
           and oli.line_item_id = t.line_item_id
       join tmp_wave_selected_orders t2 on t2.order_id = t.order_id
           and t2.line_item_id = t.line_item_id
       join item_cbo ic on ic.item_id = t.item_id
       where t.is_split_line > 0 and t.is_flushed = 0;
       wm_cs_log('New OLI''s inserted ' || sql%rowcount, p_sql_log_level => 1);

       -- coalesce(sum(case when t.is_split_line > 0 then 0 else t.qty_soft_alloc
       -- end) = qty of the orig line item that was allocated before substitutions
       -- min(t.rng_shortage) = portion of the orig need that was not allocated
       -- after all substitution attempts were exhausted
       merge into order_line_item oli
       using
       (
           select t1.order_id order_id, t1.line_item_id line_item_id,
               coalesce(min(t2.rng_shortage), t1.need_qty) shortage,
               coalesce(sum(case when t2.is_split_line > 0 then 0
                   else t2.qty_soft_alloc end), 0) qty_alloc_orig_line,
               min(t1.order_qty - t1.allocated_qty - t1.need_qty) unwaved_qty
           from tmp_wave_selected_orders t1
           left join tmp_ord_dtl_sku_invn t2 on t2.order_id = t1.order_id
               and t2.line_item_id = t1.line_item_id and t2.is_flushed = 0
           group by t1.order_id, t1.line_item_id, t1.need_qty
       ) iv on (iv.order_id = oli.order_id and iv.line_item_id = oli.line_item_id)
       when matched then
       update set oli.last_updated_source = p_user_id, 
           oli.last_updated_dttm = sysdate,
           oli.ship_wave_nbr =
           (
               case
               when oli.do_dtl_status = v_partially_allocated_status
                   then oli.ship_wave_nbr
               else p_ship_wave_nbr
               end
           ),
           oli.wave_nbr =
           (
               case
               when p_sku_cnstr = '0'
                   and iv.unwaved_qty + iv.shortage > p_zero
                   and iv.qty_alloc_orig_line + oli.allocated_qty <= p_zero
                   then oli.wave_nbr
               when oli.do_dtl_status = v_partially_allocated_status
                   then oli.wave_nbr
               else p_pick_wave_nbr
               end
           ),
           -- orig is not reduced for full shorts
           oli.adjusted_order_qty = round(iv.qty_alloc_orig_line + oli.allocated_qty
               + iv.unwaved_qty 
               + (case when p_sku_cnstr = '0' or (p_sku_cnstr = '2' 
                   and oli.do_dtl_status = 110 and iv.shortage = oli.order_qty)
                   then iv.shortage else 0 end), p_scale),
           oli.order_qty = round(
           (
               case
               when p_sku_cnstr = '0'
                   then iv.qty_alloc_orig_line + oli.allocated_qty + iv.unwaved_qty
                       + iv.shortage
               else iv.qty_alloc_orig_line + oli.allocated_qty + iv.unwaved_qty
               end
           ), p_scale),
           oli.do_dtl_status =
           (
               case
               when p_sku_cnstr = '0' then
                   case
                   -- no shortage
                   when iv.unwaved_qty + iv.shortage <= p_zero then
                       case
                       -- some orig sku allocation in now or earlier wave
                       when iv.qty_alloc_orig_line + oli.allocated_qty > p_zero then
                           case
                           when iv.qty_alloc_orig_line + oli.allocated_qty
                               - coalesce(oli.shipped_qty, 0) <= p_zero
                               then v_shipped_status
                           when iv.qty_alloc_orig_line + oli.allocated_qty
                               - coalesce(oli.units_pakd, 0) <= p_zero
                               then v_fully_packed_status
                           when coalesce(oli.units_pakd, 0) <= p_zero
                               then v_fully_allocated_status
                           else v_partially_packed_status
                           end
                       else v_cancelled_status
                       end
                   -- some shortage remains on the line
                   else
                       case
                           when iv.qty_alloc_orig_line + oli.allocated_qty > p_zero
                               then v_partially_allocated_status
                           else v_unselected_status
                       end
                   end
               -- sku cnstr is 1/2/3; this means that allocated qty = need qty *in this wave*
               else
                   case
                   -- some orig sku allocation in this and/or a prior wave
                   when iv.qty_alloc_orig_line + oli.allocated_qty > p_zero then
                       case when iv.unwaved_qty = 0 then
                           case
                               -- only when no orig sku alloc, with the remaining need in this wave *fully* going to a split line via subs/ppack
                               when iv.qty_alloc_orig_line + oli.allocated_qty
                                   - coalesce(oli.shipped_qty, 0) <= p_zero
                                   then v_shipped_status
                               when iv.qty_alloc_orig_line + oli.allocated_qty
                                   - coalesce(oli.units_pakd, 0)
                                   - coalesce(oli.user_canceled_qty, 0) <= p_zero
                                   then v_fully_packed_status
                               when coalesce(oli.units_pakd, 0) <= p_zero
                                   then v_fully_allocated_status
                               else v_partially_packed_status
                           end
                       else v_partially_allocated_status
                       end
                   else
                       case
                           when iv.unwaved_qty <= p_zero then v_cancelled_status
                           else v_unselected_status
                       end
                   end
               end
           ),
           oli.allocated_qty = round(iv.qty_alloc_orig_line + oli.allocated_qty,
               p_scale),
           oli.reason_code = case when iv.qty_alloc_orig_line + oli.allocated_qty
               <= p_zero then oli.reason_code else '45' end,
           oli.repl_wave_nbr =
           (
               case
               when p_repl_wave = '1' then
                   case
                   when p_sku_cnstr = '0'
                       and iv.unwaved_qty + iv.shortage > p_zero
                       and iv.qty_alloc_orig_line + oli.allocated_qty <= p_zero
                       then oli.repl_wave_nbr
                   when oli.do_dtl_status = v_partially_allocated_status
                       then oli.repl_wave_nbr
                   else p_pick_wave_nbr
                   end
               else oli.repl_wave_nbr
               end
           ),
           -- parentheses basically is the new ord qty
           oli.size1_value = (oli.size1_value/oli.order_qty) *
           (
               case
               when p_sku_cnstr = '0'
                   then iv.qty_alloc_orig_line + oli.allocated_qty + iv.unwaved_qty
                       + iv.shortage
               else iv.qty_alloc_orig_line + oli.allocated_qty + iv.unwaved_qty
               end            
           ),
           oli.size2_value = (oli.size2_value/oli.order_qty) *
           (
               case
               when p_sku_cnstr = '0'
                   then iv.qty_alloc_orig_line + oli.allocated_qty + iv.unwaved_qty
                       + iv.shortage
               else iv.qty_alloc_orig_line + oli.allocated_qty + iv.unwaved_qty
               end
           ),
           oli.planned_weight = (oli.planned_weight/oli.order_qty) *
           (
               case
               when p_sku_cnstr = '0'
                   then iv.qty_alloc_orig_line + oli.allocated_qty + iv.unwaved_qty
                       + iv.shortage
               else iv.qty_alloc_orig_line + oli.allocated_qty + iv.unwaved_qty
               end
           ),
           oli.planned_volume = (oli.planned_volume/oli.order_qty) *
           (
               case
               when p_sku_cnstr = '0'
                   then iv.qty_alloc_orig_line + oli.allocated_qty + iv.unwaved_qty
                       + iv.shortage
               else iv.qty_alloc_orig_line + oli.allocated_qty + iv.unwaved_qty
               end
           ),
           oli.is_cancelled = case when
           (
               case
               when p_sku_cnstr = '0'
                   then iv.qty_alloc_orig_line + oli.allocated_qty + iv.unwaved_qty
                       + iv.shortage
               else iv.qty_alloc_orig_line + oli.allocated_qty + iv.unwaved_qty
               end
           ) = 0 then '1' else '0' end;
       wm_cs_log('OLI''s updated ' || sql%rowcount, p_sql_log_level => 1);

       -- mark these soft allocations as having been taken care of
       update tmp_ord_dtl_sku_invn t
       set t.is_flushed = 1
       where t.is_flushed = 0;
       wm_cs_log('SA flushed for rule ' || sql%rowcount, p_sql_log_level => 2);
   end if;   
end;
/
show errors;
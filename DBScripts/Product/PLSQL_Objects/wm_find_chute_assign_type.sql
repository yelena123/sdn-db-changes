create or replace procedure wm_find_chute_assign_type
(
    p_whse                   in whse_master.whse%type,
    p_dflt_chute_assign_type in pack_wave_parm_hdr.chute_assign_type%type,
    p_force_cat_flag         in pack_wave_parm_hdr.force_cat_flag%type,
    p_cat_event_id           in wave_parm.chute_assign_type_event_id%type
 )
as
    v_cat_rule               varchar2(1000) := ' ';
    v_update_sql             varchar2(18000);
begin
    if (p_force_cat_flag in ('1', 'Y') and p_dflt_chute_assign_type is not null)
    then
        update tmp_pack_wave_selected_lpns t
        set t.chute_assign_type = p_dflt_chute_assign_type;
        wm_cs_log('Forced CAT on lpns ' || sql%rowcount);
    else
        -- base CAT logic; look in oli first, item_wms next and then for
        -- Rule Based assignment, default to the pwph value
        update tmp_pack_wave_selected_lpns t
        set t.chute_assign_type = coalesce(
        (
            select min(oli.chute_assign_type) 
                keep(dense_rank first order by oli.line_item_id)
            from lpn l
            join lpn_detail ld on ld.lpn_id = l.lpn_id
            join order_line_item oli on oli.order_id = l.order_id
                and oli.line_item_id = ld.distribution_order_dtl_id
            where l.lpn_id = t.lpn_id and oli.chute_assign_type is not null
        ),
        (
            select min(ifmw.chute_assign_type) 
                keep(dense_rank first order by ld.lpn_detail_id)
            from lpn_detail ld
            join item_facility_mapping_wms ifmw on ifmw.item_id = ld.item_id
            where ld.lpn_id = t.lpn_id and ifmw.chute_assign_type is not null
        ));
        wm_cs_log('order line or facility based CAT for lpns ' || sql%rowcount);
        
        --- Rule Based logic For Chute Assignment Type
        If (p_cat_event_id is not null)
        then
		        for rule_list in
            (    
                 select rh.rule_type rule_type , rh.rule_id rule_id,
                     erp.chute_assign_type cat
                 from rule_hdr rh
                 join event_rule_parm erp on erp.rule_id = rh.rule_id
                 join rule_sel_dtl rsd on rsd.rule_id = erp.rule_id
                 join event_parm ep on ep.event_parm_id = erp.event_parm_id
                 where  ep.event_type ='CAT'  and rh.stat_code = 0
                     and ep.event_parm_id = p_cat_event_id
                 order by erp.rule_prty, rh.rule_id
             )
            loop
            select manh_get_rule(rule_list.rule_id, 0, rule_list.rule_type)
            into v_cat_rule
            from dual;
            
            if (v_cat_rule is null)
            then
                return;
            end if;
            
            v_update_sql:=  
                ' update tmp_pack_wave_selected_lpns t '
                || ' set t.chute_assign_type = :cat '
                || ' outer_clause t.chute_assign_type is null '
                || '     and exists '    
                || '     ( '
                || '         select 1 '
                || '         from lpn '
                || '         join lpn_detail on lpn_detail.lpn_id = lpn.lpn_id '
                || '         where lpn.lpn_id = t.lpn_id '
                || '             and '|| v_cat_rule || ' )';
               
            wm_add_joins_for_cat_assign(v_update_sql, p_whse);
                
            v_update_sql := replace(v_update_sql, 'outer_clause', 'where');
                      
            execute immediate v_update_sql using rule_list.cat;
            wm_cs_log('Rule based CAT for lpns ' || sql%rowcount);
   
            end loop;
        end if;
    end if;
    --Default CAT Assignment for LPNS
    update tmp_pack_wave_selected_lpns t
    set t.chute_assign_type = p_dflt_chute_assign_type
    where t.chute_assign_type is null;
    wm_cs_log('Default CAT for lpns ' || sql%rowcount);
    
    -- do not attempt chute assignment for orders for which at least one lpn
    -- a) did not get a CAT or b) has chutes no chutes configured; these orders
    -- are eventually deselected by C++
    delete from tmp_pack_wave_selected_lpns t1
    where exists
    (
        select 1
        from tmp_pack_wave_selected_lpns t2
        where t2.order_id = t1.order_id
            and 
            (
                t2.chute_assign_type is null
                or not exists
                (
                    select 1
                    from chute_master cm
                    join chute_assign_prty cap on cap.chute_type = cm.chute_type
                        and cap.whse = cm.whse
                    where cap.whse = p_whse
                        and cap.chute_assign_type = t2.chute_assign_type
                        and cm.stat_code = 0 and coalesce(nullif(cm.max_wt,0),999999999) >= t2.wt
                        and coalesce(nullif(cm.max_vol,0),999999999) >= t2.vol 
                        and coalesce(nullif(cm.max_units,0),999999999) >= t2.units
                        and coalesce(nullif(cm.max_carton,0),9999999) > 0 -- and cm.max_sams >= t2.sams
                )
            )
    );
    wm_cs_log('Dropped Lpns ' || sql%rowcount);
end;
/

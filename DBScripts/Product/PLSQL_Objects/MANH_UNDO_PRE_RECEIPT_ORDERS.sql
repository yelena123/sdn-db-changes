create or replace procedure manh_undo_pre_receipt_orders 
(
    p_whse            in varchar2,
    p_cd_master_id    in number,
    p_asn_id          in varchar2,
    p_login_user_id   in varchar2,
    p_debug_flag      in number
)
is
    v_curr_date_time   date := sysdate;
    v_error_details    varchar2 (2000);
    v_chase_enabled    number := 0;
    v_prev_module_name    wm_utils.t_app_context_data;
    v_prev_action_name    wm_utils.t_app_context_data;	

    -- get all the split's available for original orders.
    -- qty check is not required in case of splited lines because the whole line was allocated to the asn the entire asn is going down.
    cursor split_agg_lines is
        select oli.order_id, oli.line_item_id
        from tmp_undo_initiate_order_dtls tmp, order_line_item oli
        where tmp.substituted_parent_line_id is not null
            and tmp.substituted_parent_line_id <> tmp.line_item_id
            and tmp.line_item_id = oli.line_item_id
            and tmp.order_id = oli.order_id    -- and oli.allocated_qty <= 0;
        union
        -- get all the split's available for aggregated orders.
        select oli.order_id, oli.line_item_id
        from tmp_undo_initiate_order_dtls tmp, order_line_item oli
        where tmp.reference_line_item_id = oli.line_item_id
            and tmp.reference_order_id = oli.order_id
            and tmp.reference_line_item_id is not null
            and oli.order_qty <= 0;
begin

    wm_utils.get_context_info(v_prev_module_name, v_prev_action_name);
    wm_utils.set_context_info(p_module_name => 'THD', p_action_name => 'UNDO_PRE_RECEIPT_ORDERS',
        p_client_id => p_asn_id);
		
    insert into tmp_undo_initiate_order_dtls 
    (
        allocated_qty,order_id, line_item_id, reference_order_id,
        reference_line_item_id, substituted_parent_line_id
    )
    select oli.allocated_qty, oli.order_id, oli.line_item_id, 
        oli.reference_order_id, oli.reference_line_item_id,
        oli.substituted_parent_line_id
    from order_line_item oli, orders o
    where o.is_original_order = 1 and o.order_id = oli.order_id
        and oli.allocation_source_id = p_asn_id
        and oli.fulfillment_type in ('2', '3')
        and do_dtl_status = 130;
    wm_log ('tmp_undo_initiate_order_dtls insert : ' || sql%rowcount, p_debug_flag);

    -- update the aggregates record's allocated_qty
    merge into order_line_item oli
    using 
    (  
        select sum (allocated_qty) as to_be_decremented_qty,
            reference_order_id as agg_order_id, 
            reference_line_item_id as agg_line_item_id
        from tmp_undo_initiate_order_dtls tmp
        where reference_line_item_id is not null
        group by tmp.reference_order_id, tmp.reference_line_item_id
        order by reference_order_id, reference_line_item_id
    ) tmp_oli_fetched on (tmp_oli_fetched.agg_order_id = oli.order_id
        and tmp_oli_fetched.agg_line_item_id = oli.line_item_id)
    when matched then
    update set
        oli.order_qty = oli.order_qty - tmp_oli_fetched.to_be_decremented_qty,
        oli.allocated_qty =
            oli.allocated_qty - tmp_oli_fetched.to_be_decremented_qty,
        oli.is_cancelled =
            (case
                when ((coalesce (oli.order_qty, 0)
                    - coalesce (tmp_oli_fetched.to_be_decremented_qty, 0)) <= 0)
                then '1' else oli.is_cancelled
             end),
        oli.last_updated_dttm = v_curr_date_time,
        oli.last_updated_source = p_login_user_id,
        oli.do_dtl_status =
            (case
                when ((coalesce (oli.order_qty, 0)
                    - coalesce (tmp_oli_fetched.to_be_decremented_qty, 0)) <= 0)
                then 200
                when ((coalesce (oli.allocated_qty, 0)
                    - coalesce (tmp_oli_fetched.to_be_decremented_qty, 0)) <= 0)
                then 110
                when ((coalesce (oli.order_qty, 0)
                    - coalesce (oli.allocated_qty, 0)) >= 0)
                then 120
                when ((coalesce (oli.order_qty, 0)
                    - coalesce (oli.allocated_qty, 0)) < 0)
                then 130
                else oli.do_dtl_status
            end);
    wm_log ('aggregate order_line_item allocated_qty update : ' || sql%rowcount,
        p_debug_flag);

    -- update the original line record's allocated_qty
    merge into order_line_item oli
    using 
    (  
        select sum (allocated_qty) as to_be_decremented_qty,
            order_id as orig_order_id, line_item_id as orig_line_item_id
        from tmp_undo_initiate_order_dtls tmp
        group by tmp.order_id, tmp.line_item_id
        order by order_id, line_item_id
    ) tmp_oli_fetched on (tmp_oli_fetched.orig_order_id = oli.order_id
        and tmp_oli_fetched.orig_line_item_id = oli.line_item_id)
    when matched then
    update set
        oli.allocated_qty = oli.allocated_qty - tmp_oli_fetched.to_be_decremented_qty,
        oli.allocation_source =
            (case
                when (oli.allocated_qty - tmp_oli_fetched.to_be_decremented_qty) > 0
                then oli.allocation_source
                else 10
            end),
        oli.allocation_source_id =
            (case
                when (oli.allocated_qty - tmp_oli_fetched.to_be_decremented_qty) > 0
                then oli.allocation_source_id
                else null
            end),
        oli.allocation_source_line_id =
            (case
                when (oli.allocated_qty - tmp_oli_fetched.to_be_decremented_qty) > 0
                then oli.allocation_source_line_id
                else null
            end),
        oli.pre_receipt_status =
            (case
                when (oli.allocated_qty - tmp_oli_fetched.to_be_decremented_qty) > 0
                then oli.pre_receipt_status
                else null
            end),
        oli.reference_order_id = null,
        oli.reference_line_item_id = null,
        oli.last_updated_dttm = v_curr_date_time,
        oli.last_updated_source = p_login_user_id,
        oli.do_dtl_status =
            (case
                when (coalesce (oli.order_qty, 0) <= 0)
                then 200
                when ((coalesce (oli.allocated_qty, 0)
                    - coalesce (tmp_oli_fetched.to_be_decremented_qty, 0)) <= 0)
                then 110
                when (( coalesce (oli.order_qty, 0)
                    - coalesce (oli.allocated_qty, 0)
                    - coalesce (tmp_oli_fetched.to_be_decremented_qty, 0)) >= 0)
                then 120
                else oli.do_dtl_status
            end);

    wm_log ('original order_line_item qty, status update : ' || sql%rowcount,
        p_debug_flag);

    -- update the original lines from which split's has occured.
    merge into order_line_item oli
    using 
    (  
        select sum (allocated_qty) as to_be_decremented_qty,
            order_id as orig_order_id,
            substituted_parent_line_id as sub_orig_line_item_id
        from tmp_undo_initiate_order_dtls tmp
        where tmp.substituted_parent_line_id is not null
            and tmp.substituted_parent_line_id <> tmp.line_item_id
        group by tmp.order_id, tmp.substituted_parent_line_id
    ) tmp_oli_fetched on (tmp_oli_fetched.orig_order_id = oli.order_id
        and tmp_oli_fetched.sub_orig_line_item_id = oli.line_item_id)
    when matched then
    update set
        oli.order_qty = oli.order_qty + tmp_oli_fetched.to_be_decremented_qty,
        oli.adjusted_order_qty =
            oli.adjusted_order_qty + tmp_oli_fetched.to_be_decremented_qty,
         -- in case this line was completely shorted resulting in cancelation, then revive this line. also, this line cannot goto cancelled because the order_qty was increased.
        oli.is_cancelled = 0,
        oli.allocation_source =
           (case
                when (oli.allocated_qty) > 0 then oli.allocation_source
                else 10
            end),
        oli.allocation_source_id =
            (case
                when (oli.allocated_qty) > 0 then oli.allocation_source_id
                else null
             end),
        oli.allocation_source_line_id =
            (case
                when (oli.allocated_qty) > 0
                then oli.allocation_source_line_id
                else null
             end),
        oli.pre_receipt_status =
            (case
                when (oli.allocated_qty) > 0
                then oli.pre_receipt_status
                else null
             end),
        oli.last_updated_dttm = v_curr_date_time,
        oli.last_updated_source = p_login_user_id,
        oli.do_dtl_status =
            (case
                when (coalesce (oli.order_qty, 0)
                    + coalesce (tmp_oli_fetched.to_be_decremented_qty, 0) <= 0)
                then 200
                when ((coalesce (oli.allocated_qty, 0)) <= 0)
                then 110
                when ((  coalesce (oli.order_qty, 0)
                    + coalesce (tmp_oli_fetched.to_be_decremented_qty, 0)
                    - coalesce (oli.allocated_qty, 0)) >= 0)
                then 120
                else oli.do_dtl_status
             end);

    wm_log ( 'substituted order_line_item qty, status update : ' || sql%rowcount,
        p_debug_flag);

   merge into pkt_dtl pkt_dtls
   using 
   (  
       -- update pick ticket detail for aggregated orders.
       select sum (allocated_qty) as to_be_decremented_qty,
           reference_order_id as order_id,
           reference_line_item_id as line_item_id
       from tmp_undo_initiate_order_dtls tmp
       where reference_line_item_id is not null
       group by tmp.reference_order_id, tmp.reference_line_item_id
       union
       -- update pick ticket detail for original orders.
       select sum (allocated_qty) as to_be_decremented_qty,
           order_id as order_id,
           line_item_id as line_item_id
       from tmp_undo_initiate_order_dtls tmp
       where reference_line_item_id is null
       group by tmp.order_id, tmp.line_item_id
    ) tmp_oli_fetched on (tmp_oli_fetched.order_id = pkt_dtls.reference_order_id
        and tmp_oli_fetched.line_item_id = pkt_dtls.reference_line_item_id)
    when matched then
    update set
        pkt_dtls.pkt_qty = pkt_dtls.pkt_qty - tmp_oli_fetched.to_be_decremented_qty,
        pkt_dtls.mod_date_time = v_curr_date_time,
        pkt_dtls.stat_code =
            (case
                when (pkt_dtls.pkt_qty - tmp_oli_fetched.to_be_decremented_qty) <= 0
                then 99
                else pkt_dtls.stat_code
             end)
    delete where pkt_dtls.stat_code = 99;
    wm_log ('pkt_dtl qty, status update : ' || sql%rowcount, p_debug_flag);

    -- aggregated / original split updates
    for fetched_split_line in split_agg_lines
    loop
        -- delete the order split line item records
        delete from order_split_line_item split_oli
        where split_oli.order_id = fetched_split_line.order_id
            and split_oli.line_item_id = fetched_split_line.line_item_id;
        wm_log ( 'aggregated / original split order_split_line_item updates : '
            || sql%rowcount, p_debug_flag);

        -- delete the order_split_size
        delete from order_split_size split_oli
        where split_oli.order_id = fetched_split_line.order_id
            and split_oli.line_item_id = fetched_split_line.line_item_id;
        wm_log ( 'aggregated / original split order_split_size updates : '
            || sql%rowcount, p_debug_flag);

        -- delete the order_split_size
        delete from order_line_item_size split_oli
        where split_oli.order_id = fetched_split_line.order_id
            and split_oli.line_item_id = fetched_split_line.line_item_id;

        wm_log ( 'aggregated / original split order_line_item_size updates : '
            || sql%rowcount, p_debug_flag);

        -- delete the order_line_item
        delete from order_line_item split_oli
        where split_oli.order_id = fetched_split_line.order_id
            and split_oli.line_item_id = fetched_split_line.line_item_id;

        wm_log ( 'aggregated / original split order_line_item updates : '
            || sql%rowcount, p_debug_flag);
    end loop;

    --send undo allocation pixes
    manh_undo_initiate_oli_pix(p_whse, p_cd_master_id, p_login_user_id);
	
    wm_utils.set_context_info(p_module_name => v_prev_module_name,
        p_action_name => v_prev_action_name);
		
exception
   when others then
   rollback;
   v_error_details := substr (to_char (sqlcode) || '<--->' || sqlerrm, 1, 2000);
   wm_log ('Error ---> ' || v_error_details, 1);
   raise;
-- commit ;

end manh_undo_pre_receipt_orders;
/

show errors;
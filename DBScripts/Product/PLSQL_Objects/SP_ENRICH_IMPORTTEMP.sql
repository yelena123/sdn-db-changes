create or replace PROCEDURE SP_ENRICH_IMPORTTEMP (v_whse_code IN NVARCHAR2) as
BEGIN

merge into import_temp it
using
(
    select it.row_num, ifs.item_facility_mapping_id, plh.pick_locn_hdr_id, si.slotitem_id, td.parent_node_id
    from import_temp it
    left outer join item_facility_slotting ifs on ifs.item_name = it.sku_name and ifs.whse_code = it.whse_code
    left outer join locn_hdr lh on lh.whse = it.whse_code and lh.dsp_locn = it.dsp_slot
    left outer join pick_locn_hdr plh on lh.locn_hdr_id = plh.locn_hdr_id
    left outer join pick_locn_hdr_slotting plhs on plh.pick_locn_hdr_id = plhs.pick_locn_hdr_id
    left outer join slot_item si on ifs.item_facility_mapping_id = si.sku_id and si.slot_id = plh.pick_locn_hdr_id and it.ship_unit_id = si.ship_unit
    left outer join tree_dtl td on td.node_id = plhs.my_range
    where it.whse_code = v_whse_code and it.sku_id is null
) iv on (iv.row_num = it.row_num)
when matched then
update set it.sku_id = iv.item_facility_mapping_id, it.slot_id = iv.pick_locn_hdr_id, it.slotitem_id = iv.slotitem_id, it.parent_node_id = iv.parent_node_id;

END;
/
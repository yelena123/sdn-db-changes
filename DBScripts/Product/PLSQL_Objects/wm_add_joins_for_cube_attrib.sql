create or replace procedure wm_add_joins_for_cube_attrib
(
    p_facility_id    in  facility.facility_id%type,
    p_rule_sql       in out varchar2
)
as
begin
    if (instr(p_rule_sql, 'ORDERS.', 1, 1) > 0 or instr(p_rule_sql, 'ORDER_NOTE.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join orders on orders.tc_order_id = alloc_invn_dtl.tc_order_id '
            || ' and orders.tc_company_id = alloc_invn_dtl.cd_master_id where ');
    end if;
    
    if (instr(p_rule_sql, 'ORDER_LINE_ITEM.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join order_line_item on order_line_item.line_item_id = alloc_invn_dtl.line_item_id where ');
    end if;

    if (instr(p_rule_sql, 'ORDER_NOTE.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            'join order_note on order_note.order_id = orders.order_id '
            || ' and (order_note.line_item_id = alloc_invn_dtl.line_item_id or order_note.line_item_id = 0 ) '
            || ' where ');
    end if;

    if (instr(p_rule_sql, 'ITEM_CBO.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join item_cbo on item_cbo.item_id = alloc_invn_dtl.item_id where ');
    end if;

    if (instr(p_rule_sql, 'ITEM_WMS.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            'join item_wms on item_wms.item_id = alloc_invn_dtl.item_id where ');
    end if;

    if (instr(p_rule_sql, 'ITEM_FACILITY_MAPPING_WMS.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join item_facility_mapping_wms on item_facility_mapping_wms.item_id '
            || ' = alloc_invn_dtl.item_id and item_facility_mapping_wms.facility_id = '
            || to_char(p_facility_id) ||' where ');
    end if;
    
    if (instr(p_rule_sql, 'ALIAS_IBPALLET.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join lpn plt on plt.tc_lpn_id = alloc_invn_dtl.cntr_nbr and plt.tc_company_id = alloc_invn_dtl.cd_master_id '
            || ' and plt.inbound_outbound_indicator = ''I'' and plt.c_facility_id = ' || to_char(p_facility_id)
            || ' and plt.lpn_type = 2 where ');
        p_rule_sql := replace(p_rule_sql, 'ALIAS_IBPALLET', 'plt');
    end if;

    if (instr(p_rule_sql, 'ALIAS_DLH.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join locn_hdr dlh on dlh.locn_id = alloc_invn_dtl.dest_locn_id where ');
        p_rule_sql := replace(p_rule_sql, 'ALIAS_DLH', 'dlh');
    end if;

    if (instr(p_rule_sql, 'LPN.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join lpn on lpn.tc_lpn_id = alloc_invn_dtl.cntr_nbr and lpn.tc_company_id = alloc_invn_dtl.cd_master_id '
            || ' and lpn.inbound_outbound_indicator = ''I'' and lpn.c_facility_id = ' || to_char(p_facility_id)
            || ' and lpn.lpn_type = 1 where ');
    end if;

    if (instr(p_rule_sql, 'LOCN_HDR.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join locn_hdr on locn_hdr.locn_id = alloc_invn_dtl.pull_locn_id ' 
            || ' where ');
    end if;

    if (instr(p_rule_sql, 'RESV_LOCN_HDR.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join resv_locn_hdr on resv_locn_hdr.locn_id = alloc_invn_dtl.pull_locn_id '
            || ' where ');
    end if;

    if (instr(p_rule_sql, 'LOCN_GRP.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' left join locn_grp on ((locn_grp.locn_id '
            || '    = alloc_invn_dtl.pull_locn_id and locn_grp.grp_type != 51) '
            || ' or (locn_grp.locn_id = alloc_invn_dtl.dest_locn_id '
            || ' and locn_grp.grp_type = 51 )) '
            || ' where ');
    end if;
    
    if instr(p_rule_sql, 'PICK_LOCN_HDR.', 1, 1) > 0
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join pick_locn_hdr on pick_locn_hdr.locn_id = alloc_invn_dtl.pull_locn_id '
            || ' where ');
    end if;

    if instr(p_rule_sql, 'PICK_LOCN_DTL.', 1, 1) > 0
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join pick_locn_dtl on pick_locn_dtl.locn_id = alloc_invn_dtl.pull_locn_id '
            || ' and pick_locn_dtl.item_id = alloc_invn_dtl.item_id where ');
    end if;    
end;
/
show errors;

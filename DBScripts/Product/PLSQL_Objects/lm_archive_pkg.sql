CREATE OR REPLACE PACKAGE lm_archive_pkg
AS
----------------------------------------------------------------------
-- PROCEDURE called by WM UI
-- (p_purge_config_ids => 'purge_config_id1,..,purge_config_idN'
----------------------------------------------------------------------
--   PROCEDURE ui_purge (p_purge_config_ids VARCHAR2);
   FUNCTION purge_dtl_conditon
   (p_cond_num         IN   varchar2 ,
    p_days             IN   purge_config.nbr_days_old%type)
   RETURN varchar2; 

   PROCEDURE LM_PURGE_SQLS_BUILD
   (  p_purge_id        IN   purge_config.purge_code%TYPE, --Instead will be using Purge code(purge_config.purge_code)
      p_days            IN   purge_config.nbr_days_old%type
   );

   PROCEDURE exec_purge (p_company_id      IN purge_config.cd_master_id%type
                         ,p_whse_id          IN purge_config.whse_master_id%type
                         ,p_commit_size      IN purge_config.nbr_rows_to_commit%type
                         ,p_days             IN purge_config.nbr_days_old%type
                         ,p_purge_cd         IN purge_config.purge_code%type
                         ,p_archive_flag     IN purge_config.archive_flag%type
                         ,p_child_comp_flag  IN purge_config.include_child_company_flag%type
                         ,p_user_id          IN purge_config.user_id%type
                         ,p_misc_parm_1      IN purge_config.misc_parm_1%type
                         ,p_misc_parm_2      IN purge_config.misc_parm_2%type
                         ,p_misc_parm_3      IN purge_config.misc_parm_3%type
                         ,p_rows_purged      OUT pls_integer
                         ,p_error_msg        OUT purge_hist.message%type) ;

END lm_archive_pkg;
/

CREATE OR REPLACE PACKAGE BODY lm_archive_pkg
AS
   c_pkg_name       CONSTANT VARCHAR2 (35)            := 'lm_archive_pkg';
   c_debug_yn       CONSTANT CHAR (1)                 := 'n';
   --c_msg_id_error   CONSTANT VARCHAR2 (4)           := '1111';
   c_msg_id_error   CONSTANT VARCHAR2 (9)             := '8888';
   c_msg_id_inform         CONSTANT varchar2(9)       := '9999';
   
   -- NORMAL:  archive transaction tables, don't archive transient
   c_arch_norm             CONSTANT char(1) := 'D';
   -- ALL:  archive all tables
   c_arch_all              CONSTANT char(1) := 'Y';
   -- NONE: archive no tables
   c_arch_none             CONSTANT char(1) := 'N'; 

   e_arch_purge_failed       EXCEPTION;
   rec_msglog                wm_archive_pkg.rt_msglog;
   -- CURRENT WAREHOUSE NAME BEING PROCESSED (SEE EXEC_PURGE)
   gv_whse                   whse_master.whse%TYPE;
   gv_company                company.company_name%type;
   
   FUNCTION purge_dtl_conditon
   (p_cond_num         IN   varchar2 ,
    p_days            IN   purge_config.nbr_days_old%type)
   RETURN varchar2 is
   stmt_4_indpnt varchar2(8000);
   BEGIN
      IF ((INSTR (UPPER (p_cond_num), 'DTTM') > 0) OR (INSTR (UPPER (p_cond_num), 'DATE') > 0) OR (INSTR (UPPER (p_cond_num), 'TIME') > 0))
             THEN
                   stmt_4_indpnt :=
                         stmt_4_indpnt
                      || p_cond_num
                    --  || ' <= trunc(sysdate) - ' || p_days;
					 || ' <=  :p_start_time - '|| p_days;
             ELSIF (INSTR (UPPER (p_cond_num), 'WHSE') > 0)
             THEN
                stmt_4_indpnt :=
                    stmt_4_indpnt || p_cond_num || '=' || '''' || gv_whse || '''';
             ELSIF (   (INSTR (p_cond_num, '>') > 0)
                    OR ((INSTR (p_cond_num, '=') > 0))
                    OR (INSTR (p_cond_num, '<>') > 0)
                    OR ((INSTR (p_cond_num, '<') > 0))
                    OR ((INSTR (p_cond_num, '!=') > 0))
                    OR ((INSTR (UPPER (p_cond_num), 'BETWEEN') > 0))
                    OR ((INSTR (UPPER (p_cond_num), 'IN') > 0))
                    OR ((INSTR (UPPER (p_cond_num), 'IS') > 0))
                   )
             THEN
                   stmt_4_indpnt := stmt_4_indpnt || p_cond_num;
             END IF;
     RETURN stmt_4_indpnt;
   END;   
   
   PROCEDURE LM_PURGE_SQLS_BUILD
   (  p_purge_id        IN   purge_config.purge_code%TYPE, --Instead will be using Purge code(purge_config.purge_code)
      p_days            IN   purge_config.nbr_days_old%type
   )
   IS
      stmt_4_del           VARCHAR2 (8000);
      stmt_4_del_dpnt      VARCHAR2 (8000);
      stmt_4_indpnt        VARCHAR2 (8000);
      v_fk_column_name     VARCHAR2 (500);
      v_hdr_cond           VARCHAR2 (500);
      v_closing            VARCHAR2 (20);
      t_prg_ordr           e_purge_dtl.purge_order%TYPE;
      t_tab_nam            e_purge_dtl.table_name%TYPE;
      t_hdr_tab            e_purge_dtl.table_name%TYPE;
      l_t_hdr_tab          e_purge_dtl.table_name%TYPE;
      t_hdr_tab_final      e_purge_dtl.table_name%TYPE;
      v_error_msg            VARCHAR2 (300) := ' ';

      CURSOR cur_purge_dtl
      IS
         SELECT   purge_order, table_name, hdr_table
             FROM e_purge_dtl
            WHERE purge_id = p_purge_id and hdr_table is not null
         ORDER BY purge_order ASC;
   BEGIN
      update e_purge_dtl
      set purge_sql = 'DELETE FROM ' || table_name || ' WHERE ' || rtrim(purge_dtl_conditon(cond_1,p_days) || ' and ' || purge_dtl_conditon(cond_2,p_days) || ' and ' || purge_dtl_conditon(cond_3,p_days) || ' and ' || purge_dtl_conditon(cond_4,p_days) || ' and ' || purge_dtl_conditon(cond_5,p_days) || ' and ' || purge_dtl_conditon(cond_6,p_days),' and ')
      where purge_id = p_purge_id and hdr_table is null;
      
      if p_purge_id = '116'
      then
          update e_purge_dtl
          set purge_sql = replace(purge_sql, 
                                 'MODULE_TYPE=30',
                                 'ENGINE_CODE in ( select ENGINE_CODE from WHSE_ENGINE_CONFIG where MODULE_TYPE = 30 )'
                                 )
          where purge_id =116 and table_name ='LABOR_MSG';     
      end if;          
      
      OPEN cur_purge_dtl;
      LOOP
         FETCH cur_purge_dtl
          INTO  t_prg_ordr, t_tab_nam, t_hdr_tab;
          
         EXIT WHEN cur_purge_dtl%NOTFOUND;
         
           --  Preparing delete statement from Child tables
           stmt_4_del_dpnt := '';              
          
            SELECT LISTAGG (ucc.column_name, ',')
                     WITHIN GROUP (ORDER BY ucc.position)
             INTO  v_fk_column_name
             FROM user_cons_columns ucc, user_constraints uc
            WHERE ucc.table_name = uc.table_name
              AND uc.constraint_type = 'P'
              AND ucc.constraint_name = uc.constraint_name
              AND ucc.table_name = upper(t_hdr_tab)
          GROUP BY ucc.constraint_name;
          
          stmt_4_del_dpnt := stmt_4_del_dpnt || ' ' || v_fk_column_name || ')';
          
          IF UPPER(t_tab_nam) = 'E_ZONE_MSG_LOG'
          THEN
             stmt_4_del_dpnt := REPLACE (stmt_4_del_dpnt, 'LABOR_MSG_ID', 'HDR_MSG_ID');
          ELSIF UPPER (t_tab_nam) = 'LABOR_MSG_MONITOR' 
          THEN
             stmt_4_del_dpnt := REPLACE(stmt_4_del_dpnt,'LABOR_TRAN_ID','MATCHED_LABOR_TRAN_ID' );
		  ELSIF UPPER (t_tab_nam) = 'E_ENGINE_OUTPUT_MSG' 
          THEN
             stmt_4_del_dpnt := REPLACE(stmt_4_del_dpnt,'LABOR_MSG_ID','TRAN_NBR' );
		  END IF;                
                 
          stmt_4_del := '( SELECT ' || v_fk_column_name || ' FROM ' || t_hdr_tab || ' WHERE ';

          l_t_hdr_tab   :=  t_hdr_tab;
          t_hdr_tab_final := '';
          v_closing       := '';
          
          LOOP
              SELECT case when hdr_table is null
                          then 'HEADER'
                          else hdr_table
                          end as hdr_table
                INTO t_hdr_tab_final
                FROM e_purge_dtl
               WHERE table_name = l_t_hdr_tab AND purge_id = p_purge_id;
               
               if t_hdr_tab_final = 'HEADER'
               then
                  select replace(purge_sql,'DELETE FROM '||table_name || ' WHERE ', '')
                  into v_hdr_cond
                  from e_purge_dtl
                  where table_name = l_t_hdr_tab AND purge_id = p_purge_id;
                  
                  stmt_4_del := stmt_4_del || v_hdr_cond || ')';
               else
                    SELECT LISTAGG (ucc.column_name, ',')
                             WITHIN GROUP (ORDER BY ucc.position)
                     INTO  v_fk_column_name
                     FROM user_cons_columns ucc, user_constraints uc
                    WHERE ucc.table_name = uc.table_name
                      AND uc.constraint_type = 'P'
                      AND ucc.constraint_name = uc.constraint_name
                      AND ucc.table_name = upper(t_hdr_tab_final)
                  GROUP BY ucc.constraint_name;                    
               
                  stmt_4_del := stmt_4_del || v_fk_column_name || ' in (select '|| v_fk_column_name || ' from ' || t_hdr_tab_final || ' where ';
                  v_closing := v_closing || ')'; 
                  
                  l_t_hdr_tab   :=  t_hdr_tab_final;
               end if;
               
               if t_hdr_tab_final = 'HEADER'
               then
                  exit;
               end if;
          END LOOP;
          
          stmt_4_del_dpnt :=
                 'DELETE FROM ' || t_tab_nam || ' WHERE(' || stmt_4_del_dpnt;
          stmt_4_del := stmt_4_del || v_closing;

          stmt_4_del_dpnt := stmt_4_del_dpnt || 'IN' || stmt_4_del;
          
          IF UPPER(t_tab_nam) IN ('E_WMS_EVNT_MSG_DTL','E_WMS_EVNT_MSG','E_AUD_LOG_TRVL','E_AUD_LOG_FACTOR','E_AUD_LOG') 
          THEN   
             stmt_4_del_dpnt := REPLACE (replace(stmt_4_del_dpnt, 'LABOR_MSG_ID', 'TRAN_NBR'),'LABOR_TRAN_ID','TRAN_NBR');
          END IF;
          
          UPDATE e_purge_dtl
             SET purge_sql = stmt_4_del_dpnt
           WHERE purge_id = p_purge_id AND TABLE_NAME= t_tab_nam AND HDR_TABLE = t_hdr_tab and purge_order=t_prg_ordr ; 

      END LOOP;
     
      COMMIT;
   
   EXCEPTION
      WHEN OTHERS
      THEN
         v_error_msg := substr(sqlerrm,1,255);
         wm_archive_pkg.msg_log_insert(p_msg_parms => 'lm_purge_sqls_build(), purge_code=' || p_purge_id||': Exception! -> ' || v_error_msg);
         raise e_arch_purge_failed;   
   END;   
   
   /*PROCEDURE purge_table_where     -- use the where clause instead of array of ids
           (
            p_table_name    in varchar2,
            p_where_clause  in varchar2,
            p_commit_size   in pls_integer,
            p_start_time    in timestamp,
            p_archive_ind   in char,
            p_purge_cd      in varchar2,
            p_comp_id       in pls_integer,
            p_warehouse_id  in pls_integer,
            p_rowcount      out pls_integer,
            p_error_msg     out varchar2)
       IS

        i_arc_rc                    pls_integer default 0;
        i_del_rc                    pls_integer default 0;
        v_msg                       varchar2(250);
        v_table_clause              varchar2(65);
        v_column_list               clob;
        v_archive_table_name        varchar2(30);
        e_ArchTableMappingNotFound  exception;

       BEGIN
           rec_MsgLog.r_start_dttm     := p_start_time;
           rec_MsgLog.r_cd_master_id   := p_comp_id;
           rec_MsgLog.r_purge_code     := p_purge_cd;
           rec_MsgLog.r_whse_id        := p_warehouse_id;
           rec_MsgLog.r_table_name     := p_table_name;
           
        if p_archive_ind in (c_arch_norm, c_arch_all) and lower(c_debug_yn)='n' then
        
           -- check table name length -------------------------------
           v_archive_table_name:= wm_archive_pkg.get_archive_table_mapping(p_table_name);
           if length(v_archive_table_name)=0 then
              raise e_ArchTableMappingNotFound;
           end if;

           EXECUTE IMMEDIATE
           'INSERT INTO '||v_archive_table_name||' ('
           ||wm_archive_pkg.get_col_list(p_table_name)
           ||') SELECT '
           ||wm_archive_pkg.get_col_list(p_table_name)     
           ||' FROM '
           ||p_table_name||' '
           ||p_where_clause using p_start_time;

           -- increment archive count
           I_ARC_RC := I_ARC_RC + SQL%ROWCOUNT;       
        end if; -- archive_ind
        
        EXECUTE IMMEDIATE 'DELETE FROM '||p_table_name||' '||p_where_clause using p_start_time;

        -- increment delete count
        i_del_rc := i_del_rc + SQL%rowcount;

        IF LOWER(c_debug_yn)='n' THEN
            COMMIT;
        END IF;

        -- set new values and log the message
        rec_MsgLog.r_msg_id         := c_msg_id_inform;
        rec_MsgLog.r_rows_archived  := i_arc_rc;
        rec_MsgLog.r_rows_deleted   := i_del_rc;
        rec_MsgLog.r_end_dttm       := systimestamp;
        wm_archive_pkg.log_msg (p_rec_MsgLog       => rec_MsgLog, p_whse => gv_whse , p_company => gv_company);
        -- return the number of deleted rows
        p_rowcount := i_del_rc;

       EXCEPTION
        WHEN E_ARCHTABLEMAPPINGNOTFOUND THEN
            p_error_msg := 'Table mapping expected but not found';
            wm_archive_pkg.msg_log_insert(p_msg_parms => 'purge_table_where('''||p_table_name||'''), purge_code=' || p_purge_cd||': Exception! -> ' || p_error_msg,p_cd_master_id => p_comp_id
                           ,p_warehouse => p_warehouse_id);
            -- set new values and log the message
            rec_MsgLog.r_msg_id := c_msg_id_error;
            rec_MsgLog.r_rows_archived := i_arc_rc;
            rec_MsgLog.r_rows_deleted := i_del_rc;
            rec_MsgLog.r_end_dttm := systimestamp;
            wm_archive_pkg.log_msg (p_rec_MsgLog => rec_MsgLog, p_whse => gv_whse , p_company => gv_company);
            
            raise e_arch_purge_failed;
            
        WHEN OTHERS THEN
            p_error_msg := substr(sqlerrm,1,200);
            wm_archive_pkg.msg_log_insert(p_msg_parms => 'purge_table_where('''||p_table_name||'''), purge_code=' || p_purge_cd||': Exception! -> ' || p_error_msg,p_cd_master_id => p_comp_id
                           ,p_warehouse => p_warehouse_id);
            -- set new values and log the message
            rec_MsgLog.r_msg_id := c_msg_id_error;
            rec_MsgLog.r_rows_archived := i_arc_rc;
            rec_MsgLog.r_rows_deleted := i_del_rc;
            rec_MsgLog.r_end_dttm := systimestamp;
            wm_archive_pkg.log_msg (p_rec_MsgLog => rec_MsgLog, p_whse => gv_whse , p_company => gv_company);
            
            raise e_arch_purge_failed;
    END; */

   /* Purge PROCEDURE called by PROCEDURE UI_PURGE */
    PROCEDURE exec_purge (p_company_id      IN purge_config.cd_master_id%type
                        ,p_whse_id          IN purge_config.whse_master_id%type
                        ,p_commit_size      IN purge_config.nbr_rows_to_commit%type
                        ,p_days             IN purge_config.nbr_days_old%type
                        ,p_purge_cd         IN purge_config.purge_code%type
                        ,p_archive_flag     IN purge_config.archive_flag%type
                        ,p_child_comp_flag  IN purge_config.include_child_company_flag%type
                        ,p_user_id          IN purge_config.user_id%type
                        ,p_misc_parm_1      IN purge_config.misc_parm_1%type
                        ,p_misc_parm_2      IN purge_config.misc_parm_2%type
                        ,p_misc_parm_3      IN purge_config.misc_parm_3%type
						,p_rows_purged      OUT pls_integer
                        ,p_error_msg        OUT purge_hist.message%type)

   IS
      dstart                TIMESTAMP ( 6 );
      v_msg                 VARCHAR2 (250);
      i_rc                  PLS_INTEGER                         DEFAULT 0;
      i_rows_purged         PLS_INTEGER                         DEFAULT 0;
      i_lpn_stat_code       PLS_INTEGER;
      e_invalidpurgecode    EXCEPTION;
      e_InvalidWhseID       EXCEPTION; 
      e_StopPurge           EXCEPTION;      
      v_purge_config_id     purge_config.purge_config_id%TYPE;
      v_archive_flag        purge_config.archive_flag%type;
      v_sql                  varchar2(32000);
      v_error_msg           purge_hist.MESSAGE%TYPE;
      v_lm_code_desc        VARCHAR2(100); 
      v_arch_starts                CONSTANT char(15):= 'ARCHIVE STARTS';
      v_arch_ends               CONSTANT char(15):= 'ARCHIVE ENDS';
	  v_count				number(10);
   BEGIN
        -- set global WHSE variable
        begin
            select whse
            into gv_whse
            from whse_master
            where whse_master_id = p_whse_id;
        exception
            when others then
                v_error_msg := 'exec_purge(): Warehouse ID Not Found --> ' || to_char(p_whse_id);
                raise e_InvalidWhseID;
        end;       
        
        select company_name
        into gv_company
        from company
        where company_id = p_company_id;
        
        dstart := systimestamp; 
		
		if (p_archive_flag <> c_arch_none) then
        
		v_sql:='select count(1) 
		from archive_run_detail
		where PURGE_CODE='||p_purge_cd||' and STATUS='''||v_arch_starts||'''';
		
		execute immediate v_sql into v_count;
		if ( v_count=0 ) then
		
			v_sql:='insert into archive_run_detail(run_id,PURGE_CODE,COMPANY_ID,START_TIME,STATUS)
					select '||archive_run_log_SEQ.nextval||','||p_purge_cd||','||p_company_id||','''||dstart||''','''||v_arch_starts||''' from dual';
			
			execute immediate v_sql;
		
		end if;
		end if;
      CASE
         WHEN p_purge_cd IN ('101','102','103','104','105','106','107','108','109','110','111','112','113','115','116','118','119','120','121','122','123','124')
         THEN
           -- dstart := SYSTIMESTAMP;

            begin
                select code_desc 
                into v_lm_code_desc 
                from sys_code 
                where rec_type ='S' 
                and code_type='708'
                and code_id = p_purge_cd;
            exception
                when others then
                    v_lm_code_desc := 'LM';
            end;             
            
            DBMS_APPLICATION_INFO.set_module(module_name => v_lm_code_desc||' Purge' , action_name => 'Archiving and Purging');
                            
            LM_PURGE_SQLS_BUILD(p_purge_cd,p_days);

            FOR i IN (SELECT purge_id, purge_order, table_name, hdr_table,
                             stat_code, cond_1, cond_2, cond_3, cond_4,
                             cond_5, cond_6, user_id, last_mod_date,
                             version_id,
                             CONCAT (' ',
                                     SUBSTR (purge_sql,
                                             (INSTR (purge_sql, 'WHERE')
                                             )
                                            )
                                    ) where_clause,
                             purge_sql ,
                             archive_flag
                        FROM e_purge_dtl
                       WHERE purge_id = p_purge_cd and purge_sql is not null
                       ORDER BY PURGE_ORDER )
            LOOP
                IF p_archive_flag='D' THEN
                    v_archive_flag:=i.archive_flag;
                ELSE
                    v_archive_flag:=p_archive_flag;
                END IF;            

                wm_archive_pkg.purge_table_where (p_table_name        => i.table_name,
                                     p_where_clause      => i.where_clause,
                                     p_commit_size       => p_commit_size,
                                     p_start_time        => dstart,
                                     p_archive_ind       => v_archive_flag,
                                     p_purge_cd          => p_purge_cd,
                                     p_comp_id           => p_company_id,
                                     p_warehouse_id      => p_whse_id,
                                     p_rowcount          => i_rc,
                                     p_error_msg         => v_error_msg
                                    );
   
                i_rows_purged := i_rows_purged + i_rc;
                if v_error_msg is not null then 
					raise e_StopPurge;
				end if;   
				
            END LOOP;
      ELSE
            RAISE e_invalidpurgecode;
      END CASE;

      -- set the return value
      p_rows_purged := i_rows_purged;
	  
      if ( p_archive_flag <> c_arch_none) then
        v_sql:='update ARCHIVE_RUN_DETAIL
              set STATUS='''||v_arch_ends||''', END_TIME = SYSTIMESTAMP
	      	  where purge_code='||p_purge_cd||' and STATUS='''||v_arch_starts||'''';
        execute immediate v_sql;
      end if;
        
                    
      IF LOWER (c_debug_yn) = 'y'
      THEN
         ROLLBACK;
      ELSE 
          commit ;
      END IF;
   EXCEPTION
        WHEN e_InvalidPurgeCode THEN
            wm_archive_pkg.msg_log_insert(p_msg_parms => 'Exception! exec_purge() - InvalidPurge');
            raise_application_error (-20001,'ERROR: ' || c_pkg_name || '.exec_purge -> Invalid Purge Code.  Call Failed');
            
        WHEN e_arch_purge_failed THEN
            wm_archive_pkg.msg_log_insert(p_msg_parms => 'Exception! exec_purge() - arch_purge_failed');
            raise_application_error (-20003,'Archive/Purge Failed.');
            
        WHEN e_InvalidWhseID THEN
            wm_archive_pkg.msg_log_insert(p_msg_parms => 'Exception! exec_purge() - arch_purge_failed');
            raise_application_error (-20007,'Warehouse ID '|| to_char(p_whse_id)||' not found.');              

        WHEN e_StopPurge THEN
            rollback;
            wm_archive_pkg.msg_log_insert (p_msg_parms => 'exec_purge(): Exception! -> Purge Code('||to_char(p_purge_cd)||') ' || v_error_msg
                           ,p_cd_master_id => p_company_id
                           ,p_warehouse => p_whse_id);
            p_error_msg := v_error_msg;

        WHEN OTHERS THEN
            rollback;
            wm_archive_pkg.msg_log_insert(p_msg_parms => 'Exception! exec_purge() - others');
            wm_archive_pkg.msg_log_insert(p_msg_parms => '--> exec_purge() Exception! -> ' || sqlerrm);
            v_msg := substr(sqlerrm,250);
            rec_MsgLog.r_start_dttm := dstart;
            rec_MsgLog.r_cd_master_id := p_company_id;
            rec_MsgLog.r_purge_code := p_purge_cd;
            rec_MsgLog.r_whse_id := p_whse_id;
            rec_MsgLog.r_table_name := null;
            rec_MsgLog.r_msg_id := c_msg_id_error;
            rec_MsgLog.r_rows_archived := 0;
            rec_MsgLog.r_rows_deleted := 0;
            rec_MsgLog.r_end_dttm := systimestamp;
            wm_archive_pkg.log_msg (p_rec_MsgLog => rec_MsgLog, p_whse => gv_whse , p_company => gv_company);
   END;
END lm_archive_pkg;
/
show errors;
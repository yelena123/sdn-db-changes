create or replace procedure wm_create_tasks_for_wave
(
    p_user_id                 in ucl_user.user_name%type,
    p_facility_id             in facility.facility_id%type,
    p_tc_company_id           in wave_parm.tc_company_id%type,
    p_task_genrtn_ref_nbr_csv in varchar2,
    p_task_genrtn_ref_code    in alloc_invn_dtl.task_genrtn_ref_code%type
)
as
begin
    wm_task_creation_intrnl(p_user_id, p_facility_id, p_tc_company_id, p_task_genrtn_ref_nbr_csv,
        p_task_genrtn_ref_code, null, null);
end;
/
show errors;

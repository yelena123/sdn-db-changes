EXEC SEQUPDT('LABEL', 'LABEL_ID', 'SEQ_LABEL_ID');

MERGE INTO LABEL L
USING (SELECT 'CBOGrid' BUNDLE_NAME,
'GenerateLoadDiagram' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'GenerateLoadDiagram',
'Generate Load Plan',
'CBOGrid');

MERGE INTO LABEL L
USING (SELECT 'CBOGrid' BUNDLE_NAME,
'ViewLoadDiagram' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'ViewLoadDiagram',
'View Load Plan',
'CBOGrid');

MERGE INTO LABEL L
USING (SELECT 'CBOGrid' BUNDLE_NAME,
'PrintLoadDiagram' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'PrintLoadDiagram',
'Print Load Plan',
'CBOGrid');

MERGE INTO LABEL L
USING (SELECT 'CBOGrid' BUNDLE_NAME,
'GenerateLaborPlan' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'GenerateLaborPlan',
'Generate Labor Plan',
'CBOGrid');


MERGE INTO LABEL L
USING (SELECT 'CBOGrid' BUNDLE_NAME,
'AssignTrailer' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'AssignTrailer',
'Assign Trailer',
'CBOGrid');

MERGE INTO LABEL L
USING (SELECT 'CBOGrid' BUNDLE_NAME,
'Diagrammedby' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'Diagrammedby',
'Planned By',
'CBOGrid');


MERGE INTO LABEL L
USING (SELECT 'CBOGrid' BUNDLE_NAME,
'PickStart' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'PickStart',
'Dispatch',
'CBOGrid');


MERGE INTO LABEL L
USING (SELECT 'CBOGrid' BUNDLE_NAME,
'Route' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'Route',
'Route',
'CBOGrid');


MERGE INTO LABEL L
USING (SELECT 'CBOGrid' BUNDLE_NAME,
'LoadPlanStatus' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'LoadPlanStatus',
'Load Plan Status',
'CBOGrid');


MERGE INTO LABEL L
USING (SELECT 'CBOGrid' BUNDLE_NAME,
'DesignatedEquipment' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'DesignatedEquipment',
'Trailer Type',
'CBOGrid');


MERGE INTO LABEL L
USING (SELECT 'CBOGrid' BUNDLE_NAME,
'LastUpdatedDateTime' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'LastUpdatedDateTime',
'Last Modifed',
'CBOGrid');


MERGE INTO LABEL L
USING (SELECT 'CBOGrid' BUNDLE_NAME,















'LoadDiagramStatusDesc' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'LoadDiagramStatusDesc',
'Load Plan Status',
'CBOGrid');

MERGE INTO LABEL L
USING (SELECT 'CBOGrid' BUNDLE_NAME,
'NumberOfStops' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'NumberOfStops',
'Stops',
'CBOGrid');
MERGE INTO LABEL L
USING (SELECT 'CBOGrid' BUNDLE_NAME,
'MarkedForErrorMessage' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'MarkedForErrorMessage',
'Error Message Log',
'CBOGrid');

--Following RESOURCES and RESOURCE_PERMISSION Entries are commented and added in C_PERMISSION_NAVIGATION_SCRIPT.sql 

--RESOURCES and RESOURCE_PERMISSION Entries
--REYELoadDiagramMain.xhtml
---- INSERT INTO RESOURCES ---
-- MERGE INTO RESOURCES R
-- USING (SELECT '/wm/ld/ui/REYELoadDiagramMain.xhtml' URI FROM DUAL)D
-- ON (R.URI = D.URI)
-- WHEN NOT MATCHED THEN
-- INSERT VALUES (SEQ_RESOURCE_ID.NEXTVAL,'/wm/ld/ui/REYELoadDiagramMain.xhtml','ACM',1,NULL);   

-- ---- INSERT INTO RESOURCE_PERMISSION ---
-- MERGE INTO RESOURCE_PERMISSION R
-- USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYELoadDiagramMain.xhtml')D
-- ON (R.RESOURCE_ID = D.RESOURCE_ID)
-- WHEN NOT MATCHED THEN
-- INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYELoadDiagramMain.xhtml'),'WMARUNWV');


--REYEOrderLineItemListUserShort.xhtml
---- INSERT INTO RESOURCES ---
-- MERGE INTO RESOURCES R
-- USING (SELECT '/basedata/popup/REYEOrderLineItemListUserShort.xhtml' URI FROM DUAL)D
-- ON (R.URI = D.URI)
-- WHEN NOT MATCHED THEN
-- INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/basedata/popup/REYEOrderLineItemListUserShort.xhtml',
-- (select module from RESOURCES where uri ='/wm/ld/ui/REYECustProdTypeOverride.xhtml'),1,NULL); 

-- ---- INSERT INTO RESOURCE_PERMISSION ---
-- MERGE INTO RESOURCE_PERMISSION R
-- USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/basedata/popup/REYEOrderLineItemListUserShort.xhtml')D
-- ON (R.RESOURCE_ID = D.RESOURCE_ID)
-- WHEN NOT MATCHED THEN
-- INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/basedata/popup/REYEOrderLineItemListUserShort.xhtml'),
-- (select permission_code from RESOURCE_PERMISSION where resource_id = (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYECustProdTypeOverride.xhtml')));

--REYEOrderLineItemListWaveShort.xhtml
---- INSERT INTO RESOURCES ---
-- MERGE INTO RESOURCES R
-- USING (SELECT '/basedata/popup/REYEOrderLineItemListWaveShort.xhtml' URI FROM DUAL)D
-- ON (R.URI = D.URI)
-- WHEN NOT MATCHED THEN
-- INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/basedata/popup/REYEOrderLineItemListWaveShort.xhtml',
-- (select module from RESOURCES where uri ='/wm/ld/ui/REYECustProdTypeOverride.xhtml'),1,NULL); 

-- ---- INSERT INTO RESOURCE_PERMISSION ---
-- MERGE INTO RESOURCE_PERMISSION R
-- USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/basedata/popup/REYEOrderLineItemListWaveShort.xhtml')D
-- ON (R.RESOURCE_ID = D.RESOURCE_ID)
-- WHEN NOT MATCHED THEN
-- INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/basedata/popup/REYEOrderLineItemListWaveShort.xhtml'),
-- (select permission_code from RESOURCE_PERMISSION where resource_id = (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYECustProdTypeOverride.xhtml')));

---Included in SF3994154
--Customer Item Category Overrides
--REYECustProdTypeOverride.xhtml
-- INSERT INTO xmenu_item
--            (xmenu_item_id,
--             NAME,
--             short_name,
--             navigation_key,
--             tile_key,
--             icon,
--             bgcolor,
--             screen_mode,
--             screen_version,
--             base_section_name,
--             base_part_name,
--             is_default)
--VALUES     ((SELECT Max(xmenu_item_id) + 1
--             FROM   xmenu_item),
--            'Customer Item Category Overrides',
--            'cust_item_cat_over',
--            '/wm/ld/ui/REYECustProdTypeOverride.jsflps',
--            '',
--            'default-icon',
--            '#002E5F',
--            0,
--            1,
--            'Distribution',
--            'GENERAL',
--            0);  

--insert into XMENU_ITEM_APP
--  (XMENU_ITEM_ID, APP_ID)
--values
--  ((select xmenu_item_id from xmenu_item where name like 'Customer Item Category Overrides'), 36);
  
--  insert into XMENU_ITEM_PERMISSION
--  (XMENU_ITEM_ID, PERMISSION_CODE)
--values
--  ((select xmenu_item_id from xmenu_item where name like 'Customer Item Category Overrides'), 'WMARUNWV');
  
--Insert into xbase_menu_item (XBASE_MENU_ITEM_ID,XBASE_MENU_PART_ID,XMENU_ITEM_ID,SEQUENCE) values 
--(
--(select max(XBASE_MENU_ITEM_ID)+1  from xbase_menu_item),
--23,
--(select xmenu_item_id from xmenu_item where name like 'Customer Item Category Overrides'),
--(select max(XBASE_MENU_ITEM_ID)+1  from xbase_menu_item)
--);

/* ################################################################################################################################## */

--Item Categories
--REYEItemCat.xhtml

--INSERT INTO xmenu_item
--            (xmenu_item_id,
--             NAME,
--             short_name,
--             navigation_key,
--             tile_key,
--             icon,
--             bgcolor,
--             screen_mode,
--             screen_version,
--             base_section_name,
--             base_part_name,
--             is_default)
--VALUES     ((SELECT Max(xmenu_item_id) + 1
--             FROM   xmenu_item),
--            'Item Categories',
--            'cust_item_cat',
--            '/wm/ld/ui/REYEItemCat.jsflps',
--            '',
--            'default-icon',
--            '#002E5F',
--            0,
--            1,
--            'Distribution',
--            'GENERAL',
--            0);  

--insert into XMENU_ITEM_APP
--  (XMENU_ITEM_ID, APP_ID)
--values
--  ((select xmenu_item_id from xmenu_item where name like 'Item Categories'), 36);
  
--  insert into XMENU_ITEM_PERMISSION
--  (XMENU_ITEM_ID, PERMISSION_CODE)
--values
--  ((select xmenu_item_id from xmenu_item where name like 'Item Categories'), 'WMARUNWV');
  
--Insert into xbase_menu_item (XBASE_MENU_ITEM_ID,XBASE_MENU_PART_ID,XMENU_ITEM_ID,SEQUENCE) values 
--(
--(select max(XBASE_MENU_ITEM_ID)+1  from xbase_menu_item),
--23,
--(select xmenu_item_id from xmenu_item where name like 'Item Categories'),
--(select max(XBASE_MENU_ITEM_ID)+1  from xbase_menu_item)
--);

-- --TODO
-- update label set Value = 'Trailer Type' where bundle_name = 'CBOGrid' and key like 'DesignatedEquipment';
-- --select * from label where key like 'DesignatedEquipment%' and bundle_name like '%CBOGrid%';

-- update label set Value = 'Dispatch' where bundle_name = 'CBOGrid' and key like 'PickStart';
-- --select * from label where key like 'PickStart%' and bundle_name like '%CBOGrid%';

-- update label set Value = 'Stops' where bundle_name = 'CBOGrid' and key like 'NumberOfStops';
-- --select * from label where key like 'NumberOfStops%' and bundle_name like '%CBOGrid%';


-- ----- 4036790

-- update label set value = 'Generate Load Plan' where label_id = '606940' and key like 'GenerateLoadDiagram';
-- update label set value = 'View Load Plan' where label_id = '606941' and key like 'ViewLoadDiagram';
-- update label set value = 'Print Load Plan' where label_id = '606942' and key like 'PrintLoadDiagram';
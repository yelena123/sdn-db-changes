--to add all permissions missing in WMROLE in MB's upgraded DB compared to WMROLE in seed data from a fresh install
merge into role_app_mod_perm ramp
using (select ri.role_id, ampi.app_id,ampi.module_id,ampi.permission_id 
from role RI,APP_MOD_PERM ampi 
where RI.ROLE_NAME in('WMROLE') 
and ampi.permission_id in (select permission_id from PERMISSION where 
PERMISSION_CODE in 
('ABU',
'ADMINPER',
'ADMINRES',
'ADMINSCREEN',
'ADMINUCG',
'ADMIN_THEME',
'ADOCKDOOR',
'AITEMEXP',
'AMOBILEWORKFLOWS',
'ANXTUPNBRB',
'APCLZONERATE',
'ASPC',
'ASYSCDOPARM',
'ASYSCDOTYPE',
'ASYSCDS',
'ASYSCDSPARM',
'ASYSCDSTYPE',
'BUILDLPNRTS',
'CRT_PUB_FILTER',
'CRT_PVT_FILTER',
'DLU',
'EDITRTSLINES',
'EPIEOD',
'ICR',
'IDL',
'INISHIPASN',
'IRG',
'ITL',
'LMAAVGESPCNFG',
'LMASCHDLRSLTCNFG',
'LMASCHDLTMPLCNFG',
'LMASCHDRSLTCNFG',
'LMVAVGESPCNFG',
'LMVC',
'LMVSCHDLRSLTCNFG',
'LMVSCHDLTMPLCNFG',
'LMVSCHDRSLTCNFG',
'MPWLFD',
'MPWML',
'MPWSSL',
'PRTEPIDOC',
'PRTPRCTRPT',
'RSA',
'RTSADDLPN',
'RTSCANCELLPN',
'RTSREMOVELPN',
'SENDMHEMSG',
'SXP',
'UCLU',
'UINISHPASN',
'VALIPRERCT',
'VCOSTPO',
'VPREALLOC',
'VPRERCTRPT',
'WMAAARSNCDE',
'WMAASSITEM',
'WMACLSHMPT',
'WMACNCLSHORT',
'WMADDLPNDO',
'WMADISPTYPE',
'WMAESLCKCNT',
'WMAGBRCL',
'WMAHAZSHPVIA',
'WMAHIER',
'WMALTRCNFG',
'WMAMMPCAILTASK',
'WMAMMPCENDCART',
'WMAMMPCENDTOTE',
'WMAMMPCRECLOCN',
'WMAMMPCTSKSELN',
'WMAMOBANCOLPN',
'WMAMOBMAKPICRT',
'WMAMOBPKPIKCRT',
'WMAMOBTASKSUM',
'WMAMPPCAILTASK',
'WMAMPPCALTERNT',
'WMAMPPCENDCART',
'WMAMPPCENDLPN',
'WMAMPPCPUTPART',
'WMAMPPCRECLOCN',
'WMAMPPCREMVLPN',
'WMAMPPCSHORT',
'WMAMPPCSKIP',
'WMAMPPCSKPREPL',
'WMAOBRUL',
'WMAPDLOCPROP',
'WMAPHLOCPROP',
'WMAPKSHRT',
'WMAPKUPTSKRUL',
'WMAPRCLHZMT',
'WMAPRTLOCN',
'WMAREMPLD',
'WMARESENDPCKWAVE',
'WMARFCHGPWD',
'WMAROLLSHORT',
'WMAUIINBDRLS',
'WMAUIITVERLS',
'WMDOPCKRPT',
'WMLLK',
'WMPACKSTN',
'WMRFAUDITOBPLT',
'WMRFPRNTOLPNLAB',
'WMVAARSNCDE',
'WMVCOMSCRN',
'WMVDISPTYPE',
'WMVHAZSHPVIA',
'WMVHIER',
'WMVINSTYP',
'WMVLTRCNFG',
'WMVOBRUL',
'WMVOLPNPLD',
'WMVPKSHRT',
'WMVPKUPTSKRUL',
'WMVPRCLHZMT',
'WMVPRTLOCN',
'WMVREMPLD',
'WMVSTRDISTRO',
'WMVUIINBDRLS',
'WMVUIITVERLS',
'WMWAVECHCLSEDO',
'YMAR',
'YMVR',
'WMSHIPCONF')
)) rampw
on (ramp.role_id = rampw.role_id
    and ramp.app_id = rampw.app_id
     and ramp.module_id = rampw.module_id
      and ramp.permission_id = rampw.permission_id)
WHEN NOT MATCHED
then
insert (role_id, app_id,module_id,permission_id,row_uid)
values(rampw.role_id, rampw.app_id,rampw.module_id,rampw.permission_id,SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);  

--to add all permisions to all roles that are missing compared to WMROLE 
--and this should be executed after above query so that missing permissions from WMROLE are already taken care
merge into role_app_mod_perm ramp
using (select ri.role_id, rampi.app_id,rampi.module_id,rampi.permission_id 
from role ri,role_app_mod_perm rampi 
where RI.ROLE_NAME <> 'WMROLE' and rampi.role_id = (select role_id from role where role_name='WMROLE')) rampw
		on (ramp.role_id = rampw.role_id
			and ramp.app_id = rampw.app_id
      and ramp.module_id = rampw.module_id
      and ramp.permission_id = rampw.permission_id)
WHEN NOT MATCHED
then
insert (role_id, app_id,module_id,permission_id,row_uid)
values(rampw.role_id, rampw.app_id,rampw.module_id,rampw.permission_id,SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);

commit;
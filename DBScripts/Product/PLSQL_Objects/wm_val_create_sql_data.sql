create or replace procedure wm_val_create_sql_data
(
    p_sql_desc      in val_sql.sql_desc%type,
    p_ident         in val_sql.ident%type,
    p_raw_text      in val_sql.raw_text%type,
    p_owner         in val_sql.owner%type default null,
    p_format_sql    in number default 1
)
as
begin
    delete from val_sql vs
    where vs.ident = p_ident;

    insert into val_sql
    (
        sql_id, sql_desc, ident, create_date_time, mod_date_time, raw_text, format_sql, owner,
        user_id
    )
    values
    (
        val_sql_id_seq.nextval, p_sql_desc, p_ident, sysdate, sysdate, p_raw_text, p_format_sql,
        p_owner, p_owner
    );
end;
/
show errors;

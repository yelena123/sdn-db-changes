declare
  v_exist number;
  begin
    select count(*) into v_exist from label where key = 'Update_Shortage_Reason_Codes' and bundle_name = 'CBOTrans';

    if (v_exist = 0) then
      insert into label
      values
        ((select max(label_id) + 1 from label),
         'Update_Shortage_Reason_Codes',
         'Update Shortage Reason Codes',
         'CBOTrans',
         sysdate,
         null);
      commit;
    end if;
  EXCEPTION
    when others THEN
      dbms_output.put_line(sqlerrm);
      raise_application_error(-20201, SQLCODE||','||SQLERRM);
end;
/
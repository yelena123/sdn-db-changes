create or replace package custom_load_diagram
		as

		FUNCTION balance_fit (i_shipment_n  varchar2,
							  i_order_type  varchar2,
							  i_deliv_id    number,
							  i_load_type   number) RETURN BOOLEAN;
		PROCEDURE C_GENERATE_LOAD_DIAGRAM (p_shipment_tc_id   IN   varchar2,
										   p_user_id          IN   varchar2,
										   p_warehouse        IN   varchar2,
										   p_company_id       IN   number,
										   p_out              OUT  number
										   );

		FUNCTION makeDlUnitsWithinRange (p_if_split number,
										 p_deliv number,
										 p_split_order_type varchar2) RETURN BOOLEAN;

		Function merge_delivery_units (p_deliv NUMBER) RETURN BOOLEAN;

		Function create_foundation_data(p_shipment_tc_id varchar2) RETURN BOOLEAN;

		Function pallet_position (p_shipment_tc_id varchar2) RETURN BOOLEAN;


		PROCEDURE exception_msg_log_insert (p_msg          msg_log.msg%TYPE DEFAULT NULL,
											p_func_name msg_log.func_name%TYPE,
											p_ref_value_1 msg_log.ref_value_1%TYPE DEFAULT NULL,
											p_log_category  varchar2 DEFAULT 'DEBUG'
											);
		FUNCTION create_bulk_delivery_units(v_shipment varchar2) RETURN BOOLEAN;
		end custom_load_diagram;
/

create or replace
package BODY custom_load_diagram AS

  cursor c_deliv_unit(i_storage_t number, i_order_n varchar2, i_item number, i_load_type number) is
				select gb.deliv_id,
								   delivery_unit_number.nextval deliv_unit_number,
								   cst.max_cntr_vol max_cube,
								   cst.min_cntr_vol min_vol,
								   cst.MAX_CNTR_WGHT max_weight,
								   cst.min_cntr_wght min_wght,
								   0 assigned_weight,
								   0 assigned_volume,
								   case
												when ceil(cst.min_cntr_vol / gb.unit_vol) >
																  ceil(cst.min_cntr_wght / gb.unit_wt) then
												  ceil(cst.min_cntr_wght / gb.unit_wt)
												else
												  ceil(cst.min_cntr_vol / gb.unit_vol)
								   end min_num_case,
								   case
												when floor(cst.max_cntr_vol / gb.unit_vol) >
																  floor(cst.max_cntr_wght / gb.unit_wt) then
												  floor(cst.max_cntr_wght / gb.unit_wt)
												else
												  floor(cst.max_cntr_vol / gb.unit_vol)
								   end max_num_case,
								   cst.vol_range,
								   cst.wght_range,
								   i_load_type load_type
				  from global_main gb, c_storage_type cst
				where gb.tc_order_id = i_order_n
				   and gb.item_id = i_item
				   and cst.STORAGE_TYPE_ID = i_storage_t;

  TYPE deliv_list is TABLE OF c_deliv_unit%ROWTYPE INDEX BY PLS_INTEGER;
  deliv_list1 deliv_list;

  g_msg           msg_log.msg%type;
  g_user_id       msg_log.user_id%type;
  g_warehouse     msg_log.whse%type;
  g_company_id    msg_log.cd_master_id%type;
  g_equipment_id  shipment.dsg_equipment_id%TYPE;
  cntr_deliv       number := 1;
  g_overflow_flag  number := 0;
  g_log_level      number;
  g_orig_order_qty number := 0;
  g_total_wght     number := 0;
  g_total_vol      number := 0;

  ----------------------------------------------------------------------------------------
  --  Function Name : create delivery units                                              --
  --  Description   : Main function for creation of delivery units                       --
  --                  This function will form the base delivery units                    --
  --                  and also call balanced best fit,make within range                  --
  --                  merge ,pallet positioning functions                                --
  -----------------------------------------------------------------------------------------

  PROCEDURE C_GENERATE_LOAD_DIAGRAM(p_shipment_tc_id IN varchar2,
																																				p_user_id        IN varchar2,
																																				p_warehouse      IN varchar2,
																																				p_company_id     IN number,
																																				p_out            OUT number) IS

				-- cursor to get all grouped order lines
				-- change to not pick deliv id 0. as these will be direct to overflow
				cursor c_deliv_id is
				  select distinct deliv_id
				  from global_main
				  where deliv_id <> 0;
				--where shipment_no = p_shipment_tc_id
				-- group by deliv_id;

				cursor c_split_min_vol is
				  select 1
								from temp_hold_delivery_units
				   where header_flag = 'N'
								and order_qty_assigned <= min_num_case
								and bulk_flag = 'N';

				TYPE rowid_table is TABLE OF ROWID INDEX BY PLS_INTEGER;

				rowid_array rowid_table;

				v_split_min_vol      number;
				rec                  c_deliv_id%ROWTYPE;
				var_rowid_counter    number := 1;
				var_max_cases        number := 0;
				var_cases_in_dl_unit number := 0;
				var_cases_reqd       number := 0;
				var_curr_dl          number := 0;
				var_dl_unit_created  number := 0;
				var_flag_put         number := 0;
				v_return_value       boolean;
				v_load_diagram       number := 0;
				v_route_id           VARCHAR2(32);
				v_num_stops          number;
				v_order_qty          number;
				--v_total_wght         number;
				--v_total_vol          number;
				v_overflow_cases     number;
				v_assigned_cases     number;
				v_shipment_id        number;
				v_prev_position      varchar2(10);
				v_next_position      varchar2(10);
				v_seq_current_value  number;
				--v_log_level          VARCHAR2(1);
				v_func_name msg_log.func_name%TYPE := 'C_GENERATE_LOAD_DIAGRAM';
				v_shipment_ref_id varchar2(10);
				
				--g_msg                msg_log.msg%type;

  BEGIN

				-- change start :SL
				/*
				SELECT misc_flags
								INTO v_log_level
								FROM sys_code
				   WHERE code_type = '01L'
								AND code_type = '01L'
								AND code_id = '001';
				  IF v_log_level = '1' THEN
								g_log_level := 1;
				  ELSIF v_log_level = '2' THEN
								g_log_level := 2;
				  ELSE
								g_log_level := 0;
				  END IF;
				*/

				SELECT to_number(misc_flags)
				  INTO g_log_level
				  FROM sys_code
				WHERE code_type = '01L'
				   AND code_id = '001';
				-- change end : SL

				p_out := 0;

				-- assign global variables
				g_user_id    := p_user_id;
				g_warehouse  := p_warehouse;
				g_company_id := p_company_id;

				--open c_deliv_id;
				--if c_deliv_id%NOTFOUND
				--then
				--return false;
				--end if;
				-- get route id
				select PROD_SCHED_REF_NUMBER, DSG_EQUIPMENT_ID, shipment_id
				  into v_route_id, g_equipment_id, v_shipment_id
				  from shipment
				where tc_shipment_id = p_shipment_tc_id;

				if v_shipment_ref_id = '40' then
				return;
				end if;
				
				if g_equipment_id is NULL
				then
				   select EQ.equipment_id
								into g_equipment_id
								from sys_code SC, EQUIPMENT EQ
								where sc.rec_type='C'
								  and sc.code_type='01E'
								  and eq.EQUIPMENT_CODE = sc.code_id;
				end if;
				-- create foundation data
				-- update shipment status and authorization number (Diagrammed by field)
				update shipment
				   set shipment_ref_id = 10,
				   auth_nbr = g_user_id
				where tc_shipment_id = p_shipment_tc_id;

				-- System updates

				delete from error_log
				where shipment_id = v_shipment_id
				   and object_type = 'LDGR';
				-- delete the old error messages
				delete from msg_log
				where ref_value_1 = p_shipment_tc_id
				   and sub_pgm_name = 'ERROR';
				  commit;

				IF g_log_level > 0 THEN
				  exception_msg_log_insert('In function generate load diagram',
																												   v_func_name,
																												   p_shipment_tc_id,
																												   'INFO');

				  g_msg := 'Starting create_foundation_data' || SYSTIMESTAMP;
				  exception_msg_log_insert(g_msg,
																												   v_func_name,
																												   p_shipment_tc_id,
																												   'INFO');
				END IF;
				if (create_foundation_data(p_shipment_tc_id)) then
				  IF g_log_level > 0 THEN
								g_msg := 'Call to create_foundation_data success' || SYSTIMESTAMP;
								exception_msg_log_insert(g_msg,
																																v_func_name,
																																p_shipment_tc_id,
																																'INFO');
				  END IF;
				else
				/*
				  exception_msg_log_insert('Call to create_foundation_data failed',
																												   v_func_name,
																												   p_shipment_tc_id,
																												   'ERROR');
*/
				  -- update shipment
				  update shipment
								set SHIPMENT_REF_ID = 15
				   where tc_shipment_id = p_shipment_tc_id;
				  p_out := 1;
				  return;
				end if;
				------------------------------------
				---Adding the bulk part-----

				if (create_bulk_delivery_units(p_shipment_tc_id)) then
				  IF g_log_level > 0 THEN
								g_msg := 'Call to create_bulk_delivery_units for all order lines success' ||
																SYSTIMESTAMP;
								exception_msg_log_insert(g_msg,
																																v_func_name,
																																p_shipment_tc_id,
																																'INFO');
				  END IF;
				else
				/*
				  g_msg := 'Call to create_bulk_delivery_units for all order lines failed' ||
												   SYSTIMESTAMP;
				  exception_msg_log_insert(g_msg,
																												   v_func_name,
																												   p_shipment_tc_id,
																												   'ERROR');
				*/
				  -- update shipment
				  update shipment
								set SHIPMENT_REF_ID = 15
				   where tc_shipment_id = p_shipment_tc_id;
				  p_out := 1;
				  return;
				end if;
				------------------------------------
				IF g_log_level > 0 THEN
				  g_msg := 'Start of packing basing units' || SYSTIMESTAMP;
				  exception_msg_log_insert(g_msg,
																												   v_func_name,
																												   p_shipment_tc_id,
																												   'INFO');
				END IF;

				-- change : SL 26/06 remove the lines which have over-ride load type
				-- check if over load types have direct overflow set and set deliv id
				-- to zero so they are assigned to overflow

				update global_main gb
				set    deliv_id = 0 ,
								   direct_overflow = 'Y'
				where exists  (
																				select 1
																				from c_load_type cl
																				where gb.load_type = cl.load_type_id
																				and direct_overflow='Y'
																  );

				-- 2changes
				/*
				update GLOBAL_MAIN
				set  ORDER_TYPE = 'N',
  GROUP_AS_FAMILY='Y'
				where category_id = 38;
				*/
				-- change end : SL 26/06
				update global_main
				set  order_type = 'N'
				where GROUP_AS_FAMILY='Y';



				for rec in c_deliv_id loop
				  -- loop for each group of order types which are formed based on logic of 3.5.3
				  -- delete the delivery list as new list will be created for new group of order lines
				  if deliv_list1.count() >= 1 then
								--dbms_output.put_line('Deleting delivery units');
								--FOR recs IN deliv_list1.first .. deliv_list1.last LOOP
								  deliv_list1.Delete();
								--END LOOP;
				  end if;

				  cntr_deliv := 1;
				  -- Basing logic create a deliv unit for each Base family
				  -- loop for each distinct load type

				  for cntr_load in (select distinct load_type, max_case
																								  from global_main
																								where group_as_family = 'Y'
																								   and deliv_id = rec.deliv_id) loop
								IF g_log_level = 2 THEN
								  g_msg := 'load_number' || cntr_load.load_type;
								  exception_msg_log_insert(g_msg,
																																   v_func_name,
																																   p_shipment_tc_id,
																																   'DEBUG');
								END IF;
								-- get total order cases
								select sum(orig_order_qty)
								  into var_max_cases
								  from global_main
								where deliv_id = rec.deliv_id
								   and group_as_family = 'Y'
								   and load_type = cntr_load.load_type;

								-- change : SL 100613 to handle case when
								-- max cases on load type is greater than the order qty
								-- put all base order lines in one unit
								if cntr_load.max_case >= var_max_cases
								then
								  var_cases_reqd := 1;
								else
								  var_cases_reqd := floor(var_max_cases / cntr_load.max_case);
								end if;

								--var_cases_reqd       := floor(var_max_cases / cntr_load.max_case);
								var_cases_in_dl_unit := 0;
								var_dl_unit_created  := 0;

								--dbms_output.put_line('var_cases_reqd ' || var_cases_reqd);
								-- loop for all orders in this load type and form the bases

								if (var_cases_reqd = 0) then
								  -- in such a case update where order qty < max_case for that
								  --load type take all lines as normal order line
								  update global_main
												set order_type = 'O'
								   where deliv_id = rec.deliv_id
												and load_type = cntr_load.load_type;
								  /* change : SL 030613 */
								  -- exiting if previous load type had zero bulk cases to be created
								  -- instead shoud process the next load type and ignore the current one
								  --exit;
								  continue;
								  /* change end: SL 030613 */
								end if;

								for cntr_orders in (select ROWID,
																																   SHIPMENT_NO,
																																   DELIV_ID,
																																   BULK_PROCESS,
																																   TC_ORDER_ID,
																																   FACILITY_ID,
																																   LINE_ITEM_ID,
																																   ITEM_NAME,
																																   ITEM_ID,
																																   ORIG_ORDER_QTY,
																																   CATEGORY_ID,
																																   LOAD_TYPE,
																																   STORAGE_TYPE,
																																   BREAK_BY_STOP,
																																   DSG_EQUIPMENT_ID,
																																   ORDER_ID,
																																   MIN_CASE,
																																   MAX_CASE,
																																   FLAG,
																																   GROUP_AS_FAMILY,
																																   SPLIT_FLAG,
																																   SHIPMENT_ID,
																																   PROCESSED,
																																   UNIT_WT * ORIG_ORDER_QTY order_wt,
																																   UNIT_VOL * ORIG_ORDER_QTY order_vol,
																																   ORDER_LOADING_SEQ,
																																   unit_wt,
																																   unit_vol
																												  from global_main
																												where deliv_id = rec.deliv_id
																												   and load_type = cntr_load.load_type
																												   and group_as_family = 'Y'
																												   and bulk_process = 'N'
																												order by orig_order_qty desc) loop

								  --dbms_output.put_line ('cntr_orders '||cntr_orders.tc_order_id);

								  -- check if entering first time
								  if (var_dl_unit_created = 0) then
												--dbms_output.put_line ('181');
												--dbms_output.put_line ('storage type' || cntr_orders.storage_type||'tc_order_id '|| cntr_orders.tc_order_id||'item_id '|| cntr_orders.item_id);
												--dbms_output.put_line (' cntr_deliv' || cntr_deliv);
												--dbms_output.put_line('creating a new base storage type is '||cntr_orders.storage_type);
												open c_deliv_unit(cntr_orders.storage_type,
																												  cntr_orders.tc_order_id,
																												  cntr_orders.item_id,
																												  cntr_load.load_type);
												fetch c_deliv_unit
												  into deliv_list1(cntr_deliv);
												close c_deliv_unit;
												-- header entry
												insert into temp_hold_delivery_units
												  (c_seq,
												   deliv_id,
												   deliv_unit_number,
												   tc_order_id,
												   item_id,
												   order_qty_assigned,
												   total_order_qty,
												   order_wt,
												   order_vol,
												   max_volume,
												  min_vol,
												   max_wght,
												   min_wght,
												   assigned_wght,
												   assigned_vol,
												   min_num_case,
												   max_num_case,
												   header_flag,
												   vol_range,
												   wght_range,
												   bulk_flag,
												   equipment_id,
												   storage_type,
												   order_id,
												   shipment_id,
												   facility_id,
												   load_type,
												   order_type)
												values
												  (c_range_seq.nextval,
												   deliv_list1(cntr_deliv).deliv_id,
												   deliv_list1(cntr_deliv).deliv_unit_number,
												   0,
												   0,
												   0,
												   0,
												   0,
												   0,
												   deliv_list1(cntr_deliv).max_cube,
												   deliv_list1(cntr_deliv).min_vol,
												   deliv_list1(cntr_deliv).max_weight,
												   deliv_list1(cntr_deliv).min_wght,
												   0,
												   0,
												   deliv_list1(cntr_deliv).min_num_case,
												   deliv_list1(cntr_deliv).max_num_case,
												   'Y',
												   deliv_list1(cntr_deliv).vol_range,
												   deliv_list1(cntr_deliv).wght_range,
												   'N',
												   0,
												   0,
												   0,
												   0,
												   0,
												  0,
												   'B');
												var_curr_dl         := cntr_deliv;
												cntr_deliv          := cntr_deliv + 1;
												var_dl_unit_created := 1;
								  end if;
								  -- check if the current order can be packed in this dl unit

								  if (cntr_orders.orig_order_qty >= cntr_load.max_case
												  or
												  cntr_orders.unit_wt*cntr_orders.orig_order_qty > deliv_list1(var_curr_dl).max_weight
												  or
												  cntr_orders.unit_vol*cntr_orders.orig_order_qty > deliv_list1(var_curr_dl).max_cube) then
												-- update global main so that we can process this order in balanced fit
												update global_main
												   set order_type = 'B', processed = 0
												where rowid = cntr_orders.rowid;
												continue;
								  end if;

								  if (var_cases_in_dl_unit + cntr_orders.orig_order_qty <=
												cntr_load.max_case) then

												-- CR : 28/06 Basing should honour max wt/vol
												if (((cntr_orders.order_wt -
												   (deliv_list1(var_curr_dl)
												   .max_weight - deliv_list1(var_curr_dl).assigned_weight)) <
												   0.001) AND ((cntr_orders.order_vol -
												   (deliv_list1(var_curr_dl)
												   .max_cube - deliv_list1(var_curr_dl)
												   .assigned_volume)) < 0.001))  then

												-- pack this order line in the dl unit
												insert into temp_hold_delivery_units
												  (c_seq,
												   deliv_id,
												   deliv_unit_number,
												   tc_order_id,
												   item_id,
												   order_qty_assigned,
												   total_order_qty,
												   order_wt,
												   order_vol,
												   max_volume,
												   min_vol,
												   max_wght,
												   min_wght,
												   assigned_wght,
												   assigned_vol,
												   min_num_case,
												   max_num_case,
												   header_flag,
												   vol_range,
												   wght_range,
												   bulk_flag,
												   equipment_id,
												   storage_type,
												   order_id,
												   shipment_id,
												   facility_id,
												   load_type,
												   order_type,
												   line_item_id,
												   item_name,
												   order_loading_seq,
												   category_id,
												   unit_wt,
												   unit_vol)
												values
												  (c_range_seq.nextval,
												   deliv_list1(var_curr_dl).deliv_id,
												   deliv_list1(var_curr_dl).deliv_unit_number,
												   cntr_orders.tc_order_id,
												   cntr_orders.item_id,
												   cntr_orders.orig_order_qty,
												   cntr_orders.orig_order_qty,
												   cntr_orders.order_wt,
												   cntr_orders.order_vol,
												   deliv_list1(var_curr_dl).max_cube,
												   deliv_list1(var_curr_dl).min_vol,
												   deliv_list1(var_curr_dl).max_weight,
												   deliv_list1(var_curr_dl).min_wght,
												   0,
												   0,
												   deliv_list1(var_curr_dl).min_num_case,
												   deliv_list1(var_curr_dl).max_num_case,
												   'N',
												   deliv_list1(var_curr_dl).vol_range,
												   deliv_list1(var_curr_dl).wght_range,
												   'N',
												   cntr_orders.dsg_equipment_id,
												   cntr_orders.storage_type,
												   cntr_orders.order_id,
												   cntr_orders.shipment_id,
												   cntr_orders.facility_id,
												   cntr_orders.load_type,
												   'B',
												   cntr_orders.line_item_id,
												   cntr_orders.item_name,
												   cntr_orders.order_loading_seq,
												   cntr_orders.category_id,
												   cntr_orders.unit_wt,
												   cntr_orders.unit_vol);

												var_cases_in_dl_unit := var_cases_in_dl_unit +
																																				cntr_orders.orig_order_qty;
												rowid_array(var_rowid_counter) := cntr_orders.rowid;
												var_rowid_counter := var_rowid_counter + 1;

												-- update assigned wt of the deliv unit

												deliv_list1(var_curr_dl).assigned_weight := deliv_list1(var_curr_dl)
																																																				   .assigned_weight +
																																																								cntr_orders.unit_wt *
																																																								cntr_orders.orig_order_qty;
												deliv_list1(var_curr_dl).assigned_volume := deliv_list1(var_curr_dl)
																																																				   .assigned_volume +
																																																								cntr_orders.unit_vol *
																																																								cntr_orders.orig_order_qty;
												end if;
								  else
												-- this is increasing the max case consideration
												-- check if within max wt and vol
												if (((cntr_orders.order_wt -
												   (deliv_list1(var_curr_dl)
												   .max_weight - deliv_list1(var_curr_dl).assigned_weight)) <
												   0.001) AND ((cntr_orders.order_vol -
												   (deliv_list1(var_curr_dl)
												   .max_cube - deliv_list1(var_curr_dl)
												   .assigned_volume)) < 0.001)) then
												  insert into temp_hold_delivery_units
																(c_seq,
																deliv_id,
																deliv_unit_number,
																tc_order_id,
																item_id,
																order_qty_assigned,
																total_order_qty,
																order_wt,
																order_vol,
																max_volume,
																min_vol,
																max_wght,
																min_wght,
																assigned_wght,
																assigned_vol,
																min_num_case,
																max_num_case,
																header_flag,
																vol_range,
																wght_range,
																bulk_flag,
																equipment_id,
																storage_type,
																order_id,
																shipment_id,
																facility_id,
																load_type,
																order_type,
																line_item_id,
																item_name,
																order_loading_seq,
																category_id,
																unit_wt,
																unit_vol)
												  values
																(c_range_seq.nextval,
																deliv_list1(var_curr_dl).deliv_id,
																deliv_list1(var_curr_dl).deliv_unit_number,
																cntr_orders.tc_order_id,
																cntr_orders.item_id,
																cntr_orders.orig_order_qty,
																cntr_orders.orig_order_qty,
																cntr_orders.order_wt,
																cntr_orders.order_vol,
																deliv_list1(var_curr_dl).max_cube,
																deliv_list1(var_curr_dl).min_vol,
																deliv_list1(var_curr_dl).max_weight,
																deliv_list1(var_curr_dl).min_wght,
																0,
																0,
																deliv_list1(var_curr_dl).min_num_case,
																deliv_list1(var_curr_dl).max_num_case,
																'N',
																deliv_list1(var_curr_dl).vol_range,
																deliv_list1(var_curr_dl).wght_range,
																'N',
																cntr_orders.dsg_equipment_id,
																cntr_orders.storage_type,
																cntr_orders.order_id,
																cntr_orders.shipment_id,
																cntr_orders.facility_id,
																cntr_orders.load_type,
																'B',
																cntr_orders.line_item_id,
																cntr_orders.item_name,
																cntr_orders.order_loading_seq,
																cntr_orders.category_id,
																cntr_orders.unit_wt,
																cntr_orders.unit_vol);

												  var_cases_in_dl_unit := var_cases_in_dl_unit +
																																				  cntr_orders.orig_order_qty;
												  rowid_array(var_rowid_counter) := cntr_orders.rowid;
												  var_rowid_counter := var_rowid_counter + 1;
												  var_flag_put := 1;
												  -- update assigned weight and volume of the dl unit
												  deliv_list1(var_curr_dl).assigned_weight := deliv_list1(var_curr_dl)
																																																								.assigned_weight +
																																																								  cntr_orders.unit_wt *
																																																								  cntr_orders.orig_order_qty;
												  deliv_list1(var_curr_dl).assigned_volume := deliv_list1(var_curr_dl)
																																																								.assigned_volume +
																																																								  cntr_orders.unit_vol *
																																																								  cntr_orders.orig_order_qty;
												end if;
												-- close this delivery units .. create a new one and move the counter
												if (var_cases_reqd > var_dl_unit_created) then
												  --dbms_output.put_line('creating a new base storage type is '||cntr_orders.storage_type);
												  open c_deliv_unit(cntr_orders.storage_type,
																																cntr_orders.tc_order_id,
																																cntr_orders.item_id,
																																cntr_load.load_type);
												  fetch c_deliv_unit
																into deliv_list1(cntr_deliv);
												  close c_deliv_unit;

												  insert into temp_hold_delivery_units
																(c_seq,
																deliv_id,
																deliv_unit_number,
																tc_order_id,
																item_id,
																order_qty_assigned,
																total_order_qty,
																order_wt,
																order_vol,
																max_volume,
																min_vol,
																max_wght,
																min_wght,
																assigned_wght,
																assigned_vol,
																min_num_case,
																max_num_case,
																header_flag,
																vol_range,
																wght_range,
																bulk_flag,
																equipment_id,
																storage_type,
																order_id,
																shipment_id,
																facility_id,
																load_type,
																order_type)
												  values
																(c_range_seq.nextval,
																deliv_list1(cntr_deliv).deliv_id,
																deliv_list1(cntr_deliv).deliv_unit_number,
																0,
																0,
																0,
																0,
																0,
																0,
																deliv_list1(cntr_deliv).max_cube,
																deliv_list1(cntr_deliv).min_vol,
																deliv_list1(cntr_deliv).max_weight,
																deliv_list1(cntr_deliv).min_wght,
																0,
																0,
																deliv_list1(cntr_deliv).min_num_case,
																deliv_list1(cntr_deliv).max_num_case,
																'Y',
																deliv_list1(cntr_deliv).vol_range,
																deliv_list1(cntr_deliv).wght_range,
																'N',
																0,
																0,
																0,
																0,
																0,
																0,
																'B');

												  var_curr_dl := cntr_deliv;
												  cntr_deliv  := cntr_deliv + 1;
												  -- initiliaze variables again

												  var_cases_in_dl_unit := 0;
												  var_dl_unit_created  := var_dl_unit_created + 1;

												  if var_flag_put = 0 then
																--  put the current order in this new dl unit
																insert into temp_hold_delivery_units
																  (c_seq,
																   deliv_id,
																   deliv_unit_number,
																   tc_order_id,
																   item_id,
																   order_qty_assigned,
																   total_order_qty,
																   order_wt,
																   order_vol,
																   max_volume,
																   min_vol,
																   max_wght,
																   min_wght,
																   assigned_wght,
																   assigned_vol,
																   min_num_case,
																   max_num_case,
																   header_flag,
																   vol_range,
																   wght_range,
																   bulk_flag,
																   equipment_id,
																   storage_type,
																   order_id,
																   shipment_id,
																   facility_id,
																   load_type,
																   order_type,
																   line_item_id,
																   item_name,
																   order_loading_seq,
																   category_id,
																   unit_wt,
																   unit_vol)
																values
																  (c_range_seq.nextval,
																   deliv_list1(var_curr_dl).deliv_id,
																   deliv_list1(var_curr_dl).deliv_unit_number,
																   cntr_orders.tc_order_id,
																   cntr_orders.item_id,
																   cntr_orders.orig_order_qty,
																   cntr_orders.orig_order_qty,
																   cntr_orders.order_wt,
																   cntr_orders.order_vol,
																   deliv_list1(var_curr_dl).max_cube,
																   deliv_list1(var_curr_dl).min_vol,
																   deliv_list1(var_curr_dl).max_weight,
																   deliv_list1(var_curr_dl).min_wght,
																   0,
																   0,
																   deliv_list1(var_curr_dl).min_num_case,
																   deliv_list1(var_curr_dl).max_num_case,
																   'N',
																   deliv_list1(var_curr_dl).vol_range,
																   deliv_list1(var_curr_dl).wght_range,
																   'N',
																   cntr_orders.dsg_equipment_id,
																   cntr_orders.storage_type,
																   cntr_orders.order_id,
																   cntr_orders.shipment_id,
																   cntr_orders.facility_id,
																   cntr_orders.load_type,
																   'B',
																   cntr_orders.line_item_id,
																   cntr_orders.item_name,
																   cntr_orders.order_loading_seq,
																   cntr_orders.category_id,
																   cntr_orders.unit_wt,
																   cntr_orders.unit_vol);

																var_cases_in_dl_unit := var_cases_in_dl_unit +
																																								cntr_orders.orig_order_qty;
																rowid_array(var_rowid_counter) := cntr_orders.rowid;
																var_rowid_counter := var_rowid_counter + 1;
																-- update assigned weight and volume of the dl unit
																deliv_list1(var_curr_dl).assigned_weight := deliv_list1(var_curr_dl)
																																																								   .assigned_weight +
																																																												cntr_orders.unit_wt *
																																																												cntr_orders.orig_order_qty;
																deliv_list1(var_curr_dl).assigned_volume := deliv_list1(var_curr_dl)
																																																								   .assigned_volume +
																																																												cntr_orders.unit_vol *
																																																												cntr_orders.orig_order_qty;
												  end if;
												else
												  var_flag_put := 0;
												  exit;
												end if;
								  end if;
								end loop;
				  end loop;

				  -- update all lines with order type 'B' which were taken as base above
				  forall i in 1 .. rowid_array.count
								update global_main
								   set order_type = 'B'
								where rowid = rowid_array(i);
				  IF g_log_level > 0 THEN
								g_msg := 'Finished basing function' || SYSTIMESTAMP;
								exception_msg_log_insert(g_msg,
																																v_func_name,
																																p_shipment_tc_id,
																																'INFO');

								g_msg := 'starting balanced best fit function for non - base order lines' ||
																SYSTIMESTAMP;
								exception_msg_log_insert(g_msg,
																																v_func_name,
																																p_shipment_tc_id,
																																'INFO');
				  END IF;
				  -- call balanced fit for all load types that were used to form base above try to fit the non base order lines with the bases of the same load type
				  for var_counter in (select distinct load_type
																												from temp_hold_delivery_units
																								   where header_flag = 'N'
																												and deliv_id = rec.deliv_id) loop
								if (balance_fit(p_shipment_tc_id,
																								'N',
																								rec.deliv_id,
																								var_counter.load_type)) then
								  null;
								else
								/*
								  g_msg := 'Call to balance_best_fit for non base lines failed' ||
																   SYSTIMESTAMP;
								  exception_msg_log_insert(g_msg,
																																   v_func_name,
																																   p_shipment_tc_id,
																																   'ERROR');
								  */
								  -- update shipment
								  update shipment
												set SHIPMENT_REF_ID = 15
								   where tc_shipment_id = p_shipment_tc_id;
								  p_out := 1;

								  return;
								end if;
				  end loop;
				  IF g_log_level > 0 THEN
								g_msg := 'Call to balance_best_fit for non base lines success' ||
																SYSTIMESTAMP;
								exception_msg_log_insert(g_msg,
																																v_func_name,
																																p_shipment_tc_id,
																																'INFO');

								g_msg := 'starting balanced best fit function for all order lines' ||
																SYSTIMESTAMP;
								exception_msg_log_insert(g_msg,
																																v_func_name,
																																p_shipment_tc_id,
																																'INFO');
				  END IF;
				  -- call balanced best fit for all order lines
				  if (balance_fit(p_shipment_tc_id, 'O', rec.deliv_id, 0)) then
								IF g_log_level > 0 THEN
								  g_msg := 'Call to balance_best_fit for all order lines success' ||
																   SYSTIMESTAMP;
								  exception_msg_log_insert(g_msg,
																																   v_func_name,
																																   p_shipment_tc_id,
																																   'INFO');
								END IF;
				  else
				  /*
								g_msg := 'Call to balance_best_fit for all order lines failed' ||
																SYSTIMESTAMP;
								exception_msg_log_insert(g_msg,
																																v_func_name,
																																p_shipment_tc_id,
																																'ERROR');
								*/
								-- update shipment
								update shipment
								   set SHIPMENT_REF_ID = 15
								where tc_shipment_id = p_shipment_tc_id;
								p_out := 1;

								return;
				  end if;

				  forall i in 1 .. deliv_list1.count()
								update temp_hold_delivery_units
								   set assigned_wght = deliv_list1(i).assigned_weight,
												   assigned_vol  = deliv_list1(i).assigned_volume
								where deliv_unit_number = deliv_list1(i).deliv_unit_number;

				  if (merge_delivery_units(rec.deliv_id)) then
								null;
				  else
								g_msg := 'Call to merge delivery units failed' || SYSTIMESTAMP;
								exception_msg_log_insert(g_msg,
																																v_func_name,
																																p_shipment_tc_id,
																																'ERROR');
								-- update shipment
								update shipment
								   set SHIPMENT_REF_ID = 15
								where tc_shipment_id = p_shipment_tc_id;
								p_out := 1;

								return;
				  end if;
				  IF g_log_level > 0 THEN
								g_msg := 'Call to merge delivery units success' || SYSTIMESTAMP;
								exception_msg_log_insert(g_msg,
																																v_func_name,
																																p_shipment_tc_id,
																																'INFO');
				  END IF;

				   IF g_log_level > 0 THEN
								g_msg := 'Calling make dl units within range' ||
																SYSTIMESTAMP;
								exception_msg_log_insert(g_msg,
																																v_func_name,
																																p_shipment_tc_id,
																																'INFO');
				  END IF;

				  if (makeDlUnitsWithinRange(0, rec.deliv_id, null)) then
								IF g_log_level > 0 THEN
								  --dbms_output.put_line('Call to makeDlUnitsWithinRange - w/o splitting success');
								  exception_msg_log_insert('Call to makeDlUnitsWithinRange - w/o splitting success',
																																   v_func_name,
																																   p_shipment_tc_id,
																																   'INFO');
								END IF;
				  else
								--dbms_output.put_line('Call to makeDlUnitsWithinRange- w/o splitting failed');
								exception_msg_log_insert('Call to makeDlUnitsWithinRange - w/o splitting failed',
																																v_func_name,
																																p_shipment_tc_id,
																																'ERROR');
								-- update shipment
								update shipment
								   set SHIPMENT_REF_ID = 15
								where tc_shipment_id = p_shipment_tc_id;
								p_out := 1;

								return;
				  end if;


				  -- change 2308 adding new algo for swapping units out of range

				   if ( c_swap_dl_units (p_shipment_tc_id,rec.deliv_id) = false )
				   then
								exception_msg_log_insert('Call to c_swap_dl_units failed',
																																   'c_swap_dl_units',
																																   p_shipment_tc_id,
																																   'ERROR');
								update shipment
								   set SHIPMENT_REF_ID = 15
								where tc_shipment_id = p_shipment_tc_id;
								p_out := 1;

								return;
				   end if;

				  -- make dl units within range with splitting set of normal order lines

				  if (makeDlUnitsWithinRange(1, rec.deliv_id, 'O')) then
								IF g_log_level > 0 THEN
								  --dbms_output.put_line('Call to makeDlUnitsWithinRange -with splitting of normal order lines success');
								  exception_msg_log_insert('Call to makeDlUnitsWithinRange -with splitting of normal order lines success',
																																   v_func_name,
																																   p_shipment_tc_id,
																																   'INFO');
								END IF;
				  else
								--dbms_output.put_line('Call to makeDlUnitsWithinRange- with splitting of normal order lines failed');
								exception_msg_log_insert('Call to makeDlUnitsWithinRange -with splitting of normal order lines failed',
																																v_func_name,
																																p_shipment_tc_id,
																																'ERROR');
								-- update shipment
								update shipment
								   set SHIPMENT_REF_ID = 15
								where tc_shipment_id = p_shipment_tc_id;
								p_out := 1;

								return;
				  end if;
				  -- make dl units within range with splitting set of non base order lines
				-- change :SL client does not want to split base lines 110613
/*
				  if (makeDlUnitsWithinRange(1, rec.deliv_id, 'N')) then
								IF g_log_level > 0 THEN
								  --dbms_output.put_line('Call to makeDlUnitsWithinRange -with splitting of non base order lines success');
								  exception_msg_log_insert('Call to makeDlUnitsWithinRange -with splitting of non base order lines success',
																																   v_func_name,
																																   p_shipment_tc_id,
																																   'INFO');
								END IF;
				  else
								--dbms_output.put_line('Call to makeDlUnitsWithinRange- with splitting of non base order lines failed');
								exception_msg_log_insert('Call to makeDlUnitsWithinRange -with splitting of non base order lines failed',
																																v_func_name,
																																p_shipment_tc_id,
																																'ERROR');
								-- update shipment
								update shipment
								   set SHIPMENT_REF_ID = 15
								where tc_shipment_id = p_shipment_tc_id;
								p_out := 1;

								return;
				  end if;
*/
   -- change done 110613
				  IF g_log_level > 0 THEN
								--dbms_output.put_line('Ending Make dl unit within range function ' ||
								--                                                                             SYSTIMESTAMP);
								exception_msg_log_insert('Ending Make dl unit within range function ' ||
																																SYSTIMESTAMP,
																																v_func_name,
																																p_shipment_tc_id,
																																'INFO');
				  END IF;
				end loop; -- done creating delivery units for this shipment

				IF g_log_level > 0 THEN
				  --dbms_output.put_line('starting pallet positioning function ' ||
				  --                                                                             SYSTIMESTAMP);
				  exception_msg_log_insert('Starting pallet positioning function ' ||
																												   SYSTIMESTAMP,
																												   v_func_name,
																												   p_shipment_tc_id,
																												   'INFO');
				END IF;

				-- change for overlapping compt
				-- SL : 23/06

				if ( c_compartment_mapping (p_shipment_tc_id,g_equipment_id)) then
				  IF g_log_level > 0 THEN
								--dbms_output.put_line('Call to c_compartment_mapping success');
								exception_msg_log_insert('Call to c_compartment_mapping success',
																																v_func_name,
																																p_shipment_tc_id,
																																'INFO');
				  END IF;
				else
				  --dbms_output.put_line('Call to pallet positioning units failed');
				  exception_msg_log_insert('Call to c_compartment_mapping units failed',
																												   v_func_name,
																												   p_shipment_tc_id,
																												   'ERROR');
				  -- update shipment
				  update shipment
								set SHIPMENT_REF_ID = 15
				   where tc_shipment_id = p_shipment_tc_id;
				  p_out := 1;

				  return;
				end if;

				IF g_log_level > 0 THEN
				  --dbms_output.put_line('Ending pallet positioning function ' ||
				  --                                                              SYSTIMESTAMP);
				  exception_msg_log_insert('Ending c_compartment_mapping function ' ||
																												   SYSTIMESTAMP,
																												   v_func_name,
																												   p_shipment_tc_id,
																												   'INFO');
				END IF;
  -- return;
				-- change end 23/06
				if (pallet_position(p_shipment_tc_id)) then
				  IF g_log_level > 0 THEN
								--dbms_output.put_line('Call to pallet positioning units success');
								exception_msg_log_insert('Call to pallet positioning units success',
																																v_func_name,
																																p_shipment_tc_id,
																																'INFO');
				  END IF;
				else
				  --dbms_output.put_line('Call to pallet positioning units failed');
				  exception_msg_log_insert('Call to pallet positioning units failed',
																												   v_func_name,
																												   p_shipment_tc_id,
																												   'ERROR');
				  -- update shipment
				  update shipment
								set SHIPMENT_REF_ID = 15
				   where tc_shipment_id = p_shipment_tc_id;
				  p_out := 1;

				  return;
				end if;

				IF g_log_level > 0 THEN
				  --dbms_output.put_line('Ending pallet positioning function ' ||
				  --                                                              SYSTIMESTAMP);
				  exception_msg_log_insert('Ending pallet positioning function ' ||
																												   SYSTIMESTAMP,
																												   v_func_name,
																												   p_shipment_tc_id,
																												   'INFO');
				END IF;
				--dbms_output.put_line('783');

				-- updating system tables

				begin

				  -- get number of stops
				  select count(distinct facility_id)
				  into v_num_stops
				  from global_main;

				  -- get number of cases , total weight , toatal volume

				  select load_diagram_id
								into v_load_diagram
								from C_LOAD_DIAGRAM_SUMMARY
				   where tc_shipment_id = p_shipment_tc_id;

				  if v_load_diagram is not null then
								delete from C_LOAD_DIAGRAM_ITEM_DETAIL
								where load_diagram_id = v_load_diagram;

								delete from C_LOAD_DIAGRAM_POSN
								where load_diagram_id = v_load_diagram;

								delete from C_LOAD_DIAGRAM_SUMMARY
								where load_diagram_id = v_load_diagram;

								select sum(order_qty_assigned)
								  into v_overflow_cases
								  from temp_hold_delivery_units
								where header_flag = 'N'
								   and cmpt_id = 00;
								/* Change Start: SL 300413
								select sum(order_qty_assigned)
								  into v_assigned_cases
								  from temp_hold_delivery_units
								where header_flag = 'N'
								   and assigned_wght <> 0
								   and cmpt_id <> 0;
								*/
								v_assigned_cases := g_orig_order_qty - v_overflow_cases;
								/* Change End : SL 300413 */
								insert into C_LOAD_DIAGRAM_SUMMARY
								  (LOAD_DIAGRAM_ID,
								   ROUTE_ID,
								   TC_SHIPMENT_ID,
								   TRLR_TYPE,
								   NUM_STOPS,
								   TOTAL_CASES,
								   TOTAL_WGT,
								   TOTAL_VOL,
								   ASSIGNED_CASES,
								   OVERFLOW_CASES,
								   EDIT_USER_ID,
								   CREATED_DTTM,
								   LAST_UPDATED_DTTM,
								   USER_ID)
								values
								  (seq_load_diagram_id.nextval,
								   v_route_id,
								   p_shipment_tc_id,
								   g_equipment_id,
								   v_num_stops,
								   g_orig_order_qty,
								   g_total_wght,
								   g_total_vol,
								   v_assigned_cases,
								   v_overflow_cases,
								   null,
								   SYSDATE,
								   SYSDATE,
								   g_user_id);

								-- insert in detail table
								insert into c_load_diagram_item_detail
								(load_diagram_item_detail_id,load_diagram_id,tc_order_id,
								  stop_number,line_item_id,item_id,item_name,load_type,
								  storage_type,compartment,trailer_position,num_cases,
								  allocated_cases,shorted_cases,volume,weight,created_dttm,
								  last_updated_dttm,user_id,product_type)
								  select SEQ_LOAD_DIAGRAM_ITEM_DETL_ID.nextval,
																seq_load_diagram_id.currval,
																thdu.tc_order_id,
																thdu.order_loading_seq,
																thdu.line_item_id,
																thdu.item_id,
																thdu.item_name,
																thdu.load_type,
																thdu.storage_type,
																thdu.cmpt_id,
																thdu.trailer_pos,
																thdu.order_qty_assigned,
																--thdu.total_order_qty,
																0,
																0,
																thdu.unit_vol * thdu.order_qty_assigned,
																thdu.unit_wt  * thdu.order_qty_assigned,
																SYSDATE,
																SYSDATE,
																g_user_id,
																cpt.prod_type
												from temp_hold_delivery_units thdu, c_prod_type cpt
								   where thdu.category_id = cpt.category_id
												and thdu.header_flag = 'N'
												and thdu.order_qty_assigned <> 0;

								insert into c_load_diagram_posn
								  select SEQ_LOAD_DIAGRAM_POSN_ID.nextval,
																seq_load_diagram_id.currval,
																s.trailer_position,
																'N',
																'N',
																'N',
																'N',
																'N',
																SYSDATE,
																SYSDATE,
																g_user_id
												from (select trailer_position
																				from C_EQUIPMENT_CMPT_dtl
																   where eq_cmpt_id in
																								(select eq_cmpt_id
																												from C_EQUIPMENT_CMPT
																								   where equipment_id = g_equipment_id)
																   GROUP BY trailer_position) s;

				  end if;
				exception
				  when no_data_found then

								select sum(order_qty_assigned)
								  into v_overflow_cases
								  from temp_hold_delivery_units
								where header_flag = 'N'
								   and cmpt_id = 00;
								/* change start 30042013
								select sum(order_qty_assigned)
								  into v_assigned_cases
								  from temp_hold_delivery_units
								where header_flag = 'N'
								   and assigned_wght <> 0
								   and cmpt_id <> 0;
								*/
								v_assigned_cases := g_orig_order_qty - v_overflow_cases;
								/* Change End : SL 300413 */
								-- insert in header
								insert into C_LOAD_DIAGRAM_SUMMARY
								  (LOAD_DIAGRAM_ID,
								   ROUTE_ID,
								   TC_SHIPMENT_ID,
								   TRLR_TYPE,
								   NUM_STOPS,
								   TOTAL_CASES,
								   TOTAL_WGT,
								   TOTAL_VOL,
								   ASSIGNED_CASES,
								   OVERFLOW_CASES,
								   EDIT_USER_ID,
								   CREATED_DTTM,
								   LAST_UPDATED_DTTM,
								   USER_ID)
								values
								  (seq_load_diagram_id.nextval,
								   v_route_id,
								   p_shipment_tc_id,
								   g_equipment_id,
								   v_num_stops,
								   g_orig_order_qty,
								   g_total_wght,
								   g_total_vol,
								   v_assigned_cases,
								   v_overflow_cases,
								   null,
								   SYSDATE,
								   SYSDATE,
								   g_user_id);

								-- insert in detail
								insert into c_load_diagram_item_detail
								(load_diagram_item_detail_id,load_diagram_id,tc_order_id,
								  stop_number,line_item_id,item_id,item_name,load_type,
								  storage_type,compartment,trailer_position,num_cases,
								  allocated_cases,shorted_cases,volume,weight,created_dttm,
								  last_updated_dttm,user_id,product_type)
								  select SEQ_LOAD_DIAGRAM_ITEM_DETL_ID.nextval,
																seq_load_diagram_id.currval,
																thdu.tc_order_id,
																thdu.order_loading_seq,
																thdu.line_item_id,
																thdu.item_id,
																thdu.item_name,
																thdu.load_type,
																thdu.storage_type,
																thdu.cmpt_id,
																thdu.trailer_pos,
																thdu.order_qty_assigned,
																--thdu.total_order_qty,
																0,
																0,
																thdu.unit_vol * thdu.order_qty_assigned,
																thdu.unit_wt  * thdu.order_qty_assigned,
																SYSDATE,
																SYSDATE,
																g_user_id,
																cpt.prod_type
												from temp_hold_delivery_units thdu, c_prod_type cpt
								   where thdu.category_id = cpt.category_id
												and thdu.header_flag = 'N'
												and thdu.order_qty_assigned <> 0;

								insert into c_load_diagram_posn
								  select SEQ_LOAD_DIAGRAM_POSN_ID.nextval,
																seq_load_diagram_id.currval,
																s.trailer_position,
																'N',
																'N',
																'N',
																'N',
																'N',
																SYSDATE,
																SYSDATE,
																g_user_id
												from (select trailer_position
																				from C_EQUIPMENT_CMPT_dtl
																   where eq_cmpt_id in
																								(select eq_cmpt_id
																												from C_EQUIPMENT_CMPT
																								   where equipment_id = g_equipment_id)
																   GROUP BY trailer_position) s;

				  when others then
								g_msg := 'exception while updating WM01 system tables' || SQLCODE ||
																SQLERRM;
								exception_msg_log_insert(g_msg,
																																v_func_name,
																																p_shipment_tc_id,
																																'ERROR');
								-- update shipment
								update shipment
								   set SHIPMENT_REF_ID = 15
								where tc_shipment_id = p_shipment_tc_id;
								p_out := 1;

								return;
				end;
				-- update c_load_diagram_posn

				-- update shipment
				update shipment
				   set SHIPMENT_REF_ID = 20
				where tc_shipment_id = p_shipment_tc_id;

				-- update orders
				update orders
				   set REF_FIELD_1 = 20
				where order_id in (select order_id from global_main);
				IF g_log_level > 0 THEN
				  g_msg := 'Exiting - c_generate_load_diagram status - ' || p_out || '  ' ||
												   SYSTIMESTAMP;
				  exception_msg_log_insert(g_msg,
																												   v_func_name,
																												   p_shipment_tc_id,
																												   'INFO');
				END IF;

				-- assigning bulk flags on positions
				-- change start : SL Straight line at the bottom of the last occupied row

				select seq_load_diagram_id.currval
				into v_seq_current_value
				from dual;

				/*  Start merging duplicate items - OP ??    */
				insert into c_temp_hold_merge
				(load_diagram_item_detail_id,
load_diagram_id,tc_order_id,stop_number,line_item_id,item_id,item_name,load_type,storage_type,compartment,trailer_position,
num_cases,allocated_cases,shorted_cases,volume,
weight,created_dttm,last_updated_dttm,
				user_id,product_type,parent_id )
select c.load_diagram_item_detail_id,
c.load_diagram_id,c.tc_order_id,c.stop_number,c.line_item_id,c.item_id,c.item_name,
c.load_type,c.storage_type,c.compartment,c.trailer_position,
--            s.total,c.allocated_cases,c.shorted_cases,c.volume,
c.num_cases,c.allocated_cases,c.shorted_cases,c.volume,
c.weight,c.created_dttm,c.last_updated_dttm,c.user_id,c.product_type,c.parent_id
from  c_load_diagram_item_detail c,
(
select c1.rowid r, c1.tc_order_id tc_order_id,c1.line_item_id line_item_id,c1.trailer_position trailer_position, c2.total total 
from c_load_diagram_item_detail c1,
(select tc_order_id ,line_item_id,trailer_position,sum(num_cases) total
from c_load_diagram_item_detail
where load_diagram_id = v_seq_current_value
group by tc_order_id, line_item_id, trailer_position
having count(*) > 1) c2
where c1.tc_order_id = c1.tc_order_id
and c1.line_item_id=c2.line_item_id
and c1.trailer_position=c2.trailer_position

--              (select min(rowid) r,tc_order_id ,line_item_id,trailer_position, sum(num_cases) total
--                 from c_load_diagram_item_detail
--                where load_diagram_id = v_seq_current_value
--                group by tc_order_id, line_item_id, trailer_position
--                having count(*) > 1)             
) s
where c.rowid = s.r;

delete from c_load_diagram_item_detail c
where exists (select 1
from c_temp_hold_merge ct
where   c.tc_order_id = ct.tc_order_id
and   c.line_item_id = ct.line_item_id
and   c.trailer_position = ct.trailer_position
and  ct.load_diagram_id = v_seq_current_value
																)
and   c.load_diagram_id = v_seq_current_value;

insert into C_LOAD_DIAGRAM_ITEM_DETAIL
select * from C_TEMP_HOLD_MERGE;
/* Finish merging duplicate items - OP ??    */
				update c_load_diagram_posn
				   set bulk_head_bottom = 'Y'
				where load_diagram_id = v_seq_current_value
				   and substr(trailer_position,1,2) in (
																																												select substr(max(trailer_pos),1,2)
																																												  from temp_hold_Delivery_units thdu , C_EQUIPMENT_CMPT cec
																																												where thdu.header_flag='Y'
																																												   and cec.equipment_id = g_equipment_id
																																												   and thdu.cmpt_id = cec.eq_cmpt_id
																																												   and draw_bulkead_after = 'Y'
																																												   and cmpt_id <> 0
																																												   group by eq_cmpt_id
																																								  );

				/*
				select num_columns
				  into g_num_columns
				  from c_equipment
				where equipment_id = g_equipment_id;

				select seq_load_diagram_id.currval into v_seq_current_value from dual;

				if (g_num_columns = 2) then
				  for cntr_pos in (select distinct thdu.cmpt_id
																								from temp_hold_delivery_units thdu,
																												  C_EQUIPMENT_CMPT         cec
																								where cec.equipment_id = g_equipment_id
																								  and thdu.cmpt_id = cec.eq_cmpt_id
																								  and draw_bulkead_after = 'Y'
																								  and cmpt_id <> 0) loop

								select s.pos, s.next_v
								  into v_prev_position, v_next_position
								  from (select inq.trailer_pos pos,
																				   lead(inq.trailer_pos, 1) over(ORDER BY inq.trailer_pos) AS next_v
																  from (select distinct trailer_pos
																								  from temp_hold_delivery_units
																								where cmpt_id = cntr_pos.cmpt_id
																								order by trailer_pos desc) inq
																where rownum < 3) s
								where rownum < 2;

								if (substr(v_prev_position, 1, 2) = substr(v_next_position, 1, 2)) then
								  update c_load_diagram_posn
												set bulk_head_bottom = 'Y'
								   where load_diagram_id = v_seq_current_value
												and trailer_position in (v_prev_position, v_next_position);

								else
								  -- left position is greater
								  -- update left position
								  update c_load_diagram_posn
												set bulk_head_bottom = 'Y', bulk_head_right = 'Y'
								   where load_diagram_id = v_seq_current_value
												and trailer_position = v_next_position;

								  -- update right position
								  update c_load_diagram_posn
												set bulk_head_bottom = 'Y'
								   where load_diagram_id = v_seq_current_value
												and trailer_position = v_prev_position;
								end if;
				  end loop;
				end if;
*/
				-- change end : SL

				-- insert dummy overflow position
				if (g_overflow_flag = 1) then
				  insert into c_load_diagram_posn
								(LOAD_DIAGRAM_POSN_ID,
								LOAD_DIAGRAM_ID,
								TRAILER_POSITION,
								BULK_HEAD_TOP,
								BULK_HEAD_LEFT,
								BULK_HEAD_RIGHT,
								BULK_HEAD_BOTTOM,
								EQUIP_POSN,
								CREATED_DTTM,
								LAST_UPDATED_DTTM,
								USER_ID)
				  values
								(SEQ_LOAD_DIAGRAM_POSN_ID.nextval,
								seq_load_diagram_id.currval,
								'00',
								'N',
								'N',
								'N',
								'N',
								'N',
								SYSDATE,
								SYSDATE,
								g_user_id);

				end if;

				commit;

  EXCEPTION
				WHEN OTHERS THEN
				  g_msg := 'Exception in C_GENERATE_LOAD_DIAGRAM function' || SQLCODE ||
												   SQLERRM;
				  exception_msg_log_insert(g_msg,
																												   v_func_name,
																												   p_shipment_tc_id,
																												   'ERROR');
				  -- update shipment
				  update shipment
								set SHIPMENT_REF_ID = 15
				   where tc_shipment_id = p_shipment_tc_id;
				  p_out := 1;
  END C_GENERATE_LOAD_DIAGRAM;

  ----------------------------------------------------------------------------------------
  --  Function Name :  Merge delivery units                                               --
  --  Description   : This function takes as input the delivery units and tries to merge  --
  --                  one delivery unit into another w.r.t                                --
  --                  max weight and volume considerations                                --
  ---------------------------------------------------------------------------------------

  FUNCTION merge_delivery_units(p_deliv number) return BOOLEAN IS
				-- select all the orders and order by fill rate desc
				cursor c_outer_dl_units is
				  select DELIV_ID,
												DELIV_UNIT_NUMBER,
												TC_ORDER_ID,
												ITEM_ID,
												ORDER_QTY_ASSIGNED,
												TOTAL_ORDER_QTY,
												ORDER_WT,
												ORDER_VOL,
												MAX_VOLUME,
												MIN_VOL,
												MAX_WGHT,
												MIN_WGHT,
												ASSIGNED_WGHT,
												ASSIGNED_VOL,
												MIN_NUM_CASE,
												MAX_NUM_CASE,
												HEADER_FLAG,
												VOL_RANGE,
												WGHT_RANGE,
												CMPT_ID,
												TRAILER_POS,
												EQUIPMENT_ID,
												STORAGE_TYPE,
												OVERFLOW_RANK,
												ORDER_ID,
												SHIPMENT_ID,
												FACILITY_ID,
												LOAD_TYPE,
												BULK_FLAG,
												ORDER_TYPE,
												LINE_ITEM_ID,
												ITEM_NAME,
												ORDER_LOADING_SEQ,
												CATEGORY_ID,
												MAX_UTIL
								from temp_hold_delivery_units
				   where header_flag = 'Y'
								and deliv_id = p_deliv
								and bulk_flag = 'N'
				   order by (((assigned_wght / max_wght) + (assigned_vol / max_volume) +
																abs((assigned_wght / max_wght) -
																				  (assigned_vol / max_volume))) * 0.5) desc;

				type list_hold_units_obj is table of c_outer_dl_units%rowtype INDEX BY PLS_INTEGER;
				list_hold_units list_hold_units_obj;

				cursor c_push_dl_unit(p_deliv number) is
				  select DELIV_ID,
												DELIV_UNIT_NUMBER,
												TC_ORDER_ID,
												ITEM_ID,
												ORDER_QTY_ASSIGNED,
												TOTAL_ORDER_QTY,
												ORDER_WT,
												ORDER_VOL,
												MAX_VOLUME,
												MIN_VOL,
												MAX_WGHT,
												MIN_WGHT,
												ASSIGNED_WGHT,
												ASSIGNED_VOL,
												MIN_NUM_CASE,
												MAX_NUM_CASE,
												HEADER_FLAG,
												VOL_RANGE,
												WGHT_RANGE,
												CMPT_ID,
												TRAILER_POS,
												EQUIPMENT_ID,
												STORAGE_TYPE,
												OVERFLOW_RANK,
												ORDER_ID,
												SHIPMENT_ID,
												FACILITY_ID,
												LOAD_TYPE,
												BULK_FLAG,
												ORDER_TYPE,
												LINE_ITEM_ID,
												ITEM_NAME,
												ORDER_LOADING_SEQ,
												CATEGORY_ID,
												MAX_UTIL
								from temp_hold_delivery_units
				   where deliv_unit_number = p_deliv
								and header_flag = 'Y';

				-- list to hold the merged delivery units
				type list_merge_units_obj is table of c_outer_dl_units%rowtype INDEX BY PLS_INTEGER;
				list_merge_units list_merge_units_obj;

				v_func_name          msg_log.func_name%TYPE := 'MERGE_DELIVERY_UNITS';
				v_merge_unit_counter number := 1;
				v_merge_flag         number := 1;

				-- Table to hold records of deliv unit number which are to be deleted

				TYPE merge_dl_table_obj is TABLE OF NUMBER INDEX BY PLS_INTEGER;

				merge_dl_table         merge_dl_table_obj;
				merge_dl_table_counter number := 1;

  BEGIN

				-- re set the flags
				v_merge_unit_counter   := 1;
				merge_dl_table_counter := 1;

				--dbms_output.put_line('line 440');

				--dbms_output.put_line('line 450');
				open c_outer_dl_units;
				fetch c_outer_dl_units bulk collect
				  into list_hold_units;
				close c_outer_dl_units;
				--dbms_output.put_line('line 454');

				--dbms_output.put_line(p_deliv);
				--dbms_output.put_line('count of delivery units in the outer loop' || list_hold_units.count);

				-- loop over the entire dl units in this group
				for list_cntr in 1 .. list_hold_units.count loop

				  --dbms_output.put_line('list_cntr'||list_cntr);

				  v_merge_flag := 1;

				  --dbms_output.put_line('coming on 456');
				  --dbms_output.put_line('list_cntr'||list_cntr);

				  if list_cntr = 1 then
								--dbms_output.put_line('coming in the if condition');
								-- list_merge_units is empty for the first loop .. so initialize here
								open c_push_dl_unit(list_hold_units(list_cntr).deliv_unit_number);
								fetch c_push_dl_unit
								  into list_merge_units(v_merge_unit_counter);
								close c_push_dl_unit;
								v_merge_unit_counter := v_merge_unit_counter + 1;
								continue;
				  end if;

				  --dbms_output.put_line('coming outside the if condition');

				  for merge_cntr in 1 .. list_merge_units.count loop
								--dbms_output.put_line('coming in the merge loop' ||
								--                                                                             list_merge_units(merge_cntr)
								--                                                                             .deliv_unit_number);
								If ((list_merge_units(merge_cntr)
								   .assigned_wght + list_hold_units(list_cntr)
								   .assigned_wght < list_merge_units(merge_cntr).max_wght) AND
								   (list_merge_units(merge_cntr)
								   .assigned_vol + list_hold_units(list_cntr)
								   .assigned_vol < list_merge_units(merge_cntr).max_volume)) then

								  --dbms_output.put_line('pushing the dl unit'||list_hold_units(list_cntr).deliv_unit_number);
								  -- push the outer dl unit in the current merge dl unit
								  list_merge_units(merge_cntr).assigned_wght := list_merge_units(merge_cntr)
																																																				   .assigned_wght +
																																																								list_hold_units(list_cntr)
																																																				   .assigned_wght;
								  list_merge_units(merge_cntr).assigned_vol := list_merge_units(merge_cntr)
																																																				  .assigned_vol +
																																																				   list_hold_units(list_cntr)
																																																				  .assigned_vol;

								  -- save  the delivery unit number.. delete it outside the loop
								  merge_dl_table(merge_dl_table_counter) := list_hold_units(list_cntr)
																																																   .deliv_unit_number;
								  merge_dl_table_counter := merge_dl_table_counter + 1;

								  -- update dl unit details

								  update temp_hold_delivery_units
												set deliv_unit_number = list_merge_units(merge_cntr)
																																				.deliv_unit_number,
																min_num_case      = list_merge_units(merge_cntr)
																																				.min_num_case,
																max_num_case      = list_merge_units(merge_cntr)
																																				.max_num_case,
																vol_range         = list_merge_units(merge_cntr).vol_range,
																wght_range        = list_merge_units(merge_cntr).wght_range
								   where deliv_unit_number = list_hold_units(list_cntr)
								  .deliv_unit_number
												and header_flag = 'N';

								  v_merge_flag := 0;
								end if;

								if v_merge_flag = 0 then
								  exit;
								end if;
				  end loop;
				  --dbms_output.put_line('coming on 493');
				  -- if merge flag is false than add the dl unit to the merged list
				  if v_merge_flag = 1 then
								open c_push_dl_unit(list_hold_units(list_cntr).deliv_unit_number);
								fetch c_push_dl_unit
								  into list_merge_units(v_merge_unit_counter);
								close c_push_dl_unit;
								v_merge_unit_counter := v_merge_unit_counter + 1;
				  end if;
				end loop;

				forall i in 1 .. merge_dl_table.count
				  delete from temp_hold_delivery_units
				   where deliv_unit_number = merge_dl_table(i)
								and header_flag = 'Y';

				-- update the new assigned wt and vol after merge

				forall i in 1 .. list_merge_units.count
				  update temp_hold_delivery_units
								set assigned_wght = list_merge_units(i).assigned_wght,
												assigned_vol  = list_merge_units(i).assigned_vol
				   where deliv_unit_number = list_merge_units(i).deliv_unit_number;

				return true;
  EXCEPTION
				WHEN OTHERS THEN
				  g_msg := 'Exception in merge_delivery_units function' || SQLCODE ||
												   SQLERRM;
				  exception_msg_log_insert(g_msg,
																												   v_func_name,
																												   'Delivery Id: ' || p_deliv,
																												   'ERROR');
				  return false;
  end merge_delivery_units;

  ---------------------------------------------------------------------------------------------------------
  --  Function Name : Balance Fit                                                                          --
  --  Description   : This function forms the core logic of delivery unit creation .                       --
  --                  This will take input as order lines and form delivery units for them                 --
  --                  For more details on design refer to WM01-Load-Diagramming-Science-Design-v9          --
  ---------------------------------------------------------------------------------------------------------

  Function balance_fit(i_shipment_n varchar2,
																				   i_order_type varchar2,
																				   i_deliv_id   number,
																				   i_load_type  number) return BOOLEAN IS

				-- variables to point to head and tail of order list
				h_order_list      number(38);
				l_order_list      number(38);
				orderCasesThatFit number := 0;

				-- variables to hold delivery units specification
v_ovrd_prod_type   varchar2(500);
				min_unit           number := 0;
				max_unit           number := 0;
				num_unit           number := 0;
				case_per_unit      number := 0;
				remaining_qty      number := 0;
				cntr_order         number;
				remaining_weight   number;
				remaining_volume   number;
				v_base_units       number := 0;
				v_sum_volume       number := 0;
				v_sum_weight       number := 0;
				v_adjusted_weight  number := 0;
				v_adjusted_volume  number := 0;
				v_avg_weight       number := 0 ;
				v_avg_volume       number := 0;
				v_max_cntr_vol     number;
				v_max_cntr_wght    number;
				v_func_name      msg_log.func_name%TYPE := 'BALANCE_FIT';
				v_missing_prameters  varchar2(32000);

				cursor c_order(ship_n varchar2, deliv_n number) is
				  select category_id,
												load_type,
												storage_type,
												tc_order_id,
												item_id,
												ORIG_ORDER_QTY,
												(unit_wt * orig_order_qty) order_wt,
												(unit_vol * orig_order_qty) order_vol,
												facility_id,
												dsg_equipment_id,
												shipment_id,
												order_id,
												rowid,
												group_as_family,
												order_type,
												line_item_id,
												item_name,
												order_loading_seq,
												unit_wt,
												unit_vol
								from global_main
				   where deliv_id = deliv_n
												--shipment_no = ship_n
												-- changing cursor to handle case for base item where a single order quantity is exceeding load_type.max_case
								and bulk_process = 'N'
								and ((order_type = 'O') or (order_type = 'B' and processed = 0))
				   order by order_vol desc,order_wt desc;

				cursor c_order_non_base(i_ship_n varchar2, i_deliv_n number, i_load_t varchar2) is
				  select category_id,
												load_type,
												storage_type,
												tc_order_id,
												item_id,
												ORIG_ORDER_QTY,
												(unit_wt * orig_order_qty) order_wt,
												(unit_vol * orig_order_qty) order_vol,
												facility_id,
												dsg_equipment_id,
												shipment_id,
												order_id,
												rowid,
												group_as_family,
												order_type,
												line_item_id,
												item_name,
												order_loading_seq,
												unit_wt,
												unit_vol
								from global_main
				   where deliv_id = i_deliv_n
												--shipment_no = i_ship_n
								and load_type = i_load_type
								and bulk_process = 'N'
								and order_type = 'N'
				  --and processed is NULL
				   order by order_vol desc,order_wt desc;

				TYPE order_list IS TABLE OF c_order%ROWTYPE INDEX BY PLS_INTEGER;

				ord1 order_list;

				-- flag to be used for checking if a delivery unit was found
				flag           number := 1;
				deliv_loop_val number := 0;

  BEGIN

				-- check if call for non base order line only
				if i_order_type = 'N' then
				  open c_order_non_base(i_shipment_n, i_deliv_id, i_load_type);
				  fetch c_order_non_base bulk collect
								into ord1;
				  close c_order_non_base;
				else
				  open c_order(i_shipment_n, i_deliv_id);
				  fetch c_order bulk collect
								into ord1;
				  close c_order;
				end if;

				-- CR 0701 SL Start
				--Fit units till average wt/vol to ensure even spreading of unit
				/*
				if i_order_type= 'O' then

				  select sum(orig_order_qty*unit_vol),sum(orig_order_qty*unit_wt)
				  into   v_sum_volume,v_sum_weight
				  from   global_main
				  where  deliv_id = i_deliv_id;

				  select count(distinct deliv_unit_number)
				  into   v_base_units
				  from   temp_hold_delivery_units
				  where  deliv_id = i_deliv_id
				  and    bulk_flag <> 'Y';

				  if v_base_units <> 0 then

								v_avg_weight := v_sum_weight/v_base_units;
								v_avg_volume := v_sum_volume/v_base_units;

								-- check if avg_wt/vol is greater than max
								select distinct max_cntr_vol,max_cntr_wght
								into   v_max_cntr_vol,v_max_cntr_wght
								from global_main gb,c_storage_type cst
								where gb.deliv_id = i_deliv_id
								and   cst.storage_type_id = gb.storage_type;

								if  v_avg_weight >= v_max_cntr_wght
								then
								  v_adjusted_weight:=0;
								else
								  v_adjusted_weight := v_max_cntr_wght - v_avg_weight;
								end if;

								if  v_avg_volume >= v_max_cntr_vol
								then
								  v_adjusted_volume:=0;
								else
								  v_adjusted_volume := v_max_cntr_vol - v_avg_volume;
								end if;

				  end if;

				end if;

				v_adjusted_volume := 0;
				v_adjusted_weight := 0;
				*/
				-- CR0701 SL End
				-- commenting out the above change as we have changed the ranging algo
				-- we do not need to fill till average

				h_order_list := ord1.first;
				l_order_list := ord1.last;

				-- loop through all the orders
				for i in 1 .. ord1.count() loop
				  IF g_log_level > 0 THEN
								--dbms_output.put_line('coming in balanaced best fit' || ord1(i)
								--                                                                             .tc_order_id);
								exception_msg_log_insert('Coming in balanced best fit ' || ord1(i)
																																.tc_order_id,
																																v_func_name,
																																i_shipment_n,
																																'INFO');
				  END IF;
				  flag := 1;
				  IF g_log_level = 2 THEN
								--dbms_output.put_line('coming in balanced best fit loop');
								exception_msg_log_insert('Coming in balanced best fit loop',
																																v_func_name,
																																i_shipment_n,
																																'DEBUG');
				  END IF;
				  if (i mod 2 <> 0) then
								cntr_order   := h_order_list;
								h_order_list := h_order_list + 1;
				  else
								cntr_order   := l_order_list;
								l_order_list := l_order_list - 1;
				  end if;
				  IF g_log_level = 2 THEN
								--dbms_output.put_line(cntr_order);
								exception_msg_log_insert('Cntr Order: ' || cntr_order,
																																v_func_name,
																																i_shipment_n,
																																'DEBUG');
				  END IF;

				  for j in 1 .. deliv_list1.count() loop
								if i_order_type = 'N' AND deliv_list1(j).load_type <> i_load_type then
								  -- this is call to balanced fit is only for non base and only put these items with same load type family
								  continue;
								end if;

								remaining_weight := deliv_list1(j).max_weight - deliv_list1(j)
																								   .assigned_weight-v_adjusted_weight;
								remaining_volume := deliv_list1(j).max_cube - deliv_list1(j)
																								   .assigned_volume-v_adjusted_volume;

								if (((ord1(cntr_order).order_wt - remaining_weight) < 0.001) AND
								   ((ord1(cntr_order).order_vol - remaining_volume) < 0.001)) then
								  deliv_list1(j).assigned_weight := deliv_list1(j).assigned_weight +
																																												ord1(cntr_order).order_wt;
								  deliv_list1(j).assigned_volume := deliv_list1(j).assigned_volume +
																																												ord1(cntr_order).order_vol;
								  IF g_log_level = 2 THEN
												--dbms_output.put_line('!! Not creating a new delivery unit found a match');
												--dbms_output.put_line(' deliv_list1(j).assigned_weight' ||
												--             deliv_list1(j).assigned_weight ||
												--'deliv_list1(j).assigned_volume' ||
												--deliv_list1(j).assigned_weight);
												--dbms_output.put_line('for order qty  ' || ord1(cntr_order)
												--                                                                             .orig_order_qty ||
												--                                                                             'ord1(cntr_order).item_id' ||
												--                                                                             ord1(cntr_order).item_id);
												exception_msg_log_insert('!! Not creating a new delivery unit found a match',
																																				v_func_name,
																																				i_shipment_n,
																																				'DEBUG');
												exception_msg_log_insert(' deliv_list1(j).assigned_weight' ||
																																				deliv_list1(j).assigned_weight ||
																																				'deliv_list1(j).assigned_volume' ||
																																				deliv_list1(j).assigned_weight,
																																				v_func_name,
																																				i_shipment_n,
																																				'DEBUG');
												exception_msg_log_insert('for order qty  ' || ord1(cntr_order)
																																				.orig_order_qty ||
																																				'ord1(cntr_order).item_id' ||
																																				ord1(cntr_order).item_id,
																																				v_func_name,
																																				i_shipment_n,
																																				'DEBUG');
								  END IF;

								  -- Insert the sku in the delivery unit
								  insert into temp_hold_delivery_units
												(c_seq,
												deliv_id,
												deliv_unit_number,
												tc_order_id,
												item_id,
												order_qty_assigned,
												total_order_qty,
												order_wt,
												order_vol,
												max_volume,
												min_vol,
												max_wght,
												min_wght,
												assigned_wght,
												assigned_vol,
												min_num_case,
												max_num_case,
												header_flag,
												vol_range,
												wght_range,
												bulk_flag,
												equipment_id,
												storage_type,
												order_id,
												shipment_id,
												facility_id,
												load_type,
												order_type,
												line_item_id,
												item_name,
												order_loading_seq,
												category_id,
												unit_wt,
												unit_vol)
								  values
												(c_range_seq.nextval,
												deliv_list1(j).deliv_id,
												deliv_list1(j).deliv_unit_number,
												ord1(cntr_order).tc_order_id,
												ord1(cntr_order).item_id,
												ord1(cntr_order).orig_order_qty,
												ord1(cntr_order).orig_order_qty,
												ord1(cntr_order).order_wt,
												ord1(cntr_order).order_vol,
												deliv_list1(j).max_cube,
												deliv_list1(j).min_vol,
												deliv_list1(j).max_weight,
												deliv_list1(j).min_wght,
												0,
												0,
												deliv_list1(j).min_num_case,
												deliv_list1(j).max_num_case,
												'N',
												deliv_list1(j).vol_range,
												deliv_list1(j).wght_range,
												'N',
												ord1(cntr_order).dsg_equipment_id,
												ord1(cntr_order).storage_type,
												ord1(cntr_order).order_id,
												ord1(cntr_order).shipment_id,
												ord1(cntr_order).facility_id,
												ord1(cntr_order).load_type,
												ord1(cntr_order).order_type,
												ord1(cntr_order).line_item_id,
												ord1(cntr_order).item_name,
												ord1(cntr_order).order_loading_seq,
												ord1(cntr_order).category_id,
												ord1(cntr_order).unit_wt,
												ord1(cntr_order).unit_vol);
								  flag := 0;
								  exit;
								end if;

				  end loop; -- end inner delivery list loop

				  -- No delivery unit exist so create one

				  if (flag = 1) then
								open c_deliv_unit(ord1(cntr_order).storage_type,
																								  ord1(cntr_order).tc_order_id,
																								  ord1(cntr_order).item_id,
																								  ord1(cntr_order).load_type);
								fetch c_deliv_unit
								  into deliv_list1(cntr_deliv);
								close c_deliv_unit;

								IF g_log_level = 2 THEN
								  --dbms_output.put_line('min num case ' || deliv_list1(cntr_deliv)
								  --                                                                              .min_num_case || 'max num of case' ||
								  --                                                                              deliv_list1(cntr_deliv).max_num_case);
								  --dbms_output.put_line('ord1(cntr_order).item_id' ||
								  --                                                                              ord1(cntr_order).item_id);

								  exception_msg_log_insert('Min num of cases: ' ||
																																   deliv_list1(cntr_deliv)
																																   .min_num_case ||
																																   ' Max number of cases: ' ||
																																   deliv_list1(cntr_deliv).max_num_case,
																																   v_func_name,
																																   i_shipment_n,
																																   'DEBUG');
								  exception_msg_log_insert('ord1(cntr_order).item_id' ||
																																   ord1(cntr_order).item_id,
																																   v_func_name,
																																   i_shipment_n,
																																   'DEBUG');
								END IF;

								-- Insert header
								insert into temp_hold_delivery_units
								 (c_seq,
								   deliv_id,
								   deliv_unit_number,
								   tc_order_id,
								   item_id,
								   order_qty_assigned,
								   total_order_qty,
								   order_wt,
								   order_vol,
								   max_volume,
								   min_vol,
								   max_wght,
								   min_wght,
								   assigned_wght,
								   assigned_vol,
								   min_num_case,
								   max_num_case,
								   header_flag,
								   vol_range,
								   wght_range,
								   bulk_flag,
								   equipment_id,
								   storage_type,
								   order_id,
								   shipment_id,
								   facility_id,
								   load_type,
								   order_type,
								   category_id)
								values
								  (c_range_seq.nextval,
								   deliv_list1(cntr_deliv).deliv_id,
								   deliv_list1(cntr_deliv).deliv_unit_number,
								   0,
								   0,
								   0,
								   0,
								   0,
								   0,
								   deliv_list1(cntr_deliv).max_cube,
								   deliv_list1(cntr_deliv).min_vol,
								   deliv_list1(cntr_deliv).max_weight,
								   deliv_list1(cntr_deliv).min_wght,
								   0,
								   0,
								   deliv_list1(cntr_deliv).min_num_case,
								   deliv_list1(cntr_deliv).max_num_case,
								   'Y',
								   deliv_list1(cntr_deliv).vol_range,
								   deliv_list1(cntr_deliv).wght_range,
								   'N',
								   ord1(cntr_order).dsg_equipment_id,
								   ord1(cntr_order).storage_type,
								   ord1(cntr_order).order_id,
								   ord1(cntr_order).shipment_id,
								   ord1(cntr_order).facility_id,
								   ord1(cntr_order).load_type,
								   ord1(cntr_order).order_type,
								   ord1(cntr_order).category_id);

								-- calculate the unit specifications

								min_unit      := ceil(ord1(cntr_order)
																												  .ORIG_ORDER_QTY / deliv_list1(cntr_deliv)
																												  .max_num_case);
								max_unit      := floor(ord1(cntr_order)
																												   .ORIG_ORDER_QTY / deliv_list1(cntr_deliv)
																												   .min_num_case);
								num_unit      := min_unit;
								case_per_unit := floor(ord1(cntr_order).ORIG_ORDER_QTY / num_unit);
								remaining_qty := ord1(cntr_order)
																								.ORIG_ORDER_QTY - (num_unit * case_per_unit);

								IF g_log_level = 2 THEN
								  --dbms_output.put_line('remaining_qty' || remaining_qty ||
								  --                                                              'case_per_unit' || case_per_unit);
								  exception_msg_log_insert('remaining_qty' || remaining_qty ||
																																   'case_per_unit' || case_per_unit,
																																   v_func_name,
																																   i_shipment_n,
																																   'DEBUG');
								END IF;
								-- fit units in this delivery unit
								-- increase the assigned value of the delivery unit to order cases that are being fitted. Multiply cases per unit by unit ord qty and unit ord vol
								deliv_list1(cntr_deliv).assigned_volume := case_per_unit *
																																																   (ord1(cntr_order)
																																																   .order_vol /
																																																				ord1(cntr_order)
																																																   .orig_order_qty);
								deliv_list1(cntr_deliv).assigned_weight := case_per_unit *
																																																   (ord1(cntr_order)
																																																   .order_wt /
																																																				ord1(cntr_order)
																																																   .orig_order_qty);

								--dbms_output.put_line('222');

								-- Insert the sku in this delivery unit
								insert into temp_hold_delivery_units
								  (c_seq,
								   deliv_id,
								   deliv_unit_number,
								   tc_order_id,
								   item_id,
								   order_qty_assigned,
								   total_order_qty,
								   order_wt,
								   order_vol,
								   max_volume,
								   min_vol,
								   max_wght,
								   min_wght,
								   assigned_wght,
								   assigned_vol,
								   min_num_case,
								   max_num_case,
								   header_flag,
								   vol_range,
								   wght_range,
								   bulk_flag,
								   equipment_id,
								   storage_type,
								   order_id,
								   shipment_id,
								   facility_id,
								   load_type,
								   order_type,
								   line_item_id,
								   item_name,
								   order_loading_seq,
								   category_id,
								   unit_wt,
								   unit_vol)
								values
								  (c_range_seq.nextval,
								   deliv_list1(cntr_deliv).deliv_id,
								   deliv_list1(cntr_deliv).deliv_unit_number,
								   ord1(cntr_order).tc_order_id,
								   ord1(cntr_order).item_id,
								   case_per_unit,
								   ord1(cntr_order).orig_order_qty,
								   ord1(cntr_order).order_wt,
								   ord1(cntr_order).order_vol,
								  deliv_list1(cntr_deliv).max_cube,
								   deliv_list1(cntr_deliv).min_vol,
								   deliv_list1(cntr_deliv).max_weight,
								   deliv_list1(cntr_deliv).min_wght,
								   0,
								   0,
								   deliv_list1(cntr_deliv).min_num_case,
								   deliv_list1(cntr_deliv).max_num_case,
								   'N',
								   deliv_list1(cntr_deliv).vol_range,
								   deliv_list1(cntr_deliv).wght_range,
								   'N',
								   ord1(cntr_order).dsg_equipment_id,
								   ord1(cntr_order).storage_type,
								   ord1(cntr_order).order_id,
								   ord1(cntr_order).shipment_id,
								   ord1(cntr_order).facility_id,
								   ord1(cntr_order).load_type,
								   ord1(cntr_order).order_type,
								   ord1(cntr_order).line_item_id,
								   ord1(cntr_order).item_name,
								   ord1(cntr_order).order_loading_seq,
								   ord1(cntr_order).category_id,
								   ord1(cntr_order).unit_wt,
								   ord1(cntr_order).unit_vol);
								-- end of creating new delivery unit
								cntr_deliv := cntr_deliv + 1;
								-- already creating one delivery unit on entering now we need to create min_unit - 1 ceil(700/3) = 3..created one on top now create only two more
								min_unit := min_unit - 1;

								for loop_var in 1 .. min_unit loop
								  -- will be using this counter to fit in the remaining qty
								  deliv_loop_val := loop_var + 1;
								  -- making a new delivery unit
								  open c_deliv_unit(ord1(cntr_order).storage_type,
																												ord1(cntr_order).tc_order_id,
																												ord1(cntr_order).item_id,
																												ord1(cntr_order).load_type);
								  fetch c_deliv_unit
												into deliv_list1(cntr_deliv);
								  close c_deliv_unit;

								  -- new delivery unit make a header entry
								  insert into temp_hold_delivery_units
												(c_seq,
												deliv_id,
												deliv_unit_number,
												tc_order_id,
												item_id,
												order_qty_assigned,
												total_order_qty,
												order_wt,
												order_vol,
												max_volume,
												min_vol,
												max_wght,
												min_wght,
												assigned_wght,
												assigned_vol,
												min_num_case,
												max_num_case,
												header_flag,
												vol_range,
												wght_range,
												bulk_flag,
												equipment_id,
												storage_type,
												order_id,
												shipment_id,
												facility_id,
												load_type,
												order_type,
												category_id)
								  values
												(c_range_seq.nextval,
												deliv_list1(cntr_deliv).deliv_id,
												deliv_list1(cntr_deliv).deliv_unit_number,
												0,
												0,
												0,
												0,
												0,
												0,
												deliv_list1(cntr_deliv).max_cube,
												deliv_list1(cntr_deliv).min_vol,
												deliv_list1(cntr_deliv).max_weight,
												deliv_list1(cntr_deliv).min_wght,
												0,
												0,
												deliv_list1(cntr_deliv).min_num_case,
												deliv_list1(cntr_deliv).max_num_case,
												'Y',
												deliv_list1(cntr_deliv).vol_range,
												deliv_list1(cntr_deliv).wght_range,
												'N',
												ord1(cntr_order).dsg_equipment_id,
												ord1(cntr_order).storage_type,
												ord1(cntr_order).order_id,
												ord1(cntr_order).shipment_id,
												ord1(cntr_order).facility_id,
												ord1(cntr_order).load_type,
												ord1(cntr_order).order_type,
												ord1(cntr_order).category_id);

								  if (deliv_loop_val <= remaining_qty + 1) then
												orderCasesThatFit := case_per_unit + 1;
								  else
												orderCasesThatFit := case_per_unit;
								  end if;

								  -- increase the assigned value of the delivery unit to order cases that are being fitted. formula = order cases that are being fitted * unitwt or unit vol

								  deliv_list1(cntr_deliv).assigned_volume := orderCasesThatFit *
																																																				(ord1(cntr_order)
																																																				.order_vol /
																																																				  ord1(cntr_order)
																																																				.orig_order_qty);
								  deliv_list1(cntr_deliv).assigned_weight := orderCasesThatFit *
																																																				(ord1(cntr_order)
																																																				.order_wt /
																																																				  ord1(cntr_order)
																																																				.orig_order_qty);

								  -- Insert the sku in this delivery unit
								  insert into temp_hold_delivery_units
												(c_seq,
												deliv_id,
												deliv_unit_number,
												tc_order_id,
												item_id,
												order_qty_assigned,
												total_order_qty,
												order_wt,
												order_vol,
												max_volume,
												min_vol,
												max_wght,
												min_wght,
												assigned_wght,
												assigned_vol,
												min_num_case,
												max_num_case,
												header_flag,
												vol_range,
												wght_range,
												equipment_id,
												storage_type,
												order_id,
												shipment_id,
												facility_id,
												load_type,
												bulk_flag,
												order_type,
												line_item_id,
												item_name,
												order_loading_seq,
												category_id,
												unit_wt,
												unit_vol)
								  values
												(c_range_seq.nextval,
												deliv_list1(cntr_deliv).deliv_id,
												deliv_list1(cntr_deliv).deliv_unit_number,
												ord1(cntr_order).tc_order_id,
												ord1(cntr_order).item_id,
												orderCasesThatFit,
												ord1(cntr_order).orig_order_qty,
												ord1(cntr_order).order_wt,
												ord1(cntr_order).order_vol,
												deliv_list1(cntr_deliv).max_cube,
												deliv_list1(cntr_deliv).min_vol,
												deliv_list1(cntr_deliv).max_weight,
												deliv_list1(cntr_deliv).min_wght,
												0,
												0,
												deliv_list1(cntr_deliv).min_num_case,
												deliv_list1(cntr_deliv).max_num_case,
												'N',
												deliv_list1(cntr_deliv).vol_range,
												deliv_list1(cntr_deliv).wght_range,
												ord1(cntr_order).dsg_equipment_id,
												ord1(cntr_order).storage_type,
												ord1(cntr_order).order_id,
												ord1(cntr_order).shipment_id,
												ord1(cntr_order).facility_id,
												ord1(cntr_order).load_type,
												'N',
												ord1(cntr_order).order_type,
												ord1(cntr_order).line_item_id,
												ord1(cntr_order).item_name,
												ord1(cntr_order).order_loading_seq,
												ord1(cntr_order).category_id,
												ord1(cntr_order).unit_wt,
												ord1(cntr_order).unit_vol);
								  -- end of creating new delivery unit
								  cntr_deliv := cntr_deliv + 1;
								end loop;
				  end if;

				-- update the global table with the final assigned values from step 1

				end loop; -- ending order loop

				--   FORALL upd_counter in 1 .. deliv_list1.count()
				--    update temp_hold_delivery_units
				--     set assigned_wght =  deliv_list1(upd_counter).assigned_weight,
				--       assigned_vol = deliv_list1(upd_counter).assigned_volume
				--where deliv_unit_number=deliv_list1(upd_counter).deliv_unit_number;

				return true;
  EXCEPTION
				  WHEN ZERO_DIVIDE THEN

				select a||','||B||','||C||','||D||','||E||','|| F
				into    v_missing_prameters
				from
				(
								select case when MAX_CNTR_VOL is null or MAX_CNTR_VOL = 0
								then 'MAX CNTR VOL'
								end a,
								case when MIN_CNTR_VOL is null or MIN_CNTR_VOL = 0
								then 'MIN CNTR VOL'
								end b,
								case when MAX_CNTR_WGHT is null or MAX_CNTR_WGHT = 0
								then 'MAX CNTR WGHT'
								end c,
								case when MIN_CNTR_WGHT is null or MIN_CNTR_WGHT = 0
								then 'MIN CNTR WGHT'
								end D,
								case when VOL_RANGE is null or VOL_RANGE = 0
								then 'VOL RANGE'
								end E,
								case when wght_range is null or wght_range = 0
								then 'WGHT_RANGE'
								end f
				  from C_STORAGE_TYPE
				  where storage_type_id = ord1(cntr_order).storage_type
				);

				   select prod_type
				   into   v_ovrd_prod_type
				   from   c_prod_type
				   where  category_id = ord1(cntr_order).storage_type;
				   --dbms_output.put_line('coming here');
				   insert into msg_log
				  (msg_log_id,module,msg_id,pgm_id,
				   sub_pgm_name,msg,create_date_time,
				   user_id,cd_master_id,log_date_time,
				   mod_date_time,whse,func_name,ref_value_1
				   )
				  select MSG_LOG_ID_SEQ.NEXTVAL,'CUST','0106',
												'Generate Load Diagram','ERROR','Storage Type capacity configuration missing for '||v_ovrd_prod_type,
												sysdate,g_user_id,g_company_id,
												sysdate,sysdate,G_WAREHOUSE,'BALANCE FIT',
												i_shipment_n
								from dual;

								return false;
				WHEN OTHERS THEN
				  g_msg := 'Exception in balanced best fit function' || SQLCODE ||
												   SQLERRM;
				  exception_msg_log_insert(g_msg, v_func_name, i_shipment_n, 'ERROR');
				  return false;
  END balance_fit;

  ---------------------------------------------------------------------------------------------------------
  --  Function Name : Make Dl units within range                                                           --
  --  Description   : This function is used to get all delivery units which do not satisfy weight/vol      --
  --                  ranges defined on the storage type. It picks up those delivery units and             --
  --                  tries to move more order lines in them so as to make them in range.                  --
  --                  For details on design refer to WM01-Load-Diagramming-Science-Design-v9               --
  ---------------------------------------------------------------------------------------------------------

  Function makeDlUnitsWithinRange(p_if_split         number,
																																  p_deliv            number,
																																  p_split_order_type varchar2) return BOOLEAN IS

				v_assig_wght           number;
				v_assig_vol            number;
				v_average_weight       number;
				v_average_volume       number;
				v_dl_size              number;
				v_max_dl_wght          number;
				v_min_dl_wght          number;
				v_max_dl_vol           number;
				v_min_dl_vol           number;
				v_is_weight_in_range   boolean;
				v_is_vol_in_range      boolean;
				v_dl_vol_range         number;
				v_dl_wght_range        number;
				v_min_v_push_range_qty number;
				v_push_range_qty       number;
				v_qty_available        number;
				v_func_name            msg_log.func_name%TYPE := 'MAKE DL UNITS WITHIN RANGE';

				cursor c_less_list(p_deliv number, avg_wght number, avg_vol number) IS
				  select DELIV_ID,
												DELIV_UNIT_NUMBER,
												TC_ORDER_ID,
												ITEM_ID,
												ORDER_QTY_ASSIGNED,
												TOTAL_ORDER_QTY,
												ORDER_WT,
												ORDER_VOL,
												MAX_VOLUME,
												MIN_VOL,
												MAX_WGHT,
												MIN_WGHT,
												ASSIGNED_WGHT,
												ASSIGNED_VOL,
												MIN_NUM_CASE,
												MAX_NUM_CASE,
												HEADER_FLAG,
												VOL_RANGE,
												WGHT_RANGE,
												CMPT_ID,
												TRAILER_POS,
												EQUIPMENT_ID,
												STORAGE_TYPE,
												OVERFLOW_RANK,
												ORDER_ID,
												SHIPMENT_ID,
												FACILITY_ID,
												LOAD_TYPE,
												BULK_FLAG,
												ORDER_TYPE,
												LINE_ITEM_ID,
												ITEM_NAME,
												ORDER_LOADING_SEQ
								from temp_hold_delivery_units
				   where deliv_id = p_deliv
								and header_flag = 'Y'
								and bulk_flag = 'N'
								and assigned_vol - avg_vol < 0.0001
				   order by (((assigned_wght / max_wght) + (assigned_vol / max_volume) +
																abs((assigned_wght / max_wght) -
																				  (assigned_vol / max_volume))) * 0.5) asc;

				TYPE dl_less_list1 is TABLE OF c_less_list%ROWTYPE INDEX BY PLS_INTEGER;
				dl_less_list dl_less_list1;

				cursor c_more_list(p_deliv number, avg_wght number, avg_vol number) IS
				  select DELIV_ID,
												DELIV_UNIT_NUMBER,
												TC_ORDER_ID,
												ITEM_ID,
												ORDER_QTY_ASSIGNED,
												TOTAL_ORDER_QTY,
												ORDER_WT,
												ORDER_VOL,
												MAX_VOLUME,
												MIN_VOL,
												MAX_WGHT,
												MIN_WGHT,
												ASSIGNED_WGHT,
												ASSIGNED_VOL,
												MIN_NUM_CASE,
												MAX_NUM_CASE,
												HEADER_FLAG,
												VOL_RANGE,
												WGHT_RANGE,
												CMPT_ID,
												TRAILER_POS,
												EQUIPMENT_ID,
												STORAGE_TYPE,
												OVERFLOW_RANK,
												ORDER_ID,
												SHIPMENT_ID,
												FACILITY_ID,
												LOAD_TYPE,
												BULK_FLAG,
												ORDER_TYPE,
												LINE_ITEM_ID,
												ITEM_NAME,
												ORDER_LOADING_SEQ
								from temp_hold_delivery_units
				   where deliv_id = p_deliv
								and header_flag = 'Y'
								and bulk_flag = 'N'
								and assigned_vol - avg_vol >= 0.0001
				   order by (((assigned_wght / max_wght) + (assigned_vol / max_volume) +
																abs((assigned_wght / max_wght) -
																				  (assigned_vol / max_volume))) * 0.5) desc;

				TYPE dl_more_list1 is TABLE OF c_more_list%ROWTYPE INDEX BY PLS_INTEGER;
				dl_more_list dl_more_list1;

				cursor c_order(d_number number) is
				  select DELIV_ID,
												DELIV_UNIT_NUMBER,
												TC_ORDER_ID,
												ITEM_ID,
												ORDER_QTY_ASSIGNED,
												TOTAL_ORDER_QTY,
												ORDER_WT,
												ORDER_VOL,
												MAX_VOLUME,
												MIN_VOL,
												MAX_WGHT,
												MIN_WGHT,
												ASSIGNED_WGHT,
												ASSIGNED_VOL,
												MIN_NUM_CASE,
												MAX_NUM_CASE,
												HEADER_FLAG,
												VOL_RANGE,
												WGHT_RANGE,
												CMPT_ID,
												TRAILER_POS,
												EQUIPMENT_ID,
												STORAGE_TYPE,
												OVERFLOW_RANK,
												ORDER_ID,
												SHIPMENT_ID,
												FACILITY_ID,
												LOAD_TYPE,
												BULK_FLAG,
												ORDER_TYPE,
												LINE_ITEM_ID,
												ITEM_NAME,
												ORDER_LOADING_SEQ,
												CATEGORY_ID,
												unit_wt,
												unit_vol
								from temp_hold_delivery_units
				   where header_flag = 'N'
								and deliv_unit_number = d_number
								and bulk_flag = 'N';
								-- change : SL 100613
								-- and order_type <> 'B';
								-- change end

				TYPE dl_order_list1 is TABLE OF c_order%ROWTYPE INDEX BY PLS_INTEGER;

				dl_order_list dl_order_list1;

				cursor c_exists(d_number number, ord_id varchar2,
																				itm_id number,line_itm_id number) is
				  select 1
								from temp_hold_delivery_units
				   where deliv_unit_number = d_number
								AND order_id = ord_id
								AND item_id = itm_id
								and line_item_id = line_itm_id;

				v_rec_exist                  number := 0;
				v_order_unit_wt              number;
				v_order_unit_vol             number;
				v_load_type_exist_min_set    number := 0;

  BEGIN

				-- fetch total assigned wght and vol

				select sum(assigned_wght), sum(assigned_vol)
				  into v_assig_wght, v_assig_vol
				  from (select distinct deliv_unit_number, assigned_wght, assigned_vol
												  from temp_hold_delivery_units
												Where Deliv_Id = P_Deliv
												   and header_flag = 'N'
												   and bulk_flag = 'N');
												  /*
												   -- 170613 adding change to enable
												   -- even ranging ,, ignore the base units
												   and deliv_unit_number not in (select deliv_unit_number
																																												from   temp_hold_delivery_units
																																												  where deliv_id = p_deliv
																																												  and header_flag = 'Y'
																																												  and bulk_flag='N'
																																												  and order_type = 'B'));*/

				select count(*)
				  into v_dl_size
				  from temp_hold_delivery_units
				where deliv_id = p_deliv
				   and bulk_flag = 'N'
				   and header_flag = 'Y';
				   -- 170613 adding change to enable
				   -- even ranging ,, ignore the base units
				   -- and order_type <> 'B';

				--dbms_output.put_line ('line 2558');
				--dbms_output.put_line('v_dl_size  ,  p_deliv'||v_dl_size||' '||p_deliv);

				v_average_weight := v_assig_wght / v_dl_size;
				v_average_volume := v_assig_vol / v_dl_size;
				--dbms_output.put_line ('line 2562');
				select max(assigned_wght),
								   min(assigned_wght),
								   max(assigned_vol),
								   min(assigned_vol)
				  into v_max_dl_wght, v_min_dl_wght, v_max_dl_vol, v_min_dl_vol
				  from temp_hold_delivery_units
				where deliv_id = p_deliv
				   and header_flag = 'Y'
				   and bulk_flag = 'N';
				   -- 170613 adding change to enable
				   -- even ranging ,, ignore the base units
				--  and order_type <> 'B';

				--dbms_output.put_line ('line 455');

				select distinct vol_range, wght_range
				  into v_dl_vol_range, v_dl_wght_range
				  from temp_hold_delivery_units
				where deliv_id = p_deliv
				   and bulk_flag = 'N';

				-- dbms_output.put_line ('line 1418');

				v_is_weight_in_range := ((v_max_dl_wght - v_min_dl_wght) -
																												v_dl_wght_range < 0.001);
				v_is_vol_in_range    := ((v_max_dl_vol - v_min_dl_vol) - v_dl_vol_range <
																												0.001);
				-- dbms_output.put_line ('line 2590');
				--dbms_output.put_line ('line 463');

				if ( v_is_vol_in_range = true AND v_is_weight_in_range = true ) then
				  -- dbms_output.put_line ('coming in line 380 ... within range returning true from function within range search ');
				  return true;
				end if;
				--dbms_output.put_line ('at line 427 will do the range processing ');

				open c_less_list(p_deliv, v_average_weight, v_average_volume);
				fetch c_less_list bulk collect
				  into dl_less_list;
				close c_less_list;

				open c_more_list(p_deliv, v_average_weight, v_average_volume);
				fetch c_more_list bulk collect
				  into dl_more_list;
				close c_more_list;

				-- done forming two sorted list

				for outer_cntr in 1 .. dl_less_list.count() loop
				  --dbms_output.put_line('in outer loop  time'||outer_cntr );
				  --dbms_output.put_line (' dl_less_list(outer_cntr).deliv_unit_number ' || dl_less_list(outer_cntr).deliv_unit_number);

				  for inner_cntr in 1 .. dl_more_list.count() loop
								-- dbms_output.put_line('  dl_more_list.count() '||  dl_more_list.count() || 'inner_cntr is '|| inner_cntr);
								--dbms_output.put_line(' in inner loop  time '||inner_cntr||' looping for deliv_unit nunmber '||dl_more_list(inner_cntr).deliv_unit_number );
								-- fetch all orders in the dl unit dl_more_list(inner_cntr)
								open c_order(dl_more_list(inner_cntr).deliv_unit_number);
								fetch c_order bulk collect
								  into dl_order_list;
								close c_order;

								<<k_loop>>
								for k in 1 .. dl_order_list.count() loop

								  --dbms_output.put_line('Order number '||dl_order_list(k).order_id);
								  v_qty_available  := dl_order_list(k).order_qty_assigned;
								  v_order_unit_wt  := dl_order_list(k).order_wt / dl_order_list(k)
																												.total_order_qty;
								  v_order_unit_vol := dl_order_list(k).order_vol / dl_order_list(k)
																												.total_order_qty;

								  -- dbms_output.put_line('v_average_weight '|| v_average_weight || 'v_dl_wght_range '||v_dl_wght_range||'dl_less_list(outer_cntr).assigned_wght '|| dl_less_list(outer_cntr).assigned_wght);
								  -- dbms_output.put_line('v_order_unit_wt ' || v_order_unit_wt);
								  -- dbms_output.put_line(' v_average_volume'|| v_average_volume||' v_dl_vol_range'||v_dl_vol_range || 'dl_less_list(outer_cntr).assigned_vol'||dl_less_list(outer_cntr).assigned_vol||'v_order_unit_vol'||v_order_unit_vol);
/*
								  if (ceil((v_average_weight - v_dl_wght_range / 2 -
																   dl_less_list(outer_cntr).assigned_wght) /
																   v_order_unit_wt) >
												ceil((v_average_volume - v_dl_vol_range / 2 -
																   dl_less_list(outer_cntr).assigned_vol) /
																   v_order_unit_vol)) then
												v_min_v_push_range_qty := ceil((v_average_weight -
																																								   v_dl_wght_range / 2 -
																																								   dl_less_list(outer_cntr)
																																								   .assigned_wght) /
																																								   v_order_unit_wt);
																																								  -- dbms_output.put_line ('line 2649');
								  else
												v_min_v_push_range_qty := ceil((v_average_volume -
																																								   v_dl_vol_range / 2 -
																																								   dl_less_list(outer_cntr)
																																								   .assigned_vol) /
																																								   v_order_unit_vol);
								  end if;
*/
								v_min_v_push_range_qty := ceil((v_average_volume -(v_dl_vol_range / 2) -dl_less_list(outer_cntr).assigned_vol) /v_order_unit_vol);


								  --dbms_output.put_line(' v_min_v_push_range_qty ' || v_min_v_push_range_qty);

								  -- check if this is a base type of order line
								  -- than if this load type exsit in the min set deliv unit
								  -- if it exist than try to push it to min set
								  -- CR 01082013

								  if (dl_order_list(k).order_type = 'N' or dl_order_list(k).order_type = 'B')
								  then
												if p_if_split = 0
												then
												--  do not move the complete base unit
												  continue;
												end if ;

												-- split flag is on .. check if this load type exist in min set

												v_load_type_exist_min_set := 0;
												select count(*)
												into  v_load_type_exist_min_set
												from  temp_hold_delivery_units
												where deliv_unit_number = dl_less_list(outer_cntr).deliv_unit_number
												and   dl_order_list(k).load_type = dl_less_list(outer_cntr).load_type;

												if v_load_type_exist_min_set = 0
												then
												  continue;
												end if;

								  end if;



								  -- change end CR 01082013

								  -- if split flag is zero don't split the order .. if split flag is 1 than ensure the order type at hand is the order type passed to the function
								  if ((p_if_split = 0) AND (v_min_v_push_range_qty < v_qty_available)) then
												--OR
												-- (p_if_split = 1) AND
												-- (dl_order_list(k).order_type <> p_split_order_type)) then
												continue;
								  else              
									if (p_if_split = 0) then
									  continue;                                                                  
									end if;
								  end if;


--                                                                  if ((p_if_split = 0) AND (v_min_v_push_range_qty < v_qty_available)) then
--                                                                                --OR
--                                                                                -- (p_if_split = 1) AND
--                                                                                -- (dl_order_list(k).order_type <> p_split_order_type)) then
--                                                                                continue;
--                                                                  end if;
								  

								  if v_min_v_push_range_qty <= 0 then
												exit k_loop;
								  end if;

								  if v_min_v_push_range_qty < v_qty_available then
												v_min_v_push_range_qty := v_min_v_push_range_qty;
								  else
												v_min_v_push_range_qty := v_qty_available;
								  end if;

								  --dbms_output.put_line (' qty available '|| v_qty_available || ' v_min_v_push_range_qty '||v_min_v_push_range_qty );

								  v_push_range_qty := v_min_v_push_range_qty;

								  if ((dl_more_list(inner_cntr)
												.assigned_wght - (v_push_range_qty * v_order_unit_wt) -
												  (v_average_weight - v_dl_wght_range * 0.5) >= 0.0001) AND
												(dl_more_list(inner_cntr)
												.assigned_vol - (v_push_range_qty * v_order_unit_vol) -
												  (v_average_volume - v_dl_vol_range * 0.5) >= 0.0001)) AND
												((dl_more_list(inner_cntr)
												.assigned_wght - (v_push_range_qty * v_order_unit_wt) -
												  dl_more_list(inner_cntr).min_wght >= 0.0001) AND
												(dl_more_list(inner_cntr)
												.assigned_vol - (v_push_range_qty * v_order_unit_vol) -
												  dl_more_list(inner_cntr).min_vol >= 0.0001)) AND
												((dl_less_list(outer_cntr)
												.assigned_wght + (v_push_range_qty * v_order_unit_wt) -
												  (v_average_weight + v_dl_wght_range * 0.5) <= 0.0001) AND
												(dl_less_list(outer_cntr)
												.assigned_vol + (v_push_range_qty * v_order_unit_vol) -
												  (v_average_volume + v_dl_vol_range * 0.5) <= 0.0001)) AND
												((dl_less_list(outer_cntr)
												.assigned_wght + (v_push_range_qty * v_order_unit_wt) -
												  dl_less_list(outer_cntr).max_wght <= 0.0001) AND
												(dl_less_list(outer_cntr)
												.assigned_vol + (v_push_range_qty * v_order_unit_vol) -
												  dl_less_list(outer_cntr).max_volume <= 0.0001)) then
												--  dbms_output.put_line('line 498---  use within range finally !! ');
												-- update ord qty and assigned vol/wght in the max list

												--dbms_output.put_line ('decreasing qty  for '|| dl_order_list(k).item_id|| ' new qty is ' ||dl_order_list(k).order_qty_assigned );

												dl_order_list(k).order_qty_assigned := dl_order_list(k)
																																																  .order_qty_assigned -
																																																   v_push_range_qty;

												-- keep the assigned wt and assigned vol updated as  will be used at the next iteration

												dl_more_list(inner_cntr).assigned_wght := dl_more_list(inner_cntr)
																																																				.assigned_wght -
																																																				  (v_push_range_qty *
																																																				   v_order_unit_wt);
												dl_more_list(inner_cntr).assigned_vol := dl_more_list(inner_cntr)
																																																				.assigned_vol -
																																																				(v_push_range_qty *
																																																				  v_order_unit_vol);

												--      dbms_output.put_line ('       dl_more_list(inner_cntr).assigned_wght ' ||      dl_more_list(inner_cntr).assigned_wght ) ;
												--    dbms_output.put_line ('       dl_more_list(inner_cntr).assigned_vol '  ||       dl_more_list(inner_cntr).assigned_vol );

												-- now check if the order already exist in the min list
												v_rec_exist := 0;

												open c_exists(dl_less_list(outer_cntr).deliv_unit_number,
																								  dl_order_list(k).order_id,
																								  dl_order_list(k).item_id,
																								  dl_order_list(k).line_item_id);
												fetch c_exists
												  into v_rec_exist;
												close c_exists;

												if v_rec_exist = 1 then
												  -- the record already exist just update the values
												  --             dbms_output.put_line(' In range and updating ');

												  UPDATE temp_hold_delivery_units
																set order_qty_assigned = order_qty_assigned +
																																								  v_push_range_qty
												   where deliv_unit_number = dl_less_list(outer_cntr)
												  .deliv_unit_number
																AND order_id = dl_less_list(outer_cntr)
												  .order_id
																AND item_id = dl_less_list(outer_cntr).item_id
																AND line_item_id = dl_less_list(outer_cntr).line_item_id;
												else
												  -- insert the order line in the delivery unit
												  IF g_log_level = 2 THEN
																--dbms_output.put_line(' In range and inserting ');
																exception_msg_log_insert(' In range and inserting ',
																																								v_func_name,
																																								null,
																																								'DEBUG');
												  END IF;

												  insert into temp_hold_delivery_units
																(c_seq,
																deliv_id,
																deliv_unit_number,
																tc_order_id,
																item_id,
																order_qty_assigned,
																total_order_qty,
																order_wt,
																order_vol,
																max_volume,
																min_vol,
																max_wght,
																min_wght,
																assigned_wght,
																assigned_vol,
																min_num_case,
																max_num_case,
																header_flag,
																vol_range,
																wght_range,
																equipment_id,
																storage_type,
																order_id,
																shipment_id,
																facility_id,
																load_type,
																bulk_flag,
																order_type,
																line_item_id,
																item_name,
																order_loading_seq,
																category_id,
																unit_wt,
																unit_vol)
												  values
																(c_range_seq.nextval,
																dl_less_list(outer_cntr).deliv_id,
																dl_less_list(outer_cntr).deliv_unit_number,
																dl_order_list(k).tc_order_id,
																dl_order_list(k).item_id,
																v_push_range_qty,
																dl_order_list(k).total_order_qty,
																dl_order_list(k).order_wt,
																dl_order_list(k).order_vol,
																dl_less_list(outer_cntr).max_volume,
																dl_less_list(outer_cntr).min_vol,
																dl_less_list(outer_cntr).max_wght,
																dl_less_list(outer_cntr).min_wght,
																v_push_range_qty * v_order_unit_wt,
																v_push_range_qty * v_order_unit_vol,
																dl_less_list(outer_cntr).min_num_case,
																dl_less_list(outer_cntr).max_num_case,
																'N',
																dl_less_list(outer_cntr).vol_range,
																dl_less_list(outer_cntr).wght_range,
																dl_order_list(k).equipment_id,
																dl_order_list(k).storage_type,
																dl_order_list(k).order_id,
																dl_order_list(k).shipment_id,
																dl_order_list(k).facility_id,
																dl_order_list(k).load_type,
																dl_order_list(k).bulk_flag,
																dl_order_list(k).order_type,
																dl_order_list(k).line_item_id,
																dl_order_list(k).item_name,
																dl_order_list(k).order_loading_seq,
																dl_order_list(k).category_id,
																dl_order_list(k).unit_wt,
																dl_order_list(k).unit_vol);
												end if;
												-- update the min list's assigned wght/vol
												dl_less_list(outer_cntr).assigned_wght := dl_less_list(outer_cntr)
																																																				.assigned_wght +
																																																				  (v_push_range_qty *
																																																				   v_order_unit_wt);
												dl_less_list(outer_cntr).assigned_vol := dl_less_list(outer_cntr)
																																																				.assigned_vol +
																																																				(v_push_range_qty *
																																																				  v_order_unit_vol);

								  end if;
								end loop;

								forall l in 1 .. dl_order_list.count()
								  update temp_hold_delivery_units
												set order_qty_assigned = dl_order_list(l).order_qty_assigned
								   where deliv_unit_number = dl_order_list(l)
								  .deliv_unit_number
												and order_id = dl_order_list(l)
								  .order_id
												and item_id = dl_order_list(l).item_id
												and line_item_id = dl_order_list(l).line_item_id;
				  end loop;
				end loop;

				-- updating to the base table
				forall l in 1 .. dl_more_list.count()
				  update temp_hold_delivery_units
								set assigned_wght = dl_more_list(l).assigned_wght,
												assigned_vol  = dl_more_list(l).assigned_vol
				   where deliv_unit_number = dl_more_list(l).deliv_unit_number;

				forall i in 1 .. dl_less_list.count()
				  update temp_hold_delivery_units
								set assigned_wght = dl_less_list(i).assigned_wght,
												assigned_vol  = dl_less_list(i).assigned_vol
				   where deliv_unit_number = dl_less_list(i).deliv_unit_number;
				   
				   
				return true;
  EXCEPTION
				WHEN NO_DATA_FOUND THEN
				  --dbms_output.put_line('No data to process.. Exiting the make units within range function');
				  exception_msg_log_insert('No data to process.. Exiting the make units within range function',
																												   v_func_name,
																												   'Delivery Id: ' || p_deliv,
																												   'ERROR');
				  return true;
				WHEN OTHERS THEN
				  g_msg := 'Exception in makeDlUnitsWithinRange function' || SQLCODE ||
												   SQLERRM;
				  exception_msg_log_insert(g_msg,
																												   v_func_name,
																												   'Delivery Id: ' || p_deliv,
																												   'ERROR');
				  --dbms_output.put_line('Exception in makeDlUnitsWithinRange function'||SQLCODE||SQLERRM);
				  return false;
  end makeDlUnitsWithinRange;

  ---------------------------------------------------------------------------------------------------------
  --  Function Name : create_foundation_data                                                               --
  --  Description   : This function populates the global main table with all order                         --
  --                  lines in the input shipment along with the details - load type,storage type          --
  --                  min , max wght .. The logic for finding all these details are encapsualted within the--
  --                  select statement given in the function.                                              --
  ---------------------------------------------------------------------------------------------------------

  -- Function to populate global main

  Function create_foundation_data(p_shipment_tc_id VARCHAR2) Return Boolean IS
				return_value        boolean;
				v_temp              number(6) := 0;
				v_prod_type         varchar2(1000) := 0;
				v_TC_COMPANY_ID     number(10) := NULL;
				v_error_shipment_id number(30) := NULL;
				v_func_name         msg_log.func_name%TYPE := 'CREATE_FOUNDATION_DATA';
				--error_desc          varchar2(300);

  begin

				return_value := TRUE;

								insert into global_main
				  (shipment_no,
				   deliv_id,
				   bulk_process,
				   tc_order_id,
				   FACILITY_ID,
				   line_item_id,
				   item_name,
				   item_id,
				   ORIG_ORDER_QTY,
				   category_id,
				   load_type,
				   storage_type,
				   break_by_stop,
				   unit_wt,
				   unit_vol,
				   dsg_equipment_id,
				   order_id,
				   min_case,
				   max_case,
				   group_as_family,
				   shipment_id,
				   order_type,
				   ovrd_prod_type,
				   order_loading_seq,
				   direct_overflow)
				  select p_shipment_tc_id,
												case
												   when cst.break_by_stop = 'N' and direct_overflow <> 'Y' then
																DENSE_RANK() OVER(order by cept.storage_type_id desc)
												-- change start : SL
												   when cst.break_by_stop = 'Y' and direct_overflow <> 'Y'
												   then
												   -- create your own random sequence number
												   --((DENSE_RANK()
												   --OVER(order by cept.storage_type_id desc) + ROWNUM) *
												   --100000) + cept.storage_type_id
												   -- Lines with same storage type and stop number will be in same deliv unit
																((DENSE_RANK()
																OVER(order by cept.storage_type_id desc,
																				   final_po.D_FACILITY_ID desc)) * 100000)
																-- change : SL - to handle direct to overflow
																when direct_overflow = 'Y'
																then
																  0
												end D_ID,
												-- change end : SL
												clt.bulk_process,
												final_po.TC_ORDER_ID,
												final_po.D_FACILITY_ID,
												final_po.LINE_ITEM_ID,
												final_po.ITEM_NAME,
												final_po.ITEM_ID,
												final_po.ORIG_ORDER_QTY,
												cpt.category_id,
												cept.load_type_id,
												cept.storage_type_id,
												cst.break_by_stop,
												final_po.unit_weight,
												final_po.unit_volume,
												final_po.dsg_equipment_id,
												final_po.order_id,
												clt.min_cases,
												clt.max_cases,
												clt.group_as_family,
												final_po.shipment_id,
												decode(clt.group_as_family, 'Y', 'N', 'N', 'O'),
												clt.ovrd_prod_type,
												final_po.order_loading_seq,
												clt.direct_overflow
								from (select inq.TC_ORDER_ID,
																				inq.D_FACILITY_ID,
																				inq.LINE_ITEM_ID,
																				inq.ITEM_NAME,
																				inq.ITEM_ID,
																				inq.ORIG_ORDER_QTY,
																				inq.PRODUCT_TYPE prod_type,
																				inq2.revised_product_type Revised_prod,
																				--cpt.prod_type Revised_prod,
																				inq.unit_weight,
																				inq.unit_volume,
																				nvl(inq.dsg_equipment_id,g_equipment_id) dsg_equipment_id,
																				inq.order_id,
																				inq.shipment_id,
																				inq.order_loading_seq
																from ( select
				  o.tc_order_id,o.d_facility_id,oli.line_item_id,
												ic.item_name,ic.item_id,oli.orig_order_qty,
												nvl(iw.prod_catgry, cpt.category_id) product_type,
												ic.unit_weight,ic.unit_volume,sh.dsg_equipment_id,
												o.order_id,sh.shipment_id,o.order_loading_seq
  from item_cbo ic,item_wms  iw,
orders  o,order_line_item oli,
								SHIPMENT SH,C_PROD_TYPE CPT
where  o.tc_shipment_id = p_shipment_tc_id
   and  o.order_id = oli.order_id
and  oli.item_id = ic.item_id
   and oli.item_id = iw.item_id
   and oli.do_dtl_status != 200
AND SH.TC_SHIPMENT_ID = O.TC_SHIPMENT_ID
   and sh.tc_shipment_id = p_shipment_tc_id
   and cpt.prod_type = ic.prod_type) inq
																LEFT OUTER JOIN (select cpt.category_id,
																																				   cficm.orig_product_type,
																																				   cficm.revised_product_type,
																																				   cficm.FACILITY_ID
																																  FROM c_prod_type               cpt,
																																				   C_FACILITY_ITEM_CTGRY_MAP cficm
																																WHERE cpt.category_id =
																																				   cficm.orig_product_type) inq2 ON inq.product_type =
																																																																								inq2.category_id
																																																																				AND inq2.facility_id =
																																																																								inq.d_facility_id ) final_po
												--             LEFT OUTER JOIN c_prod_type cpt ON cpt.category_id =
												--                                                                                                                                                inq2.revised_product_type) final_po
								LEFT OUTER JOIN c_prod_type cpt ON cpt.category_id =
																																								   nvl(final_po.Revised_prod,
																																												   final_po.prod_type)
								LEFT OUTER JOIN C_EQUIPMENT_PROD_TYPE cept ON cept.prod_type_id =
																																																				  cpt.category_id
																																																  AND cept.equipment_id =
																																																				  final_po.dsg_equipment_id
								LEFT OUTER JOIN c_storage_type cst ON cept.storage_type_id =
																																												  cst.storage_type_id
								LEFT OUTER JOIN c_load_type clt ON clt.load_type_id =
																																								   cept.load_type_id;

								select sum(orig_order_qty),
												   sum(unit_wt * orig_order_qty),
												   sum(unit_vol * orig_order_qty)
								into g_orig_order_qty, g_total_wght, g_total_vol
								from global_main;

				-- check if for any line in this shipment ,load_type or storage type is still null if found than error out
				IF g_log_level = 2 THEN
				  --dbms_output.put_line('After the create foundation data insert ');
				  exception_msg_log_insert('after the create foundation data insert',
																												   v_func_name,
																												   p_shipment_tc_id,
																												   'DEBUG');
				END IF;

				select count(*)
				  into v_temp
				  from global_main gb
				where gb.load_type is NULL
								or gb.storage_type is null;

				if v_temp >= 1 then
				  return_value := FALSE;


				  -- change start: SL
				  /*
				  v_prod_type := 'Multiple prod types';
								else
								  select category_id
												into v_prod_type
												from global_main gb
								   where gb.load_type is NULL
												  or gb.storage_type is null;
								end if;
								*/
/*
				   select LISTAGG(cpt.prod_type, ',') WITHIN
				   Group(
				   Order By gb.Category_Id)
								into v_prod_type
								From global_main Gb , c_prod_type Cpt
				   Where cpt.category_id =  gb.category_id
								and Gb.Load_Type Is Null
								  or gb.storage_type is null;
*/
				   insert into msg_log
				  (msg_log_id,module,msg_id,pgm_id,
				   sub_pgm_name,msg,create_date_time,
				   user_id,cd_master_id,log_date_time,
				   mod_date_time,whse,func_name,ref_value_1,ref_code_1
				   )
				   select MSG_LOG_ID_SEQ.NEXTVAL,'CUST','0106',
												'Generate Load Diagram','ERROR','Missing product type configuration for item '||item_name ,
												sysdate,g_user_id,g_company_id,
												sysdate,sysdate,G_WAREHOUSE,'CREATE FOUNDATION DATA',
												p_shipment_tc_id,1
				   from (
												  select distinct ITEM_NAME
																from global_main Gb , c_prod_type Cpt
												   where cpt.category_id =  gb.category_id
																and GB.LOAD_TYPE is null
																  or GB.STORAGE_TYPE is null) inq;
				  -- change end: SL
/*
				  select TC_COMPANY_ID, shipment_id
								into v_TC_COMPANY_ID, v_error_shipment_id
								from shipment
				   where tc_shipment_id = p_shipment_tc_id;

				   v_prod_type := substr(v_prod_type,1,100);

				  g_msg := ' No Product Type Mapping exists for ' || v_prod_type ||
												   ' on Shipment ' || p_shipment_tc_id;

				  exception_msg_log_insert(g_msg,
																												   v_func_name,
																												   p_shipment_tc_id,
																												   'ERROR');
				  -- need to insert in the base table
				  --  insert into error_load_diagram values(seq_error_shipment.nextVal,v_TC_COMPANY_ID,v_error_shipment_id,'LDGR',2,error_desc);
*/
				end if;

				return return_value;

  EXCEPTION
				when invalid_number then
				  INSERT INTO msg_log
				  (msg_log_id,module,msg_id,pgm_id,
				   sub_pgm_name,msg,create_date_time,
				   user_id,cd_master_id,log_date_time,
				   mod_date_time,whse,func_name,ref_value_1,ref_code_1
				   )
				  select MSG_LOG_ID_SEQ.NEXTVAL,'CUST','0106',
												'Generate Load Diagram','ERROR','A string value is stored for '||item_name|| ' override product type' ,
												sysdate,g_user_id,g_company_id,
												sysdate,sysdate,G_WAREHOUSE,'CREATE FOUNDATION DATA',
												p_shipment_tc_id,1
				  from
				   (
								select distinct length(TRIM(TRANSLATE(nvl(IW.PROD_CATGRY,1),
												   'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ' ')))  len, ic.item_name
								from item_cbo ic,item_wms  iw,
												orders  o,order_line_item oli,
												shipment sh,c_prod_type cpt
				   where o.tc_shipment_id = p_shipment_tc_id
				   and   o.order_id = oli.order_id
				   and   oli.item_id = ic.item_id
				   and   oli.item_id = iw.item_id
				   and   oli.do_dtl_status != 200
				   and sh.tc_shipment_id = o.tc_shipment_id
				   and sh.tc_shipment_id = p_shipment_tc_id
				  )
				where len is null;
				return false;
				WHEN OTHERS THEN
				  g_msg := 'Exception in create foundation data function' || SQLCODE ||
												   SQLERRM;
				  exception_msg_log_insert(g_msg,
																												   v_func_name,
																												   p_shipment_tc_id,
																												   'ERROR');
				  return false;
  end create_foundation_data;

  ---------------------------------------------------------------------------------------------------------
  --  Function Name : pallet_position                                                                     --
  --  Description   : This function assigns compartment and positions to the                              --
  --                  delivery units formed .                                                             --
  --                                                                                                      --
  ---------------------------------------------------------------------------------------------------------

  Function pallet_position(p_shipment_tc_id varchar2) return BOOLEAN IS

				cursor c_dl_units(cmpt_i number) is
								  select thdu.deliv_unit_number,
																min(thdu.order_loading_seq) order_seq,
																min(unload_seq) unload_seq,
																0
												from temp_hold_delivery_units thdu, c_load_type clt
								   where thdu.storage_type in
																(select storage_type_id
																				from C_EQUIPMENT_CMPT_ST
																   where eq_cmpt_id = cmpt_i)
												and thdu.cmpt_id is null
												and thdu.load_type = clt.load_type_id
												and thdu.header_flag = 'N'
								   group by thdu.deliv_unit_number
								   order by order_seq asc, unload_seq asc;

				TYPE rowid_table is TABLE OF ROWID INDEX BY PLS_INTEGER;
				rowid_array                    rowid_table;
				var_rowid_counter              number := 1;
				var_current_dl_unit            number;
				var_assigned_volume_dl_unit    number;
				var_max_allowed_volume_dl_unit number;
				v_func_name                    msg_log.func_name%TYPE := 'PALLET POSITION';
				/*
				cursor c_overflow_dl (cmpt_i number)
				is
				select deliv_unit_number,min(stop_seq) stop_seq , unload_seq , replace(cst.overflow_rank,0,999)
				from
				(select thdu.deliv_unit_number,stp.stop_seq,thdu.load_type ,storage_type
				   from temp_hold_delivery_units thdu, c_storage_type cst, STOP STP
				  where thdu.storage_type in  ( select storage_type
																																								from C_EQUIPMENT_CMPT_ST
																																				   where eq_cmpt_id = cmpt_i )
								and thdu.cmpt_id is null
								and thdu.header_flag='N'
								and thdu.Shipment_Id  =  stp.Shipment_Id
								and thdu.facility_id  = stp.facility_id) , c_load_type clt , c_storage_type cst
				where clt.load_type_id = load_type
				  and cst.storage_type_id = storage_type
				group by deliv_unit_number, unload_seq, cst.overflow_rank
				order by overflow_rank desc,stop_seq desc,unload_seq desc;
				*/

				cursor c_overflow_dl(cmpt_i number) is
				  select thdu.deliv_unit_number,
												min(thdu.order_loading_seq) order_seq,
												min(unload_seq) unload_seq,
												replace(cst.overflow_rank, 0, 999) overflow_rank
								from temp_hold_delivery_units thdu,
												c_load_type              clt,
												c_storage_type           cst
				   where thdu.storage_type in
												(select storage_type_id
																from C_EQUIPMENT_CMPT_ST
												   where eq_cmpt_id = cmpt_i)
								and thdu.cmpt_id is null
								and thdu.load_type = clt.load_type_id
								and thdu.storage_type = cst.storage_type_id
								and thdu.header_flag = 'N'
				   group by thdu.deliv_unit_number,
																replace(cst.overflow_rank, 0, 999)
				   order by overflow_rank desc, order_seq asc, unload_seq asc;

				TYPE list_overflow_dl1 is TABLE OF c_dl_units%ROWTYPE INDEX BY PLS_INTEGER;
				list_overflow_dl list_overflow_dl1;

				-- change:SV
				cursor c_dl_units_nonoverflow(cmpt_i number) is
				  select thdu.deliv_unit_number,
												min(thdu.order_loading_seq) order_seq,
												min(unload_seq) unload_seq,
												0
								from temp_hold_delivery_units thdu, c_load_type clt
				   where thdu.storage_type in
												(select storage_type_id
																from C_EQUIPMENT_CMPT_ST
												   where eq_cmpt_id = cmpt_i)
								and thdu.cmpt_id is null
								and thdu.load_type = clt.load_type_id
								and thdu.header_flag = 'N'
				   group by thdu.deliv_unit_number
				   order by order_seq asc, unload_seq asc;

				TYPE list_nonoverflow_units_tt is TABLE OF c_dl_units%ROWTYPE INDEX BY PLS_INTEGER;
				list_nonoverflow_units_t list_nonoverflow_units_tt;
				-- change End : SV

				TYPE list_dl_units_t is TABLE OF c_dl_units%ROWTYPE INDEX BY PLS_INTEGER;
				list_dl_units list_dl_units_t;

				-- change : SL 26/06 change to pick from final mapping table

				cursor c_positions(cmpt_id number) is
				  select eq_cmpt_id, trailer_position, max_util
								from c_final_compartment_mapping
				   where eq_cmpt_id = cmpt_id
								-- and is_active = 'Y'
								-- and trailer_position not in
								-- (select distinct trailer_pos
								--    from temp_hold_delivery_units
								--   where trailer_pos is not null)
				   order by unl_seq asc;

				cursor c_positions_asc(cmpt_id number) is
				  select eq_cmpt_id, trailer_position, max_util
								from c_final_compartment_mapping
				   where eq_cmpt_id = cmpt_id
								-- and is_active = 'Y'
								-- and trailer_position not in
								-- (select distinct trailer_pos
								--    from temp_hold_delivery_units
								--   where trailer_pos is not null)
				   order by unl_seq ;


				TYPE list_position1 is TABLE OF c_positions%ROWTYPE INDEX BY PLS_INTEGER;

				list_position list_position1;

				counter          number := 0;
				var_first_loop   number := 1;


  Begin
				-- loop through all the compartments
				-- sorted by rank in asc order
				for i in (select distinct eq_cmpt_id
																From C_EQUIPMENT_CMPT CMPT
												   where CMPT.equipment_id = g_equipment_id
--                                                                                   order by rank
												   ) loop

				  --dbms_output.put_line('looping for compartment ' || i.eq_cmpt_id);
				  --dbms_output.put_line('New');
				  -- now fetch all dl units having storage type supported by this compartment
				  -- add logic to sort them by over flow rank

				  open c_dl_units(i.eq_cmpt_id);
				  fetch c_dl_units bulk collect
								into list_dl_units;
				  close c_dl_units;

				  --dbms_output.put_line('count is ' || list_dl_units.count);
				  if list_dl_units.count = 0 then
								--dbms_output.put_line('no records present to process');
								exception_msg_log_insert('No records present to process',
																																v_func_name,
																																p_shipment_tc_id,
																																'ERROR');
								continue;
				  end if;
				  -- fetch all positions which are active in this comaprtment
				  -- sorted by unload sequence in descending order

				  open c_positions(i.eq_cmpt_id);
				  fetch c_positions bulk collect
								into list_position;
				  close c_positions;

				  if list_position.count = 0 then
								--dbms_output.put_line('Did not find any valid positions in the compartment');
								exception_msg_log_insert('Did not find any valid positions in the compartment',
																																v_func_name,
																																p_shipment_tc_id,
																																'ERROR');
								continue;
				  end if;

				  if (list_position.count < list_dl_units.count) then
								g_overflow_flag := 1;
								--dbms_output.put_line('Number of units greater than the position available');
								exception_msg_log_insert('Number of units greater than the position avaliable',
																																v_func_name,
																																p_shipment_tc_id,
																																'INFO');

								open c_overflow_dl(i.eq_cmpt_id);
								fetch c_overflow_dl bulk collect
								  into list_overflow_dl;
								close c_overflow_dl;

								-- Change Start: SV
								/*
								  forall i in 1 .. list_position.count
												update temp_hold_delivery_units
												   set cmpt_id     = list_position(i).eq_cmpt_id,
																   trailer_pos = list_position(i).trailer_position,
								   max_util    = list_position(i).max_util
												where deliv_unit_number = list_overflow_dl(i).deliv_unit_number;
								*/
								-- change end: SV
								counter := list_position.count + 1;

								forall i in counter .. list_overflow_dl.count
								  update temp_hold_delivery_units
												set cmpt_id = 00, trailer_pos = '00'
								   where deliv_unit_number = list_overflow_dl(i).deliv_unit_number;

								-- Change Start: SV
								open c_dl_units_nonoverflow(i.eq_cmpt_id);
								fetch c_dl_units_nonoverflow bulk collect
								  into list_nonoverflow_units_t;
								close c_dl_units_nonoverflow;

								forall i in 1 .. list_nonoverflow_units_t.count
								  update temp_hold_delivery_units
												set cmpt_id     = list_position(i).eq_cmpt_id,
																trailer_pos = list_position(i).trailer_position,
																max_util    = list_position(i).max_util
								   where deliv_unit_number = list_nonoverflow_units_t(i)
								  .deliv_unit_number;
								-- change End : SV
				  else
								--dbms_output.put_line('Number of units less');
								-- now loop through the list of positions and assign

								-- change : SL  26/06 overlapping position changes

								list_position.delete();

								open c_positions_asc(i.eq_cmpt_id);
								fetch c_positions_Asc bulk collect
								into list_position;
								close c_positions_Asc;

								-- change : SL 26/06 overlapping changes

								forall i in 1 .. list_dl_units.count
								-- not required to loop for entire list size of position as delivery units are less
								  update temp_hold_delivery_units
												set cmpt_id     = list_position(i).eq_cmpt_id,
																trailer_pos = list_position(i).trailer_position,
																max_util    = list_position(i).max_util
								   where deliv_unit_number = list_dl_units(i).deliv_unit_number;
				  end if;

				  IF g_log_level = 2 THEN
								--dbms_output.put_line('count of matching dl units is ' ||
								--                                                                             list_dl_units.count);
								--dbms_output.put_line('count of trailer positions in this compartment is ' ||
								--                                                                             list_position.count);

								exception_msg_log_insert('count of matching dl units is ' ||
																																list_dl_units.count,
																																v_func_name,
																																p_shipment_tc_id,
																																'DEBUG');
								exception_msg_log_insert('count of trailer positions in this compartment is ' ||
																																list_position.count,
																																v_func_name,
																																p_shipment_tc_id,
																																'DEBUG');
				  END IF;

				end loop;

				var_first_loop := 1;

				for dl_cntr in (select rowid,
																								   deliv_unit_number,
																								   order_id,
																								   item_id,
																								   assigned_vol total_assigned_volume,
																								   (max_util / 100) * max_volume max_allowed_volume,
																								   order_qty_assigned *
																								   (order_vol / total_order_qty) assigned_order_volume
																				  from temp_hold_delivery_units
																				where deliv_unit_number in
																								   (select deliv_unit_number
																												  from temp_hold_delivery_units
																												where header_flag = 'Y'
																												   and assigned_wght <> 0
																												   and max_util <> 100
																												   and assigned_vol >
																																   (max_util / 100) * max_volume)
																				   and header_flag = 'N'
																				order by deliv_unit_number, assigned_vol) loop

				  if (var_first_loop = 1) then
								var_current_dl_unit            := dl_cntr.deliv_unit_number;
								var_assigned_volume_dl_unit    := dl_cntr.total_assigned_volume;
								var_max_allowed_volume_dl_unit := dl_cntr.max_allowed_volume;
								var_first_loop                 := 0;
				  end if;

				  if (var_current_dl_unit <> dl_cntr.deliv_unit_number) then
								--  set the variables
								var_current_dl_unit            := dl_cntr.deliv_unit_number;
								var_assigned_volume_dl_unit    := dl_cntr.total_assigned_volume;
								var_max_allowed_volume_dl_unit := dl_cntr.max_allowed_volume;
				  end if;
				  if (var_assigned_volume_dl_unit > var_max_allowed_volume_dl_unit) then
								IF g_log_level > 0 THEN
								  --dbms_output.put_line('Coming in the overflow condition ');
								  exception_msg_log_insert('Coming in the overflow condition',
																																   v_func_name,
																																   p_shipment_tc_id,
																																   'INFO');
								END IF;
								var_assigned_volume_dl_unit := var_assigned_volume_dl_unit -
																																				   dl_cntr.assigned_order_volume;
								-- remove order and place in overflow
								rowid_array(var_rowid_counter) := dl_cntr.rowid;
								var_rowid_counter := var_rowid_counter + 1;
				  end if;
				end loop;

				forall i in 1 .. rowid_array.count
				  update temp_hold_delivery_units
								set cmpt_id = 00, trailer_pos = '00'
				   where rowid = rowid_array(i);
				-- End of max utilization
				-- insert direct overflow records to temp hold delivery units
				insert into temp_hold_delivery_units
												  (c_seq,deliv_id,deliv_unit_number,tc_order_id,item_id,
												   order_qty_assigned,total_order_qty,order_wt,order_vol,
												   max_volume,min_vol,max_wght,min_wght,
												   assigned_wght,assigned_vol,min_num_case,max_num_case,
												   header_flag,vol_range,wght_range,bulk_flag,
												   equipment_id,storage_type,order_id,shipment_id,
												   facility_id,load_type,order_type,line_item_id,
												   item_name,order_loading_seq,category_id,unit_wt,
												   unit_vol)
				  select   c_range_seq.nextval,0,delivery_unit_number.nextval ,
												   gb.tc_order_id,gb.item_id,
												   gb.orig_order_qty,gb.orig_order_qty,gb.orig_order_qty*gb.unit_wt,
												   gb.orig_order_qty*gb.unit_vol,null,null,null,
												   null,gb.orig_order_qty*gb.unit_wt,gb.orig_order_qty*gb.unit_vol,
												   null,null,'N',null,
												   null,'N',gb.dsg_equipment_id,gb.storage_type,
												   gb.order_id,gb.shipment_id,gb.facility_id,gb.load_type,
												   'O',gb.line_item_id,gb.item_name,gb.order_loading_seq,
												   gb.category_id,gb.unit_wt,gb.unit_vol
				  from     global_main gb
				  where    deliv_id = 0;


				-- change start : SV
				-- Update everything with blank cmpt_id
				update temp_hold_delivery_units
				   set cmpt_id = 00,
					   trailer_pos = '00'
				where cmpt_id is null;
				-- change end : SV
				return true;

  EXCEPTION
				WHEN OTHERS THEN
				  g_msg := 'Exception in pallet position function' || SQLCODE ||
												   SQLERRM;
				  exception_msg_log_insert(g_msg,
																												   v_func_name,
																												   p_shipment_tc_id,
																												   'ERROR');
				  return false;
  END pallet_position;


---------------------------------------------------------------------------------------------------------
--  Function Name : create_bulk_delivery_units                                                               --
--  Description   :                                              --
---------------------------------------------------------------------------------------------------------

----BUlk delivery unit creation start----

FUNCTION create_bulk_delivery_units(v_shipment varchar2) return boolean is

v_func_name msg_log.func_name%TYPE := 'CREATE_BULK_DELIVERY_UNITS';

-- variables to point to head and tail of order list
v_head_olist NUMBER(2);
v_tail_olist NUMBER(10);
v_err_equip      number :=0;
v_err_load_type  number:=0;

-- flag to be used for checking if a delivery unit was found
v_flag number := 1;

v_cntr_order number;
v_cntr_deliv number := 1;

v_bulk_process  char(1);
v_break_by_stop char(1);
v_deliv_id      number;

-- cursor to get all grouped order lines

cursor c_deliv_id is
  select deliv_id
				from global_main
  -- change start: SL
   where bulk_process = 'Y'
				and shipment_no = v_shipment
  -- change end: SL
   group by deliv_id;

rec c_deliv_id%ROWTYPE;

-----------------------------------
cursor c_order(deliv_num number, ship_num varchar2) is
  select SHIPMENT_NO,
								DELIV_ID,
								BULK_PROCESS,
								TC_ORDER_ID,
								FACILITY_ID,
								LINE_ITEM_ID,
								ITEM_NAME,
								ITEM_ID,
								ORIG_ORDER_QTY,
								CATEGORY_ID,
								LOAD_TYPE,
								STORAGE_TYPE,
								BREAK_BY_STOP,
								DSG_EQUIPMENT_ID,
								ORDER_ID,
								MIN_CASE,
								MAX_CASE,
								FLAG,
								GROUP_AS_FAMILY,
								SPLIT_FLAG,
								SHIPMENT_ID,
								PROCESSED,
								ORDER_TYPE,
								OVRD_PROD_TYPE,
								(unit_wt * orig_order_qty) order_wt,
								(unit_vol * orig_order_qty) order_vol,
								order_loading_seq,
								unit_wt,
								unit_vol
				from global_main
   where shipment_no = ship_num
				and deliv_id = deliv_num
				-- change SL 2006 the code has an exit
				-- once it finds non bulk item
				and bulk_process = 'Y'
  --Change VT: processing the orders for bulk processing
  --and bulk_process='N'
  --and break_by_stop = 'N'
   order by storage_type, order_loading_seq,orig_order_qty desc; --, facility_id;
--order by order_wt, order_vol desc;
-----------------------------------
TYPE order_list IS TABLE OF c_order%ROWTYPE INDEX BY PLS_INTEGER;
ord1 order_list;
--ord2 c_order%ROWTYPE;

-- List to hold delivery units

cursor c_deliv_unit(storage_t number, order_n varchar2, item_i number) is
  select gb.deliv_id,
								delivery_unit_number.nextval deliv_unit_number,
								cst.max_cntr_vol max_cube,
								cst.min_cntr_vol min_vol,
								cst.MAX_CNTR_WGHT max_weight,
								cst.min_cntr_wght min_wght,
								0 assigned_weight,
								0 assigned_volume,
								case
								   when ceil(cst.min_cntr_vol / gb.UNIT_vol) >
																ceil(cst.min_cntr_wght / gb.unit_wt) then
												ceil(cst.min_cntr_vol / gb.UNIT_vol)
								   else
												ceil(cst.min_cntr_wght / gb.unit_wt)
								end min_num_case,
								case
								   when floor(cst.max_cntr_vol / gb.unit_vol) >
																floor(cst.max_cntr_wght / gb.unit_wt) then
												floor(cst.max_cntr_wght / gb.unit_wt)
								   else
												floor(cst.max_cntr_vol / gb.unit_vol)
								end max_num_case,
								cst.vol_range,
								cst.wght_range
				from global_main gb, c_storage_type cst
   where gb.tc_order_id = order_n
				and gb.item_id = item_i
				and cst.STORAGE_TYPE_ID = storage_t;

TYPE deliv_list is TABLE OF c_deliv_unit%ROWTYPE INDEX BY PLS_INTEGER;
deliv_list1 deliv_list;

-- variables to hold delivery units specification

v_order_cases        number := 0;
v_load_type          number := 0;
v_min_cases          number := 0;
v_max_cases          number := 0;
v_line_item_id       number := 0;
v_ovrd_prod_type     number := 0;
v_ovrd_prod_char     varchar2(500);
v_storage_type       number := 0;
v_main_storage_type  number := 0;
v_orig_order_qty     number := 0;
v_deliv_unit_number  number := 0;
v_main_facility_id   number := 0;
v_facility_alias_id  number := 0;
v_break_flag_cnt     number := 0;
v_break_flag_char    char(1);
v_equipment_id       number := 0;
v_dl_qty             number;
v_assigned_order_qty number;
temp                 number;
v_ovrd_load_type     number :=0;
v_ovrd_storage_type  number :=0;

BEGIN

select count(distinct break_by_stop)
  into v_break_flag_cnt
  from global_main
where break_by_stop is not null;

IF (v_break_flag_cnt = 2) THEN
  delete from c_bulk_process;
  insert into c_bulk_process
				select sum(orig_order_qty), storage_type
				  from global_main
				where break_by_stop = 'N'
				   and bulk_process = 'Y'
				group by storage_type;

  delete from c_bulk_process_stop;
  insert into c_bulk_process_stop
				select sum(orig_order_qty), storage_type, facility_id
				  from global_main
				where break_by_stop = 'Y'
				   and bulk_process = 'Y'
				group by storage_type, facility_id;
ELSE
  select distinct break_by_stop
				into v_break_flag_char
				from global_main
   where break_by_stop is not null;
  IF (v_break_flag_char = 'N') THEN
				delete from c_bulk_process;
				insert into c_bulk_process
				  select sum(orig_order_qty), storage_type
								from global_main
				   where break_by_stop = 'N'
								and bulk_process = 'Y'
				   group by storage_type;
  ELSIF (v_break_flag_char = 'Y') THEN
				delete from c_bulk_process_stop;
				insert into c_bulk_process_stop
				  select sum(orig_order_qty), storage_type, facility_id
								from global_main
				   where break_by_stop = 'Y'
								and bulk_process = 'Y'
				   group by storage_type, facility_id;
  END IF;
END IF;

--dbms_output.put_line('Begining of the Custom_load_diagram_dev');
IF g_log_level > 0 THEN
  exception_msg_log_insert('Begining of the Custom_load_diagram_dev',
																								   v_func_name,
																								   v_shipment,
																								   'INFO');
END IF;

for rec in c_deliv_id loop

  select count(*)
				into temp
				from global_main
   where deliv_id = rec.deliv_id;
  -- loop for each group of order types which are formed based on logic of 3.5.3
  open c_order(rec.deliv_id, v_shipment);
  if SQL%NOTFOUND then
				-- change start:SL
				--dbms_output.put_line('No data found hh');
				close c_order;
				-- change end:SL
				CONTINUE;
  end if;
  --dbms_output.put_line('Inside For loop');
  --fetch c_order into ord2;
  fetch c_order bulk collect
				into ord1;

  close c_order;

  v_deliv_id := rec.deliv_id;

  v_head_olist := ord1.first;
  v_tail_olist := ord1.last;
  IF g_log_level = 2 THEN

				exception_msg_log_insert('v_deliv_id: ' || v_deliv_id,
																												v_func_name,
																												v_shipment,
																												'DEBUG');
				exception_msg_log_insert('v_head_olist: ' || v_head_olist,
																												v_func_name,
																												v_shipment,
																												'DEBUG');
				exception_msg_log_insert('v_tail_olist: ' || v_tail_olist,
																												v_func_name,
																												v_shipment,
																												'DEBUG');
  END IF;

  --dbms_output.put_line('v_head_olist: ' || v_head_olist);
  -- delete the delivery list as new list will be created for new group of order lines
  if deliv_list1.count() >= 1 then
				IF g_log_level = 2 THEN
				  --dbms_output.put_line('Deleting delivery units');
				  exception_msg_log_insert('Deleting delivery units',
																												   v_func_name,
																												   v_shipment,
																												   'DEBUG');
				END IF;

				-- change : SL 260413 -delete in bulk-
				/*
				FOR recs IN deliv_list1.first .. deliv_list1.last LOOP
				  deliv_list1.Delete(recs);
				END LOOP;
				*/
				deliv_list1.delete();
				-- change end : SL 260413
  end if;

  v_cntr_deliv := 1;

  v_storage_type     := 0;
  v_main_facility_id := 0;

  --select sum(orig_order_qty) into

  -- loop through all the orders
  for i in 1 .. ord1.count() loop
				v_flag := 1;
				--if (i mod 2 <> 0) then
				v_cntr_order := v_head_olist;
				v_head_olist := v_head_olist + 1;

				v_load_type := ord1(v_cntr_order).load_type;
				v_bulk_process := ord1(v_cntr_order).bulk_process;
				v_break_by_stop := ord1(v_cntr_order).break_by_stop;

				IF g_log_level = 2 THEN
				  exception_msg_log_insert('v_order_cases : ' || v_order_cases,
																												   v_func_name,
																												   v_shipment,
																												   'DEBUG');
				  exception_msg_log_insert('v_load_type : ' || v_load_type,
																												   v_func_name,
																												   v_shipment,
																												   'DEBUG');
				  exception_msg_log_insert('v_bulk_process : ' || v_bulk_process,
																												   v_func_name,
																												   v_shipment,
																												   'DEBUG');
				  exception_msg_log_insert('v_break_by_stop : ' || v_break_by_stop,
																												   v_func_name,
																												   v_shipment,
																												   'DEBUG');
				END IF;

				IF (v_bulk_process = 'Y') THEN

				  ---BELOW CODE IS FOR BREAK_BY_STOP = Y--
				  IF g_log_level = 2 THEN
								--dbms_output.put_line('Do bulk processing for this order');
								exception_msg_log_insert('Do bulk processing for this order',
																																v_func_name,
																																v_shipment,
																																'DEBUG');

								exception_msg_log_insert('ord1(v_cntr_order).ORIG_ORDER_QTY : ' ||
																																ord1(v_cntr_order).ORIG_ORDER_QTY,
																																v_func_name,
																																v_shipment,
																																'DEBUG');
								exception_msg_log_insert('ord(v_cntr_order).load_type : ' ||
																																ord1(v_cntr_order).load_type,
																																v_func_name,
																																v_shipment,
																																'DEBUG');
				  END IF;
				  --v_deliv_unit_number := delivery_unit_number.nextval;

				  v_main_storage_type := ord1(v_cntr_order).storage_type;
				  v_main_facility_id  := ord1(v_cntr_order).facility_id;
				  v_min_cases         := ord1(v_cntr_order).MIN_CASE;
				  v_max_cases         := ord1(v_cntr_order).MAX_CASE;
				  v_order_cases       := ord1(v_cntr_order).ORIG_ORDER_QTY;
				  v_line_item_id      := ord1(v_cntr_order).LINE_ITEM_ID;

				  --dbms_output.put_line('v_main_storage_type :' ||
				  --                      v_main_storage_type ||
				  --                     ' v_main_facility_id: ' ||
				  --                     v_main_facility_id);
				  --dbms_output.put_line('v_storage_type :' || v_storage_type ||
				  --                     ' v_facility_alias_id: ' ||
				  --                     v_facility_alias_id);
				  IF (v_break_by_stop = 'Y') THEN
								IF (v_storage_type != v_main_storage_type OR
								   v_facility_alias_id != v_main_facility_id) THEN
								  --dbms_output.put_line('New storage type or facility encountered');
								  v_storage_type       := v_main_storage_type;
								  v_facility_alias_id  := v_main_facility_id;
								  v_deliv_unit_number  := delivery_unit_number.nextval;
								  v_dl_qty             := 0;
								  v_assigned_order_qty := 0;
								  select sum_orig_order_qty
												into v_orig_order_qty
												from c_bulk_process_stop
								   where storage_type = v_storage_type
												and facility_id = v_facility_alias_id;

								  --dbms_output.put_line('v_deliv_unit_number :' ||
								  --                     v_deliv_unit_number);
								END IF;

				  ELSIF (v_break_by_stop = 'N') THEN
								IF (v_storage_type != v_main_storage_type) THEN
								  --dbms_output.put_line('New storage type encountered');
								  v_storage_type       := v_main_storage_type;
								  v_facility_alias_id  := v_main_facility_id;
								  v_deliv_unit_number  := delivery_unit_number.nextval;
								  v_dl_qty             := 0;
								  v_assigned_order_qty := 0;
								  select sum_orig_order_qty
												into v_orig_order_qty
												from c_bulk_process
								   where storage_type = v_storage_type;

								  --dbms_output.put_line('v_deliv_unit_number :' ||
								  --                     v_deliv_unit_number);
								END IF;
				  END IF;

				  open c_deliv_unit(ord1(v_cntr_order).storage_type,
																								ord1(v_cntr_order).tc_order_id,
																								ord1(v_cntr_order).item_id); --,ord1(v_cntr_order).load_type);
				  fetch c_deliv_unit
								into deliv_list1(v_cntr_order);

				  close c_deliv_unit;
				  --dbms_output.put_line('Cursor open for: ' || ord1(v_cntr_order)
				  --                     .line_item_id);
				  --v_deliv_unit_number := delivery_unit_number.nextval;
				  insert into temp_hold_delivery_units
								(c_seq,
								deliv_id,
								deliv_unit_number,
								tc_order_id,
								item_id,
								order_qty_assigned,
								total_order_qty,
								order_wt,
								order_vol,
								max_volume,
								min_vol,
								max_wght,
								min_wght,
								assigned_wght,
								assigned_vol,
								min_num_case,
								max_num_case,
								header_flag,
								vol_range,
								wght_range,
								bulk_flag,
								equipment_id,
								storage_type,
								order_id,
								shipment_id,
								facility_id,
								load_type,
								order_type,
								line_item_id,
								item_name,
								order_loading_seq,
								category_id)
				  values
								(c_range_seq.nextval,
								rec.deliv_id,
								v_deliv_unit_number, --delivery_unit_number.nextval,
								0, --ord1(v_cntr_order).tc_order_id,
								0, --ord1(v_cntr_order).item_id,
								0,
								0, --v_order_cases,
								0, --ord1(v_cntr_order).order_wt,
								0, --ord1(v_cntr_order).order_vol,
								deliv_list1(v_cntr_order).max_cube,
								deliv_list1(v_cntr_order).min_vol,
								deliv_list1(v_cntr_order).max_weight,
								deliv_list1(v_cntr_order).min_wght,
								0,
								0,
								v_min_cases, --deliv_list1(v_cntr_order).min_num_case ,
								v_max_cases, --deliv_list1(v_cntr_order).max_num_case ,
								'Y',
								deliv_list1(v_cntr_order).vol_range,
								deliv_list1(v_cntr_order).wght_range,
								'Y',
								ord1(v_cntr_order).dsg_equipment_id,
								ord1(v_cntr_order).storage_type,
								0, --ord1(v_cntr_order).order_id,
								ord1(v_cntr_order).shipment_id,
								ord1(v_cntr_order).facility_id,
								ord1(v_cntr_order).load_type,
								'O',
								0, --ord1(v_cntr_order).line_item_id,
								0, --ord1(v_cntr_order).item_name
								0, --ord1(v_cntr_order).order_loading_seq
								ord1(v_cntr_order).category_id);

				  --dbms_output.put_line('v_orig_order_qty: ' || v_orig_order_qty ||
				  --                     ' v_dl_qty: ' || v_dl_qty);

				  IF (v_orig_order_qty + v_dl_qty >= v_min_cases) THEN

								IF (v_order_cases >= v_max_cases) THEN
								  WHILE (v_order_cases >= v_max_cases) LOOP
												--dbms_output.put_line('Inside while loop. v_order_cases: ' ||
												--                     v_order_cases || ' v_max_cases: ' ||
												--                     v_max_cases);

												v_assigned_order_qty := v_max_cases - v_dl_qty;
												insert into temp_hold_delivery_units
												  (c_seq,
												   deliv_id,
												   deliv_unit_number,
												   tc_order_id,
												   item_id,
												   order_qty_assigned,
												   total_order_qty,
												   order_wt,
												   order_vol,
												   max_volume,
												   min_vol,
												   max_wght,
												   min_wght,
												   assigned_wght,
												   assigned_vol,
												   min_num_case,
												   max_num_case,
												   header_flag,
												   vol_range,
												   wght_range,
												   bulk_flag,
												   equipment_id,
												   storage_type,
												   order_id,
												   shipment_id,
												   facility_id,
												   load_type,
												   order_type,
												   line_item_id,
												   item_name,
												   order_loading_seq,
												   category_id,
												   unit_wt,
												   unit_vol)
												values
												  (c_range_seq.nextval,
												   rec.deliv_id,
												   v_deliv_unit_number, --delivery_unit_number.nextval,
												   ord1(v_cntr_order).tc_order_id, --v_tc_order_id,
												   ord1(v_cntr_order).item_id,
												   v_assigned_order_qty,
												   v_order_cases,
												   ord1(v_cntr_order).order_wt,
												   ord1(v_cntr_order).order_vol,
												   deliv_list1(v_cntr_order).max_cube,
												   deliv_list1(v_cntr_order).min_vol,
												   deliv_list1(v_cntr_order).max_weight,
												   deliv_list1(v_cntr_order).min_wght,
												   0, --deliv_list1(v_cntr_order).assigned_weight, --0,
												   0, --deliv_list1(v_cntr_order).assigned_volume, --0,
												   v_min_cases,
												   v_max_cases,
												   'N',
												   deliv_list1(v_cntr_order).vol_range,
												   deliv_list1(v_cntr_order).wght_range,
												   'Y',
												   ord1(v_cntr_order).dsg_equipment_id,
												   ord1(v_cntr_order).storage_type,
												   ord1(v_cntr_order).order_id,
												   ord1(v_cntr_order).shipment_id,
												   ord1(v_cntr_order).facility_id,
												   ord1(v_cntr_order).load_type,
												   'O',
												   ord1(v_cntr_order).line_item_id,
												   ord1(v_cntr_order).item_name,
												   ord1(v_cntr_order).order_loading_seq,
												   ord1(v_cntr_order).category_id,
												   ord1(v_cntr_order).unit_wt,
												   ord1(v_cntr_order).unit_vol);

												v_deliv_unit_number := delivery_unit_number.nextval;
												v_dl_qty            := 0;
												v_orig_order_qty    := v_orig_order_qty -
																																   v_assigned_order_qty;
												v_order_cases       := v_order_cases - v_assigned_order_qty;
								  END LOOP;

								  IF (v_order_cases > 0) THEN
												IF (v_orig_order_qty >= v_min_cases) THEN
												  v_assigned_order_qty := v_order_cases;
												  --Insert statement for order detail: Begin--
												  insert into temp_hold_delivery_units
																(c_seq,
																deliv_id,
																deliv_unit_number,
																tc_order_id,
																item_id,
																order_qty_assigned,
																total_order_qty,
																order_wt,
																order_vol,
																max_volume,
																min_vol,
																max_wght,
																min_wght,
																assigned_wght,
																assigned_vol,
																min_num_case,
																max_num_case,
																header_flag,
																vol_range,
																wght_range,
																bulk_flag,
																equipment_id,
																storage_type,
																order_id,
																shipment_id,
																facility_id,
																load_type,
																order_type,
																line_item_id,
																item_name,
																order_loading_seq,
																category_id,
																unit_wt,
																unit_vol)
												  values
																(c_range_seq.nextval,
																rec.deliv_id,
																v_deliv_unit_number, --delivery_unit_number.nextval,
																ord1(v_cntr_order).tc_order_id, --v_tc_order_id,
																ord1(v_cntr_order).item_id,
																v_assigned_order_qty,
																v_order_cases,
																ord1(v_cntr_order).order_wt,
																ord1(v_cntr_order).order_vol,
																deliv_list1(v_cntr_order).max_cube,
																deliv_list1(v_cntr_order).min_vol,
																deliv_list1(v_cntr_order).max_weight,
																deliv_list1(v_cntr_order).min_wght,
																0, --deliv_list1(v_cntr_order).assigned_weight, --0,
																0, --deliv_list1(v_cntr_order).assigned_volume, --0,
																v_min_cases,
																v_max_cases,
																'N',
																deliv_list1(v_cntr_order).vol_range,
																deliv_list1(v_cntr_order).wght_range,
																'Y',
																ord1(v_cntr_order).dsg_equipment_id,
																ord1(v_cntr_order).storage_type,
																ord1(v_cntr_order).order_id,
																ord1(v_cntr_order).shipment_id,
																ord1(v_cntr_order).facility_id,
																ord1(v_cntr_order).load_type,
																'O',
																ord1(v_cntr_order).line_item_id,
																ord1(v_cntr_order).item_name,
																ord1(v_cntr_order).order_loading_seq,
																ord1(v_cntr_order).category_id,
																ord1(v_cntr_order).unit_wt,
																ord1(v_cntr_order).unit_vol);
												  --Insert statement for order detail: End--

												  v_orig_order_qty := v_orig_order_qty -
																																  v_assigned_order_qty;
												  v_order_cases    := v_order_cases - v_assigned_order_qty;
												  v_dl_qty         := v_assigned_order_qty;

												ELSE
												  --create child entry to global_main for non-bulk processing--
												  ---------------------------------------------------
												  v_ovrd_prod_type := ord1(v_cntr_order).ovrd_prod_type;
												  v_equipment_id   := ord1(v_cntr_order).DSG_EQUIPMENT_ID;

												  if v_ovrd_prod_type is null
												  then select prod_type
																into   v_ovrd_prod_char
																from   C_PROD_TYPE
																where  category_id = ord1(v_cntr_order).load_type;

												   -- error out here
													exception_msg_log_insert( v_ovrd_prod_char || ' is missing configuration for override product type  ',
																								   v_func_name,
																								   v_shipment,
																								   'ERROR');
																v_ovrd_prod_char:= null;
													return false;
												   -- error out here

												  end if;

												  IF g_log_level = 2 THEN
																--dbms_output.put_line('v_equipment_id: ' || v_equipment_id);
																exception_msg_log_insert('v_equipment_id: ' ||
																																								v_equipment_id,
																																								v_func_name,
																																								v_shipment,
																																								'DEBUG');
												  END IF;

												  select load_type_id ,storage_type_id
												  into v_ovrd_load_type,v_ovrd_storage_type
												  from c_equipment_prod_type
												  where prod_type_id = v_ovrd_prod_type
												  and equipment_id = v_equipment_id;

												  begin

																select distinct deliv_id
																  into v_deliv_id
																  from global_main
																where storage_type = v_ovrd_storage_type
																   and break_by_stop = v_break_by_stop
																   and facility_id = ord1(v_cntr_order).facility_id;
																--and equipment_id = v_equipment_id;


												  EXCEPTION
																				   when no_data_found then
																				   v_deliv_id := rec.deliv_id;
												  end;

												  update global_main
																set orig_order_qty = v_order_cases,
																				category_id    = v_ovrd_prod_type,
																				load_type      = v_ovrd_load_type,
																				storage_type   = v_ovrd_storage_type,
																				bulk_process   = 'N',
																				min_case       = null,
																				max_case       = null,
																				deliv_id       = v_deliv_id, --(10000 * v_storage_type + rec.deliv_id)
																				group_as_family = (select group_as_family from c_load_type clt where clt.load_type_id = v_ovrd_load_type)
												   where deliv_id = rec.deliv_id
																and bulk_process = 'Y'
																and break_by_stop = v_break_by_stop
																and line_item_id = v_line_item_id;

												  --reset v_order_cases for the order line--
												  v_order_cases := 0;

												END IF;
								  END IF;
								ELSE
								  WHILE (v_order_cases > 0) LOOP
												IF (v_orig_order_qty + v_dl_qty >= v_min_cases) THEN
												  IF (v_dl_qty = 0) THEN
																v_assigned_order_qty := v_order_cases;

																---Insert statement: Begin--
																insert into temp_hold_delivery_units
																  (c_seq,
																   deliv_id,
																   deliv_unit_number,
																   tc_order_id,
																   item_id,
																   order_qty_assigned,
																   total_order_qty,
																   order_wt,
																   order_vol,
																   max_volume,
																   min_vol,
																   max_wght,
																   min_wght,
																   assigned_wght,
																   assigned_vol,
																   min_num_case,
																   max_num_case,
																   header_flag,
																   vol_range,
																   wght_range,
																   bulk_flag,
																   equipment_id,
																   storage_type,
																   order_id,
																   shipment_id,
																   facility_id,
																   load_type,
																   order_type,
																   line_item_id,
																   item_name,
																   order_loading_seq,
																   category_id,
																   unit_wt,
																   unit_vol)
																values
																  (c_range_seq.nextval,
																   rec.deliv_id,
																   v_deliv_unit_number, --delivery_unit_number.nextval,
																   ord1(v_cntr_order).tc_order_id, --v_tc_order_id,
																   ord1(v_cntr_order).item_id,
																   v_assigned_order_qty,
																   v_order_cases,
																   ord1(v_cntr_order).order_wt,
																   ord1(v_cntr_order).order_vol,
																   deliv_list1(v_cntr_order).max_cube,
																   deliv_list1(v_cntr_order).min_vol,
																   deliv_list1(v_cntr_order).max_weight,
																   deliv_list1(v_cntr_order).min_wght,
																   0, --deliv_list1(v_cntr_order).assigned_weight, --0,
																   0, --deliv_list1(v_cntr_order).assigned_volume, --0,
																   v_min_cases,
																   v_max_cases,
																   'N',
																   deliv_list1(v_cntr_order).vol_range,
																   deliv_list1(v_cntr_order).wght_range,
																   'Y',
																   ord1(v_cntr_order).dsg_equipment_id,
																   ord1(v_cntr_order).storage_type,
																   ord1(v_cntr_order).order_id,
																   ord1(v_cntr_order).shipment_id,
																   ord1(v_cntr_order).facility_id,
																   ord1(v_cntr_order).load_type,
																   'O',
																   ord1(v_cntr_order).line_item_id,
																   ord1(v_cntr_order).item_name,
																   ord1(v_cntr_order).order_loading_seq,
																   ord1(v_cntr_order).category_id,
																   ord1(v_cntr_order).unit_wt,
																   ord1(v_cntr_order).unit_vol);
																---Insert statement: End--
																v_dl_qty         := v_dl_qty + v_assigned_order_qty;
																v_orig_order_qty := v_orig_order_qty - v_assigned_order_qty;
																v_order_cases    := v_order_cases - v_assigned_order_qty;
												  ELSE
																IF (v_order_cases <= v_max_cases - v_dl_qty) THEN
																  v_assigned_order_qty := v_order_cases;
																ELSE
																  v_assigned_order_qty := v_max_cases - v_dl_qty;
																END IF;

																---Insert statement for detail order line: BEGIN---
																insert into temp_hold_delivery_units
																  (c_seq,
																   deliv_id,
																   deliv_unit_number,
																   tc_order_id,
																   item_id,
																   order_qty_assigned,
																   total_order_qty,
																   order_wt,
																   order_vol,
																   max_volume,
																   min_vol,
																   max_wght,
																   min_wght,
																   assigned_wght,
																   assigned_vol,
																   min_num_case,
																   max_num_case,
																   header_flag,
																   vol_range,
																   wght_range,
																   bulk_flag,
																   equipment_id,
																   storage_type,
																   order_id,
																   shipment_id,
																   facility_id,
																   load_type,
																   order_type,
																   line_item_id,
																   item_name,
																   order_loading_seq,
																   category_id,
																   unit_wt,
																   unit_vol)
																values
																  (c_range_seq.nextval,
																   rec.deliv_id,
																   v_deliv_unit_number, --delivery_unit_number.nextval,
																   ord1(v_cntr_order).tc_order_id, --v_tc_order_id,
																   ord1(v_cntr_order).item_id,
																   v_assigned_order_qty,
																   v_order_cases,
																   ord1(v_cntr_order).order_wt,
																   ord1(v_cntr_order).order_vol,
																   deliv_list1(v_cntr_order).max_cube,
																   deliv_list1(v_cntr_order).min_vol,
																   deliv_list1(v_cntr_order).max_weight,
																   deliv_list1(v_cntr_order).min_wght,
																   0, --deliv_list1(v_cntr_order).assigned_weight, --0,
																   0, --deliv_list1(v_cntr_order).assigned_volume, --0,
																   v_min_cases,
																   v_max_cases,
																   'N',
																   deliv_list1(v_cntr_order).vol_range,
																   deliv_list1(v_cntr_order).wght_range,
																   'Y',
																   ord1(v_cntr_order).dsg_equipment_id,
																   ord1(v_cntr_order).storage_type,
																   ord1(v_cntr_order).order_id,
																   ord1(v_cntr_order).shipment_id,
																   ord1(v_cntr_order).facility_id,
																   ord1(v_cntr_order).load_type,
																   'O',
																   ord1(v_cntr_order).line_item_id,
																   ord1(v_cntr_order).item_name,
																   ord1(v_cntr_order).order_loading_seq,
																   ord1(v_cntr_order).category_id,
																   ord1(v_cntr_order).unit_wt,
																   ord1(v_cntr_order).unit_vol);
																---Insert statement for detail order line: END---
																v_dl_qty         := v_dl_qty + v_assigned_order_qty;
																v_orig_order_qty := v_orig_order_qty - v_assigned_order_qty;
																v_order_cases    := v_order_cases - v_assigned_order_qty;
												  END IF;
												  IF (v_dl_qty = v_max_cases) THEN
																v_deliv_unit_number := delivery_unit_number.nextval;
																v_dl_qty            := 0;
												  END IF;
												ELSE
												  --Treat this order as non-bulk :BEGIN--
												  --create child entry to global_main for non-bulk processing--
												  ---------------------------------------------------
												  --dbms_output.put_line('Sending ' || v_order_cases ||
												  --                     ' to non-bulk for processing.');

												  v_ovrd_prod_type := ord1(v_cntr_order).ovrd_prod_type;
												  v_equipment_id   := ord1(v_cntr_order).DSG_EQUIPMENT_ID;

												  if v_ovrd_prod_type is null
												  then

													select prod_type
																into   v_ovrd_prod_char
																from   C_PROD_TYPE
																where  category_id = ord1(v_cntr_order).load_type;

												   -- error out here
													exception_msg_log_insert( v_ovrd_prod_char || ' is missing configuration for override product type  ',
																								   v_func_name,
																								   v_shipment,
																								   'ERROR');
																v_ovrd_prod_char:= null;
													return false;
												  end if;


												  IF g_log_level = 2 THEN
																--dbms_output.put_line('v_equipment_id: ' || v_equipment_id);
																exception_msg_log_insert('v_equipment_id: ' ||v_equipment_id,
																						v_func_name,v_shipment,'DEBUG');
												  END IF;

												  select load_type_id ,storage_type_id
												  into v_ovrd_load_type,v_ovrd_storage_type
												  from C_EQUIPMENT_PROD_TYPE
												  where prod_type_id = v_ovrd_prod_type
												  and equipment_id = v_equipment_id;

												  BEGIN
																select distinct deliv_id
																  into v_deliv_id
																  from global_main
																where storage_type = v_ovrd_storage_type
																   and break_by_stop = v_break_by_stop
																   and facility_id = ord1(v_cntr_order).facility_id;

												  EXCEPTION
																								when no_data_found then
																								v_deliv_id := rec.deliv_id;
												  END;


												  update global_main
																set orig_order_qty = v_order_cases,
																				category_id    = v_ovrd_prod_type,
																				load_type      = v_ovrd_load_type,
																				storage_type   = v_ovrd_storage_type,
																				bulk_process   = 'N',
																				min_case       = null,
																				max_case       = null,
																				deliv_id       = v_deliv_id, --(10000 * v_storage_type + rec.deliv_id)
																				group_as_family = (select group_as_family from c_load_type clt where clt.load_type_id = v_ovrd_load_type)
												   where deliv_id = rec.deliv_id
																and bulk_process = 'Y'
																and break_by_stop = v_break_by_stop
																and line_item_id = v_line_item_id;

																v_order_cases := 0;
												  --Treat this order as non-bulk :END--
												END IF;
								  END LOOP;
								END IF;

				  ELSE
								--Treat this order as non-bulk :BEGIN--
								--create child entry to global_main for non-bulk processing--
								---------------------------------------------------
								--dbms_output.put_line('Inside Else part for converting to non-bulk');
								v_ovrd_prod_type := ord1(v_cntr_order).ovrd_prod_type;
								v_equipment_id   := ord1(v_cntr_order).DSG_EQUIPMENT_ID;

								if v_ovrd_prod_type is null
					then
								  -- error out here
								  select prod_type
									into   v_ovrd_prod_char
									from   C_PROD_TYPE
									where  category_id = ord1(v_cntr_order).load_type;

												   -- error out here
													exception_msg_log_insert( v_ovrd_prod_char || ' is missing configuration for override product type  ',
																								   v_func_name,
																								   v_shipment,
																								   'ERROR');
																v_ovrd_prod_char:= null;
													return false;
								end if;



								IF g_log_level = 2 THEN
								  --dbms_output.put_line('v_equipment_id: ' || v_equipment_id);
								  exception_msg_log_insert('v_equipment_id: ' ||
														   v_equipment_id,
														   v_func_name,
														   v_shipment,
														   'DEBUG');
								END IF;

								select load_type_id ,storage_type_id
								  into v_ovrd_load_type,v_ovrd_storage_type
								  from C_EQUIPMENT_PROD_TYPE
								where prod_type_id = v_ovrd_prod_type
								   and equipment_id = v_equipment_id;


								BEGIN
								  select distinct deliv_id
												into v_deliv_id
												from global_main
								   where storage_type = v_ovrd_storage_type
												and break_by_stop = v_break_by_stop
												and facility_id = ord1(v_cntr_order).facility_id;
								  --and equipment_id = v_equipment_id;

								EXCEPTION
																  when no_data_found then
																  v_deliv_id := rec.deliv_id;
								END;


								update global_main
								   set orig_order_qty = v_order_cases,
												   category_id    = v_ovrd_prod_type,
												   load_type      = v_ovrd_load_type,
												   storage_type   = v_ovrd_storage_type,
												   bulk_process   = 'N',
												   min_case       = null,
												   max_case       = null,
												   deliv_id       = v_deliv_id,
												   group_as_family = (select group_as_family from c_load_type clt where clt.load_type_id = v_ovrd_load_type)
								where deliv_id = rec.deliv_id
								   and bulk_process = 'Y'
								   and break_by_stop = v_break_by_stop
								   and line_item_id = v_line_item_id;

								v_order_cases := 0;
								--Treat this order as non-bulk :END--

				  END IF;

				  --END IF;
				ELSE
				  IF g_log_level > 0 THEN
								--dbms_output.put_line('Do not bulk process');
								exception_msg_log_insert('Do not bulk process',
								v_func_name,
								v_shipment,
								'INFO');
				  END IF;

				  EXIT;
				END IF;
				--dbms_output.put_line(v_cntr_order);

  end loop;

-- update the global table with the final assigned values from step 1

end loop;

-- Copy from the existing function: END--

return true;

EXCEPTION
WHEN NO_DATA_FOUND THEN
  select dsg_equipment_id,load_type
				into v_err_equip,v_err_load_type
				from global_main
   where deliv_id = v_deliv_id
				and ovrd_prod_type is null
				and  bulk_process = 'Y'
				and rownum = 1;
  exception_msg_log_insert('Erorr in create bulk delivery units for equipment id '||v_err_equip||' and load type '||v_err_load_type,
																								   v_func_name,
																								   v_shipment,
																								   'ERROR');

return false;
WHEN OTHERS THEN
  --dbms_output.put_line('Exception in create delivery units function' ||
  --             SQLCODE || SQLERRM);
  exception_msg_log_insert('Error in create bulk delivery units function - Set overide Prod type ' ||
																								   SQLCODE || SQLERRM,
																								   v_func_name,
																								   v_shipment,
																								   'ERROR');
  return false;
end create_bulk_delivery_units;
----Bulk delivery unit creation end--

   PROCEDURE exception_msg_log_insert(p_msg         msg_log.msg%TYPE DEFAULT NULL,
										p_func_name    msg_log.func_name%TYPE,
										p_ref_value_1  msg_log.ref_value_1%TYPE DEFAULT NULL,
										p_log_category varchar2 DEFAULT 'DEBUG') AS
				PRAGMA AUTONOMOUS_TRANSACTION;
				-- v_errmsg     msg_log.msg%TYPE := get_msg_info (p_module, p_msg_id, g_lang_id);
  begin
				-- v_errmsg := REPLACE (NVL (v_errmsg, '{0}'), '{0}', p_msg);
				INSERT INTO msg_log
				  (msg_log_id,
				   module,
				   msg_id,
				   pgm_id,
				   sub_pgm_name,
				   msg,
				   create_date_time,
				   user_id,
				   cd_master_id,
				  log_date_time,
				   mod_date_time,
				   whse,
				   func_name,
				   ref_value_1)
				VALUES
				  (msg_log_id_seq.nextval,
				   'CUST',
				   '0106',
				   'Generate Load Diagram',
				   p_log_category,
				   p_msg,
				   SYSDATE,
				   g_user_id,
				   g_company_id,
				   SYSDATE,
				   SYSDATE,
				   g_warehouse,
				   p_func_name,
				   p_ref_value_1);

				commit;
  END exception_msg_log_insert;

end CUSTOM_LOAD_DIAGRAM;
/
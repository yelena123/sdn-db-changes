create or replace trigger wm_val_derive_select_info
after insert or update on val_sql
referencing new as new
for each row
declare
    v_cursor        int;
    v_col_list      dbms_sql.desc_tab2;
    v_col_count     int;
-- todo: need to normalize chars and bytes
    v_max_col_len   number(3) := 200;
    v_current_date  date := sysdate;
begin
    if (regexp_instr(:new.sql_text, '^\s*(select|with)', 1, 1, 0, 'i') <= 0)
    then
        return;
    end if;

    delete from val_parsed_select_list vpsl
    where vpsl.sql_id = :new.sql_id;

    v_cursor := dbms_sql.open_cursor;
    dbms_sql.parse(v_cursor, :new.sql_text, dbms_sql.native);
    dbms_sql.describe_columns2(v_cursor, v_col_count, v_col_list);

    for i in 1..v_col_count
    loop
        if (v_col_list(i).col_type not in (1, 2, 96))
        then
            raise_application_error(-20050, 'Supported data types of selected '
                || 'columns are currently varchar2/char/number (found ' 
                || v_col_list(i).col_type || ').');
        end if;
        
        if (v_col_list(i).col_max_len > v_max_col_len)
        then
            raise_application_error(-20050, 'Selected column width/precision of '
                || v_col_list(i).col_max_len || ' exceeds supported maximum.');
        end if;

        insert into val_parsed_select_list
        (
            sql_id, pos, col_type, col_name
        )
        values
        (
            :new.sql_id, i, v_col_list(i).col_type, v_col_list(i).col_name
        );
    end loop;
    
    dbms_sql.close_cursor(v_cursor);
end;
/
show errors;

create or replace procedure manh_wave_pkt_creation
(
    p_user_id           in  user_profile.user_id%type,
    p_facility_id       in  facility.facility_id%type,
    p_pick_wave_nbr     in  ship_wave_parm.pick_wave_nbr%type,
    p_is_retail_wave    in  number
)
as
    v_whse whse_master.whse%type;
    v_rtl_pkt_flag varchar2(1) := (case when p_is_retail_wave = 1 then '1'
        else '0' end);
    v_prev_module_name  wm_utils.t_app_context_data;
    v_prev_action_name  wm_utils.t_app_context_data;	
begin
    select f.whse
    into v_whse
    from facility f
    where f.facility_id = p_facility_id;
	
    wm_utils.get_context_info(v_prev_module_name, v_prev_action_name);
    wm_utils.set_context_info(p_module_name => 'WAVE', p_action_name => 'PKT CREATION',
        p_client_id => p_pick_wave_nbr);

-- check: orders.do_status to improve performance
    insert into pkt_hdr
    (
        /*acct_rcvbl_acct_nbr, */acct_rcvbl_code, addr_code, addr_valid, advt_code,
        advt_date, bill_acct_nbr, bol_break_attr,
        cancel_date_time, carton_cubng_indic, cd_master_id,
        chute_id, cod_funds, create_date_time, cust_broker_acct_nbr, dc_ctr_nbr,
        duty_tax_acct_nbr, duty_tax_payment_type, est_carton,
        est_carton_bridged, est_pallet,
        est_pallet_bridged, frt_forwarder_acct_nbr, ftsr_nbr,
        global_locn_nbr, importer_defn, intl_goods_desc, is_cust_pickup_flag,
        is_direct_allow_flag, lang_id, major_minor_pkt,
        mhe_flag, mhe_ord_state, monetary_value, mv_currency_code,
        ord_date,
        original_tc_order_id, parent_ord_id, parties_related,
        partl_carton_optn, path_id, plt_cubng_indic, pkt_hdr_id, pkt_ctrl_nbr,
        pre_pack_flag, pre_stikr_code, primary_maxi_addr_nbr,
        repl_wave_nbr, rte_guide_nbr,
        rte_id, rte_swc_nbr, rte_wave_nbr,
        rtl_pkt_flag, sched_dlvry_date, secondary_maxi_addr_nbr,
        stage_indic, stat_code, store_nbr, total_carton, total_nbr_of_plt,
        transprt_resp_code, user_id, whse,
        pkt_type, pick_wave_nbr, d_facility_id, soldto,
        /*nbr_of_contnt_label, nbr_of_label, */nbr_of_pakng_slips, carton_label_type
        , is_hazmat_flag, sngl_unit_flag
    )
    select /*o.acct_rcvbl_acct_nbr, */o.acct_rcvbl_code, o.addr_code, o.addr_valid,
        o.advt_code, o.advt_date, o.bill_acct_nbr, o.bol_break_attr,
        o.cancel_dttm, o.lpn_cubing_indic, o.tc_company_id,
        o.chute_id, o.cod_funds, sysdate create_date_time, o.cust_broker_acct_nbr,
        o.dc_ctr_nbr, o.duty_tax_acct_nbr, o.duty_tax_payment_type, o.est_lpn,
        o.est_lpn_bridged,
        o.est_pallet, coalesce(o.est_pallet_bridged, 0),
        o.freight_forwarder_acct_nbr, o.ftsr_nbr,
        o.global_locn_nbr, o.importer_defn, o.intl_goods_desc,
        o.is_customer_pickup, o.is_direct_allowed, o.lang_id,
        case when o.major_minor_order = 'M' then 'M'
            else null end major_minor_pkt,
        o.mhe_flag, o.mhe_ord_state,
        coalesce(o.monetary_value, 0), o.mv_currency_code,
        o.order_date_dttm,
        o.tc_order_id, coalesce(o.parent_order_id, 0), o.parties_related,
        o.partial_lpn_option, coalesce(o.path_id, 0), o.pallet_cubing_indic,
        pkt_hdr_id_seq.nextval pkt_hdr_id, pkt_hdr_id_seq.nextval pkt_ctrl_nbr,
        o.pre_pack_flag, o.pre_sticker_code, o.primary_maxi_addr_nbr,
        o.repl_wave_nbr,
        o.lane_name rte_guide_nbr, o.dsg_static_route_id rte_id,
        o.rte_swc_nbr,
        o.rte_wave_nbr, v_rtl_pkt_flag,
        o.sched_delivery_dttm, o.secondary_maxi_addr_nbr,
        o.stage_indic, 10 stat_code, o.d_facility_alias_id store_nbr,
        o.total_nbr_of_lpn, o.total_nbr_of_plt, o.trans_resp_code, p_user_id,
        v_whse,
        case when o.wm_order_status = 10 then '03' else null end pkt_type,
        case when o.wm_order_status = 10 then p_pick_wave_nbr
            else null end pick_wave_nbr, o.d_facility_id,
        coalesce(o.bill_facility_alias_id, o.bill_to_facility_name),
        /*o.nbr_of_contnt_label, o.nbr_of_label, */coalesce(o.nbr_of_pakng_slips,0),
        o.lpn_label_type,case when o.is_hazmat = 1 then 'Y' else 'N' end is_hazmat_flag,
        o.single_unit_flag
    from orders o
    where o.o_facility_id = p_facility_id and o.parent_order_id is null
        and exists
        (
            select 1
            from order_line_item oli
            where oli.order_id = o.order_id
                and oli.wave_nbr = p_pick_wave_nbr
                and oli.do_dtl_status in (120, 130)
        )
        and
        (
            o.wm_order_status = 10
            or not exists
            (
                select 1
                from pkt_hdr ph
                where ph.original_tc_order_id = o.tc_order_id
                    and ph.cd_master_id = o.tc_company_id and ph.whse = v_whse
                    and ph.stat_code < 90 and ph.rtl_pkt_flag = v_rtl_pkt_flag
                    and coalesce(ph.major_minor_pkt, 'X') != 'P'
            )
        );

    -- transfer oli (aggregated or not) into pd's, one-to-one
    insert into pkt_dtl
    (
        actual_cost, alloc_type, assort_nbr, batch_nbr, batch_reqmt_type,
        budget_cost, cancel_qty, carton_break_attr,
        carton_type, chute_assign_type, cntry_of_orgn, create_date_time,
        critcl_dim_1, critcl_dim_2, critcl_dim_3, cube_mult_qty, cust_sku,
        distro_w_ctrl_nbr, event_code, exp_info_code, distro_type, item_id,
        invn_type, merch_grp, merch_type, mv_currency_code,
        orig_ord_qty, orig_pkt_qty, original_order_id, pack_rate, pack_wave_nbr,
        pack_zone, parent_line_item_id, partl_fill, pick_locn_assign_type,
        pick_locn_id, pkt_ctrl_nbr, pkt_dtl_id, pkt_hdr_id, pkt_qty,
        pkt_seq_nbr, plt_type, ppack_grp_code, ppack_qty, price,
        price_tkt_type, prod_stat, ref_field_1, ref_field_2,
        ref_field_3, reference_line_item_id, reference_order_id, repl_proc_type,
        retail_price, rtl_to_be_distroed_qty,
        shelf_days, ship_wave_nbr, sku_attr_1, sku_attr_2, sku_attr_3,
        sku_attr_4, sku_attr_5, sku_break_attr, sku_sub_code_id,
        sku_sub_code_value, srl_nbr_reqd_flag, stat_code, std_bundl_qty,
        std_case_qty, std_case_vol, std_case_wt, std_pack_qty, std_plt_qty,
        std_sub_pack_qty, unit_monetary_value,
        unit_vol, unit_wt, units_pakd, user_id, vas_proc_type,
        wave_nbr, wave_proc_type, wave_stat_code, srl_nbr_reqd,
        store_nbr, qty_uom_id_base, qty_uom_id, qty_conv_factor,
        pkt_consol_attr, tc_order_line_id, is_hazmat_flag, sngl_unit_flag
    )
    select coalesce(oli.actual_cost, 0), oli.alloc_type, oli.assort_nbr,
        oli.batch_nbr, oli.batch_requirement_type, coalesce(oli.budg_cost, 0),
        coalesce(oli.user_canceled_qty, 0), oli.lpn_brk_attrib,
        oli.lpn_type, oli.chute_assign_type,
        oli.cntry_of_orgn, sysdate create_date_time,
        coalesce(oli.critcl_dim_1, 0), coalesce(oli.critcl_dim_2, 0),
        coalesce(oli.critcl_dim_3, 0), coalesce(oli.cube_multiple_qty, 0),
        substr(oli.customer_item,1,20), o.tc_order_id, oli.event_code, oli.exp_info_code,
        case when p_is_retail_wave = 1 or ph.major_minor_pkt = 'M'
            then '1' else null end distro_type, oli.item_id, oli.invn_type,
        oli.merch_grp, oli.merch_type, o.mv_currency_code,
        oli.adjusted_order_qty, oli.allocated_qty, oli.order_id,
        coalesce(oli.pack_rate, 0), p_pick_wave_nbr pack_wave_nbr,
        oli.pack_zone, oli.parent_line_item_id, oli.partl_fill,
        oli.pick_locn_assign_type, oli.pick_locn_id, ph.pkt_ctrl_nbr,
        pkt_dtl_id_seq.nextval pkt_dtl_id, ph.pkt_hdr_id, oli.allocated_qty,
        pkt_dtl_id_seq.nextval pkt_seq_nbr,
        oli.pallet_type, oli.ppack_grp_code, oli.ppack_qty,
        coalesce(oli.price, 0), oli.price_tkt_type,
        oli.prod_stat, oli.ref_field1, oli.ref_field2, oli.ref_field3,
        oli.line_item_id, oli.order_id, coalesce(oli.repl_proc_type, 0),
        coalesce(oli.retail_price, 0),
        0 rtl_to_be_distroed_qty,
        coalesce(oli.shelf_days, 0), oli.ship_wave_nbr, oli.item_attr_1,
        oli.item_attr_2, oli.item_attr_3, oli.item_attr_4, oli.item_attr_5,
        oli.sku_break_attr, oli.sku_sub_code_id, oli.sku_sub_code_value,
        oli.serial_number_required_flag, 10 stat_code,
        coalesce(oli.std_bundle_qty, 0), coalesce(oli.std_lpn_qty, 0),
        coalesce(oli.std_lpn_vol, 0), coalesce(oli.std_lpn_wt, 0),
        coalesce(oli.std_pack_qty, 0), coalesce(oli.std_pallet_qty, 0),
        coalesce(oli.std_sub_pack_qty, 0),
        coalesce(oli.unit_monetary_value, 0), coalesce(oli.unit_vol, 0),
        coalesce(oli.unit_wt, 0), coalesce(oli.units_pakd, 0),
        p_user_id, oli.vas_process_type, p_pick_wave_nbr,
        coalesce(oli.wave_proc_type, 0), 10 wave_stat_code,
        oli.serial_number_required_flag, o.store_nbr, oli.qty_uom_id_base,
        oli.qty_uom_id, oli.qty_conv_factor, oli.order_consol_attr,
        oli.tc_order_line_id,
        case when oli.un_number_id is not null then 'Y' else 'N' end is_hazmat_flag,
        ph.sngl_unit_flag
    from orders o
    join order_line_item oli on oli.order_id = o.order_id
    join pkt_hdr ph on ph.original_tc_order_id = o.tc_order_id
        and ph.cd_master_id = o.tc_company_id
    where oli.wave_nbr = p_pick_wave_nbr and oli.do_dtl_status in (120, 130)
        and ph.whse = v_whse and ph.stat_code < 90 and o.parent_order_id is null
        and coalesce(ph.major_minor_pkt, 'X') != 'P'
        and ph.rtl_pkt_flag = v_rtl_pkt_flag and coalesce(o.wm_order_status,0) != 10;

    -- create pkt_dtl for vas-pending lines
    insert into pkt_dtl
    (
        actual_cost, alloc_type, assort_nbr, batch_nbr, batch_reqmt_type,
        budget_cost, cancel_qty, carton_break_attr,
        carton_type, chute_assign_type, cntry_of_orgn, create_date_time,
        critcl_dim_1, critcl_dim_2, critcl_dim_3, cube_mult_qty, cust_sku,
        distro_w_ctrl_nbr, event_code, exp_info_code, distro_type, item_id,
        invn_type, merch_grp, merch_type, mv_currency_code,
        orig_ord_qty, orig_pkt_qty, original_order_id, pack_rate, pack_wave_nbr,
        pack_zone, parent_line_item_id, partl_fill, pick_locn_assign_type,
        pick_locn_id, pkt_ctrl_nbr, pkt_dtl_id, pkt_hdr_id, pkt_qty,
        pkt_seq_nbr, plt_type, ppack_grp_code, ppack_qty, price,
        price_tkt_type, prod_stat, ref_field_1, ref_field_2,
        ref_field_3, reference_line_item_id, reference_order_id, repl_proc_type,
        retail_price, rtl_to_be_distroed_qty,
        shelf_days, ship_wave_nbr, sku_attr_1, sku_attr_2, sku_attr_3,
        sku_attr_4, sku_attr_5, sku_break_attr, sku_sub_code_id,
        sku_sub_code_value, srl_nbr_reqd_flag, stat_code, std_bundl_qty,
        std_case_qty, std_case_vol, std_case_wt, std_pack_qty, std_plt_qty,
        std_sub_pack_qty, unit_monetary_value,
        unit_vol, unit_wt, units_pakd, user_id, vas_proc_type,
        wave_nbr, wave_proc_type, wave_stat_code, srl_nbr_reqd,
        store_nbr, qty_uom_id_base, qty_uom_id, qty_conv_factor,
        pkt_consol_attr, tc_order_line_id, is_hazmat_flag, sngl_unit_flag
    )
    select coalesce(oli.actual_cost, 0), oli.alloc_type, oli.assort_nbr,
        oli.batch_nbr, oli.batch_requirement_type, coalesce(oli.budg_cost, 0),
        coalesce(oli.user_canceled_qty, 0), oli.lpn_brk_attrib,
        oli.lpn_type, oli.chute_assign_type,
        oli.cntry_of_orgn, sysdate create_date_time,
        coalesce(oli.critcl_dim_1, 0), coalesce(oli.critcl_dim_2, 0),
        coalesce(oli.critcl_dim_3, 0), coalesce(oli.cube_multiple_qty, 0),
        substr(oli.customer_item,1,20), o.tc_order_id, oli.event_code, oli.exp_info_code,
        case when p_is_retail_wave = 1 or ph.major_minor_pkt = 'M'
            then '1' else null end distro_type, oli.item_id, oli.invn_type,
        oli.merch_grp, oli.merch_type, o.mv_currency_code,
        oli.adjusted_order_qty, oli.allocated_qty, oli.order_id,
        coalesce(oli.pack_rate, 0), p_pick_wave_nbr pack_wave_nbr,
        oli.pack_zone, oli.parent_line_item_id, oli.partl_fill,
        oli.pick_locn_assign_type, oli.pick_locn_id, ph.pkt_ctrl_nbr,
        pkt_dtl_id_seq.nextval pkt_dtl_id, ph.pkt_hdr_id, oli.allocated_qty,
        pkt_dtl_id_seq.nextval pkt_seq_nbr,
        oli.pallet_type, oli.ppack_grp_code, oli.ppack_qty,
        coalesce(oli.price, 0), oli.price_tkt_type,
        oli.prod_stat, oli.ref_field1, oli.ref_field2, oli.ref_field3,
        oli.line_item_id, oli.order_id, coalesce(oli.repl_proc_type, 0),
        coalesce(oli.retail_price, 0),
        0 rtl_to_be_distroed_qty,
        coalesce(oli.shelf_days, 0), oli.ship_wave_nbr, oli.item_attr_1,
        oli.item_attr_2, oli.item_attr_3, oli.item_attr_4, oli.item_attr_5,
        oli.sku_break_attr, oli.sku_sub_code_id, oli.sku_sub_code_value,
        oli.serial_number_required_flag, 10 stat_code,
        coalesce(oli.std_bundle_qty, 0), coalesce(oli.std_lpn_qty, 0),
        coalesce(oli.std_lpn_vol, 0), coalesce(oli.std_lpn_wt, 0),
        coalesce(oli.std_pack_qty, 0), coalesce(oli.std_pallet_qty, 0),
        coalesce(oli.std_sub_pack_qty, 0),
        coalesce(oli.unit_monetary_value, 0), coalesce(oli.unit_vol, 0),
        coalesce(oli.unit_wt, 0), coalesce(oli.units_pakd, 0),
        p_user_id, oli.vas_process_type, p_pick_wave_nbr,
        coalesce(oli.wave_proc_type, 0), 10 wave_stat_code,
        oli.serial_number_required_flag, o.store_nbr, oli.qty_uom_id_base,
        oli.qty_uom_id, oli.qty_conv_factor, oli.order_consol_attr,
        oli.tc_order_line_id,
        case when oli.un_number_id is not null then 'Y' else 'N' end is_hazmat_flag,
        ph.sngl_unit_flag
    from orders o
    join order_line_item oli on oli.order_id = o.order_id
    join pkt_hdr ph on ph.original_tc_order_id = o.tc_order_id
        and ph.cd_master_id = o.tc_company_id
        and ph.pick_wave_nbr = oli.wave_nbr
    where oli.wave_nbr = p_pick_wave_nbr and oli.do_dtl_status in (120, 130)
        and o.wm_order_status = 10 and ph.whse = v_whse and ph.pkt_type = '03';

-- check: the order_note join for retail may result in a many-to-one
-- relationship, so vas retail is as yet unsupported
    insert into vas_pkt
    (
        pkt_ctrl_nbr, pkt_seq_nbr, stat_code, reqd_qty, cmpl_qty,
        create_date_time, mod_date_time, user_id, order_id, line_item_id,
        vas_pkt_id, grp_type
    )
    select pd.pkt_ctrl_nbr, pd.pkt_seq_nbr, 10 stat_code, pd.pkt_qty reqd_qty,
        0 cmpl_qty, sysdate, sysdate, p_user_id, pd.reference_order_id,
        pd.reference_line_item_id, vas_pkt_id_seq.nextval vas_pkt_id,
        ont.note_code grp_type
    from pkt_hdr ph
    join pkt_dtl pd on pd.pkt_ctrl_nbr = ph.pkt_ctrl_nbr
    join order_note ont on ont.order_id = pd.reference_order_id
        and ont.line_item_id = pd.reference_line_item_id
    where ph.whse = v_whse and ph.pick_wave_nbr = p_pick_wave_nbr
        and ph.pkt_type = '03' and ont.note_type = 'VS';
    
    update wave_parm wp
    set wp.wave_stat_code = 6, wp.mod_date_time = sysdate, wp.user_id = p_user_id
    where wp.wave_nbr = p_pick_wave_nbr and wp.whse = v_whse;

    commit;
	
    wm_utils.set_context_info(p_module_name => v_prev_module_name,
        p_action_name => v_prev_action_name);
end;
/
show errors;
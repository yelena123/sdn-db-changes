CREATE OR REPLACE
PROCEDURE UPDATEFIXEDCONSTRAINTS (V_RFPID INTEGER, V_SCENARIOID INTEGER)
IS
   v_comments           VARCHAR2 (255);
   v_comments_package   VARCHAR2 (255);
   v_contract_type      INTEGER;
   v_round_num          INTEGER;
   v_count              INTEGER;
   v_temp               INTEGER;
   v_volume             NUMBER (24, 3);
   v_award_type         INTEGER;
BEGIN
   DELETE FROM ob200override_adv
         WHERE rfpid = V_RFPID AND scenarioid = V_SCENARIOID AND TYPE = 'F'
               AND bid_id IN
                      (SELECT DISTINCT laneid
                         FROM scenarioadjustment
                        WHERE     rfpid = V_RFPID
                              AND scenarioid = V_SCENARIOID
                              AND laneincluded = 1);

   DELETE FROM ob200override_adv
         WHERE rfpid = V_RFPID AND scenarioid = V_SCENARIOID
               AND bid_id IN
                      (SELECT DISTINCT packageid
                         FROM scenarioadjustment
                        WHERE     rfpid = V_RFPID
                              AND scenarioid = V_SCENARIOID
                              AND laneincluded = 1
                              AND packageid <> 0)
               AND TYPE = 'F';

   SELECT round_num
     INTO v_round_num
     FROM scenario
    WHERE rfpid = V_RFPID AND scenarioid = V_SCENARIOID;

   UPDATE ob200override_adv
      SET enable_flag = 0
    WHERE rfpid = V_RFPID AND scenarioid = V_SCENARIOID
          AND bid_id IN
                 (SELECT LANEID
                    FROM scenarioadjustment sa
                   WHERE     rfpid = V_RFPID
                         AND scenarioid = V_SCENARIOID
                         AND sa.laneincluded = 0) -- write the lane conditions here)
          AND enable_flag = 1
          AND comments NOT LIKE 'SYS_%';

   v_count := 0;

   -- For each lane with laneincluded=0 we need to create override
   FOR q_cur
      IN (SELECT manualawardamount,
                 autoawardamount,
                 packageawardamount,
                 numdays,
                 obcarriercodeid,
                 historicalaward,
                 packageid,
                 laneequipmenttypeid,
                 laneid
            FROM scenarioadjustment sa
           WHERE     rfpid = V_RFPID
                 AND scenarioid = V_SCENARIOID
                 AND sa.laneincluded = 0
                 AND sa.laneid IN
                        (SELECT laneid
                           FROM ob200lane_view
                          WHERE rfpid = V_RFPID AND scenarioid = V_SCENARIOID))
   LOOP
      v_count := v_count + 1;

      SELECT oc.contract_type
        INTO v_contract_type
        FROM ob200contract_type oc, laneequipmenttype le
       WHERE COALESCE (oc.equipmenttype_id, -1) =
                COALESCE (le.equipmenttype_id, -1)
             AND COALESCE (oc.servicetype_id, -1) =
                    COALESCE (le.servicetype_id, -1)
             AND COALESCE (oc.protectionlevel_id, -1) =
                    COALESCE (le.protectionlevel_id, -1)
             AND COALESCE (oc.mot, '-1-') = COALESCE (le.mot, '-1-')
             AND COALESCE (oc.commoditycode_id, -1) =
                    COALESCE (le.commoditycode_id, -1)
             AND oc.rfpid = V_RFPID
             AND le.rfpid = V_RFPID
             AND le.laneid = q_cur.laneid
             AND le.laneEquipmenttypeId = q_cur.laneequipmenttypeid;

      --exception
      --when    no_data_found then
      --SET v_contract_type = 'A';
      v_comments := 'SYS_LOCK_' || TO_CHAR (q_cur.laneid);
      v_comments_package := 'SYS_LOCK_PACKAGE';

      IF NVL (q_cur.manualawardamount, 0) = 0
      THEN
         IF q_cur.autoawardamount <> 0
         THEN
            -- Two constraints
            SELECT COUNT (*)
              INTO v_temp
              FROM ob200override_adv
             WHERE     rfpid = V_RFPID
                   AND scenarioid = V_SCENARIOID
                   AND bid_id = q_cur.laneid
                   AND TYPE = 'F'
                   AND obcarriercodeid = q_cur.obcarriercodeid
                   AND contract_type = v_contract_type;

            IF v_temp = 0
            THEN
               SELECT volume
                 INTO v_volume
                 FROM ob200lane_view
                WHERE     rfpid = V_RFPID
                      AND laneid = q_cur.laneid
                      AND scenarioid = V_SCENARIOID;

               SELECT DISTINCT awardtype
                 INTO v_award_type
                 FROM rfp
                WHERE rfpid = V_RFPID;

               INSERT INTO OB200OVERRIDE_ADV (SCENARIOID,
                                              OVERRIDE_ID,
                                              ENABLE_FLAG,
                                              SOURCE,
                                              LEVEL_OF_DETAIL,
                                              TYPE,
                                              EQUIP_TYPE,
                                              CONTRACT_TYPE,
                                              OBCARRIERCODEID,
                                              BID_ID,
                                              MAX_VALUE,
                                              MIN_VALUE,
                                              TIMESTAMP,
                                              IS_VALID,
                                              ADDITIONAL_CRITERIA,
                                              RFPID,
                                              ROUND_NUM,
                                              COMMENTS,
                                              HISTORICALAWARD,
                                              SLACK)
                    VALUES (
                              V_SCENARIOID,
                              override_id_seq.nextval,
                              1,
                              'O',
                              'L',
                              'F',
                              'A',
                              V_CONTRACT_TYPE,
                              q_cur.obcarriercodeid,
                              q_cur.laneid,
                              -1,
                              CASE v_award_type
                                 WHEN 1
                                 THEN
                                      q_cur.autoawardamount
                                    * (7 / q_cur.numdays)
                                    * v_volume
                                    / 100
                                 ELSE
                                    q_cur.autoawardamount
                                    * (7 / q_cur.numdays)
                              END,
                              SYSDATE,
                              1,
                              NULL,
                              V_RFPID,
                              v_round_num,
                              v_comments,
                              q_cur.historicalaward,
                              1);
            END IF;
         ELSIF q_cur.packageawardamount <> 0
         THEN
            SELECT COUNT (*)
              INTO v_temp
              FROM ob200override_adv
             WHERE     rfpid = V_RFPID
                   AND scenarioid = V_SCENARIOID
                   AND bid_id = q_cur.packageid
                   AND TYPE = 'F'
                   AND obcarriercodeid = q_cur.obcarriercodeid
                   AND contract_type = v_contract_type;

            IF v_temp = 0
            THEN
               INSERT INTO OB200OVERRIDE_ADV (SCENARIOID,
                                              OVERRIDE_ID,
                                              ENABLE_FLAG,
                                              SOURCE,
                                              LEVEL_OF_DETAIL,
                                              TYPE,
                                              EQUIP_TYPE,
                                              CONTRACT_TYPE,
                                              OBCARRIERCODEID,
                                              BID_ID,
                                              MAX_VALUE,
                                              MIN_VALUE,
                                              TIMESTAMP,
                                              IS_VALID,
                                              ADDITIONAL_CRITERIA,
                                              RFPID,
                                              ROUND_NUM,
                                              COMMENTS,
                                              HISTORICALAWARD,
                                              SLACK)
                    VALUES (V_SCENARIOID,
                            override_id_seq.nextval,
                            1,
                            'O',
                            'P',
                            'F',
                            'A',
                            V_CONTRACT_TYPE,
                            q_cur.obcarriercodeid,
                            q_cur.packageid,
                            1,
                            -1,
                            SYSDATE,
                            1,
                            NULL,
                            V_RFPID,
                            v_round_num,
                            v_comments_package,
                            q_cur.historicalaward,
                            1);
            END IF;
         ELSIF q_cur.packageid = 0
         THEN
            SELECT COUNT (*)
              INTO v_temp
              FROM ob200override_adv
             WHERE     rfpid = V_RFPID
                   AND scenarioid = V_SCENARIOID
                   AND bid_id = q_cur.laneid
                   AND TYPE = 'F'
                   AND obcarriercodeid = q_cur.obcarriercodeid
                   AND contract_type = v_contract_type;

            IF v_temp = 0
            THEN
               INSERT INTO OB200OVERRIDE_ADV (SCENARIOID,
                                              OVERRIDE_ID,
                                              ENABLE_FLAG,
                                              SOURCE,
                                              LEVEL_OF_DETAIL,
                                              TYPE,
                                              EQUIP_TYPE,
                                              CONTRACT_TYPE,
                                              OBCARRIERCODEID,
                                              BID_ID,
                                              MAX_VALUE,
                                              MIN_VALUE,
                                              TIMESTAMP,
                                              IS_VALID,
                                              ADDITIONAL_CRITERIA,
                                              RFPID,
                                              ROUND_NUM,
                                              COMMENTS,
                                              HISTORICALAWARD,
                                              SLACK)
                    VALUES (V_SCENARIOID,
                            override_id_seq.nextval,
                            1,
                            'O',
                            'L',
                            'F',
                            'A',
                            V_CONTRACT_TYPE,
                            q_cur.obcarriercodeid,
                            q_cur.laneid,
                            0,
                            -1,
                            SYSDATE,
                            1,
                            NULL,
                            V_RFPID,
                            v_round_num,
                            v_comments,
                            q_cur.historicalaward,
                            1);
            END IF;
         ELSE
            SELECT COUNT (*)
              INTO v_temp
              FROM ob200override_adv
             WHERE     rfpid = V_RFPID
                   AND scenarioid = V_SCENARIOID
                   AND bid_id = q_cur.packageid
                   AND TYPE = 'F'
                   AND obcarriercodeid = q_cur.obcarriercodeid
                   AND contract_type = v_contract_type;

            IF v_temp = 0
            THEN
               INSERT INTO OB200OVERRIDE_ADV (SCENARIOID,
                                              OVERRIDE_ID,
                                              ENABLE_FLAG,
                                              SOURCE,
                                              LEVEL_OF_DETAIL,
                                              TYPE,
                                              EQUIP_TYPE,
                                              CONTRACT_TYPE,
                                              OBCARRIERCODEID,
                                              BID_ID,
                                              MAX_VALUE,
                                              MIN_VALUE,
                                              TIMESTAMP,
                                              IS_VALID,
                                              ADDITIONAL_CRITERIA,
                                              RFPID,
                                              ROUND_NUM,
                                              COMMENTS,
                                              HISTORICALAWARD,
                                              SLACK)
                    VALUES (V_SCENARIOID,
                            override_id_seq.nextval,
                            1,
                            'O',
                            'P',
                            'F',
                            'A',
                            V_CONTRACT_TYPE,
                            q_cur.obcarriercodeid,
                            q_cur.packageid,
                            0,
                            -1,
                            SYSDATE,
                            1,
                            NULL,
                            V_RFPID,
                            v_round_num,
                            v_comments_package,
                            q_cur.historicalaward,
                            1);
            END IF;
         END IF;
      END IF;
   END LOOP;
END UPDATEFIXEDCONSTRAINTS;
/



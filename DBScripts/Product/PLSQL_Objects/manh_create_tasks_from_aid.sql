create or replace procedure manh_create_tasks_from_aid
(   
    p_whse                  in whse_master.whse%type,
    p_user_id               in ucl_user.user_name%type,
    p_need_id               in task_hdr.need_id%type,
    p_task_parm_id          in task_parm.task_parm_id%type,
    p_rule_id               in rule_hdr.rule_id%type,
    p_task_batch            in task_rule_parm.task_batch%type,
    p_task_desc             in task_rule_parm.task_desc%type,
    p_task_type             in task_rule_parm.task_type%type,
    p_task_prty             in task_rule_parm.task_prty%type,
    p_task_create_stat_code in task_rule_parm.task_create_stat_code%type,
    p_pick_to_tote_flag     in task_rule_parm.pick_to_tote_flag%type,
    p_prt_task_list         in task_rule_parm.prt_task_list%type,
    p_rpt_prtr_reqstr       in task_rule_parm.rpt_prtr_reqstr%type,
    p_cart_plan_id          in task_rule_parm.cart_plan_id%type,
    p_rls_date_time         in task_hdr.rls_date_time%type
)
as
    v_current_timestamp date := sysdate;
begin
    -- the spec says to take certain values from the aid even though several
    -- aid's go into one task_hdr; to accomplish this, we use the aid values
    -- corresponding to the very first task_dtl
    insert into task_hdr
    (
        task_id, whse, invn_need_type, need_id, task_batch, task_type, 
        stat_code, task_desc, task_parm_id, pick_to_tote_flag, rls_date_time,
        dflt_task_prty, curr_task_prty, rule_id, prt_task_list_flag, 
        rpt_prtr_reqstr, create_date_time, mod_date_time, user_id, 
        task_cmpl_ref_code, task_cmpl_ref_nbr, task_hdr_id, wm_version_id,
        task_genrtn_ref_nbr, task_genrtn_ref_code, pick_cart_type
    )
    select t.task_id, p_whse, t.invn_need_type, p_need_id,
        coalesce(p_task_batch, t.task_batch) task_batch, 
        coalesce(p_task_type, t.task_type) task_type, p_task_create_stat_code,
        p_task_desc, p_task_parm_id, p_pick_to_tote_flag,
        case when p_task_create_stat_code >= 10 then p_rls_date_time
            else null end rls_date_time, p_task_prty dflt_task_prty,
        p_task_prty curr_task_prty, p_rule_id,
        (case when p_task_create_stat_code = 10 and p_prt_task_list = 'Y' 
            then 'Y' else 'N' end) prt_task_list_flag, 
        (case when p_task_create_stat_code = 10 and p_prt_task_list = 'Y' 
            then p_rpt_prtr_reqstr else null end) rpt_prtr_reqstr,
        v_current_timestamp, v_current_timestamp, p_user_id, 
        t.th_task_cmpl_ref_code, t.th_task_cmpl_ref_nbr, t.task_id,
        1 wm_version_id, t.task_genrtn_ref_nbr, t.task_genrtn_ref_code,
        case when p_cart_plan_id is not null then 0 else null end
    from tmp_task_creation_selected_aid t
    where t.task_seq_nbr = 1;
    wm_cs_log('Created task headers ' || sql%rowcount);

    insert all
    when 1 = 1 then
    into task_dtl
    (
        alloc_invn_code, alloc_invn_dtl_id, alloc_uom, alloc_uom_qty, batch_nbr,
        carton_nbr, carton_seq_nbr, cd_master_id, cntr_nbr, cntry_of_orgn,
        create_date_time, curr_work_area, curr_work_grp, dest_locn_id, 
        dest_locn_seq, erlst_start_date_time, full_cntr_allocd, invn_need_type,
        invn_type, ltst_cmpl_date_time, ltst_start_date_time, 
        misc_alpha_field_1, misc_alpha_field_2, misc_alpha_field_3, 
        mod_date_time, orig_reqmt, pick_seq_code, pikr_nbr, pkt_ctrl_nbr, 
        pkt_seq_nbr, prod_stat, pull_locn_id, qty_alloc, qty_pulld, 
        reqd_batch_nbr, reqd_cntry_of_orgn, reqd_invn_type, reqd_prod_stat,
        reqd_sku_attr_1, reqd_sku_attr_2, reqd_sku_attr_3, reqd_sku_attr_4, 
        reqd_sku_attr_5, sku_attr_1, sku_attr_2, sku_attr_3, sku_attr_4, 
        sku_attr_5, stat_code, substitution_flag, task_cmpl_ref_code, 
        task_cmpl_ref_nbr, task_cmpl_ref_nbr_seq, task_genrtn_ref_code, 
        task_genrtn_ref_nbr, task_id, task_prty, task_seq_nbr, task_type, 
        trans_invn_type, user_id, task_hdr_id, item_id,
        task_dtl_id, tc_order_id, line_item_id
    )
    values
    (
        alloc_invn_code, alloc_invn_dtl_id, alloc_uom, alloc_uom_qty, batch_nbr, carton_nbr, 
        carton_seq_nbr, cd_master_id, cntr_nbr, cntry_of_orgn, v_current_timestamp, curr_work_area,
        curr_work_grp, dest_locn_id, dest_locn_seq, erlst_start_date_time, full_cntr_allocd, 
        invn_need_type, invn_type, ltst_cmpl_date_time, ltst_start_date_time, misc_alpha_field_1, 
        misc_alpha_field_2, misc_alpha_field_3, v_current_timestamp, orig_reqmt, pick_seq_code, 
        pikr_nbr, pkt_ctrl_nbr, pkt_seq_nbr, prod_stat, pull_locn_id, qty_alloc, qty_pulld, 
        reqd_batch_nbr, reqd_cntry_of_orgn, reqd_invn_type, reqd_prod_stat, reqd_sku_attr_1, 
        reqd_sku_attr_2, reqd_sku_attr_3, reqd_sku_attr_4, reqd_sku_attr_5, sku_attr_1, sku_attr_2, 
        sku_attr_3, sku_attr_4, sku_attr_5, stat_code, substitution_flag, task_cmpl_ref_code, 
        task_cmpl_ref_nbr, task_cmpl_ref_nbr_seq, task_genrtn_ref_code, task_genrtn_ref_nbr, 
        task_id, task_prty, task_seq_nbr, task_type, trans_invn_type, p_user_id, task_id, item_id,
        task_dtl_id_seq.nextval, tc_order_id, line_item_id
    )
    when parent_aid_id is not null then
    into alloc_invn_dtl
    (
        alloc_invn_code, alloc_invn_dtl_id, alloc_uom, alloc_uom_qty, batch_nbr, carton_nbr, 
        carton_seq_nbr, cd_master_id, cntry_of_orgn, cntr_nbr, create_date_time, dest_locn_id, 
        dest_locn_seq_nbr, erlst_start_date_time, full_cntr_allocd, invn_need_type, invn_type, 
        item_id, line_item_id, ltst_cmpl_date_time, ltst_start_date_time, misc_alpha_field_1, 
        misc_alpha_field_2, misc_alpha_field_3, mod_date_time, need_id, orig_reqmt, pikr_nbr, 
        pkt_ctrl_nbr, pkt_seq_nbr, prod_stat, pull_locn_id, pull_locn_seq_nbr, qty_alloc, 
        qty_pulld, reqd_batch_nbr, reqd_cntry_of_orgn, reqd_invn_type, reqd_prod_stat, 
        reqd_sku_attr_1, reqd_sku_attr_2, reqd_sku_attr_3, reqd_sku_attr_4, reqd_sku_attr_5, 
        reseq_alloc, sku_attr_1, sku_attr_2, sku_attr_3, sku_attr_4, sku_attr_5, stat_code, 
        substitution_flag, task_batch, task_cmpl_ref_code, task_cmpl_ref_nbr, task_cmpl_ref_nbr_seq, 
        task_genrtn_ref_code, task_genrtn_ref_nbr, task_prty, task_type, tc_order_id, tote_nbr, 
        trans_invn_type, user_id, whse
    )
    values
    (
        alloc_invn_code, alloc_invn_dtl_id, alloc_uom, alloc_uom_qty, batch_nbr, carton_nbr, 
        carton_seq_nbr, cd_master_id, cntry_of_orgn, cntr_nbr, v_current_timestamp, dest_locn_id, 
        dest_locn_seq_nbr, erlst_start_date_time, full_cntr_allocd, invn_need_type, invn_type, 
        item_id, line_item_id, ltst_cmpl_date_time, ltst_start_date_time, misc_alpha_field_1, 
        misc_alpha_field_2, misc_alpha_field_3, v_current_timestamp, need_id, orig_reqmt, pikr_nbr, 
        pkt_ctrl_nbr, pkt_seq_nbr, prod_stat, pull_locn_id, pull_locn_seq_nbr, qty_alloc, 
        qty_pulld, reqd_batch_nbr, reqd_cntry_of_orgn, reqd_invn_type, reqd_prod_stat, 
        reqd_sku_attr_1, reqd_sku_attr_2, reqd_sku_attr_3, reqd_sku_attr_4, reqd_sku_attr_5, 
        reseq_alloc, sku_attr_1, sku_attr_2, sku_attr_3, sku_attr_4, sku_attr_5, stat_code, 
        substitution_flag, task_batch, task_cmpl_ref_code, task_cmpl_ref_nbr, task_cmpl_ref_nbr_seq, 
        task_genrtn_ref_code, task_genrtn_ref_nbr, task_prty, task_type, tc_order_id, tote_nbr, 
        trans_invn_type, p_user_id, p_whse
    )
    select aid.alloc_invn_code, t.alloc_invn_dtl_id, aid.alloc_uom, aid.tote_nbr, aid.task_batch,
        aid.alloc_uom_qty, aid.batch_nbr, aid.carton_nbr, aid.carton_seq_nbr, aid.reseq_alloc,
        aid.cd_master_id, aid.cntr_nbr, aid.cntry_of_orgn, v_current_timestamp, aid.need_id,
        lh.work_area curr_work_area, lh.work_grp curr_work_grp, aid.pull_locn_seq_nbr,
        t.dest_locn_id, aid.dest_locn_seq_nbr dest_locn_seq, aid.dest_locn_seq_nbr,
        aid.erlst_start_date_time, aid.full_cntr_allocd, aid.invn_need_type, t.parent_aid_id,
        aid.invn_type, aid.ltst_cmpl_date_time, aid.ltst_start_date_time, 
        aid.misc_alpha_field_1, aid.misc_alpha_field_2, aid.misc_alpha_field_3,
        aid.orig_reqmt, 
        substr(lh.locn_pick_seq, 1, 10) pick_seq_code, aid.pikr_nbr, 
        aid.pkt_ctrl_nbr, aid.pkt_seq_nbr, aid.prod_stat, aid.pull_locn_id,
        t.qty_alloc, aid.qty_pulld, aid.reqd_batch_nbr, 
        aid.reqd_cntry_of_orgn, aid.reqd_invn_type, aid.reqd_prod_stat, 
        aid.reqd_sku_attr_1, aid.reqd_sku_attr_2, aid.reqd_sku_attr_3, 
        aid.reqd_sku_attr_4, aid.reqd_sku_attr_5, aid.sku_attr_1, 
        aid.sku_attr_2, aid.sku_attr_3, aid.sku_attr_4, aid.sku_attr_5, 
        aid.stat_code, aid.substitution_flag, aid.task_cmpl_ref_code, 
        aid.task_cmpl_ref_nbr, aid.task_cmpl_ref_nbr_seq, 
        aid.task_genrtn_ref_code, aid.task_genrtn_ref_nbr, t.task_id, 
        p_task_prty task_prty, t.task_seq_nbr, 
        coalesce(p_task_type, aid.task_type) task_type, aid.trans_invn_type, 
        aid.item_id, aid.tc_order_id, aid.line_item_id
    from alloc_invn_dtl aid
    join tmp_task_creation_selected_aid t on coalesce(t.parent_aid_id, t.alloc_invn_dtl_id)
        = aid.alloc_invn_dtl_id
    left join locn_hdr lh on lh.locn_id = aid.pull_locn_id;
    wm_cs_log('Created task details ' || sql%rowcount);
end;
/
show errors;

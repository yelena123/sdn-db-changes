create or replace procedure manh_wave_load_molpn_orders
(
    p_wave_by_rule          in varchar2,
    p_list_of_shpmts        in varchar2,
    p_list_of_orders        in varchar2,
    p_facility_id           in whse_master.whse_master_id%type,
    p_perf_rte              in ship_wave_parm.perf_rte%type,
    p_tc_company_id         in wave_parm.tc_company_id%type,
    p_rule_id               in wave_rule_parm.rule_id%type,
    p_rule_type             in rule_hdr.rule_type%type,
    p_rte_wave_option       in opt_param.param_value%type
)
as
    v_order_selection_sql   varchar2(32600);
    v_select_rule           varchar2(4000);
begin
    v_order_selection_sql
        := ' insert into tmp_wave_molpn_orders (order_id)'
        || ' select distinct orders.order_id'
        || ' from orders'
        || ' where orders.o_facility_id = :facility_id'
        || '    and orders.has_import_error = 0'
        || '    and orders.parent_order_id is null '
        || '    and orders.do_status between 120 and 165 '
        || '    and not exists'
        || '    ('
        || '        select 1'
        || '        from tmp_wave_selected_orders t'
        || '        inner_criteria t.order_id = orders.order_id'
        || '    )';

    if (p_perf_rte = '1')
    then
        v_order_selection_sql := v_order_selection_sql
            || case p_rte_wave_option 
                when '3' then ' and orders.order_status = 10' -- unplan
                when '2' then ' and orders.order_status in (5, 10) ' -- re-route
                else ' and orders.order_status = 5 ' -- route
               end;
    end if;

    if (p_tc_company_id is not null)
    then
        v_order_selection_sql := v_order_selection_sql 
            || ' and orders.tc_company_id = :tc_company_id ';
    end if;

    if (p_wave_by_rule = 'Y')
    then
        select manh_get_rule(p_rule_id, 0, p_rule_type)
        into v_select_rule
        from dual;

        -- select clause is mandatory
        if (v_select_rule is null)
        then
            return;
        end if;

    elsif (p_list_of_orders is not null)
    then
        -- orders/loads are quoted lists of csv's, in parantheses
        v_select_rule := ' orders.tc_order_id in ' || p_list_of_orders;
    elsif (p_list_of_shpmts is not null)
    then
        v_select_rule := ' orders.tc_shipment_id in ' || p_list_of_shpmts;
    else
        raise_application_error(-20050, 
            'Cannot wave by anything other than rules/loads/orders');
    end if;

    -- build sql
    v_order_selection_sql := v_order_selection_sql || ' and ' || v_select_rule;
    manh_wave_add_joins_molpn(v_order_selection_sql);
    v_order_selection_sql := replace(v_order_selection_sql, 'inner_criteria',
        'where');

    -- get the list of orders to be processed in this wave
    begin
        if (p_tc_company_id is not null)
        then
            execute immediate v_order_selection_sql using p_facility_id, 
                p_tc_company_id;
        else
            execute immediate v_order_selection_sql using p_facility_id;
        end if;
    wm_cs_log('Loaded ' || sql%rowcount || ' OLI''s for molpn rule ' || p_rule_id, p_sql_log_level => 1);
    exception
        when others then
            raise;
    end;
end;
/
show errors ;
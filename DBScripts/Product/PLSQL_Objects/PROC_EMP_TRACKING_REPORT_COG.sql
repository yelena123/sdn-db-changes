create or replace
PROCEDURE      PROC_EMP_TRACKING_REPORT_COG (PWHSE             IN VARCHAR,
                                    PEMP_LOGIN_LIST   IN VARCHAR,
                                    PDATE_RANGE1      IN DATE,
                                    PDATE_RANGE2      IN DATE,
                                    PREPORT_ID           VARCHAR,
                                    C_RES_CUR OUT SYS_REFCURSOR
)
   AUTHID CURRENT_USER
AS
   NUM_RECORDS                    INTEGER := 0;
   k                              INTEGER := 1;

   TEMP_INT                       INTEGER := 0;
   TEMP_STR                       VARCHAR2 (4000);
   TEMP_STR2                      VARCHAR2 (4000);
   NUM_LOG                        INTEGER := 0;

   PREV_ACTUAL_END_DATE           TIMESTAMP := NULL;
   CURR_SCHED_START_DATE          TIMESTAMP := NULL;
   PREV_CLOCK_IN_DATE             TIMESTAMP := NULL;
   PREV_CLOCK_OUT_DATE            TIMESTAMP := NULL;
   CURR_CLOCK_IN_DATE             TIMESTAMP := NULL;
   PREV_LOGIN_USER_ID             VARCHAR2 (200) := NULL;
   PREV_USER_FIRST_NAME           VARCHAR2 (200) := NULL;
   PREV_USER_LAST_NAME            VARCHAR2 (200) := NULL;

   ORDER_BY_CLAUSE                VARCHAR2 (1000)
      := ' ORDER BY VIEW_EVNT_SMRY_HDR.LOGIN_USER_ID,E_EMP_PERF_SMRY.CLOCK_IN_DATE,
    VIEW_EVNT_SMRY_HDR.SCHED_START_DATE, VIEW_EVNT_SMRY_HDR.ACTUAL_END_DATE,
    E_EMP_PERF_SMRY.CLOCK_OUT_DATE';

   TYPE RefCurTyp IS REF CURSOR;

   EMP_TR_CURSOR                  RefCurTyp;

   TYPE emp_tr_row_tab IS TABLE OF EMP_TRACKING_REPORT_TABLE_COG%ROWTYPE;

   EMP_TR_ROW_TAB_INST            emp_tr_row_tab;
   EMP_TR_ROW_TAB_INSERT_BUFFER   emp_tr_row_tab := emp_tr_row_tab ();

   QUERY_STR1                     VARCHAR2 (15000)
      := 'SELECT SYSDATE AS REPORTDATE,
       ''111111111111111111111.554'' AS REPORTID,
       ''Normal Row Type'' AS REPORT_ROW_TYPE,
       VIEW_EVNT_SMRY_HDR.LOGIN_USER_ID,
       UCL_USER.USER_LAST_NAME,
       UCL_USER.USER_FIRST_NAME,
       E_EMP_PERF_SMRY.CLOCK_IN_DATE,
       E_EMP_PERF_SMRY.CLOCK_OUT_DATE,
       NULL AS PREV_ACTUAL_END_DATE,
       VIEW_EVNT_SMRY_HDR.SCHED_START_DATE,
       VIEW_EVNT_SMRY_HDR.ACTUAL_END_DATE,
       E_ACT.LABOR_TYPE_ID,
       E_LABOR_TYPE_CODE.MISC_TXT_1 AS LABOR_TYPE_CODE_ABBREV,
       E_EMP_PERF_SMRY.WHSE,
       LABOR_ACTIVITY.NAME AS ACTIVITY_NAME,
       VIEW_EVNT_SMRY_HDR.JOB_FUNCTION_NAME AS JOB_FUNCTION_NAME,
       VIEW_EVNT_SMRY_HDR.SPVSR_LOGIN_USER_ID,
       VIEW_EVNT_SMRY_HDR.Dept_code,
       VIEW_EVNT_SMRY_HDR.Tran_Nbr,
       VIEW_EVNT_SMRY_HDR.Ref_Code,
       VIEW_EVNT_SMRY_HDR.Ref_Nbr,
       E_EVNT_STAT_CODE.DESCRIPTION,
       VIEW_EVNT_SMRY_HDR.EMP_PERF_ALLOW,
       VIEW_EVNT_SMRY_HDR.SCHED_ADJ_AMT,
       VIEW_EVNT_SMRY_HDR.ACTL_TIME,
       FN_PFD_TIME(VIEW_EVNT_SMRY_HDR.TRAN_NBR,E_EMP_PERF_SMRY.WHSE) AS PFD_SAM,
       VIEW_EVNT_SMRY_HDR.EVENT_TYPE,
       VIEW_EVNT_SMRY_HDR.EVNT_STAT_CODE,
       E_EMP_PERF_SMRY.REPROCESS_STATUS
  FROM    E_LABOR_TYPE_CODE
        INNER JOIN ( ( ( E_EMP_PERF_SMRY
                LEFT OUTER JOIN VIEW_EVNT_SMRY_HDR
                ON( (E_EMP_PERF_SMRY.LOGIN_USER_ID=VIEW_EVNT_SMRY_HDR.LOGIN_USER_ID)
                    AND (E_EMP_PERF_SMRY.WHSE =VIEW_EVNT_SMRY_HDR.WHSE ))
                INNER JOIN UCL_USER
                ON      E_EMP_PERF_SMRY.LOGIN_USER_ID = UCL_USER.USER_NAME )
                LEFT OUTER JOIN (LABOR_ACTIVITY
                        INNER JOIN E_ACT E_ACT
                        ON      LABOR_ACTIVITY.LABOR_ACTIVITY_ID=E_ACT.LABOR_ACTIVITY_ID)
                ON      VIEW_EVNT_SMRY_HDR.ACT_ID   = E_ACT.ACT_ID )
                INNER JOIN E_EVNT_STAT_CODE
                ON (VIEW_EVNT_SMRY_HDR.EVNT_STAT_CODE = E_EVNT_STAT_CODE.EVNT_STAT_CODE ) )
        ON      E_LABOR_TYPE_CODE.LABOR_TYPE_ID    =VIEW_EVNT_SMRY_HDR.LABOR_TYPE_ID
  WHERE  (E_EMP_PERF_SMRY.EMP_PERF_SMRY_ID= VIEW_EVNT_SMRY_HDR.EMP_PERF_SMRY_ID OR  (VIEW_EVNT_SMRY_HDR.EMP_PERF_SMRY_ID IS NULL AND VIEW_EVNT_SMRY_HDR.ACTUAL_END_DATE IS NULL))
     AND VIEW_EVNT_SMRY_HDR.SCHED_START_DATE >= E_EMP_PERF_SMRY.CLOCK_IN_DATE
     AND (VIEW_EVNT_SMRY_HDR.SCHED_START_DATE <= E_EMP_PERF_SMRY.CLOCK_OUT_DATE OR E_EMP_PERF_SMRY.CLOCK_OUT_DATE IS NULL)
     AND (VIEW_EVNT_SMRY_HDR.ACTUAL_END_DATE <= E_EMP_PERF_SMRY.CLOCK_OUT_DATE  OR VIEW_EVNT_SMRY_HDR.ACTUAL_END_DATE IS NULL OR E_EMP_PERF_SMRY.CLOCK_OUT_DATE IS NULL)
     AND E_EMP_PERF_SMRY.WHSE = :1
     AND E_EMP_PERF_SMRY.CLOCK_IN_DATE >= :2
     AND (E_EMP_PERF_SMRY.CLOCK_IN_DATE <= :3)
     AND VIEW_EVNT_SMRY_HDR.LOGIN_USER_ID IN ('''
         || PEMP_LOGIN_LIST
         || ''')'
         || ORDER_BY_CLAUSE;
BEGIN
   -- Truncate table EMP_TRACKING_REPORT_TABLE_COG before the Stored Procedure execution
   execute immediate 'truncate table EMP_TRACKING_REPORT_TABLE_COG';

   OPEN EMP_TR_CURSOR FOR QUERY_STR1 USING PWHSE, PDATE_RANGE1, PDATE_RANGE2;
       PREV_ACTUAL_END_DATE := NULL;
       PREV_CLOCK_IN_DATE := NULL;
       PREV_CLOCK_OUT_DATE := NULL;
       PREV_LOGIN_USER_ID := NULL;
       PREV_USER_FIRST_NAME := NULL;
       PREV_USER_LAST_NAME := NULL;

       FETCH EMP_TR_CURSOR
       BULK COLLECT INTO EMP_TR_ROW_TAB_INST;
   IF EMP_TR_ROW_TAB_INST.COUNT > 0
   THEN
      EMP_TR_ROW_TAB_INSERT_BUFFER.EXTEND (EMP_TR_ROW_TAB_INST.LAST);
      TEMP_INT := EMP_TR_ROW_TAB_INST.LAST;

      DBMS_OUTPUT.PUT_LINE (
         'Extending by total records :' || EMP_TR_ROW_TAB_INST.LAST);

      FOR i IN 1 .. EMP_TR_ROW_TAB_INST.LAST
      LOOP
         CURR_SCHED_START_DATE := EMP_TR_ROW_TAB_INST (i).SCHED_START_DATE;
         CURR_CLOCK_IN_DATE := EMP_TR_ROW_TAB_INST (i).CLOCK_IN_DATE;

         IF (CURR_CLOCK_IN_DATE <> PREV_CLOCK_IN_DATE
             OR EMP_TR_ROW_TAB_INST (i).LOGIN_USER_ID <> PREV_LOGIN_USER_ID)
         THEN
            IF (PREV_ACTUAL_END_DATE <> PREV_CLOCK_OUT_DATE)
            THEN
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPORTDATE := SYSDATE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPORTID := PREPORT_ID;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPORT_ROW_TYPE := 'Event Gap';
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).LOGIN_USER_ID :=
                  PREV_LOGIN_USER_ID;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).USER_LAST_NAME :=
                  PREV_USER_LAST_NAME;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).USER_FIRST_NAME :=
                  PREV_USER_FIRST_NAME;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).CLOCK_IN_DATE :=
                  PREV_CLOCK_IN_DATE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).CLOCK_OUT_DATE :=
                  PREV_CLOCK_OUT_DATE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).PREV_ACTUAL_END_DATE := NULL;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).SCHED_START_DATE :=
                  PREV_ACTUAL_END_DATE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).ACTUAL_END_DATE :=
                  PREV_CLOCK_OUT_DATE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).LABOR_TYPE_ID := NULL;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).LABOR_TYPE_CODE_ABBREV := NULL;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).WHSE :=
                  EMP_TR_ROW_TAB_INST (i).WHSE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).ACTIVITY_NAME := 'Event Gap';
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).JOB_FUNCTION_NAME :=
                  'Event Gap';
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).SPVSR_LOGIN_USER_ID := NULL;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).DEPT_CODE := 'Event Gap';
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).TRAN_NBR := NULL;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).REF_CODE := NULL;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).REF_NBR := NULL;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).DESCRIPTION := 'Event Gap';
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).EMP_PERF_ALLOW := NULL;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).SCHED_ADJ_AMT := NULL;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).ACTL_TIME := NULL;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).PFD_SAM := NULL;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).EVENT_TYPE := NULL;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).EVNT_STAT_CODE := NULL;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPROCESS_STATUS := NULL;
               k := k + 1;
               EMP_TR_ROW_TAB_INSERT_BUFFER.EXTEND (1);
            END IF;

            PREV_ACTUAL_END_DATE := CURR_CLOCK_IN_DATE;
            PREV_CLOCK_IN_DATE := NULL;
            PREV_LOGIN_USER_ID := NULL;
            PREV_USER_FIRST_NAME := NULL;
            PREV_USER_LAST_NAME := NULL;
         END IF;

         IF (EMP_TR_ROW_TAB_INST (i).ACTUAL_END_DATE IS NULL)
         THEN
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPORTDATE := SYSDATE;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPORTID := PREPORT_ID;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPORT_ROW_TYPE :=
               'Incomplete Event';
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).LOGIN_USER_ID :=
               EMP_TR_ROW_TAB_INST (i).LOGIN_USER_ID;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).USER_LAST_NAME :=
               EMP_TR_ROW_TAB_INST (i).USER_LAST_NAME;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).USER_FIRST_NAME :=
               EMP_TR_ROW_TAB_INST (i).USER_FIRST_NAME;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).CLOCK_IN_DATE :=
               EMP_TR_ROW_TAB_INST (i).CLOCK_IN_DATE;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).CLOCK_OUT_DATE :=
               EMP_TR_ROW_TAB_INST (i).CLOCK_OUT_DATE;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).PREV_ACTUAL_END_DATE :=
               PREV_ACTUAL_END_DATE;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).SCHED_START_DATE :=
               EMP_TR_ROW_TAB_INST (i).SCHED_START_DATE;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).ACTUAL_END_DATE :=
               EMP_TR_ROW_TAB_INST (i).ACTUAL_END_DATE;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).LABOR_TYPE_ID :=
               EMP_TR_ROW_TAB_INST (i).LABOR_TYPE_ID;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).LABOR_TYPE_CODE_ABBREV :=
               EMP_TR_ROW_TAB_INST (i).LABOR_TYPE_CODE_ABBREV;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).WHSE :=
               EMP_TR_ROW_TAB_INST (i).WHSE;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).ACTIVITY_NAME :=
               EMP_TR_ROW_TAB_INST (i).ACTIVITY_NAME;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).JOB_FUNCTION_NAME :=
               EMP_TR_ROW_TAB_INST (i).JOB_FUNCTION_NAME;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).SPVSR_LOGIN_USER_ID :=
               EMP_TR_ROW_TAB_INST (i).SPVSR_LOGIN_USER_ID;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).DEPT_CODE :=
               EMP_TR_ROW_TAB_INST (i).DEPT_CODE;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).TRAN_NBR :=
               EMP_TR_ROW_TAB_INST (i).TRAN_NBR;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).REF_CODE :=
               EMP_TR_ROW_TAB_INST (i).REF_CODE;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).REF_NBR :=
               EMP_TR_ROW_TAB_INST (i).REF_NBR;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).DESCRIPTION :=
               EMP_TR_ROW_TAB_INST (i).DESCRIPTION;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).EMP_PERF_ALLOW :=
               EMP_TR_ROW_TAB_INST (i).EMP_PERF_ALLOW;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).SCHED_ADJ_AMT :=
               EMP_TR_ROW_TAB_INST (i).SCHED_ADJ_AMT;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).ACTL_TIME :=
               EMP_TR_ROW_TAB_INST (i).ACTL_TIME;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).PFD_SAM :=
               EMP_TR_ROW_TAB_INST (i).PFD_SAM;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).EVENT_TYPE :=
               EMP_TR_ROW_TAB_INST (i).EVENT_TYPE;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).EVNT_STAT_CODE :=
               EMP_TR_ROW_TAB_INST (i).EVNT_STAT_CODE;
            EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPROCESS_STATUS :=
               EMP_TR_ROW_TAB_INST (i).REPROCESS_STATUS;
            k := k + 1;
         ELSE
            IF (PREV_CLOCK_IN_DATE IS NULL
                OR CURR_SCHED_START_DATE >= PREV_ACTUAL_END_DATE)
            THEN
               IF (PREV_ACTUAL_END_DATE IS NULL)
               THEN
                  PREV_ACTUAL_END_DATE := CURR_CLOCK_IN_DATE;
               END IF;

               IF (CURR_SCHED_START_DATE > PREV_ACTUAL_END_DATE)
               THEN
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPORTDATE := SYSDATE;
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPORTID := PREPORT_ID;
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPORT_ROW_TYPE :=
                     'Event Gap';
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).LOGIN_USER_ID :=
                     EMP_TR_ROW_TAB_INST (i).LOGIN_USER_ID;
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).USER_LAST_NAME :=
                     EMP_TR_ROW_TAB_INST (i).USER_LAST_NAME;
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).USER_FIRST_NAME :=
                     EMP_TR_ROW_TAB_INST (i).USER_FIRST_NAME;
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).CLOCK_IN_DATE :=
                     EMP_TR_ROW_TAB_INST (i).CLOCK_IN_DATE;
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).CLOCK_OUT_DATE :=
                     EMP_TR_ROW_TAB_INST (i).CLOCK_OUT_DATE;
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).PREV_ACTUAL_END_DATE :=
                     NULL;
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).SCHED_START_DATE :=
                     PREV_ACTUAL_END_DATE;
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).ACTUAL_END_DATE :=
                     EMP_TR_ROW_TAB_INST (i).SCHED_START_DATE;
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).LABOR_TYPE_ID := NULL;
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).LABOR_TYPE_CODE_ABBREV :=
                     EMP_TR_ROW_TAB_INST (i).LABOR_TYPE_CODE_ABBREV;
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).WHSE :=
                     EMP_TR_ROW_TAB_INST (i).WHSE;
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).ACTIVITY_NAME :=
                     'Event Gap';
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).JOB_FUNCTION_NAME :=
                     'Event Gap';
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).SPVSR_LOGIN_USER_ID :=
                     EMP_TR_ROW_TAB_INST (i).SPVSR_LOGIN_USER_ID;
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).DEPT_CODE := 'Event Gap';
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).TRAN_NBR :=
                     EMP_TR_ROW_TAB_INST (i).TRAN_NBR;
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).REF_CODE :=
                     EMP_TR_ROW_TAB_INST (i).REF_CODE;
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).REF_NBR :=
                     EMP_TR_ROW_TAB_INST (i).REF_NBR;
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).DESCRIPTION := 'Event Gap';
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).EMP_PERF_ALLOW :=
                     EMP_TR_ROW_TAB_INST (i).EMP_PERF_ALLOW;
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).SCHED_ADJ_AMT :=
                     EMP_TR_ROW_TAB_INST (i).SCHED_ADJ_AMT;
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).ACTL_TIME :=
                     EMP_TR_ROW_TAB_INST (i).ACTL_TIME;
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).PFD_SAM :=
                     EMP_TR_ROW_TAB_INST (i).PFD_SAM;
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).EVENT_TYPE :=
                     EMP_TR_ROW_TAB_INST (i).EVENT_TYPE;
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).EVNT_STAT_CODE :=
                     EMP_TR_ROW_TAB_INST (i).EVNT_STAT_CODE;
                  EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPROCESS_STATUS :=
                     EMP_TR_ROW_TAB_INST (i).REPROCESS_STATUS;
                  k := k + 1;
                  EMP_TR_ROW_TAB_INSERT_BUFFER.EXTEND (1);
               END IF;

               EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPORTDATE := SYSDATE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPORTID := PREPORT_ID;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPORT_ROW_TYPE := 'Normal Row Type';
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).LOGIN_USER_ID := EMP_TR_ROW_TAB_INST (i).LOGIN_USER_ID;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).USER_LAST_NAME := EMP_TR_ROW_TAB_INST (i).USER_LAST_NAME;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).USER_FIRST_NAME := EMP_TR_ROW_TAB_INST (i).USER_FIRST_NAME;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).CLOCK_IN_DATE := EMP_TR_ROW_TAB_INST (i).CLOCK_IN_DATE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).CLOCK_OUT_DATE := EMP_TR_ROW_TAB_INST (i).CLOCK_OUT_DATE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).PREV_ACTUAL_END_DATE := NULL;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).SCHED_START_DATE := EMP_TR_ROW_TAB_INST (i).SCHED_START_DATE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).ACTUAL_END_DATE := EMP_TR_ROW_TAB_INST (i).ACTUAL_END_DATE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).LABOR_TYPE_ID := EMP_TR_ROW_TAB_INST (i).LABOR_TYPE_ID;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).LABOR_TYPE_CODE_ABBREV := EMP_TR_ROW_TAB_INST (i).LABOR_TYPE_CODE_ABBREV;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).WHSE := EMP_TR_ROW_TAB_INST (i).WHSE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).ACTIVITY_NAME := EMP_TR_ROW_TAB_INST (i).ACTIVITY_NAME;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).JOB_FUNCTION_NAME := EMP_TR_ROW_TAB_INST (i).JOB_FUNCTION_NAME;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).SPVSR_LOGIN_USER_ID := EMP_TR_ROW_TAB_INST (i).SPVSR_LOGIN_USER_ID;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).DEPT_CODE := EMP_TR_ROW_TAB_INST (i).DEPT_CODE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).TRAN_NBR := EMP_TR_ROW_TAB_INST (i).TRAN_NBR;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).REF_CODE := EMP_TR_ROW_TAB_INST (i).REF_CODE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).REF_NBR := EMP_TR_ROW_TAB_INST (i).REF_NBR;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).DESCRIPTION := EMP_TR_ROW_TAB_INST (i).DESCRIPTION;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).EMP_PERF_ALLOW := EMP_TR_ROW_TAB_INST (i).EMP_PERF_ALLOW;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).SCHED_ADJ_AMT := EMP_TR_ROW_TAB_INST (i).SCHED_ADJ_AMT;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).ACTL_TIME := EMP_TR_ROW_TAB_INST (i).ACTL_TIME;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).PFD_SAM := EMP_TR_ROW_TAB_INST (i).PFD_SAM;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).EVENT_TYPE := EMP_TR_ROW_TAB_INST (i).EVENT_TYPE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).EVNT_STAT_CODE := EMP_TR_ROW_TAB_INST (i).EVNT_STAT_CODE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPROCESS_STATUS := EMP_TR_ROW_TAB_INST (i).REPROCESS_STATUS;

               PREV_ACTUAL_END_DATE := EMP_TR_ROW_TAB_INST (i).ACTUAL_END_DATE;
               k := k + 1;
            ELSE
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPORTDATE := SYSDATE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPORTID := PREPORT_ID;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPORT_ROW_TYPE :=
                  'Event Overlapp';
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).LOGIN_USER_ID :=
                  EMP_TR_ROW_TAB_INST (i).LOGIN_USER_ID;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).USER_LAST_NAME :=
                  EMP_TR_ROW_TAB_INST (i).USER_LAST_NAME;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).USER_FIRST_NAME :=
                  EMP_TR_ROW_TAB_INST (i).USER_FIRST_NAME;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).CLOCK_IN_DATE :=
                  EMP_TR_ROW_TAB_INST (i).CLOCK_IN_DATE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).CLOCK_OUT_DATE :=
                  EMP_TR_ROW_TAB_INST (i).CLOCK_OUT_DATE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).PREV_ACTUAL_END_DATE :=
                  PREV_ACTUAL_END_DATE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).SCHED_START_DATE :=
                  EMP_TR_ROW_TAB_INST (i).SCHED_START_DATE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).ACTUAL_END_DATE :=
                  EMP_TR_ROW_TAB_INST (i).ACTUAL_END_DATE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).LABOR_TYPE_ID :=
                  EMP_TR_ROW_TAB_INST (i).LABOR_TYPE_ID;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).LABOR_TYPE_CODE_ABBREV :=
                  EMP_TR_ROW_TAB_INST (i).LABOR_TYPE_CODE_ABBREV;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).WHSE :=
                  EMP_TR_ROW_TAB_INST (i).WHSE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).ACTIVITY_NAME :=
                  EMP_TR_ROW_TAB_INST (i).ACTIVITY_NAME;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).JOB_FUNCTION_NAME :=
                  EMP_TR_ROW_TAB_INST (i).JOB_FUNCTION_NAME;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).SPVSR_LOGIN_USER_ID :=
                  EMP_TR_ROW_TAB_INST (i).SPVSR_LOGIN_USER_ID;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).DEPT_CODE :=
                  EMP_TR_ROW_TAB_INST (i).DEPT_CODE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).TRAN_NBR :=
                  EMP_TR_ROW_TAB_INST (i).TRAN_NBR;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).REF_CODE :=
                  EMP_TR_ROW_TAB_INST (i).REF_CODE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).REF_NBR :=
                  EMP_TR_ROW_TAB_INST (i).REF_NBR;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).DESCRIPTION :=
                  EMP_TR_ROW_TAB_INST (i).DESCRIPTION;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).EMP_PERF_ALLOW :=
                  EMP_TR_ROW_TAB_INST (i).EMP_PERF_ALLOW;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).SCHED_ADJ_AMT :=
                  EMP_TR_ROW_TAB_INST (i).SCHED_ADJ_AMT;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).ACTL_TIME :=
                  EMP_TR_ROW_TAB_INST (i).ACTL_TIME;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).PFD_SAM :=
                  EMP_TR_ROW_TAB_INST (i).PFD_SAM;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).EVENT_TYPE :=
                  EMP_TR_ROW_TAB_INST (i).EVENT_TYPE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).EVNT_STAT_CODE :=
                  EMP_TR_ROW_TAB_INST (i).EVNT_STAT_CODE;
               EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPROCESS_STATUS :=
                  EMP_TR_ROW_TAB_INST (i).REPROCESS_STATUS;
               k := k + 1;

               IF (EMP_TR_ROW_TAB_INST (i).ACTUAL_END_DATE >
                      PREV_ACTUAL_END_DATE)
               THEN
                  PREV_ACTUAL_END_DATE :=
                     EMP_TR_ROW_TAB_INST (i).ACTUAL_END_DATE;
               END IF;
            END IF;
         END IF;

         PREV_CLOCK_IN_DATE := EMP_TR_ROW_TAB_INST (i).CLOCK_IN_DATE;
         PREV_CLOCK_OUT_DATE := EMP_TR_ROW_TAB_INST (i).CLOCK_OUT_DATE;
         PREV_LOGIN_USER_ID := EMP_TR_ROW_TAB_INST (i).LOGIN_USER_ID;
         PREV_USER_FIRST_NAME := EMP_TR_ROW_TAB_INST (i).USER_FIRST_NAME;
         PREV_USER_LAST_NAME := EMP_TR_ROW_TAB_INST (i).USER_LAST_NAME;

         NUM_RECORDS := NUM_RECORDS + 1;
      END LOOP;
   END IF;

   IF (PREV_ACTUAL_END_DATE <> PREV_CLOCK_OUT_DATE)
   THEN
      EMP_TR_ROW_TAB_INSERT_BUFFER.EXTEND (1);
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPORTDATE := SYSDATE;
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPORTID := PREPORT_ID;
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPORT_ROW_TYPE := 'Event Gap';
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).LOGIN_USER_ID := PREV_LOGIN_USER_ID;
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).USER_LAST_NAME := PREV_USER_LAST_NAME;
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).USER_FIRST_NAME := PREV_USER_FIRST_NAME;
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).CLOCK_IN_DATE := PREV_CLOCK_IN_DATE;
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).CLOCK_OUT_DATE := PREV_CLOCK_OUT_DATE;
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).PREV_ACTUAL_END_DATE := NULL;
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).SCHED_START_DATE := PREV_ACTUAL_END_DATE;
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).ACTUAL_END_DATE := PREV_CLOCK_OUT_DATE;
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).LABOR_TYPE_ID := NULL;
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).LABOR_TYPE_CODE_ABBREV := NULL;
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).WHSE := PWHSE;
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).ACTIVITY_NAME := 'Event Gap';
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).JOB_FUNCTION_NAME := 'Event Gap';
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).SPVSR_LOGIN_USER_ID := NULL;
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).DEPT_CODE := 'Event Gap';
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).TRAN_NBR := NULL;
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).REF_CODE := NULL;
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).REF_NBR := NULL;
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).DESCRIPTION := 'Event Gap';
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).EMP_PERF_ALLOW := NULL;
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).SCHED_ADJ_AMT := NULL;
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).ACTL_TIME := NULL;
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).PFD_SAM := NULL;
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).EVENT_TYPE := NULL;
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).EVNT_STAT_CODE := NULL;
      EMP_TR_ROW_TAB_INSERT_BUFFER (k).REPROCESS_STATUS := NULL;
      k := k + 1;
   END IF;

   CLOSE EMP_TR_CURSOR;

   DBMS_OUTPUT.PUT_LINE ('Inserting records :' || NUM_RECORDS || ' rows ');

   FORALL j IN 1 .. EMP_TR_ROW_TAB_INSERT_BUFFER.COUNT
      INSERT INTO EMP_TRACKING_REPORT_TABLE_COG (REPORTDATE,
                                             REPORTID,
                                             REPORT_ROW_TYPE,
                                             LOGIN_USER_ID,
                                             USER_LAST_NAME,
                                             USER_FIRST_NAME,
                                             CLOCK_IN_DATE,
                                             CLOCK_OUT_DATE,
                                             PREV_ACTUAL_END_DATE,
                                             SCHED_START_DATE,
                                             ACTUAL_END_DATE,
                                             LABOR_TYPE_ID,
                                             LABOR_TYPE_CODE_ABBREV,
                                             WHSE,
                                             ACTIVITY_NAME,
                                             JOB_FUNCTION_NAME,
                                             SPVSR_LOGIN_USER_ID,
                                             DEPT_CODE,
                                             TRAN_NBR,
                                             REF_CODE,
                                             REF_NBR,
                                             DESCRIPTION,
                                             EMP_PERF_ALLOW,
                                             SCHED_ADJ_AMT,
                                             ACTL_TIME,
                                             PFD_SAM,
                                             EVENT_TYPE,
                                             EVNT_STAT_CODE,
                                             REPROCESS_STATUS)
           VALUES (EMP_TR_ROW_TAB_INSERT_BUFFER (j).REPORTDATE,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).REPORTID,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).REPORT_ROW_TYPE,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).LOGIN_USER_ID,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).USER_LAST_NAME,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).USER_FIRST_NAME,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).CLOCK_IN_DATE,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).CLOCK_OUT_DATE,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).PREV_ACTUAL_END_DATE,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).SCHED_START_DATE,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).ACTUAL_END_DATE,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).LABOR_TYPE_ID,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).LABOR_TYPE_CODE_ABBREV,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).WHSE,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).ACTIVITY_NAME,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).JOB_FUNCTION_NAME,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).SPVSR_LOGIN_USER_ID,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).DEPT_CODE,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).TRAN_NBR,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).REF_CODE,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).REF_NBR,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).DESCRIPTION,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).EMP_PERF_ALLOW,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).SCHED_ADJ_AMT,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).ACTL_TIME,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).PFD_SAM,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).EVENT_TYPE,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).EVNT_STAT_CODE,
                   EMP_TR_ROW_TAB_INSERT_BUFFER (j).REPROCESS_STATUS);

   DBMS_OUTPUT.PUT_LINE ('Inserted Total :' || NUM_RECORDS || ' rows ');
   COMMIT;
   DBMS_OUTPUT.PUT_LINE ('NUM_RECORDS :' || NUM_RECORDS);
   OPEN C_RES_CUR FOR SELECT * FROM EMP_TRACKING_REPORT_TABLE_COG;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Error :- ' || SQLERRM);
      TEMP_STR := 'Error :- ' || SQLERRM;
      NUM_LOG :=
         FN_LM_LOG_ERROR (PWHSE,
                          PREPORT_ID,
                          'EmpTrackReport',
                          'Pr err' || TEMP_STR);

      NUM_LOG :=
         FN_LM_LOG_ERROR (
            PWHSE,
            PREPORT_ID,
            'EmpTrackReport',
               'Pr parm:'
            || TO_CHAR (PDATE_RANGE1, 'YYYY-MM-DD HH24:MI:SS')
            || ' : '
            || TO_CHAR (PDATE_RANGE2, 'YYYY-MM-DD HH24:MI:SS'));

      NUM_LOG :=
         FN_LM_LOG_ERROR (PWHSE,
                          PREPORT_ID,
                          'EmpTrackReport',
                          QUERY_STR1);
      COMMIT;

END;
/
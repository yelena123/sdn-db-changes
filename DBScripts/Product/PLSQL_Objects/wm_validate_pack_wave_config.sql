create or replace procedure wm_validate_pack_wave_config
(
    p_ship_wave_parm_id     in ship_wave_parm.ship_wave_parm_id%type,
    p_incl_actv_flag        out pack_wave_parm_hdr.incl_actv_flag%type,
    p_incl_resv_flag        out pack_wave_parm_hdr.incl_resv_flag%type,
    p_incl_case_pick_flag   out pack_wave_parm_hdr.incl_case_pick_flag%type,
    p_incl_mxd_flag         out pack_wave_parm_hdr.inc_mxd_flag%type,
    p_pack_wave_parm_id     out pack_wave_parm_hdr.pack_wave_parm_id%type,
    p_max_pack_wave         out pack_wave_parm_hdr.max_pack_wave%type,
    p_ship_wave_nbr         out ship_wave_parm.ship_wave_nbr%type,
    p_pick_wave_nbr         out ship_wave_parm.pick_wave_nbr%type,
    p_wave_parm_id          out wave_parm.wave_parm_id%type,
    p_chute_util            out number,
    p_force_cat_flag        out pack_wave_parm_hdr.force_cat_flag%type,
    p_chute_assign_type     out pack_wave_parm_hdr.chute_assign_type%type,
    p_chk_critcl_dim_flag   out pack_wave_parm_hdr.chk_critcl_dim_flag%type,
    p_bal_alg               out pack_wave_parm_hdr.bal_alg%type,
    p_cat_event_id          out wave_parm.chute_assign_type_event_id%type,
    p_start_lvl             out pack_wave_parm_hdr.start_lvl%type
)
as
begin
    -- get pack wave relevant parms; if max pack waves = 0, treat as unlimited;
    select coalesce(pwph.incl_actv_flag, 'N'), swp.pick_wave_nbr,
        coalesce(nullif(pwph.max_pack_wave, 0), 999) max_pack_wave, 
        pwph.pack_wave_parm_id, wp.wave_parm_id, swp.ship_wave_nbr, 
        pwph.bal_alg, coalesce(pwph.incl_resv_flag, 'N'),
        pwph.chute_assign_type, coalesce(pwph.incl_case_pick_flag, 'N'), 
        coalesce(pwph.force_cat_flag, 'N'),
        coalesce(pwph.inc_mxd_flag, 'N'), 
        coalesce(nullif(pwph.chute_util, 0), 100)/100,
        coalesce(pwph.chk_critcl_dim_flag, 'N'), wp.chute_assign_type_event_id,
        coalesce(pwph.start_lvl, '0')
    into p_incl_actv_flag, p_pick_wave_nbr, p_max_pack_wave,
        p_pack_wave_parm_id, p_wave_parm_id, p_ship_wave_nbr, p_bal_alg,
        p_incl_resv_flag, p_chute_assign_type, p_incl_case_pick_flag, 
        p_force_cat_flag, p_incl_mxd_flag, p_chute_util,
        p_chk_critcl_dim_flag, p_cat_event_id, p_start_lvl
    from ship_wave_parm swp
    join wave_parm wp on wp.wave_parm_id = swp.wave_parm_id 
    join pack_wave_parm_hdr pwph 
        on pwph.pack_wave_parm_id = wp.pack_wave_parm_id
    where swp.ship_wave_parm_id = p_ship_wave_parm_id;

    -- specs account for 1/0; ui/db has Y/N; for now allow both
    if (p_incl_actv_flag not in ('Y', '1') and p_incl_resv_flag not in ('Y', '1')
        and p_incl_case_pick_flag not in ('Y', '1') 
        and p_incl_mxd_flag not in ('Y', '1'))
    then
        raise_application_error(-20050, 'No include inventory flags enabled');
    end if;
end;
/

show errors ;
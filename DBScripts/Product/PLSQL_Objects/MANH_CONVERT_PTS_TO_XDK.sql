CREATE OR REPLACE PROCEDURE MANH_CONVERT_PTS_TO_XDK (
   p_tc_lpn_id             IN     LPN.TC_LPN_ID%TYPE,
   p_tc_company_id         IN     DISPOSITION_TYPE.TC_COMPANY_ID%TYPE,
   p_login_user_id         IN     VARCHAR2,
   p_disposition_type      IN     DISPOSITION_TYPE.DISPOSITION_TYPE%TYPE,
   p_next_up_number_ilpn   IN      varchar2,
   p_next_up_number_olpn   IN      varchar2,
   p_reuse_ilpn_as_pallet  IN      NUMBER,
   p_debug_flag            IN     NUMBER,
   p_whse                  IN     DISPOSITION_TYPE.WHSE%TYPE,
   p_exception_disp_type   in     varchar2
)
IS
   stmt                          VARCHAR2 (1000);
   v_error_details               VARCHAR2 (2000);
   v_lpn_count                   NUMBER;
   v_remaining_qty               NUMBER;
   v_allocated_qty               NUMBER;
   v_inbd_lpn_as_outbd_lpn         WHSE_PARAMETERS.USE_INBD_LPN_AS_OUTBD_LPN%TYPE;
   v_ilpn_id                     LPN.LPN_ID%TYPE;
   v_olpn_id                     LPN.LPN_ID%TYPE;
   v_lpn_detail_id               LPN_DETAIL.LPN_DETAIL_ID%TYPE;
   v_olpn_detail_id              LPN_DETAIL.LPN_DETAIL_ID%TYPE;
   v_tc_olpn_id                  LPN.TC_LPN_ID%TYPE;
   v_item_id                     LPN_DETAIL.ITEM_ID%TYPE;
   v_tc_order_id                 ORDERS.TC_ORDER_ID%TYPE;
   v_order_id                    ORDERS.ORDER_ID%TYPE;
   v_line_item_id                ORDER_LINE_ITEM.LINE_ITEM_ID%TYPE;
   lpns_created                  NUMBER;
   v_handle_unalloc_lpns         DISPOSITION_TYPE.HANDLING_UNALLOC_LPN%TYPE;
   v_invn_need_type              NUMBER;
   v_wm_inventory_qty            NUMBER;
   v_reference_lpn_id            LPN.TC_REFERENCE_LPN_ID%TYPE;
   v_lpn_facility_status         LPN.LPN_FACILITY_STATUS%TYPE;
   v_pallet_olpn_id              LPN.LPN_ID%TYPE;
   v_pfx_field                   nxt_up_cnt.pfx_field%TYPE;
   v_start_tc_lpn_id             NUMBER (10) := 0;
   v_nxt_up_nbr_len              NUMBER (10) := 0;
   v_total_unalloc_qty           ALLOC_INVN_DTL.QTY_ALLOC%TYPE;
   v_tc_asn_id                   ASN.TC_ASN_ID%TYPE;
   v_total_size_value            LPN_DETAIL.SIZE_VALUE%TYPE;
   v_agg_pakd_qty                ORDER_LINE_ITEM.UNITS_PAKD%TYPE;
   v_packed_qty                  ORDER_LINE_ITEM.UNITS_PAKD%TYPE;
   v_lpn_to_be_pkd               LPN_DETAIL.SIZE_VALUE%TYPE;   
   v_facility_id                 facility.facility_id%type;
   c_nxt_up_poll_freq            number(9) := 10;
   v_nxt_up_curr_nbr             number(10) := 0;
   ------------variables for original orders cursor---------------
   v_agg_order_id                ORDER_LINE_ITEM.ORDER_ID%TYPE;
   v_agg_line_item_id            ORDER_LINE_ITEM.LINE_ITEM_ID%TYPE;

   ------------variables for allocation cursor---------------
   v_lpn_rec_sec_ITEM_ID         LPN_DETAIL.ITEM_ID%TYPE;
   v_lpn_rec_sec_SKU_ATTR_1      ALLOC_INVN_DTL.SKU_ATTR_1%TYPE;
   v_lpn_rec_sec_SKU_ATTR_2      ALLOC_INVN_DTL.SKU_ATTR_2%TYPE;
   v_lpn_rec_sec_SKU_ATTR_3      ALLOC_INVN_DTL.SKU_ATTR_3%TYPE;
   v_lpn_rec_sec_SKU_ATTR_4      ALLOC_INVN_DTL.SKU_ATTR_4%TYPE;
   v_lpn_rec_sec_SKU_ATTR_5      ALLOC_INVN_DTL.SKU_ATTR_5%TYPE;
   v_lpn_rec_sec_INVN_TYPE       ALLOC_INVN_DTL.INVN_TYPE%TYPE;
   v_lpn_rec_sec_BATCH_NBR       ALLOC_INVN_DTL.BATCH_NBR%TYPE;
   v_lpn_rec_sec_PROD_STAT       ALLOC_INVN_DTL.PROD_STAT%TYPE;
   v_lpn_rec_sec_CNTRY_OF_ORGN   ALLOC_INVN_DTL.CNTRY_OF_ORGN%TYPE;
   ------------variables for allocation cursor---------------

   var_do_status                 ORDERS.DO_STATUS%TYPE;
   var_do_dtl_status             ORDER_LINE_ITEM.DO_DTL_STATUS%TYPE;
   var_order_id                  ORDER_LINE_ITEM.ORDER_ID%TYPE;
   var_line_item_id              ORDER_LINE_ITEM.LINE_ITEM_ID%TYPE;
   var_order_qty                 ORDER_LINE_ITEM.ORDER_QTY%TYPE;
   var_units_pakd                ORDER_LINE_ITEM.UNITS_PAKD%TYPE;
   var_o_facility_id             ORDERS.O_FACILITY_ID%TYPE;
   var_o_facility_alias_id         ORDERS.O_FACILITY_ALIAS_ID%TYPE;
   var_d_facility_id             ORDERS.D_FACILITY_ID%TYPE;
   var_d_facility_alias_id         ORDERS.D_FACILITY_ALIAS_ID%TYPE;
   var_lpn_label_type             ORDERS.LPN_LABEL_TYPE%TYPE;
  -------variables for updating resv_locn_hdr------------------------------ 
  v_curr_sub_locn_id            LPN.CURR_SUB_LOCN_ID%TYPE;  
  v_locn_hdr_id                 LOCN_HDR.LOCN_HDR_ID%TYPE; 
  v_outbd_stg_locn_id           DOCK_DOOR.OUTBD_STAGING_LOCN_ID%TYPE;
  v_inbd_stag_locn_class        LOCN_HDR.LOCN_CLASS%TYPE;
  v_outbd_stag_locn_class        locn_hdr.locn_class%type;
   v_prev_module_name            wm_utils.t_app_context_data;
   v_prev_action_name            wm_utils.t_app_context_data;	
  
   --CURSOR FOR LOADING THE LPN RECORDS CREATED FOR INT 60 WHICH WAS BROKEN
   CURSOR CURSOR_LPN_DETAIL
   IS
      SELECT /*+ PARALLEL */
             ---------------------------------LPN RECORDS----------------------------------------
             L.TC_LPN_ID AS TC_LPN_ID,
             L.LPN_ID AS LPN_ID,
             L.TC_COMPANY_ID,
             L.LPN_MONETARY_VALUE,
             L.C_FACILITY_ID,
             L.O_FACILITY_ID,
             L.O_FACILITY_ALIAS_ID,
             L.MANIFEST_NBR,
             L.SHIP_VIA,
             L.MASTER_BOL_NBR,
             L.BOL_NBR,
             L.INIT_SHIP_VIA,
             L.PATH_ID,
             L.REPRINT_COUNT,
             L.FREIGHT_CHARGE,
             L.LENGTH,
             L.WIDTH,
             L.HEIGHT,
             L.TC_SHIPMENT_ID,
             L.TOTAL_LPN_QTY,
             L.ESTIMATED_VOLUME,
             L.TC_ASN_ID,
             L.WEIGHT,
             L.ACTUAL_VOLUME,
             L.LPN_LABEL_TYPE,
             L.HIBERNATE_VERSION,
             L.CREATED_SOURCE,
             L.C_FACILITY_ALIAS_ID,
             L.PICK_SUB_LOCN_ID,
             L.INTERNAL_ORDER_ID,
             L.TRAILER_STOP_SEQ_NBR,
             L.WAVE_NBR,
             L.WAVE_SEQ_NBR,
             L.WAVE_STAT_CODE,
             L.STAGE_INDICATOR,
             L.PICK_DELIVERY_DURATION,
             L.LPN_BREAK_ATTR,
             L.SEQ_RULE_PRIORITY,
             L.NBR_OF_ZONES,
             L.PALLET_X_OF_Y,
             L.LPN_NBR_X_OF_Y,
             L.LOAD_SEQUENCE,
             L.SELECTION_RULE_ID,
             L.SINGLE_LINE_LPN,
             L.LPN_FACILITY_STAT_UPDATED_DTTM,
             L.ESTIMATED_WEIGHT,
             L.D_FACILITY_ID,
             L.D_FACILITY_ALIAS_ID,
             L.CUBE_UOM,
             L.WEIGHT_UOM_ID_BASE,
             L.VOLUME_UOM_ID_BASE,
             L.DISPOSITION_TYPE,
             L.VOCO_INTRN_REVERSE_ID,
             L.CONTAINER_TYPE,
             L.PLANNED_TC_ASN_ID,
             L.PHYSICAL_ENTITY_CODE,
             L.PROCESS_IMMEDIATE_NEEDS,
             L.RCVD_DTTM,
             L.MANUFACTURED_DTTM,
             L.SHIP_BY_DATE,
             L.EXPIRATION_DATE,
             L.LPN_CREATION_CODE,
             L.PACKER_USERID,
             L.CONSUMPTION_PRIORITY_DTTM,
             L.CURR_SUB_LOCN_ID,
             ---------------------------------LPN_DETAIL RECORDS----------------------------------
             LD.LPN_DETAIL_ID AS LPN_DETAIL_ID,
             LD.ITEM_ID AS ITEM_ID,
             LD.BUSINESS_PARTNER_ID,
             LD.GTIN,
             LD.INCUBATION_DATE,
             LD.SELL_BY_DTTM,
             LD.CNTRY_OF_ORGN,
             LD.INVENTORY_TYPE,
             LD.PRODUCT_STATUS,
             LD.ITEM_ATTR_1,
             LD.ITEM_ATTR_2,
             LD.ITEM_ATTR_3,
             LD.ITEM_ATTR_4,
             LD.ITEM_ATTR_5,
             LD.ITEM_NAME,
             LD.ASN_DTL_ID,
             LD.ASSORT_NBR,
             LD.CUT_NBR,
             LD.PURCHASE_ORDERS_ID,
             LD.TC_PURCHASE_ORDERS_ID,
             LD.PURCHASE_ORDERS_LINE_ID,
             LD.TC_PURCHASE_ORDERS_LINE_ID,
             LD.INSTRTN_CODE_1,
             LD.INSTRTN_CODE_2,
             LD.INSTRTN_CODE_3,
             LD.INSTRTN_CODE_4,
             LD.INSTRTN_CODE_5,
             LD.VENDOR_ITEM_NBR,
             LD.MANUFACTURED_PLANT,
             LD.BATCH_NBR,
             LD.ASSIGNED_QTY,
             LD.PREPACK_GROUP_CODE,
             LD.PACK_CODE,
             LD.STD_PACK_QTY,
             LD.STD_SUB_PACK_QTY,
             LD.STD_BUNDLE_QTY,
             LD.VOLUME_UOM_ID,
             LD.WEIGHT_UOM_ID,
             LD.QTY_UOM_ID,
             LD.SIZE_VALUE,
             LD.QTY_CONV_FACTOR,
             LD.QTY_UOM_ID_BASE,
             LD.LPN_DETAIL_STATUS,
             LD.RECEIVED_QTY,
             WM_INVENTORY_ID,
             WM.TRANSITIONAL_INVENTORY_TYPE,
             WM.LAST_UPDATED_SOURCE_TYPE,
             WM.TO_BE_FILLED_QTY,
             WM.WM_VERSION_ID,
             WM.CREATED_SOURCE_TYPE,
             WM.TO_BE_CONSOLIDATED_QTY,
             WM.LOCATION_DTL_ID,
             WM.PACK_QTY,
             WM.SUB_PACK_QTY,
             WM.LOCN_CLASS,
             WM.ON_HAND_QTY,
             WM.ALLOCATABLE
        FROM LPN_DETAIL LD
             INNER JOIN LPN L
                ON     LD.LPN_ID = L.LPN_ID
                   AND L.TC_LPN_ID = p_tc_lpn_id
                   AND LD.TC_COMPANY_ID = p_tc_company_id
                   and l.c_facility_id = v_facility_id
             INNER JOIN WM_INVENTORY WM
                ON     WM.LPN_ID = LD.LPN_ID
                   AND WM.LPN_DETAIL_ID = LD.LPN_DETAIL_ID
                   AND WM.TC_COMPANY_ID = LD.TC_COMPANY_ID
                   AND WM.MARKED_FOR_DELETE = 'N';

   --CURSOR FOR LOADING THE CHILD LPNS CREATED BY THE BREAK SP
   CURSOR CURSOR_CHILD_LPNS
   IS
        SELECT /*+ PARALLEL */
               ---------------------------------LPN RECORDS----------------------------------------
               L.TC_LPN_ID AS TC_LPN_ID,
               L.LPN_ID AS LPN_ID,
               L.TC_COMPANY_ID,
               L.LPN_MONETARY_VALUE,
               L.C_FACILITY_ID,
               L.O_FACILITY_ID,
               L.O_FACILITY_ALIAS_ID,
               L.MANIFEST_NBR,
               L.SHIP_VIA,
               L.MASTER_BOL_NBR,
               L.BOL_NBR,
               L.INIT_SHIP_VIA,
               L.PATH_ID,
               L.REPRINT_COUNT,
               L.FREIGHT_CHARGE,
               L.LENGTH,
               L.WIDTH,
               L.HEIGHT,
               L.TC_SHIPMENT_ID,
               L.TOTAL_LPN_QTY,
               L.ESTIMATED_VOLUME,
               L.TC_ASN_ID,
               L.WEIGHT,
               L.ACTUAL_VOLUME,
               L.LPN_LABEL_TYPE,
               L.HIBERNATE_VERSION,
               L.CREATED_SOURCE,
               L.C_FACILITY_ALIAS_ID,
               L.PICK_SUB_LOCN_ID,
               L.INTERNAL_ORDER_ID,
               L.TRAILER_STOP_SEQ_NBR,
               L.WAVE_NBR,
               L.WAVE_SEQ_NBR,
               L.WAVE_STAT_CODE,
               L.STAGE_INDICATOR,
               L.PICK_DELIVERY_DURATION,
               L.LPN_BREAK_ATTR,
               L.SEQ_RULE_PRIORITY,
               L.NBR_OF_ZONES,
               L.PALLET_X_OF_Y,
               L.LPN_NBR_X_OF_Y,
               L.LOAD_SEQUENCE,
               L.SELECTION_RULE_ID,
               L.SINGLE_LINE_LPN,
               L.LPN_FACILITY_STAT_UPDATED_DTTM,
               L.ESTIMATED_WEIGHT,
               L.D_FACILITY_ID,
               L.D_FACILITY_ALIAS_ID,
               L.CUBE_UOM,
               L.WEIGHT_UOM_ID_BASE,
               L.VOLUME_UOM_ID_BASE,
               L.DISPOSITION_TYPE,
               L.VOCO_INTRN_REVERSE_ID,
               L.CONTAINER_TYPE,
               L.PLANNED_TC_ASN_ID,
               L.PHYSICAL_ENTITY_CODE,
               L.PROCESS_IMMEDIATE_NEEDS,
               L.RCVD_DTTM,
               L.MANUFACTURED_DTTM,
               L.SHIP_BY_DATE,
               L.EXPIRATION_DATE,
               L.LPN_CREATION_CODE,
               L.PACKER_USERID,
               L.CONSUMPTION_PRIORITY_DTTM,
               ---------------------------------LPN_DETAIL RECORDS----------------------------------
               LD.LPN_DETAIL_ID AS LPN_DETAIL_ID,
               LD.ITEM_ID AS ITEM_ID,
               LD.BUSINESS_PARTNER_ID,
               LD.GTIN,
               LD.INCUBATION_DATE,
               LD.SELL_BY_DTTM,
               LD.CNTRY_OF_ORGN,
               LD.INVENTORY_TYPE,
               LD.PRODUCT_STATUS,
               LD.ITEM_ATTR_1,
               LD.ITEM_ATTR_2,
               LD.ITEM_ATTR_3,
               LD.ITEM_ATTR_4,
               LD.ITEM_ATTR_5,
               LD.ITEM_NAME,
               LD.ASN_DTL_ID,
               LD.ASSORT_NBR,
               LD.CUT_NBR,
               LD.PURCHASE_ORDERS_ID,
               LD.TC_PURCHASE_ORDERS_ID,
               LD.PURCHASE_ORDERS_LINE_ID,
               LD.TC_PURCHASE_ORDERS_LINE_ID,
               LD.INSTRTN_CODE_1,
               LD.INSTRTN_CODE_2,
               LD.INSTRTN_CODE_3,
               LD.INSTRTN_CODE_4,
               LD.INSTRTN_CODE_5,
               LD.VENDOR_ITEM_NBR,
               LD.MANUFACTURED_PLANT,
               LD.BATCH_NBR,
               LD.ASSIGNED_QTY,
               LD.PREPACK_GROUP_CODE,
               LD.PACK_CODE,
               LD.STD_PACK_QTY,
               LD.STD_SUB_PACK_QTY,
               LD.STD_BUNDLE_QTY,
               LD.VOLUME_UOM_ID,
               LD.WEIGHT_UOM_ID,
               LD.QTY_UOM_ID,
               LD.SIZE_VALUE,
               LD.QTY_CONV_FACTOR,
               LD.QTY_UOM_ID_BASE,
               LD.LPN_DETAIL_STATUS,
               LD.INTERNAL_ORDER_DTL_ID,
               LD.TC_ORDER_LINE_ID,
               WM.WM_INVENTORY_ID
          FROM LPN_DETAIL LD
               INNER JOIN LPN L
                  ON     LD.LPN_ID = L.LPN_ID
                     AND L.TC_REFERENCE_LPN_ID = v_reference_lpn_id
                     AND INBOUND_OUTBOUND_INDICATOR = 'I'
                     AND L.LPN_FACILITY_STATUS < 95
                     AND L.TC_COMPANY_ID = p_tc_company_id
                     and l.c_facility_id = v_facility_id
               INNER JOIN WM_INVENTORY WM
                  ON     WM.LPN_ID = LD.LPN_ID
                     AND WM.LPN_DETAIL_ID = LD.LPN_DETAIL_ID
                     AND WM.ITEM_ID = v_lpn_rec_sec_ITEM_ID
                     AND WM.TC_COMPANY_ID = LD.TC_COMPANY_ID
                     AND WM.MARKED_FOR_DELETE = 'N'             
      ORDER BY L.LPN_ID;

   --CUROSR TO LOAD THE ALLOCATIONS ASSOCIATED WITH THE INT 60 LPN CREATED FROM C++
   CURSOR CURSOR_ALLOCATIONS
   IS
      SELECT                                                   /*+ PARALLEL */
            AID.ALLOC_INVN_DTL_ID,
             AID.ITEM_ID,
             AID.TC_ORDER_ID,
             AID.CNTR_NBR,
             AID.INVN_NEED_TYPE,
             AID.QTY_ALLOC,
             AID.QTY_PULLD,
             AID.ALLOC_INVN_CODE,
             AID.TASK_PRTY,
             AID.TASK_TYPE,
             AID.TASK_BATCH,
             AID.TASK_CMPL_REF_CODE,
             AID.TASK_CMPL_REF_NBR,
             AID.TASK_GENRTN_REF_CODE,
             AID.TASK_GENRTN_REF_NBR,
             AID.DEST_LOCN_ID,
             AID.BATCH_NBR,
             AID.TRANS_INVN_TYPE,
             AID.PULL_LOCN_ID,
             AID.FULL_CNTR_ALLOCD,
             AID.NEED_ID,
             AID.REQD_BATCH_NBR,
             AID.PIKR_NBR,
             AID.PULL_LOCN_SEQ_NBR,
             AID.DEST_LOCN_SEQ_NBR,
             PD.PKT_DTL_ID,
             PD.PICK_LOCN_ID,
             PD.PKT_CTRL_NBR,
             PD.PKT_SEQ_NBR,
             PD.REFERENCE_ORDER_ID AS ORDER_ID,
             PD.REFERENCE_LINE_ITEM_ID AS LINE_ITEM_ID
        FROM ALLOC_INVN_DTL AID, PKT_DTL PD
       WHERE     AID.WHSE = p_whse
             AND AID.CNTR_NBR = p_tc_lpn_id
             AND AID.INVN_NEED_TYPE = 60
             AND AID.STAT_CODE < 90
             AND AID.ITEM_ID = v_lpn_rec_sec_ITEM_ID
             AND AID.cd_master_id = p_tc_company_id
             AND COALESCE (AID.SKU_ATTR_1, '*') =
                    COALESCE (v_lpn_rec_sec_SKU_ATTR_1, '*')
             AND COALESCE (AID.SKU_ATTR_2, '*') =
                    COALESCE (v_lpn_rec_sec_SKU_ATTR_2, '*')
             AND COALESCE (AID.SKU_ATTR_3, '*') =
                    COALESCE (v_lpn_rec_sec_SKU_ATTR_3, '*')
             AND COALESCE (AID.SKU_ATTR_4, '*') =
                    COALESCE (v_lpn_rec_sec_SKU_ATTR_4, '*')
             AND COALESCE (AID.SKU_ATTR_5, '*') =
                    COALESCE (v_lpn_rec_sec_SKU_ATTR_5, '*')
             AND COALESCE (AID.INVN_TYPE, '*') =
                    COALESCE (v_lpn_rec_sec_INVN_TYPE, '*')
             AND COALESCE (AID.BATCH_NBR, '*') =
                    COALESCE (v_lpn_rec_sec_BATCH_NBR, '*')
             AND COALESCE (AID.PROD_STAT, '*') =
                    COALESCE (v_lpn_rec_sec_PROD_STAT, '*')
             AND COALESCE (AID.CNTRY_OF_ORGN, '*') =
                    COALESCE (v_lpn_rec_sec_CNTRY_OF_ORGN, '*')
             AND AID.PKT_CTRL_NBR = PD.PKT_CTRL_NBR
             AND AID.PKT_SEQ_NBR = PD.PKT_SEQ_NBR;


   CURSOR CURSOR_ORIGINAL_ORDERS
   IS
        SELECT OLI.ORDER_ID,OLI.LINE_ITEM_ID,OLI.ORDER_QTY,OLI.UNITS_PAKD,OLI.DO_DTL_STATUS
          FROM ORDER_LINE_ITEM OLI
         WHERE     OLI.REFERENCE_ORDER_ID = v_agg_order_id
               AND OLI.REFERENCE_LINE_ITEM_ID = v_agg_line_item_id
               AND OLI.DO_DTL_STATUS IN (120, 130, 140)
      ORDER BY OLI.PRIORITY,
               OLI.ORDER_QTY,
               OLI.ORDER_ID,
               OLI.LINE_ITEM_ID ASC;
BEGIN
   wm_log (
         'PTS-->XDK Conversion : TC_LPN_ID: '
      || p_tc_lpn_id
      || ' ::  '
      || 'pts to xdk started...',
      1,
      'INVMGMT');

    wm_utils.get_context_info(v_prev_module_name, v_prev_action_name);
    wm_utils.set_context_info(p_module_name => 'THD', p_action_name => 'CONVERT_PTS_TO_XDK',
        p_client_id => p_tc_lpn_id);	 

    select f.facility_id
    into v_facility_id
    from facility f
    where f.whse = p_whse;
    
   SELECT LPN_ID, LPN_FACILITY_STATUS, TC_ASN_ID,CURR_SUB_LOCN_ID
     INTO v_ilpn_id, v_lpn_facility_status, v_tc_asn_id, v_curr_sub_locn_id
     FROM LPN
    WHERE     TC_LPN_ID = p_tc_lpn_id
          AND INBOUND_OUTBOUND_INDICATOR = 'I'
          AND TC_COMPANY_ID = p_tc_company_id
          and c_facility_id = v_facility_id;
          
    --Get the outbound_staging_location if the iLPN is received to staging
    IF v_curr_sub_locn_id IS NOT NULL
    THEN
    
      SELECT LOCN_HDR_ID,LOCN_CLASS
      INTO v_locn_hdr_id, v_inbd_stag_locn_class
      FROM LOCN_HDR
      WHERE LOCN_ID = v_curr_sub_locn_id
      AND WHSE = p_whse;      
      
      SELECT OUTBD_STAGING_LOCN_ID
      INTO v_outbd_stg_locn_id
      FROM DOCK_DOOR
      WHERE LOCN_HDR_ID = v_locn_hdr_id
      AND TC_COMPANY_ID = p_tc_company_id
      AND ROWNUM < 2;
      
        IF v_outbd_stg_locn_id IS NOT NULL
        THEN
        
            SELECT LOCN_CLASS
            INTO v_outbd_stag_locn_class
            FROM LOCN_HDR
            WHERE LOCN_ID = v_outbd_stg_locn_id
            AND WHSE = p_whse;
        
        END IF;
    
    END IF;      
    
   --INVOKE CURSOR TO BREAK THE LPNS
   wm_log (
         'PTS-->XDK Conversion : TC_LPN_ID: '
      || p_tc_lpn_id
      || ' ::  '
      || 'invoking break ilpn sp',
      1,
      'INVMGMT');
      
   manh_break_ilpns (p_tc_lpn_id, p_tc_company_id, p_login_user_id, p_disposition_type,
      coalesce(p_next_up_number_ilpn, 'P05'), p_debug_flag, p_whse, v_facility_id);
   wm_log (
         'PTS-->XDK Conversion : TC_LPN_ID: '
      || p_tc_lpn_id
      || ' ::  '
      || 'invoked break ilpn sp',
      1,
      'INVMGMT');
   v_invn_need_type := 2;

   --load the configuration
   SELECT HANDLING_UNALLOC_LPN
     INTO v_handle_unalloc_lpns
     FROM DISPOSITION_TYPE
    WHERE     DISPOSITION_TYPE = p_disposition_type
          AND WHSE = p_whse
          AND TC_COMPANY_ID = p_tc_company_id;

   wm_log (
         'PTS-->XDK Conversion : TC_LPN_ID: '
      || p_tc_lpn_id
      || ' ::  '
      || 'HANDLING_UNALLOC_LPN-->'
      || v_handle_unalloc_lpns,
      1,
      'INVMGMT');

   --START OF LOOP FOR INT 60 LPNS
   FOR lpn_detail_rec IN CURSOR_LPN_DETAIL
   LOOP
      v_item_id := lpn_detail_rec.ITEM_ID;
      v_wm_inventory_qty := lpn_detail_rec.ON_HAND_QTY;
      v_reference_lpn_id := lpn_detail_rec.tc_lpn_id;            
      wm_log (
            'PTS-->XDK Conversion : TC_LPN_ID: '
         || p_tc_lpn_id
         || ' ::  '
         || 'Looping lpn_detail_id:'
         || lpn_detail_rec.LPN_DETAIL_ID,
         1,
         'INVMGMT');

      --Setting the parameters for allocation cursor query
      v_lpn_rec_sec_ITEM_ID := lpn_detail_rec.ITEM_ID;
      v_lpn_rec_sec_SKU_ATTR_1 := lpn_detail_rec.ITEM_ATTR_1;
      v_lpn_rec_sec_SKU_ATTR_2 := lpn_detail_rec.ITEM_ATTR_2;
      v_lpn_rec_sec_SKU_ATTR_3 := lpn_detail_rec.ITEM_ATTR_3;
      v_lpn_rec_sec_SKU_ATTR_4 := lpn_detail_rec.ITEM_ATTR_4;
      v_lpn_rec_sec_SKU_ATTR_5 := lpn_detail_rec.ITEM_ATTR_5;
      v_lpn_rec_sec_INVN_TYPE := lpn_detail_rec.INVENTORY_TYPE;
      v_lpn_rec_sec_BATCH_NBR := lpn_detail_rec.BATCH_NBR;
      v_lpn_rec_sec_PROD_STAT := lpn_detail_rec.PRODUCT_STATUS;
      v_lpn_rec_sec_CNTRY_OF_ORGN := lpn_detail_rec.CNTRY_OF_ORGN;
      
      wm_log (
            'PTS-->XDK Conversion : TC_LPN_ID: '
         || p_tc_lpn_id
         || ' ::  '
         || 'parameters for allocation query :AID.WHSE = '
         || p_whse
         || ' and AID.CNTR_NBR = '
         || p_tc_lpn_id
         || 'AND AID.INVN_NEED_TYPE = '
         || 60
         || ' AND AID.cd_master_id = '
         || p_tc_company_id,
         1,
         'INVMGMT');
      wm_log (
            'PTS-->XDK Conversion : TC_LPN_ID: '
         || p_tc_lpn_id
         || ' ::  '
         || v_lpn_rec_sec_ITEM_ID
         || '--'
         || v_lpn_rec_sec_SKU_ATTR_1
         || '--'
         || v_lpn_rec_sec_SKU_ATTR_2
         || '--'
         || v_lpn_rec_sec_SKU_ATTR_3
         || '--'
         || v_lpn_rec_sec_SKU_ATTR_4
         || '--'
         || v_lpn_rec_sec_SKU_ATTR_5
         || '--INVENTORY_TYPE'
         || v_lpn_rec_sec_INVN_TYPE
         || '--BATCH_NBR'
         || v_lpn_rec_sec_BATCH_NBR
         || '--'
         || v_lpn_rec_sec_PROD_STAT
         || '--'
         || v_lpn_rec_sec_CNTRY_OF_ORGN,
         1,
         'INVMGMT');

      --START OF LOOP FOR ALLOCATIONS
      FOR alloc_rec IN CURSOR_ALLOCATIONS
      LOOP
         v_allocated_qty := alloc_rec.QTY_ALLOC;
         v_tc_order_id := alloc_rec.TC_ORDER_ID;
         --get the aggregate order and aggregate line item id
         v_order_id := alloc_rec.ORDER_ID;
         v_line_item_id := alloc_rec.LINE_ITEM_ID;
         wm_log (
               'PTS-->XDK Conversion : TC_LPN_ID: '
            || p_tc_lpn_id
            || ' ::  '
            || 'Current Allocation for lpn_detail_id:'
            || lpn_detail_rec.LPN_DETAIL_ID
            || 'is'
            || alloc_rec.alloc_invn_dtl_id,
            1,
            'INVMGMT');
         wm_log (
               'PTS-->XDK Conversion : TC_LPN_ID: '
            || p_tc_lpn_id
            || ' ::  '
            || 'Allocation Qty is: for allocation: '
            || alloc_rec.alloc_invn_dtl_id
            || ' is '
            || v_allocated_qty
            || ' ',
            1,
            'INVMGMT');
          

         -- START OF LOOP FOR CHILD LPNS ASSOCIATED WITH THE INTO 60 LPN_DETAIL BASED ON CNTR_NBR AND ITEM
         FOR child_lpn_detail_rec IN CURSOR_CHILD_LPNS
         LOOP
            EXIT WHEN v_allocated_qty = 0;            
            wm_log (
                  'PTS-->XDK Conversion : TC_LPN_ID: '
               || p_tc_lpn_id
               || ' ::  '
               || 'current child lpn:'
               || child_lpn_detail_rec.lpn_id,
               1,
               'INVMGMT');

            IF v_allocated_qty > 0
            THEN
            SELECT O.DO_STATUS,
                      OLI.DO_DTL_STATUS,
                      OLI.ORDER_ID,
                      OLI.LINE_ITEM_ID,
                      OLI.ORDER_QTY,
                      OLI.UNITS_PAKD,
                      O.o_facility_id,
                      O.o_facility_alias_id,
                      O.d_facility_id,
                      O.d_facility_alias_id,
                      O.lpn_label_type
                 INTO var_do_status,
                      var_do_dtl_status,
                      var_order_id,
                      var_line_item_id,
                      var_order_qty,
                      var_units_pakd,
                      var_o_facility_id,
                      var_o_facility_alias_id,
                      var_d_facility_id,
                      var_d_facility_alias_id,
                      var_lpn_label_type
                 FROM ORDER_LINE_ITEM OLI, ORDERS O
                WHERE     OLI.ORDER_ID = alloc_rec.order_id
                      AND OLI.LINE_ITEM_ID = alloc_rec.line_item_id
                      AND OLI.ORDER_ID = O.ORDER_ID;
                      
               SELECT CASE
                         WHEN EXISTS
                                 (SELECT '1'
                                    FROM WHSE_PARAMETERS WP, WHSE_MASTER W
                                   WHERE     W.WHSE = p_whse
                                         AND WP.WHSE_MASTER_ID = W.WHSE_MASTER_ID
                                         AND (WP.USE_INBD_LPN_AS_OUTBD_LPN = '1' OR WP.USE_INBD_LPN_AS_OUTBD_LPN = 'Y'))
                         THEN
                            '1'
                         ELSE
                            '0'
                      END
                 INTO v_inbd_lpn_as_outbd_lpn
                 FROM DUAL;                

               SELECT lpn_id_seq.NEXTVAL INTO v_olpn_id FROM DUAL;
               
               IF (v_inbd_lpn_as_outbd_lpn = '1') 
                THEN
                    v_tc_olpn_id := child_lpn_detail_rec.tc_lpn_id;
               else
                  if (v_nxt_up_curr_nbr < v_start_tc_lpn_id + c_nxt_up_poll_freq - 1
                      and v_nxt_up_curr_nbr > 0)
                  then
                      -- within the already reserved range
                      v_nxt_up_curr_nbr := v_nxt_up_curr_nbr + 1;
                  else
                      -- range exceeded; request again
                      wm_gen_lpn_nbrs (p_login_user_id, p_whse, lpn_detail_rec.tc_company_id,
                          var_o_facility_id, coalesce(p_next_up_number_olpn, 'P14'), c_nxt_up_poll_freq,
                          v_pfx_field, v_start_tc_lpn_id, v_nxt_up_nbr_len);
                      v_nxt_up_curr_nbr := v_start_tc_lpn_id;
                  end if;

                   v_tc_olpn_id :=
                      COALESCE (v_pfx_field, '')
                      || LPAD (TO_CHAR (v_nxt_up_curr_nbr), v_nxt_up_nbr_len, '0');
                END IF;
               --CREATE THE OLPN in packed status
               --wm_log('PTS-->XDK Conversion : TC_LPN_ID: '||p_tc_lpn_id||' ::  '||v_olpn_id,1,'INVMGMT');
               INSERT INTO lpn (lpn_id,
                                tc_lpn_id,
                                tc_company_id,
                                lpn_type,
                                lpn_monetary_value,
                                c_facility_id,
                                o_facility_id,
                                o_facility_alias_id,
                                lpn_status,
                                lpn_facility_status,
                                tc_reference_lpn_id,
                                tc_order_id,
                                manifest_nbr,
                                ship_via,
                                master_bol_nbr,
                                bol_nbr,
                                init_ship_via,
                                path_id,
                                reprint_count,
                                freight_charge,
                                LENGTH,
                                width,
                                height,
                                total_lpn_qty,
                                estimated_volume,
                                weight,
                                actual_volume,
                                lpn_label_type,
                                hibernate_version,
                                created_source,
                                created_dttm,
                                c_facility_alias_id,
                                pick_sub_locn_id,
                                inbound_outbound_indicator,
                                internal_order_id,
                                trailer_stop_seq_nbr,
                                wave_nbr,
                                wave_seq_nbr,
                                wave_stat_code,
                                transitional_inventory_type,
                                stage_indicator,
                                pick_delivery_duration,
                                lpn_break_attr,
                                seq_rule_priority,
                                nbr_of_zones,
                                pallet_x_of_y,
                                lpn_nbr_x_of_y,
                                load_sequence,
                                selection_rule_id,
                                single_line_lpn,
                                item_id,
                                lpn_facility_stat_updated_dttm,
                                order_id,
                                estimated_weight,
                                d_facility_id,
                                d_facility_alias_id,
                                lpn_creation_code,
                                cube_uom,
                                qty_uom_id_base,
                                weight_uom_id_base,
                                volume_uom_id_base,
                                business_partner_id,
                                PACKER_USERID,
                                VOCO_INTRN_REVERSE_ID, 
                                CONTAINER_TYPE,
                                PLANNED_TC_ASN_ID, 
                                PHYSICAL_ENTITY_CODE, 
                                PROCESS_IMMEDIATE_NEEDS, 
                                ITEM_NAME,
                                RCVD_DTTM,
                                CONSUMPTION_PRIORITY_DTTM,
                                DISPOSITION_TYPE
                                )
                  SELECT v_olpn_id,
                         v_tc_olpn_id,
                         lpn_detail_rec.tc_company_id,
                         1,
                         lpn_detail_rec.lpn_monetary_value,
                         child_lpn_detail_rec.c_facility_id,
                         var_o_facility_id,
                         var_o_facility_alias_id,
                         15,
                         20,
                         child_lpn_detail_rec.tc_lpn_id,
                         alloc_rec.tc_order_id,
                         lpn_detail_rec.manifest_nbr,
                         lpn_detail_rec.ship_via,
                         lpn_detail_rec.master_bol_nbr,
                         lpn_detail_rec.bol_nbr,
                         lpn_detail_rec.init_ship_via,
                         lpn_detail_rec.path_id,
                         0,
                         lpn_detail_rec.freight_charge,
                         lpn_detail_rec.LENGTH,
                         lpn_detail_rec.width,
                         lpn_detail_rec.height,
                         child_lpn_detail_rec.size_value,
                         child_lpn_detail_rec.estimated_volume,
                         0,
                         0,
                         var_lpn_label_type,
                         lpn_detail_rec.hibernate_version,
                         lpn_detail_rec.created_source,
                         SYSTIMESTAMP,
                         lpn_detail_rec.c_facility_alias_id,
                         lpn_detail_rec.pick_sub_locn_id,
                         'O',
                         alloc_rec.pkt_ctrl_nbr,
                         lpn_detail_rec.trailer_stop_seq_nbr,
                         lpn_detail_rec.wave_nbr,
                         lpn_detail_rec.wave_seq_nbr,
                         lpn_detail_rec.wave_stat_code,
                         lpn_detail_rec.transitional_inventory_type,
                         child_lpn_detail_rec.stage_indicator,
                         lpn_detail_rec.pick_delivery_duration,
                         lpn_detail_rec.lpn_break_attr,
                         lpn_detail_rec.seq_rule_priority,
                         lpn_detail_rec.nbr_of_zones,
                         lpn_detail_rec.pallet_x_of_y,
                         lpn_detail_rec.lpn_nbr_x_of_y,
                         lpn_detail_rec.load_sequence,
                         lpn_detail_rec.selection_rule_id,
                         lpn_detail_rec.single_line_lpn,
                         lpn_detail_rec.item_id,
                         lpn_detail_rec.lpn_facility_stat_updated_dttm,
                         v_order_id,
                         child_lpn_detail_rec.estimated_weight,
                         var_d_facility_id,
                         var_d_facility_alias_id,
                         77,
                         lpn_detail_rec.cube_uom,
                         child_lpn_detail_rec.qty_uom_id_base,
                         lpn_detail_rec.weight_uom_id_base,
                         lpn_detail_rec.volume_uom_id_base,
                         lpn_detail_rec.business_partner_id,
                         p_login_user_id,
                         child_lpn_detail_rec.VOCO_INTRN_REVERSE_ID, 
                         child_lpn_detail_rec.CONTAINER_TYPE,
                         child_lpn_detail_rec.PLANNED_TC_ASN_ID, 
                         child_lpn_detail_rec.PHYSICAL_ENTITY_CODE, 
                         child_lpn_detail_rec.PROCESS_IMMEDIATE_NEEDS, 
                         child_lpn_detail_rec.ITEM_NAME,
                         child_lpn_detail_rec.RCVD_DTTM,
                         child_lpn_detail_rec.CONSUMPTION_PRIORITY_DTTM,
                         p_disposition_type
                    FROM DUAL;

               --CREATE THE OLPN DETAIL
               SELECT lpn_detail_id_seq.NEXTVAL
                 INTO v_olpn_detail_id
                 FROM DUAL;

               INSERT INTO lpn_detail (tc_company_id,
                                       lpn_id,
                                       lpn_detail_id,
                                       lpn_detail_status,
                                       distribution_order_dtl_id,
                                       shipped_qty,
                                       received_qty,
                                       initial_qty,
                                       business_partner_id,
                                       item_id,
                                       gtin,
                                       std_pack_qty,
                                       std_sub_pack_qty,
                                       std_bundle_qty,
                                       incubation_date,
                                       expiration_date,
                                       ship_by_date,
                                       sell_by_dttm,
                                       consumption_priority_dttm,
                                       manufactured_dttm,
                                       cntry_of_orgn,
                                       inventory_type,
                                       product_status,
                                       item_attr_1,
                                       item_attr_2,
                                       item_attr_3,
                                       item_attr_4,
                                       item_attr_5,
                                       asn_dtl_id,
                                       size_value,
                                       qty_uom_id,
                                       weight_uom_id,
                                       volume_uom_id,
                                       assort_nbr,
                                       cut_nbr,
                                       purchase_orders_id,
                                       tc_purchase_orders_id,
                                       purchase_orders_line_id,
                                       tc_purchase_orders_line_id,
                                       hibernate_version,
                                       internal_order_id,
                                       instrtn_code_1,
                                       instrtn_code_2,
                                       instrtn_code_3,
                                       instrtn_code_4,
                                       instrtn_code_5,
                                       created_source_type,
                                       created_source,
                                       created_dttm,
                                       last_updated_source_type,
                                       last_updated_source,
                                       last_updated_dttm,
                                       vendor_item_nbr,
                                       manufactured_plant,
                                       batch_nbr,
                                       assigned_qty,
                                       prepack_group_code,
                                       pack_code,
                                       ITEM_NAME,
                                       QTY_CONV_FACTOR,
                                       weight_uom_id_base,
                                       volume_uom_id_base,
                                       qty_uom_id_base,
                                       INTERNAL_ORDER_DTL_ID,
                                       TC_ORDER_LINE_ID)
                  SELECT lpn_detail_rec.tc_company_id,
                         v_olpn_id,
                         v_olpn_detail_id,
                         90,
                         v_line_item_id,
                         0,
                         0,
                         child_lpn_detail_rec.size_value AS initial_qty,
                         lpn_detail_rec.business_partner_id,
                         v_item_id,
                         lpn_detail_rec.gtin,
                         lpn_detail_rec.std_pack_qty,
                         lpn_detail_rec.std_sub_pack_qty,
                         lpn_detail_rec.std_bundle_qty,
                         lpn_detail_rec.incubation_date,
                         lpn_detail_rec.expiration_date,
                         lpn_detail_rec.ship_by_date,
                         lpn_detail_rec.sell_by_dttm,
                         lpn_detail_rec.consumption_priority_dttm,
                         lpn_detail_rec.manufactured_dttm,
                         lpn_detail_rec.cntry_of_orgn,
                         lpn_detail_rec.inventory_type,
                         lpn_detail_rec.product_status,
                         child_lpn_detail_rec.item_attr_1,
                         child_lpn_detail_rec.item_attr_2,
                         child_lpn_detail_rec.item_attr_3,
                         child_lpn_detail_rec.item_attr_4,
                         child_lpn_detail_rec.item_attr_5,
                         NULL,
                         child_lpn_detail_rec.size_value AS size_value,
                         child_lpn_detail_rec.qty_uom_id,
                         lpn_detail_rec.weight_uom_id,
                         lpn_detail_rec.volume_uom_id,
                         lpn_detail_rec.assort_nbr,
                         lpn_detail_rec.cut_nbr,
                         lpn_detail_rec.purchase_orders_id,
                         lpn_detail_rec.tc_purchase_orders_id,
                         lpn_detail_rec.purchase_orders_line_id,
                         lpn_detail_rec.tc_purchase_orders_line_id,
                         lpn_detail_rec.hibernate_version,
                         alloc_rec.pkt_ctrl_nbr,
                         lpn_detail_rec.instrtn_code_1,
                         lpn_detail_rec.instrtn_code_2,
                         lpn_detail_rec.instrtn_code_3,
                         lpn_detail_rec.instrtn_code_4,
                         lpn_detail_rec.instrtn_code_5,
                         5,
                         p_login_user_id,
                         SYSTIMESTAMP,
                         5,
                         p_login_user_id,
                         SYSTIMESTAMP,
                         child_lpn_detail_rec.vendor_item_nbr,
                         child_lpn_detail_rec.manufactured_plant,
                         child_lpn_detail_rec.batch_nbr,
                         child_lpn_detail_rec.assigned_qty,
                         child_lpn_detail_rec.prepack_group_code,
                         child_lpn_detail_rec.pack_code,
                         child_lpn_detail_rec.ITEM_NAME,
                         child_lpn_detail_rec.QTY_CONV_FACTOR,
                         child_lpn_detail_rec.weight_uom_id_base,
                         child_lpn_detail_rec.volume_uom_id_base,
                         child_lpn_detail_rec.qty_uom_id_base,
                         alloc_rec.pkt_seq_nbr,
                         v_line_item_id
                    FROM DUAL;

               --CREATE inventory record for the olpn created above

               INSERT INTO WM_INVENTORY (TC_COMPANY_ID,
                                         LOCATION_ID,
                                         TC_LPN_ID,
                                         TRANSITIONAL_INVENTORY_TYPE,
                                         INVENTORY_TYPE,
                                         PRODUCT_STATUS,
                                         CNTRY_OF_ORGN,
                                         ITEM_ATTR_1,
                                         ITEM_ATTR_2,
                                         ITEM_ATTR_3,
                                         ITEM_ATTR_4,
                                         ITEM_ATTR_5,
                                         LOCN_CLASS,
                                         ALLOCATABLE,
                                         CREATED_SOURCE,
                                         CREATED_DTTM,
                                         LAST_UPDATED_SOURCE_TYPE,
                                         LAST_UPDATED_SOURCE,
                                         LAST_UPDATED_DTTM,
                                         INBOUND_OUTBOUND_INDICATOR,
                                         LPN_DETAIL_ID,
                                         C_FACILITY_ID,
                                         BATCH_NBR,
                                         LPN_ID,
                                         WM_INVENTORY_ID,
                                         ON_HAND_QTY,
                                         WM_ALLOCATED_QTY,
                                         WM_VERSION_ID,
                                         CREATED_SOURCE_TYPE,
                                         TO_BE_CONSOLIDATED_QTY,
                                         PACK_QTY,
                                         SUB_PACK_QTY,
                                         ITEM_ID,
                                         MARKED_FOR_DELETE)
                  SELECT child_lpn_detail_rec.TC_COMPANY_ID,
                         NULL,
                         v_tc_olpn_id,
                         lpn_detail_rec.TRANSITIONAL_INVENTORY_TYPE,
                         lpn_detail_rec.INVENTORY_TYPE,
                         lpn_detail_rec.PRODUCT_STATUS,
                         lpn_detail_rec.CNTRY_OF_ORGN,
                         lpn_detail_rec.ITEM_ATTR_1,
                         lpn_detail_rec.ITEM_ATTR_2,
                         lpn_detail_rec.ITEM_ATTR_3,
                         lpn_detail_rec.ITEM_ATTR_4,
                         lpn_detail_rec.ITEM_ATTR_5,
                         CAST (NULL AS CHAR (1)),
                         'Y' AS ALLOCATABLE,
                         p_login_user_id,
                         SYSTIMESTAMP,
                         lpn_detail_rec.LAST_UPDATED_SOURCE_TYPE,
                         p_login_user_id,
                         SYSTIMESTAMP,
                         'O' AS INBOUND_OUTBOUND_INDICATOR,
                         v_olpn_detail_id,
                         child_lpn_detail_rec.C_FACILITY_ID,
                         lpn_detail_rec.BATCH_NBR,
                         v_olpn_id,
                         WM_INVENTORY_ID_SEQ.NEXTVAL,
                         child_lpn_detail_rec.size_value,
                         child_lpn_detail_rec.size_value,
                         lpn_detail_rec.WM_VERSION_ID,
                         lpn_detail_rec.CREATED_SOURCE_TYPE,
                         lpn_detail_rec.TO_BE_CONSOLIDATED_QTY,
                         child_lpn_detail_rec.STD_PACK_QTY,
                         child_lpn_detail_rec.STD_SUB_PACK_QTY,
                         child_lpn_detail_rec.ITEM_ID,
                         'N' AS MARKED_FOR_DELETE
                    FROM DUAL;

               wm_log (
                     'PTS-->XDK Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'created olpn'
                  || v_olpn_id,
                  1,
                  'INVMGMT');

               --create a new allocation for the child lpn in std qty
               INSERT INTO alloc_invn_dtl (alloc_invn_dtl_id,
                                           whse,
                                           item_id,
                                           batch_nbr,
                                           alloc_invn_code,
                                           cntr_nbr,
                                           trans_invn_type,
                                           pull_locn_id,
                                           invn_need_type,
                                           task_type,
                                           task_prty,
                                           task_batch,
                                           alloc_uom,
                                           alloc_uom_qty,
                                           full_cntr_allocd,
                                           orig_reqmt,
                                           qty_alloc,
                                           qty_pulld,
                                           dest_locn_id,
                                           task_genrtn_ref_code,
                                           task_genrtn_ref_nbr,
                                           task_cmpl_ref_code,
                                           task_cmpl_ref_nbr,
                                           need_id,
                                           reqd_batch_nbr,
                                           pkt_ctrl_nbr,
                                           pkt_seq_nbr,
                                           carton_nbr,
                                           carton_seq_nbr,
                                           pikr_nbr,
                                           pull_locn_seq_nbr,
                                           dest_locn_seq_nbr,
                                           stat_code,
                                           create_date_time,
                                           mod_date_time,
                                           user_id,
                                           cd_master_id,
                                           invn_type,
                                           prod_stat,
                                           sku_attr_1,
                                           sku_attr_2,
                                           sku_attr_3,
                                           sku_attr_4,
                                           sku_attr_5,
                                           TC_ORDER_ID,
                                           LINE_ITEM_ID)
                  SELECT ALLOC_INVN_DTL_ID_SEQ.NEXTVAL,
                         p_whse,
                         v_item_id,
                         alloc_rec.batch_nbr,
                         0,
                         child_lpn_detail_rec.tc_lpn_id,
                         alloc_rec.trans_invn_type,
                         alloc_rec.pull_locn_id,
                         v_invn_need_type,
                         alloc_rec.TASK_TYPE,
                         0,
                         alloc_rec.task_batch,
                         'C',
                         1,
                         alloc_rec.full_cntr_allocd,
                         child_lpn_detail_rec.size_value,
                         child_lpn_detail_rec.size_value,
                         child_lpn_detail_rec.size_value,
                         NULL,
                         NULL,
                         NULL,
                         6,
                         v_tc_olpn_id,
                         alloc_rec.need_id,
                         alloc_rec.reqd_batch_nbr,
                         alloc_rec.pkt_ctrl_nbr,
                         alloc_rec.pkt_seq_nbr,
                         v_tc_olpn_id,
                         v_olpn_detail_id,
                         alloc_rec.pikr_nbr,
                         alloc_rec.pull_locn_seq_nbr,
                         alloc_rec.dest_locn_seq_nbr,
                         90,
                         SYSTIMESTAMP,
                         SYSTIMESTAMP,
                         p_login_user_id,
                         lpn_detail_rec.tc_company_id,
                         lpn_detail_rec.inventory_type,
                         lpn_detail_rec.product_status,
                         lpn_detail_rec.item_attr_1,
                         lpn_detail_rec.item_attr_2,
                         lpn_detail_rec.item_attr_3,
                         lpn_detail_rec.item_attr_4,
                         lpn_detail_rec.item_attr_5,
                         v_tc_order_id,
                         v_line_item_id
                    FROM DUAL;

               wm_log (
                     'PTS-->XDK Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'created allocation for olpn'
                  || v_olpn_id,
                  1,
                  'INVMGMT');               

               --update the allocation for the parent int 60 lpn detail
               UPDATE ALLOC_INVN_DTL
                  SET QTY_ALLOC = QTY_ALLOC - child_lpn_detail_rec.SIZE_VALUE,
                      QTY_PULLD = QTY_PULLD - child_lpn_detail_rec.SIZE_VALUE
                WHERE ALLOC_INVN_DTL_ID = alloc_rec.ALLOC_INVN_DTL_ID;

               wm_log (
                     'PTS-->XDK Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'updated the parent allocation record',
                  1,
                  'INVMGMT');

               --update the inventory record for the parent int 60 lpn detail
               UPDATE WM_INVENTORY
                  SET ON_HAND_QTY =
                         ON_HAND_QTY - child_lpn_detail_rec.SIZE_VALUE,
                      WM_ALLOCATED_QTY =
                         WM_ALLOCATED_QTY - child_lpn_detail_rec.SIZE_VALUE,
                      LAST_UPDATED_DTTM = SYSTIMESTAMP,
                      LAST_UPDATED_SOURCE = p_login_user_id
                WHERE WM_INVENTORY_ID = lpn_detail_rec.WM_INVENTORY_ID;
                
        --update the resv_locn_hdr record for the parent int 60 lpn detail if it is in inbound staging location
        
        IF (v_curr_sub_locn_id IS NOT NULL)
        THEN        
          UPDATE RESV_LOCN_HDR
            SET CURR_WT = CURR_WT - child_lpn_detail_rec.WEIGHT,
            CURR_VOL = CURR_VOL - child_lpn_detail_rec.ACTUAL_VOLUME,
            MOD_DATE_TIME = SYSTIMESTAMP                    
            WHERE LOCN_ID = v_curr_sub_locn_id;
        END IF;
        
        --update LPN,WM_INVENTORY and RESV_LOCN_HDR for the cross docked oLPNs 
        --when there is an outbound_staging_locn attached to the receiving dock door
        IF (v_outbd_stg_locn_id IS NOT NULL)
          THEN          
          
              
          UPDATE LPN
          SET CURR_SUB_LOCN_ID = v_outbd_stg_locn_id
          WHERE LPN_ID = v_olpn_id;
                        
              
          UPDATE WM_INVENTORY 
          SET LOCATION_ID = v_outbd_stg_locn_id,
          LOCN_CLASS = v_outbd_stag_locn_class
          WHERE LPN_ID = v_olpn_id;
              
          UPDATE RESV_LOCN_HDR
          SET CURR_WT = CURR_WT +  child_lpn_detail_rec.WEIGHT,
          CURR_VOL = CURR_VOL + child_lpn_detail_rec.ACTUAL_VOLUME,
          CURR_UOM_QTY = CURR_UOM_QTY + 1          
          WHERE LOCN_ID = v_outbd_stg_locn_id;          
              
          END IF;
          
       
        -- Reduce the Qty from the parent int 60 lpn
          UPDATE LPN
          SET WEIGHT = WEIGHT  - child_lpn_detail_rec.WEIGHT,
          ACTUAL_VOLUME = ACTUAL_VOLUME - child_lpn_detail_rec.ACTUAL_VOLUME
           WHERE LPN_ID = v_ilpn_id;
      
               wm_log (
                     'PTS-->XDK Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'updated the parent inventory',
                  1,
                  'INVMGMT');
               ------------------------------------------------------------------------------------------ --------------------------------------------- --------------------------------------------- ---------------------------------------------
               wm_log (
                     'PTS-->XDK Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'UPDATING ORDERS',
                  1,
                  'INVMGMT');
               wm_log (
                     'PTS-->XDK Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'ORDERS DETAIL',
                  1,
                  'INVMGMT');
               wm_log (
                     'PTS-->XDK Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'AGGREGATE ORDERS DETAIL',
                  1,
                  'INVMGMT');               

               wm_log (
                     'PTS-->XDK Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'var_do_status: '
                  || var_do_status
                  || ',var_do_dtl_status: '
                  || var_do_dtl_status
                  || ',var_order_id: '
                  || var_order_id
                  || ',var_line_item_id: '
                  || var_line_item_id
                  || ',var_order_qty: '
                  || var_order_qty
                  || ',var_units_pakd: '
                  || var_units_pakd,
                  1,
                  'INVMGMT');

               FOR v_order_rec IN CURSOR_ORIGINAL_ORDERS
               LOOP
                  wm_log (
                        'PTS-->XDK Conversion : TC_LPN_ID: '
                     || p_tc_lpn_id
                     || ' ::  '
                     || 'Detail of original order line: ORDER_ID:'
                     || v_order_rec.ORDER_ID
                     || ' LINE_ITEM_ID:'
                     || v_order_rec.LINE_ITEM_ID,
                     1,
                     'INVMGMT');
                  wm_log (
                        'PTS-->XDK Conversion : TC_LPN_ID: '
                     || p_tc_lpn_id
                     || ' ::  '
                     || 'OLI.ORDER_ID'
                     || v_order_rec.ORDER_ID
                     || ',
                                                        OLI.LINE_ITEM_ID'
                     || v_order_rec.LINE_ITEM_ID
                     || ',OLI.ORDER_QTY'
                     || v_order_rec.ORDER_QTY
                     || ',OLI.UNITS_PAKD'
                     || v_order_rec.UNITS_PAKD
                     || ',OLI.DO_DTL_STATUS'
                     || v_order_rec.DO_DTL_STATUS,
                     1,
                     'INVMGMT');
               END LOOP;

               --------------------------------------------- --------------------------------------------- --------------------------------------------- --------------------------------------------- ---------------------------------------------
               --update the aggregate order line item for the parent int 60 lpn detail
               UPDATE ORDER_LINE_ITEM OLI
                  SET OLI.LAST_UPDATED_SOURCE = p_login_user_id,
                      OLI.LAST_UPDATED_DTTM = SYSTIMESTAMP,
                      OLI.HIBERNATE_VERSION =
                         (COALESCE (OLI.HIBERNATE_VERSION, 0) + 1),
                      OLI.UNITS_PAKD =
                         ( (COALESCE (OLI.UNITS_PAKD, 0)
                            + child_lpn_detail_rec.SIZE_VALUE)),
                      OLI.DO_DTL_STATUS =
                         (CASE
                             WHEN OLI.ORDER_QTY - OLI.ALLOCATED_QTY <= 0
                             THEN
                                CASE
                                   when oli.order_qty - (coalesce (oli.units_pakd, 0)
                                      + child_lpn_detail_rec.size_value) <= 0
                                      THEN 150
                                   ELSE 140
                                END
                             ELSE
                                120
                          END)
                WHERE OLI.ORDER_ID = alloc_rec.ORDER_ID
                      AND OLI.LINE_ITEM_ID = alloc_rec.LINE_ITEM_ID;

               wm_log (
                     'PTS-->XDK Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'updated aggregate order line '
                  || alloc_rec.LINE_ITEM_ID,
                  1,
                  'INVMGMT');
               ----update the originial orders associated with the aggregate order
               v_lpn_to_be_pkd := child_lpn_detail_rec.SIZE_VALUE;
               v_packed_qty := 0;
               wm_log (
                     'PTS-->XDK Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'updating original orderlines',
                  1,
                  'INVMGMT');

               v_agg_order_id := alloc_rec.ORDER_ID;
               v_agg_line_item_id := alloc_rec.LINE_ITEM_ID;

               FOR v_order_rec IN CURSOR_ORIGINAL_ORDERS
               LOOP
                  wm_log (
                        'PTS-->XDK Conversion : TC_LPN_ID: '
                     || p_tc_lpn_id
                     || ' ::  '
                     || 'updating original order line: ORDER_ID:'
                     || v_order_rec.ORDER_ID
                     || ' LINE_ITEM_ID:'
                     || v_order_rec.LINE_ITEM_ID,
                     1,
                     'INVMGMT');

                  IF (v_order_rec.ORDER_QTY > 0)
                  THEN
                     IF v_lpn_to_be_pkd > v_order_rec.ORDER_QTY
                     THEN
                        v_packed_qty :=
                           v_lpn_to_be_pkd - v_order_rec.ORDER_QTY;
                        v_lpn_to_be_pkd :=
                           v_lpn_to_be_pkd - v_order_rec.ORDER_QTY;
                     ELSIF v_lpn_to_be_pkd = v_order_rec.ORDER_QTY
                     THEN
                        v_packed_qty := v_lpn_to_be_pkd;
                        v_lpn_to_be_pkd := 0;
                     ELSE
                        v_packed_qty := v_lpn_to_be_pkd;
                        v_lpn_to_be_pkd := 0;
                     END IF;

                     wm_log (
                           'PTS-->XDK Conversion : TC_LPN_ID: '
                        || p_tc_lpn_id
                        || ' ::  '
                        || 'updating original orderline'
                        || 'ORDER_ID = '
                        || v_order_rec.ORDER_ID
                        || ' AND LINE_ITEM_ID = '
                        || v_order_rec.LINE_ITEM_ID,
                        1,
                        'INVMGMT');

                     UPDATE ORDER_LINE_ITEM OLI
                        SET OLI.LAST_UPDATED_SOURCE = p_login_user_id,
                            OLI.LAST_UPDATED_DTTM = SYSTIMESTAMP,
                            OLI.HIBERNATE_VERSION =
                               (COALESCE (OLI.HIBERNATE_VERSION, 0) + 1),
                            OLI.UNITS_PAKD =
                               ( (COALESCE (OLI.UNITS_PAKD, 0) + v_packed_qty)),
                            OLI.DO_DTL_STATUS =
                               (CASE
                                   WHEN OLI.ORDER_QTY - OLI.ALLOCATED_QTY <= 0
                                   THEN
                                      case
                                         when oli.order_qty - (coalesce (oli.units_pakd, 0) + v_packed_qty) <= 0
                                         then 150
                                      else 140
                                      END
                                   ELSE
                                      120
                                END)
                      WHERE OLI.ORDER_ID = v_order_rec.ORDER_ID
                            AND OLI.LINE_ITEM_ID = v_order_rec.LINE_ITEM_ID;
                  END IF;

                  EXIT WHEN v_lpn_to_be_pkd = 0;
               END LOOP;



               --update the pickticket for the parent int 60 lpn detail
               UPDATE PKT_DTL PD
                  SET PD.USER_ID = p_login_user_id,
                      PD.MOD_DATE_TIME = SYSTIMESTAMP,
                      PD.WM_VERSION_ID = (COALESCE (PD.WM_VERSION_ID, 0) + 1),
                      PD.UNITS_PAKD =
                         ( (COALESCE (PD.UNITS_PAKD, 0)
                            + child_lpn_detail_rec.SIZE_VALUE)),
                      PD.VERF_AS_PAKD =
                         ( (COALESCE (PD.VERF_AS_PAKD, 0)
                            + child_lpn_detail_rec.SIZE_VALUE)),
                      PD.STAT_CODE = 40
                WHERE PD.PKT_DTL_ID = alloc_rec.PKT_DTL_ID;

               wm_log (
                     'PTS-->XDK Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'updated pickticket_dtl',
                  1,
                  'INVMGMT');

               --Update the parent lpn detail

               UPDATE LPN_DETAIL
                  SET SIZE_VALUE =
                         SIZE_VALUE - child_lpn_detail_rec.SIZE_VALUE,
                      LAST_UPDATED_SOURCE = p_login_user_id,
                      LAST_UPDATED_DTTM = SYSTIMESTAMP,
                      HIBERNATE_VERSION =
                         (COALESCE (HIBERNATE_VERSION, 0) + 1)
                WHERE LPN_DETAIL_ID = lpn_detail_rec.LPN_DETAIL_ID
                      AND LPN_ID = lpn_detail_rec.LPN_ID;

               wm_log (
                     'PTS-->XDK Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'updated parent_lpn id size value',
                  1,
                  'INVMGMT');

               --Update the child lpn and lpn detail to consumed

               UPDATE LPN
                  SET LPN_FACILITY_STATUS = 95,
                      LAST_UPDATED_SOURCE = p_login_user_id,
                      LAST_UPDATED_DTTM = SYSTIMESTAMP,
                      HIBERNATE_VERSION =
                         (COALESCE (HIBERNATE_VERSION, 0) + 1),
                      WEIGHT = 0,
                      ACTUAL_VOLUME = 0
                WHERE LPN_ID = child_lpn_detail_rec.LPN_ID;

               UPDATE LPN_DETAIL
                  SET SIZE_VALUE = 0,
                      LPN_DETAIL_STATUS = 70,
                      LAST_UPDATED_SOURCE = p_login_user_id,
                      LAST_UPDATED_DTTM = SYSTIMESTAMP,
                      HIBERNATE_VERSION =
                         (COALESCE (HIBERNATE_VERSION, 0) + 1)
                WHERE LPN_ID = child_lpn_detail_rec.LPN_ID
                      AND LPN_DETAIL_ID = child_lpn_detail_rec.LPN_DETAIL_ID;                     
              
               wm_log (
                     'PTS-->XDK Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'updated child lpn to consumed',
                  1,
                  'INVMGMT');

               --delete the inventory of the consumed child lpn
               DELETE FROM WM_INVENTORY
                     WHERE WM_INVENTORY_ID =
                              child_lpn_detail_rec.WM_INVENTORY_ID;

               wm_log (
                     'PTS-->XDK Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'deleted the inventory :'
                  || child_lpn_detail_rec.WM_INVENTORY_ID
                  || ' for consumed lpn');
               --DECREMENT THE ALLOCATION AND WM INVENTORY for the parent int 60 lpn detail
               v_allocated_qty :=
                  v_allocated_qty - child_lpn_detail_rec.size_value;
               v_wm_inventory_qty :=
                  v_wm_inventory_qty - child_lpn_detail_rec.size_value;
               wm_log (
                     'PTS-->XDK Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'v_allocated_qty:'
                  || v_allocated_qty,
                  1,
                  'INVMGMT');
               wm_log (
                     'PTS-->XDK Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'v_wm_inventory_qty:'
                  || v_wm_inventory_qty,
                  1,
                  'INVMGMT');
            END IF;
         END LOOP;        --DELETE THE ALLOCATION IF THE ALLOCATED QTY IS ZERO

         IF v_allocated_qty = 0
         THEN
                            DELETE                             /*+ PARALLEL */
                                  FROM  ALLOC_INVN_DTL
                                  WHERE ALLOC_INVN_DTL_ID =
                                           alloc_rec.ALLOC_INVN_DTL_ID;
         END IF;

         --update the aggregate order header
   
      END LOOP;       --end of allocation loop

      --DELETE THE INVENTORY IF THE  ON_HAND_QTY IS ZERO
      IF v_wm_inventory_qty = 0
      THEN
                         DELETE                                /*+ PARALLEL */
                               FROM  WM_INVENTORY
                               WHERE WM_INVENTORY_ID =
                                        lpn_detail_rec.WM_INVENTORY_ID;
      END IF;
   END LOOP;                                    --end of big lpn detail int 60
   
   -------------Palletize logic-----------------------------------
   SELECT SUM (LD.SIZE_VALUE)
     INTO v_total_size_value
     FROM LPN_DETAIL LD, LPN L
    WHERE L.LPN_ID = v_ilpn_id AND LD.LPN_ID = L.LPN_ID;

   IF v_lpn_facility_status = 50 AND v_total_size_value = 0 AND p_reuse_ilpn_as_pallet = 1 
   THEN
      SELECT lpn_id_seq.NEXTVAL INTO v_pallet_olpn_id FROM DUAL;

      INSERT INTO lpn (lpn_id,
                       tc_lpn_id,
                       tc_company_id,
                       lpn_type,
                       lpn_monetary_value,
                       c_facility_id,
                       o_facility_id,
                       o_facility_alias_id,
                       lpn_status,
                       lpn_facility_status,
                       tc_reference_lpn_id,
                       tc_order_id,
                       manifest_nbr,
                       ship_via,
                       master_bol_nbr,
                       bol_nbr,
                       init_ship_via,
                       path_id,
                       reprint_count,
                       freight_charge,
                       LENGTH,
                       width,
                       height,
                       total_lpn_qty,
                       estimated_volume,
                       weight,
                       actual_volume,
                       lpn_label_type,
                       hibernate_version,
                       created_source,
                       created_dttm,
                       c_facility_alias_id,
                       pick_sub_locn_id,
                       inbound_outbound_indicator,
                       internal_order_id,
                       trailer_stop_seq_nbr,
                       wave_nbr,
                       wave_seq_nbr,
                       wave_stat_code,
                       transitional_inventory_type,
                       stage_indicator,
                       pick_delivery_duration,
                       lpn_break_attr,
                       seq_rule_priority,
                       nbr_of_zones,
                       pallet_x_of_y,
                       lpn_nbr_x_of_y,
                       load_sequence,
                       selection_rule_id,
                       single_line_lpn,
                       item_id,
                       lpn_facility_stat_updated_dttm,
                       order_id,
                       estimated_weight,
                       d_facility_id,
                       d_facility_alias_id,
                       lpn_creation_code,
                       cube_uom,
                       qty_uom_id_base,
                       weight_uom_id_base,
                       volume_uom_id_base)
         SELECT v_pallet_olpn_id,
                tc_lpn_id,
                tc_company_id,
                2,
                lpn_monetary_value,
                c_facility_id,
                o_facility_id,
                o_facility_alias_id,
                lpn_status,
                20,
                tc_reference_lpn_id,
                tc_order_id,
                manifest_nbr,
                ship_via,
                master_bol_nbr,
                bol_nbr,
                init_ship_via,
                path_id,
                0,
                freight_charge,
                LENGTH,
                width,
                height,
                total_lpn_qty,
                estimated_volume,
                0,
                0,
                lpn_label_type,
                hibernate_version,
                created_source,
                created_dttm,
                c_facility_alias_id,
                pick_sub_locn_id,
                'O',
                internal_order_id,
                trailer_stop_seq_nbr,
                wave_nbr,
                wave_seq_nbr,
                wave_stat_code,
                transitional_inventory_type,
                stage_indicator,
                pick_delivery_duration,
                lpn_break_attr,
                seq_rule_priority,
                nbr_of_zones,
                pallet_x_of_y,
                lpn_nbr_x_of_y,
                load_sequence,
                selection_rule_id,
                single_line_lpn,
                item_id,
                lpn_facility_stat_updated_dttm,
                order_id,
                estimated_weight,
                d_facility_id,
                d_facility_alias_id,
                77,
                cube_uom,
                qty_uom_id_base,
                weight_uom_id_base,
                volume_uom_id_base
           FROM LPN
          WHERE LPN_ID = v_ilpn_id;

      --update the child  Olpns
      UPDATE LPN
         SET tc_parent_lpn_id = p_tc_lpn_id,
             PARENT_LPN_ID = v_pallet_olpn_id,
             LAST_UPDATED_SOURCE = p_login_user_id,
             LAST_UPDATED_DTTM = SYSTIMESTAMP,
             HIBERNATE_VERSION = (COALESCE (HIBERNATE_VERSION, 0) + 1)
       WHERE     INBOUND_OUTBOUND_INDICATOR = 'O'
             AND lpn_facility_status = 20
             AND TC_COMPANY_ID = p_tc_company_id
             and c_facility_id = v_facility_id
             AND TC_REFERENCE_LPN_ID IN
                    (SELECT TC_LPN_ID
                       FROM lpn
                      WHERE tc_reference_lpn_id = p_tc_lpn_id
                          and c_facility_id = v_facility_id);

      wm_log (
            'PTS-->XDK Conversion : TC_LPN_ID: '
         || p_tc_lpn_id
         || ' ::  for facility: ' || v_facility_id || ' :: '
         || 'palletization complete',
         1,
         'INVMGMT');
   END IF;

   --if the unallocated lpns are to be rolled back then delete the child lpns and the corresponding inventory records created by the  break SP
   SELECT COUNT (1)
     INTO v_total_unalloc_qty
     FROM lpn l
    WHERE     l.lpn_facility_status < 95
          AND l.tc_reference_lpn_id = p_tc_lpn_id
          AND TC_COMPANY_ID = p_tc_company_id
          and c_facility_id = v_facility_id;

   wm_log (
         'PTS-->XDK Conversion : TC_LPN_ID: '
      || p_tc_lpn_id
      || ' ::  '
      || 'total unallocated unit:'
      || v_total_unalloc_qty,
      1,
      'INVMGMT');
      
        IF v_total_unalloc_qty > 0
        THEN
            IF v_handle_unalloc_lpns = '1'
              THEN       
        
                --update the lpn status to 10/30
         
              --if the int 60 lpn which is located in inbound staging is not fully allocated and if the handle_unalloc_lpns is '1' for the disposition type
              --then all the unallocated child iLPNs are rolled back.
              
              --The int 60 lpn will be in status '30'. Update the location_id and locn_class on the WM_INVENTORY for the int 60 LPN 
              
              IF (v_curr_sub_locn_id IS NOT NULL)
              THEN     
                   
                -- We are updating WM_VERSION_ID here.Increment it by 1. We do not want to do it in the loop
                UPDATE RESV_LOCN_HDR
                SET 
                WM_VERSION_ID = (COALESCE (WM_VERSION_ID, 0) + 1)
                WHERE LOCN_ID = v_curr_sub_locn_id;
                       
                UPDATE LPN
                SET LPN_STATUS = 45,
                LPN_FACILITY_STATUS = 30,
                LAST_UPDATED_SOURCE = p_login_user_id,
                LAST_UPDATED_DTTM = SYSTIMESTAMP,
                HIBERNATE_VERSION = (COALESCE (HIBERNATE_VERSION, 0) + 1),
                LPN_FACILITY_STAT_UPDATED_DTTM = SYSTIMESTAMP,
                DISPOSITION_TYPE = p_exception_disp_type,
                CURR_SUB_LOCN_ID = v_curr_sub_locn_id
                WHERE LPN_ID = v_ilpn_id;   
                
              
                UPDATE WM_INVENTORY
                SET LOCATION_ID = v_curr_sub_locn_id,
                LOCN_CLASS = v_inbd_stag_locn_class
                WHERE LPN_ID = v_ilpn_id;
          
              ELSE
              UPDATE LPN
              SET LPN_STATUS = 45,
              LPN_FACILITY_STATUS = 10,
              LAST_UPDATED_SOURCE = p_login_user_id,
              LAST_UPDATED_DTTM = SYSTIMESTAMP,
              HIBERNATE_VERSION = (COALESCE (HIBERNATE_VERSION, 0) + 1),
              LPN_FACILITY_STAT_UPDATED_DTTM = SYSTIMESTAMP,
              DISPOSITION_TYPE = p_exception_disp_type
              WHERE LPN_ID = v_ilpn_id;           
            END IF;
            
            IF (v_outbd_stg_locn_id IS NOT NULL)
              THEN
                  UPDATE RESV_LOCN_HDR             
                  SET WM_VERSION_ID = (COALESCE (WM_VERSION_ID, 0) + 1),
                MOD_DATE_TIME = SYSTIMESTAMP
                  WHERE LOCN_ID = v_outbd_stg_locn_id;
               END IF;

         --delete all the child lpns inventory
         DELETE FROM WM_INVENTORY WI
               WHERE EXISTS
                        (SELECT 1
                           FROM LPN L
                          WHERE     WI.LPN_ID = L.LPN_ID
                                AND L.lpn_facility_status < 95
                                AND L.TC_REFERENCE_LPN_ID = p_tc_lpn_id
                                and c_facility_id = v_facility_id);

                         --delete all the child lpns not consumed
                         DELETE                                /*+ PARALLEL */
                               FROM  LPN_DETAIL
                               WHERE LPN_ID IN
                                        (SELECT LPN_ID
                                           FROM LPN
                                          WHERE LPN.LPN_FACILITY_STATUS < 95
                                                AND LPN.TC_REFERENCE_LPN_ID = p_tc_lpn_id
                                                and c_facility_id = v_facility_id);

                         DELETE                                /*+ PARALLEL */
                               FROM  LPN
                               WHERE lpn_facility_status < 95
                                     AND TC_REFERENCE_LPN_ID = p_tc_lpn_id
                                     and tc_company_id = p_tc_company_id
                                     and c_facility_id = v_facility_id;
                                                                                                      
      ELSIF v_handle_unalloc_lpns = '2'
      THEN
         --When the int 60 LPN is not fully allocated and the handle_unalloc_lpns ='2' for the Disposition type, then
         -- the unallocated child iLPNs will be preserved.
         --The int 60 LPN will be consumed
         
         UPDATE LPN
            SET LAST_UPDATED_SOURCE = p_login_user_id,
                LAST_UPDATED_DTTM = SYSTIMESTAMP,
                HIBERNATE_VERSION = (COALESCE (HIBERNATE_VERSION, 0) + 1),
                LPN_STATUS = 70,
                LPN_FACILITY_STATUS = 95,
                LPN_FACILITY_STAT_UPDATED_DTTM = SYSTIMESTAMP,
                CURR_SUB_LOCN_ID = NULL,
                WEIGHT = 0,
                ACTUAL_VOLUME = 0
          WHERE LPN_ID = v_ilpn_id;

         UPDATE LPN_DETAIL
            SET SIZE_VALUE = 0,
                LAST_UPDATED_SOURCE = p_login_user_id,
                LAST_UPDATED_DTTM = SYSTIMESTAMP,
                HIBERNATE_VERSION = (COALESCE (HIBERNATE_VERSION, 0) + 1)
          WHERE LPN_ID = v_ilpn_id;


         DELETE FROM WM_INVENTORY
               WHERE TC_LPN_ID = p_tc_lpn_id
               and c_facility_id = v_facility_id;

         UPDATE LPN
            SET LAST_UPDATED_SOURCE = p_login_user_id,
                LAST_UPDATED_DTTM = SYSTIMESTAMP,
                HIBERNATE_VERSION = (COALESCE (HIBERNATE_VERSION, 0) + 1),
                LPN_FACILITY_STAT_UPDATED_DTTM = SYSTIMESTAMP,
                DISPOSITION_TYPE = p_exception_disp_type
          WHERE LPN_FACILITY_STATUS < 95
                AND TC_REFERENCE_LPN_ID = p_tc_lpn_id
                and c_facility_id = v_facility_id;
                
          IF v_curr_sub_locn_id IS NOT NULL
          THEN
          
            --If the int 60 LPN was located in inbound_staging, then the unallocated child iLPNs should also be in the same location
            --Do the necessary updates on the resv_locn_hdr
            -- Curr_uom_qty will be incremented with the number of unallocated child iLPNs. Decrement 1 since the int 60 LPN is consumed
            
          UPDATE RESV_LOCN_HDR
          SET CURR_UOM_QTY = (CURR_UOM_QTY + v_total_unalloc_qty - 1),
              WM_VERSION_ID = (COALESCE (WM_VERSION_ID, 0) + 1)
          WHERE LOCN_ID = v_curr_sub_locn_id;
          
          UPDATE LPN
                        SET LPN_STATUS = 45,
                        LPN_FACILITY_STATUS = 30,
                        LAST_UPDATED_SOURCE = p_login_user_id,
                        LAST_UPDATED_DTTM = SYSTIMESTAMP,                        
                        LPN_FACILITY_STAT_UPDATED_DTTM = SYSTIMESTAMP,
                        DISPOSITION_TYPE = p_exception_disp_type,
            CURR_SUB_LOCN_ID = v_curr_sub_locn_id
                        WHERE LPN_FACILITY_STATUS < 95
                            AND TC_REFERENCE_LPN_ID = p_tc_lpn_id
                            and tc_company_id = p_tc_company_id
                            and c_facility_id = v_facility_id;
              
            --Also update the location_id and locn_class for the unallocated child iLPNS WM_INVENTORY record
          UPDATE WM_INVENTORY
                SET LOCATION_ID = v_curr_sub_locn_id,
                LOCN_CLASS = v_inbd_stag_locn_class
               WHERE LPN_ID IN
                        (SELECT LPN_ID
                           FROM LPN
                          WHERE LPN.LPN_FACILITY_STATUS < 95
                                AND LPN.TC_REFERENCE_LPN_ID = p_tc_lpn_id
                                and tc_company_id = p_tc_company_id
                                and c_facility_id = v_facility_id);   
          
          END IF;
          
          --Update the wm_version_id on resv_locn_hdr when the cross docked lpns are in outbound staging location
          --We do not want to do this in the loop. This field will be updated only once irrespective of the number of child iLPNs cross docked
          IF (v_outbd_stg_locn_id IS NOT NULL)
          THEN
              UPDATE RESV_LOCN_HDR             
              SET WM_VERSION_ID = (COALESCE (WM_VERSION_ID, 0) + 1),
              MOD_DATE_TIME = SYSTIMESTAMP
              WHERE LOCN_ID = v_outbd_stg_locn_id;
            END IF;
          
         --[WM-22821]
         UPDATE WM_INVENTORY 
            SET WM_ALLOCATED_QTY=0,
                LAST_UPDATED_SOURCE = p_login_user_id,
                LAST_UPDATED_DTTM = SYSTIMESTAMP,
                WM_VERSION_ID = (COALESCE (WM_VERSION_ID, 0) + 1)
             WHERE LPN_ID IN
                        (SELECT LPN_ID
                           FROM LPN
                          WHERE LPN.LPN_FACILITY_STATUS < 95
                                AND LPN.TC_REFERENCE_LPN_ID =
                                       p_tc_lpn_id
                                and lpn.tc_company_id = p_tc_company_id
                                and lpn.c_facility_id = v_facility_id);     
      END IF;      
   END IF;    


   IF v_total_size_value = 0
   THEN
      UPDATE LPN
         SET LAST_UPDATED_SOURCE = p_login_user_id,
             LAST_UPDATED_DTTM = SYSTIMESTAMP,
             HIBERNATE_VERSION = (COALESCE (HIBERNATE_VERSION, 0) + 1),
             LPN_STATUS = 70,
             LPN_FACILITY_STATUS = 95,
             LPN_FACILITY_STAT_UPDATED_DTTM = SYSTIMESTAMP,
             CURR_SUB_LOCN_ID = NULL,
             WEIGHT = 0,
             ACTUAL_VOLUME = 0
       WHERE LPN_ID = v_ilpn_id;
       
         --if the LPN was located in reserve staging and is fully allocated do the necessary updates on the resv_locn_hdr
       IF (v_curr_sub_locn_id IS NOT NULL)
          THEN
              UPDATE RESV_LOCN_HDR
              SET CURR_UOM_QTY = CURR_UOM_QTY - 1,
              WM_VERSION_ID = (COALESCE (WM_VERSION_ID, 0) + 1)
              WHERE LOCN_ID = v_curr_sub_locn_id;
       END IF;
       
       --Update the wm_version_id on resv_locn_hdr when the cross docked lpns are in outbound staging location
       --We do not want to do this in the loop. This field will be updated only once irrespective of the number of child iLPNs cross docked
        IF (v_outbd_stg_locn_id IS NOT NULL)
          THEN
              UPDATE RESV_LOCN_HDR             
              SET WM_VERSION_ID = (COALESCE (WM_VERSION_ID, 0) + 1),
              MOD_DATE_TIME = SYSTIMESTAMP
              WHERE LOCN_ID = v_outbd_stg_locn_id;
       END IF;
              
   END IF;  
   
    wm_utils.set_context_info(p_module_name => v_prev_module_name,
        p_action_name => v_prev_action_name);
   
END;
/
show errors;

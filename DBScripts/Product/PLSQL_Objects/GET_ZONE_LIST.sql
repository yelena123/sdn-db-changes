CREATE OR REPLACE FUNCTION GET_ZONE_LIST
    (
    coid IN rg_lane.tc_company_id%TYPE,
    facid IN rg_lane.o_facility_id%TYPE,
    city IN rg_lane.o_city%TYPE,
    st IN rg_lane.o_state_prov%TYPE,
    zip IN rg_lane.o_postal_code%TYPE,
    country IN rg_lane.o_country_code%TYPE
    )
RETURN VARCHAR2
IS
    CURSOR zones IS
    SELECT DISTINCT z1.zone_id
            FROM zone_attribute z1
                WHERE (z1.tc_company_id = coid)
                AND (
                        (z1.ATTRIBUTE_TYPE = 'FC' AND z1.attribute_value = TO_CHAR(facid))
                        OR (z1.ATTRIBUTE_TYPE = 'CI' AND z1.attribute_value = UPPER(city) AND z1.attribute_value2 = UPPER(st) AND z1.country_code = COUNTRY)
                        OR (z1.ATTRIBUTE_TYPE = 'ST' AND z1.attribute_value = UPPER(st) AND z1.country_code = COUNTRY)
                        OR (z1.ATTRIBUTE_TYPE = 'CT' AND z1.attribute_value = COUNTRY)
                        OR (z1.ATTRIBUTE_TYPE = 'Z2' AND z1.attribute_value = SUBSTR(zip,0,2) AND z1.country_code = COUNTRY)
                        OR (z1.ATTRIBUTE_TYPE = 'Z3' AND z1.attribute_value = SUBSTR(zip,0,3) AND z1.country_code = COUNTRY)
                        OR (z1.ATTRIBUTE_TYPE = 'Z4' AND z1.attribute_value = SUBSTR(zip,0,4) AND z1.country_code = COUNTRY)
                        OR (z1.ATTRIBUTE_TYPE = 'Z5' AND z1.attribute_value = SUBSTR(zip,0,5) AND z1.country_code = COUNTRY)
                        OR (z1.ATTRIBUTE_TYPE = 'Z6' AND z1.attribute_value = SUBSTR(zip,0,6) AND z1.country_code = COUNTRY)
                         OR (z1.ATTRIBUTE_TYPE = 'PR' AND zip between z1.attribute_value and
                            attribute_value2 AND z1.country_code = COUNTRY)    ) ;

    CURSOR facility_zones IS
           SELECT DISTINCT ZA.ZONE_ID  FROM ZONE_ATTRIBUTE ZA, FACILITY FA WHERE
                  ZA.ATTRIBUTE_VALUE = FA.FACILITY_ID AND FA.TC_COMPANY_ID = ZA.TC_COMPANY_ID AND
                  ZA.TC_COMPANY_ID = coid AND ZA.ATTRIBUTE_TYPE ='FC' AND
                  UPPER(FA.CITY) = UPPER(city) AND UPPER(FA.STATE_PROV) = UPPER(st) AND FA.POSTAL_CODE = ZIP AND FA.COUNTRY_CODE = COUNTRY;

    vzone       ZONE.zone_id%TYPE;
    zone_list   VARCHAR2(2000);
BEGIN
    OPEN zones;
    LOOP
        FETCH zones INTO vzone;
        EXIT WHEN zones%NOTFOUND;
        zone_list := zone_list || '''' || vzone || ''',';
    END LOOP;
    CLOSE zones;

    IF facid = -1 THEN
       OPEN facility_zones;
       LOOP
             FETCH facility_zones INTO vzone;
             EXIT WHEN facility_zones%NOTFOUND;
             zone_list := zone_list || '''' || vzone || ''',';
       END LOOP;
       CLOSE facility_zones;
    END IF;

    RETURN RTRIM(zone_list,',');
END;
/


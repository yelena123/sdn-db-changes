CREATE OR REPLACE
PROCEDURE XMLLANEBIDUPLOAD (i_rfpid              INTEGER,
                            i_obcarriercodeid    INTEGER,
                            i_username           VARCHAR2)
AS
   l_status               INTEGER;
   l_allowpackages        INTEGER;
   l_onewaypricing        INTEGER;
   l_tccompanyid          INTEGER;
   rfpresponsestatus      INTEGER;
   ratingrouting          INTEGER;
   pid                    INTEGER;
   weightbreaks           INTEGER;
   roundnum               INTEGER;
   i_status               INTEGER;
   countrfpresponse       DECIMAL (14, 3);
   countshipperpkg        DECIMAL (14, 3);
   ctotalvolume           DECIMAL (14, 3);
   c_upload_type_id       INT;                       -- used for xmlerror type
   c_errors               INT;
   -- used to check if there were errors in uploading lanes
   c_carrier_pack_count   INT;
   v_modeid               INT;
   countbid               INT;
   lpackagenamecount      INT;

   CURSOR cur_xmllanebid_upload
   IS
      SELECT DISTINCT rfpid,
                      obcarriercodeid,
                      packagename,
                      packageid,
                      TYPE
        FROM xmlpackage xp
       WHERE rfpid = i_rfpid AND obcarriercodeid = i_obcarriercodeid
             AND NOT EXISTS
                        (SELECT 1
                           FROM xmlerror xe
                          WHERE     xe.rfpid = xp.rfpid
                                AND xe.obcarriercodeid = xp.obcarriercodeid
                                AND xe.packagename = xp.packagename);
--      PV_FIELD1 INTEGER;
--      vtotalvolume    DECIMAL(14,3);
--      NEXTPKGNUM INTEGER;
--      PV_FIELD2 INTEGER;
--      PV_FIELD3 varchar(50);
--      let Ctotalvolume =0;
BEGIN
   c_upload_type_id := 2;
   c_carrier_pack_count := 0;
   rfpresponsestatus := 0;

   -- get max round numb for rfp
   SELECT MAX (round_num)
     INTO roundnum
     FROM rfp
    WHERE rfpid = i_rfpid;

   SELECT rfpstatus
     INTO l_status
     FROM rfp
    WHERE rfpid = i_rfpid AND round_num = roundnum;

   -- get other rfpdetails for latest round of rfp
   SELECT tccompanyid,
          allowpackages,
          onewaypricing,
          exporttoratingrouting
     INTO l_tccompanyid,
          l_allowpackages,
          l_onewaypricing,
          ratingrouting
     FROM rfp
    WHERE rfpid = i_rfpid AND round_num = roundnum;

   -- counting the open/declined responses from the carrier for the rfp in the last round
   SELECT COUNT (*)
     INTO rfpresponsestatus
     FROM rfpresponse
    WHERE     rfpid = i_rfpid
          AND obcarriercodeid = i_obcarriercodeid
          AND rfpresponsestatus IN (1, 2)
          AND round_num = roundnum;

   -- counting the WEIGHTBREAKCHARGE fields selected for the lanebid  customization page
   SELECT COUNT (*)
     INTO weightbreaks
     FROM rfpcolproperties
    WHERE     rfpid = i_rfpid
          AND colname LIKE 'WEIGHTBREAKCHARGE%'
          AND round_num = roundnum;

   -- counting the all responses from the carrier for the rfp in the last round
   SELECT COUNT (*)
     INTO countrfpresponse
     FROM rfpresponse
    WHERE     rfpid = i_rfpid
          AND obcarriercodeid = i_obcarriercodeid
          AND round_num = roundnum;

   SELECT rfpstatus
     INTO i_status
     FROM rfp
    WHERE rfpid = i_rfpid AND round_num = roundnum;

   -- Insert into rfpresponse if response is not present
   IF countrfpresponse = 0
   THEN
      INSERT INTO rfpresponse (rfpid,
                               tpcompanyid,
                               rfpresponsestatus,
                               lastupdateduid,
                               lastupdateddttm,
                               obcarriercodeid,
                               round_num)
         SELECT DISTINCT xlb.rfpid,
                         xlb.tpcompanyid,
                         0,
                         xlb.createduid,
                         SYSDATE,
                         xlb.obcarriercodeid,
                         roundnum
           FROM xmllanebid xlb
          WHERE xlb.rfpid = i_rfpid
                AND xlb.obcarriercodeid = i_obcarriercodeid;

      countrfpresponse := 1;
   ELSE
      -- Update Last Modified time in rfpresponse table
      UPDATE rfpresponse
         SET lastupdateddttm = SYSDATE,
             lastupdateduid =
                (SELECT DISTINCT xlb.createduid
                   FROM xmllanebid xlb
                  WHERE xlb.rfpid = i_rfpid
                        AND xlb.obcarriercodeid = i_obcarriercodeid)
       WHERE     rfpid = i_rfpid
             AND obcarriercodeid = i_obcarriercodeid
             AND round_num = roundnum;
   END IF;

   -- counting all the shipper packages from XMLPACKAGE for current round of rfp and which doesnot have any errors
   SELECT COUNT (*)
     INTO countshipperpkg
     FROM xmlpackage xp
    WHERE     rfpid = i_rfpid
          AND obcarriercodeid = i_obcarriercodeid
          AND TYPE = 'S'
          AND NOT EXISTS
                 (SELECT 1
                    FROM PACKAGE p
                   WHERE     p.rfpid = xp.rfpid
                         AND p.packagename = xp.packagename
                         AND p.obcarriercodeid = xp.obcarriercodeid
                         AND p.round_num < (SELECT MAX (round_num)
                                              FROM rfp
                                             WHERE rfpid = i_rfpid))
          AND NOT EXISTS
                     (SELECT 1
                        FROM xmlerror xe
                       WHERE     xe.rfpid = xp.rfpid
                             AND xe.obcarriercodeid = xp.obcarriercodeid
                             AND xe.packagename = xp.packagename);

   -- inserting into xmlerror all the lanes for which the line items do not have a match in laneequipmenttype for line items in xmllanebid
   INSERT INTO xmlerror (upload_type_id,
                         rfpid,
                         laneid,
                         laneequipmenttypeid,
                         obcarriercodeid,
                         errordesc)
      SELECT c_upload_type_id,
             rfpid,
             laneid,
             laneequipmenttypeid,
             obcarriercodeid,
             'bad line item'
        FROM xmllanebid xlb
       WHERE xlb.rfpid = i_rfpid AND xlb.obcarriercodeid = i_obcarriercodeid
             AND NOT EXISTS
                        (SELECT 1
                           FROM laneequipmenttype leqt
                          WHERE leqt.rfpid = xlb.rfpid
                                AND leqt.laneid = xlb.laneid
                                AND NVL (leqt.equipmenttype_id, -1) =
                                       NVL (xlb.equipmenttype_id, -1)
                                AND NVL (leqt.servicetype_id, -1) =
                                       NVL (xlb.servicetype_id, -1)
                                AND NVL (leqt.mot, '|||') =
                                       NVL (xlb.mot, '|||')
                                AND NVL (leqt.commoditycode_id, -1) =
                                       NVL (xlb.commoditycode_id, -1)
                                AND NVL (leqt.protectionlevel_id, -1) =
                                       NVL (xlb.protectionlevel_id, -1));

   -- inserting into xmlerror all the lanes which have duplicate line item
   INSERT INTO xmlerror (upload_type_id,
                         rfpid,
                         laneid,
                         obcarriercodeid,
                         errordesc)
        SELECT c_upload_type_id,
               rfpid,
               laneid,
               obcarriercodeid,
               'duplicate line item'
          FROM xmllanebid xlb
         WHERE xlb.rfpid = i_rfpid AND xlb.obcarriercodeid = i_obcarriercodeid
               AND NVL (xlb.packagename, '|||') =
                      CASE l_allowpackages
                         WHEN 1 THEN NVL (xlb.packagename, '|||')
                         ELSE '|||'
                      END
      GROUP BY rfpid,
               laneid,
               obcarriercodeid,
               packagename,
               equipmenttype_id,
               servicetype_id,
               protectionlevel_id,
               mot,
               commoditycode_id
        HAVING COUNT (*) > 1;

   -- updating packageid in xmllanebid from the package table
   UPDATE xmllanebid
      SET packageid =
             (SELECT packageid
                FROM PACKAGE p
               WHERE p.rfpid = i_rfpid
                     AND (p.obcarriercodeid = i_obcarriercodeid
                          OR p.obcarriercodeid IS NULL)
                     AND p.packagename = xmllanebid.packagename)
    WHERE     xmllanebid.rfpid = i_rfpid
          AND xmllanebid.obcarriercodeid = i_obcarriercodeid
          AND xmllanebid.packagename IS NOT NULL;

   -- updating package with id  0 to -1 in xmllanebid
   UPDATE xmllanebid
      SET packageid = -1
    WHERE     xmllanebid.rfpid = i_rfpid
          AND xmllanebid.obcarriercodeid = i_obcarriercodeid
          AND xmllanebid.packagename IS NOT NULL
          AND xmllanebid.packageid = 0;

   -- updating this rfp package  id  to 0 in xmllanebid
   UPDATE xmllanebid
      SET packageid = 0
    WHERE     xmllanebid.rfpid = i_rfpid
          AND xmllanebid.obcarriercodeid = i_obcarriercodeid
          AND xmllanebid.packageid IS NULL;

   UPDATE xmllanebidfreightcharge
      SET packageid =
             (SELECT packageid
                FROM PACKAGE p
               WHERE p.rfpid = i_rfpid
                     AND (p.obcarriercodeid = i_obcarriercodeid
                          OR p.obcarriercodeid IS NULL)
                     AND p.packagename = xmllanebidfreightcharge.packagename)
    WHERE     xmllanebidfreightcharge.rfpid = i_rfpid
          AND xmllanebidfreightcharge.obcarriercodeid = i_obcarriercodeid
          AND xmllanebidfreightcharge.packagename IS NOT NULL;

   UPDATE xmllanebidfreightcharge
      SET packageid = -1
    WHERE     xmllanebidfreightcharge.rfpid = i_rfpid
          AND xmllanebidfreightcharge.obcarriercodeid = i_obcarriercodeid
          AND xmllanebidfreightcharge.packagename IS NOT NULL
          AND xmllanebidfreightcharge.packageid = 0;

   UPDATE xmllanebidfreightcharge
      SET packageid = 0
    WHERE     xmllanebidfreightcharge.rfpid = i_rfpid
          AND xmllanebidfreightcharge.obcarriercodeid = i_obcarriercodeid
          AND xmllanebidfreightcharge.packageid IS NULL;

   INSERT INTO xmlerror (upload_type_id,
                         rfpid,
                         laneid,
                         laneequipmenttypeid,
                         obcarriercodeid,
                         errordesc)
      SELECT c_upload_type_id,
             rfpid,
             laneid,
             laneequipmenttypeid,
             obcarriercodeid,
             'Invalid Currency : ' || NVL (xlbfc.currencycode, '')
        FROM xmllanebidfreightcharge xlbfc
       WHERE xlbfc.rfpid = i_rfpid
             AND xlbfc.obcarriercodeid = i_obcarriercodeid
             AND NOT EXISTS
                        (SELECT 'x'
                           FROM currency_conversion c, rfp r
                          WHERE r.rfpid = xlbfc.rfpid
                                AND r.tccompanyid = c.tc_company_id
                                AND UPPER (r.currencycode) =
                                       UPPER (c.to_currency_code)
                                AND UPPER (c.from_currency_code) =
                                       UPPER (xlbfc.currencycode))
             AND NOT EXISTS
                        (SELECT 'x'
                           FROM rfp r
                          WHERE r.rfpid = xlbfc.rfpid
                                AND UPPER (r.currencycode) =
                                       UPPER (xlbfc.currencycode))
             AND NOT xlbfc.currencycode IS NULL;

   INSERT INTO xmlerror (upload_type_id,
                         rfpid,
                         laneid,
                         laneequipmenttypeid,
                         obcarriercodeid,
                         errordesc)
      SELECT c_upload_type_id,
             rfpid,
             laneid,
             laneequipmenttypeid,
             obcarriercodeid,
             'Invalid Currency : ' || NVL (xlb.currencycode, '')
        FROM xmllanebid xlb
       WHERE xlb.rfpid = i_rfpid AND xlb.obcarriercodeid = i_obcarriercodeid
             AND NOT EXISTS
                        (SELECT 'x'
                           FROM rfp_currency_conversion c, rfp r
                          WHERE     r.rfpid = xlb.rfpid
                                AND r.rfpid = c.rfpid
                                AND r.currencycode = c.to_currency_code
                                AND c.from_currency_code = xlb.currencycode)
             AND NOT EXISTS
                        (SELECT 'x'
                           FROM rfp r
                          WHERE r.rfpid = xlb.rfpid
                                AND r.currencycode = xlb.currencycode)
             AND NOT xlb.currencycode IS NULL;

   IF (l_onewaypricing = 1)
   THEN
      -- lanebidmin change begin
      INSERT INTO xmlerror (upload_type_id,
                            rfpid,
                            laneid,
                            obcarriercodeid,
                            errordesc)
         SELECT c_upload_type_id,
                xlb.rfpid,
                xlb.laneid,
                xlb.obcarriercodeid,
                'upward revision of price not allowed'
           FROM xmllanebid xlb,
                (  SELECT lb.rfpid,
                          lb.obcarriercodeid,
                          laneid,
                          equipmenttype_id,
                          servicetype_id,
                          protectionlevel_id,
                          mot,
                          commoditycode_id,
                          packageid,
                          MIN (baserate) baserate,
                          MIN (flatcharge) flatcharge,
                          MIN (inclusiverate) inclusiverate,
                          MIN (minimumcharge) minimumcharge,
                          MIN (rateperdistance) rateperdistance,
                          MIN (ratepersize) ratepersize,
                          MIN (rateperuom) rateperuom,
                          MIN (weightbreakcharge1) weightbreakcharge1,
                          MIN (weightbreakcharge2) weightbreakcharge2,
                          MIN (weightbreakcharge3) weightbreakcharge3,
                          MIN (weightbreakcharge4) weightbreakcharge4,
                          MIN (weightbreakcharge5) weightbreakcharge5,
                          MIN (weightbreakcharge6) weightbreakcharge6,
                          MIN (weightbreakcharge7) weightbreakcharge7,
                          MIN (weightbreakcharge8) weightbreakcharge8,
                          MIN (weightbreakcharge9) weightbreakcharge9
                     FROM lanebid lb, rfpresponse rr
                    WHERE     lb.rfpid = i_rfpid
                          AND lb.obcarriercodeid = i_obcarriercodeid
                          AND lb.round_num <> (SELECT MAX (round_num)
                                                 FROM rfp
                                                WHERE rfpid = lb.rfpid)
                          AND lb.historicalaward = 0
                          AND (lb.status = 'Bid On' OR lb.packageid <> 0)
                          AND rr.rfpid = lb.rfpid
                          AND rr.obcarriercodeid = lb.obcarriercodeid
                          AND rr.round_num = lb.round_num
                          AND rr.rfpresponsestatus = 1
                 GROUP BY lb.rfpid,
                          laneid,
                          lb.obcarriercodeid,
                          equipmenttype_id,
                          servicetype_id,
                          protectionlevel_id,
                          mot,
                          commoditycode_id,
                          packageid) tlb
          WHERE     xlb.rfpid = i_rfpid
                AND xlb.obcarriercodeid = i_obcarriercodeid
                AND tlb.rfpid = xlb.rfpid
                AND tlb.obcarriercodeid = xlb.obcarriercodeid
                AND tlb.laneid = xlb.laneid
                AND NVL (tlb.equipmenttype_id, -1) =
                       NVL (xlb.equipmenttype_id, -1)
                AND NVL (tlb.servicetype_id, -1) =
                       NVL (xlb.servicetype_id, -1)
                AND NVL (tlb.mot, '|||') = NVL (xlb.mot, '|||')
                AND NVL (tlb.commoditycode_id, -1) =
                       NVL (xlb.commoditycode_id, -1)
                AND NVL (tlb.protectionlevel_id, -1) =
                       NVL (xlb.protectionlevel_id, -1)
                AND xlb.packageid =
                       CASE l_allowpackages
                          WHEN 1 THEN xlb.packageid
                          ELSE 0
                       END
                AND xlb.packageid = tlb.packageid
                AND (xlb.packagename IS NULL OR xlb.packageid != 0) -- To avoid newly uploaded packages
                AND ( (NVL (xlb.baserate, 0) >
                          CASE
                             WHEN NVL (tlb.baserate, 0) = 0
                             THEN
                                NVL (xlb.baserate, 0)
                             ELSE
                                tlb.baserate
                          END)
                     OR (NVL (xlb.flatcharge, 0) >
                            CASE
                               WHEN NVL (tlb.flatcharge, 0) = 0
                               THEN
                                  NVL (xlb.flatcharge, 0)
                               ELSE
                                  tlb.flatcharge
                            END)
                     OR (NVL (xlb.inclusiverate, 0) >
                            CASE
                               WHEN NVL (tlb.inclusiverate, 0) = 0
                               THEN
                                  NVL (xlb.inclusiverate, 0)
                               ELSE
                                  tlb.inclusiverate
                            END)
                     OR (NVL (xlb.minimumcharge, 0) >
                            CASE
                               WHEN NVL (tlb.minimumcharge, 0) = 0
                               THEN
                                  NVL (xlb.minimumcharge, 0)
                               ELSE
                                  tlb.minimumcharge
                            END)
                     OR (NVL (xlb.rateperdistance, 0) >
                            CASE
                               WHEN NVL (tlb.rateperdistance, 0) = 0
                               THEN
                                  NVL (xlb.rateperdistance, 0)
                               ELSE
                                  tlb.rateperdistance
                            END)
                     OR (NVL (xlb.ratepersize, 0) >
                            CASE
                               WHEN NVL (tlb.ratepersize, 0) = 0
                               THEN
                                  NVL (xlb.ratepersize, 0)
                               ELSE
                                  tlb.ratepersize
                            END)
                     OR (NVL (xlb.rateperuom, 0) >
                            CASE
                               WHEN NVL (tlb.rateperuom, 0) = 0
                               THEN
                                  NVL (xlb.rateperuom, 0)
                               ELSE
                                  tlb.rateperuom
                            END)
                     OR (NVL (xlb.weightbreakcharge1, 0) >
                            CASE
                               WHEN NVL (tlb.weightbreakcharge1, 0) = 0
                               THEN
                                  NVL (xlb.weightbreakcharge1, 0)
                               ELSE
                                  tlb.weightbreakcharge1
                            END)
                     OR (NVL (xlb.weightbreakcharge2, 0) >
                            CASE
                               WHEN NVL (tlb.weightbreakcharge2, 0) = 0
                               THEN
                                  NVL (xlb.weightbreakcharge2, 0)
                               ELSE
                                  tlb.weightbreakcharge2
                            END)
                     OR (NVL (xlb.weightbreakcharge3, 0) >
                            CASE
                               WHEN NVL (tlb.weightbreakcharge3, 0) = 0
                               THEN
                                  NVL (xlb.weightbreakcharge3, 0)
                               ELSE
                                  tlb.weightbreakcharge3
                            END)
                     OR (NVL (xlb.weightbreakcharge4, 0) >
                            CASE
                               WHEN NVL (tlb.weightbreakcharge4, 0) = 0
                               THEN
                                  NVL (xlb.weightbreakcharge4, 0)
                               ELSE
                                  tlb.weightbreakcharge4
                            END)
                     OR (NVL (xlb.weightbreakcharge5, 0) >
                            CASE
                               WHEN NVL (tlb.weightbreakcharge5, 0) = 0
                               THEN
                                  NVL (xlb.weightbreakcharge5, 0)
                               ELSE
                                  tlb.weightbreakcharge5
                            END)
                     OR (NVL (xlb.weightbreakcharge6, 0) >
                            CASE
                               WHEN NVL (tlb.weightbreakcharge6, 0) = 0
                               THEN
                                  NVL (xlb.weightbreakcharge6, 0)
                               ELSE
                                  tlb.weightbreakcharge6
                            END)
                     OR (NVL (xlb.weightbreakcharge7, 0) >
                            CASE
                               WHEN NVL (tlb.weightbreakcharge7, 0) = 0
                               THEN
                                  NVL (xlb.weightbreakcharge7, 0)
                               ELSE
                                  tlb.weightbreakcharge7
                            END)
                     OR (NVL (xlb.weightbreakcharge8, 0) >
                            CASE
                               WHEN NVL (tlb.weightbreakcharge8, 0) = 0
                               THEN
                                  NVL (xlb.weightbreakcharge8, 0)
                               ELSE
                                  tlb.weightbreakcharge8
                            END)
                     OR (NVL (xlb.weightbreakcharge9, 0) >
                            CASE
                               WHEN NVL (tlb.weightbreakcharge9, 0) = 0
                               THEN
                                  NVL (xlb.weightbreakcharge9, 0)
                               ELSE
                                  tlb.weightbreakcharge9
                            END));
   -- lanebidmin change end
   END IF;

   IF l_allowpackages = 1
   THEN
      INSERT INTO xmlerror (upload_type_id,
                            rfpid,
                            laneid,
                            obcarriercodeid,
                            laneequipmenttypeid,
                            packagename,
                            errordesc)
           SELECT c_upload_type_id,
                  rfpid,
                  laneid,
                  obcarriercodeid,
                  laneequipmenttypeid,
                  packagename,
                  'same lane multiple times in a package'
             FROM xmllanebid xlb
            WHERE     xlb.rfpid = i_rfpid
                  AND xlb.obcarriercodeid = i_obcarriercodeid
                  AND (xlb.packagename IS NOT NULL)
         GROUP BY rfpid,
                  packagename,
                  laneid,
                  obcarriercodeid,
				  laneequipmenttypeid
           HAVING COUNT (*) > 1;

      INSERT INTO xmlerror (upload_type_id,
                            rfpid,
                            laneid,
                            obcarriercodeid,
                            packagename,
                            errordesc)
         SELECT c_upload_type_id,
                xlb.rfpid,
                xlb.laneid,
                obcarriercodeid,
                packagename,
                'capacity cannot exceed volume for packages'
           FROM xmllanebid xlb, lane l
          WHERE xlb.rfpid = l.rfpid AND xlb.laneid = l.laneid
                AND l.round_num =
                       (SELECT MAX (ml.round_num)
                          FROM lane ml
                         WHERE ml.rfpid = l.rfpid AND ml.laneid = l.laneid)
                AND xlb.rfpid = i_rfpid
                AND xlb.obcarriercodeid = i_obcarriercodeid
                AND xlb.packagename IS NOT NULL
                AND xlb.lanecapacity > l.volume;
   END IF;

   IF ratingrouting = 1
   THEN
      INSERT INTO xmlerror (upload_type_id,
                            rfpid,
                            laneid,
                            obcarriercodeid,
                            errordesc)
         SELECT c_upload_type_id,
                rfpid,
                laneid,
                obcarriercodeid,
                'max capacity should be less than surge capacity'
           FROM xmllanebid xlb
          WHERE     xlb.rfpid = i_rfpid
                AND xlb.obcarriercodeid = i_obcarriercodeid
                AND xlb.surgecapacity < xlb.lanecapacity;

      INSERT INTO xmlerror (upload_type_id,
                            rfpid,
                            laneid,
                            obcarriercodeid,
                            errordesc)
         SELECT DISTINCT c_upload_type_id,
                         xlb.rfpid,
                         xlb.laneid,
                         xlb.obcarriercodeid,
                         'carrier infeasible with origin facility on lane'
           FROM xmllanebid xlb, obcarriercode o, lane l
          WHERE     xlb.rfpid = i_rfpid
                AND xlb.obcarriercodeid = i_obcarriercodeid
                AND xlb.obcarriercodeid = o.obcarriercodeid
                AND l.rfpid = xlb.rfpid
                AND l.laneid = xlb.laneid
                AND l.round_num =
                       (SELECT MAX (ml.round_num)
                          FROM lane ml
                         WHERE ml.rfpid = l.rfpid AND ml.laneid = l.laneid)
                AND l.originfacilitycode IS NOT NULL
                AND EXISTS
                       (SELECT 'X'
                          FROM infeasibility i, facility_alias fa
                         WHERE     i.tc_company_id = l_tccompanyid
                               AND (i.tc_company_id = fa.tc_company_id)
                               AND (infeasibility_type = 'FATP')
                               AND (fa.facility_alias_id =
                                       l.originfacilitycode)
                               AND value1 = TO_CHAR (fa.facility_id)
                               AND value2 = o.carriercode);

      INSERT INTO xmlerror (upload_type_id,
                            rfpid,
                            laneid,
                            obcarriercodeid,
                            errordesc)
         SELECT DISTINCT
                c_upload_type_id,
                xlb.rfpid,
                xlb.laneid,
                xlb.obcarriercodeid,
                'carrier infeasible with destination facility on lane'
           FROM xmllanebid xlb, obcarriercode o, lane l
          WHERE     xlb.rfpid = i_rfpid
                AND xlb.obcarriercodeid = i_obcarriercodeid
                AND xlb.obcarriercodeid = o.obcarriercodeid
                AND l.rfpid = xlb.rfpid
                AND l.laneid = xlb.laneid
                AND l.round_num =
                       (SELECT MAX (ml.round_num)
                          FROM lane ml
                         WHERE ml.rfpid = l.rfpid AND ml.laneid = l.laneid)
                AND l.destinationfacilitycode IS NOT NULL
                AND EXISTS
                       (SELECT 'X'
                          FROM infeasibility i, facility_alias fa
                         WHERE     i.tc_company_id = l_tccompanyid
                               AND (i.tc_company_id = fa.tc_company_id)
                               AND (infeasibility_type = 'FATP')
                               AND (fa.facility_alias_id =
                                       l.destinationfacilitycode)
                               AND value1 = TO_CHAR (fa.facility_id)
                               AND value2 = o.carriercode);
   END IF;

   SELECT COUNT (*) cnt
     INTO c_errors
     FROM xmlerror
    WHERE     rfpid = i_rfpid
          AND upload_type_id = c_upload_type_id
          AND obcarriercodeid = i_obcarriercodeid;

   IF c_errors = 0
   THEN
      IF l_allowpackages = 1
      THEN
         UPDATE xmlpackage xp
            SET packageid =
                   (SELECT packageid
                      FROM PACKAGE
                     WHERE packagename = xp.packagename AND rfpid = i_rfpid)
          WHERE xp.rfpid = i_rfpid;

         FOR c1 IN cur_xmllanebid_upload
         LOOP
            IF (c1.packageid IS NULL)
            THEN
               SELECT package_sequence.NEXTVAL INTO pid FROM DUAL;
            ELSE
               pid := c1.packageid;
            END IF;

            -- Shipper Package should be available
            IF c1.TYPE = 'S'
            THEN
               SELECT COUNT (packagename)
                 INTO lpackagenamecount
                 FROM PACKAGE
                WHERE packagename = c1.packagename AND rfpid = i_rfpid;

               IF lpackagenamecount = 0
               THEN
                  INSERT INTO xmlerror (upload_type_id,
                                        rfpid,
                                        obcarriercodeid,
                                        errordesc)
                       VALUES (
                                 c_upload_type_id,
                                 c1.rfpid,
                                 c1.obcarriercodeid,
                                    'shipper package name '
                                 || c1.packagename
                                 || ' not available as part of shipper');
               END IF;
            ELSE
               SELECT COUNT (packagename)
                 INTO lpackagenamecount
                 FROM PACKAGE
                WHERE     packagename = c1.packagename
                      AND obcarriercodeid IS NULL
                      AND rfpid = i_rfpid;

               IF lpackagenamecount = 1
               THEN
                  INSERT INTO xmlerror (upload_type_id,
                                        rfpid,
                                        obcarriercodeid,
                                        errordesc)
                       VALUES (
                                 c_upload_type_id,
                                 c1.rfpid,
                                 c1.obcarriercodeid,
                                    'package name '
                                 || c1.packagename
                                 || ' already available as part of shipper');
               ELSE
                  SELECT COUNT (packagename)
                    INTO lpackagenamecount
                    FROM PACKAGE
                   WHERE packagename = c1.packagename AND rfpid = i_rfpid;

                  IF lpackagenamecount = 0
                  THEN
                     INSERT INTO PACKAGE (packageid,
                                          rfpid,
                                          obcarriercodeid,
                                          packagename,
                                          totalvolume,
                                          awardstatus,
                                          round_num)
                          VALUES (pid,
                                  c1.rfpid,
                                  c1.obcarriercodeid,
                                  c1.packagename,
                                  0,
                                  NULL,
                                  roundnum);
                  END IF;

                  INSERT INTO packagelane (packageid, rfpid, laneid)
                     SELECT pid, xp.rfpid, xp.laneid
                       FROM xmlpackage xp
                      WHERE     xp.packagename = c1.packagename
                            AND xp.rfpid = i_rfpid
                            AND xp.obcarriercodeid = i_obcarriercodeid
                            AND xp.laneid NOT IN
                                   (SELECT laneid
                                      FROM packagelane
                                     WHERE rfpid = c1.rfpid
                                           AND packageid = pid);

                  SELECT SUM (
                            l.volume * 7
                            / (CASE l.volumefrequency
                                  WHEN 0 THEN 1
                                  WHEN 1 THEN 7
                                  WHEN 2 THEN 14
                                  WHEN 3 THEN 30.4166
                                  WHEN 4 THEN 182.5
                                  WHEN 5 THEN 365
                                  ELSE 7
                               END))
                    INTO ctotalvolume
                    FROM lane l, packagelane pl, PACKAGE p
                   WHERE     l.rfpid = pl.rfpid
                         AND l.rfpid = c1.rfpid
                         AND l.laneid = pl.laneid
                         AND p.packageid = pl.packageid
                         AND p.packagename = c1.packagename
                         AND p.obcarriercodeid = c1.obcarriercodeid;

                  UPDATE PACKAGE
                     SET totalvolume = ctotalvolume
                   WHERE     packagename = c1.packagename
                         AND rfpid = c1.rfpid
                         AND obcarriercodeid = c1.obcarriercodeid;

                  UPDATE xmllanebid
                     SET packageid = pid
                   WHERE     rfpid = i_rfpid
                         AND obcarriercodeid = i_obcarriercodeid
                         AND packagename = c1.packagename;

                  UPDATE xmllanebidfreightcharge
                     SET packageid = pid
                   WHERE     rfpid = i_rfpid
                         AND obcarriercodeid = i_obcarriercodeid
                         AND packagename = c1.packagename;
               END IF;
            END IF;

            c_carrier_pack_count := 1;
         END LOOP;
      END IF;

      --commit;
      DELETE FROM lanebidfreightcharge lfc
            WHERE     obcarriercodeid = i_obcarriercodeid
                  AND round_num = roundnum
                  AND laneequipmenttypeid IN (SELECT laneequipmenttypeid
                                                FROM laneequipmenttype
                                               WHERE rfpid = i_rfpid)
                  AND EXISTS
                         (SELECT 1
                            FROM xmllanebidfreightcharge xlfc
                           WHERE xlfc.laneequipmenttypeid =
                                    lfc.laneequipmenttypeid
                                 AND xlfc.obcarriercodeid =
                                        lfc.obcarriercodeid
                                 AND xlfc.packageid = lfc.packageid
                                 AND xlfc.freightchargetypeid =
                                        lfc.freightchargetypeid
                          UNION
                          SELECT 1
                            FROM xmllanebid xlb, laneequipmenttype leqt
                           WHERE     xlb.rfpid = i_rfpid
                                 AND xlb.rfpid = leqt.rfpid
                                 AND xlb.laneid = leqt.laneid
                                 AND NVL (leqt.equipmenttype_id, -1) =
                                        NVL (xlb.equipmenttype_id, -1)
                                 AND NVL (leqt.servicetype_id, -1) =
                                        NVL (xlb.servicetype_id, -1)
                                 AND NVL (leqt.mot, '|||') =
                                        NVL (xlb.mot, '|||')
                                 AND NVL (leqt.commoditycode_id, -1) =
                                        NVL (xlb.commoditycode_id, -1)
                                 AND NVL (leqt.protectionlevel_id, -1) =
                                        NVL (xlb.protectionlevel_id, -1)
                                 AND xlb.packageid = lfc.packageid
                                 AND xlb.obcarriercodeid = i_obcarriercodeid
                                 AND leqt.laneequipmenttypeid =
                                        lfc.laneequipmenttypeid
                                 AND lfc.obcarriercodeid = i_obcarriercodeid);

      MERGE INTO lanebid lb1
           USING (SELECT leqt.laneequipmenttypeid,
                         xlb.obcarriercodeid,
                         xlb.tpcompanyid,
                         xlb.rfpid,
                         xlb.laneid,
                         laneauctionstrategy,
                         strategytype,
                         strategybaseline,
                         strategypercentage,
                         lowestbid,
                         xlb.transittime,
                         xlb.transittimeuom,
                         lanecapacity,
                         surgecapacity,
                         minimumcharge,
                         flatcharge,
                         baserate,
                         inclusiverate,
                         NVL (currencycode, 'USD') currencycode,
                         weightbreakcharge1,
                         weightbreakcharge2,
                         weightbreakcharge3,
                         weightbreakcharge4,
                         weightbreakcharge5,
                         weightbreakcharge6,
                         weightbreakcharge7,
                         weightbreakcharge8,
                         weightbreakcharge9,
                         weightcapacity,
                         xlb.weightuom,
                         rateperuom,
                         flightfrequency,
                         sailingallocation,
                         sailingfrequency,
                         chargebasis,
                         comments,
                         isawarded,
                         awardamount,
                         awardispercentage,
                         xlb.createduid,
                         xlb.createddttm,
                         xlb.lastupdateduid,
                         xlb.lastupdateddttm,
                         packageid,
                         xlb.custtext1,
                         xlb.custtext2,
                         xlb.custtext3,
                         xlb.custtext4,
                         xlb.custtext5,
                         xlb.custtext6,
                         xlb.custtext7,
                         xlb.custtext8,
                         xlb.custtext9,
                         xlb.custtext10,
                         xlb.custint1,
                         xlb.custint2,
                         xlb.custint3,
                         xlb.custint4,
                         xlb.custint5,
                         xlb.custint6,
                         xlb.custint7,
                         xlb.custint8,
                         xlb.custint9,
                         xlb.custint10,
                         xlb.custdouble1,
                         xlb.custdouble2,
                         xlb.custdouble3,
                         xlb.custdouble4,
                         xlb.custdouble5,
                         xlb.custdouble6,
                         xlb.custdouble7,
                         xlb.custdouble8,
                         xlb.custdouble9,
                         xlb.custdouble10,
                         xlb.protectionlevel_id,
                         xlb.equipmenttype_id,
                         xlb.servicetype_id,
                         xlb.mot,
                         xlb.commoditycode_id,
                         --greatest(nvl(minimumcharge,0), nvl(flatcharge,0), nvl(ratepersize,0), nvl(baserate,0),nvl(inclusiverate,0), nvl(rateperdistance*l.mileage,0)) ,
                         0 costperload,
                         ratepersize,
                         rateperdistance,
                         xlb.ratediscount,
                         roundnum round_num,
                         -1 erattributeversion
                    FROM xmllanebid xlb, laneequipmenttype leqt     --, lane l
                   WHERE     xlb.rfpid = i_rfpid
                         AND xlb.obcarriercodeid = i_obcarriercodeid
                         AND leqt.rfpid = xlb.rfpid
                         --AND l.rfpid = leqt.rfpid AND l.laneid = leqt.laneid
                         AND leqt.laneid = xlb.laneid
                         AND NVL (leqt.equipmenttype_id, -1) =
                                NVL (xlb.equipmenttype_id, -1)
                         AND NVL (leqt.servicetype_id, -1) =
                                NVL (xlb.servicetype_id, -1)
                         AND NVL (leqt.mot, '|||') = NVL (xlb.mot, '|||')
                         AND NVL (leqt.commoditycode_id, -1) =
                                NVL (xlb.commoditycode_id, -1)
                         AND NVL (leqt.protectionlevel_id, -1) =
                                NVL (xlb.protectionlevel_id, -1)
                         AND xlb.packageid =
                                CASE l_allowpackages
                                   WHEN 1 THEN xlb.packageid
                                   ELSE 0
                                END
                         AND xlb.packageid >= 0
                         AND xlb.changed = 1
                         --AND l.round_num = (SELECT MAX(ml.round_num) FROM lane ml WHERE ml.rfpid = l.rfpid  AND ml.laneid = l.laneid)
                         AND NOT EXISTS
                                    (SELECT 1
                                       FROM xmlerror xe
                                      WHERE xe.rfpid = xlb.rfpid
                                            AND xe.obcarriercodeid =
                                                   i_obcarriercodeid
                                            AND ( (xe.laneid = xlb.laneid
                                                   AND xe.packagename IS NULL
                                                   AND xlb.packagename
                                                          IS NULL)
                                                 OR (xe.packagename
                                                        IS NOT NULL
                                                     AND xe.packagename =
                                                            xlb.packagename)))) xlb1
              ON (    lb1.laneid = xlb1.laneid
                  AND lb1.rfpid = xlb1.rfpid
                  AND lb1.obcarriercodeid = xlb1.obcarriercodeid
                  AND lb1.packageid = xlb1.packageid
                  AND lb1.laneequipmenttypeid = xlb1.laneequipmenttypeid
                  AND lb1.round_num = xlb1.round_num)
      WHEN MATCHED
      THEN
         UPDATE SET
            status = 'Bid On',
            tpcompanyid = xlb1.tpcompanyid,
            laneauctionstrategy = xlb1.laneauctionstrategy,
            strategytype = xlb1.strategytype,
            strategybaseline = xlb1.strategybaseline,
            strategypercentage = xlb1.strategypercentage,
            lowestbid = xlb1.lowestbid,
            transittime = xlb1.transittime,
            transittimeuom = xlb1.transittimeuom,
            lanecapacity = xlb1.lanecapacity,
            surgecapacity = xlb1.surgecapacity,
            minimumcharge = xlb1.minimumcharge,
            flatcharge = xlb1.flatcharge,
            baserate = xlb1.baserate,
            inclusiverate = xlb1.inclusiverate,
            currencycode = xlb1.currencycode,
            weightbreakcharge1 = xlb1.weightbreakcharge1,
            weightbreakcharge2 = xlb1.weightbreakcharge2,
            weightbreakcharge3 = xlb1.weightbreakcharge3,
            weightbreakcharge4 = xlb1.weightbreakcharge4,
            weightbreakcharge5 = xlb1.weightbreakcharge5,
            weightbreakcharge6 = xlb1.weightbreakcharge6,
            weightbreakcharge7 = xlb1.weightbreakcharge7,
            weightbreakcharge8 = xlb1.weightbreakcharge8,
            weightbreakcharge9 = xlb1.weightbreakcharge9,
            weightcapacity = xlb1.weightcapacity,
            weightuom = xlb1.weightuom,
            rateperuom = xlb1.rateperuom,
            flightfrequency = xlb1.flightfrequency,
            sailingallocation = xlb1.sailingallocation,
            sailingfrequency = xlb1.sailingfrequency,
            chargebasis = xlb1.chargebasis,
            comments = xlb1.comments,
            isawarded = xlb1.isawarded,
            awardamount = xlb1.awardamount,
            awardispercentage = xlb1.awardispercentage,
            createduid = xlb1.createduid,
            createddttm = xlb1.createddttm,
            lastupdateduid = xlb1.lastupdateduid,
            lastupdateddttm = xlb1.lastupdateddttm,
            custtext1 = xlb1.custtext1,
            custtext2 = xlb1.custtext2,
            custtext3 = xlb1.custtext3,
            custtext4 = xlb1.custtext4,
            custtext5 = xlb1.custtext5,
            custtext6 = xlb1.custtext6,
            custtext7 = xlb1.custtext7,
            custtext8 = xlb1.custtext8,
            custtext9 = xlb1.custtext9,
            custtext10 = xlb1.custtext10,
            custint1 = xlb1.custint1,
            custint2 = xlb1.custint2,
            custint3 = xlb1.custint3,
            custint4 = xlb1.custint4,
            custint5 = xlb1.custint5,
            custint6 = xlb1.custint6,
            custint7 = xlb1.custint7,
            custint8 = xlb1.custint8,
            custint9 = xlb1.custint9,
            custint10 = xlb1.custint10,
            custdouble1 = xlb1.custdouble1,
            custdouble2 = xlb1.custdouble2,
            custdouble3 = xlb1.custdouble3,
            custdouble4 = xlb1.custdouble4,
            custdouble5 = xlb1.custdouble5,
            custdouble6 = xlb1.custdouble6,
            custdouble7 = xlb1.custdouble7,
            custdouble8 = xlb1.custdouble8,
            custdouble9 = xlb1.custdouble9,
            custdouble10 = xlb1.custdouble10,
            protectionlevel_id = xlb1.protectionlevel_id,
            equipmenttype_id = xlb1.equipmenttype_id,
            servicetype_id = xlb1.servicetype_id,
            costperload = xlb1.costperload,
            ratepersize = xlb1.ratepersize,
            rateperdistance = xlb1.rateperdistance,
            ratediscount = xlb1.ratediscount,
            erattributeversion = xlb1.erattributeversion,
            mot = xlb1.mot,
            commoditycode_id = xlb1.commoditycode_id,
            last_updated_source_type = 1,
            last_updated_source = i_username
                 WHERE NVL (lb1.inclusiverate, -999) <>
                          NVL (xlb1.inclusiverate, -999)
                       OR NVL (lb1.lanecapacity, -999) <>
                             NVL (xlb1.lanecapacity, -999)
                       OR NVL (lb1.surgecapacity, -999) <>
                             NVL (xlb1.surgecapacity, -999)
                       OR NVL (lb1.transittime, -999) <>
                             NVL (xlb1.transittime, -999)
                       OR NVL (lb1.weightbreakcharge1, -999) <>
                             NVL (xlb1.weightbreakcharge1, -999)
                       OR NVL (lb1.weightbreakcharge2, -999) <>
                             NVL (xlb1.weightbreakcharge2, -999)
                       OR NVL (lb1.weightbreakcharge3, -999) <>
                             NVL (xlb1.weightbreakcharge3, -999)
                       OR NVL (lb1.weightbreakcharge4, -999) <>
                             NVL (xlb1.weightbreakcharge4, -999)
                       OR NVL (lb1.weightbreakcharge5, -999) <>
                             NVL (xlb1.weightbreakcharge5, -999)
                       OR NVL (lb1.weightbreakcharge6, -999) <>
                             NVL (xlb1.weightbreakcharge6, -999)
                       OR NVL (lb1.weightbreakcharge7, -999) <>
                             NVL (xlb1.weightbreakcharge7, -999)
                       OR NVL (lb1.weightbreakcharge8, -999) <>
                             NVL (xlb1.weightbreakcharge8, -999)
                       OR NVL (lb1.weightbreakcharge9, -999) <>
                             NVL (xlb1.weightbreakcharge9, -999)
                       OR NVL (lb1.ratediscount, -999) <>
                             NVL (xlb1.ratediscount, -999)
                       OR NVL (lb1.rateperdistance, -999) <>
                             NVL (xlb1.rateperdistance, -999)
                       OR NVL (lb1.flatcharge, -999) <>
                             NVL (xlb1.flatcharge, -999)
                       OR NVL (lb1.ratepersize, -999) <>
                             NVL (xlb1.ratepersize, -999)
                       OR NVL (lb1.sailingallocation, -999) <>
                             NVL (xlb1.sailingallocation, -999)
                       OR NVL (lb1.baserate, -999) <>
                             NVL (xlb1.baserate, -999)
                       OR NVL (lb1.chargebasis, -999) <>
                             NVL (xlb1.chargebasis, -999)
                       OR NVL (lb1.comments, ' ') <> NVL (xlb1.comments, ' ')
                       OR NVL (lb1.flightfrequency, -999) <>
                             NVL (xlb1.flightfrequency, -999)
                       OR NVL (lb1.minimumcharge, -999) <>
                             NVL (xlb1.minimumcharge, -999)
                       OR NVL (lb1.sailingfrequency, -999) <>
                             NVL (xlb1.sailingfrequency, -999)
                       OR NVL (lb1.weightcapacity, -999) <>
                             NVL (xlb1.weightcapacity, -999)
                       OR NVL (lb1.custtext1, ' ') <>
                             NVL (xlb1.custtext1, ' ')
                       OR NVL (lb1.custtext2, ' ') <>
                             NVL (xlb1.custtext2, ' ')
                       OR NVL (lb1.custtext3, ' ') <>
                             NVL (xlb1.custtext3, ' ')
                       OR NVL (lb1.custtext4, ' ') <>
                             NVL (xlb1.custtext4, ' ')
                       OR NVL (lb1.custtext5, ' ') <>
                             NVL (xlb1.custtext5, ' ')
                       OR NVL (lb1.custtext6, ' ') <>
                             NVL (xlb1.custtext6, ' ')
                       OR NVL (lb1.custtext7, ' ') <>
                             NVL (xlb1.custtext7, ' ')
                       OR NVL (lb1.custtext8, ' ') <>
                             NVL (xlb1.custtext8, ' ')
                       OR NVL (lb1.custtext9, ' ') <>
                             NVL (xlb1.custtext9, ' ')
                       OR NVL (lb1.custtext10, ' ') <>
                             NVL (xlb1.custtext10, ' ')
                       OR NVL (lb1.custint1, -999) <>
                             NVL (xlb1.custint1, -999)
                       OR NVL (lb1.custint2, -999) <>
                             NVL (xlb1.custint2, -999)
                       OR NVL (lb1.custint3, -999) <>
                             NVL (xlb1.custint3, -999)
                       OR NVL (lb1.custint4, -999) <>
                             NVL (xlb1.custint4, -999)
                       OR NVL (lb1.custint5, -999) <>
                             NVL (xlb1.custint5, -999)
                       OR NVL (lb1.custint6, -999) <>
                             NVL (xlb1.custint6, -999)
                       OR NVL (lb1.custint7, -999) <>
                             NVL (xlb1.custint7, -999)
                       OR NVL (lb1.custint8, -999) <>
                             NVL (xlb1.custint8, -999)
                       OR NVL (lb1.custint9, -999) <>
                             NVL (xlb1.custint9, -999)
                       OR NVL (lb1.custint10, -999) <>
                             NVL (xlb1.custint10, -999)
                       OR NVL (lb1.custdouble1, -999) <>
                             NVL (xlb1.custdouble1, -999)
                       OR NVL (lb1.custdouble2, -999) <>
                             NVL (xlb1.custdouble2, -999)
                       OR NVL (lb1.custdouble3, -999) <>
                             NVL (xlb1.custdouble3, -999)
                       OR NVL (lb1.custdouble4, -999) <>
                             NVL (xlb1.custdouble4, -999)
                       OR NVL (lb1.custdouble5, -999) <>
                             NVL (xlb1.custdouble5, -999)
                       OR NVL (lb1.custdouble6, -999) <>
                             NVL (xlb1.custdouble6, -999)
                       OR NVL (lb1.custdouble7, -999) <>
                             NVL (xlb1.custdouble7, -999)
                       OR NVL (lb1.custdouble8, -999) <>
                             NVL (xlb1.custdouble8, -999)
                       OR NVL (lb1.custdouble9, -999) <>
                             NVL (xlb1.custdouble9, -999)
                       OR NVL (lb1.custdouble10, -999) <>
                             NVL (xlb1.custdouble10, -999)
                       OR lb1.currencycode <> xlb1.currencycode
      WHEN NOT MATCHED
      THEN
         INSERT     (status,
                     laneequipmenttypeid,
                     obcarriercodeid,
                     tpcompanyid,
                     rfpid,
                     laneid,
                     laneauctionstrategy,
                     strategytype,
                     strategybaseline,
                     strategypercentage,
                     lowestbid,
                     transittime,
                     transittimeuom,
                     lanecapacity,
                     surgecapacity,
                     minimumcharge,
                     flatcharge,
                     baserate,
                     inclusiverate,
                     currencycode,
                     weightbreakcharge1,
                     weightbreakcharge2,
                     weightbreakcharge3,
                     weightbreakcharge4,
                     weightbreakcharge5,
                     weightbreakcharge6,
                     weightbreakcharge7,
                     weightbreakcharge8,
                     weightbreakcharge9,
                     weightcapacity,
                     weightuom,
                     rateperuom,
                     flightfrequency,
                     sailingallocation,
                     sailingfrequency,
                     chargebasis,
                     comments,
                     isawarded,
                     awardamount,
                     awardispercentage,
                     createduid,
                     createddttm,
                     lastupdateduid,
                     lastupdateddttm,
                     packageid,
                     custtext1,
                     custtext2,
                     custtext3,
                     custtext4,
                     custtext5,
                     custtext6,
                     custtext7,
                     custtext8,
                     custtext9,
                     custtext10,
                     custint1,
                     custint2,
                     custint3,
                     custint4,
                     custint5,
                     custint6,
                     custint7,
                     custint8,
                     custint9,
                     custint10,
                     custdouble1,
                     custdouble2,
                     custdouble3,
                     custdouble4,
                     custdouble5,
                     custdouble6,
                     custdouble7,
                     custdouble8,
                     custdouble9,
                     custdouble10,
                     protectionlevel_id,
                     equipmenttype_id,
                     servicetype_id,
                     costperload,
                     ratepersize,
                     rateperdistance,
                     ratediscount,
                     round_num,
                     erattributeversion,
                     mot,
                     commoditycode_id,
                     override_id)
             VALUES ('Bid On',
                     xlb1.laneequipmenttypeid,
                     xlb1.obcarriercodeid,
                     xlb1.tpcompanyid,
                     xlb1.rfpid,
                     xlb1.laneid,
                     xlb1.laneauctionstrategy,
                     xlb1.strategytype,
                     xlb1.strategybaseline,
                     xlb1.strategypercentage,
                     xlb1.lowestbid,
                     xlb1.transittime,
                     xlb1.transittimeuom,
                     xlb1.lanecapacity,
                     xlb1.surgecapacity,
                     xlb1.minimumcharge,
                     xlb1.flatcharge,
                     xlb1.baserate,
                     xlb1.inclusiverate,
                     xlb1.currencycode,
                     xlb1.weightbreakcharge1,
                     xlb1.weightbreakcharge2,
                     xlb1.weightbreakcharge3,
                     xlb1.weightbreakcharge4,
                     xlb1.weightbreakcharge5,
                     xlb1.weightbreakcharge6,
                     xlb1.weightbreakcharge7,
                     xlb1.weightbreakcharge8,
                     xlb1.weightbreakcharge9,
                     xlb1.weightcapacity,
                     xlb1.weightuom,
                     xlb1.rateperuom,
                     xlb1.flightfrequency,
                     xlb1.sailingallocation,
                     xlb1.sailingfrequency,
                     xlb1.chargebasis,
                     xlb1.comments,
                     xlb1.isawarded,
                     xlb1.awardamount,
                     xlb1.awardispercentage,
                     xlb1.createduid,
                     xlb1.createddttm,
                     xlb1.lastupdateduid,
                     xlb1.lastupdateddttm,
                     xlb1.packageid,
                     xlb1.custtext1,
                     xlb1.custtext2,
                     xlb1.custtext3,
                     xlb1.custtext4,
                     xlb1.custtext5,
                     xlb1.custtext6,
                     xlb1.custtext7,
                     xlb1.custtext8,
                     xlb1.custtext9,
                     xlb1.custtext10,
                     xlb1.custint1,
                     xlb1.custint2,
                     xlb1.custint3,
                     xlb1.custint4,
                     xlb1.custint5,
                     xlb1.custint6,
                     xlb1.custint7,
                     xlb1.custint8,
                     xlb1.custint9,
                     xlb1.custint10,
                     xlb1.custdouble1,
                     xlb1.custdouble2,
                     xlb1.custdouble3,
                     xlb1.custdouble4,
                     xlb1.custdouble5,
                     xlb1.custdouble6,
                     xlb1.custdouble7,
                     xlb1.custdouble8,
                     xlb1.custdouble9,
                     xlb1.custdouble10,
                     xlb1.protectionlevel_id,
                     xlb1.equipmenttype_id,
                     xlb1.servicetype_id,
                     xlb1.costperload,
                     xlb1.ratepersize,
                     xlb1.rateperdistance,
                     xlb1.ratediscount,
                     xlb1.round_num,
                     xlb1.erattributeversion,
                     xlb1.mot,
                     xlb1.commoditycode_id,
                     override_id_seq.NEXTVAL);

      /*
      INSERT INTO lanebid ( status,laneequipmenttypeid, obcarriercodeid, tpcompanyid, rfpid, laneid,
      laneauctionstrategy, strategytype, strategybaseline, strategypercentage, lowestbid, transittime,
      transittimeuom, lanecapacity, surgecapacity, minimumcharge, flatcharge, baserate, inclusiverate, currencycode,
      weightbreakcharge1, weightbreakcharge2, weightbreakcharge3, weightbreakcharge4, weightbreakcharge5,
      weightbreakcharge6, weightbreakcharge7, weightbreakcharge8, weightbreakcharge9, weightcapacity,
      weightuom, rateperuom, flightfrequency, sailingallocation, sailingfrequency, chargebasis, comments,
      isawarded, awardamount, awardispercentage, createduid, createddttm, lastupdateduid, lastupdateddttm,
      packageid, custtext1, custtext2, custtext3, custtext4, custtext5, custtext6, custtext7, custtext8,
      custtext9, custtext10, custint1, custint2, custint3, custint4, custint5, custint6, custint7,
      custint8, custint9, custint10, custdouble1, custdouble2, custdouble3, custdouble4, custdouble5, custdouble6,
      custdouble7, custdouble8, custdouble9, custdouble10, protectionlevel_id, equipmenttype_id,
      servicetype_id, MOT, commoditycode_id, costperload, ratepersize, rateperdistance, ratediscount, round_num, erattributeversion )
      SELECT 'Bid On' st, leqt.laneequipmenttypeid, xlb.obcarriercodeid, xlb.tpcompanyid, xlb.rfpid, xlb.laneid,
      laneauctionstrategy, strategytype, strategybaseline, strategypercentage, lowestbid, xlb.transittime,
      xlb.transittimeuom, lanecapacity, surgecapacity, minimumcharge, flatcharge, baserate, inclusiverate, NVL(currencycode, 'USD') currencycode,
      weightbreakcharge1, weightbreakcharge2, weightbreakcharge3, weightbreakcharge4, weightbreakcharge5, weightbreakcharge6,
      weightbreakcharge7, weightbreakcharge8, weightbreakcharge9, weightcapacity, xlb.weightuom, rateperuom,
      flightfrequency, sailingallocation, sailingfrequency, chargebasis, comments,
      isawarded, awardamount, awardispercentage, xlb.createduid, xlb.createddttm, xlb.lastupdateduid, xlb.lastupdateddttm,
      packageid,
      xlb.custtext1, xlb.custtext2, xlb.custtext3, xlb.custtext4, xlb.custtext5, xlb.custtext6, xlb.custtext7,
      xlb.custtext8, xlb.custtext9, xlb.custtext10, xlb.custint1, xlb.custint2, xlb.custint3,
      xlb.custint4, xlb.custint5, xlb.custint6, xlb.custint7, xlb.custint8, xlb.custint9, xlb.custint10, xlb.custdouble1,
      xlb.custdouble2, xlb.custdouble3, xlb.custdouble4, xlb.custdouble5, xlb.custdouble6,
      xlb.custdouble7, xlb.custdouble8, xlb.custdouble9, xlb.custdouble10, xlb.protectionlevel_id, xlb.equipmenttype_id,
      xlb.servicetype_id, xlb.MOT, xlb.commoditycode_id,
      --greatest(nvl(minimumcharge,0), nvl(flatcharge,0), nvl(ratepersize,0), nvl(baserate,0),nvl(inclusiverate,0), nvl(rateperdistance*l.mileage,0)) ,
      0, ratepersize, rateperdistance, xlb.ratediscount,
      ROUNDNUM, -1
      FROM xmllanebid xlb , laneequipmenttype leqt    --, lane l
      WHERE xlb.rfpid = I_RFPID
      AND xlb.obcarriercodeid = I_OBCARRIERCODEID AND leqt.rfpid = xlb.rfpid
      --AND l.rfpid = leqt.rfpid AND l.laneid = leqt.laneid
      AND leqt.laneid = xlb.laneid
      AND NVL(leqt.equipmenttype_id, -1) = NVL(xlb.equipmenttype_id, -1)
      AND NVL(leqt.servicetype_id, -1) = NVL(xlb.servicetype_id, -1)
      AND NVL(leqt.MOT, '|||') = NVL(xlb.MOT, '|||')
      AND NVL(leqt.commoditycode_id, -1) = NVL(xlb.commoditycode_id, -1)
      AND NVL(leqt.protectionlevel_id, -1) = NVL(xlb.protectionlevel_id, -1)
      AND xlb.packageid = CASE l_allowpackages WHEN 1 THEN xlb.packageid
      ELSE 0 END
      AND xlb.packageid >= 0 AND xlb.CHANGED = 1
      --AND l.round_num = (SELECT MAX(ml.round_num) FROM lane ml WHERE ml.rfpid = l.rfpid  AND ml.laneid = l.laneid)
      AND NOT EXISTS(SELECT 1
      FROM xmlerror xe
      WHERE xe.rfpid = xlb.rfpid
      AND xe.obcarriercodeid = I_OBCARRIERCODEID
      AND ((xe.laneid = xlb.laneid
      AND xe.packagename IS NULL AND xlb.packagename IS NULL)
      OR (xe.packagename IS NOT NULL
      AND xe.packagename = xlb.packagename)));
      --commit;
      -- for dbRedesign
      --commit;
      */
      IF l_allowpackages = 1
      THEN
         SELECT COUNT (*)
           INTO countbid
           FROM lanebid
          WHERE     rfpid = i_rfpid
                AND obcarriercodeid = i_obcarriercodeid
                AND round_num = roundnum
                AND packageid <> 0;

         IF countbid > 0
         THEN
            UPDATE rfpresponse
               SET ispkginfosave = 1
             WHERE     rfpid = i_rfpid
                   AND obcarriercodeid = i_obcarriercodeid
                   AND round_num = roundnum;
         ELSE
            UPDATE rfpresponse
               SET ispkginfosave = 0
             WHERE     rfpid = i_rfpid
                   AND obcarriercodeid = i_obcarriercodeid
                   AND round_num = roundnum;
         END IF;
      END IF;

      SELECT modeid
        INTO v_modeid
        FROM rfp
       WHERE rfpid = i_rfpid AND round_num = roundnum;

      INSERT INTO lanebidfreightcharge (obcarriercodeid,
                                        laneequipmenttypeid,
                                        freightchargetypeid,
                                        modeid,
                                        tpcompanyid,
                                        freightchargebasisid,
                                        freightcharge,
                                        description,
                                        packageid,
                                        currencycode,
                                        round_num)
         SELECT xlbfc.obcarriercodeid,
                leqt.laneequipmenttypeid,
                xlbfc.freightchargetypeid,
                v_modeid,
                xlb.tpcompanyid,
                xlbfc.freightchargebasisid,
                xlbfc.freightcharge,
                xlbfc.description,
                xlb.packageid,
                xlbfc.currencycode,
                roundnum
           FROM xmllanebidfreightcharge xlbfc,
                lanebid xlb,
                laneequipmenttype leqt
          WHERE     xlbfc.rfpid = i_rfpid
                AND xlbfc.obcarriercodeid = i_obcarriercodeid
                AND xlb.laneequipmenttypeid = xlbfc.laneequipmenttypeid
                AND xlb.obcarriercodeid = xlbfc.obcarriercodeid
                AND xlb.rfpid = xlbfc.rfpid
                AND xlb.round_num = roundnum
                --AND nvl(xlb.packagename, '|||') = nvl(xlbfc.packagename, '|||')
                AND NVL (xlb.packageid, -100) = NVL (xlbfc.packageid, -100)
                AND leqt.rfpid = xlb.rfpid
                AND leqt.laneid = xlb.laneid
                AND NVL (leqt.equipmenttype_id, -1) =
                       NVL (xlb.equipmenttype_id, -1)
                AND NVL (leqt.servicetype_id, -1) =
                       NVL (xlb.servicetype_id, -1)
                AND NVL (leqt.mot, '|||') = NVL (xlb.mot, '|||')
                AND NVL (leqt.protectionlevel_id, -1) =
                       NVL (xlb.protectionlevel_id, -1)
                AND NVL (leqt.commoditycode_id, -1) =
                       NVL (xlb.commoditycode_id, -1)
                AND xlbfc.packageid =
                       CASE l_allowpackages
                          WHEN 1 THEN xlbfc.packageid
                          ELSE 0
                       END
                AND xlbfc.packageid >= 0;

      --commit;
      inclusiveraterecal (i_rfpid,
                          i_obcarriercodeid,
                          0,
                          i_username);
      inclusiveraterecal (i_rfpid,
                          i_obcarriercodeid,
                          1,
                          i_username);

      IF (i_status = 5)
      THEN
         INSERT INTO track (proc_name, proc_ctime, proc_comment)
              VALUES ('1', 1, '2');

         populate_sa_entries (i_rfpid, i_obcarriercodeid);
      END IF;

      recal_carrier (i_rfpid, i_obcarriercodeid);
   --commit;
   -- lanebidmin table is no more used
   --EXECUTE PROCEDURE DELETE_BID_CLEAN(i_rfpid,i_obcarriercodeid);
   --COMMIT;
   END IF;
END;
/
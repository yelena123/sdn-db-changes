CREATE OR REPLACE PROCEDURE WM_GEN_LPN_NBRS (
   p_user_id           IN     VARCHAR2,
   p_whse              IN     VARCHAR2,
   p_company_id        IN     NUMBER,
   P_FACILITY_ID       IN     NXT_UP_CNT.FACILITY_ID%TYPE,
   P_RECTYPEID         IN     NXT_UP_CNT.REC_TYPE_ID%TYPE,
   p_lpn_count         IN     NUMBER,
   p_pfx_field            OUT nxt_up_cnt.pfx_field%TYPE,
   p_start_tc_lpn_id      OUT NUMBER,
   p_nxt_up_nbr_len       OUT nxt_up_cnt.nbr_len%TYPE)
AS
   v_dummy_whse             nxt_up_cnt.whse%TYPE;
   v_dummy_rec_type_id      nxt_up_cnt.rec_type_id%TYPE;
   v_dummy_pfx_len          nxt_up_cnt.pfx_len%TYPE;
   v_dummy_start_nbr        nxt_up_cnt.start_nbr%TYPE;
   v_dummy_end_nbr          nxt_up_cnt.end_nbr%TYPE;
   v_dummy_incr_value       nxt_up_cnt.incr_value%TYPE;
   v_dummy_nxt_start_nbr    nxt_up_cnt.nxt_start_nbr%TYPE;
   v_dummy_nxt_end_nbr      nxt_up_cnt.nxt_end_nbr%TYPE;
   v_dummy_chk_digit_type   nxt_up_cnt.chk_digit_type%TYPE;
   v_dummy_chk_digit_len    nxt_up_cnt.chk_digit_len%TYPE;
   v_dummy_repeat_range     nxt_up_cnt.repeat_range%TYPE;
   v_dummy_cd_master_id     nxt_up_cnt.cd_master_id%TYPE;
   v_dummy_nxt_up_cnt_id    nxt_up_cnt.nxt_up_cnt_id%TYPE;
   v_dummy_pfx_type         nxt_up_cnt.pfx_type%TYPE;
   v_dummy_pfx_order        nxt_up_cnt.rev_order%TYPE;
   v_dummy_rc               NUMBER (1) := 0;

   
BEGIN
   wm_auton_get_nxt_up_cnt_id (P_FACILITY_ID,
                           CAST( p_company_id AS VARCHAR2),
                           P_RECTYPEID,
                           0,
                           p_user_id,
                           p_lpn_count,
                           v_dummy_whse,
                           v_dummy_rec_type_id,
                           p_pfx_field,
                           v_dummy_pfx_len,
                           v_dummy_start_nbr,
                           v_dummy_end_nbr,
                           p_start_tc_lpn_id,
                           p_nxt_up_nbr_len,
                           v_dummy_incr_value,
                           v_dummy_nxt_start_nbr,
                           v_dummy_nxt_end_nbr,
                           v_dummy_chk_digit_type,
                           v_dummy_chk_digit_len,
                           v_dummy_repeat_range,
                           v_dummy_cd_master_id,
                           v_dummy_nxt_up_cnt_id,
                           v_dummy_pfx_type,
                           v_dummy_pfx_order,
                           v_dummy_rc);

   -- TODO: Handle UCC
   COMMIT;
END;
/

SHOW ERRORS;

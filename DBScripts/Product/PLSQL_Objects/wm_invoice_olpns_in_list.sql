create or replace procedure wm_invoice_olpns_in_list
(
    p_invc_batch_nbr    in number,
    p_user_id           in user_profile.user_id%type,
    p_whse              in facility.whse%type,
    p_facility_id       in facility.facility_id%type,
    p_tc_company_id     in facility.tc_company_id%type,
    p_pre_bill_flag     in number,
    p_genrt_carton_asn  in number,
    p_srl_trk_flag      in number
)
as
    type t_aid_id is table of alloc_invn_dtl.alloc_invn_dtl_id%type index by binary_integer;
    va_aid_id              t_aid_id;
    v_plt_remov_enabled    number(1);
begin
    select case when exists
    (
        select 1
        from company_parameter cp
        where cp.tc_company_id = p_tc_company_id
            and upper(cp.param_def_id) = 'PALLET_REUSE'
            and upper(cp.param_value) = 'TRUE'
    ) then 1 else 0 end
    into v_plt_remov_enabled
    from dual;
	
    if (p_pre_bill_flag = 2)
    then
        -- quit after lpn level pre bills are generated
        wm_gen_lpn_asn_data(p_invc_batch_nbr, p_user_id, p_tc_company_id, p_srl_trk_flag);
        return;
    elsif (p_pre_bill_flag = 1)
    then
        return;
    end if;

    -- cancel pending anchor carton tasks
    update task_dtl td
    set td.stat_code = 99, td.mod_date_time = sysdate, td.user_id = p_user_id
    where td.cd_master_id = p_tc_company_id and td.invn_need_type in (70, 81, 82)
        and td.stat_code < 90
        and exists
        (
            select 1
            from tmp_invc_lpn_list t
            where t.tc_lpn_id = td.carton_nbr
        )
        and exists
        (
            select 1
            from task_hdr th
            where th.task_hdr_id = td.task_hdr_id and th.whse = p_whse
        )
    returning td.alloc_invn_dtl_id
    bulk collect into va_aid_id;
    wm_cs_log('Anchor carton task dtls cancelled ' || sql%rowcount);

    forall i in 1..va_aid_id.count
    update alloc_invn_dtl aid
    set aid.stat_code = 99, aid.mod_date_time = sysdate, aid.user_id = p_user_id
    where aid.alloc_invn_dtl_id = va_aid_id(i);
    wm_cs_log('Anchor carton aid''s cancelled ' || sql%rowcount);    

    forall i in 1..va_aid_id.count
    update task_hdr th
    set th.mod_date_time = sysdate, th.user_id = p_user_id,
        th.stat_code = 
        (
            select min(td.stat_code)
            from task_dtl td
            where td.task_id = th.task_id
        )
    where exists
    (
        select 1
        from task_dtl td
        where td.task_id = th.task_id and td.alloc_invn_dtl_id = va_aid_id(i)
            and td.cd_master_id = p_tc_company_id and td.invn_need_type in (70, 81, 82)
    );
    wm_cs_log('Anchor carton tasks removed ' || sql%rowcount);   

    -- refactor dtls of lpn's corresponding to agg orders
    wm_invc_split_lpn_dtls_for_rtl(p_facility_id, p_tc_company_id, p_user_id,
        p_srl_trk_flag);

    merge into lpn_detail ld
    using
    (
        select ld.lpn_id, ld.lpn_detail_id,
            case when oli.is_chase_created_line = 0 
                 then coalesce(prnt.tc_order_line_id, oli.tc_order_line_id)  
                 else coalesce(prnt1.tc_order_line_id, prnt.tc_order_line_id)
                 end tc_order_line_id
        from lpn_detail ld
        join tmp_invc_lpn_list t on t.lpn_id = ld.lpn_id
        join order_line_item oli on oli.line_item_id = ld.distribution_order_dtl_id
        left join order_line_item prnt on prnt.order_id = oli.order_id
            and prnt.line_item_id = oli.parent_line_item_id
        left join order_line_item prnt1 on prnt1.order_id = prnt.order_id
            and prnt1.line_item_id = prnt.parent_line_item_id     
    ) iv on (iv.lpn_id = ld.lpn_id and iv.lpn_detail_id = ld.lpn_detail_id)
    when matched then
    update set ld.lpn_detail_status = 90, ld.shipped_qty = ld.size_value, 
        ld.last_updated_source = p_user_id, ld.last_updated_dttm = sysdate,
        ld.tc_order_line_id = iv.tc_order_line_id
    log errors (to_char(sysdate));
    wm_cs_log('Lpn details updated to shipped ' || sql%rowcount);    

    -- unlocate lpns tied to staging locns
    -- curr_uom_qty � reduce by the lpn.total_lpn_qty if zone uom = units, by 1 
    -- if uom is C, do nothing if uom is P
    merge into resv_locn_hdr rlh
    using
    (
        select l.curr_sub_locn_id, min(l.total_lpn_qty) qty,
            count(l.lpn_id) num_lpns_per_locn,
            coalesce(sum(coalesce(nullif(l.weight, 0), l.estimated_weight)), 0) wt, 
            coalesce(sum(coalesce(nullif(l.actual_volume, 0), l.estimated_volume)), 0) vol, 
            min(coalesce(trim(substr(sc.misc_flags, 1, 1)), 'C')) uom
        from tmp_invc_lpn_list t
        join lpn l on l.lpn_id = t.lpn_id
        join locn_hdr lh on l.curr_sub_locn_id = lh.locn_id
        join sys_code sc on sc.code_id = lh.putwy_zone
        where sc.rec_type = 'B' and sc.code_type = '599' and lh.locn_class = 'S'
        group by l.curr_sub_locn_id
    ) iv on (iv.curr_sub_locn_id = rlh.locn_id)
    when matched then
    update set rlh.user_id = p_user_id, rlh.mod_date_time = sysdate,
        rlh.curr_wt = rlh.curr_wt - iv.wt, rlh.curr_vol = rlh.curr_vol - iv.vol,
        rlh.curr_uom_qty = (case iv.uom when 'P' then rlh.curr_uom_qty
            when 'C' then rlh.curr_uom_qty - num_lpns_per_locn
            else rlh.curr_uom_qty - iv.qty end)
    log errors (to_char(sysdate));
    wm_cs_log('Reserve locns unlocated from ' || sql%rowcount);  

    -- unlocate lpns tied to order consolidation locns
    merge into pkt_consol_locn pcl
    using
    (
        select pcl.locn_id,
            count(distinct decode(pcl.locn_id, l.curr_sub_locn_id, l.lpn_id, null)) curr_num_lpns_being_shpd,
            count(distinct decode(pcl.locn_id, l.curr_sub_locn_id, null, l.lpn_id)) dirct_num_lpns_being_shpd,
            coalesce(sum(decode(pcl.locn_id, l.curr_sub_locn_id, coalesce(nullif(l.weight, 0), l.estimated_weight, 0), 0)), 0) curr_wt,
            coalesce(sum(decode(pcl.locn_id, l.curr_sub_locn_id, 0, coalesce(nullif(l.weight, 0), l.estimated_weight, 0))), 0) dirct_wt,
            coalesce(sum(decode(pcl.locn_id, l.curr_sub_locn_id, coalesce(nullif(l.actual_volume, 0), l.estimated_volume), 0)), 0) curr_vol,
            coalesce(sum(decode(pcl.locn_id, l.curr_sub_locn_id, 0, coalesce(nullif(l.actual_volume, 0), l.estimated_volume))), 0) dirct_vol
        from tmp_invc_lpn_list t
        join lpn l on l.lpn_id = t.lpn_id
        join pkt_consol_locn pcl on pcl.locn_id in (l.curr_sub_locn_id, l.dest_sub_locn_id)
            and pcl.rec_type = 'P' and pcl.wave_nbr = '0'
        group by pcl.locn_id
    ) iv on (iv.locn_id = pcl.locn_id and pcl.rec_type = 'W' and pcl.wave_nbr = '0')
    when matched then
    update set pcl.user_id = p_user_id, pcl.mod_date_time = sysdate,
        pcl.curr_wt = pcl.curr_wt - iv.curr_wt,
        pcl.dirct_wt = pcl.dirct_wt - iv.dirct_wt,
        pcl.curr_vol = pcl.curr_vol - iv.curr_vol,
        pcl.dirct_vol = pcl.dirct_vol - iv.dirct_vol,
        pcl.curr_uom_qty = pcl.curr_uom_qty - curr_num_lpns_being_shpd,
        pcl.dirct_uom_qty = pcl.dirct_uom_qty - dirct_num_lpns_being_shpd,
        pcl.locn_stat_code =
        (
            case
            when (pcl.curr_uom_qty - iv.curr_num_lpns_being_shpd
                + pcl.dirct_uom_qty - iv.dirct_num_lpns_being_shpd > 0)
                or (pcl.curr_vol - iv.curr_vol + pcl.dirct_vol - iv.dirct_vol > 0)
                or (pcl.curr_wt - iv.curr_wt + pcl.dirct_wt - iv.dirct_wt > 0) then 10
            else 0 
            end
        )
    delete where (pcl.curr_wt + pcl.dirct_wt <= 0) and (pcl.curr_vol + pcl.dirct_vol <= 0)
        and (pcl.curr_uom_qty + pcl.dirct_uom_qty <= 0)
    log errors (to_char(sysdate));
    wm_cs_log('Pkt consol locns unlocated from ' || sql%rowcount);  

    merge into lpn l
    using
    (
        select l.lpn_id, s.bill_of_lading_number master_bol_nbr,
            coalesce(l.lpn_size_type_id,
            (
                select min(lst.lpn_size_type_id) keep(dense_rank first order by (ld.lpn_detail_id))
                from item_facility_mapping_wms ifmw
                join lpn_size_type lst on lst.lpn_size_type = ifmw.case_size_type
                join lpn_detail ld on ld.item_id = ifmw.item_id
                where ld.lpn_id = l.lpn_id
            )) lpn_size_type_id,
            decode(s.static_route_id, null, 
            (
                select st.bill_of_lading_number
                from stop st
                join stop_action_order sao on sao.shipment_id = st.shipment_id
                    and sao.stop_seq = st.stop_seq
                where st.shipment_id = s.shipment_id and st.stop_seq > 1
                    and sao.order_id = l.order_id
                    and coalesce(sao.order_split_id, -1) = coalesce(l.order_split_id, -1)
            ),
            (
                select st.bill_of_lading_number
                from stop st
                join orders o on o.d_facility_id = st.facility_id
                where st.shipment_id = s.shipment_id and st.stop_seq > 1
                    and o.order_id = l.order_id
            )) bol_nbr
        from tmp_invc_lpn_list t
        join lpn l on l.lpn_id = t.lpn_id
        left join shipment s on s.shipment_id = l.shipment_id
    ) iv on (iv.lpn_id = l.lpn_id)
    when matched then
    update set l.lpn_facility_status = 90, l.curr_sub_locn_id = null,
        l.prev_sub_locn_id = (case when l.lpn_facility_status < 50 
            then l.curr_sub_locn_id else l.prev_sub_locn_id end),
        l.stage_indicator = decode(l.stage_indicator, 30, 40, 10, 15, 0),
        l.dest_sub_locn_id = null, l.wave_stat_code = 90, l.lpn_status = 35,
        l.shipped_dttm = sysdate, l.lpn_size_type_id = iv.lpn_size_type_id,
        l.last_updated_dttm = sysdate, l.last_updated_source = p_user_id, 
        l.tc_parent_lpn_id = decode(v_plt_remov_enabled, 1, null ,l.tc_parent_lpn_id),
        l.bol_nbr = coalesce(l.bol_nbr, iv.bol_nbr), l.master_bol_nbr = iv.master_bol_nbr, 
		l.parent_lpn_id = decode(v_plt_remov_enabled, 1, null ,l.parent_lpn_id),
        l.lpn_status_updated_dttm = sysdate, l.lpn_facility_stat_updated_dttm = sysdate
    log errors (to_char(sysdate));
    wm_cs_log('Lpns invoiced ' || sql%rowcount); 

    if (p_genrt_carton_asn = 1)
    then
        wm_gen_lpn_asn_data(p_invc_batch_nbr, p_user_id, p_tc_company_id, p_srl_trk_flag);
    end if;

    if (v_plt_remov_enabled = 1)
    then
        delete from lpn_movement lm 
        where exists
        (
             select 1
             from tmp_invc_lpn_list t
             join lpn pllt on pllt.tc_lpn_id = t.tc_parent_lpn_id and pllt.tc_company_id = p_tc_company_id
                 and pllt.c_facility_id = p_facility_id and pllt.inbound_outbound_indicator = 'O'
             where t.tc_parent_lpn_id = lm.tc_lpn_id
        )
        and not exists
       (
            select 1
            from lpn l
            where l.parent_lpn_id = lm.lpn_id and l.lpn_facility_status < 90
        ); 
    
        delete from lpn plt
        where plt.inbound_outbound_indicator = 'O'
            and exists
            (
                select 1
                from tmp_invc_lpn_list t
                where t.tc_parent_lpn_id = plt.tc_lpn_id
            )
            and not exists
            (
                select 1
                from lpn l
                where l.parent_lpn_id = plt.lpn_id and l.lpn_facility_status < 90
            );
        wm_cs_log('Pallets removed ' || sql%rowcount);
    end if;
end;
/
show errors;

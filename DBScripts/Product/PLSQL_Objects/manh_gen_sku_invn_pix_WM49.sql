CREATE OR REPLACE
PACKAGE body manh_gen_sku_invn_pix
IS
  g_warehouse facility.whse%TYPE ;
  g_facility_id item_facility_mapping_wms.facility_id%TYPE;
  g_lang_id user_master.lang_id%TYPE;
  g_user_id user_master.login_user_id%TYPE;
  g_erphosttype vw_whse_master.erp_host_type%TYPE;
  g_tran_nbr pix_tran.tran_nbr%TYPE;
  g_count PLS_INTEGER         := 0;
  g_skuattr_count PLS_INTEGER := 0;
  g_company_id COMPANY.COMPANY_ID%TYPE;
  g_company_code COMPANY.COMPANY_CODE%TYPE;
  g_mod_date_time DATE;
  g_lock_time_out PLS_INTEGER;
  g_debug                   BOOLEAN        := FALSE;
  g_srlnbrpix_flag          BOOLEAN        := FALSE;
  g_transinvn_flag          BOOLEAN        := FALSE;
  g_zero_invn_flag          BOOLEAN        := FALSE;
  g_group_skuattr_flag      BOOLEAN        := FALSE;
  g_unalloc_invn_flag       BOOLEAN        := FALSE;
  g_consprtydate_flag       BOOLEAN        := FALSE;
  g_caselock_flag           BOOLEAN        := FALSE;
  g_reconcile_flag          BOOLEAN        := FALSE;
  g_apply_variance_flag     INT            := 0;
  g_active_locn_lock_flag   BOOLEAN        := false;
  g_casepick_locn_lock_flag BOOLEAN        := false;
  g_pix_level               VARCHAR2 (1)   := 'D';
  g_sku_attributes          VARCHAR2 (200) := ', alias.invn_type, alias.prod_stat, alias.batch_nbr, alias.sku_attr_1, alias.sku_attr_2, alias.sku_attr_3, alias.sku_attr_4, alias.sku_attr_5, alias.cntry_of_orgn ';
FUNCTION get_lang_id(
    p_login_user_id IN VARCHAR2)
  RETURN VARCHAR2
IS
  v_langid VARCHAR2 (3);
BEGIN
  SELECT LANGUAGE.language_suffix
  INTO v_langid
  FROM ucl_user
  INNER JOIN locale
  ON ucl_user.locale_id = locale.locale_id
  INNER JOIN LANGUAGE
  ON locale.language_id    = LANGUAGE.language_id
  WHERE ucl_user.user_name = p_login_user_id;
  RETURN COALESCE (v_langid, 'ENG');
EXCEPTION
WHEN NO_DATA_FOUND THEN
  raise_application_error (-20201, 'The passed in USER_NAME is not found in UCL_USER and LOCALE ' );
WHEN OTHERS THEN
  raise;
END get_lang_id;
FUNCTION get_srl_track_flag
  RETURN BOOLEAN
IS
  v_srl_track_flag NUMBER(1);
BEGIN
  SELECT COALESCE (wp.srl_trk_flag, 0)
  INTO v_srl_track_flag
  FROM whse_parameters wp
  WHERE wp.whse_master_id = g_facility_id;
  RETURN
  (
    CASE
    WHEN v_srl_track_flag = 1 THEN
      TRUE
    ELSE
      FALSE
    END);
EXCEPTION
WHEN NO_DATA_FOUND THEN
  RETURN FALSE;
WHEN OTHERS THEN
  raise;
END get_srl_track_flag;
FUNCTION is_unprocessed_data_present
  RETURN BOOLEAN
IS
  PRAGMA AUTONOMOUS_TRANSACTION;
  v_count PLS_INTEGER := 0;
  v_manh_bridge_summary_id manh_bridge_summary.Manh_Bridge_Summary_id%Type;
BEGIN
  BEGIN
    v_count := 0;
    SELECT err_rec_cnt
    INTO v_count
    FROM manh_bridge_summary
    WHERE bridge_name = 'INVENTORY RECONCILIATION';
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    SELECT manh_bridge_summary_id_seq.nextval
    INTO v_manh_bridge_summary_id
    FROM dual;
    INSERT
    INTO manh_bridge_summary
      (
        bridge_name,
        bridge_process_date,
        err_rec_cnt,
        create_date_time,
        mod_date_time,
        user_id,
        Manh_Bridge_Summary_id,
        BRIDGE_SUMM_ID
      )
      VALUES
      (
        'INVENTORY RECONCILIATION',
        SYSDATE,
        1,
        SYSDATE,
        SYSDATE,
        g_user_id,
        v_manh_bridge_summary_id,
        v_manh_bridge_summary_id
      )
      log errors
      (
        TO_CHAR(sysdate)
      );
  END;
  IF v_count = 1 THEN
    RETURN TRUE;
  ELSE
    UPDATE manh_bridge_summary
    SET err_rec_cnt   = 1,
      mod_date_time   = SYSDATE
    WHERE bridge_name = 'INVENTORY RECONCILIATION' log errors (TO_CHAR(sysdate));
  END IF;
  COMMIT;
  RETURN FALSE;
END is_unprocessed_data_present;
PROCEDURE LOG(
    p_text      IN VARCHAR2,
    p_printdate IN BOOLEAN DEFAULT true,
    p_tab       IN PLS_INTEGER DEFAULT 0 )
AS
  v_datetext VARCHAR2 (50) := TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS - ');
BEGIN
  IF NOT g_debug THEN
    RETURN;
  END IF;
  DBMS_OUTPUT.put_line
  (
    (
      CASE
      WHEN p_printdate THEN
        v_datetext
      ELSE
        ''
      END) ||
    (
      CASE
      WHEN p_tab = 0 THEN
        ''
      ELSE
        RPAD ('+', (4 * p_tab), ' ')
      END ) || p_text );
END LOG;
PROCEDURE GET_USER_INFO(
    p_LoginUserId IN VARCHAR2,
    p_facility_id OUT NUMBER,
    p_Company_id OUT NUMBER,
    p_LangId OUT VARCHAR2,
    p_Whse OUT VARCHAR2 )
IS
BEGIN
  p_facility_id := NULL;
  p_company_id  := NULL;
  p_LangId      := NULL;
  SELECT FACILITY.FACILITY_ID,
    COMPANY.COMPANY_ID,
    FACILITY.WHSE
  INTO p_facility_id,
    p_company_id,
    p_Whse
  FROM UCL_USER
  INNER JOIN COMPANY
  ON UCL_USER.DEFAULT_BUSINESS_UNIT_ID = COMPANY.COMPANY_ID
  INNER JOIN FACILITY
  ON (UCL_USER.DEFAULT_WHSE_REGION_ID = FACILITY.WHSE_REGION)
  WHERE UCL_USER.USER_NAME            = p_LoginUserId;
  p_LangId                           := GET_LANG_ID ( p_LoginUserId ) ;
EXCEPTION
WHEN NO_DATA_FOUND THEN
  raise_application_error (-20209, 'User Information not found.  Please check whether passed in Login_User_Id is Valid.' );
END get_user_info;
PROCEDURE set_erp_host_type
AS
BEGIN
  g_erphosttype := '0';
  /*
  SELECT (CASE
  WHEN COALESCE (wp.erp_host_type, '0') <> '1'
  THEN '0'
  ELSE '1'
  END)
  INTO g_erphosttype
  FROM whse_parameters wp
  WHERE wp.whse_master_id = g_facility_id;
  */
EXCEPTION
WHEN NO_DATA_FOUND THEN
  g_erphosttype := '0';
END set_erp_host_type;
PROCEDURE update_unprocessed_605_pix
AS
BEGIN
  LOG ('Started Update of un-processed 605 PIX.', TRUE);
  UPDATE pix_tran pt
  SET pt.proc_stat_code  = 90,
    pt.mod_date_time     = g_mod_date_time,
    pt.user_id           = g_user_id
  WHERE pt.tran_type    IN ('605')
  AND pt.proc_stat_code IN (0, 10)
  AND pt.facility_id     = g_facility_id
  AND pt.tc_company_id   = g_company_id log errors (TO_CHAR(sysdate));
  UPDATE pix_tran pt
  SET pt.proc_stat_code  = 90,
    pt.mod_date_time     = g_mod_date_time,
    pt.user_id           = g_user_id
  WHERE pt.tran_type    IN ('608')
  AND pt.tran_code      IN ('00','13')
  AND pt.proc_stat_code IN (0, 10)
  AND pt.facility_id     = g_facility_id
  AND pt.tc_company_id   = g_company_id log errors (TO_CHAR(sysdate));
END update_unprocessed_605_pix;
PROCEDURE exception_msg_log_insert(
    p_msg msg_log.msg%TYPE DEFAULT NULL,
    p_module msg_log.module%TYPE DEFAULT 'HOST',
    p_msg_id msg_log.msg_id%TYPE DEFAULT '1190' )
AS
  PRAGMA AUTONOMOUS_TRANSACTION;
  v_errmsg msg_log.msg%TYPE := get_msg_info (p_module, p_msg_id, g_lang_id);
BEGIN
  v_errmsg := REPLACE (NVL (v_errmsg, '{0}'), '{0}', p_msg);
  INSERT
  INTO msg_log
    (
      msg_log_id,
      module,
      msg_id,
      pgm_id,
      msg,
      create_date_time,
      user_id,
      cd_master_id,
      log_date_time,
      mod_date_time,
      whse
    )
    VALUES
    (
      msg_log_id_seq.nextval,
      p_module,
      p_msg_id,
      'Generate SKU Inventory',
      v_errmsg,
      SYSDATE,
      g_user_id,
      g_company_id,
      SYSDATE,
      SYSDATE,
      g_warehouse
    )
    log errors
    (
      TO_CHAR(sysdate)
    );
  COMMIT;
END exception_msg_log_insert;
PROCEDURE update_manh_bridge_summary
AS
BEGIN
  UPDATE manh_bridge_summary
  SET err_rec_cnt   = 0,
    mod_date_time   = SYSDATE
  WHERE bridge_name = 'INVENTORY RECONCILIATION'
  AND NOT EXISTS
    (SELECT 1
    FROM pix_summ
    WHERE pix_summ.facility_id = g_facility_id
    AND pix_summ.stat_code    IN (0, 20, 30)
    ) log errors (TO_CHAR(sysdate));
END update_manh_bridge_summary;
PROCEDURE snap_item_master(
    p_season         IN VARCHAR2 DEFAULT NULL,
    p_season_yr      IN VARCHAR2 DEFAULT NULL,
    p_style          IN VARCHAR2 DEFAULT NULL,
    p_style_sfx      IN VARCHAR2 DEFAULT NULL,
    p_color          IN VARCHAR2 DEFAULT NULL,
    p_color_sfx      IN VARCHAR2 DEFAULT NULL,
    p_sec_dim        IN VARCHAR2 DEFAULT NULL,
    p_qual           IN VARCHAR2 DEFAULT NULL,
    p_size_desc      IN VARCHAR2 DEFAULT NULL,
    p_pix_summ_check IN BOOLEAN DEFAULT FALSE,
    p_item_name      IN VARCHAR2 DEFAULT NULL )
AS
  v_insert VARCHAR2 (5000);
  v_count PLS_INTEGER := 0;
  exp_sku_not_found EXCEPTION;
BEGIN
  log('Started snapshot of ITEM_MASTER', TRUE, 1);
  v_insert := 'INSERT INTO gen605_sku_gtt(item_name, item_id,season,season_yr,style,style_sfx,color,color_sfx,sec_dim,qual,size_desc,srl_nbr_reqd, unit_weight, item_avg_wt ) ' || 'SELECT ic.item_name, ic.item_id, ic.item_season, ic.item_season_year, ic.item_style, ic.item_style_sfx, ic.item_color, ic.item_color_sfx, ic.item_second_dim, ic.item_quality, ic.item_size_desc , iw.srl_nbr_reqd, coalesce(unit_weight, 0), item_avg_wt ' || 'FROM item_cbo ic left join item_wms iw on iw.item_id = ic.item_id left join item_facility_mapping_wms ifmc on ifmc.item_id = ic.item_id ' || 'WHERE company_id = :1 and ifmc.facility_id = :2 and ic.mark_for_deletion = 0 and coalesce(iw.mark_for_deletion, 0) = 0' ||
  (
    CASE
    WHEN p_item_name IS NOT NULL THEN
      ' AND COALESCE(ic.item_name, '' '') = ''' || p_item_name || ''''
    ELSE
      ''
    END ) ||
  (
    CASE
    WHEN p_season IS NOT NULL THEN
      ' AND COALESCE(ic.item_season, '' '') = ''' || p_season || ''''
    ELSE
      ''
    END ) ||
  (
    CASE
    WHEN p_season_yr IS NOT NULL THEN
      ' AND COALESCE(ic.item_season_year, '' '') = ''' || p_season_yr || ''''
    ELSE
      ''
    END ) ||
  (
    CASE
    WHEN p_style IS NOT NULL THEN
      ' AND COALESCE(ic.item_style, '' '') = ''' || p_style || ''''
    ELSE
      ''
    END ) ||
  (
    CASE
    WHEN p_style_sfx IS NOT NULL THEN
      ' AND COALESCE(ic.item_style_sfx, '' '') = ''' || p_style_sfx || ''''
    ELSE
      ''
    END ) ||
  (
    CASE
    WHEN p_color IS NOT NULL THEN
      ' AND COALESCE(ic.item_color, '' '') = ''' || p_color || ''''
    ELSE
      ''
    END ) ||
  (
    CASE
    WHEN p_color_sfx IS NOT NULL THEN
      ' AND COALESCE(ic.item_color_sfx, '' '') = ''' || p_color_sfx || ''''
    ELSE
      ''
    END ) ||
  (
    CASE
    WHEN p_sec_dim IS NOT NULL THEN
      ' AND COALESCE(ic.item_second_dim, '' '') = ''' || p_sec_dim || ''''
    ELSE
      ''
    END ) ||
  (
    CASE
    WHEN p_qual IS NOT NULL THEN
      ' AND COALESCE(ic.item_quality, '' '') = ''' || p_qual || ''''
    ELSE
      ''
    END ) ||
  (
    CASE
    WHEN p_size_desc IS NOT NULL THEN
      ' AND COALESCE(ic.item_size_desc, '' '') = ''' || p_size_desc || ''''
    ELSE
      ''
    END );
  IF p_pix_summ_check THEN
    v_insert := v_insert || ' AND EXISTS (SELECT 1 FROM Pix_Summ ps WHERE ps.item_id = ic.item_id AND ps.stat_code = 30) ';
  END IF;
  v_insert := v_insert || ' log errors (to_char(sysdate))';
  -- Get all the item_id that match the passed in sku components
  EXECUTE IMMEDIATE v_insert USING g_company_id,
  g_facility_id;
  v_count    := SQL%ROWCOUNT;
  IF v_count <= 0 THEN
    RAISE exp_sku_not_found;
  END IF;
  log('Finished snapshot of ITEM_MASTER. Records count = ' || v_Count, TRUE, 1);
EXCEPTION
WHEN exp_sku_not_found THEN
  raise_application_error (-20121, 'No Items found in item_cbo,item_facility_mapping_wms tables for the passed in Item Component(s).  Please check the input Item Components' );
WHEN OTHERS THEN
  raise;
END snap_item_master;
PROCEDURE snap_sku_attributes(
    p_invn_type IN VARCHAR2 DEFAULT '*',
    p_prod_stat IN VARCHAR2 DEFAULT NULL )
AS
  v_insert              VARCHAR2 (5000);
  exp_skuattr_not_found EXCEPTION;
BEGIN
  log('Started snapshot of SKU Attributes', TRUE, 1);
  v_insert := 'INSERT INTO gen605_sku_attr_gtt(item_id, invn_type, prod_stat, sku_attr_1, sku_attr_2, sku_attr_3, sku_attr_4, sku_attr_5, cntry_of_orgn, batch_nbr, sku_attr_id) ' || 'SELECT a.item_id, a.inventory_type, a.product_status, a.item_attr_1, a.item_attr_2, a.item_attr_3, a.item_attr_4, a.item_attr_5, a.cntry_of_orgn, a.batch_nbr, ROW_NUMBER() OVER(ORDER BY a.item_id) ' || 'FROM (SELECT DISTINCT si.item_id, si.inventory_type, si.product_status, si.item_attr_1, si.item_attr_2, si.item_attr_3, si.item_attr_4, si.item_attr_5, si.cntry_of_orgn, si.batch_nbr ' || 'FROM  gen605_sku_gtt sku, wm_inventory as of timestamp :start_605_time si where sku.item_id = si.item_id and si.c_facility_id = :1 ' ||
  (
    CASE
    WHEN p_Invn_Type <> '*' THEN
      'AND si.inventory_type = ''' || p_Invn_Type || ''' '
    ELSE
      ''
    END) ||
  (
    CASE
    WHEN p_Prod_Stat IS NOT NULL THEN
      'AND si.product_status = ''' || p_Prod_Stat || ''' '
    ELSE
      ''
    END) || ') a';
  v_insert := v_insert ||''|| ' log errors (to_char(sysdate))';
  EXECUTE immediate v_insert USING g_mod_date_time,
  g_facility_id;
  g_skuattr_count := SQL%ROWCOUNT;
  IF g_zero_invn_flag THEN
    INSERT
    INTO gen605_sku_attr_gtt
      (
        item_id,
        invn_type,
        prod_stat,
        sku_attr_1,
        sku_attr_2,
        sku_attr_3,
        sku_attr_4,
        sku_attr_5,
        cntry_of_orgn,
        batch_nbr,
        sku_attr_id
      )
    SELECT item_id,
      NULL,
      NULL,
      NULL,
      NULL,
      NULL,
      NULL,
      NULL,
      NULL,
      NULL,
      NVL (g_skuattr_count, 0) + ROWNUM
    FROM gen605_sku_gtt ic
    WHERE NOT EXISTS
      (SELECT 1 FROM gen605_sku_attr_gtt sa WHERE ic.item_id = sa.item_id
      ) log errors (TO_CHAR(sysdate));
    log('Finished snapshot of SKU Attributes. Records count = ' || (g_SkuAttr_Count+SQL%ROWCOUNT), TRUE, 1);
  ELSE
    log('Finished snapshot of SKU Attributes. Records count = ' || (g_SkuAttr_Count), TRUE, 1);
  END IF;
  IF (NOT g_zero_invn_flag AND g_skuattr_count <= 0) THEN
    RAISE exp_skuattr_not_found;
  END IF;
EXCEPTION
WHEN exp_skuattr_not_found THEN
  raise_application_error (-20121, 'No Item Attributes found in WM_INVENTORY table for the passed in Item Attribute filter.  Please check the input Item Attributes' );
WHEN OTHERS THEN
  raise;
END snap_sku_attributes;
PROCEDURE snap_pkt_dtl
AS
BEGIN
  log('Started snapshot of PKT_DTL.', TRUE, 1);
  INSERT
  INTO gen605_pkt_dtl_gtt
    (
      pkt_ctrl_nbr,
      pkt_seq_nbr,
      sku_attr_id,
      pkt_qty
    )
  SELECT
    /*+ALL_ROWS*/
    ph.tc_order_id,
    pd.line_item_id,
    si.sku_attr_id,
    pd.order_qty
  FROM order_line_item pd
  INNER JOIN orders ph
  ON ph.order_id = pd.order_id
  INNER JOIN gen605_sku_attr_gtt si
  ON si.item_id                    = pd.item_id
  AND COALESCE (si.invn_type, ' ') = COALESCE (
    CASE pd.invn_type
      WHEN '*'
      THEN ' '
      ELSE pd.invn_type
    END, ' ' )
  AND COALESCE (si.prod_stat, ' ') = COALESCE (
    CASE pd.prod_stat
      WHEN '*'
      THEN ' '
      ELSE pd.prod_stat
    END, ' ' )
  AND COALESCE (si.batch_nbr, ' ') = COALESCE (
    CASE pd.batch_nbr
      WHEN '*'
      THEN ' '
      ELSE pd.batch_nbr
    END, ' ' )
  AND COALESCE (si.sku_attr_1, ' ') = COALESCE (
    CASE pd.item_attr_1
      WHEN '*'
      THEN ' '
      ELSE pd.item_attr_1
    END, ' ' )
  AND COALESCE (si.sku_attr_2, ' ') = COALESCE (
    CASE pd.item_attr_2
      WHEN '*'
      THEN ' '
      ELSE pd.item_attr_2
    END, ' ' )
  AND COALESCE (si.sku_attr_3, ' ') = COALESCE (
    CASE pd.item_attr_3
      WHEN '*'
      THEN ' '
      ELSE pd.item_attr_3
    END, ' ' )
  AND COALESCE (si.sku_attr_4, ' ') = COALESCE (
    CASE pd.item_attr_4
      WHEN '*'
      THEN ' '
      ELSE pd.item_attr_4
    END, ' ' )
  AND COALESCE (si.sku_attr_5, ' ') = COALESCE (
    CASE pd.item_attr_5
      WHEN '*'
      THEN ' '
      ELSE pd.item_attr_5
    END, ' ' )
  AND COALESCE (si.cntry_of_orgn, ' ') = COALESCE (
    CASE pd.cntry_of_orgn
      WHEN '*'
      THEN ' '
      ELSE pd.cntry_of_orgn
    END, ' ' )
  INNER JOIN orders phi
  ON pd.order_id         = phi.order_id
  WHERE ph.O_FACILITY_ID = g_facility_id
  AND ph.tc_company_id   = g_company_id
  AND phi.do_status      < 190 log errors (TO_CHAR(sysdate));
  log('Finished snapshot of PKT_DTL. Record count = ' || SQL%ROWCOUNT, TRUE, 1);
END snap_pkt_dtl;
PROCEDURE snap_srl_nbr_track
AS
BEGIN
  INSERT
  INTO gen605_srl_nbr_track_gtt
    (
      srl_nbr_track_id,
      item_id,
      srl_nbr,
      srl_seq_nbr,
      stat_code,
      lpn_id,
      lpn_detail_id,
      wm_inventory_id
    )
  SELECT srt.srl_nbr_trk_id,
    srt.item_id,
    srt.srl_nbr,
    srt.seq_nbr,
    srt.stat_code,
    srt.lpn_id,
    srt.lpn_detail_id,
    srt.wm_inventory_id
  FROM srl_nbr_track srt
  INNER JOIN gen605_sku_gtt sku
  ON srt.item_id        = sku.item_id
  WHERE srt.facility_id = g_facility_id
  AND srt.stat_code    >= 10
  AND srt.stat_code     < 90
  AND sku.srl_nbr_reqd  > 0 log errors (TO_CHAR(sysdate));
  log('Finished snapshot of SRL_NBR_TRACK. Record count = ' || SQL%ROWCOUNT, TRUE, 1);
END snap_srl_nbr_track;
PROCEDURE snap_pix_tran
AS
BEGIN
  INSERT
  INTO gen605_pix_tran_gtt
    (
      tran_type,
      tran_code,
      tran_nbr,
      pix_seq_nbr,
      proc_stat_code,
      case_nbr,
      sku_attr_id,
      invn_adjmt_qty,
      invn_adjmt_type,
      create_date_time,
      mod_date_time,
      user_id
    )
  SELECT
    /*+ALL_ROWS*/
    pt.tran_type,
    pt.tran_code,
    pt.tran_nbr,
    pt.pix_seq_nbr,
    pt.proc_stat_code,
    pt.case_nbr,
    si.sku_attr_id,
    pt.invn_adjmt_qty,
    pt.invn_adjmt_type,
    pt.create_date_time,
    pt.mod_date_time,
    pt.user_id
  FROM pix_tran pt
  INNER JOIN gen605_sku_attr_gtt si
  ON si.item_id                        = pt.item_id
  AND COALESCE (si.invn_type, ' ')     = COALESCE (pt.invn_type, ' ')
  AND COALESCE (si.prod_stat, ' ')     = COALESCE (pt.prod_stat, ' ')
  AND COALESCE (si.batch_nbr, ' ')     = COALESCE (pt.batch_nbr, ' ')
  AND COALESCE (si.sku_attr_1, ' ')    = COALESCE (pt.sku_attr_1, ' ')
  AND COALESCE (si.sku_attr_2, ' ')    = COALESCE (pt.sku_attr_2, ' ')
  AND COALESCE (si.sku_attr_3, ' ')    = COALESCE (pt.sku_attr_3, ' ')
  AND COALESCE (si.sku_attr_4, ' ')    = COALESCE (pt.sku_attr_4, ' ')
  AND COALESCE (si.sku_attr_5, ' ')    = COALESCE (pt.sku_attr_5, ' ')
  AND COALESCE (si.cntry_of_orgn, ' ') = COALESCE (pt.cntry_of_orgn, ' ')
  WHERE pt.tc_company_id               = g_company_id
  AND pt.facility_id                   = g_facility_id log errors (TO_CHAR(sysdate));
  log('Finished snapshot of PIX_TRAN. Record count = ' || SQL%ROWCOUNT, TRUE, 1);
END snap_pix_tran;
PROCEDURE snap_wm_inventory
AS
BEGIN
  INSERT
  INTO gen605_wm_inventory_gtt
    (
      lpn_detail_id,
      sku_attr_id,
      inventory_type,
      consumption_priority_dttm,
      on_hand_qty,
      qty_not_alloc,
      qty_alloc,
      lpn_facility_status,
      location_id,
      location_dtl_id,
      locn_class,
      wm_inventory_id,
      transitional_inventory_type,
      allocable,
      inbound_outbound_indicator,
      flag,
      actl_weight,
      single_line_lpn,
      lpn_id,
      item_id
    )
  SELECT lpn_detail_id,
    sku_attr_id,
    inventory_type,
    consumption_priority_dttm,
    on_hand_qty,
    qty_not_alloc,
    qty_alloc,
    lpn_facility_status,
    location_id,
    location_dtl_id,
    locn_class,
    wm_inventory_id,
    transitional_inventory_type,
    allocable,
    inbound_outbound_indicator,
    flag,
    weight,
    single_line_lpn,
    lpn_id,
    item_id
  FROM
    (
    --INBOUND LPN's
    SELECT wi.lpn_detail_id,
      si.sku_attr_id,
      wi.inventory_type,
      l.consumption_priority_dttm,
      wi.on_hand_qty AS on_hand_qty,
      CASE
        WHEN wi.allocatable = 'Y'
        THEN 0
        ELSE wi.on_hand_qty
      END AS qty_not_alloc,
      CASE
        WHEN wi.allocatable = 'Y'
        THEN wi.wm_allocated_qty
        ELSE 0
      END AS qty_alloc,
      l.lpn_facility_status,
      wi.location_id,
      wi.location_dtl_id,
      wi.locn_class,
      wi.wm_inventory_id,
      wi.transitional_inventory_type,
      CASE
        WHEN wi.allocatable = 'Y'
        THEN 1
        ELSE 0
      END AS allocable,
      wi.inbound_outbound_indicator,
      'I' AS flag,
      COALESCE(l.weight, 0) weight,
      l.single_line_lpn,
      wi.lpn_id,
      wi.item_id
    FROM gen605_sku_attr_gtt si,
      wm_inventory AS OF TIMESTAMP g_mod_date_time wi,
      lpn          AS OF TIMESTAMP g_mod_date_time l
    WHERE wi.lpn_id                     = l.lpn_id
    AND wi.item_id                      = si.item_id
    AND ((si.invn_type                  = wi.inventory_type)
    OR (si.invn_type                   IS NULL
    AND wi.inventory_type              IS NULL))
    AND ((si.prod_stat                  = wi.product_status)
    OR (si.prod_stat                   IS NULL
    AND wi.product_status              IS NULL))
    AND ((si.batch_nbr                  = wi.batch_nbr)
    OR (si.batch_nbr                   IS NULL
    AND wi.batch_nbr                   IS NULL))
    AND ((si.sku_attr_1                 = wi.item_attr_1)
    OR (si.sku_attr_1                  IS NULL
    AND wi.item_attr_1                 IS NULL))
    AND ((si.sku_attr_2                 = wi.item_attr_2)
    OR (si.sku_attr_2                  IS NULL
    AND wi.item_attr_2                 IS NULL))
    AND ((si.sku_attr_3                 = wi.item_attr_3)
    OR (si.sku_attr_3                  IS NULL
    AND wi.item_attr_3                 IS NULL))
    AND ((si.sku_attr_4                 = wi.item_attr_4)
    OR (si.sku_attr_4                  IS NULL
    AND wi.item_attr_4                 IS NULL))
    AND ((si.sku_attr_5                 = wi.item_attr_5)
    OR (si.sku_attr_5                  IS NULL
    AND wi.item_attr_5                 IS NULL))
    AND ((si.cntry_of_orgn              = wi.cntry_of_orgn)
    OR (si.cntry_of_orgn               IS NULL
    AND wi.cntry_of_orgn               IS NULL))
    AND l.lpn_facility_status          >= 10
    AND l.lpn_facility_status           < 90
    AND wi.c_facility_id                = g_facility_id
    AND wi.inbound_outbound_indicator   = 'I'
    AND wi.transitional_inventory_type IS NULL
    AND wi.marked_for_delete            = 'N'
    UNION ALL -- outbound lpn
    SELECT wi.lpn_detail_id,
      si.sku_attr_id,
      wi.inventory_type,
      l.consumption_priority_dttm,
      wi.on_hand_qty      AS on_hand_qty,
      0                   AS qty_not_alloc,
      wi.wm_allocated_qty AS qty_alloc,
      l.lpn_facility_status,
      wi.location_id,
      wi.location_dtl_id,
      wi.locn_class,
      wi.wm_inventory_id,
      wi.transitional_inventory_type,
      1 AS allocable,
      wi.inbound_outbound_indicator,
      'O' AS flag,
      COALESCE(l.weight, 0) weight,
      l.single_line_lpn,
      wi.lpn_id,
      wi.item_id
    FROM wm_inventory AS OF TIMESTAMP g_mod_date_time wi,
      lpn             AS OF TIMESTAMP g_mod_date_time l,
      gen605_sku_attr_gtt si
    WHERE wi.lpn_id                     = l.lpn_id
    AND wi.item_id                      = si.item_id
    AND ((si.invn_type                  = wi.inventory_type)
    OR (si.invn_type                   IS NULL
    AND wi.inventory_type              IS NULL))
    AND ((si.prod_stat                  = wi.product_status)
    OR (si.prod_stat                   IS NULL
    AND wi.product_status              IS NULL))
    AND ((si.batch_nbr                  = wi.batch_nbr)
    OR (si.batch_nbr                   IS NULL
    AND wi.batch_nbr                   IS NULL))
    AND ((si.sku_attr_1                 = wi.item_attr_1)
    OR (si.sku_attr_1                  IS NULL
    AND wi.item_attr_1                 IS NULL))
    AND ((si.sku_attr_2                 = wi.item_attr_2)
    OR (si.sku_attr_2                  IS NULL
    AND wi.item_attr_2                 IS NULL))
    AND ((si.sku_attr_3                 = wi.item_attr_3)
    OR (si.sku_attr_3                  IS NULL
    AND wi.item_attr_3                 IS NULL))
    AND ((si.sku_attr_4                 = wi.item_attr_4)
    OR (si.sku_attr_4                  IS NULL
    AND wi.item_attr_4                 IS NULL))
    AND ((si.sku_attr_5                 = wi.item_attr_5)
    OR (si.sku_attr_5                  IS NULL
    AND wi.item_attr_5                 IS NULL))
    AND ((si.cntry_of_orgn              = wi.cntry_of_orgn)
    OR (si.cntry_of_orgn               IS NULL
    AND wi.cntry_of_orgn               IS NULL))
    AND l.lpn_facility_status          >= 10
    AND l.lpn_facility_status           < 90
    AND wi.c_facility_id                = g_facility_id
    AND wi.inbound_outbound_indicator   = 'O'
    AND wi.transitional_inventory_type IS NULL
    AND wi.marked_for_delete            = 'N'
    UNION ALL
    --transitional when tc_lpn_id is null
    --       no lpn fields required as tc_lpn_id is null, join on lpn hence removed
    SELECT wi.lpn_detail_id,
      si.sku_attr_id,
      wi.inventory_type,
      sysdate        AS consumption_priority_dttm,
      wi.on_hand_qty AS on_hand_qty,
      CASE
        WHEN wi.allocatable = 'N'
        THEN wi.on_hand_qty
        ELSE 0
      END                 AS qty_not_alloc,
      wi.wm_allocated_qty AS qty_alloc,
      0                   AS lpn_facility_status,
      wi.location_id,
      wi.location_dtl_id,
      wi.locn_class,
      wi.wm_inventory_id,
      wi.transitional_inventory_type,
      CASE
        WHEN wi.allocatable = 'N'
        THEN 0
        ELSE 1
      END AS allocable,
      wi.inbound_outbound_indicator,
      'T' AS flag,
      0 weight,
      'N',
      wi.lpn_id,
      wi.item_id
    FROM wm_inventory AS OF TIMESTAMP g_mod_date_time wi,
      gen605_sku_attr_gtt si
    WHERE wi.item_id                    = si.item_id
    AND ((si.invn_type                  = wi.inventory_type)
    OR (si.invn_type                   IS NULL
    AND wi.inventory_type              IS NULL))
    AND ((si.prod_stat                  = wi.product_status)
    OR (si.prod_stat                   IS NULL
    AND wi.product_status              IS NULL))
    AND ((si.batch_nbr                  = wi.batch_nbr)
    OR (si.batch_nbr                   IS NULL
    AND wi.batch_nbr                   IS NULL))
    AND ((si.sku_attr_1                 = wi.item_attr_1)
    OR (si.sku_attr_1                  IS NULL
    AND wi.item_attr_1                 IS NULL))
    AND ((si.sku_attr_2                 = wi.item_attr_2)
    OR (si.sku_attr_2                  IS NULL
    AND wi.item_attr_2                 IS NULL))
    AND ((si.sku_attr_3                 = wi.item_attr_3)
    OR (si.sku_attr_3                  IS NULL
    AND wi.item_attr_3                 IS NULL))
    AND ((si.sku_attr_4                 = wi.item_attr_4)
    OR (si.sku_attr_4                  IS NULL
    AND wi.item_attr_4                 IS NULL))
    AND ((si.sku_attr_5                 = wi.item_attr_5)
    OR (si.sku_attr_5                  IS NULL
    AND wi.item_attr_5                 IS NULL))
    AND ((si.cntry_of_orgn              = wi.cntry_of_orgn)
    OR (si.cntry_of_orgn               IS NULL
    AND wi.cntry_of_orgn               IS NULL))
    AND wi.c_facility_id                = g_facility_id
    AND wi.lpn_id                      IS NULL
    AND wi.transitional_inventory_type IS NOT NULL
    AND wi.marked_for_delete            = 'N'
    -- pld case1: when tc_lpn_id is not null ----> Tracks cases
    -- case2: when tc_lpn_id is null ----> does not track cases
    -- assumption: wm_invn rows are deleted when cases are consumed
    UNION ALL
    SELECT wi.lpn_detail_id,
      si.sku_attr_id,
      wi.inventory_type,
      COALESCE(l.consumption_priority_dttm, sysdate) AS consumption_priority_dttm,
      wi.on_hand_qty                                 AS on_hand_qty,
      CASE
        WHEN wi.allocatable = 'Y'
        THEN 0
        ELSE wi.on_hand_qty
      END AS qty_not_alloc,
      CASE
        WHEN wi.allocatable = 'Y'
        THEN wi.wm_allocated_qty
        ELSE 0
      END                                AS qty_alloc,
      COALESCE(l.lpn_facility_status, 0) AS lpn_facility_status,
      wi.location_id,
      wi.location_dtl_id,
      wi.locn_class,
      wi.wm_inventory_id,
      wi.transitional_inventory_type,
      CASE
        WHEN wi.allocatable = 'Y'
        THEN 1
        ELSE 0
      END AS allocable,
      wi.inbound_outbound_indicator,
      CASE
        WHEN wi.locn_class = 'A'
        THEN 'A'
        ELSE 'C'
      END AS flag,
      COALESCE(l.weight, 0) weight,
      COALESCE(l.single_line_lpn, 'N') AS single_line_lpn,
      wi.lpn_id,
      wi.item_id
    FROM wm_inventory AS OF TIMESTAMP g_mod_date_time wi,
      gen605_sku_attr_gtt si,
      lpn AS OF TIMESTAMP g_mod_date_time l
    WHERE wi.lpn_id                     = l.lpn_id(+)
    AND wi.item_id                      = si.item_id
    AND ((si.invn_type                  = wi.inventory_type)
    OR (si.invn_type                   IS NULL
    AND wi.inventory_type              IS NULL))
    AND ((si.prod_stat                  = wi.product_status)
    OR (si.prod_stat                   IS NULL
    AND wi.product_status              IS NULL))
    AND ((si.batch_nbr                  = wi.batch_nbr)
    OR (si.batch_nbr                   IS NULL
    AND wi.batch_nbr                   IS NULL))
    AND ((si.sku_attr_1                 = wi.item_attr_1)
    OR (si.sku_attr_1                  IS NULL
    AND wi.item_attr_1                 IS NULL))
    AND ((si.sku_attr_2                 = wi.item_attr_2)
    OR (si.sku_attr_2                  IS NULL
    AND wi.item_attr_2                 IS NULL))
    AND ((si.sku_attr_3                 = wi.item_attr_3)
    OR (si.sku_attr_3                  IS NULL
    AND wi.item_attr_3                 IS NULL))
    AND ((si.sku_attr_4                 = wi.item_attr_4)
    OR (si.sku_attr_4                  IS NULL
    AND wi.item_attr_4                 IS NULL))
    AND ((si.sku_attr_5                 = wi.item_attr_5)
    OR (si.sku_attr_5                  IS NULL
    AND wi.item_attr_5                 IS NULL))
    AND ((si.cntry_of_orgn              = wi.cntry_of_orgn)
    OR (si.cntry_of_orgn               IS NULL
    AND wi.cntry_of_orgn               IS NULL))
    AND wi.c_facility_id                = g_facility_id
    AND wi.marked_for_delete            = 'N'
    AND wi.locn_class                  IN ('A', 'C')
    AND wi.transitional_inventory_type IS NULL
    AND (l.lpn_facility_status         IN (90, 91)
    OR l.lpn_id                        IS NULL)
    )
  GROUP BY flag,
    qty_alloc,
    qty_not_alloc,
    on_hand_qty,
    lpn_detail_id,
    sku_attr_id,
    inventory_type,
    consumption_priority_dttm,
    allocable,
    transitional_inventory_type,
    wm_inventory_id,
    locn_class,
    location_dtl_id,
    location_id,
    lpn_facility_status,
    inbound_outbound_indicator,
    weight,
    single_line_lpn,
    lpn_id,
    item_id log errors (TO_CHAR(sysdate));
END;
PROCEDURE snap_lpn_lock
AS
BEGIN
  INSERT
  INTO gen605_lpn_lock_gtt
    (
      lpn_id,
      inventory_lock_code,
      misc_flags_2,
      misc_flags_34
    )
WITH t_lock_codes AS
  (SELECT code_id,
    misc_flags
  FROM sys_code
  WHERE rec_type = 'B'
  AND code_type  = '527'
  UNION
  SELECT code_id ,
    misc_flags
  FROM whse_sys_code
  WHERE rec_type = 'B'
  AND code_type  = '527'
  AND whse       = g_warehouse
  UNION
  SELECT code_id ,
    misc_flags
  FROM cd_sys_code
  WHERE rec_type   = 'B'
  AND code_type    = '527'
  AND cd_master_id = g_company_id
  )
-- assumes a max of 99 possible locks for an lpn
SELECT DISTINCT ll.lpn_id,
  ll.inventory_lock_code,
  SUBSTR (sc.misc_flags, 2, 1),
  TO_CHAR(row_number() over(partition BY ll.lpn_id order by lpad(COALESCE(SUBSTR(ltrim(rtrim(sc.misc_flags)), 3, 2), '99'), 2, '0'), ll.inventory_lock_code))
FROM lpn_lock ll
JOIN t_lock_codes sc
ON ll.inventory_lock_code = sc.code_id
WHERE EXISTS
  (SELECT 1
  FROM gen605_wm_inventory_gtt wi
  WHERE ll.lpn_id             = wi.lpn_id
  AND wi.lpn_facility_status >= 10
  AND wi.lpn_facility_status  < 90
  )log errors (TO_CHAR(sysdate));
END;
PROCEDURE snap_lpn_catch_wt
AS
BEGIN
  INSERT INTO gen605_lpn_catch_wt_gtt
    (lpn_id, catch_weight, lpn_detail_id
    )
  SELECT lcw.lpn_id,
    SUM(lcw.catch_weight),
    lcw.lpn_detail_id
  FROM lpn_catch_weight lcw,
    lpn l,
    lpn_detail ld
  WHERE lcw.lpn_id           = l.lpn_id
  AND lcw.lpn_detail_id      = ld.lpn_detail_id
  AND l.lpn_facility_status >= 10
  AND l.lpn_facility_status  < 90
  AND l.c_facility_id        = g_facility_id
  AND l.lpn_id               = ld.lpn_id
  AND l.tc_company_id        = g_company_id
  GROUP BY lcw.lpn_id,
    lcw.lpn_detail_id log errors (TO_CHAR(sysdate));
END snap_lpn_catch_wt;
PROCEDURE create_snapshots(
    p_season    IN VARCHAR2 DEFAULT NULL,
    p_season_yr IN VARCHAR2 DEFAULT NULL,
    p_style     IN VARCHAR2 DEFAULT NULL,
    p_style_sfx IN VARCHAR2 DEFAULT NULL,
    p_color     IN VARCHAR2 DEFAULT NULL,
    p_color_sfx IN VARCHAR2 DEFAULT NULL,
    p_sec_dim   IN VARCHAR2 DEFAULT NULL,
    p_qual      IN VARCHAR2 DEFAULT NULL,
    p_size_desc IN VARCHAR2 DEFAULT NULL,
    p_invn_type IN VARCHAR2 DEFAULT '*',
    p_prod_stat IN VARCHAR2 DEFAULT NULL,
    p_item_name IN VARCHAR2 DEFAULT NULL )
AS
BEGIN
  log('Started Creation of Snapshots', TRUE);
  snap_item_master (p_season, p_season_yr, p_style, p_style_sfx, p_color, p_color_sfx, p_sec_dim, p_qual, p_size_desc, FALSE, p_item_name );
  --       IF (g_reconcile_flag)
  --       THEN
  --          -- Create gen605_sku_gtt_invn_dtl
  --          gen_sku_invn (p_invn_type, p_prod_stat);
  --       ELSE
  --          -- Create gen605_sku_gtt_invn_dtl
  --          manh_pix_sku_invn (g_warehouse, v_rc1);
  --       END IF;
  snap_sku_attributes (p_invn_type, p_prod_stat);
  snap_wm_inventory;
  snap_lpn_lock;
  snap_lpn_catch_wt;
  IF g_erphosttype = '1' AND g_pix_level = 'S' THEN
    snap_pkt_dtl;
  END IF;
  IF g_srlnbrpix_flag THEN
    snap_srl_nbr_track;
  END IF;
  IF g_reconcile_flag THEN
    snap_pix_tran;
  END IF;
  log('Finished Creation of Snapshots', TRUE);
END create_snapshots;
PROCEDURE generate_actv_cpick_srlpix
AS
BEGIN
  log('Started serial pix generation for AXX, CXX', true, 1);
  IF g_Active_Locn_Lock_flag THEN
    -- SRL_PIX_TRAN for Un-allocable active Inventory
    INSERT
    INTO srl_pix_tran
      (
        srl_pix_tran_id,
        tran_type,
        tran_code,
        tran_nbr,
        pix_seq_nbr,
        proc_stat_code,
        srl_nbr,
        seq_nbr,
        create_date_time,
        mod_date_time,
        user_id,
        pix_tran_id
      )
    -- Case Inventory i.e. VERF_SRL_NBR_TRACK.STAT_CODE = 30
    SELECT srl_pix_tran_id_seq.nextval,
      tran_type,
      tran_code,
      tran_nbr,
      pix_seq_nbr,
      10 proc_stat_code,
      srl_nbr,
      srl_seq_nbr,
      create_date_time,
      mod_date_time,
      user_id,
      pix_tran_id
    FROM
      (SELECT
        /*+ALL_ROWS*/
        pt.tran_type,
        pt.tran_code,
        pt.tran_nbr,
        pt.pix_seq_nbr,
        10,
        srl.srl_nbr,
        srl.srl_seq_nbr,
        g_mod_date_time create_date_time,
        g_mod_date_time mod_date_time,
        g_user_id user_id,
        pt.pix_tran_id
      FROM gen605_srl_nbr_track_gtt srl
      JOIN pix_tran pt
      ON srl.item_id = pt.item_id
      JOIN gen605_wm_inventory_gtt ti
      ON ti.wm_inventory_id = srl.wm_inventory_id
      AND ti.allocable      = 0
      JOIN pick_locn_dtl pld
      ON ti.location_dtl_id = pld.pick_locn_dtl_id
      JOIN gen605_sku_attr_gtt si
      ON ( si.sku_attr_id                  = ti.sku_attr_id
      AND si.item_id                       = pt.item_id
      AND COALESCE (si.invn_type, ' ')     = COALESCE (pt.invn_type, ' ')
      AND COALESCE (si.prod_stat, ' ')     = COALESCE (pt.prod_stat, ' ')
      AND COALESCE (si.batch_nbr, ' ')     = COALESCE (pt.batch_nbr, ' ')
      AND COALESCE (si.sku_attr_1, ' ')    = COALESCE (pt.sku_attr_1, ' ')
      AND COALESCE (si.sku_attr_2, ' ')    = COALESCE (pt.sku_attr_2, ' ')
      AND COALESCE (si.sku_attr_3, ' ')    = COALESCE (pt.sku_attr_3, ' ')
      AND COALESCE (si.sku_attr_4, ' ')    = COALESCE (pt.sku_attr_4, ' ')
      AND COALESCE (si.sku_attr_5, ' ')    = COALESCE (pt.sku_attr_5, ' ')
      AND COALESCE (si.cntry_of_orgn, ' ') = COALESCE (pt.cntry_of_orgn, ' '))
      WHERE srl.stat_code                  = 30
      AND pt.proc_stat_code                = 10
      AND pt.create_date_time              = g_mod_date_time
      AND pt.tran_type                     = '605'
      AND pt.tran_code                     = 'A'
        || pld.pikng_lock_code
      AND pt.tran_nbr = g_tran_nbr
      AND ti.flag     = 'A'
      )log errors (TO_CHAR(sysdate));
    log('Generated Un-allocable SRL_PIX_TRAN Records for AXX.  PIX Count = ' || SQL%ROWCOUNT, TRUE, 2);
  END IF;
  IF g_CasePick_Locn_Lock_flag THEN
    -- SRL_PIX_TRAN for Un-allocable case pick Inventory
    INSERT
    INTO srl_pix_tran
      (
        srl_pix_tran_id,
        tran_type,
        tran_code,
        tran_nbr,
        pix_seq_nbr,
        proc_stat_code,
        srl_nbr,
        seq_nbr,
        create_date_time,
        mod_date_time,
        user_id,
        pix_tran_id
      )
    -- Case Inventory i.e. VERF_SRL_NBR_TRACK.STAT_CODE = 10
    SELECT srl_pix_tran_id_seq.nextval,
      tran_type,
      tran_code,
      tran_nbr,
      pix_seq_nbr,
      10 proc_stat_code,
      srl_nbr,
      srl_seq_nbr,
      create_date_time,
      mod_date_time,
      user_id,
      pix_tran_id
    FROM
      (SELECT
        /*+ALL_ROWS*/
        pt.tran_type,
        pt.tran_code,
        pt.tran_nbr,
        pt.pix_seq_nbr,
        10,
        srl.srl_nbr,
        srl.srl_seq_nbr,
        g_mod_date_time create_date_time,
        g_mod_date_time mod_date_time,
        g_user_id user_id,
        pt.pix_tran_id
      FROM gen605_srl_nbr_track_gtt srl
      JOIN pix_tran pt
      ON srl.item_id = pt.item_id
      JOIN gen605_wm_inventory_gtt ti
      ON ti.wm_inventory_id = srl.wm_inventory_id
      AND ti.allocable      = 0
      JOIN pick_locn_dtl pld
      ON ti.location_dtl_id = pld.pick_locn_dtl_id
      JOIN gen605_sku_attr_gtt si
      ON (si.sku_attr_id                   = ti.sku_attr_id
      AND si.item_id                       = pt.item_id
      AND COALESCE(si.invn_type, ' ')      = COALESCE(pt.invn_type, ' ')
      AND COALESCE (si.prod_stat, ' ')     = COALESCE (pt.prod_stat, ' ')
      AND COALESCE (si.batch_nbr, ' ')     = COALESCE (pt.batch_nbr, ' ')
      AND COALESCE (si.sku_attr_1, ' ')    = COALESCE (pt.sku_attr_1, ' ')
      AND COALESCE (si.sku_attr_2, ' ')    = COALESCE (pt.sku_attr_2, ' ')
      AND COALESCE (si.sku_attr_3, ' ')    = COALESCE (pt.sku_attr_3, ' ')
      AND COALESCE (si.sku_attr_4, ' ')    = COALESCE (pt.sku_attr_4, ' ')
      AND COALESCE (si.sku_attr_5, ' ')    = COALESCE (pt.sku_attr_5, ' ')
      AND COALESCE (si.cntry_of_orgn, ' ') = COALESCE (pt.cntry_of_orgn, ' '))
      WHERE srl.stat_code                  = 40
      AND pt.proc_stat_code                = 10
      AND pt.create_date_time              = g_mod_date_time
      AND pt.tran_type                     = '605'
      AND pt.tran_code                     = 'C'
        || pld.pikng_lock_code
      AND pt.tran_nbr = g_tran_nbr
      AND ti.flag     = 'C'
      )log errors (TO_CHAR(sysdate));
    log('Generated Un-allocable SRL_PIX_TRAN Records for CXX.  PIX Count = ' || SQL%ROWCOUNT, TRUE, 2);
  END IF;
END generate_actv_cpick_srlpix;
PROCEDURE generate_summary_srlpix
AS
BEGIN
  log('Started Summary Level SRL_PIX_TRAN generation', TRUE, 1);
  IF g_unalloc_invn_flag THEN
    -- SRL_PIX_TRAN for Un-allocable Inventory
    INSERT
    INTO srl_pix_tran
      (
        SRL_PIX_TRAN_ID,
        tran_type,
        tran_code,
        tran_nbr,
        pix_seq_nbr,
        proc_stat_code,
        srl_nbr,
        seq_nbr,
        create_date_time,
        mod_date_time,
        user_id,
        pix_tran_id
      )
    -- Case Inventory i.e. VERF_SRL_NBR_TRACK.STAT_CODE = 10
    SELECT SRL_PIX_TRAN_ID_SEQ.nextval,
      tran_type,
      tran_code,
      tran_nbr,
      pix_seq_nbr,
      proc_stat_code,
      srl_nbr,
      srl_seq_nbr,
      create_date_time,
      mod_date_time,
      user_id,
      pix_tran_id
    FROM
      (SELECT
        /*+ALL_ROWS*/
        pt.tran_type,
        pt.tran_code,
        pt.tran_nbr,
        pt.pix_seq_nbr,
        10 proc_stat_code,
        srl.srl_nbr,
        srl.srl_seq_nbr,
        g_mod_date_time create_date_time,
        g_mod_date_time mod_date_time,
        g_user_id user_id,
        pt.pix_tran_id
      FROM gen605_srl_nbr_track_gtt srl
      INNER JOIN pix_tran pt
      ON srl.item_id = pt.item_id
      INNER JOIN gen605_wm_inventory_gtt cd
      ON
        /*srl.case_nbr = cd.tc_lpn_id AND*/
        srl.lpn_detail_id = cd.lpn_detail_id
      INNER JOIN gen605_sku_attr_gtt si
      ON ( si.sku_attr_id                  = cd.sku_attr_id
      AND si.item_id                       = pt.item_id
      AND COALESCE (si.invn_type, ' ')     = COALESCE (pt.invn_type, ' ')
      AND COALESCE (si.prod_stat, ' ')     = COALESCE (pt.prod_stat, ' ')
      AND COALESCE (si.batch_nbr, ' ')     = COALESCE (pt.batch_nbr, ' ')
      AND COALESCE (si.sku_attr_1, ' ')    = COALESCE (pt.sku_attr_1, ' ')
      AND COALESCE (si.sku_attr_2, ' ')    = COALESCE (pt.sku_attr_2, ' ')
      AND COALESCE (si.sku_attr_3, ' ')    = COALESCE (pt.sku_attr_3, ' ')
      AND COALESCE (si.sku_attr_4, ' ')    = COALESCE (pt.sku_attr_4, ' ')
      AND COALESCE (si.sku_attr_5, ' ')    = COALESCE (pt.sku_attr_5, ' ')
      AND COALESCE (si.cntry_of_orgn, ' ') = COALESCE (pt.cntry_of_orgn, ' ') )
      WHERE srl.stat_code                  = 10
      AND pt.proc_stat_code                = 10
      AND pt.create_date_time              = g_mod_date_time
      AND pt.tran_type                     = '605'
      AND pt.tran_code                     = '98'
      AND pt.tran_nbr                      = g_tran_nbr
      AND cd.flag                          = 'I'
      AND EXISTS
        (SELECT 1
        FROM gen605_lpn_lock_gtt cl
        WHERE cl.lpn_id                    = srl.lpn_id
        AND COALESCE(cl.misc_flags_2, 'Z') = 'N'
        )
      UNION ALL
      -- Transitional Inventory i.e. VERF_SRL_NBR_TRACK.STAT_CODE = 20
      SELECT
        /*+ALL_ROWS*/
        pt.tran_type,
        pt.tran_code,
        pt.tran_nbr,
        pt.pix_seq_nbr,
        10,
        srl.srl_nbr,
        srl.srl_seq_nbr,
        g_mod_date_time create_date_time,
        g_mod_date_time mod_date_time,
        g_user_id user_id,
        pt.pix_tran_id
      FROM gen605_srl_nbr_track_gtt srl
      INNER JOIN pix_tran pt
      ON srl.item_id = pt.item_id
      INNER JOIN gen605_wm_inventory_gtt ti
      ON ti.wm_inventory_id = srl.wm_inventory_id
      AND ti.allocable      = 0
      INNER JOIN gen605_sku_attr_gtt si
      ON ( si.sku_attr_id                  = ti.sku_attr_id
      AND si.item_id                       = pt.item_id
      AND COALESCE (si.invn_type, ' ')     = COALESCE (pt.invn_type, ' ')
      AND COALESCE (si.prod_stat, ' ')     = COALESCE (pt.prod_stat, ' ')
      AND COALESCE (si.batch_nbr, ' ')     = COALESCE (pt.batch_nbr, ' ')
      AND COALESCE (si.sku_attr_1, ' ')    = COALESCE (pt.sku_attr_1, ' ')
      AND COALESCE (si.sku_attr_2, ' ')    = COALESCE (pt.sku_attr_2, ' ')
      AND COALESCE (si.sku_attr_3, ' ')    = COALESCE (pt.sku_attr_3, ' ')
      AND COALESCE (si.sku_attr_4, ' ')    = COALESCE (pt.sku_attr_4, ' ')
      AND COALESCE (si.sku_attr_5, ' ')    = COALESCE (pt.sku_attr_5, ' ')
      AND COALESCE (si.cntry_of_orgn, ' ') = COALESCE (pt.cntry_of_orgn, ' ') )
      WHERE srl.stat_code                  = 20
      AND pt.proc_stat_code                = 10
      AND pt.create_date_time              = g_mod_date_time
      AND pt.tran_type                     = '605'
      AND pt.tran_code                     = '98'
      AND pt.tran_nbr                      = g_tran_nbr
      AND ti.flag                          = 'T'
      ) unalloc_srl_pix log errors (TO_CHAR(sysdate));
    log('Generated Summary Level Un-allocable SRL_PIX_TRAN Records (TRAN_CODE = 98).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 2);
    -- SRL_PIX_TRAN for Allocable Inventory
    INSERT
    INTO srl_pix_tran
      (
        srl_pix_tran_id,
        tran_type,
        tran_code,
        tran_nbr,
        pix_seq_nbr,
        proc_stat_code,
        srl_nbr,
        seq_nbr,
        create_date_time,
        mod_date_time,
        user_id,
        pix_tran_id
      )
    -- Case Inventory i.e. VERF_SRL_NBR_TRACK.STAT_CODE = 10
    SELECT srl_pix_tran_id_seq.nextval,
      tran_type,
      tran_code,
      tran_nbr,
      pix_seq_nbr,
      proc_stat_code,
      srl_nbr,
      srl_seq_nbr,
      create_date_time,
      mod_date_time,
      user_id,
      pix_tran_id
    FROM
      (SELECT
        /*+ALL_ROWS*/
        pt.tran_type,
        pt.tran_code,
        pt.tran_nbr,
        pt.pix_seq_nbr,
        10 proc_stat_code,
        srl.srl_nbr,
        srl.srl_seq_nbr,
        g_mod_date_time create_date_time,
        g_mod_date_time mod_date_time,
        g_user_id user_id,
        pt.pix_tran_id
      FROM gen605_srl_nbr_track_gtt srl
      INNER JOIN pix_tran pt
      ON srl.item_id = pt.item_id
      INNER JOIN gen605_wm_inventory_gtt cd
      ON
        /*srl.case_nbr = cd.tc_lpn_id AND*/
        srl.lpn_detail_id = cd.lpn_detail_id
      INNER JOIN gen605_sku_attr_gtt si
      ON ( si.sku_attr_id                  = cd.sku_attr_id
      AND si.item_id                       = pt.item_id
      AND COALESCE (si.invn_type, ' ')     = COALESCE (pt.invn_type, ' ')
      AND COALESCE (si.prod_stat, ' ')     = COALESCE (pt.prod_stat, ' ')
      AND COALESCE (si.batch_nbr, ' ')     = COALESCE (pt.batch_nbr, ' ')
      AND COALESCE (si.sku_attr_1, ' ')    = COALESCE (pt.sku_attr_1, ' ')
      AND COALESCE (si.sku_attr_2, ' ')    = COALESCE (pt.sku_attr_2, ' ')
      AND COALESCE (si.sku_attr_3, ' ')    = COALESCE (pt.sku_attr_3, ' ')
      AND COALESCE (si.sku_attr_4, ' ')    = COALESCE (pt.sku_attr_4, ' ')
      AND COALESCE (si.sku_attr_5, ' ')    = COALESCE (pt.sku_attr_5, ' ')
      AND COALESCE (si.cntry_of_orgn, ' ') = COALESCE (pt.cntry_of_orgn, ' ') )
      WHERE srl.stat_code                  = 10
      AND pt.proc_stat_code                = 10
      AND pt.create_date_time              = g_mod_date_time
      AND pt.tran_type                     = '605'
      AND pt.tran_code                     = '97'
      AND pt.tran_nbr                      = g_tran_nbr
      AND cd.flag                          = 'I'
      AND NOT EXISTS
        (SELECT 1
        FROM gen605_lpn_lock_gtt Cl2
        WHERE cl2.lpn_id                    = srl.lpn_id
        AND COALESCE(cl2.misc_flags_2, 'Z') = 'N'
        )
      UNION ALL
      -- Transitional Inventory i.e. VERF_SRL_NBR_TRACK.STAT_CODE = 20
      SELECT
        /*+ALL_ROWS*/
        pt.tran_type,
        pt.tran_code,
        pt.tran_nbr,
        pt.pix_seq_nbr,
        10,
        srl.srl_nbr,
        srl.srl_seq_nbr,
        g_mod_date_time create_date_time,
        g_mod_date_time mod_date_time,
        g_user_id user_id,
        pt.pix_tran_id
      FROM gen605_srl_nbr_track_gtt srl
      INNER JOIN pix_tran pt
      ON srl.item_id = pt.item_id
      INNER JOIN gen605_wm_inventory_gtt ti
      ON ti.wm_inventory_id = srl.wm_inventory_id
      AND ti.allocable      = 1
      INNER JOIN gen605_sku_attr_gtt si
      ON ( si.sku_attr_id                  = ti.sku_attr_id
      AND si.item_id                       = pt.item_id
      AND COALESCE (si.invn_type, ' ')     = COALESCE (pt.invn_type, ' ')
      AND COALESCE (si.prod_stat, ' ')     = COALESCE (pt.prod_stat, ' ')
      AND COALESCE (si.batch_nbr, ' ')     = COALESCE (pt.batch_nbr, ' ')
      AND COALESCE (si.sku_attr_1, ' ')    = COALESCE (pt.sku_attr_1, ' ')
      AND COALESCE (si.sku_attr_2, ' ')    = COALESCE (pt.sku_attr_2, ' ')
      AND COALESCE (si.sku_attr_3, ' ')    = COALESCE (pt.sku_attr_3, ' ')
      AND COALESCE (si.sku_attr_4, ' ')    = COALESCE (pt.sku_attr_4, ' ')
      AND COALESCE (si.sku_attr_5, ' ')    = COALESCE (pt.sku_attr_5, ' ')
      AND COALESCE (si.cntry_of_orgn, ' ') = COALESCE (pt.cntry_of_orgn, ' ') )
      WHERE srl.stat_code                  = 20
      AND pt.proc_stat_code                = 10
      AND pt.create_date_time              = g_mod_date_time
      AND pt.tran_type                     = '605'
      AND pt.tran_code                     = '97'
      AND pt.tran_nbr                      = g_tran_nbr
      AND ti.flag                          = 'T'
      UNION ALL
      -- Active/Case Pick Inventory i.e. VERF_SRL_NBR_TRACK.STAT_CODE = 30/40
      SELECT
        /*+ALL_ROWS*/
        pt.tran_type,
        pt.tran_code,
        pt.tran_nbr,
        pt.pix_seq_nbr,
        10,
        srl.srl_nbr,
        srl.srl_seq_nbr,
        g_mod_date_time create_date_time,
        g_mod_date_time mod_date_time,
        g_user_id user_id,
        pt.pix_tran_id
      FROM gen605_srl_nbr_track_gtt srl
      INNER JOIN pix_tran pt
      ON srl.item_id = pt.item_id
      INNER JOIN gen605_wm_inventory_gtt pld
      ON pld.wm_inventory_id = srl.wm_inventory_id
        /*pld.location_id = srl.locn_id AND pld.location_dtl_id = srl.locn_seq_nbr*/
      INNER JOIN gen605_sku_attr_gtt si
      ON ( si.sku_attr_id                  = pld.sku_attr_id
      AND si.item_id                       = pt.item_id
      AND COALESCE (si.invn_type, ' ')     = COALESCE (pt.invn_type, ' ')
      AND COALESCE (si.prod_stat, ' ')     = COALESCE (pt.prod_stat, ' ')
      AND COALESCE (si.batch_nbr, ' ')     = COALESCE (pt.batch_nbr, ' ')
      AND COALESCE (si.sku_attr_1, ' ')    = COALESCE (pt.sku_attr_1, ' ')
      AND COALESCE (si.sku_attr_2, ' ')    = COALESCE (pt.sku_attr_2, ' ')
      AND COALESCE (si.sku_attr_3, ' ')    = COALESCE (pt.sku_attr_3, ' ')
      AND COALESCE (si.sku_attr_4, ' ')    = COALESCE (pt.sku_attr_4, ' ')
      AND COALESCE (si.sku_attr_5, ' ')    = COALESCE (pt.sku_attr_5, ' ')
      AND COALESCE (si.cntry_of_orgn, ' ') = COALESCE (pt.cntry_of_orgn, ' ') )
      WHERE (srl.stat_code                 = 30
      OR srl.stat_code                     = 40)
      AND srl.stat_code                    < 90
      AND pt.proc_stat_code                = 10
      AND pt.create_date_time              = g_mod_date_time
      AND pt.tran_type                     = '605'
      AND pt.tran_code                     = '97'
      AND pt.tran_nbr                      = g_tran_nbr
      AND pld.flag                        IN ('A','C')
      UNION ALL
      -- Carton Inventory i.e. VERF_SRL_NBR_TRACK.STAT_CODE = 70
      SELECT
        /*+ALL_ROWS*/
        pt.tran_type,
        pt.tran_code,
        pt.tran_nbr,
        pt.pix_seq_nbr,
        10,
        srl.srl_nbr,
        srl.srl_seq_nbr,
        g_mod_date_time create_date_time,
        g_mod_date_time mod_date_time,
        g_user_id user_id,
        pt.pix_tran_id
      FROM gen605_srl_nbr_track_gtt srl
      INNER JOIN pix_tran pt
      ON srl.item_id = pt.item_id
      INNER JOIN gen605_wm_inventory_gtt ca
      ON
        /*srl.carton_nbr = ca.tc_lpn_id AND */
        srl.lpn_detail_id = ca.lpn_detail_id
      INNER JOIN gen605_sku_attr_gtt si
      ON ( si.sku_attr_id                  = ca.sku_attr_id
      AND si.item_id                       = pt.item_id
      AND COALESCE (si.invn_type, ' ')     = COALESCE (pt.invn_type, ' ')
      AND COALESCE (si.prod_stat, ' ')     = COALESCE (pt.prod_stat, ' ')
      AND COALESCE (si.batch_nbr, ' ')     = COALESCE (pt.batch_nbr, ' ')
      AND COALESCE (si.sku_attr_1, ' ')    = COALESCE (pt.sku_attr_1, ' ')
      AND COALESCE (si.sku_attr_2, ' ')    = COALESCE (pt.sku_attr_2, ' ')
      AND COALESCE (si.sku_attr_3, ' ')    = COALESCE (pt.sku_attr_3, ' ')
      AND COALESCE (si.sku_attr_4, ' ')    = COALESCE (pt.sku_attr_4, ' ')
      AND COALESCE (si.sku_attr_5, ' ')    = COALESCE (pt.sku_attr_5, ' ')
      AND COALESCE (si.cntry_of_orgn, ' ') = COALESCE (pt.cntry_of_orgn, ' ') )
      WHERE srl.stat_code                  = 70
      AND pt.proc_stat_code                = 10
      AND pt.create_date_time              = g_mod_date_time
      AND pt.tran_type                     = '605'
      AND pt.tran_code                     = '97'
      AND pt.tran_nbr                      = g_tran_nbr
      AND ca.flag                          = 'O'
      ) alloc_srl_pix log errors (TO_CHAR(sysdate));
    log('Generated Summary Level Allocable SRL_PIX_TRAN Records (TRAN_CODE = 97).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 2);
    -- generate serial pix for AXX/CXX
    generate_actv_cpick_srlpix;
  ELSE
    -- SRL_PIX_TRAN for Allocable Inventory
    INSERT
    INTO srl_pix_tran
      (
        srl_pix_tran_id,
        tran_type,
        tran_code,
        tran_nbr,
        pix_seq_nbr,
        proc_stat_code,
        srl_nbr,
        seq_nbr,
        create_date_time,
        mod_date_time,
        user_id,
        pix_tran_id
      )
    -- Case Inventory i.e. VERF_SRL_NBR_TRACK.STAT_CODE = 10
    SELECT srl_pix_tran_id_seq.nextval,
      tran_type,
      tran_code,
      tran_nbr,
      pix_seq_nbr,
      proc_stat_code,
      srl_nbr,
      srl_seq_nbr,
      create_date_time,
      mod_date_time,
      user_id,
      pix_tran_id
    FROM
      (SELECT
        /*+ALL_ROWS*/
        pt.tran_type,
        pt.tran_code,
        pt.tran_nbr,
        pt.pix_seq_nbr,
        10 proc_stat_code,
        srl.srl_nbr,
        srl.srl_seq_nbr,
        g_mod_date_time create_date_time,
        g_mod_date_time mod_date_time,
        g_user_id user_id,
        pt.pix_tran_id
      FROM gen605_srl_nbr_track_gtt srl
      INNER JOIN pix_tran pt
      ON srl.item_id = pt.item_id
      INNER JOIN gen605_wm_inventory_gtt cd
      ON
        /*srl.case_nbr = cd.tc_lpn_id AND */
        srl.lpn_detail_id = cd.lpn_detail_id
      INNER JOIN gen605_sku_attr_gtt si
      ON ( si.sku_attr_id                  = cd.sku_attr_id
      AND si.item_id                       = pt.item_id
      AND COALESCE (si.invn_type, ' ')     = COALESCE (pt.invn_type, ' ')
      AND COALESCE (si.prod_stat, ' ')     = COALESCE (pt.prod_stat, ' ')
      AND COALESCE (si.batch_nbr, ' ')     = COALESCE (pt.batch_nbr, ' ')
      AND COALESCE (si.sku_attr_1, ' ')    = COALESCE (pt.sku_attr_1, ' ')
      AND COALESCE (si.sku_attr_2, ' ')    = COALESCE (pt.sku_attr_2, ' ')
      AND COALESCE (si.sku_attr_3, ' ')    = COALESCE (pt.sku_attr_3, ' ')
      AND COALESCE (si.sku_attr_4, ' ')    = COALESCE (pt.sku_attr_4, ' ')
      AND COALESCE (si.sku_attr_5, ' ')    = COALESCE (pt.sku_attr_5, ' ')
      AND COALESCE (si.cntry_of_orgn, ' ') = COALESCE (pt.cntry_of_orgn, ' ') )
      WHERE srl.stat_code                  = 10
      AND pt.proc_stat_code                = 10
      AND pt.create_date_time              = g_mod_date_time
      AND pt.tran_type                     = '605'
      AND pt.tran_code                     = '00'
      AND pt.tran_nbr                      = g_tran_nbr
      AND cd.flag                          = 'I'
      AND NOT EXISTS
        (SELECT 1
        FROM gen605_lpn_lock_gtt Cl2
        WHERE cl2.lpn_id                    = srl.lpn_id
        AND COALESCE(cl2.misc_flags_2, 'Z') = 'N'
        )
      UNION ALL
      -- Transitional Inventory i.e. VERF_SRL_NBR_TRACK.STAT_CODE = 20
      SELECT
        /*+ALL_ROWS*/
        pt.tran_type,
        pt.tran_code,
        pt.tran_nbr,
        pt.pix_seq_nbr,
        10,
        srl.srl_nbr,
        srl.srl_seq_nbr,
        g_mod_date_time create_date_time,
        g_mod_date_time mod_date_time,
        g_user_id user_id,
        pt.pix_tran_id
      FROM gen605_srl_nbr_track_gtt srl
      INNER JOIN pix_tran pt
      ON srl.item_id = pt.item_id
      INNER JOIN gen605_wm_inventory_gtt ti
      ON ti.wm_inventory_id = srl.wm_inventory_id
      AND ti.allocable      = 1
      INNER JOIN gen605_sku_attr_gtt si
      ON ( si.sku_attr_id                  = ti.sku_attr_id
      AND si.item_id                       = pt.item_id
      AND COALESCE (si.invn_type, ' ')     = COALESCE (pt.invn_type, ' ')
      AND COALESCE (si.prod_stat, ' ')     = COALESCE (pt.prod_stat, ' ')
      AND COALESCE (si.batch_nbr, ' ')     = COALESCE (pt.batch_nbr, ' ')
      AND COALESCE (si.sku_attr_1, ' ')    = COALESCE (pt.sku_attr_1, ' ')
      AND COALESCE (si.sku_attr_2, ' ')    = COALESCE (pt.sku_attr_2, ' ')
      AND COALESCE (si.sku_attr_3, ' ')    = COALESCE (pt.sku_attr_3, ' ')
      AND COALESCE (si.sku_attr_4, ' ')    = COALESCE (pt.sku_attr_4, ' ')
      AND COALESCE (si.sku_attr_5, ' ')    = COALESCE (pt.sku_attr_5, ' ')
      AND COALESCE (si.cntry_of_orgn, ' ') = COALESCE (pt.cntry_of_orgn, ' ') )
      WHERE srl.stat_code                  = 20
      AND pt.proc_stat_code                = 10
      AND pt.create_date_time              = g_mod_date_time
      AND pt.tran_type                     = '605'
      AND pt.tran_code                     = '00'
      AND pt.tran_nbr                      = g_tran_nbr
      AND ti.flag                          = 'T'
      -- Active/Case Pick Inventory i.e. VERF_SRL_NBR_TRACK.STAT_CODE = 30/40
      UNION ALL
      SELECT
        /*+ALL_ROWS*/
        pt.tran_type,
        pt.tran_code,
        pt.tran_nbr,
        pt.pix_seq_nbr,
        10,
        srl.srl_nbr,
        srl.srl_seq_nbr,
        g_mod_date_time create_date_time,
        g_mod_date_time mod_date_time,
        g_user_id user_id,
        pt.pix_tran_id
      FROM gen605_srl_nbr_track_gtt srl
      INNER JOIN pix_tran pt
      ON srl.item_id = pt.item_id
      INNER JOIN gen605_wm_inventory_gtt pld
      ON pld.wm_inventory_id = srl.wm_inventory_id
        /*pld.location_id = srl.locn_id AND pld.location_dtl_id = srl.locn_seq_nbr*/
      INNER JOIN gen605_sku_attr_gtt si
      ON ( si.sku_attr_id                  = pld.sku_attr_id
      AND si.item_id                       = pt.item_id
      AND COALESCE (si.invn_type, ' ')     = COALESCE (pt.invn_type, ' ')
      AND COALESCE (si.prod_stat, ' ')     = COALESCE (pt.prod_stat, ' ')
      AND COALESCE (si.batch_nbr, ' ')     = COALESCE (pt.batch_nbr, ' ')
      AND COALESCE (si.sku_attr_1, ' ')    = COALESCE (pt.sku_attr_1, ' ')
      AND COALESCE (si.sku_attr_2, ' ')    = COALESCE (pt.sku_attr_2, ' ')
      AND COALESCE (si.sku_attr_3, ' ')    = COALESCE (pt.sku_attr_3, ' ')
      AND COALESCE (si.sku_attr_4, ' ')    = COALESCE (pt.sku_attr_4, ' ')
      AND COALESCE (si.sku_attr_5, ' ')    = COALESCE (pt.sku_attr_5, ' ')
      AND COALESCE (si.cntry_of_orgn, ' ') = COALESCE (pt.cntry_of_orgn, ' ') )
      WHERE (srl.stat_code                 = 30
      OR srl.stat_code                     = 40)
      AND pt.proc_stat_code                = 10
      AND pt.create_date_time              = g_mod_date_time
      AND pt.tran_type                     = '605'
      AND pt.tran_code                     = '00'
      AND pt.tran_nbr                      = g_tran_nbr
      AND pld.flag                        IN ('A','C')
      UNION ALL
      -- Carton Inventory i.e. VERF_SRL_NBR_TRACK.STAT_CODE = 70
      SELECT
        /*+ALL_ROWS*/
        pt.tran_type,
        pt.tran_code,
        pt.tran_nbr,
        pt.pix_seq_nbr,
        10,
        srl.srl_nbr,
        srl.srl_seq_nbr,
        g_mod_date_time create_date_time,
        g_mod_date_time mod_date_time,
        g_user_id user_id,
        pt.pix_tran_id
      FROM gen605_srl_nbr_track_gtt srl
      INNER JOIN pix_tran pt
      ON srl.item_id = pt.item_id
      INNER JOIN gen605_wm_inventory_gtt ca
      ON
        /*srl.carton_nbr = ca.tc_lpn_id AND */
        srl.lpn_detail_id = ca.lpn_detail_id
      INNER JOIN gen605_sku_attr_gtt si
      ON ( si.sku_attr_id                  = ca.sku_attr_id
      AND si.item_id                       = pt.item_id
      AND COALESCE (si.invn_type, ' ')     = COALESCE (pt.invn_type, ' ')
      AND COALESCE (si.prod_stat, ' ')     = COALESCE (pt.prod_stat, ' ')
      AND COALESCE (si.batch_nbr, ' ')     = COALESCE (pt.batch_nbr, ' ')
      AND COALESCE (si.sku_attr_1, ' ')    = COALESCE (pt.sku_attr_1, ' ')
      AND COALESCE (si.sku_attr_2, ' ')    = COALESCE (pt.sku_attr_2, ' ')
      AND COALESCE (si.sku_attr_3, ' ')    = COALESCE (pt.sku_attr_3, ' ')
      AND COALESCE (si.sku_attr_4, ' ')    = COALESCE (pt.sku_attr_4, ' ')
      AND COALESCE (si.sku_attr_5, ' ')    = COALESCE (pt.sku_attr_5, ' ')
      AND COALESCE (si.cntry_of_orgn, ' ') = COALESCE (pt.cntry_of_orgn, ' ') )
      WHERE srl.stat_code                  = 70
      AND pt.proc_stat_code                = 10
      AND pt.create_date_time              = g_mod_date_time
      AND pt.tran_type                     = '605'
      AND pt.tran_code                     = '00'
      AND ca.flag                          = 'O'
      AND pt.tran_nbr                      = g_tran_nbr
      ) alloc_srl_pix log errors (TO_CHAR(sysdate));
    log('Generated Summary Level Allocable SRL_PIX_TRAN Records (TRAN_CODE = 00).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 2);
  END IF;
  log('Finished Summary Level SRL_PIX_TRAN generation', TRUE, 1);
END generate_summary_srlpix;
PROCEDURE generate_unalloc_invn_lock_pix
AS
  v_pix_insert     VARCHAR2 (5000);
  v_proc_stat_code NUMBER;
  v_xml_group_attr VARCHAR2(10);
  v_tran_code pix_tran.tran_code%type;
BEGIN
  log('started generation of unalloc lock pix', true);
  IF g_caselock_flag THEN
    manh_get_pix_config_data ('605','LXX',NULL,g_company_id,v_proc_stat_code,v_xml_group_attr);
    IF (v_proc_stat_code   != 91) THEN
      IF (v_xml_group_attr IS NULL) THEN
        v_xml_group_attr   := NULL;
      ELSE
        v_xml_group_attr := v_xml_group_attr;
      END IF;
      v_Pix_Insert := 'INSERT INTO pix_tran(tc_company_id, facility_id, company_code, tran_type, tran_code, tran_nbr, sys_user_id, pix_seq_nbr, proc_stat_code,xml_group_id, create_date_time, mod_date_time, user_id, ' || 'item_name, item_id, season, season_yr, style, style_sfx, color, color_sfx, sec_dim, qual, size_desc, invn_adjmt_qty, wt_adjmt_qty, invn_adjmt_type, whse' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias.', '')
        ELSE
          ''
        END) ||
      (
        CASE
        WHEN g_ConsPrtyDate_Flag THEN
          ', ref_code_id_1, ref_field_1'
        ELSE
          ''
        END) || ') SELECT :company_id, :facility_id, :company_code, ''605'', concat(''L'',pcd.inventory_lock_code), :tran_nbr, :user_id, row_number() over (order by pcd.item_id) + :count, ' || ':v_proc_stat_code,:v_xml_group_attr, :create_date_time, :mod_date_time, :user_id, ' || 'psku.item_name, pcd.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, pcd.actl_qty, nvl(pcd.wt_adjmt_qty, 0), NULL, :g_warehouse ' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'pcd')
        ELSE
          ''
        END) ||
      (
        CASE
        WHEN g_ConsPrtyDate_Flag THEN
          ', ''93'', pcd.cons_prty_date '
        ELSE
          ''
        END) || 'FROM (SELECT si.item_id, cl.inventory_lock_code, SUM(cd.on_hand_qty) AS actl_qty, ' || ' SUM((case when cd.single_line_lpn =''Y'' then cd.actl_weight else cd.on_hand_qty end) * ' || ' (case when cd.single_line_lpn =''Y'' then 1 else nvl(pku.unit_weight, 0) end)) AS wt_adjmt_qty ' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'si')
        ELSE
          ''
        END) ||
      (
        CASE
        WHEN g_ConsPrtyDate_Flag THEN
          ', substr(tochar(cd.CONSUMPTION_PRIORITY_DTTM,''YYYY-MM-DD''), 1,10) AS cons_prty_date '
        ELSE
          ''
        END) || ' FROM gen605_wm_inventory_gtt cd INNER JOIN gen605_sku_attr_gtt si ON cd.sku_attr_id = si.sku_attr_id inner join gen605_sku_gtt pku ON pku.item_id = si.item_id ' || 'INNER JOIN gen605_lpn_lock_gtt cl ON cl.lpn_id = cd.lpn_id AND cd.ALLOCABLE = 0 ' || 'WHERE cl.misc_flags_34 = (SELECT MIN(to_number(cl2.misc_flags_34,99)) FROM gen605_lpn_lock_gtt cl2 WHERE cl2.lpn_id = cl.lpn_id) ' ||
      (
        CASE
        WHEN g_ConsPrtyDate_Flag THEN
          ' AND cd.lpn_facility_status >= 10 AND cd.lpn_facility_status < 90 and cd.flag = ''I'''
        ELSE
          ' and cd.flag = ''I'''
        END) || ' GROUP BY si.item_id, cl.inventory_lock_code' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'si')
        ELSE
          ''
        END) ||
      (
        CASE
        WHEN g_consprtydate_flag THEN
          ', substr(tochar(cd.CONSUMPTION_PRIORITY_DTTM,''YYYY-MM-DD''), 1,10)'
        ELSE
          ''
        END) || ') pcd INNER JOIN gen605_sku_gtt psku ON psku.item_id = pcd.item_id GROUP BY psku.item_name, pcd.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, pcd.actl_qty, pcd.inventory_lock_code, pcd.wt_adjmt_qty ' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'pcd')
        ELSE
          ''
        END) ||
      (
        CASE
        WHEN g_consprtydate_flag THEN
          ', pcd.cons_prty_date '
        ELSE
          ''
        END);
      v_Pix_Insert := v_Pix_Insert || ' log errors (to_char(sysdate))';
      EXECUTE IMMEDIATE v_pix_insert USING g_company_id,
      g_facility_id,
      g_company_code,
      g_tran_nbr,
      g_user_id,
      g_count,
      v_proc_stat_code,
      v_xml_group_attr,
      g_mod_date_time,
      g_mod_date_time,
      g_user_id,
      g_warehouse;
      g_count := g_count + SQL%ROWCOUNT;
    END IF;
    log('Generated Un-allocable Case Inventory Summary by CASE_LOCK.INVN_LOCK_CODE Pix Records (TRAN_CODE = Lxx).  PIX Count = ' || sql%rowcount, true, 1);
  END IF;
  IF g_CasePick_Locn_Lock_flag THEN
    manh_get_pix_config_data ('605','CXX',NULL,g_company_id,v_proc_stat_code,v_xml_group_attr);
    IF (v_proc_stat_code  != 91) THEN
      IF v_xml_group_attr IS NULL THEN
        v_xml_group_attr  := NULL;
      ELSE
        v_xml_group_attr := v_xml_group_attr;
      END IF;
      v_Pix_Insert := 'INSERT INTO pix_tran(tc_company_id, facility_id, company_code, tran_type, tran_code, tran_nbr, sys_user_id, pix_seq_nbr, proc_stat_code,xml_group_id, create_date_time, mod_date_time, user_id, ' || 'item_name, item_id, season, season_yr, style, style_sfx, color, color_sfx, sec_dim, qual, size_desc, invn_adjmt_qty, invn_adjmt_type, whse' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias.', '')
        ELSE
          ''
        END) || ') SELECT /*+ALL_ROWS*/ :company_id, :facility_id, :company_code, ''605'', concat(''C'',to_char(ci.pikng_lock_code)), :tran_nbr, :user_id, (row_number() over (order by ci.item_id)+:counter), ' || ':v_proc_stat_code, :v_xml_group_attr,:create_date_time, :mod_date_time, :user_id, ' || 'psku.item_name, ci.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, ci.actl_invn_units, NULL, :g_warehouse ' ||
      (
        CASE
        WHEN g_group_skuattr_flag THEN
          REPLACE(g_sku_attributes, 'alias', 'ci')
        ELSE
          ''
        END) || 'FROM (SELECT /*+ no_push_pred */ si.item_id, pd.pikng_lock_code, sum(t.on_hand_qty) AS actl_invn_units ' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'si')
        ELSE
          ''
        END) || ' FROM gen605_wm_inventory_gtt t INNER JOIN gen605_sku_attr_gtt si ON t.sku_attr_id = si.sku_attr_id ' || ' join pick_locn_dtl pd on pd.pick_locn_dtl_id = t.location_dtl_id ' || ' WHERE t.allocable = 0 and  t.flag = ''C'' ' || 'GROUP BY si.item_id, pd.pikng_lock_code ' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'si')
        ELSE
          ''
        END) || ') ci INNER JOIN gen605_sku_gtt psku ON psku.item_id = ci.item_id ' ||
      (
        CASE
        WHEN g_Zero_Invn_Flag THEN
          ' '
        ELSE
          ' where ci.actl_invn_units <> 0 '
        END) || ' GROUP BY psku.item_name, ci.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, ci.actl_invn_units, ci.pikng_lock_code' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'ci')
        ELSE
          ''
        END);
      v_Pix_Insert := v_Pix_Insert || ' log errors (to_char(sysdate))';
      EXECUTE IMMEDIATE v_pix_insert USING g_company_id,
      g_facility_id,
      g_company_code,
      g_tran_nbr,
      g_user_id,
      g_count,
      v_proc_stat_code,
      v_xml_group_attr,
      g_mod_date_time,
      g_mod_date_time,
      g_user_id,
      g_warehouse;
      g_count := g_count + SQL%ROWCOUNT;
    END IF;
    log('Generated casepick lock code Inventory PIX Records.  PIX Count = ' || SQL%ROWCOUNT, TRUE, 1);
  END IF;
  IF g_Active_Locn_Lock_flag THEN
    manh_get_pix_config_data ('605','AXX',NULL,g_company_id,v_proc_stat_code,v_xml_group_attr);
    IF (v_proc_stat_code  != 91) THEN
      IF v_xml_group_attr IS NULL THEN
        v_xml_group_attr  := NULL;
      ELSE
        v_xml_group_attr := v_xml_group_attr;
      END IF;
      v_Pix_Insert := 'INSERT INTO pix_tran(tc_company_id, facility_id, company_code, tran_type, tran_code, tran_nbr, sys_user_id, pix_seq_nbr, proc_stat_code,xml_group_id, create_date_time, mod_date_time, user_id, ' || 'item_name, item_id, season, season_yr, style, style_sfx, color, color_sfx, sec_dim, qual, size_desc, invn_adjmt_qty, invn_adjmt_type, whse' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias.', '')
        ELSE
          ''
        END) || ') SELECT /*+ALL_ROWS*/ :company_id, :facility_id, :company_code, ''605'', concat(''A'',to_char(ai.pikng_lock_code)), :tran_nbr, :user_id, (row_number() over (order by ai.item_id)+:counter), ' || ':v_proc_stat_code, :v_xml_group_attr,:create_date_time, :mod_date_time, :user_id, ' || 'psku.item_name, ai.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, ai.actl_invn_units, NULL, :g_warehouse ' ||
      (
        CASE
        WHEN g_group_skuattr_flag THEN
          REPLACE(g_sku_attributes, 'alias', 'ai')
        ELSE
          ''
        END) || 'FROM (SELECT /*+ no_push_pred */ si.item_id, pd.pikng_lock_code, sum(t.on_hand_qty) AS actl_invn_units ' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'si')
        ELSE
          ''
        END) || ' FROM gen605_wm_inventory_gtt t INNER JOIN gen605_sku_attr_gtt si ON t.sku_attr_id = si.sku_attr_id ' || ' join pick_locn_dtl pd on pd.pick_locn_dtl_id = t.location_dtl_id ' || ' WHERE t.allocable = 0 and  t.flag = ''A'' ' || 'GROUP BY si.item_id, pd.pikng_lock_code ' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'si')
        ELSE
          ''
        END) || ') ai INNER JOIN gen605_sku_gtt psku ON psku.item_id = ai.item_id ' ||
      (
        CASE
        WHEN g_Zero_Invn_Flag THEN
          ' '
        ELSE
          ' where ai.actl_invn_units <> 0 '
        END) || ' GROUP BY psku.item_name, ai.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, ai.actl_invn_units, ai.pikng_lock_code' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'ai')
        ELSE
          ''
        END);
      v_Pix_Insert := v_Pix_Insert || ' log errors (to_char(sysdate))';
      EXECUTE IMMEDIATE v_pix_insert USING g_company_id,
      g_facility_id,
      g_company_code,
      g_tran_nbr,
      g_user_id,
      g_count,
      v_proc_stat_code,
      v_xml_group_attr,
      g_mod_date_time,
      g_mod_date_time,
      g_user_id,
      g_warehouse;
      g_count := g_count + SQL%ROWCOUNT;
    END IF;
    log('Generated Active location lock code Inventory PIX Records (AXX).  PIX Count = ' || sql%rowcount, true, 1);
  END IF;
  IF g_TransInvn_Flag THEN
    manh_get_pix_config_data ('605','TXX',NULL,g_company_id,v_proc_stat_code,v_xml_group_attr);
    IF (v_proc_stat_code  != 91) THEN
      IF v_xml_group_attr IS NULL THEN
        v_xml_group_attr  := NULL;
      ELSE
        v_xml_group_attr := v_xml_group_attr;
      END IF;
      v_Pix_Insert := 'INSERT INTO pix_tran(tc_company_id, facility_id, company_code, tran_type, tran_code, tran_nbr, sys_user_id, pix_seq_nbr, proc_stat_code,xml_group_id, create_date_time, mod_date_time, user_id, ' || 'item_name, item_id, season, season_yr, style, style_sfx, color, color_sfx, sec_dim, qual, size_desc, invn_adjmt_qty, invn_adjmt_type, whse' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias.', '')
        ELSE
          ''
        END) || ') SELECT /*+ALL_ROWS*/ :company_id, :facility_id, :company_code, ''605'', concat(''T'',to_char(ti.transitional_inventory_type)), :tran_nbr, :user_id, (row_number() over (order by ti.item_id)+:counter), ' || ':v_proc_stat_code, :v_xml_group_attr,:create_date_time, :mod_date_time, :user_id, ' || 'psku.item_name, ti.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, ti.actl_invn_units, NULL, :g_warehouse ' ||
      (
        CASE
        WHEN g_group_skuattr_flag THEN
          REPLACE(g_sku_attributes, 'alias', 'ti')
        ELSE
          ''
        END) || 'FROM (SELECT /*+ no_push_pred */ si.item_id, t.transitional_inventory_type, sum(t.on_hand_qty) AS actl_invn_units ' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'si')
        ELSE
          ''
        END) || ' FROM gen605_wm_inventory_gtt t INNER JOIN gen605_sku_attr_gtt si ON t.sku_attr_id = si.sku_attr_id WHERE t.allocable = 0 and  t.flag = ''T'' ' || 'GROUP BY si.item_id, t.transitional_inventory_type' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'si')
        ELSE
          ''
        END) || ') ti INNER JOIN gen605_sku_gtt psku ON psku.item_id = ti.item_id GROUP BY psku.item_name, ti.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, ti.actl_invn_units, ti.transitional_inventory_type' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'ti')
        ELSE
          ''
        END);
      v_Pix_Insert := v_Pix_Insert || ' log errors (to_char(sysdate))';
      EXECUTE IMMEDIATE v_pix_insert USING g_company_id,
      g_facility_id,
      g_company_code,
      g_tran_nbr,
      g_user_id,
      g_count,
      v_proc_stat_code,
      v_xml_group_attr,
      g_mod_date_time,
      g_mod_date_time,
      g_user_id,
      g_warehouse;
      g_count := g_count + SQL%ROWCOUNT;
    END IF;
    log('Generated Un-allocable Transitional Inventory by TRANS_INVN_TYPE Pix Records (TRAN_CODE = Txx).  PIX Count = ' || sql%rowcount, true, 1);
  END IF;
EXCEPTION
WHEN OTHERS THEN
  raise;
END generate_unalloc_invn_lock_pix;
PROCEDURE generate_summary_pix
AS
  v_pix_insert     VARCHAR2 (5000);
  v_proc_stat_code NUMBER;
  v_xml_group_attr VARCHAR2(10);
  v_tran_code pix_tran.tran_code%type;
type g_pix_tab
IS
  TABLE OF pix_tran%rowtype INDEX BY pls_integer;
  v_pix_tab g_pix_tab;
  v_ref_for_item NUMBER;
  v_consmd_act_lpn_qty NUMBER;
  v_wm49_flag    VARCHAR2 (1);
BEGIN
  log('Started generation of Summary Level PIX', TRUE);
  IF g_unalloc_invn_flag THEN
    manh_get_pix_config_data ('605','97',NULL,g_company_id,v_proc_stat_code,v_xml_group_attr);
    IF (v_proc_stat_code  != 91) THEN
      IF v_xml_group_attr IS NULL THEN
        v_xml_group_attr  := NULL;
      ELSE
        v_xml_group_attr := v_xml_group_attr;
      END IF;
      /*** Allocable ***/
      /* ************************* Changes for WM49 start by Mrinal, PSO DBA, MA-I ******************** */
      /* WM49 Mod Flag Check */
      BEGIN
        SELECT SUBSTR (misc_flags, 1, 1)
        INTO v_wm49_flag
        FROM sys_code
        WHERE rec_type = 'C'
        AND code_type  = 'PIX'
        AND code_id    = '001';
      EXCEPTION
      WHEN OTHERS THEN
        wm_log('Error : '||sqlerrm||' , '||SQLCODE);
      END;
      /* Added for WM49 Alocatable Changes */
      IF v_wm49_flag  = 'Y' THEN
        v_pix_insert := 'INSERT INTO pix_tran(ref_code_id_10,tc_company_id, facility_id, company_code, tran_type, tran_code, tran_nbr, sys_user_id, pix_seq_nbr, proc_stat_code, xml_group_id,create_date_time, mod_date_time, user_id, ' || 'item_name, item_id, season, season_yr, style, style_sfx, color, color_sfx, sec_dim, qual, size_desc, invn_adjmt_qty, invn_adjmt_type, whse, wt_adjmt_qty' ||
        (
          CASE
          WHEN g_group_skuattr_flag THEN
            REPLACE (g_sku_attributes, 'alias.', '')
          ELSE
            ''
          END) || ') ' || ' with wt_adj_qty_tmp as' || ' ( ' || '     SELECT iv.item_id, iv.sku_attr_id, SUM (iv.wt) wt' || '      FROM (SELECT wi.item_id, wi.sku_attr_id,' || '     ( (CASE WHEN wi.single_line_lpn = ''Y'' THEN wi.actl_weight ' || '        ELSE wi.on_hand_qty END) * (CASE WHEN wi.single_line_lpn = ''Y'' THEN 1 ' || '        ELSE cd.unit_weight END)) AS wt ' || '       FROM    gen605_wm_inventory_gtt wi ' || '           JOIN gen605_sku_gtt cd ' || '           ON wi.item_id = cd.item_id ' || '            WHERE wi.allocable = 1 AND wi.flag IN (''A'', ''I'') ' || '           AND NOT EXISTS ' || '              (SELECT 1 ' || '                 FROM gen605_lpn_lock_gtt cl ' || '                 WHERE cl.lpn_id = wi.lpn_id ' || '                       AND COALESCE (cl.misc_flags_2, ''Z'') = ''N'') ' || '         UNION ALL ' || '         SELECT wi.item_id, wi.sku_attr_id, lcw.catch_weight wt ' || '         FROM    gen605_lpn_catch_wt_gtt lcw ' ||
        '         JOIN  gen605_wm_inventory_gtt wi ' || '         ON wi.lpn_id = lcw.lpn_id ' || '         AND lcw.lpn_detail_id = wi.lpn_detail_id ' || '         WHERE wi.flag = ''O'' ' || '         UNION ALL ' || '         SELECT wi.item_id,wi.sku_attr_id, ' || '         (wi.on_hand_qty * (CASE WHEN cd.item_avg_wt = 0 THEN cd.unit_weight ' || '           ELSE cd.item_avg_wt END)) AS wt ' || '         FROM    gen605_wm_inventory_gtt wi ' || '         JOIN gen605_sku_gtt cd ' || '         ON wi.item_id = cd.item_id ' || '         WHERE wi.flag IN (''C'', ''O'', ''T'') ' || '         AND NOT EXISTS ' || '           (SELECT 1 ' || '              FROM gen605_lpn_catch_wt_gtt ccw ' || '              WHERE ccw.lpn_id = wi.lpn_id)) iv GROUP BY iv.item_id, iv.sku_attr_id ' || ' ) ' || ' ' ||
        ' SELECT ''997'',:company_id, :facility_id, :company_code, ''605'', ''97'', :tran_nbr, :user_id, row_number() over (order by psi.item_id) + :count, :v_proc_stat_code, :v_xml_group_attr,:create_date_time, :mod_date_time, :user_id, ' || 'psku.item_name, psi.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, sum(NVL(psi.sku_invn_qty,0)), NULL, :g_warehouse , sum(nvl(psi.wt, 0)) ' ||
        (
          CASE
          WHEN g_group_skuattr_flag THEN
            REPLACE (g_sku_attributes, 'alias', 'psi')
          ELSE
            ''
          END) || 'FROM (SELECT /*+ no_push_pred */ sa.item_id, NVL(COALESCE(SUM(si.on_hand_qty - si.qty_not_alloc),0),0) AS sku_invn_qty, wt_adj_qty_tmp.wt ' ||
        (
          CASE
          WHEN g_group_skuattr_flag THEN
            REPLACE (g_sku_attributes, 'alias', 'sa')
          ELSE
            ''
          END) || ' FROM gen605_wm_inventory_gtt si ' ||
        (
          CASE
          WHEN g_zero_invn_flag THEN
            'RIGHT JOIN'
          ELSE
            'INNER JOIN'
          END) || ' gen605_sku_attr_gtt sa ON si.sku_attr_id = sa.sku_attr_id ' || ' left join wt_adj_qty_tmp on wt_adj_qty_tmp.item_id = si.item_id and wt_adj_qty_tmp.sku_attr_id = si.sku_attr_id ' ||
        (
          CASE
          WHEN g_zero_invn_flag THEN
            ' '
          ELSE
            ' where si.on_hand_qty <> 0 '
          END) || 'GROUP BY sa.item_id, wt_adj_qty_tmp.wt ' ||
        (
          CASE
          WHEN g_group_skuattr_flag THEN
            REPLACE (g_sku_attributes, 'alias', 'sa')
          ELSE
            ''
          END) || ') psi INNER JOIN gen605_sku_gtt psku ON psku.item_id = psi.item_id ' ||
        (
          CASE
          WHEN g_zero_invn_flag THEN
            ' '
          ELSE
            ' where psi.sku_invn_qty <> 0 '
          END) || ' group by psku.item_name, psi.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc  ' ||
        (
          CASE
          WHEN g_group_skuattr_flag THEN
            REPLACE (g_sku_attributes, 'alias', 'psi')
          ELSE
            ''
          END);
      ELSE
      IF g_erphosttype = '0' THEN
        v_Pix_Insert  := 'INSERT INTO pix_tran(tc_company_id, facility_id, company_code, tran_type, tran_code, tran_nbr, sys_user_id, pix_seq_nbr, proc_stat_code, xml_group_id,create_date_time, mod_date_time, user_id, ' || 'item_name, item_id, season, season_yr, style, style_sfx, color, color_sfx, sec_dim, qual, size_desc, invn_adjmt_qty, invn_adjmt_type, whse, wt_adjmt_qty' ||
        (
          CASE
          WHEN g_Group_SkuAttr_Flag THEN
            REPLACE(g_Sku_Attributes, 'alias.', '')
          ELSE
            ''
          END) || ') ' || ' with wt_adj_qty_tmp as' || ' ( ' || '     SELECT iv.item_id, iv.sku_attr_id, SUM (iv.wt) wt' || '      FROM (SELECT wi.item_id, wi.sku_attr_id,' || '     ( (CASE WHEN wi.single_line_lpn = ''Y'' THEN wi.actl_weight ' || '        ELSE wi.on_hand_qty END) * (CASE WHEN wi.single_line_lpn = ''Y'' THEN 1 ' || '        ELSE cd.unit_weight END)) AS wt ' || '       FROM    gen605_wm_inventory_gtt wi ' || '           JOIN gen605_sku_gtt cd ' || '           ON wi.item_id = cd.item_id ' || '            WHERE wi.allocable = 1 AND wi.flag IN (''A'', ''I'') ' || '           AND NOT EXISTS ' || '              (SELECT 1 ' || '                 FROM gen605_lpn_lock_gtt cl ' || '                 WHERE cl.lpn_id = wi.lpn_id ' || '                       AND COALESCE (cl.misc_flags_2, ''Z'') = ''N'') ' || '         UNION ALL ' || '         SELECT wi.item_id, wi.sku_attr_id, lcw.catch_weight wt ' || '         FROM    gen605_lpn_catch_wt_gtt lcw ' ||
        '         JOIN  gen605_wm_inventory_gtt wi ' || '         ON wi.lpn_id = lcw.lpn_id ' || '         AND lcw.lpn_detail_id = wi.lpn_detail_id ' || '         WHERE wi.flag = ''O'' ' || '         UNION ALL ' || '         SELECT wi.item_id,wi.sku_attr_id, ' || '         (wi.on_hand_qty * (CASE WHEN cd.item_avg_wt = 0 THEN cd.unit_weight ' || '           ELSE cd.item_avg_wt END)) AS wt ' || '         FROM    gen605_wm_inventory_gtt wi ' || '         JOIN gen605_sku_gtt cd ' || '         ON wi.item_id = cd.item_id ' || '         WHERE wi.flag IN (''C'', ''O'', ''T'') ' || '         AND NOT EXISTS ' || '           (SELECT 1 ' || '              FROM gen605_lpn_catch_wt_gtt ccw ' || '              WHERE ccw.lpn_id = wi.lpn_id)) iv GROUP BY iv.item_id, iv.sku_attr_id ' || ' ) ' || ' ' ||
        ' SELECT :company_id, :facility_id, :company_code, ''605'', ''97'', :tran_nbr, :user_id, row_number() over (order by psi.item_id) + :count, :v_proc_stat_code, :v_xml_group_attr,:create_date_time, :mod_date_time, :user_id, ' || 'psku.item_name, psi.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, sum(NVL(psi.sku_invn_qty,0)), NULL, :g_warehouse , sum(nvl(psi.wt, 0)) ' ||
        (
          CASE
          WHEN g_group_skuattr_flag THEN
            REPLACE(g_sku_attributes, 'alias', 'psi')
          ELSE
            ''
          END) || 'FROM (SELECT /*+ no_push_pred */ sa.item_id, NVL(COALESCE(SUM(si.on_hand_qty - si.qty_not_alloc),0),0) AS sku_invn_qty, wt_adj_qty_tmp.wt ' ||
        (
          CASE
          WHEN g_Group_SkuAttr_Flag THEN
            REPLACE(g_Sku_Attributes, 'alias', 'sa')
          ELSE
            ''
          END) || ' FROM gen605_wm_inventory_gtt si ' ||
        (
          CASE
          WHEN g_Zero_Invn_Flag THEN
            'RIGHT JOIN'
          ELSE
            'INNER JOIN'
          END)||' gen605_sku_attr_gtt sa ON si.sku_attr_id = sa.sku_attr_id ' || ' left join wt_adj_qty_tmp on wt_adj_qty_tmp.item_id = si.item_id and wt_adj_qty_tmp.sku_attr_id = si.sku_attr_id ' ||
        (
          CASE
          WHEN g_Zero_Invn_Flag THEN
            ' '
          ELSE
            ' where si.on_hand_qty <> 0 '
          END) || 'GROUP BY sa.item_id, wt_adj_qty_tmp.wt ' ||
        (
          CASE
          WHEN g_Group_SkuAttr_Flag THEN
            REPLACE(g_Sku_Attributes, 'alias', 'sa')
          ELSE
            ''
          END) || ') psi INNER JOIN gen605_sku_gtt psku ON psku.item_id = psi.item_id ' ||
        (
          CASE
          WHEN g_Zero_Invn_Flag THEN
            ' '
          ELSE
            ' where psi.sku_invn_qty <> 0 '
          END) || ' group by psku.item_name, psi.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc  ' ||
        (
          CASE
          WHEN g_Group_SkuAttr_Flag THEN
            REPLACE(g_Sku_Attributes, 'alias', 'psi')
          ELSE
            ''
          END);
      ELSE
        v_Pix_Insert := 'INSERT INTO pix_tran(tc_company_id, facility_id, company_code, tran_type, tran_code, tran_nbr, sys_user_id, pix_seq_nbr, proc_stat_code,xml_group_id, create_date_time, mod_date_time, user_id, ' || 'item_name, item_id, season, season_yr, style, style_sfx, color, color_sfx, sec_dim, qual, size_desc, invn_adjmt_qty, invn_adjmt_type, whse' ||
        (
          CASE
          WHEN g_Group_SkuAttr_Flag THEN
            REPLACE(g_Sku_Attributes, 'alias.', '')
          ELSE
            ''
          END) || ') SELECT :company_id, :facility_id, :company_code,  ''605'', ''97'', :tran_nbr, :user_id, row_number() over (order by psi.item_id) + :count, :v_proc_stat_code, :v_xml_group_attr,:create_date_time, :mod_date_time, :user_id, ' || 'psku.item_name, psi.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, NVL(psi.sku_invn_qty,0), NULL, :g_warehouse ' ||
        (
          CASE
          WHEN g_group_skuattr_flag THEN
            REPLACE(g_sku_attributes, 'alias', 'psi')
          ELSE
            ''
          END) || 'FROM (SELECT /*+ no_push_pred */ sa.item_id, NVL((SUM(si.on_hand_qty - si.qty_not_alloc) - COALESCE(SUM(pd.pkt_qty), 0)),0) AS sku_invn_qty' ||
        (
          CASE
          WHEN g_Group_SkuAttr_Flag THEN
            REPLACE(g_Sku_Attributes, 'alias', 'sa')
          ELSE
            ''
          END) || ' FROM gen605_wm_inventory_gtt si INNER JOIN gen605_sku_attr_gtt sa ON si.sku_attr_id = sa.sku_attr_id ' || 'LEFT JOIN (SELECT sku_attr_id, SUM(pkt_qty) AS pkt_qty FROM gen605_pkt_dtl_gtt GROUP BY sku_attr_id) PD ON si.sku_attr_id = pd.sku_attr_id ' ||
        (
          CASE
          WHEN g_Zero_Invn_Flag THEN
            ' '
          ELSE
            ' where si.on_hand_qty <> 0 '
          END) || 'GROUP BY sa.item_id' ||
        (
          CASE
          WHEN g_Group_SkuAttr_Flag THEN
            REPLACE(g_Sku_Attributes, 'alias', 'sa')
          ELSE
            ''
          END) || ') psi INNER JOIN gen605_sku_gtt psku ON psku.item_id = psi.item_id ' ||
        (
          CASE
          WHEN g_Zero_Invn_Flag THEN
            ' '
          ELSE
            ' where psi.sku_invn_qty <> 0 '
          END) || ' group by psku.item_name, psi.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, psi.sku_invn_qty ' ||
        (
          CASE
          WHEN g_Group_SkuAttr_Flag THEN
            REPLACE(g_Sku_Attributes, 'alias', 'psi')
          ELSE
            ''
          END);
      END IF;
      END IF;
      v_Pix_Insert := v_Pix_Insert || ' log errors (to_char(sysdate))';
      EXECUTE IMMEDIATE v_pix_insert USING g_company_id,
      g_facility_id,
      g_company_code,
      g_tran_nbr,
      g_user_id,
      g_count,
      v_proc_stat_code,
      v_xml_group_attr,
      g_mod_date_time,
      g_mod_date_time,
      g_user_id,
      g_warehouse;
      g_count := g_count + SQL%ROWCOUNT;
      log('Generated Summary Level Allocable Inventory Pix Records (TRAN_CODE = 97).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 1);
      /* Added for WM49 Alocatable Changes */
      SELECT * bulk collect
      INTO v_pix_tab
      FROM pix_tran
      WHERE ref_code_id_10 = '997'
      AND ref_field_10    IS NULL
	  AND proc_stat_code IN (0, 10)
	  AND tran_nbr = g_tran_nbr;
      FOR i               IN 1 .. v_pix_tab.count
      LOOP
        SELECT COALESCE(SUM (ld.size_value), 0)
        INTO v_ref_for_item
        FROM lpn
        INNER JOIN lpn_detail ld
        ON lpn.lpn_id                      = ld.lpn_id
        AND lpn.inbound_outbound_indicator = 'I'
        AND lpn.lpn_facility_status       >= '10'
        AND lpn.lpn_facility_status        < '95'
        AND lpn.asn_id                    IS NOT NULL
        INNER JOIN wm_inventory wi
        ON ld.lpn_detail_id = wi.lpn_detail_id
        --INNER JOIN wm_inventory win
        And  lpn.lpn_id       = wi.lpn_id
        AND wi.allocatable = 'Y'
        INNER JOIN asn
        ON lpn.asn_id      = asn.asn_id
        AND asn.asn_status < 40
        AND ld.item_id     = v_pix_tab(i).item_id;
		
		SELECT COALESCE(SUM (ld.received_qty), 0)
        INTO v_consmd_act_lpn_qty
        FROM lpn
        INNER JOIN lpn_detail ld
        ON lpn.lpn_id                      = ld.lpn_id
        AND lpn.inbound_outbound_indicator = 'I'
        AND lpn.lpn_facility_status       = '96'
        AND lpn.asn_id                    IS NOT NULL
        INNER JOIN asn
        ON lpn.asn_id      = asn.asn_id
        AND asn.asn_status < 40
        AND ld.item_id     = v_pix_tab(i).item_id;
		
        UPDATE pix_tran
        SET ref_field_10   = (v_ref_for_item + v_consmd_act_lpn_qty)
        WHERE item_id      = v_pix_tab(i).item_id
        AND ref_code_id_10 = '997'
        AND pix_tran_id    = v_pix_tab(i).pix_tran_id;
      END LOOP;
    END IF;
    /*** Un-allocable ***/
    manh_get_pix_config_data ('605','98',NULL,g_company_id,v_proc_stat_code,v_xml_group_attr);
    IF (v_proc_stat_code != 91) THEN
      IF v_xml_group_attr IS NULL THEN
        v_xml_group_attr  := NULL;
      ELSE
        v_xml_group_attr := v_xml_group_attr;
      END IF;
      /* Added for WM49 Unallocatable Changes */
      IF v_wm49_flag  = 'Y' THEN
        v_pix_insert := 'INSERT INTO pix_tran(ref_code_id_10,tc_company_id, facility_id, company_code, tran_type, tran_code, tran_nbr, sys_user_id, pix_seq_nbr, proc_stat_code,xml_group_id,  create_date_time, mod_date_time, user_id, ' || 'item_name, item_id, season, season_yr, style, style_sfx, color, color_sfx, sec_dim, qual, size_desc, invn_adjmt_qty, invn_adjmt_type, whse, wt_adjmt_qty' ||
        (
          CASE
          WHEN g_group_skuattr_flag THEN
            REPLACE (g_sku_attributes, 'alias.', '')
          ELSE
            ''
          END) || ')  ' || ' with wt_adj_qty_tmp as' || ' ( ' || '    SELECT iv.item_id, iv.sku_attr_id, SUM (iv.wt) wt ' || '         FROM (SELECT si.item_id, si.sku_attr_id, ' || '                   ( (CASE WHEN si.single_line_lpn = ''Y'' THEN si.actl_weight ELSE si.on_hand_qty  END)  * (CASE WHEN si.single_line_lpn = ''Y'' THEN 1 ' || '                        ELSE cd.unit_weight  END)) AS wt ' || '                   FROM    gen605_wm_inventory_gtt si ' || '                   JOIN gen605_sku_gtt cd ' || '                 ON si.item_id = cd.item_id ' || '           WHERE si.allocable = 0 AND si.flag IN (''I'') ' || '                 AND EXISTS ' || '                        (SELECT 1 ' || '                           FROM gen605_lpn_lock_gtt cl ' || '                          WHERE cl.lpn_id = si.lpn_id ' || '                                AND COALESCE (cl.misc_flags_2, ''Z'') = ''N'') ' || '           UNION ALL ' || '           SELECT si.item_id, si.sku_attr_id, ' ||
        '                 ( (CASE  WHEN si.single_line_lpn = ''Y'' THEN si.actl_weight  ELSE si.on_hand_qty  END) * (CASE  WHEN si.single_line_lpn = ''Y'' THEN 1 ' || '                   ELSE cd.unit_weight  END)) AS wt ' || '            FROM    gen605_wm_inventory_gtt si ' || '                 JOIN  gen605_sku_gtt cd ' || '                 ON si.item_id = cd.item_id ' || '            WHERE si.allocable = 0 AND si.flag IN (''A'') ' || '            UNION ALL ' || '            SELECT wi.item_id,  wi.sku_attr_id, ' || '                 (wi.on_hand_qty * (CASE WHEN cd.item_avg_wt = 0 THEN cd.unit_weight ' || '                      ELSE cd.item_avg_wt END)) AS wt ' || '            FROM    gen605_wm_inventory_gtt wi ' || '                 JOIN gen605_sku_gtt cd ' || '                 ON wi.item_id = cd.item_id ' || '            WHERE wi.allocable = 0 AND wi.flag IN (''C'')) iv GROUP BY iv.item_id, iv.sku_attr_id ' || ' ) ' || ' ' ||
        ' SELECT ''997'',:company_id, :facility_id, :company_code, ''605'', ''98'', :tran_nbr, :user_id, row_number() over (order by psi.item_id) + :count, :v_proc_stat_code, :v_xml_group_attr,:create_date_time, :mod_date_time, :user_id, ' || 'psku.item_name, psi.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, sum(NVL(psi.sku_invn_qty,0)), NULL, :g_warehouse , sum(nvl(psi.wt, 0)) ' ||
        (
          CASE
          WHEN g_group_skuattr_flag THEN
            REPLACE (g_sku_attributes, 'alias', 'psi')
          ELSE
            ''
          END) || 'FROM (SELECT /*+ no_push_pred */ sa.item_id, NVL(COALESCE(SUM(si.qty_not_alloc),0),0) AS sku_invn_qty, wt_adj_qty_tmp.wt ' ||
        (
          CASE
          WHEN g_group_skuattr_flag THEN
            REPLACE (g_sku_attributes, 'alias', 'sa')
          ELSE
            ''
          END) || ' FROM gen605_wm_inventory_gtt si ' ||
        (
          CASE
          WHEN g_zero_invn_flag THEN
            'RIGHT JOIN'
          ELSE
            'INNER JOIN'
          END) || ' gen605_sku_attr_gtt sa ON si.sku_attr_id = sa.sku_attr_id ' || ' left join wt_adj_qty_tmp on wt_adj_qty_tmp.item_id = si.item_id and wt_adj_qty_tmp.sku_attr_id = si.sku_attr_id ' ||
        (
          CASE
          WHEN g_zero_invn_flag THEN
            ' '
          ELSE
            'where si.qty_not_alloc <> 0 '
          END) || 'GROUP BY sa.item_id, wt_adj_qty_tmp.wt ' ||
        (
          CASE
          WHEN g_group_skuattr_flag THEN
            REPLACE (g_sku_attributes, 'alias', 'sa')
          ELSE
            ''
          END) || ') psi INNER JOIN gen605_sku_gtt psku ON psku.item_id = psi.item_id ' ||
        (
          CASE
          WHEN g_zero_invn_flag THEN
            ' '
          ELSE
            ' where psi.sku_invn_qty <> 0 '
          END) || ' group by psku.item_name, psi.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc ' ||
        (
          CASE
          WHEN g_group_skuattr_flag THEN
            REPLACE (g_sku_attributes, 'alias', 'psi')
          ELSE
            ''
          END);
      ELSE
      v_Pix_Insert := 'INSERT INTO pix_tran(tc_company_id, facility_id, company_code, tran_type, tran_code, tran_nbr, sys_user_id, pix_seq_nbr, proc_stat_code,xml_group_id,  create_date_time, mod_date_time, user_id, ' || 'item_name, item_id, season, season_yr, style, style_sfx, color, color_sfx, sec_dim, qual, size_desc, invn_adjmt_qty, invn_adjmt_type, whse, wt_adjmt_qty' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias.', '')
        ELSE
          ''
        END) || ')  ' || ' with wt_adj_qty_tmp as' || ' ( ' || '    SELECT iv.item_id, iv.sku_attr_id, SUM (iv.wt) wt ' || '         FROM (SELECT si.item_id, si.sku_attr_id, ' || '                   ( (CASE WHEN si.single_line_lpn = ''Y'' THEN si.actl_weight ELSE si.on_hand_qty  END)  * (CASE WHEN si.single_line_lpn = ''Y'' THEN 1 ' || '                        ELSE cd.unit_weight  END)) AS wt ' || '                   FROM    gen605_wm_inventory_gtt si ' || '                   JOIN gen605_sku_gtt cd ' || '                 ON si.item_id = cd.item_id ' || '           WHERE si.allocable = 0 AND si.flag IN (''I'') ' || '                 AND EXISTS ' || '                        (SELECT 1 ' || '                           FROM gen605_lpn_lock_gtt cl ' || '                          WHERE cl.lpn_id = si.lpn_id ' || '                                AND COALESCE (cl.misc_flags_2, ''Z'') = ''N'') ' || '           UNION ALL ' || '           SELECT si.item_id, si.sku_attr_id, ' ||
      '                 ( (CASE  WHEN si.single_line_lpn = ''Y'' THEN si.actl_weight  ELSE si.on_hand_qty  END) * (CASE  WHEN si.single_line_lpn = ''Y'' THEN 1 ' || '                   ELSE cd.unit_weight  END)) AS wt ' || '            FROM    gen605_wm_inventory_gtt si ' || '                 JOIN  gen605_sku_gtt cd ' || '                 ON si.item_id = cd.item_id ' || '            WHERE si.allocable = 0 AND si.flag IN (''A'') ' || '            UNION ALL ' || '            SELECT wi.item_id,  wi.sku_attr_id, ' || '                 (wi.on_hand_qty * (CASE WHEN cd.item_avg_wt = 0 THEN cd.unit_weight ' || '                      ELSE cd.item_avg_wt END)) AS wt ' || '            FROM    gen605_wm_inventory_gtt wi ' || '                 JOIN gen605_sku_gtt cd ' || '                 ON wi.item_id = cd.item_id ' || '            WHERE wi.allocable = 0 AND wi.flag IN (''C'')) iv GROUP BY iv.item_id, iv.sku_attr_id ' || ' ) ' || ' ' ||
      ' SELECT :company_id, :facility_id, :company_code, ''605'', ''98'', :tran_nbr, :user_id, row_number() over (order by psi.item_id) + :count, :v_proc_stat_code, :v_xml_group_attr,:create_date_time, :mod_date_time, :user_id, ' || 'psku.item_name, psi.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, sum(NVL(psi.sku_invn_qty,0)), NULL, :g_warehouse , sum(nvl(psi.wt, 0)) ' ||
      (
        CASE
        WHEN g_group_skuattr_flag THEN
          REPLACE(g_sku_attributes, 'alias', 'psi')
        ELSE
          ''
        END) || 'FROM (SELECT /*+ no_push_pred */ sa.item_id, NVL(COALESCE(SUM(si.qty_not_alloc),0),0) AS sku_invn_qty, wt_adj_qty_tmp.wt ' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'sa')
        ELSE
          ''
        END) || ' FROM gen605_wm_inventory_gtt si ' ||
      (
        CASE
        WHEN g_Zero_Invn_Flag THEN
          'RIGHT JOIN'
        ELSE
          'INNER JOIN'
        END)||' gen605_sku_attr_gtt sa ON si.sku_attr_id = sa.sku_attr_id ' || ' left join wt_adj_qty_tmp on wt_adj_qty_tmp.item_id = si.item_id and wt_adj_qty_tmp.sku_attr_id = si.sku_attr_id ' ||
      (
        CASE
        WHEN g_Zero_Invn_Flag THEN
          ' '
        ELSE
          'where si.qty_not_alloc <> 0 '
        END) || 'GROUP BY sa.item_id, wt_adj_qty_tmp.wt ' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'sa')
        ELSE
          ''
        END) || ') psi INNER JOIN gen605_sku_gtt psku ON psku.item_id = psi.item_id ' ||
      (
        CASE
        WHEN g_Zero_Invn_Flag THEN
          ' '
        ELSE
          ' where psi.sku_invn_qty <> 0 '
        END) || ' group by psku.item_name, psi.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc ' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'psi')
        ELSE
          ''
        END);
      END IF;
      v_Pix_Insert := v_Pix_Insert || ' log errors (to_char(sysdate))';
      EXECUTE IMMEDIATE v_pix_insert USING g_company_id,
      g_facility_id,
      g_company_code,
      g_tran_nbr,
      g_user_id,
      g_count,
      v_proc_stat_code,
      v_xml_group_attr,
      g_mod_date_time,
      g_mod_date_time,
      g_user_id,
      g_warehouse;
      g_count := g_count + sql%rowcount;
      log('Generated Summary Level Un-allocable Inventory Pix Records (TRAN_CODE = 98).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 1);
      /* Added for WM49 Unallocatable Changes */
      v_pix_tab.delete;
      SELECT * bulk collect
      INTO v_pix_tab
      FROM pix_tran
      WHERE ref_code_id_10 = '997'
      AND ref_field_10    IS NULL
	  AND proc_stat_code IN (0, 10)
	  AND tran_nbr = g_tran_nbr;
      FOR i               IN 1 .. v_pix_tab.count
      LOOP
        SELECT COALESCE(SUM (ld.size_value), 0)
        INTO v_ref_for_item
        FROM lpn
        INNER JOIN lpn_detail ld
        ON lpn.lpn_id                      = ld.lpn_id
        AND lpn.inbound_outbound_indicator = 'I'
        AND lpn.lpn_facility_status       >= '10'
        AND lpn.lpn_facility_status        < '95'
        AND lpn.asn_id                    IS NOT NULL
        INNER JOIN wm_inventory wi
        ON ld.lpn_detail_id = wi.lpn_detail_id
        --INNER JOIN wm_inventory win
        AND lpn.lpn_id       = wi.lpn_id
        AND wi.allocatable = 'N'
        INNER JOIN asn
        ON lpn.asn_id      = asn.asn_id
        AND asn.asn_status < 40 --Receiving Verified
        AND ld.item_id     = v_pix_tab(i).item_id;
        UPDATE pix_tran
        SET ref_field_10   = v_ref_for_item
        WHERE item_id      = v_pix_tab(i).item_id
        AND ref_code_id_10 = '997'
        AND pix_tran_id    = v_pix_tab(i).pix_tran_id;
      END LOOP;
    END IF;
    --generate LXX/TXX/AXX/CXX
    generate_unalloc_invn_lock_pix ;
  ELSE
    manh_get_pix_config_data ('605','00',NULL,g_company_id,v_proc_stat_code,v_xml_group_attr);
    IF (v_proc_stat_code!= 91) THEN
      -- Allocable
      IF g_erphosttype       = '0' THEN
        IF v_xml_group_attr IS NULL THEN
          v_xml_group_attr  := NULL;
        ELSE
          v_xml_group_attr := v_xml_group_attr;
        END IF;
        v_pix_insert := 'insert into pix_tran(tc_company_id, facility_id, company_code, tran_type, tran_code, tran_nbr, sys_user_id, pix_seq_nbr, proc_stat_code, xml_group_id,create_date_time, mod_date_time, user_id, ' || 'item_name, item_id, season, season_yr, style, style_sfx, color, color_sfx, sec_dim, qual, size_desc, invn_adjmt_qty, invn_adjmt_type, whse, wt_adjmt_qty' ||
        (
          CASE
          WHEN g_group_skuattr_flag THEN
            REPLACE (g_sku_attributes, 'alias.', '')
          ELSE
            ''
          END ) || ') ' || ' with wt_adj_qty_tmp as' || ' ( ' || '     SELECT iv.item_id, iv.sku_attr_id, SUM (iv.wt) wt' || '      FROM (SELECT wi.item_id, wi.sku_attr_id,' || '     ( (CASE WHEN wi.single_line_lpn = ''Y'' THEN wi.actl_weight ' || '        ELSE wi.on_hand_qty END) * (CASE WHEN wi.single_line_lpn = ''Y'' THEN 1 ' || '        ELSE nvl(cd.unit_weight,0) END)) AS wt ' || '       FROM    gen605_wm_inventory_gtt wi ' || '           JOIN gen605_sku_gtt cd ' || '           ON wi.item_id = cd.item_id ' || '            WHERE wi.allocable = 1 AND wi.flag IN (''A'', ''I'') ' || '           AND NOT EXISTS ' || '              (SELECT 1 ' || '                 FROM gen605_lpn_lock_gtt cl ' || '                 WHERE cl.lpn_id = wi.lpn_id ' || '                       AND COALESCE (cl.misc_flags_2, ''Z'') = ''N'') ' || '         UNION ALL ' || '         SELECT wi.item_id, wi.sku_attr_id, lcw.catch_weight wt ' || '         FROM    gen605_lpn_catch_wt_gtt lcw ' ||
        '         JOIN  gen605_wm_inventory_gtt wi ' || '         ON wi.lpn_id = lcw.lpn_id ' || '         AND lcw.lpn_detail_id = wi.lpn_detail_id ' || '         WHERE wi.flag = ''O'' ' || '         UNION ALL ' || '         SELECT wi.item_id,wi.sku_attr_id, ' || '         (wi.on_hand_qty * (CASE WHEN cd.item_avg_wt = 0 THEN nvl(cd.unit_weight,0) ' || '           ELSE cd.item_avg_wt END)) AS wt ' || '         FROM    gen605_wm_inventory_gtt wi ' || '         JOIN gen605_sku_gtt cd ' || '         ON wi.item_id = cd.item_id ' || '         WHERE wi.flag IN (''C'', ''O'', ''T'') ' || '         AND NOT EXISTS ' || '           (SELECT 1 ' || '              FROM gen605_lpn_catch_wt_gtt ccw ' || '              WHERE ccw.lpn_id = wi.lpn_id)) iv GROUP BY iv.item_id, iv.sku_attr_id ' || ' ) ' || ' ' ||
        ' SELECT /*+ALL_ROWS*/ :company_id, :facility_id, :company_code, ''605'', ''00'', :tran_nbr, :user_id, (row_number() over (order by psi.item_id)+:counter), :v_proc_stat_code,:v_xml_group_attr, :create_date_time, :mod_date_time, :user_id, ' || 'psku.item_name, psi.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, sum(psi.sku_invn_qty), NULL, :g_warehouse, sum(nvl(psi.wt, 0)) ' ||
        (
          CASE
          WHEN g_group_skuattr_flag THEN
            REPLACE (g_sku_attributes, 'alias', 'psi')
          ELSE
            ''
          END ) || 'FROM (SELECT /*+ no_push_pred */ sa.item_id, COALESCE(SUM(si.on_hand_qty - si.qty_not_alloc),0) AS sku_invn_qty, wt_adj_qty_tmp.wt ' ||
        (
          CASE
          WHEN g_group_skuattr_flag THEN
            REPLACE (g_sku_attributes, 'alias', 'sa')
          ELSE
            ''
          END ) || ' FROM gen605_wm_inventory_gtt si ' ||
        (
          CASE
          WHEN g_Zero_Invn_Flag THEN
            'RIGHT JOIN'
          ELSE
            'INNER JOIN'
          END)||' gen605_sku_attr_gtt sa ON si.sku_attr_id = sa.sku_attr_id ' || ' left join wt_adj_qty_tmp on wt_adj_qty_tmp.item_id = si.item_id and wt_adj_qty_tmp.sku_attr_id = si.sku_attr_id ' ||
        (
          CASE
          WHEN g_Zero_Invn_Flag THEN
            ' '
          ELSE
            'where si.on_hand_qty <> 0 '
          END) || 'GROUP BY sa.item_id, wt_adj_qty_tmp.wt ' ||
        (
          CASE
          WHEN g_group_skuattr_flag THEN
            REPLACE (g_sku_attributes, 'alias', 'sa')
          ELSE
            ''
          END ) || ') psi INNER JOIN gen605_sku_gtt psku ON psku.item_id = psi.item_id ' ||
        (
          CASE
          WHEN g_Zero_Invn_Flag THEN
            ' '
          ELSE
            ' where psi.sku_invn_qty <> 0 '
          END) || ' group by psku.item_name, psi.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc  ' ||
        (
          CASE
          WHEN g_Group_SkuAttr_Flag THEN
            REPLACE(g_Sku_Attributes, 'alias', 'psi')
          ELSE
            ''
          END);
      ELSE
        v_pix_insert := 'INSERT INTO pix_tran(tc_company_id, facility_id, company_code, tran_type, tran_code, tran_nbr, sys_user_id, pix_seq_nbr, proc_stat_code, xml_group_id,create_date_time, mod_date_time, user_id, ' || 'item_name, item_id, season, season_yr, style, style_sfx, color, color_sfx, sec_dim, qual, size_desc, invn_adjmt_qty, invn_adjmt_type, whse' ||
        (
          CASE
          WHEN g_group_skuattr_flag THEN
            REPLACE (g_sku_attributes, 'alias.', '')
          ELSE
            ''
          END ) || ') SELECT /*+ALL_ROWS*/ :company_id, :facility_id, :company_code, ''605'', ''00'', :tran_nbr, :user_id, (row_number() over (order by psi.item_id)+:counter), :v_proc_stat_code, :v_xml_group_attr,:create_date_time, :mod_date_time, :user_id, ' || 'psku.item_name, psi.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, psi.sku_invn_qty, NULL, :g_warehouse ' ||
        (
          CASE
          WHEN g_group_skuattr_flag THEN
            REPLACE (g_sku_attributes, 'alias', 'psi')
          ELSE
            ''
          END ) || 'FROM (SELECT /*+ no_push_pred */ sa.item_id, (SUM(si.on_hand_qty - si.qty_not_alloc) - COALESCE(SUM(pd.pkt_qty), 0)) AS sku_invn_qty' ||
        (
          CASE
          WHEN g_group_skuattr_flag THEN
            REPLACE (g_sku_attributes, 'alias', 'sa')
          ELSE
            ''
          END ) || ' FROM gen605_wm_inventory_gtt si INNER JOIN gen605_sku_attr_gtt sa ON si.sku_attr_id = sa.sku_attr_id ' || 'LEFT JOIN (SELECT sku_attr_id, SUM(pkt_qty) AS pkt_qty FROM gen605_pkt_dtl_gtt GROUP BY sku_attr_id) pd ON si.sku_attr_id = pd.sku_attr_id ' ||
        (
          CASE
          WHEN g_Zero_Invn_Flag THEN
            ' '
          ELSE
            'where si.on_hand_qty <> 0 '
          END) || 'GROUP BY sa.item_id' ||
        (
          CASE
          WHEN g_group_skuattr_flag THEN
            REPLACE (g_sku_attributes, 'alias', 'sa')
          ELSE
            ''
          END ) || ') psi INNER JOIN gen605_sku_gtt psku ON psku.item_id = psi.item_id ' ||
        (
          CASE
          WHEN g_Zero_Invn_Flag THEN
            ' '
          ELSE
            ' where psi.sku_invn_qty <> 0 '
          END) || ' group by psku.item_name, psi.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, psi.sku_invn_qty ' ||
        (
          CASE
          WHEN g_Group_SkuAttr_Flag THEN
            REPLACE(g_Sku_Attributes, 'alias', 'psi')
          ELSE
            ''
          END);
      END IF;
      v_Pix_Insert := v_Pix_Insert || ' log errors (to_char(sysdate))';
      EXECUTE IMMEDIATE v_pix_insert USING g_company_id,
      g_facility_id,
      g_company_code,
      g_tran_nbr,
      g_user_id,
      g_count,
      v_proc_stat_code,
      v_xml_group_attr,
      g_mod_date_time,
      g_mod_date_time,
      g_user_id,
      g_warehouse;
      g_count := g_count + SQL%ROWCOUNT;
      log('Generated Summary Level Allocable Inventory Pix Records (TRAN_CODE = 00).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 1);
    END IF;
  END IF;
  -- Generate SRL_PIX_TRAN if SrlPix flag is set
  IF g_srlnbrpix_flag THEN
    generate_summary_srlpix;
  END IF;
  log('Finished generation of Summary Level PIX', TRUE);
END generate_summary_pix;
PROCEDURE generate_detail_srlpix
AS
BEGIN
  log('Started Detail Level SRL_PIX_TRAN generation', TRUE, 1);
  -- Allocable Case Inventory
  INSERT
  INTO srl_pix_tran
    (
      SRL_PIX_TRAN_ID,
      tran_type,
      tran_code,
      tran_nbr,
      pix_seq_nbr,
      proc_stat_code,
      srl_nbr,
      seq_nbr,
      create_date_time,
      mod_date_time,
      user_id,
      pix_tran_id
    )
  SELECT
    /*+ALL_ROWS*/
    SRL_PIX_TRAN_ID_SEQ.nextval,
    pt.tran_type,
    pt.tran_code,
    pt.tran_nbr,
    pt.pix_seq_nbr,
    10,
    srl.srl_nbr,
    srl.srl_seq_nbr,
    g_mod_date_time,
    g_mod_date_time,
    g_user_id,
    pt.pix_tran_id
  FROM gen605_srl_nbr_track_gtt srl
  INNER JOIN pix_tran pt
  ON srl.item_id = pt.item_id
  INNER JOIN gen605_wm_inventory_gtt cd
  ON
    /*cd.tc_lpn_id = srl.case_nbr AND */
    cd.lpn_detail_id      = srl.lpn_detail_id
  WHERE srl.stat_code     = 10
  AND pt.proc_stat_code   = 10
  AND pt.create_date_time = g_mod_date_time
  AND pt.tran_type        = '605'
  AND pt.tran_code        = '01'
  AND pt.tran_nbr         = g_tran_nbr
  AND cd.flag             = 'I'
  AND NOT EXISTS
    (SELECT 1
    FROM gen605_lpn_lock_gtt Cl2
    WHERE cl2.lpn_id                    = srl.lpn_id
    AND COALESCE(cl2.misc_flags_2, 'Z') = 'N'
    ) log errors (TO_CHAR(sysdate));
  g_count := g_count + SQL%ROWCOUNT;
  log('Generated SRL_PIX_TRAN for Allocable Case Inventory Records (TRAN_CODE = 01).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 2);
  -- Un-allocable Case Inventory
  IF g_unalloc_invn_flag AND NOT g_caselock_flag THEN
    INSERT
    INTO srl_pix_tran
      (
        SRL_PIX_TRAN_ID,
        tran_type,
        tran_code,
        tran_nbr,
        pix_seq_nbr,
        proc_stat_code,
        srl_nbr,
        seq_nbr,
        create_date_time,
        mod_date_time,
        user_id,
        pix_tran_id
      )
      (SELECT
          /*+ALL_ROWS*/
          SRL_PIX_TRAN_ID_SEQ.nextval,
          pt.tran_type,
          pt.tran_code,
          pt.tran_nbr,
          pt.pix_seq_nbr,
          10,
          srl.srl_nbr,
          srl.srl_seq_nbr,
          g_mod_date_time,
          g_mod_date_time,
          g_user_id,
          pt.pix_tran_id
        FROM gen605_srl_nbr_track_gtt srl
        INNER JOIN pix_tran pt
        ON srl.item_id = pt.item_id
        INNER JOIN gen605_wm_inventory_gtt cd
        ON
          /*cd.tc_lpn_id = srl.case_nbr AND */
          cd.lpn_detail_id      = srl.lpn_detail_id
        WHERE srl.stat_code     = 10
        AND pt.proc_stat_code   = 10
        AND pt.create_date_time = g_mod_date_time
        AND pt.tran_type        = '605'
        AND pt.tran_code        = '11'
        AND pt.tran_nbr         = g_tran_nbr
        AND cd.flag             = 'I'
        AND EXISTS
          (SELECT 1
          FROM gen605_lpn_lock_gtt cl
          WHERE cl.lpn_id                    = srl.lpn_id
          AND COALESCE(cl.misc_flags_2, 'Z') = 'N'
          )
      )
      log errors
      (
        TO_CHAR(sysdate)
      );
    g_count := g_count + SQL%ROWCOUNT;
    log('Generated SRL_PIX_TRAN for Un-allocable Case Inventory Records (TRAN_CODE = 11).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 2);
  END IF;
  -- Un-allocable Case Inventory Summary by CASE_LOCK.INVN_LOCK_CODE
  IF g_unalloc_invn_flag AND g_caselock_flag THEN
    INSERT
    INTO srl_pix_tran
      (
        SRL_PIX_TRAN_ID,
        tran_type,
        tran_code,
        tran_nbr,
        pix_seq_nbr,
        proc_stat_code,
        srl_nbr,
        seq_nbr,
        create_date_time,
        mod_date_time,
        user_id,
        pix_tran_id
      )
      (SELECT
          /*+ALL_ROWS*/
          SRL_PIX_TRAN_ID_SEQ.nextval,
          pt.tran_type,
          pt.tran_code,
          pt.tran_nbr,
          pt.pix_seq_nbr,
          10,
          srl.srl_nbr,
          srl.srl_seq_nbr,
          g_mod_date_time,
          g_mod_date_time,
          g_user_id,
          pt.pix_tran_id
        FROM gen605_srl_nbr_track_gtt srl
        INNER JOIN pix_tran pt
        ON srl.item_id          = pt.item_id
        WHERE srl.stat_code     = 10
        AND pt.proc_stat_code   = 10
        AND pt.create_date_time = g_mod_date_time
        AND pt.tran_type        = '605'
        AND pt.tran_nbr         = g_tran_nbr
        AND EXISTS
          (SELECT 1
          FROM gen605_wm_inventory_gtt cd
          INNER JOIN gen605_lpn_lock_gtt cl
          ON cd.lpn_id = cl.lpn_id
          WHERE
            /*cd.tc_lpn_id = srl.case_nbr AND */
            cd.lpn_detail_id = srl.lpn_detail_id
          AND pt.tran_code   = 'L'
            ||cl.inventory_lock_code
          AND cd.flag          = 'I'
          AND cl.misc_flags_34 =
            (SELECT MIN(cl2.misc_flags_34)
            FROM gen605_lpn_lock_gtt cl2
            WHERE cl2.lpn_id = cl.lpn_id
            )
          )
        AND EXISTS
          (SELECT 1
          FROM gen605_lpn_lock_gtt cl
          WHERE cl.lpn_id                    = srl.lpn_id
          AND COALESCE(cl.misc_flags_2, 'Z') = 'N'
          )
      )
      log errors
      (
        TO_CHAR(sysdate)
      );
    g_count := g_count + SQL%ROWCOUNT;
    log('Generated SRL_PIX_TRAN for Un-allocable Case Inventory Summary by CASE_LOCK.INVN_LOCK_CODE Records (TRAN_CODE = Lxx).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 2);
  END IF;
  -- Process CARTON Inventory SRL_PIX
  INSERT
  INTO srl_pix_tran
    (
      SRL_PIX_TRAN_ID,
      tran_type,
      tran_code,
      tran_nbr,
      pix_seq_nbr,
      proc_stat_code,
      srl_nbr,
      seq_nbr,
      create_date_time,
      mod_date_time,
      user_id,
      pix_tran_id
    )
  SELECT
    /*+ALL_ROWS*/
    SRL_PIX_TRAN_ID_SEQ.nextval,
    pt.tran_type,
    pt.tran_code,
    pt.tran_nbr,
    pt.pix_seq_nbr,
    10,
    srl.srl_nbr,
    srl.srl_seq_nbr,
    g_mod_date_time,
    g_mod_date_time,
    g_user_id,
    pt.pix_tran_id
  FROM gen605_srl_nbr_track_gtt srl
  INNER JOIN pix_tran pt
  ON srl.item_id = pt.item_id
  INNER JOIN gen605_wm_inventory_gtt cd
  ON
    /*cd.tc_lpn_id = srl.carton_nbr AND */
    cd.lpn_detail_id      = srl.lpn_detail_id
  WHERE srl.stat_code     = 70
  AND pt.proc_stat_code   = 10
  AND pt.create_date_time = g_mod_date_time
  AND pt.tran_type        = '605'
  AND pt.tran_code        = '02'
  AND cd.flag             = 'O'
  AND pt.tran_nbr         = g_tran_nbr log errors (TO_CHAR(sysdate));
  g_count                := g_count + SQL%ROWCOUNT;
  log('Generated SRL_PIX_TRAN for Carton Inventory Records (TRAN_CODE = 02).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 2);
  -- Active Locations
  INSERT
  INTO srl_pix_tran
    (
      SRL_PIX_TRAN_ID,
      tran_type,
      tran_code,
      tran_nbr,
      pix_seq_nbr,
      proc_stat_code,
      srl_nbr,
      seq_nbr,
      create_date_time,
      mod_date_time,
      user_id,
      pix_tran_id
    )
  SELECT
    /*+ALL_ROWS*/
    SRL_PIX_TRAN_ID_SEQ.nextval,
    pt.tran_type,
    pt.tran_code,
    pt.tran_nbr,
    pt.pix_seq_nbr,
    10,
    srl.srl_nbr,
    srl.srl_seq_nbr,
    g_mod_date_time,
    g_mod_date_time,
    g_user_id,
    pt.pix_tran_id
  FROM gen605_srl_nbr_track_gtt srl
  INNER JOIN pix_tran pt
  ON srl.item_id = pt.item_id
  INNER JOIN gen605_wm_inventory_gtt Pd
  ON pd.wm_inventory_id = srl.wm_inventory_id
    /*pd.location_id = srl.locn_id AND pd.location_dtl_id = srl.locn_seq_nbr*/
  WHERE srl.stat_code     = 30
  AND pt.proc_stat_code   = 10
  AND pt.create_date_time = g_mod_date_time
  AND pt.tran_type        = '605'
  AND pt.tran_code        = '03'
  AND pt.tran_nbr         = g_tran_nbr
  AND pd.locn_class       = 'A'
  AND pd.flag             = 'A' log errors (TO_CHAR(sysdate));
  g_count                := g_count + SQL%ROWCOUNT;
  log('Generated SRL_PIX_TRAN for Active Inventory (Active Locations) Records (TRAN_CODE = 03).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 2);
  -- CasePick Locations
  INSERT
  INTO srl_pix_tran
    (
      SRL_PIX_TRAN_ID,
      tran_type,
      tran_code,
      tran_nbr,
      pix_seq_nbr,
      proc_stat_code,
      srl_nbr,
      seq_nbr,
      create_date_time,
      mod_date_time,
      user_id,
      pix_tran_id
    )
  SELECT
    /*+ALL_ROWS*/
    SRL_PIX_TRAN_ID_SEQ.nextval,
    pt.tran_type,
    pt.tran_code,
    pt.tran_nbr,
    pt.pix_seq_nbr,
    10,
    srl.srl_nbr,
    srl.srl_seq_nbr,
    g_mod_date_time,
    g_mod_date_time,
    g_user_id,
    pt.pix_tran_id
  FROM gen605_srl_nbr_track_gtt srl
  INNER JOIN pix_tran pt
  ON srl.item_id = pt.item_id
  INNER JOIN gen605_wm_inventory_gtt pd
  ON pd.wm_inventory_id = srl.wm_inventory_id
    /*pd.location_id = srl.locn_id AND pd.location_dtl_id = srl.locn_seq_nbr*/
  WHERE srl.stat_code     = 40
  AND pt.proc_stat_code   = 10
  AND pt.create_date_time = g_mod_date_time
  AND pt.tran_type        = '605'
  AND pt.tran_code        = '06'
  AND pt.tran_nbr         = g_tran_nbr
  AND pd.locn_class       = 'C'
  AND pd.flag             = 'C' log errors (TO_CHAR(sysdate));
  g_count                := g_count + SQL%ROWCOUNT;
  log('Generated SRL_PIX_TRAN for Active Inventory (CasePick Locations) Records (TRAN_CODE = 06).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 2);
  -- Allocable Transitional Inventory
  INSERT
  INTO srl_pix_tran
    (
      SRL_PIX_TRAN_ID,
      tran_type,
      tran_code,
      tran_nbr,
      pix_seq_nbr,
      proc_stat_code,
      srl_nbr,
      seq_nbr,
      create_date_time,
      mod_date_time,
      user_id,
      pix_tran_id
    )
  SELECT
    /*+ALL_ROWS*/
    SRL_PIX_TRAN_ID_SEQ.nextval,
    pt.tran_type,
    pt.tran_code,
    pt.tran_nbr,
    pt.pix_seq_nbr,
    10,
    srl.srl_nbr,
    srl.srl_seq_nbr,
    g_mod_date_time,
    g_mod_date_time,
    g_user_id,
    pt.pix_tran_id
  FROM gen605_srl_nbr_track_gtt srl
  INNER JOIN pix_tran pt
  ON srl.item_id = pt.item_id
  INNER JOIN gen605_wm_inventory_gtt ti
  ON ti.wm_inventory_id   = srl.wm_inventory_id
  WHERE srl.stat_code     = 20
  AND pt.proc_stat_code   = 10
  AND pt.create_date_time = g_mod_date_time
  AND pt.tran_type        = '605'
  AND pt.tran_code        = '04'
  AND pt.tran_nbr         = g_tran_nbr
  AND ti.allocable        = 1
  AND ti.flag             = 'T' log errors (TO_CHAR(sysdate));
  g_count                := g_count + SQL%ROWCOUNT;
  log('Generated SRL_PIX_TRAN for Allocable Transitional Inventory Records (TRAN_CODE = 04).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 2);
  -- Un-allocable Transitional Inventory
  IF g_unalloc_invn_flag AND NOT g_transinvn_flag THEN
    INSERT
    INTO srl_pix_tran
      (
        SRL_PIX_TRAN_ID,
        tran_type,
        tran_code,
        tran_nbr,
        pix_seq_nbr,
        proc_stat_code,
        srl_nbr,
        seq_nbr,
        create_date_time,
        mod_date_time,
        user_id,
        pix_tran_id
      )
    SELECT
      /*+ALL_ROWS*/
      SRL_PIX_TRAN_ID_SEQ.nextval,
      pt.tran_type,
      pt.tran_code,
      pt.tran_nbr,
      pt.pix_seq_nbr,
      10,
      srl.srl_nbr,
      srl.srl_seq_nbr,
      g_mod_date_time,
      g_mod_date_time,
      g_user_id,
      pt.pix_tran_id
    FROM gen605_srl_nbr_track_gtt srl
    INNER JOIN pix_tran pt
    ON srl.item_id = pt.item_id
    INNER JOIN gen605_wm_inventory_gtt ti
    ON ti.wm_inventory_id   = srl.wm_inventory_id
    WHERE srl.stat_code     = 20
    AND pt.proc_stat_code   = 10
    AND pt.create_date_time = g_mod_date_time
    AND pt.tran_type        = '605'
    AND pt.tran_code        = '14'
    AND pt.tran_nbr         = g_tran_nbr
    AND ti.allocable        = 0
    AND ti.flag             = 'T' log errors (TO_CHAR(sysdate));
    g_count                := g_count + SQL%ROWCOUNT;
    log('Generated SRL_PIX_TRAN for Un-allocable Transitional Inventory Records (TRAN_CODE = 14).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 2);
  END IF;
  -- Un-allocable Transitional Inventory by TRANS_INVN_TYPE
  IF g_unalloc_invn_flag AND g_transinvn_flag THEN
    INSERT
    INTO srl_pix_tran
      (
        SRL_PIX_TRAN_ID,
        tran_type,
        tran_code,
        tran_nbr,
        pix_seq_nbr,
        proc_stat_code,
        srl_nbr,
        seq_nbr,
        create_date_time,
        mod_date_time,
        user_id,
        pix_tran_id
      )
    SELECT
      /*+ALL_ROWS*/
      SRL_PIX_TRAN_ID_SEQ.nextval,
      pt.tran_type,
      pt.tran_code,
      pt.tran_nbr,
      pt.pix_seq_nbr,
      10,
      srl.srl_nbr,
      srl.srl_seq_nbr,
      g_mod_date_time,
      g_mod_date_time,
      g_user_id,
      pt.pix_tran_id
    FROM gen605_srl_nbr_track_gtt srl
    INNER JOIN pix_tran pt
    ON srl.item_id = pt.item_id
    INNER JOIN gen605_wm_inventory_gtt ti
    ON ti.wm_inventory_id   = srl.wm_inventory_id
    WHERE srl.stat_code     = 20
    AND pt.proc_stat_code   = 10
    AND pt.create_date_time = g_mod_date_time
    AND pt.tran_type        = '605'
    AND pt.tran_nbr         = g_tran_nbr
    AND ti.allocable        = 0
    AND pt.tran_code        = 'T'
      ||TO_CHAR(ti.transitional_inventory_type)
    AND ti.flag = 'T' log errors (TO_CHAR(sysdate));
    g_count    := g_count + SQL%ROWCOUNT;
    log('Generated SRL_PIX_TRAN for Un-allocable Transitional Inventory by TRANS_INVN_TYPE Records (TRAN_CODE = Txx).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 2);
  END IF;
  IF g_unalloc_invn_flag THEN
    -- generate AXX/CXX serial pix
    generate_actv_cpick_srlpix;
  END IF;
  log('Finished Detail Level SRL_PIX_TRAN generation', TRUE, 1);
EXCEPTION
WHEN OTHERS THEN
  raise;
END generate_detail_srlpix;
PROCEDURE generate_detail_pix
AS
  v_pix_insert     VARCHAR2 (5000);
  v_proc_stat_code NUMBER;
  v_xml_group_attr VARCHAR(10);
BEGIN
  log('Started generation of Detail Level PIX', TRUE);
  -- Process Allocable Case Inventory PIX
  log('Started Case Inventory PIX generation', TRUE, 1);
  manh_get_pix_config_data ('605','01',NULL,g_company_id,v_proc_stat_code,v_xml_group_attr);
  IF (v_proc_stat_code  != 91) THEN
    IF v_xml_group_attr IS NULL THEN
      v_xml_group_attr  := NULL;
    ELSE
      v_xml_group_attr := v_xml_group_attr;
    END IF;
    v_Pix_Insert := 'INSERT INTO pix_tran(tc_company_id, facility_id, company_code, tran_type, tran_code, tran_nbr, sys_user_id, pix_seq_nbr, proc_stat_code,xml_group_id,                
create_date_time, mod_date_time, user_id, item_name, item_id, season, season_yr, style, style_sfx, color, color_sfx, sec_dim, qual, size_desc, invn_adjmt_qty, wt_adjmt_qty, whse ' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias.', '')
      ELSE
        ''
      END) ||
    (
      CASE
      WHEN g_ConsPrtyDate_Flag THEN
        ', ref_code_id_1, ref_field_1'
      ELSE
        ''
      END) || ') SELECT :company_id, :facility_id, :company_code, ''605'', ''01'', :tran_nbr, :user_id, row_number() over (order by pcd.item_id) + :count, :v_proc_stat_code,:v_xml_group_attr, :create_date_time, :mod_date_time, :user_id, ' || 'psku.item_name, pcd.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, pcd.actl_qty, nvl(pcd.wt_adjmt_qty,0), :g_warehouse' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'pcd')
      ELSE
        ''
      END) ||
    (
      CASE
      WHEN g_ConsPrtyDate_Flag THEN
        ', ''93'', pcd.cons_prty_date '
      ELSE
        ''
      END) || ' FROM (SELECT si.item_id, SUM(cd.on_hand_qty) AS actl_qty, ' || ' SUM((case when cd.single_line_lpn =''Y'' then cd.actl_weight else cd.on_hand_qty end) * ' || ' (case when cd.single_line_lpn =''Y'' then 1 else nvl(pku.unit_weight,0) end)) AS wt_adjmt_qty ' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'si')
      ELSE
        ''
      END) ||
    (
      CASE
      WHEN g_ConsPrtyDate_Flag THEN
        ', substr(tochar(cd.CONSUMPTION_PRIORITY_DTTM,''YYYY-MM-DD''), 1,10) AS cons_prty_date'
      ELSE
        ''
      END) || ' FROM gen605_wm_inventory_gtt cd INNER JOIN gen605_sku_attr_gtt si ON cd.sku_attr_id = si.sku_attr_id inner join gen605_sku_gtt pku ON pku.item_id = si.item_id ' || ' WHERE NOT EXISTS (SELECT 1 FROM gen605_lpn_lock_gtt cl WHERE cl.lpn_id = cd.lpn_id  AND COALESCE(cl.misc_flags_2, ''Z'') = ''N'') ' ||
    (
      CASE
      WHEN g_ConsPrtyDate_Flag THEN
        ' and cd.lpn_facility_status >= 10 AND cd.lpn_facility_status < 90  and cd.flag = ''I'''
      ELSE
        ' and cd.flag = ''I'''
      END) || ' GROUP BY si.item_id' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'si')
      ELSE
        ''
      END) ||
    (
      CASE
      WHEN g_ConsPrtyDate_Flag THEN
        ', substr(tochar(cd.CONSUMPTION_PRIORITY_DTTM,''YYYY-MM-DD''), 1,10)'
      ELSE
        ''
      END) || ') pcd INNER JOIN gen605_sku_gtt psku ON psku.item_id = pcd.item_id GROUP BY psku.item_name, pcd.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, pcd.actl_qty, pcd.wt_adjmt_qty ' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'pcd')
      ELSE
        ''
      END) ||
    (
      CASE
      WHEN g_ConsPrtyDate_Flag THEN
        ', pcd.cons_prty_date '
      ELSE
        ''
      END);
    v_Pix_Insert := v_Pix_Insert || ' log errors (to_char(sysdate))';
    EXECUTE IMMEDIATE v_pix_insert USING g_company_id,
    g_facility_id,
    g_company_code,
    g_tran_nbr,
    g_user_id,
    g_count,
    v_proc_stat_code,
    v_xml_group_attr,
    g_mod_date_time,
    g_mod_date_time,
    g_user_id,
    g_warehouse;
    g_count := g_count + SQL%ROWCOUNT;
  END IF;
  log('Generated Allocable Case Inventory Pix Records (TRAN_CODE = 01).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 2);
  -- Process Un-allocable Case Inventory PIX
  IF g_unalloc_invn_flag THEN --AND NOT g_caselock_flag THEN  Changed according to BA comments.
    manh_get_pix_config_data ('605','11',NULL,g_company_id,v_proc_stat_code,v_xml_group_attr);
    IF (v_proc_stat_code  != 91) THEN
      IF v_xml_group_attr IS NULL THEN
        v_xml_group_attr  := NULL;
      ELSE
        v_xml_group_attr := v_xml_group_attr;
      END IF;
      v_Pix_Insert := 'INSERT INTO pix_tran(tc_company_id, facility_id, company_code, tran_type, tran_code, tran_nbr, sys_user_id, pix_seq_nbr, proc_stat_code,xml_group_id,  create_date_time, mod_date_time, user_id, ' || 'item_name, item_id, season, season_yr, style, style_sfx, color, color_sfx, sec_dim, qual, size_desc, invn_adjmt_qty, wt_adjmt_qty, invn_adjmt_type, whse' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias.', '')
        ELSE
          ''
        END) ||
      (
        CASE
        WHEN g_ConsPrtyDate_Flag THEN
          ', ref_code_id_1, ref_field_1'
        ELSE
          ''
        END) || ') SELECT :company_id, :facility_id, :company_code, ''605'', ''11'', :tran_nbr, :user_id, row_number() over (order by pcd.item_id) + :count, :v_proc_stat_code, :v_xml_group_attr, :create_date_time, :mod_date_time, :user_id, ' || 'psku.item_name, pcd.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, pcd.actl_qty, nvl(pcd.wt_adjmt_qty,0), NULL,  :g_warehouse ' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'pcd')
        ELSE
          ''
        END) ||
      (
        CASE
        WHEN g_ConsPrtyDate_Flag THEN
          ', ''93'', pcd.cons_prty_date '
        ELSE
          ''
        END) || ' FROM (SELECT si.item_id, SUM(cd.on_hand_qty) AS actl_qty, ' || ' SUM((case when cd.single_line_lpn =''Y'' then cd.actl_weight else cd.on_hand_qty end) * ' || ' (case when cd.single_line_lpn =''Y'' then 1 else nvl(pku.unit_weight,0) end)) AS wt_adjmt_qty ' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'si')
        ELSE
          ''
        END) ||
      (
        CASE
        WHEN g_ConsPrtyDate_Flag THEN
          ', substr(tochar(cd.CONSUMPTION_PRIORITY_DTTM,''YYYY-MM-DD''), 1,10) AS cons_prty_date'
        ELSE
          ''
        END) || ' FROM gen605_wm_inventory_gtt cd INNER JOIN gen605_sku_attr_gtt si ON cd.sku_attr_id = si.sku_attr_id inner join gen605_sku_gtt pku ON pku.item_id = si.item_id ' || ' WHERE EXISTS (SELECT 1 FROM gen605_lpn_lock_gtt cl WHERE cl.lpn_id = cd.lpn_id   AND COALESCE(cl.misc_flags_2, ''Z'') = ''N'') ' ||
      (
        CASE
        WHEN g_ConsPrtyDate_Flag THEN
          ' AND cd.lpn_facility_status >= 10 AND cd.lpn_facility_status < 90 and cd.flag = ''I'''
        ELSE
          'and cd.flag = ''I'''
        END) || ' GROUP BY si.item_id' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'SI')
        ELSE
          ''
        END) ||
      (
        CASE
        WHEN g_ConsPrtyDate_Flag THEN
          ', substr(tochar(cd.CONSUMPTION_PRIORITY_DTTM,''YYYY-MM-DD''), 1,10)'
        ELSE
          ''
        END) || ') pcd INNER JOIN gen605_sku_gtt psku ON psku.item_id = pcd.item_id GROUP BY psku.item_name, pcd.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, pcd.actl_qty, pcd.wt_adjmt_qty ' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'pcd')
        ELSE
          ''
        END) ||
      (
        CASE
        WHEN g_ConsPrtyDate_Flag THEN
          ', pcd.cons_prty_date '
        ELSE
          ''
        END);
      v_Pix_Insert := v_Pix_Insert || ' log errors (to_char(sysdate))';
      EXECUTE IMMEDIATE v_pix_insert USING g_company_id,
      g_facility_id,
      g_company_code,
      g_tran_nbr,
      g_user_id,
      g_count,
      v_proc_stat_code,
      v_xml_group_attr,
      g_mod_date_time,
      g_mod_date_time,
      g_user_id,
      g_warehouse;
      g_count := g_count + SQL%ROWCOUNT;
    END IF;
    log('Generated Un-allocable Case Inventory Pix Records (TRAN_CODE = 11).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 2);
  END IF;
  -- Process Carton Inventory PIX
  log('Started Carton Inventory PIX generation', TRUE, 1);
  manh_get_pix_config_data ('605','02',NULL,g_company_id,v_proc_stat_code,v_xml_group_attr);
  IF (v_proc_stat_code  != 91) THEN
    IF v_xml_group_attr IS NULL THEN
      v_xml_group_attr  := NULL;
    ELSE
      v_xml_group_attr := v_xml_group_attr;
    END IF;
    v_Pix_Insert := 'INSERT INTO pix_tran(tc_company_id, facility_id, company_code, tran_type, tran_code, tran_nbr, sys_user_id, pix_seq_nbr, proc_stat_code, xml_group_id, create_date_time, mod_date_time, user_id, ' || 'item_name, item_id, season, season_yr, style, style_sfx, color, color_sfx, sec_dim, qual, size_desc, invn_adjmt_qty, wt_adjmt_qty, invn_adjmt_type, whse' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias.', '')
      ELSE
        ''
      END) ||
    (
      CASE
      WHEN g_ConsPrtyDate_Flag THEN
        ', ref_code_id_1, ref_field_1'
      ELSE
        ''
      END) || ') SELECT :company_id, :facility_id, :company_code, ''605'', ''02'', :tran_nbr, :user_id, row_number() over (order by pcd.item_id) + :count, :v_proc_stat_code, :v_xml_group_attr, :create_date_time, :mod_date_time, :user_id, ' || 'psku.item_name, pcd.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, pcd.units_pakd, nvl(pcd.wt_adjmt_qty,0), NULL, :g_warehouse ' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'pcd')
      ELSE
        ''
      END) ||
    (
      CASE
      WHEN g_ConsPrtyDate_Flag THEN
        ', ''93'', pcd.cons_prty_date '
      ELSE
        ''
      END) || ' FROM (SELECT si.item_id, SUM(cd.on_hand_qty) AS units_pakd, ' || ' sum(ccw.catch_weight) as wt_adjmt_qty ' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'si')
      ELSE
        ''
      END) ||
    (
      CASE
      WHEN g_ConsPrtyDate_Flag THEN
        ', substr(tochar(cd.CONSUMPTION_PRIORITY_DTTM,''YYYY-MM-DD''), 1,10) AS cons_prty_date'
      ELSE
        ''
      END) || ' FROM gen605_wm_inventory_gtt cd INNER JOIN gen605_sku_attr_gtt si ON cd.sku_attr_id = si.sku_attr_id ' || ' inner join gen605_lpn_catch_wt_gtt ccw ON ccw.lpn_id = cd.lpn_id and ccw.lpn_detail_id = cd.lpn_detail_id ' ||
    (
      CASE
      WHEN g_ConsPrtyDate_Flag THEN
        ' WHERE cd.lpn_facility_status >= 15 AND cd.lpn_facility_status < 90 and cd.flag = ''O'''
      ELSE
        'WHERE cd.flag = ''O'''
      END) || ' GROUP BY si.item_id' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'si')
      ELSE
        ''
      END) ||
    (
      CASE
      WHEN g_ConsPrtyDate_Flag THEN
        ', substr(tochar(cd.CONSUMPTION_PRIORITY_DTTM,''YYYY-MM-DD''), 1,10)'
      ELSE
        ''
      END) || ') pcd INNER JOIN gen605_sku_gtt psku ON psku.item_id = pcd.item_id GROUP BY psku.item_name, pcd.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, pcd.units_pakd, pcd.wt_adjmt_qty ' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'pcd')
      ELSE
        ''
      END) ||
    (
      CASE
      WHEN g_ConsPrtyDate_Flag THEN
        ', pcd.cons_prty_date '
      ELSE
        ''
      END);
    v_Pix_Insert := v_Pix_Insert || ' log errors (to_char(sysdate))';
    EXECUTE IMMEDIATE v_pix_insert USING g_company_id,
    g_facility_id,
    g_company_code,
    g_tran_nbr,
    g_user_id,
    g_count,
    v_proc_stat_code,
    v_xml_group_attr,
    g_mod_date_time,
    g_mod_date_time,
    g_user_id,
    g_warehouse;
    g_count      := g_count + SQL%ROWCOUNT;
    v_Pix_Insert := 'INSERT INTO pix_tran(tc_company_id, facility_id, company_code, tran_type, tran_code, tran_nbr, sys_user_id, pix_seq_nbr, proc_stat_code, xml_group_id, create_date_time, mod_date_time, user_id, ' || 'item_name, item_id, season, season_yr, style, style_sfx, color, color_sfx, sec_dim, qual, size_desc, invn_adjmt_qty, wt_adjmt_qty, invn_adjmt_type, whse' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias.', '')
      ELSE
        ''
      END) ||
    (
      CASE
      WHEN g_ConsPrtyDate_Flag THEN
        ', ref_code_id_1, ref_field_1'
      ELSE
        ''
      END) || ') SELECT :company_id, :facility_id, :company_code, ''605'', ''02'', :tran_nbr, :user_id, row_number() over (order by pcd.item_id) + :count, :v_proc_stat_code, :v_xml_group_attr, :create_date_time, :mod_date_time, :user_id, ' || 'psku.item_name, pcd.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, pcd.units_pakd, nvl(pcd.wt_adjmt_qty,0), NULL, :g_warehouse ' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'pcd')
      ELSE
        ''
      END) ||
    (
      CASE
      WHEN g_ConsPrtyDate_Flag THEN
        ', ''93'', pcd.cons_prty_date '
      ELSE
        ''
      END) || ' FROM (SELECT si.item_id, SUM(cd.on_hand_qty) AS units_pakd, ' ||' sum(cd.on_hand_qty * (case when pku.item_avg_wt = 0 then nvl(pku.unit_weight,0) else pku.item_avg_wt end)) as wt_adjmt_qty ' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'si')
      ELSE
        ''
      END) ||
    (
      CASE
      WHEN g_ConsPrtyDate_Flag THEN
        ', substr(tochar(cd.CONSUMPTION_PRIORITY_DTTM,''YYYY-MM-DD''), 1,10) AS cons_prty_date'
      ELSE
        ''
      END) || ' FROM gen605_wm_inventory_gtt cd INNER JOIN gen605_sku_attr_gtt si ON cd.sku_attr_id = si.sku_attr_id inner join gen605_sku_gtt pku ON pku.item_id = si.item_id ' || ' and not exists ( select 1 from gen605_lpn_catch_wt_gtt ccw where  ccw.lpn_id = cd.lpn_id ' || ' and ccw.lpn_detail_id = cd.lpn_detail_id ) ' ||
    (
      CASE
      WHEN g_ConsPrtyDate_Flag THEN
        ' WHERE cd.lpn_facility_status >= 15 AND cd.lpn_facility_status < 90 and cd.flag = ''O'''
      ELSE
        'WHERE cd.flag = ''O'''
      END) || ' GROUP BY si.item_id' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'si')
      ELSE
        ''
      END) ||
    (
      CASE
      WHEN g_ConsPrtyDate_Flag THEN
        ', substr(tochar(cd.CONSUMPTION_PRIORITY_DTTM,''YYYY-MM-DD''), 1,10)'
      ELSE
        ''
      END) || ') pcd INNER JOIN gen605_sku_gtt psku ON psku.item_id = pcd.item_id GROUP BY psku.item_name, pcd.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, pcd.units_pakd, pcd.wt_adjmt_qty ' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'pcd')
      ELSE
        ''
      END) ||
    (
      CASE
      WHEN g_ConsPrtyDate_Flag THEN
        ', pcd.cons_prty_date '
      ELSE
        ''
      END);
    v_Pix_Insert := v_Pix_Insert || ' log errors (to_char(sysdate))';
    EXECUTE IMMEDIATE v_pix_insert USING g_company_id,
    g_facility_id,
    g_company_code,
    g_tran_nbr,
    g_user_id,
    g_count,
    v_proc_stat_code,
    v_xml_group_attr,
    g_mod_date_time,
    g_mod_date_time,
    g_user_id,
    g_warehouse;
    g_count := g_count + SQL%ROWCOUNT;
  END IF;
  log('Generated Carton Inventory Pix Records (TRAN_CODE = 02).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 2);
  log('Finished Carton Inventory PIX generation', TRUE, 1);
  -- Process Active Inventory PIX
  log('Started Active Inventory PIX generation', TRUE, 1);
  -- Active Locations
  manh_get_pix_config_data ('605','03',NULL,g_company_id,v_proc_stat_code,v_xml_group_attr);
  IF (v_proc_stat_code != 91) THEN
    IF v_xml_group_attr IS NULL THEN
      v_xml_group_attr  := NULL;
    ELSE
      v_xml_group_attr := v_xml_group_attr;
    END IF;
    v_Pix_Insert := 'INSERT INTO pix_tran(tc_company_id, facility_id, company_code, tran_type, tran_code, tran_nbr, sys_user_id, pix_seq_nbr, proc_stat_code, xml_group_id, create_date_time, mod_date_time, user_id, ' || 'item_name, item_id, season, season_yr, style, style_sfx, color, color_sfx, sec_dim, qual, size_desc, invn_adjmt_qty, wt_adjmt_qty, invn_adjmt_type, whse' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias.', '')
      ELSE
        ''
      END) || ') SELECT :company_id, :facility_id, :company_code, ''605'', ''03'', :tran_nbr, :user_id, row_number() over (order by pld.item_id) + :count, :v_proc_stat_code, :v_xml_group_attr, :create_date_time, :mod_date_time, :user_id, ' || 'psku.item_name, pld.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, pld.actl_invn_qty, nvl(pld.wt_adjmt_qty,0), NULL, :g_warehouse' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'pld')
      ELSE
        ''
      END) || ' FROM (SELECT si.item_id, SUM(pd.on_hand_qty) AS actl_invn_qty, ' || ' SUM((case when pd.single_line_lpn =''Y'' then pd.actl_weight else pd.on_hand_qty end) * ' || ' (case when pd.single_line_lpn =''Y'' then 1 else nvl(pku.unit_weight,0) end)) AS wt_adjmt_qty ' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'si')
      ELSE
        ''
      END) || ' FROM gen605_wm_inventory_gtt pd INNER JOIN gen605_sku_attr_gtt si ON pd.sku_attr_id = si.sku_attr_id inner join gen605_sku_gtt pku ON pku.item_id = si.item_id ' ||
    (
      CASE
      WHEN g_zero_invn_flag THEN
        ' '
      ELSE
        ' join pick_locn_dtl pkld on pkld.pick_locn_dtl_id = pd.location_dtl_id and pkld.ltst_sku_assign != ''N'''
      END ) || ' WHERE pd.flag = ''A'' and pd.locn_class = ''A'' and  pd.allocable = 1 ' || ' GROUP BY si.item_id' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'si')
      ELSE
        ''
      END) || ') pld INNER JOIN gen605_sku_gtt psku ON psku.item_id = pld.item_id GROUP BY psku.item_name, pld.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, pld.actl_invn_qty, pld.wt_adjmt_qty ' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'pld')
      ELSE
        ''
      END);
    v_Pix_Insert := v_Pix_Insert || ' log errors (to_char(sysdate))';
    EXECUTE IMMEDIATE v_pix_insert USING g_company_id,
    g_facility_id,
    g_company_code,
    g_tran_nbr,
    g_user_id,
    g_count,
    v_proc_stat_code,
    v_xml_group_attr,
    g_mod_date_time,
    g_mod_date_time,
    g_user_id,
    g_warehouse;
    g_count := g_count + SQL%ROWCOUNT;
    log('Generated Active Inventory (Active Locations) Pix Records (TRAN_CODE = 03).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 2);
  END IF;
  -- Process Unallocatable Active Inventory PIX
  log('Started Unallocatable Active Inventory PIX generation', TRUE, 1);
  -- Unallocatable Active Locations
  IF g_unalloc_invn_flag THEN
    manh_get_pix_config_data ('605','12',NULL,g_company_id,v_proc_stat_code,v_xml_group_attr);
    IF (v_proc_stat_code != 91) THEN
      IF v_xml_group_attr IS NULL THEN
        v_xml_group_attr  := NULL;
      ELSE
        v_xml_group_attr := v_xml_group_attr;
      END IF;
      v_Pix_Insert := 'INSERT INTO pix_tran(tc_company_id, facility_id, company_code, tran_type, tran_code, tran_nbr, sys_user_id, pix_seq_nbr, proc_stat_code, xml_group_id, create_date_time, mod_date_time, user_id, ' || 'item_name, item_id, season, season_yr, style, style_sfx, color, color_sfx, sec_dim, qual, size_desc, invn_adjmt_qty, wt_adjmt_qty, invn_adjmt_type, whse' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias.', '')
        ELSE
          ''
        END) || ') SELECT :company_id, :facility_id, :company_code, ''605'', ''12'', :tran_nbr, :user_id, row_number() over (order by pld.item_id) + :count, :v_proc_stat_code, :v_xml_group_attr, :create_date_time, :mod_date_time, :user_id, ' || 'psku.item_name, pld.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, pld.actl_invn_qty, nvl(pld.wt_adjmt_qty,0), NULL, :g_warehouse' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'pld')
        ELSE
          ''
        END) || ' FROM (SELECT si.item_id, SUM(pd.on_hand_qty) AS actl_invn_qty, ' || ' SUM((case when pd.single_line_lpn =''Y'' then pd.actl_weight else pd.on_hand_qty end) * ' || ' (case when pd.single_line_lpn =''Y'' then 1 else nvl(pku.unit_weight,0) end)) AS wt_adjmt_qty ' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'si')
        ELSE
          ''
        END) || ' FROM gen605_wm_inventory_gtt pd INNER JOIN gen605_sku_attr_gtt si ON pd.sku_attr_id = si.sku_attr_id inner join gen605_sku_gtt pku ON pku.item_id = si.item_id ' || ' WHERE pd.flag = ''A'' and pd.locn_class = ''A'' and  pd.allocable = 0' || ' GROUP BY si.item_id' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'si')
        ELSE
          ''
        END) || ') pld INNER JOIN gen605_sku_gtt psku ON psku.item_id = pld.item_id GROUP BY psku.item_name, pld.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, pld.actl_invn_qty, pld.wt_adjmt_qty ' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'pld')
        ELSE
          ''
        END);
      v_Pix_Insert := v_Pix_Insert || ' log errors (to_char(sysdate))';
      EXECUTE IMMEDIATE v_pix_insert USING g_company_id,
      g_facility_id,
      g_company_code,
      g_tran_nbr,
      g_user_id,
      g_count,
      v_proc_stat_code,
      v_xml_group_attr,
      g_mod_date_time,
      g_mod_date_time,
      g_user_id,
      g_warehouse;
      g_count := g_count + SQL%ROWCOUNT;
      log('Generated Unallocatable Active Inventory (Active Locations) Pix Records (TRAN_CODE = 12).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 2);
    END IF;
  END IF;
  -- CasePick Locations
  manh_get_pix_config_data ('605','06',NULL,g_company_id,v_proc_stat_code,v_xml_group_attr);
  IF (v_proc_stat_code  != 91) THEN
    IF v_xml_group_attr IS NULL THEN
      v_xml_group_attr  := NULL;
    ELSE
      v_xml_group_attr := v_xml_group_attr;
    END IF;
    v_Pix_Insert := 'INSERT INTO pix_tran(tc_company_id, facility_id, company_code, tran_type, tran_code, tran_nbr, sys_user_id, pix_seq_nbr, proc_stat_code, xml_group_id, create_date_time, mod_date_time, user_id, ' || 'item_name, item_id, season, season_yr, style, style_sfx, color, color_sfx, sec_dim, qual, size_desc, invn_adjmt_qty,wt_adjmt_qty, invn_adjmt_type, whse' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias.', '')
      ELSE
        ''
      END) || ') SELECT :company_id, :facility_id, :company_code, ''605'', ''06'', :tran_nbr, :user_id, row_number() over (order by pld.item_id) + :count, :v_proc_stat_code, :v_xml_group_attr, :create_date_time, :mod_date_time, :user_id, ' || 'psku.item_name, pld.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, pld.actl_invn_qty,nvl(pld.wt_adjmt_qty,0), NULL, :g_warehouse' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'pld')
      ELSE
        ''
      END) || ' FROM (SELECT si.item_id, SUM(pd.on_hand_qty) AS actl_invn_qty, ' ||' sum(pd.on_hand_qty * (case when psik.item_avg_wt = 0 then nvl(psik.unit_weight,0) else nvl(psik.item_avg_wt,0) end)) as wt_adjmt_qty ' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'si')
      ELSE
        ''
      END) || ' FROM gen605_wm_inventory_gtt pd INNER JOIN gen605_sku_attr_gtt si ON pd.sku_attr_id = si.sku_attr_id ' || ' INNER JOIN gen605_sku_gtt psik ON psik.item_id = si.item_id ' ||
    (
      CASE
      WHEN g_zero_invn_flag THEN
        ' '
      ELSE
        ' join pick_locn_dtl pkld on pkld.pick_locn_dtl_id = pd.location_dtl_id and pkld.ltst_sku_assign != ''N'''
      END ) || ' WHERE pd.flag = ''C'' and pd.locn_class = ''C'' and  pd.allocable = 1 ' || ' GROUP BY si.item_id' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'si')
      ELSE
        ''
      END) || ') pld INNER JOIN gen605_sku_gtt psku ON psku.item_id = pld.item_id GROUP BY psku.item_name, pld.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, pld.actl_invn_qty,pld.wt_adjmt_qty ' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'pld')
      ELSE
        ''
      END);
    v_Pix_Insert := v_Pix_Insert || ' log errors (to_char(sysdate))';
    EXECUTE IMMEDIATE v_pix_insert USING g_company_id,
    g_facility_id,
    g_company_code,
    g_tran_nbr,
    g_user_id,
    g_count,
    v_proc_stat_code,
    v_xml_group_attr,
    g_mod_date_time,
    g_mod_date_time,
    g_user_id,
    g_warehouse;
    g_count := g_count + SQL%ROWCOUNT;
  END IF;
  log('Generated Active Inventory (CasePick Locations) Pix Records (TRAN_CODE = 06).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 2);
  -- Unallocatable CasePick inventory PIX
  IF g_unalloc_invn_flag THEN
    manh_get_pix_config_data ('605','13',NULL,g_company_id,v_proc_stat_code,v_xml_group_attr);
    IF (v_proc_stat_code  != 91) THEN
      IF v_xml_group_attr IS NULL THEN
        v_xml_group_attr  := NULL;
      ELSE
        v_xml_group_attr := v_xml_group_attr;
      END IF;
      v_Pix_Insert := 'INSERT INTO pix_tran(tc_company_id, facility_id, company_code, tran_type, tran_code, tran_nbr, sys_user_id, pix_seq_nbr, proc_stat_code, xml_group_id, create_date_time, mod_date_time, user_id, ' || 'item_name, item_id, season, season_yr, style, style_sfx, color, color_sfx, sec_dim, qual, size_desc, invn_adjmt_qty,wt_adjmt_qty, invn_adjmt_type, whse' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias.', '')
        ELSE
          ''
        END) || ') SELECT :company_id, :facility_id, :company_code, ''605'', ''13'', :tran_nbr, :user_id, row_number() over (order by pld.item_id) + :count, :v_proc_stat_code, :v_xml_group_attr, :create_date_time, :mod_date_time, :user_id, ' || 'psku.item_name, pld.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, pld.actl_invn_qty,nvl(pld.wt_adjmt_qty,0), NULL, :g_warehouse' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'pld')
        ELSE
          ''
        END) || ' FROM (SELECT si.item_id, SUM(pd.on_hand_qty) AS actl_invn_qty, ' ||' sum(pd.on_hand_qty * (case when psik.item_avg_wt = 0 then nvl(psik.unit_weight,0) else nvl(psik.item_avg_wt,0) end)) as wt_adjmt_qty ' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'si')
        ELSE
          ''
        END) || ' FROM gen605_wm_inventory_gtt pd INNER JOIN gen605_sku_attr_gtt si ON pd.sku_attr_id = si.sku_attr_id ' || ' INNER JOIN gen605_sku_gtt psik ON psik.item_id = si.item_id ' || ' WHERE pd.flag = ''C'' and pd.locn_class = ''C'' and  pd.allocable = 0 ' || ' GROUP BY si.item_id' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'si')
        ELSE
          ''
        END) || ') pld INNER JOIN gen605_sku_gtt psku ON psku.item_id = pld.item_id GROUP BY psku.item_name, pld.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, pld.actl_invn_qty,pld.wt_adjmt_qty ' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'pld')
        ELSE
          ''
        END);
      v_Pix_Insert := v_Pix_Insert || ' log errors (to_char(sysdate))';
      EXECUTE IMMEDIATE v_pix_insert USING g_company_id,
      g_facility_id,
      g_company_code,
      g_tran_nbr,
      g_user_id,
      g_count,
      v_proc_stat_code,
      v_xml_group_attr,
      g_mod_date_time,
      g_mod_date_time,
      g_user_id,
      g_warehouse;
      g_count := g_count + SQL%ROWCOUNT;
    END IF;
    log('Generated Active Inventory (CasePick Locations) Pix Records (TRAN_CODE = 13).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 2);
  END IF;
  log('Finished Active Inventory PIX generation', TRUE, 1);
  -- Process Allocable Transitional Inventory PIX
  log('Started Transitional Inventory PIX generation', TRUE, 1);
  manh_get_pix_config_data ('605','04',NULL,g_company_id,v_proc_stat_code,v_xml_group_attr);
  IF (v_proc_stat_code != 91) THEN
    IF v_xml_group_attr IS NULL THEN
      v_xml_group_attr  := NULL;
    ELSE
      v_xml_group_attr := v_xml_group_attr;
    END IF;
    v_Pix_Insert := 'INSERT INTO pix_tran(tc_company_id, facility_id, company_code, tran_type, tran_code, tran_nbr, sys_user_id, pix_seq_nbr, proc_stat_code, xml_group_id, create_date_time, mod_date_time, user_id, ' || 'item_name, item_id, season, season_yr, style, style_sfx, color, color_sfx, sec_dim, qual, size_desc, invn_adjmt_qty,wt_adjmt_qty, whse' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias.', '')
      ELSE
        ''
      END) || ') SELECT :company_id, :facility_id, :company_code, ''605'', ''04'', :tran_nbr, :user_id, row_number() over (order by ti.item_id) + :count, :v_proc_stat_code, :v_xml_group_attr, :create_date_time, :mod_date_time, :user_id, ' || 'psku.item_name, ti.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, ti.actl_invn_units, nvl(ti.wt_adjmt_qty,0), :g_warehouse ' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'ti')
      ELSE
        ''
      END) || ' FROM (SELECT SI.item_id, SUM(T.on_hand_qty) AS ACTL_INVN_UNITS, ' ||' sum(t.on_hand_qty * (case when pku.item_avg_wt = 0 then nvl(pku.unit_weight,0) else nvl(pku.item_avg_wt,0) end)) as wt_adjmt_qty ' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'si')
      ELSE
        ''
      END) || ' FROM gen605_wm_inventory_gtt t INNER JOIN gen605_sku_attr_gtt si ON t.sku_attr_id = si.sku_attr_id  INNER JOIN gen605_sku_gtt pku ON pku.item_id = si.item_id WHERE t.allocable = 1 and  t.flag = ''T'' ' || ' GROUP BY si.item_id' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'si')
      ELSE
        ''
      END) || ') ti INNER JOIN gen605_sku_gtt psku ON psku.item_id = ti.item_id GROUP BY psku.item_name, ti.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, ti.actl_invn_units, ti.wt_adjmt_qty ' ||
    (
      CASE
      WHEN g_Group_SkuAttr_Flag THEN
        REPLACE(g_Sku_Attributes, 'alias', 'ti')
      ELSE
        ''
      END);
    v_Pix_Insert := v_Pix_Insert || ' log errors (to_char(sysdate))';
    EXECUTE IMMEDIATE v_pix_insert USING g_company_id,
    g_facility_id,
    g_company_code,
    g_tran_nbr,
    g_user_id,
    g_count,
    v_proc_stat_code,
    v_xml_group_attr,
    g_mod_date_time,
    g_mod_date_time,
    g_user_id,
    g_warehouse;
    g_count := g_count + SQL%ROWCOUNT;
  END IF;
  log('Generated Allocable Transitional Inventory Pix Records (TRAN_CODE = 04).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 2);
  -- Process Un-allocable Transitional Inventory PIX where TRAN_INVN summary is not requested
  IF g_unalloc_invn_flag THEN
    manh_get_pix_config_data ('605','14',NULL,g_company_id,v_proc_stat_code,v_xml_group_attr);
    IF (v_proc_stat_code != 91) THEN
      IF v_xml_group_attr IS NULL THEN
        v_xml_group_attr  := NULL;
      ELSE
        v_xml_group_attr := v_xml_group_attr;
      END IF;
      v_Pix_Insert := 'INSERT INTO pix_tran(tc_company_id, facility_id, company_code, tran_type, tran_code, tran_nbr, sys_user_id, pix_seq_nbr, proc_stat_code, xml_group_id, create_date_time, mod_date_time, user_id, ' || 'item_name, item_id, season, season_yr, style, style_sfx, color, color_sfx, sec_dim, qual, size_desc, invn_adjmt_qty, invn_adjmt_type, whse' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias.', '')
        ELSE
          ''
        END) || ') SELECT :company_id, :facility_id, :company_code, ''605'', ''14'', :tran_nbr, :user_id, row_number() over (order by ti.item_id) + :count, :v_proc_stat_code, :v_xml_group_attr, :create_date_time, :mod_date_time, :user_id, ' || 'psku.item_name, ti.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, ti.actl_invn_units, NULL, :g_warehouse' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'ti')
        ELSE
          ''
        END) || ' FROM (SELECT si.item_id, sum(t.on_hand_qty) AS actl_invn_units ' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'si')
        ELSE
          ''
        END) || ' FROM gen605_wm_inventory_gtt t INNER JOIN gen605_sku_attr_gtt si ON t.sku_attr_id = si.sku_attr_id WHERE t.allocable = 0 and  t.flag = ''T'' ' || ' GROUP BY si.item_id' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'SI')
        ELSE
          ''
        END) || ') tI INNER JOIN gen605_sku_gtt psku ON psku.item_id = ti.item_id GROUP BY psku.item_name, ti.item_id, psku.season, psku.season_yr, psku.style, psku.style_sfx, psku.color, psku.color_sfx, psku.sec_dim, psku.qual, psku.size_desc, ti.actl_invn_units ' ||
      (
        CASE
        WHEN g_Group_SkuAttr_Flag THEN
          REPLACE(g_Sku_Attributes, 'alias', 'ti')
        ELSE
          ''
        END);
      v_Pix_Insert := v_Pix_Insert || ' log errors (to_char(sysdate))';
      EXECUTE IMMEDIATE v_pix_insert USING g_company_id,
      g_facility_id,
      g_company_code,
      g_tran_nbr,
      g_user_id,
      g_count,
      v_proc_stat_code,
      v_xml_group_attr,
      g_mod_date_time,
      g_mod_date_time,
      g_user_id,
      g_warehouse;
      g_count := g_count + SQL%ROWCOUNT;
    END IF;
    log('Generated Un-allocable Transitional Inventory Pix Records (TRAN_CODE = 14).  PIX Count = ' || SQL%ROWCOUNT, TRUE, 2);
  END IF;
  -- generate LXX/TXX/CXX/AXX
  IF g_unalloc_invn_flag THEN
    generate_unalloc_invn_lock_pix ;
  END IF;
  -- Process SRL_PIX_TRAN if SrlPix Flag is set
  IF g_srlnbrpix_flag THEN
    generate_detail_srlpix;
  END IF;
  log('Finished generation of Detail Level PIX.', TRUE);
EXCEPTION
WHEN OTHERS THEN
  raise;
END generate_detail_pix;
PROCEDURE generate_variance_pix(
    p_end_date  IN DATE,
    p_stat_code IN PLS_INTEGER )
AS
  v_tran_nbr pix_tran.tran_nbr%TYPE;
  v_proc_stat_code NUMBER;
  v_xml_group_attr VARCHAR(10);
BEGIN
  log('Started generation of Variance PIX', TRUE, 1);
  -- Get the Tran_Nbr value for writing Variance PIXs
  SELECT pix_tran_id_seq.nextval + 1
  INTO v_tran_nbr
  FROM DUAL;
  -- Generate PIX for PIX_SUMM.ON_HAND_VAR_ALLOC <> 0
  manh_get_pix_config_data ('618','55','01',g_company_id,v_proc_stat_code,v_xml_group_attr);
  IF (v_proc_stat_code != 91) THEN
    IF v_xml_group_attr IS NULL THEN
      v_xml_group_attr  := NULL;
    ELSE
      v_xml_group_attr := v_xml_group_attr;
    END IF;
    INSERT
    INTO pix_tran
      (
        pix_tran_id,
        tc_company_id,
        facility_id,
        company_code,
        tran_type,
        tran_code,
        tran_nbr,
        sys_user_id,
        pix_seq_nbr,
        proc_stat_code,
        xml_group_id,
        item_name,
        item_id,
        season,
        season_yr,
        style,
        style_sfx,
        color,
        color_sfx,
        sec_dim,
        qual,
        size_desc,
        invn_type,
        prod_stat,
        batch_nbr,
        sku_attr_1,
        sku_attr_2,
        sku_attr_3,
        sku_attr_4,
        sku_attr_5,
        cntry_of_orgn,
        invn_adjmt_qty,
        invn_adjmt_type,
        actn_code,
        create_date_time,
        mod_date_time,
        user_id,
        whse
      )
    SELECT
      /*+INDEX(ps,PIX_SUMM_IND_3)*/
      pix_tran_id_seq.nextval,
      g_company_id,
      g_facility_id,
      g_company_code,
      '618',
      '55',
      v_Tran_Nbr,
      g_user_id,
      (g_Count + row_number() over (order by ps.item_id)),
      v_proc_stat_code,
      v_xml_group_attr,
      sku.item_name,
      ps.item_id,
      sku.season,
      sku.season_yr,
      sku.style,
      sku.style_sfx,
      sku.color,
      sku.color_sfx,
      sku.sec_dim,
      sku.qual,
      sku.size_desc,
      ps.invn_type,
      ps.prod_stat,
      ps.batch_nbr,
      ps.sku_attr_1,
      ps.sku_attr_2,
      ps.sku_attr_3,
      ps.sku_attr_4,
      ps.sku_attr_5,
      ps.cntry_of_orgn,
      ps.on_hand_var_alloc,
      (
      CASE
        WHEN ps.on_hand_var_alloc > 0
        THEN 'A'
        ELSE 'S'
      END),
      '01',
      g_Mod_Date_Time,
      g_Mod_Date_Time,
      g_User_Id,
      g_warehouse
    FROM pix_summ ps
    INNER JOIN gen605_sku_gtt sku
    ON sku.item_id = ps.item_id
    INNER JOIN gen605_sku_attr_gtt sa
    ON ps.item_id                       = sa.item_id
    AND COALESCE(ps.invn_type, ' ')     = COALESCE(sa.invn_type, ' ')
    AND COALESCE(ps.prod_stat, ' ')     = COALESCE(sa.prod_stat, ' ')
    AND COALESCE(ps.batch_nbr, ' ')     = COALESCE(sa.batch_nbr, ' ')
    AND COALESCE(ps.sku_attr_1, ' ')    = COALESCE(sa.sku_attr_1, ' ')
    AND COALESCE(ps.sku_attr_2, ' ')    = COALESCE(sa.sku_attr_2, ' ')
    AND COALESCE(ps.sku_attr_3, ' ')    = COALESCE(sa.sku_attr_3, ' ')
    AND COALESCE(ps.sku_attr_4, ' ')    = COALESCE(sa.sku_attr_4, ' ')
    AND COALESCE(ps.sku_attr_5, ' ')    = COALESCE(sa.sku_attr_5, ' ')
    AND COALESCE(ps.cntry_of_orgn, ' ') = COALESCE(sa.cntry_of_orgn, ' ')
    WHERE ps.end_summ_date_time         = p_End_Date
    AND ps.facility_id                  = g_facility_id
    AND ps.stat_code                    = p_stat_code
    AND ps.on_hand_var_alloc           <> 0 log errors (TO_CHAR(sysdate));
    g_count                            := g_count + SQL%ROWCOUNT;
  END IF;
  -- Generate PIX for PIX_SUMM.ON_HAND_VAR_UNALLOC <> 0
  manh_get_pix_config_data ('618','55','02',g_company_id,v_proc_stat_code,v_xml_group_attr);
  IF (v_proc_stat_code != 91) THEN
    IF v_xml_group_attr IS NULL THEN
      v_xml_group_attr  := NULL;
    ELSE
      v_xml_group_attr := v_xml_group_attr;
    END IF;
    INSERT
    INTO pix_tran
      (
        pix_tran_id,
        tc_company_id,
        facility_id,
        company_code,
        tran_type,
        tran_code,
        tran_nbr,
        sys_user_id,
        pix_seq_nbr,
        proc_stat_code,
        xml_group_id,
        item_name,
        item_id,
        season,
        season_yr,
        style,
        style_sfx,
        color,
        color_sfx,
        sec_dim,
        qual,
        size_desc,
        invn_type,
        prod_stat,
        batch_nbr,
        sku_attr_1,
        sku_attr_2,
        sku_attr_3,
        sku_attr_4,
        sku_attr_5,
        cntry_of_orgn,
        invn_adjmt_qty,
        invn_adjmt_type,
        actn_code,
        create_date_time,
        mod_date_time,
        user_id,
        whse
      )
    SELECT pix_tran_id_seq.nextval,
      g_company_id,
      g_facility_id,
      g_company_code,
      '618',
      '55',
      v_Tran_Nbr,
      g_user_id,
      (g_Count + row_number() over (order by ps.item_id)),
      v_proc_stat_code,
      v_xml_group_attr,
      sku.item_name,
      ps.item_id,
      sku.season,
      sku.season_yr,
      sku.style,
      sku.style_sfx,
      sku.color,
      sku.color_sfx,
      sku.sec_dim,
      sku.qual,
      sku.size_desc,
      ps.invn_type,
      ps.prod_stat,
      ps.batch_nbr,
      ps.sku_attr_1,
      ps.sku_attr_2,
      ps.sku_attr_3,
      ps.sku_attr_4,
      ps.sku_attr_5,
      ps.cntry_of_orgn,
      ps.on_hand_var_unalloc,
      (
      CASE
        WHEN ps.on_hand_var_unalloc > 0
        THEN 'A'
        ELSE 'S'
      END),
      '02',
      g_Mod_Date_Time,
      g_Mod_Date_Time,
      g_User_Id,
      g_warehouse
    FROM pix_summ ps
    INNER JOIN gen605_sku_gtt sku
    ON sku.item_id = ps.item_id
    INNER JOIN gen605_sku_attr_gtt sa
    ON ps.item_id                       = sa.item_id
    AND COALESCE(ps.invn_type, ' ')     = COALESCE(sa.invn_type, ' ')
    AND COALESCE(ps.prod_stat, ' ')     = COALESCE(sa.prod_stat, ' ')
    AND COALESCE(ps.batch_nbr, ' ')     = COALESCE(sa.batch_nbr, ' ')
    AND COALESCE(ps.sku_attr_1, ' ')    = COALESCE(sa.sku_attr_1, ' ')
    AND COALESCE(ps.sku_attr_2, ' ')    = COALESCE(sa.sku_attr_2, ' ')
    AND COALESCE(ps.sku_attr_3, ' ')    = COALESCE(sa.sku_attr_3, ' ')
    AND COALESCE(ps.sku_attr_4, ' ')    = COALESCE(sa.sku_attr_4, ' ')
    AND COALESCE(ps.sku_attr_5, ' ')    = COALESCE(sa.sku_attr_5, ' ')
    AND COALESCE(ps.cntry_of_orgn, ' ') = COALESCE(sa.cntry_of_orgn, ' ')
    WHERE ps.end_summ_date_time         = p_End_Date
    AND ps.facility_id                  = g_facility_id
    AND ps.stat_code                    = p_Stat_Code
    AND ps.on_hand_var_unalloc         <> 0 log errors (TO_CHAR(sysdate));
    g_count                            := g_count + SQL%ROWCOUNT;
  END IF;
  -- Generate PIX for PIX_SUMM.ALLOCATED_VAR <> 0
  manh_get_pix_config_data ('618','55','03',g_company_id,v_proc_stat_code,v_xml_group_attr);
  IF (v_proc_stat_code != 91) THEN
    IF v_xml_group_attr IS NULL THEN
      v_xml_group_attr  := NULL;
    ELSE
      v_xml_group_attr := v_xml_group_attr;
    END IF;
    INSERT
    INTO pix_tran
      (
        pix_tran_id,
        tc_company_id,
        facility_id,
        company_code,
        tran_type,
        tran_code,
        tran_nbr,
        sys_user_id,
        pix_seq_nbr,
        proc_stat_code,
        xml_group_id,
        item_name,
        item_id,
        season,
        season_yr,
        style,
        style_sfx,
        color,
        color_sfx,
        sec_dim,
        qual,
        size_desc,
        invn_type,
        prod_stat,
        batch_nbr,
        sku_attr_1,
        sku_attr_2,
        sku_attr_3,
        sku_attr_4,
        sku_attr_5,
        cntry_of_orgn,
        invn_adjmt_qty,
        invn_adjmt_type,
        actn_code,
        create_date_time,
        mod_date_time,
        user_id,
        whse
      )
    SELECT pix_tran_id_seq.nextval,
      g_company_id,
      g_facility_id,
      g_company_code,
      '618',
      '55',
      v_Tran_Nbr,
      g_user_id,
      (g_Count + row_number() over (order by ps.item_id)),
      v_proc_stat_code,
      v_xml_group_attr,
      sku.item_name,
      ps.item_id,
      sku.season,
      sku.season_yr,
      sku.style,
      sku.style_sfx,
      sku.color,
      sku.color_sfx,
      sku.sec_dim,
      sku.qual,
      sku.size_desc,
      ps.invn_type,
      ps.prod_stat,
      ps.batch_nbr,
      ps.sku_attr_1,
      ps.sku_attr_2,
      ps.sku_attr_3,
      ps.sku_attr_4,
      ps.sku_attr_5,
      ps.cntry_of_orgn,
      ps.allocated_var,
      (
      CASE
        WHEN ps.allocated_var > 0
        THEN 'A'
        ELSE 'S'
      END),
      '03',
      g_Mod_Date_Time,
      g_Mod_Date_Time,
      g_User_Id,
      g_warehouse
    FROM pix_summ ps
    INNER JOIN gen605_sku_gtt sku
    ON sku.item_id = ps.item_id
    INNER JOIN gen605_sku_attr_gtt sa
    ON ps.item_id                       = sa.item_id
    AND COALESCE(ps.invn_type, ' ')     = COALESCE(sa.invn_type, ' ')
    AND COALESCE(ps.prod_stat, ' ')     = COALESCE(sa.prod_stat, ' ')
    AND COALESCE(ps.batch_nbr, ' ')     = COALESCE(sa.batch_nbr, ' ')
    AND COALESCE(ps.sku_attr_1, ' ')    = COALESCE(sa.sku_attr_1, ' ')
    AND COALESCE(ps.sku_attr_2, ' ')    = COALESCE(sa.sku_attr_2, ' ')
    AND COALESCE(ps.sku_attr_3, ' ')    = COALESCE(sa.sku_attr_3, ' ')
    AND COALESCE(ps.sku_attr_4, ' ')    = COALESCE(sa.sku_attr_4, ' ')
    AND COALESCE(ps.sku_attr_5, ' ')    = COALESCE(sa.sku_attr_5, ' ')
    AND COALESCE(ps.cntry_of_orgn, ' ') = COALESCE(sa.cntry_of_orgn, ' ')
    WHERE ps.end_summ_date_time         = p_End_Date
    AND ps.facility_id                  = g_facility_id
    AND ps.stat_code                    = p_Stat_Code
    AND ps.allocated_var               <> 0 log errors (TO_CHAR(sysdate));
    g_count                            := g_count + SQL%ROWCOUNT;
  END IF;
  log('Generated variance Pix.  Record count = ' || g_Count, TRUE, 2);
  log('Finished generation of Variance PIX', TRUE, 1);
EXCEPTION
WHEN OTHERS THEN
  raise;
END generate_variance_pix;
PROCEDURE apply_variances_ui(
    p_season    IN VARCHAR2 DEFAULT NULL,
    p_season_yr IN VARCHAR2 DEFAULT NULL,
    p_style     IN VARCHAR2 DEFAULT NULL,
    p_style_sfx IN VARCHAR2 DEFAULT NULL,
    p_color     IN VARCHAR2 DEFAULT NULL,
    p_color_sfx IN VARCHAR2 DEFAULT NULL,
    p_sec_dim   IN VARCHAR2 DEFAULT NULL,
    p_qual      IN VARCHAR2 DEFAULT NULL,
    p_size_desc IN VARCHAR2 DEFAULT NULL,
    p_invn_type IN VARCHAR2 DEFAULT '*',
    p_prod_stat IN VARCHAR2 DEFAULT NULL,
    p_item_name IN VARCHAR2 DEFAULT NULL )
AS
  v_end_date DATE;
BEGIN
  log('Started applying variances', TRUE, 1);
  snap_item_master (p_season, p_season_yr, p_style, p_style_sfx, p_color, p_color_sfx, p_sec_dim, p_qual, p_size_desc, TRUE, p_item_name );
  snap_sku_attributes (p_invn_type, p_prod_stat);
  -- Get the Begin and Summary dates from PIX_SUMM
  SELECT MAX (ps.end_summ_date_time)
  INTO v_end_date
  FROM pix_summ ps
  INNER JOIN gen605_sku_attr_gtt sa
  ON sa.item_id                        = ps.item_id
  AND COALESCE (ps.invn_type, ' ')     = COALESCE (sa.invn_type, ' ')
  AND COALESCE (ps.prod_stat, ' ')     = COALESCE (sa.prod_stat, ' ')
  AND COALESCE (ps.batch_nbr, ' ')     = COALESCE (sa.batch_nbr, ' ')
  AND COALESCE (ps.sku_attr_1, ' ')    = COALESCE (sa.sku_attr_1, ' ')
  AND COALESCE (ps.sku_attr_2, ' ')    = COALESCE (sa.sku_attr_2, ' ')
  AND COALESCE (ps.sku_attr_3, ' ')    = COALESCE (sa.sku_attr_3, ' ')
  AND COALESCE (ps.sku_attr_4, ' ')    = COALESCE (sa.sku_attr_4, ' ')
  AND COALESCE (ps.sku_attr_5, ' ')    = COALESCE (sa.sku_attr_5, ' ')
  AND COALESCE (ps.cntry_of_orgn, ' ') = COALESCE (sa.cntry_of_orgn, ' ')
  WHERE ps.facility_id                 = g_facility_id
  AND ps.stat_code                     = 30;
  IF v_end_date                       IS NULL THEN
    log('There are no records found in PIX_SUMM with STAT_CODE = 30 for WHSE = ' || g_Warehouse || '.  Exiting...', TRUE, 2);
    log('Finished applying of variances', TRUE, 1);
    RETURN;
  END IF;
  log('Records that are in PIX_SUMM with STAT_CODE = 30 have END_SUMM_DATE_TIME = ' || TO_CHAR(v_End_Date, 'DD-Mon-YYYY HH24:Mi:SS'), TRUE, 2);
  -- Generate the Variance PIXes
  generate_variance_pix (v_end_date, 30);
  -- Now update the PIX_SUMM records as processed, because we have applied the variances.
  UPDATE pix_summ ps
  SET ps.stat_code            = 90,
    ps.mod_date_time          = g_mod_date_time,
    ps.user_id                = g_user_id
  WHERE ps.end_summ_date_time = v_end_date
  AND ps.facility_id          = g_facility_id
  AND ps.stat_code            = 30 log errors (TO_CHAR(sysdate));
  log('Updated PIX_SUMM records as processed.  Record count = ' || SQL%ROWCOUNT, TRUE, 2);
  log('Finished applying of variances', TRUE, 1);
EXCEPTION
WHEN OTHERS THEN
  raise;
END apply_variances_ui;
PROCEDURE generate_pix_summ(
    p_apply_variances IN BOOLEAN DEFAULT FALSE)
AS
TYPE r_pixsumm_dates
IS
  RECORD
  (
    begin_date DATE,
    end_date DATE );
TYPE t_skuid
IS
  TABLE OF pix_summ.item_id%TYPE INDEX BY BINARY_INTEGER;
TYPE t_invntype
IS
  TABLE OF pix_summ.invn_type%TYPE INDEX BY BINARY_INTEGER;
TYPE t_prodstat
IS
  TABLE OF pix_summ.prod_stat%TYPE INDEX BY BINARY_INTEGER;
TYPE t_batchnbr
IS
  TABLE OF pix_summ.batch_nbr%TYPE INDEX BY BINARY_INTEGER;
TYPE t_skuattr1
IS
  TABLE OF pix_summ.sku_attr_1%TYPE INDEX BY BINARY_INTEGER;
TYPE t_skuattr2
IS
  TABLE OF pix_summ.sku_attr_2%TYPE INDEX BY BINARY_INTEGER;
TYPE t_skuattr3
IS
  TABLE OF pix_summ.sku_attr_3%TYPE INDEX BY BINARY_INTEGER;
TYPE t_skuattr4
IS
  TABLE OF pix_summ.sku_attr_4%TYPE INDEX BY BINARY_INTEGER;
TYPE t_skuattr5
IS
  TABLE OF pix_summ.sku_attr_5%TYPE INDEX BY BINARY_INTEGER;
TYPE t_cntryoforgn
IS
  TABLE OF pix_summ.cntry_of_orgn%TYPE INDEX BY BINARY_INTEGER;
TYPE t_actlonhandalloc
IS
  TABLE OF pix_summ.actl_on_hand_alloc%TYPE INDEX BY BINARY_INTEGER;
TYPE t_actlonhandunalloc
IS
  TABLE OF pix_summ.actl_on_hand_unalloc%TYPE INDEX BY BINARY_INTEGER;
TYPE t_actlallocatedqty
IS
  TABLE OF pix_summ.actl_allocated_qty%TYPE INDEX BY BINARY_INTEGER;
TYPE t_endsummdatetime
IS
  TABLE OF pix_summ.end_summ_date_time%TYPE INDEX BY BINARY_INTEGER;
  vr_new r_pixsumm_dates;
  vt_skuid t_skuid;
  vt_invntype t_invntype;
  vt_prodstat t_prodstat;
  vt_batchnbr t_batchnbr;
  vt_skuattr1 t_skuattr1;
  vt_skuattr2 t_skuattr2;
  vt_skuattr3 t_skuattr3;
  vt_skuattr4 t_skuattr4;
  vt_skuattr5 t_skuattr5;
  vt_cntroforgn t_cntryoforgn;
  vt_beginonhandalloc t_actlonhandalloc;
  vt_beginonhandunalloc t_actlonhandunalloc;
  vt_beginallocatedqty t_actlallocatedqty;
  vt_endsummdatetime t_endsummdatetime;
  v_variancestatcode PLS_INTEGER := 0;
  val            INTEGER                    := 0;
  exp_no_pixtran EXCEPTION;
BEGIN
  log('Started generation of PIX_SUMM records', TRUE);
  vr_new.end_date := COALESCE(g_mod_date_time, sysdate);
  IF p_apply_variances THEN
    v_variancestatcode := 1;
  ELSE
    v_variancestatcode := 0;
  END IF;
  log('Current  PIX_SUMM: END_SUMM_DATE_TIME = ' || TO_CHAR(vr_new.End_Date, 'DD-Mon-YYYY HH24:MI:SS'), TRUE, 1);
  -- Get the next up for PIX_SUMM_ID
  --v_pix_summ_id :=
  --          get_nxtup_cnt ('*', '150', g_skuattr_count, g_lock_time_out);
  --Insert the keys in PIX_SUMM
  --If MVIEW add actl_on_hand_alloc, actl_on_hand_unalloc, actl_allocated_qty in this insert i.e. copy from gen605_sku_gtt_invn
  SELECT COUNT (1)
  INTO val
  FROM pix_summ;
  INSERT
  INTO pix_summ
    (
      facility_id,
      item_id,
      begin_summ_date_time,
      end_summ_date_time,
      invn_type,
      prod_stat,
      batch_nbr,
      sku_attr_1,
      sku_attr_2,
      sku_attr_3,
      sku_attr_4,
      sku_attr_5,
      cntry_of_orgn,
      begin_on_hand_alloc,
      begin_on_hand_unalloc,
      begin_allocated_qty,
      qty_from_rcpt_alloc,
      qty_from_rcpt_unalloc,
      qty_from_adj_alloc,
      qty_from_adj_unalloc,
      qty_from_shpmt,
      qty_from_allocation,
      actl_on_hand_alloc,
      actl_on_hand_unalloc,
      actl_allocated_qty,
      stat_code,
      create_date_time,
      mod_date_time,
      user_id,
      whse
    )
  SELECT g_facility_id,
    ps.item_id,
    COALESCE(ps.begin_date_time, sysdate),
    vr_new.end_date,
    invn_type,
    prod_stat,
    batch_nbr,
    sku_attr_1,
    sku_attr_2,
    sku_attr_3,
    sku_attr_4,
    sku_attr_5,
    cntry_of_orgn,
    (SUM(si.on_hand_qty    - si.qty_not_alloc) - (qty_from_rcpt_alloc + qty_from_adj_alloc - qty_from_shpmt)) AS begin_on_hand_alloc,
    (SUM(si.qty_not_alloc) - (qty_from_rcpt_unalloc + qty_from_adj_unalloc))                                  AS begin_on_hand_unalloc,
    (SUM(si.qty_alloc)     - (qty_from_allocation - qty_from_shpmt))                                          AS begin_allocated_qty,
    qty_from_rcpt_alloc,
    qty_from_rcpt_unalloc,
    qty_from_adj_alloc,
    qty_from_adj_unalloc,
    qty_from_shpmt,
    qty_from_allocation,
    SUM(si.on_hand_qty - si.qty_not_alloc),
    SUM(si.qty_not_alloc),
    SUM(si.qty_alloc),
    (
    CASE
      WHEN v_VarianceStatCode = 1
      THEN 90
      ELSE 20
    END),
    g_Mod_Date_Time,
    g_Mod_Date_Time,
    g_User_Id,
    g_warehouse
  FROM
    (SELECT ptt.sku_attr_id,
      sa.item_id,
      sa.invn_type,
      sa.prod_stat,
      sa.batch_nbr,
      sa.sku_attr_1,
      sa.sku_attr_2,
      sa.sku_attr_3,
      sa.sku_attr_4,
      sa.sku_attr_5,
      sa.cntry_of_orgn,
      SUM(ptt.qty_from_rcpt_alloc)   AS qty_from_rcpt_alloc,
      SUM(ptt.qty_from_rcpt_unalloc) AS qty_from_rcpt_unalloc,
      SUM(ptt.qty_from_adj_alloc)    AS qty_from_adj_alloc,
      SUM(ptt.qty_from_adj_unalloc)  AS qty_from_adj_unalloc,
      SUM(ptt.qty_from_shpmt)        AS qty_from_shpmt,
      SUM(ptt.qty_from_allocation)   AS qty_from_allocation,
      MIN(ptt.begin_date_time) begin_date_time
    FROM
      (SELECT pt.sku_attr_id,
        SUM(
        CASE
          WHEN ptc.invn_bucket = '1'
          THEN pt.invn_adjmt_qty
          ELSE 0
        END) AS qty_from_rcpt_alloc,
        SUM(
        CASE
          WHEN ptc.invn_bucket = '2'
          THEN pt.invn_adjmt_qty
          ELSE 0
        END) AS qty_from_rcpt_unalloc,
        SUM(
        CASE
          WHEN ptc.invn_bucket = '3'
          THEN ((
            CASE
              WHEN pt.invn_adjmt_type = 'A'
              THEN pt.invn_adjmt_qty
              ELSE 0
            END) - (
            CASE
              WHEN pt.invn_adjmt_type = 'S'
              THEN pt.invn_adjmt_qty
              ELSE 0
            END))
          ELSE 0
        END) AS qty_from_adj_alloc,
        SUM(
        CASE
          WHEN ptc.invn_bucket = '4'
          THEN ((
            CASE
              WHEN pt.invn_adjmt_type = 'A'
              THEN pt.invn_adjmt_qty
              ELSE 0
            END) - (
            CASE
              WHEN pt.invn_adjmt_type = 'S'
              THEN pt.invn_adjmt_qty
              ELSE 0
            END))
          ELSE 0
        END) AS qty_from_adj_unalloc,
        SUM(
        CASE
          WHEN ptc.invn_bucket = '5'
          THEN pt.invn_adjmt_qty
          ELSE 0
        END) AS qty_from_shpmt,
        SUM(
        CASE
          WHEN ptc.invn_bucket = '6'
          THEN ((
            CASE
              WHEN Pt.Invn_Adjmt_Type = 'A'
              THEN pt.invn_adjmt_qty
              ELSE 0
            END) - (
            CASE
              WHEN pt.invn_adjmt_type = 'S'
              THEN pt.invn_adjmt_qty
              ELSE 0
            END))
          ELSE 0
        END) AS qty_from_allocation,
        MIN(pt.create_date_time) begin_date_time
      FROM gen605_pix_tran_gtt pt
      INNER JOIN
        (SELECT DISTINCT tran_type,
          tran_code,
          invn_bucket
        FROM pix_intrnl_config
        ) ptc
      ON pt.tran_type            = ptc.tran_type
      AND pt.tran_code           = ptc.tran_code
      WHERE pt.create_date_time >= COALESCE(
        (SELECT MAX(ps.end_summ_date_time)
        FROM pix_summ ps
        INNER JOIN gen605_sku_attr_gtt sa
        ON ps.item_id                       = sa.item_id
        AND COALESCE(ps.invn_type, ' ')     = COALESCE(sa.Invn_Type, ' ')
        AND COALESCE(ps.prod_stat, ' ')     = COALESCE(sa.Prod_Stat, ' ')
        AND COALESCE(ps.batch_nbr, ' ')     = COALESCE(sa.Batch_Nbr, ' ')
        AND COALESCE(ps.sku_attr_1, ' ')    = COALESCE(sa.Sku_Attr_1, ' ')
        AND COALESCE(ps.sku_attr_2, ' ')    = COALESCE(sa.Sku_Attr_2, ' ')
        AND COALESCE(ps.sku_attr_3, ' ')    = COALESCE(sa.Sku_Attr_3, ' ')
        AND COALESCE(ps.sku_attr_4, ' ')    = COALESCE(sa.Sku_Attr_4, ' ')
        AND COALESCE(ps.sku_attr_5, ' ')    = COALESCE(sa.Sku_Attr_5, ' ')
        AND COALESCE(ps.cntry_of_orgn, ' ') = COALESCE(sa.Cntry_Of_Orgn, ' ')
        WHERE ps.facility_id                = g_facility_id
        AND ps.stat_code                    = 90
        AND sa.sku_attr_id                  = pt.sku_attr_id
        ),pt.create_date_time)-- vr_new.Begin_Date
      AND pt.create_date_time < vr_new.end_date
      GROUP BY pt.sku_attr_id,
        ptc.invn_bucket,
        pt.invn_adjmt_type
      ) ptt
    INNER JOIN gen605_sku_attr_gtt sa
    ON ptt.sku_attr_id = sa.sku_attr_id
    GROUP BY ptt.sku_attr_id,
      sa.item_id,
      sa.invn_type,
      sa.prod_stat,
      sa.batch_nbr,
      sa.sku_attr_1,
      sa.sku_attr_2,
      sa.sku_attr_3,
      sa.sku_attr_4,
      sa.sku_attr_5,
      sa.cntry_of_orgn
    ) ps
  INNER JOIN gen605_wm_inventory_gtt si
  ON ps.sku_attr_id = si.sku_attr_id
  GROUP BY si.sku_attr_id,
    ps.CNTRY_OF_ORGN,
    ps.SKU_ATTR_5,
    ps.SKU_ATTR_4,
    ps.SKU_ATTR_3,
    ps.SKU_ATTR_2,
    ps.SKU_ATTR_1,
    ps.prod_stat,
    ps.batch_nbr,
    ps.invn_type,
    ps.item_id,
    ps.begin_date_time,
    qty_from_allocation,
    qty_from_shpmt,
    qty_from_adj_unalloc,
    qty_from_adj_alloc,
    qty_from_rcpt_unalloc,
    qty_from_rcpt_alloc log errors (TO_CHAR(sysdate));
  log('Generated distinct Sku Attribute records in PIX_SUMM for sku / sku attr FOUND in pix_tran for given time frame.  Record count = ' || SQL%ROWCOUNT, TRUE, 1);
  -- Get the next up for PIX_SUMM_ID
  --v_pix_summ_id :=
  --          get_nxtup_cnt ('*', '150', g_skuattr_count, g_lock_time_out);
  -- Create record in pix_summ for sku / sku attr not found in pix_tran for given time frame
  -- So now for each sku found in sku_invn we will have record with pix_summ
  INSERT
  INTO pix_summ
    (
      facility_id,
      item_id,
      begin_summ_date_time,
      end_summ_date_time,
      invn_type,
      prod_stat,
      batch_nbr,
      sku_attr_1,
      sku_attr_2,
      sku_attr_3,
      sku_attr_4,
      sku_attr_5,
      cntry_of_orgn,
      begin_on_hand_alloc,
      begin_on_hand_unalloc,
      begin_allocated_qty,
      qty_from_rcpt_alloc,
      qty_from_rcpt_unalloc,
      qty_from_adj_alloc,
      qty_from_adj_unalloc,
      qty_from_shpmt,
      qty_from_allocation,
      actl_on_hand_alloc,
      actl_on_hand_unalloc,
      actl_allocated_qty,
      stat_code,
      create_date_time,
      mod_date_time,
      user_id,
      whse
    )
  SELECT g_facility_id,
    sa.item_id,--(ROW_NUMBER() OVER() + v_Pix_Summ_Id),
    COALESCE(vr_new.end_date, sysdate),
    vr_new.end_date,
    invn_type,
    prod_stat,
    batch_nbr,
    sku_attr_1,
    sku_attr_2,
    sku_attr_3,
    sku_attr_4,
    sku_attr_5,
    cntry_of_orgn,
    (SUM(si.on_hand_qty    - si.qty_not_alloc) - (0 + 0 - 0)) AS begin_on_hand_alloc,
    (SUM(si.qty_not_alloc) - (0 + 0))                         AS begin_on_hand_unalloc,
    (SUM(si.qty_alloc)     - (0 - 0))                         AS begin_allocated_qty,
    0 qty_from_rcpt_alloc,
    0 qty_from_rcpt_unalloc,
    0 qty_from_adj_alloc,
    0 qty_from_adj_unalloc,
    0 qty_from_shpmt,
    0 qty_from_allocation,
    SUM(si.on_hand_qty - si.qty_not_alloc),
    SUM(si.qty_not_alloc),
    SUM(si.qty_alloc),
    (
    CASE
      WHEN v_VarianceStatCode = 1
      THEN 90
      ELSE 0
    END),
    g_Mod_Date_Time,
    g_Mod_Date_Time,
    g_User_Id,
    g_warehouse
  FROM gen605_sku_attr_gtt sa
  INNER JOIN gen605_wm_inventory_gtt si
  ON sa.sku_attr_id = si.sku_attr_id
  WHERE NOT EXISTS
    (SELECT 1
    FROM pix_summ ps
    WHERE ps.item_id                    = sa.item_id
    AND COALESCE(ps.invn_type, ' ')     = COALESCE(sa.Invn_Type, ' ')
    AND COALESCE(ps.prod_stat, ' ')     = COALESCE(sa.Prod_Stat, ' ')
    AND COALESCE(ps.batch_nbr, ' ')     = COALESCE(sa.Batch_Nbr, ' ')
    AND COALESCE(ps.sku_attr_1, ' ')    = COALESCE(sa.Sku_Attr_1, ' ')
    AND COALESCE(ps.sku_attr_2, ' ')    = COALESCE(sa.Sku_Attr_2, ' ')
    AND COALESCE(ps.sku_attr_3, ' ')    = COALESCE(sa.Sku_Attr_3, ' ')
    AND COALESCE(ps.sku_attr_4, ' ')    = COALESCE(sa.Sku_Attr_4, ' ')
    AND COALESCE(ps.sku_attr_5, ' ')    = COALESCE(sa.Sku_Attr_5, ' ')
    AND COALESCE(ps.cntry_of_orgn, ' ') = COALESCE(sa.Cntry_Of_Orgn, ' ')
    AND ps.end_summ_date_time           = vr_new.end_date
    )
  GROUP BY si.sku_attr_id,
    sa.CNTRY_OF_ORGN,
    sa.SKU_ATTR_5,
    sa.SKU_ATTR_4,
    sa.SKU_ATTR_3,
    sa.SKU_ATTR_2,
    sa.SKU_ATTR_1,
    sa.prod_stat,
    sa.BATCH_NBR,
    sa.INVN_TYPE,
    sa.item_id log errors (TO_CHAR(sysdate));
  log('Generated record in pix_summ for sku / sku attr NOT FOUND in pix_tran for given time frame.  Record count = ' || SQL%ROWCOUNT, TRUE, 1);
  -- UPDATING PIX SUMM (begin_on_hand_alloc =0,ETC) IF  RECORDS ARE INSERTED FOR THE FIRST TIME
  UPDATE PIX_SUMM PS
  SET PS.BEGIN_ON_HAND_ALLOC = 0,
    PS.BEGIN_ON_HAND_UNALLOC = 0,
    PS.BEGIN_ALLOCATED_QTY   = 0,
    PS.BEGIN_SUMM_DATE_TIME  =
    (SELECT COALESCE(MIN(PT.CREATE_DATE_TIME),vr_new.end_date)
    FROM gen605_pix_tran_gtt PT,
      gen605_sku_attr_gtt SA,
      PIX_INTRNL_CONFIG PTC
    WHERE PT.SKU_ATTR_ID                = SA.SKU_ATTR_ID
    AND PT.TRAN_TYPE                    = PTC.TRAN_TYPE
    AND PT.TRAN_CODE                    = PTC.TRAN_CODE
    AND SA.ITEM_ID                      = PS.ITEM_ID
    AND COALESCE(sa.invn_type, ' ')     = COALESCE(ps.Invn_Type, ' ')
    AND COALESCE(sa.prod_stat, ' ')     = COALESCE(ps.Prod_Stat, ' ')
    AND COALESCE(sa.batch_nbr, ' ')     = COALESCE(ps.Batch_Nbr, ' ')
    AND COALESCE(sa.sku_attr_1, ' ')    = COALESCE(ps.Sku_Attr_1, ' ')
    AND COALESCE(sa.sku_attr_2, ' ')    = COALESCE(ps.Sku_Attr_2, ' ')
    AND COALESCE(sa.sku_attr_3, ' ')    = COALESCE(ps.Sku_Attr_3, ' ')
    AND COALESCE(sa.sku_attr_4, ' ')    = COALESCE(ps.Sku_Attr_4, ' ')
    AND COALESCE(sa.sku_attr_5, ' ')    = COALESCE(ps.Sku_Attr_5, ' ')
    AND COALESCE(sa.cntry_of_orgn, ' ') = COALESCE(ps.Cntry_Of_Orgn, ' ')
    )
  WHERE NOT EXISTS
    (SELECT 'X'
    FROM pix_summ ps2
    WHERE ps2.facility_id                = ps.facility_id
    AND ps2.item_id                      = ps.item_id
    AND COALESCE(ps2.invn_type, ' ')     = COALESCE(ps.Invn_Type, ' ')
    AND COALESCE(ps2.prod_stat, ' ')     = COALESCE(ps.Prod_Stat, ' ')
    AND COALESCE(ps2.batch_nbr, ' ')     = COALESCE(ps.Batch_Nbr, ' ')
    AND COALESCE(ps2.sku_attr_1, ' ')    = COALESCE(ps.Sku_Attr_1, ' ')
    AND COALESCE(ps2.sku_attr_2, ' ')    = COALESCE(ps.Sku_Attr_2, ' ')
    AND COALESCE(ps2.sku_attr_3, ' ')    = COALESCE(ps.Sku_Attr_3, ' ')
    AND COALESCE(ps2.sku_attr_4, ' ')    = COALESCE(ps.Sku_Attr_4, ' ')
    AND COALESCE(ps2.sku_attr_5, ' ')    = COALESCE(ps.Sku_Attr_5, ' ')
    AND COALESCE(ps2.cntry_of_orgn, ' ') = COALESCE(ps.Cntry_Of_Orgn, ' ')
    AND ps2.end_summ_date_time          <> vr_new.end_date
    )
  AND ps.facility_id = g_facility_id log errors (TO_CHAR(sysdate));
  IF val             < 1 THEN
    UPDATE pix_summ
    SET begin_summ_date_time =
      (SELECT COALESCE (MIN (vpt.create_date_time), TO_DATE('01/01/1900','DD/MM/YYYY') )
      FROM gen605_pix_tran_gtt vpt
      )
    WHERE create_date_time = g_mod_date_time log errors (TO_CHAR(sysdate));
  ELSE
    UPDATE pix_summ
    SET begin_summ_date_time = COALESCE(
      (SELECT MAX (ps.end_summ_date_time)
      FROM pix_summ ps
      WHERE end_summ_date_time < g_mod_date_time
      AND stat_code            = 90
      ), sysdate)
    WHERE create_date_time = g_mod_date_time log errors (TO_CHAR(sysdate));
  END IF;
  -- now update the BEGIN_ON_HAND_ALLOC, BEGIN_ON_HAND_UNALLOC and BEGIN_ALLOCATED_QTY fields from previous PIX_SUMM data
  SELECT ps.item_id,
    ps.invn_type,
    ps.prod_stat,
    ps.batch_nbr,
    ps.sku_attr_1,
    ps.sku_attr_2,
    ps.sku_attr_3,
    ps.sku_attr_4,
    ps.sku_attr_5,
    ps.cntry_of_orgn,
    SUM (ps.actl_on_hand_alloc),
    SUM (ps.actl_on_hand_unalloc),
    SUM (ps.actl_allocated_qty),
    MIN (COALESCE(ps.end_summ_date_time, TO_DATE('01/01/1900','DD/MM/YYYY'))) BULK COLLECT
  INTO vt_skuid,
    vt_invntype,
    vt_prodstat,
    vt_batchnbr,
    vt_skuattr1,
    vt_skuattr2,
    vt_skuattr3,
    vt_skuattr4,
    vt_skuattr5,
    vt_cntroforgn,
    vt_beginonhandalloc,
    vt_beginonhandunalloc,
    vt_beginallocatedqty,
    vt_endsummdatetime
  FROM pix_summ ps
  WHERE ps.end_summ_date_time =
    (SELECT MAX (ps2.end_summ_date_time)
    FROM pix_summ ps2
    WHERE ps2.whse                        = ps.whse
    AND ps2.stat_code                     = 90
    AND ps2.item_id                       = ps.item_id
    AND COALESCE (ps2.invn_type, ' ')     = COALESCE (ps.invn_type, ' ')
    AND COALESCE (ps2.prod_stat, ' ')     = COALESCE (ps.prod_stat, ' ')
    AND COALESCE (ps2.batch_nbr, ' ')     = COALESCE (ps.batch_nbr, ' ')
    AND COALESCE (ps2.sku_attr_1, ' ')    = COALESCE (ps.sku_attr_1, ' ')
    AND COALESCE (ps2.sku_attr_2, ' ')    = COALESCE (ps.sku_attr_2, ' ')
    AND COALESCE (ps2.sku_attr_3, ' ')    = COALESCE (ps.sku_attr_3, ' ')
    AND COALESCE (ps2.sku_attr_4, ' ')    = COALESCE (ps.sku_attr_4, ' ')
    AND COALESCE (ps2.sku_attr_5, ' ')    = COALESCE (ps.sku_attr_5, ' ')
    AND COALESCE (ps2.cntry_of_orgn, ' ') = COALESCE (ps.cntry_of_orgn, ' ')
    AND ps2.end_summ_date_time           <> vr_new.end_date
    GROUP BY ps2.whse,
      ps2.stat_code,
      ps2.item_id,
      ps2.invn_type,
      ps2.prod_stat,
      ps2.batch_nbr,
      ps2.sku_attr_1,
      ps2.sku_attr_2,
      ps2.sku_attr_3,
      ps2.sku_attr_4,
      ps2.sku_attr_5,
      ps2.cntry_of_orgn
    )
  AND ps.facility_id = g_facility_id
  AND ps.stat_code   = 90
  GROUP BY ps.item_id,
    ps.invn_type,
    ps.prod_stat,
    ps.batch_nbr,
    ps.sku_attr_1,
    ps.sku_attr_2,
    ps.sku_attr_3,
    ps.sku_attr_4,
    ps.sku_attr_5,
    ps.cntry_of_orgn;
  IF vt_skuid.COUNT > 0 THEN
    FORALL i       IN 1 .. vt_skuid.COUNT
    UPDATE pix_summ ps
    SET ps.begin_on_hand_alloc           = vt_beginonhandalloc (i),
      ps.begin_on_hand_unalloc           = vt_beginonhandunalloc (i),
      ps.begin_allocated_qty             = vt_beginallocatedqty (i),
      ps.begin_summ_date_time            = vt_endsummdatetime (i)
    WHERE ps.end_summ_date_time          = vr_new.end_date
    AND ps.item_id                       = vt_skuid (i)
    AND COALESCE (ps.invn_type, ' ')     = COALESCE (vt_invntype (i), ' ')
    AND COALESCE (ps.prod_stat, ' ')     = COALESCE (vt_prodstat (i), ' ')
    AND COALESCE (ps.batch_nbr, ' ')     = COALESCE (vt_batchnbr (i), ' ')
    AND COALESCE (ps.sku_attr_1, ' ')    = COALESCE (vt_skuattr1 (i), ' ')
    AND COALESCE (ps.sku_attr_2, ' ')    = COALESCE (vt_skuattr2 (i), ' ')
    AND COALESCE (ps.sku_attr_3, ' ')    = COALESCE (vt_skuattr3 (i), ' ')
    AND COALESCE (ps.sku_attr_4, ' ')    = COALESCE (vt_skuattr4 (i), ' ')
    AND COALESCE (ps.sku_attr_5, ' ')    = COALESCE (vt_skuattr5 (i), ' ')
    AND COALESCE (ps.cntry_of_orgn, ' ') = COALESCE (vt_cntroforgn (i), ' ') log errors (TO_CHAR(sysdate));
  END IF;
  vt_skuid.DELETE;
  vt_invntype.DELETE;
  vt_prodstat.DELETE;
  vt_batchnbr.DELETE;
  vt_skuattr1.DELETE;
  vt_skuattr2.DELETE;
  vt_skuattr3.DELETE;
  vt_skuattr4.DELETE;
  vt_skuattr5.DELETE;
  vt_cntroforgn.DELETE;
  vt_beginonhandalloc.DELETE;
  vt_beginonhandunalloc.DELETE;
  vt_beginallocatedqty.DELETE;
  /*** Update ON_HAND_VAR_ALLOC, ON_HAND_VAR_UNALLOC and ALLOCATED_VAR columns ***/
  UPDATE pix_summ ps
  SET ps.on_hand_var_alloc    = ps.actl_on_hand_alloc   - ( ps.begin_on_hand_alloc + ps.qty_from_rcpt_alloc + ps.qty_from_adj_alloc - ps.qty_from_shpmt ),
    ps.on_hand_var_unalloc    = ps.actl_on_hand_unalloc - ( ps.begin_on_hand_unalloc + ps.qty_from_rcpt_unalloc + ps.qty_from_adj_unalloc ),
    ps.allocated_var          = ps.actl_allocated_qty   - ( ps.begin_allocated_qty + ps.qty_from_allocation - ps.qty_from_shpmt )
  WHERE ps.end_summ_date_time = vr_new.end_date log errors (TO_CHAR(sysdate));
  log('Updated the VAR columns.  Record count = ' || SQL%ROWCOUNT, TRUE, 1);
  IF p_apply_variances THEN
    log('Generating  Variances Pix', TRUE);
    generate_variance_pix (vr_new.end_date, 90);
  END IF;
  log('Finished generation of PIX_SUMM records', TRUE);
EXCEPTION
WHEN exp_no_pixtran THEN
  raise_application_error (-20209, 'No data found in the PIX_TRAN snapshot' );
WHEN OTHERS THEN
  raise;
END generate_pix_summ;
PROCEDURE generate_608_pix(
    v_pix_type VARCHAR2 )
AS
  v_proc_stat_code NUMBER;
  v_xml_group_attr VARCHAR2(10);
BEGIN
  log('Started generation of 608 PIX', TRUE);
  IF (v_pix_type = 'B') THEN
    manh_get_pix_config_data ('608','00',NULL,g_company_id,v_proc_stat_code,v_xml_group_attr);
    IF (v_proc_stat_code  != 91) THEN
      IF v_xml_group_attr IS NULL THEN
        v_xml_group_attr  := NULL;
      ELSE
        v_xml_group_attr := v_xml_group_attr;
      END IF;
      INSERT
      INTO pix_tran
        (
          tran_type,
          tran_code,
          tran_nbr,
          sys_user_id,
          pix_seq_nbr,
          proc_stat_code,
          xml_group_id, --whse, co, div,
          create_date_time,
          mod_date_time,
          user_id,
          tc_company_id,
          facility_id,
          company_code,
          whse
          --sys_user_id
        )
        VALUES
        (
          '608',
          '00',
          g_tran_nbr,
          g_user_id,
          g_count + 1,
          v_proc_stat_code,
          v_xml_group_attr, --g_warehouse, g_company, g_division,
          g_mod_date_time,
          g_mod_date_time, --g_user_id,
          g_user_id,
          g_company_id,
          g_facility_id,
          g_company_code,
          g_warehouse
        )
        log errors
        (
          TO_CHAR(sysdate)
        );
      g_count := g_count + SQL%ROWCOUNT;
    END IF;
  elsif( v_pix_type = 'E') THEN
    manh_get_pix_config_data ('608','13',NULL,g_company_id,v_proc_stat_code,v_xml_group_attr);
    IF (v_proc_stat_code != 91) THEN
      IF v_xml_group_attr IS NULL THEN
        v_xml_group_attr  := NULL;
      ELSE
        v_xml_group_attr := v_xml_group_attr;
      END IF;
      INSERT
      INTO pix_tran
        (
          tran_type,
          tran_code,
          tran_nbr,
          sys_user_id,
          pix_seq_nbr,
          proc_stat_code,
          xml_group_id,--whse, co, div,
          create_date_time,
          mod_date_time,
          user_id,
          tc_company_id,
          facility_id,
          company_code,
          whse
          --sys_user_id
        )
        VALUES
        (
          '608',
          '13',
          g_tran_nbr,
          g_user_id,
          g_count + 1,
          v_proc_stat_code,
          v_xml_group_attr,--g_warehouse, g_company, g_division,
          g_mod_date_time,
          g_mod_date_time, --g_user_id,
          g_user_id,
          g_company_id,
          g_facility_id,
          g_company_code,
          g_warehouse
        )
        log errors
        (
          TO_CHAR(sysdate)
        );
      g_count := g_count + SQL%ROWCOUNT;
    END IF;
  END IF;
  log('Finished generation of 608 PIX.', TRUE);
EXCEPTION
WHEN OTHERS THEN
  raise;
END generate_608_pix;
PROCEDURE update_pix_ref_fields
AS
  v_msg VARCHAR2
  (
    32000
  )
  ;
  v_proc_stat_code pix_tran.proc_stat_code%type := 0;
  v_xml_group_attr pix_tran.xml_group_id%type;
  v_ref_codes_select_list VARCHAR2(1000);
  v_ref_codes_update_list VARCHAR2(1000);
  v_merge_sql             VARCHAR2(4000);
BEGIN
  log('start ref field update for pix', true);
  --todo! implement the array type data collection instead of select below
  FOR cur_pix IN
  ( SELECT DISTINCT pt.tran_type,
      pt.tran_code,
      pt.actn_code
    FROM pix_tran pt
    WHERE pt.tran_type IN ('605', '618')
    AND pt.tran_nbr     = g_tran_nbr
  )
  LOOP
    manh_get_pix_ref_code_col_list
    (
      cur_pix.tran_type,
      CASE
      WHEN SUBSTR(cur_pix.tran_code, 1, 1) = 'L' THEN
        'LXX'
      WHEN SUBSTR(cur_pix.tran_code, 1, 1) = 'T' THEN
        'TXX'
      WHEN SUBSTR(cur_pix.tran_code, 1, 1) = 'A' THEN
        'AXX'
      WHEN SUBSTR(cur_pix.tran_code, 1, 1) = 'C' THEN
        'CXX'
      ELSE
        cur_pix.tran_code
      END, cur_pix.actn_code, g_company_id, v_ref_codes_update_list, v_ref_codes_select_list, p_is_merge_stmt => 1
    )
    ;
    IF (v_ref_codes_select_list IS NULL AND g_company_id IS NOT NULL) THEN
      manh_get_pix_ref_code_col_list(cur_pix.tran_type,
      CASE
      WHEN SUBSTR(cur_pix.tran_code, 1, 1) = 'L' THEN
        'LXX'
      WHEN SUBSTR(cur_pix.tran_code, 1, 1) = 'T' THEN
        'TXX'
      WHEN SUBSTR(cur_pix.tran_code, 1, 1) = 'A' THEN
        'AXX'
      WHEN SUBSTR(cur_pix.tran_code, 1, 1) = 'C' THEN
        'CXX'
      ELSE
        cur_pix.tran_code
      END, cur_pix.actn_code, NULL, v_ref_codes_update_list, v_ref_codes_select_list, p_is_merge_stmt => 1);
    END IF;
    IF (v_ref_codes_update_list                IS NOT NULL) THEN
      v_merge_sql                              := 'merge into pix_tran ' || 'using ' || '(' || '    select pt.pix_tran_id ' || v_ref_codes_select_list || '    from pix_tran pt ' || '    where pt.tran_nbr = :g_tran_nbr and pt.tran_type = :tran_type ' || '        and pt.tran_code = :tran_code ' || '        and coalesce(pt.actn_code, '' '') = coalesce(:actn_code, '' '') ' || ') iv on (iv.pix_tran_id = pix_tran.pix_tran_id) ' || 'when matched then ' || 'update set mod_date_time = :g_mod_date_time, user_id = :g_user_id ' || v_ref_codes_update_list ;
      IF (instr(v_merge_sql, 'ITEM_CBO.', 1, 1) > 0) THEN
        v_merge_sql                            := REPLACE(v_merge_sql, 'where ', ' join item_cbo on item_cbo.item_id = pt.item_id ' || ' where ');
      END IF;
      IF (instr(v_merge_sql, 'ITEM_WMS.', 1, 1) > 0) THEN
        v_merge_sql                            := REPLACE(v_merge_sql, 'where ', ' join item_wms on item_wms.item_id = pt.item_id ' || ' where ');
      END IF;
      IF (instr(v_merge_sql, 'ITEM_FACILITY_MAPPING_WMS.', 1, 1) > 0) THEN
        v_merge_sql                                             := REPLACE(v_merge_sql, 'where ', ' join item_facility_mapping_wms on item_facility_mapping_wms.item_id = pt.item_id ' || ' and item_facility_mapping_wms.facility_id = ' || TO_CHAR(g_facility_id) || ' where ');
      END IF;
      v_merge_sql := v_merge_sql || ' log errors (to_char(sysdate))';
      EXECUTE immediate v_merge_sql USING g_tran_nbr,
      cur_pix.tran_type,
      cur_pix.tran_code,
      cur_pix.actn_code,
      g_mod_date_time,
      g_user_id;
    END IF;
  END LOOP;
EXCEPTION
WHEN no_data_found THEN
  v_msg := 'Skipping pix ref fields update';
  exception_msg_log_insert(v_msg);
END update_pix_ref_fields;
PROCEDURE generate_pix
  (
    p_login_user_id          IN VARCHAR2,
    p_company_id             IN NUMBER DEFAULT NULL,
    p_facility_id            IN NUMBER DEFAULT NULL,
    p_lock_time_out          IN VARCHAR2 DEFAULT 10,
    p_default_pix_level      IN VARCHAR2 DEFAULT 'D',
    p_case_lock_code         IN VARCHAR2 DEFAULT 'N',
    p_active_summ_by_lock    IN VARCHAR2 DEFAULT 'N',
    p_CasePick_summ_by_Lock  IN VARCHAR2 DEFAULT 'N',
    p_trans_invn_type        IN VARCHAR2 DEFAULT 'N',
    p_update_unproc_605_pix  IN VARCHAR2 DEFAULT 'N',
    p_include_unalloc_invn   IN VARCHAR2 DEFAULT 'N',
    p_group_sku_attributes   IN VARCHAR2 DEFAULT 'N',
    p_include_cons_prty_date IN VARCHAR2 DEFAULT 'N',
    p_include_srl_nbr_pix    IN VARCHAR2 DEFAULT 'N',
    p_season                 IN VARCHAR2 DEFAULT NULL,
    p_season_yr              IN VARCHAR2 DEFAULT NULL,
    p_style                  IN VARCHAR2 DEFAULT NULL,
    p_style_sfx              IN VARCHAR2 DEFAULT NULL,
    p_color                  IN VARCHAR2 DEFAULT NULL,
    p_color_sfx              IN VARCHAR2 DEFAULT NULL,
    p_sec_dim                IN VARCHAR2 DEFAULT NULL,
    p_qual                   IN VARCHAR2 DEFAULT NULL,
    p_size_desc              IN VARCHAR2 DEFAULT NULL,
    p_invn_type              IN VARCHAR2 DEFAULT '*',
    p_prod_stat              IN VARCHAR2 DEFAULT NULL,
    p_create_zero_inventory  IN VARCHAR2 DEFAULT 'N',
    p_reconcile              IN VARCHAR2 DEFAULT 'N',
    p_auto_apply_variance    IN INT DEFAULT 0,
    p_debug                  IN VARCHAR2 DEFAULT 'N',
    p_item_name              IN VARCHAR2,
    p_returncode OUT INT
  )
AS
  exp_user_info_notfound EXCEPTION;
  exp_invalid_pix_level  EXCEPTION;
  exp_invalid_variance   EXCEPTION;
  exp_previous_run       EXCEPTION;
  v_msg msg_log.msg%type;
  v_prev_module_name wm_utils.t_app_context_data;
  v_prev_action_name wm_utils.t_app_context_data;
BEGIN
  p_returncode    := 0;
  g_lock_time_out := p_lock_time_out;
  g_user_id       := p_login_user_id;
  g_mod_date_time := SYSDATE;
  g_debug         :=
  CASE
  WHEN p_debug = 'Y' THEN
    TRUE
  ELSE
    FALSE
  END;
  wm_utils.get_context_info(v_prev_module_name, v_prev_action_name);
  wm_utils.set_context_info(p_module_name => '605', p_action_name => 'Generate 605 Pix' , p_client_id => p_company_id || p_facility_id || p_default_pix_level || p_case_lock_code || p_active_summ_by_lock || p_casepick_summ_by_lock || p_trans_invn_type || p_update_unproc_605_pix || p_include_unalloc_invn || p_group_sku_attributes || p_include_cons_prty_date || p_include_srl_nbr_pix || p_invn_type || p_prod_stat || p_create_zero_inventory);
  IF P_COMPANY_ID IS NULL AND P_FACILITY_ID IS NULL THEN
    get_user_info (p_login_user_id, g_company_id, g_facility_id, g_lang_id, g_warehouse );
  ELSE
    g_company_id  := P_COMPANY_ID;
    g_facility_id := P_FACILITY_ID;
    g_lang_id     := get_lang_id (p_login_user_id);
  END IF;
  SELECT whse INTO g_warehouse FROM facility WHERE facility_id = g_facility_id;
  SELECT company_code
  INTO g_company_code
  FROM company
  WHERE company_id = g_company_id;
  -- Validate passed in PIX Level
  IF p_default_pix_level NOT IN ('D', 'S') THEN
    RAISE exp_invalid_pix_level;
  END IF;
  -- Validate passed in Auto Apply Variance option
  IF p_auto_apply_variance NOT IN (0, 1, 2) THEN
    RAISE exp_invalid_variance;
  END IF;
  -- Set global variables
  g_pix_level      := p_default_pix_level;
  g_srlnbrpix_flag :=
  (
    CASE
    WHEN p_include_srl_nbr_pix = 'Y' AND get_srl_track_flag () THEN
      TRUE
    ELSE
      FALSE
    END );
  g_transinvn_flag :=
  (
    CASE
    WHEN p_trans_invn_type = 'Y' THEN
      TRUE
    ELSE
      FALSE
    END);
  g_zero_invn_flag :=
  (
    CASE
    WHEN p_create_zero_inventory = 'Y' THEN
      TRUE
    ELSE
      FALSE
    END );
  g_group_skuattr_flag :=
  (
    CASE
    WHEN p_group_sku_attributes = 'Y' THEN
      TRUE
    ELSE
      FALSE
    END );
  g_unalloc_invn_flag :=
  (
    CASE
    WHEN p_include_unalloc_invn = 'Y' THEN
      TRUE
    ELSE
      FALSE
    END );
  g_consprtydate_flag :=
  (
    CASE
    WHEN p_include_cons_prty_date = 'Y' THEN
      TRUE
    ELSE
      FALSE
    END );
  g_caselock_flag :=
  (
    CASE
    WHEN p_case_lock_code = 'Y' THEN
      TRUE
    ELSE
      FALSE
    END);
  g_reconcile_flag :=
  (
    CASE
    WHEN p_reconcile = 'Y' THEN
      TRUE
    ELSE
      FALSE
    END);
  g_CasePick_Locn_Lock_flag :=
  (
    CASE
    WHEN p_CasePick_summ_by_Lock = 'Y' THEN
      TRUE
    ELSE
      FALSE
    END);
  g_Active_Locn_Lock_flag :=
  (
    CASE
    WHEN p_active_summ_by_lock = 'Y' THEN
      TRUE
    ELSE
      FALSE
    END);
  g_apply_variance_flag := p_auto_apply_variance;
  -- If Apply variances is set to 2 then...
  -- Only apply the variances and exit out of the procedure
  IF g_apply_variance_flag = 2 THEN
    log('*** Applying variances ***', TRUE);
    apply_variances_ui (p_season, p_season_yr, p_style, p_style_sfx, p_color, p_color_sfx, p_sec_dim, p_qual, p_size_desc, p_invn_type, p_prod_stat, p_item_name );
    -- Check for PIX_SUMM records for stat_code in 0, 20, 30 and the SKU_INVN_VAR if not found update MANH_BRIDGE_SUMMARY.ERR_REC_CNT = 0
    update_manh_bridge_summary;
    p_returncode := 0;
    RETURN;
  END IF;
  -- Check to see if the user has processed the previous run information.
  -- If not we will stop and log a message in the the MSG_log table.
  IF is_unprocessed_data_present AND g_reconcile_flag THEN
    log('Another Reconciliation process is in Progress.  Variances must be applied before running this process', TRUE);
    RAISE exp_previous_run;
  END IF;
  -- Get the Tran_Nbr value for this run of Pix
  SELECT PIX_TRAN_ID_SEQ.NEXTVAL
  INTO g_tran_nbr
  FROM DUAL;
  g_count := 0;
  log('Transaction Number for PIX in this run: ' || g_Tran_Nbr, TRUE);
  set_erp_host_type; -- Set the ERP_HOST_TYPE
  create_snapshots (p_season, p_season_yr, p_style, p_style_sfx, p_color, p_color_sfx, p_sec_dim, p_qual, p_size_desc, p_invn_type, p_prod_stat, p_item_name );
  -- check the unprocessed 605 Pix flag...if set to 'Y' then update the unprocessed PIXs to 90 in PIX_TRAN table
  IF p_update_unproc_605_pix = 'Y' THEN
    update_unprocessed_605_pix;
  END IF;
  generate_608_pix('B');
  -- Check the Default_Pix_Level flag...if set to 'S' then run the summary level pix generation else if 'D' then detail level
  IF p_default_pix_level = 'S' THEN
    generate_summary_pix;
  ELSE
    generate_detail_pix;
  END IF;
  -- update configurable ref fields
  update_pix_ref_fields;
  generate_608_pix('E');
  -- If Reconcillation flag is set then generate PIX_SUMM, SKU_INVN_VAR records and to apply variances if flag = 1
  IF g_reconcile_flag THEN
    generate_pix_summ
    (
      CASE
      WHEN g_apply_variance_flag = 1 THEN
        TRUE
      ELSE
        FALSE
      END);
  END IF;
  -- Check for PIX_SUMM records for stat_code in 0, 20, 30 and the SKU_INVN_VAR if not found update MANH_BRIDGE_SUMMARY.ERR_REC_CNT = 0
  update_manh_bridge_summary;
  COMMIT;
EXCEPTION
WHEN exp_user_info_notfound THEN
  ROLLBACK;
  p_returncode := 1;
  v_msg        := 'Warehouse/Company/Division combination for the passed in ----Login_User_Id is not found in WHSE_MASTER';
  exception_msg_log_insert (v_msg);
WHEN exp_invalid_pix_level THEN
  ROLLBACK;
  p_returncode := 1;
  v_msg        := 'Invalid PIX Level.  Valid values are D-Detail, S-Summary';
  exception_msg_log_insert (v_msg);
WHEN exp_invalid_variance THEN
  ROLLBACK;
  p_returncode := 1;
  v_msg        := 'Invalid value for Auto_Apply_Variance argument.  Value values are 0/1/2';
  exception_msg_log_insert (v_msg);
WHEN exp_previous_run THEN
  ROLLBACK;
  p_returncode := 1;
  v_msg        := get_msg_info('INVMGMT', '1628','ENG');
  exception_msg_log_insert (v_msg, 'INVMGMT', '1628');
WHEN OTHERS THEN
  ROLLBACK;
  update_manh_bridge_summary;
  COMMIT;
  p_returncode := 1;
  log('************ Manh_Gen_Sku_Invn_Pix Failed ************');
  v_msg := '(Generate_Pix) ' || SQLERRM;
  exception_msg_log_insert (v_msg);
  log('(Generate_Pix) ' || SQLERRM);
  wm_utils.set_context_info(p_module_name => v_prev_module_name, p_action_name => v_prev_action_name);
END generate_pix;
END manh_gen_sku_invn_pix;
/
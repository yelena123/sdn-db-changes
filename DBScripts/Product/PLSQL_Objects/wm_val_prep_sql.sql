create or replace trigger wm_val_prep_sql
before insert or update on val_sql
referencing new as new
for each row
declare
    v_job_dependency  number(1);
    v_bind_var_list   val_sql.bind_var_list%type;
    v_bind_var        val_sql_binds.bind_var%type;
    v_bind_delim      varchar2(2) := 'bv';
    v_bind_pattern    varchar2(20) := ':[a-z]+\w*';
begin
    if (:new.format_sql = 0)
    then
        :new.sql_text := :new.raw_text;
    else
        :new.sql_text := regexp_replace(replace(replace(replace(trim(:new.raw_text), chr(10), ' '), chr(9), ' '), chr(13), ' '), '( ){2,}', ' ');
        if (instr(:new.sql_text, '''', 1, 1) = 0 and instr(:new.sql_text, '"', 1, 1) = 0)
        then
            :new.sql_text := lower(:new.sql_text);
        end if;
    end if;

    for i in 1..regexp_count(:new.sql_text, v_bind_pattern, 1, 'i')
    loop
        v_bind_var := lower(substr(regexp_substr(:new.sql_text, v_bind_pattern, 1, i, 'i'), 2));
        exit when v_bind_var is null;
        v_bind_var := v_bind_var || ',';
        if (v_bind_var_list is null or instr(v_bind_var_list, v_bind_var, 1, 1) = 0)
        then
            v_bind_var_list := v_bind_var_list || v_bind_var;
        end if;
    end loop;
    v_bind_var_list := lower(v_bind_var_list);

    if (updating and coalesce(v_bind_var_list, ' ') != coalesce(:old.bind_var_list, ' '))
    then
        select case when exists
        (
            select 1
            from val_job_dtl vjd
            where vjd.sql_id = :new.sql_id
        ) then 1 else 0 end
        into v_job_dependency
        from dual;
        
        if (v_job_dependency = 1)
        then
            raise_application_error(-20050, 'Cannot modify bind vars. Purge dependent jobs first.');
        end if;
    end if;
    :new.bind_var_list := v_bind_var_list;

    :new.text_len := length(replace(:new.sql_text, ' '));
end;
/
show errors;

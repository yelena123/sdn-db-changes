create or replace procedure manh_wave_check_orders_capcty
(
    p_user_id                   in ucl_user.user_name%type,
    p_pull_all_swc              in wave_parm.pull_all_swc%type,
    p_reject_distro_rule        in wave_parm.reject_distro_rule%type,
    p_remaining_orders_capcty   in wave_parm.max_orders%type
)
as
    v_rsn_code varchar2(2) default '11';
begin
    -- exclude lines that were a part of selection due to swc; collect soft
    -- alloc lines with orders that exceeded the remaining cap; aside from 
    -- these specific lines, collect a) swcs b) items c) orders
    delete from tmp_wave_rejected_lines;
    insert into tmp_wave_rejected_lines
    (
        order_id, line_item_id, item_id, ship_group_id
    )
    select t3.order_id, t3.line_item_id,
        case when p_reject_distro_rule = '2' then t3.item_id 
            else null end item_id,
        case when p_pull_all_swc = 'Y' then t3.ship_group_id
            else null end ship_group_id
    from
    (
        select row_number() over(order by iv.order_group_num) order_index,
            iv.order_id
        from
        (    
            select min(t1.id) order_group_num, t1.order_id
            from tmp_wave_selected_orders t1
            where t1.is_swc_pull = 0
                and exists
                (
                    select 1
                    from tmp_ord_dtl_sku_invn t2
                    where t2.order_id = t1.order_id
                        and t2.line_item_id = t1.line_item_id
                )
            group by t1.order_id
        ) iv
    ) iv2
    join tmp_wave_selected_orders t3 on t3.order_id = iv2.order_id
    where iv2.order_index > p_remaining_orders_capcty and t3.is_swc_pull = 0
        and exists
        (
            select 1
            from tmp_ord_dtl_sku_invn t2
            where t2.order_id = t3.order_id
                and t2.line_item_id = t3.line_item_id
        );

    if (sql%rowcount = 0)
    then
        return;
    end if;
    wm_cs_log('Rejected lines captured for num orders check ' || sql%rowcount, p_sql_log_level => 1);

    manh_wave_capcty_rejections(p_user_id, p_pull_all_swc, p_reject_distro_rule,
        v_rsn_code);    
end;
/

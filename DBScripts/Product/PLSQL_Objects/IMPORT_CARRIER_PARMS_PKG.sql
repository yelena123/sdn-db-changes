
CREATE OR REPLACE 
PACKAGE IMPORT_CARRIER_PARMS_PKG
AS
   /*---------------------------------------------------------------------------------------
    #
    #   DATE               AUTHOR         DESCRIPTION
    #  ----------         ---------      ---------------------------------------------------
    #  05/31/2002         S. Lal         Initial development for UC 1104B - Auto Import
    #                                    Carrier Parms
    #
    #  04/07/2003         P Pathak      Added Payee_carrier_code in PROCEDURE imp_accessorial_rate,
    #          FUNCTION insert_accessorial_rate.
    #  09/02/2003         Karthik       Procedure insert_cm_rate_di1 added to support insert_cm_rate_discount function (DB2 Migration - Karthik)
    #  09/10/2003  MB   MINIMUM_SIZE,MAXIMUM_SIZE,SIZE_UOM support added to IMP_ACCESSORIAL_RATE (Deepak-Accessorial)
    #  09/10/2003  MB   MINIMUM_SIZE,MAXIMUM_SIZE,SIZE_UOM support added to INSERT_ACCESSORIAL_RATE (Deepak-Accessorial)
    # ---------------------------------------------------------------------------------------- */

   TYPE ref_curtype IS REF CURSOR;

   PROCEDURE imp_accessorial_param_set (
      pParamId               IN import_accessorial_param_set.param_id%TYPE,
      pTCCompanyId           IN import_accessorial_param_set.tc_company_id%TYPE,
      pCarrierCode           IN import_accessorial_param_set.carrier_code%TYPE,
      pBusinessUnit          IN import_accessorial_param_set.business_unit%TYPE,
      pUseFakCommodity       IN import_accessorial_param_set.use_fak_commodity%TYPE,
      pCommodityClass        IN import_accessorial_param_set.commodity_class%TYPE,
      pEquipmentCode         IN import_accessorial_param_set.equipment_code%TYPE,
      pEffectiveDt           IN import_accessorial_param_set.effective_dt%TYPE,
      pExpirationDt          IN import_accessorial_param_set.expiration_dt%TYPE,
      pCmRateType            IN import_accessorial_param_set.cm_rate_type%TYPE,
      pCmRateCurrecyCode     IN import_accessorial_param_set.cm_rate_currency_code%TYPE,
      pStopOffCurrencyCode   IN import_accessorial_param_set.stop_off_currency_code%TYPE,
      pDistanceUom           IN import_accessorial_param_set.distance_uom%TYPE,
      pBudgetedApproved      IN import_accessorial_param_set.budgeted_approved%TYPE,
      pSizeUom               IN import_accessorial_param_set.size_uom%TYPE,
      pIsGlobal              IN import_accessorial_param_set.is_global%TYPE,
      pStatus                IN import_accessorial_param_set.status%TYPE,
      pCarrierId             IN import_accessorial_param_set.carrier_id%TYPE,
      pEquipmentId           IN import_accessorial_param_set.equipment_id%TYPE,
      pSizeUomId             IN import_accessorial_param_set.size_uom_id%TYPE);

   PROCEDURE imp_cm_rate_discount (
      pParamId       IN import_cm_rate_discount.param_id%TYPE,
      pStartNumber   IN import_cm_rate_discount.start_number%TYPE,
      pEndNumber     IN import_cm_rate_discount.end_number%TYPE,
      pValue         IN import_cm_rate_discount.VALUE%TYPE,
      pStatus        IN import_cm_rate_discount.status%TYPE);

   PROCEDURE imp_stop_off_charge (
      pParamId       IN import_stop_off_charge.param_id%TYPE,
      pStartNumber   IN import_stop_off_charge.start_number%TYPE,
      pEndNumber     IN import_stop_off_charge.end_number%TYPE,
      pValue         IN import_stop_off_charge.VALUE%TYPE,
      pStatus        IN import_stop_off_charge.status%TYPE);

   PROCEDURE imp_accessorial_rate (
      pAccRateId                IN import_accessorial_rate.accessorial_rate_id%TYPE,
      pParamId                  IN import_accessorial_rate.param_id%TYPE,
      pTCCompanyId              IN import_accessorial_rate.accessorial_rate_id%TYPE,
      pMot                      IN import_accessorial_rate.mot%TYPE,
      pEquipmentCode            IN import_accessorial_rate.equipment_code%TYPE,
      pServiceLevel             IN import_accessorial_rate.service_level%TYPE,
      pProtectionLevel          IN import_accessorial_rate.protection_level%TYPE,
      pAccessorialCode          IN import_accessorial_rate.accessorial_code%TYPE,
      pRate                     IN import_accessorial_rate.rate%TYPE,
      pMinimumRate              IN import_accessorial_rate.minimum_rate%TYPE,
      pCurrencyCode             IN import_accessorial_rate.currency_code%TYPE,
      p_minimum_size            IN IMPORT_ACCESSORIAL_RATE.MINIMUM_SIZE%TYPE,
      p_maximum_size            IN IMPORT_ACCESSORIAL_RATE.MAXIMUM_SIZE%TYPE,
      p_size_uom                IN IMPORT_ACCESSORIAL_RATE.SIZE_UOM%TYPE,
      pIsAutoApprove            IN import_accessorial_rate.is_auto_approve%TYPE,
      pEffectiveDt              IN import_accessorial_rate.effective_dt%TYPE,
      pExpirationDt             IN import_accessorial_rate.expiration_dt%TYPE,
      pStatus                   IN import_accessorial_rate.status%TYPE,
      pPayeeCarrierCode         IN import_accessorial_rate.payee_carrier_code%TYPE,
      pMotId                    IN import_accessorial_rate.mot_id%TYPE,
      pEquipmentId              IN import_accessorial_rate.equipment_id%TYPE,
      pServiceLevelId           IN import_accessorial_rate.service_level_id%TYPE,
      pProtectionLevelId        IN import_accessorial_rate.protection_level_id%TYPE,
      pPayeeCarrierId           IN import_accessorial_rate.payee_carrier_id%TYPE,
      p_size_uom_id             IN import_accessorial_rate.size_uom_id%TYPE,
      pAccessorialId            IN import_accessorial_rate.accessorial_id%TYPE,
      pLongestDimension         IN import_accessorial_rate.longest_dimension%TYPE,
      pSecondLongestDimension   IN import_accessorial_rate.second_longest_dimension%TYPE,
      pLengthPlusGrith          IN import_accessorial_rate.Length_Plus_Grith%TYPE,
      weight                    IN import_accessorial_rate.Weight%TYPE,
      pBaseAmount               IN import_accessorial_rate.Base_Amount%TYPE,
      pChargePerXfield1         IN import_accessorial_rate.Charge_Per_x_Field1%TYPE,
      pChargePerXfield2         IN import_accessorial_rate.Charge_Per_x_Field2%TYPE,
      pLowerRange               IN import_accessorial_rate.Lower_Range%TYPE,
      pHigherRange              IN import_accessorial_rate.higher_range%TYPE,
      pMaxLongestDim            IN import_accessorial_rate.MAX_LONGEST_DIM%TYPE,
      pMinLongestDim            IN import_accessorial_rate.MIN_LONGEST_DIM%TYPE,
      pMaxSecLongestDim         IN import_accessorial_rate.MAX_SEC_LONGEST_DIM%TYPE,
      pMinSecLongestDim         IN import_accessorial_rate.MIN_SEC_LONGEST_DIM%TYPE,
      pMaxTrdLongestDim         IN import_accessorial_rate.MAX_TRD_LONGEST_DIM%TYPE,
      pMinTrdLongestDim         IN import_accessorial_rate.MIN_TRD_LONGEST_DIM%TYPE,
      pMaxLengthGirth           IN import_accessorial_rate.MAX_LENGTH_GIRTH%TYPE,
      pMinLengthGirth           IN import_accessorial_rate.MIN_LENGTH_GIRTH%TYPE,
      pMaxWeight                IN import_accessorial_rate.MAX_WEIGHT%TYPE,
      pMinWeight                IN import_accessorial_rate.MIN_WEIGHT%TYPE,
      pBillingMethod            IN import_accessorial_rate.BILLING_METHOD%TYPE,
      pIsZoneOrigin             IN import_accessorial_rate.IS_ZONE_ORIGIN%TYPE,
      pIsZoneDest               IN import_accessorial_rate.IS_ZONE_DEST%TYPE,
      pIsZoneStopoff            IN import_accessorial_rate.IS_ZONE_STOPOFF%TYPE,
      pMaximumRate              IN import_accessorial_rate.MAXIMUM_RATE%TYPE,
      pCustomerRate             IN import_accessorial_rate.CUSTOMER_RATE%TYPE,
      pCustomerMinCharge        IN import_accessorial_rate.CUSTOMER_MIN_CHARGE%TYPE,
      pCalculatedRate           IN import_accessorial_rate.CALCULATED_RATE%TYPE,
      pBaseCharge               IN import_accessorial_rate.BASE_CHARGE%TYPE,
      pAmount                   IN import_accessorial_rate.AMOUNT%TYPE,
      pIncrementVal             IN import_accessorial_rate.INCREMENT_VAL%TYPE,
      pZoneId                   IN import_accessorial_rate.ZONE_ID%TYPE,
      pzoneName                 IN import_accessorial_rate.ZONE_NAME%TYPE,
	  vCommID				    IN import_accessorial_rate.COMMODITY_CODE_ID%TYPE, -- CMGT-615 Starts here
	  vMaxCommId			    IN import_accessorial_rate.MAX_RANGE_COMMODITY_CODE_ID%TYPE -- CMGT-615 Ends here
	  );
   -- deprecated!
   
   -----------------------Added here for INfeasibility between SL and AOPTG------------------------------------------------------
    FUNCTION VAL_SL_FEASBLE_ACCOPTGROUP
(
   vTCCompanyId			IN import_accessorial_rate.TC_COMPANY_ID%TYPE,
   vAccId   IN    import_accessorial_rate.accessorial_id%TYPE,
   vServLevelId IN import_accessorial_rate.service_level_id%TYPE
)
  RETURN NUMBER;
  -----------------------------ended here---------------------------------------------------------------

   
   FUNCTION validate_carrier_data (
      pTCCompanyId         IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId             IN import_accessorial_param_set.param_id%TYPE,
      pCarrierCode         IN import_accessorial_param_set.carrier_code%TYPE,
      pEquipment           IN import_accessorial_param_set.equipment_code%TYPE,
      pBusinessUnit        IN import_accessorial_param_set.business_unit%TYPE,
      pBudgetedApproved    IN import_accessorial_param_set.budgeted_approved%TYPE,
      pIsGlobal            IN import_accessorial_param_set.is_global%TYPE,
      pCmRateType          IN import_accessorial_param_set.cm_rate_type%TYPE,
      pCmRateCurrecyCode   IN import_accessorial_param_set.cm_rate_currency_code%TYPE,
      pDistanceUom         IN import_accessorial_param_set.distance_uom%TYPE)
      RETURN NUMBER;

   -- deprecated!
   PROCEDURE validate_carrier_parms_old (
      pTCCompanyId IN COMPANY.COMPANY_ID%TYPE);

   PROCEDURE validate_carrier_parms (pTCCompanyId IN COMPANY.COMPANY_ID%TYPE);

   FUNCTION validate_cm_rate_type (
      pTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
      pParamId       IN import_accessorial_param_set.param_id%TYPE,
      pCmRateType    IN import_accessorial_param_set.cm_rate_type%TYPE,
      pCarrierCode   IN import_accessorial_param_set.carrier_code%TYPE)
      RETURN NUMBER;

   FUNCTION validate_distance_uom (
      pTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
      pParamId       IN import_accessorial_param_set.param_id%TYPE,
      pCmRateType    IN import_accessorial_param_set.cm_rate_type%TYPE,
      pDistanceUom   IN import_accessorial_param_set.distance_uom%TYPE)
      RETURN NUMBER;

   -- deprecated !
   FUNCTION validate_cm (
      pTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      pParamId               IN import_accessorial_param_set.param_id%TYPE,
      pCmRateType            IN import_accessorial_param_set.cm_rate_type%TYPE,
      pCmRateCurrencyCode    IN import_accessorial_param_set.cm_rate_currency_code%TYPE,
      pStopOffCurrencyCode   IN import_accessorial_param_set.stop_off_currency_code%TYPE)
      RETURN NUMBER;

   PROCEDURE validate_cm_discount (
      pTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
      pParamId       IN import_accessorial_param_set.param_id%TYPE,
      pCmRateType    IN import_accessorial_param_set.cm_rate_type%TYPE);

   FUNCTION does_cm_rate_discount_overlap (
      pParamId            IN import_accessorial_param_set.param_id%TYPE,
      pCmRateDiscountId   IN import_cm_rate_discount.cm_rate_discount_id%TYPE,
      pStartNumber        IN import_cm_rate_discount.start_number%TYPE,
      pEndNumber          IN import_cm_rate_discount.end_number%TYPE,
      pValue              IN import_cm_rate_discount.VALUE%TYPE)
      RETURN NUMBER;

   PROCEDURE validate_stop_off (
      pTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
      pParamId       IN import_accessorial_param_set.param_id%TYPE);

   FUNCTION does_stop_off_overlap (
      pParamId       IN import_accessorial_param_set.param_id%TYPE,
      pStopOffId     IN import_stop_off_charge.stop_off_charge_id%TYPE,
      pStartNumber   IN import_stop_off_charge.start_number%TYPE,
      pEndNumber     IN import_stop_off_charge.end_number%TYPE,
      pCharge        IN import_stop_off_charge.VALUE%TYPE)
      RETURN NUMBER;

   PROCEDURE validate_accessorials (
      pTCCompanyId   IN import_accessorial_rate.TC_COMPANY_ID%TYPE,
      pParamId       IN import_accessorial_rate.param_id%TYPE);

   PROCEDURE update_carrier_param_status (
      pTcCompanyId   IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId       IN import_accessorial_param_set.param_id%TYPE);

   FUNCTION validate_carrier_param_details (
      pTcCompanyId           IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId               IN import_accessorial_param_set.param_id%TYPE,
      pCarrierCode           IN import_accessorial_param_set.carrier_code%TYPE,
      pEquipment             IN import_accessorial_param_set.equipment_code%TYPE,
      pBusinessUnit          IN import_accessorial_param_set.business_unit%TYPE,
      pBudgetedApproved      IN import_accessorial_param_set.budgeted_approved%TYPE,
      pIsGlobal              IN import_accessorial_param_set.is_global%TYPE,
      pCmRateType            IN import_accessorial_param_set.cm_rate_type%TYPE,
      pCmRateCurrencyCode    IN import_accessorial_param_set.cm_rate_currency_code%TYPE,
      pDistanceUom           IN import_accessorial_param_set.distance_uom%TYPE,
      pStopOffCurrencyCode   IN import_accessorial_param_set.stop_off_currency_code%TYPE,
      pUseFAK                IN import_accessorial_param_set.use_fak_commodity%TYPE,
      pCommodityClass        IN import_accessorial_param_set.commodity_class%TYPE,
      pEffectiveDt           IN import_accessorial_param_set.effective_dt%TYPE,
      pExpirationDt          IN import_accessorial_param_set.expiration_dt%TYPE,
      pWeightUOM             IN import_accessorial_param_set.size_uom%TYPE,
      pCarrierId             IN import_accessorial_param_set.carrier_id%TYPE,
      pEquipmentId           IN import_accessorial_param_set.equipment_id%TYPE)
      RETURN NUMBER;

   FUNCTION validate_carrier (
      pTcCompanyId        IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId            IN import_accessorial_param_set.param_id%TYPE,
      pCarrierCode        IN import_accessorial_param_set.carrier_code%TYPE,
      pBudgetedApproved   IN import_accessorial_param_set.budgeted_approved%TYPE,
      pIsGlobal           IN import_accessorial_param_set.is_global%TYPE,
      pCarrierId          IN import_accessorial_param_set.carrier_id%TYPE)
      RETURN NUMBER;

   FUNCTION validate_bu (
      pTcCompanyId    IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId        IN import_accessorial_param_set.param_id%TYPE,
      pBusinessUnit   IN import_accessorial_param_set.business_unit%TYPE)
      RETURN NUMBER;

   FUNCTION validate_equipment (
      pTcCompanyId   IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId       IN import_accessorial_param_set.param_id%TYPE,
      pCarrierCode   IN import_accessorial_param_set.carrier_code%TYPE,
      pEquipment     IN import_accessorial_param_set.equipment_code%TYPE,
      pEquipmentId   IN import_accessorial_param_set.equipment_id%TYPE,
      pCarrierId     IN import_accessorial_param_set.carrier_id%TYPE)
      RETURN NUMBER;

   FUNCTION validate_dates (
      pTcCompanyId    IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId        IN import_accessorial_param_set.param_id%TYPE,
      pEffectiveDt    IN import_accessorial_param_set.effective_dt%TYPE,
      pExpirationDt   IN import_accessorial_param_set.expiration_dt%TYPE)
      RETURN NUMBER;

   FUNCTION validate_use_FAK (
      pTcCompanyId      IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId          IN import_accessorial_param_set.param_id%TYPE,
      pUseFAK           IN import_accessorial_param_set.use_fak_commodity%TYPE,
      pCommodityClass   IN import_accessorial_param_set.commodity_class%TYPE)
      RETURN NUMBER;

   FUNCTION validate_weight_UOM (
      pTcCompanyId   IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId       IN import_accessorial_param_set.param_id%TYPE,
      pWeightUOM     IN import_accessorial_param_set.size_uom%TYPE)
      RETURN NUMBER;

   FUNCTION validate_currency_code (
      pTcCompanyId    IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId        IN import_accessorial_param_set.param_id%TYPE,
      pCurrencyCode   IN import_accessorial_param_set.stop_off_currency_code%TYPE,
      pCurrencyType   IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION validate_stop_off_currency (
      pTcCompanyId           IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId               IN import_accessorial_param_set.param_id%TYPE,
      pStopOffCurrencyCode   IN import_accessorial_param_set.stop_off_currency_code%TYPE)
      RETURN NUMBER;

   FUNCTION validate_CM_data (
      pTcCompanyId          IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId              IN import_accessorial_param_set.param_id%TYPE,
      pCarrierCode          IN import_accessorial_param_set.carrier_code%TYPE,
      pCmRateType           IN import_accessorial_param_set.cm_rate_type%TYPE,
      pCmRateCurrencyCode   IN import_accessorial_param_set.cm_rate_currency_code%TYPE,
      pDistanceUom          IN import_accessorial_param_set.distance_uom%TYPE)
      RETURN NUMBER;

   PROCEDURE transfer_carrier_parms (pTCCompanyId IN COMPANY.COMPANY_ID%TYPE);

   PROCEDURE transfer_cm_rate_discount (
      pImportedParamId   IN import_cm_rate_discount.param_id%TYPE,
      pExistingParamId   IN import_cm_rate_discount.param_id%TYPE,
      pCarrierCode       IN accessorial_param_set.carrier_id%TYPE);

   PROCEDURE transfer_stop_off_charge (
      pImportedParamId   IN import_cm_rate_discount.param_id%TYPE,
      pExistingParamId   IN import_cm_rate_discount.param_id%TYPE);

   PROCEDURE transfer_accessorial_rate (
      pImportedParamId   IN import_cm_rate_discount.param_id%TYPE,
      pExistingParamId   IN import_cm_rate_discount.param_id%TYPE,
      pTCCompanyId       IN import_accessorial_param_set.tc_company_id%TYPE);

   FUNCTION insert_accessorial_param_set (
      pParamId                IN import_accessorial_param_set.param_id%TYPE,
      pTCCompanyId            IN import_accessorial_param_set.tc_company_id%TYPE,
      pCarrierCode            IN import_accessorial_param_set.carrier_id%TYPE,
      /*  pBusinessUnit             IN import_accessorial_param_set.business_unit%TYPE, */
      pUseFakCommodity        IN import_accessorial_param_set.use_fak_commodity%TYPE,
      pCommodityClass         IN import_accessorial_param_set.commodity_class%TYPE,
      pEquipmentCode          IN import_accessorial_param_set.equipment_id%TYPE,
      pEffectiveDt            IN import_accessorial_param_set.effective_dt%TYPE,
      pExpirationDt           IN import_accessorial_param_set.expiration_dt%TYPE,
      pCmRateType             IN import_accessorial_param_set.cm_rate_type%TYPE,
      pCmRateCurrecyCode      IN import_accessorial_param_set.cm_rate_currency_code%TYPE,
      pStopOffCurrency_code   IN import_accessorial_param_set.stop_off_currency_code%TYPE,
      pDistanceUom            IN import_accessorial_param_set.distance_uom%TYPE,
      pBudgetedApproved       IN import_accessorial_param_set.budgeted_approved%TYPE,
      pSizeUom                IN import_accessorial_param_set.size_uom_id%TYPE,
      pIsGlobal               IN import_accessorial_param_set.is_global%TYPE)
      RETURN NUMBER;

   FUNCTION find_aps_exact_match (
      pParamId               IN import_accessorial_param_set.param_id%TYPE,
      pTCCompanyId           IN import_accessorial_param_set.tc_company_id%TYPE,
      pCarrierCode           IN import_accessorial_param_set.carrier_id%TYPE,
      /* pBusinessUnit          IN import_accessorial_param_set.business_unit%TYPE, */
      pUseFakCommodity       IN import_accessorial_param_set.use_fak_commodity%TYPE,
      pCommodityClass        IN import_accessorial_param_set.commodity_class%TYPE,
      pEquipmentCode         IN import_accessorial_param_set.equipment_id%TYPE,
      pEffectiveDt           IN import_accessorial_param_set.effective_dt%TYPE,
      pExpirationDt          IN import_accessorial_param_set.expiration_dt%TYPE,
      pCmRateType            IN import_accessorial_param_set.cm_rate_type%TYPE,
      pCmRateCurrencyCode    IN import_accessorial_param_set.cm_rate_currency_code%TYPE,
      pStopOffCurrencyCode   IN import_accessorial_param_set.stop_off_currency_code%TYPE,
      pDistanceUom           IN import_accessorial_param_set.distance_uom%TYPE,
      pBudgetedApproved      IN import_accessorial_param_set.budgeted_approved%TYPE,
      pSizeUom               IN import_accessorial_param_set.size_uom_id%TYPE,
      pIsGlobal              IN import_accessorial_param_set.is_global%TYPE)
      -- returns the id of the exact match or NULL if none found
      RETURN accessorial_param_set.accessorial_param_set_id%TYPE;

   PROCEDURE update_existing_aps (
      pExistingParamId               IN import_accessorial_param_set.param_id%TYPE,
      pImportedParamId               IN import_accessorial_param_set.param_id%TYPE,
      pTCCompanyId                   IN import_accessorial_param_set.tc_company_id%TYPE,
      pImportedCarrierCode           IN import_accessorial_param_set.carrier_id%TYPE,
      /*pImportedBusinessUnit          IN import_accessorial_param_set.business_unit%TYPE,*/
      pImportedUseFakCommodity       IN import_accessorial_param_set.use_fak_commodity%TYPE,
      pImportedCommodityClass        IN import_accessorial_param_set.commodity_class%TYPE,
      pImportedEquipmentCode         IN import_accessorial_param_set.equipment_id%TYPE,
      pImportedEffectiveDt           IN import_accessorial_param_set.effective_dt%TYPE,
      pImportedExpirationDt          IN import_accessorial_param_set.expiration_dt%TYPE,
      pImportedCmRateType            IN import_accessorial_param_set.cm_rate_type%TYPE,
      pImportedCmRateCurrencyCode    IN import_accessorial_param_set.cm_rate_currency_code%TYPE,
      pImportedStopOffCurrencyCode   IN import_accessorial_param_set.stop_off_currency_code%TYPE,
      pImportedDistanceUom           IN import_accessorial_param_set.distance_uom%TYPE,
      pImportedBudgetedApproved      IN import_accessorial_param_set.budgeted_approved%TYPE,
      pImportedSizeUom               IN import_accessorial_param_set.size_uom_id%TYPE,
      pImportedIsGlobal              IN import_accessorial_param_set.is_global%TYPE);

   FUNCTION insert_stop_off_charge (
      pAccessorialParamSetId   IN stop_off_charge.accessorial_param_set_id%TYPE,
      pStopOffChargeId         IN stop_off_charge.stop_off_charge_id%TYPE,
      pStartNumber             IN stop_off_charge.start_number%TYPE,
      pEndNumber               IN stop_off_charge.end_number%TYPE,
      pValue                   IN stop_off_charge.VALUE%TYPE)
      RETURN NUMBER;

   FUNCTION insert_cm_rate_discount (
      pAccessorialParamSetId   IN cm_rate_discount.accessorial_param_set_id%TYPE,
      pCmRateDiscountId        IN cm_rate_discount.cm_rate_discount_id%TYPE,
      pStartNumber             IN cm_rate_discount.start_number%TYPE,
      pEndNumber               IN cm_rate_discount.end_number%TYPE,
      pValue                   IN cm_rate_discount.VALUE%TYPE)
      RETURN NUMBER;

   PROCEDURE insert_cm_rate_di1 (
      pAccessorialParamSetId   IN     cm_rate_discount.accessorial_param_set_id%TYPE,
      pCmRateDiscountId        IN     cm_rate_discount.cm_rate_discount_id%TYPE,
      pStartNumber             IN     cm_rate_discount.start_number%TYPE,
      pEndNumber               IN     cm_rate_discount.end_number%TYPE,
      pValue                   IN     cm_rate_discount.VALUE%TYPE,
      return_val                  OUT NUMBER);

   FUNCTION insert_accessorial_rate (
      pParamId               IN import_accessorial_rate.param_id%TYPE,
      pAccessorialRateId     IN import_accessorial_rate.accessorial_rate_id%TYPE,
      pTCCompanyId           IN import_accessorial_rate.tc_company_id%TYPE,
      pMot                   IN import_accessorial_rate.mot_id%TYPE,
      pEquipmentCode         IN import_accessorial_rate.equipment_id%TYPE,
      pServiceLevel          IN import_accessorial_rate.service_level_id%TYPE,
      pProtectionLevel       IN import_accessorial_rate.protection_level_id%TYPE,
      pAccessorialCode       IN import_accessorial_rate.accessorial_id%TYPE,
      pRate                  IN import_accessorial_rate.rate%TYPE,
      pMinimumRate           IN import_accessorial_rate.minimum_rate%TYPE,
      pCurrencyCode          IN import_accessorial_rate.currency_code%TYPE,
      p_minimum_size         IN IMPORT_ACCESSORIAL_RATE.MINIMUM_SIZE%TYPE,
      p_maximum_size         IN IMPORT_ACCESSORIAL_RATE.MAXIMUM_SIZE%TYPE,
      p_size_uom             IN IMPORT_ACCESSORIAL_RATE.SIZE_UOM_ID%TYPE,
      pIsAutoApprove         IN import_accessorial_rate.is_auto_approve%TYPE,
      pEffectiveDt           IN import_accessorial_rate.effective_dt%TYPE,
      pExpirationDt          IN import_accessorial_rate.expiration_dt%TYPE,
      pPayeeCarrierCode      IN import_accessorial_rate.payee_carrier_id%TYPE,
      pLongestDimension      IN import_accessorial_rate.Longest_Dimension%TYPE,
      pSecLongestDimension   IN import_accessorial_rate.Second_Longest_Dimension%TYPE,
      pLengthplusgrith       IN import_accessorial_rate.Length_Plus_Grith%TYPE,
      pWeight                IN import_accessorial_rate.Weight%TYPE,
      pBaseAmount            IN import_accessorial_rate.Base_Amount%TYPE,
      pChargePerXField1      IN import_accessorial_rate.Charge_Per_x_Field1%TYPE,
      pChargePerXField2      IN import_accessorial_rate.Charge_Per_x_Field2%TYPE,
      pLowerRange            IN import_accessorial_rate.Lower_Range%TYPE,
      pHigherRange           IN import_accessorial_rate.Higher_Range%TYPE,
      pMaxLongestDim         IN import_accessorial_rate.MAX_LONGEST_DIM%TYPE,
      pMinLongestDim         IN import_accessorial_rate.MIN_LONGEST_DIM%TYPE,
      pMaxSecLongestDim      IN import_accessorial_rate.MAX_SEC_LONGEST_DIM%TYPE,
      pMinSecLongestDim      IN import_accessorial_rate.MIN_SEC_LONGEST_DIM%TYPE,
      pMaxTrdLongestDim      IN import_accessorial_rate.MAX_TRD_LONGEST_DIM%TYPE,
      pMinTrdLongestDim      IN import_accessorial_rate.MIN_TRD_LONGEST_DIM%TYPE,
      pMaxLengthGirth        IN import_accessorial_rate.MAX_LENGTH_GIRTH%TYPE,
      pMinLengthGirth        IN import_accessorial_rate.MIN_LENGTH_GIRTH%TYPE,
      pMaxWeight             IN import_accessorial_rate.MAX_WEIGHT%TYPE,
      pMinWeight             IN import_accessorial_rate.MIN_WEIGHT%TYPE,
      pBillingMethod         IN import_accessorial_rate.BILLING_METHOD%TYPE,
      pIsZoneOrigin          IN import_accessorial_rate.IS_ZONE_ORIGIN%TYPE,
      pIsZoneDest            IN import_accessorial_rate.IS_ZONE_DEST%TYPE,
      pIsZoneStopoff         IN import_accessorial_rate.IS_ZONE_STOPOFF%TYPE,
      pMaximumRate           IN import_accessorial_rate.MAXIMUM_RATE%TYPE,
      pCustomerRate          IN import_accessorial_rate.CUSTOMER_RATE%TYPE,
      pCustomerMinCharge     IN import_accessorial_rate.CUSTOMER_MIN_CHARGE%TYPE,
      pCalculatedRate        IN import_accessorial_rate.CALCULATED_RATE%TYPE,
      pBaseCharge            IN import_accessorial_rate.BASE_CHARGE%TYPE,
      pAmount                IN import_accessorial_rate.AMOUNT%TYPE,
      pIncrementVal          IN import_accessorial_rate.INCREMENT_VAL%TYPE,
      pZoneId                IN import_accessorial_rate.ZONE_ID%TYPE,
	  vCommID				IN import_accessorial_rate.COMMODITY_CODE_ID%TYPE, -- CMGT-615 Starts here
	  vMaxCommId			IN import_accessorial_rate.MAX_RANGE_COMMODITY_CODE_ID%TYPE ) -- CMGT-615 Ends here
      RETURN NUMBER;

   PROCEDURE insert_carrier_parms_error (
      pTCCompanyId     IN accessorial_param_set_errors.tc_company_id%TYPE,
      pParamId         IN accessorial_param_set_errors.param_id%TYPE,
      pErrorMsgDesc    IN accessorial_param_set_errors.ERROR_MSG_DESC%TYPE,
      pSubObjectId     IN accessorial_param_set_errors.sub_object_id%TYPE,
      pSubObjectType   IN accessorial_param_set_errors.sub_object_type%TYPE);

   FUNCTION get_resource_where_clause (
      pCarrierCode     IN import_accessorial_param_set.carrier_id%TYPE,
      /* pBusinessUnit     IN import_accessorial_param_set.business_unit%TYPE, */
      pEquipmentCode   IN import_accessorial_param_set.equipment_id%TYPE)
      RETURN VARCHAR2;

   FUNCTION get_rsce_wh_clause_exact_match (
      pCarrierCode     IN import_accessorial_param_set.carrier_id%TYPE,
      /* pBusinessUnit     IN import_accessorial_param_set.business_unit%TYPE, */
      pEquipmentCode   IN import_accessorial_param_set.equipment_id%TYPE)
      RETURN VARCHAR2;

   FUNCTION get_acc_rate_where_clause (
      pMot               IN import_accessorial_rate.mot_id%TYPE,
      pEquipmentCode     IN import_accessorial_rate.equipment_id%TYPE,
      pServiceLevel      IN import_accessorial_rate.service_level_id%TYPE,
      pProtectionLevel   IN import_accessorial_rate.protection_level_id%TYPE,
      pAccessorialCode   IN import_accessorial_rate.accessorial_id%TYPE)
      RETURN VARCHAR2;

   PROCEDURE adjust_eff_exp_dt (
      pTCCompanyId             IN accessorial_param_set.tc_company_id%TYPE,
      pAccessorialParamSetId   IN accessorial_param_set.accessorial_param_set_id%TYPE,
      pCarrierCode             IN accessorial_param_set.carrier_id%TYPE,
      /*pBusinessUnit             IN business_unit.business_unit%TYPE, */
      pEquipmentCode           IN accessorial_param_set.equipment_id%TYPE,
      pEffectiveDt             IN accessorial_param_set.effective_dt%TYPE,
      pExpirationDt            IN accessorial_param_set.expiration_dt%TYPE,
      pBudgetedApproved        IN accessorial_param_set.budgeted_approved%TYPE,
      pIsGlobal                IN accessorial_param_set.is_global%TYPE);

   PROCEDURE adjust_acc_rate_eff_exp_dt (
      pExistingAccessorialParamSetId   IN accessorial_rate.accessorial_param_set_id%TYPE,
      --pAccessorialParamSetId IN accessorial_rate.accessorial_param_set_id%TYPE,
      --pAccessorialRateId     IN accessorial_rate.accessorial_rate_id%TYPE,
      pTCCompanyId                     IN accessorial_rate.tc_company_id%TYPE,
      pMot                             IN accessorial_rate.Mot_id%TYPE,
      pEquipmentCode                   IN accessorial_rate.equipment_id%TYPE,
      pServiceLevel                    IN accessorial_rate.service_level_id%TYPE,
      pProtectionLevel                 IN accessorial_rate.protection_level_id%TYPE,
      pAccessorialCode                 IN accessorial_rate.accessorial_id%TYPE,
      pEffectiveDt                     IN accessorial_rate.effective_dt%TYPE,
      pExpirationDt                    IN accessorial_rate.expiration_dt%TYPE);

   FUNCTION adjust_acc_rate_eff_exp_dt_1 (
      pExistingAccessorialParamSetId   IN accessorial_rate.accessorial_param_set_id%TYPE,
      pImportedParamId                 IN import_accessorial_rate.param_id%TYPE,
	  pImpAccRateId     			   IN accessorial_rate.accessorial_rate_id%TYPE,
      --pAccessorialParamSetId IN accessorial_rate.accessorial_param_set_id%TYPE,
      pTCCompanyId                     IN accessorial_rate.tc_company_id%TYPE,
      pMot                             IN accessorial_rate.Mot_id%TYPE,
      pEquipmentCode                   IN accessorial_rate.equipment_id%TYPE,
      pServiceLevel                    IN accessorial_rate.service_level_id%TYPE,
      pProtectionLevel                 IN accessorial_rate.protection_level_id%TYPE,
      pAccessorialCode                 IN accessorial_rate.accessorial_id%TYPE,
      pEffectiveDt                     IN accessorial_rate.effective_dt%TYPE,
      pExpirationDt                    IN accessorial_rate.expiration_dt%TYPE,
      vminsize                         IN accessorial_rate.minimum_size%TYPE,
      vmaxsize                         IN accessorial_rate.maximum_size%TYPE,
      vsizeuomid                       IN accessorial_rate.size_uom_id%TYPE,
	  vCommID                          IN accessorial_rate.COMMODITY_CODE_ID%TYPE,
      vMaxCommId                       IN accessorial_rate.MAX_RANGE_COMMODITY_CODE_ID%TYPE)
      RETURN INTEGER;

   PROCEDURE test_find_aps_exact_match (
      pParamId       IN import_accessorial_param_set.param_id%TYPE,
      pTCCompanyId   IN import_accessorial_param_set.tc_company_id%TYPE);

   PROCEDURE DUPLICATE_CARRIER_PARAMS (
      pTcCompnayId             IN accessorial_param_set.TC_COMPANY_ID%TYPE,
      pAccessorialParamSetId   IN accessorial_param_set.accessorial_param_set_id%TYPE,
      vNewParamSetId           IN accessorial_param_set.accessorial_param_set_id%TYPE,
      NewEffectiveDt           IN accessorial_param_set.effective_dt%TYPE,
      NewExpirationDt          IN accessorial_param_set.expiration_dt%TYPE);

   PROCEDURE DELETE_CARRIER_PARAMS (
      pTcCompnayId             IN accessorial_param_set.TC_COMPANY_ID%TYPE,
      pAccessorialParamSetId   IN accessorial_param_set.accessorial_param_set_id%TYPE);

   PROCEDURE TRANSFER_DAS_RS (PIMPORTEDPARAMID IN imp_DAS_RS.Param_Id%TYPE,
   PEXISTINGPARAMID IN imp_DAS_RS.Param_Id%TYPE);

   PROCEDURE VALIDATE_DAS_RS (
      PTCCOMPANYID   IN IMP_DAS_RS.TC_COMPANY_ID%TYPE,
      PPARAMID       IN IMP_DAS_RS.PARAM_ID%TYPE);

   PROCEDURE VALIDATE_EXCL_ACC (
      PTCCOMPANYID   IN COMPANY.COMPANY_ID%TYPE,
      PPARAMID       IN IMP_EXCLUDED_APS_ACC.PARAM_ID%TYPE,
      PACCRATEID     IN IMP_EXCLUDED_APS_ACC.ACCESSORIAL_RATE_ID%TYPE);

   PROCEDURE VALIDATE_APS_OVERLAPS (
      PCARRIERID      IN     accessorial_param_set.carrier_id%TYPE,
      PTCCOMPANYID    IN     accessorial_param_set.tc_company_id%TYPE,
      PEQUIPMENTID    IN     accessorial_param_set.equipment_id%TYPE,
      PEFFECTIVEDT    IN     accessorial_param_set.effective_dt%TYPE,
      PEXPIRATIONDT   IN     accessorial_param_set.expiration_dt%TYPE,
      PPARAMID        IN     accessorial_param_set.accessorial_param_set_id%TYPE,
      RETURN_VAL         OUT NUMBER);
	  
   PROCEDURE VALIDT_CARR_PRM_FEAS_INCOTERM ( 
   vTcCompantId IN COMPANY.COMPANY_ID%TYPE,
   vParamSetId IN IMP_CARR_PARAM_ACC_INCOTERM.ACC_PARAMSET_ID%TYPE,
   vAccRateId IN IMP_CARR_PARAM_ACC_INCOTERM.ACC_RATE_ID%TYPE,
   vFail OUT NUMBER );
 
   PROCEDURE TRSFR_CARR_PRM_FEAS_INCOTERM (	
   vTCCompanyId			IN COMPANY.COMPANY_ID%TYPE,
   vImpParamSetId IN IMP_CARR_PARAM_ACC_INCOTERM.ACC_PARAMSET_ID%TYPE,
   vActualParamSetId IN IMP_CARR_PARAM_ACC_INCOTERM.ACC_PARAMSET_ID%TYPE,
   vAccRateId IN import_accessorial_rate.ACCESSORIAL_RATE_ID%TYPE);
		
END import_carrier_parms_pkg;
/

--
-- IMPORT_CARRIER_PARMS_PKG  (Package Body) 
--Formatted on 11/3/2011 10:29:16 AM (QP5 v5.163.1008.3004) 
CREATE OR REPLACE 
PACKAGE BODY IMPORT_CARRIER_PARMS_PKG
AS
   /*---------------------------------------------------------------------------------------
    #
    #   DATE               AUTHOR         DESCRIPTION
    #  ----------         ---------      ---------------------------------------------------
    #  05/31/2002         S. Lal         Initial development for UC 1104B - Auto Import
    #                                    Carrier Parms
    #
    #  04/07/2003         P  Pathak     Added Payee_carrier_code in PROCEDURE imp_accessorial_rate,
    #                                                                        FUNCTION insert_accessorial_rate,PROCEDURE transfer_accessorial_rate.
    #  09/02/2003         Karthik       Procedure insert_cm_rate_di1 added to support insert_cm_rate_discount function (DB2 Migration - Karthik)
    #  09/10/2003          MB                      MINIMUM_SIZE,MAXIMUM_SIZE,SIZE_UOM support added to IMP_ACCESSORIAL_RATE (Deepak-Accessorial)
    #  09/10/2003          MB                      MINIMUM_SIZE,MAXIMUM_SIZE,SIZE_UOM support added to INSERT_ACCESSORIAL_RATE (Deepak-Accessorial)
    #  09/10/2003          MB                      MINIMUM_SIZE,MAXIMUM_SIZE,SIZE_UOM support added to TRANSFER_ACCESSORIAL_RATE (Deepak-Accessorial)
    #  02/02/2005  MB  modified ADJUST_ACC_RATE_EFF_EXP_DT (TT 44471, Gates Riley)
    #  06/06/2006  Ganesh  CR # 26130
    ----------------------------------------------------------------------------------------*/

   /* List of Statuses in the Import Accessorial Param Set Table

      Status - 3: Imported Successfully
      Status - 1: Validation Pass
      Status - 4: Validation Failed

    */

   PROCEDURE imp_accessorial_param_set (
      pParamId               IN import_accessorial_param_set.param_id%TYPE,
      pTCCompanyId           IN import_accessorial_param_set.tc_company_id%TYPE,
      pCarrierCode           IN import_accessorial_param_set.carrier_code%TYPE,
      pBusinessUnit          IN import_accessorial_param_set.business_unit%TYPE,
      pUseFakCommodity       IN import_accessorial_param_set.use_fak_commodity%TYPE,
      pCommodityClass        IN import_accessorial_param_set.commodity_class%TYPE,
      pEquipmentCode         IN import_accessorial_param_set.equipment_code%TYPE,
      pEffectiveDt           IN import_accessorial_param_set.effective_dt%TYPE,
      pExpirationDt          IN import_accessorial_param_set.expiration_dt%TYPE,
      pCmRateType            IN import_accessorial_param_set.cm_rate_type%TYPE,
      pCmRateCurrecyCode     IN import_accessorial_param_set.cm_rate_currency_code%TYPE,
      pStopOffCurrencyCode   IN import_accessorial_param_set.stop_off_currency_code%TYPE,
      pDistanceUom           IN import_accessorial_param_set.distance_uom%TYPE,
      pBudgetedApproved      IN import_accessorial_param_set.budgeted_approved%TYPE,
      pSizeUom               IN import_accessorial_param_set.size_uom%TYPE,
      pIsGlobal              IN import_accessorial_param_set.is_global%TYPE,
      pStatus                IN import_accessorial_param_set.status%TYPE,
      pCarrierId             IN import_accessorial_param_set.carrier_id%TYPE,
      pEquipmentId           IN import_accessorial_param_set.equipment_id%TYPE,
      pSizeUomId             IN import_accessorial_param_set.size_uom_id%TYPE)
   IS
   BEGIN
      INSERT INTO import_accessorial_param_set (param_id,
                                                tc_company_id,
                                                carrier_code,
                                                business_unit,
                                                use_fak_commodity,
                                                commodity_class,
                                                equipment_code,
                                                effective_dt,
                                                expiration_dt,
                                                cm_rate_type,
                                                cm_rate_currency_code,
                                                stop_off_currency_code,
                                                distance_uom,
                                                budgeted_approved,
                                                size_uom,
                                                created_source_type,
                                                created_source,
                                                created_dttm,
                                                last_updated_source_type,
                                                last_updated_source,
                                                last_updated_dttm,
                                                status,
                                                is_global,
                                                carrier_id,
                                                equipment_id,
                                                size_uom_id)
           VALUES (pParamId,
                   pTCCompanyId,
                   pCarrierCode,
                   pBusinessUnit,
                   pUseFakCommodity,
                   pCommodityClass,
                   pEquipmentCode,
                   pEffectiveDt,
                   pExpirationDt,
                   pCmRateType,
                   pCmRateCurrecyCode,
                   pStopOffCurrencyCode,
                   pDistanceUom,
                   pBudgetedApproved,
                   pSizeUom,
                   3,
                   'OMS',
                   SYSDATE,
                   3,
                   'OMS',
                   SYSDATE,
                   pStatus,
                   pIsGlobal,
                   pCarrierId,
                   pEquipmentId,
                   pSizeUomId);

      COMMIT;
   /*
   EXCEPTION

     WHEN OTHERS THEN

            DBMS_OUTPUT.PUT_LINE('Procedure imp_accessorial_param_set: ' || SQLERRM);
   */
   END imp_accessorial_param_set;

   PROCEDURE imp_cm_rate_discount (
      pParamId       IN import_cm_rate_discount.param_id%TYPE,
      pStartNumber   IN import_cm_rate_discount.start_number%TYPE,
      pEndNumber     IN import_cm_rate_discount.end_number%TYPE,
      pValue         IN import_cm_rate_discount.VALUE%TYPE,
      pStatus        IN import_cm_rate_discount.status%TYPE)
   IS
   BEGIN
      INSERT INTO import_cm_rate_discount (param_id,
                                           cm_rate_discount_id,
                                           start_number,
                                           end_number,
                                           VALUE,
                                           status)
           VALUES (pParamId,
                   cm_rate_discount_id_seq.NEXTVAL,
                   pStartNumber,
                   pEndNumber,
                   pValue,
                   pStatus);

      COMMIT;
   /* EXCEPTION

     WHEN OTHERS THEN

            DBMS_OUTPUT.PUT_LINE('Procedure imp_cm_rate_discount: ' || SQLERRM);
   */
   END imp_cm_rate_discount;

   PROCEDURE imp_stop_off_charge (
      pParamId       IN import_stop_off_charge.param_id%TYPE,
      pStartNumber   IN import_stop_off_charge.start_number%TYPE,
      pEndNumber     IN import_stop_off_charge.end_number%TYPE,
      pValue         IN import_stop_off_charge.VALUE%TYPE,
      pStatus        IN import_stop_off_charge.status%TYPE)
   IS
   BEGIN
      INSERT INTO import_stop_off_charge (param_id,
                                          stop_off_charge_id,
                                          start_number,
                                          end_number,
                                          VALUE,
                                          status)
           VALUES (pParamId,
                   stop_off_charge_id_seq.NEXTVAL,
                   pStartNumber,
                   pEndNumber,
                   pValue,
                   pStatus);

      COMMIT;
   /* EXCEPTION

     WHEN OTHERS THEN

            DBMS_OUTPUT.PUT_LINE('Procedure imp_stop_off_charge: ' || SQLERRM);
   */
   END imp_stop_off_charge;

   PROCEDURE imp_accessorial_rate (
      pAccRateId                IN import_accessorial_rate.accessorial_rate_id%TYPE,
      pParamId                  IN import_accessorial_rate.param_id%TYPE,
      pTCCompanyId              IN import_accessorial_rate.accessorial_rate_id%TYPE,
      pMot                      IN import_accessorial_rate.mot%TYPE,
      pEquipmentCode            IN import_accessorial_rate.equipment_code%TYPE,
      pServiceLevel             IN import_accessorial_rate.service_level%TYPE,
      pProtectionLevel          IN import_accessorial_rate.protection_level%TYPE,
      pAccessorialCode          IN import_accessorial_rate.accessorial_code%TYPE,
      pRate                     IN import_accessorial_rate.rate%TYPE,
      pMinimumRate              IN import_accessorial_rate.minimum_rate%TYPE,
      pCurrencyCode             IN import_accessorial_rate.currency_code%TYPE,
      p_minimum_size            IN IMPORT_ACCESSORIAL_RATE.MINIMUM_SIZE%TYPE,
      p_maximum_size            IN IMPORT_ACCESSORIAL_RATE.MAXIMUM_SIZE%TYPE,
      p_size_uom                IN IMPORT_ACCESSORIAL_RATE.SIZE_UOM%TYPE,
      pIsAutoApprove            IN import_accessorial_rate.is_auto_approve%TYPE,
      pEffectiveDt              IN import_accessorial_rate.effective_dt%TYPE,
      pExpirationDt             IN import_accessorial_rate.expiration_dt%TYPE,
      pStatus                   IN import_accessorial_rate.status%TYPE,
      pPayeeCarrierCode         IN import_accessorial_rate.payee_carrier_code%TYPE,
      pMotId                    IN import_accessorial_rate.mot_id%TYPE,
      pEquipmentId              IN import_accessorial_rate.equipment_id%TYPE,
      pServiceLevelId           IN import_accessorial_rate.service_level_id%TYPE,
      pProtectionLevelId        IN import_accessorial_rate.protection_level_id%TYPE,
      pPayeeCarrierId           IN import_accessorial_rate.payee_carrier_id%TYPE,
      p_size_uom_id             IN import_accessorial_rate.size_uom_id%TYPE,
      pAccessorialId            IN import_accessorial_rate.accessorial_id%TYPE,
      pLongestDimension         IN import_accessorial_rate.longest_dimension%TYPE,
      pSecondLongestDimension   IN import_accessorial_rate.second_longest_dimension%TYPE,
      pLengthPlusGrith          IN import_accessorial_rate.Length_Plus_Grith%TYPE,
      weight                    IN import_accessorial_rate.Weight%TYPE,
      pBaseAmount               IN import_accessorial_rate.Base_Amount%TYPE,
      pChargePerXfield1         IN import_accessorial_rate.Charge_Per_x_Field1%TYPE,
      pChargePerXfield2         IN import_accessorial_rate.Charge_Per_x_Field2%TYPE,
      pLowerRange               IN import_accessorial_rate.Lower_Range%TYPE,
      pHigherRange              IN import_accessorial_rate.higher_range%TYPE,
      pMaxLongestDim            IN import_accessorial_rate.MAX_LONGEST_DIM%TYPE,
      pMinLongestDim            IN import_accessorial_rate.MIN_LONGEST_DIM%TYPE,
      pMaxSecLongestDim         IN import_accessorial_rate.MAX_SEC_LONGEST_DIM%TYPE,
      pMinSecLongestDim         IN import_accessorial_rate.MIN_SEC_LONGEST_DIM%TYPE,
      pMaxTrdLongestDim         IN import_accessorial_rate.MAX_TRD_LONGEST_DIM%TYPE,
      pMinTrdLongestDim         IN import_accessorial_rate.MIN_TRD_LONGEST_DIM%TYPE,
      pMaxLengthGirth           IN import_accessorial_rate.MAX_LENGTH_GIRTH%TYPE,
      pMinLengthGirth           IN import_accessorial_rate.MIN_LENGTH_GIRTH%TYPE,
      pMaxWeight                IN import_accessorial_rate.MAX_WEIGHT%TYPE,
      pMinWeight                IN import_accessorial_rate.MIN_WEIGHT%TYPE,
      pBillingMethod            IN import_accessorial_rate.BILLING_METHOD%TYPE,
      pIsZoneOrigin             IN import_accessorial_rate.IS_ZONE_ORIGIN%TYPE,
      pIsZoneDest               IN import_accessorial_rate.IS_ZONE_DEST%TYPE,
      pIsZoneStopoff            IN import_accessorial_rate.IS_ZONE_STOPOFF%TYPE,
      pMaximumRate              IN import_accessorial_rate.MAXIMUM_RATE%TYPE,
      pCustomerRate             IN import_accessorial_rate.CUSTOMER_RATE%TYPE,
      pCustomerMinCharge        IN import_accessorial_rate.CUSTOMER_MIN_CHARGE%TYPE,
      pCalculatedRate           IN import_accessorial_rate.CALCULATED_RATE%TYPE,
      pBaseCharge               IN import_accessorial_rate.BASE_CHARGE%TYPE,
      pAmount                   IN import_accessorial_rate.AMOUNT%TYPE,
      pIncrementVal             IN import_accessorial_rate.INCREMENT_VAL%TYPE,
      pZoneId                   IN import_accessorial_rate.ZONE_ID%TYPE,
      pZoneName                 IN IMPORT_ACCESSORIAL_RATE.ZONE_NAME%TYPE,
	  -- CMGT-615 Starts here
	  vCommID				IN import_accessorial_rate.COMMODITY_CODE_ID%TYPE,
	  vMaxCommId			IN import_accessorial_rate.MAX_RANGE_COMMODITY_CODE_ID%TYPE
	  -- CMGT-615 Ends here
		  )
   IS
   BEGIN
      /* INSERT INTO import_accessorial_rate
       (
          param_id,
          accessorial_rate_id,
          tc_company_id,
          mot,
          equipment_code,
          service_level,
          protection_level,
          accessorial_code,
          rate,
          minimum_rate,
          currency_code,
          is_auto_approve,
          effective_dt,
          expiration_dt,
          status,
          payee_carrier_code,
          MINIMUM_SIZE,
          MAXIMUM_SIZE,
          SIZE_UOM,
          mot_id,
          equipment_id,
          service_level_id,
          protection_level_id,
          payee_carrier_id,
          size_uom_id,
          accessorial_id
       )
       VALUES
       (
          pParamId,
          accessorial_rate_id_seq.NEXTVAL,
          pTCCompanyId,
          pMot ,
          pEquipmentCode,
          pServiceLevel,
          pProtectionLevel,
          pAccessorialCode,
          pRate,
          pMinimumRate,
          pCurrencyCode,
          pIsAutoApprove,
          pEffectiveDt,
          pExpirationDt,
          pStatus,
          pPayeeCarrierCode,
          p_minimum_size,
          p_maximum_size,
          p_size_uom,
          pMotId,
          pEquipmentId,
          pServiceLevelId,
          pProtectionLevelId,
          pPayeeCarrierId,
          p_size_uom_id,
          pAccessorialId
       ); */
      INSERT INTO IMPORT_ACCESSORIAL_RATE (PARAM_ID,
                                           ACCESSORIAL_RATE_ID,
                                           TC_COMPANY_ID,
                                           MOT,
                                           EQUIPMENT_CODE,
                                           SERVICE_LEVEL,
                                           PROTECTION_LEVEL,
                                           ACCESSORIAL_CODE,
                                           RATE,
                                           MINIMUM_RATE,
                                           CURRENCY_CODE,
                                           IS_AUTO_APPROVE,
                                           EFFECTIVE_DT,
                                           EXPIRATION_DT,
                                           STATUS,
                                           PAYEE_CARRIER_CODE,
                                           MINIMUM_SIZE,
                                           MAXIMUM_SIZE,
                                           SIZE_UOM,
                                           MOT_ID,
                                           EQUIPMENT_ID,
                                           SERVICE_LEVEL_ID,
                                           PROTECTION_LEVEL_ID,
                                           PAYEE_CARRIER_ID,
                                           SIZE_UOM_ID,
                                           ACCESSORIAL_ID,
                                           LONGEST_DIMENSION,
                                           SECOND_LONGEST_DIMENSION,
                                           LENGTH_PLUS_GRITH,
                                           WEIGHT,
                                           BASE_AMOUNT,
                                           CHARGE_PER_X_FIELD1,
                                           CHARGE_PER_X_FIELD2,
                                           LOWER_RANGE,
                                           HIGHER_RANGE,
                                           MAX_LONGEST_DIM,
                                           MIN_LONGEST_DIM,
                                           MAX_SEC_LONGEST_DIM,
                                           MIN_SEC_LONGEST_DIM,
                                           MAX_TRD_LONGEST_DIM,
                                           MIN_TRD_LONGEST_DIM,
                                           MAX_LENGTH_GIRTH,
                                           MIN_LENGTH_GIRTH,
                                           MAX_WEIGHT,
                                           MIN_WEIGHT,
                                           BILLING_METHOD,
                                           IS_ZONE_ORIGIN,
                                           IS_ZONE_DEST,
                                           IS_ZONE_STOPOFF,
                                           MAXIMUM_RATE,
                                           CUSTOMER_RATE,
                                           CUSTOMER_MIN_CHARGE,
                                           CALCULATED_RATE,
                                           BASE_CHARGE,
                                           AMOUNT,
                                           INCREMENT_VAL,
                                           ZONE_ID,
                                           ZONE_NAME,
										   commodity_code_id,   -- CMGT-615 Starts here
									max_range_commodity_code_id  -- CMGT-615 Ends here
									)
           VALUES (PPARAMID,
                   PACCRATEID,
                   PTCCOMPANYID,
                   PMOT,
                   PEQUIPMENTCODE,
                   PSERVICELEVEL,
                   PPROTECTIONLEVEL,
                   PACCESSORIALCODE,
                   PRATE,
                   PMINIMUMRATE,
                   PCURRENCYCODE,
                   PISAUTOAPPROVE,
                   PEFFECTIVEDT,
                   PEXPIRATIONDT,
                   PSTATUS,
                   PPAYEECARRIERCODE,
                   P_MINIMUM_SIZE,
                   P_MAXIMUM_SIZE,
                   P_SIZE_UOM,
                   PMOTID,
                   PEQUIPMENTID,
                   PSERVICELEVELID,
                   PPROTECTIONLEVELID,
                   PPAYEECARRIERID,
                   P_SIZE_UOM_ID,
                   PACCESSORIALID,
                   PLONGESTDIMENSION,
                   PSECONDLONGESTDIMENSION,
                   PLENGTHPLUSGRITH,
                   WEIGHT,
                   PBASEAMOUNT,
                   PCHARGEPERXFIELD1,
                   PCHARGEPERXFIELD2,
                   PLOWERRANGE,
                   PHIGHERRANGE,
                   PMAXLONGESTDIM,
                   PMINLONGESTDIM,
                   PMAXSECLONGESTDIM,
                   PMINSECLONGESTDIM,
                   PMAXTRDLONGESTDIM,
                   PMINTRDLONGESTDIM,
                   PMAXLENGTHGIRTH,
                   PMINLENGTHGIRTH,
                   PMAXWEIGHT,
                   PMINWEIGHT,
                   PBILLINGMETHOD,
                   PISZONEORIGIN,
                   PISZONEDEST,
                   PISZONESTOPOFF,
                   PMAXIMUMRATE,
                   PCUSTOMERRATE,
                   PCUSTOMERMINCHARGE,
                   PCALCULATEDRATE,
                   PBASECHARGE,
                   PAMOUNT,
                   PINCREMENTVAL,
                   PZONEID,
                   PZONENAME,
				   vCommId,				-- CMGT-615 Starts here
				   vMaxCommId 				-- CMGT-615 Ends here
				   );

      COMMIT;
   /* EXCEPTION

     WHEN OTHERS THEN

            DBMS_OUTPUT.PUT_LINE('Procedure imp_accessorial_rate: ' || SQLERRM);
   */
   END imp_accessorial_rate;

   -- Driving Procedure

   -- deprecated !
   PROCEDURE validate_carrier_parms_old (
      pTcCompanyId IN COMPANY.COMPANY_ID%TYPE)
   IS
      vParamId          import_accessorial_param_set.param_id%TYPE;
      vTCCompanyId      import_accessorial_param_set.tc_company_id%TYPE;
      vCarrierCode      import_accessorial_param_set.carrier_code%TYPE;
      vEquipment        import_accessorial_param_set.equipment_code%TYPE;
      vBusinessUnit     import_accessorial_param_set.business_unit%TYPE;
      vEffectiveDt      import_accessorial_param_set.effective_dt%TYPE;
      vExpirationDt     import_accessorial_param_set.expiration_dt%TYPE;
      vCommodityClass   import_accessorial_param_set.commodity_class%TYPE;
      vPassFail         NUMBER;
      vFailure          NUMBER;
      vCheckCommodity   NUMBER := 0;

      CURSOR carrier_parms
      IS
         SELECT param_id,
                carrier_code,
                business_unit,
                use_fak_commodity,
                commodity_class,
                equipment_code,
                effective_dt,
                expiration_dt,
                cm_rate_type,
                cm_rate_currency_code,
                stop_off_currency_code,
                distance_uom,
                budgeted_approved,
                is_global
           FROM import_accessorial_param_set
          WHERE tc_company_id = pTcCompanyId AND status = 3;
   BEGIN
      FOR c_carrier_parms IN carrier_parms
      LOOP
         vParamId := c_carrier_parms.param_id;
         vCarrierCode := c_carrier_parms.carrier_code;
         vEquipment := c_carrier_parms.equipment_code;
         vBusinessUnit := c_carrier_parms.business_unit;
         vEffectiveDt := c_carrier_parms.effective_dt;
         vExpirationDt := c_carrier_parms.expiration_dt;
         vCommodityClass := c_carrier_parms.commodity_class;

         vFailure := 0;

         vPassFail :=
            validate_carrier_data (pTcCompanyId,
                                   vParamId,
                                   vCarrierCode,
                                   vEquipment,
                                   vBusinessUnit,
                                   c_carrier_parms.budgeted_approved,
                                   c_carrier_parms.is_global,
                                   c_carrier_parms.cm_rate_type,
                                   c_carrier_parms.cm_rate_currency_code,
                                   c_carrier_parms.distance_uom);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
         END IF;

         -- Validate the CM Rate Type

         vPassFail :=
            validate_cm_rate_type (pTcCompanyId,
                                   vParamId,
                                   c_carrier_parms.cm_rate_type,
                                   c_carrier_parms.carrier_code);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
         END IF;

         -- Validate the Distance UOM if the CM Rate Type is Distance Bands

         vPassFail :=
            validate_distance_uom (pTcCompanyId,
                                   vParamId,
                                   c_carrier_parms.cm_rate_type,
                                   c_carrier_parms.distance_uom);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
         END IF;

         -- Check if the Effective Date <= Expiration Date

         IF vEffectiveDt > vExpirationDt
         THEN
            vPassFail := 1;

            insert_carrier_parms_error (
               pTcCompanyId,
               vParamId,
               'Accessorial Param: Effective Date is greater than Expiration Date',
               NULL,
               'iaps');

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
            END IF;
         END IF;

         -- Commodity Class is required only if the Commodity Flag is selected.

         IF c_carrier_parms.use_fak_commodity = 1
         THEN
            IF vCommodityClass IS NULL
            THEN
               vPassFail := 1;

               insert_carrier_parms_error (
                  pTcCompanyId,
                  vParamId,
                  'Commodity Class is required if Commodity Flag is checked',
                  NULL,
                  'iaps');
            ELSE -- Commodity class needs to be in commodity_class static table.
               SELECT COUNT (1)
                 INTO vCheckCommodity
                 FROM commodity_class
                WHERE commodity_class = vCommodityClass
                      AND commodity_class.description <> 'FAK';

               IF vCheckCommodity = 0
               THEN
                  vPassFail := 1;
                  insert_carrier_parms_error (pTcCompanyId,
                                              vParamId,
                                              'Invalid Commodity Class',
                                              NULL,
                                              'iaps');
               END IF;
            END IF;

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
            END IF;
         ELSE                                -- Commodity class should be null
            vCommodityClass := NULL;
         END IF;

         -- CM Rate Currency Code is Required if Rate Type is not Percentage

         vPassFail :=
            validate_cm (pTcCompanyId,
                         vParamId,
                         c_carrier_parms.cm_rate_type,
                         c_carrier_parms.cm_rate_currency_code,
                         c_carrier_parms.stop_off_currency_code);

         IF (vFailure = 1)
         THEN
            UPDATE import_accessorial_param_set
               SET status = 4,
                   last_updated_dttm = SYSDATE,
                   last_updated_source_type = 3,
                   last_updated_source = 'OMS'
             WHERE tc_company_id = pTcCompanyId AND param_id = vParamId;
         ELSE
            UPDATE import_accessorial_param_set
               SET status = 1,
                   commodity_class = vCommodityClass, -- this is to save null when USE_FAK_COMMODITY is false (0).
                   last_updated_dttm = SYSDATE,
                   last_updated_source_type = 3,
                   last_updated_source = 'OMS'
             WHERE tc_company_id = pTcCompanyId AND param_id = vParamId;
         END IF;

         validate_stop_off (pTcCompanyId, vParamId);
         validate_cm_discount (pTcCompanyId,
                               vParamId,
                               c_carrier_parms.cm_rate_type);
         validate_accessorials (pTCCompanyId, vParamId);
      END LOOP;

      transfer_carrier_parms (pTcCompanyId);

      COMMIT;
   END validate_carrier_parms_old;

   -- new validate_carrier_parms function as of 10/24/02
   PROCEDURE validate_carrier_parms (pTcCompanyId IN COMPANY.COMPANY_ID%TYPE)
   IS
      vParamId          import_accessorial_param_set.param_id%TYPE;
      vTCCompanyId      import_accessorial_param_set.tc_company_id%TYPE;
      vCarrierCode      import_accessorial_param_set.carrier_code%TYPE;
      vCarrierId        import_accessorial_param_set.carrier_id%TYPE;
      vEquipment        import_accessorial_param_set.equipment_code%TYPE;
      vBusinessUnit     import_accessorial_param_set.business_unit%TYPE;
      vEffectiveDt      import_accessorial_param_set.effective_dt%TYPE;
      vExpirationDt     import_accessorial_param_set.expiration_dt%TYPE;
      vCommodityClass   import_accessorial_param_set.commodity_class%TYPE;
      vPassFail         NUMBER;
      vFailure          NUMBER;
      vCheckCommodity   NUMBER := 0;

      CURSOR carrier_parms
      IS
         SELECT param_id,
                carrier_code,
                carrier_id,
                business_unit,
                use_fak_commodity,
                commodity_class,
                equipment_code,
                equipment_id,
                effective_dt,
                expiration_dt,
                cm_rate_type,
                cm_rate_currency_code,
                stop_off_currency_code,
                distance_uom,
                size_uom,
                budgeted_approved,
                is_global
           FROM import_accessorial_param_set
          WHERE tc_company_id = pTcCompanyId AND status = 3;
   BEGIN
      FOR c_carrier_parms IN carrier_parms
      LOOP
         vParamId := c_carrier_parms.param_id;
         vCommodityClass := c_carrier_parms.commodity_class;

         vPassFail :=
            validate_carrier_param_details (
               pTcCompanyId,
               vParamId,
               c_carrier_parms.carrier_code,
               c_carrier_parms.equipment_code,
               c_carrier_parms.business_unit,
               c_carrier_parms.budgeted_approved,
               c_carrier_parms.is_global,
               c_carrier_parms.cm_rate_type,
               c_carrier_parms.cm_rate_currency_code,
               c_carrier_parms.distance_uom,
               c_carrier_parms.stop_off_currency_code,
               c_carrier_parms.use_fak_commodity,
               c_carrier_parms.commodity_class,
               c_carrier_parms.effective_dt,
               c_carrier_parms.expiration_dt,
               c_carrier_parms.size_uom,
               c_carrier_parms.carrier_id,
               c_carrier_parms.equipment_id);

         IF (c_carrier_parms.use_fak_commodity = 0)
         THEN
            vCommodityClass := NULL;
         END IF;

         IF (vPassFail = 1)
         THEN
            UPDATE import_accessorial_param_set
               SET status = 4,
                   last_updated_dttm = SYSDATE,
                   last_updated_source_type = 3,
                   last_updated_source = 'OMS'
             WHERE tc_company_id = pTcCompanyId AND param_id = vParamId;
         ELSE
            UPDATE import_accessorial_param_set
               SET status = 1,
                   commodity_class = vCommodityClass, -- this is to save null when USE_FAK_COMMODITY is false (0).
                   last_updated_dttm = SYSDATE,
                   last_updated_source_type = 3,
                   last_updated_source = 'OMS'
             WHERE tc_company_id = pTcCompanyId AND param_id = vParamId;
         END IF;

         --007 TO BE UNCOMMENTED AFTER PORTING  VALIDATE_DAS_RS
         -- SET PPARAMID0 = vParamId;

         VALIDATE_DAS_RS (pTcCompanyId, vParamId);

         validate_stop_off (pTcCompanyId, vParamId);

         validate_cm_discount (pTcCompanyId,
                               vParamId,
                               c_carrier_parms.cm_rate_type);

         validate_accessorials (pTcCompanyId, vParamId);

         IF (vPassFail = 0)
         THEN
            -- import_accessorial_param_set status is currently 1
            -- and might need to be updated to 4 if details are invalid
            update_carrier_param_status (pTcCompanyId, vParamId);
         END IF;
      END LOOP;

      transfer_carrier_parms (pTcCompanyId);

      COMMIT;
   END validate_carrier_parms;

   FUNCTION validate_cm_rate_type (
      pTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
      pParamId       IN import_accessorial_param_set.param_id%TYPE,
      pCmRateType    IN import_accessorial_param_set.cm_rate_type%TYPE,
      pCarrierCode   IN import_accessorial_param_set.carrier_code%TYPE)
      RETURN NUMBER
   IS
      vFailure           NUMBER := 0;
      vCMRateTypeCheck   NUMBER;
   BEGIN
      IF pCarrierCode IS NOT NULL
      THEN
         IF pCmRateType IS NULL
         THEN
            vFailure := 1;
            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
               'CM Rate Type is required when carrier code is specified.',
               NULL,
               'iaps');
         END IF;
      END IF;

      IF pCmRateType IS NOT NULL
      THEN
         SELECT COUNT (1)
           INTO vCMRateTypeCheck
           FROM cm_rate_type
          WHERE cm_rate_type = pCmRateType;

         IF vCMRateTypeCheck = 0
         THEN
            vFailure := 1;
            DBMS_OUTPUT.PUT_LINE (
               'Function validate_cm_rate_type: CM Rate Type is Invalid.');
            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
               '''' || pCmRateType || ''' is an invalid CM Rate Type',
               NULL,
               'iaps');
         END IF;
      END IF;

      RETURN vFailure;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE ('Function validate_cm_rate_type: ' || SQLERRM);
   END validate_cm_rate_type;

   -- deprecated !
   FUNCTION validate_distance_uom (
      pTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
      pParamId       IN import_accessorial_param_set.param_id%TYPE,
      pCmRateType    IN import_accessorial_param_set.cm_rate_type%TYPE,
      pDistanceUom   IN import_accessorial_param_set.distance_uom%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER := 0;
      vFailure    NUMBER := 0;
   BEGIN
      IF pCmRateType = 'DB'
      THEN
         IF pDistanceUom IS NULL
         THEN
            vFailure := 1;

            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
               'Distance UOM code is required for the rate type:'
               || pCmRateType,
               NULL,
               'iaps');
         END IF;
      END IF;

      SELECT COUNT (1)
        INTO vPassFail
        FROM distance_uom
       WHERE distance_uom = pDistanceUom;

      IF vPassFail = 0
      THEN
         vFailure := 1;

         insert_carrier_parms_error (
            pTcCompanyId,
            pParamId,
            pDistanceUom || ' is an invalid Distance UOM Code',
            NULL,
            'iaps');
      END IF;

      RETURN vFailure;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE ('Function validate_distance_uom: ' || SQLERRM);
         RETURN 1;
   END validate_distance_uom;

   -- deprecated !
   FUNCTION validate_cm (
      pTCCompanyId           IN COMPANY.COMPANY_ID%TYPE,
      pParamId               IN import_accessorial_param_set.param_id%TYPE,
      pCmRateType            IN import_accessorial_param_set.cm_rate_type%TYPE,
      pCmRateCurrencyCode    IN import_accessorial_param_set.cm_rate_currency_code%TYPE,
      pStopOffCurrencyCode   IN import_accessorial_param_set.stop_off_currency_code%TYPE)
      RETURN NUMBER
   IS
      vPassFail               NUMBER := 0;
      vFailure                NUMBER := 0;
      vCmRateCurrencyCount    NUMBER := 0;
      vStopOffCurrencyCount   NUMBER := 0;
   BEGIN
      -- Stop Off Currency Code is Required

      IF pStopOffCurrencyCode IS NULL
      THEN
         vPassFail := 1;

         insert_carrier_parms_error (pTcCompanyId,
                                     pParamId,
                                     'Stop Off Currency Code is required',
                                     NULL,
                                     'iaps');
      END IF;

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      -- CM Rate Currency Code is required if CM Rate Type is not Percentage

      IF pCmRateType != 'PD'
      THEN
         IF pCmRateCurrencyCode IS NULL
         THEN
            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
               'CM Rate Currecncy Code is required if CM Rate Type is not Percentage',
               NULL,
               'iaps');
            vPassFail := 1;
         END IF;
      END IF;

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      -- Validate that the Stop Off and the CM Currency Codes are Valid

      SELECT COUNT (1)
        INTO vCmRateCurrencyCount
        FROM currency
       WHERE currency_code = pCmRateCurrencyCode;

      IF vCmRateCurrencyCount = 0
      THEN
         insert_carrier_parms_error (pTcCompanyId,
                                     pParamId,
                                     'CM Rate Currency Code should be valid',
                                     NULL,
                                     'iaps');
         vPassFail := 1;

         IF vPassFail = 1
         THEN
            vFailure := 1;
         END IF;
      END IF;

      SELECT COUNT (1)
        INTO vStopOffCurrencyCount
        FROM currency
       WHERE currency_code = pStopOffCurrencyCode;

      IF vCmRateCurrencyCount = 0
      THEN
         insert_carrier_parms_error (
            pTcCompanyId,
            pParamId,
            'Stop Off Currency Code should be valid',
            NULL,
            'iaps');
         vPassFail := 1;

         IF vPassFail = 1
         THEN
            vFailure := 1;
         END IF;
      END IF;

      RETURN vFailure;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE ('Function validate_cm: ' || SQLERRM);
         RETURN 1;
   END validate_cm;

   PROCEDURE validate_cm_discount (
      pTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
      pParamId       IN import_accessorial_param_set.param_id%TYPE,
      pCmRateType    IN import_accessorial_param_set.cm_rate_type%TYPE)
   IS
      CURSOR distance_list_cursor
      IS
         SELECT start_number,
                end_number,
                VALUE,
                cm_rate_discount_id
           FROM import_cm_rate_discount
          WHERE param_id = pParamId;

      vFailure              NUMBER;
      vNbCmDiscountFailed   NUMBER := 0;
      vDescription          VARCHAR2 (50);

      vStartNum             import_cm_rate_discount.start_number%TYPE;
      vEndNum               import_cm_rate_discount.end_number%TYPE;
      vDicsount             import_cm_rate_discount.VALUE%TYPE;
      vDiscountId           import_cm_rate_discount.cm_rate_discount_id%TYPE;
   BEGIN
      /** FIRST, check if each element is valid **/
      FOR cm_rate_record IN distance_list_cursor
      LOOP
         vFailure := 0;
         vStartNum := cm_rate_record.start_number;
         vEndNum := cm_rate_record.end_number;
         vDicsount := cm_rate_record.VALUE;
         vDiscountId := cm_rate_record.cm_rate_discount_id;

         -- just determine which description will be used in the error message.
         IF (pCmRateType = 'DB')
         THEN
            vDescription := 'Distance';
         ELSE
            vDescription := 'Leg Number';
         END IF;

         IF (vStartNum IS NULL) OR (vEndNum IS NULL)
         THEN
            vFailure := 1;
            DBMS_OUTPUT.PUT_LINE (
               'Procedure validate_cm_discount: missing end and/or start num.');
            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
                  'Continuous Move: Starting '
               || vDescription
               || ' and Ending '
               || vDescription
               || ' are required.',
               vDiscountId,
               'icrd');
         ELSIF vStartNum > vEndNum
         THEN
            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
                  'Continuous Move: Starting '
               || vDescription
               || ' must be less than Ending '
               || vDescription
               || '.',
               vDiscountId,
               'icrd');

            vFailure := 1;
         END IF;

         -- Charge is required
         IF (vDicsount IS NULL)
         THEN
            vFailure := 1;
            DBMS_OUTPUT.PUT_LINE (
               'Procedure validate_cm_discount: charge is required.');
            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
               'Continuous Move: Charge is required.',
               vDiscountId,
               'icrd');
         END IF;

         IF vFailure = 1
         THEN
            UPDATE import_cm_rate_discount
               SET status = 4
             WHERE param_id = pParamId AND cm_rate_discount_id = vDiscountId;

            vNbCmDiscountFailed := vNbCmDiscountFailed + 1;
         ELSE
            UPDATE import_cm_rate_discount
               SET status = 1
             WHERE param_id = pParamId AND cm_rate_discount_id = vDiscountId;
         END IF;
      END LOOP;

      /** SECOND, if all elements are valid, check if elements overlap **/

      IF (vNbCmDiscountFailed = 0)
      THEN
         -- we need to check for overlaps

         -- first update the error message description
         IF (pCmRateType = 'DB')
         THEN
            vDescription := 'Distance Band';
         ELSE
            vDescription := 'Discount element';
         END IF;

         FOR cm_rate_record IN distance_list_cursor
         LOOP
            vFailure := 0;
            vStartNum := cm_rate_record.start_number;
            vEndNum := cm_rate_record.end_number;
            vDicsount := cm_rate_record.VALUE;
            vDiscountId := cm_rate_record.cm_rate_discount_id;

            vFailure :=
               does_cm_rate_discount_overlap     -- returns 1 if overlap found
                                             (pParamId,
                                              vDiscountId,
                                              vStartNum,
                                              vEndNum,
                                              vDicsount);

            IF vFailure = 1
            THEN
               -- set status to invalid.
               UPDATE import_cm_rate_discount
                  SET status = 4
                WHERE param_id = pParamId
                      AND cm_rate_discount_id = vDiscountId;

               -- update the number of invalid elements
               vNbCmDiscountFailed := vNbCmDiscountFailed + 1;

               -- add error message
               insert_carrier_parms_error (
                  pTcCompanyId,
                  pParamId,
                     'Continuous Move: This '
                  || vDescription
                  || ' overlaps with other '
                  || vDescription
                  || 's.',
                  vDiscountId,
                  'icrd');
            END IF;
         END LOOP;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE ('Procedure validate_cm_discount: ' || SQLERRM);
   END validate_cm_discount;

   FUNCTION does_cm_rate_discount_overlap (
      pParamId            IN import_accessorial_param_set.param_id%TYPE,
      pCmRateDiscountId   IN import_cm_rate_discount.cm_rate_discount_id%TYPE,
      pStartNumber        IN import_cm_rate_discount.start_number%TYPE,
      pEndNumber          IN import_cm_rate_discount.end_number%TYPE,
      pValue              IN import_cm_rate_discount.VALUE%TYPE)
      RETURN NUMBER
   IS
      vOverlapFound   NUMBER := 0;
   BEGIN
      IF (pEndNumber IS NOT NULL)
      THEN
         SELECT COUNT (1)
           INTO vOverlapFound
           FROM import_cm_rate_discount
          WHERE     param_id = pParamId
                AND (end_number >= pStartNumber OR end_number IS NULL)
                AND start_number <= pEndNumber
                AND cm_rate_discount_id <> pCmRateDiscountId;

         IF (vOverlapFound > 0)
         THEN
            RETURN 1;                                   -- true: overlap found
         ELSE
            RETURN 0;                           -- false: no overlap was found
         END IF;
      ELSE -- this should never happen since for now, we don't have the and over... functionality on cm discounts
         SELECT COUNT (1)
           INTO vOverlapFound
           FROM import_cm_rate_discount
          WHERE     param_id = pParamId
                AND (end_number >= pStartNumber OR end_number IS NULL)
                AND cm_rate_discount_id <> pCmRateDiscountId;

         IF (vOverlapFound > 0)
         THEN
            RETURN 1;                                   -- true: overlap found
         ELSE
            RETURN 0;                           -- false: no overlap was found
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Procedure does_cm_rate_discount_overlap: ' || SQLERRM);
         RETURN 1;
   END does_cm_rate_discount_overlap;

   PROCEDURE validate_stop_off (
      pTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
      pParamId       IN import_accessorial_param_set.param_id%TYPE)
   IS
      CURSOR stop_list_cursor
      IS
         SELECT start_number,
                end_number,
                VALUE,
                stop_off_charge_id
           FROM import_stop_off_charge
          WHERE param_id = pParamId;

      vFailure                 NUMBER;
      vNbStopOffChargeFailed   NUMBER := 0;
      vStartNum                import_stop_off_charge.start_number%TYPE;
      vEndNum                  import_stop_off_charge.end_number%TYPE;
      vCharge                  import_stop_off_charge.VALUE%TYPE;
      vStopOffId               import_stop_off_charge.stop_off_charge_id%TYPE;
   BEGIN
      /** FIRST, check if each element is valid **/
      FOR c_stop_list_record IN stop_list_cursor
      LOOP
         vFailure := 0;

         vStartNum := c_stop_list_record.start_number;
         vEndNum := c_stop_list_record.end_number;
         vCharge := c_stop_list_record.VALUE;
         vStopOffId := c_stop_list_record.stop_off_charge_id;

         -- Start Num is required. End Num is NOT (if it is null, it means: and up).
         IF (vStartNum IS NULL)
         THEN
            vFailure := 1;
            DBMS_OUTPUT.PUT_LINE (
               'Procedure validate_stop_off: start num is required.');
            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
               'Stop Off Charges: Initial Stop Off Number is required.',
               vStopOffId,
               'isoc');
         -- if they are both not null, StartNum must be <= EndNum.
         ELSIF (vEndNum IS NOT NULL) AND (vStartNum > vEndNum)
         THEN
            vFailure := 1;
            DBMS_OUTPUT.PUT_LINE (
               'Procedure validate_stop_off: start num > end num');
            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
               'Stop Off Charges: Initial Stop Off Number must be less than or equal to Final Stop Off Number.',
               vStopOffId,
               'isoc');
         END IF;

         -- Charge is required
         IF (vCharge IS NULL)
         THEN
            vFailure := 1;
            DBMS_OUTPUT.PUT_LINE (
               'Procedure validate_stop_off: charge is required.');
            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
               'Stop Off Charges: Charge is required.',
               vStopOffId,
               'isoc');
         END IF;

         IF vFailure = 1
         THEN
            UPDATE import_stop_off_charge
               SET status = 4
             WHERE param_id = pParamId AND stop_off_charge_id = vStopOffId;

            vNbStopOffChargeFailed := vNbStopOffChargeFailed + 1;
         ELSE
            UPDATE import_stop_off_charge
               SET status = 1
             WHERE param_id = pParamId AND stop_off_charge_id = vStopOffId;
         END IF;
      END LOOP;

      /** SECOND, if all elements are valid, check if elements overlap **/

      IF (vNbStopOffChargeFailed = 0)
      THEN
         -- we need to check for overlaps

         FOR c_stop_list_record IN stop_list_cursor
         LOOP
            vFailure := 0;

            vStartNum := c_stop_list_record.start_number;
            vEndNum := c_stop_list_record.end_number;
            vCharge := c_stop_list_record.VALUE;
            vStopOffId := c_stop_list_record.stop_off_charge_id;

            vFailure :=
               does_stop_off_overlap             -- returns 1 if overlap found
                                     (pParamId,
                                      vStopOffId,
                                      vStartNum,
                                      vEndNum,
                                      vCharge);

            IF vFailure = 1
            THEN
               -- set status to invalid.
               UPDATE import_stop_off_charge
                  SET status = 4
                WHERE param_id = pParamId AND stop_off_charge_id = vStopOffId;

               -- update the number of invalid elements
               vNbStopOffChargeFailed := vNbStopOffChargeFailed + 1;

               -- add error message
               insert_carrier_parms_error (
                  pTcCompanyId,
                  pParamId,
                  'Stop Off Charges: This Stop Off Charge element overlaps with other Stop Off Charge elements.',
                  vStopOffId,
                  'isoc');
            END IF;
         END LOOP;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE ('Procedure validate_stop_off: ' || SQLERRM);
   END validate_stop_off;

   PROCEDURE VALIDATE_DAS_RS (
      PTCCOMPANYID   IN IMP_DAS_RS.TC_COMPANY_ID%TYPE,
      PPARAMID       IN IMP_DAS_RS.PARAM_ID%TYPE)
   IS
      VPASSFAIL                     NUMBER;
      VFAILURE                      NUMBER;
      VDELIVERY_AREA_SURCHARGE_ID   IMP_DAS_RS.DAS_RS_ID%TYPE;
      VACCESSORIAL_PARAM_SET_ID     IMP_DAS_RS.PARAM_ID%TYPE;
      VSERVICE_LEVEL_ID             IMP_DAS_RS.SERVICE_LEVEL_ID%TYPE;
      VSERVICE_LEVEL                IMP_DAS_RS.SERVICE_LEVEL%TYPE;
      VSERVICE_LEVEL_BU             IMP_DAS_RS.SERVICE_LEVEL_BU%TYPE;
      VDAS_TYPE_ID                  IMP_DAS_RS.DAS_RS_SURCHARGE_TYPE_ID%TYPE;
      VCHARGE                       IMP_DAS_RS.CHARGE%TYPE;
      VTC_COMPANY_ID                IMP_DAS_RS.TC_COMPANY_ID%TYPE;
      VCHARGE_CURRENCY_CODE         IMP_DAS_RS.CHARGE_CURRENCY_CODE%TYPE;
      VZONE_ID                      IMP_DAS_RS.ZONE_ID%TYPE;
      VACCESSORIAL_ID               IMP_DAS_RS.ACCESSORIAL_ID%TYPE;
      VACCESSORIAL_CODE             VARCHAR (8);
      -- VSQLERRM VARCHAR(255);
      -- VSQLCODE INTEGER DEFAULT 0;
      VPERRORMSGDESC                VARCHAR2 (255);
      PERRORMSGDESC1                VARCHAR2 (255);
      PSUBOBJECTID1                 NUMBER;
      PSUBOBJECTTYPE1               VARCHAR2 (4);

      CURSOR vdas_rs
      IS
         SELECT DAS_RS_ID,
                SERVICE_LEVEL_ID,
                SERVICE_LEVEL,
                SERVICE_LEVEL_BU,
                DAS_RS_SURCHARGE_TYPE_ID,
                CHARGE,
                TC_COMPANY_ID,
                CHARGE_CURRENCY_CODE,
                ZONE_ID,
                PARAM_ID,
                ACCESSORIAL_ID,
                ACCESSORIAL_CODE
           FROM IMP_DAS_RS
          WHERE     param_id = PPARAMID
                AND tc_company_id = PTCCOMPANYID
                AND status = 3;
   BEGIN
      FOR c_vdas_rs IN vdas_rs
      LOOP
         VDELIVERY_AREA_SURCHARGE_ID := c_vdas_rs.DAS_RS_ID;
         VSERVICE_LEVEL_ID := c_vdas_rs.SERVICE_LEVEL_ID;
         VSERVICE_LEVEL := c_vdas_rs.SERVICE_LEVEL;
         VSERVICE_LEVEL_BU := c_vdas_rs.SERVICE_LEVEL_BU;
         VDAS_TYPE_ID := c_vdas_rs.DAS_RS_SURCHARGE_TYPE_ID;
         VCHARGE := c_vdas_rs.CHARGE;
         VTC_COMPANY_ID := c_vdas_rs.TC_COMPANY_ID;
         VCHARGE_CURRENCY_CODE := c_vdas_rs.CHARGE_CURRENCY_CODE;
         VZONE_ID := c_vdas_rs.ZONE_ID;
         VACCESSORIAL_PARAM_SET_ID := c_vdas_rs.PARAM_ID;
         VACCESSORIAL_ID := c_vdas_rs.ACCESSORIAL_ID;
         VACCESSORIAL_CODE := c_vdas_rs.ACCESSORIAL_CODE;

         --|  -- Surcharge type are required.

         IF (VDAS_TYPE_ID IS NULL)
         THEN
            VFAILURE := 1;
            -- SET PERRORMSGDESC1 = ': Surcharge type are required.';
            -- SET PSUBOBJECTID1 = PPARAMID;
            --  SET PSUBOBJECTTYPE1 = 'iar';
            --  CALL IMPORT_CARRIER_PARMS_PKG.INSERT_CARRIER_PARMS_ERROR(PTCCOMPANYID,PPARAMID,PERRORMSGDESC1,PSUBOBJECTID1,PSUBOBJECTTYPE1);
            insert_carrier_parms_error (PTCCOMPANYID,
                                        PPARAMID,
                                        ': Surcharge type are required.',
                                        PPARAMID,
                                        'iar');
         END IF;

         --|  -- Zone are required.

         IF (VZONE_ID IS NULL)
         THEN
            VFAILURE := 1;
            --  SET PERRORMSGDESC1 = ': Zone is invalid.';
            --  SET PSUBOBJECTID1 = PPARAMID;
            -- SET PSUBOBJECTTYPE1 = 'iar';
            --  CALL IMPORT_CARRIER_PARMS_PKG.INSERT_CARRIER_PARMS_ERROR(PTCCOMPANYID,PPARAMID,PERRORMSGDESC1,PSUBOBJECTID1,PSUBOBJECTTYPE1);
            insert_carrier_parms_error (PTCCOMPANYID,
                                        PPARAMID,
                                        ': Zone is invalid.',
                                        PPARAMID,
                                        'iar');
         ELSE
            -- RG_SUB_VALIDATION_PKG.VALIDATE_ZONE(PTCCOMPANYID,CHAR(VZONE_ID),vPassFail);
            VPASSFAIL :=
               RG_SUB_VALIDATION_PKG.VALIDATE_ZONE (PTCCOMPANYID, VZONE_ID);

            IF (VPASSFAIL = 1)
            THEN
               --  SET PERRORMSGDESC1 = 'Accessorials: ''' || CHAR(VZONE_ID) || ''' is an invalid Zone.';
               --     SET PSUBOBJECTID1 = PPARAMID;
               --     SET PSUBOBJECTTYPE1 = 'iar';
               --     CALL IMPORT_CARRIER_PARMS_PKG.INSERT_CARRIER_PARMS_ERROR(PTCCOMPANYID,PPARAMID,PERRORMSGDESC1,PSUBOBJECTID1,PSUBOBJECTTYPE1);
               insert_carrier_parms_error (
                  PTCCOMPANYID,
                  PPARAMID,
                     'Accessorials: '''
                  || TO_CHAR (VZONE_ID)
                  || ''' is an invalid Zone.',
                  PPARAMID,
                  'iar');
            END IF;
         END IF;

         --|  -- VCHARGE are required.
         IF (VCHARGE IS NULL)
         THEN
            VFAILURE := 1;
            -- SET PERRORMSGDESC1 = ': Amount charged is required.';
            --  SET PSUBOBJECTID1 = PPARAMID;
            -- SET PSUBOBJECTTYPE1 = 'iar';
            --  CALL IMPORT_CARRIER_PARMS_PKG.INSERT_CARRIER_PARMS_ERROR(PTCCOMPANYID,PPARAMID,PERRORMSGDESC1,PSUBOBJECTID1,PSUBOBJECTTYPE1);
            insert_carrier_parms_error (PTCCOMPANYID,
                                        PPARAMID,
                                        ': Amount charged is required.',
                                        PPARAMID,
                                        'iar');
         END IF;

         --|  -- VCHARGE_CURRENCY_CODE are required.
         IF (VCHARGE_CURRENCY_CODE IS NULL)
         THEN
            VFAILURE := 1;
            -- SET PERRORMSGDESC1 = ': Charge currency is required.';
            --  SET PSUBOBJECTID1 = PPARAMID;
            --  SET PSUBOBJECTTYPE1 = 'iar';
            --  CALL IMPORT_CARRIER_PARMS_PKG.INSERT_CARRIER_PARMS_ERROR(PTCCOMPANYID,PPARAMID,PERRORMSGDESC1,PSUBOBJECTID1,PSUBOBJECTTYPE1);
            insert_carrier_parms_error (PTCCOMPANYID,
                                        PPARAMID,
                                        ': Charge currency is required.',
                                        PPARAMID,
                                        'iar');
         END IF;

         --|  -- Service Level are required.

         IF (VSERVICE_LEVEL IS NULL)
         THEN
            VFAILURE := 1;
            --  SET PERRORMSGDESC1 = ': Service level are required.';
            --  SET PSUBOBJECTID1 =PPARAMID;
            --  SET PSUBOBJECTTYPE1 = 'iar';
            --  CALL IMPORT_CARRIER_PARMS_PKG.INSERT_CARRIER_PARMS_ERROR(PTCCOMPANYID,PPARAMID,PERRORMSGDESC1,PSUBOBJECTID1,PSUBOBJECTTYPE1);
            insert_carrier_parms_error (PTCCOMPANYID,
                                        PPARAMID,
                                        ': Service level are required.',
                                        PPARAMID,
                                        'iar');
         END IF;

         IF (VSERVICE_LEVEL_ID IS NULL)
         THEN
            VFAILURE := 1;
            -- SET PERRORMSGDESC1 = 'Accessorials: ''' || CHAR(VSERVICE_LEVEL_ID) || ''' is an invalid Service Level.';
            --  SET PSUBOBJECTID1 = PPARAMID;
            --  SET PSUBOBJECTTYPE1 = 'iar';
            -- CALL IMPORT_CARRIER_PARMS_PKG.INSERT_CARRIER_PARMS_ERROR(PTCCOMPANYID,PPARAMID,PERRORMSGDESC1,PSUBOBJECTID1,PSUBOBJECTTYPE1);
            insert_carrier_parms_error (
               PTCCOMPANYID,
               PPARAMID,
                  'Accessorials: '''
               || TO_CHAR (VSERVICE_LEVEL_ID)
               || ''' is an invalid Service Level.',
               PPARAMID,
               'iar');
         END IF;

         --|  -- Accessorial Code is required.
         IF (VACCESSORIAL_CODE IS NULL)
         THEN
            VFAILURE := 1;
            -- SET PERRORMSGDESC1 = 'Accessorials: Accessorial Code is required ';
            -- SET PSUBOBJECTID1 = PPARAMID;
            -- SET PSUBOBJECTTYPE1 = 'iar';
            -- CALL IMPORT_CARRIER_PARMS_PKG.INSERT_CARRIER_PARMS_ERROR(PTCCOMPANYID,PPARAMID,PERRORMSGDESC1,PSUBOBJECTID1,PSUBOBJECTTYPE1);
            insert_carrier_parms_error (
               PTCCOMPANYID,
               PPARAMID,
               'Accessorials: Accessorial Code is required ',
               PPARAMID,
               'iar');
         END IF;

         IF (VACCESSORIAL_ID IS NULL)
         THEN
            VFAILURE := 1;
            --  SET PERRORMSGDESC1 = 'Accessorials: ''' || CHAR(VACCESSORIAL_ID) || ''' is an invalid Accessorial code.';
            --  SET PSUBOBJECTID1 = PPARAMID;
            --  SET PSUBOBJECTTYPE1 = 'iar';
            -- CALL IMPORT_CARRIER_PARMS_PKG.INSERT_CARRIER_PARMS_ERROR(PTCCOMPANYID,PPARAMID,PERRORMSGDESC1,PSUBOBJECTID1,PSUBOBJECTTYPE1);
            insert_carrier_parms_error (
               PTCCOMPANYID,
               PPARAMID,
                  'Accessorials: '''
               || TO_CHAR (VACCESSORIAL_ID)
               || ''' is an invalid Accessorial code.',
               PPARAMID,
               'iar');
         END IF;

         IF VFAILURE = 1
         THEN
            UPDATE IMP_DAS_RS
               SET status = 4
             WHERE param_id = PPARAMID AND tc_company_id = PTCCOMPANYID;
         ELSE
            UPDATE IMP_DAS_RS
               SET status = 1
             WHERE param_id = PPARAMID AND tc_company_id = PTCCOMPANYID;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE ('Procedure VALIDATE_DAS_RS: ' || SQLERRM);
   END VALIDATE_DAS_RS;


   FUNCTION does_stop_off_overlap (
      pParamId       IN import_accessorial_param_set.param_id%TYPE,
      pStopOffId     IN import_stop_off_charge.stop_off_charge_id%TYPE,
      pStartNumber   IN import_stop_off_charge.start_number%TYPE,
      pEndNumber     IN import_stop_off_charge.end_number%TYPE,
      pCharge        IN import_stop_off_charge.VALUE%TYPE)
      RETURN NUMBER
   IS
      vOverlapFound   NUMBER := 0;
   BEGIN
      IF (pEndNumber IS NOT NULL)
      THEN
         SELECT COUNT (1)
           INTO vOverlapFound
           FROM import_stop_off_charge
          WHERE     param_id = pParamId
                AND (end_number >= pStartNumber OR end_number IS NULL)
                AND start_number <= pEndNumber
                AND stop_off_charge_id <> pStopOffId;

         IF (vOverlapFound > 0)
         THEN
            RETURN 1;                                   -- true: overlap found
         ELSE
            RETURN 0;                           -- false: no overlap was found
         END IF;
      ELSE                             -- handle the and over functionality...
         SELECT COUNT (1)
           INTO vOverlapFound
           FROM import_stop_off_charge
          WHERE     param_id = pParamId
                AND (end_number >= pStartNumber OR end_number IS NULL)
                AND stop_off_charge_id <> pStopOffId;

         IF (vOverlapFound > 0)
         THEN
            RETURN 1;                                   -- true: overlap found
         ELSE
            RETURN 0;                           -- false: no overlap was found
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Procedure does_stop_off_overlap: ' || SQLERRM);
         RETURN 1;
   END does_stop_off_overlap;

   PROCEDURE validate_accessorials (
      pTCCompanyId   IN import_accessorial_rate.TC_COMPANY_ID%TYPE,
      pParamId       IN import_accessorial_rate.param_id%TYPE)
   IS
      vPassFail              NUMBER;
      vFailure               NUMBER;
	  vFail 				NUMBER;
      vAccCnt                NUMBER;
      vAccType               accessorial_code.accessorial_type%TYPE;
	  vUseRangeForCommodityCode   VARCHAR2 (5);
      vCommodityCode              VARCHAR2(16);
      vMaxRangeCommodityCode      VARCHAR2(16);
      paramValueExists            NUMBER;
	  vCarrierCode      VARCHAR2(10);
	  
      CURSOR accessorial_list
      IS
         SELECT accessorial_rate_id,
                mot,
                equipment_code,
                service_level,
                protection_level,
                accessorial_code,
                rate,
                minimum_rate,
                effective_dt,
                expiration_dt,
                mot_id,
                equipment_id,
                service_level_id,
                protection_level_id,
                accessorial_id,
                longest_dimension,
                second_longest_dimension,
                length_plus_grith,
                weight,
                base_amount,
                charge_per_x_field1,
                charge_per_x_field2,
                lower_range,
                higher_range,
                MAX_LONGEST_DIM,
                MIN_LONGEST_DIM,
                MAX_SEC_LONGEST_DIM,
                MIN_SEC_LONGEST_DIM,
                MAX_TRD_LONGEST_DIM,
                MIN_TRD_LONGEST_DIM,
                MAX_LENGTH_GIRTH,
                MIN_LENGTH_GIRTH,
                MAX_WEIGHT,
                MIN_WEIGHT,
                BILLING_METHOD,
                IS_ZONE_ORIGIN,
                IS_ZONE_DEST,
                IS_ZONE_STOPOFF,
                MAXIMUM_RATE,
                CUSTOMER_RATE,
                CUSTOMER_MIN_CHARGE,
                CALCULATED_RATE,
                BASE_CHARGE,
                AMOUNT,
                INCREMENT_VAL,
				MINIMUM_SIZE,
				MAXIMUM_SIZE,
				ZONE_ID,ZONE_NAME,
				COMMODITY_CODE_ID,
				MAX_RANGE_COMMODITY_CODE_ID,
				PAYEE_CARRIER_ID 
           FROM import_accessorial_rate
          WHERE     param_id = pParamId
                AND tc_company_id = pTCCompanyId
                AND status = 3;

      vAccessorialRateId     import_accessorial_rate.accessorial_rate_id%TYPE;
      vMot                   import_accessorial_rate.mot%TYPE;
      vEquipmentCode         import_accessorial_rate.equipment_code%TYPE;
      vServiceLevel          import_accessorial_rate.service_level%TYPE;
      vProtectionLevel       import_accessorial_rate.protection_level%TYPE;
      vRate                  import_accessorial_rate.rate%TYPE;
      vEffectiveDt           import_accessorial_rate.effective_dt%TYPE;
      vExpirationDt          import_accessorial_rate.expiration_dt%TYPE;
      vAccessorialCode       import_accessorial_rate.accessorial_code%TYPE;
      vMotId                 import_accessorial_rate.mot_id%TYPE;
      vEquipmentId           import_accessorial_rate.equipment_id%TYPE;
      vServiceLevelId        import_accessorial_rate.service_level_id%TYPE;
      vProtectionLevelId     import_accessorial_rate.protection_level_id%TYPE;
      vAccessorialId         import_accessorial_rate.accessorial_id%TYPE;
      vLongestDimension      import_accessorial_rate.longest_dimension%TYPE;
      vSecLongestDimension   import_accessorial_rate.Second_Longest_Dimension%TYPE;
      vLengthplusgrith       import_accessorial_rate.Length_Plus_Grith%TYPE;
      vWeight                import_accessorial_rate.Weight%TYPE;
      vBaseAmount            import_accessorial_rate.Base_Amount%TYPE;
      vChargePerXField1      import_accessorial_rate.Charge_Per_x_Field1%TYPE;
      vChargePerXField2      import_accessorial_rate.Charge_Per_x_Field2%TYPE;
      vLowerRange            import_accessorial_rate.Lower_Range%TYPE;
      vHigherRange           import_accessorial_rate.Higher_Range%TYPE;

      -- 2008 enhancement
      vMaxLongestDim         import_accessorial_rate.MAX_LONGEST_DIM%TYPE;
      vMinLongestDim         import_accessorial_rate.MIN_LONGEST_DIM%TYPE;
      vMaxSecLongestDim      import_accessorial_rate.MAX_SEC_LONGEST_DIM%TYPE;
      vMinSecLongestDim      import_accessorial_rate.MIN_SEC_LONGEST_DIM%TYPE;
      vMaxTrdLongestDim      import_accessorial_rate.MAX_TRD_LONGEST_DIM%TYPE;
      vMinTrdLongestDim      import_accessorial_rate.MIN_TRD_LONGEST_DIM%TYPE;
      vMaxLengthGirth        import_accessorial_rate.MAX_LENGTH_GIRTH%TYPE;
      vMinLengthGirth        import_accessorial_rate.MIN_LENGTH_GIRTH%TYPE;
      vMaxWeight             import_accessorial_rate.MAX_WEIGHT%TYPE;
      vMinWeight             import_accessorial_rate.MIN_WEIGHT%TYPE;
      vBillingMethod         import_accessorial_rate.BILLING_METHOD%TYPE;
      vIsZoneOrigin          import_accessorial_rate.IS_ZONE_ORIGIN%TYPE;
      vIsZoneDest            import_accessorial_rate.IS_ZONE_DEST%TYPE;
      vIsZoneStopoff         import_accessorial_rate.IS_ZONE_STOPOFF%TYPE;
      vMaximumRate           import_accessorial_rate.MAXIMUM_RATE%TYPE;
      vCustomerRate          import_accessorial_rate.CUSTOMER_RATE%TYPE;
      vCustomerMinCharge     import_accessorial_rate.CUSTOMER_MIN_CHARGE%TYPE;
      vCalculatedRate        import_accessorial_rate.CALCULATED_RATE%TYPE;
      vBaseCharge            import_accessorial_rate.BASE_CHARGE%TYPE;
      vAmount                import_accessorial_rate.AMOUNT%TYPE;
      vIncrementVal          import_accessorial_rate.INCREMENT_VAL%TYPE;
	  vMinimumSize 			 import_accessorial_rate.MINIMUM_SIZE%TYPE;
	  vMaximumSize 			 import_accessorial_rate.MAXIMUM_SIZE%TYPE;
	  vZoneId            import_accessorial_rate.ZONE_ID%TYPE;
	  VZoneName          import_accessorial_rate.ZONE_NAME%TYPE;
	  vCommID				 import_accessorial_rate.COMMODITY_CODE_ID%TYPE;
	  vMaxCommId			 import_accessorial_rate.MAX_RANGE_COMMODITY_CODE_ID%TYPE;	
	  vPayeeCarrierId		 import_accessorial_rate.payee_carrier_id%TYPE;
	  vExtCarrCount          NUMBER;
   BEGIN
      FOR c_accessorials IN accessorial_list
      LOOP
         vMot := c_accessorials.mot;
         vEquipmentCode := c_accessorials.equipment_code;
         vServiceLevel := c_accessorials.service_level;
         vProtectionLevel := c_accessorials.protection_level;
         vRate := c_accessorials.rate;
         vAccessorialRateId := c_accessorials.accessorial_rate_id;
         vEffectiveDt := c_accessorials.effective_dt;
         vExpirationDt := c_accessorials.expiration_dt;
         vAccessorialCode := c_accessorials.accessorial_code;
         vMotId := c_accessorials.mot_id;
         vEquipmentId := c_accessorials.equipment_id;
         vServiceLevelId := c_accessorials.service_level_id;
         vProtectionLevelId := c_accessorials.protection_level_id;
         vAccessorialId := c_accessorials.accessorial_id;
         vLongestDimension := c_accessorials.longest_dimension;
         vSecLongestDimension := c_accessorials.second_longest_dimension;
         vLengthplusgrith := c_accessorials.length_plus_grith;
         vWeight := c_accessorials.weight;
         vBaseAmount := c_accessorials.base_amount;
         vChargePerXField1 := c_accessorials.charge_per_x_field1;
         vChargePerXField2 := c_accessorials.charge_per_x_field2;
         vLowerRange := c_accessorials.lower_range;
         vHigherRange := c_accessorials.higher_range;
         vMaxLongestDim := c_accessorials.MAX_LONGEST_DIM;
         vMinLongestDim := c_accessorials.MIN_LONGEST_DIM;
         vMaxSecLongestDim := c_accessorials.MAX_SEC_LONGEST_DIM;
         vMinSecLongestDim := c_accessorials.MIN_SEC_LONGEST_DIM;
         vMaxTrdLongestDim := c_accessorials.MAX_TRD_LONGEST_DIM;
         vMinTrdLongestDim := c_accessorials.MIN_TRD_LONGEST_DIM;
         vMaxLengthGirth := c_accessorials.MAX_LENGTH_GIRTH;
         vMinLengthGirth := c_accessorials.MIN_LENGTH_GIRTH;
         vMaxWeight := c_accessorials.MAX_WEIGHT;
         vMinWeight := c_accessorials.MIN_WEIGHT;
         vBillingMethod := c_accessorials.BILLING_METHOD;
         vIsZoneOrigin := c_accessorials.IS_ZONE_ORIGIN;
         vIsZoneDest := c_accessorials.IS_ZONE_DEST;
         vIsZoneStopoff := c_accessorials.IS_ZONE_STOPOFF;
         vMaximumRate := c_accessorials.MAXIMUM_RATE;
         vCustomerRate := c_accessorials.CUSTOMER_RATE;
         vCustomerMinCharge := c_accessorials.CUSTOMER_MIN_CHARGE;
         vCalculatedRate := c_accessorials.CALCULATED_RATE;
         vBaseCharge := c_accessorials.BASE_CHARGE;
         vAmount := c_accessorials.AMOUNT;
         vIncrementVal := c_accessorials.INCREMENT_VAL;
		 vMinimumSize := c_accessorials.MINIMUM_SIZE;
		 vMaximumSize := c_accessorials.MAXIMUM_SIZE;
		 vZoneId := c_accessorials.ZONE_ID;
		 vZoneName := c_accessorials.ZONE_NAME;
		 vCommID :=   c_accessorials.COMMODITY_CODE_ID;
		 vMaxCommId :=  c_accessorials.MAX_RANGE_COMMODITY_CODE_ID;
		 vPayeeCarrierId := c_accessorials.PAYEE_CARRIER_ID;
	
		SELECT COUNT (PARAM_VALUE)
           INTO paramValueExists
           FROM COMPANY_PARAMETER
          WHERE TC_COMPANY_ID = pTCCompanyId
                AND PARAM_DEF_ID = 'use_range_for_commodity_codes';

         IF (paramValueExists > 0)
         THEN
            SELECT PARAM_VALUE
              INTO vUseRangeForCommodityCode
              FROM COMPANY_PARAMETER
             WHERE TC_COMPANY_ID = pTCCompanyId
                   AND PARAM_DEF_ID = 'use_range_for_commodity_codes';
         ELSE
            SELECT DFLT_VALUE
              INTO vUseRangeForCommodityCode
              FROM PARAM_DEF
             WHERE PARAM_DEF_ID = 'use_range_for_commodity_codes';
         END IF;
		  
		 IF (c_accessorials.COMMODITY_CODE_ID IS NOT NULL)
         THEN
         begin 
		 SELECT DESCRIPTION_SHORT
              INTO vCommodityCode
              FROM COMMODITY_CODE
             WHERE COMMODITY_CODE_ID = c_accessorials.COMMODITY_CODE_ID;
			 exception when no_data_found then
			 vCommodityCode := c_accessorials.COMMODITY_CODE_ID;
               vFailure := 1;
                insert_carrier_parms_error (
                  pTCCompanyId,
                  pParamId,
                  'Commodity Code ' || c_accessorials.COMMODITY_CODE_ID  || ' does not exist.',
                  vAccessorialRateId,
                  'iar');
                  end;
            END IF;
			
		IF (c_accessorials.MAX_RANGE_COMMODITY_CODE_ID IS NOT NULL)
         THEN
         begin 
		 SELECT DESCRIPTION_SHORT
              INTO vMaxRangeCommodityCode
              FROM COMMODITY_CODE
             WHERE COMMODITY_CODE_ID = c_accessorials.MAX_RANGE_COMMODITY_CODE_ID;
			 exception when no_data_found then
			 vMaxRangeCommodityCode := c_accessorials.MAX_RANGE_COMMODITY_CODE_ID;
               vFailure := 1;
                insert_carrier_parms_error (
                  pTCCompanyId,
                  pParamId,
                  'Commodity Code ' || c_accessorials.MAX_RANGE_COMMODITY_CODE_ID  || ' does not exist.',
                  vAccessorialRateId,
                  'iar');
                  end;
            END IF;
         
         
         IF (vUseRangeForCommodityCode = 'true')
         THEN
            IF ( (c_accessorials.COMMODITY_CODE_ID IS NOT NULL
                  AND c_accessorials.MAX_RANGE_COMMODITY_CODE_ID IS NULL)
                OR (c_accessorials.COMMODITY_CODE_ID IS NULL
                    AND c_accessorials.MAX_RANGE_COMMODITY_CODE_ID IS NOT NULL))
            THEN
               vFailure := 1;
               insert_carrier_parms_error (
                  pTCCompanyId,
                  pParamId,
                  'Use range for commodity codes parameter is set true. So enter both Commodity Code and Max Commodity Code values.',
                  vAccessorialRateId,
                  'iar');
            END IF;
         END IF;
         
		 -- MinumumSize should be less than MaximumSize
		 IF( vMinimumSize IS NOT NULL AND vMaximumSize IS NOT NULL) THEN
			IF(vMinimumSize > vMaximumSize) THEN
			vFailure := 1;
			DBMS_OUTPUT.PUT_LINE(
				'Procedure validate_accessorials: Minumum Size should be less than Maximum Size.');
			insert_carrier_parms_error (pTcCompanyId,
										pParamId,
										'Accessorials: Minumum Size should be less than Maximum Size.',
										vAccessorialRateId,
										'iar');
			END IF;
		END IF;
		 
         -- Rate is required
         IF (vRate IS NULL)
         THEN
            vFailure := 1;
            DBMS_OUTPUT.PUT_LINE (
               'Procedure validate_accessorials: rate is required.');
            insert_carrier_parms_error (pTcCompanyId,
                                        pParamId,
                                        'Accessorials: Rate is required.',
                                        vAccessorialRateId,
                                        'iar');
         END IF;

         -- dates are required.
         IF (vEffectiveDt IS NULL) OR (vExpirationDt IS NULL)
         THEN
            vFailure := 1;
            DBMS_OUTPUT.PUT_LINE (
               'Procedure validate_accessorials: Dates Missing.');
            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
               'Accessorials: Effective Date and Expiration Date are required.',
               vAccessorialRateId,
               'iar');
         -- Check if the Effective Date <= Expiration Date
         ELSIF vEffectiveDt > vExpirationDt
         THEN
            vFailure := 1;
            DBMS_OUTPUT.PUT_LINE (
               'Procedure validate_accessorials: Dates are Invalid.');
            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
               'Accessorials: Effective Date is greater than Expiration Date',
               vAccessorialRateId,
               'iar');
         END IF;
		 
		 -- Check is external Parcel Carrier is used
		 IF (vPayeeCarrierId IS NOT NULL)
		 THEN
			SELECT COUNT(CARRIER_ID) INTO vExtCarrCount FROM CARRIER_CODE WHERE CARRIER_ID = vPayeeCarrierId AND carrier_type_id = 24;
			IF (vExtCarrCount>0) 
			THEN
				vFailure := 1;
				SELECT CARRIER_CODE INTO vCarrierCode FROM CARRIER_CODE WHERE CARRIER_ID =  vPayeeCarrierId;
				insert_carrier_parms_error (
					pTcCompanyId,
					pParamId,
					'Accessorials: Payee Carrier cannot be an External Parcel Carrier '||vCarrierCode,
					vAccessorialRateId,
					'iar');
			END IF;
		 END IF;

         -- Mode must be valid.
         IF (vMot IS NOT NULL)
         THEN
            -- check whether motid exist no need to validate on mot
            --  vPassFail := RG_SUB_VALIDATION_PKG.VALIDATE_MODE (pTCCompanyId, vMot);

            IF (vMotId IS NULL)
            THEN
               vFailure := 1;
               DBMS_OUTPUT.PUT_LINE (
                  'Procedure validate_accessorials: Mode is Invalid.');
               insert_carrier_parms_error (
                  pTCCompanyId,
                  pParamId,
                  'Accessorials: ''' || vMot || ''' is an invalid Mode.',
                  vAccessorialRateId,
                  'iar');
            END IF;
         END IF;

         -- Service Level must be valid.
         IF (vServiceLevel IS NOT NULL)
         THEN
            -- check whether ServiceLevelid exist , no need to validate on ServiceLevel
            --vPassFail := RG_SUB_VALIDATION_PKG.VALIDATE_SERVICE_LEVEL (pTCCompanyId, vServiceLevel);

            IF (vServiceLevelId IS NULL)
            THEN
               vFailure := 1;
               DBMS_OUTPUT.PUT_LINE (
                  'Procedure validate_accessorials: Service Level is Invalid.');
               insert_carrier_parms_error (
                  pTCCompanyId,
                  pParamId,
                     'Accessorials: '''
                  || vServiceLevel
                  || ''' is an invalid Service Level.',
                  vAccessorialRateId,
                  'iar');
            END IF;
         END IF;
		 
----------------------------------------------------Added here for INfeasibility between SL and AOPTG		 
		 
		 IF (vServiceLevel IS NOT NULL)
		 THEN
			IF (vServiceLevelId IS NOT NULL)
			THEN
				vPassFail:=  VAL_SL_FEASBLE_ACCOPTGROUP(pTCCompanyId,vAccessorialId,vServiceLevelId);
				IF (vPassFail = 1) 
				THEN
					vFailure:= 1;
					DBMS_OUTPUT.PUT_LINE (
                  'Procedure validate_accessorials: Service Level is Invalid.');
				  
				  insert_carrier_parms_error (
                  pTCCompanyId,
                  pParamId,
                   'Service Level infeasible with Accessorial Option Group on Accessorial Code',
                  vAccessorialRateId,
                  'iar');
				END IF;
			END IF;
		 END IF;
		 
-------------------------ended here	-------------------------------------------------------------------------	 


         -- Equipment Code must be valid.
         IF (vEquipmentCode IS NOT NULL)
         THEN
            -- check whether vEquipmentId exist , no need to validate on vEquipmentCode
            --vPassFail := RG_SUB_VALIDATION_PKG.VALIDATE_EQUIPMENT (pTCCompanyId, vEquipmentCode);

            IF (vEquipmentId IS NULL)
            THEN
               vFailure := 1;
               DBMS_OUTPUT.PUT_LINE (
                  'Procedure validate_accessorials: Equipment Code is Invalid.');
               insert_carrier_parms_error (
                  pTCCompanyId,
                  pParamId,
                     'Accessorials: '''
                  || vEquipmentCode
                  || ''' is an invalid Equipment Code.',
                  vAccessorialRateId,
                  'iar');
            END IF;
         END IF;

         -- Protection Level must be valid.
         IF (vProtectionLevel IS NOT NULL)
         THEN
            -- check whether vProtectionLevelId exist , no need to validate on vProtectionLevel
            --vPassFail := RG_SUB_VALIDATION_PKG.VALIDATE_PROTECTION_LEVEL (pTCCompanyId, vProtectionLevel);

            IF (vProtectionLevelId IS NULL)
            THEN
               vFailure := 1;
               DBMS_OUTPUT.PUT_LINE (
                  'Procedure validate_accessorials: Protection Level is Invalid.');
               insert_carrier_parms_error (
                  pTCCompanyId,
                  pParamId,
                  vProtectionLevel || ' is an invalid Protection Level.',
                  vAccessorialRateId,
                  'iar');
            END IF;
         END IF;

         -- Accessorial Code is required.
         IF (vAccessorialCode IS NULL)
         THEN
            vFailure := 1;
            DBMS_OUTPUT.PUT_LINE (
               'Procedure validate_accessorials: Accessorial Code is required.');
            insert_carrier_parms_error (
               pTCCompanyId,
               pParamId,
               'Accessorials: Accessorial Code is required ',
               vAccessorialRateId,
               'iar');
         ELSE                                 --  Accessorial Code is not null
            -- Accessorial Code must be valid.
            BEGIN -- check if the accessiorial code exists. If yes, get its type.
               SELECT accessorial_type
                 INTO vAccType
                 FROM accessorial_code
                WHERE accessorial_id = vAccessorialId;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  vAccType := NULL;
            END;

            IF vAccType IS NULL
            THEN
               vFailure := 1;
               DBMS_OUTPUT.PUT_LINE (
                  'Procedure validate_accessorials: Accessorial Code is Invalid.');
               insert_carrier_parms_error (
                  pTCCompanyId,
                  pParamId,
                     'Accessorials: '
                  || vAccessorialCode
                  || ' is an invalid Accessorial Code.',
                  vAccessorialRateId,
                  'iar');
		    -- if accessorial type is NOT VAL then we should not allow Amount / Base Amount / Base Charge / Range Min Amount / Range Max Amount
			ELSIF  vAccType <> 'VAL' AND (c_accessorials.AMOUNT IS NOT NULL OR c_accessorials.base_amount IS NOT NULL OR c_accessorials.BASE_CHARGE IS NOT NULL OR c_accessorials.lower_range IS NOT NULL OR c_accessorials.higher_range IS NOT NULL ) THEN
				vFailure := 1;
				DBMS_OUTPUT.PUT_LINE (
					'Procedure validate_accessorials: Accessorial Type VAL.');
					insert_carrier_parms_error (
						pTCCompanyId,
						pParamId,
						'Accessorials: Amount, Base Amount, Base Charge, Range Min Amount, Range Max Amount should not be provided for Accessorials of type ''' || vAccType || '''.',
						vAccessorialRateId,
						'iar');
						
            -- Miminum Rate is required for PCT and RPU accessorial types.
            ELSIF vAccType <> 'FC'
            THEN                    -- it's either Percentage or Rate Per Unit
               IF (c_accessorials.minimum_rate IS NULL)
               THEN
                  vFailure := 1;
                  DBMS_OUTPUT.PUT_LINE (
                     'Procedure validate_accessorials: Minimum Rate is required.');
                  insert_carrier_parms_error (
                     pTCCompanyId,
                     pParamId,
                     'Accessorials: Minimum Rate is required for accessorials of type '''
                     || vAccType
                     || '''.',
                     vAccessorialRateId,
                     'iar');
               END IF;
            END IF;

            /******  08 carrier parameter interface changes *****/
            -- Max & Min is a pair, neither can be missing, and max >= min
            IF (vMaxLongestDim IS NOT NULL)
            THEN
               IF (vMinLongestDim IS NULL)
               THEN
                  VFAILURE := 1;
                  DBMS_OUTPUT.PUT_LINE (
                     'Accessorials: When Maximum Longest Dimension is given, Minimum Longest Dimension is required');
                  insert_carrier_parms_error (
                     pTCCompanyId,
                     pParamId,
                     'Accessorials: When Maximum Longest Dimension is given, Minimum Longest Dimension is required',
                     vAccessorialRateId,
                     'iar');
               ELSIF (vMinLongestDim > vMaxLongestDim)
               THEN
                  VFAILURE := 1;
                  DBMS_OUTPUT.PUT_LINE (
                     'Accessorials: The Minimum Longest Dimension must NOT be greater than the Maximum Longest Dimension');
                  insert_carrier_parms_error (
                     pTCCompanyId,
                     pParamId,
                     'Accessorials: The Minimum Longest Dimension must NOT be greater than the Maximum Longest Dimension',
                     vAccessorialRateId,
                     'iar');
               END IF;
            ELSIF (vMinLongestDim IS NOT NULL)
            THEN
               VFAILURE := 1;
               DBMS_OUTPUT.PUT_LINE (
                  'Accessorials: When Minimum Longest Dimension is given, Maximum Longest Dimension is required');
               insert_carrier_parms_error (
                  pTCCompanyId,
                  pParamId,
                  'Accessorials: When Minimum Longest Dimension is given, Maximum Longest Dimension is required',
                  vAccessorialRateId,
                  'iar');
            END IF;

            -- 2nd max & 2nd min is a pair
            IF (vMaxSecLongestDim IS NOT NULL)
            THEN
               IF (vMinSecLongestDim IS NULL)
               THEN
                  VFAILURE := 1;
                  DBMS_OUTPUT.PUT_LINE (
                     'Accessorials: When Maximum Second Longest Dimension is given, Minimum Second Longest Dimension is required');
                  insert_carrier_parms_error (
                     pTCCompanyId,
                     pParamId,
                     'Accessorials: When Maximum Second Longest Dimension is given, Minimum Second Longest Dimension is required',
                     vAccessorialRateId,
                     'iar');
               ELSIF (vMinSecLongestDim > vMaxSecLongestDim)
               THEN
                  VFAILURE := 1;
                  DBMS_OUTPUT.PUT_LINE (
                     'Accessorials: The Minimum Second Longest Dimension must NOT be greater than the Maximum Second Longest Dimension');
                  insert_carrier_parms_error (
                     pTCCompanyId,
                     pParamId,
                     'Accessorials: The Minimum Second Longest Dimension must NOT be greater than the Maximum Second Longest Dimension',
                     vAccessorialRateId,
                     'iar');
               END IF;

               -- 2nd max < min
               IF (vMinLongestDim IS NULL
                   OR (vMaxSecLongestDim >= vMinLongestDim))
               THEN
                  VFAILURE := 1;
                  DBMS_OUTPUT.PUT_LINE (
                     'Accessorials: The Maximum Second Longest Dimension must be small than the Minimum Longest Dimension');
                  insert_carrier_parms_error (
                     pTCCompanyId,
                     pParamId,
                     'Accessorials: The Maximum Second Longest Dimension must be small than the Minimum Longest Dimension',
                     vAccessorialRateId,
                     'iar');
               END IF;
            ELSIF (vMinSecLongestDim IS NOT NULL)
            THEN
               VFAILURE := 1;
               DBMS_OUTPUT.PUT_LINE (
                  'Accessorials: When Minimum Second Longest Dimension is given, Maximum Second Longest Dimension is required');
               insert_carrier_parms_error (
                  pTCCompanyId,
                  pParamId,
                  'Accessorials: When Minimum Second Longest Dimension is given, Maximum Second Longest Dimension is required',
                  vAccessorialRateId,
                  'iar');
            END IF;

            -- 3rd max & 3rd min are a pair
            IF (vMaxTrdLongestDim IS NOT NULL)
            THEN
               IF (vMinTrdLongestDim IS NULL)
               THEN
                  VFAILURE := 1;
                  DBMS_OUTPUT.PUT_LINE (
                     'Accessorials: When Maximum Third Longest Dimension is given, Minimum Third Longest Dimension is required');
                  insert_carrier_parms_error (
                     pTCCompanyId,
                     pParamId,
                     'Accessorials: When Maximum Third Longest Dimension is given, Minimum Third Longest Dimension is required',
                     vAccessorialRateId,
                     'iar');
               ELSIF (vMinTrdLongestDim > vMaxTrdLongestDim)
               THEN
                  VFAILURE := 1;
                  DBMS_OUTPUT.PUT_LINE (
                     'Accessorials: The Minimum Third Longest Dimension must NOT be greater than the Maximum Second Longest Dimension');
                  insert_carrier_parms_error (
                     pTCCompanyId,
                     pParamId,
                     'Accessorials: The Minimum Third Longest Dimension must NOT be greater than the Maximum Second Longest Dimension',
                     vAccessorialRateId,
                     'iar');
               END IF;

               IF (vMinSecLongestDim IS NULL
                   OR (vMaxTrdLongestDim >= vMinSecLongestDim))
               THEN
                  VFAILURE := 1;
                  DBMS_OUTPUT.PUT_LINE (
                     'Accessorials: The Maximum Third Longest Dimension must be small than the Minimum Second Longest Dimension');
                  insert_carrier_parms_error (
                     pTCCompanyId,
                     pParamId,
                     'Accessorials: The Maximum Third Longest Dimension must be small than the Minimum Second Longest Dimension',
                     vAccessorialRateId,
                     'iar');
               END IF;
            ELSIF (vMinTrdLongestDim IS NOT NULL)
            THEN
               VFAILURE := 1;
               DBMS_OUTPUT.PUT_LINE (
                  'Accessorials: When Minimum Third Longest Dimension is given, Maximum Third Longest Dimension is required');
               insert_carrier_parms_error (
                  pTCCompanyId,
                  pParamId,
                  'Accessorials: When Minimum Third Longest Dimension is given, Maximum Third Longest Dimension is required',
                  vAccessorialRateId,
                  'iar');
            END IF;

            -- maxlengthGirth & minLengthGirth are a pair
            IF (vMaxLengthGirth IS NOT NULL)
            THEN
               IF (vMinLengthGirth IS NULL)
               THEN
                  VFAILURE := 1;
                  DBMS_OUTPUT.PUT_LINE (
                     'Accessorials: When Maximum Length Plus Girth is given, Minimum Length Plus Girth is required');
                  insert_carrier_parms_error (
                     pTCCompanyId,
                     pParamId,
                     'Accessorials: When Maximum Length Plus Girth is given, Minimum Length Plus Girth is required',
                     vAccessorialRateId,
                     'iar');
               ELSIF (vMinLengthGirth > vMaxLengthGirth)
               THEN
                  VFAILURE := 1;
                  DBMS_OUTPUT.PUT_LINE (
                     'Accessorials: The Minimum Length Plus Girth must NOT be greater than the Maximum Length Plus Girth');
                  insert_carrier_parms_error (
                     pTCCompanyId,
                     pParamId,
                     'Accessorials: The Minimum Length Plus Girth must NOT be greater than the Maximum Length Plus Girth',
                     vAccessorialRateId,
                     'iar');
               END IF;
            ELSIF (vMinLengthGirth IS NOT NULL)
            THEN
               VFAILURE := 1;
               DBMS_OUTPUT.PUT_LINE (
                  'Accessorials: When Minimum Length Plus Girth is given, Maximum Length Plus Girth is required');
               insert_carrier_parms_error (
                  pTCCompanyId,
                  pParamId,
                  'Accessorials: When Minimum Length Plus Girth is given, Maximum Length Plus Girth is required',
                  vAccessorialRateId,
                  'iar');
            END IF;

            -- maxWeight & minWeight are a pair
            IF (vMaxWeight IS NOT NULL)
            THEN
               IF (vMinWeight IS NULL)
               THEN
                  VFAILURE := 1;
                  DBMS_OUTPUT.PUT_LINE (
                     'Accessorials: When Maximum Weight is given, Minimum Weight is required');
                  insert_carrier_parms_error (
                     pTCCompanyId,
                     pParamId,
                     'Accessorials: When Maximum Weight is given, Minimum Weight is required',
                     vAccessorialRateId,
                     'iar');
               ELSIF (vMinWeight > vMaxWeight)
               THEN
                  VFAILURE := 1;
                  DBMS_OUTPUT.PUT_LINE (
                     'Accessorials: The Minimum Weight must NOT be greater than the Maximum Weight');
                  insert_carrier_parms_error (
                     pTCCompanyId,
                     pParamId,
                     'Accessorials: The Minimum Weight must NOT be greater than the Maximum Weight',
                     vAccessorialRateId,
                     'iar');
               END IF;
            ELSIF (vMinWeight IS NOT NULL)
            THEN
               VFAILURE := 1;
               DBMS_OUTPUT.PUT_LINE (
                  'Accessorials: When Minimum Weight is given, Maximum Weight is required');
               insert_carrier_parms_error (
                  pTCCompanyId,
                  pParamId,
                  'Accessorials: When Minimum Weight is given, Maximum Weight is required',
                  vAccessorialRateId,
                  'iar');
            END IF;
         END IF;
		 IF(vZoneName IS NOT NULL)
         THEN
            IF(vZoneId IS NULL)
            THEN
            VFAILURE := 1;
                  DBMS_OUTPUT.PUT_LINE (
                     'Accessorials: Invalid Zone');
                  insert_carrier_parms_error (
                     pTCCompanyId,
                     pParamId,
                     'Accessorials: Invalid Zone',
                     vAccessorialRateId,
                     'iar');
            END IF;
             END IF;
            IF((vIsZoneOrigin = 1 OR vIsZoneDest = 1 OR vIsZoneStopOff = 1) AND vZoneId IS NULL)
            THEN
            VFAILURE := 1;
                  DBMS_OUTPUT.PUT_LINE (
                     'Accessorials: Please Specify the Zone for the selected Zone Apply Type');
                  insert_carrier_parms_error (
                     pTCCompanyId,
                     pParamId,
                     'Accessorials: Please Specify the Zone for the selected Zone Apply Type',
                     vAccessorialRateId,
                     'iar');
            END IF;

		 			VALIDT_CARR_PRM_FEAS_INCOTERM(pTCCompanyId, pParamId, vAccessorialRateId, vFail );
		 IF vFail = 1 THEN
			vFailure := vFail;
		 END IF;
         IF vFailure = 1
         THEN
            UPDATE import_accessorial_rate
               SET status = 4
             WHERE     param_id = pParamId
                   AND tc_company_id = pTCCompanyId
                   AND accessorial_rate_id = vAccessorialRateId;
         ELSE
            UPDATE import_accessorial_rate
               SET status = 1
             WHERE     param_id = pParamId
                   AND tc_company_id = pTCCompanyId
                   AND accessorial_rate_id = vAccessorialRateId;
		 END IF;

         --007 TO UNCOMMENT AFTER PORTING VALIDATE_EXCL_ACC
         VALIDATE_EXCL_ACC (pTCCompanyId, pParamId, vAccessorialRateId);
		 --VALIDT_CARR_PRM_FEAS_INCOTERM(pTCCompanyId, pParamId, vAccessorialRateId);
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Procedure validate_accessorials: ' || SQLERRM);
   END validate_accessorials;

   PROCEDURE VALIDATE_EXCL_ACC (
      PTCCOMPANYID   IN COMPANY.COMPANY_ID%TYPE,
      PPARAMID       IN IMP_EXCLUDED_APS_ACC.PARAM_ID%TYPE,
      PACCRATEID     IN IMP_EXCLUDED_APS_ACC.ACCESSORIAL_RATE_ID%TYPE)
   IS
      VFAILURE                 NUMBER;
      VNBSTOPOFFCHARGEFAILED   NUMBER DEFAULT 0;
      vStartNum                NUMBER;
      vEndNum                  NUMBER;
      vExclAccid               IMP_EXCLUDED_APS_ACC.ACCESSORIAL_ID%TYPE;
      -- SQLERRM VARCHAR(255);

      PERRORMSGDESC            VARCHAR2 (255);
      PSUBOBJECTID             NUMBER;
      PSUBOBJECTTYPE           VARCHAR2 (4);

      PERRORMSGDESC1           VARCHAR2 (255);
      PSUBOBJECTID1            NUMBER;
      PSUBOBJECTTYPE1          VARCHAR2 (4);

      PERRORMSGDESC2           VARCHAR (255);
      PSUBOBJECTID2            NUMBER;
      PSUBOBJECTTYPE2          VARCHAR (4);
      -- DECLARE RETURN_VAL INTEGER;
      PSTOPOFFID               NUMBER;
      PSTARTNUMBER             NUMBER;
      PENDNUMBER               NUMBER;
      PCHARGE                  NUMBER (13, 2);
      PERRORMSGDESC3           VARCHAR (255);
      PSUBOBJECTID3            NUMBER;
      PSUBOBJECTTYPE3          VARCHAR (4);

      CURSOR EXCL_ACC_RECORD
      IS
         SELECT ACCESSORIAL_ID,
                ACCESSORIAL_CODE_EXCL,
                ACCESSORIAL_CODE_EXCL_BU
           FROM IMP_EXCLUDED_APS_ACC
          WHERE param_id = PPARAMID AND ACCESSORIAL_RATE_ID = PACCRATEID;
   BEGIN
      FOR C_EXCL_ACC_RECORD IN EXCL_ACC_RECORD
      LOOP
         VFAILURE := 0;
         vExclAccid := C_EXCL_ACC_RECORD.ACCESSORIAL_ID;

         IF (vExclAccid IS NULL)
         THEN
            VFAILURE := 1;
            --  SET PERRORMSGDESC = 'Excluded Accessorials: Excluded Accessorial is invalid.';
            --   SET PSUBOBJECTID = PACCRATEID;
            --  SET PSUBOBJECTTYPE = 'iea';
            --   CALL IMPORT_CARRIER_PARMS_PKG.INSERT_CARRIER_PARMS_ERROR(PTCCOMPANYID,PPARAMID,PERRORMSGDESC,PSUBOBJECTID,PSUBOBJECTTYPE);
            insert_carrier_parms_error (
               PTCCOMPANYID,
               PPARAMID,
               'Excluded Accessorials: Excluded Accessorial is invalid.',
               PACCRATEID,
               'iea');
         END IF;

         IF (VFAILURE = 1)
         THEN
            UPDATE IMP_EXCLUDED_APS_ACC
               SET status = 4
             WHERE param_id = PPARAMID AND ACCESSORIAL_RATE_ID = PACCRATEID;

            VNBSTOPOFFCHARGEFAILED := VNBSTOPOFFCHARGEFAILED + 1;
         ELSE
            UPDATE IMP_EXCLUDED_APS_ACC
               SET status = 1
             WHERE param_id = PPARAMID AND ACCESSORIAL_RATE_ID = PACCRATEID;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE ('Procedure VALIDATE_EXCL_ACC: ' || SQLERRM);
   END VALIDATE_EXCL_ACC;

   PROCEDURE VALIDATE_APS_OVERLAPS (
      PCARRIERID      IN     accessorial_param_set.carrier_id%TYPE,
      PTCCOMPANYID    IN     accessorial_param_set.tc_company_id%TYPE,
      PEQUIPMENTID    IN     accessorial_param_set.equipment_id%TYPE,
      PEFFECTIVEDT    IN     accessorial_param_set.effective_dt%TYPE,
      PEXPIRATIONDT   IN     accessorial_param_set.expiration_dt%TYPE,
      PPARAMID        IN     accessorial_param_set.accessorial_param_set_id%TYPE,
      RETURN_VAL         OUT NUMBER)
   IS
      VFAILURE   NUMBER;
      VOVLP      INTEGER;
   -- PERRORMSGDESC VARCHAR(255);

   BEGIN
      VFAILURE := 0;

      SELECT COUNT (1)
        INTO VOVLP
        FROM accessorial_param_set
       WHERE     carrier_id = PCARRIERID
             AND tc_company_id = PTCCOMPANYID
             AND (equipment_id = PEQUIPMENTID OR equipment_id IS NULL)
             AND effective_dt <= PEFFECTIVEDT
             AND expiration_dt >= PEXPIRATIONDT;

      -- AND accessorial_param_set_id <> PPARAMID;

      IF (VOVLP > 0)
      THEN
         VFAILURE := 1;

         --PERRORMSGDESC = 'A duplicate carrier parameter (carrier/equipment/business unit/date range) already exists.' ;

         --  CALL IMPORT_CARRIER_PARMS_PKG.INSERT_CARRIER_PARMS_ERROR(PTCCOMPANYID,PPARAMID,PERRORMSGDESC,CAST(NULL AS BIGINT),'iaps');

         insert_carrier_parms_error (
            PTCCOMPANYID,
            PPARAMID,
            'A duplicate carrier parameter (carrier/equipment/business unit/date range) already exists.',
            NULL,
            'iaps');
      END IF;

      RETURN_VAL := VFAILURE;
   END VALIDATE_APS_OVERLAPS;

   PROCEDURE update_carrier_param_status (
      pTcCompanyId   IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId       IN import_accessorial_param_set.param_id%TYPE)
   IS
      vInvalidRecordICRDExists   NUMBER;
      vInvalidRecordISOCExists   NUMBER;
      vInvalidRecordIARExists    NUMBER;
	  vInvalidRecordIDASExists   NUMBER;
   BEGIN
      SELECT COUNT (1)
        INTO vInvalidRecordICRDExists
        FROM import_cm_rate_discount
       WHERE param_id = pParamId AND status = 4;

      SELECT COUNT (1)
        INTO vInvalidRecordISOCExists
        FROM import_stop_off_charge
       WHERE param_id = pParamId AND status = 4;

      SELECT COUNT (1)
        INTO vInvalidRecordIARExists
        FROM import_accessorial_rate
       WHERE param_id = pParamId AND status = 4;
	   
	  SELECT COUNT (1)
       INTO vInvalidRecordIDASExists
       FROM IMP_DAS_RS
      WHERE param_id = pParamId AND status = 4;

      IF    (vInvalidRecordICRDExists > 0)
         OR (vInvalidRecordISOCExists > 0)
         OR (vInvalidRecordIARExists > 0)
		 OR (vInvalidRecordIDASExists > 0)
      THEN
         UPDATE import_accessorial_param_set
            SET status = 4,
                last_updated_dttm = SYSDATE,
                last_updated_source_type = 3,
                last_updated_source = 'OMS'
          WHERE tc_company_id = pTcCompanyId AND param_id = pParamId;
      END IF;
   END update_carrier_param_status;
   
   
   -----------------------Added here for INfeasibility between SL and AOPTG------------------------------------------------------
    FUNCTION VAL_SL_FEASBLE_ACCOPTGROUP
(
   vTCCompanyId			IN import_accessorial_rate.TC_COMPANY_ID%TYPE,
   vAccId   IN    import_accessorial_rate.accessorial_id%TYPE,
   vServLevelId IN import_accessorial_rate.service_level_id%TYPE
)
  RETURN NUMBER
  IS
  vPassFail			NUMBER;
  vCount        NUMBER;
  vaccOptionGroup  VARCHAR2 (4);
  
  BEGIN
  vCount := 0;
  vaccOptionGroup := NULL;
   vPassFail := 0;
  
	SELECT ACCESSORIAL_OPTION_GROUP_ID  
	INTO vaccOptionGroup
	FROM ACCESSORIAL_CODE WHERE ACCESSORIAL_ID=vAccId  AND TC_COMPANY_ID=vTCCompanyId ;
	
	SELECT COUNT (INFEASIBILITY_ID)
        INTO vCount
        FROM INFEASIBILITY
       WHERE INFEASIBILITY_TYPE = 'SLOG'
             AND VALUE1 = vServLevelId
             AND value2 = vaccOptionGroup
             AND TC_COMPANY_ID = vTCCompanyId;
			 
IF (vaccOptionGroup IS NOT NULL)
      THEN	
	 IF (vCount > 0)
        THEN
               vPassFail := 1;
               
      END IF;
END IF;	  
	  
  
	return vPassFail;
  
  END VAL_SL_FEASBLE_ACCOPTGROUP; 

----------------------------------------------------------ended here------------------------------------------
   -- function that validated the main body of the Carrier Param XML
   FUNCTION validate_carrier_param_details (
      pTcCompanyId           IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId               IN import_accessorial_param_set.param_id%TYPE,
      pCarrierCode           IN import_accessorial_param_set.carrier_code%TYPE,
      pEquipment             IN import_accessorial_param_set.equipment_code%TYPE,
      pBusinessUnit          IN import_accessorial_param_set.business_unit%TYPE,
      pBudgetedApproved      IN import_accessorial_param_set.budgeted_approved%TYPE,
      pIsGlobal              IN import_accessorial_param_set.is_global%TYPE,
      pCmRateType            IN import_accessorial_param_set.cm_rate_type%TYPE,
      pCmRateCurrencyCode    IN import_accessorial_param_set.cm_rate_currency_code%TYPE,
      pDistanceUom           IN import_accessorial_param_set.distance_uom%TYPE,
      pStopOffCurrencyCode   IN import_accessorial_param_set.stop_off_currency_code%TYPE,
      pUseFAK                IN import_accessorial_param_set.use_fak_commodity%TYPE,
      pCommodityClass        IN import_accessorial_param_set.commodity_class%TYPE,
      pEffectiveDt           IN import_accessorial_param_set.effective_dt%TYPE,
      pExpirationDt          IN import_accessorial_param_set.expiration_dt%TYPE,
      pWeightUOM             IN import_accessorial_param_set.size_uom%TYPE,
      pCarrierId             IN import_accessorial_param_set.carrier_id%TYPE,
      pEquipmentId           IN import_accessorial_param_set.equipment_id%TYPE)
      RETURN NUMBER
   IS
      vPassFail            NUMBER;
      vFailure             NUMBER;
      vCompanyEquipCount   NUMBER;
   BEGIN
      vPassFail := 0;
      vFailure := 0;

      -- validate the carrier code along with the budgeted and global flags
      vPassFail :=
         validate_carrier (pTcCompanyId,
                           pParamId,
                           pCarrierCode,
                           pBudgetedApproved,
                           pIsGlobal,
                           pCarrierId);

      IF (vPassFail = 1)
      THEN
         vFailure := 1;
      END IF;

      IF (PBUSINESSUNIT IS NOT NULL) THEN

     vPassFail := validate_bu(
                                       pTcCompanyId,
                                       pParamId,
                                       pBusinessUnit
                                       );

        IF (vPassFail = 1) THEN
             vFailure := 1;
        END IF;
     END IF; 

      vPassFail :=
         validate_equipment (pTcCompanyId,
                             pParamId,
                             pCarrierCode,
                             pEquipment,
                             pEquipmentId,
                             pCarrierId);

      IF (vPassFail = 1)
      THEN
         vFailure := 1;
      END IF;

      vPassFail :=
         validate_dates (pTcCompanyId,
                         pParamId,
                         pEffectiveDt,
                         pExpirationDt);

      IF (vPassFail = 1)
      THEN
         vFailure := 1;
      END IF;

      vPassFail :=
         validate_use_FAK (pTcCompanyId,
                           pParamId,
                           pUseFAK,
                           pCommodityClass);

      IF (vPassFail = 1)
      THEN
         vFailure := 1;
      END IF;

      vPassFail := validate_weight_UOM (pTcCompanyId, pParamId, pWeightUOM);

      IF (vPassFail = 1)
      THEN
         vFailure := 1;
      END IF;

      -- validate stop_off_currency
      vPassFail :=
         validate_stop_off_currency (pTcCompanyId,
                                     pParamId,
                                     pStopOffCurrencyCode);

      IF (vPassFail = 1)
      THEN
         vFailure := 1;
      END IF;

      -- validate CM Rate Type, CM Currency and CM Distance UOM
      vPassFail :=
         validate_CM_data (pTcCompanyId,
                           pParamId,
                           pCarrierCode,
                           pCmRateType,
                           pCmRateCurrencyCode,
                           pDistanceUom);

      IF (vPassFail = 1)
      THEN
         vFailure := 1;
      END IF;

      RETURN vFailure;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Function validate_carrier_param_details: ' || SQLERRM);
   END VALIDATE_CARRIER_PARAM_DETAILS;

   FUNCTION validate_carrier (
      pTcCompanyId        IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId            IN import_accessorial_param_set.param_id%TYPE,
      pCarrierCode        IN import_accessorial_param_set.carrier_code%TYPE,
      pBudgetedApproved   IN import_accessorial_param_set.budgeted_approved%TYPE,
      pIsGlobal           IN import_accessorial_param_set.is_global%TYPE,
      pCarrierId          IN import_accessorial_param_set.carrier_id%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
      vFailure    NUMBER;
	  vCount    NUMBER;
   BEGIN
      vPassFail := 0;
      vFailure := 0;
	  vCount := 0;

      IF pCarrierCode IS NOT NULL
      THEN
         -- check if the carrier code exists
         --vPassFail := rg_sub_validation_pkg.validate_carrier_code(pTcCompanyId, pCarrierCode);
         -- check if carrierId exists or not no need to validate on carriercode
         IF (pCarrierId IS NULL OR LTRIM (RTRIM (pCarrierCode)) = '')
         THEN
            vFailure := 1;
            DBMS_OUTPUT.PUT_LINE (
               'Function validate_carrier: Carrier Invalid');
            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
               '''' || pCarrierCode || ''' is an invalid Carrier Code',
               NULL,
               'iaps');
         ELSIF pIsGlobal = 1
         THEN
            vFailure := 1;
            DBMS_OUTPUT.PUT_LINE (
               'Function validate_carrier: Carrier Code Specified and is_global is 1.');
            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
               'Carrier: ' || pCarrierCode
               || ' - cannot be flagged as global when a carrier code is specified ',
               NULL,
               'iaps');
         END IF;
		 
		 IF (pCarrierId IS NOT NULL) 
		 THEN
			SELECT COUNT(CARRIER_ID) INTO vCount FROM CARRIER_CODE WHERE CARRIER_ID = pCarrierId AND carrier_type_id = 24;
			IF (vCount>0) 
			THEN
				vFailure := 1;
				insert_carrier_parms_error (
					pTcCompanyId,
					pParamId,
					'Carrier Parameter cannot be created for External Parcel Carrier '|| pCarrierCode,
					NULL,
					'iaps');
			END IF;			
		 END IF;
		 
      ELSE                                             -- Carrier Code is Null
         /* The following was removed since Budgeted functionality is disabled

         IF pBudgetedApproved = 0 AND pIsGlobal = 0 THEN
                         vFailure := 1;
                         DBMS_OUTPUT.PUT_LINE('Function validate_carrier: Carrier Required if not global nor budgeted.');
                         insert_carrier_parms_error(pTcCompanyId, pParamId,
                                                                                 'Carrier Code is required if both budgeted flag and global flag is set to false',
                                                                                 null,
                                                                                 'iaps'
                                                                                 );
                 END IF;

                 * The following replaces the above until the Budgeted functionality gets enabled again
                 */
         IF pIsGlobal = 0
         THEN
            vFailure := 1;
            DBMS_OUTPUT.PUT_LINE (
               'Function validate_carrier: Carrier Required if not global.');
            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
               'Carrier Code is required if global flag is set to false',
               NULL,
               'iaps');
         END IF;
      END IF;

      RETURN vFailure;
   END validate_carrier;

   FUNCTION validate_bu (
      pTcCompanyId    IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId        IN import_accessorial_param_set.param_id%TYPE,
      pBusinessUnit   IN import_accessorial_param_set.business_unit%TYPE)
      RETURN NUMBER
   IS
      vPassFail            NUMBER;
      vBusinessUnitCount   NUMBER;
   BEGIN
      vPassFail := 0;

      /*  IF (pBusinessUnit IS NOT NULL) THEN
        vPassFail := Rg_Sub_Validation_Pkg.validate_business_unit(pTCCompanyId, pBusinessUnit);
   */

      SELECT COUNT (COMPANY_NAME)
        INTO vBusinessUnitCount
        FROM COMPANY
       WHERE COMPANY_NAME = PBUSINESSUNIT;

      IF (vBusinessUnitCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      IF (vPassFail = 1)
      THEN
         DBMS_OUTPUT.PUT_LINE ('Function validate_bu: Business Unit Invalid');
         insert_carrier_parms_error (
            pTcCompanyId,
            pParamId,
            '''' || pBusinessUnit || ''' is an invalid Business Unit',
            NULL,
            'iaps');
      END IF;

      RETURN vPassFail;
   END validate_bu;

   FUNCTION validate_equipment (
      pTcCompanyId   IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId       IN import_accessorial_param_set.param_id%TYPE,
      pCarrierCode   IN import_accessorial_param_set.carrier_code%TYPE,
      pEquipment     IN import_accessorial_param_set.equipment_code%TYPE,
      pEquipmentId   IN import_accessorial_param_set.equipment_id%TYPE,
      pCarrierId     IN import_accessorial_param_set.carrier_id%TYPE)
      RETURN NUMBER
   IS
      vPassFail            NUMBER;
      vCompanyEquipCount   NUMBER;
   BEGIN
      vPassFail := 0;

      IF (pEquipment IS NOT NULL)
      THEN
         -- validate whether equipmentid exist no need to validate on equipmentcode
         --vPassFail := rg_sub_validation_pkg.validate_equipment(pTCCompanyId, pEquipment);

         IF (pEquipmentId IS NULL)
         THEN
            DBMS_OUTPUT.PUT_LINE (
               'Function validate_equipment: Equipment Invalid');
            vPassFail := 1;
            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
                  'Carrier Parameter: '''
               || pEquipment
               || ''' is an invalid Equipment Code',
               NULL,
               'iaps');
         ELSE                                                 -- vPassFail = 0
            IF (pCarrierCode IS NOT NULL)
            THEN
               SELECT COUNT (equipment_id)
                 INTO vCompanyEquipCount
                 FROM tp_company_equipment
                WHERE carrier_id = pCarrierId AND equipment_id = pEquipmentId;

               IF (vCompanyEquipCount < 1)
               THEN
                  vPassFail := 1;

                  insert_carrier_parms_error (
                     pTcCompanyId,
                     pParamId,
                        'Carrier Parameter: Equipment Code '''
                     || pEquipment
                     || ''' infeasible with Carrier '''
                     || pCarrierCode
                     || '''',
                     NULL,
                     'iaps');
               END IF;                                 -- vCompanyEquipCount<1
            END IF;                                -- pCarrierCode is not null
         END IF;                                              -- vPassFail = 0
      END IF;                                        -- pEquipment is not null

      RETURN vPassFail;
   END validate_equipment;

   FUNCTION validate_dates (
      pTcCompanyId    IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId        IN import_accessorial_param_set.param_id%TYPE,
      pEffectiveDt    IN import_accessorial_param_set.effective_dt%TYPE,
      pExpirationDt   IN import_accessorial_param_set.expiration_dt%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
   BEGIN
      vPassFail := 0;

      IF (pEffectiveDt IS NULL) OR (pExpirationDt IS NULL)
      THEN
         vPassFail := 1;
         DBMS_OUTPUT.PUT_LINE ('Function validate_dates: Dates Missing');
         insert_carrier_parms_error (
            pTcCompanyId,
            pParamId,
            'Carrier Parameter: Effective Date and Expiration Date are required.',
            NULL,
            'iaps');
      -- Check if the Effective Date <= Expiration Date
      ELSIF pEffectiveDt > pExpirationDt
      THEN
         vPassFail := 1;
         DBMS_OUTPUT.PUT_LINE ('Function validate_dates: Dates Invalid');
         insert_carrier_parms_error (
            pTcCompanyId,
            pParamId,
            'Carrier Parameter: Effective Date is greater than Expiration Date',
            NULL,
            'iaps');
      END IF;

      RETURN vPassFail;
   END validate_dates;

   FUNCTION validate_use_FAK (
      pTcCompanyId      IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId          IN import_accessorial_param_set.param_id%TYPE,
      pUseFAK           IN import_accessorial_param_set.use_fak_commodity%TYPE,
      pCommodityClass   IN import_accessorial_param_set.commodity_class%TYPE)
      RETURN NUMBER
   IS
      vPassFail         NUMBER;
      vCheckCommodity   NUMBER;
   BEGIN
      vPassFail := 0;

      IF (pUseFAK IS NOT NULL) AND (pUseFAK = 1)
      THEN
         -- validate the commodity class
         IF pCommodityClass IS NULL
         THEN
            vPassFail := 1;
            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
               'Carrier Parameter: Commodity Class is required if ''use FAK Commodity Code'' is checked.',
               NULL,
               'iaps');
         -- Commodity Class Validation has been moved to JAVA
         /*
        ELSE   -- Commodity Class is not null
                    -- Check if the pCommodityClass is valid: pCommodityClass needs to be in commodity_class static table
                    SELECT COUNT(1)
                            INTO vCheckCommodity
                            FROM commodity_class
                            WHERE commodity_class = pCommodityClass
                            AND commodity_class.description<>'FAK'; -- FAK commodity class is not allowed

                    IF vCheckCommodity = 0 THEN
                            vPassFail := 1;
                            DBMS_OUTPUT.PUT_LINE('Function validate_use_FAK: Commodity Class Invalid');
                            insert_carrier_parms_error
                                    (
                                            pTcCompanyId,
                                            pParamId,
                                            'Carrier Parameter: ''' || pCommodityClass || ''' is an invalid Commodity Class',
                                            NULL,
                                            'iaps'
                                    );
                    END IF; */
         END IF;
      END IF;

      RETURN vPassFail;
   END validate_use_FAK;

   FUNCTION validate_weight_UOM (
      pTcCompanyId   IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId       IN import_accessorial_param_set.param_id%TYPE,
      pWeightUOM     IN import_accessorial_param_set.size_uom%TYPE)
      RETURN NUMBER
   IS
      vPassFail         NUMBER;
      vCheckWeightUOM   NUMBER;
   BEGIN
      vPassFail := 0;

      IF (pWeightUOM IS NOT NULL)
      THEN
         SELECT COUNT (1)
           INTO vCheckWeightUOM
           FROM size_uom
          WHERE tc_company_id = pTcCompanyId AND size_uom = pWeightUOM;

         IF (vCheckWeightUOM = 0)
         THEN
            vPassFail := 1;
            DBMS_OUTPUT.PUT_LINE (
               'Function validate_weight_UOM: Weight UOM is Invalid');
            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
                  'Carrier Parameter: '''
               || pWeightUOM
               || ''' is an invalid Weight UOM.',
               NULL,
               'iaps');
         END IF;
      END IF;

      RETURN vPassFail;
   END validate_weight_UOM;

   FUNCTION validate_currency_code (
      pTcCompanyId    IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId        IN import_accessorial_param_set.param_id%TYPE,
      pCurrencyCode   IN import_accessorial_param_set.stop_off_currency_code%TYPE,
      pCurrencyType   IN VARCHAR2)
      RETURN NUMBER
   IS
      vPassFail        NUMBER;
      vCheckCurrency   NUMBER;
   BEGIN
      vPassFail := 0;

      IF (pCurrencyCode IS NOT NULL)
      THEN
         SELECT COUNT (1)
           INTO vCheckCurrency
           FROM currency
          WHERE currency_code = pCurrencyCode;

         IF (vCheckCurrency = 0)
         THEN
            vPassFail := 1;
            DBMS_OUTPUT.PUT_LINE (
                  'Function validate_currency_code: '
               || pCurrencyType
               || ' is Invalid');
            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
                  'Carrier Parameter: '''
               || pCurrencyCode
               || ''' is an invalid '
               || pCurrencyType
               || '.',
               NULL,
               'iaps');
         END IF;
      END IF;

      RETURN vPassFail;
   END validate_currency_code;

   FUNCTION validate_stop_off_currency (
      pTcCompanyId           IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId               IN import_accessorial_param_set.param_id%TYPE,
      pStopOffCurrencyCode   IN import_accessorial_param_set.stop_off_currency_code%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
   BEGIN
      vPassFail := 0;

      -- Stop Off Currency Code is Required
      IF (pStopOffCurrencyCode IS NULL)
      THEN
         vPassFail := 1;
         DBMS_OUTPUT.PUT_LINE (
            'Function validate_stop_off_currency: Stop Off Currency Code is required.');
         insert_carrier_parms_error (
            pTcCompanyId,
            pParamId,
            'Carrier Parameter: Stop Off Currency Code is required.',
            NULL,
            'iaps');
      ELSE                -- Validate that the Stop Off Currency Code is valid
         vPassFail :=
            validate_currency_code (pTcCompanyId,
                                    pParamId,
                                    pStopOffCurrencyCode,
                                    'Stop Off Currency');
      END IF;

      RETURN vPassFail;
   END validate_stop_off_currency;

   -- validate CM Rate Type, CM Currency and CM Distance UOM
   FUNCTION validate_CM_data (
      pTcCompanyId          IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId              IN import_accessorial_param_set.param_id%TYPE,
      pCarrierCode          IN import_accessorial_param_set.carrier_code%TYPE,
      pCmRateType           IN import_accessorial_param_set.cm_rate_type%TYPE,
      pCmRateCurrencyCode   IN import_accessorial_param_set.cm_rate_currency_code%TYPE,
      pDistanceUom          IN import_accessorial_param_set.distance_uom%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
      vFailure    NUMBER;
      vCount      NUMBER;
   BEGIN
      vPassFail := 0;
      vFailure := 0;

      -- Validate the CM Rate Type
      vPassFail :=
         validate_cm_rate_type (pTcCompanyId,
                                pParamId,
                                pCmRateType,
                                pCarrierCode);

      IF (vPassFail = 1)
      THEN
         vFailure := 1;
      END IF;

      -- if the CM Rate Type is valid and not null, check for required fields
      IF (vPassFail = 0) AND (pCmRateType IS NOT NULL)
      THEN
         IF (pCmRateType = 'DB')
         THEN
            -- both currency code and distance UOM are required
            IF (pCmRateCurrencyCode IS NULL)
            THEN
               vPassFail := 1;
               DBMS_OUTPUT.PUT_LINE (
                  'Function validate_CM_data: for DB, CM Currency Code is required.');
               insert_carrier_parms_error (
                  pTcCompanyId,
                  pParamId,
                  'Carrier Parameter: for Distance Bands, CM Currency Code is required.',
                  NULL,
                  'iaps');
            END IF;

            IF (pDistanceUom IS NULL)
            THEN
               vPassFail := 1;
               DBMS_OUTPUT.PUT_LINE (
                  'Function validate_CM_data: for DB, CM Distance UOM is required.');
               insert_carrier_parms_error (
                  pTcCompanyId,
                  pParamId,
                  'Carrier Parameter: for Distance Bands,  CM Distance UOM is required.',
                  NULL,
                  'iaps');
            END IF;
         ELSIF (pCmRateType = 'FD')
         THEN
            -- currency code is required
            IF (pCmRateCurrencyCode IS NULL)
            THEN
               vPassFail := 1;
               DBMS_OUTPUT.PUT_LINE (
                  'Function validate_CM_data: for FD, CM Currency Code is required.');
               insert_carrier_parms_error (
                  pTcCompanyId,
                  pParamId,
                  'Carrier Parameter: for Flat Discount, CM Currency Code is required.',
                  NULL,
                  'iaps');
            END IF;
         -- ELSE, pCmRateType is 'PD' and nothing is required
         END IF;
      END IF;

      IF (vPassFail = 1)
      THEN
         vFailure := 1;
      END IF;

      -- verify that distance UOM code is valid
      IF (pDistanceUom IS NOT NULL)
      THEN
         SELECT COUNT (1)
           INTO vCount
           FROM DISTANCE_UOM
          WHERE DISTANCE_UOM = pDistanceUom;

         IF (vCount = 0)
         THEN
            vPassFail := 1;
            DBMS_OUTPUT.PUT_LINE (
               'Function validate_CM_data: CM Distance UOM is invalid.');
            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
                  'Carrier Parameter: CM Distance UOM '''
               || pDistanceUom
               || ''' is invalid.',
               NULL,
               'iaps');
         END IF;
      END IF;

      IF (vPassFail = 1)
      THEN
         vFailure := 1;
      END IF;

      -- verify that CM currency code is valid
      vPassFail :=
         validate_currency_code (pTcCompanyId,
                                 pParamId,
                                 pCmRateCurrencyCode,
                                 'CM Currency');

      IF (vPassFail = 1)
      THEN
         vFailure := 1;
      END IF;

      RETURN vFailure;
   END validate_CM_data;

   -- deprecated!
   FUNCTION validate_carrier_data (
      pTcCompanyId         IN import_accessorial_param_set.tc_company_id%TYPE,
      pParamId             IN import_accessorial_param_set.param_id%TYPE,
      pCarrierCode         IN import_accessorial_param_set.carrier_code%TYPE,
      pEquipment           IN import_accessorial_param_set.equipment_code%TYPE,
      pBusinessUnit        IN import_accessorial_param_set.business_unit%TYPE,
      pBudgetedApproved    IN import_accessorial_param_set.budgeted_approved%TYPE,
      pIsGlobal            IN import_accessorial_param_set.is_global%TYPE,
      pCmRateType          IN import_accessorial_param_set.cm_rate_type%TYPE,
      pCmRateCurrecyCode   IN import_accessorial_param_set.cm_rate_currency_code%TYPE,
      pDistanceUom         IN import_accessorial_param_set.distance_uom%TYPE)
      RETURN NUMBER
   IS
      vPassFail            NUMBER;
      vFailure             NUMBER;
      vCompanyEquipCount   NUMBER;
   BEGIN
      vPassFail := 0;
      vFailure := 0;

      IF (pCarrierCode IS NOT NULL)
      THEN
         vPassFail :=
            Rg_Sub_Validation_Pkg.validate_carrier_code (pTCCompanyId,
                                                         pCarrierCode);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;

            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
               pCarrierCode || ' is an invalid Carrier Code',
               NULL,
               'iaps');
         END IF;

         IF     (pCmRateType IS NULL)
            AND (pCmRateCurrecyCode IS NULL)
            AND (pDistanceUom IS NULL)
         THEN
            vFailure := 1;

            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
               'Carrier: ' || pCarrierCode
               || ' - CM Rate Type, CM Rate Currency Code and Distance UOM are all required.',
               NULL,
               'iaps');
         END IF;

         IF pBudgetedApproved = 1
         THEN
            IF pIsGlobal = 1
            THEN
               vFailure := 1;

               insert_carrier_parms_error (
                  pTcCompanyId,
                  pParamId,
                  'Carrier: ' || pCarrierCode
                  || ' - cannot be flagged as global when specific carrier code is specified ',
                  NULL,
                  'iaps');
            END IF;
         ELSIF pBudgetedApproved = 0
         THEN
            IF pIsGlobal = 1
            THEN
               vFailure := 1;

               insert_carrier_parms_error (
                  pTcCompanyId,
                  pParamId,
                  'Carrier:' || pCarrierCode
                  || ' - cannot be flagged as global when specific carrier code is specified ',
                  NULL,
                  'iaps');
            END IF;
         END IF;
      ELSE                                             -- Carrier Code is Null
         IF pBudgetedApproved = 0 AND pIsGlobal = 0
         THEN
            vFailure := 1;

            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
               'Carrier Code is required if either budgeted flag or global flag is set to false',
               NULL,
               'iaps');
         END IF;
      END IF;

      IF (pEquipment IS NOT NULL)
      THEN
         vPassFail := 0;

         vPassFail :=
            Rg_Sub_Validation_Pkg.validate_equipment (pTCCompanyId,
                                                      pEquipment);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;

            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
               pEquipment || ' is an invalid Equipment Code',
               NULL,
               'iaps');
         ELSE
            IF (pCarrierCode IS NOT NULL)
            THEN
               SELECT COUNT (equipment_id)
                 INTO vCompanyEquipCount
                 FROM tp_company_equipment
                WHERE carrier_id = pCarrierCode AND equipment_id = pEquipment;

               IF (vCompanyEquipCount < 1)
               THEN
                  vFailure := 1;

                  insert_carrier_parms_error (
                     pTcCompanyId,
                     pParamId,
                        'equipment code '
                     || pEquipment
                     || ' infeasible with carrier '
                     || pCarrierCode,
                     NULL,
                     'iaps');
               END IF;
            END IF;
         END IF;
      END IF;

      IF (pBusinessUnit IS NOT NULL)
      THEN
         vPassFail := 0;

         vPassFail := validate_bu (pTCCompanyId, pParamId, pBusinessUnit);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;

            DBMS_OUTPUT.PUT_LINE (
               'Function validate_carrier_data: Business Unit Invalid');

            insert_carrier_parms_error (
               pTcCompanyId,
               pParamId,
               pBusinessUnit || ' is an invalid Business Unit',
               NULL,
               'iaps');
         END IF;
      END IF;

      RETURN vFailure;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE ('Function validate_carrier_data: ' || SQLERRM);
   END validate_carrier_data;

   PROCEDURE transfer_carrier_parms (pTCCompanyId IN COMPANY.COMPANY_ID%TYPE)
   AS
      vParamId                import_accessorial_param_set.param_id%TYPE;
      vCarrierCode            import_accessorial_param_set.carrier_id%TYPE;
      vBusinessUnit           import_accessorial_param_set.business_unit%TYPE;
      vEquipment              import_accessorial_param_set.equipment_id%TYPE;
      vUseFakCommodity        import_accessorial_param_set.use_fak_commodity%TYPE;
      vCommodityClass         import_accessorial_param_set.commodity_class%TYPE;
      vEffectiveDt            import_accessorial_param_set.effective_dt%TYPE;
      vExpirationDt           import_accessorial_param_set.expiration_dt%TYPE;
      vCmRateType             import_accessorial_param_set.cm_rate_type%TYPE;
      vCmRateCurrecyCode      import_accessorial_param_set.cm_rate_currency_code%TYPE;
      vStopOffCurrency_code   import_accessorial_param_set.stop_off_currency_code%TYPE;
      vDistanceUom            import_accessorial_param_set.distance_uom%TYPE;
      vBudgetedApproved       import_accessorial_param_set.budgeted_approved%TYPE;
      vSizeUom                import_accessorial_param_set.size_uom_id%TYPE;
      vRecordICRDExists       NUMBER;
      vRecordISOCExists       NUMBER;
      vRecordIARExists        NUMBER;
      vPassFail               NUMBER;

      -- Get a list of all the records to be transferred
      -- Make sure that the records do not exist in the accessorial_param_set table
      -- ad they might have failed vaildation at the detail level

      CURSOR carrier_list
      IS
         SELECT i.param_id,
                i.carrier_id,
                i.business_unit,
                i.use_fak_commodity,
                i.commodity_class,
                i.equipment_id,
                i.effective_dt,
                i.expiration_dt,
                i.cm_rate_type,
                i.cm_rate_currency_code,
                i.stop_off_currency_code,
                i.distance_uom,
                i.budgeted_approved,
                i.size_uom_id,
                i.is_global
           FROM import_accessorial_param_set i
          WHERE     i.tc_company_id = pTcCompanyId
                AND i.status = 1                         -- Valid Records Only
                AND NOT EXISTS
                       (SELECT a.accessorial_param_set_id
                          FROM accessorial_param_set a
                         WHERE a.accessorial_param_set_id = i.param_id);

      vExistsExactMatch       accessorial_param_set.accessorial_param_set_id%TYPE;
   BEGIN
      FOR c_carrier_list IN carrier_list
      LOOP
         vPassFail := 0;

         vParamId := c_carrier_list.param_id;

         vExistsExactMatch :=
            find_aps_exact_match (vParamId,
                                  pTcCompanyId,
                                  c_carrier_list.carrier_id,
                                  /*  c_carrier_list.business_unit, */
                                  c_carrier_list.use_fak_commodity,
                                  c_carrier_list.commodity_class,
                                  c_carrier_list.equipment_id,
                                  c_carrier_list.effective_dt,
                                  c_carrier_list.expiration_dt,
                                  c_carrier_list.cm_rate_type,
                                  c_carrier_list.cm_rate_currency_code,
                                  c_carrier_list.stop_off_currency_code,
                                  c_carrier_list.distance_uom,
                                  c_carrier_list.budgeted_approved,
                                  c_carrier_list.size_uom_id,
                                  c_carrier_list.is_global);

         IF (vExistsExactMatch IS NOT NULL)
         THEN
            update_existing_aps (vExistsExactMatch,      -- existing parameter
                                 vParamId,
                                 pTcCompanyId,
                                 c_carrier_list.carrier_id,
                                 /* c_carrier_list.business_unit, */
                                 c_carrier_list.use_fak_commodity,
                                 c_carrier_list.commodity_class,
                                 c_carrier_list.equipment_id,
                                 c_carrier_list.effective_dt,
                                 c_carrier_list.expiration_dt,
                                 c_carrier_list.cm_rate_type,
                                 c_carrier_list.cm_rate_currency_code,
                                 c_carrier_list.stop_off_currency_code,
                                 c_carrier_list.distance_uom,
                                 c_carrier_list.budgeted_approved,
                                 c_carrier_list.size_uom_id,
                                 c_carrier_list.is_global);
         ELSE                                            -- insert the new aps
            vPassFail :=
               insert_accessorial_param_set (
                  vParamId,
                  pTcCompanyId,
                  c_carrier_list.carrier_id,
                  /*   c_carrier_list.business_unit, */
                  c_carrier_list.use_fak_commodity,
                  c_carrier_list.commodity_class,
                  c_carrier_list.equipment_id,
                  c_carrier_list.effective_dt,
                  c_carrier_list.expiration_dt,
                  c_carrier_list.cm_rate_type,
                  c_carrier_list.cm_rate_currency_code,
                  c_carrier_list.stop_off_currency_code,
                  c_carrier_list.distance_uom,
                  c_carrier_list.budgeted_approved,
                  c_carrier_list.size_uom_id,
                  c_carrier_list.is_global);
				  
				  transfer_das_rs(vParamId,
                                       vParamId);
            transfer_cm_rate_discount (vParamId,
                                       vParamId,
                                       c_carrier_list.carrier_id);

            transfer_stop_off_charge (vParamId, vParamId);

            transfer_accessorial_rate (vParamId, vParamId, pTcCompanyId);
         END IF;

         -- Check whether the insert into accessorial_param_set was successful

         IF vPassFail = 0
         THEN
            -- Check whether there are no records in the referenced tables
            -- to avoid the exception for the delete from the main object

            SELECT COUNT (1)
              INTO vRecordICRDExists
              FROM import_cm_rate_discount
             WHERE param_id = vParamId;

            SELECT COUNT (1)
              INTO vRecordISOCExists
              FROM import_stop_off_charge
             WHERE param_id = vParamId;

            SELECT COUNT (1)
              INTO vRecordIARExists
              FROM import_accessorial_rate
             WHERE param_id = vParamId;

            IF     (vRecordICRDExists = 0)
               AND (vRecordISOCExists = 0)
               AND (vRecordIARExists = 0)
            THEN
               DBMS_OUTPUT.PUT_LINE (
                  'Procedure transfer_carrier_parms: Deleting from the import_accessorial_param_set table');

               DELETE FROM import_accessorial_param_set
                     WHERE tc_company_id = pTcCompanyId
                           AND param_id = vParamId;
            END IF;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Procedure transfer_carrier_parms: ' || SQLERRM);
   END transfer_carrier_parms;

   PROCEDURE transfer_cm_rate_discount (
      pImportedParamId   IN import_cm_rate_discount.param_id%TYPE,
      pExistingParamId   IN import_cm_rate_discount.param_id%TYPE,
      pCarrierCode       IN accessorial_param_set.carrier_id%TYPE)
   IS
      CURSOR cm_discount
      IS
         SELECT param_id,
                cm_rate_discount_id,
                start_number,
                end_number,
                VALUE
           FROM import_cm_rate_discount
          WHERE param_id = pImportedParamId AND status = 1;

      vPassFail   NUMBER;
   BEGIN
      /* If no carrier code is specified on the carrier paremeter, no CM discount
              can be saved.

      */

      IF (pCarrierCode IS NOT NULL)
      THEN                                           -- Carrier code specified
         FOR c_cm_discount IN cm_discount
         LOOP
            vPassFail :=
               insert_cm_rate_discount (pExistingParamId,
                                        c_cm_discount.cm_rate_discount_id,
                                        c_cm_discount.start_number,
                                        c_cm_discount.end_number,
                                        c_cm_discount.VALUE);

            IF vPassFail = 0
            THEN
               DELETE FROM import_cm_rate_discount
                     WHERE param_id = pImportedParamId
                           AND cm_rate_discount_id =
                                  c_cm_discount.cm_rate_discount_id;
            END IF;
         END LOOP;
      ELSE                                            --  NO CM Discount Saved
         DELETE FROM import_cm_rate_discount
               WHERE param_id = pImportedParamId;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Procedure transfer_cm_rate_discount: ' || SQLERRM);
   END transfer_cm_rate_discount;

   FUNCTION insert_cm_rate_discount (
      pAccessorialParamSetId   IN cm_rate_discount.accessorial_param_set_id%TYPE,
      pCmRateDiscountId        IN cm_rate_discount.cm_rate_discount_id%TYPE,
      pStartNumber             IN cm_rate_discount.start_number%TYPE,
      pEndNumber               IN cm_rate_discount.end_number%TYPE,
      pValue                   IN cm_rate_discount.VALUE%TYPE)
      RETURN NUMBER
   IS
      vPassfail   NUMBER := 0;
   BEGIN
      INSERT INTO cm_rate_discount (accessorial_param_set_id,
                                    cm_rate_discount_id,
                                    start_number,
                                    end_number,
                                    VALUE)
           VALUES (pAccessorialParamSetId,
                   pCmRateDiscountId,
                   pStartNumber,
                   pEndNumber,
                   pValue);

      RETURN vPassFail;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Procedure insert_cm_rate_discount: ' || SQLERRM);
         RETURN 1;
   END insert_cm_rate_discount;

   PROCEDURE insert_cm_rate_di1 (
      pAccessorialParamSetId   IN     cm_rate_discount.accessorial_param_set_id%TYPE,
      pCmRateDiscountId        IN     cm_rate_discount.cm_rate_discount_id%TYPE,
      pStartNumber             IN     cm_rate_discount.start_number%TYPE,
      pEndNumber               IN     cm_rate_discount.end_number%TYPE,
      pValue                   IN     cm_rate_discount.VALUE%TYPE,
      return_val                  OUT NUMBER)
   IS
   BEGIN
      return_val :=
         insert_cm_rate_discount (pAccessorialParamSetId,
                                  pCmRateDiscountId,
                                  pStartNumber,
                                  pEndNumber,
                                  pValue);
   END insert_cm_rate_di1;

   PROCEDURE transfer_stop_off_charge (
      pImportedParamId   IN import_cm_rate_discount.param_id%TYPE,
      pExistingParamId   IN import_cm_rate_discount.param_id%TYPE)
   IS
      CURSOR stop_off_charge
      IS
         SELECT param_id,
                stop_off_charge_id,
                start_number,
                end_number,
                VALUE
           FROM import_stop_off_charge
          WHERE param_id = pImportedParamId AND status = 1;

      vPassFail   NUMBER;
   BEGIN
      FOR c_stop_off_charge IN stop_off_charge
      LOOP
         vPassFail :=
            insert_stop_off_charge (pExistingParamId,
                                    c_stop_off_charge.stop_off_charge_id,
                                    c_stop_off_charge.start_number,
                                    c_stop_off_charge.end_number,
                                    c_stop_off_charge.VALUE);

         IF vpassFail = 0
         THEN
            DELETE FROM import_stop_off_charge
                  WHERE param_id = pImportedParamId
                        AND stop_off_charge_id =
                               c_stop_off_charge.stop_off_charge_id;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Procedure transfer_stop_off_charge: ' || SQLERRM);
   END transfer_stop_off_charge;

   PROCEDURE TRANSFER_DAS_RS (PIMPORTEDPARAMID IN imp_DAS_RS.Param_Id%TYPE, PEXISTINGPARAMID IN imp_DAS_RS.Param_Id%TYPE)
   IS
      VPASSFAIL                     NUMBER;
      VFAILURE                      NUMBER;
      VDELIVERY_AREA_SURCHARGE_ID   IMP_DAS_RS.DAS_RS_ID%TYPE;
      VACCESSORIAL_PARAM_SET_ID     IMP_DAS_RS.PARAM_ID%TYPE;
      VSERVICE_LEVEL_ID             IMP_DAS_RS.SERVICE_LEVEL_ID%TYPE;
      VDAS_TYPE_ID                  IMP_DAS_RS.DAS_RS_SURCHARGE_TYPE_ID%TYPE;
      VCHARGE                       IMP_DAS_RS.CHARGE%TYPE;
      VTC_COMPANY_ID                IMP_DAS_RS.TC_COMPANY_ID%TYPE;
      VCHARGE_CURRENCY_CODE         IMP_DAS_RS.CHARGE_CURRENCY_CODE%TYPE;
      VZONE_ID                      IMP_DAS_RS.ZONE_ID%TYPE;
      VACCESSORIAL_ID               IMP_DAS_RS.ACCESSORIAL_ID%TYPE;

      -- DECLARE SQLERRM VARCHAR(255);
      -- DECLARE VSQLCODE INTEGER DEFAULT 0;
      -- DECLARE PERRORMSGDESC1 VARCHAR(255);
      -- DECLARE PSUBOBJECTID1 BIGINT;
      -- DECLARE PSUBOBJECTTYPE1 VARCHAR(4);

      CURSOR das_rs
      IS
         SELECT DAS_RS_ID,
                SERVICE_LEVEL_ID,
                DAS_RS_SURCHARGE_TYPE_ID,
                CHARGE,
                TC_COMPANY_ID,
                CHARGE_CURRENCY_CODE,
                ZONE_ID,
                PARAM_ID,
                ACCESSORIAL_ID
           FROM IMP_DAS_RS
          WHERE param_id = PIMPORTEDPARAMID AND status = 1;
   BEGIN
      FOR c_das_rs IN das_rs
      LOOP
         VDELIVERY_AREA_SURCHARGE_ID := c_das_rs.DAS_RS_ID;
         VSERVICE_LEVEL_ID := c_das_rs.SERVICE_LEVEL_ID;
         VDAS_TYPE_ID := c_das_rs.DAS_RS_SURCHARGE_TYPE_ID;
         VCHARGE := c_das_rs.CHARGE;
         VTC_COMPANY_ID := c_das_rs.TC_COMPANY_ID;
         VCHARGE_CURRENCY_CODE := c_das_rs.CHARGE_CURRENCY_CODE;
         VZONE_ID := c_das_rs.ZONE_ID;
         VACCESSORIAL_PARAM_SET_ID := c_das_rs.PARAM_ID;
         VACCESSORIAL_ID := c_das_rs.ACCESSORIAL_ID;

         INSERT INTO DELIVERY_AREA_SURCHARGE (DELIVERY_AREA_SURCHARGE_ID,
                                              SERVICE_LEVEL_ID,
                                              DELIVERY_AREA_SURCHG_TYPE_ID,
                                              CHARGE,
                                              TC_COMPANY_ID,
                                              CHARGE_CURR_CODE,
                                              ZONE_ID,
                                              ACCESSORIAL_PARAM_SET_ID,
                                              ACCESSORIAL_ID,
											  CREATE_SOURCE_TYPE,
                                              CREATED_SOURCE,
                                              CREATED_DTTM,
                                              LAST_UPDATED_SOURCE_TYPE,
                                              LAST_UPDATED_SOURCE,
                                              LAST_UPDATED_DTTM
                                              )
              VALUES (VDELIVERY_AREA_SURCHARGE_ID,
                      VSERVICE_LEVEL_ID,
                      VDAS_TYPE_ID,
                      VCHARGE,
                      VTC_COMPANY_ID,
                      VCHARGE_CURRENCY_CODE,
                      VZONE_ID,
                      PEXISTINGPARAMID,
                      VACCESSORIAL_ID,
					  1,
                      'OMS',
                      getDate(),
                      1,
                      'OMS',
                      getDate()
                      );

         DELETE FROM imp_DAS_RS
               WHERE     param_id = PIMPORTEDPARAMID
                     AND DAS_RS_ID = c_das_rs.DAS_RS_ID
                     AND status = 1;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE ('Procedure TRANSFER_DAS_RS: ' || SQLERRM);
   END TRANSFER_DAS_RS;

   FUNCTION insert_stop_off_charge (
      pAccessorialParamSetId   IN stop_off_charge.accessorial_param_set_id%TYPE,
      pStopOffChargeId         IN stop_off_charge.stop_off_charge_id%TYPE,
      pStartNumber             IN stop_off_charge.start_number%TYPE,
      pEndNumber               IN stop_off_charge.end_number%TYPE,
      pValue                   IN stop_off_charge.VALUE%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER := 0;
   BEGIN
      INSERT INTO stop_off_charge (accessorial_param_set_id,
                                   stop_off_charge_id,
                                   start_number,
                                   end_number,
                                   VALUE)
           VALUES (pAccessorialParamSetId,
                   pStopOffChargeId,
                   pStartNumber,
                   pEndNumber,
                   pValue);

      RETURN vPassFail;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Procedure insert_stop_off_charge: ' || SQLERRM);
         RETURN 1;
   END insert_stop_off_charge;

   PROCEDURE transfer_accessorial_rate (
      pImportedParamId   IN import_cm_rate_discount.param_id%TYPE,
      pExistingParamId   IN import_cm_rate_discount.param_id%TYPE,
      pTCCompanyId       IN import_accessorial_param_set.tc_company_id%TYPE)
   IS
      CURSOR acc_rate
      IS
         SELECT param_id,
                accessorial_rate_id,
                tc_company_id,
                mot_id,
                equipment_id,
                service_level_id,
                protection_level_id,
                accessorial_id,
                rate,
                minimum_rate,
                currency_code,
                is_auto_approve,
                effective_dt,
                expiration_dt,
                payee_carrier_id,
                MINIMUM_SIZE,
                MAXIMUM_SIZE,
                SIZE_UOM_id,
                longest_dimension,
                second_longest_dimension,
                length_plus_grith,
                weight,
                base_amount,
                charge_per_x_field1,
                charge_per_x_field2,
                lower_range,
                higher_range,
                MAX_LONGEST_DIM,
                MIN_LONGEST_DIM,
                MAX_SEC_LONGEST_DIM,
                MIN_SEC_LONGEST_DIM,
                MAX_TRD_LONGEST_DIM,
                MIN_TRD_LONGEST_DIM,
                MAX_LENGTH_GIRTH,
                MIN_LENGTH_GIRTH,
                MAX_WEIGHT,
                MIN_WEIGHT,
                BILLING_METHOD,
                IS_ZONE_ORIGIN,
                IS_ZONE_DEST,
                IS_ZONE_STOPOFF,
                MAXIMUM_RATE,
                CUSTOMER_RATE,
                CUSTOMER_MIN_CHARGE,
                CALCULATED_RATE,
                BASE_CHARGE,
                AMOUNT,
                INCREMENT_VAL,
				ZONE_ID,
				COMMODITY_CODE_ID,
				MAX_RANGE_COMMODITY_CODE_ID
				
           FROM import_accessorial_rate
          WHERE param_id = pImportedParamId AND status = 1;

      vPassFail                    NUMBER;
      vPassFail1                   NUMBER := 0;

      vImportedAccessorialRateId   import_accessorial_rate.accessorial_rate_id%TYPE;
      vImportedMot                 import_accessorial_rate.mot_id%TYPE;
      vImportedEquipmentCode       import_accessorial_rate.equipment_id%TYPE;
      vImportedServiceLevel        import_accessorial_rate.service_level_id%TYPE;
      vImportedProtectionLevel     import_accessorial_rate.protection_level_id%TYPE;
      vImportedAccessorialCode     import_accessorial_rate.accessorial_id%TYPE;
      vImportedRate                import_accessorial_rate.rate%TYPE;
      vImportedMinimumRate         import_accessorial_rate.minimum_rate%TYPE;
      vImportedCurrencyCode        import_accessorial_rate.currency_code%TYPE;
      l_imported_minimum_size      IMPORT_ACCESSORIAL_RATE.MINIMUM_SIZE%TYPE;
      l_imported_maximum_size      IMPORT_ACCESSORIAL_RATE.MAXIMUM_SIZE%TYPE;
      l_imported_size_uom          IMPORT_ACCESSORIAL_RATE.SIZE_UOM_id%TYPE;
      vImportedAutoApprove         import_accessorial_rate.is_auto_approve%TYPE;
      vImportedEffectiveDt         import_accessorial_rate.effective_dt%TYPE;
      vImportedExpirationDt        import_accessorial_rate.expiration_dt%TYPE;
      vPayeeCarrierCode            import_accessorial_rate.payee_carrier_id%TYPE;
      vLongestDimension            import_accessorial_rate.Longest_Dimension%TYPE;
      vSecLongestDimension         import_accessorial_rate.Second_Longest_Dimension%TYPE;
      vLengthplusgrith             import_accessorial_rate.Length_Plus_Grith%TYPE;
      vWeight                      import_accessorial_rate.Weight%TYPE;
      vBaseAmount                  import_accessorial_rate.Base_Amount%TYPE;
      vChargePerXField1            import_accessorial_rate.Charge_Per_x_Field1%TYPE;
      vChargePerXField2            import_accessorial_rate.Charge_Per_x_Field2%TYPE;
      vLowerRange                  import_accessorial_rate.Lower_Range%TYPE;
      vHigherRange                 import_accessorial_rate.Higher_Range%TYPE;

      vMaxLongestDim               import_accessorial_rate.MAX_LONGEST_DIM%TYPE;
      vMinLongestDim               import_accessorial_rate.MIN_LONGEST_DIM%TYPE;
      vMaxSecLongestDim            import_accessorial_rate.MAX_SEC_LONGEST_DIM%TYPE;
      vMinSecLongestDim            import_accessorial_rate.MIN_SEC_LONGEST_DIM%TYPE;
      vMaxTrdLongestDim            import_accessorial_rate.MAX_TRD_LONGEST_DIM%TYPE;
      vMinTrdLongestDim            import_accessorial_rate.MIN_TRD_LONGEST_DIM%TYPE;
      vMaxLengthGirth              import_accessorial_rate.MAX_LENGTH_GIRTH%TYPE;
      vMinLengthGirth              import_accessorial_rate.MIN_LENGTH_GIRTH%TYPE;
      vMaxWeight                   import_accessorial_rate.MAX_WEIGHT%TYPE;
      vMinWeight                   import_accessorial_rate.MIN_WEIGHT%TYPE;
      vBillingMethod               import_accessorial_rate.BILLING_METHOD%TYPE;
      vIsZoneOrigin                import_accessorial_rate.IS_ZONE_ORIGIN%TYPE;
      vIsZoneDest                  import_accessorial_rate.IS_ZONE_DEST%TYPE;
      vIsZoneStopoff               import_accessorial_rate.IS_ZONE_STOPOFF%TYPE;
      vMaximumRate                 import_accessorial_rate.MAXIMUM_RATE%TYPE;
      vCustomerRate                import_accessorial_rate.CUSTOMER_RATE%TYPE;
      vCustomerMinCharge           import_accessorial_rate.CUSTOMER_MIN_CHARGE%TYPE;
      vCalculatedRate              import_accessorial_rate.CALCULATED_RATE%TYPE;
      vBaseCharge                  import_accessorial_rate.BASE_CHARGE%TYPE;
      vAmount                      import_accessorial_rate.AMOUNT%TYPE;
      vIncrementVal                import_accessorial_rate.INCREMENT_VAL%TYPE;
	  acc_rate_id                  import_accessorial_rate.accessorial_rate_id%TYPE;
	  paramset_id                  IMP_CARR_PARAM_ACC_INCOTERM.ACC_PARAMSET_ID%TYPE;
	  vZoneID                       import_accessorial_rate.ZONE_ID%TYPE;	  
	   -- CMGT-615 Starts here
	  vCommID				       import_accessorial_rate.COMMODITY_CODE_ID%TYPE;
	  vMaxCommId			       import_accessorial_rate.MAX_RANGE_COMMODITY_CODE_ID%TYPE;
	  	  -- CMGT-615 Ends here	
      ---Adding new Cursor
      CURSOR excluded_acc_rate
      IS
         SELECT ACCESSORIAL_ID
           FROM IMP_EXCLUDED_APS_ACC
          WHERE     param_id = PIMPORTEDPARAMID
                AND ACCESSORIAL_RATE_ID = vImportedAccessorialRateId
                AND status = 1;
   ---end

   BEGIN
      FOR c_acc_rate IN acc_rate
      LOOP
         vImportedAccessorialRateId := c_acc_rate.accessorial_rate_id;
         vImportedMot := c_acc_rate.mot_id;
         vImportedEquipmentCode := c_acc_rate.equipment_id;
         vImportedServiceLevel := c_acc_rate.service_level_id;
         vImportedProtectionLevel := c_acc_rate.protection_level_id;
         vImportedAccessorialCode := c_acc_rate.accessorial_id;
         vImportedRate := c_acc_rate.rate;
         vImportedMinimumRate := c_acc_rate.minimum_rate;
         vImportedCurrencyCode := c_acc_rate.currency_code;
         l_imported_minimum_size := c_acc_rate.MINIMUM_SIZE;
         l_imported_maximum_size := c_acc_rate.MAXIMUM_SIZE;
         l_imported_size_uom := c_acc_rate.SIZE_UOM_id;
         vImportedAutoApprove := c_acc_rate.is_auto_approve;
         vImportedEffectiveDt := c_acc_rate.effective_dt;
         vImportedExpirationDt := c_acc_rate.expiration_dt;
         vPayeeCarrierCode := c_acc_rate.payee_carrier_id;
         vLongestDimension := c_acc_rate.longest_dimension;
         vSecLongestDimension := c_acc_rate.second_longest_dimension;
         vLengthplusgrith := c_acc_rate.length_plus_grith;
         vWeight := c_acc_rate.weight;
         vBaseAmount := c_acc_rate.base_amount;
         vChargePerXField1 := c_acc_rate.charge_per_x_field1;
         vChargePerXField2 := c_acc_rate.charge_per_x_field2;
         vLowerRange := c_acc_rate.lower_range;
         vHigherRange := c_acc_rate.higher_range;
         vMaxLongestDim := c_acc_rate.MAX_LONGEST_DIM;
         vMinLongestDim := c_acc_rate.MIN_LONGEST_DIM;
         vMaxSecLongestDim := c_acc_rate.MAX_SEC_LONGEST_DIM;
         vMinSecLongestDim := c_acc_rate.MIN_SEC_LONGEST_DIM;
         vMaxTrdLongestDim := c_acc_rate.MAX_TRD_LONGEST_DIM;
         vMinTrdLongestDim := c_acc_rate.MIN_TRD_LONGEST_DIM;
         vMaxLengthGirth := c_acc_rate.MAX_LENGTH_GIRTH;
         vMinLengthGirth := c_acc_rate.MIN_LENGTH_GIRTH;
         vMaxWeight := c_acc_rate.MAX_WEIGHT;
         vMinWeight := c_acc_rate.MIN_WEIGHT;
         vBillingMethod := c_acc_rate.BILLING_METHOD;
         vIsZoneOrigin := c_acc_rate.IS_ZONE_ORIGIN;
         vIsZoneDest := c_acc_rate.IS_ZONE_DEST;
         vIsZoneStopoff := c_acc_rate.IS_ZONE_STOPOFF;
         vMaximumRate := c_acc_rate.MAXIMUM_RATE;
         vCustomerRate := c_acc_rate.CUSTOMER_RATE;
         vCustomerMinCharge := c_acc_rate.CUSTOMER_MIN_CHARGE;
         vCalculatedRate := c_acc_rate.CALCULATED_RATE;
         vBaseCharge := c_acc_rate.BASE_CHARGE;
         vAmount := c_acc_rate.AMOUNT;
         vIncrementVal := c_acc_rate.INCREMENT_VAL;
		 vZoneId       := c_acc_rate.ZONE_ID;
		vCommID        := c_acc_rate.COMMODITY_CODE_ID;
		vMaxCommId     := c_acc_rate.MAX_RANGE_COMMODITY_CODE_ID;
         /*adjust_acc_rate_eff_exp_dt(
                                                                 pExistingParamId,
                                                                 --pImportedParamId,
                                                                 --vImportedAccessorialRateId,
                                                                 pTCCompanyId,
                                                                 vImportedMot,
                                                                 vImportedEquipmentCode,
                                                                 vImportedServiceLevel,
                                                                 vImportedProtectionLevel,
                                                                 vImportedAccessorialCode,
                                                                 vImportedEffectiveDt,
                                                                 vImportedExpirationDt
                                                                 );*/

         vpassfail1 :=
            adjust_acc_rate_eff_exp_dt_1 (pexistingparamid,
                                          pimportedparamid,
                                          vimportedaccessorialrateid,
                                          ptccompanyid,
                                          vimportedmot,
                                          vimportedequipmentcode,
                                          vimportedservicelevel,
                                          vimportedprotectionlevel,
                                          vimportedaccessorialcode,
                                          vimportedeffectivedt,
                                          vimportedexpirationdt,
                                          l_imported_minimum_size,
                                          l_imported_maximum_size,
                                          l_imported_size_uom,
										  vCommID,
										  vMaxCommId); -- SYSCO MERGE 83158

         IF (vPassFail1 = 0)
         THEN
            vPassFail :=
               insert_accessorial_rate (pExistingParamId,
                                        vImportedAccessorialRateId,
                                        pTcCompanyId,
                                        vImportedMot,
                                        vImportedEquipmentCode,
                                        vImportedServiceLevel,
                                        vImportedProtectionLevel,
                                        vImportedAccessorialCode,
                                        vImportedRate,
                                        vImportedMinimumRate,
                                        vImportedCurrencyCode,
                                        l_imported_minimum_size,
                                        l_imported_maximum_size,
                                        l_imported_size_uom,
                                        vImportedAutoApprove,
                                        vImportedEffectiveDt,
                                        vImportedExpirationDt,
                                        vPayeeCarrierCode,
                                        vLongestDimension,
                                        vSecLongestDimension,
                                        vLengthplusgrith,
                                        vWeight,
                                        vBaseAmount,
                                        vChargePerXField1,
                                        vChargePerXField2,
                                        vLowerRange,
                                        vHigherRange,
                                        vMaxLongestDim,
                                        vMinLongestDim,
                                        vMaxSecLongestDim,
                                        vMinSecLongestDim,
                                        vMaxTrdLongestDim,
                                        vMinTrdLongestDim,
                                        vMaxLengthGirth,
                                        vMinLengthGirth,
                                        vMaxWeight,
                                        vMinWeight,
                                        vBillingMethod,
                                        vIsZoneOrigin,
                                        vIsZoneDest,
                                        vIsZoneStopoff,
                                        vMaximumRate,
                                        vCustomerRate,
                                        vCustomerMinCharge,
                                        vCalculatedRate,
                                        vBaseCharge,
                                        vAmount,
                                        vIncrementVal,
                                        vZoneId,
										vCommID,
										vMaxCommId);

            FOR c_excluded_acc_rate IN excluded_acc_rate
            LOOP
               INSERT INTO CARR_PARAM_ACC_EXCLUDED_LIST(ACCESSORIAL_PARAM_SET_ID,ACCESSORIAL_RATE_ID,EXCLUDED_ACC_ID)
                    VALUES (
                              PIMPORTEDPARAMID,
                              vImportedAccessorialRateId,
                              c_excluded_acc_rate.accessorial_id);
            END LOOP;
         END IF;
		
                        
        TRSFR_CARR_PRM_FEAS_INCOTERM(pTCCompanyId,PIMPORTEDPARAMID, pExistingParamId,vImportedAccessorialRateId);
		   
         --IF vPassFail = 0 THEN   -- Make sure insert into accessorial_rate was successful
         IF (vPassFail = 0)
         THEN         -- Make sure insert into accessorial_rate was successful

  		  --  DELETE FROM IMP_CARR_PARAM_ACC_INCOTERM WHERE ACC_PARAMSET_ID = PIMPORTEDPARAMID AND ACC_RATE_ID = vImportedAccessorialRateId;
		 
            DELETE FROM import_accessorial_rate
                  WHERE param_id = pImportedParamId
                        AND tc_company_id = pTCCompanyId
                        AND accessorial_rate_id =
                               c_acc_rate.accessorial_rate_id;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Procedure transfer_accessorial_rate: ' || SQLERRM);
   END transfer_accessorial_rate;

   FUNCTION insert_accessorial_param_set (
      pParamId                IN import_accessorial_param_set.param_id%TYPE,
      pTCCompanyId            IN import_accessorial_param_set.tc_company_id%TYPE,
      pCarrierCode            IN import_accessorial_param_set.carrier_id%TYPE,
      /*  pBusinessUnit          IN import_accessorial_param_set.business_unit%TYPE,*/
      pUseFakCommodity        IN import_accessorial_param_set.use_fak_commodity%TYPE,
      pCommodityClass         IN import_accessorial_param_set.commodity_class%TYPE,
      pEquipmentCode          IN import_accessorial_param_set.equipment_Id%TYPE,
      pEffectiveDt            IN import_accessorial_param_set.effective_dt%TYPE,
      pExpirationDt           IN import_accessorial_param_set.expiration_dt%TYPE,
      pCmRateType             IN import_accessorial_param_set.cm_rate_type%TYPE,
      pCmRateCurrecyCode      IN import_accessorial_param_set.cm_rate_currency_code%TYPE,
      pStopOffCurrency_code   IN import_accessorial_param_set.stop_off_currency_code%TYPE,
      pDistanceUom            IN import_accessorial_param_set.distance_uom%TYPE,
      pBudgetedApproved       IN import_accessorial_param_set.budgeted_approved%TYPE,
      pSizeUom                IN import_accessorial_param_set.size_uom_id%TYPE,
      pIsGlobal               IN import_accessorial_param_set.is_global%TYPE)
      RETURN NUMBER
   IS
      expire_dtl_sql           VARCHAR2 (1000);
      vAccessorialParamSetId   accessorial_param_set.accessorial_param_set_id%TYPE;
      vAccessorialParamId      accessorial_param_set.accessorial_param_set_id%TYPE;
      dtl_sql_cursor           ref_curtype;
      vPassFail                NUMBER := 0;
   BEGIN
      /*--------------------------------------------------------------------------------
       #
       #  Check whether all the Carrier key fields match including the effective and the
       #  expiration dates, then existing carrier parameters is expired using the
       #  effective date minus 1 as the expiration date.
       #
       #  Same Rule applies when Carrier Code is not specified and Carrier parameter is
       #  flagged as global or budgeted
       ---------------------------------------------------------------------------------*/

      expire_dtl_sql :=
            ' SELECT accessorial_param_set_id'
         || ' FROM accessorial_param_set'
         || ' WHERE tc_company_id = '
         || pTCCompanyId
         || ' AND effective_dt = '''
         || pEffectiveDt
         || ''''
         || ' AND expiration_dt = '''
         || pExpirationDt
         || '''';

      expire_dtl_sql :=
         expire_dtl_sql
         || get_resource_where_clause (pCarrierCode, pEquipmentCode);

      IF pCarrierCode IS NULL AND pIsGlobal = 1
      THEN
         expire_dtl_sql := expire_dtl_sql || ' AND is_global = 1 ';
      ELSIF pCarrierCode IS NULL AND pBudgetedApproved = 1
      THEN
         expire_dtl_sql := expire_dtl_sql || ' AND budgeted_approved = 1 ';
      END IF;

      /*  DBMS_OUTPUT.PUT_LINE('Function insert_accessorial_param_set: RESOURCE CLAUSE: ' || SUBSTR(expire_dtl_sql,1,132));
          DBMS_OUTPUT.PUT_LINE(SUBSTR(expire_dtl_sql,133,200)); */

      OPEN dtl_sql_cursor FOR expire_dtl_sql;

      LOOP
         FETCH dtl_sql_cursor INTO vAccessorialParamId;

         EXIT WHEN dtl_sql_cursor%NOTFOUND;

         DBMS_OUTPUT.PUT_LINE (
            'Function insert_accessorial_param_set: Expiring Param ID: '
            || TO_CHAR (vAccessorialParamId));
         DBMS_OUTPUT.PUT_LINE ('');
      /*UPDATE accessorial_param_set
         SET expiration_dt = effective_dt - 1
       WHERE tc_company_id = pTCCompanyId
         AND accessorial_param_set_id = vAccessorialParamId;*/

      END LOOP;

      CLOSE dtl_sql_cursor;

      -- Insert into the accessorial_param_set table

      INSERT INTO accessorial_param_set (accessorial_param_set_id,
                                         tc_company_id,
                                         carrier_id,
                                         use_fak_commodity,
                                         commodity_class,
                                         equipment_id,
                                         effective_dt,
                                         expiration_dt,
                                         cm_rate_type,
                                         cm_rate_currency_code,
                                         stop_off_currency_code,
                                         distance_uom,
                                         budgeted_approved,
                                         size_uom_id,
                                         is_global)
           VALUES (pParamId,
                   pTCCompanyId,
                   pCarrierCode,
                   pUseFakCommodity,
                   pCommodityClass,
                   pEquipmentCode,
                   pEffectiveDt,
                   pExpirationDt,
                   pCmRateType,
                   pCmRateCurrecyCode,
                   pStopOffCurrency_code,
                   pDistanceUom,
                   pBudgetedApproved,
                   pSizeUom,
                   pIsGlobal);

      UPDATE import_accessorial_param_set
         SET accessorial_param_set_id = pParamId
       WHERE param_id = pParamId AND tc_company_id = pTCCompanyId;

      adjust_eff_exp_dt (pTCCompanyId,
                         pParamId,
                         pCarrierCode,
                         /*pBusinessUnit,*/
                         pEquipmentCode,
                         pEffectiveDt,
                         pExpirationDt,
                         pBudgetedApproved,
                         pIsGlobal);

      COMMIT;

      RETURN vPassFail;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Function insert_accessorial_param_set: ' || SQLERRM);

         RETURN 1;
   END insert_accessorial_param_set;

   FUNCTION find_aps_exact_match (
      pParamId               IN import_accessorial_param_set.param_id%TYPE,
      pTCCompanyId           IN import_accessorial_param_set.tc_company_id%TYPE,
      pCarrierCode           IN import_accessorial_param_set.carrier_id%TYPE,
      /*   pBusinessUnit          IN import_accessorial_param_set.business_unit%TYPE, */
      pUseFakCommodity       IN import_accessorial_param_set.use_fak_commodity%TYPE,
      pCommodityClass        IN import_accessorial_param_set.commodity_class%TYPE,
      pEquipmentCode         IN import_accessorial_param_set.equipment_id%TYPE,
      pEffectiveDt           IN import_accessorial_param_set.effective_dt%TYPE,
      pExpirationDt          IN import_accessorial_param_set.expiration_dt%TYPE,
      pCmRateType            IN import_accessorial_param_set.cm_rate_type%TYPE,
      pCmRateCurrencyCode    IN import_accessorial_param_set.cm_rate_currency_code%TYPE,
      pStopOffCurrencyCode   IN import_accessorial_param_set.stop_off_currency_code%TYPE,
      pDistanceUom           IN import_accessorial_param_set.distance_uom%TYPE,
      pBudgetedApproved      IN import_accessorial_param_set.budgeted_approved%TYPE,
      pSizeUom               IN import_accessorial_param_set.size_uom_id%TYPE,
      pIsGlobal              IN import_accessorial_param_set.is_global%TYPE)
      -- returns the id of the exact match or NULL if none found
      RETURN accessorial_param_set.accessorial_param_set_id%TYPE
   IS
      find_exact_match_sql          VARCHAR2 (3000);
      vMatchingAccessorialParamId   accessorial_param_set.accessorial_param_set_id%TYPE;
      dtl_sql_cursor                ref_curtype;
   BEGIN
      find_exact_match_sql :=
            ' SELECT accessorial_param_set_id'
         || ' FROM accessorial_param_set'
         || ' WHERE tc_company_id = '
         || pTCCompanyId
         || ' AND effective_dt = to_date('''
         || TO_CHAR (pEffectiveDt, 'yyyy-MM-dd')
         || ''', ''yyyy-MM-dd'')'
         || ' AND expiration_dt = to_date('''
         || TO_CHAR (pExpirationDt, 'yyyy-MM-dd')
         || ''', ''yyyy-MM-dd'')';

      find_exact_match_sql :=
            find_exact_match_sql
         || ' '
         || get_rsce_wh_clause_exact_match (pCarrierCode, pEquipmentCode);

      IF pCarrierCode IS NULL AND pIsGlobal = 1
      THEN
         find_exact_match_sql := find_exact_match_sql || ' AND is_global = 1 ';
      ELSIF pCarrierCode IS NULL AND pBudgetedApproved = 1
      THEN
         find_exact_match_sql :=
            find_exact_match_sql || ' AND budgeted_approved = 1 ';
      END IF;

      OPEN dtl_sql_cursor FOR find_exact_match_sql;

      LOOP
         FETCH dtl_sql_cursor INTO vMatchingAccessorialParamId;

         EXIT WHEN dtl_sql_cursor%NOTFOUND;
         DBMS_OUTPUT.PUT_LINE (
            'Function find_accessorial_param_set_exact_match: Found exact match - Param ID: '
            || TO_CHAR (vMatchingAccessorialParamId));
      END LOOP;

      CLOSE dtl_sql_cursor;

      RETURN vMatchingAccessorialParamId;
   END find_aps_exact_match;

   PROCEDURE update_existing_aps (
      pExistingParamId               IN import_accessorial_param_set.param_id%TYPE,
      pImportedParamId               IN import_accessorial_param_set.param_id%TYPE,
      pTCCompanyId                   IN import_accessorial_param_set.tc_company_id%TYPE,
      pImportedCarrierCode           IN import_accessorial_param_set.carrier_id%TYPE,
      /*  pImportedBusinessUnit          IN import_accessorial_param_set.business_unit%TYPE,*/
      pImportedUseFakCommodity       IN import_accessorial_param_set.use_fak_commodity%TYPE,
      pImportedCommodityClass        IN import_accessorial_param_set.commodity_class%TYPE,
      pImportedEquipmentCode         IN import_accessorial_param_set.equipment_id%TYPE,
      pImportedEffectiveDt           IN import_accessorial_param_set.effective_dt%TYPE,
      pImportedExpirationDt          IN import_accessorial_param_set.expiration_dt%TYPE,
      pImportedCmRateType            IN import_accessorial_param_set.cm_rate_type%TYPE,
      pImportedCmRateCurrencyCode    IN import_accessorial_param_set.cm_rate_currency_code%TYPE,
      pImportedStopOffCurrencyCode   IN import_accessorial_param_set.stop_off_currency_code%TYPE,
      pImportedDistanceUom           IN import_accessorial_param_set.distance_uom%TYPE,
      pImportedBudgetedApproved      IN import_accessorial_param_set.budgeted_approved%TYPE,
      pImportedSizeUom               IN import_accessorial_param_set.size_uom_id%TYPE,
      pImportedIsGlobal              IN import_accessorial_param_set.is_global%TYPE)
   IS
      vExistingCmRateType   accessorial_param_set.cm_rate_type%TYPE;
      vNbCMDiscount         NUMBER;
      vNbStopOffCharge      NUMBER;
      VNBDASRSCOUNT         FLOAT;
   BEGIN
      -- select the existing cm rate type
      SELECT cm_rate_type
        INTO vExistingCmRateType
        FROM accessorial_param_set
       WHERE accessorial_param_set_id = pExistingParamId;

      SELECT COUNT (1)
        INTO VNBDASRSCOUNT
        FROM imp_das_rs
       WHERE param_id = pImportedParamId;

      -- are cm discounts imported?
      SELECT COUNT (1)
        INTO vNbCmDiscount
        FROM import_cm_rate_discount
       WHERE param_id = pImportedParamId;

      -- are stop off charges imported?
      SELECT COUNT (1)
        INTO vNbStopOffCharge
        FROM import_stop_off_charge
       WHERE param_id = pImportedParamId;

      IF (VNBDASRSCOUNT > 0)
      THEN
         DELETE FROM DELIVERY_AREA_SURCHARGE
               WHERE accessorial_param_set_id = PEXISTINGPARAMID;
      END IF;

      -- delete the existing cm rate types if the rate type has changed
      -- or if we are currently importing new ones.
      IF (vNbCmDiscount > 0) OR (vExistingCmRateType <> pImportedCmRateType)
      THEN
         DELETE FROM cm_rate_discount
               WHERE accessorial_param_set_id = pExistingParamId;
      END IF;

      -- delete the existing stop off charges
      -- if we are currently importing new ones.
      IF (vNbStopOffCharge > 0)
      THEN
         DELETE FROM stop_off_charge
               WHERE accessorial_param_set_id = pExistingParamId;
      END IF;

      -- update the current aps element:
      -- Rate Type, CM Currency, Distance UOM, Stop Off Currency, Use FAK Commodity Code,
      -- FAK Commodity Code, Weight and Is Global
      UPDATE accessorial_param_set
         SET use_fak_commodity = pImportedUseFakCommodity,
             commodity_class = pImportedCommodityClass,
             cm_rate_type = pImportedCmRateType,
             cm_rate_currency_code = pImportedCmRateCurrencyCode,
             stop_off_currency_code = pImportedStopOffCurrencyCode,
             distance_uom = pImportedDistanceUom,
             budgeted_approved = pImportedBudgetedApproved,
             size_uom_id = pImportedSizeUom,
             is_global = pImportedIsGlobal
       WHERE accessorial_param_set_id = pExistingParamId;

      --007 Need to uncomment
      IF (VNBDASRSCOUNT > 0)
      THEN
         TRANSFER_DAS_RS (PIMPORTEDPARAMID,pExistingParamId);
      END IF;

      -- then add new elements:
      -- add new cm discounts:
      IF (vNbCmDiscount > 0)
      THEN
         transfer_cm_rate_discount (pImportedParamId,
                                    pExistingParamId,
                                    pImportedCarrierCode);
      END IF;

      -- add new stop off charges
      IF (vNbStopOffCharge > 0)
      THEN
         transfer_stop_off_charge (pImportedParamId, pExistingParamId); -- Carrier Code specified
      END IF;

      -- add new accessorials
      -- and expire/adjust the dates on the existing accessorials
      transfer_accessorial_rate (pImportedParamId,
                                 pExistingParamId,
                                 pTCCompanyId);
   END update_existing_aps;

   PROCEDURE insert_carrier_parms_error (
      pTCCompanyId     IN accessorial_param_set_errors.tc_company_id%TYPE,
      pParamId         IN accessorial_param_set_errors.param_id%TYPE,
      pErrorMsgDesc    IN accessorial_param_set_errors.ERROR_MSG_DESC%TYPE,
      pSubObjectId     IN accessorial_param_set_errors.sub_object_id%TYPE,
      pSubObjectType   IN accessorial_param_set_errors.sub_object_type%TYPE)
   IS
   BEGIN
      INSERT INTO accessorial_param_set_errors (accessorial_param_set_error,
                                                tc_company_id,
                                                param_id,
                                                error_msg_desc,
                                                sub_object_id,
                                                sub_object_type)
           VALUES (acc_param_set_error_seq.NEXTVAL,
                   pTCCompanyId,
                   pParamId,
                   pErrorMsgDesc,
                   pSubObjectId,
                   pSubObjectType);

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Procedure insert_carrier_parms_error: ' || SQLERRM);
   END insert_carrier_parms_error;

   FUNCTION insert_accessorial_rate (
      pParamId               IN import_accessorial_rate.param_id%TYPE,
      pAccessorialRateId     IN import_accessorial_rate.accessorial_rate_id%TYPE,
      pTCCompanyId           IN import_accessorial_rate.tc_company_id%TYPE,
      pMot                   IN import_accessorial_rate.mot_id%TYPE,
      pEquipmentCode         IN import_accessorial_rate.equipment_id%TYPE,
      pServiceLevel          IN import_accessorial_rate.service_level_id%TYPE,
      pProtectionLevel       IN import_accessorial_rate.protection_level_id%TYPE,
      pAccessorialCode       IN import_accessorial_rate.accessorial_id%TYPE,
      pRate                  IN import_accessorial_rate.rate%TYPE,
      pMinimumRate           IN import_accessorial_rate.minimum_rate%TYPE,
      pCurrencyCode          IN import_accessorial_rate.currency_code%TYPE,
      p_minimum_size         IN IMPORT_ACCESSORIAL_RATE.MINIMUM_SIZE%TYPE,
      p_maximum_size         IN IMPORT_ACCESSORIAL_RATE.MAXIMUM_SIZE%TYPE,
      p_size_uom             IN IMPORT_ACCESSORIAL_RATE.SIZE_UOM_ID%TYPE,
      pIsAutoApprove         IN import_accessorial_rate.is_auto_approve%TYPE,
      pEffectiveDt           IN import_accessorial_rate.effective_dt%TYPE,
      pExpirationDt          IN import_accessorial_rate.expiration_dt%TYPE,
      pPayeeCarrierCode      IN import_accessorial_rate.payee_carrier_id%TYPE,
      pLongestDimension      IN import_accessorial_rate.Longest_Dimension%TYPE,
      pSecLongestDimension   IN import_accessorial_rate.Second_Longest_Dimension%TYPE,
      pLengthplusgrith       IN import_accessorial_rate.Length_Plus_Grith%TYPE,
      pWeight                IN import_accessorial_rate.Weight%TYPE,
      pBaseAmount            IN import_accessorial_rate.Base_Amount%TYPE,
      pChargePerXField1      IN import_accessorial_rate.Charge_Per_x_Field1%TYPE,
      pChargePerXField2      IN import_accessorial_rate.Charge_Per_x_Field2%TYPE,
      pLowerRange            IN import_accessorial_rate.Lower_Range%TYPE,
      pHigherRange           IN import_accessorial_rate.Higher_Range%TYPE,
      pMaxLongestDim         IN import_accessorial_rate.MAX_LONGEST_DIM%TYPE,
      pMinLongestDim         IN import_accessorial_rate.MIN_LONGEST_DIM%TYPE,
      pMaxSecLongestDim      IN import_accessorial_rate.MAX_SEC_LONGEST_DIM%TYPE,
      pMinSecLongestDim      IN import_accessorial_rate.MIN_SEC_LONGEST_DIM%TYPE,
      pMaxTrdLongestDim      IN import_accessorial_rate.MAX_TRD_LONGEST_DIM%TYPE,
      pMinTrdLongestDim      IN import_accessorial_rate.MIN_TRD_LONGEST_DIM%TYPE,
      pMaxLengthGirth        IN import_accessorial_rate.MAX_LENGTH_GIRTH%TYPE,
      pMinLengthGirth        IN import_accessorial_rate.MIN_LENGTH_GIRTH%TYPE,
      pMaxWeight             IN import_accessorial_rate.MAX_WEIGHT%TYPE,
      pMinWeight             IN import_accessorial_rate.MIN_WEIGHT%TYPE,
      pBillingMethod         IN import_accessorial_rate.BILLING_METHOD%TYPE,
      pIsZoneOrigin          IN import_accessorial_rate.IS_ZONE_ORIGIN%TYPE,
      pIsZoneDest            IN import_accessorial_rate.IS_ZONE_DEST%TYPE,
      pIsZoneStopoff         IN import_accessorial_rate.IS_ZONE_STOPOFF%TYPE,
      pMaximumRate           IN import_accessorial_rate.MAXIMUM_RATE%TYPE,
      pCustomerRate          IN import_accessorial_rate.CUSTOMER_RATE%TYPE,
      pCustomerMinCharge     IN import_accessorial_rate.CUSTOMER_MIN_CHARGE%TYPE,
      pCalculatedRate        IN import_accessorial_rate.CALCULATED_RATE%TYPE,
      pBaseCharge            IN import_accessorial_rate.BASE_CHARGE%TYPE,
      pAmount                IN import_accessorial_rate.AMOUNT%TYPE,
      pIncrementVal          IN import_accessorial_rate.INCREMENT_VAL%TYPE,
      pZoneId                IN import_accessorial_rate.ZONE_ID%TYPE,
	  vCommID				 IN import_accessorial_rate.COMMODITY_CODE_ID%TYPE, -- CMGT-615 Starts here
	  vMaxCommId			 IN import_accessorial_rate.MAX_RANGE_COMMODITY_CODE_ID%TYPE -- CMGT-615 Ends here
	  )
      RETURN NUMBER
   IS
      expire_dtl_sql           VARCHAR2 (2000);
      vAccessorialParamSetId   accessorial_rate.accessorial_param_set_id%TYPE;
      vAccessorialRateId       accessorial_rate.accessorial_rate_id%TYPE;
      dtl_sql_cursor           ref_curtype;
      vPassFail                NUMBER := 0;
   BEGIN
      DBMS_OUTPUT.PUT_LINE ('Function insert_accessorial_rate...');

      -- Insert into the accessorial_rate table

      INSERT INTO accessorial_rate (accessorial_param_set_id,
                                    accessorial_rate_id,
                                    tc_company_id,
                                    mot_id,
                                    equipment_id,
                                    service_level_id,
                                    protection_level_id,
                                    accessorial_id,
                                    rate,
                                    minimum_rate,
                                    currency_code,
                                    is_auto_approve,
                                    effective_dt,
                                    expiration_dt,
                                    payee_carrier_id,
                                    MINIMUM_SIZE,
                                    MAXIMUM_SIZE,
                                    SIZE_UOM_ID,
                                    longest_dimension,
                                    second_longest_dimension,
                                    length_plus_grith,
                                    weight,
                                    base_amount,
                                    charge_per_x_field1,
                                    charge_per_x_field2,
                                    --lower_range,
                                    RANGE_MIN_AMOUNT,
                                    --higher_range
                                    RANGE_MAX_AMOUNT,
                                    MAX_LONGEST_DIMENSION,
                                    MIN_LONGEST_DIMENSION,
                                    MAX_SECOND_LONGEST_DIMENSION,
                                    MIN_SECOND_LONGEST_DIMENSION,
                                    MAX_THIRD_LONGEST_DIMENSION,
                                    MIN_THIRD_LONGEST_DIMENSION,
                                    MAX_LENGTH_PLUS_GRITH,
                                    MIN_LENGTH_PLUS_GRITH,
                                    MAX_WEIGHT,
                                    MIN_WEIGHT,
                                    BILLING_METHOD,
                                    IS_ZONE_ORIGIN,
                                    IS_ZONE_DEST,
                                    IS_ZONE_STOPOFF,
                                    MAXIMUM_RATE,
                                    CUSTOMER_RATE,
                                    CUSTOMER_MIN_CHARGE,
                                    CALCULATED_RATE,
                                    BASE_CHARGE,
                                    AMOUNT,
                                    INCREMENT_VAL,
                                    ZONE_ID,
									COMMODITY_CODE_ID,
									MAX_RANGE_COMMODITY_CODE_ID)
           VALUES (pParamId,
                   pAccessorialRateId,
                   pTCCompanyId,
                   pMot,
                   pEquipmentCode,
                   pServiceLevel,
                   pProtectionLevel,
                   pAccessorialCode,
                   pRate,
                   pMinimumRate,
                   pCurrencyCode,
                   pIsAutoApprove,
                   pEffectiveDt,
                   pExpirationDt,
                   pPayeeCarrierCode,
                   p_minimum_size,
                   p_maximum_size,
                   p_size_uom,
                   pLongestDimension,
                   pSecLongestDimension,
                   pLengthplusgrith,
                   pWeight,
                   pBaseAmount,
                   pChargePerXField1,
                   pChargePerXField2,
                   pLowerRange,
                   pHigherRange,
                   pMaxLongestDim,
                   pMinLongestDim,
                   pMaxSecLongestDim,
                   pMinSecLongestDim,
                   pMaxTrdLongestDim,
                   pMinTrdLongestDim,
                   pMaxLengthGirth,
                   pMinLengthGirth,
                   pMaxWeight,
                   pMinWeight,
                   pBillingMethod,
                   pIsZoneOrigin,
                   pIsZoneDest,
                   pIsZoneStopoff,
                   pMaximumRate,
                   pCustomerRate,
                   pCustomerMinCharge,
                   pCalculatedRate,
                   pBaseCharge,
                   pAmount,
                   pIncrementVal,
                   pZoneId,
				   vCommID,
				   vMaxCommId);

      RETURN vPassFail;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Function insert_accessorial_rate: ' || SQLERRM);
         RETURN 1;
   END insert_accessorial_rate;

   FUNCTION get_resource_where_clause (
      pCarrierCode     IN import_accessorial_param_set.carrier_id%TYPE,
      /*    pBusinessUnit     IN import_accessorial_param_set.business_unit%TYPE, */
      pEquipmentCode   IN import_accessorial_param_set.equipment_id%TYPE)
      RETURN VARCHAR2
   IS
      resource_sql   VARCHAR2 (2000);
   BEGIN
      IF (pCarrierCode IS NOT NULL)
      THEN
         resource_sql := resource_sql || ' AND carrier_id = ' || pCarrierCode;
      ELSE
         resource_sql := resource_sql || ' AND carrier_id IS NUll ';
      END IF;

      IF (pEquipmentCode IS NOT NULL)
      THEN
         --  resource_sql := resource_sql || ' AND equipment_id IS NOT NUll ';

         resource_sql :=
            resource_sql || ' AND  equipment_id = ' || pEquipmentCode;
      ELSE
         resource_sql := resource_sql || ' AND equipment_id IS NUll  ';
      END IF;

      /*  ELSE

                     resource_sql := resource_sql || ' AND equipment_code IS NUll ';

        END IF; */

      /* IF (pBusinessUnit IS NOT NULL) THEN

                     resource_sql := resource_sql || ' AND business_unit = ''' || pBusinessUnit || ''' ';

            ELSE

                     resource_sql := resource_sql || ' AND business_unit IS NUll ';

       END IF; */

      RETURN resource_sql;
   END get_resource_where_clause;

   FUNCTION get_rsce_wh_clause_exact_match (
      pCarrierCode     IN import_accessorial_param_set.carrier_id%TYPE,
      /*   pBusinessUnit     IN import_accessorial_param_set.business_unit%TYPE, */
      pEquipmentCode   IN import_accessorial_param_set.equipment_id%TYPE)
      RETURN VARCHAR2
   IS
      resource_sql   VARCHAR2 (2000);
   BEGIN
      IF (pCarrierCode IS NOT NULL)
      THEN
         resource_sql := resource_sql || ' AND carrier_id = ' || pCarrierCode;
      ELSE
         resource_sql := resource_sql || ' AND carrier_id IS NULL ';
      END IF;

      IF (pEquipmentCode IS NOT NULL)
      THEN
         resource_sql :=
               resource_sql
            || ' AND (equipment_id = '
            || pEquipmentCode
            || ' or equipment_id IS NULL)';
      --        ELSE
      --                resource_sql := resource_sql || ' AND equipment_id IS NULL ';
      END IF;

      /*   IF (pBusinessUnit IS NOT NULL) THEN
                 resource_sql := resource_sql || ' AND business_unit = ''' || pBusinessUnit || ''' ';
         ELSE
                 resource_sql := resource_sql || ' AND business_unit IS NULL ';
         END IF; */

      RETURN resource_sql;
   END get_rsce_wh_clause_exact_match;

   FUNCTION get_acc_rate_where_clause (
      pMot               IN import_accessorial_rate.mot_id%TYPE,
      pEquipmentCode     IN import_accessorial_rate.equipment_id%TYPE,
      pServiceLevel      IN import_accessorial_rate.service_level_id%TYPE,
      pProtectionLevel   IN import_accessorial_rate.protection_level_id%TYPE,
      pAccessorialCode   IN import_accessorial_rate.accessorial_id%TYPE)
      RETURN VARCHAR2
   IS
      resource_sql   VARCHAR2 (2000);
   BEGIN
      IF (pMot IS NOT NULL)
      THEN
         resource_sql := resource_sql || ' AND mot_id = ' || pMot;
      ELSE
         resource_sql := resource_sql || ' AND mot_id IS NULL ';
      END IF;

      IF (pEquipmentCode IS NOT NULL)
      THEN
         resource_sql :=
            resource_sql || ' AND equipment_id = ' || pEquipmentCode;
      ELSE
         resource_sql := resource_sql || ' AND equipment_id IS NULL ';
      END IF;

      IF (pServiceLevel IS NOT NULL)
      THEN
         resource_sql :=
            resource_sql || ' AND service_level_id = ' || pServiceLevel;
      ELSE
         resource_sql := resource_sql || ' AND service_level_id IS NULL ';
      END IF;

      IF (pProtectionLevel IS NOT NULL)
      THEN
         resource_sql :=
            resource_sql || ' AND protection_level_id = ' || pProtectionLevel;
      ELSE
         resource_sql := resource_sql || ' AND protection_level_id IS NULL ';
      END IF;

      IF (pAccessorialCode IS NOT NULL)
      THEN
         resource_sql :=
            resource_sql || ' AND accessorial_id = ' || pAccessorialCode;
      END IF;

      RETURN resource_sql;
   END get_acc_rate_where_clause;

   --007 pending
   PROCEDURE adjust_eff_exp_dt (
      pTCCompanyId             IN accessorial_param_set.tc_company_id%TYPE,
      pAccessorialParamSetId   IN accessorial_param_set.accessorial_param_set_id%TYPE,
      pCarrierCode             IN accessorial_param_set.carrier_id%TYPE,
      /*   pBusinessUnit               IN business_unit.business_unit%TYPE, */
      pEquipmentCode           IN accessorial_param_set.equipment_id%TYPE,
      pEffectiveDt             IN accessorial_param_set.effective_dt%TYPE,
      pExpirationDt            IN accessorial_param_set.expiration_dt%TYPE,
      pBudgetedApproved        IN accessorial_param_set.budgeted_approved%TYPE,
      pIsGlobal                IN accessorial_param_set.is_global%TYPE)
   IS
      eff_exp_sql                VARCHAR2 (2000);
      eff_exp_cursor             ref_curtype;
      vExistingEffDT             accessorial_param_set.effective_dt%TYPE;
      vExistingExpDT             accessorial_param_set.expiration_dt%TYPE;
      X0                         accessorial_param_set.effective_dt%TYPE;
      Y0                         accessorial_param_set.expiration_dt%TYPE;
      X1                         accessorial_param_set.effective_dt%TYPE;
      Y1                         accessorial_param_set.expiration_dt%TYPE;
      NewEffectiveDt             accessorial_param_set.effective_dt%TYPE;
      NewExpirationDt            accessorial_param_set.effective_dt%TYPE;

      vAccessorialParamSetId     accessorial_param_set.accessorial_param_set_id%TYPE;
      newAccessorialParamSetId   accessorial_param_set.accessorial_param_set_id%TYPE;
   BEGIN
      X1 := pEffectiveDt;
      Y1 := pExpirationDt;
      COMMIT;
      DBMS_OUTPUT.PUT_LINE (
         'Procedure adjust_eff_exp_dt (Adjusting effective/expiration dates)...');

      /*eff_exp_sql :=  ' SELECT accessorial_param_set_id, effective_dt, expiration_dt ' ||
                                            ' FROM accessorial_param_set ' ||
                                                    ' WHERE tc_company_id = ' || pTCCompanyId ||
                                                    ' AND expiration_dt >= effective_dt ' ||
                                                    ' AND accessorial_param_set_id != ' ||  pAccessorialParamSetId;*/

      eff_exp_sql :=
            ' SELECT accessorial_param_set_id, effective_dt, expiration_dt'
         || ' FROM accessorial_param_set'
         || ' WHERE tc_company_id = '
         || pTCCompanyId
         || '   AND NOT ( '
         || ' effective_dt > to_date('''
         || TO_CHAR (pExpirationDt, 'yyyy-MM-dd')
         || ''', ''yyyy-MM-dd'') '
         || ' OR '
         || ' expiration_dt < to_date('''
         || TO_CHAR (pEffectiveDt, 'yyyy-MM-dd')
         || ''', ''yyyy-MM-dd'') '
         || ' ) '
         || ' AND accessorial_param_set_id != '
         || pAccessorialParamSetId;
      eff_exp_sql :=
         eff_exp_sql
         || get_rsce_wh_clause_exact_match (pCarrierCode, pEquipmentCode);

      /*-------------------------------------------------------------------------------
       # When the Carrier code is not specified and carrier parameters are flagged as global,
       # the equipment and business unit combinations are unique for overlapping dates
       ----------------------------------------------------------------------------------*/

      IF pCarrierCode IS NULL AND pIsGlobal = 1
      THEN
         eff_exp_sql := eff_exp_sql || ' AND is_global = 1 ';
      /*-------------------------------------------------------------------------------
       # When the Carrier code is not specified and carrier parameters are flagged as budgeted,
       # the equipment and business unit combinations are unique for overlapping dates
       ----------------------------------------------------------------------------------*/

      ELSIF pCarrierCode IS NULL AND pBudgetedApproved = 1
      THEN
         eff_exp_sql := eff_exp_sql || ' AND budgeted_approved = 1 ';
      END IF;

      DBMS_OUTPUT.PUT_LINE (
         'Procedure adjust_eff_exp_dt: RESOURCE CLAUSE: '
         || SUBSTR (eff_exp_sql, 1, 132));
      DBMS_OUTPUT.PUT_LINE (SUBSTR (eff_exp_sql, 133, 200));

      OPEN eff_exp_cursor FOR eff_exp_sql;

      LOOP
         FETCH eff_exp_cursor
         INTO vAccessorialParamSetId, vExistingEffDT, vExistingExpDT;

         EXIT WHEN eff_exp_cursor%NOTFOUND;

         DBMS_OUTPUT.PUT_LINE (
            'Procedure adjust_eff_exp_dt: Existing Param Id:'
            || TO_CHAR (vAccessorialParamSetId));

         X0 := vExistingEffDT;
         Y0 := vExistingExpDT;

         IF ( (X1 <= X0) AND (Y1 >= Y0))
         THEN
            /*
            -------X0------------Y0---------
            ---X1-------------------Y1------

             */
            UPDATE accessorial_param_set
               SET expiration_dt = X0 - 1
             WHERE tc_company_id = pTCCompanyId
                   AND accessorial_param_set_id = vAccessorialParamSetId;
         --DELETE_CARRIER_PARAMS(pTCCompanyId,vAccessorialParamSetId);

         ELSIF ( (X1 <= X0) AND (Y1 >= X0) AND (Y1 <= Y0))
         THEN
            /*
            -------X0------------Y0---------
            ---X1----------Y1---------------

             */
            UPDATE accessorial_param_set
               SET effective_dt = (Y1 + 1)
             WHERE tc_company_id = pTCCompanyId
                   AND accessorial_param_set_id = vAccessorialParamSetId;
         ELSIF ( (X1 >= X0) AND (X1 <= Y0) AND (Y1 >= Y0))
         THEN
            /*
            -------X0------------Y0---------
            ------------X1------------Y1----

             */

            UPDATE accessorial_param_set
               SET expiration_dt = X1 - 1
             WHERE tc_company_id = pTCCompanyId
                   AND accessorial_param_set_id = vAccessorialParamSetId;
         ELSIF ( (X1 >= X0) AND (Y1 <= Y0))
         THEN
            /*
            -------X0------------Y0---------
            -----------X1-----Y1------------

             */
            SELECT SEQ_ACCESSORIAL_PARAM_SET_ID.NEXTVAL
              INTO newAccessorialParamSetId
              FROM DUAL;

            NewEffectiveDt := Y1 + 1;
            NewExpirationDt := Y0;

            UPDATE accessorial_param_set
               SET expiration_dt = X1 - 1
             WHERE tc_company_id = pTCCompanyId
                   AND accessorial_param_set_id = vAccessorialParamSetId;

            DUPLICATE_CARRIER_PARAMS (pTCCompanyId,
                                      vAccessorialParamSetId,
                                      newAccessorialParamSetId,
                                      NewEffectiveDt,
                                      NewExpirationDt);
         END IF;
       /*
       IF ( vExistingEffDT >= pEffectiveDt) THEN

                  IF ( vExistingExpDt <= pExpirationDt) THEN

                 -- Rule 7

                 DBMS_OUTPUT.PUT_LINE('Rule 7...');

                UPDATE accessorial_param_set
                   SET expiration_dt = effective_dt - 1
                 WHERE tc_company_id = pTCCompanyId
                   AND accessorial_param_set_id = vAccessorialParamSetId;

              ELSIF ( vExistingEffDT <= pExpirationDT ) THEN

                --set eff_dt to new expiration + 1, Rule 6

                DBMS_OUTPUT.PUT_LINE('Rule 6...');

                UPDATE accessorial_param_set
                   SET effective_dt = (pExpirationDt + 1)
                 WHERE tc_company_id = pTCCompanyId
                                            AND accessorial_param_set_id = vAccessorialParamSetId;

              END IF;

       ELSIF ((vExistingExpDT >= pEffectiveDT)
                    AND (vExistingExpDT <= pExpirationDT)) THEN

                        -- set exp_dt to new effective - 1,  Rule 2

                        DBMS_OUTPUT.PUT_LINE('Rule 2...');

                UPDATE accessorial_param_set
                SET expiration_dt = (pEffectiveDt - 1)
                WHERE tc_company_id = pTCCompanyId
                        AND accessorial_param_set_id = vAccessorialParamSetId;

                 ELSIF ((vExistingEffDT <= pEffectiveDT)
                                   AND (vExistingExpDT >= pExpirationDT)) THEN

                                -- Rule 5
                                -- Set the existing expiration date to the effective date from new detail

                               DBMS_OUTPUT.PUT_LINE('Rule 5...');

                               UPDATE accessorial_param_set
                                           SET expiration_dt = (pEffectiveDT - 1)
                                          WHERE tc_company_id = pTCCompanyId
                                            AND accessorial_param_set_id = vAccessorialParamSetId;

       END IF;
*/
      END LOOP;

      CLOSE eff_exp_cursor;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE ('Procedure adjust_eff_exp_dt: ' || SQLERRM);
   END adjust_eff_exp_dt;

   PROCEDURE adjust_acc_rate_eff_exp_dt (
      pExistingAccessorialParamSetId   IN accessorial_rate.accessorial_param_set_id%TYPE,
      --pAccessorialParamSetId IN accessorial_rate.accessorial_param_set_id%TYPE,
      --pAccessorialRateId     IN accessorial_rate.accessorial_rate_id%TYPE,
      pTCCompanyId                     IN accessorial_rate.tc_company_id%TYPE,
      pMot                             IN accessorial_rate.Mot_id%TYPE,
      pEquipmentCode                   IN accessorial_rate.equipment_id%TYPE,
      pServiceLevel                    IN accessorial_rate.service_level_id%TYPE,
      pProtectionLevel                 IN accessorial_rate.protection_level_id%TYPE,
      pAccessorialCode                 IN accessorial_rate.accessorial_id%TYPE,
      pEffectiveDt                     IN accessorial_rate.effective_dt%TYPE,
      pExpirationDt                    IN accessorial_rate.expiration_dt%TYPE)
   IS
      eff_exp_sql              VARCHAR2 (2000);
      eff_exp_cursor           ref_curtype;
      vExistingEffDT           accessorial_rate.effective_dt%TYPE;
      vExistingExpDT           accessorial_rate.expiration_dt%TYPE;
      vAccessorialParamSetId   accessorial_rate.accessorial_param_set_id%TYPE;
      vAccessorialRateId       accessorial_rate.accessorial_rate_id%TYPE;

      --TT 44471
      vRATE                    accessorial_rate.RATE%TYPE;
      vMINIMUM_RATE            accessorial_rate.MINIMUM_RATE%TYPE;
      vCURRENCY_CODE           accessorial_rate.CURRENCY_CODE%TYPE;
      vMINIMUM_SIZE            accessorial_rate.MINIMUM_SIZE%TYPE;
      vMAXIMUM_SIZE            accessorial_rate.MAXIMUM_SIZE%TYPE;
      vSIZE_UOM                accessorial_rate.SIZE_UOM_ID%TYPE;
      vIS_AUTO_APPROVE         accessorial_rate.IS_AUTO_APPROVE%TYPE;
      vPAYEE_CARRIER_CODE      accessorial_rate.PAYEE_CARRIER_ID%TYPE;

      vPassFail                NUMBER;
   BEGIN
      IF (pExistingAccessorialParamSetId IS NOT NULL)
      THEN
         eff_exp_sql :=
            ' SELECT accessorial_param_set_id, accessorial_rate_id, effective_dt, expiration_dt, '
            || 'rate, minimum_rate, currency_code, minimum_size, maximum_size, size_uom_id, '
            || 'is_auto_approve, payee_carrier_id '
            || ' FROM accessorial_rate '
            || ' WHERE tc_company_id = '
            || pTCCompanyId
            || ' AND accessorial_param_set_id = '
            || pExistingAccessorialParamSetId
            || ' AND expiration_dt >= to_date('''
            || TO_CHAR (pEffectiveDt, 'yyyy-MM-dd')
            || ''', ''yyyy-MM-dd'') '
            || ' AND effective_dt <= to_date('''
            || TO_CHAR (pExpirationDt, 'yyyy-MM-dd')
            || ''', ''yyyy-MM-dd'') ';

         --DBMS_OUTPUT.PUT_LINE('eff_exp_sql::' || eff_exp_sql);

         -- just FYI: to_date('2000-01-01', 'yyyy-MM-dd')

         eff_exp_sql :=
            eff_exp_sql
            || get_acc_rate_where_clause (pMot,
                                          pEquipmentCode,
                                          pServiceLevel,
                                          pProtectionLevel,
                                          pAccessorialCode);

         --   DBMS_OUTPUT.PUT_LINE('Procedure adjust_acc_rate_eff_exp_dt: RESOURCE CLAUSE: ' || SUBSTR(eff_exp_sql,1,132));
         --   DBMS_OUTPUT.PUT_LINE(SUBSTR(eff_exp_sql,133,200));

         OPEN eff_exp_cursor FOR eff_exp_sql;

         LOOP
            FETCH eff_exp_cursor
            INTO vAccessorialParamSetId,
                 vAccessorialRateId,
                 vExistingEffDT,
                 vExistingExpDT,
                 vRATE,
                 vMINIMUM_RATE,
                 vCURRENCY_CODE,
                 vMINIMUM_SIZE,
                 vMAXIMUM_SIZE,
                 vSIZE_UOM,
                 vIS_AUTO_APPROVE,
                 vPAYEE_CARRIER_CODE;

            EXIT WHEN eff_exp_cursor%NOTFOUND;

            DBMS_OUTPUT.PUT_LINE (
               'Procedure adjust_acc_rate_eff_exp_dt: (PARAM ID, Rate_id):'
               || TO_CHAR (vAccessorialParamSetId)
               || ',  '
               || TO_CHAR (vAccessorialRateId)
               || ').');

            IF (vExistingEffDt >= pEffectiveDt)
            THEN
               IF (vExistingExpDt <= pExpirationDt)
               THEN
                  -- Rule 7

                  DBMS_OUTPUT.PUT_LINE ('Rule 7...');

                  UPDATE accessorial_rate
                     SET expiration_dt = effective_dt - 1
                   WHERE tc_company_id = pTCCompanyId
                         AND accessorial_param_set_id =
                                vAccessorialParamSetId
                         AND accessorial_rate_id = vAccessorialRateId;
               ELSIF (vExistingEffDT <= pExpirationDT)
               THEN
                  --set eff_dt to new expiration + 1, Rule 6

                  DBMS_OUTPUT.PUT_LINE ('Rule 6...');

                  UPDATE accessorial_rate
                     SET effective_dt = (pExpirationDt + 1)
                   WHERE tc_company_id = pTCCompanyId
                         AND accessorial_param_set_id =
                                vAccessorialParamSetId
                         AND accessorial_rate_id = vAccessorialRateId;
               END IF;
            ELSIF ( (vExistingExpDT >= pEffectiveDT)
                   AND (vExistingExpDT <= pExpirationDT))
            THEN
               -- set exp_dt to new effective - 1,  Rule 2

               DBMS_OUTPUT.PUT_LINE ('Rule 2...');

               UPDATE accessorial_rate
                  SET expiration_dt = (pEffectiveDt - 1)
                WHERE     tc_company_id = pTCCompanyId
                      AND accessorial_param_set_id = vAccessorialParamSetId
                      AND accessorial_rate_id = vAccessorialRateId;
            ELSIF ( (vExistingEffDT <= pEffectiveDT)
                   AND (vExistingExpDT >= pExpirationDT))
            THEN
               -- Rule 5
               -- Set the existing expiration date to the effective date - 1 from new detail

               DBMS_OUTPUT.PUT_LINE ('Rule 5...');

               UPDATE accessorial_rate
                  SET expiration_dt = (pEffectiveDT - 1)
                WHERE     tc_company_id = pTCCompanyId
                      AND accessorial_param_set_id = vAccessorialParamSetId
                      AND accessorial_rate_id = vAccessorialRateId;

               --TT 44471 -
               INSERT INTO accessorial_rate (accessorial_param_set_id,
                                             accessorial_rate_id,
                                             tc_company_id,
                                             mot_id,
                                             equipment_id,
                                             service_level_id,
                                             protection_level_id,
                                             accessorial_id,
                                             rate,
                                             minimum_rate,
                                             currency_code,
                                             is_auto_approve,
                                             effective_dt,
                                             expiration_dt,
                                             payee_carrier_id,
                                             MINIMUM_SIZE,
                                             MAXIMUM_SIZE,
                                             SIZE_UOM_ID)
                    VALUES (vAccessorialParamSetId,
                            accessorial_rate_id_seq.NEXTVAL,
                            pTCCompanyId,
                            pMot,
                            pEquipmentCode,
                            pServiceLevel,
                            pProtectionLevel,
                            pAccessorialCode,
                            vRATE,
                            vMINIMUM_RATE,
                            vCURRENCY_CODE,
                            vIS_AUTO_APPROVE,
                            (pExpirationDT + 1),
                            vExistingExpDT,
                            vPAYEE_CARRIER_CODE,
                            vMINIMUM_SIZE,
                            vMAXIMUM_SIZE,
                            vSIZE_UOM);
            END IF;
         END LOOP;

         CLOSE eff_exp_cursor;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Procedure adjust_acc_rate_eff_exp_dt: ' || SQLERRM);
   END adjust_acc_rate_eff_exp_dt;

   FUNCTION adjust_acc_rate_eff_exp_dt_1 (
      pExistingAccessorialParamSetId   IN accessorial_rate.accessorial_param_set_id%TYPE,
      pImportedParamId                 IN import_accessorial_rate.param_id%TYPE,
	  pImpAccRateId     			   IN accessorial_rate.accessorial_rate_id%TYPE,
      --pAccessorialParamSetId IN accessorial_rate.accessorial_param_set_id%TYPE,
      pTCCompanyId                     IN accessorial_rate.tc_company_id%TYPE,
      pMot                             IN accessorial_rate.Mot_id%TYPE,
      pEquipmentCode                   IN accessorial_rate.equipment_id%TYPE,
      pServiceLevel                    IN accessorial_rate.service_level_id%TYPE,
      pProtectionLevel                 IN accessorial_rate.protection_level_id%TYPE,
      pAccessorialCode                 IN accessorial_rate.accessorial_id%TYPE,
      pEffectiveDt                     IN accessorial_rate.effective_dt%TYPE,
      pExpirationDt                    IN accessorial_rate.expiration_dt%TYPE,
      vminsize                         IN accessorial_rate.minimum_size%TYPE,
      vmaxsize                         IN accessorial_rate.maximum_size%TYPE,
      vsizeuomid                       IN accessorial_rate.size_uom_id%TYPE,
	  vCommID                          IN accessorial_rate.COMMODITY_CODE_ID%TYPE,
      vMaxCommId                       IN accessorial_rate.MAX_RANGE_COMMODITY_CODE_ID%TYPE)
      RETURN INTEGER
   IS
      eff_exp_sql              VARCHAR2 (2000);
      eff_exp_cursor           ref_curtype;
      vExistingEffDT           accessorial_rate.effective_dt%TYPE;
      vExistingExpDT           accessorial_rate.expiration_dt%TYPE;
      vAccessorialParamSetId   accessorial_rate.accessorial_param_set_id%TYPE;
      vAccessorialRateId       accessorial_rate.accessorial_rate_id%TYPE;

      --TT 44471
      vRATE                    accessorial_rate.RATE%TYPE;
      vMINIMUM_RATE            accessorial_rate.MINIMUM_RATE%TYPE;
      vCURRENCY_CODE           accessorial_rate.CURRENCY_CODE%TYPE;
      vMINIMUM_SIZE            accessorial_rate.MINIMUM_SIZE%TYPE;
      vMAXIMUM_SIZE            accessorial_rate.MAXIMUM_SIZE%TYPE;
      vSIZE_UOM                accessorial_rate.SIZE_UOM_ID%TYPE;
      vIS_AUTO_APPROVE         accessorial_rate.IS_AUTO_APPROVE%TYPE;
      vPAYEE_CARRIER_CODE      accessorial_rate.PAYEE_CARRIER_ID%TYPE;

      -- 2008 enhancement
      vMaxLongestDim           accessorial_rate.MAX_LONGEST_DIMENSION%TYPE;
      vMinLongestDim           accessorial_rate.MIN_LONGEST_DIMENSION%TYPE;
      vMaxSecLongestDim        accessorial_rate.MAX_SECOND_LONGEST_DIMENSION%TYPE;
      vMinSecLongestDim        accessorial_rate.MIN_SECOND_LONGEST_DIMENSION%TYPE;
      vMaxTrdLongestDim        accessorial_rate.MAX_THIRD_LONGEST_DIMENSION%TYPE;
      vMinTrdLongestDim        accessorial_rate.MIN_THIRD_LONGEST_DIMENSION%TYPE;
      vMaxLengthGirth          accessorial_rate.MAX_LENGTH_PLUS_GRITH%TYPE;
      vMinLengthGirth          accessorial_rate.MIN_LENGTH_PLUS_GRITH%TYPE;
      vMaxWeight               accessorial_rate.MAX_WEIGHT%TYPE;
      vMinWeight               accessorial_rate.MIN_WEIGHT%TYPE;
      vBillingMethod           accessorial_rate.BILLING_METHOD%TYPE;
      vIsZoneOrigin            accessorial_rate.IS_ZONE_ORIGIN%TYPE;
      vIsZoneDest              accessorial_rate.IS_ZONE_DEST%TYPE;
      vIsZoneStopoff           accessorial_rate.IS_ZONE_STOPOFF%TYPE;
      vMaximumRate             accessorial_rate.MAXIMUM_RATE%TYPE;
      vCustomerRate            accessorial_rate.CUSTOMER_RATE%TYPE;
      vCustomerMinCharge       accessorial_rate.CUSTOMER_MIN_CHARGE%TYPE;
      vCalculatedRate          accessorial_rate.CALCULATED_RATE%TYPE;
      vBaseCharge              accessorial_rate.BASE_CHARGE%TYPE;
      vAmount                  accessorial_rate.AMOUNT%TYPE;
      vIncrementVal            accessorial_rate.INCREMENT_VAL%TYPE;
	  vzoneId                   accessorial_rate.ZONE_ID%TYPE;
	  vCommodityCode           accessorial_rate.COMMODITY_CODE_ID%TYPE;
      vMaxCommodityCode        accessorial_rate.MAX_RANGE_COMMODITY_CODE_ID%TYPE;
      vPassFail                NUMBER := 0;
	  
	  CURSOR INCOTERM_CURSOR IS
			SELECT INCOTERM_ID
			FROM IMP_CARR_PARAM_ACC_INCOTERM
			WHERE ACC_PARAMSET_ID = pImportedParamId			
			AND ACC_RATE_ID = pImpAccRateId
			AND INCOTERM_ID NOT LIKE 'NULL';
			
   BEGIN
   
      IF (pExistingAccessorialParamSetId IS NOT NULL)
      THEN
         /* eff_exp_sql :=  ' SELECT accessorial_param_set_id, accessorial_rate_id, effective_dt, expiration_dt, ' ||
                                   'rate, minimum_rate, currency_code, minimum_size, maximum_size, size_uom_id, ' ||
     'is_auto_approve, payee_carrier_id ' ||
          ' FROM accessorial_rate ' ||
                                                    ' WHERE tc_company_id = ' || pTCCompanyId ||
                                                    ' AND accessorial_param_set_id = ' || pExistingAccessorialParamSetId ||
                                                    ' AND expiration_dt >= to_date(''' || TO_CHAR(pEffectiveDt, 'yyyy-MM-dd') || ''', ''yyyy-MM-dd'') ' ||
                                                    ' AND effective_dt <= to_date(''' || TO_CHAR(pExpirationDt, 'yyyy-MM-dd') || ''', ''yyyy-MM-dd'') ' ;
          */

         eff_exp_sql :=
            'SELECT accessorial_param_set_id, accessorial_rate_id, effective_dt, expiration_dt, '
            || ' rate, minimum_rate, currency_code, minimum_size, maximum_size, size_uom_id, '
            || ' is_auto_approve, payee_carrier_id, '
            || ' MAX_LONGEST_DIMENSION, MIN_LONGEST_DIMENSION, MAX_SECOND_LONGEST_DIMENSION, MIN_SECOND_LONGEST_DIMENSION, '
            || ' MAX_THIRD_LONGEST_DIMENSION, MIN_THIRD_LONGEST_DIMENSION, MAX_LENGTH_PLUS_GRITH, MIN_LENGTH_PLUS_GRITH, MAX_WEIGHT, MIN_WEIGHT, '
            || ' BILLING_METHOD, IS_ZONE_ORIGIN, IS_ZONE_DEST, IS_ZONE_STOPOFF, MAXIMUM_RATE, CUSTOMER_RATE, CUSTOMER_MIN_CHARGE, CALCULATED_RATE, '
            || ' BASE_CHARGE, AMOUNT, INCREMENT_VAL,ZONE_ID,COMMODITY_CODE_ID,MAX_RANGE_COMMODITY_CODE_ID'
            || ' FROM accessorial_rate'
            || ' WHERE tc_company_id = '
            || pTCCompanyId
            || ' AND accessorial_param_set_id = '
            || pExistingAccessorialParamSetId
            || ' AND NOT ( '
            || ' effective_dt > to_date('''
            || TO_CHAR (pExpirationDt, 'yyyy-MM-dd')
            || ''', ''yyyy-MM-dd'') '
            || ' AND expiration_dt < to_date('''
            || TO_CHAR (pEffectiveDt, 'yyyy-MM-dd')
            || ''', ''yyyy-MM-dd'') '
            || ' ) ';

         --DBMS_OUTPUT.PUT_LINE('eff_exp_sql::' || eff_exp_sql);

         -- just FYI: to_date('2000-01-01', 'yyyy-MM-dd')

         -- BEGIN SYSCO CR 83158
         IF (vminsize IS NOT NULL AND vmaxsize IS NOT NULL)
         THEN
            eff_exp_sql :=
                  eff_exp_sql
               || ' AND ( '
               || '( '
               || vminsize
               || ' >= minimum_size and '
               || vminsize
               || ' <= maximum_size) OR '
               || '( '
               || vmaxsize
               || ' >= minimum_size and '
               || vmaxsize
               || ' <= maximum_size) OR '
               || '( minimum_size > '
               || vminsize
               || ' and minimum_size < '
               || vmaxsize
               || ')  OR '
               || '( maximum_size > '
               || vminsize
               || ' and maximum_size < '
               || vmaxsize
               || '))';
         ELSE
            eff_exp_sql :=
               eff_exp_sql
               || ' AND minimum_size is null and maximum_size is null';
         END IF;
		IF (vCommID IS NOT NULL AND vMaxCommId IS NOT NULL)
         THEN
            eff_exp_sql :=
                  eff_exp_sql
               || ' AND ( '
               || '( '
               || vCommID
               || ' >= COMMODITY_CODE_ID and '
               || vCommID
               || ' <= MAX_RANGE_COMMODITY_CODE_ID) OR '
               || '( '
               || vMaxCommId
               || ' >= COMMODITY_CODE_ID and '
               || vMaxCommId
               || ' <= MAX_RANGE_COMMODITY_CODE_ID) OR '
               || '( COMMODITY_CODE_ID > '
               || vCommID
               || ' and COMMODITY_CODE_ID < '
               || vMaxCommId
               || ')  OR '
               || '( MAX_RANGE_COMMODITY_CODE_ID > '
               || vCommID
               || ' and MAX_RANGE_COMMODITY_CODE_ID < '
               || vMaxCommId
               || '))';
         ELSE
            eff_exp_sql :=
               eff_exp_sql
               || ' AND COMMODITY_CODE_ID is null and MAX_RANGE_COMMODITY_CODE_ID is null';
         END IF;
         IF (vsizeuomid IS NOT NULL)
         THEN
            eff_exp_sql := eff_exp_sql || ' AND size_uom_id = ' || vsizeuomid;
         ELSE
            eff_exp_sql := eff_exp_sql || ' AND size_uom_id is null';
         END IF;

         -- END SYSCO MERGE 83158

         eff_exp_sql :=
            eff_exp_sql
            || get_acc_rate_where_clause (pMot,
                                          pEquipmentCode,
                                          pServiceLevel,
                                          pProtectionLevel,
                                          pAccessorialCode);

         --   DBMS_OUTPUT.PUT_LINE('Procedure adjust_acc_rate_eff_exp_dt: RESOURCE CLAUSE: ' || SUBSTR(eff_exp_sql,1,132));
         --   DBMS_OUTPUT.PUT_LINE(SUBSTR(eff_exp_sql,133,200));
         OPEN eff_exp_cursor FOR eff_exp_sql;

         LOOP
            FETCH eff_exp_cursor
            INTO vAccessorialParamSetId,
                 vAccessorialRateId,
                 vExistingEffDT,
                 vExistingExpDT,
                 vRATE,
                 vMINIMUM_RATE,
                 vCURRENCY_CODE,
                 vMINIMUM_SIZE,
                 vMAXIMUM_SIZE,
                 vSIZE_UOM,
                 vIS_AUTO_APPROVE,
                 vPAYEE_CARRIER_CODE,
                 vMaxLongestDim,
                 vMinLongestDim,
                 vMaxSecLongestDim,
                 vMinSecLongestDim,
                 vMaxTrdLongestDim,
                 vMinTrdLongestDim,
                 vMaxLengthGirth,
                 vMinLengthGirth,
                 vMaxWeight,
                 vMinWeight,
                 vBillingMethod,
                 vIsZoneOrigin,
                 vIsZoneDest,
                 vIsZoneStopoff,
                 vMaximumRate,
                 vCustomerRate,
                 vCustomerMinCharge,
                 vCalculatedRate,
                 vBaseCharge,
                 vAmount,
                 vIncrementVal,
				 vZoneId,
                 vCommodityCode,
                 vMaxCommodityCode;

            EXIT WHEN eff_exp_cursor%NOTFOUND;

            DBMS_OUTPUT.PUT_LINE (
               'Procedure adjust_acc_rate_eff_exp_dt: (PARAM ID, Rate_id):'
               || TO_CHAR (vAccessorialParamSetId)
               || ',  '
               || TO_CHAR (vAccessorialRateId)
               || ').');

            IF (vExistingEffDt = pEffectiveDt
                AND vExistingExpDt = pExpirationDt)
            THEN
               vPassFail := 1;

               UPDATE accessorial_rate AR
                  SET (AR.rate,
                       AR.minimum_rate,
                       AR.currency_code,
                       AR.MINIMUM_SIZE,
                       AR.MAXIMUM_SIZE,
                       AR.SIZE_UOM_id,
                       AR.is_auto_approve,
                       AR.payee_carrier_id,
                       AR.MAX_LONGEST_DIMENSION,
                       AR.MIN_LONGEST_DIMENSION,
                       AR.MAX_SECOND_LONGEST_DIMENSION,
                       AR.MIN_SECOND_LONGEST_DIMENSION,
                       AR.MAX_THIRD_LONGEST_DIMENSION,
                       AR.MIN_THIRD_LONGEST_DIMENSION,
                       AR.MAX_LENGTH_PLUS_GRITH,
                       AR.MIN_LENGTH_PLUS_GRITH,
                       AR.MAX_WEIGHT,
                       AR.MIN_WEIGHT,
                       AR.BILLING_METHOD,
                       AR.IS_ZONE_ORIGIN,
                       AR.IS_ZONE_DEST,
                       AR.IS_ZONE_STOPOFF,
                       AR.MAXIMUM_RATE,
                       AR.CUSTOMER_RATE,
                       AR.CUSTOMER_MIN_CHARGE,
                       AR.CALCULATED_RATE,
                       AR.BASE_CHARGE,
                       AR.AMOUNT,
					   AR.INCREMENT_VAL,AR.ZONE_ID,
                       AR.COMMODITY_CODE_ID,
                       AR.MAX_RANGE_COMMODITY_CODE_ID) =
                         (SELECT IAR.RATE,
                                 IAR.MINIMUM_RATE,
                                 IAR.CURRENCY_CODE,
                                 IAR.MINIMUM_SIZE,
                                 IAR.MAXIMUM_SIZE,
                                 IAR.SIZE_UOM_ID,
                                 IAR.IS_AUTO_APPROVE,
                                 IAR.PAYEE_CARRIER_ID,
                                 IAR.MAX_LONGEST_DIM,
                                 IAR.MIN_LONGEST_DIM,
                                 IAR.MAX_SEC_LONGEST_DIM,
                                 IAR.MIN_SEC_LONGEST_DIM,
                                 IAR.MAX_TRD_LONGEST_DIM,
                                 IAR.MIN_TRD_LONGEST_DIM,
                                 IAR.MAX_LENGTH_GIRTH,
                                 IAR.MIN_LENGTH_GIRTH,
                                 IAR.MAX_WEIGHT,
                                 IAR.MIN_WEIGHT,
                                 IAR.BILLING_METHOD,
                                 IAR.IS_ZONE_ORIGIN,
                                 IAR.IS_ZONE_DEST,
                                 IAR.IS_ZONE_STOPOFF,
                                 IAR.MAXIMUM_RATE,
                                 IAR.CUSTOMER_RATE,
                                 IAR.CUSTOMER_MIN_CHARGE,
                                 IAR.CALCULATED_RATE,
                                 IAR.BASE_CHARGE,
                                 IAR.AMOUNT,
                                 IAR.INCREMENT_VAL,IAR.ZONE_ID,
                                 IAR.COMMODITY_CODE_ID,
								 IAR.MAX_RANGE_COMMODITY_CODE_ID
                            FROM IMPORT_ACCESSORIAL_RATE IAR
                           WHERE IAR.TC_COMPANY_ID = pTCCompanyId
                                 AND IAR.PARAM_ID = pImportedParamId
								 AND IAR.accessorial_rate_id = pImpAccRateId )
                WHERE AR.tc_company_id = pTCCompanyId
                      AND AR.accessorial_param_set_id =
                             vAccessorialParamSetId
                      AND AR.accessorial_rate_id = vAccessorialRateId;
			
		-- TRANSFER INCOTERM		
			DELETE FROM CARR_PRM_ACC_FEASIBLE_INCOTERM WHERE ACC_PARAMSET_ID = vAccessorialParamSetId AND ACC_RATE_ID = vAccessorialRateId;
		
			FOR INCOTERM_CUR IN INCOTERM_CURSOR
			LOOP
				  INSERT INTO CARR_PRM_ACC_FEASIBLE_INCOTERM VALUES(vAccessorialParamSetId, vAccessorialRateId, INCOTERM_CUR.INCOTERM_ID);          
			END LOOP;
		-- TRANSFER INCOTERM END	
					  
            ELSIF (vExistingEffDt >= pEffectiveDt)
            THEN
               IF (vExistingExpDt <= pExpirationDt)
               THEN
                  -- Rule 7
                  DBMS_OUTPUT.PUT_LINE ('Rule 7...');

                  UPDATE accessorial_rate
                     SET expiration_dt = effective_dt - 1
                   WHERE tc_company_id = pTCCompanyId
                         AND accessorial_param_set_id =
                                vAccessorialParamSetId
                         AND accessorial_rate_id = vAccessorialRateId;
               ELSIF (vExistingEffDT <= pExpirationDT)
               THEN
                  --set eff_dt to new expiration + 1, Rule 6
                  DBMS_OUTPUT.PUT_LINE ('Rule 6...');

                  UPDATE accessorial_rate
                     SET effective_dt = (pExpirationDt + 1)
                   WHERE tc_company_id = pTCCompanyId
                         AND accessorial_param_set_id =
                                vAccessorialParamSetId
                         AND accessorial_rate_id = vAccessorialRateId;
               END IF;
            ELSIF ( (vExistingExpDT >= pEffectiveDT)
                   AND (vExistingExpDT <= pExpirationDT))
            THEN
               -- set exp_dt to new effective - 1,  Rule 2
               DBMS_OUTPUT.PUT_LINE ('Rule 2...');

               UPDATE accessorial_rate
                  SET expiration_dt = (pEffectiveDt - 1)
                WHERE     tc_company_id = pTCCompanyId
                      AND accessorial_param_set_id = vAccessorialParamSetId
                      AND accessorial_rate_id = vAccessorialRateId;
            ELSIF ( (vExistingEffDT <= pEffectiveDT)
                   AND (vExistingExpDT >= pExpirationDT))
            THEN
               -- Rule 5
               -- Set the existing expiration date to the effective date - 1 from new detail

               DBMS_OUTPUT.PUT_LINE ('Rule 5...');

               UPDATE accessorial_rate
                  SET expiration_dt = (pEffectiveDT - 1)
                WHERE     tc_company_id = pTCCompanyId
                      AND accessorial_param_set_id = vAccessorialParamSetId
                      AND accessorial_rate_id = vAccessorialRateId;

               --TT 44471 -
               INSERT INTO accessorial_rate (accessorial_param_set_id,
                                             accessorial_rate_id,
                                             tc_company_id,
                                             mot_id,
                                             equipment_id,
                                             service_level_id,
                                             protection_level_id,
                                             accessorial_id,
                                             rate,
                                             minimum_rate,
                                             currency_code,
                                             is_auto_approve,
                                             effective_dt,
                                             expiration_dt,
                                             payee_carrier_id,
                                             MINIMUM_SIZE,
                                             MAXIMUM_SIZE,
                                             SIZE_UOM_ID,
                                             MAX_LONGEST_DIMENSION,
                                             MIN_LONGEST_DIMENSION,
                                             MAX_SECOND_LONGEST_DIMENSION,
                                             MIN_SECOND_LONGEST_DIMENSION,
                                             MAX_THIRD_LONGEST_DIMENSION,
                                             MIN_THIRD_LONGEST_DIMENSION,
                                             MAX_LENGTH_PLUS_GRITH,
                                             MIN_LENGTH_PLUS_GRITH,
                                             MAX_WEIGHT,
                                             MIN_WEIGHT,
                                             BILLING_METHOD,
                                             IS_ZONE_ORIGIN,
                                             IS_ZONE_DEST,
                                             IS_ZONE_STOPOFF,
                                             MAXIMUM_RATE,
                                             CUSTOMER_RATE,
                                             CUSTOMER_MIN_CHARGE,
                                             CALCULATED_RATE,
                                             BASE_CHARGE,
                                             AMOUNT,
                                             INCREMENT_VAL,ZONE_ID,
											 COMMODITY_CODE_ID,
											 MAX_RANGE_COMMODITY_CODE_ID)
                    VALUES (vAccessorialParamSetId,
                            accessorial_rate_id_seq.NEXTVAL,
                            pTCCompanyId,
                            pMot,
                            pEquipmentCode,
                            pServiceLevel,
                            pProtectionLevel,
                            pAccessorialCode,
                            vRATE,
                            vMINIMUM_RATE,
                            vCURRENCY_CODE,
                            vIS_AUTO_APPROVE,
                            (pExpirationDT + 1),
                            vExistingExpDT,
                            vPAYEE_CARRIER_CODE,
                            vMINIMUM_SIZE,
                            vMAXIMUM_SIZE,
                            vSIZE_UOM,
                            vMaxLongestDim,
                            vMinLongestDim,
                            vMaxSecLongestDim,
                            vMinSecLongestDim,
                            vMaxTrdLongestDim,
                            vMinTrdLongestDim,
                            vMaxLengthGirth,
                            vMinLengthGirth,
                            vMaxWeight,
                            vMinWeight,
                            vBillingMethod,
                            vIsZoneOrigin,
                            vIsZoneDest,
                            vIsZoneStopoff,
                            vMaximumRate,
                            vCustomerRate,
                            vCustomerMinCharge,
                            vCalculatedRate,
                            vBaseCharge,
                            vAmount,
                            vIncrementVal,vZoneId,
							vCommodityCode,
                          	vMaxCommodityCode);
            END IF;
         END LOOP;

         CLOSE eff_exp_cursor;
      END IF;

      RETURN vPassFail;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Procedure adjust_acc_rate_eff_exp_dt: ' || SQLERRM);
   END adjust_acc_rate_eff_exp_dt_1;

   PROCEDURE test_find_aps_exact_match (
      pParamId       IN import_accessorial_param_set.param_id%TYPE,
      pTCCompanyId   IN import_accessorial_param_set.tc_company_id%TYPE)
   IS
      CURSOR carrier_list
      IS
         SELECT i.accessorial_param_set_id,
                i.carrier_id,
                i.use_fak_commodity,
                i.commodity_class,
                i.equipment_id,
                i.effective_dt,
                i.expiration_dt,
                i.cm_rate_type,
                i.cm_rate_currency_code,
                i.stop_off_currency_code,
                i.distance_uom,
                i.budgeted_approved,
                i.size_uom_id,
                i.is_global
           FROM accessorial_param_set i
          WHERE i.tc_company_id = pTcCompanyId
                AND i.accessorial_param_set_id = pParamId;

      vIndex              NUMBER;
      vExistsExactMatch   accessorial_param_set.accessorial_param_set_id%TYPE;
   BEGIN
      vIndex := 0;

      FOR c_carrier_list IN carrier_list
      LOOP
         vIndex := vIndex + 1;

         vExistsExactMatch :=
            find_aps_exact_match (pParamId,
                                  pTcCompanyId,
                                  c_carrier_list.carrier_id,
                                  /*  null, */
                                  c_carrier_list.use_fak_commodity,
                                  c_carrier_list.commodity_class,
                                  c_carrier_list.equipment_id,
                                  c_carrier_list.effective_dt,
                                  c_carrier_list.expiration_dt,
                                  c_carrier_list.cm_rate_type,
                                  c_carrier_list.cm_rate_currency_code,
                                  c_carrier_list.stop_off_currency_code,
                                  c_carrier_list.distance_uom,
                                  c_carrier_list.budgeted_approved,
                                  c_carrier_list.size_uom_id,
                                  c_carrier_list.is_global);
         DBMS_OUTPUT.PUT_LINE (
               'Procedure test_find_aps_exact_match called for '
            || pParamId
            || ' ('
            || vIndex
            || ') : match='
            || vExistsExactMatch);
      END LOOP;
   END test_find_aps_exact_match;

   PROCEDURE DUPLICATE_CARRIER_PARAMS (
      pTcCompnayId             IN accessorial_param_set.TC_COMPANY_ID%TYPE,
      pAccessorialParamSetId   IN accessorial_param_set.accessorial_param_set_id%TYPE,
      vNewParamSetId           IN accessorial_param_set.accessorial_param_set_id%TYPE,
      NewEffectiveDt           IN accessorial_param_set.effective_dt%TYPE,
      NewExpirationDt          IN accessorial_param_set.expiration_dt%TYPE)
   AS
      NewAccRateId   NUMBER;
   BEGIN
      --  copy    accessorial_param_set records
      INSERT INTO accessorial_param_set (accessorial_param_set_id,
                                         tc_company_id,
                                         carrier_id,
                                         use_fak_commodity,
                                         commodity_class,
                                         equipment_id,
                                         effective_dt,
                                         expiration_dt,
                                         cm_rate_type,
                                         cm_rate_currency_code,
                                         stop_off_currency_code,
                                         distance_uom,
                                         budgeted_approved,
                                         size_uom_id,
                                         is_global)
         SELECT vNewParamSetId,
                tc_company_id,
                carrier_id,
                use_fak_commodity,
                commodity_class,
                equipment_id,
                NewEffectiveDt,
                NewExpirationDt,
                cm_rate_type,
                cm_rate_currency_code,
                stop_off_currency_code,
                distance_uom,
                budgeted_approved,
                size_uom_id,
                is_global
           FROM accessorial_param_set
          WHERE tc_company_id = pTcCompnayId
                AND accessorial_param_set_id = pAccessorialParamSetId;

      COMMIT;

      ---copy delivery_area_surcharge records
      INSERT INTO DELIVERY_AREA_SURCHARGE (DELIVERY_AREA_SURCHARGE_ID,
                                           SERVICE_LEVEL_ID,
                                           DELIVERY_AREA_SURCHG_TYPE_ID,
                                           CHARGE,
                                           TC_COMPANY_ID,
                                           CHARGE_CURR_CODE,
                                           ZONE_ID,
                                           ACCESSORIAL_PARAM_SET_ID,
                                           ACCESSORIAL_ID)
         SELECT DELIVERY_AREA_SURCHARGE_ID_SEQ.NEXTVAL,
                SERVICE_LEVEL_ID,
                DELIVERY_AREA_SURCHG_TYPE_ID,
                CHARGE,
                TC_COMPANY_ID,
                CHARGE_CURR_CODE,
                ZONE_ID,
                ACCESSORIAL_PARAM_SET_ID,
                ACCESSORIAL_ID
           FROM DELIVERY_AREA_SURCHARGE
          WHERE accessorial_param_set_id = pAccessorialParamSetId;

      COMMIT;

      --  copy    cm_rate_discount records
      INSERT INTO cm_rate_discount (accessorial_param_set_id,
                                    cm_rate_discount_id,
                                    start_number,
                                    end_number,
                                    VALUE)
         SELECT vNewParamSetId,
                cm_rate_discount_id_seq.NEXTVAL,
                start_number,
                end_number,
                VALUE
           FROM cm_rate_discount cm2
          WHERE cm2.accessorial_param_set_id = pAccessorialParamSetId;

      COMMIT;

      --  copy    stop_off_charge records
      INSERT INTO stop_off_charge (accessorial_param_set_id,
                                   stop_off_charge_id,
                                   start_number,
                                   end_number,
                                   VALUE)
         SELECT vNewParamSetId,
                stop_off_charge_id_seq.NEXTVAL,
                start_number,
                end_number,
                VALUE
           FROM stop_off_charge
          WHERE accessorial_param_set_id = pAccessorialParamSetId;

      COMMIT;

      SELECT accessorial_rate_id_seq.NEXTVAL INTO NewAccRateId FROM DUAL;

      --  copy    accessorial_rate records
      INSERT INTO accessorial_rate (accessorial_param_set_id,
                                    accessorial_rate_id,
                                    tc_company_id,
                                    mot_id,
                                    equipment_id,
                                    service_level_id,
                                    protection_level_id,
                                    accessorial_id,
                                    rate,
                                    minimum_rate,
                                    currency_code,
                                    is_auto_approve,
                                    effective_dt,
                                    expiration_dt,
                                    payee_carrier_id,
                                    MINIMUM_SIZE,
                                    MAXIMUM_SIZE,
                                    SIZE_UOM_ID)
         SELECT vNewParamSetId,
                NewAccRateId,
                tc_company_id,
                mot_id,
                equipment_id,
                service_level_id,
                protection_level_id,
                accessorial_id,
                rate,
                minimum_rate,
                currency_code,
                is_auto_approve,
                effective_dt,
                expiration_dt,
                payee_carrier_id,
                MINIMUM_SIZE,
                MAXIMUM_SIZE,
                SIZE_UOM_ID
           FROM accessorial_rate
          WHERE tc_company_id = pTcCompnayId
                AND accessorial_param_set_id = pAccessorialParamSetId;

      COMMIT;

      INSERT INTO CARR_PARAM_ACC_EXCLUDED_LIST
         SELECT *
           FROM CARR_PARAM_ACC_EXCLUDED_LIST
          WHERE accessorial_param_set_id = pAccessorialParamSetId
                AND accessorial_rate_id = NewAccRateId;

      COMMIT;
   --return vNewParamSetId;
   END DUPLICATE_CARRIER_PARAMS;

   PROCEDURE DELETE_CARRIER_PARAMS (
      pTcCompnayId             IN accessorial_param_set.TC_COMPANY_ID%TYPE,
      pAccessorialParamSetId   IN accessorial_param_set.accessorial_param_set_id%TYPE)
   AS
   BEGIN
      DELETE FROM cm_rate_discount
            WHERE accessorial_param_set_id = pAccessorialParamSetId;

      DELETE FROM stop_off_charge
            WHERE accessorial_param_set_id = pAccessorialParamSetId;

      DELETE FROM accessorial_rate
            WHERE tc_company_id = pTcCompnayId
                  AND accessorial_param_set_id = pAccessorialParamSetId;

      DELETE FROM accessorial_param_set
            WHERE tc_company_id = pTcCompnayId
                  AND accessorial_param_set_id = pAccessorialParamSetId;
   END DELETE_CARRIER_PARAMS;
   
PROCEDURE VALIDT_CARR_PRM_FEAS_INCOTERM
 ( 
   vTcCompantId IN COMPANY.COMPANY_ID%TYPE,
   vParamSetId IN IMP_CARR_PARAM_ACC_INCOTERM.ACC_PARAMSET_ID%TYPE,
   vAccRateId IN IMP_CARR_PARAM_ACC_INCOTERM.ACC_RATE_ID%TYPE,
   vFail OUT NUMBER
 ) IS
 
    vPassFail NUMBER;
    vCount NUMBER;
    vIncotermName INCOTERM.INCOTERM_NAME%TYPE;
    vIncotermId INCOTERM.INCOTERM_ID%TYPE;

    CURSOR CARR_PRM_ACC_INCOTERM_CURSOR IS 
      SELECT INCOTERM_ID
       FROM IMP_CARR_PARAM_ACC_INCOTERM
       WHERE ACC_PARAMSET_ID = vParamSetId 
       AND ACC_RATE_ID = vAccRateId;

   BEGIN

    OPEN CARR_PRM_ACC_INCOTERM_CURSOR;
      <<CAR_PRM_INCOTERM_LOOP>>
      LOOP
      vPassFail := 0;
      FETCH CARR_PRM_ACC_INCOTERM_CURSOR INTO vIncotermId;
      EXIT CAR_PRM_INCOTERM_LOOP WHEN CARR_PRM_ACC_INCOTERM_CURSOR%NOTFOUND;
          
          SELECT COUNT(INCOTERM_ID) INTO vCount FROM INCOTERM WHERE INCOTERM_ID = vIncotermId;
          
          IF(vCount = 0) THEN
				     vPassFail := 1;
				     INSERT_CARRIER_PARMS_ERROR
                                (
                                        vTcCompantId,
                                        vParamSetId,
                                        'INCOTERM NAME IS INVALID',
                                        vAccRateId,
                                        'iea'
                                );
         -- ELSE
         -- SELECT INCOTERM_ID INTO vIncotermId FROM INCOTERM WHERE INCOTERM_NAME = vIncotermName;
         -- UPDATE IMP_CARR_PARAM_ACC_INCOTERM SET INCOTERM_ID = vIncotermId WHERE
         --           ACC_PARAMSET_ID = vParamSetId			                    
         --           AND ACC_RATE_ID = vAccRateId
         --           AND INCOTERM_NAME = vIncotermName;
 		      END IF;          
      END LOOP CAR_PRM_INCOTERM_LOOP;
	  vFail := vPassFail;
END VALIDT_CARR_PRM_FEAS_INCOTERM;

PROCEDURE TRSFR_CARR_PRM_FEAS_INCOTERM
(	
   vTCCompanyId			IN COMPANY.COMPANY_ID%TYPE,
   vImpParamSetId IN IMP_CARR_PARAM_ACC_INCOTERM.ACC_PARAMSET_ID%TYPE,
   vActualParamSetId IN IMP_CARR_PARAM_ACC_INCOTERM.ACC_PARAMSET_ID%TYPE,
   vAccRateId IN import_accessorial_rate.ACCESSORIAL_RATE_ID%TYPE
) AS
  vCount        NUMBER;
  vIncotermId IMP_CARR_PARAM_ACC_INCOTERM.INCOTERM_ID%TYPE;
    
  CURSOR INCOTERM_CURSOR IS
			SELECT INCOTERM_ID
			FROM IMP_CARR_PARAM_ACC_INCOTERM
			WHERE ACC_PARAMSET_ID = vImpParamSetId			
			AND ACC_RATE_ID = vAccRateId
      AND INCOTERM_ID NOT LIKE 'NULL';
      
  BEGIN
  
      OPEN INCOTERM_CURSOR;
      <<INCOTERM_LOOP>>
      LOOP
      FETCH INCOTERM_CURSOR INTO vIncotermId;
      EXIT INCOTERM_LOOP WHEN INCOTERM_CURSOR%NOTFOUND;          
          INSERT INTO CARR_PRM_ACC_FEASIBLE_INCOTERM VALUES(vActualParamSetId, vAccRateId, vIncotermId);
          DELETE FROM IMP_CARR_PARAM_ACC_INCOTERM WHERE ACC_PARAMSET_ID = vImpParamSetId AND ACC_RATE_ID = vAccRateId and INCOTERM_ID = vIncotermId;          
      END LOOP INCOTERM_LOOP;
END TRSFR_CARR_PRM_FEAS_INCOTERM;
   
END Import_Carrier_Parms_Pkg;
/



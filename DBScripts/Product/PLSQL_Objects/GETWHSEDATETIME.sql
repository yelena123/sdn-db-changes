CREATE OR REPLACE FUNCTION GETWHSEDATETIME (CLOCK_IN_DATE    DATE,
                                            p_WHSE             VARCHAR2)
   RETURN DATE
   
IS
   WHSE_DATE       DATE;
   RETURN_DATE     DATE;
   FACILITY_DT     DATE;
   CLOCK_IN_TEMP   DATE;
   V_TZ_NAME       VARCHAR2 (100);
   RESULT          INT;
BEGIN
   SELECT DAY_BEGIN_TIME
     INTO WHSE_DATE
     FROM WHSE_LABOR_PARAMETERS
    WHERE FACILITY_ID = (SELECT FACILITY_ID
                           FROM FACILITY
                          WHERE WHSE = p_WHSE);

   RESULT :=
      COMPARETIMECOMPONET (CLOCK_IN_DATE, WHSE_DATE,  p_WHSE);
   V_TZ_NAME := FN_GET_TZ_NAME ( p_WHSE);

   IF RESULT >= 0
   THEN
      RETURN_DATE := CLOCK_IN_DATE;
   ELSE
      -- convert to facility timezone.
      CLOCK_IN_TEMP :=
         FN_TZ_CONVERT_WITH_FROM_TZ (CLOCK_IN_DATE,
                                     FN_GET_TIMEZONE (),
                                     V_TZ_NAME);

      RETURN_DATE := (CLOCK_IN_TEMP - 1);
      -- convert back to application timezone.
      RETURN_DATE :=
         FN_TZ_CONVERT_WITH_FROM_TZ (RETURN_DATE,
                                     V_TZ_NAME,
                                     FN_GET_TIMEZONE ());
   END IF;


   -- RETURN_DATE :=FN_TZ_CONVERT_WITH_FROM_TZ (RETURN_DATE,
   --                                V_TZ_NAME,
   --                                FN_GET_TIMEZONE ());

   --date operation carried out in facility timezone.
   FACILITY_DT :=
      TO_DATE (
         (   FN_LM_TOCHAR (RETURN_DATE,  p_WHSE, 'YYYY-MM-DD')
          || ' '
          || FN_LM_TOCHAR (WHSE_DATE,  p_WHSE, 'HH24:MI:SS')),
         'YYYY-MM-DD HH24:MI:SS');

   --convert the date back to application timezone.
   RETURN FACILITY_DT;
END;
/
create or replace procedure manh_wave_refresh_sku_invn

(
    p_facility_id    in facility.facility_id%type,
    p_whse           in facility.whse%type,
    p_sku_sub        in wave_parm.sku_sub%type,
    p_pick_wave_nbr  in ship_wave_parm.ship_wave_nbr%type,
    p_scale          in number,
    p_rnd_threshold  in number
)
as
begin
    insert into tmp_wave_sku_invn_cache
    (
        item_id, invn_type, prod_stat, batch_nbr, item_attr_1, item_attr_2,
        item_attr_3, item_attr_4, item_attr_5, cntry_of_orgn, qty_available
    )
    with giv as
    (
        select t.item_id item_id
        from tmp_wave_selected_orders t
        union
        select t.item_id item_id
        from tmp_wave_prepack_components t
        union
        select isdc.sub_item_id item_id
        from item_substitution_dtl_cbo isdc
        join item_substitution_cbo isc
            on isc.item_sub_hdr_id = isdc.item_sub_hdr_id
        join tmp_wave_selected_orders t on t.item_id = isc.base_item_id
        where p_sku_sub in ('1', '2')
    )
    select iv.item_id, iv.invn_type, iv.prod_stat, iv.batch_nbr,
        iv.item_attr_1, iv.item_attr_2, iv.item_attr_3, iv.item_attr_4,
        iv.item_attr_5, iv.cntry_of_orgn,
        round(sum(iv.qty_available), p_scale)
    from
    (
        select wi.item_id, wi.inventory_type invn_type,
               wi.product_status prod_stat, wi.batch_nbr batch_nbr, 
               wi.item_attr_1 item_attr_1,  wi.item_attr_2 item_attr_2,
               wi.item_attr_3 item_attr_3,  wi.item_attr_4 item_attr_4, 
               wi.item_attr_5 item_attr_5,  wi.cntry_of_orgn cntry_of_orgn, 
            (wi.on_hand_qty - wi.wm_allocated_qty + wi.to_be_filled_qty) qty_available
        from wm_inventory wi
        join giv on giv.item_id = wi.item_id
        left join pick_locn_dtl pld on pld.pick_locn_dtl_id = wi.location_dtl_id
        left join locn_hdr on locn_hdr.locn_id = wi.location_id  
        where wi.c_facility_id = p_facility_id
            and coalesce(wi.allocatable, 'Y') = 'Y'
            and wi.marked_for_delete = 'N'
            and wi.transitional_inventory_type is null
            and pld.pikng_lock_code is null
            and locn_hdr.whse = p_whse 
            and ((wi.locn_class='R' and locn_hdr.pull_zone is not null) or (wi.locn_class <> 'R'))
            and (wi.on_hand_qty - wi.wm_allocated_qty + wi.to_be_filled_qty) > p_rnd_threshold
            and not exists
            (
                select 1
                from lpn_lock ll
                where ll.lpn_id = wi.lpn_id
                    and not exists
                    (
                        select 1
                        from int_alloc_lock_codes ialc
                        where ialc.lock_code = ll.inventory_lock_code
                            and ialc.whse = p_whse
                            and ialc.cd_master_id = wi.tc_company_id
                            and ialc.invn_need_type not in ('11','34')
                    )
            )
        union all
        select oli.item_id, oli.invn_type invn_type, oli.prod_stat prod_stat,
               oli.batch_nbr batch_nbr, oli.item_attr_1 item_attr_1, 
               oli.item_attr_2 item_attr_2, oli.item_attr_3 item_attr_3,  
               oli.item_attr_4 item_attr_4, oli.item_attr_5 item_attr_5,
               oli.cntry_of_orgn cntry_of_orgn, -oli.allocated_qty qty_available
        from order_line_item oli
        join orders o on o.order_id = oli.order_id
        join giv on giv.item_id = oli.item_id
        where o.o_facility_id = p_facility_id and oli.do_dtl_status = 115
            and oli.wave_nbr != p_pick_wave_nbr
    ) iv
    group by iv.item_id, iv.invn_type, iv.prod_stat, iv.batch_nbr,
        iv.item_attr_1, iv.item_attr_2, iv.item_attr_3, iv.item_attr_4,
        iv.item_attr_5, iv.cntry_of_orgn
    having sum(iv.qty_available) > p_rnd_threshold;
    wm_cs_log('Loaded sku invn cache ' || sql%rowcount);
end;
/

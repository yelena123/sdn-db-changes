CREATE OR REPLACE PROCEDURE LTL_RATESHIPMENT (P_BATCH_ID  INT, P_SHIPPERID INT, P_SHIPMENT_ID INT, L_COST OUT FLOAT, L_WEIGHT OUT INT) IS
  P_TARIFF_ZONE_ID         INT;
  L_BATCHID                INT;
  L_SHIPPERID              INT;
  L_SHIPMENTID             INT;
  L_LANEID                 INT;
  L_ORIGINPOSTALCODE       VARCHAR2(5);
  L_ORIGINCOUNTRYCODE      VARCHAR2(3);
  L_DESTINATIONPOSTALCODE  VARCHAR2(5);
  L_DESTINATIONCOUNTRYCODE VARCHAR2(3);
  L_SOURCINGFACTOR         DECIMAL;
  L_COMMODITYCLASS         DECIMAL;
  L_COMMODITYCLASSWEIGHT   DECIMAL;
  L_CARRIER_ZONE           VARCHAR2(32);
  L_TARIFF_ZONE_RATE_SEQ   INT;
  L_RATE                   DECIMAL;
BEGIN
  P_TARIFF_ZONE_ID := 1;

  -- INSERT INTO DEBUG_SQL(SOURCE_LOCATION, DATETIME, SQLTEXT) VALUES ('LTL_RATESHIPMENT', SYSDATE, '1-- '||p_batch_id);
  -- INSERT INTO DEBUG_SQL(SOURCE_LOCATION, DATETIME, SQLTEXT) VALUES ('LTL_RATESHIPMENT', SYSDATE, '2-- '||p_shipperid);
  FOR CURSOR_LTL_SHIPMENT IN (SELECT BATCHID, SHIPPERID, SHIPMENTID, LANEID, ORIGINPOSTALCODE, ORIGINCOUNTRYCODE, 
									DESTINATIONPOSTALCODE, DESTINATIONCOUNTRYCODE, SOURCINGFACTOR 
							  FROM   LTL_SHIPMENT
                              WHERE  BATCHID = P_BATCH_ID AND SHIPPERID = P_SHIPPERID AND SHIPMENTID = P_SHIPMENT_ID
                              AND    ISRATED = 0)
  LOOP
    L_BATCHID                := CURSOR_LTL_SHIPMENT.BATCHID;
    L_SHIPPERID              := CURSOR_LTL_SHIPMENT.SHIPPERID;
    L_SHIPMENTID             := CURSOR_LTL_SHIPMENT.SHIPMENTID;
    L_LANEID                 := CURSOR_LTL_SHIPMENT.LANEID;
    L_ORIGINPOSTALCODE       := CURSOR_LTL_SHIPMENT.ORIGINPOSTALCODE;
    L_ORIGINCOUNTRYCODE      := CURSOR_LTL_SHIPMENT.ORIGINCOUNTRYCODE;
    L_DESTINATIONPOSTALCODE  := CURSOR_LTL_SHIPMENT.DESTINATIONPOSTALCODE;
    L_DESTINATIONCOUNTRYCODE := CURSOR_LTL_SHIPMENT.DESTINATIONCOUNTRYCODE;
    L_SOURCINGFACTOR         := CURSOR_LTL_SHIPMENT.SOURCINGFACTOR;

    L_COST   := 0;
    L_WEIGHT := 0;

    ------------------------ LOOP 2 Starts----------------------------
    FOR CURSOR_LTLSHIPMENTCOMMODITIES IN (SELECT COMMODITYCLASS,
                                                 COMMODITYCLASSWEIGHT
                                          FROM   LTL_SHIPMENT_COMMODITIES
                                          WHERE  BATCHID = P_BATCH_ID
                                          AND    SHIPPERID = P_SHIPPERID
                                          AND    SHIPMENTID = L_SHIPMENTID)
    LOOP
      L_COMMODITYCLASS       := CURSOR_LTLSHIPMENTCOMMODITIES.COMMODITYCLASS;
      L_COMMODITYCLASSWEIGHT := CURSOR_LTLSHIPMENTCOMMODITIES.COMMODITYCLASSWEIGHT;
      -- INSERT INTO DEBUG_SQL(SOURCE_LOCATION, DATETIME, SQLTEXT) VALUES ('LTL_RATESHIPMENT', SYSDATE, '3-- '||l_COMMODITYCLASS);
      -- INSERT INTO DEBUG_SQL(SOURCE_LOCATION, DATETIME, SQLTEXT) VALUES ('LTL_RATESHIPMENT', SYSDATE, '4-- '||l_COMMODITYCLASSWEIGHT);
      -- INSERT INTO DEBUG_SQL(SOURCE_LOCATION, DATETIME, SQLTEXT) VALUES ('LTL_RATESHIPMENT', SYSDATE, '5-- '||l_DESTINATIONPOSTALCODE);
      ------------------------ LOOP 3 Starts----------------------------
      FOR CURSOR_TARIFF_LANE IN (SELECT DISTINCT CARRIER_ZONE
                                 FROM   TARIFF_LANE
                                 WHERE  TC_COMPANY_ID = P_SHIPPERID
                                 AND    TARIFF_ZONE_ID IN
                                        (SELECT DISTINCT TARIFF_ZONE_ID
                                          FROM   TARIFF_LANE
                                          WHERE  TC_COMPANY_ID = P_SHIPPERID)
                                 AND    (
									O_ZONE_ID = (SELECT ZONE_ID FROM ZONE_ATTRIBUTE WHERE ATTRIBUTE_VALUE = SUBSTR(L_ORIGINPOSTALCODE, 1, 3)) OR 
									O_ZONE_ID = (SELECT ZONE_ID FROM ZONE_ATTRIBUTE WHERE ATTRIBUTE_VALUE = L_ORIGINPOSTALCODE)
								 
								 )
								 AND    (
									D_ZONE_ID = (SELECT ZONE_ID FROM ZONE_ATTRIBUTE WHERE ATTRIBUTE_VALUE = SUBSTR(L_DESTINATIONPOSTALCODE, 1, 3)) OR
									D_ZONE_ID = (SELECT ZONE_ID FROM ZONE_ATTRIBUTE WHERE ATTRIBUTE_VALUE = L_DESTINATIONPOSTALCODE)
								) )
      LOOP
        L_CARRIER_ZONE := CURSOR_TARIFF_LANE.CARRIER_ZONE;
        -- INSERT INTO DEBUG_SQL(SOURCE_LOCATION, DATETIME, SQLTEXT) VALUES ('LTL_RATESHIPMENT', SYSDATE, '6-- '||l_carrier_zone);
        ------------------------ LOOP 4 Starts----------------------------
        FOR CURSOR_TARIFF_ZONE_RATE IN (SELECT TARIFF_ZONE_RATE_SEQ
                                        FROM   COMMODITY_CLASS  CS, --commodity_class2 DOESNOT EXIST
                                               TARIFF_ZONE_RATE TZR

                                        WHERE  TC_COMPANY_ID = P_SHIPPERID
                                        AND    TARIFF_ZONE_ID IN
                                               (SELECT DISTINCT TARIFF_ZONE_ID
                                                 FROM   TARIFF_LANE
                                                 WHERE  TC_COMPANY_ID =
                                                        P_SHIPPERID)
                                        AND    CARRIER_ZONE = L_CARRIER_ZONE
                                        AND    TZR.COMMODITY_CLASS =
                                               CS.COMMODITY_CLASS
                                        AND    CS.DESCRIPTION =
                                               L_COMMODITYCLASS)
        LOOP
          L_TARIFF_ZONE_RATE_SEQ := CURSOR_TARIFF_ZONE_RATE.TARIFF_ZONE_RATE_SEQ;
          -- INSERT INTO DEBUG_SQL(SOURCE_LOCATION, DATETIME, SQLTEXT) VALUES ('LTL_RATESHIPMENT', SYSDATE, '7-- '||l_tariff_zone_rate_seq);
          ------------------------ LOOP 5 Starts----------------------------
          FOR CURSOR_TARIFF_ZONE_RATE_TIER IN (SELECT RATE
                                               FROM   TARIFF_ZONE_RATE_TIER
                                               WHERE  TC_COMPANY_ID =
                                                      P_SHIPPERID
                                               AND    TARIFF_ZONE_ID IN
                                                      (SELECT DISTINCT TARIFF_ZONE_ID
                                                        FROM   TARIFF_LANE
                                                        WHERE  TC_COMPANY_ID =
                                                               P_SHIPPERID)
                                               AND    CARRIER_ZONE =
                                                      L_CARRIER_ZONE
                                               AND    TARIFF_ZONE_RATE_SEQ =
                                                      L_TARIFF_ZONE_RATE_SEQ
                                               AND    MINIMUM_SIZE <=
                                                      L_COMMODITYCLASSWEIGHT
                                               AND    MAXIMUM_SIZE >=
                                                      L_COMMODITYCLASSWEIGHT)
          LOOP
            L_RATE := CURSOR_TARIFF_ZONE_RATE_TIER.RATE;
            -- INSERT INTO DEBUG_SQL(SOURCE_LOCATION, DATETIME, SQLTEXT) VALUES ('LTL_RATESHIPMENT', SYSDATE, '8-- '||l_rate);
            L_COST   := L_COST + L_RATE;
            L_WEIGHT := L_WEIGHT + L_COMMODITYCLASSWEIGHT;
            -- INSERT INTO DEBUG_SQL(SOURCE_LOCATION, DATETIME, SQLTEXT) VALUES ('LTL_RATESHIPMENT', SYSDATE, '9-- '||l_cost);
            -- INSERT INTO DEBUG_SQL(SOURCE_LOCATION, DATETIME, SQLTEXT) VALUES ('LTL_RATESHIPMENT', SYSDATE, '10-- '||l_weight);
          END LOOP;
          ------------------------ LOOP 5 Ends----------------------------
        END LOOP;
        ------------------------ LOOP 4 Ends----------------------------
      END LOOP;
      ------------------------ LOOP 3 Ends----------------------------
    END LOOP;
    ------------------------ LOOP 2 Ends----------------------------
    IF L_WEIGHT = 0
    THEN
      L_WEIGHT := 1;
    END IF;
    L_COST := (L_COST * L_WEIGHT) / 100;
    -- INSERT INTO DEBUG_SQL(SOURCE_LOCATION, DATETIME, SQLTEXT) VALUES ('LTL_RATESHIPMENT', SYSDATE, '11-- '||l_cost);
  END LOOP;

END LTL_RATESHIPMENT;
/
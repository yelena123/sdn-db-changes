create or replace procedure manh_wave_sku_invn_rejections
(
    p_user_id            in ucl_user.user_name%type,
    p_sku_cnstr          in wave_parm.sku_cnstr%type,
    p_pull_all_swc       in wave_parm.pull_all_swc%type,
    p_reject_distro_rule in wave_parm.reject_distro_rule%type
)
as
    v_ship_wave_nbr     ship_wave_parm.ship_wave_nbr%type;
    v_rule_type         rule_hdr.rule_type%type;
    v_sa_failed         varchar2(2) := '1';
    v_ppack_failed      varchar2(2) := '15';
    v_swc_failed        varchar2(2) := '6';
    v_item_grp_failed   varchar2(2) := '10';
    v_order_grp_failed  varchar2(2) := '8';
begin
    select t.ship_wave_nbr, t.curr_rule_type
    into v_ship_wave_nbr, v_rule_type
    from tmp_wave_selection_parms t;
    
    -- if this is a fill wave rule
    if (v_rule_type = 'PF')
    then
        v_sa_failed         := '26';
        v_order_grp_failed  := '31';
    end if;

    -- reason code updates
    if (p_pull_all_swc = 'Y')
    then
        merge into order_line_item oli
        using
        (
            select t1.order_id, t1.line_item_id,
                case when t1.order_id = t2.order_id and t1.line_item_id = t2.line_item_id
                    then decode(t1.pre_pack_flag, 1, v_ppack_failed, v_sa_failed)
                    else v_swc_failed end reason_code
            from tmp_wave_selected_orders t1
            join tmp_wave_group_rejected_swcs t2
                on t2.ship_group_id = t1.ship_group_id
        ) iv on (iv.order_id = oli.order_id and iv.line_item_id = oli.line_item_id)
        when matched then
        update set oli.last_updated_source = p_user_id,
            oli.ship_wave_nbr = v_ship_wave_nbr, oli.last_updated_dttm = sysdate,
            oli.reason_code = iv.reason_code;
        wm_cs_log('OLI rejected due to reasons 1/6: ' || sql%rowcount, p_sql_log_level => 1);
    end if;

    if (p_reject_distro_rule = '1')
    then
        merge into order_line_item oli
        using
        (
            select t1.order_id, t1.line_item_id,
                case when t1.line_item_id = t2.line_item_id 
                    then decode(t1.pre_pack_flag, 1, v_ppack_failed, v_sa_failed)
                    else v_order_grp_failed end reason_code
            from tmp_wave_selected_orders t1
            join tmp_wave_group_rejected_orders t2 on t2.order_id = t1.order_id
        ) iv on (iv.order_id = oli.order_id and iv.line_item_id = oli.line_item_id)
        when matched then
        update set oli.last_updated_source = p_user_id,
            oli.ship_wave_nbr = v_ship_wave_nbr, oli.last_updated_dttm = sysdate,
            oli.reason_code = iv.reason_code;
        wm_cs_log('OLI rejected due to reasons 1/8: ' || sql%rowcount, p_sql_log_level => 1);
    elsif (p_reject_distro_rule = '2')
    then
        merge into order_line_item oli
        using
        (
            select t1.order_id, t1.line_item_id,
                case when t1.order_id = t2.order_id and t1.line_item_id = t2.line_item_id
                    then decode(t1.pre_pack_flag, 1, v_ppack_failed, v_sa_failed)
                    else v_item_grp_failed end reason_code
            from tmp_wave_selected_orders t1
            join tmp_wave_group_rejected_items t2 on t2.item_id = t1.item_id
        ) iv on (iv.order_id = oli.order_id and iv.line_item_id = oli.line_item_id)
        when matched then
        update set oli.last_updated_source = p_user_id,
            oli.ship_wave_nbr = v_ship_wave_nbr, oli.last_updated_dttm = sysdate,
            oli.reason_code = iv.reason_code;
        wm_cs_log('OLI rejected due to reasons 1/10: ' || sql%rowcount, p_sql_log_level => 1);
    else
        merge into order_line_item oli
        using
        (
            select t1.order_id, t1.line_item_id,
                decode(t1.pre_pack_flag, 1, v_ppack_failed, v_sa_failed) reason_code
            from tmp_wave_selected_orders t1
            where not exists
            (
                select 1
                from tmp_ord_dtl_sku_invn t2
                where t2.order_id = t1.order_id
                    and t2.line_item_id = t1.line_item_id
            )
        ) iv on (iv.order_id = oli.order_id and iv.line_item_id = oli.line_item_id)
        when matched then
        update set oli.last_updated_source = p_user_id,
            oli.ship_wave_nbr = v_ship_wave_nbr, oli.last_updated_dttm = sysdate,
            oli.reason_code = iv.reason_code;
        wm_cs_log('OLI rejected due to reason 1: ' || sql%rowcount, p_sql_log_level => 1);
    end if;

    if (p_sku_cnstr = '1')
    then
        -- remove the orders that were not soft allocated (primary failures)
        delete from tmp_wave_selected_orders t
        where not exists
        (
            select 1
            from tmp_ord_dtl_sku_invn t1
            where t1.order_id = t.order_id and t1.line_item_id = t.line_item_id
        );
        wm_cs_log('Undo OLI for primary failures ' || sql%rowcount, p_sql_log_level => 1);
    else
        -- per CV, always deselect vas lines that did not allocate fully
        delete from tmp_wave_selected_orders t
        where t.perform_vas > 0
            and not exists
            (
                select 1
                from tmp_ord_dtl_sku_invn t1
                where t1.order_id = t.order_id
                    and t1.line_item_id = t.line_item_id
            );
        wm_cs_log('Undo of partially allocated VAS lines ' || sql%rowcount, p_sql_log_level => 1);
    end if;
    
    -- remove all swc failures; these are exempt from sku_cnstr post-processing
    if (p_pull_all_swc = 'Y')
    then
        delete from tmp_wave_selected_orders t
        where exists
        (
            select 1
            from tmp_wave_group_rejected_swcs t1
            where t1.ship_group_id = t.ship_group_id
        );
        wm_cs_log('Undo of lines due to rejected SWCs ' || sql%rowcount, p_sql_log_level => 1);
    end if;

    -- remove (deselect) secondary failures but keep the original failures
    -- so that they are updated appropriately according to sku_cnstr
    if (p_reject_distro_rule = '1')
    then
        delete from tmp_wave_selected_orders t
        where exists
        (
            select 1
            from tmp_wave_group_rejected_orders t1
            where t1.order_id = t.order_id and t1.line_item_id != t.line_item_id
        );
        wm_cs_log('Undo OLI for secondary failures due to matching orders ' || sql%rowcount, p_sql_log_level => 1);
    elsif (p_reject_distro_rule = '2')
    then
        delete from tmp_wave_selected_orders t
        where exists
        (
            select 1
            from tmp_wave_group_rejected_items t1
            where t1.item_id = t.item_id
                and (t1.order_id != t.order_id
                    or t1.line_item_id != t.line_item_id)
        );
        wm_cs_log('Undo OLI for secondary failures due to matching items ' || sql%rowcount, p_sql_log_level => 1);
    end if;
end;
/

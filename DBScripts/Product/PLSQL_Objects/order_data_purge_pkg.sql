SET DEFINE OFF SCAN OFF;

CREATE OR REPLACE PACKAGE ORDER_DATA_PURGE_PKG
AS
   PROCEDURE INVENTORY_PURGE_PROC (PURGE_DAYS IN NUMBER);

   PROCEDURE INV_BUSSINESS_RULES_PURGE_PROC (PURGE_DAYS IN NUMBER);

   PROCEDURE COMMERCE_PURGE_PROC (PURGE_DAYS IN NUMBER);

   PROCEDURE INV_PURGE_PROC (PURGE_DAYS IN NUMBER);

   PROCEDURE PURGE_STAGING_EVENT_PROCESS (PURGE_DAYS IN NUMBER);

   PROCEDURE CO_BASE_DATA_PURGE (PURGE_DAYS IN NUMBER);

   PROCEDURE INV_LOG_PURGE_PROC (PURGE_DAYS IN NUMBER);

   PROCEDURE insert_del_data_bulk (p_prod_table_name   IN VARCHAR2,
                                   p_arch_table_name   IN VARCHAR2,
                                   p_temp_table           VARCHAR2,
                                   p_column_name       IN VARCHAR2,
                                   p_query                VARCHAR2,
                                   p_archive_flag         VARCHAR2);

   PROCEDURE update_purge_status (purge_days IN NUMBER, status IN VARCHAR2);

   PROCEDURE exp_table_dump (p_directory      IN VARCHAR2,
                             p_table_name     IN VARCHAR2,
                             p_table_filter   IN VARCHAR2,
                             p_time           IN VARCHAR2);

   PROCEDURE imp_table_dump (p_directory    IN VARCHAR2,
                             p_table_name   IN VARCHAR2,
                             p_time         IN VARCHAR2);

   PROCEDURE purge_history_data (p_datapump_directory   IN VARCHAR2,
                                 p_table_name           IN VARCHAR2,
                                 p_retention_days       IN NUMBER);
END ORDER_DATA_PURGE_PKG;
/

CREATE OR REPLACE PACKAGE BODY ORDER_DATA_PURGE_PKG
AS
   gvprocedurename    VARCHAR2 (100);
   atccompanyid       company.company_id%TYPE;
   varchiveflag       NUMBER (1) := 0;
   nsqlcode           NUMBER;
   vsqlerrm           VARCHAR2 (300);
   garchivebasedttm   DATE := TRUNC (SYSDATE);
   gv_job_class_chk   PLS_INTEGER := 0;
   gcommitfrequency   PLS_INTEGER := 10000;
   n_parallel_level   PLS_INTEGER := 0;
   gdelete_error      EXCEPTION;

   PROCEDURE exp_table_dump (p_directory      IN VARCHAR2,
                             p_table_name     IN VARCHAR2,
                             p_table_filter   IN VARCHAR2,
                             p_time           IN VARCHAR2)
   IS
      v_handle               NUMBER;                             -- job handle
      v_current_time         DATE := SYSDATE; -- consistent timestamp for files, job_name etc.
      v_start_time           DATE;                  -- start time for log file
      v_logfile_name         VARCHAR2 (200);                   -- logfile name
      v_dumpfile_name        VARCHAR2 (200);                   -- logfile name
      v_line_no              INTEGER := 0;                    -- debug line no
      v_sqlcode              NUMBER;                                -- sqlcode
      v_compatible           VARCHAR2 (40) := 'COMPATIBLE'; -- default is 'COMPATIBLE'
      vc_job_mode   CONSTANT VARCHAR2 (200) := 'TABLE';            -- Job mode
      garchivebasedttm       DATE := SYSDATE;
      p_schema_name          VARCHAR2 (100);

      percent_done           NUMBER;             -- Percentage of job complete
      job_state              VARCHAR2 (30);      -- To keep track of job state
      le                     ku$_LogEntry;       -- For WIP and error messages
      js                     ku$_JobStatus;  -- The job status from get_status
      jd                     ku$_JobDesc; -- The job description from get_status
      sts                    ku$_Status; -- The status object returned by get_status
      rtn_code               NUMBER := 0;
      ind                    NUMBER;                             -- Loop index
      verror                 EXCEPTION;
   BEGIN
      SELECT USER INTO p_schema_name FROM DUAL;

      n_parallel_level := 6;
      v_logfile_name :=
         'expdp_' || p_table_name || '_' || p_time || '_export.log';
      v_dumpfile_name :=
         'expdp_' || p_table_name || '_' || p_time || '_%U.dmp';

      -- Open the job
      BEGIN
         v_handle :=
            DBMS_DATAPUMP.open (
               operation   => 'EXPORT',
               job_mode    => vc_job_mode,
               job_name    =>    'EXPORT_'
                              || vc_job_mode
                              || '_'
                              || TO_CHAR (v_current_time, 'YYYY_MMDD_HH24MI'),
               version     => v_compatible);
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line (
               SUBSTR ('Failure in dbms_datapump.open', 1, 255));
            RAISE;
      END;

      -- Add a logfile
      DBMS_DATAPUMP.add_file (
         handle      => v_handle,
         filename    => v_logfile_name,
         directory   => p_directory,
         filetype    => DBMS_DATAPUMP.ku$_file_type_log_file);


      -- Add a datafile
      DBMS_DATAPUMP.add_file (
         handle      => v_handle,
         filename    => v_dumpfile_name,
         directory   => p_directory,
         filetype    => DBMS_DATAPUMP.ku$_file_type_dump_file);
      v_line_no := 500;                                       -- debug line no


      -- Filter for the schemma
      DBMS_DATAPUMP.metadata_filter (
         handle   => v_handle,
         name     => 'SCHEMA_LIST',
         VALUE    => '''' || p_schema_name || '''');

      --Filter for the table
      DBMS_DATAPUMP.metadata_filter (handle   => v_handle,
                                     name     => 'NAME_LIST',
                                     VALUE    => '''' || p_table_name || '''');


      -- Add a subquery
      DBMS_DATAPUMP.data_filter (handle   => v_handle,
                                 name     => 'SUBQUERY',
                                 VALUE    => p_table_filter);

      DBMS_DATAPUMP.set_parallel (handle   => v_handle,
                                  degree   => n_parallel_level);
      DBMS_DATAPUMP.metadata_filter (
         handle   => v_handle,
         name     => 'EXCLUDE_PATH_EXPR',
         VALUE    => 'IN (''INDEX'', ''SYNONYMS'',''GRANTS'',''STATISTICS'',''CONSTRAINT'',''REF_CONSTRAINT'')');
      -- Get the start time
      v_start_time := SYSDATE;

      -- Add a start time to the log file
      DBMS_DATAPUMP.log_entry (
         handle          => v_handle,
         MESSAGE         =>    'Job Start at '
                            || TO_CHAR (v_start_time, 'DD-Mon-RR HH24:MI:SS'),
         log_file_only   => 0);

      -- Start the job
      DBMS_DATAPUMP.start_job (handle => v_handle, cluster_ok => 0);

      percent_done := 0;
      job_state := 'UNDEFINED';

      -- dbms_output.put_line('here1');
      WHILE (job_state != 'COMPLETED') AND (job_state != 'STOPPED')
      LOOP
         DBMS_DATAPUMP.get_status (
            v_handle,
              DBMS_DATAPUMP.ku$_status_job_error
            + DBMS_DATAPUMP.ku$_status_job_status
            + DBMS_DATAPUMP.ku$_status_wip
            + DBMS_DATAPUMP.ku$_job_complete_errors,
            -1,
            job_state,
            sts);
         js := sts.job_status;

         -- If the percentage done changed, display the new value.
         IF js.percent_done != percent_done
         THEN
            DBMS_OUTPUT.put_line (
               '*** Job percent done = ' || TO_CHAR (js.percent_done));
            percent_done := js.percent_done;
         END IF;

         -- If any work-in-progress (WIP) or error messages were received for the job,
         -- display them.

         --            IF (BITAND (sts.mask, DBMS_DATAPUMP.ku$_status_wip) != 0)
         --            THEN
         --               le := sts.wip;
         --            ELSE
         IF (BITAND (sts.mask, DBMS_DATAPUMP.ku$_status_job_error) != 0)
         THEN
            le := sts.error;
         ELSIF (BITAND (sts.mask, DBMS_DATAPUMP.ku$_job_complete_errors) != 0)
         THEN
            le := sts.error;
         ELSE
            le := NULL;
         END IF;

         --            END IF;

         IF le IS NOT NULL
         THEN
            ind := le.FIRST;

            WHILE ind IS NOT NULL
            LOOP
               DBMS_OUTPUT.put_line (le (ind).LogText);
               ind := le.NEXT (ind);
               RAISE verror;
            END LOOP;
         END IF;
      END LOOP;

      -- Indicate that the job finished and detach from it.
      IF job_state != 'COMPLETED'
      THEN
         rtn_code := 1;
      END IF;

      DBMS_DATAPUMP.detach (handle => v_handle);
   EXCEPTION
      WHEN verror
      THEN
         raise_application_error (
            -20006,
               'Export Failed, please check the logfile - '
            || v_logfile_name
            || ' at database datapump directory - '
            || p_directory);
      WHEN OTHERS
      THEN
         BEGIN
            DBMS_DATAPUMP.detach (handle => v_handle);
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;

         DBMS_OUTPUT.put_line (
            SUBSTR ('Value of v_line_no=' || TO_CHAR (v_line_no), 1, 255));
         RAISE;
   END exp_table_dump;

   PROCEDURE imp_table_dump (p_directory    IN VARCHAR2,
                             p_table_name   IN VARCHAR2,
                             p_time         IN VARCHAR2)
   AS
      v_handle           NUMBER;                                 -- job handle
      v_current_time     DATE := SYSDATE; -- consistent timestamp for files, job_name etc.
      v_start_time       DATE;                      -- start time for log file
      v_dumpfile_name    VARCHAR2 (200);
      v_logfile_name     VARCHAR2 (200);                       -- logfile name
      --   v_default_dir          VARCHAR (30) := 'DATA_PUMP_DIR';        -- directory
      --v_default_dir      VARCHAR (30) := 'CDTMW';                    -- directory
      v_line_no          INTEGER := 0;                        -- debug line no
      v_sqlcode          NUMBER;                                    -- sqlcode
      garchivebasedttm   DATE := SYSDATE;
      p_schema_name      VARCHAR2 (100);

      percent_done       NUMBER;                 -- Percentage of job complete
      job_state          VARCHAR2 (30);          -- To keep track of job state
      le                 ku$_LogEntry;           -- For WIP and error messages
      js                 ku$_JobStatus;      -- The job status from get_status
      jd                 ku$_JobDesc;   -- The job description from get_status
      sts                ku$_Status; -- The status object returned by get_status
      rtn_code           NUMBER := 0;
      ind                NUMBER;                                 -- Loop index
      verror             EXCEPTION;
   BEGIN
      SELECT USER INTO p_schema_name FROM DUAL;

      n_parallel_level := 6;

      BEGIN
         v_handle :=
            DBMS_DATAPUMP.open (
               operation   => 'IMPORT',
               job_mode    => 'TABLE',
               job_name    =>    'Import_Table_'
                              || TO_CHAR (v_current_time,
                                          'YYYY_MMDD_HH24MI_SS'),
               version     => 'COMPATIBLE');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line (
               SUBSTR ('Failure in dbms_datapump.open', 1, 255));
            RAISE;
      END;

      v_logfile_name :=
         'impdp_' || p_table_name || '_' || p_time || '_import.log';
      v_dumpfile_name :=
         'expdp_' || p_table_name || '_' || p_time || '_%U.dmp';

      DBMS_DATAPUMP.add_file (
         handle      => v_handle,
         filename    => v_logfile_name,
         directory   => p_directory,
         filetype    => DBMS_DATAPUMP.ku$_file_type_log_file);

      DBMS_DATAPUMP.add_file (
         handle      => v_handle,
         filename    => v_dumpfile_name,
         directory   => p_directory,
         filetype    => DBMS_DATAPUMP.ku$_file_type_dump_file);

      DBMS_DATAPUMP.metadata_filter (
         handle   => v_handle,
         name     => 'SCHEMA_LIST',
         VALUE    => '''' || p_schema_name || '''');


      DBMS_DATAPUMP.metadata_filter (handle   => v_handle,
                                     name     => 'NAME_LIST',
                                     VALUE    => '''' || p_table_name || '''');

      DBMS_DATAPUMP.SET_PARAMETER (handle   => v_handle,
                                   name     => 'INCLUDE_METADATA',
                                   VALUE    => 0);

      DBMS_DATAPUMP.SET_PARAMETER (handle   => v_handle,
                                   name     => 'TABLE_EXISTS_ACTION',
                                   VALUE    => 'TRUNCATE');

      DBMS_DATAPUMP.set_parallel (handle   => v_handle,
                                  degree   => n_parallel_level);
      DBMS_DATAPUMP.start_job (handle => v_handle, cluster_ok => 0);

      percent_done := 0;
      job_state := 'UNDEFINED';

      -- dbms_output.put_line('here1');
      WHILE (job_state != 'COMPLETED') AND (job_state != 'STOPPED')
      LOOP
         DBMS_DATAPUMP.get_status (
            v_handle,
              DBMS_DATAPUMP.ku$_status_job_error
            + DBMS_DATAPUMP.ku$_status_job_status
            + DBMS_DATAPUMP.ku$_status_wip
            + DBMS_DATAPUMP.ku$_job_complete_errors,
            -1,
            job_state,
            sts);
         js := sts.job_status;

         -- If the percentage done changed, display the new value.
         IF js.percent_done != percent_done
         THEN
            DBMS_OUTPUT.put_line (
               '*** Job percent done = ' || TO_CHAR (js.percent_done));
            percent_done := js.percent_done;
         END IF;

         -- If any work-in-progress (WIP) or error messages were received for the job,
         -- display them.

         --            IF (BITAND (sts.mask, DBMS_DATAPUMP.ku$_status_wip) != 0)
         --            THEN
         --               le := sts.wip;
         --            ELSE
         IF (BITAND (sts.mask, DBMS_DATAPUMP.ku$_status_job_error) != 0)
         THEN
            le := sts.error;
         ELSIF (BITAND (sts.mask, DBMS_DATAPUMP.ku$_job_complete_errors) != 0)
         THEN
            le := sts.error;
         ELSE
            le := NULL;
         END IF;

         --            END IF;

         IF le IS NOT NULL
         THEN
            ind := le.FIRST;

            WHILE ind IS NOT NULL
            LOOP
               DBMS_OUTPUT.put_line (le (ind).LogText);
               ind := le.NEXT (ind);
               RAISE verror;
            END LOOP;
         END IF;
      END LOOP;

      -- Indicate that the job finished and detach from it.
      IF job_state != 'COMPLETED'
      THEN
         rtn_code := 1;
      END IF;

      DBMS_DATAPUMP.detach (handle => v_handle);
   EXCEPTION
      WHEN verror
      THEN
         raise_application_error (
            -20006,
               'Import Failed, please check the logfile - '
            || v_logfile_name
            || ' at database datapump directory - '
            || p_directory);
      WHEN OTHERS
      THEN
         BEGIN
            DBMS_DATAPUMP.detach (handle => v_handle);
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;

         DBMS_OUTPUT.put_line (
            SUBSTR ('Value of v_line_no=' || TO_CHAR (v_line_no), 1, 255));
         RAISE;
   END imp_table_dump;

   PROCEDURE purge_history_data (p_datapump_directory   IN VARCHAR2,
                                 p_table_name           IN VARCHAR2,
                                 p_retention_days       IN NUMBER)
   AS
      v_where_clause     VARCHAR2 (1000);
      garchivebasedttm   DATE := SYSDATE;
      v_sql              VARCHAR2 (1000);
      v_count_tab1       NUMBER := 0;
      v_count_tab2       NUMBER := 0;
      v_count_tab3       NUMBER := 0;
      v_count_tab4       NUMBER := 0;
      v_count_tab5       NUMBER := 0;
      v_count_tab6       NUMBER := 0;
      v_count_tab7       NUMBER := 0;
      v_temp             VARCHAR2 (1000);
      v_timestamp        VARCHAR2 (10);
      v_notsupported     EXCEPTION;
      v_cons_count       NUMBER := 0;
   BEGIN
      EXECUTE IMMEDIATE
         'alter session set NLS_TIMESTAMP_FORMAT = ''YYYY-MM-DD HH24:MI:SS.FF''';

      EXECUTE IMMEDIATE
         'alter session set NLS_DATE_FORMAT = ''YYYY-MM-DD HH24:MI:SS''';

      SELECT TO_CHAR (SYSDATE, 'HHSS') INTO v_timestamp FROM DUAL;

      IF p_table_name = 'I_AVAIL_INVENTORY_EVENT'
      THEN
         BEGIN
            archive_pkg.log_archive (
               NULL,
               'Purge_History_Data - I_AVAIL_INVENTORY_EVENT - Begin',
               NULL,
               NULL,
               SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

            QUIET_DROP ('TABLE', 'COMMERCE_PURGE_GTT4');
            QUIET_DROP ('TABLE', 'COMMERCE_PURGE_GTT5');

            BEGIN
               EXECUTE IMMEDIATE
                  'LOCK TABLE MASTER_COMMERCE_EVENT IN SHARE MODE NOWAIT';

               EXECUTE IMMEDIATE
                  'LOCK TABLE I_AVAIL_INVENTORY_EVENT_DETAIL IN SHARE MODE NOWAIT';

               EXECUTE IMMEDIATE
                  'LOCK TABLE I_AVAIL_INVENTORY_EVENT IN SHARE MODE NOWAIT';
            EXCEPTION
               WHEN OTHERS
               THEN
                  archive_pkg.log_error (SQLCODE,
                                         'Table locked by other transaction');
                  raise_application_error (
                     -20005,
                     'Table locked by other transaction');
            END;

            EXECUTE IMMEDIATE
                  'CREATE TABLE COMMERCE_PURGE_GTT4 TABLESPACE DOM_DT_TBS AS SELECT EVENT_ID FROM MASTER_COMMERCE_EVENT WHERE LAST_UPDATED_DTTM < TO_DATE('''
               || garchivebasedttm
               || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
               || p_retention_days;

            EXECUTE IMMEDIATE
               'CREATE TABLE COMMERCE_PURGE_GTT5 TABLESPACE DOM_DT_TBS AS SELECT AVAIL_INV_EVENT_ID FROM I_AVAIL_INVENTORY_EVENT WHERE EVENT_ID IN (SELECT EVENT_ID FROM COMMERCE_PURGE_GTT4)';

            v_where_clause :=
               'WHERE AVAIL_INV_EVENT_ID NOT IN (SELECT AVAIL_INV_EVENT_ID FROM COMMERCE_PURGE_GTT5)';

            v_sql :=
               'SELECT COUNT(1) FROM I_AVAIL_INVENTORY_EVENT_DETAIL WHERE AVAIL_INV_EVENT_ID IN (SELECT AVAIL_INV_EVENT_ID FROM COMMERCE_PURGE_GTT5)';

            EXECUTE IMMEDIATE (v_sql) INTO v_count_tab1;

            exp_table_dump (p_datapump_directory,
                            'I_AVAIL_INVENTORY_EVENT_DETAIL',
                            v_where_clause,
                            v_timestamp);

            v_where_clause :=
               'WHERE EVENT_ID NOT IN (SELECT EVENT_ID FROM COMMERCE_PURGE_GTT4)';

            v_sql :=
               'SELECT COUNT(1) FROM I_AVAIL_INVENTORY_EVENT WHERE EVENT_ID IN (SELECT EVENT_ID FROM COMMERCE_PURGE_GTT4)';

            EXECUTE IMMEDIATE (v_sql) INTO v_count_tab2;

            exp_table_dump (p_datapump_directory,
                            'I_AVAIL_INVENTORY_EVENT',
                            v_where_clause,
                            v_timestamp);

            EXECUTE IMMEDIATE
               'ALTER TABLE I_AVAIL_INVENTORY_EVENT_DETAIL DISABLE CONSTRAINT I_AVL_INVTRY_EVENT_DTL_FK0';

            imp_table_dump (p_datapump_directory,
                            'I_AVAIL_INVENTORY_EVENT',
                            v_timestamp);

            archive_pkg.log_archive (NULL,
                                     'I_AVAIL_INVENTORY_EVENT',
                                     v_count_tab2,
                                     NULL,
                                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

            imp_table_dump (p_datapump_directory,
                            'I_AVAIL_INVENTORY_EVENT_DETAIL',
                            v_timestamp);

            EXECUTE IMMEDIATE
               'ALTER TABLE I_AVAIL_INVENTORY_EVENT_DETAIL ENABLE CONSTRAINT I_AVL_INVTRY_EVENT_DTL_FK0';

            archive_pkg.log_archive (NULL,
                                     'I_AVAIL_INVENTORY_EVENT_DETAIL',
                                     v_count_tab1,
                                     NULL,
                                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
            archive_pkg.log_archive (
               NULL,
               'Purge_History_Data - I_AVAIL_INVENTORY_EVENT - End',
               NULL,
               NULL,
               SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
         EXCEPTION
            WHEN OTHERS
            THEN
               archive_pkg.log_error (
                  SQLCODE,
                  'Error in Purge_History_Data Procedure');

               SELECT COUNT (*)
                 INTO v_cons_count
                 FROM user_constraints
                WHERE     constraint_name = 'I_AVL_INVTRY_EVENT_DTL_FK0'
                      AND status = 'DISABLED';

               IF v_cons_count = 1
               THEN
                  EXECUTE IMMEDIATE
                     'ALTER TABLE I_AVAIL_INVENTORY_EVENT_DETAIL ENABLE CONSTRAINT I_AVL_INVTRY_EVENT_DTL_FK0';
               END IF;

               raise_application_error (
                  -20005,
                  'Error in history_purge_data procedure execution.');
         END;
      ELSIF p_table_name = 'MASTER_COMMERCE_EVENT'
      THEN
         BEGIN
            archive_pkg.log_archive (
               NULL,
               'Purge_History_Data - MASTER_COMMERCE_EVENT - Begin',
               NULL,
               NULL,
               SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

            QUIET_DROP ('TABLE', 'PURGE_STAGING_EVENT_GTT2');
            QUIET_DROP ('TABLE', 'PURGE_STAGING_EVENT_GTT3');

            BEGIN
               EXECUTE IMMEDIATE
                  'LOCK TABLE MASTER_COMMERCE_EVENT IN SHARE MODE NOWAIT';

               EXECUTE IMMEDIATE
                  'LOCK TABLE I_AVAIL_INVENTORY_EVENT_DETAIL IN SHARE MODE NOWAIT';

               EXECUTE IMMEDIATE
                  'LOCK TABLE I_AVAIL_INVENTORY_EVENT IN SHARE MODE NOWAIT';

               EXECUTE IMMEDIATE
                  'LOCK TABLE I_FULFILLMENT_OUTAGE_EVENT IN SHARE MODE NOWAIT';

               EXECUTE IMMEDIATE
                  'LOCK TABLE I_REBUILD_STAGING IN SHARE MODE NOWAIT';

               EXECUTE IMMEDIATE
                  'LOCK TABLE I_AVAIL_SYNC_STAGING IN SHARE MODE NOWAIT';
            EXCEPTION
               WHEN OTHERS
               THEN
                  archive_pkg.log_error (SQLCODE,
                                         'Table locked by other transaction');
                  raise_application_error (
                     -20005,
                     'Table locked by other transaction');
            END;

            EXECUTE IMMEDIATE
                  'CREATE TABLE PURGE_STAGING_EVENT_GTT2 TABLESPACE DOM_DT_TBS AS SELECT event_id FROM MASTER_COMMERCE_EVENT WHERE EVENT_STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC IN (''Success'', ''Validation Failure'', ''Error'')) AND  LAST_UPDATED_DTTM < TO_DATE('''
               || garchivebasedttm
               || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
               || p_retention_days;

            EXECUTE IMMEDIATE
               'CREATE TABLE PURGE_STAGING_EVENT_GTT3 TABLESPACE DOM_DT_TBS AS SELECT AVAIL_INV_EVENT_ID FROM I_AVAIL_INVENTORY_EVENT WHERE event_id IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

            v_where_clause :=
               'WHERE AVAIL_INV_EVENT_ID NOT IN (SELECT AVAIL_INV_EVENT_ID FROM PURGE_STAGING_EVENT_GTT3)';

            v_sql :=
               'SELECT COUNT(1) FROM I_AVAIL_INVENTORY_EVENT_DETAIL WHERE AVAIL_INV_EVENT_ID IN (SELECT AVAIL_INV_EVENT_ID FROM PURGE_STAGING_EVENT_GTT3)';

            EXECUTE IMMEDIATE (v_sql) INTO v_count_tab1;

            exp_table_dump (p_datapump_directory,
                            'I_AVAIL_INVENTORY_EVENT_DETAIL',
                            v_where_clause,
                            v_timestamp);

            v_where_clause :=
               'WHERE event_id NOT IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

            v_sql :=
               'SELECT COUNT(1) FROM I_AVAIL_INVENTORY_EVENT WHERE event_id IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

            EXECUTE IMMEDIATE (v_sql) INTO v_count_tab2;

            exp_table_dump (p_datapump_directory,
                            'I_AVAIL_INVENTORY_EVENT',
                            v_where_clause,
                            v_timestamp);
            v_where_clause :=
               'WHERE event_id NOT IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

            v_sql :=
               'SELECT COUNT(1) FROM I_FULFILLMENT_OUTAGE_EVENT WHERE event_id IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

            EXECUTE IMMEDIATE (v_sql) INTO v_count_tab3;

            exp_table_dump (p_datapump_directory,
                            'I_FULFILLMENT_OUTAGE_EVENT',
                            v_where_clause,
                            v_timestamp);

            v_where_clause :=
               'WHERE rebuild_event_id NOT IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';
            v_sql :=
               'SELECT COUNT(1) FROM I_REBUILD_EVENT WHERE rebuild_event_id IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

            EXECUTE IMMEDIATE (v_sql) INTO v_count_tab4;

            exp_table_dump (p_datapump_directory,
                            'I_REBUILD_EVENT',
                            v_where_clause,
                            v_timestamp);

            v_where_clause :=
               'WHERE event_id NOT IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

            v_sql :=
               'SELECT COUNT(1) FROM I_REBUILD_STAGING WHERE event_id IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

            EXECUTE IMMEDIATE (v_sql) INTO v_count_tab5;

            exp_table_dump (p_datapump_directory,
                            'I_REBUILD_STAGING',
                            v_where_clause,
                            v_timestamp);

            v_where_clause :=
               'WHERE event_id NOT IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

            v_sql :=
               'SELECT COUNT(1) FROM I_AVAIL_SYNC_STAGING WHERE event_id IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

            EXECUTE IMMEDIATE (v_sql) INTO v_count_tab6;

            exp_table_dump (p_datapump_directory,
                            'I_AVAIL_SYNC_STAGING',
                            v_where_clause,
                            v_timestamp);

            v_where_clause :=
               'WHERE event_id NOT IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

            v_sql :=
               'SELECT COUNT(1) FROM MASTER_COMMERCE_EVENT WHERE event_id IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

            EXECUTE IMMEDIATE (v_sql) INTO v_count_tab7;

            exp_table_dump (p_datapump_directory,
                            'MASTER_COMMERCE_EVENT',
                            v_where_clause,
                            v_timestamp);

            EXECUTE IMMEDIATE
               'ALTER TABLE I_AVAIL_INVENTORY_EVENT_DETAIL DISABLE CONSTRAINT I_AVL_INVTRY_EVENT_DTL_FK0';

            EXECUTE IMMEDIATE
               'ALTER TABLE I_AVAIL_INVENTORY_EVENT DISABLE CONSTRAINT I_AVAIL_INVENTORY_EVENT_FK0';

            EXECUTE IMMEDIATE
               'ALTER TABLE I_FULFILLMENT_OUTAGE_EVENT DISABLE CONSTRAINT I_FULFILLMENT_OUTAGE_EVENT_FK0';


            SELECT    'ALTER TABLE '
                   || B.TABLE_NAME
                   || ' DISABLE CONSTRAINT '
                   || B.CONSTRAINT_NAME
              INTO v_temp
              FROM user_constraints A, user_constraints B
             WHERE     A.CONSTRAINT_NAME = B.R_CONSTRAINT_NAME
                   AND a.table_name = 'MASTER_COMMERCE_EVENT'
                   AND b.table_name = 'I_REBUILD_STAGING'
                   AND B.CONSTRAINT_TYPE = 'R';

            EXECUTE IMMEDIATE (v_temp);

            SELECT    'ALTER TABLE '
                   || B.TABLE_NAME
                   || ' DISABLE CONSTRAINT '
                   || B.CONSTRAINT_NAME
              INTO v_temp
              FROM user_constraints A, user_constraints B
             WHERE     A.CONSTRAINT_NAME = B.R_CONSTRAINT_NAME
                   AND a.table_name = 'MASTER_COMMERCE_EVENT'
                   AND b.table_name = 'I_AVAIL_SYNC_STAGING'
                   AND B.CONSTRAINT_TYPE = 'R';

            EXECUTE IMMEDIATE (v_temp);

            imp_table_dump (p_datapump_directory,
                            'MASTER_COMMERCE_EVENT',
                            v_timestamp);

            archive_pkg.log_archive (NULL,
                                     'MASTER_COMMERCE_EVENT',
                                     v_count_tab7,
                                     NULL,
                                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

            imp_table_dump (p_datapump_directory,
                            'I_AVAIL_SYNC_STAGING',
                            v_timestamp);

            SELECT    'ALTER TABLE '
                   || B.TABLE_NAME
                   || ' ENABLE CONSTRAINT '
                   || B.CONSTRAINT_NAME
              INTO v_temp
              FROM user_constraints A, user_constraints B
             WHERE     A.CONSTRAINT_NAME = B.R_CONSTRAINT_NAME
                   AND a.table_name = 'MASTER_COMMERCE_EVENT'
                   AND b.table_name = 'I_AVAIL_SYNC_STAGING'
                   AND B.CONSTRAINT_TYPE = 'R';

            EXECUTE IMMEDIATE (v_temp);

            archive_pkg.log_archive (NULL,
                                     'I_AVAIL_SYNC_STAGING',
                                     v_count_tab6,
                                     NULL,
                                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

            imp_table_dump (p_datapump_directory,
                            'I_REBUILD_EVENT',
                            v_timestamp);

            archive_pkg.log_archive (NULL,
                                     'I_REBUILD_EVENT',
                                     v_count_tab5,
                                     NULL,
                                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

            imp_table_dump (p_datapump_directory,
                            'I_REBUILD_STAGING',
                            v_timestamp);

            SELECT    'ALTER TABLE '
                   || B.TABLE_NAME
                   || ' ENABLE CONSTRAINT '
                   || B.CONSTRAINT_NAME
              INTO v_temp
              FROM user_constraints A, user_constraints B
             WHERE     A.CONSTRAINT_NAME = B.R_CONSTRAINT_NAME
                   AND a.table_name = 'MASTER_COMMERCE_EVENT'
                   AND b.table_name = 'I_REBUILD_STAGING'
                   AND B.CONSTRAINT_TYPE = 'R';

            EXECUTE IMMEDIATE (v_temp);

            archive_pkg.log_archive (NULL,
                                     'I_REBUILD_STAGING',
                                     v_count_tab5,
                                     NULL,
                                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

            imp_table_dump (p_datapump_directory,
                            'I_FULFILLMENT_OUTAGE_EVENT',
                            v_timestamp);

            EXECUTE IMMEDIATE
               'ALTER TABLE I_FULFILLMENT_OUTAGE_EVENT ENABLE CONSTRAINT I_FULFILLMENT_OUTAGE_EVENT_FK0';

            archive_pkg.log_archive (NULL,
                                     'I_FULFILLMENT_OUTAGE_EVENT',
                                     v_count_tab3,
                                     NULL,
                                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

            imp_table_dump (p_datapump_directory,
                            'I_AVAIL_INVENTORY_EVENT',
                            v_timestamp);

            EXECUTE IMMEDIATE
               'ALTER TABLE I_AVAIL_INVENTORY_EVENT ENABLE CONSTRAINT I_AVAIL_INVENTORY_EVENT_FK0';

            archive_pkg.log_archive (NULL,
                                     'I_AVAIL_INVENTORY_EVENT',
                                     v_count_tab2,
                                     NULL,
                                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

            imp_table_dump (p_datapump_directory,
                            'I_AVAIL_INVENTORY_EVENT_DETAIL',
                            v_timestamp);

            EXECUTE IMMEDIATE
               'ALTER TABLE I_AVAIL_INVENTORY_EVENT_DETAIL ENABLE CONSTRAINT I_AVL_INVTRY_EVENT_DTL_FK0';

            archive_pkg.log_archive (NULL,
                                     'I_AVAIL_INVENTORY_EVENT_DETAIL',
                                     v_count_tab1,
                                     NULL,
                                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

            archive_pkg.log_archive (
               NULL,
               'Purge_History_Data - MASTER_COMMERCE_EVENT - End',
               NULL,
               NULL,
               SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
         EXCEPTION
            WHEN OTHERS
            THEN
               archive_pkg.log_error (
                  SQLCODE,
                  'Error in Purge_History_Data Procedure');

               SELECT COUNT (*)
                 INTO v_cons_count
                 FROM user_constraints
                WHERE     constraint_name = 'I_AVL_INVTRY_EVENT_DTL_FK0'
                      AND status = 'DISABLED';

               IF v_cons_count = 1
               THEN
                  EXECUTE IMMEDIATE
                     'ALTER TABLE I_AVAIL_INVENTORY_EVENT_DETAIL ENABLE CONSTRAINT I_AVL_INVTRY_EVENT_DTL_FK0';
               END IF;

               v_cons_count := 0;

               SELECT COUNT (*)
                 INTO v_cons_count
                 FROM user_constraints
                WHERE     constraint_name = 'I_AVAIL_INVENTORY_EVENT_FK0'
                      AND status = 'DISABLED';

               IF v_cons_count = 1
               THEN
                  EXECUTE IMMEDIATE
                     'ALTER TABLE I_AVAIL_INVENTORY_EVENT ENABLE CONSTRAINT I_AVAIL_INVENTORY_EVENT_FK0';
               END IF;

               v_cons_count := 0;

               SELECT COUNT (*)
                 INTO v_cons_count
                 FROM user_constraints
                WHERE     constraint_name = 'I_FULFILLMENT_OUTAGE_EVENT_FK0'
                      AND status = 'DISABLED';

               IF v_cons_count = 1
               THEN
                  EXECUTE IMMEDIATE
                     'ALTER TABLE I_FULFILLMENT_OUTAGE_EVENT ENABLE CONSTRAINT I_FULFILLMENT_OUTAGE_EVENT_FK0';
               END IF;

               v_cons_count := 0;

               SELECT COUNT (*)
                 INTO v_cons_count
                 FROM user_constraints A, user_constraints B
                WHERE     A.CONSTRAINT_NAME = B.R_CONSTRAINT_NAME
                      AND a.table_name = 'MASTER_COMMERCE_EVENT'
                      AND b.table_name = 'I_REBUILD_STAGING'
                      AND B.CONSTRAINT_TYPE = 'R'
                      AND b.status = 'DISABLED';

               IF v_cons_count = 1
               THEN
                  SELECT    'ALTER TABLE '
                         || B.TABLE_NAME
                         || ' ENABLE CONSTRAINT '
                         || B.CONSTRAINT_NAME
                    INTO v_temp
                    FROM user_constraints A, user_constraints B
                   WHERE     A.CONSTRAINT_NAME = B.R_CONSTRAINT_NAME
                         AND a.table_name = 'MASTER_COMMERCE_EVENT'
                         AND b.table_name = 'I_REBUILD_STAGING'
                         AND B.CONSTRAINT_TYPE = 'R';

                  EXECUTE IMMEDIATE (v_temp);
               END IF;

               v_cons_count := 0;

               SELECT COUNT (*)
                 INTO v_cons_count
                 FROM user_constraints A, user_constraints B
                WHERE     A.CONSTRAINT_NAME = B.R_CONSTRAINT_NAME
                      AND a.table_name = 'MASTER_COMMERCE_EVENT'
                      AND b.table_name = 'I_AVAIL_SYNC_STAGING'
                      AND B.CONSTRAINT_TYPE = 'R'
                      AND b.status = 'DISABLED';

               IF v_cons_count = 1
               THEN
                  SELECT    'ALTER TABLE '
                         || B.TABLE_NAME
                         || ' ENABLE CONSTRAINT '
                         || B.CONSTRAINT_NAME
                    INTO v_temp
                    FROM user_constraints A, user_constraints B
                   WHERE     A.CONSTRAINT_NAME = B.R_CONSTRAINT_NAME
                         AND a.table_name = 'MASTER_COMMERCE_EVENT'
                         AND b.table_name = 'I_AVAIL_SYNC_STAGING'
                         AND B.CONSTRAINT_TYPE = 'R';

                  EXECUTE IMMEDIATE (v_temp);
               END IF;

               raise_application_error (
                  -20005,
                  'Error in history_purge_data procedure execution.');
         END;
      ELSIF p_table_name = 'I_INV_EVENT_LOG'
      THEN
         BEGIN
            archive_pkg.log_archive (
               NULL,
               'Purge_History_Data - I_INV_EVENT_LOG - Begin',
               NULL,
               NULL,
               SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

            QUIET_DROP ('TABLE', 'INV_PURGE_PROC_GTT2');

            BEGIN
               EXECUTE IMMEDIATE
                  'LOCK TABLE I_INV_EVENT_LOG IN SHARE MODE NOWAIT';

               EXECUTE IMMEDIATE
                  'LOCK TABLE I_PERPETUAL_INV_LOG IN SHARE MODE NOWAIT';

               EXECUTE IMMEDIATE
                  'LOCK TABLE I_SEGMENTED_INV_LOG IN SHARE MODE NOWAIT';
            EXCEPTION
               WHEN OTHERS
               THEN
                  archive_pkg.log_error (SQLCODE,
                                         'Table locked by other transaction');
                  raise_application_error (
                     -20005,
                     'Table locked by other transaction');
            END;

            EXECUTE IMMEDIATE
                  'CREATE TABLE INV_PURGE_PROC_GTT2 TABLESPACE DOM_DT_TBS AS SELECT INV_EVENT_LOG_ID from I_INV_EVENT_LOG
          WHERE CREATED_DTTM < TO_DATE('''
               || garchivebasedttm
               || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
               || p_retention_days;

            v_where_clause :=
               'WHERE INV_EVENT_LOG_ID NOT IN (SELECT INV_EVENT_LOG_ID FROM INV_PURGE_PROC_GTT2)';

            v_sql :=
               'SELECT COUNT(1) FROM I_PERPETUAL_INV_LOG WHERE INV_EVENT_LOG_ID IN (SELECT INV_EVENT_LOG_ID FROM INV_PURGE_PROC_GTT2)';

            EXECUTE IMMEDIATE (v_sql) INTO v_count_tab1;

            exp_table_dump (p_datapump_directory,
                            'I_PERPETUAL_INV_LOG',
                            v_where_clause,
                            v_timestamp);
            v_where_clause :=
               'WHERE INV_EVENT_LOG_ID NOT IN (SELECT INV_EVENT_LOG_ID FROM INV_PURGE_PROC_GTT2)';

            v_sql :=
               'SELECT COUNT(1) FROM I_SEGMENTED_INV_LOG WHERE INV_EVENT_LOG_ID IN (SELECT INV_EVENT_LOG_ID FROM INV_PURGE_PROC_GTT2)';

            EXECUTE IMMEDIATE (v_sql) INTO v_count_tab2;

            exp_table_dump (p_datapump_directory,
                            'I_SEGMENTED_INV_LOG',
                            v_where_clause,
                            v_timestamp);
            v_where_clause :=
               'WHERE INV_EVENT_LOG_ID NOT IN (SELECT INV_EVENT_LOG_ID FROM INV_PURGE_PROC_GTT2)';

            v_sql :=
               'SELECT COUNT(1) FROM I_INV_EVENT_LOG WHERE INV_EVENT_LOG_ID IN (SELECT INV_EVENT_LOG_ID FROM INV_PURGE_PROC_GTT2)';

            EXECUTE IMMEDIATE (v_sql) INTO v_count_tab3;

            exp_table_dump (p_datapump_directory,
                            'I_INV_EVENT_LOG',
                            v_where_clause,
                            v_timestamp);

            EXECUTE IMMEDIATE
               'ALTER TABLE I_SEGMENTED_INV_LOG DISABLE CONSTRAINT I_SEGMENTED_INV_LOG_FK0';

            EXECUTE IMMEDIATE
               'ALTER TABLE I_PERPETUAL_INV_LOG DISABLE CONSTRAINT I_PERPETUAL_INV_LOG_FK0';

            imp_table_dump (p_datapump_directory,
                            'I_INV_EVENT_LOG',
                            v_timestamp);

            archive_pkg.log_archive (NULL,
                                     'I_INV_EVENT_LOG',
                                     v_count_tab3,
                                     NULL,
                                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

            imp_table_dump (p_datapump_directory,
                            'I_SEGMENTED_INV_LOG',
                            v_timestamp);

            EXECUTE IMMEDIATE
               'ALTER TABLE I_SEGMENTED_INV_LOG ENABLE CONSTRAINT I_SEGMENTED_INV_LOG_FK0';

            archive_pkg.log_archive (NULL,
                                     'I_SEGMENTED_INV_LOG',
                                     v_count_tab2,
                                     NULL,
                                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

            imp_table_dump (p_datapump_directory,
                            'I_PERPETUAL_INV_LOG',
                            v_timestamp);

            EXECUTE IMMEDIATE
               'ALTER TABLE I_PERPETUAL_INV_LOG ENABLE CONSTRAINT I_PERPETUAL_INV_LOG_FK0';

            archive_pkg.log_archive (NULL,
                                     'I_PERPETUAL_INV_LOG',
                                     v_count_tab1,
                                     NULL,
                                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

            archive_pkg.log_archive (
               NULL,
               'Purge_History_Data - I_INV_EVENT_LOG - End',
               NULL,
               NULL,
               SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
         EXCEPTION
            WHEN OTHERS
            THEN
               archive_pkg.log_error (
                  SQLCODE,
                  'Error in Purge_History_Data Procedure');

               SELECT COUNT (*)
                 INTO v_cons_count
                 FROM user_constraints
                WHERE     constraint_name = 'I_SEGMENTED_INV_LOG_FK0'
                      AND status = 'DISABLED';

               IF v_cons_count = 1
               THEN
                  EXECUTE IMMEDIATE
                     'ALTER TABLE I_SEGMENTED_INV_LOG ENABLE CONSTRAINT I_SEGMENTED_INV_LOG_FK0';
               END IF;

               v_cons_count := 0;

               SELECT COUNT (*)
                 INTO v_cons_count
                 FROM user_constraints
                WHERE     constraint_name = 'I_PERPETUAL_INV_LOG_FK0'
                      AND status = 'DISABLED';

               IF v_cons_count = 1
               THEN
                  EXECUTE IMMEDIATE
                     'ALTER TABLE I_PERPETUAL_INV_LOG ENABLE CONSTRAINT I_PERPETUAL_INV_LOG_FK0';
               END IF;

               raise_application_error (
                  -20005,
                  'Error in history_purge_data procedure execution.');
         END;
      ELSIF p_table_name = 'MASTER_STAGING_DATA'
      THEN
         BEGIN
            archive_pkg.log_archive (
               NULL,
               'Purge_History_Data - MASTER_STAGING_DATA - Begin',
               NULL,
               NULL,
               SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

            quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT');

            BEGIN
               EXECUTE IMMEDIATE
                  'LOCK TABLE MASTER_STAGING_DATA IN SHARE MODE NOWAIT';

               EXECUTE IMMEDIATE
                  'LOCK TABLE I_EVENT_DEPENDENCY IN SHARE MODE NOWAIT';

               EXECUTE IMMEDIATE
                  'LOCK TABLE I_EVENT_STAGING IN SHARE MODE NOWAIT';

               EXECUTE IMMEDIATE
                  'LOCK TABLE I_SYNC_PROCESS_CONTROL IN SHARE MODE NOWAIT';

               EXECUTE IMMEDIATE
                  'LOCK TABLE I_SYNC_STAGING IN SHARE MODE NOWAIT';

               EXECUTE IMMEDIATE
                  'LOCK TABLE I_PLAN_DETAIL_STAGING IN SHARE MODE NOWAIT';

               EXECUTE IMMEDIATE
                  'LOCK TABLE I_SEGMENT_PLAN_STAGING IN SHARE MODE NOWAIT';
            EXCEPTION
               WHEN OTHERS
               THEN
                  archive_pkg.log_error (SQLCODE,
                                         'Table locked by other transaction');
                  raise_application_error (
                     -20005,
                     'Table locked by other transaction');
            END;

            EXECUTE IMMEDIATE
                  'CREATE TABLE PURGE_STAGING_EVENT_GTT TABLESPACE DOM_DT_TBS AS SELECT event_id,SYNC_TRANSACTION_ID
            FROM MASTER_STAGING_DATA WHERE EVENT_STATUS IN
                                                 (SELECT SYS_CODE_ID
                                                    FROM SYS_CODE
                                                   WHERE CODE_TYPE = ''I08''
                                                         AND REC_TYPE = ''S''
                                                         AND CODE_DESC IN
                                                                (''Success'',
                                                                 ''Validation Failure'',
                                                                 ''Error''))
                                              AND LAST_UPDATED_DTTM <
                                                     TO_DATE('''
               || garchivebasedttm
               || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
               || p_retention_days;

            v_where_clause :=
               'WHERE event_id NOT IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT)';

            v_sql :=
               'SELECT COUNT(1) FROM I_EVENT_DEPENDENCY WHERE event_id IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT)';

            EXECUTE IMMEDIATE (v_sql) INTO v_count_tab1;

            exp_table_dump (p_datapump_directory,
                            'I_EVENT_DEPENDENCY',
                            v_where_clause,
                            v_timestamp);
            v_where_clause :=
               'WHERE event_id NOT IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT)';

            v_sql :=
               'SELECT COUNT(1) FROM I_EVENT_STAGING WHERE event_id IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT)';

            EXECUTE IMMEDIATE (v_sql) INTO v_count_tab2;

            exp_table_dump (p_datapump_directory,
                            'I_EVENT_STAGING',
                            v_where_clause,
                            v_timestamp);
            v_where_clause :=
               'WHERE TRANSACTION_NUMBER NOT IN (SELECT SYNC_TRANSACTION_ID FROM PURGE_STAGING_EVENT_GTT WHERE SYNC_TRANSACTION_ID IS NOT NULL)';

            v_sql :=
               'SELECT COUNT(1) FROM I_SYNC_PROCESS_CONTROL WHERE TRANSACTION_NUMBER NOT IN (SELECT SYNC_TRANSACTION_ID FROM PURGE_STAGING_EVENT_GTT WHERE SYNC_TRANSACTION_ID IS NOT NULL)';

            EXECUTE IMMEDIATE (v_sql) INTO v_count_tab3;

            exp_table_dump (p_datapump_directory,
                            'I_SYNC_PROCESS_CONTROL',
                            v_where_clause,
                            v_timestamp);
            v_where_clause :=
               'WHERE TRANSACTION_NUMBER NOT IN (SELECT SYNC_TRANSACTION_ID FROM PURGE_STAGING_EVENT_GTT WHERE SYNC_TRANSACTION_ID IS NOT NULL)';

            v_sql :=
               'SELECT COUNT(1) FROM I_SYNC_STAGING WHERE TRANSACTION_NUMBER NOT IN (SELECT SYNC_TRANSACTION_ID FROM PURGE_STAGING_EVENT_GTT WHERE SYNC_TRANSACTION_ID IS NOT NULL)';

            EXECUTE IMMEDIATE (v_sql) INTO v_count_tab4;

            exp_table_dump (p_datapump_directory,
                            'I_SYNC_STAGING',
                            v_where_clause,
                            v_timestamp);
            v_where_clause :=
               'WHERE EVENT_ID NOT IN (SELECT EVENT_ID FROM PURGE_STAGING_EVENT_GTT)';

            v_sql :=
               'SELECT COUNT(1) FROM I_PLAN_DETAIL_STAGING WHERE EVENT_ID IN (SELECT EVENT_ID FROM PURGE_STAGING_EVENT_GTT)';

            EXECUTE IMMEDIATE (v_sql) INTO v_count_tab5;

            exp_table_dump (p_datapump_directory,
                            'I_PLAN_DETAIL_STAGING',
                            v_where_clause,
                            v_timestamp);
            v_where_clause :=
               'WHERE EVENT_ID NOT IN (SELECT EVENT_ID FROM PURGE_STAGING_EVENT_GTT)';

            v_sql :=
               'SELECT COUNT(1) FROM I_SEGMENT_PLAN_STAGING WHERE EVENT_ID IN (SELECT EVENT_ID FROM PURGE_STAGING_EVENT_GTT)';

            EXECUTE IMMEDIATE (v_sql) INTO v_count_tab6;

            exp_table_dump (p_datapump_directory,
                            'I_SEGMENT_PLAN_STAGING',
                            v_where_clause,
                            v_timestamp);
            v_where_clause :=
               'WHERE EVENT_ID NOT IN (SELECT EVENT_ID FROM PURGE_STAGING_EVENT_GTT)';

            v_sql :=
               'SELECT COUNT(1) FROM MASTER_STAGING_DATA WHERE EVENT_ID IN (SELECT EVENT_ID FROM PURGE_STAGING_EVENT_GTT)';

            EXECUTE IMMEDIATE (v_sql) INTO v_count_tab7;

            exp_table_dump (p_datapump_directory,
                            'MASTER_STAGING_DATA',
                            v_where_clause,
                            v_timestamp);

            EXECUTE IMMEDIATE
               'ALTER TABLE I_SEGMENT_PLAN_STAGING DISABLE CONSTRAINT I_SEGMENT_PLAN_FK0';

            EXECUTE IMMEDIATE
               'ALTER TABLE I_EVENT_STAGING DISABLE CONSTRAINT I_EVENT_STAGING_FK0';

            EXECUTE IMMEDIATE
               'ALTER TABLE I_EVENT_DEPENDENCY DISABLE CONSTRAINT FK_I_EVENT_DEPENDENCY';

            imp_table_dump (p_datapump_directory,
                            'MASTER_STAGING_DATA',
                            v_timestamp);

            archive_pkg.log_archive (NULL,
                                     'MASTER_STAGING_DATA',
                                     v_count_tab7,
                                     NULL,
                                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

            imp_table_dump (p_datapump_directory,
                            'I_SEGMENT_PLAN_STAGING',
                            v_timestamp);

            EXECUTE IMMEDIATE
               'ALTER TABLE I_SEGMENT_PLAN_STAGING ENABLE CONSTRAINT I_SEGMENT_PLAN_FK0';

            archive_pkg.log_archive (NULL,
                                     'I_SEGMENT_PLAN_STAGING',
                                     v_count_tab6,
                                     NULL,
                                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

            imp_table_dump (p_datapump_directory,
                            'I_PLAN_DETAIL_STAGING',
                            v_timestamp);

            archive_pkg.log_archive (NULL,
                                     'I_PLAN_DETAIL_STAGING',
                                     v_count_tab5,
                                     NULL,
                                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

            imp_table_dump (p_datapump_directory,
                            'I_SYNC_STAGING',
                            v_timestamp);

            archive_pkg.log_archive (NULL,
                                     'I_SYNC_STAGING',
                                     v_count_tab4,
                                     NULL,
                                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

            imp_table_dump (p_datapump_directory,
                            'I_SYNC_PROCESS_CONTROL',
                            v_timestamp);

            archive_pkg.log_archive (NULL,
                                     'I_SYNC_PROCESS_CONTROL',
                                     v_count_tab3,
                                     NULL,
                                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

            imp_table_dump (p_datapump_directory,
                            'I_EVENT_STAGING',
                            v_timestamp);

            EXECUTE IMMEDIATE
               'ALTER TABLE I_EVENT_STAGING ENABLE CONSTRAINT I_EVENT_STAGING_FK0';

            archive_pkg.log_archive (NULL,
                                     'I_EVENT_STAGING',
                                     v_count_tab2,
                                     NULL,
                                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

            imp_table_dump (p_datapump_directory,
                            'I_EVENT_DEPENDENCY',
                            v_timestamp);

            EXECUTE IMMEDIATE
               'ALTER TABLE I_EVENT_DEPENDENCY ENABLE CONSTRAINT FK_I_EVENT_DEPENDENCY';

            archive_pkg.log_archive (NULL,
                                     'I_EVENT_DEPENDENCY',
                                     v_count_tab1,
                                     NULL,
                                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

            archive_pkg.log_archive (
               NULL,
               'Purge_History_Data - MASTER_STAGING_DATA - End',
               NULL,
               NULL,
               SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
         EXCEPTION
            WHEN OTHERS
            THEN
               archive_pkg.log_error (
                  SQLCODE,
                  'Error in Purge_History_Data Procedure');

               SELECT COUNT (*)
                 INTO v_cons_count
                 FROM user_constraints
                WHERE     constraint_name = 'I_SEGMENT_PLAN_FK0'
                      AND status = 'DISABLED';

               IF v_cons_count = 1
               THEN
                  EXECUTE IMMEDIATE
                     'ALTER TABLE I_SEGMENT_PLAN_STAGING ENABLE CONSTRAINT I_SEGMENT_PLAN_FK0';
               END IF;

               v_cons_count := 0;

               SELECT COUNT (*)
                 INTO v_cons_count
                 FROM user_constraints
                WHERE     constraint_name = 'I_EVENT_STAGING_FK0'
                      AND status = 'DISABLED';

               IF v_cons_count = 1
               THEN
                  EXECUTE IMMEDIATE
                     'ALTER TABLE I_EVENT_STAGING ENABLE CONSTRAINT I_EVENT_STAGING_FK0';
               END IF;

               v_cons_count := 0;

               SELECT COUNT (*)
                 INTO v_cons_count
                 FROM user_constraints
                WHERE     constraint_name = 'FK_I_EVENT_DEPENDENCY'
                      AND status = 'DISABLED';

               IF v_cons_count = 1
               THEN
                  EXECUTE IMMEDIATE
                     'ALTER TABLE I_EVENT_DEPENDENCY ENABLE CONSTRAINT FK_I_EVENT_DEPENDENCY';
               END IF;

               raise_application_error (
                  -20005,
                  'Error in history_purge_data procedure execution.');
         END;
      ELSE
         RAISE v_notsupported;
      END IF;
   EXCEPTION
      WHEN v_notsupported
      THEN
         archive_pkg.log_error (SQLCODE,
                                'Purge_History_Data - Not Supported Table');
         raise_application_error (
            -20005,
               'Not Supported for Table - '
            || p_table_name
            || '. Supported only for I_AVAIL_INVENTORY_EVENT, MASTER_COMMERCE_EVENT, MASTER_STAGING_DATA, I_INV_EVENT_LOG tables.');
      WHEN OTHERS
      THEN
         archive_pkg.log_error (SQLCODE,
                                'Error in Purge_History_Data Procedure');
         raise_application_error (
            -20005,
            'Error in history_purge_data procedure execution.');
   END purge_history_data;

   PROCEDURE insert_del_data_bulk (p_prod_table_name   IN VARCHAR2,
                                   p_arch_table_name   IN VARCHAR2,
                                   p_temp_table           VARCHAR2,
                                   p_column_name       IN VARCHAR2,
                                   p_query                VARCHAR2,
                                   p_archive_flag         VARCHAR2)
   IS
      v_ins_sql           VARCHAR2 (32767);
      v_del_sql           VARCHAR2 (32767);
      v_sel_sql           VARCHAR2 (32767);
      v_dblink_name       VARCHAR2 (30) := '@omarchive_link';
      t_col_list          CLOB;
      n_chunk_size        PLS_INTEGER := 1000000;
      n_count             PLS_INTEGER := 0;
      v_schema_owner      VARCHAR2 (100);
      v_prod_table_name   VARCHAR2 (30);
      v_rec_count         PLS_INTEGER := 0;
      gvprocedurename     VARCHAR2 (100);
      atccompanyid        company.company_id%TYPE;
      varchiveflag        NUMBER (1) := 0;
   BEGIN
      gvprocedurename := p_prod_table_name;

      v_prod_table_name := UPPER (p_prod_table_name);

      n_chunk_size := (gcommitfrequency / n_parallel_level);

      SELECT SYS_CONTEXT ('userenv', 'current_schema')
        INTO v_schema_owner
        FROM DUAL;

      --Check for existance of records to be archived

      IF p_query IS NULL
      THEN
         v_sel_sql :=
               'SELECT COUNT(1) FROM '
            || v_prod_table_name
            || ' a '
            || ' WHERE '
            || p_column_name
            || ' IN (SELECT '
            || p_column_name
            || ' FROM '
            || p_temp_table
            || ' WHERE IS_ARCHIVED=-1)';
      ELSE
         v_sel_sql :=
               'SELECT COUNT(1) FROM '
            || v_prod_table_name
            || ' a '
            || ' WHERE '
            || p_query;
      END IF;


      EXECUTE IMMEDIATE (v_sel_sql) INTO v_rec_count;

      -- Backporting USTL  HEB changes from 2013
      IF v_rec_count > 0
      THEN
         IF varchiveflag = 0 AND UPPER (p_archive_flag) = 'Y'
         THEN
            t_col_list := get_col_list (v_prod_table_name);

            IF p_query IS NULL
            THEN
               v_ins_sql :=
                     'INSERT INTO '
                  || p_arch_table_name
                  || v_dblink_name
                  || ' ( '
                  || t_col_list
                  || ') SELECT '
                  || t_col_list
                  || ' FROM '
                  || v_prod_table_name
                  || ' WHERE '
                  || p_column_name
                  || ' IN (SELECT '
                  || p_column_name
                  || ' FROM '
                  || p_temp_table
                  || ' WHERE IS_ARCHIVED=-1)';
            ELSE
               v_ins_sql :=
                     'INSERT INTO '
                  || p_arch_table_name
                  || v_dblink_name
                  || ' ( '
                  || t_col_list
                  || ') SELECT '
                  || t_col_list
                  || ' FROM '
                  || v_prod_table_name
                  || ' WHERE '
                  || p_query;
            END IF;

            EXECUTE IMMEDIATE (v_ins_sql);
         END IF;

         -- drop task  del_records_task if it exist
         SELECT COUNT (*)
           INTO n_count
           FROM user_parallel_execute_tasks
          WHERE task_name = 'del_records_task';

         IF (n_count > 0)
         THEN
            DBMS_PARALLEL_EXECUTE.drop_task ('del_records_task');
         END IF;

         -- create tasks to run in parallel chunks

         DBMS_PARALLEL_EXECUTE.create_task (task_name => 'del_records_task');

         IF p_query IS NULL
         THEN
            v_del_sql :=
                  'DELETE  FROM '
               || v_prod_table_name
               || ' a '
               || ' WHERE '
               || p_column_name
               || ' IN (SELECT '
               || p_column_name
               || ' FROM '
               || p_temp_table
               || ' WHERE IS_ARCHIVED=-1)'
               || ' AND rowid between :start_id and :end_id';
         ELSE
            v_del_sql :=
                  'DELETE  FROM '
               || v_prod_table_name
               || ' a '
               || ' WHERE '
               || p_query
               || ' AND rowid between :start_id and :end_id';
         END IF;


         --creating chunks and assigning taskname to it

         DBMS_PARALLEL_EXECUTE.create_chunks_by_rowid (
            task_name     => 'del_records_task',
            table_owner   => v_schema_owner,
            table_name    => v_prod_table_name,
            by_row        => TRUE,
            chunk_size    => n_chunk_size);

         --execute task in parallel
         IF gv_job_class_chk = 1
         THEN
            DBMS_PARALLEL_EXECUTE.run_task (
               task_name        => 'del_records_task',
               sql_stmt         => v_del_sql,
               language_flag    => DBMS_SQL.native,
               parallel_level   => n_parallel_level,
               job_class        => 'MA_ARCH_JOB_CLASS');
         ELSE
            DBMS_PARALLEL_EXECUTE.run_task (
               task_name        => 'del_records_task',
               sql_stmt         => v_del_sql,
               language_flag    => DBMS_SQL.native,
               parallel_level   => n_parallel_level);
         END IF;


         --2013.1
         n_count := 0;

         SELECT COUNT (1)
           INTO n_count
           FROM user_parallel_execute_chunks
          WHERE     status = 'PROCESSED_WITH_ERROR'
                AND task_name = 'del_records_task';

         IF n_count > 0
         THEN
            --Delete the records from archive schema if delete task fails
            IF UPPER (p_archive_flag) = 'Y'
            THEN
               IF p_query IS NULL
               THEN
                  v_del_sql :=
                        'DELETE FROM '
                     || p_arch_table_name
                     || v_dblink_name
                     || ' WHERE '
                     || p_column_name
                     || ' IN (SELECT '
                     || p_column_name
                     || ' FROM '
                     || p_temp_table
                     || ' WHERE IS_ARCHIVED=-1)';
               ELSE
                  v_del_sql :=
                        'DELETE FROM '
                     || p_arch_table_name
                     || v_dblink_name
                     || ' WHERE '
                     || p_query;
               END IF;

               EXECUTE IMMEDIATE (v_del_sql);

               COMMIT;
            END IF;

            --Log the error message using the dictionary table

            SELECT ERROR_CODE, error_message
              INTO nsqlcode, vsqlerrm
              FROM user_parallel_execute_chunks
             WHERE     LOWER (task_name) = 'del_records_task'
                   AND status = 'PROCESSED_WITH_ERROR'
                   AND ROWNUM < 2;

            archive_pkg.log_error (nsqlcode, vsqlerrm);
            RAISE gdelete_error;
         END IF;

         IF     (   v_prod_table_name = 'PURCHASE_ORDERS'
                 OR v_prod_table_name = 'ORDERS'
                 OR v_prod_table_name = 'LPN'
                 OR v_prod_table_name = 'SHIPMENT'
                 OR v_prod_table_name = 'ASN')
            AND p_archive_flag = 'Y'
         THEN
            v_ins_sql :=
                  'INSERT INTO SOLR_ARCHIVE_INFO (TABLE_NAME, PK_COLUMN, ARCHIVED_PK_VALUE)  (select  '''
               || v_prod_table_name
               || ''','''
               || p_column_name
               || ''','
               || p_column_name
               || ' from '
               || p_temp_table
               || ' WHERE IS_ARCHIVED=-1)';


            EXECUTE IMMEDIATE (v_ins_sql);
         END IF;

         COMMIT;
      END IF;

      archive_pkg.log_archive (atccompanyid,
                               gvprocedurename,
                               v_rec_count,
                               NULL,
                               SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
   END insert_del_data_bulk;

   PROCEDURE update_purge_status (purge_days IN NUMBER, status IN VARCHAR2)
   AS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      IF status = 'Failed'
      THEN
         UPDATE ARCHIVE_STATUS
            SET arch_run_status = 'Order_Data_Purge_Pkg - Failed',
                arch_end_dttm = SYSDATE
          WHERE     arch_run_status LIKE 'Order_Data_Purge_Pkg%'
                AND tc_company_id = purge_days;
      ELSIF status = 'Success'
      THEN
         UPDATE ARCHIVE_STATUS
            SET arch_run_status = 'Order_Data_Purge_Pkg - Completed',
                arch_end_dttm = SYSDATE
          WHERE     arch_run_status = 'Order_Data_Purge_Pkg - Running'
                AND tc_company_id = purge_days;
      END IF;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         archive_pkg.log_error (nsqlcode,
                                'Error in update_purge_status' || vsqlerrm);
   END update_purge_status;

   PROCEDURE INV_BUSSINESS_RULES_PURGE_PROC (PURGE_DAYS IN NUMBER)
   AS
      BUSS_PURGE_DAYS   NUMBER;
      v_count           NUMBER := 0;
   BEGIN
      EXECUTE IMMEDIATE
         'alter session set NLS_TIMESTAMP_FORMAT = ''YYYY-MM-DD HH24:MI:SS.FF''';

      EXECUTE IMMEDIATE
         'alter session set NLS_DATE_FORMAT = ''YYYY-MM-DD HH24:MI:SS''';

      -- Get Dynamic Parallel Level and Commit Frequency
      SELECT NVL (company_parameter.param_value, param_def.dflt_value)
        INTO gcommitfrequency
        FROM param_def, company_parameter
       WHERE     param_def.param_def_id = company_parameter.param_def_id(+)
             AND param_def.param_group_id =
                    company_parameter.param_group_id(+)
             AND param_def.param_def_id = 'ARCHIVE_COMMIT_FREQUENCY'
             AND param_def.param_group_id = 'ARCHIVE'
             AND param_def.is_disabled = 0;

      SELECT NVL (company_parameter.param_value, param_def.dflt_value)
        INTO n_parallel_level
        FROM param_def, company_parameter
       WHERE     param_def.param_def_id = company_parameter.param_def_id(+)
             AND param_def.param_group_id =
                    company_parameter.param_group_id(+)
             AND param_def.param_def_id = 'ARCHIVE_PARALLEL_THREADS'
             AND param_def.param_group_id = 'ARCHIVE'
             AND param_def.is_disabled = 0;

      -- Check to execute the purge process in one node in case of RAC environment using  MA_ARCH_JOB_CLASS.
      -- This job class need to be created expilicitly only in case RAC setup. If the job class is not created, purge process will execute using DEFAULT_JOB_CLASS
      SELECT COUNT (1)
        INTO gv_job_class_chk
        FROM all_scheduler_job_classes
       WHERE job_class_name = 'MA_ARCH_JOB_CLASS';


      gvprocedurename :=
         'ORDER_DATA_PURGE_PKG.INV_BUSSINESS_RULES_PURGE_PROC - Begin';

      archive_pkg.log_archive (NULL,
                               gvprocedurename,
                               NULL,
                               NULL,
                               SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

      BEGIN
         SELECT COUNT (1)
           INTO v_count
           FROM PURGE_CRITERIA
          WHERE PURGE_GROUP = 'INVENTORY_BUSS' AND purge_module = 'OLM';

         IF v_count > 0
         THEN
            SELECT NO_OF_DAYS
              INTO BUSS_PURGE_DAYS
              FROM PURGE_CRITERIA
             WHERE PURGE_GROUP = 'INVENTORY_BUSS' AND purge_module = 'OLM';
         ELSE
            BUSS_PURGE_DAYS := PURGE_DAYS;
         END IF;

         quiet_drop ('TABLE', 'INV_BUSSINESS_RULES_PURGE_GTT');

         EXECUTE IMMEDIATE
               'CREATE TABLE INV_BUSSINESS_RULES_PURGE_GTT TABLESPACE DOM_DT_TBS AS SELECT A_identity FROM A_PROCESS_TEMPLATE WHERE (IS_DELETED = 1
            AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || BUSS_PURGE_DAYS
            || ') OR (EXPIRY_DATE < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || BUSS_PURGE_DAYS
            || ')';


         INSERT_DEL_DATA_BULK (
            'A_ALLOC_GROUP_PARAM',
            '',
            '',
            '',
            'DOM_PROCESS_TEMPLATE_ID IN (SELECT /*+ parallel*/ A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');


         INSERT_DEL_DATA_BULK (
            'A_DO_ATTR_TEMPLATE_VALUES',
            '',
            '',
            '',
            'TEMPLATE_ID IN (SELECT /*+ parallel*/ A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'A_DO_MAX_SIZE',
            '',
            '',
            '',
            'A_PROCESS_TEMPLATE_ID IN (SELECT /*+ parallel*/ A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'A_SEGMENTED_INVENTORY',
            '',
            '',
            '',
            'DOM_PROCESS_TEMPLATE_ID IN (SELECT /*+ parallel*/ A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'A_SEGMENT_TEMPLATE_PARAM',
            '',
            '',
            '',
            'DOM_PROCESS_TEMPLATE_ID IN (SELECT /*+ parallel*/ A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'A_SO_OUTBOUND_CHG_MGMT',
            '',
            '',
            '',
            'A_PROCESS_TEMPLATE_ID IN (SELECT /*+ parallel*/ A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'A_PROC_TMPL_FILTER',
            '',
            '',
            '',
            'DOM_PROCESS_TEMPLATE_ID IN (SELECT /*+ parallel*/ A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'RLM_FEE',
            '',
            '',
            '',
            'TEMPLATE_ID IN (SELECT /*+ parallel*/ A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'A_BAND_DETAILS',
            '',
            '',
            '',
            'ALLOC_FULFILL_PARAM_ID IN (select ALLOC_FULFILL_PARAM_ID from A_ALLOC_FULFILL_PARAM where DOM_PROCESS_TEMPLATE_ID IN (SELECT /*+ parallel*/ A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT))',
            'N');

         INSERT_DEL_DATA_BULK (
            'A_TIER_FACILITY_RANK',
            '',
            '',
            '',
            'ALLOC_FULFILL_PARAM_ID IN (select ALLOC_FULFILL_PARAM_ID from A_ALLOC_FULFILL_PARAM where DOM_PROCESS_TEMPLATE_ID IN (SELECT /*+ parallel*/ A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT))',
            'N');

         INSERT_DEL_DATA_BULK (
            'A_ALLOC_RULE_SEGEMENT',
            '',
            '',
            '',
            'ALLOC_FULFILL_PARAM_ID IN (select ALLOC_FULFILL_PARAM_ID from A_ALLOC_FULFILL_PARAM where DOM_PROCESS_TEMPLATE_ID IN (SELECT /*+ parallel*/ A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT))',
            'N');

         INSERT_DEL_DATA_BULK (
            'A_ALLOC_FULFILL_PARAM',
            '',
            '',
            '',
            'DOM_PROCESS_TEMPLATE_ID IN (SELECT /*+ parallel*/ A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'A_ALLOC_GROUP_PARAM',
            '',
            '',
            '',
            'DOM_PROCESS_TEMPLATE_ID IN (SELECT /*+ parallel*/ A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');


         INSERT_DEL_DATA_BULK (
            'A_ALLOC_TIER_PARAM',
            '',
            '',
            '',
            'DOM_PROCESS_TEMPLATE_ID IN (SELECT /*+ parallel*/ A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'A_RANKING_SEQUENCE',
            '',
            '',
            '',
            'DOM_PROCESS_TEMPLATE_ID IN (SELECT /*+ parallel*/ A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'A_PROCESS_TEMPLATE',
            '',
            '',
            '',
            'A_identity IN (SELECT /*+ parallel*/ A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'WF_FAILED_ENTITY',
            '',
            '',
            '',
               'CREATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || BUSS_PURGE_DAYS,
            'N');

         quiet_drop ('TABLE', 'INV_BUSSINESS_RULES_PURGE_GTT');

         gvprocedurename :=
            'ORDER_DATA_PURGE_PKG.INV_BUSSINESS_RULES_PURGE_PROC - End';
         archive_pkg.log_archive (NULL,
                                  gvprocedurename,
                                  NULL,
                                  NULL,
                                  SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            update_purge_status (PURGE_DAYS, 'Failed');
            archive_pkg.log_error (SQLCODE,
                                   'No Data Found for BUSI_PURGE purge type');

            quiet_drop ('TABLE', 'INV_BUSSINESS_RULES_PURGE_GTT');
         WHEN OTHERS
         THEN
            update_purge_status (PURGE_DAYS, 'Failed');
            nsqlcode := SQLCODE;
            vsqlerrm := SQLERRM;
            archive_pkg.log_error (
               SQLCODE,
               'Error message in INV_BUSSINESS_RULES_PURGE_PROC: ' || SQLERRM);
            ROLLBACK;
            quiet_drop ('TABLE', 'INV_BUSSINESS_RULES_PURGE_GTT');
      END;
   END INV_BUSSINESS_RULES_PURGE_PROC;

   PROCEDURE COMMERCE_PURGE_PROC (PURGE_DAYS IN NUMBER)
   AS
      COMM_PURGE_DAYS   NUMBER;

      v_count           NUMBER (10) := 0;
   BEGIN
      EXECUTE IMMEDIATE
         'alter session set NLS_TIMESTAMP_FORMAT = ''YYYY-MM-DD HH24:MI:SS.FF''';

      EXECUTE IMMEDIATE
         'alter session set NLS_DATE_FORMAT = ''YYYY-MM-DD HH24:MI:SS''';

      -- Get Dynamic Parallel Level and Commit Frequency
      SELECT NVL (company_parameter.param_value, param_def.dflt_value)
        INTO gcommitfrequency
        FROM param_def, company_parameter
       WHERE     param_def.param_def_id = company_parameter.param_def_id(+)
             AND param_def.param_group_id =
                    company_parameter.param_group_id(+)
             AND param_def.param_def_id = 'ARCHIVE_COMMIT_FREQUENCY'
             AND param_def.param_group_id = 'ARCHIVE'
             AND param_def.is_disabled = 0;

      SELECT NVL (company_parameter.param_value, param_def.dflt_value)
        INTO n_parallel_level
        FROM param_def, company_parameter
       WHERE     param_def.param_def_id = company_parameter.param_def_id(+)
             AND param_def.param_group_id =
                    company_parameter.param_group_id(+)
             AND param_def.param_def_id = 'ARCHIVE_PARALLEL_THREADS'
             AND param_def.param_group_id = 'ARCHIVE'
             AND param_def.is_disabled = 0;

      -- Check to execute the purge process in one node in case of RAC environment using  MA_ARCH_JOB_CLASS.
      -- This job class need to be created expilicitly only in case RAC setup. If the job class is not created, purge process will execute using DEFAULT_JOB_CLASS
      SELECT COUNT (1)
        INTO gv_job_class_chk
        FROM all_scheduler_job_classes
       WHERE job_class_name = 'MA_ARCH_JOB_CLASS';


      gvprocedurename := 'ORDER_DATA_PURGE_PKG.COMMERCE_PURGE_PROC - Begin';

      archive_pkg.log_archive (NULL,
                               gvprocedurename,
                               NULL,
                               NULL,
                               SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

      BEGIN
         SELECT COUNT (1)
           INTO v_count
           FROM PURGE_CRITERIA
          WHERE PURGE_GROUP = 'INVENTORY_COMM' AND purge_module = 'OLM';

         IF v_count > 0
         THEN
            SELECT NO_OF_DAYS
              INTO COMM_PURGE_DAYS
              FROM PURGE_CRITERIA
             WHERE PURGE_GROUP = 'INVENTORY_COMM' AND purge_module = 'OLM';
         ELSE
            COMM_PURGE_DAYS := PURGE_DAYS;
         END IF;

         quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT');
         quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT1');
         quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT2');
         quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT3');
         quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT4');
         quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT5');

         EXECUTE IMMEDIATE
               'CREATE TABLE COMMERCE_PURGE_GTT TABLESPACE DOM_DT_TBS AS SELECT VIEW_RULESET_ID FROM I_VIEW_RULESET WHERE IS_DELETED = 1
            AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS;

         EXECUTE IMMEDIATE
               'CREATE TABLE COMMERCE_PURGE_GTT1 TABLESPACE DOM_DT_TBS AS SELECT CONS_TEMPLATE_ID FROM CONS_TEMPLATE WHERE FILTER_ID IN
        (SELECT FILTER_ID FROM filter WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || ')';

         EXECUTE IMMEDIATE
               'CREATE TABLE COMMERCE_PURGE_GTT2 TABLESPACE DOM_DT_TBS AS SELECT FILTER_ID FROM filter WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS;

         EXECUTE IMMEDIATE
               'CREATE TABLE COMMERCE_PURGE_GTT3 TABLESPACE DOM_DT_TBS AS SELECT VIEW_DEF_ID FROM I_VIEW_DEF WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS;

         EXECUTE IMMEDIATE
               'CREATE TABLE COMMERCE_PURGE_GTT4 TABLESPACE DOM_DT_TBS AS SELECT EVENT_ID FROM MASTER_COMMERCE_EVENT WHERE LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS;

         EXECUTE IMMEDIATE
            'CREATE TABLE COMMERCE_PURGE_GTT5 TABLESPACE DOM_DT_TBS AS SELECT AVAIL_INV_EVENT_ID FROM I_AVAIL_INVENTORY_EVENT WHERE EVENT_ID IN (SELECT EVENT_ID FROM COMMERCE_PURGE_GTT4)';

         INSERT_DEL_DATA_BULK (
            'I_ASSOCIATED_AVAILABILITY',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_1',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_1 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || ')',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_2',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_2 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || ')',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_3',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_3 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || ')',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_4',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_4 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || ')',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_5',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_5 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || ')',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_6',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_6 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || ')',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_7',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_7 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || ')',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_8',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_8 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || ')',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_9',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_9 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || ')',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_10',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_10 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || ')',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_11',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_11 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || ')',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_12',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_12 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || ')',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_13',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_13 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || ')',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_14',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_14 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || ')',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_15',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_15 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || ')',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_16',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_16 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || ')',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_17',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_17 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || ')',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_18',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_18 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || ')',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_19',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_19 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || ')',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_20',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_20 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || ')',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_1',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_2',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_3',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_4',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_5',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_6',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_7',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_8',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_9',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_10',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_11',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_12',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_13',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_14',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_15',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_16',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_17',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_18',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_19',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_20',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_EXCLUSION',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_VIEW_RULESET_FILTER',
            '',
            '',
            '',
            'VIEW_RULESET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_1',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_1 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_2',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_2 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_3',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_3 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_4',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_4 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_5',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_5 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_6',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_6 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_7',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_7 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_8',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_8 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_9',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_9 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_10',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_10 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_11',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_11 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_12',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_12 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_13',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_13 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_14',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_14 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_15',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_15 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_16',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_16 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_17',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_17 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_18',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_18 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_19',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_19 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_20',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_20 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');

         INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_1',
                               '',
                               '',
                               '',
                               'MARK_FOR_DELETE = 1',
                               'N');
         INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_2',
                               '',
                               '',
                               '',
                               'MARK_FOR_DELETE = 1',
                               'N');
         INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_3',
                               '',
                               '',
                               '',
                               'MARK_FOR_DELETE = 1',
                               'N');
         INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_4',
                               '',
                               '',
                               '',
                               'MARK_FOR_DELETE = 1',
                               'N');
         INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_5',
                               '',
                               '',
                               '',
                               'MARK_FOR_DELETE = 1',
                               'N');
         INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_6',
                               '',
                               '',
                               '',
                               'MARK_FOR_DELETE = 1',
                               'N');
         INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_7',
                               '',
                               '',
                               '',
                               'MARK_FOR_DELETE = 1',
                               'N');
         INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_8',
                               '',
                               '',
                               '',
                               'MARK_FOR_DELETE = 1',
                               'N');
         INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_9',
                               '',
                               '',
                               '',
                               'MARK_FOR_DELETE = 1',
                               'N');
         INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_10',
                               '',
                               '',
                               '',
                               'MARK_FOR_DELETE = 1',
                               'N');
         INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_11',
                               '',
                               '',
                               '',
                               'MARK_FOR_DELETE = 1',
                               'N');
         INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_12',
                               '',
                               '',
                               '',
                               'MARK_FOR_DELETE = 1',
                               'N');
         INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_13',
                               '',
                               '',
                               '',
                               'MARK_FOR_DELETE = 1',
                               'N');
         INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_14',
                               '',
                               '',
                               '',
                               'MARK_FOR_DELETE = 1',
                               'N');
         INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_15',
                               '',
                               '',
                               '',
                               'MARK_FOR_DELETE = 1',
                               'N');
         INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_16',
                               '',
                               '',
                               '',
                               'MARK_FOR_DELETE = 1',
                               'N');
         INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_17',
                               '',
                               '',
                               '',
                               'MARK_FOR_DELETE = 1',
                               'N');
         INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_18',
                               '',
                               '',
                               '',
                               'MARK_FOR_DELETE = 1',
                               'N');
         INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_19',
                               '',
                               '',
                               '',
                               'MARK_FOR_DELETE = 1',
                               'N');
         INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_20',
                               '',
                               '',
                               '',
                               'MARK_FOR_DELETE = 1',
                               'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_1',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_1 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_2',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_2 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_3',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_3 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_4',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_4 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_5',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_5 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_6',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_6 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_7',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_7 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_8',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_8 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_9',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_9 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_10',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_10 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_11',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_11 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_12',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_12 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_13',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_13 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_14',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_14 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_15',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_15 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_16',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_16 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_17',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_17 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_18',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_18 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_19',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_19 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_20',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_20 WHERE RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_1',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_2',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_3',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_4',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_5',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_6',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_7',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_8',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_9',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_10',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_11',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_12',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_13',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_14',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_15',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_16',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_17',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_18',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_19',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_20',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS,
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_1',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_2',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_3',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_4',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_5',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_6',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_7',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_8',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_9',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_10',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_11',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_12',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_13',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_14',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_15',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_16',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_17',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_18',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_19',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_20',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_1_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_2_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_3_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_4_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_5_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_6_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_7_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_8_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_9_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_10_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_11_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_12_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_13_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_14_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_15_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_16_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_17_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_18_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_19_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_20_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_VIEW_RULESET',
            '',
            '',
            '',
            'VIEW_RULESET_ID IN (SELECT /*+ parallel*/ VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'CONSTEMP_DEST_REGION_MAP',
            '',
            '',
            '',
            'CONS_TEMPLATE_ID IN (SELECT /*+ parallel*/ CONS_TEMPLATE_ID FROM COMMERCE_PURGE_GTT1)',
            'N');

         INSERT_DEL_DATA_BULK (
            'CONSTEMP_ORIG_REGION_MAP',
            '',
            '',
            '',
            'CONS_TEMPLATE_ID IN (SELECT /*+ parallel*/ CONS_TEMPLATE_ID FROM COMMERCE_PURGE_GTT1)',
            'N');

         INSERT_DEL_DATA_BULK (
            'CONSTEMP_PRODCLASS_MAP',
            '',
            '',
            '',
            'CONS_TEMPLATE_ID IN (SELECT /*+ parallel*/ CONS_TEMPLATE_ID FROM COMMERCE_PURGE_GTT1)',
            'N');

         INSERT_DEL_DATA_BULK (
            'CONS_OPT_RULE',
            '',
            '',
            '',
            'CONS_TEMPLATE_ID IN (SELECT /*+ parallel*/ CONS_TEMPLATE_ID FROM COMMERCE_PURGE_GTT1)',
            'N');

         INSERT_DEL_DATA_BULK (
            'CONS_TEMPLATE_RES_DRIVER',
            '',
            '',
            '',
            'CONS_TEMPLATE_ID IN (SELECT /*+ parallel*/ CONS_TEMPLATE_ID FROM COMMERCE_PURGE_GTT1)',
            'N');

         INSERT_DEL_DATA_BULK (
            'CONS_TEMPLATE_RES_EQUIP',
            '',
            '',
            '',
            'CONS_TEMPLATE_ID IN (SELECT /*+ parallel*/ CONS_TEMPLATE_ID FROM COMMERCE_PURGE_GTT1)',
            'N');

         INSERT_DEL_DATA_BULK (
            'CONS_TEMPLATE',
            '',
            '',
            '',
            'FILTER_ID IN (SELECT /*+ parallel*/ FILTER_ID FROM COMMERCE_PURGE_GTT2)',
            'N');

         INSERT_DEL_DATA_BULK (
            'A_PROC_TMPL_FILTER',
            '',
            '',
            '',
            'FILTER_ID IN (SELECT /*+ parallel*/ FILTER_ID FROM COMMERCE_PURGE_GTT2)',
            'N');

         INSERT_DEL_DATA_BULK (
            'CONTACT_FILTER_MAP',
            '',
            '',
            '',
            'FILTER_ID IN (SELECT /*+ parallel*/ FILTER_ID FROM COMMERCE_PURGE_GTT2)',
            'N');


         INSERT_DEL_DATA_BULK (
            'FILTER_CRITERIA',
            '',
            '',
            '',
            'FILTER_ID IN (SELECT /*+ parallel*/ FILTER_ID FROM COMMERCE_PURGE_GTT2)',
            'N');

         INSERT_DEL_DATA_BULK (
            'FILTER_GROUPBY_DETAIL',
            '',
            '',
            '',
            'FILTER_ID IN (SELECT /*+ parallel*/ FILTER_ID FROM COMMERCE_PURGE_GTT2)',
            'N');

         INSERT_DEL_DATA_BULK (
            'FILTER_GROUP_BY',
            '',
            '',
            '',
            'FILTER_ID IN (SELECT /*+ parallel*/ FILTER_ID FROM COMMERCE_PURGE_GTT2)',
            'N');

         INSERT_DEL_DATA_BULK (
            'FILTER_GROUP_DEFINITION',
            '',
            '',
            '',
            'FILTER_ID IN (SELECT /*+ parallel*/ FILTER_ID FROM COMMERCE_PURGE_GTT2)',
            'N');

         INSERT_DEL_DATA_BULK (
            'FILTER_ORDERBY_DETAIL',
            '',
            '',
            '',
            'FILTER_ID IN (SELECT /*+ parallel*/ FILTER_ID FROM COMMERCE_PURGE_GTT2)',
            'N');

         INSERT_DEL_DATA_BULK (
            'FILTER_DETAIL',
            '',
            '',
            '',
            'FILTER_ID IN (SELECT /*+ parallel*/ FILTER_ID FROM COMMERCE_PURGE_GTT2)',
            'N');

         INSERT_DEL_DATA_BULK (
            'FILTER',
            '',
            '',
            '',
            'FILTER_ID IN (SELECT /*+ parallel*/ FILTER_ID FROM COMMERCE_PURGE_GTT2)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ALERT_STAGING',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_FUL_OUTAGE_RULE',
            '',
            '',
            '',
               '(IS_DELETED = 1 OR status =  (SELECT SYS_CODE_ID FROM SYS_CODE WHERE REC_TYPE = ''S'' AND CODE_TYPE = ''I23'' AND CODE_ID = ''Expired'')) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');

         --2016 OLM STARTS
         INSERT_DEL_DATA_BULK (
            'I_COMMERCE_STATS_EVENT_ERROR',
            '',
            '',
            '',
               'COMMERCE_VIEW_STATS_EVENT_ID IN (SELECT COMMERCE_VIEW_STATS_EVENT_ID FROM I_COMMERCE_VIEW_STATS_EVENT WHERE LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || ')',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_COMMERCE_VIEW_STATS_EVENT',
            '',
            '',
            '',
               'LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || '',
            'N');
         --2016 OLM ENDS

         INSERT_DEL_DATA_BULK (
            'I_COMMERCE_EVENT',
            '',
            '',
            '',
               'EVENT_ID IN (SELECT /*+ parallel*/ EVENT_ID FROM MASTER_COMMERCE_EVENT WHERE LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || COMM_PURGE_DAYS
            || ')',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAIL_INVENTORY_EVENT_DETAIL',
            '',
            '',
            '',
            'AVAIL_INV_EVENT_ID IN (SELECT AVAIL_INV_EVENT_ID FROM COMMERCE_PURGE_GTT5)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAIL_INVENTORY_EVENT',
            '',
            '',
            '',
            'EVENT_ID IN (SELECT EVENT_ID FROM COMMERCE_PURGE_GTT4)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_FULFILLMENT_OUTAGE_EVENT',
            '',
            '',
            '',
            'EVENT_ID IN (SELECT EVENT_ID FROM COMMERCE_PURGE_GTT4)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_VIEW_OUTAGE_REASON',
            '',
            '',
            '',
            'VIEW_CONFIG_ID IN (SELECT VIEW_CONFIG_ID FROM I_VIEW_CONFIG WHERE VIEW_DEF_ID IN (SELECT /*+ parallel*/ VIEW_DEF_ID FROM COMMERCE_PURGE_GTT3))',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_VIEW_SYNC_DETAIL',
            '',
            '',
            '',
            'VIEW_CONFIG_ID IN (SELECT VIEW_CONFIG_ID FROM I_VIEW_CONFIG WHERE VIEW_DEF_ID IN (SELECT /*+ parallel*/ VIEW_DEF_ID FROM COMMERCE_PURGE_GTT3))',
            'N');


         INSERT_DEL_DATA_BULK (
            'I_VIEW_AVAIL_RULE_DETAIL',
            '',
            '',
            '',
            ' VIEW_AVAIL_RULE_ID IN (SELECT VIEW_AVAIL_RULE_ID FROM I_VIEW_AVAIL_RULE WHERE VIEW_CONFIG_ID IN (SELECT VIEW_CONFIG_ID 
                                              FROM I_VIEW_CONFIG WHERE VIEW_DEF_ID IN (SELECT /*+ parallel*/ VIEW_DEF_ID FROM COMMERCE_PURGE_GTT3)))',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_VIEW_AVAIL_RULE',
            '',
            '',
            '',
            ' VIEW_CONFIG_ID IN (SELECT VIEW_CONFIG_ID FROM I_VIEW_CONFIG WHERE VIEW_DEF_ID IN (SELECT /*+ parallel*/ VIEW_DEF_ID FROM COMMERCE_PURGE_GTT3))',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_VIEW_STORE_EXCLUSION',
            '',
            '',
            '',
            ' VIEW_CONFIG_ID IN (SELECT VIEW_CONFIG_ID FROM I_VIEW_CONFIG WHERE VIEW_DEF_ID IN (SELECT /*+ parallel*/ VIEW_DEF_ID FROM COMMERCE_PURGE_GTT3))',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_VIEW_NETWORK_PROTECTION',
            '',
            '',
            '',
            ' VIEW_CONFIG_ID IN (SELECT VIEW_CONFIG_ID FROM I_VIEW_CONFIG WHERE VIEW_DEF_ID IN (SELECT /*+ parallel*/ VIEW_DEF_ID FROM COMMERCE_PURGE_GTT3))',
            'N');


         INSERT_DEL_DATA_BULK (
            'I_VIEW_METADATA_MAPPING',
            '',
            '',
            '',
            'VIEW_DEF_ID IN (SELECT /*+ parallel*/ VIEW_DEF_ID FROM COMMERCE_PURGE_GTT3)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_VIEW_CONFIG',
            '',
            '',
            '',
            'VIEW_DEF_ID IN (SELECT /*+ parallel*/ VIEW_DEF_ID FROM COMMERCE_PURGE_GTT3)',
            'N');


         INSERT_DEL_DATA_BULK (
            'I_VIEW_DEF_THRESHOLD',
            '',
            '',
            '',
            'VIEW_DEF_ID IN (SELECT /*+ parallel*/ VIEW_DEF_ID FROM COMMERCE_PURGE_GTT3)',
            'N');


         INSERT_DEL_DATA_BULK (
            'I_VIEW_DEF',
            '',
            '',
            '',
            'VIEW_DEF_ID IN (SELECT /*+ parallel*/ VIEW_DEF_ID FROM COMMERCE_PURGE_GTT3)',
            'N');

         quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT');
         quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT1');
         quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT2');
         quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT3');
         quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT4');
         quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT5');
         gvprocedurename := 'ORDER_DATA_PURGE_PKG.COMMERCE_PURGE_PROC - End';

         archive_pkg.log_archive (NULL,
                                  gvprocedurename,
                                  NULL,
                                  NULL,
                                  SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            update_purge_status (PURGE_DAYS, 'Failed');
            archive_pkg.log_error (SQLCODE,
                                   'No Data Found for COMM_PURGE purge type');

            quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT');
            quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT1');
            quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT2');
            quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT3');
            quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT4');
            quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT5');
            ROLLBACK;
         WHEN OTHERS
         THEN
            update_purge_status (PURGE_DAYS, 'Failed');
            nsqlcode := SQLCODE;
            vsqlerrm := SQLERRM;
            archive_pkg.log_error (
               SQLCODE,
               'Error message in COMMERCE_PURGE_PROC: ' || SQLERRM);

            ROLLBACK;

            quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT');
            quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT1');
            quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT2');
            quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT3');
            quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT4');
            quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT5');
      END;
   END COMMERCE_PURGE_PROC;

   PROCEDURE INV_PURGE_PROC (PURGE_DAYS IN NUMBER)
   AS
      v_count          NUMBER := 0;
      inv_purge_days   NUMBER;
   BEGIN
      EXECUTE IMMEDIATE
         'alter session set NLS_TIMESTAMP_FORMAT = ''YYYY-MM-DD HH24:MI:SS.FF''';

      EXECUTE IMMEDIATE
         'alter session set NLS_DATE_FORMAT = ''YYYY-MM-DD HH24:MI:SS''';

      -- Get Dynamic Parallel Level and Commit Frequency
      SELECT NVL (company_parameter.param_value, param_def.dflt_value)
        INTO gcommitfrequency
        FROM param_def, company_parameter
       WHERE     param_def.param_def_id = company_parameter.param_def_id(+)
             AND param_def.param_group_id =
                    company_parameter.param_group_id(+)
             AND param_def.param_def_id = 'ARCHIVE_COMMIT_FREQUENCY'
             AND param_def.param_group_id = 'ARCHIVE'
             AND param_def.is_disabled = 0;

      SELECT NVL (company_parameter.param_value, param_def.dflt_value)
        INTO n_parallel_level
        FROM param_def, company_parameter
       WHERE     param_def.param_def_id = company_parameter.param_def_id(+)
             AND param_def.param_group_id =
                    company_parameter.param_group_id(+)
             AND param_def.param_def_id = 'ARCHIVE_PARALLEL_THREADS'
             AND param_def.param_group_id = 'ARCHIVE'
             AND param_def.is_disabled = 0;

      -- Check to execute the purge process in one node in case of RAC environment using  MA_ARCH_JOB_CLASS.
      -- This job class need to be created expilicitly only in case RAC setup. If the job class is not created, purge process will execute using DEFAULT_JOB_CLASS
      SELECT COUNT (1)
        INTO gv_job_class_chk
        FROM all_scheduler_job_classes
       WHERE job_class_name = 'MA_ARCH_JOB_CLASS';

      gvprocedurename := 'ORDER_DATA_PURGE_PKG.INV_PURGE_PROC - Begin';

      archive_pkg.log_archive (NULL,
                               gvprocedurename,
                               NULL,
                               NULL,
                               SEQ_ARCHIVE_RUN_LOG.NEXTVAL);


      BEGIN
         SELECT COUNT (1)
           INTO v_count
           FROM PURGE_CRITERIA
          WHERE PURGE_GROUP = 'INVENTORY_INV' AND purge_module = 'OLM';

         IF v_count > 0
         THEN
            SELECT NO_OF_DAYS
              INTO inv_purge_days
              FROM PURGE_CRITERIA
             WHERE PURGE_GROUP = 'INVENTORY_INV' AND purge_module = 'OLM';
         ELSE
            inv_purge_days := PURGE_DAYS;
         END IF;

         quiet_drop ('TABLE', 'INV_PURGE_PROC_GTT');
         quiet_drop ('TABLE', 'INV_PURGE_PROC_GTT1');
         quiet_drop ('TABLE', 'INV_PURGE_PROC_GTT2');

         EXECUTE IMMEDIATE
               'CREATE TABLE INV_PURGE_PROC_GTT TABLESPACE DOM_DT_TBS AS SELECT I_SEGMENTED.INVENTORY_ID FROM I_SEGMENTED
         LEFT OUTER JOIN I_ALLOCATION IA on IA.INVENTORY_ID = I_SEGMENTED.INVENTORY_ID
        WHERE (OBJECT_TYPE_ID IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I10'' AND REC_TYPE = ''S'' AND SHORT_DESC=''OH'') AND IS_DELETED  =1) OR (OBJECT_TYPE_ID IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I10'' AND REC_TYPE = ''S'' AND SHORT_DESC=''OH'')
        AND (AVAILABLE_QUANTITY =0 OR AVAILABLE_QUANTITY IS NULL) AND (AVAILABLE_SOON_QUANTITY =0 OR AVAILABLE_SOON_QUANTITY IS NULL) AND (UNAVAILABLE_QUANTITY =0 OR UNAVAILABLE_QUANTITY IS NULL)
        AND (IA.ALLOCATED_QUANTITY =0 OR IA.ALLOCATED_QUANTITY IS NULL) AND (IA.DO_QUANTITY =0 OR IA.DO_QUANTITY IS NULL)
        AND I_SEGMENTED.LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || INV_PURGE_DAYS
            || ')';

         EXECUTE IMMEDIATE
               'CREATE TABLE INV_PURGE_PROC_GTT1 TABLESPACE DOM_DT_TBS AS SELECT IP.INVENTORY_ID FROM I_PERPETUAL IP
         LEFT OUTER JOIN I_ALLOCATION IA on IA.INVENTORY_ID = IP.INVENTORY_ID
        WHERE (OBJECT_TYPE_ID IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I10'' AND REC_TYPE = ''S'' AND SHORT_DESC=''OH'') AND IS_DELETED  =1) OR (OBJECT_TYPE_ID IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I10'' AND REC_TYPE = ''S'' AND SHORT_DESC=''OH'')
        AND (AVAILABLE_QUANTITY =0 OR AVAILABLE_QUANTITY IS NULL) AND (AVAILABLE_SOON_QUANTITY =0 OR AVAILABLE_SOON_QUANTITY IS NULL) AND (UNAVAILABLE_QUANTITY =0 OR UNAVAILABLE_QUANTITY IS NULL)
        AND (IA.ALLOCATED_QUANTITY =0 OR IA.ALLOCATED_QUANTITY IS NULL) AND (IA.DO_QUANTITY =0 OR IA.DO_QUANTITY IS NULL)
        AND IP.LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || INV_PURGE_DAYS
            || ')';

         EXECUTE IMMEDIATE
               'CREATE TABLE INV_PURGE_PROC_GTT2 TABLESPACE DOM_DT_TBS AS SELECT INV_EVENT_LOG_ID from I_INV_EVENT_LOG
          WHERE CREATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || INV_PURGE_DAYS;


         INSERT_DEL_DATA_BULK (
            'I_ASSOCIATED_AVAILABILITY',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_1',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_1 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_2',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_2 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_3',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_3 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_4',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_4 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_5',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_5 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_6',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_6 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_7',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_7 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_8',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_8 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_9',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_9 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_10',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_10 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_11',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_11 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_12',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_12 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_13',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_13 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_14',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_14 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_15',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_15 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_16',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_16 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_17',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_17 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_18',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_18 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_19',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_19 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_20',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_20 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_1',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_1 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_2',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_2 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_3',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_3 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_4',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_4 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_5',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_5 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_6',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_6 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_7',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_7 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_8',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_8 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_9',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_9 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_10',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_10 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_11',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_11 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_12',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_12 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_13',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_13 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_14',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_14 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_15',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_15 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_16',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_16 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_17',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_17 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_18',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_18 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_19',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_19 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_20',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_20 WHERE SEGMENTED_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');


         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_1',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_2',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_3',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_4',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_5',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_6',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_7',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_8',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_9',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_10',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_11',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_12',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_13',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_14',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_15',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_16',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_17',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_18',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_19',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_20',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_1',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_2',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_3',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_4',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_5',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_6',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_7',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_8',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_9',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_10',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_11',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_12',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_13',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_14',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_15',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_16',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_17',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_18',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_19',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_20',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_ALLOCATION',
            '',
            '',
            '',
            'SEGMENTED_INVENTORY_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_INVENTORY_EVENT_DETAIL',
            '',
            '',
            '',
            'SEGMENTED_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_SEGMENTED_INV_LOG',
            '',
            '',
            '',
            'INVENTORY_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_ASSOCIATED_AVAILABILITY',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_1',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_1 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_2',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_2 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_3',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_3 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_4',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_4 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_5',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_5 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_6',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_6 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_7',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_7 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_8',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_8 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_9',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_9 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_10',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_10 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_11',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_11 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_12',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_12 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_13',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_13 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_14',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_14 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_15',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_15 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_16',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_16 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_17',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_17 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_18',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_18 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_19',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_19 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_20',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_20 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_1',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_1 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_2',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_2 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_3',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_3 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_4',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_4 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_5',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_5 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_6',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_6 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_7',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_7 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_8',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_8 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_9',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_9 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_10',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_10 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_11',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_11 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_12',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_12 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_13',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_13 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_14',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_14 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_15',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_15 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_16',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_16 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_17',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_17 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_18',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_18 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_19',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_19 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_20',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_20 WHERE PERPETUAL_INV_ID in (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');


         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_1',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_2',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_3',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_4',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_5',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_6',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_7',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_8',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_9',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_10',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_11',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_12',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_13',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_14',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_15',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_16',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_17',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_18',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_19',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_20',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_1',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_2',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_3',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_4',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_5',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_6',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_7',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_8',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_9',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_10',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_11',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_12',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_13',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_14',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_15',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_16',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_17',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_18',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_19',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_20',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_1_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_2_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_3_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_4_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_5_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_6_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_7_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_8_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_9_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_10_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_11_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_12_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_13_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_14_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_15_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_16_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_17_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_18_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_19_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_20_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_1_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_2_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_3_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_4_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_5_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_6_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_7_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_8_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_9_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_10_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_11_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_12_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_13_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_14_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_15_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_16_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_17_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_18_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_19_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_20_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_ALLOCATION',
            '',
            '',
            '',
            'INVENTORY_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_INVENTORY_EVENT_DETAIL',
            '',
            '',
            '',
            'PERPETUAL_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_EXCLUSION',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_PERPETUAL_INV_LOG',
            '',
            '',
            '',
            'INVENTORY_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_ONHAND_MOVEMENT',
            '',
            '',
            '',
            'INVENTORY_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');

         -- DOM-22217
         INSERT_DEL_DATA_BULK (
            'I_PERPETUAL_INV_LOG',
            '',
            '',
            '',
            'INV_EVENT_LOG_ID IN (SELECT INV_EVENT_LOG_ID FROM INV_PURGE_PROC_GTT2)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_SEGMENTED_INV_LOG',
            '',
            '',
            '',
            'INV_EVENT_LOG_ID IN (SELECT INV_EVENT_LOG_ID FROM INV_PURGE_PROC_GTT2)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_INV_EVENT_LOG',
            '',
            '',
            '',
            'INV_EVENT_LOG_ID IN (SELECT /*+ parallel*/ INV_EVENT_LOG_ID FROM INV_PURGE_PROC_GTT2)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_SEGMENTED',
            '',
            '',
            '',
            'INVENTORY_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_SEGMENTED',
            '',
            '',
            '',
            'REFERENCE_INV_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_PERPETUAL',
            '',
            '',
            '',
            'INVENTORY_ID IN  (SELECT /*+ parallel*/ INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');

         quiet_drop ('TABLE', 'INV_PURGE_PROC_GTT');
         quiet_drop ('TABLE', 'INV_PURGE_PROC_GTT1');
         quiet_drop ('TABLE', 'INV_PURGE_PROC_GTT2');

         gvprocedurename := 'ORDER_DATA_PURGE_PKG.INV_PURGE_PROC - End';
         archive_pkg.log_archive (NULL,
                                  gvprocedurename,
                                  NULL,
                                  NULL,
                                  SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            update_purge_status (PURGE_DAYS, 'Failed');
            archive_pkg.log_error (SQLCODE,
                                   'No Data Found for INV_PURGE purge type');

            quiet_drop ('TABLE', 'INV_PURGE_PROC_GTT');
            quiet_drop ('TABLE', 'INV_PURGE_PROC_GTT1');
            quiet_drop ('TABLE', 'INV_PURGE_PROC_GTT2');
         WHEN OTHERS
         THEN
            update_purge_status (PURGE_DAYS, 'Failed');
            nsqlcode := SQLCODE;
            vsqlerrm := SQLERRM;
            archive_pkg.log_error (
               nsqlcode,
               'Error message in INV_PURGE_PROC: ' || vsqlerrm);
            ROLLBACK;
            quiet_drop ('TABLE', 'INV_PURGE_PROC_GTT');
            quiet_drop ('TABLE', 'INV_PURGE_PROC_GTT1');
            quiet_drop ('TABLE', 'INV_PURGE_PROC_GTT2');
      END;
   END INV_PURGE_PROC;

   PROCEDURE PURGE_STAGING_EVENT_PROCESS (PURGE_DAYS IN NUMBER)
   AS
      TYPE RANGET IS TABLE OF NUMBER (10);

      -- Master Staging data
      MST_STG_DAYS   NUMBER (4);

      RST_STG_DAYS   NUMBER (4);

      AES_PRG_DAYS   NUMBER (4);

      MCE_PRG_DAYS   NUMBER (4);

      -- Out Bound Staging Data
      OB_PRG_DAYS    NUMBER (4);


      v_count2       NUMBER := 0;
      v_count3       NUMBER := 0;
      v_count4       NUMBER := 0;
      v_count5       NUMBER := 0;
      v_count6       NUMBER := 0;
   BEGIN
      EXECUTE IMMEDIATE
         'alter session set NLS_TIMESTAMP_FORMAT = ''YYYY-MM-DD HH24:MI:SS.FF''';

      EXECUTE IMMEDIATE
         'alter session set NLS_DATE_FORMAT = ''YYYY-MM-DD HH24:MI:SS''';

      -- Get Dynamic Parallel Level and Commit Frequency
      SELECT NVL (company_parameter.param_value, param_def.dflt_value)
        INTO gcommitfrequency
        FROM param_def, company_parameter
       WHERE     param_def.param_def_id = company_parameter.param_def_id(+)
             AND param_def.param_group_id =
                    company_parameter.param_group_id(+)
             AND param_def.param_def_id = 'ARCHIVE_COMMIT_FREQUENCY'
             AND param_def.param_group_id = 'ARCHIVE'
             AND param_def.is_disabled = 0;

      SELECT NVL (company_parameter.param_value, param_def.dflt_value)
        INTO n_parallel_level
        FROM param_def, company_parameter
       WHERE     param_def.param_def_id = company_parameter.param_def_id(+)
             AND param_def.param_group_id =
                    company_parameter.param_group_id(+)
             AND param_def.param_def_id = 'ARCHIVE_PARALLEL_THREADS'
             AND param_def.param_group_id = 'ARCHIVE'
             AND param_def.is_disabled = 0;

      -- Check to execute the purge process in one node in case of RAC environment using  MA_ARCH_JOB_CLASS.
      -- This job class need to be created expilicitly only in case RAC setup. If the job class is not created, purge process will execute using DEFAULT_JOB_CLASS
      SELECT COUNT (1)
        INTO gv_job_class_chk
        FROM all_scheduler_job_classes
       WHERE job_class_name = 'MA_ARCH_JOB_CLASS';


      gvprocedurename :=
         'ORDER_DATA_PURGE_PKG.PURGE_STAGING_EVENT_PROCESS - Begin';

      archive_pkg.log_archive (NULL,
                               gvprocedurename,
                               NULL,
                               NULL,
                               SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

      SELECT COUNT (1)
        INTO v_count2
        FROM PURGE_CRITERIA
       WHERE PURGE_GROUP = 'INVENTORY_STAG' AND purge_module = 'OLM';

      IF v_count2 > 0
      THEN
         SELECT NO_OF_DAYS
           INTO MST_STG_DAYS
           FROM PURGE_CRITERIA
          WHERE PURGE_GROUP = 'INVENTORY_STAG' AND purge_module = 'OLM';

         RST_STG_DAYS := MST_STG_DAYS;

         AES_PRG_DAYS := MST_STG_DAYS;

         MCE_PRG_DAYS := MST_STG_DAYS;

         OB_PRG_DAYS := MST_STG_DAYS;
      ELSE
         MST_STG_DAYS := PURGE_DAYS;

         RST_STG_DAYS := PURGE_DAYS;

         AES_PRG_DAYS := PURGE_DAYS;

         MCE_PRG_DAYS := PURGE_DAYS;

         OB_PRG_DAYS := PURGE_DAYS;
      END IF;

      -- Master Staging Data begins
      BEGIN
         quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT');

         EXECUTE IMMEDIATE
               'CREATE TABLE PURGE_STAGING_EVENT_GTT TABLESPACE DOM_DT_TBS AS SELECT event_id,SYNC_TRANSACTION_ID
            FROM MASTER_STAGING_DATA WHERE EVENT_STATUS IN
                                                 (SELECT SYS_CODE_ID
                                                    FROM SYS_CODE
                                                   WHERE CODE_TYPE = ''I08''
                                                         AND REC_TYPE = ''S''
                                                         AND CODE_DESC IN
                                                                (''Success'',
                                                                 ''Validation Failure'',
                                                                 ''Error''))
                                              AND LAST_UPDATED_DTTM <
                                                     TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || mst_stg_days;


         INSERT_DEL_DATA_BULK (
            'I_EVENT_DEPENDENCY',
            '',
            '',
            '',
            'event_id IN (SELECT /*+ parallel*/ event_id FROM PURGE_STAGING_EVENT_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_EVENT_STAGING',
            '',
            '',
            '',
            'event_id IN (SELECT /*+ parallel*/ event_id FROM PURGE_STAGING_EVENT_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_SYNC_PROCESS_CONTROL',
            '',
            '',
            '',
            'TRANSACTION_NUMBER IN (SELECT /*+ parallel*/ SYNC_TRANSACTION_ID FROM PURGE_STAGING_EVENT_GTT WHERE SYNC_TRANSACTION_ID IS NOT NULL)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_SYNC_STAGING',
            '',
            '',
            '',
            'TRANSACTION_NUMBER IN (SELECT /*+ parallel*/ SYNC_TRANSACTION_ID FROM PURGE_STAGING_EVENT_GTT WHERE SYNC_TRANSACTION_ID IS NOT NULL)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_PLAN_DETAIL_STAGING',
            '',
            '',
            '',
            'EVENT_ID IN (SELECT /*+ parallel*/ EVENT_ID FROM PURGE_STAGING_EVENT_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_SEGMENT_PLAN_STAGING',
            '',
            '',
            '',
            'EVENT_ID IN (SELECT /*+ parallel*/ EVENT_ID FROM PURGE_STAGING_EVENT_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'MASTER_STAGING_DATA',
            '',
            '',
            '',
            'EVENT_ID IN (SELECT /*+ parallel*/ EVENT_ID FROM PURGE_STAGING_EVENT_GTT)',
            'N');

         quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT');
      EXCEPTION
         WHEN OTHERS
         THEN
            update_purge_status (PURGE_DAYS, 'Failed');
            nsqlcode := SQLCODE;
            vsqlerrm := SQLERRM;
            archive_pkg.log_error (
               SQLCODE,
               'Error message in PURGE_STAGING_EVENT_PROCESS: ' || SQLERRM);
            ROLLBACK;
            quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT');
      END;

      -- Master Staging Data ends

      BEGIN
         quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT1');

         EXECUTE IMMEDIATE
            'CREATE TABLE PURGE_STAGING_EVENT_GTT1 TABLESPACE DOM_DT_TBS AS SELECT SYS_CODE_ID
                                                    FROM SYS_CODE
                                                   WHERE CODE_TYPE = ''I08''
                                                         AND REC_TYPE = ''S''
                                                         AND CODE_DESC IN
                                                                (''Success'',
                                                                 ''Validation Failure'',
                                                                 ''Error'')';

         -- Relay Staging Data begins

         INSERT_DEL_DATA_BULK (
            'I_RELAY_STAGING',
            '',
            '',
            '',
               'RELAY_STATUS IN (SELECT /*+ parallel*/ SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || rst_stg_days
            || '',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_ERROR',
            '',
            '',
            '',
               'LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''',''YYYY-MM-DD HH24:MI:SS'') - 30',
            'N');

         -- Relay Staging Data ends

         -- AES_PRG_days purge begins



         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_1',
            '',
            '',
            '',
               'STATUS IN (SELECT /*+ parallel*/ SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || AES_PRG_days
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_2',
            '',
            '',
            '',
               'STATUS IN (SELECT /*+ parallel*/ SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || AES_PRG_days
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_3',
            '',
            '',
            '',
               'STATUS IN (SELECT /*+ parallel*/ SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || AES_PRG_days
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_4',
            '',
            '',
            '',
               'STATUS IN (SELECT /*+ parallel*/ SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || AES_PRG_days
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_5',
            '',
            '',
            '',
               'STATUS IN (SELECT /*+ parallel*/ SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || AES_PRG_days
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_6',
            '',
            '',
            '',
               'STATUS IN (SELECT /*+ parallel*/ SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || AES_PRG_days
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_7',
            '',
            '',
            '',
               'STATUS IN (SELECT /*+ parallel*/ SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || AES_PRG_days
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_8',
            '',
            '',
            '',
               'STATUS IN (SELECT /*+ parallel*/ SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || AES_PRG_days
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_9',
            '',
            '',
            '',
               'STATUS IN (SELECT /*+ parallel*/ SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || AES_PRG_days
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_10',
            '',
            '',
            '',
               'STATUS IN (SELECT /*+ parallel*/ SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || AES_PRG_days
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_11',
            '',
            '',
            '',
               'STATUS IN (SELECT /*+ parallel*/ SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || AES_PRG_days
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_12',
            '',
            '',
            '',
               'STATUS IN (SELECT /*+ parallel*/ SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || AES_PRG_days
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_13',
            '',
            '',
            '',
               'STATUS IN (SELECT /*+ parallel*/ SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || AES_PRG_days
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_14',
            '',
            '',
            '',
               'STATUS IN (SELECT /*+ parallel*/ SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || AES_PRG_days
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_15',
            '',
            '',
            '',
               'STATUS IN (SELECT /*+ parallel*/ SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || AES_PRG_days
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_16',
            '',
            '',
            '',
               'STATUS IN (SELECT /*+ parallel*/ SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || AES_PRG_days
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_17',
            '',
            '',
            '',
               'STATUS IN (SELECT /*+ parallel*/ SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || AES_PRG_days
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_18',
            '',
            '',
            '',
               'STATUS IN (SELECT /*+ parallel*/ SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || AES_PRG_days
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_19',
            '',
            '',
            '',
               'STATUS IN (SELECT /*+ parallel*/ SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || AES_PRG_days
            || '',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_20',
            '',
            '',
            '',
               'STATUS IN (SELECT /*+ parallel*/ SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || AES_PRG_days
            || '',
            'N');

         quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT1');
      EXCEPTION
         WHEN OTHERS
         THEN
            update_purge_status (PURGE_DAYS, 'Failed');
            nsqlcode := SQLCODE;
            vsqlerrm := SQLERRM;
            archive_pkg.log_error (
               SQLCODE,
               'Error message in PURGE_STAGING_EVENT_PROCESS: ' || SQLERRM);
            ROLLBACK;
            quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT1');
      END;

      -- AES_PRG_days purge ends

      -- mce_prg_days purge begins

      BEGIN
         quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT2');
         quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT4');

         EXECUTE IMMEDIATE
               'CREATE TABLE PURGE_STAGING_EVENT_GTT2 TABLESPACE DOM_DT_TBS AS SELECT event_id FROM MASTER_COMMERCE_EVENT WHERE EVENT_STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC IN (''Success'', ''Validation Failure'', ''Error'')) AND  LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || mce_prg_days;

         EXECUTE IMMEDIATE
            'CREATE TABLE PURGE_STAGING_EVENT_GTT4 TABLESPACE DOM_DT_TBS AS SELECT AVAIL_INV_EVENT_ID FROM I_AVAIL_INVENTORY_EVENT WHERE event_id IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

         INSERT_DEL_DATA_BULK (
            'I_AVAIL_INVENTORY_EVENT_DETAIL',
            '',
            '',
            '',
            'AVAIL_INV_EVENT_ID IN (SELECT AVAIL_INV_EVENT_ID FROM PURGE_STAGING_EVENT_GTT4)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAIL_INVENTORY_EVENT',
            '',
            '',
            '',
            'event_id IN (SELECT /*+ parallel*/ event_id FROM PURGE_STAGING_EVENT_GTT2)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_FULFILLMENT_OUTAGE_EVENT',
            '',
            '',
            '',
            'event_id IN (SELECT /*+ parallel*/ event_id FROM PURGE_STAGING_EVENT_GTT2)',
            'N');


         INSERT_DEL_DATA_BULK (
            'I_REBUILD_EVENT',
            '',
            '',
            '',
            'rebuild_event_id in (SELECT /*+ parallel*/ event_id FROM PURGE_STAGING_EVENT_GTT2)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_REBUILD_STAGING',
            '',
            '',
            '',
            'event_id in (SELECT /*+ parallel*/ event_id FROM PURGE_STAGING_EVENT_GTT2)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAIL_SYNC_STAGING',
            '',
            '',
            '',
            'event_id in (SELECT /*+ parallel*/ event_id FROM PURGE_STAGING_EVENT_GTT2)',
            'N');


         INSERT_DEL_DATA_BULK (
            'MASTER_COMMERCE_EVENT',
            '',
            '',
            '',
            'event_id IN (SELECT /*+ parallel*/ event_id FROM PURGE_STAGING_EVENT_GTT2)',
            'N');

         quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT2');
      EXCEPTION
         WHEN OTHERS
         THEN
            update_purge_status (PURGE_DAYS, 'Failed');
            nsqlcode := SQLCODE;
            vsqlerrm := SQLERRM;
            archive_pkg.log_error (
               SQLCODE,
               'Error message in PURGE_STAGING_EVENT_PROCESS: ' || SQLERRM);
            ROLLBACK;
            quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT2');
      END;

      -- mce_prg_days purge ends

      -- MST_STG_DAYS purge begins

      BEGIN
         quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT3');

         EXECUTE IMMEDIATE
               'CREATE TABLE PURGE_STAGING_EVENT_GTT3 TABLESPACE DOM_DT_TBS AS SELECT SKU_LOCATION_ID FROM SKU_LOCATION  WHERE MARK_FOR_DELETION = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') -'
            || MST_STG_DAYS;


         INSERT_DEL_DATA_BULK (
            'SKU_FORECAST',
            '',
            '',
            '',
            'SKU_LOCATION_ID IN (SELECT /*+ parallel*/ SKU_LOCATION_ID FROM PURGE_STAGING_EVENT_GTT3)',
            'N');

         INSERT_DEL_DATA_BULK (
            'SKU_LOCATION_NEED',
            '',
            '',
            '',
            'SKU_LOCATION_ID IN (SELECT /*+ parallel*/ SKU_LOCATION_ID FROM PURGE_STAGING_EVENT_GTT3)',
            'N');

         INSERT_DEL_DATA_BULK (
            'SKU_LOCATION',
            '',
            '',
            '',
            'SKU_LOCATION_ID IN (SELECT /*+ parallel*/ SKU_LOCATION_ID FROM PURGE_STAGING_EVENT_GTT3)',
            'N');


         INSERT_DEL_DATA_BULK (
            'ITEM_AVAILABILITY',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') -'
            || mst_stg_days
            || '',
            'N');


         quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT3');
      EXCEPTION
         WHEN OTHERS
         THEN
            update_purge_status (PURGE_DAYS, 'Failed');
            nsqlcode := SQLCODE;
            vsqlerrm := SQLERRM;
            archive_pkg.log_error (
               SQLCODE,
               'Error message in PURGE_STAGING_EVENT_PROCESS: ' || SQLERRM);
            ROLLBACK;
            quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT3');
      END;

      -- MST_STG_DAYS purge ends

      -- Out Bound Staging Data Begins

      INSERT_DEL_DATA_BULK (
         'I_AVAIL_SYNC_OB_PROCESS_DTL',
         '',
         '',
         '',
            'SYNC_PROCESS_END_DTTM < TO_DATE('''
         || garchivebasedttm
         || ''', ''YYYY-MM-DD HH24:MI:SS'') -'
         || OB_PRG_DAYS
         || '',
         'N');

      INSERT_DEL_DATA_BULK (
         'I_SYNC_OB_PROCESS_CONTROL',
         '',
         '',
         '',
            'SYNC_PROCESS_END_DTTM < TO_DATE('''
         || garchivebasedttm
         || ''', ''YYYY-MM-DD HH24:MI:SS'') -'
         || OB_PRG_DAYS
         || '',
         'N');

      -- Out Bound Staging Data ends
      gvprocedurename :=
         'ORDER_DATA_PURGE_PKG.PURGE_STAGING_EVENT_PROCESS - End';
      archive_pkg.log_archive (NULL,
                               gvprocedurename,
                               NULL,
                               NULL,
                               SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
   EXCEPTION
      WHEN OTHERS
      THEN
         update_purge_status (PURGE_DAYS, 'Failed');
         nsqlcode := SQLCODE;
         vsqlerrm := SQLERRM;
         archive_pkg.log_error (
            SQLCODE,
            'Error message in PURGE_STAGING_EVENT_PROCESS: ' || SQLERRM);
         ROLLBACK;
   END PURGE_STAGING_EVENT_PROCESS;

   PROCEDURE CO_BASE_DATA_PURGE (PURGE_DAYS IN NUMBER)
   AS
      v_count           NUMBER := 0;
      BUSS_PURGE_DAYS   NUMBER;
   BEGIN
      EXECUTE IMMEDIATE
         'alter session set NLS_TIMESTAMP_FORMAT = ''YYYY-MM-DD HH24:MI:SS.FF''';

      EXECUTE IMMEDIATE
         'alter session set NLS_DATE_FORMAT = ''YYYY-MM-DD HH24:MI:SS''';

      gvprocedurename := 'ORDER_DATA_PURGE_PKG.CO_BASE_DATA_PURGE - Begin';

      archive_pkg.log_archive (NULL,
                               gvprocedurename,
                               NULL,
                               NULL,
                               SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

      -- Get Dynamic Parallel Level and Commit Frequency
      SELECT NVL (company_parameter.param_value, param_def.dflt_value)
        INTO gcommitfrequency
        FROM param_def, company_parameter
       WHERE     param_def.param_def_id = company_parameter.param_def_id(+)
             AND param_def.param_group_id =
                    company_parameter.param_group_id(+)
             AND param_def.param_def_id = 'ARCHIVE_COMMIT_FREQUENCY'
             AND param_def.param_group_id = 'ARCHIVE'
             AND param_def.is_disabled = 0;

      SELECT NVL (company_parameter.param_value, param_def.dflt_value)
        INTO n_parallel_level
        FROM param_def, company_parameter
       WHERE     param_def.param_def_id = company_parameter.param_def_id(+)
             AND param_def.param_group_id =
                    company_parameter.param_group_id(+)
             AND param_def.param_def_id = 'ARCHIVE_PARALLEL_THREADS'
             AND param_def.param_group_id = 'ARCHIVE'
             AND param_def.is_disabled = 0;

      -- Check to execute the purge process in one node in case of RAC environment using  MA_ARCH_JOB_CLASS.
      -- This job class need to be created expilicitly only in case RAC setup. If the job class is not created, purge process will execute using DEFAULT_JOB_CLASS
      SELECT COUNT (1)
        INTO gv_job_class_chk
        FROM all_scheduler_job_classes
       WHERE job_class_name = 'MA_ARCH_JOB_CLASS';

      SELECT COUNT (1)
        INTO v_count
        FROM PURGE_CRITERIA
       WHERE PURGE_GROUP = 'INVENTORY_COBASE' AND purge_module = 'OLM';

      IF v_count > 0
      THEN
         SELECT NO_OF_DAYS
           INTO BUSS_PURGE_DAYS
           FROM PURGE_CRITERIA
          WHERE PURGE_GROUP = 'INVENTORY_COBASE' AND purge_module = 'OLM';
      ELSE
         BUSS_PURGE_DAYS := PURGE_DAYS;
      END IF;


      INSERT_DEL_DATA_BULK (
         'A_CHARGE_DETAIL',
         '',
         '',
         '',
            'MARK_FOR_DELETION = 1 AND LAST_UPDATED_DTTM < (TO_DATE('''
         || garchivebasedttm
         || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
         || BUSS_PURGE_DAYS
         || ')',
         'N');

      INSERT_DEL_DATA_BULK (
         'A_DISCOUNT_DETAIL',
         '',
         '',
         '',
            'MARK_FOR_DELETION = 1 AND LAST_UPDATED_DTTM < (TO_DATE('''
         || garchivebasedttm
         || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
         || BUSS_PURGE_DAYS
         || ')',
         'N');

      INSERT_DEL_DATA_BULK (
         'A_TAX_DETAIL',
         '',
         '',
         '',
            'MARK_FOR_DELETION = 1 AND LAST_UPDATED_DTTM < (TO_DATE('''
         || garchivebasedttm
         || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
         || BUSS_PURGE_DAYS
         || ')',
         'N');

      INSERT_DEL_DATA_BULK (
         'A_SHIPPING_CHARGE_RULE',
         '',
         '',
         '',
            'END_DATE < (TO_DATE('''
         || garchivebasedttm
         || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
         || BUSS_PURGE_DAYS
         || ')',
         'N');

      INSERT_DEL_DATA_BULK (
         'A_SHIPPING_CHARGE_RULE',
         '',
         '',
         '',
            'IS_DELETED =1 AND LAST_UPDATED_DTTM < (TO_DATE('''
         || garchivebasedttm
         || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
         || BUSS_PURGE_DAYS
         || ')',
         'N');

      INSERT_DEL_DATA_BULK (
         'A_SHIPPING_CHARGE_MATRIX',
         '',
         '',
         '',
            'IS_DELETED = 1 AND LAST_UPDATED_DTTM < (TO_DATE('''
         || garchivebasedttm
         || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
         || BUSS_PURGE_DAYS
         || ')',
         'N');

      INSERT_DEL_DATA_BULK (
         'A_DELIVERYZONE',
         '',
         '',
         '',
            'IS_DELETED = 1 AND LAST_UPDATED_DTTM < (TO_DATE('''
         || garchivebasedttm
         || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
         || BUSS_PURGE_DAYS
         || ')',
         'N');

      INSERT_DEL_DATA_BULK (
         'A_PROMOTION_DETAIL',
         '',
         '',
         '',
            'MARK_FOR_DELETION = 1 AND LAST_UPDATED_DTTM < (TO_DATE('''
         || garchivebasedttm
         || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
         || BUSS_PURGE_DAYS
         || ')',
         'N');

      INSERT_DEL_DATA_BULK (
         'A_PROMOTED_PROD_ASSOCIATION',
         '',
         '',
         '',
            'MARK_FOR_DELETION = 1 AND LAST_UPDATED_DTTM < (TO_DATE('''
         || garchivebasedttm
         || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
         || BUSS_PURGE_DAYS
         || ')',
         'N');

      INSERT_DEL_DATA_BULK (
         'A_CUSTOMER_INFO',
         '',
         '',
         '',
            'IS_DELETED = 1 AND LAST_UPDATED_DTTM < (TO_DATE('''
         || garchivebasedttm
         || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
         || BUSS_PURGE_DAYS
         || ')',
         'N');

      INSERT_DEL_DATA_BULK ('A_CO_REASON_CODE',
                            '',
                            '',
                            '',
                            'MARK_FOR_DELETION = 1',
                            'N');

      INSERT_DEL_DATA_BULK (
         'A_CO_NOTE',
         '',
         '',
         '',
         'NOTE_TYPE_ID IN (SELECT NOTE_TYPE_ID FROM A_CO_NOTE_TYPE WHERE MARK_FOR_DELETION = 1)',
         'N');

      INSERT_DEL_DATA_BULK ('A_CO_NOTE_TYPE',
                            '',
                            '',
                            '',
                            'MARK_FOR_DELETION = 1',
                            'N');

      INSERT_DEL_DATA_BULK ('A_CO_APPEASEMENT',
                            '',
                            '',
                            '',
                            'MARK_FOR_DELETION = 1',
                            'N');

      gvprocedurename := 'ORDER_DATA_PURGE_PKG.CO_BASE_DATA_PURGE - End';
      archive_pkg.log_archive (NULL,
                               gvprocedurename,
                               NULL,
                               NULL,
                               SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
   EXCEPTION
      WHEN OTHERS
      THEN
         update_purge_status (PURGE_DAYS, 'Failed');
         nsqlcode := SQLCODE;
         vsqlerrm := SQLERRM;
         archive_pkg.log_error (
            SQLCODE,
            'Error message in CO_BASE_DATA_PURGE: ' || SQLERRM);
         ROLLBACK;
   END CO_BASE_DATA_PURGE;

   PROCEDURE INV_LOG_PURGE_PROC (PURGE_DAYS IN NUMBER)
   AS
      inv_log_purge_days   PLS_INTEGER := 0;
      v_count              PLS_INTEGER := 0;
   BEGIN
      EXECUTE IMMEDIATE
         'alter session set NLS_TIMESTAMP_FORMAT = ''YYYY-MM-DD HH24:MI:SS.FF''';

      EXECUTE IMMEDIATE
         'alter session set NLS_DATE_FORMAT = ''YYYY-MM-DD HH24:MI:SS''';


      -- Get Dynamic Parallel Level and Commit Frequency
      SELECT NVL (company_parameter.param_value, param_def.dflt_value)
        INTO gcommitfrequency
        FROM param_def, company_parameter
       WHERE     param_def.param_def_id = company_parameter.param_def_id(+)
             AND param_def.param_group_id =
                    company_parameter.param_group_id(+)
             AND param_def.param_def_id = 'ARCHIVE_COMMIT_FREQUENCY'
             AND param_def.param_group_id = 'ARCHIVE'
             AND param_def.is_disabled = 0;

      SELECT NVL (company_parameter.param_value, param_def.dflt_value)
        INTO n_parallel_level
        FROM param_def, company_parameter
       WHERE     param_def.param_def_id = company_parameter.param_def_id(+)
             AND param_def.param_group_id =
                    company_parameter.param_group_id(+)
             AND param_def.param_def_id = 'ARCHIVE_PARALLEL_THREADS'
             AND param_def.param_group_id = 'ARCHIVE'
             AND param_def.is_disabled = 0;

      -- Check to execute the purge process in one node in case of RAC environment using  MA_ARCH_JOB_CLASS.
      -- This job class need to be created expilicitly only in case RAC setup. If the job class is not created, purge process will execute using DEFAULT_JOB_CLASS
      SELECT COUNT (1)
        INTO gv_job_class_chk
        FROM all_scheduler_job_classes
       WHERE job_class_name = 'MA_ARCH_JOB_CLASS';

      gvprocedurename := 'ORDER_DATA_PURGE_PKG.INV_LOG_PURGE_PROC - Begin';

      archive_pkg.log_archive (NULL,
                               gvprocedurename,
                               NULL,
                               NULL,
                               SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

      BEGIN
         SELECT COUNT (1)
           INTO v_count
           FROM PURGE_CRITERIA
          WHERE PURGE_GROUP = 'INVENTORY_LOG' AND purge_module = 'OLM';

         IF v_count > 0
         THEN
            SELECT NO_OF_DAYS
              INTO inv_log_purge_days
              FROM PURGE_CRITERIA
             WHERE PURGE_GROUP = 'INVENTORY_LOG' AND purge_module = 'OLM';
         ELSE
            inv_log_purge_days := PURGE_DAYS;
         END IF;

         quiet_drop ('TABLE', 'INV_LOG_PURGE_PROC_GTT');

         EXECUTE IMMEDIATE
               'CREATE TABLE INV_LOG_PURGE_PROC_GTT TABLESPACE DOM_DT_TBS AS SELECT INV_EVENT_LOG_ID from I_INV_EVENT_LOG
          WHERE CREATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days;


         INSERT_DEL_DATA_BULK (
            'I_PERPETUAL_INV_LOG',
            '',
            '',
            '',
            'INV_EVENT_LOG_ID IN (SELECT /*+ parallel*/ INV_EVENT_LOG_ID FROM INV_LOG_PURGE_PROC_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_SEGMENTED_INV_LOG',
            '',
            '',
            '',
            'INV_EVENT_LOG_ID IN (SELECT /*+ parallel*/ INV_EVENT_LOG_ID FROM INV_LOG_PURGE_PROC_GTT)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_INV_EVENT_LOG',
            '',
            '',
            '',
            'INV_EVENT_LOG_ID IN (SELECT /*+ parallel*/ INV_EVENT_LOG_ID FROM INV_LOG_PURGE_PROC_GTT)',
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_1',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_2',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_3',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_4',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_5',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_6',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_7',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_8',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_9',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_10',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_11',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_12',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_13',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_14',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_15',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_16',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_17',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_18',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_19',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days,
            'N');
         INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_20',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days,
            'N');

         INSERT_DEL_DATA_BULK (
            'A_INVENTORY_EVENT',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days,
            'N');

         INSERT_DEL_DATA_BULK (
            'A_INVENTORY_EVENT_HISTORY',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days,
            'N');

         INSERT_DEL_DATA_BULK (
            'I_ALLOCATION_AUDIT',
            '',
            '',
            '',
               '(PERPETUAL_ID, ALLOCATION_VERSION) NOT IN (SELECT PERPETUAL_ID, MAX (ALLOCATION_VERSION) FROM I_ALLOCATION_AUDIT
                WHERE CREATED_DTTM  < TO_DATE('''
            || TO_CHAR (garchivebasedttm, 'yyyy-mon-dd hh24:mi:ss')
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days
            || ' AND  ALLOCATION_VERSION > 0 GROUP BY PERPETUAL_ID) AND ALLOCATION_VERSION > 0 AND
				CREATED_DTTM  < TO_DATE('''
            || TO_CHAR (garchivebasedttm, 'yyyy-mon-dd hh24:mi:ss')
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || inv_log_purge_days,
            'N');							
			
         quiet_drop ('TABLE', 'INV_LOG_PURGE_PROC_GTT');

         gvprocedurename := 'ORDER_DATA_PURGE_PKG.INV_LOG_PURGE_PROC - End';
         archive_pkg.log_archive (NULL,
                                  gvprocedurename,
                                  NULL,
                                  NULL,
                                  SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            update_purge_status (PURGE_DAYS, 'Failed');
            archive_pkg.log_error (SQLCODE,
                                   'No Data Found for INV_LOG_PURGE Days');

            quiet_drop ('TABLE', 'INV_LOG_PURGE_PROC_GTT');
         WHEN OTHERS
         THEN
            update_purge_status (PURGE_DAYS, 'Failed');
            nsqlcode := SQLCODE;
            vsqlerrm := SQLERRM;
            archive_pkg.log_error (
               nsqlcode,
               'Error message in INV_LOG_PURGE_PROC: ' || vsqlerrm);

            ROLLBACK;
            quiet_drop ('TABLE', 'INV_LOG_PURGE_PROC_GTT');
      END;
   END INV_LOG_PURGE_PROC;

   PROCEDURE INVENTORY_PURGE_PROC (PURGE_DAYS IN NUMBER)
   AS
      v_packagestatus   VARCHAR2 (40);
      purgeinprogress   EXCEPTION;
      v_purge_days      NUMBER (9);
      v_count           NUMBER := 0;
   BEGIN
      EXECUTE IMMEDIATE
         'alter session set NLS_TIMESTAMP_FORMAT = ''YYYY-MM-DD HH24:MI:SS.FF''';

      EXECUTE IMMEDIATE
         'alter session set NLS_DATE_FORMAT = ''YYYY-MM-DD HH24:MI:SS''';

      SELECT COUNT (1)
        INTO v_count
        FROM archive_status
       WHERE arch_run_status LIKE 'Order_Data_Purge_Pkg%';

      IF v_count = 0
      THEN
         v_packagestatus := 'Order_Data_Purge_Pkg - Begin';
         v_purge_days := PURGE_DAYS;
      ELSE
         SELECT arch_run_status, tc_company_id
           INTO v_packagestatus, v_purge_days
           FROM archive_status
          WHERE arch_run_status LIKE 'Order_Data_Purge_Pkg%';
      END IF;

      IF v_packagestatus = 'Order_Data_Purge_Pkg - Running'
      THEN
         RAISE purgeinprogress;
      END IF;

      IF v_packagestatus IN
            ('Order_Data_Purge_Pkg - Completed',
             'Order_Data_Purge_Pkg - Failed',
             'Order_Data_Purge_Pkg - Begin')
      THEN
         DELETE FROM archive_status
               WHERE arch_run_status LIKE 'Order_Data_Purge_Pkg%';

         INSERT
           INTO archive_status (arch_begin_dttm,
                                tc_company_id,
                                arch_run_status)
         VALUES (SYSDATE, PURGE_DAYS, 'Order_Data_Purge_Pkg - Running');

         COMMIT;
         gvprocedurename :=
            'ORDER_DATA_PURGE_PKG.INVENTORY_PURGE_PROC - Begin';
         archive_pkg.log_archive (NULL,
                                  gvprocedurename,
                                  NULL,
                                  NULL,
                                  SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

         -- Get Dynamic Parallel Level and Commit Frequency
         SELECT NVL (company_parameter.param_value, param_def.dflt_value)
           INTO gcommitfrequency
           FROM param_def, company_parameter
          WHERE     param_def.param_def_id =
                       company_parameter.param_def_id(+)
                AND param_def.param_group_id =
                       company_parameter.param_group_id(+)
                AND param_def.param_def_id = 'ARCHIVE_COMMIT_FREQUENCY'
                AND param_def.param_group_id = 'ARCHIVE'
                AND param_def.is_disabled = 0;

         SELECT NVL (company_parameter.param_value, param_def.dflt_value)
           INTO n_parallel_level
           FROM param_def, company_parameter
          WHERE     param_def.param_def_id =
                       company_parameter.param_def_id(+)
                AND param_def.param_group_id =
                       company_parameter.param_group_id(+)
                AND param_def.param_def_id = 'ARCHIVE_PARALLEL_THREADS'
                AND param_def.param_group_id = 'ARCHIVE'
                AND param_def.is_disabled = 0;

         -- Check to execute the purge process in one node in case of RAC environment using  MA_ARCH_JOB_CLASS.
         -- This job class need to be created expilicitly only in case RAC setup. If the job class is not created, purge process will execute using DEFAULT_JOB_CLASS
         SELECT COUNT (1)
           INTO gv_job_class_chk
           FROM all_scheduler_job_classes
          WHERE job_class_name = 'MA_ARCH_JOB_CLASS';

         INV_BUSSINESS_RULES_PURGE_PROC (PURGE_DAYS);
         COMMERCE_PURGE_PROC (PURGE_DAYS);
         INV_PURGE_PROC (PURGE_DAYS);
         PURGE_STAGING_EVENT_PROCESS (PURGE_DAYS);
         INV_LOG_PURGE_PROC (PURGE_DAYS);
         CO_BASE_DATA_PURGE (PURGE_DAYS);

         gvprocedurename := 'ORDER_DATA_PURGE_PKG.INVENTORY_PURGE_PROC - End';
         archive_pkg.log_archive (NULL,
                                  gvprocedurename,
                                  NULL,
                                  NULL,
                                  SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

         update_purge_status (PURGE_DAYS, 'Success');
      END IF;
   EXCEPTION
      WHEN purgeinprogress
      THEN
         archive_pkg.log_error (
            -20005,
            'EXCEPTION ORDER_DATA_PURGE_PKG - PurgeInProgress');
         raise_application_error (
            -20005,
            'ERROR: Another Purge Process is already in progress Days.');
      WHEN gdelete_error
      THEN
         update_purge_status (PURGE_DAYS, 'Failed');
         raise_application_error (
            -20005,
            'ERROR: Please Check ARCHIVE_ERROR_LOG Table for details.');
      WHEN OTHERS
      THEN
         ROLLBACK;

         update_purge_status (PURGE_DAYS, 'Failed');
         nsqlcode := SQLCODE;
         vsqlerrm := SQLERRM;
         archive_pkg.log_error (
            nsqlcode,
            'Error message in INVENTORY_PURGE_PROC: ' || vsqlerrm);
   END INVENTORY_PURGE_PROC;
END ORDER_DATA_PURGE_PKG;
/
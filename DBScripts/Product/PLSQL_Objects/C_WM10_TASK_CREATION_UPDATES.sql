create or replace
procedure c_wm10_task_creation_updates
(
  p_wave_nbr    IN wave_parm.wave_nbr%type,
  p_whse        IN wave_parm.whse%type,
  p_rc                  out number
)
as
pickLoadPrty NUMBER;
pickStagePrty NUMBER;
v_mod_enabled varchar(1);

begin
 p_rc := 0;

SELECT SUBSTR(SC.MISC_FLAGS, 1, 1) INTO v_mod_enabled FROM 
              SYS_CODE SC
            WHERE  SC.CODE_TYPE            = '303'
            AND SC.REC_TYPE                = 'C';
      
Insert Into C_REYE_TMP_TASK (
TASK_ID,
TASK_TYPE ,
MIN_ALPHA ,
MAX_ALPHA
)
(Select task_id,TASK_TYPE,min(MISC_ALPHA_FIELD_1),Max(MISC_ALPHA_FIELD_1) From task_dtl Where task_genrtn_ref_nbr = p_wave_nbr AND INVN_NEED_TYPE = 50
Group By task_id,TASK_TYPE);

--select task priority from sys code  010
Select to_number(substr(misc_flags,1,2)) into pickLoadPrty From SYS_CODE SYS Where SYS.rec_type = 'C' and SYS.code_type = '010' and SYS.code_id = 'CUS';
Select to_number(substr(misc_flags,3,2)) into pickStagePrty From SYS_CODE SYS Where SYS.rec_type = 'C' and SYS.code_type = '010' and SYS.code_id = 'CUS';

--update priority for PICK to Load
Update  C_REYE_TMP_TASK CT Set PRTY =
(
select inq.val
from
  (
      select distinct TD.TASK_ID, dense_rank () OVER (Partition by CRTT.TASK_TYPE,L.SHIPMENT_ID ORDER BY MIN_ALPHA,TD.TASK_ID) + pickLoadPrty - 1 val
      from C_REYE_TMP_TASK CRTT, TASK_DTL TD, LPN L
           where  Exists
           (
           Select 1 From sys_code Where rec_type = 'S' And code_type = '590'
           And CODE_ID = crtt.task_type And SUBSTR(MISC_FLAGS,1,1) = '1'
          )
          and CRTT.task_id = TD.task_id
		  and TD.carton_nbr = L.tc_lpn_id
		  and L.inbound_outbound_indicator='O'
      ) inq
      where ct.TASK_ID = inq.TASK_ID
   )
where  Exists
  (
      Select 1 From sys_code Where rec_type = 'S' And code_type = '590'
      And CODE_ID = ct.task_type And SUBSTR(MISC_FLAGS,1,1) = '1'
);

--update priority for PICK to Stage

Update  C_REYE_TMP_TASK CT Set PRTY =
(
select inq.val
from
  (
      select distinct TD.TASK_ID, pickStagePrty - dense_rank () OVER (Partition by CRTT.TASK_TYPE,L.SHIPMENT_ID  ORDER BY MIN_ALPHA,TD.TASK_ID) + 1 val
      from C_REYE_TMP_TASK CRTT, TASK_DTL TD, LPN L
           where  Exists
           (
           Select 1 From sys_code Where rec_type = 'S' And code_type = '590'
           And CODE_ID = crtt.task_type And SUBSTR(MISC_FLAGS,1,1) = '2'
          )
          and CRTT.task_id = TD.task_id
		  and TD.carton_nbr = L.tc_lpn_id
		  and L.inbound_outbound_indicator='O'
      ) inq
      where ct.TASK_ID = inq.TASK_ID
   )
where  Exists
  (
      Select 1 From sys_code Where rec_type = 'S' And code_type = '590'
      And CODE_ID = ct.task_type And SUBSTR(MISC_FLAGS,1,1) = '2'
);

UPDATE TASK_HDR th
set CURR_TASK_PRTY = coalesce((select PRTY from C_REYE_TMP_TASK crtt
                      where crtt.task_id=th.task_id),40)
where exists (select 1 from  C_REYE_TMP_TASK crtt
                      where crtt.task_id=th.task_id );




UPDATE TASK_HDR TH Set TH.DOC_ID =
( Select DISTINCT TC_SHIPMENT_ID From ORDERS ORD , TASK_DTL TD Where TD.TASK_ID = TH.TASK_ID And
  ORD.TC_ORDER_ID =  TD.TC_ORDER_ID
)
Where TH.INVN_NEED_TYPE = 50 And TH.TASK_GENRTN_REF_NBR = p_wave_nbr;

if  (v_mod_enabled='Y') then
              update TASK_DTL set PICK_SEQ_CODE ='000000000' where TASK_CMPL_REF_NBR in 
                        (SELECT TD.TASK_CMPL_REF_NBR FROM TASK_DTL TD, LPN L1, LPN_DETAIL LD
                        WHERE TD.TASK_GENRTN_REF_NBR = p_wave_nbr AND 
                        TD.INVN_NEED_TYPE = '50' AND 
                        TD.TASK_CMPL_REF_NBR = L1.TC_LPN_ID AND
                        L1.LPN_ID = LD.LPN_ID AND 
                        LD.INSTRTN_CODE_1 = 'Y'
                        );
end if;
commit;

exception when others then
 p_rc := -1;

end;
/
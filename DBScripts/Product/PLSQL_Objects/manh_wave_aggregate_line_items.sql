create or replace procedure manh_wave_aggregate_line_items
(
    p_user_id               in  ucl_user.user_name%type,
    p_facility_id           in  facility.facility_id%type,
    p_pick_wave_nbr         in  varchar2,
    p_ship_wave_nbr         in  varchar2,
    p_is_major_minor_flow   in  number default 0
)
as
    v_whse facility.whse%type;
    v_force_wpt wave_parm.force_wpt%type := 'N';
begin
    select f.whse
    into v_whse
    from facility f
    where f.facility_id = p_facility_id;

    select coalesce(wp.force_wpt, 'N')
    into v_force_wpt
    from wave_parm wp
    where wp.whse = v_whse and wp.wave_nbr = p_pick_wave_nbr;

    -- collect line items to aggregate
    insert into tmp_aggregation_line_items
    (
        order_id, line_item_id, allocated_qty, item_id, reference_order_id,
        line_group_num, price, planned_weight, planned_volume, size1_value,
        size2_value
    )
    select oli.order_id, oli.line_item_id, oli.allocated_qty, oli.item_id,
        o.parent_order_id reference_order_id,
        dense_rank() over(partition by o.parent_order_id
            order by oli.item_id, oli.invn_type, oli.prod_stat, oli.batch_nbr,
            oli.cntry_of_orgn, oli.alloc_type, oli.item_attr_1, oli.item_attr_2,
            oli.item_attr_3, oli.item_attr_4, oli.item_attr_5, oli.lpn_size,
            oli.lpn_type, oli.protection_level_id, oli.product_class_id,
            oli.commodity_code_id, oli.commodity_class, o.purchase_order_id,
            case when v_force_wpt = 'Y' then 0 else oli.wave_proc_type end,
            oli.pick_locn_id,
            oli.sku_break_attr, oli.assort_nbr, oli.ppack_grp_code,
            oli.ppack_qty, oli.lpn_brk_attrib, oli.allocation_source_id,
            oli.allocation_source_line_id, oli.allocation_source,
            oli.vas_process_type, oli.pallet_type, oli.fulfillment_type,
            oli.purchase_order_number,oli.is_hazmat,oli.is_chase_created_line)
            line_group_num, oli.price, coalesce(oli.planned_weight, 0),
            coalesce(oli.planned_volume, 0), coalesce(oli.size1_value, 0),
            coalesce(oli.size2_value, 0)
    from order_line_item oli
    join orders o on o.order_id = oli.order_id
    where oli.do_dtl_status in (120, 130) and oli.wave_nbr = p_pick_wave_nbr
        and o.o_facility_id = p_facility_id and o.do_status between 120 and 140
        and
        (
            p_is_major_minor_flow = 0
            or exists
            (
                select 1
                from tmp_aggregation_order_groups t
                where t.parent_order_id = o.parent_order_id
                    and t.is_pre_stikr_logic < 1
            )
        );
    wm_cs_log('Collected Orig lines ' || sql%rowcount, p_sql_log_level => 1);

    if (p_is_major_minor_flow = 1)
    then
        -- lines for majors following the pre stikr logic *in this wave*
        insert into tmp_aggregation_line_items
        (
            order_id, line_item_id, allocated_qty, item_id, reference_order_id,
            price, planned_weight, planned_volume, size1_value, size2_value,
            line_group_num
        )
        select oli.order_id, oli.line_item_id, oli.allocated_qty, oli.item_id, 
            o.parent_order_id reference_order_id, oli.price,
            coalesce(oli.planned_weight, 0), coalesce(oli.planned_volume, 0),
            coalesce(oli.size1_value, 0), coalesce(oli.size2_value, 0),
            dense_rank() over(partition by o.parent_order_id
                order by o.ext_purchase_order, oli.item_id, oli.ppack_qty,
                oli.assort_nbr, oli.ppack_grp_code, oli.invn_type, oli.prod_stat,
                oli.batch_nbr, oli.item_attr_1, oli.item_attr_2, oli.item_attr_3,
                oli.item_attr_4, oli.item_attr_5, oli.cntry_of_orgn,oli.is_hazmat
                ,oli.is_chase_created_line)
        from order_line_item oli
        join orders o on o.order_id = oli.order_id
        where oli.do_dtl_status in (120, 130) and oli.wave_nbr = p_pick_wave_nbr
            and o.o_facility_id = p_facility_id
            and o.do_status between 120 and 140
            and exists
            (
                select 1
                from tmp_aggregation_order_groups t
                where t.parent_order_id = o.parent_order_id
                    and t.is_pre_stikr_logic > 0
            );
        wm_cs_log('Collected Orig lines needing pre stikr logic ' || sql%rowcount, p_sql_log_level => 1);
    end if;

    insert into tmp_aggregation_line_groups
    (
        reference_order_id, line_group_num, reference_line_item_id,
        line_item_id, allocated_qty, planned_weight, planned_volume,
        size1_value, size2_value, price
    )
    select iv.reference_order_id, iv.line_group_num,
        seq_line_item_id.nextval reference_line_item_id, iv.line_item_id,
        iv.allocated_qty, iv.planned_weight, iv.planned_volume, iv.size1_value,
        iv.size2_value, iv.price
    from
    (
        select t.reference_order_id, t.line_group_num,
            min(t.line_item_id) line_item_id,
            sum(t.allocated_qty) allocated_qty,
            sum(t.planned_weight) planned_weight,
            sum(t.planned_volume) planned_volume,
            sum(t.size1_value) size1_value, sum(t.size2_value) size2_value,
            sum(t.price) price
        from tmp_aggregation_line_items t
        group by t.reference_order_id, t.line_group_num
    ) iv;
    wm_cs_log('Found line groups ' || sql%rowcount, p_sql_log_level => 1);
	
    -- create the aggregated line items; assumption is that all uom id's are the
    -- same across original lines; we cheat a little by using the fact that
    -- line_item_id is generated with a seq; the wpt in wmos is copied over from
    -- the orig order and changed when forced, so we retain that logic
    insert into order_line_item
    (
        item_id, item_attr_1, item_attr_2, item_attr_3, item_attr_4,
        item_attr_5, lpn_size, lpn_type, protection_level_id,
        product_class_id, commodity_code_id, commodity_class, item_name,
        wave_proc_type, pick_locn_id,
        sku_break_attr, assort_nbr, ppack_grp_code, ppack_qty, lpn_brk_attrib,
        allocation_source_id, allocation_source_line_id, allocation_source,
        vas_process_type, order_id, line_item_id, allocated_qty, order_qty,
         orig_order_qty,adjusted_order_qty, do_dtl_status, last_updated_source, last_updated_dttm,
        created_source, created_dttm, tc_order_line_id, wave_nbr, invn_type,
        prod_stat, batch_nbr, cntry_of_orgn, alloc_type, qty_uom_id,
        mv_size_uom_id, shipped_qty, user_canceled_qty, rtl_to_be_distroed_qty,
        ship_wave_nbr, planned_weight, planned_volume, 
        qty_uom_id_base, weight_uom_id_base, volume_uom_id_base, 
        is_cancelled, tc_company_id, weight_uom_id, volume_uom_id,
        pallet_type, price, fulfillment_type, budg_cost_currency_code,
        actual_cost_currency_code, std_pack_qty, std_bundle_qty, std_lpn_qty,
        std_lpn_vol, std_lpn_wt, std_pallet_qty, std_sub_pack_qty, units_pakd,
        purchase_order_number,
        actual_cost, actual_shipped_dttm, alloc_line_id, batch_requirement_type,
        budg_cost, chute_assign_type, critcl_dim_1, critcl_dim_2, critcl_dim_3,
        cube_multiple_qty, customer_item,custom_tag, delivery_end_dttm, 
        delivery_reference_number, delivery_start_dttm, description, event_code,
        exp_info_code, ext_purchase_order, ext_sys_line_item_id, 
        internal_order_id, internal_order_seq_nbr, is_emergency, is_hazmat, 
        is_stackable, line_type, manufacturing_dttm, master_order_id, 
        merchandizing_department_id, merch_grp, merch_type, mo_line_item_id, 
        mv_currency_code, order_consol_attr, order_line_id, 
        original_ordered_item_id, orig_budg_cost, package_type_id, pack_rate, 
        pack_zone,  partl_fill, pickup_end_dttm, pickup_reference_number, 
        pickup_start_dttm, pick_locn_assign_type, planned_ship_date, 
        price_tkt_type, priority, purchase_order_line_number, qty_conv_factor, 
        received_qty, received_size1, received_size2, received_volume, 
        received_weight, ref_field1, ref_field2, ref_field3, ref_field4, 
        ref_field5, ref_field6, ref_field7, ref_field8, ref_field9, ref_field10,
        repl_proc_type, repl_wave_nbr, repl_wave_run, retail_price, rts_id, rts_line_item_id, 
        serial_number_required_flag, shelf_days, shipped_size1, shipped_size2, 
        shipped_volume, shipped_weight, single_unit_flag, sku_gtin, 
        stack_diameter_standard_uom, stack_diameter_value, 
        stack_height_standard_uom, stack_height_value,stack_length_standard_uom,
        stack_length_value, stack_rank, stack_width_standard_uom, 
        stack_width_value, store_dept, substitution_type, total_monetary_value, 
        unit_cost, unit_monetary_value, unit_price_amount, unit_tax_amount, 
        unit_vol, unit_wt, un_number_id,is_chase_created_line, ref_num1, ref_num2, ref_num3, 
		ref_num4, freight_revenue_currency_code,ref_num5
    )
    select oli.item_id, oli.item_attr_1, oli.item_attr_2, oli.item_attr_3,
        oli.item_attr_4, oli.item_attr_5, oli.lpn_size, oli.lpn_type,
        oli.protection_level_id, oli.product_class_id, oli.commodity_code_id,
        oli.commodity_class, oli.item_name,
        oli.wave_proc_type, oli.pick_locn_id, oli.sku_break_attr,oli.assort_nbr,
        oli.ppack_grp_code, oli.ppack_qty, oli.lpn_brk_attrib,
        oli.allocation_source_id, oli.allocation_source_line_id,
        oli.allocation_source, oli.vas_process_type,
        t.reference_order_id order_id, t.reference_line_item_id line_item_id,
        t.allocated_qty allocated_qty, t.allocated_qty order_qty,
         t.allocated_qty orig_order_qty,t.allocated_qty adjusted_order_qty, 130 do_dtl_status,
        p_user_id last_updated_source, sysdate last_updated_dttm,
        p_user_id created_source, sysdate created_dttm,
        to_char(t.reference_line_item_id) tc_order_line_id,
        p_pick_wave_nbr wave_nbr, oli.invn_type, oli.prod_stat, oli.batch_nbr,
        oli.cntry_of_orgn, oli.alloc_type, oli.qty_uom_id, oli.mv_size_uom_id,
        0 shipped_qty, 0 user_canceled_qty, 0 rtl_to_be_distroed_qty,
        p_ship_wave_nbr,
        case when oli.weight_uom_id is null and t.planned_weight = 0 then null
            else t.planned_weight end planned_weight,
        case when oli.volume_uom_id is null and t.planned_volume = 0 then null
            else t.planned_volume end planned_volume,
        oli.qty_uom_id_base, oli.weight_uom_id_base, oli.volume_uom_id_base,
        '0' is_cancelled, oli.tc_company_id,
        oli.weight_uom_id, oli.volume_uom_id, oli.pallet_type, t.price,
        oli.fulfillment_type, oli.budg_cost_currency_code,
        oli.actual_cost_currency_code, oli.std_pack_qty, oli.std_bundle_qty,
        oli.std_lpn_qty, oli.std_lpn_vol, oli.std_lpn_wt, oli.std_pallet_qty,
        oli.std_sub_pack_qty, 0 units_pakd, oli.purchase_order_number,
        oli.actual_cost, oli.actual_shipped_dttm, oli.alloc_line_id, oli.batch_requirement_type,
        oli.budg_cost, oli.chute_assign_type, oli.critcl_dim_1, oli.critcl_dim_2, oli.critcl_dim_3,
        oli.cube_multiple_qty, oli.customer_item,custom_tag, oli.delivery_end_dttm,
        oli.delivery_reference_number, oli.delivery_start_dttm, oli.description, oli.event_code,
        oli.exp_info_code, oli.ext_purchase_order, oli.ext_sys_line_item_id,
        oli.internal_order_id, oli.internal_order_seq_nbr, oli.is_emergency, oli.is_hazmat,
        oli.is_stackable, oli.line_type, oli.manufacturing_dttm, oli.master_order_id,
        oli.merchandizing_department_id, oli.merch_grp, oli.merch_type, oli.mo_line_item_id,
        oli.mv_currency_code, oli.order_consol_attr, oli.order_line_id,
        oli.original_ordered_item_id, oli.orig_budg_cost, oli.package_type_id, oli.pack_rate,
        oli.pack_zone, oli.partl_fill, oli.pickup_end_dttm, oli.pickup_reference_number,
        oli.pickup_start_dttm, oli.pick_locn_assign_type, oli.planned_ship_date,
        oli.price_tkt_type, oli.priority, oli.purchase_order_line_number, oli.qty_conv_factor,
        oli.received_qty, oli.received_size1, oli.received_size2, oli.received_volume,
        oli.received_weight, oli.ref_field1, oli.ref_field2, oli.ref_field3, 
        oli.ref_field4, oli.ref_field5, oli.ref_field6, oli.ref_field7, 
        oli.ref_field8, oli.ref_field9, oli.ref_field10, oli.repl_proc_type,
        oli.repl_wave_nbr, oli.repl_wave_run, oli.retail_price, oli.rts_id, oli.rts_line_item_id,
        oli.serial_number_required_flag, oli.shelf_days, oli.shipped_size1, oli.shipped_size2,
        oli.shipped_volume, oli.shipped_weight, oli.single_unit_flag, oli.sku_gtin,
        oli.stack_diameter_standard_uom, oli.stack_diameter_value,
        oli.stack_height_standard_uom, oli.stack_height_value,stack_length_standard_uom,
        oli.stack_length_value, oli.stack_rank, oli.stack_width_standard_uom,
        oli.stack_width_value, oli.store_dept, oli.substitution_type, oli.total_monetary_value,
        oli.unit_cost, oli.unit_monetary_value, oli.unit_price_amount, oli.unit_tax_amount,
        oli.unit_vol, oli.unit_wt, oli.un_number_id,oli.is_chase_created_line, oli.ref_num1, 
		oli.ref_num2, oli.ref_num3, oli.ref_num4, oli.freight_revenue_currency_code, oli.ref_num5
    from tmp_aggregation_line_groups t
    join order_line_item oli on oli.line_item_id = t.line_item_id;
    wm_cs_log('Created agg OLI ' || sql%rowcount, p_sql_log_level => 1);

    -- point original line items to aggregated line items
    merge into order_line_item oli
    using
    (
        select t1.reference_order_id, t1.reference_line_item_id, t2.order_id,
            t2.line_item_id
        from tmp_aggregation_line_groups t1
        join tmp_aggregation_line_items t2
            on t2.line_group_num = t1.line_group_num
            and t2.reference_order_id = t1.reference_order_id
    ) iv on (iv.order_id = oli.order_id and iv.line_item_id = oli.line_item_id)
    when matched then
    update set oli.reference_order_id = iv.reference_order_id,
        oli.reference_line_item_id = iv.reference_line_item_id;
    wm_cs_log('Repointed orig OLI ' || sql%rowcount, p_sql_log_level => 1);
end;
/

show errors;
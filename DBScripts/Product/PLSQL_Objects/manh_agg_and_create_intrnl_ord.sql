create or replace procedure manh_agg_and_create_intrnl_ord
(
    p_user_id           in  ucl_user.user_name%type,
    p_facility_id       in  facility.facility_id%type,
    p_tc_company_id   	in  wave_parm.tc_company_id%type,
    p_lock_time_out 	in  number,
    p_ship_wave_nbr     in  ship_wave_parm.ship_wave_nbr%type,
    p_pick_wave_nbr  	in  ship_wave_parm.pick_wave_nbr%type,
    p_is_retail_wave	in  number,
    p_debug_level       in  number
)
as
    v_use_locking         varchar2(1);
    v_is_major_minor_flow number(1) := 0;
    v_commit_freq         number(5);
    v_max_log_lvl         number(1);
    v_code_id             msg_log.ref_code_1%type := 'AGG';
    v_whse                ship_wave_parm.whse%type;
    v_rte_wave_nbr        ship_wave_parm.rte_wave_nbr%type;
    v_prev_module_name    wm_utils.t_app_context_data;
    v_prev_action_name    wm_utils.t_app_context_data;
begin
    if (p_is_retail_wave = 0)
    then
        -- not retail; could be a major/minor flow
        select case when exists
        (
            select 1
            from whse_parameters wp
            where wp.whse_master_id = p_facility_id
                and wp.create_major_pkt in ('1', '2')
        ) then 1 else 0 end
        into v_is_major_minor_flow
        from dual;
    end if;

    if (p_is_retail_wave = 0 and v_is_major_minor_flow = 0)
    then
        return;
    end if;

    select case when swp.perf_rte in ('1', '2') then p_ship_wave_nbr else null end, swp.whse
    into v_rte_wave_nbr, v_whse
    from ship_wave_parm swp
    where swp.ship_wave_nbr = p_ship_wave_nbr
        and swp.whse =
        (
            select f.whse
            from facility f
            where f.facility_id = p_facility_id
        );
	
    select to_number(coalesce(trim(substr(sc.misc_flags, 3, 5)), '99999')),
        to_number(coalesce(trim(substr(sc.misc_flags, 8, 1)), '0')),
        substr(sc.misc_flags, 2, 1)
    into v_commit_freq, v_max_log_lvl, v_use_locking
    from sys_code sc 
    where sc.rec_type = 'S' and sc.code_type = '520' and sc.code_id = v_code_id;
    v_commit_freq := case v_commit_freq when 0 then 99999 else v_commit_freq end;

    delete from tmp_log_parm;
    insert into tmp_log_parm
    (
        log_lvl, pgm_id, module, msg_id, ref_code_1, ref_value_1, user_id, whse,
        cd_master_id
    )
    values
    (
        v_max_log_lvl, 'manh_agg_and_create_intrnl_ord', 'WAVE', '1094',
        v_code_id, p_pick_wave_nbr, p_user_id, v_whse, coalesce(p_tc_company_id, wm_utils.wm_get_user_bu(p_user_id))
    );    
	
  	if (v_use_locking = 'Y'
        and wm_wave_allow_store_locking(p_facility_id, p_pick_wave_nbr) = 0)
    then
        v_use_locking := 'N';
        wm_cs_log('Store locking turned off!', p_sql_log_level => 0);
    end if;

    wm_utils.get_context_info(v_prev_module_name, v_prev_action_name);
    wm_utils.set_context_info(p_module_name => 'WAVE', p_action_name => 'AGGREGATION',
        p_client_id => p_ship_wave_nbr);

    wm_cs_log('Beginning Aggregation', p_sql_log_level => 0);
    if (v_use_locking = 'Y' and p_is_retail_wave = 1)
    then
        -- this is only to support retail, fta flows
        manh_wave_aggregate_with_lock(p_user_id, p_facility_id, v_rte_wave_nbr,
            p_pick_wave_nbr, p_ship_wave_nbr, coalesce(p_tc_company_id, wm_utils.wm_get_user_bu(p_user_id)), v_commit_freq);
    else
        manh_retail_orders_aggregation(p_user_id, p_facility_id,
            p_pick_wave_nbr, p_ship_wave_nbr, v_rte_wave_nbr, 
            coalesce(p_tc_company_id, wm_utils.wm_get_user_bu(p_user_id)), v_is_major_minor_flow);
    end if;
    
    manh_wave_aggregate_line_items(p_user_id, p_facility_id, p_pick_wave_nbr,
        p_ship_wave_nbr, v_is_major_minor_flow);

    manh_create_major_order_notes();

    if (v_is_major_minor_flow = 1)
    then
        -- copy accessorial onto major order
        insert into order_accessorial_option_grp 
       (
            order_id, accessorial_group_code
        )
        select distinct o.parent_order_id, oaog.accessorial_group_code
        from orders o
        join tmp_aggregation_orders t on t.order_id = o.order_id
        join order_accessorial_option_grp oaog on oaog.order_id = t.order_id
        where not exists
        (
             select 1
             from order_accessorial_option_grp oaog2
             where oaog2.order_id = o.parent_order_id
                 and oaog2.accessorial_group_code = oaog.accessorial_group_code
        );
        wm_cs_log('Created accessorial options ', p_sql_log_level => 1);
    end if ;

    update wave_parm wp
    set wp.wave_stat_code = 5, wp.mod_date_time = sysdate, wp.user_id = p_user_id
    where wp.wave_nbr = p_pick_wave_nbr and wp.whse = v_whse;

    commit;
    wm_cs_log('Completed Aggregation', p_sql_log_level => 0);
    wm_utils.set_context_info(p_module_name => v_prev_module_name,
        p_action_name => v_prev_action_name);
end;
/
show errors;

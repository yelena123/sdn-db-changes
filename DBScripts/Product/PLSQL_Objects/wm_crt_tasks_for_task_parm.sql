create or replace procedure wm_crt_tasks_for_task_parm
(
    p_task_parm_id            in task_parm.task_parm_id%type,
    p_user_id                 in ucl_user.user_name%type,
    p_facility_id             in facility.facility_id%type,
    p_whse                    in facility.whse%type,
    p_tc_company_id           in wave_parm.tc_company_id%type,
    p_task_genrtn_ref_nbr_csv in varchar2,
    p_task_genrtn_ref_code    in alloc_invn_dtl.task_genrtn_ref_code%type,
    p_need_id                 in alloc_invn_dtl.need_id%type,
    p_rls_date_time           in task_hdr.rls_date_time%type
)
as
    v_is_lpn_cache_loaded   varchar2(1) := 'N';
begin
    -- unlike wrp, trp has rh.stat_code correctly set when a rule is toggled in the UI
    for trp_rec in
    (
        select trp.rule_id, trp.create_sec_tasks, trp.prty, trp.task_desc,
            coalesce(nullif(trp.max_wt, 0), 999999999) max_wt,
            coalesce(nullif(trp.max_vol, 0), 999999999) max_vol,
            coalesce(nullif(trp.task_capcty, 0), 99999) task_capcty,
            trp.task_create_stat_code, trp.labor_rate, trp.task_batch, 
            trp.task_type, trp.task_capcty_uom, trp.pick_to_tote_flag,
            trp.task_prty, trp.prt_task_list, trp.rpt_prtr_reqstr,
            trp.cart_plan_id, trp.max_lpns_per_task, trp.dest_locn_id,
            case when trp.group_lpn_to_tasks = 'Y' and trp.max_lpns_per_task > 0
                then 'Y' else 'N' end group_lpn_to_tasks
        from task_rule_parm trp
        where trp.task_parm_id = p_task_parm_id
            and exists
            (
                select 1 
                from rule_hdr rh
                join rule_sel_dtl rsd on rsd.rule_id = rh.rule_id
                where rh.rule_id = trp.rule_id and rh.rule_type = 'TS'
                    and rh.stat_code = 0 
            )
        order by trp.create_sec_tasks, trp.prty
    )
    loop
        wm_cs_log('Processing rule ' || trp_rec.rule_id);
        if (p_task_genrtn_ref_code in ('1','44') and v_is_lpn_cache_loaded = 'N'
            and trp_rec.group_lpn_to_tasks = 'Y')
        then
            -- olpn cache for all allocations in the wave
            insert into tmp_task_lpn_min_locn_seq
            (
                carton_nbr, min_locn_pick_seq
            )
            select aid.carton_nbr, min(lh.locn_pick_seq)
            from alloc_invn_dtl aid
            join table(cl_parse_endpoint_name_csv(p_task_genrtn_ref_nbr_csv)) t
                on t.column_value = aid.task_genrtn_ref_nbr
            join locn_hdr lh on lh.locn_id = aid.pull_locn_id
            where aid.task_genrtn_ref_code = p_task_genrtn_ref_code and aid.whse = p_whse 
                and aid.carton_nbr is not null
            group by aid.carton_nbr;

            v_is_lpn_cache_loaded := 'Y';
            wm_cs_log('Computed min locn pick seq for each olpn ' || sql%rowcount);
        end if;

        manh_task_load_aids_for_rule(p_whse, p_facility_id, trp_rec.rule_id, 
            trp_rec.create_sec_tasks, p_task_genrtn_ref_nbr_csv, p_task_genrtn_ref_code, 
            p_need_id, trp_rec.group_lpn_to_tasks, trp_rec.dest_locn_id);

        if (trp_rec.create_sec_tasks = 1)
        then
            -- update primary task_dtl's with secondary task info (next 
            -- task); these are actually created later during pick from tote
            manh_process_secondary_tasks(trp_rec.task_desc, trp_rec.task_type);
        else
            manh_calculate_aid_capacities(trp_rec.task_capcty_uom, trp_rec.labor_rate, p_whse,
                p_facility_id, trp_rec.max_wt, trp_rec.max_vol);

            manh_break_aids_for_tasking(trp_rec.task_capcty, trp_rec.max_wt, trp_rec.max_vol, 
                trp_rec.rule_id, trp_rec.cart_plan_id, trp_rec.group_lpn_to_tasks,
                trp_rec.max_lpns_per_task);

            -- sequence dtls using previously gen id or new print rules
            manh_generate_task_seq_nbr(p_facility_id, trp_rec.rule_id);

            manh_create_tasks_from_aid(p_whse, p_user_id, p_need_id, p_task_parm_id, 
                trp_rec.rule_id, trp_rec.task_batch, trp_rec.task_desc, trp_rec.task_type, 
                trp_rec.task_prty, trp_rec.task_create_stat_code, trp_rec.pick_to_tote_flag,
                trp_rec.prt_task_list, trp_rec.rpt_prtr_reqstr, trp_rec.cart_plan_id,
                p_rls_date_time);
                
            manh_task_updates_for_rule();
        end if;

        -- account for primary/sec tasks after task creation
        merge into alloc_invn_dtl aid
        using
        (
            select t.alloc_invn_dtl_id, t.qty_alloc
            from tmp_task_creation_selected_aid t
        ) iv on (iv.alloc_invn_dtl_id = aid.alloc_invn_dtl_id)
        when matched then
        update set aid.stat_code = decode(trp_rec.create_sec_tasks, 1, 92, 91),
            aid.user_id = p_user_id, aid.mod_date_time = sysdate, 
            aid.task_prty = trp_rec.task_prty, aid.task_type = trp_rec.task_type, 
            aid.task_batch = trp_rec.task_batch, 
            aid.qty_alloc = decode(trp_rec.create_sec_tasks, 1, aid.qty_alloc, iv.qty_alloc),
            aid.dest_locn_id = case when aid.invn_need_type in (10, 53, 54)
                then coalesce(trp_rec.dest_locn_id, aid.dest_locn_id) else aid.dest_locn_id end;
        wm_cs_log('AIDs converted to tasks ' || sql%rowcount);

        if (trp_rec.dest_locn_id is not null)
        then
            update lpn l
            set l.dest_sub_locn_id = trp_rec.dest_locn_id, l.last_updated_dttm = sysdate,
                l.last_updated_source = p_user_id
            where l.inbound_outbound_indicator = 'I' and l.c_facility_id = p_facility_id
                and exists
                (
                    select 1
                    from tmp_task_creation_selected_aid t
                    where t.cntr_nbr = l.tc_lpn_id and t.cd_master_id = l.tc_company_id
                        and t.invn_need_type in (10, 53, 54)
                );
            wm_cs_log('Lpns updated with dest locn on TRP ' || sql%rowcount);
        end if;

        commit;
    end loop;
end;
/
show errors;

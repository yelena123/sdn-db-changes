create or replace procedure wm_adj_lpn_alloc
(
    p_user_id          in user_profile.user_id%type,
    p_tc_company_id    in wave_parm.tc_company_id%type,
    p_facility_id      in facility.facility_id%type,
    p_whse             in facility.whse%type
)
as
begin

    insert into tmp_orig_oli_qty
    (
        line_item_id, parent_line_item_id, reference_line_item_id, available_qty,
        rng_avail_qty, lag_rng_avail_qty
    )
    select iv.line_item_id, iv.parent_line_item_id, iv.reference_line_item_id,
        iv.available_qty, iv.rng_avail_qty, iv.lag_rng_avail_qty
    from
    (
        select iv1.line_item_id, iv1.parent_line_item_id, iv1.reference_line_item_id,
            iv1.available_qty, t2.qty_to_dealloc agg_dealloc_qty, iv1.rng_avail_qty,
            lag(iv1.rng_avail_qty, 1, 0) over(partition by iv1.reference_line_item_id order by iv1.rng_avail_qty) lag_rng_avail_qty
        from
        (
            select orig.line_item_id, orig.parent_line_item_id, orig.reference_line_item_id,
                orig.allocated_qty - coalesce(orig.units_pakd, 0) - coalesce(orig.user_canceled_qty, 0) available_qty,
                sum(orig.allocated_qty - coalesce(orig.units_pakd, 0) - coalesce(orig.user_canceled_qty, 0))
                over(partition by orig.reference_line_item_id order by orig.priority desc, orig.order_id asc, orig.line_item_id desc) rng_avail_qty
            from tmp_dealloc_oli agg
            join order_line_item orig on orig.reference_order_id = agg.order_id
                and orig.reference_line_item_id = agg.line_item_id
            where (orig.do_dtl_status between 120 and 140)
                and orig.allocated_qty - coalesce(orig.units_pakd, 0) - coalesce(orig.user_canceled_qty, 0) > 0 
                and exists
                (
                    select 1
                    from orders o
                    where o.order_id = orig.order_id and o.is_original_order = 1
                )
        ) iv1
        join tmp_dealloc_oli t2 on t2.line_item_id = iv1.reference_line_item_id
    ) iv where iv.lag_rng_avail_qty < iv.agg_dealloc_qty;
    wm_cs_log('Original lines collected for processing ' || sql%rowcount, p_sql_log_level => 1);
    
    -- we're forced to cache the status because configurable do status
    -- pix drives off of orders after it is updated with the new status
    insert into tmp_old_ord_status
    (
        order_id, do_status
    )
    select o.order_id, o.do_status
    from orders o
    where o.o_facility_id = p_facility_id
        and exists
        (
            select 1
            from order_line_item oli
            join tmp_orig_oli_qty t on (t.line_item_id = oli.line_item_id)
            where oli.order_id = o.order_id
        );
    wm_cs_log('Old order status recorded fr pix ' || sql%rowcount, p_sql_log_level => 1);
    
    -- parent line updates     
    merge into order_line_item olip
    using
    (
        select t1.parent_line_item_id, t1.available_qty, t1.rng_avail_qty, 
            t2.qty_to_dealloc agg_dealloc_qty, t1.lag_rng_avail_qty
        from tmp_orig_oli_qty t1
        join tmp_dealloc_oli t2 on t2.line_item_id = t1.reference_line_item_id
    ) iv on (iv.parent_line_item_id = olip.line_item_id and iv.parent_line_item_id is not null)
    when matched then
    update set olip.order_qty = olip.order_qty + (case when iv.rng_avail_qty <= iv.agg_dealloc_qty then iv.available_qty
            else iv.agg_dealloc_qty - iv.lag_rng_avail_qty end),
        olip.adjusted_order_qty = adjusted_order_qty + (case when iv.rng_avail_qty <= iv.agg_dealloc_qty then iv.available_qty
            else iv.agg_dealloc_qty - iv.lag_rng_avail_qty end),
        olip.is_cancelled = decode(olip.do_dtl_status, 200, 0, olip.is_cancelled),
        olip.last_updated_source = p_user_id, olip.last_updated_dttm = sysdate;
    wm_cs_log('Parent lines updated ' || sql%rowcount);    
    
    -- original line updates
    merge into order_line_item oli
    using
    (
        select t1.line_item_id, t1.available_qty, t1.rng_avail_qty, t2.qty_to_dealloc agg_dealloc_qty,
            t1.lag_rng_avail_qty
        from tmp_orig_oli_qty t1
        join tmp_dealloc_oli t2 on t2.line_item_id = t1.reference_line_item_id
    ) iv on (iv.line_item_id = oli.line_item_id)
    when matched then
    update set 
        oli.allocated_qty = oli.allocated_qty - (case when iv.rng_avail_qty <= iv.agg_dealloc_qty then iv.available_qty
            else iv.agg_dealloc_qty - iv.lag_rng_avail_qty end),
        oli.order_qty = 
        (
            case when oli.parent_line_item_id is not null then
                oli.order_qty - (case when iv.rng_avail_qty <= iv.agg_dealloc_qty then iv.available_qty
                    else iv.agg_dealloc_qty - iv.lag_rng_avail_qty end)
            else oli.order_qty end
        ),
        oli.adjusted_order_qty = 
        (
            case when oli.parent_line_item_id is not null then
                oli.adjusted_order_qty - (case when iv.rng_avail_qty <= iv.agg_dealloc_qty then iv.available_qty
                    else iv.agg_dealloc_qty - iv.lag_rng_avail_qty end)
            else oli.adjusted_order_qty end
        ),
        oli.last_updated_source = p_user_id, oli.last_updated_dttm = sysdate;
    wm_cs_log('Original lines updated ' || sql%rowcount);

    -- original/parent line status update 
    merge into order_line_item oli
    using
    (
        select t1.parent_line_item_id line_item_id
        from tmp_orig_oli_qty t1
        union all
        select t1.line_item_id
        from tmp_orig_oli_qty t1
    ) iv on (iv.line_item_id = oli.line_item_id)
    when matched then
    update set
        oli.do_dtl_status =
        (
            case 
            when oli.allocated_qty > 0 then
                case
                when oli.parent_line_item_id is not null 
                    and oli.order_qty = coalesce(oli.shipped_qty, 0) then 190
                when oli.parent_line_item_id is not null 
                    and oli.order_qty = coalesce(oli.units_pakd, 0) then 150
                when oli.order_qty > oli.allocated_qty then 120
                when oli.parent_line_item_id is not null 
                    and coalesce(oli.units_pakd, 0) + coalesce(oli.user_canceled_qty,0) = 0 then 130
                when oli.parent_line_item_id is not null 
                    and coalesce(oli.allocated_qty,0) > (coalesce(oli.units_pakd, 0) + coalesce(oli.user_canceled_qty,0)) then  140
                end
            else 110
            end
        ),
        oli.reference_order_id = (case when oli.allocated_qty = 0 then null
            else oli.reference_order_id end),
        oli.reference_line_item_id = (case when oli.allocated_qty = 0 then null
            else oli.reference_line_item_id end),
        oli.last_updated_source = p_user_id , oli.last_updated_dttm = sysdate;
    wm_cs_log('Original/Parent lines status,referencing updated ' || sql%rowcount); 
    
    -- olis records for child split lines
    delete from order_line_item_size olis
    where exists
    (
        select 1
        from tmp_orig_oli_qty t1
        where t1.line_item_id = olis.line_item_id
    )
    and not exists
    (
        select 1
        from order_line_item oli
        where oli.line_item_id = olis.line_item_id
    );
    wm_cs_log('Olis records deleted ' || sql%rowcount, p_sql_log_level => 1);

    -- delete child lines which were nullified
    delete from order_line_item oli
    where oli.order_qty = 0
        and exists
        (
            select 1
            from tmp_orig_oli_qty t1
            where t1.line_item_id = oli.line_item_id
                and t1.parent_line_item_id = oli.parent_line_item_id
                and t1.parent_line_item_id is not null
        );
     wm_cs_log('Child lines deleted ' || sql%rowcount, p_sql_log_level => 1);
     
    -- dealloc qty on agg line
    merge into order_line_item oli
    using
    (
        select t.line_item_id, t.qty_to_dealloc
        from tmp_dealloc_oli t
    ) iv on (iv.line_item_id = oli.line_item_id)
    when matched then
    update set oli.last_updated_source = p_user_id, oli.last_updated_dttm = sysdate,
        oli.allocated_qty = oli.allocated_qty - iv.qty_to_dealloc,
        oli.order_qty = oli.order_qty - iv.qty_to_dealloc,
        oli.adjusted_order_qty = oli.adjusted_order_qty - iv.qty_to_dealloc
    delete where oli.order_qty <= 0;
    wm_cs_log('Aggregate line qty adjusted ' || sql%rowcount, p_sql_log_level => 1);
    
    -- reduce on pkt
    merge into pkt_dtl pd
    using
    (
        select t.order_id, t.line_item_id, t.qty_to_dealloc
        from tmp_dealloc_oli t
    ) iv on (iv.line_item_id = pd.reference_line_item_id
        and iv.order_id = pd.reference_order_id)
    when matched then
    update set pd.user_id = p_user_id, pd.mod_date_time = sysdate,
      --  pd.orig_ord_qty = pd.orig_ord_qty - iv.qty_to_dealloc,
      --  pd.orig_pkt_qty = pd.orig_pkt_qty - iv.qty_to_dealloc,
        pd.pkt_qty = pd.pkt_qty - iv.qty_to_dealloc
    delete where pd.pkt_qty <= 0;
    wm_cs_log('pkt_dtl adjustments done ' || sql%rowcount);
    
    merge into pkt_hdr ph
    using
    (
        select t1.pkt_ctrl_nbr, coalesce(sum(pd.pkt_qty), 0) pkt_qty
        from tmp_lpn_alloc_dtl t1
        left join pkt_dtl pd on pd.pkt_ctrl_nbr = t1.pkt_ctrl_nbr
        where exists
        (
            select 1
            from tmp_orig_oli_qty t2 
            where t2.reference_line_item_id = t1.line_item_id
        )
        group by t1.pkt_ctrl_nbr
    ) iv
    on (iv.pkt_ctrl_nbr = ph.pkt_ctrl_nbr)
    when matched then
    update set ph.stat_code = (case when iv.pkt_qty <= 0 then 99 else ph.stat_code end),
    ph.original_tc_order_id = (case when iv.pkt_qty <= 0 then null else original_tc_order_id end),
    ph.user_id = p_user_id, ph.mod_date_time = sysdate;
    wm_cs_log('pkt_hdr adjustments done ' || sql%rowcount);

    -- reset orig do_status
    merge into orders o
    using
    (
        select oli.order_id,
            case
            when min(oli.do_dtl_status) in (110, 112)
                and min(oli.do_dtl_status) != max(oli.do_dtl_status)
                then 120
            when min(oli.do_dtl_status) = 130
                and max(oli.do_dtl_status) > 130 then 140
            else coalesce(min(oli.do_dtl_status), 200)
            end do_status
        from order_line_item oli
        -- we're dealloc qty on at least one oli on this order, so its got to be
        -- in < 200 status
        where oli.do_dtl_status < 200
           and exists
           (
               select 1
               from tmp_old_ord_status t
               where t.order_id = oli.order_id
           )
        group by oli.order_id
    ) iv on (iv.order_id = o.order_id)
    when matched then
    update set o.do_status = iv.do_status, o.last_updated_source = p_user_id,
        o.last_updated_dttm = sysdate,
        o.parent_order_id = (case when iv.do_status = 110 then null else o.parent_order_id end);
    wm_cs_log('original Order status updated ' || sql%rowcount);
    
    -- reset agg status
    merge into orders o
    using
    (
        select t.order_id agg_order_id, coalesce(min(o1.do_status), 200) min_orig_status
        from tmp_dealloc_oli t
        left join orders o1 on (t.order_id = o1.parent_order_id and o1.o_facility_id = p_facility_id 
            and o1.is_original_order = 1)
        group by t.order_id
    ) iv on (iv.agg_order_id = o.order_id)
    when matched then
    update set o.do_status = (case when o.do_status > 130 and iv.min_orig_status = 130 then 140 else iv.min_orig_status end),
        o.last_updated_source = p_user_id, o.last_updated_dttm = sysdate;
    wm_cs_log('Aggregate Order status updated ' || sql%rowcount, p_sql_log_level => 1);
    
    merge into wm_inventory wi
    using
    (
        select t.cntr_nbr, t.item_id, sum(t.qty_alloc - t.qty_pulld) item_dealloc_qty,
            t.invn_type, t.prod_stat, t.cntry_of_orgn, t.batch_nbr, 
            t.sku_attr_1, t.sku_attr_2, t.sku_attr_3, t.sku_attr_4, 
            t.sku_attr_5
        from tmp_lpn_alloc_dtl t
        where exists
        (
            select 1  
            from tmp_orig_oli_qty t2 
            where t2.reference_line_item_id = t.line_item_id
        )
        group by t.cntr_nbr, t.item_id, t.invn_type, t.prod_stat, t.cntry_of_orgn, 
            t.batch_nbr, t.sku_attr_1, t.sku_attr_2, t.sku_attr_3, 
            t.sku_attr_4, t.sku_attr_5
    ) iv on (iv.cntr_nbr = wi.tc_lpn_id and wi.tc_company_id = p_tc_company_id 
        and wi.c_facility_id = p_facility_id and wi.inbound_outbound_indicator = 'I')
    when matched then
    update set wi.last_updated_source = p_user_id, wi.last_updated_dttm = sysdate,
        wi.wm_allocated_qty = wi.wm_allocated_qty - iv.item_dealloc_qty,
        wi.wm_version_id = (wi.wm_version_id + 1)
        --, wi.location_id = null ** not updating this field based on latest tech doc
    where iv.item_id = wi.item_id
        and (iv.invn_type = '*' or wi.inventory_type = '*'
            or coalesce(iv.invn_type, '0') = coalesce(wi.inventory_type, '0'))
        and (iv.prod_stat = '*' or wi.product_status = '*'
            or coalesce(iv.prod_stat, '0') = coalesce(wi.product_status, '0'))
        and (iv.batch_nbr = '*' or wi.batch_nbr = '*'
            or coalesce(iv.batch_nbr, '0') = coalesce(wi.batch_nbr, '0'))
        and (iv.cntry_of_orgn = '*' or wi.cntry_of_orgn = '*'
            or coalesce(iv.cntry_of_orgn, '0') = coalesce(wi.cntry_of_orgn, '0'))
        and (iv.sku_attr_1 = '*' or wi.item_attr_1 = '*'
            or coalesce(iv.sku_attr_1, '0') = coalesce(wi.item_attr_1, '0'))
        and (iv.sku_attr_2 = '*' or wi.item_attr_2 = '*'
            or coalesce(iv.sku_attr_2, '0') = coalesce(wi.item_attr_2, '0'))
        and (iv.sku_attr_3 = '*' or wi.item_attr_3 = '*'
            or coalesce(iv.sku_attr_3, '0') = coalesce(wi.item_attr_3, '0'))
        and (iv.sku_attr_4 = '*' or wi.item_attr_4 = '*'
            or coalesce(iv.sku_attr_4, '0') = coalesce(wi.item_attr_4, '0'))
        and (iv.sku_attr_5 = '*' or wi.item_attr_5 = '*'
            or coalesce(iv.sku_attr_5, '0') = coalesce(wi.item_attr_5, '0'));
    wm_cs_log('WM Inventory details updated '|| sql%rowcount);

    -- handle task_dtl which have been partially fulfilled 
    merge into task_dtl td
    using
    (
        select t.task_id, t.cntr_nbr, t.line_item_id
        from tmp_lpn_alloc_dtl t
        where t.task_id is not null
            and exists
            (
                select 1
                from tmp_orig_oli_qty t2 
                where t2.reference_line_item_id = t.line_item_id
            )
    ) iv on (iv.task_id = td.task_id and td.cntr_nbr = iv.cntr_nbr 
        and iv.line_item_id = td.line_item_id)
    when matched then
    update
    set td.stat_code = (case when td.qty_pulld > 0 then 90
        when td.qty_pulld = 0 then 99 end), 
        td.qty_alloc = (case when td.qty_pulld > 0 then td.qty_pulld
            when td.qty_pulld = 0 then 0 end),
        td.mod_date_time = sysdate, td.user_id = p_user_id;
    wm_cs_log('tasks_dtl updated ' || sql%rowcount);
    
    -- handle task_hdr    
    merge into task_hdr th
    using
    (
        select t.task_id, coalesce(min(td.stat_code), 99) min_stat_code,
            coalesce(max(td.stat_code), 99) max_stat_code
        from tmp_lpn_alloc_dtl t
        left join task_dtl td on t.task_id = td.task_id 
        where t.task_id is not null
            and exists
            (
                select 1
                from tmp_orig_oli_qty t2 
                where t2.reference_line_item_id = t.line_item_id
            )
        group by t.task_id
    ) iv
    on (th.task_id = iv.task_id)
    when matched then
    update set th.mod_date_time = sysdate, th.user_id = p_user_id,
    th.stat_code = ( case 
        when (iv.min_stat_code = 90 and iv.max_stat_code >= 90) then 90 
        when (iv.min_stat_code = 99 and iv.max_stat_code >= 99) then 99
        else th.stat_code
        end ) ;
    wm_cs_log('tasks_hdr updated ' || sql%rowcount); 

/*
    delete from task_hdr th 
    where exists
    (
        select 1
        from tmp_lpn_alloc_dtl t
        where t.task_id = th.task_id
            and t.task_id is not null
            and not exists
            (
                select 1
                from task_dtl td
                where td.task_id = t.task_id
            )
    );    
    wm_cs_log('Orphan tasks_hdr rows deleted ' || sql%rowcount); 
*/    
    -- handle alloc_invn_dtl records
    merge into alloc_invn_dtl aid
    using
    (
        select t.cntr_nbr, t.line_item_id, t.stat_code
        from tmp_lpn_alloc_dtl t
        where t.task_id is null
            and exists
            (
                select 1
                from tmp_orig_oli_qty t2 
                where t2.reference_line_item_id = t.line_item_id
            )
    ) iv on (iv.cntr_nbr = aid.cntr_nbr and aid.line_item_id = iv.line_item_id and aid.whse = p_whse)
    when matched then
    update
    set aid.qty_alloc = (case when aid.qty_pulld > 0 then aid.qty_pulld
        when aid.qty_pulld = 0 then 0 end), 
        aid.stat_code = (case when aid.qty_pulld > 0 then 90
        when aid.qty_pulld = 0 then 99 end),
        aid.mod_date_time = sysdate, aid.user_id = p_user_id;
    wm_cs_log('alloc_invn_dtl adjusted '|| sql%rowcount );
    
    wm_complete_dealloc_lpn(p_user_id, p_tc_company_id, p_facility_id, p_whse);
end;
/
show errors;

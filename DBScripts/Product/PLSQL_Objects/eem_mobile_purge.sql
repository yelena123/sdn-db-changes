CREATE OR REPLACE PROCEDURE EEM_MOBILE_PURGE (
   atccompanyid   shipment.tc_company_id%TYPE,
   mdays          NUMBER,
   tdays          NUMBER
)
AS
BEGIN

-- mdays -  EEM Mobile days
-- tdays -  TRAN LOG days

DELETE FROM MOBILE_TRANSACTION_MESSAGE
         WHERE MOBILE_TRANSACTION_MESSAGE_ID IN
                  (SELECT MOBILE_TRANSACTION_MESSAGE_ID
                     FROM MOBILE_TRANSACTION_LOG
                    WHERE MOBILE_SESSION_ID IN
                             (SELECT MOBILE_SESSION_ID
                                FROM MOBILE_SESSION
                               WHERE created_dttm < TRUNC (SYSDATE) - mdays
                               AND company_id=atccompanyid));

   DELETE FROM MOBILE_TRANSACTION_LOG
         WHERE MOBILE_SESSION_ID IN
                  (SELECT MOBILE_SESSION_ID
                     FROM MOBILE_SESSION
                    WHERE created_dttm < TRUNC (SYSDATE) - mdays
                    AND company_id=atccompanyid);
  
  DELETE FROM MOBILE_TRANSACTION_REQUEST 
  	WHERE MOBILE_TRANSACTION_LOG_ID IN
  		( SELECT MOBILE_TRANSACTION_LOG_ID 
  		  FROM MOBILE_TRANSACTION_LOG
         	  WHERE MOBILE_SESSION_ID IN
			  (SELECT MOBILE_SESSION_ID
			     FROM MOBILE_SESSION
			    WHERE created_dttm < TRUNC (SYSDATE) - mdays
			    AND company_id=atccompanyid)
	         );

  DELETE FROM MOBILE_SYNC_OBJECT
         WHERE MOBILE_DEVICE_ID IN
                  (SELECT MOBILE_DEVICE_ID
                     FROM MOBILE_DEVICE
                    WHERE CURRENT_SESSION_ID IN
                             (SELECT MOBILE_SESSION_ID
                                FROM MOBILE_SESSION
                               WHERE created_dttm < TRUNC (SYSDATE) - mdays
                               AND company_id=atccompanyid));
   
   
   DELETE FROM MOBILE_DEVICE
         WHERE CURRENT_SESSION_ID IN
                  (SELECT MOBILE_SESSION_ID
                     FROM MOBILE_SESSION
                    WHERE created_dttm < TRUNC (SYSDATE) - mdays
                    AND company_id=atccompanyid);
   
   
   DELETE FROM MOBILE_SESSION
         WHERE created_dttm < TRUNC (SYSDATE) - mdays
         AND company_id=atccompanyid;

COMMIT;

   DELETE FROM tran_log_message
         WHERE tran_log_id IN (
                              SELECT tran_log_id
                                FROM tran_log
                               WHERE last_updated_dttm <
                                                        TRUNC (SYSDATE)
                                                        - tdays);

   DELETE FROM tran_log
         WHERE last_updated_dttm < TRUNC (SYSDATE) - tdays;
         
COMMIT ;
EXCEPTION 
WHEN OTHERS THEN
	ROLLBACK ;
	DBMS_OUTPUT.PUT_LINE('EXCEPTION : CODE -> ' ||TO_CHAR(SQLCODE)|| ' EXCEPTION : MESSAGE -> '||SQLERRM );

END EEM_MOBILE_PURGE;
/
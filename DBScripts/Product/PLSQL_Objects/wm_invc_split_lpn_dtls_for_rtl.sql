create or replace procedure wm_invc_split_lpn_dtls_for_rtl
(
    p_facility_id       in facility.facility_id%type,
    p_tc_company_id     in company.company_id%type,
    p_user_id           in user_profile.user_id%type,
    p_srl_trk_flag      in number
)
as
begin
    insert into tmp_refactored_lpn_dtls
    (
        lpn_id, grp_ld_id, orig_oli_id, new_ld_id, new_units_pakd
    )
    select iv3.lpn_id, iv3.grp_ld_id, iv3.orig_line_item_id, lpn_detail_id_seq.nextval,
        case when iv3.curr_units_pakd >= iv3.rng_avail_qty_sum - iv3.rng_prev_units_pakd
            then iv3.rng_avail_qty_sum 
                - (case when iv3.rng_prev_units_pakd > iv3.rng_prev_avail_qty_sum 
                then iv3.rng_prev_units_pakd else iv3.rng_prev_avail_qty_sum end)
            else iv3.curr_units_pakd + iv3.rng_prev_units_pakd 
                - (case when iv3.rng_prev_units_pakd > iv3.rng_prev_avail_qty_sum 
                then iv3.rng_prev_units_pakd else iv3.rng_prev_avail_qty_sum end)
        end new_units_pakd
    from
    (
         select iv2.lpn_id, iv2.grp_ld_id, iv2.orig_line_item_id,
            iv2.curr_units_pakd, iv2.rng_avail_qty_sum, iv2.rng_prev_units_pakd,
            lag(iv2.rng_avail_qty_sum, 1, 0) over(partition by iv2.grp_ld_id
                order by iv2.priority, iv2.orig_line_item_id) rng_prev_avail_qty_sum
        from
        (
             -- orig and agg oli should share the same attrs
            -- grp_ld_id uniquely represents a group (lpn/agg line/item attrs)
            select iv.lpn_id, iv.grp_ld_id, iv.curr_units_pakd,
                orig.line_item_id orig_line_item_id,
                orig.priority, iv.rng_prev_units_pakd,
                sum(orig.units_pakd - coalesce(orig.shipped_qty, 0)) 
                    over(partition by grp_ld_id 
                        order by orig.priority, orig.line_item_id) rng_avail_qty_sum
            from
            (
                select iv4.lpn_id, iv4.agg_line_item_id, iv4.grp_ld_id, 
                    iv4.curr_units_pakd, iv4.batch_nbr, iv4.cntry_of_orgn, 
                    iv4.inventory_type, iv4.product_status, iv4.item_attr_1, 
                    iv4.item_attr_2, iv4.item_attr_3, iv4.item_attr_4, iv4.item_attr_5, 
                    sum(iv4.prev_units_pakd) over(partition by iv4.agg_line_item_id
                        order by iv4.grp_ld_id) rng_prev_units_pakd
                from
                (
                    -- group LD's pointing to the same agg line and also with the same attrs
                    select ld.lpn_id, ld.distribution_order_dtl_id agg_line_item_id,
                        ld.batch_nbr, ld.cntry_of_orgn, ld.inventory_type, 
                        ld.product_status, ld.item_attr_1, ld.item_attr_2, 
                        ld.item_attr_3, ld.item_attr_4, ld.item_attr_5,
                        sum(ld.size_value) curr_units_pakd, 
                        min(ld.lpn_detail_id) grp_ld_id,
                        lag(sum(ld.size_value), 1, 0) over(partition by ld.distribution_order_dtl_id
                            order by min(ld.lpn_detail_id)) prev_units_pakd
                    from lpn_detail ld
                     join tmp_invc_lpn_list t on t.lpn_id = ld.lpn_id
                    join orders o on o.order_id = t.order_id and o.is_original_order = 0
                    where ld.size_value > 0
                    group by ld.lpn_id, ld.distribution_order_dtl_id, 
                        ld.batch_nbr, ld.cntry_of_orgn, ld.inventory_type, 
                        ld.product_status, ld.item_attr_1, ld.item_attr_2, 
                        ld.item_attr_3, ld.item_attr_4, ld.item_attr_5
                ) iv4
            ) iv
            join order_line_item orig on orig.reference_line_item_id = iv.agg_line_item_id
            where orig.do_dtl_status < 190 and orig.units_pakd - coalesce(orig.shipped_qty, 0) > 0
        ) iv2
    ) iv3
    where iv3.rng_avail_qty_sum > iv3.rng_prev_units_pakd
        and iv3.rng_prev_avail_qty_sum < iv3.rng_prev_units_pakd + iv3.curr_units_pakd
    log errors (to_char(sysdate));
    wm_cs_log('Lpn_details refactored to ' || sql%rowcount || ' rows');

    -- create new ld's pointing to orig oli; copy data from the old ld's that
    -- were pointing to the agg oli
    insert into lpn_detail
    (
        lpn_id, lpn_detail_id, distribution_order_dtl_id,
        size_value, shipped_qty, initial_qty, tc_company_id, lpn_detail_status, 
        business_partner_id, item_id, gtin, std_pack_qty, std_sub_pack_qty, 
        std_bundle_qty, incubation_date, expiration_date,
        ship_by_date, sell_by_dttm, consumption_priority_dttm,
        manufactured_dttm, cntry_of_orgn, inventory_type,
        product_status, item_attr_1, item_attr_2,
        item_attr_3, item_attr_4, item_attr_5,
        asn_dtl_id, pack_weight, estimated_weight,
        estimated_volume, weight,
        qty_uom_id, qty_uom_id_base, weight_uom_id, volume_uom_id,
        assort_nbr, cut_nbr, purchase_orders_id,
        tc_purchase_orders_id, purchase_orders_line_id, tc_purchase_orders_line_id,
        hibernate_version, internal_order_id,instrtn_code_1,
        instrtn_code_2, instrtn_code_3, instrtn_code_4,
        instrtn_code_5, created_source_type, created_source,
        created_dttm, last_updated_source_type, last_updated_source,
        last_updated_dttm, vendor_item_nbr, manufactured_plant,
        batch_nbr, assigned_qty, prepack_group_code,
        pack_code, qty_conv_factor, tc_order_line_id         -- Added tc_order_line_id
    )
    select t.lpn_id, t.new_ld_id, t.orig_oli_id,
        t.new_units_pakd, t.new_units_pakd, t.new_units_pakd, p_tc_company_id, 90,
        ld.business_partner_id, ld.item_id, ld.gtin, ld.std_pack_qty, 
        ld.std_sub_pack_qty, ld.std_bundle_qty, ld.incubation_date, 
        ld.expiration_date, ld.ship_by_date, ld.sell_by_dttm, 
        ld.consumption_priority_dttm, ld.manufactured_dttm, ld.cntry_of_orgn, 
        ld.inventory_type, ld.product_status, ld.item_attr_1, ld.item_attr_2, 
        ld.item_attr_3, ld.item_attr_4, ld.item_attr_5, ld.asn_dtl_id, ld.pack_weight, 
        ld.estimated_weight, ld.estimated_volume, ld.weight, 
        ld.qty_uom_id, ld.qty_uom_id_base, ld.weight_uom_id, ld.volume_uom_id, ld.assort_nbr, ld.cut_nbr,
        ld.purchase_orders_id, ld.tc_purchase_orders_id, ld.purchase_orders_line_id, 
        ld.tc_purchase_orders_line_id, ld.hibernate_version, ld.internal_order_id, 
        ld.instrtn_code_1, ld.instrtn_code_2, ld.instrtn_code_3, ld.instrtn_code_4,
        ld.instrtn_code_5, 5, p_user_id, systimestamp, 5, p_user_id,
        systimestamp, ld.vendor_item_nbr, ld.manufactured_plant, ld.batch_nbr, 
        ld.assigned_qty, ld.prepack_group_code, ld.pack_code , ld.qty_conv_factor, ld.tc_order_line_id -- Added tc_order_line_id
    from tmp_refactored_lpn_dtls t
    join lpn_detail ld on ld.lpn_id = t.lpn_id and ld.lpn_detail_id = t.grp_ld_id
    log errors (to_char(sysdate));
    wm_cs_log('New lpn_details created ' || sql%rowcount); 

    if (p_srl_trk_flag = 1)
    then
        -- sync associated srl nbrs with the refactored lpn dtls
        -- assumption: each srl nbr corresponds to a qty of 1 and there will
        -- always be as many srl nbrs as the total size_value within the lpn
        merge into srl_nbr_track snt
        using
        (
            select iv3.lpn_detail_id, iv3.orig_line_item_id, iv3.srl_nbr_trk_id,
                oli.order_id orig_order_id
            from
            (
                -- LD's contain the actual item attrs; the agg OLI may not - it 
                -- may be summmarized by batch, wildcarded etc.
                -- srl nbr rows contain the item attrs as well, so we need to 
                -- match SNT with attrs included
                select snt.srl_nbr_trk_id, iv2.lpn_detail_id, iv2.orig_line_item_id, 
 
                    iv2.rng_sum_ld_qty, iv2.rng_sum_prev_ld_qty,
                    dense_rank() over(partition by snt.lpn_id, snt.item_id,
                        wi.batch_nbr, wi.cntry_of_orgn, wi.inventory_type, 
                        wi.product_status, wi.item_attr_1, wi.item_attr_2, 
                        wi.item_attr_3, wi.item_attr_4, wi.item_attr_5 
                        order by snt.srl_nbr_trk_id) rn
                from srl_nbr_track snt
                join wm_inventory wi on wi.wm_inventory_id = snt.wm_inventory_id
                join
                (
                    select iv1.lpn_id, iv1.lpn_detail_id, iv1.item_id, iv1.batch_nbr,
                        iv1.cntry_of_orgn, iv1.inventory_type, iv1.product_status, 
                        iv1.item_attr_1, iv1.item_attr_2, iv1.item_attr_3, 
                         iv1.item_attr_4, iv1.item_attr_5, iv1.orig_line_item_id,
                        iv1.rng_sum_ld_qty,
                        lag(iv1.rng_sum_ld_qty, 1, 0) over(partition by iv1.lpn_id, 
                            iv1.item_id, iv1.batch_nbr, iv1.cntry_of_orgn, 
                            iv1.inventory_type, iv1.product_status, iv1.item_attr_1, 
                            iv1.item_attr_2, iv1.item_attr_3, iv1.item_attr_4, 
                            iv1.item_attr_5 order by iv1.lpn_detail_id) rng_sum_prev_ld_qty
                    from
                    (
                        -- many orig ld's in an lpn can contain the same item attrs
                        select ld.lpn_id, ld.lpn_detail_id, ld.item_id, ld.batch_nbr,
                            ld.cntry_of_orgn, ld.inventory_type, ld.product_status, 
                            ld.item_attr_1, ld.item_attr_2, ld.item_attr_3, 
                            ld.item_attr_4, ld.item_attr_5, 
                            ld.distribution_order_dtl_id orig_line_item_id,
                            sum(ld.size_value) over(partition by ld.lpn_id, ld.item_id,
                                ld.batch_nbr, ld.cntry_of_orgn, ld.inventory_type, 
                                ld.product_status, ld.item_attr_1, ld.item_attr_2, 
                                ld.item_attr_3, ld.item_attr_4, ld.item_attr_5
                                order by ld.lpn_detail_id) rng_sum_ld_qty
                         from lpn_detail ld
                        join item_wms iw on iw.item_id = ld.item_id 
                            and iw.srl_nbr_reqd in (1, 2, 4)
                        join tmp_refactored_lpn_dtls t on t.lpn_id = ld.lpn_id 
                            and t.new_ld_id = ld.lpn_detail_id
                    ) iv1
                ) iv2 on iv2.lpn_id = snt.lpn_id and iv2.item_id = snt.item_id
                    and coalesce(iv2.batch_nbr, ' ') = coalesce(wi.batch_nbr, ' ')
                    and coalesce(iv2.cntry_of_orgn, ' ') = coalesce(wi.cntry_of_orgn, ' ')
                    and coalesce(iv2.inventory_type, ' ') = coalesce(wi.inventory_type, ' ')
                    and coalesce(iv2.product_status, ' ') = coalesce(wi.product_status, ' ')
                    and coalesce(iv2.item_attr_1, ' ') = coalesce(wi.item_attr_1, ' ')
                    and coalesce(iv2.item_attr_2, ' ') = coalesce(wi.item_attr_2, ' ')
                    and coalesce(iv2.item_attr_3, ' ') = coalesce(wi.item_attr_3, ' ')
                    and coalesce(iv2.item_attr_4, ' ') = coalesce(wi.item_attr_4, ' ')
                    and coalesce(iv2.item_attr_5, ' ') = coalesce(wi.item_attr_5, ' ')
                where snt.stat_code < 99
            ) iv3
            join order_line_item oli on oli.line_item_id = iv3.orig_line_item_id            
            where iv3.rn <= iv3.rng_sum_ld_qty and iv3.rn > rng_sum_prev_ld_qty
        ) iv on (iv.srl_nbr_trk_id = snt.srl_nbr_trk_id)
        when matched then
        update set snt.wm_inventory_id = null, snt.last_updated_dttm = sysdate,
            snt.last_updated_source = p_user_id, snt.stat_code = 90,
            snt.lpn_detail_id = iv.lpn_detail_id,
            snt.distribution_order = iv.orig_order_id,
            snt.distribution_order_line_id = iv.orig_line_item_id
        log errors (to_char(sysdate));
        wm_cs_log('Srl nbr trk rows refactored ' || sql%rowcount);

        update srl_nbr_track snt
        set snt.last_updated_dttm = sysdate, snt.wm_inventory_id = null,
            snt.last_updated_source = p_user_id, snt.stat_code = 90
        where snt.stat_code < 90 
            and exists
            (
                select 1
                from tmp_invc_lpn_list t
                where t.lpn_id = snt.lpn_id
            );
        wm_cs_log('Lpn srl nbrs updated to shipped ' || sql%rowcount);
    end if;

    -- clean these out now prior to removing any refactored ld's
    delete from wm_inventory wi
    where exists
    (
        select 1
        from tmp_invc_lpn_list t
        where t.lpn_id = wi.lpn_id
    )
    log errors (to_char(sysdate));
    wm_cs_log('Wm invn rows removed '|| sql%rowcount);

--todo! multiple LD's pointing to same OLI needs to be accounted for
    merge into lpn_catch_weight lcw
    using
    (
        select t.lpn_id, t.grp_ld_id, min(t.new_ld_id) lpn_detail_id
        from tmp_refactored_lpn_dtls t
        group by t.lpn_id, t.grp_ld_id
    ) iv on (iv.lpn_id = lcw.lpn_id)
    when matched then
    update set lcw.lpn_detail_id = iv.lpn_detail_id, lcw.last_updated_dttm = sysdate,
        lcw.userid = p_user_id
    where iv.grp_ld_id = lcw.lpn_detail_id
    log errors (to_char(sysdate));

    delete from lpn_detail ld
    where exists
        (
            select 1
            from tmp_refactored_lpn_dtls t1
            where t1.lpn_id = ld.lpn_id
        )
        and not exists
        (
            select 1
            from tmp_refactored_lpn_dtls t2
            where t2.lpn_id = ld.lpn_id and t2.new_ld_id = ld.lpn_detail_id
        )
    log errors (to_char(sysdate));
    wm_cs_log('Old lpn_details removed '|| sql%rowcount);
end;
/
show errors;

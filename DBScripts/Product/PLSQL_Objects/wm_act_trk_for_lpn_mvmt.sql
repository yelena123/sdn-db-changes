create or replace procedure wm_act_trk_for_lpn_mvmt
(
    p_user_id         in user_profile.user_id%type,
    p_facility_id     in facility.facility_id%type,
    p_tc_company_id   in company.company_id%type,
    p_wkstn_id        in prod_trkg_tran.wkstn_id%type,
    p_task_id         in task_hdr.task_id%type,
    p_dest_locn_id    in locn_hdr.locn_id%type
)
as
    v_new_lpn_status  number(2) := 64;
    v_whse            facility.whse%type;
    v_tran_nbr        prod_trkg_tran.tran_nbr%type;
begin
    select f.whse
    into v_whse
    from facility f
    where f.facility_id = p_facility_id;

    v_tran_nbr := prod_trkg_tran_id_seq.nextval;
    insert into prod_trkg_tran
    (
        tran_type, tran_code, whse, seq_nbr, user_id, wkstn_id, item_id, cntr_nbr,
        nbr_of_cases, nbr_units, from_locn, to_locn, ref_code_id_1, ref_field_1,
        begin_date, create_date_time, mod_date_time, old_stat_code, task_id,
        new_stat_code, stat_code, module_name, menu_optn_name, plt_id,
        tran_nbr, prod_trkg_tran_id, cd_master_id, pkt_seq_nbr, nbr_of_piks,
        nbr_scan, sam, wm_version_id, task_hdr_id, end_date
    )
    select '200', decode(iv.alloc_uom, 'P', '002', '001'), v_whse, rownum,
        p_user_id, p_wkstn_id, iv.item_id, iv.tc_lpn_id, 1, iv.nbr_units, 
        null, p_dest_locn_id, '08', iv.tc_asn_id, sysdate, sysdate, 
        sysdate, iv.lpn_facility_status, p_task_id, v_new_lpn_status, 0, 
        'Invn Mvmt', 'User Directed P/A', iv.tc_parent_lpn_id, v_tran_nbr,
        prod_trkg_tran_id_seq.nextval, p_tc_company_id, 0, 0, 0, 0, 1, 
        iv.task_hdr_id, sysdate
    from 
    (
        select l.tc_lpn_id, l.tc_asn_id, l.tc_parent_lpn_id, l.item_id, 
            l.lpn_facility_status, iv2.alloc_uom, iv2.task_hdr_id,
            sum(ld.size_value) nbr_units
        from
        (
                select td.cntr_nbr tc_lpn_id, td.alloc_uom, td.task_hdr_id
                from task_dtl td
                where td.task_id = p_task_id and td.alloc_invn_code = 1
                    and td.full_cntr_allocd = 'Y' and td.stat_code < '90'
                union all
                select l.tc_lpn_id, td.alloc_uom, td.task_hdr_id
                from task_dtl td
                join lpn l on l.tc_parent_lpn_id = td.cntr_nbr
                where td.task_id = p_task_id and td.alloc_uom = 'P' and td.stat_code < '90'
        ) iv2
        join lpn l on l.tc_lpn_id = iv2.tc_lpn_id
            and l.tc_company_id = p_tc_company_id and l.lpn_type = 1
            and l.inbound_outbound_indicator = 'I'
            and l.c_facility_id = p_facility_id
        join lpn_detail ld on ld.lpn_id = l.lpn_id
        group by l.tc_lpn_id, l.tc_asn_id, l.tc_parent_lpn_id, l.item_id,
            l.lpn_facility_status, iv2.alloc_uom, iv2.task_hdr_id
    ) iv;
end;
/
show errors;

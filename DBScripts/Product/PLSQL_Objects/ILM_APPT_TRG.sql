CREATE OR REPLACE TRIGGER ILM_APPT_TRG
   BEFORE INSERT OR UPDATE
   ON ILM_APPOINTMENTS
   REFERENCING OLD AS OLD NEW AS NEW
   FOR EACH ROW
DECLARE
   vOldValue                VARCHAR2 (100);
   vnewValue                VARCHAR2 (100);
   vEQUIPMENT_INSTANCE_ID   NUMBER;
BEGIN
   IF UPDATING
   THEN
      BEGIN
         SELECT EQUIPMENT_INSTANCE_ID
           INTO vEQUIPMENT_INSTANCE_ID
           FROM ILM_APPT_EQUIPMENTS_VIEW
          WHERE     APPOINTMENT_ID = :OLD.APPOINTMENT_ID
                AND company_id = :OLD.company_id
                AND EQUIPMENT_TYPE = 'TRAILER';
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      IF :NEW.FACILITY_ID <> :OLD.FACILITY_ID
      THEN
         INS_APPOINTMENT_EVENT (:OLD.APPOINTMENT_ID,
                                'FACILITY ',
                                :OLD.FACILITY_ID,
                                :NEW.FACILITY_ID,
                                :NEW.LAST_UPDATED_SOURCE_TYPE,
                                :NEW.LAST_UPDATED_SOURCE);
      END IF;

      IF :NEW.CURRENT_LOCATION_ID <> :OLD.CURRENT_LOCATION_ID
      THEN
         INS_APPOINTMENT_EVENT (:OLD.APPOINTMENT_ID,
                                'CURRENT_LOCATION_ID',
                                :OLD.CURRENT_LOCATION_ID,
                                :NEW.CURRENT_LOCATION_ID,
                                :NEW.LAST_UPDATED_SOURCE_TYPE,
                                :NEW.LAST_UPDATED_SOURCE);
      END IF;

      IF :NEW.APPT_REASON_CODE <> :OLD.APPT_REASON_CODE
      THEN
         INS_APPOINTMENT_EVENT (:OLD.APPOINTMENT_ID,
                                'APPT_REASON_CODE',
                                :OLD.APPT_REASON_CODE,
                                :NEW.APPT_REASON_CODE,
                                :NEW.LAST_UPDATED_SOURCE_TYPE,
                                :NEW.LAST_UPDATED_SOURCE);
      END IF;

      IF :NEW.COMMENTS <> :OLD.COMMENTS
      THEN
         INS_APPOINTMENT_EVENT (:OLD.APPOINTMENT_ID,
                                'COMMENTS',
                                :OLD.COMMENTS,
                                :NEW.COMMENTS,
                                :NEW.LAST_UPDATED_SOURCE_TYPE,
                                :NEW.LAST_UPDATED_SOURCE);
      END IF;

      IF :NEW.CARRIER_ID <> :OLD.CARRIER_ID
      THEN
         INS_APPOINTMENT_EVENT (:OLD.APPOINTMENT_ID,
                                'CARRIER_CODE',
                                :OLD.CARRIER_ID,
                                :NEW.CARRIER_ID,
                                :NEW.LAST_UPDATED_SOURCE_TYPE,
                                :NEW.LAST_UPDATED_SOURCE);
      END IF;

      IF :NEW.BUSINESS_PARTNER_ID <> :OLD.BUSINESS_PARTNER_ID
      THEN
         INS_APPOINTMENT_EVENT (:OLD.APPOINTMENT_ID,
                                'BUSINESS_PARTNER_ID',
                                :OLD.BUSINESS_PARTNER_ID,
                                :NEW.BUSINESS_PARTNER_ID,
                                :NEW.LAST_UPDATED_SOURCE_TYPE,
                                :NEW.LAST_UPDATED_SOURCE);
      END IF;

      IF :NEW.APPT_TYPE <> :OLD.APPT_TYPE
      THEN
         INS_APPOINTMENT_EVENT (:OLD.APPOINTMENT_ID,
                                'APPT_TYPE',
                                :OLD.APPT_TYPE,
                                :NEW.APPT_TYPE,
                                :NEW.LAST_UPDATED_SOURCE_TYPE,
                                :NEW.LAST_UPDATED_SOURCE);
      END IF;

      IF :NEW.APPT_STATUS <> :OLD.APPT_STATUS
      THEN
         INS_APPOINTMENT_EVENT (:OLD.APPOINTMENT_ID,
                                'APPT_STATUS',
                                :OLD.APPT_STATUS,
                                :NEW.APPT_STATUS,
                                :NEW.LAST_UPDATED_SOURCE_TYPE,
                                :NEW.LAST_UPDATED_SOURCE);
      END IF;

      IF :NEW.CHECKIN_DTTM <> :OLD.CHECKIN_DTTM
      THEN
         vOldValue := TO_CHAR (:OLD.CHECKIN_DTTM, 'yyyy-MM-dd HH24:MI:ss');
         vNewValue := TO_CHAR (:NEW.CHECKIN_DTTM, 'yyyy-MM-dd HH24:MI:ss');
         INS_APPOINTMENT_EVENT (:OLD.APPOINTMENT_ID,
                                'CHECKIN_DTTM',
                                vOldValue,
                                vNewValue,
                                :NEW.LAST_UPDATED_SOURCE_TYPE,
                                :NEW.LAST_UPDATED_SOURCE);

         IF vEQUIPMENT_INSTANCE_ID IS NOT NULL
         THEN
            INS_TRAILER_EVENT (vEQUIPMENT_INSTANCE_ID,
                               'CHECKIN_DTTM',
                               vOldValue,
                               vNewValue,
                               :NEW.LAST_UPDATED_SOURCE_TYPE,
                               :NEW.LAST_UPDATED_DTTM);
         END IF;
      END IF;

      IF :NEW.CHECKUOUT_DTTM <> :OLD.CHECKUOUT_DTTM
      THEN
         vOldValue := TO_CHAR (:OLD.CHECKUOUT_DTTM, 'yyyy-MM-dd HH24:MI:ss');
         vNewValue := TO_CHAR (:NEW.CHECKUOUT_DTTM, 'yyyy-MM-dd HH24:MI:ss');
         INS_APPOINTMENT_EVENT (:OLD.APPOINTMENT_ID,
                                'CHECKOUT_DTTM',
                                vOldValue,
                                vNewValue,
                                :NEW.LAST_UPDATED_SOURCE_TYPE,
                                :NEW.LAST_UPDATED_SOURCE);

         IF vEQUIPMENT_INSTANCE_ID IS NOT NULL
         THEN
            INS_TRAILER_EVENT (vEQUIPMENT_INSTANCE_ID,
                               'CHECKOUT_DTTM',
                               vOldValue,
                               vNewValue,
                               :NEW.LAST_UPDATED_SOURCE_TYPE,
                               :NEW.LAST_UPDATED_DTTM);
         END IF;
      END IF;

      IF :NEW.LOAD_UNLOAD_ST_DTTM <> :OLD.LOAD_UNLOAD_ST_DTTM
      THEN
         vOldValue :=
            TO_CHAR (:OLD.LOAD_UNLOAD_ST_DTTM, 'yyyy-MM-dd HH24:MI:ss');
         vNewValue :=
            TO_CHAR (:NEW.LOAD_UNLOAD_ST_DTTM, 'yyyy-MM-dd HH24:MI:ss');
         INS_APPOINTMENT_EVENT (:OLD.APPOINTMENT_ID,
                                'LOAD_UNLOAD_ST_DTTM',
                                vOldValue,
                                vNewValue,
                                :NEW.LAST_UPDATED_SOURCE_TYPE,
                                :NEW.LAST_UPDATED_SOURCE);

         IF vEQUIPMENT_INSTANCE_ID IS NOT NULL
         THEN
            INS_TRAILER_EVENT (vEQUIPMENT_INSTANCE_ID,
                               'LOAD_UNLOAD_ST_DTTM',
                               vOldValue,
                               vNewValue,
                               :NEW.LAST_UPDATED_SOURCE_TYPE,
                               :NEW.LAST_UPDATED_DTTM);
         END IF;
      END IF;

      IF :NEW.LOAD_UNLOAD_END_DTTM <> :OLD.LOAD_UNLOAD_END_DTTM
      THEN
         vOldValue :=
            TO_CHAR (:OLD.LOAD_UNLOAD_END_DTTM, 'yyyy-MM-dd HH24:MI:ss');
         vNewValue :=
            TO_CHAR (:NEW.LOAD_UNLOAD_END_DTTM, 'yyyy-MM-dd HH24:MI:ss');
         INS_APPOINTMENT_EVENT (:OLD.APPOINTMENT_ID,
                                'LOAD_UNLOAD_END_DTTM',
                                vOldValue,
                                vNewValue,
                                :NEW.LAST_UPDATED_SOURCE_TYPE,
                                :NEW.LAST_UPDATED_SOURCE);

         IF vEQUIPMENT_INSTANCE_ID IS NOT NULL
         THEN
            INS_TRAILER_EVENT (vEQUIPMENT_INSTANCE_ID,
                               'LOAD_UNLOAD_END_DTTM',
                               vOldValue,
                               vNewValue,
                               :NEW.LAST_UPDATED_SOURCE_TYPE,
                               :NEW.LAST_UPDATED_DTTM);
         END IF;
      END IF;

      IF :NEW.APPT_PRIORITY <> :OLD.APPT_PRIORITY
      THEN
         INS_APPOINTMENT_EVENT (:OLD.APPOINTMENT_ID,
                                'APPT_PRIORITY',
                                :OLD.APPT_PRIORITY,
                                :NEW.APPT_PRIORITY,
                                :NEW.LAST_UPDATED_SOURCE_TYPE,
                                :NEW.LAST_UPDATED_SOURCE);
      END IF;

      IF :NEW.REQUEST_COMM_TYPE <> :OLD.REQUEST_COMM_TYPE
      THEN
         INS_APPOINTMENT_EVENT (:OLD.APPOINTMENT_ID,
                                'REQUEST_COMM_TYPE',
                                :OLD.REQUEST_COMM_TYPE,
                                :NEW.REQUEST_COMM_TYPE,
                                :NEW.LAST_UPDATED_SOURCE_TYPE,
                                :NEW.LAST_UPDATED_SOURCE);
      END IF;

      IF :NEW.DRIVER_ID <> :OLD.DRIVER_ID
      THEN
         INS_APPOINTMENT_EVENT (:OLD.APPOINTMENT_ID,
                                'DRIVER_ID',
                                :OLD.DRIVER_ID,
                                :NEW.DRIVER_ID,
                                :NEW.LAST_UPDATED_SOURCE_TYPE,
                                :NEW.LAST_UPDATED_SOURCE);

         IF vEQUIPMENT_INSTANCE_ID IS NOT NULL
         THEN
            INS_TRAILER_EVENT (vEQUIPMENT_INSTANCE_ID,
                               'DRIVER_ID',
                               :OLD.DRIVER_ID,
                               :NEW.DRIVER_ID,
                               :NEW.LAST_UPDATED_SOURCE_TYPE,
                               :NEW.LAST_UPDATED_DTTM);
         END IF;
      END IF;

      IF :NEW.YARD_ID <> :OLD.YARD_ID
      THEN
         INS_APPOINTMENT_EVENT (:OLD.APPOINTMENT_ID,
                                'YARD_ID',
                                :OLD.YARD_ID,
                                :NEW.YARD_ID,
                                :NEW.LAST_UPDATED_SOURCE_TYPE,
                                :NEW.LAST_UPDATED_SOURCE);
      END IF;

      IF :NEW.CONTROL_NO <> :OLD.CONTROL_NO
      THEN
         INS_APPOINTMENT_EVENT (:OLD.APPOINTMENT_ID,
                                'CONTROL_NO',
                                :OLD.CONTROL_NO,
                                :NEW.CONTROL_NO,
                                :NEW.LAST_UPDATED_SOURCE_TYPE,
                                :NEW.LAST_UPDATED_SOURCE);
      END IF;

      IF :NEW.DOCK_DOOR_ID <> :OLD.DOCK_DOOR_ID
      THEN
         INS_APPOINTMENT_EVENT (:OLD.APPOINTMENT_ID,
                                'DOCK_DOOR_ID',
                                :OLD.DOCK_DOOR_ID,
                                :NEW.DOCK_DOOR_ID,
                                :NEW.LAST_UPDATED_SOURCE_TYPE,
                                :NEW.LAST_UPDATED_SOURCE);

         IF     :NEW.DOCK_DOOR_ID IS NOT NULL
            AND vEQUIPMENT_INSTANCE_ID IS NOT NULL
         THEN
            INS_DOCK_DOOR_EVENT (:NEW.DOCK_DOOR_ID,
                                 :NEW.DOOR_TYPE_ID,
                                 :NEW.FACILITY_ID,
                                 'TRAILER',
                                 vEQUIPMENT_INSTANCE_ID,
                                 NULL,
                                 :NEW.LAST_UPDATED_SOURCE_TYPE,
                                 :NEW.LAST_UPDATED_SOURCE,
                                 :NEW.LAST_UPDATED_DTTM);
         END IF;
      END IF;

      IF :NEW.DOOR_TYPE_ID <> :OLD.DOOR_TYPE_ID
      THEN
         INS_APPOINTMENT_EVENT (:OLD.APPOINTMENT_ID,
                                'DOOR_TYPE_ID',
                                :OLD.DOOR_TYPE_ID,
                                :NEW.DOOR_TYPE_ID,
                                :NEW.LAST_UPDATED_SOURCE_TYPE,
                                :NEW.LAST_UPDATED_SOURCE);
      END IF;

      IF :NEW.REQUESTOR_NAME <> :OLD.REQUESTOR_NAME
      THEN
         INS_APPOINTMENT_EVENT (:OLD.APPOINTMENT_ID,
                                'REQUESTOR_NAME',
                                :OLD.REQUESTOR_NAME,
                                :NEW.REQUESTOR_NAME,
                                :NEW.LAST_UPDATED_SOURCE_TYPE,
                                :NEW.LAST_UPDATED_SOURCE);
      END IF;

      IF :NEW.LOADING_TYPE <> :OLD.LOADING_TYPE
      THEN
         INS_APPOINTMENT_EVENT (:OLD.APPOINTMENT_ID,
                                'LOADING_TYPE',
                                :OLD.LOADING_TYPE,
                                :NEW.LOADING_TYPE,
                                :NEW.LAST_UPDATED_SOURCE_TYPE,
                                :NEW.LAST_UPDATED_SOURCE);
      END IF;

      IF :NEW.TP_COMPANY_ID <> :OLD.TP_COMPANY_ID
      THEN
         INS_APPOINTMENT_EVENT (:OLD.APPOINTMENT_ID,
                                'TP_COMPANY_ID',
                                :OLD.TP_COMPANY_ID,
                                :NEW.TP_COMPANY_ID,
                                :NEW.LAST_UPDATED_SOURCE_TYPE,
                                :NEW.LAST_UPDATED_SOURCE);
      END IF;
   END IF;

   IF :NEW.facility_id IS NOT NULL
   THEN
      BEGIN
         SELECT YARD_ID
           INTO :NEW.yard_id
           FROM FACILITY_YARD
          WHERE FACILITY_ID = :NEW.facility_id AND IS_PRIMARY = 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            :NEW.YARD_ID := NULL;
      END;
   END IF;
END ILM_APPT_TRG;
/

create or replace procedure manh_undo_create_620_XX_02_pix 
(
    p_whse                in  varchar2,
    p_cd_master_id        in  number,
    p_ship_wave_nbr       in  varchar2,
    p_login_user_id       in  varchar2,
    p_order_id            in  number,
    p_is_original         in  number,
    p_rc                  out number
)
as
    v_facility_id                facility.facility_id%type;
    v_proc_stat_code_01          varchar2(2);
    v_proc_stat_code_03          varchar2(2); 
    v_xml_group_attr_01          varchar2(10);
    v_xml_group_attr_03          varchar2(10);
    v_pix62001                   varchar2(4000);
    v_pix62003                   varchar2(4000);
    e_pix62001_witout_wave_nbr   varchar2(4000);
    e_pix62003_witout_wave_nbr   varchar2(4000);
    e_pix62001                   varchar2(4000);
    e_pix62003                   varchar2(4000);
    v_ins_ref_col_string_01      varchar2(1000);
    v_ins_ref_col_string_03      varchar2(1000);
    v_sel_ref_col_string_01      varchar2(1000);
    v_sel_ref_col_string_03      varchar2(1000);
    v_orig_order_id              number(9);
    v_prev_module_name           wm_utils.t_app_context_data;
    v_prev_action_name           wm_utils.t_app_context_data;	
begin
    p_rc := 0;
    select facility_id into v_facility_id
    from facility
    where whse = p_whse
    and mark_for_deletion = 0
    and rownum<2;
	
    wm_utils.get_context_info(v_prev_module_name, v_prev_action_name);
    wm_utils.set_context_info(p_module_name => 'WAVE', p_action_name => '620_XX_02_PIX',
        p_client_id => p_ship_wave_nbr);

    manh_get_pix_config_data  ('620','01','02',
        p_cd_master_id,v_proc_stat_code_01,v_xml_group_attr_01);
    
    manh_get_pix_config_data  ('620','03','02',
        p_cd_master_id,v_proc_stat_code_03,v_xml_group_attr_03);
    
    if (p_order_id != -1) 
    then 
        v_pix62001 
            :=  'insert into pix_tran '                               
            || '('
            || '   item_name, pix_tran_id, tran_type, tran_code, tran_nbr,'     
            || '   pix_seq_nbr, proc_stat_code, whse, season, season_yr, style,' 
            || '   style_sfx, color, color_sfx, sec_dim, qual, size_desc,'  
            || '   size_range_code,size_rel_posn_in_table,invn_type, prod_stat,' 
            || '   batch_nbr, sku_attr_1, sku_attr_2, sku_attr_3, sku_attr_4,'  
            || '   sku_attr_5, cntry_of_orgn, invn_adjmt_qty, invn_adjmt_type,'  
            || '   uom, actn_code, create_date_time, mod_date_time, user_id,'    
            || '   wm_version_id, item_id, facility_id, tc_company_id,'         
            || '   company_code, xml_group_id ';
       
        v_sel_ref_col_string_01 := '';
        v_sel_ref_col_string_03 := '';
        
        manh_get_pix_ref_code_col_list('620','01','02',p_cd_master_id,
            v_ins_ref_col_string_01,v_sel_ref_col_string_01);
        manh_get_pix_ref_code_col_list('620','03','02',p_cd_master_id,
            v_ins_ref_col_string_03,v_sel_ref_col_string_03);
            
        v_pix62003 := v_pix62001 || v_ins_ref_col_string_03;
        v_pix62001 := v_pix62001 || v_ins_ref_col_string_01;
                
        if (v_sel_ref_col_string_01 is null and p_cd_master_id is not null)
        then
            manh_get_pix_ref_code_col_list('620','01','02',null,
                v_ins_ref_col_string_01,v_sel_ref_col_string_01);
            v_pix62001 := v_pix62001 || v_ins_ref_col_string_01;
        end if;
            
        if (v_sel_ref_col_string_03 is null and p_cd_master_id is not null)
        then
            manh_get_pix_ref_code_col_list('620','03','02',null,
                v_ins_ref_col_string_03,v_sel_ref_col_string_03);
            v_pix62003 := v_pix62003 || v_ins_ref_col_string_03;
        end if;   
         
        e_pix62001_witout_wave_nbr := v_pix62001                                       
            ||' ) '
            ||'select item_cbo.item_name, pix_tran_id_seq.nextval,'                                                     
            ||'    ''620'',''01'', pix_tran_id_seq.nextval,'
            ||'    rownum, '
            ||     coalesce(v_proc_stat_code_01,'10') ||', '''||p_Whse || ''''    
            ||'    , item_cbo.item_season, item_cbo.item_season_year,' 
            ||'    item_cbo.item_style, item_cbo.item_style_sfx,'             
            ||'    item_cbo.item_color, item_cbo.item_color_sfx,'             
            ||'    item_cbo.item_second_dim, item_cbo.item_quality,'
            ||'    item_cbo.item_size_desc, item_wms.size_range_code,'
            ||'    item_wms.size_rel_posn_in_table, order_line_item.invn_type,'
            ||'    order_line_item.prod_stat,order_line_item.batch_nbr, '      
            ||'    order_line_item.item_attr_1, order_line_item.item_attr_2,'
            ||'    order_line_item.item_attr_3, order_line_item.item_attr_4,' 
            ||'    order_line_item.item_attr_5, order_line_item.cntry_of_orgn,'
            ||'    order_line_item.allocated_qty, ''S'', size_uom.size_uom,' 
            ||'    ''02'', current_timestamp, current_timestamp, '
            ||'    '''|| p_Login_User_Id ||''', 1, order_line_item.item_id,'
            ||     v_facility_id   ||', order_line_item.tc_company_id, '        
            ||'    company.company_code, '
            || ( case when v_xml_group_attr_01 is null then 'null' else ''''
            || v_xml_group_attr_01 ||''' ' end ) 
            || replace(v_sel_ref_col_string_01,'ORDER_LINE_ITEM.TC_ORDER_LINE_ID',
                   'prnt.tc_order_line_id') 
            ||     ' '   
            ||'from orders '                                          
            ||'join order_line_item on order_line_item.order_id = orders.order_id'
            ||'    and order_line_item.is_cancelled = ''0'''    
            ||'    and order_line_item.allocated_qty > 0 '
            ||'    and orders.is_original_order = 1 '                  
            ||'    and coalesce(orders.wm_order_status,0) != 12 '      
            ||'    and orders.o_facility_id = :facility_id '  
            ||'join order_line_item prnt on prnt.order_id=orders.order_id '
            ||'    and coalesce(order_line_item.substituted_parent_line_id,'
            ||'        order_line_item.line_item_id) = prnt.line_item_id '            
            ||'join item_cbo on item_cbo.item_id = order_line_item.item_id '             
            ||'join item_wms on item_wms.item_id = item_cbo.item_id '             
            ||'join size_uom on size_uom.size_uom_id = order_line_item.qty_uom_id_base '
            ||'join company on company.company_id = orders.tc_company_id '           
            ||'    and order_line_item.order_id = :orig_order_id '     
            ||'    and (order_line_item.fulfillment_type=''1'''        
            ||'        or order_line_item.fulfillment_type is null) ';     
            
        e_pix62001 := v_pix62001                                       
            ||' ) '
            ||'select item_cbo.item_name, pix_tran_id_seq.nextval,'                                                     
            ||'    ''620'',''01'', pix_tran_id_seq.nextval,'
            ||'    rownum, '
            ||     coalesce(v_proc_stat_code_01,'10') ||', '''||p_Whse ||''''    
            ||'    , item_cbo.item_season, item_cbo.item_season_year,' 
            ||'    item_cbo.item_style, item_cbo.item_style_sfx,'             
            ||'    item_cbo.item_color, item_cbo.item_color_sfx,'             
            ||'    item_cbo.item_second_dim, item_cbo.item_quality,'
            ||'    item_cbo.item_size_desc, item_wms.size_range_code,'
            ||'    item_wms.size_rel_posn_in_table, order_line_item.invn_type,'
            ||'    order_line_item.prod_stat,order_line_item.batch_nbr, '      
            ||'    order_line_item.item_attr_1, order_line_item.item_attr_2,'
            ||'    order_line_item.item_attr_3, order_line_item.item_attr_4,' 
            ||'    order_line_item.item_attr_5, order_line_item.cntry_of_orgn,'
            ||'    order_line_item.allocated_qty, ''S'', size_uom.size_uom,' 
            ||'    ''02'', current_timestamp, current_timestamp, '
            ||'    '''|| p_Login_User_Id ||''', 1, order_line_item.item_id,'
            ||     v_facility_id   ||', order_line_item.tc_company_id, '        
            ||'    company.company_code, '
            || ( case when v_xml_group_attr_01 is null then 'null' else ''''
            || v_xml_group_attr_01 ||''' ' end ) 
            || replace(v_sel_ref_col_string_01,'ORDER_LINE_ITEM.TC_ORDER_LINE_ID',
                   'prnt.tc_order_line_id') 
            ||     ' '            
            ||'from orders '                                          
            ||'join order_line_item on order_line_item.order_id = orders.order_id'
            ||'    and order_line_item.is_cancelled = ''0'''    
            ||'    and order_line_item.allocated_qty > 0 '
            ||'    and orders.is_original_order = 1 '                  
            ||'    and coalesce(orders.wm_order_status,0) != 12 '      
            ||'    and orders.o_facility_id = :facility_id '    
            ||'join order_line_item prnt on prnt.order_id=orders.order_id '
            ||'    and coalesce(order_line_item.substituted_parent_line_id,'
            ||'        order_line_item.line_item_id) = prnt.line_item_id '
            ||'join item_cbo on item_cbo.item_id = order_line_item.item_id '             
            ||'join item_wms on item_wms.item_id = item_cbo.item_id '             
            ||'join size_uom on size_uom.size_uom_id = order_line_item.qty_uom_id_base '
            ||'join company on company.company_id = orders.tc_company_id '           
            ||'    and order_line_item.order_id = :orig_order_id ' 
            ||'    and order_line_item.ship_wave_nbr = :ship_wave_nbr '
            ||'    and (order_line_item.fulfillment_type=''1'''        
            ||'        or order_line_item.fulfillment_type is null) ';  
            
        e_pix62003_witout_wave_nbr := v_pix62003                                       
            ||' ) '
            ||'select item_cbo.item_name, pix_tran_id_seq.nextval,'                                                     
            ||'    ''620'',''03'', pix_tran_id_seq.nextval,'
            ||'    rownum, '
            ||     coalesce(v_proc_stat_code_03,'10') ||', '''||p_Whse   || ''''  
            ||'    , item_cbo.item_season, item_cbo.item_season_year,' 
            ||'    item_cbo.item_style, item_cbo.item_style_sfx,'             
            ||'    item_cbo.item_color, item_cbo.item_color_sfx,'             
            ||'    item_cbo.item_second_dim, item_cbo.item_quality,'
            ||'    item_cbo.item_size_desc, item_wms.size_range_code,'
            ||'    item_wms.size_rel_posn_in_table, order_line_item.invn_type,'
            ||'    order_line_item.prod_stat,order_line_item.batch_nbr, '      
            ||'    order_line_item.item_attr_1, order_line_item.item_attr_2,'
            ||'    order_line_item.item_attr_3, order_line_item.item_attr_4,' 
            ||'    order_line_item.item_attr_5, order_line_item.cntry_of_orgn,'
            ||'    order_line_item.allocated_qty, ''S'', size_uom.size_uom,' 
            ||'    ''02'', current_timestamp, current_timestamp, '
            ||'    '''|| p_Login_User_Id ||''', 1, order_line_item.item_id,'
            ||     v_facility_id   ||', order_line_item.tc_company_id, '        
            ||'    company.company_code, '
            || ( case when v_xml_group_attr_03 is null then 'null' else ''''
            || v_xml_group_attr_03 ||''' ' end ) 
            || replace(v_sel_ref_col_string_03 ,'ORDER_LINE_ITEM.TC_ORDER_LINE_ID',
                   'prnt.tc_order_line_id') 
            ||     ' '             
            ||'from orders '                                          
            ||'join order_line_item on order_line_item.order_id = orders.order_id'
            ||'    and order_line_item.is_cancelled = ''0'''  
            ||'    and order_line_item.allocated_qty > 0 '
            ||'    and orders.is_original_order = 1 '                  
            ||'    and coalesce(orders.wm_order_status,0) != 12 '      
            ||'    and orders.o_facility_id = :facility_id '     
            ||'join order_line_item prnt on prnt.order_id=orders.order_id '
            ||'    and coalesce(order_line_item.substituted_parent_line_id,'
            ||'        order_line_item.line_item_id) = prnt.line_item_id '
            ||'join item_cbo on item_cbo.item_id = order_line_item.item_id '             
            ||'join item_wms on item_wms.item_id = item_cbo.item_id '             
            ||'join size_uom on size_uom.size_uom_id = order_line_item.qty_uom_id_base '
            ||'join company on company.company_id = orders.tc_company_id '           
            ||'    and order_line_item.order_id = :orig_order_id '     
            ||'    and order_line_item.fulfillment_type=''3''';     
            
        e_pix62003 := v_pix62003                                       
            ||' ) '
            ||'select item_cbo.item_name, pix_tran_id_seq.nextval,'                                                     
            ||'    ''620'',''03'', pix_tran_id_seq.nextval,'
            ||'    rownum, '
            ||     coalesce(v_proc_stat_code_03,'10') ||', '''||p_Whse || ''''    
            ||'    , item_cbo.item_season, item_cbo.item_season_year,' 
            ||'    item_cbo.item_style, item_cbo.item_style_sfx,'             
            ||'    item_cbo.item_color, item_cbo.item_color_sfx,'             
            ||'    item_cbo.item_second_dim, item_cbo.item_quality,'
            ||'    item_cbo.item_size_desc, item_wms.size_range_code,'
            ||'    item_wms.size_rel_posn_in_table, order_line_item.invn_type,'
            ||'    order_line_item.prod_stat,order_line_item.batch_nbr, '      
            ||'    order_line_item.item_attr_1, order_line_item.item_attr_2,'
            ||'    order_line_item.item_attr_3, order_line_item.item_attr_4,' 
            ||'    order_line_item.item_attr_5, order_line_item.cntry_of_orgn,'
            ||'    order_line_item.allocated_qty, ''S'', size_uom.size_uom,' 
            ||'    ''02'', current_timestamp, current_timestamp, '
            ||'    '''|| p_Login_User_Id ||''', 1, order_line_item.item_id,'
            ||     v_facility_id   ||', order_line_item.tc_company_id, '        
            ||'    company.company_code, '
            || ( case when v_xml_group_attr_03 is null then 'null' else ''''
            || v_xml_group_attr_03 ||''' ' end ) 
            || replace(v_sel_ref_col_string_03 ,'ORDER_LINE_ITEM.TC_ORDER_LINE_ID',
	           'prnt.tc_order_line_id') 
            ||     ' '    
            ||'from orders '                                          
            ||'join order_line_item on order_line_item.order_id = orders.order_id'
            ||'    and order_line_item.is_cancelled = ''0'''   
            ||'    and order_line_item.allocated_qty > 0 '
            ||'    and orders.is_original_order = 1 '                  
            ||'    and coalesce(orders.wm_order_status,0) != 12 '      
            ||'    and orders.o_facility_id = :facility_id '    
            ||'join order_line_item prnt on prnt.order_id=orders.order_id '
            ||'    and coalesce(order_line_item.substituted_parent_line_id,'
            ||'        order_line_item.line_item_id) = prnt.line_item_id '
            ||'join item_cbo on item_cbo.item_id = order_line_item.item_id '             
            ||'join item_wms on item_wms.item_id = item_cbo.item_id '             
            ||'join size_uom on size_uom.size_uom_id = order_line_item.qty_uom_id_base '
            ||'join company on company.company_id = orders.tc_company_id '           
            ||'    and order_line_item.order_id = :orig_order_id ' 
            ||'    and order_line_item.ship_wave_nbr = :ship_wave_nbr '
            ||'    and order_line_item.fulfillment_type=''3''' ;
        
        if (p_is_original = 1 )
        then
            v_orig_order_id := p_order_id;
            if (p_ship_wave_nbr is null)
            then
                if (v_proc_stat_code_01 != 91) then 
                execute immediate e_pix62001_witout_wave_nbr using v_facility_id, v_orig_order_id ;
                end if;    
                if (v_proc_stat_code_03 != 91) then     
                execute immediate e_pix62003_witout_wave_nbr using v_facility_id, v_orig_order_id ;
                end if;    
            else
                if (v_proc_stat_code_01 != 91) then 
                execute immediate e_pix62001 using v_facility_id, v_orig_order_id, p_ship_wave_nbr;
                end if;    
                if (v_proc_stat_code_03 != 91) then         
                execute immediate e_pix62003 using v_facility_id, v_orig_order_id, p_ship_wave_nbr;
                end if;                    
            end if;
        else 
            for order_cur in 
            (
                select order_id 
                from orders 
                where parent_order_id = p_order_id
            )
            loop
                if (p_ship_wave_nbr is null)
                then
                    if (v_proc_stat_code_01 != 91) then 
                        execute immediate e_pix62001_witout_wave_nbr using v_facility_id, 
                            order_cur.order_id ;
                    end if;
                    if (v_proc_stat_code_03 != 91) then
                        execute immediate e_pix62003_witout_wave_nbr using v_facility_id, 
                            order_cur.order_id ;
                    end if;    
                else
                    if (v_proc_stat_code_01 != 91) then 
                        execute immediate e_pix62001 using v_facility_id, order_cur.order_id, 
                            p_ship_wave_nbr;
                    end if; 
                    if (v_proc_stat_code_03 != 91) then
                        execute immediate e_pix62003 using v_facility_id, order_cur.order_id, 
                            p_ship_wave_nbr;
                    end if;    
                end if;
            end loop;
        end if;
    end if;
	
    wm_utils.set_context_info(p_module_name => v_prev_module_name,
        p_action_name => v_prev_action_name);
exception
    when no_data_found then
        null;
end;
/
show errors;

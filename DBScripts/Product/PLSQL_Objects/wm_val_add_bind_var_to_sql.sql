create or replace procedure wm_val_add_bind_var_to_sql
(
    p_vjd_id        in val_job_dtl.val_job_dtl_id%type,
    p_bind_var      in val_sql_binds.bind_var%type,
    p_bind_val      in val_sql_binds.bind_val%type default null,
    p_bind_val_int  in val_sql_binds.bind_val_int%type default null
)
as
begin
    -- persist any preset bind values for the sqls to be used within the above
    -- job details
    -- UI should validate bind var entry in the context of each sql
    -- bind vars known only at runtime need to be filled in by the calling code
    -- or a wrapper at some point prior to validation
    insert into val_sql_binds
    (
        val_job_dtl_id, bind_var, bind_val, bind_val_int
    )
    values
    (
        p_vjd_id, lower(p_bind_var), p_bind_val, p_bind_val_int
    );
end;
/
show errors;

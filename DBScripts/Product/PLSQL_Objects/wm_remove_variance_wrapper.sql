create or replace procedure wm_remove_variance_wrapper
(
    p_user_id               in user_profile.user_id%type,
    p_use_locking           in number,
    p_commit_freq           in number,
    p_caller_id             in varchar2,
    p_shipment_id           in shipment.shipment_id%type default null,
    p_manifest_id           in shipment.manifest_id%type default null,
    p_act_trk_tran_nbr      in prod_trkg_tran.tran_nbr%type default null,
    p_act_trk_rec_count     in number default null
)
as
    v_num_stores_to_process number(5) := 0;
    type t_d_facility_id is table of orders.d_facility_id%type index by binary_integer;
    va_active_stores        t_d_facility_id;
    v_is_cs_or_cm           number(1) := (case when p_shipment_id is not null 
        or p_manifest_id is not null then 1 else 0 end);
    v_act_trk_tran_nbr      prod_trkg_tran.tran_nbr%type;
    v_act_trk_rec_count     number(9) := p_act_trk_rec_count;
    v_trk_prod_flag         varchar2(1);
    v_is_cm                 number(1) := case when p_manifest_id is not null
        then 1 else 0 end;
begin
    if (v_is_cs_or_cm = 1)
    then
        -- activity tracking is at the origin facility level
        select wp.trk_prod_flag
        into v_trk_prod_flag
        from whse_parameters wp
        join facility f on f.facility_id = wp.whse_master_id
        where f.facility_id in
        (
            select mh.o_facility_id
            from manifest_hdr mh
            where mh.manifest_id = p_manifest_id
            union all
            select s.o_facility_number
            from shipment s
            where s.shipment_id = p_shipment_id
        );
        
        if (v_trk_prod_flag = 'Y')
        then
            v_act_trk_tran_nbr := prod_trkg_tran_id_seq.nextval;
        end if;
    end if;

    if (p_use_locking = 0)
    then
        wm_remove_variance_intrnl(p_user_id, v_is_cm, v_is_cs_or_cm, p_caller_id, p_act_trk_tran_nbr,
            v_act_trk_rec_count);
    else
        -- capture stores for these splits
        insert into tmp_store_master
        (
            d_facility_id
        )
        select distinct t.d_facility_id
        from tmp_order_splits_master t;
        v_num_stores_to_process := sql%rowcount;
        wm_cs_log('Stores found on shpmt ' || v_num_stores_to_process);

        if (v_is_cs_or_cm = 1)
        then
            -- here, we are able to track per-store progress; remove any that we
            -- may have already processed
            delete from tmp_store_master t
            where exists
            (
                select 1
                from stop st
                where st.facility_id = t.d_facility_id and st.is_wave_man_changed = 1
                    and
                    (
                        st.shipment_id = p_shipment_id 
                        or exists
                        (
                            select 1
                            from shipment s
                            where s.shipment_id = st.shipment_id
                                and s.manifest_id = p_manifest_id
                        )
                    )
            );
            v_num_stores_to_process := v_num_stores_to_process - sql%rowcount;
            wm_cs_log('Will process variance for ' || v_num_stores_to_process || ' stores');
        end if;

        loop
            if (v_num_stores_to_process = 1 or p_commit_freq = 1)
            then
                -- block on this one
                select f.facility_id
                bulk collect into va_active_stores
                from facility f
                where exists
                    (
                        select 1
                        from tmp_store_master t
                        where t.d_facility_id = f.facility_id
                    )
                    and rownum <= p_commit_freq
                for update;
            else
                -- otherwise, cherry-pick stores to process
                select f.facility_id
                bulk collect into va_active_stores
                from facility f
                where exists
                    (
                        select 1
                        from tmp_store_master t
                        where t.d_facility_id = f.facility_id
                    )
                    and rownum <= p_commit_freq
                for update skip locked;
            end if;

            if (va_active_stores.count > 0)
            then
                wm_cs_log('Preparing to process stores on shpmt ' || va_active_stores.count, p_sql_log_level => 2);
                forall i in 1..va_active_stores.count
                insert into tmp_order_splits_scratch
                (
                    order_id, order_split_id, shipment_id
                )
                select t.order_id, t.order_split_id, t.shipment_id
                from tmp_order_splits_master t 
                where t.d_facility_id = va_active_stores(i);
                wm_cs_log('Num splits in locked store collection ' || sql%rowcount, p_sql_log_level => 2);

                wm_remove_variance_intrnl(p_user_id, v_is_cm, v_is_cs_or_cm, p_caller_id, 
                    p_act_trk_tran_nbr, v_act_trk_rec_count);

                if (v_is_cs_or_cm = 1)
                then
                    forall i in 1..va_active_stores.count
                    update stop st
                    set st.is_wave_man_changed = 1
                    where st.facility_id = va_active_stores(i)
                        and 
                        (
                            st.shipment_id = p_shipment_id
                            or exists
                            (
                                select 1
                                from shipment s
                                where s.shipment_id = st.shipment_id
                                    and s.manifest_id = p_manifest_id
                            )
                        );
                    wm_cs_log('Variance processing completed for stops ' || sql%rowcount);
                end if;

                -- we're done with these stores on the shpmt
                v_num_stores_to_process := v_num_stores_to_process - va_active_stores.count;
                forall i in 1..va_active_stores.count
                delete from tmp_store_master t
                where t.d_facility_id = va_active_stores(i);
                va_active_stores.delete;
                commit;
            end if;

            if (v_num_stores_to_process <= 0)
            then
                exit;
            end if;
        end loop;
    end if;
end;
/

show errors;

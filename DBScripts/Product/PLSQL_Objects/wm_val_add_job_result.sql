create or replace procedure wm_val_add_job_result
(
    p_val_job_dtl_id  in val_job_dtl.val_job_dtl_id%type,
    p_msg             in val_result_hist.msg%type,
    p_last_run_passed in val_result_hist.last_run_passed%type
)
as
    v_user_id         user_profile.user_id%type;
    v_parent_txn_id   val_result_hist.txn_id%type;
    v_txn_id          val_result_hist.txn_id%type;
    v_app_hook        val_result_hist.app_hook%type;
    v_val_pgm_id      val_result_hist.pgm_id%type;
begin
    select t.user_id, t.parent_txn_id, t.txn_id, t.app_hook, t.val_pgm_id
    into v_user_id, v_parent_txn_id, v_txn_id, v_app_hook, v_val_pgm_id
    from tmp_val_parm t;
    
    wm_val_auton_log_result(v_user_id, v_parent_txn_id, v_txn_id, p_val_job_dtl_id, p_msg, 
        p_last_run_passed, v_app_hook, v_val_pgm_id);
    wm_val_log('Validation ' || p_val_job_dtl_id 
        || case p_last_run_passed when 1 then ' passed' else ' failed' end 
        || ', msg: <' || p_msg || '>');
end;
/
show errors;

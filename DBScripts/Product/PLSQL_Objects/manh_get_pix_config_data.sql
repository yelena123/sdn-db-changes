create or replace procedure manh_get_pix_config_data 
(
    p_tran_type             in varchar2,
    p_tran_code             in varchar2,
    p_actn_code             in varchar2,
    p_cd_master_id          in number,
    p_pix_create_stat_code  out number,
    p_xml_grp_attr          out varchar
)
as
begin
    p_pix_create_stat_code := null;
    p_xml_grp_attr := null;
    
    if (p_cd_master_id is not null)
    then
    begin
        select coalesce(to_char(ptc.pix_create_stat_code),'10'), pix_xml_grouping_attr
        into p_pix_create_stat_code, p_xml_grp_attr
        from pix_tran_code ptc
        where ptc.tran_type = p_tran_type
            and ptc.tran_code = p_tran_code
            and coalesce(ptc.actn_code,'*') = coalesce(p_actn_code,'*')
            and ptc.cd_master_id = p_cd_master_id
            and rownum < 2;
    exception
    when no_data_found then   
        select coalesce(to_char(ptc.pix_create_stat_code),'10'), pix_xml_grouping_attr
        into p_pix_create_stat_code, p_xml_grp_attr
        from pix_tran_code ptc
        where ptc.tran_type = p_tran_type
            and ptc.tran_code = p_tran_code
            and coalesce(ptc.actn_code,'*') = coalesce(p_actn_code,'*')
            and ptc.cd_master_id is null
            and rownum < 2;
    end;                          
    else
        select coalesce(to_char(ptc.pix_create_stat_code),'10'), pix_xml_grouping_attr
        into p_pix_create_stat_code, p_xml_grp_attr
        from pix_tran_code ptc
        where ptc.tran_type = p_tran_type
            and ptc.tran_code = p_tran_code
            and coalesce(ptc.actn_code,'*') = coalesce(p_actn_code,'*')
            and ptc.cd_master_id is null
            and rownum < 2;
    end if;

exception
    when no_data_found then
        p_pix_create_stat_code := '10';
end;
/


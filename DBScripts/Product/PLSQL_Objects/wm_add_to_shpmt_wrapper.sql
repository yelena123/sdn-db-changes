create or replace procedure wm_add_to_shpmt_wrapper
(
    p_user_id             in user_profile.user_id%type,
    p_tc_company_id       in shipment.tc_company_id%type,
    p_use_locking         in number,
    p_commit_freq         in number,
    p_caller_id           in varchar2,
    p_trk_prod_flag       in varchar2,
    p_shipment_id         in shipment.shipment_id%type,
    p_om_shipment_id      in shipment.shipment_id%type,
    p_static_route_id     in static_route.static_route_id%type,
    p_lpn_id              in lpn.lpn_id%type,
    p_order_id            in orders.order_id%type,
    p_order_split_id      in order_split.order_split_id%type,
    p_cons_run_id         in cons_run.cons_run_id%type,
    p_load_split_data     in number,
    p_mode                in varchar2
)
as
    type t_d_facility_id is table of orders.d_facility_id%type index by binary_integer;
    va_active_stores        t_d_facility_id;
    v_num_stores_to_process number(5) := 0;
    v_act_trk_tran_nbr      prod_trkg_tran.tran_nbr%type
        := case when p_trk_prod_flag = 'Y' then prod_trkg_tran_id_seq.nextval else null end;
    v_act_trk_rec_count     number(9) := 0;
    v_is_bad_shpmt          number(1) := 0;
begin
    select case when exists
    (
        select 1
        from shipment s
        where s.shipment_id = p_shipment_id and s.shipment_closed_indicator = 1
        union all
        select 1
        from shipment s
        join tmp_order_splits_master t on t.shipment_id = s.shipment_id
        where s.shipment_closed_indicator = 1
        union all
        select 1
        from shipment s
        join tmp_order_splits_scratch t on t.shipment_id = s.shipment_id
        where s.shipment_closed_indicator = 1
    ) then 1 else 0 end
    into v_is_bad_shpmt
    from dual;
    
    if (v_is_bad_shpmt = 1)
    then
        wm_cs_log('Shipment already closed!', p_sql_log_level => 0);
        raise_application_error(-20050, 'Shipment ' || p_shipment_id
            || ' is already closed.');
    end if;
    
    if (p_load_split_data = 1)
    then
        wm_load_splits_for_input_crit(p_use_locking, p_order_id, p_order_split_id,
            p_cons_run_id, p_lpn_id => p_lpn_id, 
            p_assign_shipment_id => p_shipment_id,
            p_om_shipment_id => p_om_shipment_id,
            p_static_route_id => p_static_route_id, p_mode => p_mode);
    select decode(wp.trk_prod_flag, 'Y', prod_trkg_tran_id_seq.nextval, null)
        into v_act_trk_tran_nbr
        from whse_parameters wp
        join facility f on f.facility_id = wp.whse_master_id
        where f.facility_id =
        (
            select s.o_facility_number
            from shipment s
            where s.shipment_id = p_shipment_id
        );
    else
        select decode(wp.trk_prod_flag, 'Y', prod_trkg_tran_id_seq.nextval, null)
        into v_act_trk_tran_nbr
        from whse_parameters wp
        join facility f on f.facility_id = wp.whse_master_id
        where f.facility_id =
        (
            select s.o_facility_number
            from shipment s
            join tmp_order_splits_scratch t on t.shipment_id = s.shipment_id
            where rownum < 2
            union all
            select s.o_facility_number
            from shipment s
            join tmp_order_splits_master t on t.shipment_id = s.shipment_id
            where rownum < 2
        );            
    end if;

    if (p_use_locking = 0)
    then
        wm_add_to_shpmt_intrnl(p_user_id, p_tc_company_id, p_static_route_id, 
            p_lpn_id, p_order_id, p_order_split_id, p_cons_run_id, 
            p_load_split_data, p_caller_id, v_act_trk_tran_nbr, 
            v_act_trk_rec_count);
    else
        insert into tmp_store_master
        (
            d_facility_id
        )
        select distinct t.d_facility_id
        from tmp_order_splits_master t;
        v_num_stores_to_process := sql%rowcount;
        wm_cs_log('Stores found ' || sql%rowcount);

        loop
            if (v_num_stores_to_process = 1 or p_commit_freq = 1)
            then
                -- block on this one store
                select f.facility_id
                bulk collect into va_active_stores
                from facility f
                where exists
                    (
                        select 1
                        from tmp_store_master t
                        where t.d_facility_id = f.facility_id
                    )
                    and rownum <= p_commit_freq
                for update;
            else
                -- otherwise, cherry-pick stores to process
                select f.facility_id
                bulk collect into va_active_stores
                from facility f
                where exists
                    (
                        select 1
                        from tmp_store_master t
                        where t.d_facility_id = f.facility_id
                    )
                    and rownum <= p_commit_freq
                for update skip locked;
                wm_cs_log('Processing stores ' || sql%rowcount, p_sql_log_level => 2);
            end if;

            if (va_active_stores.count > 0)
            then
                forall i in 1..va_active_stores.count
                insert into tmp_order_splits_scratch
                (
                    order_id, order_split_id, shipment_id, om_shipment_id, 
                    static_route_id
                )
                select t.order_id, t.order_split_id, t.shipment_id, 
                    t.om_shipment_id, t.static_route_id
                from tmp_order_splits_master t 
                where t.d_facility_id = va_active_stores(i);
                wm_cs_log('Processing splits on stores ' || sql%rowcount, p_sql_log_level => 2);

                wm_add_to_shpmt_intrnl(p_user_id, p_tc_company_id, 
                    p_static_route_id, p_lpn_id, p_order_id, p_order_split_id, 
                    p_cons_run_id, p_load_split_data, p_caller_id,
                    v_act_trk_tran_nbr, v_act_trk_rec_count);

                -- we're done with these stores on the shpmt
                v_num_stores_to_process := v_num_stores_to_process - va_active_stores.count;
                forall i in 1..va_active_stores.count
                delete from tmp_store_master t
                where t.d_facility_id = va_active_stores(i);
                va_active_stores.delete;
                commit;
            end if;

            if (v_num_stores_to_process <= 0)
            then
                exit;
            end if;
        end loop;
    end if;
end;
/

show errors;

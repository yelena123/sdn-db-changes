CREATE OR REPLACE PROCEDURE sp_update_pickline2_tmp
AS
   v_if_exists          NUMBER (10, 0);
   SWV_PLG_Mbr          NUMBER (10, 0);
   SWV_PLG_ID           NUMBER (10, 0);
   SWV_whse_code        CHAR (3);
   SWV_name             VARCHAR2 (65);
   SWV_item_attribute   VARCHAR2 (65);
   SWV_importance       NUMBER (10, 0);
   SWV_tree_id          NUMBER (10, 0);
   SWV_tree_node_name   VARCHAR2 (32);
   SWV_tree_node_path   VARCHAR2 (100);
   SWV_parent_node_id   NUMBER (10, 0);
   SWV_proportion       NUMBER (5, 2);
   SWV_plb_score        NUMBER (9, 2);
   SWV_calculated       NUMBER (9, 2);

   CURSOR SWV_RCur
   IS
      SELECT "PLG_MBR",
             "PLG_ID",
             "WHSE_CODE",
             "NAME",
             "ITEM_ATTRIBUTE",
             "IMPORTANCE",
             "TREE_ID",
             "TREE_NODE_NAME",
             "TREE_NODE_PATH",
             "PARENT_NODE_ID",
             "PROPORTION",
             "PLB_SCORE",
             "CALCULATED"
        FROM tt_TBLPLGTEST2;
BEGIN
   SELECT COUNT (*)
     INTO v_if_exists
     FROM user_tables
    WHERE table_name = 'PICK_LINE2_SP_AS_TMP';

   IF v_if_exists > 0
   THEN
      EXECUTE IMMEDIATE ' TRUNCATE TABLE pick_line2_sp_as_tmp ';
   END IF;


   pick_line2 ();

   OPEN SWV_RCur;

   FETCH SWV_RCur
   INTO SWV_PLG_Mbr,
        SWV_PLG_ID,
        SWV_whse_code,
        SWV_name,
        SWV_item_attribute,
        SWV_importance,
        SWV_tree_id,
        SWV_tree_node_name,
        SWV_tree_node_path,
        SWV_parent_node_id,
        SWV_proportion,
        SWV_plb_score,
        SWV_calculated;

   WHILE SWV_RCur%FOUND
   LOOP
      INSERT INTO pick_line2_sp_as_tmp
           VALUES (SWV_PLG_Mbr,
                   SWV_PLG_ID,
                   SWV_whse_code,
                   SWV_name,
                   SWV_item_attribute,
                   SWV_importance,
                   SWV_tree_id,
                   SWV_tree_node_name,
                   SWV_tree_node_path,
                   SWV_parent_node_id,
                   SWV_proportion,
                   SWV_plb_score,
                   SWV_calculated);

      FETCH SWV_RCur
      INTO SWV_PLG_Mbr,
           SWV_PLG_ID,
           SWV_whse_code,
           SWV_name,
           SWV_item_attribute,
           SWV_importance,
           SWV_tree_id,
           SWV_tree_node_name,
           SWV_tree_node_path,
           SWV_parent_node_id,
           SWV_proportion,
           SWV_plb_score,
           SWV_calculated;
   END LOOP;

   CLOSE SWV_RCur;
END;
/
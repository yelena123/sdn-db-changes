create or replace function wm_val_get_user_id
return val_result_hist.user_id%type
as
    v_user_id val_result_hist.user_id%type;
begin
    select t.user_id
    into v_user_id
    from tmp_val_parm t;
    
    return v_user_id;
end;
/
show errors;

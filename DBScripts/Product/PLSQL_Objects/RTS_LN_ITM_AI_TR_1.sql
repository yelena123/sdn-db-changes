create or replace TRIGGER RTS_LN_ITM_AI_TR_1
   AFTER INSERT
   ON RTS_LINE_ITEM    REFERENCING NEW AS NEW
   FOR EACH ROW
DECLARE
   l_rts_id           NUMBER;
   l_line_item_id     NUMBER;
   l_field_name       VARCHAR2 (25);
   l_lu_source_type   NUMBER (2);
   l_lu_source        RTS_LINE_ITEM.CREATED_SOURCE%TYPE;
   l_old_value        VARCHAR2 (500);
   l_new_value        VARCHAR2 (500);
   veventseq          NUMBER;
BEGIN
   l_rts_id := :NEW.rts_id;
   l_line_item_id := :NEW.line_item_id;
   l_field_name := 'INSERTED Line Item';
   l_old_value := NULL;

   SELECT tc_po_line_id
     INTO l_new_value
     FROM purchase_orders_line_item
    WHERE purchase_orders_line_item_id = l_line_item_id;

   l_lu_source_type := :NEW.created_source_type;
   l_lu_source := :NEW.created_source;

   /*SELECT SEQ_RTS_EVENT_ID.NEXTVAL
     INTO veventseq
     FROM rts_event
    WHERE rts_id = l_rts_id;*/

   INSERT INTO rts_event (rts_id,
                          event_seq,
                          field_name,
                          old_value,
                          new_value,
                          created_source_type,
                          created_source,
                          created_dttm,
                          line_item_id)
        VALUES (l_rts_id,
                SEQ_RTS_EVENT_ID.NEXTVAL,
                l_field_name,
                l_old_value,
                l_new_value,
                l_lu_source_type,
                l_lu_source,
                SYSDATE,
                l_line_item_id);
END;
/
create or replace procedure sp_run_warehouse_import
(
   p_whse        in locn_hdr.whse%type,
   p_is_weekly   in number,
   p_user_name   in varchar2,
   p_log_lvl     in number default 0)
is
begin
    so_whse_import.import_slot_data_from_wm (p_whse => p_whse, p_is_weekly => p_is_weekly, 
        p_user_name => p_user_name, p_is_standalone => 0, p_log_lvl => p_log_lvl);
    --commit;
end;
/

create or replace procedure wm_dealloc_lpn
(
    p_tc_lpn_id        in lpn.tc_lpn_id%type,
    p_facility_id      in facility.facility_id%type,
    p_tc_company_id    in wave_parm.tc_company_id%type,
    p_user_id          in user_profile.user_id%type
)
as
    v_whse                  facility.whse%type;
    type t_d_facility_id is table of orders.d_facility_id%type index by binary_integer;
    va_active_stores    t_d_facility_id;
    v_num_stores_to_process number(5) := 0;
    v_commit_freq           number(5);
    v_max_log_lvl           number(1) := 0;
    v_use_locking           number(1) := 0;
    v_ref_value_1           msg_log.ref_value_1%type := to_char(p_tc_lpn_id); 
    v_error_msg             msg_log.msg%type;
    v_prev_module_name      wm_utils.t_app_context_data;
    v_prev_action_name      wm_utils.t_app_context_data;
begin
--todo! productivity_tracking
    select to_number(coalesce(trim(substr(sc.misc_flags, 3, 5)), '99999')),
        to_number(coalesce(trim(substr(sc.misc_flags, 8, 1)), '0')),
        case when substr(sc.misc_flags, 2, 1) = 'Y' then 1 else 0 end
    into v_commit_freq, v_max_log_lvl, v_use_locking
    from sys_code sc 
    where sc.rec_type = 'S' and sc.code_type = '520' and sc.code_id = 'DAL';
    v_commit_freq := case v_commit_freq when 0 then 99999 else v_commit_freq end;

    delete from tmp_lpn_alloc_dtl;
    delete from tmp_log_parm;
    insert into tmp_log_parm
    (
        log_lvl, pgm_id, module, msg_id, ref_code_1, ref_value_1, user_id        
    )
    values
    (
        v_max_log_lvl, 'wm_dealloc_lpn', 'WAVE', '1094', 'DAL',
        v_ref_value_1, p_user_id
    );  

    select f.whse
    into v_whse
    from facility f
    where f.facility_id = p_facility_id;
	
    wm_utils.get_context_info(v_prev_module_name, v_prev_action_name);
    wm_utils.set_context_info(p_module_name => 'LPN DEALLOCATION', p_action_name => 'DEALLOCATE LPN',
        p_client_id => p_tc_lpn_id);
    
    --assumption! All validations done by application
    wm_cs_log('Start deallocation for LPN: ' || p_tc_lpn_id);

    -- assumption! allocations from the lpn done only via alloc_invn_dtl and task_dtl
    -- all the records from alloc_invn_dtl and task_dtl are for agg lines
    -- collect all the allocations
    insert into tmp_lpn_alloc_dtl
    (
        order_id, line_item_id, cd_master_id, cntr_nbr, item_id, invn_type,
        prod_stat, batch_nbr, sku_attr_1, sku_attr_2, sku_attr_3, sku_attr_4, 
        sku_attr_5, cntry_of_orgn, invn_need_type, alloc_uom, qty_alloc, 
        qty_pulld, stat_code, pkt_ctrl_nbr, task_id, d_facility_id 
    )
    select o.order_id, iv.line_item_id, iv.cd_master_id, iv.cntr_nbr, iv.item_id, 
        iv.invn_type, iv.prod_stat, iv.batch_nbr, iv.sku_attr_1, iv.sku_attr_2, 
        iv.sku_attr_3, iv.sku_attr_4, iv.sku_attr_5, iv.cntry_of_orgn, 
        iv.invn_need_type, iv.alloc_uom, iv.qty_alloc, iv.qty_pulld, 
        iv.stat_code, iv.pkt_ctrl_nbr, iv.task_id, o.d_facility_id 
    from
    (
        select aid.tc_order_id, aid.line_item_id, aid.cd_master_id, aid.cntr_nbr, 
            aid.item_id, aid.invn_type, aid.prod_stat, aid.batch_nbr, aid.sku_attr_1, 
            aid.sku_attr_2, aid.sku_attr_3, aid.sku_attr_4, aid.sku_attr_5, 
            aid.cntry_of_orgn, aid.invn_need_type, aid.alloc_uom, aid.qty_alloc, 
            aid.qty_pulld, aid.stat_code, aid.pkt_ctrl_nbr, null task_id
        from alloc_invn_dtl aid
        where aid.cntr_nbr = p_tc_lpn_id and aid.stat_code = 0 and aid.invn_need_type = 60
            and aid.whse = v_whse 
        union all
        select td.tc_order_id, td.line_item_id, td.cd_master_id, td.cntr_nbr, 
                td.item_id, td.invn_type, td.prod_stat, td.batch_nbr, td.sku_attr_1, 
                td.sku_attr_2, td.sku_attr_3, td.sku_attr_4, td.sku_attr_5, 
                td.cntry_of_orgn, td.invn_need_type, td.alloc_uom, td.qty_alloc, 
                td.qty_pulld, td.stat_code, td.pkt_ctrl_nbr, td.task_id
        from task_dtl td
        join task_hdr th on th.task_id = td.task_id and th.task_hdr_id = td.task_hdr_id and th.whse = v_whse
        where td.cntr_nbr = p_tc_lpn_id and td.stat_code = 0 and td.invn_need_type = 60
    ) iv
    join orders o on o.tc_order_id = iv.tc_order_id and o.tc_company_id = iv.cd_master_id
        and o.o_facility_id = p_facility_id;
    wm_cs_log('Allocations/tasks collected ' || sql%rowcount );

    if (v_use_locking = 1)
    then
        insert into tmp_store_master
        (
            d_facility_id
        )
        select distinct f.facility_id
        from tmp_lpn_alloc_dtl t
        left join facility f on f.facility_id = t.d_facility_id;
        v_num_stores_to_process := sql%rowcount;
        wm_cs_log('Stores to process ' || v_num_stores_to_process);

        loop
            if (v_num_stores_to_process = 1 or v_commit_freq = 1)
            then
                -- block on this one
                select f.facility_id
                bulk collect into va_active_stores
                from facility f
                where exists
                (
                    select 1
                    from tmp_store_master t
                    where t.d_facility_id = f.facility_id
                )
                for update;
                
                if (sql%rowcount = 0)
                then
                    raise_application_error(-20050, 'No rows found for a facility lock acquisition!');
                end if;
            else
                -- otherwise, cherry-pick stores to process
                select f.facility_id
                bulk collect into va_active_stores
                from facility f
                where exists
                    (
                        select 1
                        from tmp_store_master t
                        where t.d_facility_id = f.facility_id
                    )
                    and rownum <= v_commit_freq
                for update skip locked;
            end if;

         -- todo: backoff mechanism
            if (va_active_stores.count > 0)
            then
                wm_cs_log('Preparing to process stores ' || va_active_stores.count, p_sql_log_level => 2);
                forall i in 1..va_active_stores.count
                insert into tmp_dealloc_oli
                (
                    order_id, line_item_id, qty_to_dealloc 
                )
                select t.order_id, t.line_item_id, sum(t.qty_alloc - t.qty_pulld) qty_to_dealloc
                from tmp_lpn_alloc_dtl t
                where t.d_facility_id = va_active_stores(i)
                group by t.order_id, t.line_item_id;
                wm_cs_log('Num order lines in locked store collection ' || sql%rowcount, p_sql_log_level => 2);

                wm_adj_lpn_alloc(p_user_id, p_tc_company_id, p_facility_id, 
                    v_whse);

                -- we're done with these stores in the wave
                v_num_stores_to_process := v_num_stores_to_process - va_active_stores.count;
                forall i in 1..va_active_stores.count
                delete from tmp_store_master t
                where t.d_facility_id = va_active_stores(i);
                va_active_stores.delete;
                commit;
            end if;

            if (v_num_stores_to_process <= 0)
            then
                exit;
            end if;
        end loop;
    else
        insert into tmp_dealloc_oli
        (
            order_id, line_item_id, qty_to_dealloc 
        )
        select t.order_id, t.line_item_id, sum(t.qty_alloc - t.qty_pulld) qty_to_dealloc
        from tmp_lpn_alloc_dtl t
        group by t.order_id, t.line_item_id;

        wm_adj_lpn_alloc(p_user_id, p_tc_company_id, p_facility_id, 
            v_whse);
        commit;
    end if;
    
    -- dealloc lpn
    merge into lpn l
    using
    (
        select wi.tc_lpn_id, sum(wi.wm_allocated_qty) alloc_qty, 
            sum(wi.on_hand_qty) on_hand_qty
        from wm_inventory wi
        where wi.tc_lpn_id = p_tc_lpn_id and wi.tc_company_id = p_tc_company_id
            and wi.inbound_outbound_indicator = 'I' and wi.c_facility_id = p_facility_id
        group by wi.tc_lpn_id
    ) iv
    on (iv.tc_lpn_id = l.tc_lpn_id and l.tc_company_id = p_tc_company_id and l.c_facility_id = p_facility_id
        and l.inbound_outbound_indicator = 'I')
    when matched then
    update set l.last_updated_source = p_user_id, l.last_updated_dttm = sysdate,
        l.lpn_facility_status  = (case when (iv.alloc_qty > 0 and iv.alloc_qty < iv.on_hand_qty ) then 45
            when (iv.alloc_qty = 0 and l.curr_sub_locn_id is not null) then 30
            when (iv.alloc_qty = 0 and l.curr_sub_locn_id is null) then 10
            else l.lpn_facility_status end),
        l.dest_sub_locn_id = (case when l.lpn_facility_status  in (10, 30) then null else l.dest_sub_locn_id end),
        l.disposition_type = (case when l.lpn_facility_status  in (10, 30) then null else l.disposition_type end) ;
    wm_cs_log('Lpn status changes done ' || sql%rowcount);
    
    wm_cs_log('Deallocation complete for LPN: ' || p_tc_lpn_id);
    commit;
	
    wm_utils.set_context_info(p_module_name => v_prev_module_name,
        p_action_name => v_prev_action_name);
end;
/
show errors ;
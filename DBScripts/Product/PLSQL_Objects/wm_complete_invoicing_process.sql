create or replace procedure wm_complete_invoicing_process
(
    p_invc_batch_nbr    in number,
    p_user_id           in user_profile.user_id%type,
    p_cd_master_id      in company.company_id%type,
    p_facility_id       in facility.facility_id%type,
    p_whse              in facility.whse%type
)
as
    v_tran_nbr        pix_tran.tran_nbr%type := pix_tran_id_seq.nextval;
    v_proc_stat_code  pix_tran.proc_stat_code%type := 0;
    v_xml_group_id    pix_tran.xml_group_id%type;
    v_ref_codes_insert_list varchar2(1000);
    v_ref_codes_select_list varchar2(1000);
    v_insert_sql            varchar2(4000);
    v_is_601_pix_configured number(1) := 0;
begin
    -- gen item level summary pix
    manh_get_pix_config_data('500', '01', null, p_cd_master_id, v_proc_stat_code,
        v_xml_group_id);

    if (v_proc_stat_code != 91)
    then
        insert into pix_tran
        (
          item_name, pix_tran_id, tran_type, tran_code,sys_user_id, tran_nbr, pix_seq_nbr,
          proc_stat_code, whse, season, season_yr, style, style_sfx, color,
          color_sfx, sec_dim, qual, size_desc, size_range_code, size_rel_posn_in_table,
          invn_type, prod_stat, batch_nbr, sku_attr_1, sku_attr_2, sku_attr_3,
          sku_attr_4, sku_attr_5, cntry_of_orgn, invn_adjmt_qty, invn_adjmt_type,
          create_date_time, mod_date_time, user_id, item_id, facility_id,company_code,
          tc_company_id, xml_group_id, wt_adjmt_qty
        )
        select ic.item_name, pix_tran_id_seq.nextval, '500', '01', p_user_id, v_tran_nbr,
           rownum, v_proc_stat_code, p_whse, ic.item_season, ic.item_season_year,
           ic.item_style, ic.item_style_sfx, ic.item_color, ic.item_color_sfx,
           ic.item_second_dim, ic.item_quality, ic.item_size_desc, iw.size_range_code,
           iw.size_rel_posn_in_table, iv.invn_type, iv.product_status, iv.batch_nbr,
           iv.item_attr_1, iv.item_attr_2, iv.item_attr_3, iv.item_attr_4,
           iv.item_attr_5, iv.cntry_of_orgn, iv.item_qty, 'S', sysdate, sysdate,
           p_user_id, iv.item_id, p_facility_id,c.company_code, ic.company_id, v_xml_group_id, 0
        from item_cbo ic
        join item_wms iw on iw.item_id = ic.item_id
        join company c on c.company_id = ic.company_id 
        join
        (
            select ooli.item_id, ooli.item_attr_1, ooli.item_attr_2, ooli.item_attr_3,
                ooli.item_attr_4, ooli.item_attr_5, ooli.batch_nbr, ooli.cntry_of_orgn,
                ooli.invn_type, ooli.product_status, sum(ooli.shipped_qty) item_qty
            from outpt_order_line_item ooli
            where ooli.invc_batch_nbr = p_invc_batch_nbr and ooli.shipped_qty > 0
            group by ooli.item_id, ooli.item_attr_1, ooli.item_attr_2, ooli.item_attr_3,
                ooli.item_attr_4, ooli.item_attr_5, ooli.batch_nbr, ooli.cntry_of_orgn,
                ooli.invn_type, ooli.product_status
        ) iv on iv.item_id = ic.item_id
        log errors (to_char(sysdate));
        wm_cs_log('500 01 pix insert ' || sql%rowcount);
    end if;

    -- gen line level summary pix for shpd qty
    manh_get_pix_config_data('620', '14', '01', p_cd_master_id, v_proc_stat_code, v_xml_group_id);

    if (v_proc_stat_code != 91)
    then
        manh_get_pix_ref_code_col_list('620', '14', '01', p_cd_master_id, v_ref_codes_insert_list, 
            v_ref_codes_select_list);
        
                v_insert_sql :=
            ' insert into pix_tran'
            || ' ('
            || '   item_name, pix_tran_id, tran_type, tran_code, actn_code, tran_nbr, pix_seq_nbr,'
            || '   proc_stat_code, whse, season, season_yr, style, style_sfx, color,'
            || '   color_sfx, sec_dim, qual, size_desc, size_range_code, size_rel_posn_in_table,'
            || '   invn_type, prod_stat, batch_nbr, sku_attr_1, sku_attr_2, sku_attr_3,'
            || '   sku_attr_4, sku_attr_5, cntry_of_orgn, invn_adjmt_qty, invn_adjmt_type,'
            || '   create_date_time, mod_date_time, user_id, item_id, facility_id,company_code,'
            || '   tc_company_id, xml_group_id, date_proc, sys_user_id, uom '
            || v_ref_codes_insert_list
            || ' )'
            || ' select ic.item_name, pix_tran_id_seq.nextval, ''620'', ''14'', ''01'', :tran_nbr,'
            || '    rownum, :proc_stat_code, :whse, ic.item_season, ic.item_season_year,'
            || '    ic.item_style, ic.item_style_sfx, ic.item_color, ic.item_color_sfx,'
            || '    ic.item_second_dim, ic.item_quality, ic.item_size_desc, iw.size_range_code,'
            || '    iw.size_rel_posn_in_table, iv.invn_type, iv.product_status, iv.batch_nbr,'
            || '    iv.item_attr_1, iv.item_attr_2, iv.item_attr_3, iv.item_attr_4,'
            || '    iv.item_attr_5, iv.cntry_of_orgn, iv.item_qty, ''S'', sysdate, sysdate,'
            || '    :user_id, ic.item_id, :facility_id, c.company_code, ic.company_id, :xml_group_id,'
            || '    sysdate, :user_id, iv.uom ' || v_ref_codes_select_list
            || ' from item_cbo ic'
            || ' join item_wms iw on iw.item_id = ic.item_id'
            || ' join company c on c.company_id = ic.company_id '
            || ' join'
            || ' ('
            || '     select ooli.item_id, ooli.item_attr_1, ooli.item_attr_2, ooli.item_attr_3,'
            || '         ooli.item_attr_4, ooli.item_attr_5, ooli.batch_nbr, ooli.cntry_of_orgn,'
            || '         ooli.invn_type, ooli.product_status, ooli.tc_order_id,'
            || '         ooli.tc_order_line_id, sum(ooli.shipped_qty) item_qty, ooli.uom'
            || '     from outpt_order_line_item ooli'
            || '     where ooli.invc_batch_nbr = :invc_batch_nbr and ooli.shipped_qty > 0'
            || '     group by ooli.tc_order_id, ooli.tc_order_line_id, ooli.item_id,'
            || '         ooli.item_attr_1, ooli.item_attr_2, ooli.item_attr_3,'
            || '         ooli.item_attr_4, ooli.item_attr_5, ooli.batch_nbr,'
            || '         ooli.cntry_of_orgn, ooli.invn_type, ooli.product_status, ooli.uom'
            || ' ) iv on iv.item_id = ic.item_id'
            || ' join orders on orders.tc_order_id = iv.tc_order_id and orders.tc_company_id = c.company_id'
            || ' join order_line_item on order_line_item.order_id = orders.order_id '
            || '    and order_line_item.tc_order_line_id = iv.tc_order_line_id '
            || ' log errors (to_char(sysdate))';

        execute immediate v_insert_sql using v_tran_nbr, v_proc_stat_code, p_whse, p_user_id, 
            p_facility_id, v_xml_group_id, p_user_id, p_invc_batch_nbr;
    end if;

     -- the do status informational pix can also be turned off in S501
    select case when exists
    (
        select 1
        from sys_code sc
        where sc.rec_type = 'S' and sc.code_type = '501'
            and sc.code_id = '190' and substr(sc.misc_flags, 1, 1) = '1'
    ) then 1 else 0 end
    into v_is_601_pix_configured
    from dual;
    
    manh_get_pix_config_data('601', '001', null, p_cd_master_id, v_proc_stat_code,
        v_xml_group_id);
        
    -- gen do status change pix
    if (v_proc_stat_code != 91 and v_is_601_pix_configured = 1)
    then
        manh_get_pix_ref_code_col_list('601', '001', null, p_cd_master_id,
            v_ref_codes_insert_list, v_ref_codes_select_list);

        v_insert_sql :=
               'insert into pix_tran'
            || '('
            || '    tran_type, tran_code, sys_user_id, user_id, create_date_time, mod_date_time,'
            || '    pix_seq_nbr, tran_nbr, proc_stat_code, facility_id, company_code, whse,'
            || '    tc_company_id, pix_tran_id, xml_group_id'
            ||      v_ref_codes_insert_list
            || ') '
            || 'select ''601'', ''001'', :user_id, :user_id, sysdate, sysdate, rownum,'
            ||     v_tran_nbr ||', :proc_stat_code, :p_facility_id, c.company_code,'
            || '    :p_whse, orders.tc_company_id, pix_tran_id_seq.nextval, '''
            ||      v_xml_group_id || '''' || v_ref_codes_select_list || ' '
            || 'from orders '
            || 'join company c on c.company_id = orders.tc_company_id '
            || 'join tmp_wave_old_order_status t on t.order_id = orders.order_id '
            || 'where orders.do_status = 190 and t.do_status != orders.do_status '
            || '    and exists'
            || '    ('
            || '        select 1'
            || '        from outpt_orders oo'
            || '        where oo.tc_order_id = orders.tc_order_id '
            || '            and oo.batch_ctrl_nbr = :invc_batch_nbr '
            || '    )'
            || 'log errors (to_char(sysdate))';

        execute immediate v_insert_sql using p_user_id, p_user_id, v_proc_stat_code,
            p_facility_id, p_whse, p_invc_batch_nbr;
        wm_cs_log('order status change pix - 601 001 generated ' || sql%rowcount);
    end if;
   
end;
/
show errors;

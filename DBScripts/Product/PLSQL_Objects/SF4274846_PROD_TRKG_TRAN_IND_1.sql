DECLARE
index_exists NUMBER;
BEGIN
index_exists := 0;

SELECT count(*) INTO index_exists FROM dual
WHERE EXISTS (SELECT * FROM user_indexes WHERE index_name = 'PROD_TRKG_TRAN_IND_1');

IF index_exists = 0 THEN
	
  EXECUTE IMMEDIATE 'create index PROD_TRKG_TRAN_IND_1 on PROD_TRKG_TRAN(CREATE_DATE_TIME DESC, ITEM_ID, CD_MASTER_ID) TABLESPACE MLMINDX
	PCTFREE    5
	INITRANS   20
	MAXTRANS   255
	STORAGE    (
				INITIAL          1M
				MINEXTENTS       1
				MAXEXTENTS       2147483645
				PCTINCREASE      0
				BUFFER_POOL      DEFAULT
			   )
	compute statistics';
END IF;
END;
/
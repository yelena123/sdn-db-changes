create or replace PROCEDURE sp_get_range_ids(v_rg_id IN NUMBER,
	v_range_ids_table IN VARCHAR2) as
   v_rg_level  NUMBER(10,0);
   v_line  CHAR(20);
   v_sql_statement_string  VARCHAR2(255);
   v_sql_statement_string1  VARCHAR2(255);
   SWV_rg_id NUMBER(10,0);
 v_no_of_items1  NUMBER(10,0);
   v_if_exists NUMBER(10,0);
BEGIN
   SWV_rg_id := v_rg_id;

   INSERT INTO TT_STACK  VALUES(SWV_rg_id, 1);

   v_rg_level := 1;

   WHILE v_rg_level > 0 LOOP
      SELECT   COUNT(*) INTO v_if_exists FROM TT_STACK WHERE range_level = v_rg_level;
      IF v_if_exists > 0 then

         begin
            select   range_id INTO SWV_rg_id FROM TT_STACK WHERE range_level = v_rg_level and rownum=1;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         end;
         DELETE FROM TT_STACK
         WHERE range_level = v_rg_level
         AND range_id = SWV_rg_id;
         INSERT INTO TT_STACK
         SELECT node_id, v_rg_level+1
         FROM tree_dtl
         WHERE parent_node_id = SWV_rg_id;
         IF SQL%ROWCOUNT > 0 then
            v_rg_level := v_rg_level+1;
         end if;
         IF SQL%ROWCOUNT = 0 then


            v_sql_statement_string := 'INSERT INTO ' ||
            v_range_ids_table ||
            ' VALUES( '  ||
            SUBSTR(CAST(SWV_rg_id AS CHAR),1,20) ||
            ' )';
            EXECUTE IMMEDIATE v_sql_statement_string;
         end if;
      ELSE
         v_rg_level := v_rg_level -1;
      end if;
   END LOOP; -- WHILE


   EXECUTE IMMEDIATE ' TRUNCATE TABLE TT_STACK ';
END;
/
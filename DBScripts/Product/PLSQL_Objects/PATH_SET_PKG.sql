
create or replace
PACKAGE BODY PATH_SET_PKG
AS
   -- ***********************
   -- Change Tracker
   -- Date:  By: Desc:
   -- ***********************
   -- 01/13/2004 MB  modified: fnFormatLocationList CZONES cursor definition
   -- 01/13/2004 MB  modified: fnGetZoneListCsv ZONES cursor definition
   --      02/19/2004      AP  modified:       get_path_sets_from_filter definiton (sql_statement)
   -- 03/30/2004 MB modified: PATH_SET_RETRIEVAL_REQUEST (loop insert replaced by insert select) (Deepak)
   -- 04/30/2004 MB modified: PATH_SET_RETRIEVAL_REQUEST (MIN and NVL added around select to prevent TOO_MANY_ROWS exception) (Deepak)
   -- 01/20/2005 MB modified: GET_PATH_SETS_FROM_FILTER TT-38618 (IS_MANUAL_CREATION = 0 added to vSQLFromWhere)
   -- 2-Mar-2005 DS added: fnFormatZoneList and PATH_SET_RETRIEVAL_REQUEST_NEW; 46065; Ramaswamy Meka
   -- 19-Sept-2005 NH added: is_tandem flag in all list queries from path_set.
   --                             In get_path_set_from_filter, if isTandem = 0 then make sure we get only is_tandem=0 records from path set(naresh)
   -- 08-Dec-2005 IN IN_IS_TANDEM INTEGER added to PATH_SET_RETRIEVAL_REQUEST CR # 12894
   -- 17th Dec 2008 : MACR00203786 Fix ,Modified PATH_SET_RETRIEVAL_REQUEST_NEW as Zones cannot be formed using facility_id on path_set records ; Dileep Kumar G
   -- 9th April 2009 : gakumar MACR00262049 fix
   -- 11th November 2011 : Prashant G Sudhapalli , Data Loader Path Set Requirement
   -- ***********************

   PROCEDURE get_path_sets_from_filter (
      coid                     IN     PATH_SET.tc_company_id%TYPE,
      ofacid                   IN     VARCHAR2,
      ocity                    IN     PATH_SET.o_city%TYPE,
      ocnty                    IN     PATH_SET.o_county%TYPE,
      ost                      IN     PATH_SET.o_state_prov%TYPE,
      ozip                     IN     PATH_SET.o_postal_code%TYPE,
      ocountry                 IN     PATH_SET.o_country_code%TYPE,
      ozone                    IN     PATH_SET.o_zone_id%TYPE,
      dfacid                   IN     VARCHAR2,
      dcity                    IN     PATH_SET.d_city%TYPE,
      dcnty                    IN     PATH_SET.d_county%TYPE,
      dst                      IN     PATH_SET.d_state_prov%TYPE,
      dzip                     IN     PATH_SET.d_postal_code%TYPE,
      dcountry                 IN     PATH_SET.d_country_code%TYPE,
      dzone                    IN     PATH_SET.d_zone_id%TYPE,
      pathsetname              IN     PATH_SET.path_set_name%TYPE, -- Gopalakrishnan Modified for CM changes on 3/21/2008 Start
      customername             IN     VARCHAR2,
      incotermid               IN     PATH_SET.incoterm_id%TYPE,
      billingmethod            IN     PATH_SET.billing_method%TYPE,
      routeto                  IN     PATH_SET.route_to%TYPE,
      routetype1               IN     PATH_SET.route_type_1%TYPE,
      routetype2               IN     PATH_SET.route_type_2%TYPE, -- Gopalakrishnan Modified for CM changes on 3/21/2008 End
      idt                      IN     DATE,
      search_type              IN     VARCHAR2 DEFAULT NULL,
      sort_field               IN     VARCHAR2,
      sort_direction           IN     VARCHAR2,
      start_row                IN     NUMBER,
      end_row                  IN     NUMBER,
      istandem                 IN     NUMBER,
      row_count                   OUT NUMBER,
      path_set_filter_refcur      OUT path_set_filter_curtype,
      coids                    IN     VARCHAR2,
	  isValid					IN		NUMBER)
   IS
      path_set_cursor            path_set_filter_curtype;

      vTableName        		 VARCHAR2 (50) := 'PATH_SET';
      vKeyDateStr                VARCHAR2 (50);
      origin_where_clause        VARCHAR2 (2000);
      destination_where_clause   VARCHAR2 (2000);
      date_where_clause          VARCHAR2 (400) := NULL;
      sql_statement              VARCHAR2 (4000);
      order_by_clause            VARCHAR2 (200);
      vSQLFromWhere              VARCHAR2 (4000);

      vBaseTable         		VARCHAR2 (10) := 'PATH_SET';
      vIdColumn          		VARCHAR2 (12) := 'PATH_SET_ID';
      vSqlErrm                   VARCHAR2 (255);
   BEGIN
      --*********************************************************
      -- Path Set filtering clauses
      --*********************************************************

    IF(isValid = 1) THEN

      vTableName := 'PATH_SET';

    ELSE
      vTableName := 'IMPORT_PATH_SET';
    END IF;
      -- Add the where clauses when filtering data is provided
      IF (idt IS NOT NULL)
      THEN
         vKeyDateStr :=
               'TO_DATE('''
            || TO_CHAR (idt, 'MM/DD/YYYY')
            || ''', ''MM/DD/YYYY'')';
         date_where_clause :=
               ' AND effective_dt <= '
            || vKeyDateStr
            || ' AND expiration_dt >= '
            || vKeyDateStr;
      END IF;

      origin_where_clause :=
         fnFormatFilterLocWhereClause (search_type,
                                       'o',
                                       coid,
                                       ofacid,
                                       ocity,
                                       ocnty,
                                       ost,
                                       ozip,
                                       ocountry,
                                       UPPER (ozone),
                                       coids);
      destination_where_clause :=
         fnFormatFilterLocWhereClause (search_type,
                                       'd',
                                       coid,
                                       dfacid,
                                       dcity,
                                       dcnty,
                                       dst,
                                       dzip,
                                       dcountry,
                                       UPPER (dzone),
                                       coids);

      --*********************************************************
      -- Prepare the SQL Statement with the Lane level
      -- filtering clauses
      --*********************************************************
      vSQLFromWhere :=
         ' FROM ' || vTableName || ' WHERE tc_company_id = ' || coid --|| ' WHERE tc_company_id in ( ' || coids || ' ) '
         || ' AND IS_VALID=' || isValid || ' AND IS_MANUAL_CREATION = 0 ';

      IF (isValid = 1) THEN
      vSQLFromWhere := vSQLFromWhere || ' AND IS_VALID=1';
      END IF;

      IF (istandem = 0)
      THEN
         vSQLFromWhere := vSQLFromWhere || ' AND IS_TANDEM = 0 ';
      END IF;

      -- Gopalakrishnan Modified for CM changes on 3/21/2008 Start
      IF (pathsetname IS NOT NULL)
      THEN
         vSQLFromWhere :=
               COALESCE (vSQLFromWhere, '')
            || ' AND PATH_SET_NAME = '''
            || pathsetname
            || '''';
      END IF;

      IF (customername IS NOT NULL)
      THEN
         vSQLFromWhere :=
            COALESCE (vSQLFromWhere, '')
            || ' AND CUSTOMER_ID = (SELECT CUSTOMER_ID FROM CUSTOMER WHERE CUSTOMER_NAME = '''
            || customername
            || ''' AND TC_COMPANY_ID = ''' || coid || ''')';
      END IF;

      IF (incotermid IS NOT NULL)
      THEN
         vSQLFromWhere :=
               COALESCE (vSQLFromWhere, '')
            || ' AND INCOTERM_ID = '
            || incotermid;
      END IF;

      IF (billingmethod IS NOT NULL)
      THEN
         vSQLFromWhere :=
               COALESCE (vSQLFromWhere, '')
            || ' AND BILLING_METHOD = '
            || billingmethod;
      END IF;

      IF (routeto IS NOT NULL)
      THEN
         vSQLFromWhere :=
               COALESCE (vSQLFromWhere, '')
            || ' AND ROUTE_TO = '''
            || routeto
            || '''';
      END IF;

      IF (routetype1 IS NOT NULL)
      THEN
         vSQLFromWhere :=
               COALESCE (vSQLFromWhere, '')
            || ' AND ROUTE_TYPE_1 = '''
            || routetype1
            || '''';
      END IF;

      IF (routetype2 IS NOT NULL)
      THEN
         vSQLFromWhere :=
               COALESCE (vSQLFromWhere, '')
            || ' AND ROUTE_TYPE_2 = '''
            || routetype2
            || '''';
      END IF;

      -- Gopalakrishnan Modified for CM changes on 3/21/2008 Start

      IF (origin_where_clause IS NOT NULL)
      THEN
         vSQLFromWhere := vSQLFromWhere || ' AND ' || origin_where_clause;
      END IF;

      IF (destination_where_clause IS NOT NULL)
      THEN
         vSQLFromWhere := vSQLFromWhere || ' AND ' || destination_where_clause;
      END IF;

      vSQLFromWhere := vSQLFromWhere || date_where_clause;

      -- Sorting of result set
      IF (sort_field = 'path_set_id')
      THEN
         order_by_clause :=
            ' ORDER BY ' || sort_field || ' ' || sort_direction;
      ELSE
         order_by_clause :=
               ' ORDER BY '
            || sort_field
            || ' '
            || sort_direction
            || ', path_set_hierarchy '
            || sort_direction;
      END IF;

      sql_statement :=
            '/* path_set_package.get_path_sets_from_filter page detail */ '
         || 'SELECT tc_company_id , '
         || vIdColumn
         || ' path_set_id'
         || ', path_set_hierarchy,'
         || ' o_facility_id, o_facility_alias_id, o_city, o_state_prov, o_county'
         || ' ,o_postal_code, o_country_code, o_zone_id, '
         || ' d_facility_id, d_facility_alias_id, d_city, d_state_prov, d_county'
         || ' ,d_postal_code, d_country_code, d_zone_id, effective_dt, expiration_dt '
         || -- Gopalakrishnan Modified for CM changes on 3/21/2008
            ' , path_set_name, customer_id, incoterm_id, billing_method, route_to, route_type_1, route_type_2 '
         || ' FROM '
         || vTableName
         || ' WHERE (tc_company_id, path_set_id) IN '
         || '(SELECT coid,id FROM ('
         || ' SELECT ROWNUM rn, tc_company_id coid, path_set_id id FROM ('
         || ' SELECT tc_company_id, path_set_id '
         || vSQLFromWhere
         || order_by_clause
         || ') WHERE ROWNUM <= '
         || end_row
         || ') WHERE RN >= '
         || start_row
         || ')'
         || ' AND IS_MANUAL_CREATION = 0  ';

      IF (istandem = 0)
      THEN
         sql_statement := sql_statement || ' AND IS_TANDEM = 0 ';
      END IF;

      sql_statement := sql_statement || order_by_clause;

      BEGIN
         EXECUTE IMMEDIATE
            '/* path_set_package.get_path_sets_from_filter count */ SELECT COUNT(*) '
            || vSQLFromWhere
            INTO row_count;

         OPEN path_set_cursor FOR sql_statement;

         path_set_filter_refcur := path_set_cursor;
      EXCEPTION
         WHEN OTHERS
         THEN
            vSqlErrm := SQLERRM;
            --   INSERT INTO DEBUG_SQL VALUES ('path_set_package.get_path_sets_from_filter COUNT',SYSDATE,'SELECT COUNT(*) ' || vSQLFromWhere,vSqlErrm);
            --   INSERT INTO DEBUG_SQL VALUES ('path_set_package.get_path_sets_from_filter detail',SYSDATE,sql_statement, vSqlErrm);
            COMMIT;

            RAISE;
      END;
   END get_path_sets_from_filter;

   FUNCTION fnFormatFilterLocWhereClause (
      aSearchType   IN VARCHAR2,
      loc_prefix    IN VARCHAR2,
      coid          IN PATH_SET.tc_company_id%TYPE,
      facid         IN VARCHAR2,
      city          IN PATH_SET.o_city%TYPE,
      cnty          IN PATH_SET.o_county%TYPE,
      st            IN PATH_SET.o_state_prov%TYPE,
      zip           IN PATH_SET.o_postal_code%TYPE,
      COUNTRY       IN PATH_SET.o_country_code%TYPE,
      ZONE          IN PATH_SET.o_zone_id%TYPE,
      coids         IN VARCHAR2)
      RETURN VARCHAR2
   IS
      formatted_where_clause         VARCHAR2 (2000);
      vCountyClause                  VARCHAR2 (200) := NULL;
      vSearchString                  VARCHAR2 (2000) := NULL;
      bAllPathSets                   BOOLEAN := FALSE;
      --CR 20487
      firstCondition                 BOOLEAN := TRUE;
      locTypeSetToSearchDetermined   BOOLEAN := FALSE;
      locTypeClause                  VARCHAR2 (200) := NULL;
   --possible combinations of input for filtering paths (the valid combination for storing a origin or dest of a path)
   --1.only facid(loc type facility)
   --2.only zone id(loc type zone)
   --3.postalcode country(loc type postal code level)

   --4.  country                   (loc type country)
   --4.1.    country state         (loc type state)
   --4.1.2   country state city     (loc type city)
   --4.1.2.3 country state city county (loc type **city** there is no loc type called county)

   --If all path sets is the option for filtering path sets
   --If county is given then search for paths sets in 4.1.2.3 to 4  ( locType in (county,city,state,country))
   --else if city is there then search for paths sets in 4.1.2 to 4 (loc type in( city ,state ,county))
   --else if state is there then search for paths sets in 4.1 to 4  (loc type in (state ,county))
   --else if postal code is present, check path sets in for 4 and 3  (loctype in ( country ,postalcode))
   --else if country is there get all path sets from level 4 alone(loc type = country)

   --if the fac or zone input is given, it doesnt matter if it is all pathsets or not.,it filters based on search string
   BEGIN
      IF ( (aSearchType IS NOT NULL AND aSearchType = 'All Path Sets')
          AND (ZONE IS NULL))
      THEN
         bAllPathSets := TRUE;
      END IF;

      IF (facid IS NOT NULL)
      THEN
         --vSearchString := Lane_Location_Pkg.fnGetFacilityCSVFromAliasCSV(coid,facid);
         vSearchString :=
            Lane_Location_Pkg.fnGetFacilityCSVFromAlias3PL (coids, facid);
         GOTO CreateWhereClause;
      END IF;

      IF (city IS NOT NULL)
      THEN
         --<CR 20487>
         IF (bAllPathSets)
         THEN
            locTypeClause :=
                  '  '
               || loc_prefix
               || '_loc_type in ( ''CS'' ,  ''ST'' ,  ''CO'' , ''ZN'' ) ';
            locTypeSetToSearchDetermined := TRUE;
            vSearchString :=
                  '  ( UPPER( '
               || loc_prefix
               || '_city ) = UPPER('
               || city
               || ') OR '
               || loc_prefix
               || '_city  IS NULL )';
            firstCondition := FALSE;

            IF (cnty IS NOT NULL)
            THEN
               vSearchString :=
                     vSearchString
                  || ' AND ( UPPER( '
                  || loc_prefix
                  || '_county ) = '
                  || cnty
                  || ' OR '
                  || loc_prefix
                  || '_county IS NULL )';
            ELSE
               vSearchString :=
                     vSearchString
                  || ' AND (  '
                  || loc_prefix
                  || '_county IS NULL )';
            END IF;
         ELSE
            --</CR 20487>
            vSearchString :=
               RTRIM (
                  vSearchString || ','
                  || Lane_Location_Pkg.fnGetLocationCSVFromCSV (
                        'CS',
                        city,
                        st || '~' || COUNTRY),
                  ',');

            IF (cnty IS NOT NULL)
            THEN
               vCountyClause :=
                     ' AND UPPER('
                  || loc_prefix
                  || '_county) IN ('
                  || cnty
                  || ')';
            --fnFormatContextInClause(REPLACE(cnty,'''',''), loc_prefix || 'CNTY')

            -- OR rl.' || loc_prefix || '_county IS NULL )';
            END IF;

            GOTO CreateWhereClause;
         END IF;
      END IF;

      IF (st IS NOT NULL)
      THEN
         --<CR 20487>
         IF (bAllPathSets)
         THEN
            IF (firstCondition)
            THEN
               vSearchString :=
                     '  ( UPPER('
                  || loc_prefix
                  || '_state_prov ) = '''
                  || st
                  || ''' OR '
                  || loc_prefix
                  || '_state_prov IS NULL OR ('
                  || loc_prefix
                  || '_zone_id IN ( select zone_id from zone_attribute where attribute_type = ''ST'' AND UPPER( attribute_value ) = '''
                  || st
                  || ''' AND country_code = '''
                  || COUNTRY
                  || ''' AND tc_company_id in ( '
                  || coids
                  || ' ) ) ) ) ';
               firstCondition := FALSE;
            ELSE
               vSearchString :=
                     vSearchString
                  || ' AND ( UPPER( '
                  || loc_prefix
                  || '_state_prov ) = '''
                  || st
                  || ''' OR '
                  || loc_prefix
                  || '_state_prov IS NULL OR ('
                  || loc_prefix
                  || '_zone_id IN ( select zone_id from zone_attribute where attribute_type = ''ST'' AND UPPER( attribute_value ) = '''
                  || st
                  || ''' AND country_code = '''
                  || COUNTRY
                  || ''' AND tc_company_id in ( '
                  || coids
                  || ' ) ) ) ) ';
            END IF;

            IF NOT locTypeSetToSearchDetermined
            THEN
               locTypeClause :=
                     '  '
                  || loc_prefix
                  || '_loc_type in ( ''ST'', ''CO'' , ''ZN'') ';
               locTypeSetToSearchDetermined := TRUE;
            END IF;
         ELSE
            --</CR 20487>
            vSearchString :=
               RTRIM (
                  vSearchString || ','
                  || Lane_Location_Pkg.fnGetLocationCSVFromCSV ('ST',
                                                                st,
                                                                COUNTRY),
                  ',');
            GOTO CreateWhereClause;
         END IF;
      END IF;

      IF (zip IS NOT NULL)
      THEN
         vSearchString :=
            REPLACE (
               RTRIM (
                  vSearchString || ','
                  || Lane_Location_Pkg.fnGetLocationCSVFromCSV (
                        'P2',
                        fnCreateCsvSubstr (zip, 2, bAllPathSets),
                        COUNTRY)
                  || ','
                  || Lane_Location_Pkg.fnGetLocationCSVFromCSV (
                        'P3',
                        fnCreateCsvSubstr (zip, 3, bAllPathSets),
                        COUNTRY)
                  || ','
                  || Lane_Location_Pkg.fnGetLocationCSVFromCSV (
                        'P4',
                        fnCreateCsvSubstr (zip, 4, bAllPathSets),
                        COUNTRY)
                  || ','
                  || Lane_Location_Pkg.fnGetLocationCSVFromCSV (
                        'P5',
                        fnCreateCsvSubstr (zip, 5, bAllPathSets),
                        COUNTRY)
                  || ','
                  || Lane_Location_Pkg.fnGetLocationCSVFromCSV (
                        'P6',
                        fnCreateCsvSubstr (zip, 6, bAllPathSets),
                        COUNTRY),
                  ','),
               ',''''',
               '');

         GOTO CreateWhereClause;
      END IF;

      IF (COUNTRY IS NOT NULL)
      THEN
         --<CR 20487>
         IF (bAllPathSets)
         THEN
            IF NOT locTypeSetToSearchDetermined
            THEN
               locTypeClause :=
                  '  ' || loc_prefix || '_loc_type in ( ''CO'' , ''ZN'') ';
               locTypeSetToSearchDetermined := TRUE;
            END IF;

            IF (firstCondition)
            THEN
               vSearchString :=
                     ' ( '
                  || loc_prefix
                  || '_country_code = '''
                  || COUNTRY
                  || ''' OR '
                  || ' ( '
                  || loc_prefix
                  || '_zone_id IN ( select zone_id from zone_attribute where attribute_type = ''CT'' AND  attribute_value  = '''
                  || COUNTRY
                  || ''' AND country_code = '''
                  || COUNTRY
                  || ''' AND tc_company_id in ( '
                  || coids
                  || ' ) ) ) ) ';
               firstCondition := FALSE;
            ELSE
               vSearchString :=
                     vSearchString
                  || ' AND ( '
                  || loc_prefix
                  || '_country_code = '''
                  || COUNTRY
                  || ''' OR '
                  || ' ( '
                  || loc_prefix
                  || '_zone_id IN ( select zone_id from zone_attribute where attribute_type = ''CT'' AND  attribute_value  = '''
                  || COUNTRY
                  || ''' AND country_code = '''
                  || COUNTRY
                  || ''' AND tc_company_id in ( '
                  || coids
                  || ' ) ) ) ) ';
            END IF;
         ELSE
            --</CR 20487>
            vSearchString :=
               RTRIM (
                  vSearchString || ','
                  || Lane_Location_Pkg.fnGetLocationCSVFromCSV ('CO',
                                                                COUNTRY),
                  ',');
            GOTO CreateWhereClause;
         END IF;
      END IF;

      IF (ZONE IS NOT NULL)
      THEN
         vSearchString :=
            RTRIM (
                  vSearchString
               || ','
               || Lane_Location_Pkg.fnGetLocationCSVFromCSV ('ZN', ZONE),
               ',');
      ELSE
         IF NOT bAllPathSets
         THEN                                                     --<CR 20487>
            vSearchString :=
               RTRIM (   vSearchString
                      || ','
                      || Lane_Location_Pkg.fnGetLocationCSVFromCSV (
                            'ZN',
                            fnGetZoneListCsv (coid,
                                              facid,
                                              city,
                                              st,
                                              zip,
                                              COUNTRY)),
                      ',');
         END IF;
      END IF;

     <<CreateWhereClause>>
      vSearchString := LTRIM (vSearchString, ',');
      DBMS_OUTPUT.PUT_LINE ('vSearchString' || vSearchString);

      IF vSearchString IS NOT NULL
      THEN
         IF (bAllPathSets AND facid IS NULL AND zip IS NULL)
         THEN
            IF locTypeSetToSearchDetermined AND locTypeClause IS NOT NULL
            THEN
               vSearchString := vSearchString || ' AND ' || locTypeClause;
            END IF;

            RETURN vSearchString;
         ELSE
            formatted_where_clause :=
                  'UPPER('
               || loc_prefix
               || '_SEARCH_LOCATION) IN ('
               || vSearchString
               || ')';
         --fnFormatContextInClause(REPLACE(vSearchString,'''',''),loc_prefix || 'SL') ||

         END IF;
      END IF;

      IF vCountyClause IS NOT NULL
      THEN
         IF (NOT bAllPathSets OR facid IS NOT NULL OR zip IS NOT NULL)
         THEN
            formatted_where_clause := formatted_where_clause || vCountyClause;
         END IF;
      END IF;

      RETURN formatted_where_clause;
   END fnFormatFilterLocWhereClause;

   /************************************
   *************************************/
   FUNCTION fnGetZoneListCsv (coid      IN PATH_SET.tc_company_id%TYPE,
                              facid     IN VARCHAR2,
                              city      IN PATH_SET.o_city%TYPE,
                              st        IN PATH_SET.o_state_prov%TYPE,
                              zip       IN PATH_SET.o_postal_code%TYPE,
                              COUNTRY   IN PATH_SET.o_country_code%TYPE)
      RETURN VARCHAR2
   IS
      TYPE ZONE_CURSOR IS REF CURSOR;

      zones          ZONE_CURSOR;

      vzone          ZONE.zone_id%TYPE;
      zone_list      VARCHAR2 (1000);
      sql_str        VARCHAR2 (4000);
      where_clause   VARCHAR2 (4000) := NULL;
      date_str       VARCHAR (30);
   BEGIN
      sql_str :=
            'select distinct z1.zone_id '
         || 'from zone_view z1 '
         || 'where (z1.tc_company_id = '
         || coid
         || ') ';

      IF (facid IS NOT NULL)
      THEN
         where_clause :=
            where_clause
            || '( z1.attribute_type = ''FC'' and z1.attribute_value IN ( select to_char(facility_id) from facility_alias where facility_alias_id in ( '
            || facid
            || ') and tc_company_id = '
            || coid
            || ') ) ';
      END IF;

      IF (country IS NOT NULL)
      THEN
         IF (where_clause IS NOT NULL)
         THEN
            where_clause := where_clause || ' or ';
         END IF;

         where_clause :=
               where_clause
            || '( z1.attribute_type = ''CT'' and z1.attribute_value = '''
            || country
            || ''' ) ';

         IF (st IS NOT NULL)
         THEN
            where_clause :=
               where_clause
               || ' or ( z1.attribute_type = ''ST'' and z1.attribute_value = '''
               || st
               || ''' and z1.country_code = '''
               || country
               || ''') ';
         END IF;

         IF (zip IS NOT NULL)
         THEN
            -- gakumar, Modified for PE reasons. Ref: CBO CR's: MACR00122848,MACR00122905
            where_clause :=
               where_clause
               || 'or (z1.attribute_type = ''Z2'' and z1.attribute_value in ( '
               || Create_Csv_Substr (zip, 2)
               || ' ) and z1.country_code = '''
               || country
               || ''') '
               || 'or (z1.attribute_type = ''Z3'' and z1.attribute_value in ( '
               || Create_Csv_Substr (zip, 3)
               || ' ) and z1.country_code = '''
               || country
               || ''') '
               || 'or (z1.attribute_type = ''Z4'' and z1.attribute_value in ( '
               || Create_Csv_Substr (zip, 4)
               || ' ) and z1.country_code = '''
               || country
               || ''') '
               || 'or (z1.attribute_type = ''Z5'' and z1.attribute_value in ( '
               || Create_Csv_Substr (zip, 5)
               || ' ) and z1.country_code = '''
               || country
               || ''') '
               || 'or (z1.attribute_type = ''Z6'' and z1.attribute_value in ( '
               || Create_Csv_Substr (zip, 6)
               || ' ) and z1.country_code = '''
               || country
               || ''') '
               || 'or (  z1.ATTRIBUTE_TYPE = ''PR'' '
               || 'and '''
               || zip
               || '''  between z1.attribute_value and  attribute_value2  '
               || 'and z1.COUNTRY_CODE = '''
               || country
               || ''' '
               || ')';
         END IF;
      END IF;

      IF where_clause IS NULL
      THEN
         RETURN NULL;
      END IF;

      sql_str := sql_str || ' and ( ' || where_clause || ' ) ';

      OPEN zones FOR sql_str;

      LOOP
         FETCH zones INTO vzone;

         EXIT WHEN zones%NOTFOUND;

         zone_list := zone_list || '''' || vzone || ''',';
      END LOOP;

      CLOSE zones;

      zone_list := RTRIM (zone_list, ',');

      RETURN zone_list;
   END;

   /*************************************
   *************************************/

   /********************************************
   ********************************************/
   FUNCTION fnCreateCsvSubstr (aString      VARCHAR2,
                               aLength      NUMBER,
                               aAllLanes    BOOLEAN)
      RETURN VARCHAR2
   IS
      vWorkString   VARCHAR2 (500) := REPLACE (aString, '''') || ',';
      vResultStr    VARCHAR2 (500) := NULL;
      vValue        VARCHAR2 (20);

      i             NUMBER := 0;
   BEGIN
      WHILE LENGTH (vWorkString) > 1 AND i < 10
      LOOP
         i := i + 1;

         vValue := SUBSTR (vWorkString, 1, INSTR (vWorkString, ',') - 1);
         vWorkString := LTRIM (LTRIM (vWorkString, vValue), ',');

         IF aAllLanes
         THEN
            vValue := SUBSTR (vValue, 1, aLength);
         END IF;

         IF LENGTH (vValue) = aLength
         THEN
            vResultStr := vResultStr || ',''' || vValue || '''';
         END IF;
      END LOOP;

      RETURN LTRIM (NVL (vResultStr, ''''''), ',');
   END;

   --  Adding Code For Path Set Retrieval

   FUNCTION fnFormatLocationList (
      coid      IN rating_lane.tc_company_id%TYPE,
      facid     IN rating_lane.o_facility_id%TYPE,
      city      IN rating_lane.o_city%TYPE,
      cnty      IN rating_lane.o_county%TYPE,
      st        IN rating_lane.o_state_prov%TYPE,
      zip       IN rating_lane.o_postal_code%TYPE,
      country   IN rating_lane.o_country_code%TYPE)
      RETURN tyLocationList
   IS
      CURSOR czones
      IS
         -- gakumar, Modified for PE reasons. Ref: CBO CR's: MACR00122848,MACR00122905
         SELECT DISTINCT z1.zone_id
           FROM zone_attribute z1
          WHERE (z1.tc_company_id = coid)
                AND ( (z1.ATTRIBUTE_TYPE = 'FC'
                       AND z1.attribute_value = TO_CHAR (facid))
                     OR (    z1.ATTRIBUTE_TYPE = 'CI'
                         AND z1.attribute_value = city
                         AND z1.attribute_value2 = st
                         AND z1.country_code = COUNTRY)
                     OR (    z1.ATTRIBUTE_TYPE = 'ST'
                         AND z1.attribute_value = st
                         AND z1.country_code = COUNTRY)
                     OR (z1.ATTRIBUTE_TYPE = 'CT'
                         AND z1.attribute_value = COUNTRY)
                     OR (    z1.ATTRIBUTE_TYPE = 'Z2'
                         AND z1.attribute_value = SUBSTR (zip, 0, 2)
                         AND z1.country_code = COUNTRY)
                     OR (    z1.ATTRIBUTE_TYPE = 'Z3'
                         AND z1.attribute_value = SUBSTR (zip, 0, 3)
                         AND z1.country_code = COUNTRY)
                     OR (    z1.ATTRIBUTE_TYPE = 'Z4'
                         AND z1.attribute_value = SUBSTR (zip, 0, 4)
                         AND z1.country_code = COUNTRY)
                     OR (    z1.ATTRIBUTE_TYPE = 'Z5'
                         AND z1.attribute_value = SUBSTR (zip, 0, 5)
                         AND z1.country_code = COUNTRY)
                     OR (    z1.ATTRIBUTE_TYPE = 'Z6'
                         AND z1.attribute_value = SUBSTR (zip, 0, 6)
                         AND z1.country_code = COUNTRY)
                     OR (z1.ATTRIBUTE_TYPE = 'PR'
                         AND zip BETWEEN z1.attribute_value
                                     AND attribute_value2
                         AND z1.country_code = COUNTRY));

      formatted_where_clause   VARCHAR2 (4000);
      -- zones  VARCHAR2(2000);
      tblLocationList          tyLocationList;
      i                        NUMBER DEFAULT -1;
   BEGIN
      -- zones := get_zone_list( coid, facid, city, st, zip, country );
      IF (facid IS NOT NULL)
      THEN
         -- the lane_location_pkg.fnGetLocationString function only
         -- uses the fields corresponding to the aLocType value
         -- pass everything incase this formula changes
         i := i + 1;
         /* Horea
            tblLocationList(i) := '''' || lane_location_pkg.fnGetLocationString (
               aLocType      => 'FA'
               ,aFacilityId  => facid
               ,aCity        => city
               ,aCounty      => cnty
               ,aStateProv   => st
               ,aPostalCode  => zip
               ,aCountryCode => country
               ,aZoneId      => null
               ) || ''''
               ;
         */
         tblLocationList (i) :='FA' || cvDelimiter || TO_CHAR (facid);
      END IF;

      IF (city IS NOT NULL)
      THEN
         i := i + 1;
         tblLocationList (i) :='CS'
                      || cvDelimiter
                      || REPLACE (city, '''', '''''')
                      || cvDelimiter
                      || st
                      || cvDelimiter
                      || country;
      END IF;

      IF (st IS NOT NULL)
      THEN
         i := i + 1;
         tblLocationList (i) :='ST'
                      || cvDelimiter
                      || st
                      || cvDelimiter
                      || country;
      END IF;

      IF (country IS NOT NULL)
      THEN
         i := i + 1;
          tblLocationList (i) :='CO' || cvDelimiter || country;
      END IF;

      IF (zip IS NOT NULL)
      THEN
         i := i + 1;
          tblLocationList (i) :='P2'
                      || cvDelimiter
                      || SUBSTR (zip, 1, 2)
                      || cvDelimiter
                      || country;
         i := i + 1;
          tblLocationList (i) :='P3'
                      || cvDelimiter
                      || SUBSTR (zip, 1, 3)
                      || cvDelimiter
                      || country;
         i := i + 1;
         tblLocationList (i) :='P4'
                      || cvDelimiter
                      || SUBSTR (zip, 1, 4)
                      || cvDelimiter
                      || country;
         i := i + 1;
         tblLocationList (i) :='P5'
                      || cvDelimiter
                      || SUBSTR (zip, 1, 5)
                      || cvDelimiter
                      || country;
         i := i + 1;
         tblLocationList (i) :='P6'
                      || cvDelimiter
                      || SUBSTR (zip, 1, 6)
                      || cvDelimiter
                      || country;
      END IF;

      FOR r IN cZones
      LOOP
         i := i + 1;
         tblLocationList (i) := 'ZN' || '~' || r.zone_id;
      END LOOP;

      RETURN tblLocationList;
   END fnFormatLocationList;

   PROCEDURE PATH_SET_RETRIEVAL_REQUEST (IN_TC_COMPANY_ID    NUMBER,
                                         IS_MANUAL           INTEGER)
   IS
      vOriginList          VARCHAR2 (2000) DEFAULT NULL;
      vDestinationList     VARCHAR2 (2000) DEFAULT NULL;
      tblOriginList        tyLocationList;
      tblDestinationList   tyLocationList;
      l_tc_company_id      NUMBER (8);
      i                    PLS_INTEGER DEFAULT -1;
      l_path_set_id        NUMBER;
      iflag                BOOLEAN;
      paths_matched        NUMBER;
   BEGIN
      FOR REQ IN (  SELECT *
                      FROM PATH_SET_RETRIEVAL_GTT
                  ORDER BY REQUEST_SEQ)
      LOOP
         iflag := FALSE;
         --begin

         tblOriginList :=
            fnFormatLocationList (coid      => l_tc_company_id,
                                  facid     => req.o_facility_id,
                                  city      => req.o_city,
                                  cnty      => req.o_county,
                                  st        => req.o_state_prov,
                                  zip       => req.o_postal_code,
                                  country   => req.o_country_code);

         vOriginList := '';

         FOR i IN 0 .. tblOriginList.COUNT - 1
         LOOP
            vOriginList := vOriginList || '''' || tblOriginList (i) || ''',';
         END LOOP;

         vOriginList := RTRIM (vOriginList, ',');

         tblDestinationList :=
            fnFormatLocationList (req.tc_company_id,
                                  req.d_facility_id,
                                  req.d_city,
                                  req.d_county,
                                  req.d_state_prov,
                                  req.d_postal_code,
                                  req.d_country_code);

         vDestinationList := '';

         FOR i IN 0 .. tblDestinationList.COUNT - 1
         LOOP
            vDestinationList :=
               vDestinationList || '''' || tblDestinationList (i) || ''',';
         END LOOP;

         vDestinationList := RTRIM (vDestinationList, ',');

         --begin
         /*
             FOR  PATHS IN ( SELECT PATH_SET_ID,PATH_SET_HIERARCHY,BP_HAS_POOLPOINTs FROM PATH_SET
                   WHERE TC_COMPANY_ID=REQ.TC_COMPANY_ID AND EFFECTIVE_DT <= REQ.ORDER_DATE AND
               EXPIRATION_DT >= REQ.ORDER_DATE AND instr( VORIGINlISt,O_SEARCH_LOCATION,1) <>0 AND
               instr(vDestinationList,D_SEARCH_LOCATION,1) <>0  and is_valid=1 and is_manual_creation=IS_MANUAL)
                  loop
            iflag :=true;

            INSERT INTO PATH_SETS_GTT
            VALUES(PATHS.PATH_SET_ID,PATHS.PATH_SET_HIERARCHY,PATHS.BP_HAS_POOLPoINTs);
            END LOOP;
         */
         INSERT INTO PATH_SETS_GTT (PATH_SET_ID,
                                    PATH_SET_HIERARCHY,
                                    BP_HAS_POOLPOINTS)
            SELECT PATH_SET_ID, PATH_SET_HIERARCHY, BP_HAS_POOLPOINTS
              FROM PATH_SET
             WHERE     TC_COMPANY_ID = req.TC_COMPANY_ID
                   AND EFFECTIVE_DT <= req.ORDER_DATE
                   AND EXPIRATION_DT >= req.ORDER_DATE
                  AND INSTR(vOriginList, CONCAT(O_SEARCH_LOCATION,''''),1) > 0
					AND INSTR(vDestinationList, CONCAT(D_SEARCH_LOCATION,''''),1) > 0
                   AND (NVL (CUSTOMER_ID, -1) = req.CUSTOMER_ID
                        OR CUSTOMER_ID IS NULL) -- Gopalakrishnan S 2/25/2008 - Path Set Changes Start
                   AND (NVL (INCOTERM_ID, -1) = req.INCOTERM_ID
                        OR INCOTERM_ID IS NULL)
                   AND (NVL (BILLING_METHOD, -1) = req.BILLING_METHOD
                        OR BILLING_METHOD IS NULL)
                   AND (NVL (ROUTE_TO, '') = req.ROUTE_TO OR ROUTE_TO IS NULL)
                   AND (NVL (ROUTE_TYPE_1, '') = req.ROUTE_TYPE_1
                        OR ROUTE_TYPE_1 IS NULL)
                   AND (NVL (ROUTE_TYPE_2, '') = req.ROUTE_TYPE_2
                        OR ROUTE_TYPE_2 IS NULL) -- Gopalakrishnan S 2/25/2008 - Path Set Changes End
                   AND IS_VALID = 1
                   AND IS_MANUAL_CREATION = is_manual
                   AND IS_TANDEM = 0;

         IF (SQL%ROWCOUNT > 0)
         THEN
            iflag := TRUE;
         END IF;

         IF iflag = TRUE
         THEN
            IF REQ.ORDER_SIZE_PARAMETER = 0
            THEN
               -- begin

               SELECT NVL (MIN (PATH_SET_ID), -1)
                 INTO L_PATH_SET_ID
                 FROM PATH_SETS_GTT
                WHERE PATH_SET_HIERARCHY = (SELECT MIN (PATH_SET_HIERARCHY)
                                              FROM PATH_SETS_GTT
                                             WHERE BP_HAS_POOLPOINTs = 0)
                      AND BP_HAS_POOLPOINTs = 0;
            /*
            exception
            when no_data_found then
             begin
             L_PATH_SET_ID := -1;
             end;
                end;
           */
            ELSE
               -- begin

               SELECT NVL (MIN (PATH_SET_ID), -1)
                 INTO L_PATH_SET_ID
                 FROM PATH_SETS_GTT
                WHERE (PATH_SET_HIERARCHY, BP_HAS_POOLPOINTs) IN
                         (  SELECT PATH_SET_HIERARCHY, MAX (BP_HAS_POOLPOINTs)
                              FROM PATH_SETS_GTT
                             WHERE PATH_SET_HIERARCHY =
                                      (SELECT MIN (PATH_SET_HIERARCHY)
                                         FROM PATH_SETS_GTT)
                          GROUP BY PATH_SET_HIERARCHY);
            /*
               exception
              when no_data_found then
                begin
                    L_PATH_SET_ID := -1;
                end;
                        end;
            */
            END IF;

            IF (L_PATH_SET_ID >= 0)
            THEN
               UPDATE PATH_SET_RETRIEVAL_GTT
                  SET PATH_SET_ID = L_PATH_SET_ID
                WHERE request_seq = req.request_seq;
            END IF;

            DELETE FROM PATH_SETS_GTT;
         END IF;

         IFLAG := FALSE;
      --END;

      END LOOP;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         NULL;
   END;

   FUNCTION fnFormatZoneList (p_tc_company_id NUMBER, p_facility_id NUMBER)
      RETURN tyLocationList
   IS
      tblLocationList   tyLocationList;
   BEGIN
      SELECT 'ZN~' || zone_id AS zone_id
        BULK COLLECT INTO tblLocationList
        FROM facility_zone, facility
       WHERE     facility_zone.tc_company_id = p_tc_company_id
             AND facility_zone.facility_id = p_facility_id
             AND facility.facility_id = facility_zone.facility_id
             AND facility.FACILITY_TYPE_BITS >= (SELECT bit_value
                                                   FROM FACILITY_TYPE
                                                  WHERE FACILITY_TYPE = 'PT'); --should at least port

      RETURN tblLocationList;
   END fnFormatZoneList;

      PROCEDURE PATH_SET_RETRIEVAL_REQUEST_NEW (in_tc_company_id    NUMBER,
                                             is_manual           INTEGER)
   IS
      voriginlist          VARCHAR2 (2000) DEFAULT NULL;
      vdestinationlist     VARCHAR2 (2000) DEFAULT NULL;
      tbloriginlist        tylocationlist;
      tbldestinationlist   tylocationlist;
      l_tc_company_id      NUMBER (8);
      i                    PLS_INTEGER DEFAULT -1;
      l_path_set_id        NUMBER;
      iflag                BOOLEAN;
      paths_matched        NUMBER;
      tblzonelist          tylocationlist;
      oflag                BOOLEAN;
      dflag                BOOLEAN;
      p_hierarchy          NUMBER (4);
      temp                 VARCHAR2 (4000);
   BEGIN
      FOR req IN (  SELECT *
                      FROM PATH_SET_RETRIEVAL_GTT where path_set_id is null
                  ORDER BY request_seq)
      LOOP
         iflag := FALSE;
         tbloriginlist :=
            fnformatlocationlist (coid      => req.tc_company_id,
                                  facid     => req.o_facility_id,
                                  city      => req.o_city,
                                  cnty      => req.o_county,
                                  st        => req.o_state_prov,
                                  zip       => req.o_postal_code,
                                  country   => req.o_country_code);
         voriginlist := '';

         FOR i IN 0 .. tbloriginlist.COUNT - 1
         LOOP
            voriginlist := voriginlist || '''' || tbloriginlist (i) || ''',';
         END LOOP;

         voriginlist := RTRIM (voriginlist, ',');
         tbldestinationlist :=
            fnformatlocationlist (req.tc_company_id,
                                  req.d_facility_id,
                                  req.d_city,
                                  req.d_county,
                                  req.d_state_prov,
                                  req.d_postal_code,
                                  req.d_country_code);
         vdestinationlist := '';

         FOR i IN 0 .. tbldestinationlist.COUNT - 1
         LOOP
            vdestinationlist :=
               vdestinationlist || '''' || tbldestinationlist (i) || ''',';
         END LOOP;

         vdestinationlist := RTRIM (vdestinationlist, ',');

		--INSERT INTO DEBUG_SQL VALUES ('RNDTLM', sysdate, 'voriginlist = ' || voriginlist, null);

		--INSERT INTO DEBUG_SQL VALUES ('RNDTLM', sysdate, 'vdestinationlist = ' || vdestinationlist, null);


         FOR paths
            IN (SELECT path_set_id,
                       path_set_hierarchy,
                       bp_has_poolpoints,
                       o_facility_id,
                       d_facility_id,
                       o_search_location,
                       d_search_location,
					   incoterm_id
                  FROM path_set
                 WHERE     tc_company_id = req.tc_company_id
                       AND effective_dt <= req.order_date
                       AND expiration_dt >= req.order_date
                       AND is_valid = 1
                       AND is_manual_creation = is_manual
                       AND IS_TANDEM = 0)
         LOOP
            oflag := FALSE;
            dflag := FALSE;

			 FOR i IN 0 .. tbloriginlist.COUNT - 1
			 LOOP
				IF ( tbloriginlist (i) = paths.o_search_location )
				THEN
					oflag := TRUE;
				END IF;
			 END LOOP;

			 FOR i IN 0 .. tbldestinationlist.COUNT - 1
			 LOOP
				IF ( tbldestinationlist (i) = paths.d_search_location )
				THEN
					dflag := TRUE;
				END IF;
			 END LOOP;

			IF (req.incoterm_id != -1 and ( req.incoterm_id !=  paths.incoterm_id AND paths.incoterm_id is not null))
            THEN
         	   oflag    := FALSE;
            END IF;

            -- gakumar: MACR00203786 Fix
            -- gakumar: Zones cannot be formed using facility_id on path_set records.
            -- gakumar: It should be formed based on Requested facilities

            IF (oflag = TRUE) AND (dflag = TRUE)
            THEN
               iflag := TRUE;

			   --INSERT INTO DEBUG_SQL VALUES ('RNDTLM', sysdate, 'paths.path_set_id = ' || to_char(paths.path_set_id), null);



               INSERT INTO PATH_SETS_GTT
                    VALUES (
                              paths.path_set_id,
                              paths.path_set_hierarchy,
                              paths.bp_has_poolpoints);
            END IF;
         END LOOP;

         IF iflag = TRUE
         THEN
            IF req.order_size_parameter = 0
            THEN
               
               i := 0;

               -- these changes are done for handling of multiple path sets for an order
               FOR paths_sets
                  IN (SELECT NVL (path_set_id, -1) path_set_id
                        FROM PATH_SETS_GTT
                       WHERE  
                       bp_has_poolpoints = 0)
               LOOP
			   --INSERT INTO DEBUG_SQL VALUES ('RNDTLM', sysdate, 'paths_sets.path_set_id = ' || to_char(paths_sets.path_set_id), null);


                  IF (paths_sets.path_set_id >= 0 AND i = 0)

                  THEN
				  --INSERT INTO DEBUG_SQL VALUES ('RNDTLM', sysdate, 'Updating PATH_SET_RETRIEVAL_GTT for paths_sets.path_set_id = ' || to_char(paths_sets.path_set_id), null);


                     UPDATE PATH_SET_RETRIEVAL_GTT
                        SET path_set_id = paths_sets.path_set_id
                      WHERE request_seq = req.request_seq;

                     i := 1;
                  ELSE
				  --INSERT INTO DEBUG_SQL VALUES ('RNDTLM', sysdate, 'Inserting into PATH_SET_RETRIEVAL_GTT for paths_sets.path_set_id = ' || to_char(paths_sets.path_set_id), null);


                     INSERT INTO PATH_SET_RETRIEVAL_GTT (
                                    request_seq,
                                    tc_company_id,
                                    o_facility_id,
                                    o_city,
                                    o_state_prov,
                                    o_county,
                                    o_postal_code,
                                    o_country_code,
                                    d_facility_id,
                                    d_city,
                                    d_state_prov,
                                    d_county,
                                    d_postal_code,
                                    d_country_code,
                                    order_date,
                                    order_size_parameter,
                                    path_set_id,
                                    comments)
                        SELECT request_seq,
                               tc_company_id,
                               o_facility_id,
                               o_city,
                               o_state_prov,
                               o_county,
                               o_postal_code,
                               o_country_code,
                               d_facility_id,
                               d_city,
                               d_state_prov,
                               d_county,
                               d_postal_code,
                               d_country_code,
                               order_date,
                               order_size_parameter,
                               paths_sets.path_set_id,
                               comments
                          FROM PATH_SET_RETRIEVAL_GTT
                         WHERE request_seq = req.request_seq and rownum=1;
                  END IF;
               END LOOP;
            ELSE
               

               i := 0;

               -- these changes are done for handling of multiple path sets for an order
               FOR paths_sets
                  IN (SELECT NVL (path_set_id, -1) path_set_id
                        FROM PATH_SETS_GTT
                       WHERE (path_set_hierarchy, bp_has_poolpoints) IN
                                (  SELECT path_set_hierarchy,
                                          MAX (bp_has_poolpoints)
                                     FROM PATH_SETS_GTT
                                 GROUP BY path_set_hierarchy))
               LOOP
			   --INSERT INTO DEBUG_SQL VALUES ('RNDTLM', sysdate, 'paths_sets.path_set_id = ' || to_char(paths_sets.path_set_id), null);


                  IF (paths_sets.path_set_id >= 0 AND i = 0)
                  THEN
				  --INSERT INTO DEBUG_SQL VALUES ('RNDTLM', sysdate, 'Updating PATH_SET_RETRIEVAL_GTT for paths_sets.path_set_id = ' || to_char(paths_sets.path_set_id), null);


                     UPDATE PATH_SET_RETRIEVAL_GTT
                        SET path_set_id = paths_sets.path_set_id
                      WHERE request_seq = req.request_seq;

                     i := 1;
                  ELSE
				  --INSERT INTO DEBUG_SQL VALUES ('RNDTLM', sysdate, 'Inserting into PATH_SET_RETRIEVAL_GTT for paths_sets.path_set_id = ' || to_char(paths_sets.path_set_id), null);


                     INSERT INTO PATH_SET_RETRIEVAL_GTT (
                                    request_seq,
                                    tc_company_id,
                                    o_facility_id,
                                    o_city,
                                    o_state_prov,
                                    o_county,
                                    o_postal_code,
                                    o_country_code,
                                    d_facility_id,
                                    d_city,
                                    d_state_prov,
                                    d_county,
                                    d_postal_code,
                                    d_country_code,
                                    order_date,
                                    order_size_parameter,
                                    path_set_id,
                                    comments)
                        SELECT request_seq,
                               tc_company_id,
                               o_facility_id,
                               o_city,
                               o_state_prov,
                               o_county,
                               o_postal_code,
                               o_country_code,
                               d_facility_id,
                               d_city,
                               d_state_prov,
                               d_county,
                               d_postal_code,
                               d_country_code,
                               order_date,
                               order_size_parameter,
                               paths_sets.path_set_id,
                               comments
                          FROM PATH_SET_RETRIEVAL_GTT
                         WHERE request_seq = req.request_seq and rownum=1;
                  END IF;
               END LOOP;
            END IF;

            DELETE FROM PATH_SETS_GTT;
         END IF;

         iflag := FALSE;
      END LOOP;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
		 --INSERT INTO DEBUG_SQL VALUES ('RNDTLM', sysdate, 'NO_DATA_FOUND', null);

         NULL;
   END path_set_retrieval_request_new;


PROCEDURE PATH_SET_RETRIEVAL_REQUEST (IN_TC_COMPANY_ID    NUMBER,
                                         IS_MANUAL           INTEGER,
                                         IN_IS_TANDEM        INTEGER)
   IS
      voriginlist          VARCHAR2 (2000) DEFAULT NULL;
      vdestinationlist     VARCHAR2 (2000) DEFAULT NULL;
      tbloriginlist        tylocationlist;
      tbldestinationlist   tylocationlist;
      l_tc_company_id      NUMBER (8);
      i                    PLS_INTEGER DEFAULT -1;
      l_path_set_id        NUMBER;
      iflag                BOOLEAN;
      paths_matched        NUMBER;
      tblzonelist          tylocationlist;
      oflag                BOOLEAN;
      dflag                BOOLEAN;
      p_hierarchy          NUMBER (4);
      temp                 VARCHAR2 (4000);
   BEGIN
      FOR req IN (  SELECT *
                      FROM PATH_SET_RETRIEVAL_GTT where path_set_id is null
                  ORDER BY request_seq)
      LOOP
         iflag := FALSE;
         tbloriginlist :=
            fnformatlocationlist (coid      => req.tc_company_id,
                                  facid     => req.o_facility_id,
                                  city      => req.o_city,
                                  cnty      => req.o_county,
                                  st        => req.o_state_prov,
                                  zip       => req.o_postal_code,
                                  country   => req.o_country_code);
         voriginlist := '';

         FOR i IN 0 .. tbloriginlist.COUNT - 1
         LOOP
            voriginlist := voriginlist || '''' || tbloriginlist (i) || ''',';
         END LOOP;

         voriginlist := RTRIM (voriginlist, ',');
         tbldestinationlist :=
            fnformatlocationlist (req.tc_company_id,
                                  req.d_facility_id,
                                  req.d_city,
                                  req.d_county,
                                  req.d_state_prov,
                                  req.d_postal_code,
                                  req.d_country_code);
         vdestinationlist := '';

         FOR i IN 0 .. tbldestinationlist.COUNT - 1
         LOOP
            vdestinationlist :=
               vdestinationlist || '''' || tbldestinationlist (i) || ''',';
         END LOOP;

         vdestinationlist := RTRIM (vdestinationlist, ',');

		--INSERT INTO DEBUG_SQL VALUES ('RNDTLM', sysdate, 'voriginlist = ' || voriginlist, null);

		--INSERT INTO DEBUG_SQL VALUES ('RNDTLM', sysdate, 'vdestinationlist = ' || vdestinationlist, null);


         FOR paths
            IN (SELECT path_set_id,
                       path_set_hierarchy,
                       bp_has_poolpoints,
                       o_facility_id,
                       d_facility_id,
                       o_search_location,
                       d_search_location,
					   incoterm_id
                  FROM path_set
                 WHERE     tc_company_id = req.tc_company_id
                       AND effective_dt <= req.order_date
                       AND expiration_dt >= req.order_date
                       AND is_valid = 1
                       AND is_manual_creation = is_manual
                       AND IS_TANDEM = IN_IS_TANDEM)
         LOOP
            oflag := FALSE;
            dflag := FALSE;

			 FOR i IN 0 .. tbloriginlist.COUNT - 1
			 LOOP
				IF ( tbloriginlist (i) = paths.o_search_location )
				THEN
					oflag := TRUE;
				END IF;
			 END LOOP;

			 FOR i IN 0 .. tbldestinationlist.COUNT - 1
			 LOOP
				IF ( tbldestinationlist (i) = paths.d_search_location )
				THEN
					dflag := TRUE;
				END IF;
			 END LOOP;

			IF (req.incoterm_id != -1 and ( req.incoterm_id !=  paths.incoterm_id AND paths.incoterm_id is not null))
            THEN
         	   oflag    := FALSE;
            END IF;

            -- gakumar: MACR00203786 Fix
            -- gakumar: Zones cannot be formed using facility_id on path_set records.
            -- gakumar: It should be formed based on Requested facilities

            IF (oflag = TRUE) AND (dflag = TRUE)
            THEN
               iflag := TRUE;

			   --INSERT INTO DEBUG_SQL VALUES ('RNDTLM', sysdate, 'paths.path_set_id = ' || to_char(paths.path_set_id), null);



               INSERT INTO PATH_SETS_GTT
                    VALUES (
                              paths.path_set_id,
                              paths.path_set_hierarchy,
                              paths.bp_has_poolpoints);
            END IF;
         END LOOP;

         IF iflag = TRUE
         THEN
            IF req.order_size_parameter = 0
            THEN
               

               i := 0;

               -- these changes are done for handling of multiple path sets for an order
               FOR paths_sets
                  IN (SELECT NVL (path_set_id, -1) path_set_id
                        FROM PATH_SETS_GTT
                       WHERE  
                       bp_has_poolpoints = 0)
               LOOP
			   --INSERT INTO DEBUG_SQL VALUES ('RNDTLM', sysdate, 'paths_sets.path_set_id = ' || to_char(paths_sets.path_set_id), null);


                  IF (paths_sets.path_set_id >= 0 AND i = 0)

                  THEN
				  --INSERT INTO DEBUG_SQL VALUES ('RNDTLM', sysdate, 'Updating PATH_SET_RETRIEVAL_GTT for paths_sets.path_set_id = ' || to_char(paths_sets.path_set_id), null);


                     UPDATE PATH_SET_RETRIEVAL_GTT
                        SET path_set_id = paths_sets.path_set_id
                      WHERE request_seq = req.request_seq;

                     i := 1;
                  ELSE
				  --INSERT INTO DEBUG_SQL VALUES ('RNDTLM', sysdate, 'Inserting into PATH_SET_RETRIEVAL_GTT for paths_sets.path_set_id = ' || to_char(paths_sets.path_set_id), null);


                     INSERT INTO PATH_SET_RETRIEVAL_GTT (
                                    request_seq,
                                    tc_company_id,
                                    o_facility_id,
                                    o_city,
                                    o_state_prov,
                                    o_county,
                                    o_postal_code,
                                    o_country_code,
                                    d_facility_id,
                                    d_city,
                                    d_state_prov,
                                    d_county,
                                    d_postal_code,
                                    d_country_code,
                                    order_date,
                                    order_size_parameter,
                                    path_set_id,
                                    comments)
                        SELECT request_seq,
                               tc_company_id,
                               o_facility_id,
                               o_city,
                               o_state_prov,
                               o_county,
                               o_postal_code,
                               o_country_code,
                               d_facility_id,
                               d_city,
                               d_state_prov,
                               d_county,
                               d_postal_code,
                               d_country_code,
                               order_date,
                               order_size_parameter,
                               paths_sets.path_set_id,
                               comments
                          FROM PATH_SET_RETRIEVAL_GTT
                         WHERE request_seq = req.request_seq and rownum=1;
                  END IF;
               END LOOP;
            ELSE
               

               i := 0;

               -- these changes are done for handling of multiple path sets for an order
               FOR paths_sets
                  IN (SELECT NVL (path_set_id, -1) path_set_id
                        FROM PATH_SETS_GTT
                       WHERE (path_set_hierarchy, bp_has_poolpoints) IN
                                (  SELECT path_set_hierarchy,
                                          MAX (bp_has_poolpoints)
                                     FROM PATH_SETS_GTT
                                 GROUP BY path_set_hierarchy))
               LOOP
			   --INSERT INTO DEBUG_SQL VALUES ('RNDTLM', sysdate, 'paths_sets.path_set_id = ' || to_char(paths_sets.path_set_id), null);


                  IF (paths_sets.path_set_id >= 0 AND i = 0)
                  THEN
				  --INSERT INTO DEBUG_SQL VALUES ('RNDTLM', sysdate, 'Updating PATH_SET_RETRIEVAL_GTT for paths_sets.path_set_id = ' || to_char(paths_sets.path_set_id), null);


                     UPDATE PATH_SET_RETRIEVAL_GTT
                        SET path_set_id = paths_sets.path_set_id
                      WHERE request_seq = req.request_seq;

                     i := 1;
                  ELSE
				  --INSERT INTO DEBUG_SQL VALUES ('RNDTLM', sysdate, 'Inserting into PATH_SET_RETRIEVAL_GTT for paths_sets.path_set_id = ' || to_char(paths_sets.path_set_id), null);


                     INSERT INTO PATH_SET_RETRIEVAL_GTT (
                                    request_seq,
                                    tc_company_id,
                                    o_facility_id,
                                    o_city,
                                    o_state_prov,
                                    o_county,
                                    o_postal_code,
                                    o_country_code,
                                    d_facility_id,
                                    d_city,
                                    d_state_prov,
                                    d_county,
                                    d_postal_code,
                                    d_country_code,
                                    order_date,
                                    order_size_parameter,
                                    path_set_id,
                                    comments)
                        SELECT request_seq,
                               tc_company_id,
                               o_facility_id,
                               o_city,
                               o_state_prov,
                               o_county,
                               o_postal_code,
                               o_country_code,
                               d_facility_id,
                               d_city,
                               d_state_prov,
                               d_county,
                               d_postal_code,
                               d_country_code,
                               order_date,
                               order_size_parameter,
                               paths_sets.path_set_id,
                               comments
                          FROM PATH_SET_RETRIEVAL_GTT
                         WHERE request_seq = req.request_seq and rownum=1;
                  END IF;
               END LOOP;
            END IF;

            DELETE FROM PATH_SETS_GTT;
         END IF;

         iflag := FALSE;
      END LOOP;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
		 --INSERT INTO DEBUG_SQL VALUES ('RNDTLM', sysdate, 'NO_DATA_FOUND', null);

         NULL;
	END;

END;
/
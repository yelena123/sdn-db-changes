--WM19A Label_display Changes
--Chamber 1, label_display updates
MERGE INTO LABEL_DISPLAY L
USING (SELECT 'en' TO_LANG,
(select value from label where key = 'Chamber1' and bundle_name = 'CBOGrid') DISP_KEY
FROM DUAL) B
ON (L.DISP_KEY = B.DISP_KEY AND L.TO_LANG = B.TO_LANG)
WHEN MATCHED THEN
  update set L.DISP_VALUE = (select nvl((SELECT code_desc FROM sys_code WHERE code_type = 'PRD' AND rec_type = 'C' AND code_id = '001'),'Chamber 1') from dual) where L.DISP_KEY = (select value from label where key = 'Chamber1' and bundle_name = 'CBOGrid') and L.TO_LANG = 'en'
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_DISPLAY_ID,
L.DISP_KEY,
L.DISP_VALUE,
L.TO_LANG)
VALUES (SEQ_LABEL_DISPLAY_ID.NEXTVAL,
(select value from label where key = 'Chamber1' and bundle_name = 'CBOGrid'),
(select nvl((SELECT code_desc FROM sys_code WHERE code_type = 'PRD' AND rec_type = 'C' AND code_id = '001'),'Chamber 1') from dual),
'en');


--Chamber 2, label_display updates
MERGE INTO LABEL_DISPLAY L
USING (SELECT 'en' TO_LANG,
(select value from label where key = 'Chamber2' and bundle_name = 'CBOGrid') DISP_KEY
FROM DUAL) B
ON (L.DISP_KEY = B.DISP_KEY AND L.TO_LANG = B.TO_LANG)
WHEN MATCHED THEN
  update set L.DISP_VALUE = (select nvl((SELECT code_desc FROM sys_code WHERE code_type = 'PRD' AND rec_type = 'C' AND code_id = '002'),'Chamber 2') from dual) where L.DISP_KEY = (select value from label where key = 'Chamber2' and bundle_name = 'CBOGrid') and L.TO_LANG = 'en'
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_DISPLAY_ID,
L.DISP_KEY,
L.DISP_VALUE,
L.TO_LANG)
VALUES (SEQ_LABEL_DISPLAY_ID.NEXTVAL,
(select value from label where key = 'Chamber2' and bundle_name = 'CBOGrid'),
(select nvl((SELECT code_desc FROM sys_code WHERE code_type = 'PRD' AND rec_type = 'C' AND code_id = '002'),'Chamber 2') from dual),
'en');

--Chamber 3, label_display updates
MERGE INTO LABEL_DISPLAY L
USING (SELECT 'en' TO_LANG,
(select value from label where key = 'Chamber3' and bundle_name = 'CBOGrid') DISP_KEY
FROM DUAL) B
ON (L.DISP_KEY = B.DISP_KEY AND L.TO_LANG = B.TO_LANG)
WHEN MATCHED THEN
  update set L.DISP_VALUE = (select nvl((SELECT code_desc FROM sys_code WHERE code_type = 'PRD' AND rec_type = 'C' AND code_id = '003'),'Chamber 3') from dual) where L.DISP_KEY = (select value from label where key = 'Chamber3' and bundle_name = 'CBOGrid') and L.TO_LANG = 'en'
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_DISPLAY_ID,
L.DISP_KEY,
L.DISP_VALUE,
L.TO_LANG)
VALUES (SEQ_LABEL_DISPLAY_ID.NEXTVAL,
(select value from label where key = 'Chamber3' and bundle_name = 'CBOGrid'),
(select nvl((SELECT code_desc FROM sys_code WHERE code_type = 'PRD' AND rec_type = 'C' AND code_id = '003'),'Chamber 3') from dual),
'en');

--Chamber 4, label_display updates
MERGE INTO LABEL_DISPLAY L
USING (SELECT 'en' TO_LANG,
(select value from label where key = 'Chamber4' and bundle_name = 'CBOGrid') DISP_KEY
FROM DUAL) B
ON (L.DISP_KEY = B.DISP_KEY AND L.TO_LANG = B.TO_LANG)
WHEN MATCHED THEN
  update set L.DISP_VALUE = (select nvl((SELECT code_desc FROM sys_code WHERE code_type = 'PRD' AND rec_type = 'C' AND code_id = '004'),'Chamber 4') from dual) where L.DISP_KEY = (select value from label where key = 'Chamber4' and bundle_name = 'CBOGrid') and L.TO_LANG = 'en'
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_DISPLAY_ID,
L.DISP_KEY,
L.DISP_VALUE,
L.TO_LANG)
VALUES (SEQ_LABEL_DISPLAY_ID.NEXTVAL,
(select value from label where key = 'Chamber4' and bundle_name = 'CBOGrid'),
(select nvl((SELECT code_desc FROM sys_code WHERE code_type = 'PRD' AND rec_type = 'C' AND code_id = '004'),'Chamber 4') from dual),
'en');

--Chamber 5, label_display updates
MERGE INTO LABEL_DISPLAY L
USING (SELECT 'en' TO_LANG,
(select value from label where key = 'Chamber5' and bundle_name = 'CBOGrid') DISP_KEY
FROM DUAL) B
ON (L.DISP_KEY = B.DISP_KEY AND L.TO_LANG = B.TO_LANG)
WHEN MATCHED THEN
  update set L.DISP_VALUE = (select nvl((SELECT code_desc FROM sys_code WHERE code_type = 'PRD' AND rec_type = 'C' AND code_id = '005'),'Chamber 5') from dual) where L.DISP_KEY = (select value from label where key = 'Chamber5' and bundle_name = 'CBOGrid') and L.TO_LANG = 'en'
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_DISPLAY_ID,
L.DISP_KEY,
L.DISP_VALUE,
L.TO_LANG)
VALUES (SEQ_LABEL_DISPLAY_ID.NEXTVAL,
(select value from label where key = 'Chamber5' and bundle_name = 'CBOGrid'),
(select nvl((SELECT code_desc FROM sys_code WHERE code_type = 'PRD' AND rec_type = 'C' AND code_id = '005'),'Chamber 5') from dual),
'en');

--Chamber 6, label_display updates
MERGE INTO LABEL_DISPLAY L
USING (SELECT 'en' TO_LANG,
(select value from label where key = 'Chamber6' and bundle_name = 'CBOGrid') DISP_KEY
FROM DUAL) B
ON (L.DISP_KEY = B.DISP_KEY AND L.TO_LANG = B.TO_LANG)
WHEN MATCHED THEN
  update set L.DISP_VALUE = (select nvl((SELECT code_desc FROM sys_code WHERE code_type = 'PRD' AND rec_type = 'C' AND code_id = '006'),'Chamber 6') from dual) where L.DISP_KEY = (select value from label where key = 'Chamber6' and bundle_name = 'CBOGrid') and L.TO_LANG = 'en'
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_DISPLAY_ID,
L.DISP_KEY,
L.DISP_VALUE,
L.TO_LANG)
VALUES (SEQ_LABEL_DISPLAY_ID.NEXTVAL,
(select value from label where key = 'Chamber6' and bundle_name = 'CBOGrid'),
(select nvl((SELECT code_desc FROM sys_code WHERE code_type = 'PRD' AND rec_type = 'C' AND code_id = '006'),'Chamber 6') from dual),
'en');

--Chamber 7, label_display updates
MERGE INTO LABEL_DISPLAY L
USING (SELECT 'en' TO_LANG,
(select value from label where key = 'Chamber7' and bundle_name = 'CBOGrid') DISP_KEY
FROM DUAL) B
ON (L.DISP_KEY = B.DISP_KEY AND L.TO_LANG = B.TO_LANG)
WHEN MATCHED THEN
  update set L.DISP_VALUE = (select nvl((SELECT code_desc FROM sys_code WHERE code_type = 'PRD' AND rec_type = 'C' AND code_id = '007'),'Chamber 7') from dual) where L.DISP_KEY = (select value from label where key = 'Chamber7' and bundle_name = 'CBOGrid') and L.TO_LANG = 'en'
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_DISPLAY_ID,
L.DISP_KEY,
L.DISP_VALUE,
L.TO_LANG)
VALUES (SEQ_LABEL_DISPLAY_ID.NEXTVAL,
(select value from label where key = 'Chamber7' and bundle_name = 'CBOGrid'),
(select nvl((SELECT code_desc FROM sys_code WHERE code_type = 'PRD' AND rec_type = 'C' AND code_id = '007'),'Chamber 7') from dual),
'en');

--Chamber 8, label_display updates
MERGE INTO LABEL_DISPLAY L
USING (SELECT 'en' TO_LANG,
(select value from label where key = 'Chamber8' and bundle_name = 'CBOGrid') DISP_KEY
FROM DUAL) B
ON (L.DISP_KEY = B.DISP_KEY AND L.TO_LANG = B.TO_LANG)
WHEN MATCHED THEN
  update set L.DISP_VALUE = (select nvl((SELECT code_desc FROM sys_code WHERE code_type = 'PRD' AND rec_type = 'C' AND code_id = '008'),'Chamber 8') from dual) where L.DISP_KEY = (select value from label where key = 'Chamber8' and bundle_name = 'CBOGrid') and L.TO_LANG = 'en'
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_DISPLAY_ID,
L.DISP_KEY,
L.DISP_VALUE,
L.TO_LANG)
VALUES (SEQ_LABEL_DISPLAY_ID.NEXTVAL,
(select value from label where key = 'Chamber8' and bundle_name = 'CBOGrid'),
(select nvl((SELECT code_desc FROM sys_code WHERE code_type = 'PRD' AND rec_type = 'C' AND code_id = '008'),'Chamber 8') from dual),
'en');
CREATE OR REPLACE procedure validate_yard_assigned
(
    p_locn_parm_id in locn_wiz_hdr.locn_parm_id%type,
    p_whse in whse_master.whse%type,
    p_cd_master_id in cd_master.cd_master_id%type,
    p_lang_id in VARCHAR2,
    p_user_id in VARCHAR2,
    p_timestamp in date,
    p_validation_passed out boolean
)
as

--$Revision: 8$

type t_dsp_locns is table of locn_hdr.dsp_locn%type;
v_dsp_locns t_dsp_locns;

type t_locn_ids is table of locn_hdr.locn_id%type;
v_locn_ids t_locn_ids;

v_error_msg msg_log.msg%type;
v_msg_log_id_start msg_log.msg_log_id%type;

begin
    p_validation_passed := true;

        select locn_id, dsp_locn
        bulk collect into v_locn_ids, v_dsp_locns
        from locn_wizard_locn_tmp t
        where exists
    (
        select 1 from yard_zone_slot yzs,location ln ,equipment_instance ei  
         where yzs.locn_id = t.locn_id
         and ln.location_objid_pk3 = yzs.yard_zone_slot_id 
         and ln.location_id in (ei.current_location_id,ei.assigned_location_id)
    );

    if v_dsp_locns.count != 0
    then
        p_validation_passed := false;

        v_error_msg := get_msg_info('SYSCONTROL', '1121', p_Lang_Id);

        forall i in 1..v_dsp_locns.count
            insert into msg_log(msg_log_id, module, msg_id, pgm_id, msg,
                ref_code_1, ref_value_1, ref_code_5, ref_value_5,
                create_date_time, mod_date_time, user_id, whse,
                cd_master_id, log_date_time)
            values (msg_log_id_seq.nextval, 'SYSCONTROL', '1121',
                'L-C-W_CRT_MNTN_LOC', replace(v_error_msg, '{0}',
                v_dsp_locns(i)), '98', v_locn_ids(i), '95',
                p_locn_parm_id, p_timestamp, p_timestamp,
                p_user_id, p_whse, p_cd_master_id, p_timestamp);
    end if;
end;
/

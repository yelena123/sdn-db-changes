create or replace procedure manh_calculate_aid_capacities
(
    p_task_capcty_uom   in task_rule_parm.task_capcty_uom%type,
    p_labor_rate        in number,
    p_whse              in facility.whse%type,
    p_facility_id       in facility.facility_id%type,
    p_max_wt            in task_rule_parm.max_wt%type,
    p_max_vol           in task_rule_parm.max_vol%type
)
as
begin
    -- determine wt, vol, 'task capacity', duration for every aid
    -- find wt, vol for carton level allocations
-- check: c++ logic differs from spec; match c++ for now
-- check: add entire carton wt/vol to every aid??? (per c++/spec)
    update tmp_task_creation_selected_aid t
    set (t.wt, t.vol) = 
    (
        select case when l.lpn_facility_status >= 30 then l.weight
            else l.estimated_weight end wt, l.estimated_volume vol
        from lpn l
        where l.inbound_outbound_indicator = 'O'
            and l.tc_company_id = t.cd_master_id
            and l.tc_lpn_id = (case when t.task_cmpl_ref_code = '6'
                then t.task_cmpl_ref_nbr else t.carton_nbr end)
            and l.c_facility_id = p_facility_id
    )
    where t.alloc_invn_code = 7
        and 
        (
            (t.task_cmpl_ref_code = '6' and t.task_cmpl_ref_nbr is not null)
            or (t.task_cmpl_ref_code != '6' and t.carton_nbr is not null) 
        );
    wm_cs_log('Wt/vol Calc 1 ' || sql%rowcount);

    -- find wt, vol for plt level allocations
-- check: cannot understand c++ calc for pallets (it may not have worked)
    update tmp_task_creation_selected_aid t
    set (t.wt, t.vol) = 
    (
            select sum(case when l.lpn_facility_status >= 30 then l.weight
                else l.estimated_weight end) wt, sum(l.estimated_volume) vol
            from lpn l
            where l.inbound_outbound_indicator = 'O'
                and l.tc_company_id = t.cd_master_id
                and l.tc_parent_lpn_id = t.cntr_nbr
                and l.c_facility_id = p_facility_id
    )
    where t.alloc_invn_code = 8 and t.cntr_nbr is not null;
    wm_cs_log('Wt/vol Calc 2 ' || sql%rowcount);

    -- find wt, vol for all other allocations
    merge into tmp_task_creation_selected_aid t
    using
    (
        select t.alloc_invn_dtl_id, ic.unit_weight * t.qty_alloc wt, ic.unit_volume * t.qty_alloc vol
        from tmp_task_creation_selected_aid t
        join item_cbo ic on ic.item_id = t.item_id
        where t.alloc_invn_code not in (7, 8)
    ) iv on (iv.alloc_invn_dtl_id = t.alloc_invn_dtl_id)
    when matched then
    update set t.wt = coalesce(iv.wt, 0), t.vol = coalesce(iv.vol, 0);
    wm_cs_log('Wt/vol Calc 3 ' || sql%rowcount);

    wm_task_split_large_aid(p_max_wt, p_max_vol);

    -- find aid (task_dtl) capacity wrt task_rule_parm config
    -- check: spec doesnt talk about what to do when the sku_id is null
    update tmp_task_creation_selected_aid t
    set (t.capcty) =
    (
        select
        (
           case when p_task_capcty_uom = 'U' then t.qty_alloc
            when p_task_capcty_uom = 'B' and t.alloc_uom = 'B' then
                (
                    case 
                    when t.alloc_uom = 'B' and t.alloc_uom_qty > 0 then
                        t.qty_alloc/t.alloc_uom_qty
                    when t.alloc_uom != 'B' and im.std_bundl_qty > 0 then
                        t.qty_alloc/im.std_bundl_qty 
                    else t.qty_alloc 
                    end
                )
            when p_task_capcty_uom = 'C' and t.alloc_uom = 'C' then 1
            when p_task_capcty_uom = 'C' and t.alloc_uom = 'P' then
                (
                    select count(1) 
                        from lpn l
                        where l.tc_parent_lpn_id = t.cntr_nbr 
                            and l.inbound_outbound_indicator = 'O' 
                            and l.tc_company_id = t.cd_master_id
                            and l.c_facility_id = p_facility_id
                )
            when p_task_capcty_uom = 'C' and t.alloc_uom not in ('C', 'P') then
                (
                    case 
                        when im.std_case_qty > 0 then t.qty_alloc/im.std_case_qty
                        else 1 
                    end
                )            
            when p_task_capcty_uom = 'P' and t.alloc_uom = 'P' then 1
            when p_task_capcty_uom = 'P' and t.alloc_uom != 'P' then
                (
                    case 
                    when im.std_plt_qty > 0 then t.qty_alloc/im.std_plt_qty 
                    else 1 
                    end
                )
            when p_task_capcty_uom = 'K' then 
                (
                    case 
                    when t.alloc_uom = 'K' and t.alloc_uom_qty > 0 then 
                        t.qty_alloc/t.alloc_uom_qty 
                    when t.alloc_uom != 'K' and im.std_pack_qty > 0 then
                        t.qty_alloc/im.std_pack_qty 
                    else t.qty_alloc 
                    end
                )
            when p_task_capcty_uom = 'S' and t.alloc_uom = 'S' then
                (
                    case 
                    when t.alloc_uom = 'S' and t.alloc_uom_qty > 0 then 
                        t.qty_alloc/t.alloc_uom_qty 
                    when t.alloc_uom != 'S' and im.std_sub_pack_qty > 0 then
                        t.qty_alloc/im.std_sub_pack_qty 
                    else t.qty_alloc 
                    end
                )
            else 0
            end
           ) capcty
        from item_quantity im
        where im.item_id = t.item_id 
    )
    where t.item_id is not null;
    wm_cs_log('Capacity calc ' || sql%rowcount);

    -- begin labor_rate, uom determination
    update tmp_task_creation_selected_aid t
    set t.uom =
    (
        case when p_task_capcty_uom = t.alloc_uom then p_task_capcty_uom
            else 'U' end 
    ),
    labor_rate =
    (
        case when p_task_capcty_uom = t.alloc_uom then p_labor_rate
            else coalesce(
            (
                select ilr.labor_rate
                from int_labor_rate ilr
                where ilr.whse = p_whse and ilr.cd_master_id = t.cd_master_id
                    and ilr.invn_need_type = t.invn_need_type and ilr.uom = 'U'
            ), 0)
        end 
    )
    where t.item_id is not null;
    wm_cs_log('Labor rate/uom calc 1 ' || sql%rowcount);

    -- continue to find a labor_rate for those that are still zero using the uom
    -- specified on the aid
    update tmp_task_creation_selected_aid t
    set t.uom = t.alloc_uom, t.labor_rate =
    coalesce(
    (
            select ilr.labor_rate
            from int_labor_rate ilr
            where ilr.whse = p_whse and ilr.cd_master_id = t.cd_master_id
                and ilr.invn_need_type = t.invn_need_type 
                and ilr.uom = t.alloc_uom
    ), 0)
    where t.labor_rate = 0 and t.alloc_uom is not null 
        and t.item_id is not null;
    wm_cs_log('Labor rate/uom calc 2 ' || sql%rowcount);

    -- last attempt: for any more remaining rows, use the rule uom/rate if valid
    if (p_labor_rate > 0 and p_task_capcty_uom is not null)
    then
        update tmp_task_creation_selected_aid t
            set t.uom = p_task_capcty_uom, t.labor_rate = p_labor_rate
        where t.labor_rate = 0 and t.item_id is not null;
        wm_cs_log('Default rate/uom update ' || sql%rowcount);
    end if;

    -- calculate the seconds needed to execute each aid (labor duration)
    update tmp_task_creation_selected_aid t
    set t.xpectd_durtn =
    (
        select
            case 
            when t.uom in ('C', 'P') and t.uom = t.alloc_uom then 
                3600 / t.labor_rate
            when t.uom = 'U' then t.qty_alloc * 3600 / t.labor_rate
            when t.uom = 'C' and im.std_case_qty > 0 then 
                t.qty_alloc * 3600 / (t.labor_rate * im.std_case_qty)
            when t.uom = 'P' and im.std_plt_qty > 0 then 
                t.qty_alloc * 3600 / (t.labor_rate * im.std_plt_qty)
            when t.uom = 'K' and im.std_pack_qty > 0 then 
                t.qty_alloc * 3600 / (t.labor_rate * im.std_pack_qty)
            when t.uom = 'B' and im.std_bundl_qty > 0 then 
                t.qty_alloc * 3600 / (t.labor_rate * im.std_bundl_qty)
            when t.uom = 'S' and im.std_sub_pack_qty > 0 then 
                t.qty_alloc * 3600 / (t.labor_rate * im.std_sub_pack_qty)
            else
                0
            end
        from item_quantity im
        where im.item_id = t.item_id 
    )
    where t.labor_rate > 0 and t.item_id is not null;
    wm_cs_log('Expected duration calc ' || sql%rowcount);
end;
/
show errors;

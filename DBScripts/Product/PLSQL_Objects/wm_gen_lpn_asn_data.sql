create or replace procedure wm_gen_lpn_asn_data
(
    p_invc_batch_nbr    in number,
    p_user_id           in user_profile.user_id%type,
    p_tc_company_id     in facility.tc_company_id%type,
    p_srl_trk_flag      in number
)
as
    v_delv_confirmation_enabled   number(1);
    v_current_dttm                timestamp := current_timestamp;
    v_consolidate_lds             varchar2(1);
begin
    select case when exists
    (
        select 1
        from company_parameter cp
        where cp.tc_company_id = p_tc_company_id
            and upper(cp.param_def_id) = 'DELV_NOTIFICATION_ENABLED'
            and upper(cp.param_value) = 'TRUE'
    ) then 1 else 0 end
    into v_delv_confirmation_enabled
    from dual;

    -- if single code_id is '1', then consolidate by item_id only; when '2',
    -- consolidate by item_id and attrs
    select coalesce(max(iv.max_code_id), '0')
    into v_consolidate_lds
    from
    (
        select max(sc.code_id) max_code_id, count(*) num_code_ids
        from sys_code sc
        where sc.rec_type = 'S' and sc.code_type = '704'
    ) iv
    where iv.max_code_id != '0' and iv.num_code_ids = 1;

    insert into outpt_lpn
    (
        actual_charge, actual_charge_currency, actual_monetary_value,
        actual_volume, addnl_option_charge, addnl_option_charge_currency,
        adf_transmit_flag, auditor_userid, base_charge, base_charge_currency,
        bill_of_lading_number, c_facility_alias_id, cod_amount,
        cod_payment_method, container_size, container_type, created_dttm,
        created_source, created_source_type, declared_monetary_value,
        dist_charge, dist_charge_currency, epc_match_flag, estimated_weight,
        final_dest_facility_alias_id, freight_charge, freight_charge_currency,
        insur_charge, insur_charge_currency, invc_batch_nbr, last_updated_dttm,
        loaded_dttm, loaded_posn, lpn_epc, lpn_nbr_x_of_y, manifest_nbr,
        master_bol_nbr, misc_instr_code_1, misc_instr_code_2, misc_instr_code_3,
        misc_instr_code_4, misc_instr_code_5, misc_num_1, misc_num_2,
        non_inventory_lpn_flag, non_machineable, outpt_lpn_id,
        package_description, packer_userid, pallet_epc, pallet_master_lpn_flag,
        parcel_service_mode, parcel_shipment_nbr, picker_userid,
        pre_bulk_add_opt_chr, pre_bulk_add_opt_chr_currency,
        pre_bulk_base_charge, proc_dttm, proc_stat_code, qty_uom, rate_zone,
        rated_weight, rated_weight_uom, ship_via, shipped_dttm,
        static_route_id, tc_company_id, tc_lpn_id, tc_order_id,
        tc_parent_lpn_id, tc_shipment_id, total_lpn_qty, tracking_nbr,
        alt_tracking_nbr, version_nbr, volume_uom, weight, weight_uom,
        load_sequence, return_tracking_nbr, xref_olpn, tc_purchase_order_id, return_reference_number,
        epi_package_id, epi_shipment_id
    )
    select l.actual_charge, c.currency_name , null actual_monetary_value,
        coalesce(nullif(l.actual_volume, 0), l.estimated_volume), l.addnl_option_charge, 
        l.addnl_option_charge_curr_code,
        null adf_transmit_flag, l.auditor_userid, l.base_charge, l.base_charge_curr_code,
        l.bol_nbr, l.c_facility_alias_id, l.cod_amount,
        l.cod_payment_method, l.container_size, l.container_type, v_current_dttm,
        p_user_id, 1, l.lpn_monetary_value,
        l.dist_charge, l.dist_charge_curr_code, l.epc_match_flag, l.estimated_weight,
        coalesce(l.final_dest_facility_alias_id, l.d_facility_alias_id, o.d_facility_alias_id), 
        l.freight_charge, l.freight_charge_curr_code,
        l.insur_charge, l.insur_charge_curr_code, p_invc_batch_nbr, v_current_dttm,
        l.loaded_dttm, l.loaded_posn, null lpn_epc, l.lpn_nbr_x_of_y,
        case when exists
        (    
            select 1
            from ship_via sv
            where sv.ship_via = l.ship_via
                and exists
                (
                    select 1
                    from carrier_code cc
                    where cc.carrier_id = sv.carrier_id and cc.carrier_type_id = 24
                )
        )then l.epi_manifest_id else l.manifest_nbr end manifest_nbr,
        l.master_bol_nbr, l.misc_instr_code_1, l.misc_instr_code_2, l.misc_instr_code_3,
        l.misc_instr_code_4, l.misc_instr_code_5, l.misc_num_1, l.misc_num_2,
        l.non_inventory_lpn_flag, l.non_machineable, outpt_lpn_id_seq.nextval,
        null package_description, l.packer_userid, null pallet_epc, l.pallet_master_lpn_flag,
        null parcel_service_mode, l.parcel_shipment_nbr, l.picker_userid,
        l.pre_bulk_add_opt_chr, l.pre_bulk_add_opt_chr_curr_code, l.pre_bulk_base_charge,
        v_current_dttm, 0, suq.size_uom, l.rate_zone, l.rated_weight,
        null rated_weight_uom, l.ship_via, l.shipped_dttm,
        l.static_route_id, l.tc_company_id, l.tc_lpn_id, l.tc_order_id,
        t.tc_parent_lpn_id, l.tc_shipment_id, l.total_lpn_qty, l.tracking_nbr,
        l.alt_tracking_nbr, 1 version_nbr, suv.size_uom, l.weight,
        suw.size_uom, l.load_sequence, l.return_tracking_nbr, l.xref_olpn, l.tc_purchase_orders_id, 
        l.return_reference_number, l.epi_package_id, l.epi_shipment_id
    from lpn l
    join tmp_invc_lpn_list t on t.lpn_id = l.lpn_id
    join orders o on o.order_id = l.order_id
    left join size_uom suq on suq.size_uom_id = l.qty_uom_id_base
    left join size_uom suw on suw.size_uom_id = l.weight_uom_id_base
    left join size_uom suv on suv.size_uom_id = l.volume_uom_id_base
    left join currency c on c.currency_code = l.actual_charge_curr_code
    log errors (to_char(sysdate));
    wm_cs_log('outpt_lpn insert ' || sql%rowcount);
	
    merge into lpn l
    using
        ( select lp.lpn_id ,lp.xref_olpn 
          from lpn lp
          join tmp_invc_lpn_list t on t.lpn_id = lp.lpn_id
        ) iv on (iv.lpn_id = l.lpn_id)  
    when matched then
        update set l.xref_olpn =null
        where l.lpn_facility_status = 90
    log errors (to_char(sysdate));
    wm_cs_log('xref_olpn Update ' || sql%rowcount);

    if (v_delv_confirmation_enabled = 1)
    then
        -- for lpns via ltl/tl only
        insert into outpt_delv_notification
        (
            bill_of_lading_number, created_dttm, created_source,
            created_source_type, d_address_1, d_address_2, d_address_3, d_city,
            d_contact, d_country_code, d_county, d_dock_door_id, d_dock_id,
            d_email, d_facility_alias_id, d_facility_id, d_facility_name,
            d_fax_number, d_name, d_phone_number, d_postal_code, d_state_prov,
            error_seq_nbr, invc_batch_nbr, last_updated_dttm, last_updated_source,
            last_updated_source_type, proc_dttm, proc_stat_code,
            sched_dlvry_date, tc_lpn_id, outpt_delv_notification_id
        )
        select l.bol_nbr, v_current_dttm, p_user_id, 1, o.d_address_1,
            o.d_address_2, o.d_address_3, o.d_city, o.d_contact, o.d_country_code,
            o.d_county, o.d_dock_door_id, o.d_dock_id, o.d_email,
            o.d_facility_alias_id, o.d_facility_id, o.d_facility_name,
            o.d_fax_number, o.d_name, o.d_phone_number, o.d_postal_code,
            o.d_state_prov, 0, p_invc_batch_nbr, v_current_dttm, p_user_id,
            1, v_current_dttm, 0, o.delivery_start_dttm, l.tc_lpn_id,
            outpt_delv_notification_id_seq.nextval
        from lpn l
        join tmp_invc_lpn_list t on t.lpn_id = l.lpn_id
        join orders o on o.order_id = l.order_id
        where exists
        (
            select 1
            from ship_via sv
            where sv.ship_via = l.ship_via
                and exists
                (
                    select 1
                    from carrier_code cc
                    where cc.carrier_id = sv.carrier_id and cc.carrier_type_id >= 50
                )
        )
        log errors (to_char(sysdate));
        wm_cs_log('Outpt delv conf insert ' || sql%rowcount);
    end if;

    if (v_consolidate_lds = '1')
    then
        null;/*insert into outpt_lpn_detail
        (
            assort_nbr, business_partner, consumption_priority_dttm, created_dttm,
            created_source, created_source_type, gtin, invc_batch_nbr, inventory_type,
            item_attr_1, item_attr_2, item_attr_3, item_attr_4, item_attr_5, item_id,
            last_updated_dttm, last_updated_source, last_updated_source_type,
            lpn_detail_id, manufactured_dttm, batch_nbr, manufactured_plant,
            cntry_of_orgn, outpt_lpn_detail_id, product_status, proc_dttm, proc_stat_code,
            qty_uom, rec_proc_indic, size_value, tc_company_id, tc_lpn_id, version_nbr,
            distribution_order_dtl_id, item_color, item_color_sfx, item_season,
            item_season_year, item_second_dim, item_size_desc, item_quality, item_style,
            item_style_sfx, size_range_code, size_rel_posn_in_table, vendor_item_nbr,
            minor_order_nbr, minor_po_nbr, item_name, distro_number
        )
        select ld.assort_nbr, oli.business_partner_id, ld.consumption_priority_dttm,
            v_current_dttm, p_user_id, 1, oli.sku_gtin,
            p_invc_batch_nbr, ld.inventory_type, ld.item_attr_1, ld.item_attr_2,
            ld.item_attr_3, ld.item_attr_4, ld.item_attr_5, ld.item_id,
            v_current_dttm, p_user_id, 1,
            ld.lpn_detail_id, ld.manufactured_dttm, ld.batch_nbr, ld.manufactured_plant,
            ld.cntry_of_orgn, outpt_lpn_detail_id_seq.nextval, ld.product_status,
            v_current_dttm, 0, null qty_uom, null rec_proc_indic,
            ld.size_value, ld.tc_company_id, t.tc_lpn_id, 1 version_nbr,
            oli.line_item_id, ic.item_color, ic.item_color_sfx, ic.item_season,
            ic.item_season_year, ic.item_second_dim, ic.item_size_desc, ic.item_quality,
            ic.item_style, ic.item_style_sfx, iw.size_range_code, iw.size_rel_posn_in_table,
            ld.vendor_item_nbr, o.tc_order_id, o.ext_purchase_order minor_po_nbr,
            ic.item_name, o.distro_number
        from lpn_detail ld
        join item_cbo ic on ic.item_id = ld.item_id
        join item_wms iw on iw.item_id = ic.item_id
        join order_line_item oli on oli.line_item_id = ld.distribution_order_dtl_id
        join orders o on o.order_id = oli.order_id and o.is_original_order = 1
        join tmp_invc_lpn_list t on t.lpn_id = ld.lpn_id
        where ld.size_value > 0
        group by oli.line_item_id, ld.item_id
        log errors (to_char(sysdate));
        wm_cs_log('Outpt_lpn_detail insert ' || sql%rowcount); */
    elsif (v_consolidate_lds = '2')
    then
        null;/*insert into outpt_lpn_detail
        (
            assort_nbr, business_partner, consumption_priority_dttm, created_dttm,
            created_source, created_source_type, gtin, invc_batch_nbr, inventory_type,
            item_attr_1, item_attr_2, item_attr_3, item_attr_4, item_attr_5, item_id,
            last_updated_dttm, last_updated_source, last_updated_source_type,
            lpn_detail_id, manufactured_dttm, batch_nbr, manufactured_plant,
            cntry_of_orgn, outpt_lpn_detail_id, product_status, proc_dttm, proc_stat_code,
            qty_uom, rec_proc_indic, size_value, tc_company_id, tc_lpn_id, version_nbr,
            distribution_order_dtl_id, item_color, item_color_sfx, item_season,
            item_season_year, item_second_dim, item_size_desc, item_quality, item_style,
            item_style_sfx, size_range_code, size_rel_posn_in_table, vendor_item_nbr,
            minor_order_nbr, minor_po_nbr, item_name, distro_number
        )
        select ld.assort_nbr, oli.business_partner_id, ld.consumption_priority_dttm,
            v_current_dttm, p_user_id, 1, oli.sku_gtin, p_invc_batch_nbr,
            ld.inventory_type, ld.item_attr_1, ld.item_attr_2,
            ld.item_attr_3, ld.item_attr_4, ld.item_attr_5, ld.item_id,
            v_current_dttm, p_user_id, 1, ld.lpn_detail_id,
            ld.manufactured_dttm, ld.batch_nbr, ld.manufactured_plant,
            ld.cntry_of_orgn, outpt_lpn_detail_id_seq.nextval, ld.product_status,
            v_current_dttm, 0, null qty_uom, null rec_proc_indic,
            ld.size_value, ld.tc_company_id, t.tc_lpn_id, 1 version_nbr,
            oli.line_item_id, ic.item_color, ic.item_color_sfx, ic.item_season,
            ic.item_season_year, ic.item_second_dim, ic.item_size_desc, ic.item_quality,
            ic.item_style, ic.item_style_sfx, iw.size_range_code, iw.size_rel_posn_in_table,
            ld.vendor_item_nbr, o.tc_order_id, o.ext_purchase_order minor_po_nbr,
            ic.item_name, o.distro_number
        from lpn_detail ld
        join item_cbo ic on ic.item_id = ld.item_id
        join item_wms iw on iw.item_id = ic.item_id
        join order_line_item oli on oli.line_item_id = ld.distribution_order_dtl_id
        join orders o on o.order_id = oli.order_id and o.is_original_order = 1
        join tmp_invc_lpn_list t on t.lpn_id = ld.lpn_id
        where ld.size_value > 0
            and exists
            (
                select 1
                from tmp_invc_lpn_list t
                where t.lpn_id = ld.lpn_id
            )
        group by oli.line_item_id, ld.item_id, ld.item_attr_1, ld.item_attr_2,
            ld.item_attr_3, ld.item_attr_4, ld.item_attr_5, ld.product_status,
            ld.inventory_type, ld.batch_nbr, ld.cntry_of_orgn
        log errors (to_char(sysdate));
        wm_cs_log('Outpt_lpn_detail insert ' || sql%rowcount);*/
    else
        -- no consolidation
        insert into outpt_lpn_detail
        (
            assort_nbr, business_partner, consumption_priority_dttm, created_dttm,
            created_source, created_source_type, gtin, invc_batch_nbr, inventory_type,
            item_attr_1, item_attr_2, item_attr_3, item_attr_4, item_attr_5, item_id,
            last_updated_dttm, last_updated_source, last_updated_source_type, lpn_detail_id
            , manufactured_dttm, batch_nbr, manufactured_plant, cntry_of_orgn,
            outpt_lpn_detail_id, product_status, proc_dttm, proc_stat_code, qty_uom,
            rec_proc_indic, size_value, tc_company_id, tc_lpn_id, version_nbr,
            distribution_order_dtl_id, item_color, item_color_sfx, item_season,
            item_season_year, item_second_dim, item_size_desc, item_quality, item_style,
            item_style_sfx, size_range_code, size_rel_posn_in_table, vendor_item_nbr,
            minor_order_nbr, minor_po_nbr, item_name, distro_number , tc_order_line_id 
        )
        select ld.assort_nbr, null, ld.consumption_priority_dttm,
            v_current_dttm, p_user_id, 1, oli.sku_gtin,
            p_invc_batch_nbr, ld.inventory_type, ld.item_attr_1, ld.item_attr_2,
            ld.item_attr_3, ld.item_attr_4, ld.item_attr_5, ld.item_id,
            v_current_dttm, p_user_id, 1,
            ld.lpn_detail_id, ld.manufactured_dttm, ld.batch_nbr, ld.manufactured_plant,
            ld.cntry_of_orgn, outpt_lpn_detail_id_seq.nextval, ld.product_status, v_current_dttm,
            0, suq.size_uom, null rec_proc_indic, ld.size_value,
            ld.tc_company_id, t.tc_lpn_id, 1 version_nbr, ld.distribution_order_dtl_id,
            ic.item_color, ic.item_color_sfx, ic.item_season, ic.item_season_year,
            ic.item_second_dim, ic.item_size_desc, ic.item_quality, ic.item_style,
            ic.item_style_sfx, iw.size_range_code, iw.size_rel_posn_in_table,
            ld.vendor_item_nbr, o.tc_order_id, o.ext_purchase_order minor_po_nbr,
            ic.item_name, o.distro_number , ld.tc_order_line_id 
        from lpn_detail ld
        join item_cbo ic on ic.item_id = ld.item_id
        join item_wms iw on iw.item_id = ic.item_id
        join order_line_item oli on oli.line_item_id = ld.distribution_order_dtl_id
        join orders o on o.order_id = oli.order_id
        join tmp_invc_lpn_list t on t.lpn_id = ld.lpn_id
        left join size_uom suq on suq.size_uom_id = ld.qty_uom_id_base
        where ld.size_value > 0
        log errors (to_char(sysdate));
        wm_cs_log('Outpt_lpn_detail insert ' || sql%rowcount);        
    end if;

    insert into outpt_lpn_catch_weight
    (
        catch_wt, created_dttm, created_source, created_source_type,
        invc_batch_nbr, last_updated_dttm, last_updated_source,
        last_updated_source_type, lpn_detail_id, seq_nbr, tc_lpn_id,
        version_nbr, proc_stat_code, outpt_lpn_catch_weight_id,
        proc_dttm
    )
    select lcw.catch_weight, v_current_dttm, p_user_id, 1, p_invc_batch_nbr,
        v_current_dttm, p_user_id, 1, lcw.lpn_detail_id, lcw.seq_nbr,
        t.tc_lpn_id, 1, 0, outpt_lpn_catch_weight_id_seq.nextval,
        v_current_dttm
    from lpn_catch_weight lcw
    join tmp_invc_lpn_list t on t.lpn_id = lcw.lpn_id
    log errors (to_char(sysdate));
    wm_cs_log('Outpt_lpn_catch_weight insert ' || sql%rowcount);

    if (p_srl_trk_flag = 1)
    then
        insert all
        when (rn = 1)
        then
            into outpt_lpn_serial_nbr
            (
                created_dttm, created_source, created_source_type, invc_batch_nbr,
                last_updated_dttm, last_updated_source, last_updated_source_type,
                lpn_detail_id, outpt_lpn_serial_nbr_id, seq_nbr, srl_nbr, lpn_id,
                version_nbr, proc_dttm, proc_stat_code, quantity
            )
            values
            (
                v_current_dttm, p_user_id, 1, p_invc_batch_nbr, v_current_dttm,
                p_user_id, 1, lpn_detail_id, outpt_lpn_serial_nbr_id_seq.nextval,
                seq_nbr, srl_nbr, lpn_id, 1, v_current_dttm, 0, 1
            )
            log errors (to_char(sysdate))
        when (minor_srl_track_id is not null)
        then
            into outpt_lpn_minor_serial_nbr
            (
                created_dttm, created_source, created_source_type,
                invc_batch_nbr, last_updated_dttm, last_updated_source,
                last_updated_source_type, minor_seq_nbr, minor_srl_nbr,
                seq_nbr, srl_nbr, version_nbr, proc_dttm, proc_stat_code,
                outpt_lpn_minor_serial_nbr_id
            )
            values
            (
                v_current_dttm, p_user_id, 1, p_invc_batch_nbr, v_current_dttm,
                p_user_id, 1, minor_seq_nbr, minor_srl_nbr, seq_nbr, srl_nbr,
                1, v_current_dttm, 0, outpt_lpn_minor_srl_nbr_id_seq.nextval
            )
            log errors into err$_outpt_lpn_minor_serial_nb(to_char(sysdate))
        select snt.lpn_detail_id, snt.seq_nbr, snt.srl_nbr, snt.lpn_id,
            mst.minor_seq_nbr, mst.minor_srl_nbr, mst.minor_srl_track_id,
            row_number() over(partition by snt.srl_nbr_trk_id order by mst.minor_srl_track_id) rn
        from srl_nbr_track snt
        join lpn_detail ld on ld.lpn_id = snt.lpn_id
            and ld.lpn_detail_id = snt.lpn_detail_id
        join item_wms iw on iw.item_id = ld.item_id and iw.srl_nbr_reqd in (1, 2, 4)
        left join minor_srl_track mst on mst.srl_nbr_trk_id = snt.srl_nbr_trk_id
            and iw.minor_srl_nbr_req = 1
        where ld.size_value > 0
            and exists
            (
                select 1
                from tmp_invc_lpn_list t
                where t.lpn_id = snt.lpn_id
            );
        wm_cs_log('Srl nbr and minor srl nbr insert ' || sql%rowcount);
    end if;
end;
/
show errors;
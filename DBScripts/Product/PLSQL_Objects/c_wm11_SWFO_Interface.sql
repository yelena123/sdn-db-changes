create or replace PROCEDURE "C_WM11_SWFO_INTERFACE" 
(
       p_line_item_id                         IN order_line_item.line_item_id%type,
       p_num_cases                            IN NUMBER,
       p_tc_shipment_id                       IN c_load_diagram_summary.tc_shipment_id%type,
       p_tc_order_id                          IN c_load_diagram_item_detail.tc_order_id%TYPE,
       p_user_id                              IN c_load_diagram_item_detail.user_id%TYPE,
       p_rc                                   OUT NUMBER
)
AS
v_overflowCount number(5);
v_ErrMsg VARCHAR2(500);
v_loadDiagramId NUMBER(9);
v_overflowcases number(5);
v_clditRecExists number(1);
v_totalCases  NUMBER(9);
v_totalWgt NUMBER(10,3);
v_totalVol NUMBER(10,3);
v_assignedCases NUMBER(9);
v_loadType NUMBER(9);
v_stType NUMBER(9);
v_prodType VARCHAR2(20);
v_oliStatus varchar2(1);
V_OLINUMCASES number(10);
v_delivery_stops number(10);

begin
	p_rc 			:=0;
	v_overflowCount :=0;
	v_overflowcases :=0;
	v_totalCases  	:=0;
	v_totalWgt 		:=0;
	v_totalVol 		:=0;
	v_assignedCases :=0;
	v_loadType := -1;
	v_stType := -1;
	v_oliNumCases := -1;
  V_PRODTYPE := 'NULL';
  v_delivery_stops :=0;




   BEGIN
		/* select the load_diagram_id from summary table for the shipments ,load diagrammed */
    SELECT load_diagram_id into v_loadDiagramId
		FROM C_LOAD_DIAGRAM_SUMMARY CLS
		WHERE CLS.tc_shipment_id = p_tc_shipment_id;
    exception when others then
	  v_ErrMsg := '(No shipmemt is load diagrammed so exiting...) ' || SQLCODE || SQLERRM;

    if v_loadDiagramId IS NULL then
       return;
    end if;
   end;

	begin
         select is_cancelled into v_oliStatus from ORDER_LINE_ITEM  where Line_Item_Id = p_line_item_id;

                    if v_oliStatus = 1 then
                                  SELECT NVL(SUM(NUM_CASES),0) into v_oliNumCases
                                  FROM C_LOAD_DIAGRAM_ITEM_DETAIL CLD
                                  WHERE CLD.load_diagram_id = v_loadDiagramId
                                  AND CLD.tc_order_id = p_tc_order_id
                                  AND CLD.Line_Item_Id = p_line_item_id;

                                  if v_oliNumCases = 0 then
                                       return;
                                  end if;

                    end if;
                exception when no_data_found then
                 return;
    END;

    begin
      /* Check for any records in overflow location */
  		SELECT count(*) into v_overflowCount
  		FROM C_LOAD_DIAGRAM_SUMMARY CLS,C_LOAD_DIAGRAM_ITEM_DETAIL CLD
  		WHERE CLD.load_diagram_id = v_loadDiagramId
  		AND CLD.tc_order_id = p_tc_order_id
  		AND CLD.Line_Item_Id = p_line_item_id
  		AND cld.trailer_position = '00';

      exception when others then
     	  v_ErrMsg := '(No overflow is found so insert into c_load_diagram_item_detail table ...) ' || SQLCODE || SQLERRM;
			  v_overflowCount :=0;

    END;

    if v_overflowCount > 0 then

			/* If an existing record exists in the overflow location for the item then adjust
       the overflow location quantity accordingly.  If the adjustment requires increasing
       the overflow quantity then the overflow quantity (LOAD_DIAGRAM_ITEM_DETAIL.NUM_CASES)
       is increased.  If the adjustment requires decreasing the overflow quantity then the
       overflow quantity is decreased
       */
       UPDATE c_load_diagram_item_detail cld
       SET cld.num_cases = cld.num_cases + p_num_cases
       WHERE CLD.load_diagram_id = v_loadDiagramId
		   AND CLD.tc_order_id = p_tc_order_id
		   AND CLD.Line_Item_Id = p_line_item_id
		   AND cld.trailer_position = '00';

       /*
       If at any time the adjustment causes the overflow location quantity for the order
       and line number to be equal to 0, the record is deleted from the
       Load_Diagram_Item_Detail_table.
       */
       delete from c_load_diagram_item_detail cld
       where CLD.load_diagram_id = v_loadDiagramId
		   AND CLD.tc_order_id = p_tc_order_id
		   AND CLD.Line_Item_Id = p_line_item_id
		   AND cld.trailer_position = '00'
		   and cld.num_cases = 0;

    ELSE
      /* If an existing record is not found in the overflow location for the item then
        a new record is created in the Load Diagram Item Detail table for the shipment,
        order, and overflow position.
		  */

  	 begin
  	    select nvl(load_type,-1),nvl(storage_type,-1), nvl(product_type, 'DEFAULT') into v_loadType, v_stType,v_prodType from
  	    C_LOAD_DIAGRAM_ITEM_DETAIL CLD
    		WHERE CLD.load_diagram_id = v_loadDiagramId
    		AND CLD.tc_order_id = p_tc_order_id
    		AND CLD.Line_Item_Id = p_line_item_id
  		   AND rownum = 1;
  	 exception when others then
      	      v_ErrMsg := '(No overflow is found so insert into c_load_diagram_item_detail table ...) ' || SQLCODE || SQLERRM;
      END;

      INSERT INTO c_load_diagram_item_detail
      (LOAD_DIAGRAM_ITEM_DETAIL_ID,LOAD_DIAGRAM_ID,TC_ORDER_ID,STOP_NUMBER,LINE_ITEM_ID,ITEM_ID,ITEM_NAME,LOAD_TYPE,STORAGE_TYPE,COMPARTMENT,TRAILER_POSITION,
      NUM_CASES,ALLOCATED_CASES,SHORTED_CASES,VOLUME,WEIGHT,CREATED_DTTM,LAST_UPDATED_DTTM,USER_ID,PRODUCT_TYPE,parent_id)
		  (
      			select
      			seq_load_diagram_item_detl_id.NEXTVAL	    AS LOAD_DIAGRAM_ITEM_DETL_ID,
      			v_loadDiagramId               			      AS LOAD_DIGRAM_ID,
      			p_tc_order_id							                as TC_ORDER_ID,
            final_po.order_loading_seq                AS stop_number,
            p_line_item_id                            AS line_item_id,
            final_po.item_id                          AS ITEM_ID,
            final_po.item_name                        AS ITEM_NAME,
      		  nvl(cept.load_type_id,v_loadType)         AS LOAD_TYPE,
      		  nvl(cept.storage_type_id,v_stType)        AS STORAGE_TYPE,
            '0'                                       AS COMPARTMENT,
            '00'                                      AS trailer_position,
            p_num_cases                               AS NUM_CASES,
            0                                         AS ALLOCATED_CASES,
            0                                         AS SHORTED_CASES,
            p_num_cases * final_po.unit_volume        AS VOLUME,
            p_num_cases * final_po.unit_weight        AS WEIGHT,
            SYSDATE                                   AS CREATED_DTTM,
            SYSDATE                                   AS LAST_UPDATED_DTTM,
            p_user_id                                 AS USER_ID,
	          cept.prod_type_id         AS PRODUCT_TYPE,
            0                          AS Parent_id

			from
         (select inq.TC_ORDER_ID,
						 inq.D_FACILITY_ID,
						 inq.LINE_ITEM_ID,
						 inq.ITEM_NAME,
						 inq.ITEM_ID,
						 inq.ORIG_ORDER_QTY,
						 inq.PRODUCT_TYPE prod_type,
						 inq2.revised_product_type Revised_prod,
						 inq.unit_weight,
						 inq.unit_volume,
						 inq.dsg_equipment_id,
						 inq.order_id,
						 inq.shipment_id,
						 inq.order_loading_seq
					from ( select
          o.tc_order_id,o.d_facility_id,oli.line_item_id,
			    ic.item_name,ic.item_id,oli.orig_order_qty,
			    nvl(iw.prod_catgry, cpt.category_id) product_type,
			    ic.unit_weight,ic.unit_volume,sh.dsg_equipment_id,
			    o.order_id,sh.shipment_id,o.order_loading_seq
	  from item_cbo ic,item_wms  iw,
    orders  o,order_line_item oli,
			SHIPMENT SH,C_PROD_TYPE CPT
	 where  o.tc_shipment_id = p_tc_shipment_id
	   and  o.order_id = oli.order_id
     and  oli.item_id = ic.item_id
	   and oli.item_id = iw.item_id
     AND SH.TC_SHIPMENT_ID = O.TC_SHIPMENT_ID
	   and sh.tc_shipment_id = p_tc_shipment_id
	   and cpt.prod_type = ic.prod_type) inq
					LEFT OUTER JOIN (select cpt.category_id,
										   cficm.orig_product_type,
										   cficm.revised_product_type,
										   cficm.FACILITY_ID
									  FROM c_prod_type               cpt,
										   C_FACILITY_ITEM_CTGRY_MAP cficm
									 WHERE cpt.category_id =
										   cficm.orig_product_type) inq2 ON inq.product_type =
																			inq2.category_id
																		AND inq2.facility_id =
																			inq.d_facility_id ) final_po
			LEFT OUTER JOIN c_prod_type cpt ON cpt.category_id =
											   nvl(final_po.Revised_prod,
												   final_po.prod_type)
			LEFT OUTER JOIN C_EQUIPMENT_PROD_TYPE cept ON cept.prod_type_id =
														  cpt.category_id
													  AND cept.equipment_id =
														  final_po.dsg_equipment_id
			LEFT OUTER JOIN c_storage_type cst ON cept.storage_type_id =
												  cst.storage_type_id
			LEFT OUTER JOIN c_load_type clt ON clt.load_type_id =
											   cept.load_type_id

			WHERE
			   final_po.tc_order_id = p_tc_order_id
			   AND final_po.line_Item_Id = p_line_item_id
			);

    END IF;

		select Sum(OLI.ORIG_ORDER_QTY),Sum(OLI.ORIG_ORDER_QTY*IC.UNIT_WEIGHT),Sum(OLI.ORIG_ORDER_QTY*IC.UNIT_VOLUME) into v_totalCases, v_totalWgt, v_totalVol
		from order_line_item oli,ITEM_CBO IC
		where IC.ITEM_ID = oli.ITEM_ID
		and oli.line_item_id in ( select DISTINCT cldid.line_item_id from c_load_diagram_item_detail cldid where load_diagram_id = v_loadDiagramId) AND 
		OLI.IS_CANCELLED = 0;

		select Sum(NUM_CASES) into v_overflowCases from C_LOAD_DIAGRAM_ITEM_DETAIL where TRAILER_POSITION = '00' and load_diagram_id = v_loadDiagramId;

		v_assignedCases :=  v_totalCases - v_overflowCases;

    /*Retrieve No of stops per shipment*/
    select COUNT(*) into v_delivery_stops
    from STOP_ACTION SA, SHIPMENT S 
    where s.shipment_id=sa.shipment_id and s.tc_shipment_id=P_TC_SHIPMENT_ID and sa.action_type='DL';

		UPDATE C_LOAD_DIAGRAM_SUMMARY CLS
		SET CLS.TOTAL_CASES = v_totalCases,
		CLS.ASSIGNED_CASES = v_assignedCases,
		CLS.TOTAL_VOL = v_totalVol,
		CLS.TOTAL_WGT = V_TOTALWGT,
		CLS.OVERFLOW_CASES = V_OVERFLOWCASES,
    CLS.NUM_STOPS = v_delivery_stops
		WHERE CLS.LOAD_DIAGRAM_ID = V_LOADDIAGRAMID
		AND CLS.TC_SHIPMENT_ID = P_TC_SHIPMENT_ID;


  COMMIT;

exception when others then
	p_rc := -1;
	v_ErrMsg := 'SWFO exception'||SQLERRM;
end C_WM11_SWFO_INTERFACE;
/
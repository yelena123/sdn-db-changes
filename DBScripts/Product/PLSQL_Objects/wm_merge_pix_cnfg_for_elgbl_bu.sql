create or replace procedure wm_merge_pix_cnfg_for_elgbl_bu
(
    p_ucl_user_name     in ucl_user.user_name%type,
    p_facility_id       in facility.facility_id%type,
    p_tran_type         in pix_tran.tran_type%type,
    p_tran_code         in pix_tran.tran_code%type,
    p_actn_code         in pix_tran.actn_code%type,
    pa_bu_cnfg          in out wm_utils.ta_bu_cnfg
)
as
    v_proc_stat_code      pix_tran_code.pix_create_stat_code%type;
    v_cnfg_str            varchar2(32000);
    v_xml_group_attr      varchar2(10);
    v_ins_ref_col_string  varchar2(4000);
    v_sel_ref_col_string  varchar2(4000);
begin
    pa_bu_cnfg.delete();
    for elgbl_bu in
    (
        select business_unit_id
        from wm_eligible_bu_vw
        where user_name = p_ucl_user_name and facility_id = p_facility_id
    )
    loop
        v_proc_stat_code := null;
        v_xml_group_attr := null;
        v_ins_ref_col_string := null;
        v_sel_ref_col_string := null;
        manh_get_pix_config_data(p_tran_type, p_tran_code, p_actn_code, elgbl_bu.business_unit_id,
            v_proc_stat_code, v_xml_group_attr);

        if (v_proc_stat_code = 91)
        then
            -- no pix at this BU's level
            continue;
        end if;

        manh_get_pix_ref_code_col_list(p_tran_type, p_tran_code, p_actn_code, 
            elgbl_bu.business_unit_id, v_ins_ref_col_string, v_sel_ref_col_string);
            
        -- concat pix config
        v_cnfg_str := v_proc_stat_code || '|' || v_xml_group_attr || '|' || v_ins_ref_col_string 
            || '|' || v_sel_ref_col_string;
        if (v_cnfg_str is null)
        then
            continue;
        end if;

        if (pa_bu_cnfg.exists(v_cnfg_str))
        then
            -- append the BU found now
            pa_bu_cnfg(v_cnfg_str) := pa_bu_cnfg(v_cnfg_str) || ',' || elgbl_bu.business_unit_id;
        else
            -- create a new entry for this BU
            pa_bu_cnfg(v_cnfg_str) := elgbl_bu.business_unit_id;
        end if;
    end loop;
end;
/
show errors;

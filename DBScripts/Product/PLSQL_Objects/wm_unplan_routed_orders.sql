create or replace procedure wm_unplan_routed_orders
(
    p_user_id       in user_profile.user_id%type,
    p_cons_run_id   in orders.cons_run_id%type,
    p_order_id      in orders.order_id%type,
    p_order_split_id      in order_split.order_split_id%type,
    p_lock_time_out in number,
    p_debug_flag    in number,
    p_delete_stops    in number default 0 -- Control if we need to delete stop and stop action
)
as
begin

    if coalesce(p_cons_run_id, -1) > 0
    then
  
-- check: agg/orig?
-- todo: contention analysis
        insert into tmp_planned_orders
        (
         order_id, order_split_id
        )
        select o.order_id, coalesce(os.order_split_id, -1)
        from orders o
        left join order_split os on os.order_id = o.order_id and os.order_split_status = 10
        where o.cons_run_id = p_cons_run_id
        and not exists
        (
            select 1
            from lpn l
            where l.order_id = o.order_id 
                and coalesce(l.order_split_id, -1) = coalesce(os.order_split_id, -1)
                and l.lpn_facility_status >= 40
        );
        
    else
       insert into tmp_planned_orders
       (
           order_id, order_split_id
       )
       select o.order_id, coalesce(os.order_split_id, -1)
       from orders o
       left join order_split os on os.order_id = o.order_id and os.order_split_status = 10 and os.order_split_id = p_order_split_id
       where o.order_id = p_order_id
           and not exists
           (
               select 1
               from lpn l
               where l.order_id = o.order_id
                   and coalesce(l.order_split_id, -1) = coalesce(os.order_split_id, -1)
                   and l.lpn_facility_status >= 40
           );
    end if;    
    
    -- we cant determine that the ship_via was only assigned during routing and 
    -- not at any other time; so we nullify it anyway
    update lpn l
    set l.tc_shipment_id = null, l.shipment_id = null, l.plan_load_id = null,
        l.ship_via = null, l.tracking_nbr = null, l.alt_tracking_nbr = null,
        l.last_updated_dttm = sysdate, l.last_updated_source = p_user_id,
        l.last_updated_source_type = 1, l.init_ship_via = null,
        l.distribution_leg_carrier_id = null, l.distribution_leg_mode_id = null,
        l.distribution_lev_svce_level_id = null
    where exists
    (
        select 1
        from tmp_planned_orders t
        where t.order_id = l.order_id 
            and t.order_split_id = coalesce(l.order_split_id, -1)
    );

    insert into tmp_stop_action
    (
        shipment_id, stop_seq, stop_action_seq, static_route_id
    )
    select distinct sao.shipment_id, sao.stop_seq, sao.stop_action_seq, s.static_route_id
    from stop_action_order sao join shipment s on sao.shipment_id = s.shipment_id
    where exists
    (
        select 1
        from tmp_planned_orders t
        where t.order_id = sao.order_id
            and t.order_split_id = coalesce(sao.order_split_id, -1)
    );

   delete from order_movement om
    where exists
    (
        select 1
        from tmp_planned_orders t
        where t.order_id = om.order_id
            and t.order_split_id = coalesce(om.order_split_id, -1)
    );
    
    delete from stop_action_order sao
    where exists
    (
        select 1
        from tmp_planned_orders t
        where t.order_id = sao.order_id
            and t.order_split_id = coalesce(sao.order_split_id, -1)
    );

    delete from stop_action sa
    where exists
        (
            select 1
            from tmp_stop_action t
            where t.shipment_id = sa.shipment_id and t.stop_seq = sa.stop_seq
                and t.stop_action_seq = sa.stop_action_seq
        )
        and not exists
        (
            select 1
            from stop_action_order sao
            where sao.shipment_id = sa.shipment_id
                and sao.stop_seq = sa.stop_seq
                and sao.stop_action_seq = sa.stop_action_seq
        )and p_delete_stops = 1; -- Control deletion based on parameter...

-- check: per Karthik, having holes in stop_seq is not an issue; need to review
    delete from stop s
    where exists
        (
            select 1
            from tmp_stop_action t
            where t.shipment_id = s.shipment_id and t.stop_seq = s.stop_seq
        )
        and not exists
        (
            select 1
            from stop_action sa
            where sa.shipment_id = s.shipment_id and sa.stop_seq = s.stop_seq
        );

    update order_split os
    set os.order_split_status = 5, os.last_updated_dttm = sysdate,
        os.last_updated_source = p_user_id, os.last_updated_source_type = 1,
        os.shipment_id = null, os.assigned_mot_id = null, 
        os.assigned_service_level_id = null, os.assigned_carrier_id = null, 
        os.assigned_equipment_id = null, os.dynamic_routing_reqd = null, 
        os.line_haul_ship_via = null, os.zone_skip_hub_location_id = null, 
        os.distribution_ship_via = null, --os.actual_cost = null, 
        --os.actual_cost_currency_code = null, 
        os.baseline_cost = null,
        os.baseline_cost_currency_code = null, os.original_assigned_ship_via = null,
        os.order_loading_seq = null
    where exists
    (
        select 1
        from tmp_planned_orders t
        where t.order_split_id = coalesce(os.order_split_id, -1)
            and t.order_split_id > 0
    );

-- todo: swap two fields for 2012; override billing
    update orders o
    set o.actual_cost = null, o.actual_cost_currency_code = null, 
        o.assigned_carrier_id = null, o.assigned_mot_id = null, 
        o.assigned_service_level_id = null, o.assigned_equipment_id = null,
        o.baseline_cost = null, o.baseline_cost_currency_code = null, 
        o.distribution_ship_via = null, o.dynamic_routing_reqd = null,
        o.last_updated_dttm = sysdate, o.last_updated_source = p_user_id,
        o.last_updated_source_type = 1, o.line_haul_ship_via = null, 
        o.shipment_id = null, o.tc_shipment_id = null,
        o.zone_skip_hub_location_id = null, o.order_loading_seq = null,
        o.path_id = null, o.path_set_id = null, o.plan_d_facility_alias_id = null,
        o.plan_d_facility_id = null,
        o.order_status =
        (
            select coalesce(min(os.order_split_status), 5)
            from order_split os
            where os.order_id = o.order_id
        )
    where exists
    (
        select 1
        from tmp_planned_orders t
        where t.order_id = o.order_id
-- check: why do we need these nullification updates if the above fields are already tracked only on the split level?
-- so filter below added for unsplit orders only; arun to verify
            --and t.order_split_id < 0
    );

    -- clean up child data for cancelled shpmts
    delete from shipment_accessorial sa
    where exists
        (
            select 1
            from tmp_stop_action t
            where t.shipment_id = sa.shipment_id
        )
        and not exists
        (
            select 1
            from stop_action_order sao
            where sao.shipment_id = sa.shipment_id
        );

    delete from shipment_commodity sc
    where exists
        (
            select 1
            from tmp_stop_action t
            where t.shipment_id = sc.shipment_id
        )
        and not exists
        (
            select 1
            from stop_action_order sao
            where sao.shipment_id = sc.shipment_id
        );

    delete from shipment_size ss
    where exists
        (
            select 1
            from tmp_stop_action t
            where t.shipment_id = ss.shipment_id
        )
        and not exists
        (
            select 1
            from stop_action_order sao
            where sao.shipment_id = ss.shipment_id
        );
        
    update shipment s
    set s.is_cancelled = 1, s.shipment_status = 120, s.o_address = null, 
        s.o_city = null, s.o_country_code = null, s.o_county = null, 
        s.o_facility_id = null, s.o_postal_code = null, s.o_state_prov = null,
        s.d_address = null, s.d_city = null, s.d_country_code = null, 
        s.d_county = null, s.d_facility_id = null, s.d_facility_number = null,
        s.d_postal_code = null, s.d_state_prov = null, s.baseline_cost = null,
        s.baseline_cost_currency_code = null, s.total_cost = null,
        s.linehaul_cost = null, s.num_stops = 0, s.num_docks = 0,
        s.direct_distance = null, s.distance = null,
        s.out_of_route_distance = null, s.estimated_cost = null,
        s.earned_income = null, s.assigned_carrier_id = null, 
        s.assigned_service_level_id = null, s.assigned_mot_id = null, 
        s.assigned_equipment_id = null, s.assigned_ship_via = null,
        s.last_updated_dttm = sysdate, s.last_updated_source = p_user_id,
        s.last_updated_source_type = 1
    where exists
        (
            select 1
            from tmp_stop_action t
            where t.shipment_id = s.shipment_id
        )
        and not exists
        (
            select 1
            from stop_action sa
            where sa.shipment_id = s.shipment_id
        );

-- todo: remove at an order/split level shipment_accessorial, shipment_size for updated shpmts; need to test and see if app will rebuild after SP deletes all data for shipment

-- todo: account for preplanning where there are no lpn's; currently this only accounts for printed to weighed olpns
-- check: will save shpmt in app do all of this anyway? arun to confirm
-- todo: pickup/delv times on stop/shipment? arun to provide details
    merge into shipment s
    using
    (
        select st.shipment_id, count(distinct st.stop_seq) num_stops,
            sum(coalesce(l.weight, l.estimated_weight)) wt,
            sum(coalesce(l.actual_volume, l.estimated_volume)) vol,
            null pickup_start_dttm,
            null pickup_end_dttm, 
            null delivery_start_dttm,
            null delivery_end_dttm
        from stop st
        left join lpn l on l.shipment_id = st.shipment_id
        where exists
        (
            select 1
            from tmp_stop_action t
            where t.shipment_id = st.shipment_id
        )
        group by st.shipment_id
    ) iv on (iv.shipment_id = s.shipment_id)
    when matched then
    update set s.last_updated_source = p_user_id, s.planned_weight = iv.wt,
        s.planned_volume = iv.vol, s.last_updated_dttm = sysdate,
        s.pickup_start_dttm = iv.pickup_start_dttm,
        s.pickup_end_dttm = iv.pickup_end_dttm, 
        s.delivery_start_dttm = iv.delivery_start_dttm,
        s.delivery_end_dttm = iv.delivery_end_dttm,
        s.bill_of_lading_number = (case when iv.num_stops = 2 then null 
            else s.bill_of_lading_number end);

    -- no commit here as app needs to rerate in the same transaction
end;
/

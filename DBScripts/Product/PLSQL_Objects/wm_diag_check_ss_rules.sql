create or replace procedure wm_diag_check_ss_rules
(
    p_ship_wave_parm_id     in ship_wave_parm.ship_wave_parm_id%type,
    p_preview_wave_flag     in number default 0,
    p_ucl_user_name         in ucl_user.user_name%type,
    p_rc                    out number
)
as
    v_error                 msg_log.msg%type;
    v_sql_piece_count       number(2) := 0;
    v_msg_length            number(9) := 0;
    v_msg_log_length        number(5) := 512;
    v_start_pos             number(5) := 0;
    v_str_length            number(5) := 0;
    v_whse                  facility.whse%type;
    v_perf_pickng_wave      ship_wave_parm.perf_pickng_wave%type;
    v_perf_rte              ship_wave_parm.perf_rte%type;
    v_wave_type_indic       wave_parm.wave_type_indic%type;
    v_tc_company_id         wave_parm.tc_company_id%type;
    v_pull_all_swc          wave_parm.pull_all_swc%type;
    v_order_selection_sql   varchar2(32600);
    v_rte_wave_option       opt_param.param_value%type;
    v_facility_id           facility.facility_id%type;
    v_chase_wave       wave_parm.chase_wave%type;
    v_ship_wave_nbr         ship_wave_parm.ship_wave_nbr%type;
begin
    p_rc := 0;
    select swp.whse, wp.tc_company_id, coalesce(wp.wave_type_indic, '1'), 
        coalesce(swp.perf_pickng_wave, 'N'), coalesce(swp.perf_rte, 'N'), 
        coalesce(wp.pull_all_swc, 'N'),chase_wave
    into v_whse, v_tc_company_id, v_wave_type_indic, v_perf_pickng_wave,
        v_perf_rte, v_pull_all_swc,v_chase_wave
    from ship_wave_parm swp
    left join wave_parm wp on wp.wave_parm_id = swp.wave_parm_id
    where swp.ship_wave_parm_id = p_ship_wave_parm_id;

    select f.facility_id
    into v_facility_id
    from facility f
    where f.whse = v_whse;
	
    select coalesce(op.param_value, '1')
    into v_rte_wave_option
    from ship_wave_parm swp
    left join cons_template ct on ct.cons_template_id = swp.rte_wave_parm_id
    left join opt_param op on op.tc_company_id = ct.tc_company_id
        and op.opt_param_list_id = ct.opt_param_list_id
        and op.param_def_id ='routing_run_option'
        and op.param_group_id = 'CON_TEPE'
    -- check: and op.instance_num = ?
    where swp.ship_wave_parm_id = p_ship_wave_parm_id;    

    for rec in
    (
        select wrp.rule_id, rh.rule_type, wrp.rule_name,
            case when exists
            (
                select 1 
                from rule_sel_dtl rsd
                where rsd.rule_id = wrp.rule_id
                    and 
                    (
                        rsd.tbl_name = 'SHIPMENT'
                        or (rsd.tbl_name = 'ORDERS' 
                            and rsd.colm_name in ('SHIPMENT_ID', 'TC_SHIPMENT_ID'))
                    )
            ) then 'Y' else 'N' end is_wave_by_shpmt
        from wave_rule_parm wrp
        join rule_hdr rh on rh.rule_id = wrp.rule_id and rh.rule_type = 'SS'
        where wrp.wave_parm_id = p_ship_wave_parm_id
        and exists 
           (select 1 
            from rule_sel_dtl rsd
            where rsd.rule_id = rh.rule_id)
        order by wrp.rule_prty
    )
    loop
        begin
            manh_wave_build_selection_rule('Y', null, null, v_facility_id,
                v_perf_pickng_wave, v_perf_rte, p_preview_wave_flag,
                v_wave_type_indic, v_tc_company_id, v_pull_all_swc, rec.rule_id,
                rec.rule_type, rec.is_wave_by_shpmt, v_chase_wave,
                v_order_selection_sql, p_diag_mode => 1);

            if (v_tc_company_id is null)
            then
                execute immediate v_order_selection_sql using p_ucl_user_name,v_facility_id,
                    v_facility_id;
            else
                execute immediate v_order_selection_sql using p_ucl_user_name,v_facility_id,
                    v_facility_id, v_tc_company_id;
            end if;
            
            dbms_output.put_line('Rule ' || rec.rule_id || ' checked out ok.');
        exception
            when others then
                v_error :=  substr(sqlerrm, 1, 512);
                p_rc := 1;
                wm_cs_log(v_error, p_ref_value_2 => rec.rule_name, p_sql_log_level => 0 );
                
                v_msg_length := length(v_order_selection_sql);
                v_start_pos := 1;
                v_sql_piece_count := 0;
                while(v_start_pos <= v_msg_length)
                loop
                    v_sql_piece_count := v_sql_piece_count + 1;
                    v_str_length := case when length(substr(v_order_selection_sql, 
                        v_start_pos)) > v_msg_log_length then v_msg_log_length
                        else length(substr(v_order_selection_sql, v_start_pos)) end;
                    wm_cs_log(substr(v_order_selection_sql, v_start_pos, v_str_length),
                        p_ref_value_2 => rec.rule_name || '| chunk ' || v_sql_piece_count,
                        p_sql_log_level => 0);
                    v_start_pos := v_start_pos + v_str_length;
                end loop;                
        end;
    end loop;
end;
/
show errors;

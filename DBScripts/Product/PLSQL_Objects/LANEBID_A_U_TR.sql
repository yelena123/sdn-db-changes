CREATE OR REPLACE TRIGGER LANEBID_A_U_TR
AFTER
UPDATE ON LANEBID
REFERENCING OLD  AS O NEW AS N
FOR EACH ROW
BEGIN
 	LANEBID_A_U_PROC	(1 , :N.LAST_UPDATED_SOURCE ,
	:O.RFPID, :O.LANEID, :O.LANEEQUIPMENTTYPEID, :O.PACKAGEID, :N.ROUND_NUM, :O.OBCARRIERCODEID,
	:O.LANECAPACITY , :N.LANECAPACITY , :O.SURGECAPACITY , :N.SURGECAPACITY , :O.MINIMUMCHARGE ,
	:N.MINIMUMCHARGE , :O.FLATCHARGE , :N.FLATCHARGE , :O.BASERATE , :N.BASERATE ,
	:O.INCLUSIVERATE , :N.INCLUSIVERATE , :O.WEIGHTBREAKCHARGE1 , :N.WEIGHTBREAKCHARGE1 , :O.WEIGHTBREAKCHARGE2 ,
	:N.WEIGHTBREAKCHARGE2 , :O.WEIGHTBREAKCHARGE3 , :N.WEIGHTBREAKCHARGE3 , :O.WEIGHTBREAKCHARGE4 , :N.WEIGHTBREAKCHARGE4 , :O.WEIGHTBREAKCHARGE5 ,
	:N.WEIGHTBREAKCHARGE5 , :O.WEIGHTBREAKCHARGE6 , :N.WEIGHTBREAKCHARGE6 , :O.WEIGHTBREAKCHARGE7 , :N.WEIGHTBREAKCHARGE7 ,
	:O.WEIGHTBREAKCHARGE8 , :N.WEIGHTBREAKCHARGE8 , :O.WEIGHTBREAKCHARGE9 , :N.WEIGHTBREAKCHARGE9 , :O.WEIGHTCAPACITY , :N.WEIGHTCAPACITY ,
	:O.WEIGHTUOM , :N.WEIGHTUOM , :O.RATEPERUOM , :N.RATEPERUOM , :O.FLIGHTFREQUENCY ,
	:N.FLIGHTFREQUENCY , :O.SAILINGALLOCATION , :N.SAILINGALLOCATION , :O.SAILINGFREQUENCY , :N.SAILINGFREQUENCY , :O.CHARGEBASIS ,
	:N.CHARGEBASIS , :O.COMMENTS , :N.COMMENTS , :O.RATEPERSIZE , :N.RATEPERSIZE ,
	:O.RATEPERDISTANCE , :N.RATEPERDISTANCE , :O.RATEDISCOUNT , :N.RATEDISCOUNT ,
	:O.CURRENCYCODE , :N.CURRENCYCODE ,:O.TRANSITTIME , :N.TRANSITTIME ,:O.DAILYCAPACITY , :N.DAILYCAPACITY , :O.CUSTTEXT1 , :N.CUSTTEXT1 , :O.CUSTTEXT2 , :N.CUSTTEXT2 ,
	:O.CUSTTEXT3 , :N.CUSTTEXT3 , :O.CUSTTEXT4 , :N.CUSTTEXT4 , :O.CUSTTEXT5 , :N.CUSTTEXT5 ,
	:O.CUSTTEXT6 , :N.CUSTTEXT6 , :O.CUSTTEXT7 , :N.CUSTTEXT7 , :O.CUSTTEXT8 , :N.CUSTTEXT8 , :O.CUSTTEXT9 ,
	:N.CUSTTEXT9 , :O.CUSTTEXT10 , :N.CUSTTEXT10, :O.CUSTINT1 , :N.CUSTINT1 , :O.CUSTINT2 , :N.CUSTINT2 , :O.CUSTINT3 ,
	:N.CUSTINT3 , :O.CUSTINT4 , :N.CUSTINT4 , :O.CUSTINT5 , :N.CUSTINT5 , :O.CUSTINT6 ,
	:N.CUSTINT6 , :O.CUSTINT7 , :N.CUSTINT7 , :O.CUSTINT8 , :N.CUSTINT8 , :O.CUSTINT9 ,
	:N.CUSTINT9 , :O.CUSTINT10 , :N.CUSTINT10 , :O.CUSTDOUBLE1 , :N.CUSTDOUBLE1 , :O.CUSTDOUBLE2 , :N.CUSTDOUBLE2 , :O.CUSTDOUBLE3 ,
	:N.CUSTDOUBLE3 , :O.CUSTDOUBLE4 , :N.CUSTDOUBLE4 , :O.CUSTDOUBLE5 , :N.CUSTDOUBLE5 , :O.CUSTDOUBLE6 ,
	:N.CUSTDOUBLE6 , :O.CUSTDOUBLE7 , :N.CUSTDOUBLE7 , :O.CUSTDOUBLE8 , :N.CUSTDOUBLE8 , :O.CUSTDOUBLE9 ,
	:N.CUSTDOUBLE9 , :O.CUSTDOUBLE10 , :N.CUSTDOUBLE10 );
END ;
/



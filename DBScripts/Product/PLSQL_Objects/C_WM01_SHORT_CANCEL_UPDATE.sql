create or replace procedure C_WM01_SHORT_CANCEL_UPDATE(
  p_tc_order_id  in c_load_diagram_item_detail.tc_order_id%type,
  p_line_item_id in c_load_diagram_item_detail.line_item_id%type,
  p_item_id      in c_load_diagram_item_detail.item_id%type,
  p_trailer_posn in c_load_diagram_item_detail.trailer_position%type,
  p_qty_to_short in c_load_diagram_item_detail.allocated_cases%type,
  p_fromPacking  in number,
  p_rc           out number) as

v_parent_id		c_load_diagram_item_detail.parent_id%type;
v_cldit_id		c_load_diagram_item_detail.parent_id%type;
v_unitWt 		item_cbo.unit_weight%type; --NUMBER(16,4)
v_unitVol		item_cbo.unit_volume%type; --NUMBER(16,4)
v_remQtyToShort c_load_diagram_item_detail.allocated_cases%type;
v_perOrder VARCHAR2(1);
v_load_diagram_id  number(9);

BEGIN

	p_rc := 0;
	v_parent_id :=0;
	v_cldit_id :=0;
  v_remQtyToShort := p_qty_to_short;
  v_perOrder := 'N';

	-- shorts Flow
	if p_fromPacking = 1 then

		update c_load_diagram_item_detail
		set    allocated_cases = allocated_cases - p_qty_to_short,
			   shorted_cases = shorted_cases + p_qty_to_short
		where  tc_order_id = p_tc_order_id and
			   line_item_id = p_line_item_id and
			   item_id = p_item_id and
			   trailer_position = p_trailer_posn;

		return;
	end if;

  -- Undo Flow
	BEGIN
		select nvl(parent_id,0),nvl(load_diagram_item_detail_id,0),	NVL(CLS.ONE_OLPN_PER_ORDER,'N') PER_ORDER
		into   v_parent_id,v_cldit_id,v_perOrder
		from   c_load_diagram_item_detail cld,C_STORAGE_TYPE CLS
		where  line_item_id = p_line_item_id
		and    trailer_position = p_trailer_posn
	--	and    allocated_cases = p_qty_to_short
		AND CLS.STORAGE_TYPE_ID = CLD.STORAGE_TYPE 
		and rownum = 1;

		EXCEPTION WHEN OTHERS THEN  -- might be Non LD scenario and hence we need to return from here
			v_parent_id := 0;
		return;
	END;

	SELECT 	IC.UNIT_WEIGHT,IC.UNIT_VOLUME INTO v_unitWt,v_unitVol
			FROM ITEM_CBO IC WHERE ITEM_ID = p_item_id;
	-- for Item substitution
	IF (v_parent_id > 0)  THEN
     IF v_perOrder = 'N' THEN
		-- increment the parent id's number cases
		update c_load_diagram_item_detail
		set 	num_cases = num_cases + p_qty_to_short,
				WEIGHT = WEIGHT + (p_qty_to_short * v_unitWt),
				VOLUME = VOLUME + (p_qty_to_short * v_unitVol)
		where  load_diagram_item_detail_id = v_parent_id;

		-- decrement the line item id
		update c_load_diagram_item_detail
		set 	allocated_cases = allocated_cases - p_qty_to_short,
				num_cases = num_cases - p_qty_to_short,
				WEIGHT = WEIGHT - (p_qty_to_short * v_unitWt),
				VOLUME = VOLUME - (p_qty_to_short * v_unitVol)
		where  load_diagram_item_detail_id = v_cldit_id;

       ELSE
          <<INNER_LOOP>>  FOR REC1 In
        		(SELECT TRAILER_POSITION, NUM_CASES, ALLOCATED_CASES, LOAD_DIAGRAM_ITEM_DETAIL_ID,parent_id
        		FROM C_LOAD_DIAGRAM_ITEM_DETAIL  CLDIT
        		WHERE CLDIT.ITEM_ID = p_item_id
        		AND CLDIT.TC_ORDER_ID = p_tc_order_id
        		AND CLDIT.LINE_ITEM_ID = p_line_item_id
        		order by ALLOCATED_CASES DESC)
        	Loop
                update c_load_diagram_item_detail
            		set 	num_cases = num_cases + REC1.NUM_CASES,
            				WEIGHT = WEIGHT + (REC1.NUM_CASES * v_unitWt),
            				VOLUME = VOLUME + (REC1.NUM_CASES * v_unitVol)
            		where  load_diagram_item_detail_id = REC1.parent_id;

                delete from C_LOAD_DIAGRAM_ITEM_DETAIL where  load_diagram_item_detail_id = REC1.LOAD_DIAGRAM_ITEM_DETAIL_ID
                and parent_id is not null and line_item_id = p_line_item_id;

                v_remQtyToShort := v_remQtyToShort - REC1.NUM_CASES;

          End Loop INNER_LOOP;
      END IF;

		-- delete the substituted record
		delete from c_load_diagram_item_detail
		where num_cases <= 0
		and line_item_id = p_line_item_id;

	ELSE -- original OLI

		update c_load_diagram_item_detail
		set    allocated_cases = allocated_cases - p_qty_to_short
		where  load_diagram_item_detail_id = v_cldit_id;
		SELECT DTLL.LOAD_DIAGRAM_ID  into v_load_diagram_id FROM C_LOAD_DIAGRAM_ITEM_DETAIL DTLL WHERE DTLL.LOAD_DIAGRAM_ITEM_DETAIL_ID=v_cldit_id;
			UPDATE C_LOAD_DIAGRAM_SUMMARY SUMM
			SET SUMM.TOTAL_WGT=(SELECT SUM(DTL.WEIGHT) FROM C_LOAD_DIAGRAM_ITEM_DETAIL DTL WHERE DTL.LOAD_DIAGRAM_ID=v_load_diagram_id),
				SUMM.TOTAL_VOL=(SELECT SUM(DTL.VOLUME) FROM C_LOAD_DIAGRAM_ITEM_DETAIL DTL WHERE DTL.LOAD_DIAGRAM_ID=v_load_diagram_id),
				SUMM.TOTAL_CASES=(SELECT SUM(DTL.NUM_CASES) FROM C_LOAD_DIAGRAM_ITEM_DETAIL DTL WHERE DTL.LOAD_DIAGRAM_ID=v_load_diagram_id)
			WHERE SUMM.LOAD_DIAGRAM_ID=v_load_diagram_id ;

	END IF;

--	commit;
	EXCEPTION WHEN OTHERS THEN
		p_rc := -1;
		rollback;
		custom_load_diagram.exception_msg_log_insert('Exception in procedure c_wm01_short_cancel_update: p_line_item_id-->'|| p_line_item_id, 'c_wm01_short_cancel_update', p_line_item_id,'Error');
END c_wm01_short_cancel_update;
/
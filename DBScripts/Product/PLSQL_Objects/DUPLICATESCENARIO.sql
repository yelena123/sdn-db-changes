create or replace
PROCEDURE DUPLICATESCENARIO
 ( I_RFPID INTEGER,
   I_SCENID INTEGER,
   I_SCENIDNEW INTEGER
 ) IS
     fromscenarioroundnum integer;
     toscenarioroundnum integer;
     copiedscenarioid integer;
     l_complex_optimization integer;
     lVolumeoverrideminmax VARCHAR2(10);
     lVolumeoverride NUMBER(14,3);
     lVolumeoverridetype VARCHAR2(10);
     lGlobaladjpct NUMBER(14,3);
     lFacilityadjpct NUMBER(14,3);
     lLaneadjpct NUMBER(14,3);
     lCarrieradjpct NUMBER(14,3);
     lManualawardamount NUMBER(14,3);
	 lRank integer;
     lFound integer;
BEGIN
select round_num into fromscenarioroundnum  from scenario  where rfpid = i_rfpid and scenarioid = i_scenid;
select round_num into toscenarioroundnum from scenario where rfpid = i_rfpid and scenarioid = i_scenidnew;
select complex_optimization into l_complex_optimization  from rfp  where rfpid=i_rfpid  and round_num = fromscenarioroundnum;
if fromscenarioroundnum = toscenarioroundnum then
	copiedscenarioid := i_scenid;
else
	select max(scenarioid) into copiedscenarioid from scenario where rfpid = i_rfpid
    			and round_num = toscenarioroundnum and isbasescenario = 1;
end if;
FOR CURSOR_SCENARIO IN ( select laneequipmenttypeid, obcarriercodeid, i_scenidnew, packageid,
        			rfpid, laneid, historicalaward, equipmenttype_id, servicetype_id,
        			protectionlevel_id, incumbent,  inboundfacilitycode, outboundfacilitycode,inboundcapacityareacode, outboundcapacityareacode,
        			costperload, globaladjpct, facilityadjpct, laneadjpct, carrieradjpct,
        			manualawardamount, autoawardamount, packageawardamount, historicalcost,
        			needsautoaward, lanevolume, capacity, createddttm, numdays,
        			volumeoverrideminmax, volumeoverride, volumeoverridetype, bid_round_num,mot,commoditycode_id,laneincluded,
        			DISCOUNT_CRITERION_ID, rank
    			from scenarioadjustment
    			where rfpid = i_rfpid and scenarioid = copiedscenarioid )
LOOP
insert into scenarioadjustment (laneequipmenttypeid, obcarriercodeid,
				scenarioid, packageid,
				rfpid, laneid,
				historicalaward, equipmenttype_id,
				servicetype_id, protectionlevel_id,mot,
        			incumbent, inboundfacilitycode,
        			outboundfacilitycode, inboundcapacityareacode,
              outboundcapacityareacode ,  costperload,
        			globaladjpct, facilityadjpct,
        			laneadjpct, carrieradjpct,
        			manualawardamount, autoawardamount,
        			packageawardamount,  historicalcost,
        			needsautoaward, lanevolume,
        			capacity, createddttm,
        			numdays, volumeoverrideminmax,
        			volumeoverride, volumeoverridetype,
        			bid_round_num,commoditycode_id,laneincluded,DISCOUNT_CRITERION_ID,rank)
    		       values ( CURSOR_SCENARIO.laneequipmenttypeid, CURSOR_SCENARIO.obcarriercodeid,
    		       		i_scenidnew, CURSOR_SCENARIO.packageid,
        			CURSOR_SCENARIO.rfpid, CURSOR_SCENARIO.laneid,
        			CURSOR_SCENARIO.historicalaward, CURSOR_SCENARIO.equipmenttype_id,
        			CURSOR_SCENARIO.servicetype_id, CURSOR_SCENARIO.protectionlevel_id, CURSOR_SCENARIO.mot,
        			CURSOR_SCENARIO.incumbent, CURSOR_SCENARIO.inboundfacilitycode,
        			CURSOR_SCENARIO.outboundfacilitycode,CURSOR_SCENARIO.inboundcapacityareacode,
              CURSOR_SCENARIO.outboundcapacityareacode, CURSOR_SCENARIO.costperload,
        			CURSOR_SCENARIO.globaladjpct, CURSOR_SCENARIO.facilityadjpct,
        			CURSOR_SCENARIO.laneadjpct, CURSOR_SCENARIO.carrieradjpct,
        			CURSOR_SCENARIO.manualawardamount, CURSOR_SCENARIO.autoawardamount,
        			CURSOR_SCENARIO.packageawardamount,  CURSOR_SCENARIO.historicalcost,
        			CURSOR_SCENARIO.needsautoaward, CURSOR_SCENARIO.lanevolume,
        			CURSOR_SCENARIO.capacity, sysdate,
        			CURSOR_SCENARIO.numdays, CURSOR_SCENARIO.volumeoverrideminmax,
        			CURSOR_SCENARIO.volumeoverride, CURSOR_SCENARIO.volumeoverridetype,
				CURSOR_SCENARIO.bid_round_num,CURSOR_SCENARIO.commoditycode_id,CURSOR_SCENARIO.laneincluded,CURSOR_SCENARIO.DISCOUNT_CRITERION_ID, CURSOR_SCENARIO.rank );
if ( fromscenarioroundnum != toscenarioroundnum )  then
     lVolumeoverrideminmax := null;
     lVolumeoverride := null;
     lVolumeoverridetype := null;
     lGlobaladjpct := null;
     lFacilityadjpct := null;
     lLaneadjpct := null;
     lCarrieradjpct := null;
     lManualawardamount := null;
     lFound := 0;
	 lRank := null;
BEGIN
select  volumeoverrideminmax,  volumeoverride,  volumeoverridetype, globaladjpct,
    facilityadjpct, laneadjpct,  carrieradjpct,  manualawardamount,  1 , rank
into lVolumeoverrideminmax,
     lVolumeoverride,
     lVolumeoverridetype,
     lGlobaladjpct,
     lFacilityadjpct,
     lLaneadjpct,
     lCarrieradjpct,
     lManualawardamount,
     lFound,
	 lRank
from scenarioadjustment
where rfpid=i_rfpid
    and scenarioid = i_scenid
    and laneid = CURSOR_SCENARIO.laneid
    and laneequipmenttypeid=CURSOR_SCENARIO.laneequipmenttypeid
    and obcarriercodeid=CURSOR_SCENARIO.obcarriercodeid
    and historicalaward=CURSOR_SCENARIO.historicalaward
    and packageid=CURSOR_SCENARIO.packageid;
   exception
   	when no_data_found then
	lFound := 0;
	lVolumeoverrideminmax := null;
	lVolumeoverride := null;
	lVolumeoverridetype := null;
	lGlobaladjpct := null;
	lFacilityadjpct := null;
	lLaneadjpct := null;
	lCarrieradjpct := null;
        lManualawardamount := null;
		lRank := null;
		
END;
if (lFound = 1)  then
update scenarioadjustment
    set volumeoverrideminmax=lVolumeoverrideminmax,
    volumeoverride=lVolumeoverride,
    volumeoverridetype=lVolumeoverridetype,
    globaladjpct=lGlobaladjpct,
    facilityadjpct=lFacilityadjpct,
    laneadjpct=lLaneadjpct,
    carrieradjpct=lCarrieradjpct,
    manualawardamount=lManualawardamount,
	rank = lRank
where rfpid=i_rfpid
    and scenarioid=i_scenidnew
    and laneid=CURSOR_SCENARIO.laneid
    and laneequipmenttypeid=CURSOR_SCENARIO.laneequipmenttypeid
    and obcarriercodeid=CURSOR_SCENARIO.obcarriercodeid
    and historicalaward=CURSOR_SCENARIO.historicalaward
    and packageid=CURSOR_SCENARIO.packageid;
end if;
end if;
END LOOP;
insert into scenariorulehistory
	( rfpid, scenarioid, changeseq, filtertext, adj,
	  createduid, createddttm, createdusername )
select    rfpid, i_scenidnew,  changeseq, filtertext, adj,
	  createduid, createddttm,  createdusername
from scenariorulehistory
where rfpid = i_rfpid and scenarioid = i_scenid;
insert  into scenariorule  (
        rfpid,
        scenarioid,
        ruleid,
        facilitycode,
        areacode,
        equipmenttype_id,
        servicetype_id,
        protectionlevel_id,
        mot,
        commoditycode_id,
        incumbent,
        obcarriercodeid,
        ibadj,
        obadj,
        globaladj,
        clause1,
        clause2,
        clause3,
        clause4,
        specreqtype,
        ibvolumeoverrideminmax,
        obvolumeoverrideminmax,
        obvolumeoverride,
        obvolumeoverridetype,
        ibvolumeoverride,
        ibvolumeoverridetype,
        newrule,
        laneadj,
        filterid,
        applieddatetime
    )
select rfpid,  i_scenidnew scenarioid,scenariorule_sequence.nextval ruleid,
    facilitycode,areacode, equipmenttype_id,  servicetype_id,  protectionlevel_id, mot, commoditycode_id,
    incumbent, obcarriercodeid,  ibadj,  obadj,  globaladj,  clause1,
    clause2, clause3,  clause4,  specreqtype,   ibvolumeoverrideminmax,
    obvolumeoverrideminmax,  obvolumeoverride, obvolumeoverridetype, ibvolumeoverride,
    ibvolumeoverridetype,
    case   when copiedscenarioid = i_scenid  or  ( i_scenid is null  and i_scenid is null )  then newrule
    else 1 end newrule,  laneadj,  filterid, applieddatetime
from scenariorule
where rfpid = i_rfpid and scenarioid = i_scenid;
if copiedscenarioid <> i_scenid  then
FOR CURSOR_SCENARIO2 IN (  select carrieradjpct,obcarriercodeid
    				from scenarioadjustment s
    				where s.rfpid = i_rfpid
        				and s.scenarioid = i_scenid
        				and s.obcarriercodeid = obcarriercodeid
        				and carrieradjpct is not null)
LOOP
update scenarioadjustment set   needsautoaward=1,
				carrieradjpct= CURSOR_SCENARIO2.carrieradjpct
	where rfpid=i_rfpid
	and scenarioid=i_scenidnew
	and obcarriercodeid=CURSOR_SCENARIO2.obcarriercodeid;
END LOOP;
end if;
if l_complex_optimization = 1  then
	if copiedscenarioid = i_scenid    then
	 	insert into ob200override_adv  (
        scenarioid,
        override_id,
        enable_flag,
        source,
        level_of_detail,
        type,
        equip_type,
        contract_type,
        movement_type,
        obcarriercodeid,
        carrier_group_id,
        location,
        country_code,
        bid_id,
        min_value,
        max_value,
        slack,
        user_id,
        comments,
        timestamp,
        is_valid,
        additional_criteria,
        rfpid,
        round_num,
        specreqcondition,
        value_type_id
    )
select
    i_scenidnew,
    override_id,
    enable_flag,
    source,
    level_of_detail,
    type,
    equip_type,
    contract_type,
    movement_type,
    obcarriercodeid,
    carrier_group_id,
    location,
    country_code,
    bid_id,
    min_value,
    max_value,
    slack,
    user_id,
    comments,
    timestamp,
    is_valid,
    additional_criteria,
    rfpid,
    round_num,
    specreqcondition,
    value_type_id
from ob200override_adv
where rfpid=i_rfpid
    and scenarioid=i_scenid;
insert into scenario_additional_lanes (rfp_id, lane_id, scenario_id)
	select i_rfpid, lane_id, i_scenidnew from scenario_additional_lanes
		where rfp_id=i_rfpid and scenario_id = i_scenid;
else
insert into ob200override_adv  (
        scenarioid,
        override_id,
        enable_flag,
        source,
        level_of_detail,
        type,
        equip_type,
        contract_type,
        movement_type,
        obcarriercodeid,
        carrier_group_id,
        location,
        country_code,
        bid_id,
        min_value,
        max_value,
        slack,
        user_id,
        comments,
        timestamp,
        is_valid,
        additional_criteria,
        rfpid,
        round_num,
        specreqcondition,
        value_type_id
    )
select
    i_scenidnew,
    override_id,
    enable_flag,
    source,
    level_of_detail,
    type,
    equip_type,
    contract_type,
    movement_type,
    obcarriercodeid,
    carrier_group_id,
    location,
    country_code,
    bid_id,
    min_value,
    max_value,
    slack,
    user_id,
    comments,
    timestamp,
    is_valid,
    additional_criteria,
    rfpid,
    round_num,
    specreqcondition,
    value_type_id
from ob200override_adv
where rfpid=i_rfpid
    and scenarioid=i_scenid and NVL(comments, '-1-')  not like 'SYS_LOCK%' and source <> 'B';
end if;
insert into ob200scenario(
		scenarioid,name,
		status, creation_dt, scheduled_dt,
		raw_cost, wkly_volume_awarded,lock_flag,
		is_valid, rfpid, tc_company_id)
        select
    		distinct s.scenarioid, s.name,
    		s.status, sysdate,  sysdate,
    		s.awardedscenariocost,s.awardedloads,0,
    		1,r.rfpid,r.tccompanyid
        		from scenario s, rfp r where s.rfpid=i_rfpid
        		and s.rfpid=r.rfpid and s.scenarioid=i_scenidnew;
end if;

POPULATE_SCENARIO_CAPACITY (I_RFPID, I_SCENIDNEW);
end DUPLICATESCENARIO;
/
create or replace function wm_val_get_txn_id
return val_result_hist.txn_id%type
as
    v_txn_id val_result_hist.txn_id%type;
begin
    select t.txn_id
    into v_txn_id
    from tmp_val_parm t;
    
    return v_txn_id;
end;
/
show errors;

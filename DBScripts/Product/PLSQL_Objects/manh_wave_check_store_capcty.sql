create or replace procedure manh_wave_check_store_capcty
(
    p_user_id                   in ucl_user.user_name%type,
    p_pull_all_swc              in wave_parm.pull_all_swc%type,
    p_reject_distro_rule        in wave_parm.reject_distro_rule%type,
    p_remaining_store_capcty    in wave_parm.retail_max_stores%type
)
as
    v_rsn_code varchar2(1) default '4';
begin
    -- exclude lines that were a part of selection due to swc; collect soft
    -- alloc lines with stores that exceeded the remaining cap; aside from
    -- these specific lines, collect their a) swcs b) items c) order_ids
    delete from tmp_wave_rejected_lines;
    insert into tmp_wave_rejected_lines
    (
        order_id, line_item_id, item_id, ship_group_id
    )
    select t3.order_id, t3.line_item_id,
        case when p_reject_distro_rule = '2' then t3.item_id 
            else null end item_id,
        case when p_pull_all_swc = 'Y' then t3.ship_group_id
            else null end ship_group_id
    from
    (
        select row_number() over(order by iv.store_group_num) store_index,
            iv.d_facility_id d_facility_id
        from
        (    
            select min(t1.id) store_group_num, t1.d_facility_id
            from tmp_wave_selected_orders t1
            where t1.is_swc_pull = 0 and t1.d_facility_id is not null
                and exists
                (
                    select 1
                    from tmp_ord_dtl_sku_invn t2
                    where t2.order_id = t1.order_id
                        and t2.line_item_id = t1.line_item_id
                )        
            group by t1.d_facility_id
        ) iv
    ) iv2
    join tmp_wave_selected_orders t3 on t3.d_facility_id = iv2.d_facility_id
    where iv2.store_index > p_remaining_store_capcty and t3.is_swc_pull = 0
        and exists
        (
            select 1
            from tmp_ord_dtl_sku_invn t2
            where t2.order_id = t3.order_id
                and t2.line_item_id = t3.line_item_id
        );
    wm_cs_log('Rejected lines captured for num stores check ' || sql%rowcount, p_sql_log_level => 1);

    if (sql%rowcount = 0)
    then
        return;
    end if;

    manh_wave_capcty_rejections(p_user_id, p_pull_all_swc, p_reject_distro_rule,
        v_rsn_code);
end;
/

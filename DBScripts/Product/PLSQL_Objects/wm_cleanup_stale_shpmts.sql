create or replace procedure wm_cleanup_stale_shpmts
(
    p_user_id       in ucl_user.user_name%type,
    pa_shpmt_list   in t_num
)
as
begin
    if (pa_shpmt_list is null)
    then
        return;
    end if;
    
    forall i in 1..pa_shpmt_list.count
    delete from order_movement om
    where om.shipment_id = pa_shpmt_list(i);

    forall i in 1..pa_shpmt_list.count
    delete from shipment_note sn
    where sn.shipment_id = pa_shpmt_list(i);

    forall i in 1..pa_shpmt_list.count
    delete from shipment_accessorial sa
    where sa.shipment_id = pa_shpmt_list(i);

    forall i in 1..pa_shpmt_list.count
    delete from shipment_size ss
    where ss.shipment_id = pa_shpmt_list(i);

    forall i in 1..pa_shpmt_list.count
    delete from shipment_commodity sc
    where sc.shipment_id = pa_shpmt_list(i);

    forall i in 1..pa_shpmt_list.count
    delete from stop_action_order sao
    where sao.shipment_id = pa_shpmt_list(i);

    forall i in 1..pa_shpmt_list.count
    delete from stop_action sa
    where sa.shipment_id = pa_shpmt_list(i);

    forall i in 1..pa_shpmt_list.count
    delete from stop s
    where s.shipment_id = pa_shpmt_list(i);

    forall i in 1..pa_shpmt_list.count
    update shipment s
    set s.is_cancelled = 1, s.shipment_status=120, 
        s.last_updated_source = p_user_id, 
        s.last_updated_dttm = sysdate
    where s.shipment_id = pa_shpmt_list(i);
end;
/
show errors;

create or replace package so_whse_import
is
    procedure import_slot_data_from_wm
    (
        p_whse          in locn_hdr.whse%type,
        p_is_weekly     in number,
        p_user_name     in varchar2,
        p_is_standalone in number,
        p_import_id     in import_offset_master.import_id%type default null,
        p_log_lvl       in number default 0
    );
    
    function get_slot_unit
    (
        p_rtl_uom_mask      in slot_item.slot_unit%type,
        p_slot_unit         in slot_item.slot_unit%type,
        p_item_uom_mask     in slot_item.slot_unit%type,
        p_ship_unit         in slot_item.ship_unit%type
    )
    return slot_item.slot_unit%type;

    function uom_csv_to_mask
    (
        p_uom_csv           in varchar2
    )
    return number;
end;
/
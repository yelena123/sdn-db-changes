CREATE OR REPLACE PROCEDURE SP_POPULATEIMPORTTEMP (v_import_id    NUMBER,
                                                   v_format_id    NUMBER,
                                                   v_whse_code    VARCHAR2)
AS
   v_SQLString          VARCHAR2 (32000);
   v_col_name           VARCHAR2 (32000);
   v_stmnt              VARCHAR2 (32000);
   v_col_part           VARCHAR2 (32000);
   v_value_part         VARCHAR2 (32000);
   v_row_cnt            NUMBER;
   v_row_cntS           NUMBER;
   v_offset_fieldname   VARCHAR2 (32000);
   v_default_value      VARCHAR2 (50);
   v_start_posn         NUMBER (10);
   v_length             NUMBER (10);

   CURSOR TmpColInfo_cursor
   IS
      (SELECT DISTINCT
              (CASE
                  WHEN iom.custom = 0
                  THEN
                     (CASE (iom.offset_fieldname)
                         WHEN 'SKU'
                         THEN
                            'sku_name'
                         WHEN 'SKU Description'
                         THEN
                            'sku_desc'
                         WHEN 'Slot Number'
                         THEN
                            'dsp_slot'
                         WHEN 'Slot Unit'
                         THEN
                            'slot_unit'
                         WHEN 'Ship Unit'
                         THEN
                            'ship_unit'
                         WHEN 'Current Lanes'
                         THEN
                            'rec_lanes'
                         WHEN 'Current Stacking'
                         THEN
                            'rec_stacking'
                         WHEN 'Optimize Plt Pattern'
                         THEN
                            'opt_pallet_pattern'
                         WHEN 'Allow 1 HI Residual'
                         THEN
                            'hi_residual'
                         WHEN 'Orientation'
                         THEN
                            'current_orientation'
                         WHEN 'Ignore for Reslot'
                         THEN
                            'ign_for_reslot'
                         WHEN 'SI Char 1'
                         THEN
                            'info1'
                         WHEN 'SI Char 2'
                         THEN
                            'info2'
                         WHEN 'SI Char 3'
                         THEN
                            'info3'
                         WHEN 'SI Char 4'
                         THEN
                            'info4'
                         WHEN 'SI Char 5'
                         THEN
                            'info5'
                         WHEN 'SI Char 6'
                         THEN
                            'info6'
                         WHEN 'Movement'
                         THEN
                            'movement'
                         WHEN 'Hits'
                         THEN
                            'nbr_of_picks'
                         WHEN 'Inventory'
                         THEN
                            'inventory'
                         WHEN 'Estimated Movement'
                         THEN
                            'est_movement'
                         WHEN 'Estimated Hits'
                         THEN
                            'est_hits'
                         WHEN 'Estimated Inventory'
                         THEN
                            'est_inventory'
                         WHEN 'History Match'
                         THEN
                            'hist_match'
                         WHEN 'UPC'
                         THEN
                            'upc'
                         WHEN 'Commodity Class'
                         THEN
                            'commodity_class'
                         WHEN 'Crushability'
                         THEN
                            'crushability_code'
                         WHEN 'Haz Code'
                         THEN
                            'hazard_code'
                         WHEN 'Vendor Code'
                         THEN
                            'vendor_code'
                         WHEN 'Misc 1'
                         THEN
                            'misc1'
                         WHEN 'Misc 2'
                         THEN
                            'misc2'
                         WHEN 'Misc 3'
                         THEN
                            'misc3'
                         WHEN 'Misc 4'
                         THEN
                            'misc4'
                         WHEN 'Misc 5'
                         THEN
                            'misc5'
                         WHEN 'Misc 6'
                         THEN
                            'misc6'
                         WHEN 'Item status'
                         THEN
                            'item_status'
                         WHEN 'Case height'
                         THEN
                            'case_ht'
                         WHEN 'Case length'
                         THEN
                            'case_len'
                         WHEN 'Case width'
                         THEN
                            'case_wid'
                         WHEN 'Case weight'
                         THEN
                            'case_wt'
                         WHEN 'Each height'
                         THEN
                            'each_ht'
                         WHEN 'Each length'
                         THEN
                            'each_len'
                         WHEN 'Each width'
                         THEN
                            'each_wid'
                         WHEN 'Each weight'
                         THEN
                            'each_wt'
                         WHEN 'Inner height'
                         THEN
                            'inn_ht'
                         WHEN 'Inner length'
                         THEN
                            'inn_len'
                         WHEN 'Inner width'
                         THEN
                            'inn_wid'
                         WHEN 'Inner weight'
                         THEN
                            'inn_wt'
                         WHEN 'Warehouse TI'
                         THEN
                            'wh_ti'
                         WHEN 'Warehouse HI'
                         THEN
                            'wh_hi'
                         WHEN 'Vendor TI'
                         THEN
                            'ven_ti'
                         WHEN 'Vendor HI'
                         THEN
                            'ven_hi'
                         WHEN 'Order TI'
                         THEN
                            'ord_ti'
                         WHEN 'Order HI'
                         THEN
                            'ord_hi'
                         WHEN 'Exp rec dt'
                         THEN
                            'ex_recpt_date'
                         WHEN 'Conveyability'
                         THEN
                            'conveyable'
                         WHEN 'Max stacking'
                         THEN
                            'max_stacking'
                         WHEN 'Max lanes'
                         THEN
                            'max_lanes'
                         WHEN 'Alw ship units'
                         THEN
                            'allow_su'
                         WHEN 'Alw slot units'
                         THEN
                            'allow_slu'
                         WHEN 'Units per bin'
                         THEN
                            'num_units_per_bin'
                         WHEN 'Inner pack'
                         THEN
                            'eaches_per_inner'
                         WHEN 'Vendor pack'
                         THEN
                            'eaches_per_case'
                         WHEN 'Rotate eaches'
                         THEN
                            'allow_rotate_each'
                         WHEN 'Rotate inners'
                         THEN
                            'allow_rotate_inner'
                         WHEN 'Rotate bins'
                         THEN
                            'allow_rotate_bin'
                         WHEN 'Rotate cases'
                         THEN
                            'allow_rotate_case'
                         WHEN 'Use 3D slotting'
                         THEN
                            'use_3d_slot'
                         WHEN 'Nest eaches'
                         THEN
                            'allow_nest_each'
                         WHEN 'Incremental height'
                         THEN
                            'incremental_height'
                         WHEN 'Incremental length'
                         THEN
                            'incremental_length'
                         WHEN 'Incremental width'
                         THEN
                            'incremental_width'
                         WHEN '1'
                         THEN
                            'custom_1'
                         WHEN '2'
                         THEN
                            'custom_2'
                         WHEN '3'
                         THEN
                            'custom_3'
                         WHEN '4'
                         THEN
                            'custom_4'
                         WHEN '5'
                         THEN
                            'custom_5'
                         WHEN '6'
                         THEN
                            'custom_6'
                         WHEN '7'
                         THEN
                            'custom_7'
                         WHEN '8'
                         THEN
                            'custom_8'
                         WHEN '9'
                         THEN
                            'custom_9'
                         WHEN '10'
                         THEN
                            'custom_10'
                         WHEN '11'
                         THEN
                            'custom_11'
                         WHEN '12'
                         THEN
                            'custom_12'
                         WHEN '13'
                         THEN
                            'custom_13'
                         WHEN '14'
                         THEN
                            'custom_14'
                         WHEN '15'
                         THEN
                            'custom_15'
                         WHEN '16'
                         THEN
                            'custom_16'
                         WHEN '17'
                         THEN
                            'custom_17'
                         WHEN '18'
                         THEN
                            'custom_18'
                         WHEN '19'
                         THEN
                            'custom_19'
                         WHEN '20'
                         THEN
                            'custom_20'
                         WHEN '21'
                         THEN
                            'custom_21'
                         WHEN '22'
                         THEN
                            'custom_22'
                         WHEN '23'
                         THEN
                            'custom_23'
                         WHEN '24'
                         THEN
                            'custom_24'
                         WHEN '25'
                         THEN
                            'custom_25'
                         WHEN 'Qty per grab (each)'
                         THEN
                            'qty_per_grab_each'
                         WHEN 'Qty per grab (inner)'
                         THEN
                            'qty_per_grab_inner'
                         WHEN 'Qty per grab (case)'
                         THEN
                            'qty_per_grab_case'
                         WHEN 'Handling attr(each)'
                         THEN
                            'handling_attrib_each'
                         WHEN 'Handling attr(inner)'
                         THEN
                            'handling_attrib_inner'
                         WHEN 'Handling attr(case)'
                         THEN
                            'handling_attrib_case'
                         WHEN 'Item Num 1'
                         THEN
                            'item_num_1'
                         WHEN 'Item Num 2'
                         THEN
                            'item_num_2'
                         WHEN 'Item Num 3'
                         THEN
                            'item_num_3'
                         WHEN 'Item Num 4'
                         THEN
                            'item_num_4'
                         WHEN 'Item Num 5'
                         THEN
                            'item_num_5'
                         WHEN 'Item Char 1'
                         THEN
                            'item_char_1'
                         WHEN 'Item Char 2'
                         THEN
                            'item_char_2'
                         WHEN 'Item Char 3'
                         THEN
                            'item_char_3'
                         WHEN 'Item Char 4'
                         THEN
                            'item_char_4'
                         WHEN 'Item Char 5'
                         THEN
                            'item_char_5'
                         WHEN 'SI Num 1'
                         THEN
                            'si_num_1'
                         WHEN 'SI Num 2'
                         THEN
                            'si_num_2'
                         WHEN 'SI Num 3'
                         THEN
                            'si_num_3'
                         WHEN 'SI Num 4'
                         THEN
                            'si_num_4'
                         WHEN 'SI Num 5'
                         THEN
                            'si_num_5'
                         WHEN 'SI Num 6'
                         THEN
                            'si_num_6'
                         WHEN 'Action code'
                         THEN
                            'action_code'
                         WHEN 'Update type'
                         THEN
                            'update_type'
                         WHEN 'Previous slot'
                         THEN
                            'previous_slot'
                         WHEN 'Max num of slots'
                         THEN
                            'max_slots'
                         WHEN 'Multiple location grp'
                         THEN
                            'mult_loc_grp'
                         WHEN 'Max pallet stacking'
                         THEN
                            'max_pallet_stacking'
                         WHEN 'A-Frame slot allowed'
                         THEN
                            'aframe_allow'
                         WHEN 'A-frame height'
                         THEN
                            'aframe_ht'
                         WHEN 'A-frame length'
                         THEN
                            'aframe_len'
                         WHEN 'A-frame width'
                         THEN
                            'aframe_wid'
                         WHEN 'A-frame weight'
                         THEN
                            'aframe_wt'
                         WHEN 'Replenishment group'
                         THEN
                            'replen_group'
                      END)
                  ELSE
                     (CASE (iom.offset_fieldname)
                         WHEN 'SKU'
                         THEN
                            'sku_name'
                         WHEN 'SKU Description'
                         THEN
                            'sku_desc'
                         WHEN 'Slot Number'
                         THEN
                            'dsp_slot'
                         WHEN 'Slot Unit'
                         THEN
                            'slot_unit'
                         WHEN 'Ship Unit'
                         THEN
                            'ship_unit'
                         WHEN 'Current Lanes'
                         THEN
                            'rec_lanes'
                         WHEN 'Current Stacking'
                         THEN
                            'rec_stacking'
                         WHEN 'Optimize Plt Pattern'
                         THEN
                            'opt_pallet_pattern'
                         WHEN 'Allow 1 HI Residual'
                         THEN
                            'hi_residual'
                         WHEN 'Orientation'
                         THEN
                            'current_orientation'
                         WHEN 'Ignore for Reslot'
                         THEN
                            'ign_for_reslot'
                         WHEN 'SI Char 1'
                         THEN
                            'info1'
                         WHEN 'SI Char 2'
                         THEN
                            'info2'
                         WHEN 'SI Char 3'
                         THEN
                            'info3'
                         WHEN 'SI Char 4'
                         THEN
                            'info4'
                         WHEN 'SI Char 5'
                         THEN
                            'info5'
                         WHEN 'SI Char 6'
                         THEN
                            'info6'
                         WHEN 'Movement'
                         THEN
                            'movement'
                         WHEN 'Hits'
                         THEN
                            'nbr_of_picks'
                         WHEN 'Inventory'
                         THEN
                            'inventory'
                         WHEN 'Estimated Movement'
                         THEN
                            'est_movement'
                         WHEN 'Estimated Hits'
                         THEN
                            'est_hits'
                         WHEN 'Estimated Inventory'
                         THEN
                            'est_inventory'
                         WHEN 'History Match'
                         THEN
                            'hist_match'
                         WHEN 'UPC'
                         THEN
                            'upc'
                         WHEN 'Commodity Class'
                         THEN
                            'commodity_class'
                         WHEN 'Crushability'
                         THEN
                            'crushability_code'
                         WHEN 'Haz Code'
                         THEN
                            'hazard_code'
                         WHEN 'Vendor Code'
                         THEN
                            'vendor_code'
                         WHEN 'Misc 1'
                         THEN
                            'misc1'
                         WHEN 'Misc 2'
                         THEN
                            'misc2'
                         WHEN 'Misc 3'
                         THEN
                            'misc3'
                         WHEN 'Misc 4'
                         THEN
                            'misc4'
                         WHEN 'Misc 5'
                         THEN
                            'misc5'
                         WHEN 'Misc 6'
                         THEN
                            'misc6'
                         WHEN 'Item status'
                         THEN
                            'item_status'
                         WHEN 'Case height'
                         THEN
                            'case_ht'
                         WHEN 'Case length'
                         THEN
                            'case_len'
                         WHEN 'Case width'
                         THEN
                            'case_wid'
                         WHEN 'Case weight'
                         THEN
                            'case_wt'
                         WHEN 'Each height'
                         THEN
                            'each_ht'
                         WHEN 'Each length'
                         THEN
                            'each_len'
                         WHEN 'Each width'
                         THEN
                            'each_wid'
                         WHEN 'Each weight'
                         THEN
                            'each_wt'
                         WHEN 'Inner height'
                         THEN
                            'inn_ht'
                         WHEN 'Inner length'
                         THEN
                            'inn_len'
                         WHEN 'Inner width'
                         THEN
                            'inn_wid'
                         WHEN 'Inner weight'
                         THEN
                            'inn_wt'
                         WHEN 'Warehouse TI'
                         THEN
                            'wh_ti'
                         WHEN 'Warehouse HI'
                         THEN
                            'wh_hi'
                         WHEN 'Vendor TI'
                         THEN
                            'ven_ti'
                         WHEN 'Vendor HI'
                         THEN
                            'ven_hi'
                         WHEN 'Order TI'
                         THEN
                            'ord_ti'
                         WHEN 'Order HI'
                         THEN
                            'ord_hi'
                         WHEN 'Exp rec dt'
                         THEN
                            'ex_recpt_date'
                         WHEN 'Conveyability'
                         THEN
                            'conveyable'
                         WHEN 'Max stacking'
                         THEN
                            'max_stacking'
                         WHEN 'Max lanes'
                         THEN
                            'max_lanes'
                         WHEN 'Alw ship units'
                         THEN
                            'allow_su'
                         WHEN 'Alw slot units'
                         THEN
                            'allow_slu'
                         WHEN 'Units per bin'
                         THEN
                            'num_units_per_bin'
                         WHEN 'Inner pack'
                         THEN
                            'eaches_per_inner'
                         WHEN 'Vendor pack'
                         THEN
                            'eaches_per_case'
                         WHEN 'Rotate eaches'
                         THEN
                            'allow_rotate_each'
                         WHEN 'Rotate inners'
                         THEN
                            'allow_rotate_inner'
                         WHEN 'Rotate bins'
                         THEN
                            'allow_rotate_bin'
                         WHEN 'Rotate cases'
                         THEN
                            'allow_rotate_case'
                         WHEN 'Use 3D slotting'
                         THEN
                            'use_3d_slot'
                         WHEN 'Nest eaches'
                         THEN
                            'allow_nest_each'
                         WHEN 'Incremental height'
                         THEN
                            'incremental_height'
                         WHEN 'Incremental length'
                         THEN
                            'incremental_length'
                         WHEN 'Incremental width'
                         THEN
                            'incremental_width'
                         WHEN '1'
                         THEN
                            'custom_1'
                         WHEN '2'
                         THEN
                            'custom_2'
                         WHEN '3'
                         THEN
                            'custom_3'
                         WHEN '4'
                         THEN
                            'custom_4'
                         WHEN '5'
                         THEN
                            'custom_5'
                         WHEN '6'
                         THEN
                            'custom_6'
                         WHEN '7'
                         THEN
                            'custom_7'
                         WHEN '8'
                         THEN
                            'custom_8'
                         WHEN '9'
                         THEN
                            'custom_9'
                         WHEN '10'
                         THEN
                            'custom_10'
                         WHEN '11'
                         THEN
                            'custom_11'
                         WHEN '12'
                         THEN
                            'custom_12'
                         WHEN '13'
                         THEN
                            'custom_13'
                         WHEN '14'
                         THEN
                            'custom_14'
                         WHEN '15'
                         THEN
                            'custom_15'
                         WHEN '16'
                         THEN
                            'custom_16'
                         WHEN '17'
                         THEN
                            'custom_17'
                         WHEN '18'
                         THEN
                            'custom_18'
                         WHEN '19'
                         THEN
                            'custom_19'
                         WHEN '20'
                         THEN
                            'custom_20'
                         WHEN '21'
                         THEN
                            'custom_21'
                         WHEN '22'
                         THEN
                            'custom_22'
                         WHEN '23'
                         THEN
                            'custom_23'
                         WHEN '24'
                         THEN
                            'custom_24'
                         WHEN '25'
                         THEN
                            'custom_25'
                         WHEN 'Qty per grab (each)'
                         THEN
                            'qty_per_grab_each'
                         WHEN 'Qty per grab (inner)'
                         THEN
                            'qty_per_grab_inner'
                         WHEN 'Qty per grab (case)'
                         THEN
                            'qty_per_grab_case'
                         WHEN 'Handling attr(each)'
                         THEN
                            'handling_attrib_each'
                         WHEN 'Handling attr(inner)'
                         THEN
                            'handling_attrib_inner'
                         WHEN 'Handling attr(case)'
                         THEN
                            'handling_attrib_case'
                         WHEN 'Item Num 1'
                         THEN
                            'item_num_1'
                         WHEN 'Item Num 2'
                         THEN
                            'item_num_2'
                         WHEN 'Item Num 3'
                         THEN
                            'item_num_3'
                         WHEN 'Item Num 4'
                         THEN
                            'item_num_4'
                         WHEN 'Item Num 5'
                         THEN
                            'item_num_5'
                         WHEN 'Item Char 1'
                         THEN
                            'item_char_1'
                         WHEN 'Item Char 2'
                         THEN
                            'item_char_2'
                         WHEN 'Item Char 3'
                         THEN
                            'item_char_3'
                         WHEN 'Item Char 4'
                         THEN
                            'item_char_4'
                         WHEN 'Item Char 5'
                         THEN
                            'item_char_5'
                         WHEN 'SI Num 1'
                         THEN
                            'si_num_1'
                         WHEN 'SI Num 2'
                         THEN
                            'si_num_2'
                         WHEN 'SI Num 3'
                         THEN
                            'si_num_3'
                         WHEN 'SI Num 4'
                         THEN
                            'si_num_4'
                         WHEN 'SI Num 5'
                         THEN
                            'si_num_5'
                         WHEN 'SI Num 6'
                         THEN
                            'si_num_6'
                         WHEN 'Action code'
                         THEN
                            'action_code'
                         WHEN 'Update type'
                         THEN
                            'update_type'
                         WHEN 'Previous slot'
                         THEN
                            'previous_slot'
                         WHEN 'Max num of slots'
                         THEN
                            'max_slots'
                         WHEN 'Multiple location grp'
                         THEN
                            'mult_loc_grp'
                         WHEN 'Max pallet stacking'
                         THEN
                            'max_pallet_stacking'
                         WHEN 'A-Frame slot allowed'
                         THEN
                            'aframe_allow'
                         WHEN 'A-frame height'
                         THEN
                            'aframe_ht'
                         WHEN 'A-frame length'
                         THEN
                            'aframe_len'
                         WHEN 'A-frame width'
                         THEN
                            'aframe_wid'
                         WHEN 'A-frame weight'
                         THEN
                            'aframe_wt'
                         WHEN 'Replenishment group'
                         THEN
                            'replen_group'
                      END)
               END)
                 AS offset_fieldname,
              ifo.start_posn,
              ifo.LENGTH,
              DEFAULT_VALUE
         FROM import_format_offsets ifo, import_offset_master iom
        WHERE     iom.import_id = v_import_id
              AND iom.offset_id = ifo.offset_id
              AND ifo.format_id = v_format_id
              AND iom.offset_type = 2);
BEGIN
   UPDATE staging
      SET whse_code = v_whse_code
    WHERE whse_code IS NULL;

   DELETE FROM staging
         WHERE whse_code = v_whse_code AND LTRIM (line) = '' OR line IS NULL;



   v_col_part := 'INSERT INTO IMPORT_TEMP (whse_code,row_num';
   v_value_part :=
         ' SELECT '''
      || v_whse_code
      || ''''
      || ','
      || 'import_temp_row_num.nextval ';
   v_row_cnt := 0;

   OPEN TmpColInfo_cursor;

   LOOP
      FETCH TmpColInfo_cursor
      INTO v_offset_fieldname, v_start_posn, v_length, v_default_value;

      EXIT WHEN TmpColInfo_cursor%NOTFOUND;

      IF (v_offset_fieldname IS NOT NULL)
      THEN
         v_col_name := ',' || v_offset_fieldname;

         IF (v_start_posn > 0 AND v_length > 0)
         THEN
            v_start_posn := v_start_posn + 1;
         END IF;

         v_stmnt :=
               ','
            || 'nvl(rtrim(substr(line,'
            || v_start_posn
            || ','
            || v_length
            || ')), '''
            || v_default_value
            || ''')';

         v_col_part := v_col_part || v_col_name;
         v_value_part := v_value_part || v_stmnt;
         v_row_cnt := v_row_cnt + 1;
      END IF;
   END LOOP;

   CLOSE TmpColInfo_cursor;

   DBMS_OUTPUT.put_line (v_col_part);
   DBMS_OUTPUT.put_line (v_value_part);

   v_col_part := v_col_part || ') ';
   v_value_part :=
         v_value_part
      || ' FROM STAGING WHERE whse_code = '''
      || v_whse_code
      || '''';

   v_SQLString := v_col_part || v_value_part;


   DELETE FROM IMPORT_TEMP
         WHERE WHSE_CODE = v_whse_code;

   DBMS_OUTPUT.put_line (v_SQLString);

   EXECUTE IMMEDIATE v_SQLString;

   UPDATE import_temp
      SET ex_recpt_date = SYSDATE
    WHERE ex_recpt_date = '01-01-1900';

   DELETE FROM staging
         WHERE whse_code = v_whse_code;

   COMMIT;
END;
/
CREATE OR REPLACE
TRIGGER ILM_APPT_CUST_TRG_B_I_U
	BEFORE INSERT OR UPDATE
	ON ILM_APPT_CUSTOM_ATTRIBUTE    REFERENCING OLD AS OLD NEW AS NEW
	FOR EACH ROW
	DECLARE
		vLAST_UPDATED_SOURCE_TYPE   NUMBER;
		vLAST_UPDATED_SOURCE		VARCHAR2(32)  ;
	BEGIN
        IF INSERTING 
		THEN	
			BEGIN
				SELECT LAST_UPDATED_SOURCE_TYPE, LAST_UPDATED_SOURCE INTO vLAST_UPDATED_SOURCE_TYPE, vLAST_UPDATED_SOURCE
				FROM ILM_APPOINTMENTS 
				WHERE  APPOINTMENT_ID = :NEW.APPOINTMENT_ID ;
				INS_APPOINTMENT_EVENT (	:NEW.APPOINTMENT_ID,
										:NEW.ATTRIBUTE_NAME,
										:NEW.ATTRIBUTE_VALUE,
										:NEW.ATTRIBUTE_VALUE,
										vLAST_UPDATED_SOURCE_TYPE,
										vLAST_UPDATED_SOURCE
									);	
			end;
      END IF;
		IF UPDATING
		THEN
			BEGIN
				SELECT LAST_UPDATED_SOURCE_TYPE, LAST_UPDATED_SOURCE INTO vLAST_UPDATED_SOURCE_TYPE, vLAST_UPDATED_SOURCE
				FROM ILM_APPOINTMENTS 
				WHERE  APPOINTMENT_ID = :OLD.APPOINTMENT_ID ;  
			END;
			
			IF ( 	:NEW.ATTRIBUTE_VALUE IS NULL AND :OLD.ATTRIBUTE_VALUE IS NOT NULL OR
					:NEW.ATTRIBUTE_VALUE IS NOT NULL AND :OLD.ATTRIBUTE_VALUE IS NULL OR
					:NEW.ATTRIBUTE_VALUE <> :OLD.ATTRIBUTE_VALUE
				)
			THEN
				INS_APPOINTMENT_EVENT (	:OLD.APPOINTMENT_ID,
										:OLD.ATTRIBUTE_NAME,
										:OLD.ATTRIBUTE_VALUE,
										:NEW.ATTRIBUTE_VALUE,
										vLAST_UPDATED_SOURCE_TYPE,
										vLAST_UPDATED_SOURCE
									);
			END IF;
		END IF;
END;
/
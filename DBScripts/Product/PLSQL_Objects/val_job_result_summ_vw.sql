create or replace force view val_job_result_summ_vw
(
    flow_name, pgm_id, owner, intrnl_job_name, curr_num_sqls, curr_disabled, val_txn_id, job_txn_id,
    passed, failed, first_sql_end_time, last_sql_end_time
)
as
with curr_job_data as
(
    select vjh.job_name, vjh.val_job_hdr_id, count(*) curr_num_sqls,
        sum(case when vjd.is_locked = 1 or vs.is_locked = 1 then 1 
        else 0 end) curr_disabled
    from val_job_hdr vjh
    join val_job_dtl vjd on vjd.val_job_hdr_id = vjh.val_job_hdr_id
    join val_sql vs on vs.sql_id = vjd.sql_id
    group by vjh.job_name, vjh.val_job_hdr_id
)
select coalesce(vrh.app_hook, vjs.app_hook) flow_name, coalesce(vrh.pgm_id, vjs.pgm_id) pgm_id, 
    vjs.owner, cjd.job_name intrnl_job_name, min(cjd.curr_num_sqls) curr_num_sqls, 
    min(cjd.curr_disabled) curr_disabled, vrh.parent_txn_id val_txn_id, vrh.txn_id job_txn_id, 
    sum(vrh.last_run_passed) passed, 
    sum(decode(vrh.last_run_passed, 0, 1, decode(vrh.parent_txn_id, null, null, 0))) failed, 
    min(vrh.create_date_time) first_sql_end_time, max(vrh.create_date_time) last_sql_end_time
from val_job_seq vjs
join curr_job_data cjd on cjd.job_name = vjs.job_name
join val_job_dtl vjd on vjd.val_job_hdr_id = cjd.val_job_hdr_id
left join val_result_hist vrh on vrh.val_job_dtl_id = vjd.val_job_dtl_id
    and vrh.app_hook = vjs.app_hook and vrh.pgm_id = vjs.pgm_id
group by coalesce(vrh.app_hook, vjs.app_hook), coalesce(vrh.pgm_id, vjs.pgm_id), cjd.job_name, 
    vjs.owner, vrh.txn_id, vrh.parent_txn_id
/

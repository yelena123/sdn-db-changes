DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_columns
    where upper(column_name) = 'PRODUCTION_DATE'
      and upper(table_name) = 'C_ILPN_LOT_MAPPING';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE C_ILPN_LOT_MAPPING  ADD PRODUCTION_DATE DATE';
  end if;
end;
/

--WM62 DB Scripts--
MERGE INTO MESSAGE_MASTER M USING (SELECT '110306201' KEY, 'ErrorMessage' bundle_name FROM DUAL) d ON (m.key = d.key AND m.bundle_name = d.bundle_name)
WHEN NOT MATCHED THEN
INSERT (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME,MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)
VALUES (seq_message_master_id.nextval, '6201', '110306201', 'wm', 'CUST', 'Invalid GTIN', 'ErrorMessage',
'USER', 'WARNING', '', 'N'); 
COMMIT;
/

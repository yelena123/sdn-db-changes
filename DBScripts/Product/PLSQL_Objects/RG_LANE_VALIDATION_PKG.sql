CREATE OR REPLACE
PACKAGE RG_LANE_VALIDATION_PKG
AS

   TYPE ref_curtype IS REF CURSOR;

   PROCEDURE IMPORT_RG_LANES (
      vRootCompanyId          IN company.COMPANY_ID%TYPE,
      vTCCompanyId            IN company.COMPANY_ID%TYPE,
      vBatchId                IN import_rg_lane.BATCH_ID%TYPE,
      vOFacilityAliasId       IN import_rg_lane.O_FACILITY_ALIAS_ID%TYPE,
      vOFacilityBU            IN import_rg_lane.O_FACILITY_BU%TYPE,
      vOCity                  IN import_rg_lane.O_CITY%TYPE,
      vOStateProv             IN import_rg_lane.O_STATE_PROV%TYPE,
      vOCounty                IN import_rg_lane.O_COUNTY%TYPE,
      vOPostal                IN import_rg_lane.O_POSTAL_CODE%TYPE,
      vOCountry               IN import_rg_lane.O_COUNTRY_CODE%TYPE,
      vOZoneId                IN import_rg_lane.O_ZONE_ID%TYPE,
      vOZoneName              IN import_rg_lane.O_ZONE_NAME%TYPE,
      vDFacilityAliasId       IN import_rg_lane.D_FACILITY_ALIAS_ID%TYPE,
      vDFacilityBU            IN import_rg_lane.D_FACILITY_BU%TYPE,
      vDCity                  IN import_rg_lane.D_CITY%TYPE,
      vDStateProv             IN import_rg_lane.D_STATE_PROV%TYPE,
      vDCounty                IN import_rg_lane.D_COUNTY%TYPE,
      vDPostal                IN import_rg_lane.D_POSTAL_CODE%TYPE,
      vDCountry               IN import_rg_lane.D_COUNTRY_CODE%TYPE,
      vDZoneId                IN import_rg_lane.D_ZONE_ID%TYPE,
      vDZoneName              IN import_rg_lane.d_ZONE_NAME%TYPE,
      vRGQualifier            IN import_rg_lane.RG_QUALIFIER%TYPE,
      vFrequency              IN import_rg_lane.FREQUENCY%TYPE,
      vLaneStatus             IN import_rg_lane.LANE_STATUS%TYPE,
      vBusinessUnit           IN import_rg_lane.BUSINESS_UNIT%TYPE,
      vCustomerCode           IN import_rg_lane.CUSTOMER_CODE%TYPE,
      vCustomerCodeBU         IN import_rg_lane.CUSTOMER_CODE_BU%TYPE,
      vBillingMethodCode      IN import_rg_lane.BILLING_METHOD_CODE%TYPE,
      vIncotermName           IN import_rg_lane.INCOTERM_NAME%TYPE,
      vIncotermNameBU         IN import_rg_lane.INCOTERM_NAME_BU%TYPE,
      vRouteTo                IN import_rg_lane.ROUTE_TO%TYPE,
      vRouteType1             IN import_rg_lane.ROUTE_TYPE_1%TYPE,
      vRouteType2             IN import_rg_lane.ROUTE_TYPE_2%TYPE,
      vLaneName               IN import_rg_lane.LANE_NAME%TYPE,
      vNoRating               IN import_rg_lane.NO_RATING%TYPE,
      vSelectFastestCarrier   IN import_rg_lane.USE_FASTEST%TYPE,
      vUsePreferenceBonus     IN import_rg_lane.USE_PREFERENCE_BONUS%TYPE,
	  vUseEpi                 IN import_rg_lane.USE_EPI%TYPE,
	  vEpiServiceGroup        IN import_rg_lane.EPI_SERVICE_GROUP%TYPE);

   PROCEDURE IMPORT_RG_LANE_DETAILS (
      vTCCompanyId             IN company.COMPANY_ID%TYPE,
      vBatchId                 IN import_rg_lane.BATCH_ID%TYPE,
      vRgLaneDtlSeq            IN import_rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      vCarrierCode             IN import_rg_lane_dtl.CARRIER_CODE%TYPE,
      vCarrierCodeBU           IN import_rg_lane_dtl.TP_CODE_BU%TYPE,
      vMot                     IN import_rg_lane_dtl.MOT%TYPE,
      vMotBU                   IN import_rg_lane_dtl.MOT_BU%TYPE,
      vEquipment               IN import_rg_lane_dtl.EQUIPMENT_CODE%TYPE,
      vEquipmentBU             IN import_rg_lane_dtl.EQUIPMENT_BU%TYPE,
      vServiceLevel            IN import_rg_lane_dtl.SERVICE_LEVEL%TYPE,
      vServiceLevelBU          IN import_rg_lane_dtl.SERVICE_LEVEL_BU%TYPE,
      vPackageId               IN import_rg_lane_dtl.PACKAGE_ID%TYPE,
      vPackageName             IN import_rg_lane_dtl.PACKAGE_NAME%TYPE,
      p_scndr_carrier_code     IN IMPORT_RG_LANE_DTL.SCNDR_CARRIER_CODE%TYPE,
      p_scndr_carrier_codeBU   IN IMPORT_RG_LANE_DTL.SECONDARY_CARRIER_CODE_BU%TYPE,
      vProtectionLevel         IN import_rg_lane_dtl.PROTECTION_LEVEL%TYPE,
      vProtectionLevelBU       IN import_rg_lane_dtl.PROTECTION_LEVEL_BU%TYPE,
      vTierId                  IN import_rg_lane_dtl.TIER_ID%TYPE,
      vRank                    IN import_rg_lane_dtl.RANK%TYPE,
      vRepTpFlag               IN import_rg_lane_dtl.REP_TP_FLAG%TYPE,
      vYearlyCapacity          IN import_rg_lane_dtl.YEARLY_CAPACITY%TYPE,
      vMonthlyCapacity         IN import_rg_lane_dtl.MONTHLY_CAPACITY%TYPE,
      vWeeklyCapacity          IN import_rg_lane_dtl.WEEKLY_CAPACITY%TYPE,
      vDailyCapacity           IN import_rg_lane_dtl.Daily_Capacity%TYPE,
      vCapacitySun             IN import_rg_lane_dtl.CAPACITY_SUN%TYPE,
      vCapacityMon             IN import_rg_lane_dtl.CAPACITY_MON%TYPE,
      vCapacityTue             IN import_rg_lane_dtl.CAPACITY_TUE%TYPE,
      vCapacityWed             IN import_rg_lane_dtl.CAPACITY_WED%TYPE,
      vCapacityThu             IN import_rg_lane_dtl.CAPACITY_THU%TYPE,
      vCapacityFri             IN import_rg_lane_dtl.CAPACITY_FRI%TYPE,
      vCapacitySat             IN import_rg_lane_dtl.CAPACITY_SAT%TYPE,
      vYearlyCommitment        IN import_rg_lane_dtl.YEARLY_COMMITMENT%TYPE,
      vMonthlyCommitment       IN import_rg_lane_dtl.MONTHLY_COMMITMENT%TYPE,
      vWeeklyCommitment        IN import_rg_lane_dtl.WEEKLY_COMMITMENT%TYPE,
      vDailyCommitment         IN import_rg_lane_dtl.DAILY_COMMITMENT%TYPE,
      vCommitmentSun           IN import_rg_lane_dtl.COMMITMENT_SUN%TYPE,
      vCommitmentMon           IN import_rg_lane_dtl.COMMITMENT_MON%TYPE,
      vCommitmentTue           IN import_rg_lane_dtl.COMMITMENT_TUE%TYPE,
      vCommitmentWed           IN import_rg_lane_dtl.COMMITMENT_WED%TYPE,
      vCommitmentThu           IN import_rg_lane_dtl.COMMITMENT_THU%TYPE,
      vCommitmentFri           IN import_rg_lane_dtl.COMMITMENT_FRI%TYPE,
      vCommitmentSat           IN import_rg_lane_dtl.COMMITMENT_SAT%TYPE,
      vYearlyCommitmentPct     IN import_rg_lane_dtl.YEARLY_COMMIT_PCT%TYPE,
      vMonthlyCommitmentPct    IN import_rg_lane_dtl.MONTHLY_COMMIT_PCT%TYPE,
      vWeeklyCommitmentPct     IN import_rg_lane_dtl.WEEKLY_COMMIT_PCT%TYPE,
      vDailyCommitmentPct      IN import_rg_lane_dtl.DAILY_COMMIT_PCT%TYPE,
      vCommitPctSun            IN import_rg_lane_dtl.COMMIT_PCT_SUN%TYPE,
      vCommitPctMon            IN import_rg_lane_dtl.COMMIT_PCT_MON%TYPE,
      vCommitPctTue            IN import_rg_lane_dtl.COMMIT_PCT_TUE%TYPE,
      vCommitPctWed            IN import_rg_lane_dtl.COMMIT_PCT_WED%TYPE,
      vCommitPctThu            IN import_rg_lane_dtl.COMMIT_PCT_THU%TYPE,
      vCommitPctFri            IN import_rg_lane_dtl.COMMIT_PCT_FRI%TYPE,
      vCommitPctSat            IN import_rg_lane_dtl.COMMIT_PCT_SAT%TYPE,
      vLastUpdatedSourceType   IN import_rg_lane_dtl.last_updated_source_type%TYPE,
      vLastUpdatedSource       IN import_rg_lane_dtl.last_updated_source%TYPE,
      vIsPreferred             IN import_rg_lane_dtl.IS_PREFERRED%TYPE,
      vEffectiveDT             IN import_rg_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT            IN import_rg_lane_dtl.EXPIRATION_DT%TYPE,
      vLaneDtlStatus           IN import_rg_lane_dtl.IMPORT_RG_DTL_STATUS%TYPE,
      vToleranceFactor         IN import_rg_lane_dtl.TT_TOLERANCE_FACTOR%TYPE,
      vSizeUOM                 IN import_rg_lane_dtl.SIZE_UOM%TYPE,
      vSizeUOMBU               IN import_rg_lane_dtl.SIZE_UOM_BU%TYPE,
      VOVERRIDECODE            IN import_rg_lane_dtl.OVERRIDE_CODE%TYPE,
      VPREFERENCEBONUSVALUE    IN import_rg_lane_dtl.PREFERENCE_BONUS_VALUE%TYPE,
      VCUBINGINDICATOR         IN import_rg_lane_dtl.CUBING_INDICATOR%TYPE,
      VLPNTYPE                 IN import_rg_lane_dtl.SP_LPN_TYPE%TYPE,
      VMINLPNCOUNT             IN import_rg_lane_dtl.SP_MIN_LPN_COUNT%TYPE,
      VMAXLPNCOUNT             IN import_rg_lane_dtl.SP_MAX_LPN_COUNT%TYPE,
      VMINWEIGHT               IN import_rg_lane_dtl.SP_MIN_WEIGHT%TYPE,
      VMAXWEIGHT               IN import_rg_lane_dtl.SP_MAX_WEIGHT%TYPE,
      VMINVOLUME               IN import_rg_lane_dtl.SP_MIN_VOLUME%TYPE,
      VMAXVOLUME               IN import_rg_lane_dtl.SP_MAX_VOLUME%TYPE,
      VMINLENGTH               IN import_rg_lane_dtl.SP_MIN_LINEAR_FEET%TYPE,
      VMAXLENGTH               IN import_rg_lane_dtl.SP_MAX_LINEAR_FEET%TYPE,
      VMINMONETARYVALUE        IN import_rg_lane_dtl.SP_MIN_MONETARY_VALUE%TYPE,
      VMAXMONETARYVALUE        IN import_rg_lane_dtl.SP_MAX_MONETARY_VALUE%TYPE,
      VMVCURRENCYCODE          IN import_rg_lane_dtl.SP_CURRENCY_CODE%TYPE,
	  VVOYAGE				   IN import_rg_lane_dtl.VOYAGE%TYPE);

   PROCEDURE IMPORT_RG_LANE_SURGE (
      vSurgeCapacityId        IN import_surge_capacity.surge_capacity_id%TYPE,
      vBatchId                IN import_rg_lane.batch_id%TYPE,
      vTcCompanyId            IN import_surge_capacity.tc_company_id%TYPE,
      vRGLaneDtlSeq           IN import_surge_capacity.rg_lane_dtl_seq%TYPE,
      vEffectiveDt            IN import_surge_capacity.effective_dt%TYPE,
      vExpirationDt           IN import_surge_capacity.expiration_dt%TYPE,
      vYearlySurgeCapacity    IN import_surge_capacity.yearly_surge_capacity%TYPE,
      vMonthlySurgeCapacity   IN import_surge_capacity.monthly_surge_capacity%TYPE,
      vWeeklySurgeCapacity    IN import_surge_capacity.weekly_surge_capacity%TYPE,
      vDailySurgeCapacity     IN import_surge_capacity.daily_surge_capacity%TYPE,
      vSurgeCapacitySun       IN import_surge_capacity.surge_capacity_sun%TYPE,
      vSurgeCapacityMon       IN import_surge_capacity.surge_capacity_mon%TYPE,
      vSurgeCapacityTue       IN import_surge_capacity.surge_capacity_tue%TYPE,
      vSurgeCapacityWed       IN import_surge_capacity.surge_capacity_wed%TYPE,
      vSurgeCapacityThu       IN import_surge_capacity.surge_capacity_thu%TYPE,
      vSurgeCapacityFri       IN import_surge_capacity.surge_capacity_fri%TYPE,
      vSurgeCapacitySat       IN import_surge_capacity.surge_capacity_sat%TYPE);

   FUNCTION IS_NOT_NUMERIC (pValue IN NUMBER)
      RETURN NUMBER;

   PROCEDURE TRANSFER_IMPORT_LANES (
      vTCCompanyId    IN company.COMPANY_ID%TYPE,
      vFirstBatchId   IN rg_lane.LANE_ID%TYPE,
      vLastBatchId    IN rg_lane.LANE_ID%TYPE);

   PROCEDURE TRANSFER_IMPORT_LANE_DTL (
      vTCCompanyId    IN company.COMPANY_ID%TYPE,
      vImportLaneId   IN rg_lane.LANE_ID%TYPE,
      vLaneId         IN rg_lane.LANE_ID%TYPE);

   PROCEDURE TRANSFER_SURGE_CAPACITY (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vImportLaneId      IN surge_capacity.LANE_ID%TYPE,
      vLaneId            IN surge_capacity.LANE_ID%TYPE,
      vImpRGLaneDtlSeq   IN surge_capacity.RG_LANE_DTL_SEQ%TYPE,
      vRGLaneDtlSeq      IN surge_capacity.RG_LANE_DTL_SEQ%TYPE);

   PROCEDURE TRANSFER_DAILY_UTILIZATION (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vImportLaneId      IN surge_capacity.LANE_ID%TYPE,
      vLaneId            IN surge_capacity.LANE_ID%TYPE,
      vImpRGLaneDtlSeq   IN surge_capacity.RG_LANE_DTL_SEQ%TYPE,
      vRGLaneDtlSeq      IN surge_capacity.RG_LANE_DTL_SEQ%TYPE);

   PROCEDURE VALIDATE_IMPORT_LANES (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vFirstBatchId   IN rg_lane.LANE_ID%TYPE,
      vLastBatchId    IN rg_lane.LANE_ID%TYPE);

   PROCEDURE VALIDATE_LANE (vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
                            vLaneId        IN rg_lane.LANE_ID%TYPE);

   PROCEDURE VALIDATE_LANE_UI (vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
                               vRgLaneId      IN rg_lane.LANE_ID%TYPE);

   PROCEDURE INITIALIZE_LANE_DTL_STATUS (
      vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
      vLaneId        IN rg_lane.LANE_ID%TYPE);

   PROCEDURE VALIDATE_LANE_DTL (vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
                                vLaneId        IN rg_lane.lane_id%TYPE /* S Lal 05/22  */
                                                                      );

   PROCEDURE ADJUST_EFF_EXP_DT (
      vTCCompanyId           IN company.COMPANY_ID%TYPE,
      vLaneId                IN comb_lane.LANE_ID%TYPE,
      vRgDtlSeq              IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vEffectiveDT           IN comb_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN comb_lane_dtl.EXPIRATION_DT%TYPE,
      vCarrierCode           IN comb_lane_dtl.CARRIER_ID%TYPE,
      vMot                   IN comb_lane_dtl.MOT_ID%TYPE,
      vEquipment             IN comb_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN comb_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN comb_lane_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN comb_lane_dtl.PROTECTION_LEVEL_ID%TYPE,
	  vVoyage				 IN comb_lane_dtl.VOYAGE%TYPE);

   PROCEDURE ADJUST_EFF_EXP_DT (
      vTCCompanyId           IN company.COMPANY_ID%TYPE,
      vLaneId                IN comb_lane.LANE_ID%TYPE,
      vRgDtlSeq              IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vRgDtlSeqUpd           IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vEffectiveDT           IN comb_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN comb_lane_dtl.EXPIRATION_DT%TYPE,
      vCarrierCode           IN comb_lane_dtl.CARRIER_ID%TYPE,
      vMot                   IN comb_lane_dtl.MOT_ID%TYPE,
      vEquipment             IN comb_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN comb_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN comb_lane_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN comb_lane_dtl.PROTECTION_LEVEL_ID%TYPE,
	  vVoyage				 IN comb_lane_dtl.VOYAGE%TYPE);

   PROCEDURE COPY_CARRIERS (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                            vLaneId        IN comb_lane.LANE_ID%TYPE,
                            vOldLaneId     IN comb_lane.LANE_ID%TYPE);

   PROCEDURE MERGE_DRAFT (
      vTCCompanyId     IN     company.COMPANY_ID%TYPE,
      vLaneId          IN     comb_lane.LANE_ID%TYPE,
      vLaneDtlSeq      IN     comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vLaneDtlSeqUpd   IN     comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      REC_CNT             OUT NUMBER);

   FUNCTION MERGE_DRAFT (vTCCompanyId     IN company.COMPANY_ID%TYPE,
                         vLaneId          IN comb_lane.LANE_ID%TYPE,
                         vLaneDtlSeq      IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
                         vLaneDtlSeqUpd   IN comb_lane_dtl.LANE_DTL_SEQ%TYPE)
      RETURN INT;

   PROCEDURE UPDATE_ROW (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vLaneId            IN comb_lane.LANE_ID%TYPE,
      vRGDtlSeq          IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vCurrentRGDtlSeq   IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vExpirationDT      IN comb_lane_dtl.EXPIRATION_DT%TYPE);

   PROCEDURE CREATE_NEW_ROW (
      vTCCompanyId    IN company.COMPANY_ID%TYPE,
      vLaneId         IN comb_lane.LANE_ID%TYPE,
      vRGDtlSeq       IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vEffectiveDT    IN comb_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT   IN comb_lane_dtl.EXPIRATION_DT%TYPE);

   PROCEDURE REPLICATE_ROW (
      vTCCompanyId     IN company.COMPANY_ID%TYPE,
      vLaneId          IN comb_lane.LANE_ID%TYPE,
      vOverlappedSeq   IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vRGDtlSeq        IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vEffectiveDT     IN comb_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT    IN comb_lane_dtl.EXPIRATION_DT%TYPE,
      vIsRating        IN comb_lane_dtl.IS_RATING%TYPE,
      vIsRouting       IN comb_lane_dtl.IS_ROUTING%TYPE,
      vIsSailing       IN comb_lane_dtl.IS_SAILING%TYPE);

   PROCEDURE DELETE_CHILD_ROW (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vLaneId        IN comb_lane.LANE_ID%TYPE,
      vRGDtlSeq      IN comb_lane_dtl.LANE_DTL_SEQ%TYPE);

   PROCEDURE COPY_CHILD_ROW (
      vTCCompanyId     IN company.COMPANY_ID%TYPE,
      vLaneId          IN comb_lane.LANE_ID%TYPE,
      vOverlappedSeq   IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vRGDtlSeq        IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vRGDtlSeqNew     IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vIsRating        IN comb_lane_dtl.IS_RATING%TYPE,
      vIsRouting       IN comb_lane_dtl.IS_ROUTING%TYPE,
      vIsSailing       IN comb_lane_dtl.IS_SAILING%TYPE);

   PROCEDURE REPLICATE_LANE_DETAIL (
	  laneDtlId             OUT NUMBER,
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vLaneId        IN comb_lane.LANE_ID%TYPE,
      vRGDtlSeq      IN comb_lane_dtl.LANE_DTL_SEQ%TYPE);

   PROCEDURE INSERT_RG_LANE_ERROR (
      vTCCompanyId         IN company.COMPANY_ID%TYPE,
      vLaneId              IN rg_lane.LANE_ID%TYPE,
      vErrorMsgDesc        IN rg_lane_errors.ERROR_MSG_DESC%TYPE,
      p_error_msg_id       IN RG_LANE_ERRORS.ERROR_MSG_ID%TYPE DEFAULT NULL,
      p_error_param_list   IN RG_LANE_ERRORS.ERROR_PARAM_LIST%TYPE DEFAULT NULL);

   PROCEDURE INSERT_RG_LANE_DTL_ERROR (
      vTCCompanyId         IN company.COMPANY_ID%TYPE,
      vLaneId              IN rg_lane.LANE_ID%TYPE,
      vRGLaneDtlSeq        IN rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      vErrorMsgDesc        IN rg_lane_errors.ERROR_MSG_DESC%TYPE,
      p_error_msg_id       IN RG_LANE_ERRORS.ERROR_MSG_ID%TYPE DEFAULT NULL,
      p_error_param_list   IN RG_LANE_ERRORS.ERROR_PARAM_LIST%TYPE DEFAULT NULL);

   FUNCTION VALIDATE_MULTIPLE_REP_TP_FLAGS (
      vTCCompanyId           IN company.COMPANY_ID%TYPE,
      vLaneId                IN rg_lane.LANE_ID%TYPE,
      vCarrierId             IN carrier_code.CARRIER_ID%TYPE,
      vRepTpFlag             IN rg_lane_dtl.REP_TP_FLAG%TYPE,
      vMode                  IN mot.mot%TYPE,
      p_scndr_carrier_code   IN CARRIER_CODE.CARRIER_CODe%TYPE,
      p_effective_dt         IN RG_LANE_DTL.EFFECTIVE_DT%TYPE,
      p_expiration_dt        IN RG_LANE_DTL.EXPIRATION_DT%TYPE)
      RETURN NUMBER;

   PROCEDURE UPDATE_RG_LANE_ERROR (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vBatchId       IN rg_lane_errors.BATCH_ID%TYPE);

   FUNCTION INSERT_RG_LANE (
      vTCCompanyId            IN company.COMPANY_ID%TYPE,
      vImportLaneId           IN import_rg_lane.LANE_ID%TYPE,
      /* vBusinessUnit    IN business_unit.BUSINESS_UNIT%TYPE,*/
      vLaneHierarchy          IN rg_lane.LANE_HIERARCHY%TYPE,
      vOLocType               IN rg_lane.O_LOC_TYPE%TYPE,
      vOFacilityId            IN facility.FACILITY_ID%TYPE,
      vOFacilityAlias         IN facility_alias.FACILITY_ALIAS_ID%TYPE,
      vOFacilityBU            IN import_rg_lane.O_FACILITY_BU%TYPE,
      vOCity                  IN rg_lane.O_CITY%TYPE,
      vOStateProv             IN state_prov.STATE_PROV%TYPE,
      vOCounty                IN rg_lane.O_COUNTY%TYPE,
      vOPostal                IN postal_code.POSTAL_CODE%TYPE,
      vOCountry               IN country.COUNTRY_CODE%TYPE,
      vOZone                  IN zone.ZONE_ID%TYPE,
      vDLocType               IN rg_lane.D_LOC_TYPE%TYPE,
      vDFacilityId            IN facility.FACILITY_ID%TYPE,
      vDFacilityAlias         IN facility_alias.FACILITY_ALIAS_ID%TYPE,
      vDFacilityBU            IN import_rg_lane.D_FACILITY_BU%TYPE,
      vDCity                  IN rg_lane.D_CITY%TYPE,
      vDStateProv             IN state_prov.STATE_PROV%TYPE,
      vDCounty                IN rg_lane.D_COUNTY%TYPE,
      vDPostal                IN postal_code.POSTAL_CODE%TYPE,
      vDCountry               IN country.COUNTRY_CODE%TYPE,
      vDZone                  IN zone.ZONE_ID%TYPE,
      vRGQualifier            IN rg_lane.RG_QUALIFIER%TYPE,
      vFrequency              IN rg_lane.FREQUENCY%TYPE,
      vCustomerId             IN rg_lane.CUSTOMER_ID%TYPE,
      vBillingMethod          IN rg_lane.BILLING_METHOD%TYPE,
      vIncotermId             IN rg_lane.INCOTERM_ID%TYPE,
      vRouteTo                IN rg_lane.ROUTE_TO%TYPE,
      vRouteType1             IN rg_lane.ROUTE_TYPE_1%TYPE,
      vRouteType2             IN rg_lane.ROUTE_TYPE_2%TYPE,
      vLaneName               IN rg_lane.LANE_NAME%TYPE,
      vNoRating               IN rg_lane.NO_RATING%TYPE,
      vSelectFastestCarrier   IN rg_lane.USE_FASTEST%TYPE,
      vUsePreferenceBonus     IN rg_lane.USE_PREFERENCE_BONUS%TYPE,
	  vUseEpi				  IN rg_lane.USE_EPI%TYPE,
	  vEpiServiceGroup		  IN rg_lane.EPI_SERVICE_GROUP%TYPE,
      vRgLaneId               IN rg_lane.lane_id%TYPE,       /* SL 05/21/02 */
      vBatchId                IN import_rg_lane.BATCH_ID%TYPE /* RC 06/05/2003 */
                                                             )
      RETURN NUMBER;

   FUNCTION INSERT_RG_LANE_DTL (
      vTCCompanyId            IN company.COMPANY_ID%TYPE,
      vRgLaneId               IN rg_lane.LANE_ID%TYPE,
      vCarrierCode            IN rg_lane_dtl.CARRIER_ID%TYPE,
      vMot                    IN rg_lane_dtl.MOT_ID%TYPE,
      vEquipment              IN rg_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel           IN rg_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code    IN RG_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel        IN rg_lane_dtl.PROTECTION_LEVEL_ID%TYPE,
      vTier                   IN rg_lane_dtl.TIER_ID%TYPE,
      vRank                   IN rg_lane_dtl.RANK%TYPE,
      vRepTPFlag              IN rg_lane_dtl.REP_TP_FLAG%TYPE,
      vYearlyCapacity         IN rg_lane_dtl.YEARLY_CAPACITY%TYPE,
      vMonthlyCapacity        IN rg_lane_dtl.MONTHLY_CAPACITY%TYPE,
      vWeeklyCapacity         IN rg_lane_dtl.WEEKLY_CAPACITY%TYPE,
      vDailyCapacity          IN rg_lane_dtl.DAILY_CAPACITY%TYPE,
      vCapacitySun            IN rg_lane_dtl.CAPACITY_SUN%TYPE,
      vCapacityMon            IN rg_lane_dtl.CAPACITY_MON%TYPE,
      vCapacityTue            IN rg_lane_dtl.CAPACITY_TUE%TYPE,
      vCapacityWed            IN rg_lane_dtl.CAPACITY_WED%TYPE,
      vCapacityThu            IN rg_lane_dtl.CAPACITY_THU%TYPE,
      vCapacityFri            IN rg_lane_dtl.CAPACITY_FRI%TYPE,
      vCapacitySat            IN rg_lane_dtl.CAPACITY_SAT%TYPE,
      vYearlyCommitment       IN rg_lane_dtl.YEARLY_COMMITMENT%TYPE,
      vMonthlyCommitment      IN rg_lane_dtl.MONTHLY_COMMITMENT%TYPE,
      vWeeklyCommitment       IN rg_lane_dtl.WEEKLY_COMMITMENT%TYPE,
      vDailyCommitment        IN rg_lane_dtl.DAILY_COMMITMENT%TYPE,
      vCommitmentSun          IN rg_lane_dtl.COMMITMENT_SUN%TYPE,
      vCommitmentMon          IN rg_lane_dtl.COMMITMENT_MON%TYPE,
      vCommitmentTue          IN rg_lane_dtl.COMMITMENT_TUE%TYPE,
      vCommitmentWed          IN rg_lane_dtl.COMMITMENT_WED%TYPE,
      vCommitmentThu          IN rg_lane_dtl.COMMITMENT_THU%TYPE,
      vCommitmentFri          IN rg_lane_dtl.COMMITMENT_FRI%TYPE,
      vCommitmentSat          IN rg_lane_dtl.COMMITMENT_SAT%TYPE,
      vYearlyCommitPct        IN rg_lane_dtl.YEARLY_COMMIT_PCT%TYPE,
      vMonthlyCommitPct       IN rg_lane_dtl.MONTHLY_COMMIT_PCT%TYPE,
      vWeeklyCommitPct        IN rg_lane_dtl.WEEKLY_COMMIT_PCT%TYPE,
      vDailyCommitPct         IN rg_lane_dtl.DAILY_COMMIT_PCT%TYPE,
      vCommitPctSun           IN rg_lane_dtl.COMMIT_PCT_SUN%TYPE,
      vCommitPctMon           IN rg_lane_dtl.COMMIT_PCT_MON%TYPE,
      vCommitPctTue           IN rg_lane_dtl.COMMIT_PCT_TUE%TYPE,
      vCommitPctWed           IN rg_lane_dtl.COMMIT_PCT_WED%TYPE,
      vCommitPctThu           IN rg_lane_dtl.COMMIT_PCT_THU%TYPE,
      vCommitPctFri           IN rg_lane_dtl.COMMIT_PCT_FRI%TYPE,
      vCommitPctSat           IN rg_lane_dtl.COMMIT_PCT_SAT%TYPE,
      vIsPreferred            IN rg_lane_dtl.IS_PREFERRED%TYPE,
      vEffectiveDT            IN rg_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT           IN rg_lane_dtl.EXPIRATION_DT%TYPE,
      vImpRGDtlStatus         IN import_rg_lane_dtl.IMPORT_RG_DTL_STATUS%TYPE,
      vToleranceFactor        IN rg_lane_dtl.TT_TOLERANCE_FACTOR%TYPE,
      vPackageId              IN import_rg_lane_dtl.PACKAGE_ID%TYPE,
      vPackageName            IN rg_lane_dtl.PACKAGE_NAME%TYPE,
      vSizeUOMId              IN rg_lane_dtl.SIZE_UOM_ID%TYPE,
      vOverrideCode           IN rg_lane_dtl.OVERRIDE_CODE%TYPE,
      vPreferenceBonusValue   IN rg_lane_dtl.PREFERENCE_BONUS_VALUE%TYPE,
      vCubingIndicator        IN rg_lane_dtl.CUBING_INDICATOR%TYPE,
      vLPNType                IN rg_lane_dtl.SP_LPN_TYPE%TYPE,
      vMinLPNcount            IN rg_lane_dtl.SP_MIN_LPN_COUNT%TYPE,
      vMaxLPNcount            IN rg_lane_dtl.SP_MAX_LPN_COUNT%TYPE,
      vMinWeight              IN rg_lane_dtl.SP_MIN_WEIGHT%TYPE,
      vMaxWeight              IN rg_lane_dtl.SP_MAX_WEIGHT%TYPE,
      vMinVolume              IN rg_lane_dtl.SP_MIN_VOLUME%TYPE,
      vMaxVolume              IN rg_lane_dtl.SP_MAX_VOLUME%TYPE,
      vMinLength              IN rg_lane_dtl.SP_MIN_LINEAR_FEET%TYPE,
      vMaxLength              IN rg_lane_dtl.SP_MAX_LINEAR_FEET%TYPE,
      vMinMonetaryValue       IN rg_lane_dtl.SP_MIN_MONETARY_VALUE%TYPE,
      vMaxMonetaryValue       IN rg_lane_dtl.SP_MAX_MONETARY_VALUE%TYPE,
      vMVCurrencyCode         IN rg_lane_dtl.SP_CURRENCY_CODE%TYPE,
	  vVoyage				  IN rg_lane_dtl.VOYAGE%TYPE)
      RETURN NUMBER;

   PROCEDURE INSERT_SURGE_CAPACITY (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vRGLaneId          IN surge_capacity.LANE_ID%TYPE,
      vRGLaneDtlId       IN surge_capacity.RG_LANE_DTL_SEQ%TYPE,
      vEffectiveDT       IN surge_capacity.EFFECTIVE_DT%TYPE,
      vExpirationDT      IN surge_capacity.EXPIRATION_DT%TYPE,
      vYearlyCapacity    IN surge_capacity.YEARLY_SURGE_CAPACITY%TYPE,
      vMonthlyCapacity   IN surge_capacity.MONTHLY_SURGE_CAPACITY%TYPE,
      vWeeklyCapacity    IN surge_capacity.WEEKLY_SURGE_CAPACITY%TYPE,
      vDailyCapacity     IN surge_capacity.DAILY_SURGE_CAPACITY%TYPE,
      vCapacitySun       IN surge_capacity.SURGE_CAPACITY_SUN%TYPE,
      vCapacityMon       IN surge_capacity.SURGE_CAPACITY_MON%TYPE,
      vCapacityTue       IN surge_capacity.SURGE_CAPACITY_TUE%TYPE,
      vCapacityWed       IN surge_capacity.SURGE_CAPACITY_WED%TYPE,
      vCapacityThu       IN surge_capacity.SURGE_CAPACITY_THU%TYPE,
      vCapacityFri       IN surge_capacity.SURGE_CAPACITY_FRI%TYPE,
      vCapacitySat       IN surge_capacity.SURGE_CAPACITY_SAT%TYPE);

   PROCEDURE INSERT_DAILY_UTILIZATION (
      vTCCompanyId        IN company.COMPANY_ID%TYPE,
      vUtilizationDT      IN daily_utilization.UTILIZATION_DATE%TYPE,
      vRGLaneId           IN daily_utilization.LANE_ID%TYPE,
      vRGLaneDtlSeq       IN daily_utilization.RG_LANE_DTL_SEQ%TYPE,
      vDailyUtilization   IN daily_utilization.DAILY_UTILIZATION%TYPE);

   PROCEDURE CHECK_DUPLICATE_RG_LANE_DTL (
      VLANEID       IN     COMB_LANE.LANE_ID%TYPE,
      CARRIERID     IN     COMB_LANE_DTL.CARRIER_ID%TYPE,
      MOTID         IN     COMB_LANE_DTL.MOT_ID%TYPE,
      SLID          IN     COMB_LANE_DTL.SERVICE_LEVEL_ID%TYPE,
      EQID          IN     COMB_LANE_DTL.EQUIPMENT_ID%TYPE,
      PLID          IN     COMB_LANE_DTL.PROTECTION_LEVEL_ID%TYPE,
      SCCARRIERID   IN     COMB_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
	  VVOYAGE		IN	   COMB_LANE_DTL.VOYAGE%TYPE,
      VCOUNT           OUT INTEGER);

   FUNCTION DELETE_IMP_RG_LANES (
      vTCCompanyId    IN company.COMPANY_ID%TYPE,
      vImportLaneID   IN import_rg_lane.LANE_ID%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_HIERARCHY (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vLaneId            IN rg_lane.LANE_ID%TYPE,
      oFacilityId        IN rg_lane.O_FACILITY_ID%TYPE,
      oFacilityAliasId   IN rg_lane.O_FACILITY_ALIAS_ID%TYPE,
      oCity              IN rg_lane.O_CITY%TYPE,
      oState             IN rg_lane.O_STATE_PROV%TYPE,
      oCounty            IN rg_lane.O_COUNTY%TYPE,
      oPostal            IN rg_lane.O_POSTAL_CODE%TYPE,
      oCountry           IN rg_lane.O_COUNTRY_CODE%TYPE,
      oZoneId            IN zone.ZONE_ID%TYPE,
      oZoneName          IN import_rg_lane.O_ZONE_NAME%TYPE,
      dFacilityId        IN rg_lane.D_FACILITY_ID%TYPE,
      dFacilityAliasId   IN rg_lane.D_FACILITY_ALIAS_ID%TYPE,
      dState             IN rg_lane.D_STATE_PROV%TYPE,
      dCity              IN rg_lane.D_CITY%TYPE,
      dCounty            IN rg_lane.D_COUNTY%TYPE,
      dCountry           IN rg_lane.D_COUNTRY_CODE%TYPE,
      dPostal            IN rg_lane.D_POSTAL_CODE%TYPE,
      dZoneId            IN zone.ZONE_ID%TYPE,
      dZoneName          IN import_rg_lane.D_ZONE_NAME%TYPE,
	routeTo		 IN import_rg_lane.ROUTE_TO%TYPE,
	routeType1	 IN import_rg_lane.ROUTE_TYPE_1%TYPE,
	routetype2	 IN import_rg_lane.ROUTE_TYPE_2%TYPE)

      RETURN NUMBER;

   FUNCTION VALIDATE_RG_QUALIFIER   /* SL 05/29  Validate the RG Qualifier  */
                                  (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                                   vLaneId        IN rg_lane.LANE_ID%TYPE,
                                   vRgQualifier   IN rg_lane.rg_qualifier%TYPE)
      RETURN NUMBER;

   FUNCTION GET_BUID_FROM_BUSINESS_UNIT (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vBUColumn         VARCHAR2)
      RETURN NUMBER;

   FUNCTION VALIDATE_LANE_BASE_DATA (
      vTCCompanyId         IN company.COMPANY_ID%TYPE,
      vLaneId              IN rg_lane.LANE_ID%TYPE,
      vBusinessUnit        IN business_unit.BUSINESS_UNIT%TYPE,
      vOFacilityId         IN facility.FACILITY_ID%TYPE,
      vOFacilityAlias      IN facility_alias.FACILITY_ALIAS_ID%TYPE,
      vOFacilityBU         IN business_unit.BUSINESS_UNIT%TYPE,
      vOStateProv          IN state_prov.STATE_PROV%TYPE,
      vOPostal             IN postal_code.POSTAL_CODE%TYPE,
      vOCountry            IN country.COUNTRY_CODE%TYPE,
      vOZoneId             IN zone.ZONE_ID%TYPE,
      vOZoneName           IN import_rg_lane.O_ZONE_NAME%TYPE,
      vDFacilityId         IN facility.FACILITY_ID%TYPE,
      vDFacilityAlias      IN facility_alias.FACILITY_ALIAS_ID%TYPE,
      vDFacilityBU         IN business_unit.BUSINESS_UNIT%TYPE,
      vDStateProv          IN state_prov.STATE_PROV%TYPE,
      vDPostal             IN postal_code.POSTAL_CODE%TYPE,
      vDCountry            IN country.COUNTRY_CODE%TYPE,
      vDZoneId             IN zone.ZONE_ID%TYPE,
      vDZoneName           IN import_rg_lane.D_ZONE_NAME%TYPE,
      vCustomerId          IN rg_lane.CUSTOMER_ID%TYPE,
      vCustomerCode        IN import_rg_lane.CUSTOMER_CODE%TYPE,
      vCustomerCodeBU      IN import_rg_lane.CUSTOMER_CODE_BU%TYPE,
      vBillingMethod       IN rg_lane.BILLING_METHOD%TYPE,
      vBillingMethodCode   IN import_rg_lane.BILLING_METHOD_CODE%TYPE,
      vIncotermId          IN rg_lane.INCOTERM_ID%TYPE,
      vIncotermName        IN import_rg_lane.INCOTERM_NAME%TYPE,
      vIncotermNameBU      IN import_rg_lane.INCOTERM_NAME_BU%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_DETAIL_BASE_DATA (
      vTCCompanyId               IN company.COMPANY_ID%TYPE,
      vLaneId                    IN rg_lane.LANE_ID%TYPE,
      vRgDtlSeq                  IN rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      vCarrierCode               IN IMPORT_RG_LANE_DTL.CARRIER_CODE%TYPE,
      vCarrierCodeBUID           IN company.COMPANY_ID%TYPE,
      vMode                      IN IMPORT_RG_LANE_DTL.MOT%TYPE,
      vModeBUID                  IN company.COMPANY_ID%TYPE,
      vServiceLevel              IN IMPORT_RG_LANE_DTL.SERVICE_LEVEL%TYPE,
      vServiceLevelBUID          IN company.COMPANY_ID%TYPE,
      vEquipment                 IN IMPORT_RG_LANE_DTL.EQUIPMENT_CODE%TYPE,
      vEquipmentBUID             IN company.COMPANY_ID%TYPE,
      vProtectionLevel           IN IMPORT_RG_LANE_DTL.PROTECTION_LEVEL%TYPE,
      vProtectionLevelBUID       IN company.COMPANY_ID%TYPE,
      p_scndr_carrier_code       IN CARRIER_CODE.CARRIER_CODE%TYPE,
      p_scndr_carrier_codeBUID   IN company.COMPANY_ID%TYPE,
      vSizeUOM                   IN IMPORT_RG_LANE_DTL.SIZE_UOM%TYPE,
      vSizeUOMBUID               IN IMPORT_RG_LANE_DTL.SIZE_UOM_ID%TYPE,
	  vRank                      IN IMPORT_RG_LANE_DTL.RANK%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_TP_EQUIP_SERV_MODE (
      vTCCompanyId           IN company.COMPANY_ID%TYPE,
      vLaneId                IN rg_lane.LANE_ID%TYPE,
      vRgDtlSeq              IN rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      vCarrierCode           IN carrier_code.CARRIER_CODE%TYPE,
      vEquipment             IN equipment.EQUIPMENT_CODE%TYPE,
      vServiceLevel          IN service_level.SERVICE_LEVEL%TYPE,
      vMode                  IN mot.MOT%TYPE,
      p_scndr_carrier_code   IN CARRIER_CODE.CARRIER_CODE%TYPE,
      vRepTpFlag             IN rg_lane_dtl.rep_tp_flag%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_FEASIBLE (
      vTCCompanyId               IN company.COMPANY_ID%TYPE,
      vLaneId                    IN rg_lane.LANE_ID%TYPE,
      vRgDtlSeq                  IN rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      vCarrierCode               IN carrier_code.CARRIER_CODE%TYPE,
      vCarrierCodeBUID           IN company.COMPANY_ID%TYPE,
      vEquipment                 IN equipment.EQUIPMENT_CODE%TYPE,
      vEquipmentBUID             IN company.COMPANY_ID%TYPE,
      vProtectionLevel           IN protection_level.PROTECTION_LEVEL%TYPE,
      vProtectionLevelBUID       IN company.COMPANY_ID%TYPE,
      p_scndr_carrier_code       IN CARRIER_CODE.CARRIER_CODE%TYPE,
      p_scndr_carrier_codeBUID   IN company.COMPANY_ID%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_FAC_CARR_FEASIBLE (
      vTCCompanyId               IN company.COMPANY_ID%TYPE,
      vLaneId                    IN rg_lane.LANE_ID%TYPE,
      vRgDtlSeq                  IN rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      vCarrierCode               IN carrier_code.CARRIER_CODE%TYPE,
      vCarrierCodeBUID           IN company.COMPANY_ID%TYPE,
      p_scndr_carrier_code       IN CARRIER_CODE.CARRIER_CODE%TYPE,
      p_scndr_carrier_codeBUID   IN company.COMPANY_ID%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_CAPACITY_COMMITMENT (
      vTCCompanyId    IN company.COMPANY_ID%TYPE,
      vLaneId         IN import_rg_lane.LANE_ID%TYPE,
      vRGLaneDtlSeq   IN import_rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      vEffectiveDt    IN import_rg_lane_dtl.effective_dt%TYPE,
      vExpirationDt   IN import_rg_lane_dtl.expiration_dt%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_CARRIER_MODE_UOM (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vLaneId            IN import_rg_lane.LANE_ID%TYPE,
      vRGLaneDtlSeq      IN import_rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      vCarrierCode       IN import_rg_lane_dtl.CARRIER_CODE%TYPE,
      vCarrierCodeBUID   IN import_rg_lane_dtl.TP_CODE_BU%TYPE,
      vMode              IN import_rg_lane_dtl.MOT%TYPE,
      vModeBUID          IN import_rg_lane_dtl.MOT_BU%TYPE,
      vSizeUOM           IN import_rg_lane_dtl.SIZE_UOM%TYPE,
      vSizeUOMBUID       IN import_rg_lane_dtl.SIZE_UOM_BU%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_DOW_CAPACITY_COMMIT (
      vTCCompanyId        IN company.COMPANY_ID%TYPE,
      vLaneId             IN import_rg_lane.LANE_ID%TYPE,
      vRGLaneDtlSeq       IN import_rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      vDowCapacity        IN import_rg_lane_dtl.commitment_sun%TYPE,
      vDowCommitment      IN import_rg_lane_dtl.commitment_sun%TYPE,
      vDowCommitPct       IN import_rg_lane_dtl.commitment_sun%TYPE,
      vDowSurgeCapacity   IN import_rg_lane_dtl.commitment_sun%TYPE,
      vDayOfWeek          IN VARCHAR2,
      vMot                IN mot.mot%TYPE,
      vTrackCommitment    IN mot.track_commitment%TYPE,
      vSurgeCount         IN NUMBER)
      RETURN NUMBER;

   FUNCTION VALIDATE_LANE_ADDRESSES (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vLaneId        IN rg_lane.LANE_ID%TYPE,
      vPostalCode    IN rg_lane.O_POSTAL_CODE%TYPE,
      vState         IN rg_lane.O_STATE_PROV%TYPE,
      vCountry       IN rg_lane.O_COUNTRY_CODE%TYPE)
      RETURN NUMBER;

   FUNCTION GET_BUDG_RESOURCE_WHERE_CLAUSE (
      vMot                   IN rg_lane_dtl.MOT_ID%TYPE,
      vEquipment             IN rg_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN rg_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN RG_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN rg_lane_dtl.PROTECTION_LEVEL_ID%TYPE)
      RETURN VARCHAR2;

   FUNCTION GET_RESOURCE_WHERE_CLAUSE (
      vCarrierCode           IN rg_lane_dtl.CARRIER_ID%TYPE,
      vMot                   IN rg_lane_dtl.MOT_ID%TYPE,
      vEquipment             IN rg_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN rg_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN RG_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN rg_lane_dtl.PROTECTION_LEVEL_ID%TYPE,
	  vVoyage				 IN rg_lane_dtl.VOYAGE%TYPE)
      RETURN VARCHAR2;

   FUNCTION GET_PARENT_BU_IDS (vTCCompanyId IN company.COMPANY_ID%TYPE)
      RETURN VARCHAR2;

   PROCEDURE REPLICATE_RA_SA_LANE_DETAIL (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vLaneId        IN comb_lane.LANE_ID%TYPE,
      vRGDtlSeq      IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vIsRating      IN comb_lane_dtl.IS_RATING%TYPE,
      vIsSailing     IN comb_lane_dtl.IS_SAILING%TYPE);

   FUNCTION VALIDATE_SHIPPING_PARAMETERS (
      vTCCompanyId        IN company.COMPANY_ID%TYPE,
      vLaneId             IN rg_lane.LANE_ID%TYPE,
      vRgLaneDtlSeq       IN rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      vLPNType            IN rg_lane_dtl.SP_LPN_TYPE%TYPE,
      vMinLPNcount        IN rg_lane_dtl.SP_MIN_LPN_COUNT%TYPE,
      vMaxLPNcount        IN rg_lane_dtl.SP_MAX_LPN_COUNT%TYPE,
      vMinWeight          IN rg_lane_dtl.SP_MIN_WEIGHT%TYPE,
      vMaxWeight          IN rg_lane_dtl.SP_MAX_WEIGHT%TYPE,
      vMinVolume          IN rg_lane_dtl.SP_MIN_VOLUME%TYPE,
      vMaxVolume          IN rg_lane_dtl.SP_MAX_VOLUME%TYPE,
      vMinLength          IN rg_lane_dtl.SP_MIN_LINEAR_FEET%TYPE,
      vMaxLength          IN rg_lane_dtl.SP_MAX_LINEAR_FEET%TYPE,
      vMinMonetaryValue   IN rg_lane_dtl.SP_MIN_MONETARY_VALUE%TYPE,
      vMaxMonetaryValue   IN rg_lane_dtl.SP_MAX_MONETARY_VALUE%TYPE,
      vMVCurrencyCode     IN rg_lane_dtl.SP_CURRENCY_CODE%TYPE)
      RETURN NUMBER;

   FUNCTION VALI_MUL_R_TP_FLAG (
      vTCCompanyId           IN company.COMPANY_ID%TYPE,
      vLaneId                IN rg_lane.LANE_ID%TYPE,
      vCarrierCode           IN CARRIER_CODE.CARRIER_CODE%TYPE,
      vRepTpFlag             IN rg_lane_dtl.REP_TP_FLAG%TYPE,
      vMode                  IN mot.mot%TYPE,
      p_effective_dt         IN RG_LANE_DTL.EFFECTIVE_DT%TYPE,
      p_expiration_dt        IN RG_LANE_DTL.EXPIRATION_DT%TYPE,
      p_scndr_carrier_code   IN CARRIER_CODE.CARRIER_CODE%TYPE)
      RETURN NUMBER;

   FUNCTION GET_SHIP_PARAMS_WHERE_CLAUSE (
      vMinVolume          IN RG_LANE_DTL.SP_MIN_VOLUME%TYPE,
      vMaxVolume          IN RG_LANE_DTL.SP_MAX_VOLUME%TYPE,
      vMinWeight          IN RG_LANE_DTL.SP_MIN_WEIGHT%TYPE,
      vMaxWeight          IN RG_LANE_DTL.SP_MAX_WEIGHT%TYPE,
      vMinLength          IN RG_LANE_DTL.SP_MIN_LINEAR_FEET%TYPE,
      vMaxLength          IN RG_LANE_DTL.SP_MAX_LINEAR_FEET%TYPE,
      vMinLpnCount        IN RG_LANE_DTL.SP_MIN_LPN_COUNT%TYPE,
      vMaxLpnCount        IN RG_LANE_DTL.SP_MAX_LPN_COUNT%TYPE,
      vLpnType            IN RG_LANE_DTL.SP_LPN_TYPE%TYPE,
      vMinMonetary        IN RG_LANE_DTL.SP_MIN_MONETARY_VALUE%TYPE,
      vMaxMonetary        IN RG_LANE_DTL.SP_MAX_MONETARY_VALUE%TYPE,
      vMonetaryCurrency   IN RG_LANE_DTL.SP_CURRENCY_CODE%TYPE)
      RETURN VARCHAR2;
	  
	  FUNCTION GET_SHIP_PARAMS_UPDATE_CLAUSE (
      vMinVolume          IN RG_LANE_DTL.SP_MIN_VOLUME%TYPE,
      vMaxVolume          IN RG_LANE_DTL.SP_MAX_VOLUME%TYPE,
      vMinWeight          IN RG_LANE_DTL.SP_MIN_WEIGHT%TYPE,
      vMaxWeight          IN RG_LANE_DTL.SP_MAX_WEIGHT%TYPE,
      vMinLength          IN RG_LANE_DTL.SP_MIN_LINEAR_FEET%TYPE,
      vMaxLength          IN RG_LANE_DTL.SP_MAX_LINEAR_FEET%TYPE,
      vMinLpnCount        IN RG_LANE_DTL.SP_MIN_LPN_COUNT%TYPE,
      vMaxLpnCount        IN RG_LANE_DTL.SP_MAX_LPN_COUNT%TYPE,
      vLpnType            IN RG_LANE_DTL.SP_LPN_TYPE%TYPE,
      vMinMonetary        IN RG_LANE_DTL.SP_MIN_MONETARY_VALUE%TYPE,
      vMaxMonetary        IN RG_LANE_DTL.SP_MAX_MONETARY_VALUE%TYPE,
      vMonetaryCurrency   IN RG_LANE_DTL.SP_CURRENCY_CODE%TYPE)
      RETURN VARCHAR2;

   FUNCTION VALIDATE_OVERLAP_SHIP_PARAMS (
      vTCCompanyId          IN company.COMPANY_ID%TYPE,
      vImportLaneId         IN rg_lane.LANE_ID%TYPE,
      vLaneId               IN rg_lane.LANE_ID%TYPE,
      vImpRGLaneDtlSeq      IN rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      carrier_Id            IN rg_lane_dtl.CARRIER_ID%TYPE,
      mot_Id                IN rg_lane_dtl.MOT_ID%TYPE,
      equipment_Id          IN rg_lane_dtl.EQUIPMENT_ID%TYPE,
      service_Level_Id      IN rg_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      scndr_carrier_Id      IN rg_lane_dtl.SCNDR_CARRIER_ID%TYPE,
      protection_Level_Id   IN rg_lane_dtl.PROTECTION_LEVEL_ID%TYPE,
      vMinVolume            IN RG_LANE_DTL.SP_MIN_VOLUME%TYPE,
      vMaxVolume            IN RG_LANE_DTL.SP_MAX_VOLUME%TYPE,
      vMinWeight            IN RG_LANE_DTL.SP_MIN_WEIGHT%TYPE,
      vMaxWeight            IN RG_LANE_DTL.SP_MAX_WEIGHT%TYPE,
      vMinLength            IN RG_LANE_DTL.SP_MIN_LINEAR_FEET%TYPE,
      vMaxLength            IN RG_LANE_DTL.SP_MAX_LINEAR_FEET%TYPE,
      vMinLpnCount          IN RG_LANE_DTL.SP_MIN_LPN_COUNT%TYPE,
      vMaxLpnCount          IN RG_LANE_DTL.SP_MAX_LPN_COUNT%TYPE,
      vLpnType              IN RG_LANE_DTL.SP_LPN_TYPE%TYPE,
      vMinMonetary          IN RG_LANE_DTL.SP_MIN_MONETARY_VALUE%TYPE,
      vMaxMonetary          IN RG_LANE_DTL.SP_MAX_MONETARY_VALUE%TYPE,
      vMonetaryCurrency     IN RG_LANE_DTL.SP_CURRENCY_CODE%TYPE,
      vEffectiveDT          IN rg_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT         IN rg_lane_dtl.EXPIRATION_DT%TYPE,
	  vVoyage				IN rg_lane_dtl.VOYAGE%TYPE
	  )
      RETURN NUMBER;

   FUNCTION VALIDATE_CUST_CARR_MOT_FEAS (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vLaneId        IN rg_lane.LANE_ID%TYPE,
      vRgDtlSeq      IN rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      vCarrierCode   IN carrier_code.CARRIER_CODE%TYPE,
      vMot           IN MOT.MOT%TYPE)
      RETURN NUMBER;
	  
   PROCEDURE VALIDATE_RANK (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vLaneId        IN IMPORT_RATING_LANE.LANE_ID%TYPE,
      vRGDtlSeq      IN IMPORT_RG_LANE_DTL.RG_LANE_DTL_SEQ%TYPE,
      vRank          IN IMPORT_RG_LANE_DTL.RANK%TYPE);
	  
   FUNCTION VALIDATE_USE_EPI (
   vTCCompanyId   		IN company.COMPANY_ID%TYPE,
   vLaneId        		IN IMPORT_RATING_LANE.LANE_ID%TYPE,
   vImportLaneId        IN IMPORT_RATING_LANE.LANE_ID%TYPE,
   vUseEpi    			IN IMPORT_RG_LANE.USE_EPI%TYPE,
   vEpiServiceGroup    	IN IMPORT_RG_LANE.EPI_SERVICE_GROUP%TYPE)
      RETURN NUMBER;
	  
   FUNCTION VALIDATE_EPI_SERVICE_GROUP (
   vTCCompanyId   		IN company.COMPANY_ID%TYPE,
   vLaneId        		IN IMPORT_RATING_LANE.LANE_ID%TYPE,
   vUseEpi    			IN IMPORT_RG_LANE.USE_EPI%TYPE,
   vEpiServiceGroup    	IN IMPORT_RG_LANE.EPI_SERVICE_GROUP%TYPE)
      RETURN NUMBER;
	  
   FUNCTION VALIDATE_EPI_FOR_LANE_DTL (
   vTCCompanyId   		IN company.COMPANY_ID%TYPE,
   vLaneId        		IN rg_lane.LANE_ID%TYPE,
   vRgDtlSeq      		IN rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
   vCarrierId   		IN IMPORT_RG_LANE_DTL.CARRIER_ID%TYPE,
   vUseEpi          	IN IMPORT_RG_LANE.USE_EPI%TYPE,
   vEpiServiceGroup     IN IMPORT_RG_LANE.EPI_SERVICE_GROUP%TYPE)
      RETURN NUMBER;
	  
   PROCEDURE UPDATE_ROUT_STATUS_FLAGS (
   vLaneID    IN COMB_LANE.LANE_ID%TYPE);
	  
END RG_LANE_VALIDATION_PKG;
/

create or replace
PACKAGE BODY RG_LANE_VALIDATION_PKG
AS
   -- -----------------------------------------------------------------------------------------
   -- Change History /BUG FIXES
   -- ID  DATE  AUTHOR DETAILS
   -- n/a 08/29/2003 MB:  added SCNDR_CARRIER_CODE support to IMPORT_RG_LANE_DETAILS
   -- n/a 09/02/2003 MB:  added SCNDR_CARRIER_CODE support to INSERT_RG_LANE_DTL
   -- n/a 09/02/2003 MB:  added SCNDR_CARRIER_CODE support to TRANSFER_IMPORT_LANE_DTL
   -- n/a 09/03/2003 MB:  added SCNDR_CARRIER_CODE support to ADJUST_EFF_EXP_DT
   -- n/a 09/03/2003 MB:  added SCNDR_CARRIER_CODE support to VALIDATE_MULTIPLE_REP_TP_FLAGS,VALIDATE_DETAIL_BASE_DATA,VALIDATE_TP_EQUIP_SERV_MODE,VALIDATE_FEASIBLE,VALIDATE_FAC_CARR_FEASIBLE
   -- n/a 09/03/2003 MB:  added SCNDR_CARRIER_CODE support to GET_BUDG_RESOURCE_WHERE_CLAUSE,GET_RESOURCE_WHERE_CLAUSE
   -- n/a 09/03/2003 MB:  added SCNDR_CARRIER_CODE support to VALIDATE_LANE_DTL
   -- n/a 09/04/2003 MB:  added p_error_msg_id,p_error_param_list to INSERT_RG_LANE_ERROR, INSERT_RG_LANE_DTL_ERROR (globalization - Fumin)
   -- n/a 09/05/2003 MB:  error messages globalized (every call to INSERT_RG_LANE_ERROR, INSERT_RG_LANE_DTL_ERROR) (globalization - Fumin)
   -- n/a 09/08/2003 Karthik: '??? ' replaced with '???' (right hand side space removal from spacial character set (Alt 0191) (globalization - Ramesh Chandra)
   -- n/a 09/08/2003 MB:  '???' replaced with '<sep>' as the separator string for RG_LANE_ERRORS.ERROR_PARAM_LIST (globalization - Fumin)
   -- N/A 09/10/2003 mb:  fixed: call to VALIDATE_TP_EQUIP_SERV_MODE, parameter order (in VALIDATE_LANE_DTL)
   -- n/a 11/24/2003 MB  added:  CARRIER_CODE,MOT,SIZE_UOM to DAILY_UTILIZATION(Deepak-BaseData)
   --  01/30/2004 MB:  modified: INSERT_RG_LANE (switched to COMB_LANE)
   --  04/08/2004 MB:  modified: IMPORT_RG_LANE_DETAILS, IMPORT_RG_LANE_SURGE  NO_DATA_FOUND error hadled (Deepak)
   -- n/a  22/06/2004 RAM  added  Procedure COPY CARRIERS for updation of relavent carriers with the lane details changes when modified  SR1, 4R1 change, build92,  TT31182
   -- n/a 01/25/2005 MB  modified INSERT_SURGE_CAPACITY (cosmetic) pre TT44119
   -- n/a 01/25/2005 MB  modified VALIDATE_LANE_DTL, (LANE_DTL_STATUS in 3, 4)
   -- n/a 01/25/2005 MB  modified VALIDATE_MULTIPLE_REP_TP_FLAGS (TT42403)
   -- n/a 01/25/2005 MB  modified INSERT_RG_LANE (TT42403)
   -- n/a 01/31/2005 MB  modified VALIDATE_MULTIPLE_REP_TP_FLAGS (TT42403)
   -- n/a 01/31/2005 MB  modified INSERT_RG_LANE (TT42403)
   -- n/a 02/01/2005 MB  modified: many... (Gates Riley, TT44119)
   -- n/a 02/07/2005 MB  modified  validate_capacity_commitment (Vasif, TT42340)
   -- n/a 02/07/2005 MB  modified  VALIDATE_LANE_DTL (Ramaswamy Meka, TT 45901)
   --      07/15/2005      RS              modified by Priti Khare (CR#3354)
   -- 11/08/2005  Ganesh Modified VALIDATE_MULTIPLE_REP_TP_FLAGS (Wilson, TT 52818)
   --      06/15/2005      ROUTING          added new  FUNCTION GET_BU_COMB_LANE_DTL (Ganesh Kadoor Prasad),
   --      01/03/2006  Rajeev  DBCR 16475
   --      01/04/2006  Rajeev  DBCR 12286
   --      01/05/2006  Rajeev  DBCR 16627
   --      01/10/2006  Ganesh  DBCR 16742
   --      01/11/2006      modified by Gates Riley, CR 16855
   --      01/17/2006      modified by Shobhit, CR 17299 --This change undoes change made for CR 16855 - Fix was moved to code
   -- 07/25/2006 CR 30436(Vaibhav Nirkhe)-Addressed by Abhishek Midha
   -- 08/11/2006 Ganesh DBCR 31479 (Vaibhav N) modified VALIDATE_MULTIPLE_REP_TP_FLAGS
   -- 17/12/2008 rvittal Modified VALIDATE_LANE_UI by commenting the tc_company_id in the where clause.
   -- -----------------------------------------------------------------------------------------

   PROCEDURE IMPORT_RG_LANES (
      vRootCompanyId          IN company.COMPANY_ID%TYPE,
      vTCCompanyId            IN company.COMPANY_ID%TYPE,
      vBatchId                IN import_rg_lane.BATCH_ID%TYPE,
      vOFacilityAliasId       IN import_rg_lane.O_FACILITY_ALIAS_ID%TYPE,
      vOFacilityBU            IN import_rg_lane.O_FACILITY_BU%TYPE,
      vOCity                  IN import_rg_lane.O_CITY%TYPE,
      vOStateProv             IN import_rg_lane.O_STATE_PROV%TYPE,
      vOCounty                IN import_rg_lane.O_COUNTY%TYPE,
      vOPostal                IN import_rg_lane.O_POSTAL_CODE%TYPE,
      vOCountry               IN import_rg_lane.O_COUNTRY_CODE%TYPE,
      vOZoneId                IN import_rg_lane.O_ZONE_ID%TYPE,
      vOZoneName              IN import_rg_lane.O_ZONE_NAME%TYPE,
      vDFacilityAliasId       IN import_rg_lane.D_FACILITY_ALIAS_ID%TYPE,
      vDFacilityBU            IN import_rg_lane.D_FACILITY_BU%TYPE,
      vDCity                  IN import_rg_lane.D_CITY%TYPE,
      vDStateProv             IN import_rg_lane.D_STATE_PROV%TYPE,
      vDCounty                IN import_rg_lane.D_COUNTY%TYPE,
      vDPostal                IN import_rg_lane.D_POSTAL_CODE%TYPE,
      vDCountry               IN import_rg_lane.D_COUNTRY_CODE%TYPE,
      vDZoneId                IN import_rg_lane.D_ZONE_ID%TYPE,
      vDZoneName              IN import_rg_lane.d_ZONE_NAME%TYPE,
      vRGQualifier            IN import_rg_lane.RG_QUALIFIER%TYPE,
      vFrequency              IN import_rg_lane.FREQUENCY%TYPE,
      vLaneStatus             IN import_rg_lane.LANE_STATUS%TYPE,
      vBusinessUnit           IN import_rg_lane.BUSINESS_UNIT%TYPE,
      vCustomerCode           IN import_rg_lane.CUSTOMER_CODE%TYPE,
      vCustomerCodeBU         IN import_rg_lane.CUSTOMER_CODE_BU%TYPE,
      vBillingMethodCode      IN import_rg_lane.BILLING_METHOD_CODE%TYPE,
      vIncotermName           IN import_rg_lane.INCOTERM_NAME%TYPE,
      vIncotermNameBU         IN import_rg_lane.INCOTERM_NAME_BU%TYPE,
      vRouteTo                IN import_rg_lane.ROUTE_TO%TYPE,
      vRouteType1             IN import_rg_lane.ROUTE_TYPE_1%TYPE,
      vRouteType2             IN import_rg_lane.ROUTE_TYPE_2%TYPE,
      vLaneName               IN import_rg_lane.LANE_NAME%TYPE,
      vNoRating               IN import_rg_lane.NO_RATING%TYPE,
      vSelectFastestCarrier   IN import_rg_lane.USE_FASTEST%TYPE,
      vUsePreferenceBonus     IN import_rg_lane.USE_PREFERENCE_BONUS%TYPE,
	  vUseEpi                 IN import_rg_lane.USE_EPI%TYPE,
	  vEpiServiceGroup        IN import_rg_lane.EPI_SERVICE_GROUP%TYPE)
   AS
      vRGLaneId       rg_lane.lane_id%TYPE;
      vWhereClause    VARCHAR2 (2000);
      vsqlStatement   VARCHAR2 (2000);
      vLaneId       NUMBER;
      eLargeValue   EXCEPTION;
   BEGIN
      SELECT SEQ_IMP_RG_LANE_ID.NEXTVAL INTO vLaneId FROM DUAL;

            INSERT INTO IMPORT_RG_LANE (ROOTCOMPANYID,
                                        TC_COMPANY_ID,
                                        LANE_ID,
                                        LANE_STATUS,
                                        BUSINESS_UNIT,
                                        O_FACILITY_ALIAS_ID,
                                        O_FACILITY_BU,
                                        O_CITY,
                                        O_STATE_PROV,
                                        O_COUNTY,
                                        O_POSTAL_CODE,
                                        O_COUNTRY_CODE,
                                        O_ZONE_ID,
                                        O_ZONE_NAME,
                                        D_FACILITY_ALIAS_ID,
                                        D_FACILITY_BU,
                                        D_CITY,
                                        D_STATE_PROV,
                                        D_COUNTY,
                                        D_POSTAL_CODE,
                                        D_COUNTRY_CODE,
                                        D_ZONE_ID,
                                        D_ZONE_NAME,
                                        RG_QUALIFIER,
                                        FREQUENCY,
                                        CUSTOMER_CODE,
                                        CUSTOMER_CODE_BU,
                                        BILLING_METHOD_CODE,
                                        INCOTERM_NAME,
                                        INCOTERM_NAME_BU,
                                        ROUTE_TO,
                                        ROUTE_TYPE_1,
                                        ROUTE_TYPE_2,
                                        LANE_NAME,
                                        NO_RATING,
                                        USE_FASTEST,
                                        USE_PREFERENCE_BONUS,
										USE_EPI,
										EPI_SERVICE_GROUP,
                                        CREATED_SOURCE_TYPE,
                                        CREATED_SOURCE,
                                        CREATED_DTTM,
                                        LAST_UPDATED_SOURCE_TYPE,
                                        LAST_UPDATED_SOURCE,
                                        LAST_UPDATED_DTTM,
                                        BATCH_ID,
                                        RG_LANE_ID)
                 VALUES (vRootCompanyId,
                         vTCCompanyId,
                         -- SEQ_IMP_RG_LANE_ID.NEXTVAL,
                         vLaneId,
                         vLaneStatus,
                         vBusinessUnit,
                         vOFacilityAliasId,
                         vOFacilityBU,
                         vOCity,
                         vOStateProv,
                         vOCounty,
                         vOPostal,
                         vOCountry,
                         vOZoneId,
                         vOZoneName,
                         vDFacilityAliasId,
                         vDFacilityBU,
                         vDCity,
                         vDStateProv,
                         vDCounty,
                         vDPostal,
                         vDCountry,
                         vDZoneId,
                         vDZoneName,
                         vRGQualifier,
                         vFrequency,
                         vCustomerCode,
                         vCustomerCodeBU,
                         vBillingMethodCode,
                         vIncotermName,
                         vIncotermNameBU,
                         vRouteTo,
                         vRouteType1,
                         vRouteType2,
                         vLaneName,
                         vNoRating,
                         vSelectFastestCarrier,
                         vUsePreferenceBonus,
						 vUseEpi,
						 vEpiServiceGroup,
                         3,
                         'OMS',
                         SYSDATE,
                         3,
                         'OMS',
                         SYSDATE,
                         vBatchId,
                         vBatchId);

            COMMIT;
         EXCEPTION
            WHEN eLargeValue
            THEN
               DBMS_OUTPUT.PUT_LINE (
                  'IMPORT_RG_LANE: Value specified larger than allowed for the column');

               INSERT INTO IMPORT_RG_LANE (rootcompanyid,
                                           tc_company_id,
                                           lane_id,
                                           created_source,
                                           last_updated_source,
                                           batch_id,
                                           rg_lane_id,
                                           lane_status)
                    VALUES (vRootCompanyId,
                            vTCCompanyId,
                            vLaneId,
                            'OMS',
                            'OMS',
                            vBatchId,
                            vBatchId,
                            4);

               INSERT_RG_LANE_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  'IMPORT_RG_LANE: Value specified larger than allowed for the column',
                  4720000,
                  NULL);
            WHEN OTHERS
            THEN
               DBMS_OUTPUT.PUT_LINE ('Procedure:IMPORT_RG_LANE: ' || SQLERRM);

               INSERT_RG_LANE_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  'IMPORT_RG_LANE: Database Exception Raised - '
                  || SUBSTR (SQLERRM, 1, 200),
                  4720001,
                  SUBSTR (SQLERRM, 1, 200));

   END IMPORT_RG_LANES;

   PROCEDURE IMPORT_RG_LANE_DETAILS (
      vTCCompanyId             IN company.COMPANY_ID%TYPE,
      vBatchId                 IN import_rg_lane.BATCH_ID%TYPE,
      vRgLaneDtlSeq            IN import_rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      vCarrierCode             IN import_rg_lane_dtl.CARRIER_CODE%TYPE,
      vCarrierCodeBU           IN import_rg_lane_dtl.TP_CODE_BU%TYPE,
      vMot                     IN import_rg_lane_dtl.MOT%TYPE,
      vMotBU                   IN import_rg_lane_dtl.MOT_BU%TYPE,
      vEquipment               IN import_rg_lane_dtl.EQUIPMENT_CODE%TYPE,
      vEquipmentBU             IN import_rg_lane_dtl.EQUIPMENT_BU%TYPE,
      vServiceLevel            IN import_rg_lane_dtl.SERVICE_LEVEL%TYPE,
      vServiceLevelBU          IN import_rg_lane_dtl.SERVICE_LEVEL_BU%TYPE,
      vPackageId               IN import_rg_lane_dtl.PACKAGE_ID%TYPE,
      vPackageName             IN import_rg_lane_dtl.PACKAGE_NAME%TYPE,
      p_scndr_carrier_code     IN IMPORT_RG_LANE_DTL.SCNDR_CARRIER_CODE%TYPE,
      p_scndr_carrier_codeBU   IN IMPORT_RG_LANE_DTL.SECONDARY_CARRIER_CODE_BU%TYPE,
      vProtectionLevel         IN import_rg_lane_dtl.PROTECTION_LEVEL%TYPE,
      vProtectionLevelBU       IN import_rg_lane_dtl.PROTECTION_LEVEL_BU%TYPE,
      vTierId                  IN import_rg_lane_dtl.TIER_ID%TYPE,
      vRank                    IN import_rg_lane_dtl.RANK%TYPE,
      vRepTpFlag               IN import_rg_lane_dtl.REP_TP_FLAG%TYPE,
      vYearlyCapacity          IN import_rg_lane_dtl.YEARLY_CAPACITY%TYPE,
      vMonthlyCapacity         IN import_rg_lane_dtl.MONTHLY_CAPACITY%TYPE,
      vWeeklyCapacity          IN import_rg_lane_dtl.WEEKLY_CAPACITY%TYPE,
      vDailyCapacity           IN import_rg_lane_dtl.Daily_Capacity%TYPE,
      vCapacitySun             IN import_rg_lane_dtl.CAPACITY_SUN%TYPE,
      vCapacityMon             IN import_rg_lane_dtl.CAPACITY_MON%TYPE,
      vCapacityTue             IN import_rg_lane_dtl.CAPACITY_TUE%TYPE,
      vCapacityWed             IN import_rg_lane_dtl.CAPACITY_WED%TYPE,
      vCapacityThu             IN import_rg_lane_dtl.CAPACITY_THU%TYPE,
      vCapacityFri             IN import_rg_lane_dtl.CAPACITY_FRI%TYPE,
      vCapacitySat             IN import_rg_lane_dtl.CAPACITY_SAT%TYPE,
      vYearlyCommitment        IN import_rg_lane_dtl.YEARLY_COMMITMENT%TYPE,
      vMonthlyCommitment       IN import_rg_lane_dtl.MONTHLY_COMMITMENT%TYPE,
      vWeeklyCommitment        IN import_rg_lane_dtl.WEEKLY_COMMITMENT%TYPE,
      vDailyCommitment         IN import_rg_lane_dtl.DAILY_COMMITMENT%TYPE,
      vCommitmentSun           IN import_rg_lane_dtl.COMMITMENT_SUN%TYPE,
      vCommitmentMon           IN import_rg_lane_dtl.COMMITMENT_MON%TYPE,
      vCommitmentTue           IN import_rg_lane_dtl.COMMITMENT_TUE%TYPE,
      vCommitmentWed           IN import_rg_lane_dtl.COMMITMENT_WED%TYPE,
      vCommitmentThu           IN import_rg_lane_dtl.COMMITMENT_THU%TYPE,
      vCommitmentFri           IN import_rg_lane_dtl.COMMITMENT_FRI%TYPE,
      vCommitmentSat           IN import_rg_lane_dtl.COMMITMENT_SAT%TYPE,
      vYearlyCommitmentPct     IN import_rg_lane_dtl.YEARLY_COMMIT_PCT%TYPE,
      vMonthlyCommitmentPct    IN import_rg_lane_dtl.MONTHLY_COMMIT_PCT%TYPE,
      vWeeklyCommitmentPct     IN import_rg_lane_dtl.WEEKLY_COMMIT_PCT%TYPE,
      vDailyCommitmentPct      IN import_rg_lane_dtl.DAILY_COMMIT_PCT%TYPE,
      vCommitPctSun            IN import_rg_lane_dtl.COMMIT_PCT_SUN%TYPE,
      vCommitPctMon            IN import_rg_lane_dtl.COMMIT_PCT_MON%TYPE,
      vCommitPctTue            IN import_rg_lane_dtl.COMMIT_PCT_TUE%TYPE,
      vCommitPctWed            IN import_rg_lane_dtl.COMMIT_PCT_WED%TYPE,
      vCommitPctThu            IN import_rg_lane_dtl.COMMIT_PCT_THU%TYPE,
      vCommitPctFri            IN import_rg_lane_dtl.COMMIT_PCT_FRI%TYPE,
      vCommitPctSat            IN import_rg_lane_dtl.COMMIT_PCT_SAT%TYPE,
      vLastUpdatedSourceType   IN import_rg_lane_dtl.last_updated_source_type%TYPE,
      vLastUpdatedSource       IN import_rg_lane_dtl.last_updated_source%TYPE,
      vIsPreferred             IN import_rg_lane_dtl.IS_PREFERRED%TYPE,
      vEffectiveDT             IN import_rg_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT            IN import_rg_lane_dtl.EXPIRATION_DT%TYPE,
      vLaneDtlStatus           IN import_rg_lane_dtl.IMPORT_RG_DTL_STATUS%TYPE,
      vToleranceFactor         IN import_rg_lane_dtl.TT_TOLERANCE_FACTOR%TYPE,
      vSizeUOM                 IN import_rg_lane_dtl.SIZE_UOM%TYPE,
      vSizeUOMBU               IN import_rg_lane_dtl.SIZE_UOM_BU%TYPE,
      VOVERRIDECODE            IN import_rg_lane_dtl.OVERRIDE_CODE%TYPE,
      VPREFERENCEBONUSVALUE    IN import_rg_lane_dtl.PREFERENCE_BONUS_VALUE%TYPE,
      VCUBINGINDICATOR         IN import_rg_lane_dtl.CUBING_INDICATOR%TYPE,
      VLPNTYPE                 IN import_rg_lane_dtl.SP_LPN_TYPE%TYPE,
      VMINLPNCOUNT             IN import_rg_lane_dtl.SP_MIN_LPN_COUNT%TYPE,
      VMAXLPNCOUNT             IN import_rg_lane_dtl.SP_MAX_LPN_COUNT%TYPE,
      VMINWEIGHT               IN import_rg_lane_dtl.SP_MIN_WEIGHT%TYPE,
      VMAXWEIGHT               IN import_rg_lane_dtl.SP_MAX_WEIGHT%TYPE,
      VMINVOLUME               IN import_rg_lane_dtl.SP_MIN_VOLUME%TYPE,
      VMAXVOLUME               IN import_rg_lane_dtl.SP_MAX_VOLUME%TYPE,
      VMINLENGTH               IN import_rg_lane_dtl.SP_MIN_LINEAR_FEET%TYPE,
      VMAXLENGTH               IN import_rg_lane_dtl.SP_MAX_LINEAR_FEET%TYPE,
      VMINMONETARYVALUE        IN import_rg_lane_dtl.SP_MIN_MONETARY_VALUE%TYPE,
      VMAXMONETARYVALUE        IN import_rg_lane_dtl.SP_MAX_MONETARY_VALUE%TYPE,
      VMVCURRENCYCODE          IN import_rg_lane_dtl.SP_CURRENCY_CODE%TYPE,
	  VVOYAGE				   IN import_rg_lane_dtl.voyage%TYPE)
   AS
      vLaneId              rg_lane.lane_id%TYPE;

      eInvalidNumber       EXCEPTION;
      eNegativeValue       EXCEPTION;
      eLargeValue          EXCEPTION;
      PRAGMA EXCEPTION_INIT (eLargeValue, -1401);
      eLargeNumericValue   EXCEPTION;
      PRAGMA EXCEPTION_INIT (eLargeNumericValue, -1438);
      vPassFail            NUMBER;
      vFailure             NUMBER;
   BEGIN
      SELECT lane_id
        INTO vLaneId
        FROM import_rg_lane
       WHERE tc_company_id = vTcCompanyId AND batch_id = vBatchId;

      -- Check if the capacities/commitment (weekly/Daily/DOW) should not be less than zero

      IF    (vYearlyCapacity < 0)
         OR (vMonthlyCapacity < 0)
         OR (vWeeklyCapacity < 0)
         OR (vDailyCapacity < 0)
         OR (vCapacitySun < 0)
         OR (vCapacityMon < 0)
         OR (vCapacityTue < 0)
         OR (vCapacityWed < 0)
         OR (vCapacityThu < 0)
         OR (vCapacityFri < 0)
         OR (vCapacitySat < 0)
         OR (vYearlyCommitment < 0)
         OR (vMonthlyCommitment < 0)
         OR (vWeeklyCommitment < 0)
         OR (vDailyCommitment < 0)
         OR (vCommitmentSun < 0)
         OR (vCommitmentMon < 0)
         OR (vCommitmentTue < 0)
         OR (vCommitmentWed < 0)
         OR (vCommitmentThu < 0)
         OR (vCommitmentFri < 0)
         OR (vCommitmentSat < 0)
         OR (vYearlyCommitmentPct < 0)
         OR (vMonthlyCommitmentPct < 0)
         OR (vWeeklyCommitmentPct < 0)
         OR (vDailyCommitmentPct < 0)
         OR (vCommitPctSun < 0)
         OR (vCommitPctMon < 0)
         OR (vCommitPctTue < 0)
         OR (vCommitPctWed < 0)
         OR (vCommitPctThu < 0)
         OR (vCommitPctFri < 0)
         OR (vCommitPctSat < 0)
      THEN
         RAISE eNegativeValue;
      END IF;

      IF    (vYearlyCommitmentPct > 100)
         OR (vMonthlyCommitmentPct > 100)
         OR (vWeeklyCommitmentPct > 100)
         OR (vDailyCommitmentPct > 100)
      THEN
         RAISE eLargeValue;
      END IF;

      vPassFail := is_not_numeric (vYearlyCapacity);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vMonthlyCapacity);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vWeeklyCapacity);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vDailyCapacity);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vCapacitySun);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vCapacityMon);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vCapacityTue);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vCapacityWed);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vCapacityThu);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vCapacityFri);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vCapacitySat);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vYearlyCommitment);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vMonthlyCommitment);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vWeeklyCommitment);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vDailyCommitment);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vCommitmentSun);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vCommitmentMon);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vCommitmentTue);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vCommitmentWed);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vCommitmentThu);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vCommitmentFri);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vCommitmentSat);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      IF vFailure = 1
      THEN
         RAISE eInvalidNumber;
      END IF;

      INSERT INTO IMPORT_RG_LANE_DTL (tc_company_id,
                                      lane_id,
                                      rg_lane_dtl_seq,
                                      carrier_code,
                                      tp_code_bu,
                                      mot,
                                      mot_bu,
                                      equipment_code,
                                      equipment_bu,
                                      service_level,
                                      service_level_bu,
                                      size_uom,
                                      size_uom_bu,
                                      package_id,
                                      package_name,
                                      protection_level,
                                      protection_level_bu,
                                      tier_id,
                                      RANK,
                                      rep_tp_flag,
                                      yearly_capacity,
                                      monthly_capacity,
                                      weekly_capacity,
                                      daily_capacity,
                                      capacity_sun,
                                      capacity_mon,
                                      capacity_tue,
                                      capacity_wed,
                                      capacity_thu,
                                      capacity_fri,
                                      capacity_sat,
                                      yearly_commitment,
                                      monthly_commitment,
                                      weekly_commitment,
                                      daily_commitment,
                                      commitment_sun,
                                      commitment_mon,
                                      commitment_tue,
                                      commitment_wed,
                                      commitment_thu,
                                      commitment_fri,
                                      commitment_sat,
                                      yearly_commit_pct,
                                      monthly_commit_pct,
                                      weekly_commit_pct,
                                      daily_commit_pct,
                                      commit_pct_sun,
                                      commit_pct_mon,
                                      commit_pct_tue,
                                      commit_pct_wed,
                                      commit_pct_thu,
                                      commit_pct_fri,
                                      commit_pct_sat,
                                      last_updated_source_type,
                                      last_updated_source,
                                      last_updated_dttm,
                                      is_preferred,
                                      import_rg_dtl_status,
                                      effective_dt,
                                      expiration_dt,
                                      lane_dtl_status,
                                      SCNDR_CARRIER_CODE,
                                      SECONDARY_CARRIER_CODE_BU,
                                      TT_TOLERANCE_FACTOR,
                                      OVERRIDE_CODE,
                                      PREFERENCE_BONUS_VALUE,
                                      CUBING_INDICATOR,
                                      SP_LPN_TYPE,
                                      SP_MIN_LPN_COUNT,
                                      SP_MAX_LPN_COUNT,
                                      SP_MIN_WEIGHT,
                                      SP_MAX_WEIGHT,
                                      SP_MIN_VOLUME,
                                      SP_MAX_VOLUME,
                                      SP_MIN_LINEAR_FEET,
                                      SP_MAX_LINEAR_FEET,
                                      SP_MIN_MONETARY_VALUE,
                                      SP_MAX_MONETARY_VALUE,
                                      SP_CURRENCY_CODE,
									  VOYAGE)
           VALUES (vTCCompanyId,
                   vLaneId,
                   vRgLaneDtlSeq,
                   vCarrierCode,
                   vCarrierCodeBU,
                   vMot,
                   vMotBU,
                   vEquipment,
                   vEquipmentBU,
                   vServiceLevel,
                   vServiceLevelBU,
                   vSizeUOM,
                   vSizeUOMBU,
                   vPackageId,
                   vPackageName,
                   vProtectionLevel,
                   vProtectionLevelBU,
                   vTierId,
                   vRank,
                   vRepTpFlag,
                   vYearlyCapacity,
                   vMonthlyCapacity,
                   vWeeklyCapacity,
                   vDailyCapacity,
                   vCapacitySun,
                   vCapacityMon,
                   vCapacityTue,
                   vCapacityWed,
                   vCapacityThu,
                   vCapacityFri,
                   vCapacitySat,
                   vYearlyCommitment,
                   vMonthlyCommitment,
                   vWeeklyCommitment,
                   vDailyCommitment,
                   vCommitmentSun,
                   vCommitmentMon,
                   vCommitmentTue,
                   vCommitmentWed,
                   vCommitmentThu,
                   vCommitmentFri,
                   vCommitmentSat,
                   vYearlyCommitmentPct,
                   vMonthlyCommitmentPct,
                   vWeeklyCommitmentPct,
                   vDailyCommitmentPct,
                   vCommitPctSun,
                   vCommitPctMon,
                   vCommitPctTue,
                   vCommitPctWed,
                   vCommitPctThu,
                   vCommitPctFri,
                   vCommitPctSat,
                   vLastUpdatedSourceType,
                   vLastUpdatedSource,
                   SYSDATE,
                   vIsPreferred,
                   3,
                   vEffectiveDT,
                   vExpirationDT,
                   vLaneDtlStatus,
                   p_scndr_carrier_code,
                   p_scndr_carrier_codeBU,
                   vToleranceFactor,
                   VOVERRIDECODE,
                   VPREFERENCEBONUSVALUE,
                   VCUBINGINDICATOR,
                   VLPNTYPE,
                   VMINLPNCOUNT,
                   VMAXLPNCOUNT,
                   VMINWEIGHT,
                   VMAXWEIGHT,
                   VMINVOLUME,
                   VMAXVOLUME,
                   VMINLENGTH,
                   VMAXLENGTH,
                   VMINMONETARYVALUE,
                   VMAXMONETARYVALUE,
                   VMVCURRENCYCODE,
				   VVOYAGE);

      COMMIT;
   EXCEPTION
      WHEN eInvalidNumber
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Capacity/Commitment should be a numeric whole number');

         INSERT INTO IMPORT_RG_LANE_DTL (tc_company_id,
                                         lane_id,
                                         rg_lane_dtl_seq,
                                         carrier_code,
                                         tp_code_bu,
                                         last_updated_source,
                                         mot,
                                         mot_bu,
                                         service_level,
                                         service_level_bu,
                                         package_id,
                                         package_name,
                                         equipment_code,
                                         equipment_bu,
                                         protection_level,
                                         protection_level_bu,
                                         effective_dt,
                                         expiration_dt,
										 voyage,
                                         lane_dtl_status)
              VALUES (vTCCompanyId,
                      vLaneId,
                      vRgLaneDtlSeq,
                      vCarrierCode,
                      vCarrierCodeBU,
                      vLastUpdatedSource,
                      vmot,
                      vmotBU,
                      vservicelevel,
                      vservicelevelBU,
                      vPackageId,
                      vPackageName,
                      vequipment,
                      vequipmentBU,
                      vProtectionlevel,
                      vProtectionlevelBU,
                      vEffectiveDT,
                      vExpirationDT,
					  VVOYAGE,
                      4);

         INSERT_RG_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRGLaneDtlSeq,
            'Capacity/Commitment should be a numeric number',
            4720023,
            NULL);
      WHEN eNegativeValue
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Capacity/Commitment should be a greater than zero');

         INSERT INTO IMPORT_RG_LANE_DTL (tc_company_id,
                                         lane_id,
                                         rg_lane_dtl_seq,
                                         carrier_code,
                                         tp_code_bu,
                                         last_updated_source,
                                         mot,
                                         mot_bu,
                                         service_level,
                                         service_level_bu,
                                         equipment_code,
                                         equipment_bu,
                                         protection_level,
                                         protection_level_bu,
                                         effective_dt,
                                         expiration_dt,
										 voyage,
                                         lane_dtl_status)
              VALUES (vTCCompanyId,
                      vLaneId,
                      vRgLaneDtlSeq,
                      vCarrierCode,
                      vCarrierCodeBU,
                      vLastUpdatedSource,
                      vMot,
                      vMotBU,
                      vServicelevel,
                      vServicelevelBU,
                      vEquipment,
                      vEquipmentBU,
                      vProtectionlevel,
                      vProtectionlevelBU,
                      vEffectiveDT,
                      vExpirationDT,
					  VVOYAGE,
                      4);
      WHEN eLargeValue
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'IMPORT_RG_LANE_DTL: Value specified larger than allowed for the column');

         INSERT INTO IMPORT_RG_LANE_DTL (tc_company_id,
                                         lane_id,
                                         rg_lane_dtl_seq,
                                         carrier_code,
                                         tp_code_bu,
                                         last_updated_source,
                                         mot,
                                         mot_bu,
                                         service_level,
                                         service_level_bu,
                                         package_id,
                                         package_name,
                                         equipment_code,
                                         equipment_bu,
                                         protection_level,
                                         protection_level_bu,
                                         effective_dt,
                                         expiration_dt,
										 voyage,
                                         lane_dtl_status)
              VALUES (vTCCompanyId,
                      vLaneId,
                      vRgLaneDtlSeq,
                      vCarrierCode,
                      vCarrierCodeBU,
                      vLastUpdatedSource,
                      vMot,
                      vMotBU,
                      vServicelevel,
                      vServicelevelBU,
                      vPackageId,
                      vPackageName,
                      vEquipment,
                      vEquipmentBU,
                      vProtectionlevel,
                      vProtectionlevelBU,
                      vEffectiveDT,
                      vExpirationDT,
					  VVOYAGE,
                      4);

         INSERT INTO RG_LANE_ERRORS (RG_LANE_ERROR,
                                     TC_COMPANY_ID,
                                     LANE_ID,
                                     RG_LANE_DTL_SEQ,
                                     ERROR_MSG_DESC,
                                     BATCH_ID)
              VALUES (SEQ_RG_LANE_ERROR.NEXTVAL,
                      vTCCompanyId,
                      vLaneId,
                      vRgLaneDtlSeq,
                      'Commitment Percentage Value is greated than 100',
                      vBatchId);
      WHEN eLargeNumericValue
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'IMPORT_RG_LANE_DTL: Value specified larger than allowed for the numeric column');

         INSERT INTO IMPORT_RG_LANE_DTL (tc_company_id,
                                         lane_id,
                                         rg_lane_dtl_seq,
                                         carrier_code,
                                         tp_code_bu,
                                         last_updated_source,
                                         mot,
                                         mot_bu,
                                         service_level,
                                         service_level_bu,
                                         package_id,
                                         package_name,
                                         equipment_code,
                                         equipment_bu,
                                         protection_level,
                                         protection_level_bu,
                                         effective_dt,
                                         expiration_dt,
										 voyage,
                                         lane_dtl_status)
              VALUES (vTCCompanyId,
                      vLaneId,
                      vRgLaneDtlSeq,
                      vCarrierCode,
                      vCarrierCodeBU,
                      vLastUpdatedSource,
                      vmot,
                      vmotBU,
                      vservicelevel,
                      vservicelevelBU,
                      vPackageId,
                      vPackageName,
                      vequipment,
                      vequipmentBU,
                      vprotectionlevel,
                      vprotectionlevelBU,
                      vEffectiveDT,
                      vExpirationDT,
					  VVOYAGE,
                      4);
      WHEN NO_DATA_FOUND
      THEN
         INSERT INTO RG_LANE_ERRORS (RG_LANE_ERROR,
                                     TC_COMPANY_ID,
                                     LANE_ID,
                                     RG_LANE_DTL_SEQ,
                                     ERROR_MSG_DESC,
                                     BATCH_ID)
              VALUES (
                        SEQ_RG_LANE_ERROR.NEXTVAL,
                        vTCCompanyId,
                        0,
                        vRgLaneDtlSeq,
                        'Duplicate lanes are not allowed within the same batch import. Some lane details and capacities have been lost.',
                        vBatchId);
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Procedure:IMPORT_RG_LANE_DETAILS: ' || SQLERRM);

         INSERT INTO IMPORT_RG_LANE_DTL (tc_company_id,
                                         lane_id,
                                         rg_lane_dtl_seq,
                                         carrier_code,
                                         tp_code_bu,
                                         last_updated_source,
                                         mot,
                                         mot_bu,
                                         service_level,
                                         service_level_bu,
                                         package_id,
                                         package_name,
                                         equipment_code,
                                         equipment_bu,
                                         protection_level,
                                         protection_level_bu,
                                         effective_dt,
                                         expiration_dt,
										 voyage,
                                         lane_dtl_status)
              VALUES (vTCCompanyId,
                      vLaneId,
                      vRgLaneDtlSeq,
                      vCarrierCode,
                      vCarrierCodeBU,
                      vLastUpdatedSource,
                      vmot,
                      vmotBU,
                      vservicelevel,
                      vservicelevelBU,
                      vPackageId,
                      vPackageName,
                      vequipment,
                      vequipmentBU,
                      vprotectionlevel,
                      vprotectionlevelBU,
                      vEffectiveDT,
                      vExpirationDT,
					  VVOYAGE,
                      4);

         COMMIT;
   END IMPORT_RG_LANE_DETAILS;

   PROCEDURE IMPORT_RG_LANE_SURGE (
      vSurgeCapacityId        IN import_surge_capacity.surge_capacity_id%TYPE,
      vBatchId                IN import_rg_lane.batch_id%TYPE,
      vTcCompanyId            IN import_surge_capacity.tc_company_id%TYPE,
      vRGLaneDtlSeq           IN import_surge_capacity.rg_lane_dtl_seq%TYPE,
      vEffectiveDt            IN import_surge_capacity.effective_dt%TYPE,
      vExpirationDt           IN import_surge_capacity.expiration_dt%TYPE,
      vYearlySurgeCapacity    IN import_surge_capacity.yearly_surge_capacity%TYPE,
      vMonthlySurgeCapacity   IN import_surge_capacity.monthly_surge_capacity%TYPE,
      vWeeklySurgeCapacity    IN import_surge_capacity.weekly_surge_capacity%TYPE,
      vDailySurgeCapacity     IN import_surge_capacity.daily_surge_capacity%TYPE,
      vSurgeCapacitySun       IN import_surge_capacity.surge_capacity_sun%TYPE,
      vSurgeCapacityMon       IN import_surge_capacity.surge_capacity_mon%TYPE,
      vSurgeCapacityTue       IN import_surge_capacity.surge_capacity_tue%TYPE,
      vSurgeCapacityWed       IN import_surge_capacity.surge_capacity_wed%TYPE,
      vSurgeCapacityThu       IN import_surge_capacity.surge_capacity_thu%TYPE,
      vSurgeCapacityFri       IN import_surge_capacity.surge_capacity_fri%TYPE,
      vSurgeCapacitySat       IN import_surge_capacity.surge_capacity_sat%TYPE)
   AS
      vLaneId              rg_lane.lane_id%TYPE;

      vPassFail            NUMBER;
      vFailure             NUMBER := 0;

      eNegativeSurge       EXCEPTION;
      eInvalidNumber       EXCEPTION;

      eLargeNumericValue   EXCEPTION;
      PRAGMA EXCEPTION_INIT (eLargeNumericValue, -1438);
   BEGIN
      SELECT lane_id
        INTO vLaneId
        FROM import_rg_lane
       WHERE tc_company_id = vTcCompanyId AND batch_id = vBatchId;

      -- Check if the surge capacities (weekly/Daily/DOW) should not be less than zero

      IF    (vYearlySurgeCapacity < 0)
         OR (vMonthlySurgeCapacity < 0)
         OR (vWeeklySurgeCapacity < 0)
         OR (vDailySurgeCapacity < 0)
         OR (vSurgeCapacitySun < 0)
         OR (vSurgeCapacityMon < 0)
         OR (vSurgeCapacityTue < 0)
         OR (vSurgeCapacityWed < 0)
         OR (vSurgeCapacityThu < 0)
         OR (vSurgeCapacityFri < 0)
         OR (vSurgeCapacitySat < 0)
      THEN
         RAISE eNegativeSurge;
      END IF;

      -- Check whether the proper numeric datatypes are passed for the capacities

      vPassFail := is_not_numeric (vYearlySurgeCapacity);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vMonthlySurgeCapacity);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vWeeklySurgeCapacity);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vDailySurgeCapacity);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vSurgeCapacitySun);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vSurgeCapacityMon);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vSurgeCapacityTue);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vSurgeCapacityWed);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vSurgeCapacityThu);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vSurgeCapacityFri);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      vPassFail := is_not_numeric (vSurgeCapacitySat);

      IF vPassFail = 1
      THEN
         vFailure := 1;
      END IF;

      IF vFailure = 1
      THEN
         RAISE eInvalidNumber;
      END IF;

      IF vFailure != 1
      THEN
         INSERT INTO IMPORT_SURGE_CAPACITY (surge_capacity_id,
                                            tc_company_id,
                                            lane_Id,
                                            rg_lane_dtl_seq,
                                            effective_dt,
                                            expiration_dt,
                                            yearly_surge_capacity,
                                            monthly_surge_capacity,
                                            weekly_surge_capacity,
                                            daily_surge_capacity,
                                            surge_capacity_sun,
                                            surge_capacity_mon,
                                            surge_capacity_tue,
                                            surge_capacity_wed,
                                            surge_capacity_thu,
                                            surge_capacity_fri,
                                            surge_capacity_sat)
              VALUES (vSurgeCapacityId,
                      vTcCompanyId,
                      vLaneId,
                      vRGLaneDtlSeq,
                      vEffectiveDt,
                      vExpirationDt,
                      vYearlySurgeCapacity,
                      vMonthlySurgeCapacity,
                      vWeeklySurgeCapacity,
                      vDailySurgeCapacity,
                      vSurgeCapacitySun,
                      vSurgeCapacityMon,
                      vSurgeCapacityTue,
                      vSurgeCapacityWed,
                      vSurgeCapacityThu,
                      vSurgeCapacityFri,
                      vSurgeCapacitySat);
      END IF;

      COMMIT;
   EXCEPTION
      WHEN eInvalidNumber
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Surge Capacity should be a numeric whole number');

         INSERT_RG_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRGLaneDtlSeq,
            'Import Surge Capacity: Capacity should be a numeric whole number',
            4720028,
            NULL);
      WHEN eNegativeSurge
      THEN
         DBMS_OUTPUT.PUT_LINE ('Capacity should be a greater than zero');

         INSERT_RG_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRGLaneDtlSeq,
            'Import Surge Capacity: Capacity should be a greater than zero',
            4720029,
            NULL);
      WHEN eLargeNumericValue
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Import Surge Capacity: Value specified larger than allowed for the numeric column ');

         INSERT_RG_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRGLaneDtlSeq,
            'Import Surge Capacity: Value specified larger than allowed for the numeric column',
            4720030,
            NULL);
      WHEN NO_DATA_FOUND
      THEN
         INSERT INTO RG_LANE_ERRORS (RG_LANE_ERROR,
                                     TC_COMPANY_ID,
                                     LANE_ID,
                                     RG_LANE_DTL_SEQ,
                                     ERROR_MSG_DESC,
                                     BATCH_ID)
              VALUES (
                        SEQ_RG_LANE_ERROR.NEXTVAL,
                        vTCCompanyId,
                        0,
                        vRgLaneDtlSeq,
                        'Duplicate lanes are not allowed within the same batch import. Some lane surges have been lost.',
                        vBatchId);
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE ('Procedure IMPORT_RG_LANE_SURGE: ' || SQLERRM);
         INSERT_RG_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRGLaneDtlSeq,
            'Import Surge Capacity: Database Exception Raised - '
            || SUBSTR (SQLERRM, 1, 200),
            4720031,
            SUBSTR (SQLERRM, 1, 200));
   END IMPORT_RG_LANE_SURGE;

   FUNCTION is_not_numeric (pValue IN NUMBER)
      RETURN NUMBER
   IS
      vCount   NUMBER;
   BEGIN
      SELECT COUNT (*)
        INTO vCount
        FROM DUAL
       WHERE ROUND (pValue, 10) != TRUNC (pValue);

      RETURN vCOUNT;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE ('Function is_not_numeric: ' || SQLERRM);
         RAISE;
   END is_not_numeric;

   PROCEDURE TRANSFER_IMPORT_LANES (
      vTCCompanyId    IN company.COMPANY_ID%TYPE,
      vFirstBatchId   IN rg_lane.LANE_ID%TYPE,
      vLastBatchId    IN rg_lane.LANE_ID%TYPE)
   AS
      vBusinessUnit           BUSINESS_UNIT.BUSINESS_UNIT%TYPE;
      vOLocType               rg_lane.O_LOC_TYPE%TYPE;
      vLaneHierarchy          rg_lane.LANE_HIERARCHY%TYPE;
      vOFacilityId            rg_lane.O_FACILITY_ID%TYPE;
      vOFacilityAliasId       rg_lane.O_FACILITY_ALIAS_ID%TYPE;
      vOFacilityBU            IMPORT_RG_LANE.O_FACILITY_BU%TYPE;
      vOCity                  rg_lane.O_CITY%TYPE;
      vOStateProv             rg_lane.O_STATE_PROV%TYPE;
      vOCounty                rg_lane.O_COUNTY%TYPE;
      vOPostal                rg_lane.O_POSTAL_CODE%TYPE;
      vOCountry               rg_lane.O_COUNTRY_CODE%TYPE;
      vOZone                  rg_lane.O_ZONE_ID%TYPE;
      vDLocType               rg_lane.D_LOC_TYPE%TYPE;
      vDFacilityId            rg_lane.D_FACILITY_ID%TYPE;
      vDFacilityAliasId       rg_lane.D_FACILITY_ALIAS_ID%TYPE;
      vDFacilityBU            IMPORT_RG_LANE.D_FACILITY_BU%TYPE;
      vDCity                  rg_lane.D_CITY%TYPE;
      vDStateProv             rg_lane.D_STATE_PROV%TYPE;
      vDCounty                rg_lane.D_COUNTY%TYPE;
      vDPostal                rg_lane.D_POSTAL_CODE%TYPE;
      vDCountry               rg_lane.D_COUNTRY_CODE%TYPE;
      vDZone                  rg_lane.D_ZONE_ID%TYPE;
      vRGQualifier            rg_lane.RG_QUALIFIER%TYPE;
      vFrequency              rg_lane.FREQUENCY%TYPE;
      vCustomerId             rg_lane.CUSTOMER_ID%TYPE;
      vBillingMethod          rg_lane.BILLING_METHOD%TYPE;
      vIncotermId             rg_lane.INCOTERM_ID%TYPE;
      vRouteTo                rg_lane.ROUTE_TO%TYPE;
      vRouteType1             rg_lane.ROUTE_TYPE_1%TYPE;
      vRouteType2             rg_lane.ROUTE_TYPE_2%TYPE;
      vLaneName               rg_lane.LANE_NAME%TYPE;
      vNoRating               rg_lane.NO_RATING%TYPE;
      vSelectFastestCarrier   rg_lane.USE_FASTEST%TYPE;
      vUsePreferenceBonus     rg_lane.USE_PREFERENCE_BONUS%TYPE;
	  vUseEpi             	  rg_lane.USE_EPI%TYPE;
	  vEpiServiceGroup        rg_lane.EPI_SERVICE_GROUP%TYPE;
      vImportLaneId           rg_lane.LANE_ID%TYPE;
      vLaneID                 rg_lane.LANE_ID%TYPE;
      vPassFail               NUMBER;
      vRgLaneId               rg_lane.lane_id%TYPE;
      vBatchId                import_rg_lane.batch_id%TYPE; /* CR 06/05/2003 */

      CURSOR TRANSFER_LANE_CURSOR
      IS
         SELECT lane_id,
                /*     business_unit, */
                lane_hierarchy,
                o_loc_type,
                o_facility_id,
                o_facility_alias_id,
                o_facility_bu,
                o_city,
                o_state_prov,
                o_county,
                o_postal_code,
                o_country_code,
                o_zone_id,
                d_loc_type,
                d_facility_id,
                d_facility_alias_id,
                d_facility_bu,
                d_city,
                d_state_prov,
                d_county,
                d_postal_code,
                d_country_code,
                d_zone_id,
                rg_qualifier,
                frequency,
                customer_id,
                billing_method,
                incoterm_id,
                route_to,
                route_type_1,
                route_type_2,
                lane_name,
                no_rating,
                use_fastest,
                use_preference_bonus,
				use_epi,
				epi_service_group,
                rg_lane_id,                                  /* SL 05/21/02 */
                batch_id                                   /* CR 06/05/2003 */
           FROM IMPORT_RG_LANE
          WHERE     LANE_STATUS = 1
                AND tc_company_id = vTCCompanyId
                AND BATCH_ID >= vFirstBatchId
                AND BATCH_ID <= vLastBatchId;
   BEGIN
      OPEN TRANSFER_LANE_CURSOR;

     <<TRANSFER_LANE_LOOP>>
      LOOP
         FETCH TRANSFER_LANE_CURSOR
         INTO vImportLaneId,
              /*   vBusinessUnit, */
              vLaneHierarchy,
              vOLocType,
              vOFacilityId,
              vOFacilityAliasId,
              vOFacilityBU,
              vOCity,
              vOStateProv,
              vOCounty,
              vOPostal,
              vOCountry,
              vOZone,
              vDLocType,
              vDFacilityId,
              vDFacilityAliasId,
              vDFacilityBU,
              vDCity,
              vDStateProv,
              vDCounty,
              vDPostal,
              vDCountry,
              vDZone,
              vRGQualifier,
              vFrequency,
              vCustomerId,
              vBillingMethod,
              vIncotermId,
              vRouteTo,
              vRouteType1,
              vRouteType2,
              vLaneName,
              vNoRating,
              vSelectFastestCarrier,
              vUsePreferenceBonus,
			  vUseEpi,
			  vEpiServiceGroup,
              vRgLaneId,                                     /* SL 05/21/02 */
              vBatchId;                                    /* CR 06/05/2003 */

         DBMS_OUTPUT.PUT_LINE ('RG_LANE_ID: ' || TO_CHAR (vRgLaneId));

         EXIT TRANSFER_LANE_LOOP WHEN TRANSFER_LANE_CURSOR%NOTFOUND;

         DBMS_OUTPUT.PUT_LINE ('Calling INSERT_RG_LANE....');

         vLaneId :=
            INSERT_RG_LANE (vTCCompanyId,
                            vImportLaneId,
                            /* vBusinessUnit,*/
                            vLaneHierarchy,
                            vOLocType,
                            vOFacilityId,
                            vOFacilityAliasId,
                            vOFacilityBU,
                            vOCity,
                            vOStateProv,
                            vOCounty,
                            vOPostal,
                            vOCountry,
                            vOZone,
                            vDLocType,
                            vDFacilityId,
                            vDFacilityAliasId,
                            vDFacilityBU,
                            vDCity,
                            vDStateProv,
                            vDCounty,
                            vDPostal,
                            vDCountry,
                            vDZone,
                            vRGQualifier,
                            vFrequency,
                            vCustomerId,
                            vBillingMethod,
                            vIncotermId,
                            vRouteTo,
                            vRouteType1,
                            vRouteType2,
                            vLaneName,
                            vNoRating,
                            vSelectFastestCarrier,
                            vUsePreferenceBonus,
							vUseEpi,
							vEpiServiceGroup,
                            vRgLaneId,                   /*    SL 05/21/02  */
                            vBatchId);                  /*    CR 06/05/2003 */

         IF (vLaneId <> -999)
         THEN
            DBMS_OUTPUT.PUT_LINE (
               'LANE_ID INSERTED IN RG_LANE TABLE ' || TO_CHAR (vLaneId));

            UPDATE IMPORT_RG_LANE
               SET RG_LANE_ID = vLaneId
             WHERE LANE_ID = vImportLaneID;

            COMMIT;

            TRANSFER_IMPORT_LANE_DTL (vTCCompanyId, vImportLaneID, vLaneId);

            vPassFail := DELETE_IMP_RG_LANES (vTCCompanyId, vImportLaneID);
			UPDATE_ROUT_STATUS_FLAGS(vLaneId);
         -- ELSE   there was a problem with frequency
         END IF;
      END LOOP TRANSFER_LANE_LOOP;

      CLOSE TRANSFER_LANE_CURSOR;
   END TRANSFER_IMPORT_LANES;

   PROCEDURE TRANSFER_IMPORT_LANE_DTL (
      vTCCompanyId    IN company.COMPANY_ID%TYPE,
      vImportLaneId   IN rg_lane.LANE_ID%TYPE,
      vLaneId         IN rg_lane.LANE_ID%TYPE)
   AS
      vRGLaneDtlSeq           rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE;
      vImpRGLaneDtlSeq        rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE;
      vCarrierCode            import_rg_lane_dtl.CARRIER_CODE%TYPE;
      vMot                    import_rg_lane_dtl.MOT%TYPE;
      vEquipment              import_rg_lane_dtl.EQUIPMENT_CODE%TYPE;
      vServiceLevel           import_rg_lane_dtl.SERVICE_LEVEL%TYPE;
      l_scndr_carrier_code    import_rg_lane_dtl.SCNDR_CARRIER_CODE%TYPE;
      vProtectionLevel        import_rg_lane_dtl.PROTECTION_LEVEL%TYPE;
      vTier                   rg_lane_dtl.TIER_ID%TYPE;
      vRank                   rg_lane_dtl.RANK%TYPE;
      vRepTPFlag              rg_lane_dtl.REP_TP_FLAG%TYPE;
      vYearlyCapacity         rg_lane_dtl.YEARLY_CAPACITY%TYPE;
      vMonthlyCapacity        rg_lane_dtl.MONTHLY_CAPACITY%TYPE;
      vWeeklyCapacity         rg_lane_dtl.WEEKLY_CAPACITY%TYPE;
      vDailyCapacity          rg_lane_dtl.DAILY_CAPACITY%TYPE;
      vCapacitySun            rg_lane_dtl.CAPACITY_SUN%TYPE;
      vCapacityMon            rg_lane_dtl.CAPACITY_MON%TYPE;
      vCapacityTue            rg_lane_dtl.CAPACITY_TUE%TYPE;
      vCapacityWed            rg_lane_dtl.CAPACITY_WED%TYPE;
      vCapacityThu            rg_lane_dtl.CAPACITY_THU%TYPE;
      vCapacityFri            rg_lane_dtl.CAPACITY_FRI%TYPE;
      vCapacitySat            rg_lane_dtl.CAPACITY_SAT%TYPE;
      vYearlyCommitment       rg_lane_dtl.YEARLY_COMMITMENT%TYPE;
      vMonthlyCommitment      rg_lane_dtl.MONTHLY_COMMITMENT%TYPE;
      vWeeklyCommitment       rg_lane_dtl.WEEKLY_COMMITMENT%TYPE;
      vDailyCommitment        rg_lane_dtl.DAILY_COMMITMENT%TYPE;
      vCommitmentSun          rg_lane_dtl.COMMITMENT_SUN%TYPE;
      vCommitmentMon          rg_lane_dtl.COMMITMENT_MON%TYPE;
      vCommitmentTue          rg_lane_dtl.COMMITMENT_TUE%TYPE;
      vCommitmentWed          rg_lane_dtl.COMMITMENT_WED%TYPE;
      vCommitmentThu          rg_lane_dtl.COMMITMENT_THU%TYPE;
      vCommitmentFri          rg_lane_dtl.COMMITMENT_FRI%TYPE;
      vCommitmentSat          rg_lane_dtl.COMMITMENT_SAT%TYPE;
      vYearlyCommitPct        rg_lane_dtl.YEARLY_COMMIT_PCT%TYPE;
      vMonthlyCommitPct       rg_lane_dtl.MONTHLY_COMMIT_PCT%TYPE;
      vWeeklyCommitPct        rg_lane_dtl.WEEKLY_COMMIT_PCT%TYPE;
      vDailyCommitPct         rg_lane_dtl.DAILY_COMMIT_PCT%TYPE;
      vCommitPctSun           rg_lane_dtl.COMMIT_PCT_SUN%TYPE;
      vCommitPctMon           rg_lane_dtl.COMMIT_PCT_MON%TYPE;
      vCommitPctTue           rg_lane_dtl.COMMIT_PCT_TUE%TYPE;
      vCommitPctWed           rg_lane_dtl.COMMIT_PCT_WED%TYPE;
      vCommitPctThu           rg_lane_dtl.COMMIT_PCT_THU%TYPE;
      vCommitPctFri           rg_lane_dtl.COMMIT_PCT_FRI%TYPE;
      vCommitPctSat           rg_lane_dtl.COMMIT_PCT_SAT%TYPE;
      vIsPreferred            rg_lane_dtl.IS_PREFERRED%TYPE;
      vEffectiveDT            rg_lane_dtl.EFFECTIVE_DT%TYPE;
      vExpirationDT           rg_lane_dtl.EXPIRATION_DT%TYPE;
      vImpRGDtlStatus         import_rg_lane_dtl.IMPORT_RG_DTL_STATUS%TYPE;
      vToleranceFactor        rg_lane_dtl.TT_TOLERANCE_FACTOR%TYPE;
      vOverrideCode           rg_lane_dtl.OVERRIDE_CODE%TYPE;
      vPreferenceBonusValue   rg_lane_dtl.PREFERENCE_BONUS_VALUE%TYPE;
      vCubingIndicator        rg_lane_dtl.CUBING_INDICATOR%TYPE;
      vLPNType                rg_lane_dtl.SP_LPN_TYPE%TYPE;
      vMinLPNcount            rg_lane_dtl.SP_MIN_LPN_COUNT%TYPE;
      vMaxLPNcount            rg_lane_dtl.SP_MAX_LPN_COUNT%TYPE;
      vMinWeight              rg_lane_dtl.SP_MIN_WEIGHT%TYPE;
      vMaxWeight              rg_lane_dtl.SP_MAX_WEIGHT%TYPE;
      vMinVolume              rg_lane_dtl.SP_MIN_VOLUME%TYPE;
      vMaxVolume              rg_lane_dtl.SP_MAX_VOLUME%TYPE;
      vMinLength              rg_lane_dtl.SP_MIN_LINEAR_FEET%TYPE;
      vMaxLength              rg_lane_dtl.SP_MAX_LINEAR_FEET%TYPE;
      vMinMonetaryValue       rg_lane_dtl.SP_MIN_MONETARY_VALUE%TYPE;
      vMaxMonetaryValue       rg_lane_dtl.SP_MAX_MONETARY_VALUE%TYPE;
      vMVCurrencyCode         rg_lane_dtl.SP_CURRENCY_CODE%TYPE;
      vPackageId              rg_lane_dtl.PACKAGE_ID%TYPE;
      vPackageName            rg_lane_dtl.PACKAGE_NAME%TYPE;
      vSizeUOMId              rg_lane_dtl.SIZE_UOM_ID%TYPE;
      r_Carrier_Id            rg_lane_dtl.CARRIER_ID%TYPE;
      r_Mot_Id                rg_lane_dtl.MOT_ID%TYPE;
      r_Equipment_Id          rg_lane_dtl.EQUIPMENT_ID%TYPE;
      r_service_Level_Id      rg_lane_dtl.SERVICE_LEVEL_ID%TYPE;
      r_scndr_carrier_Id      rg_lane_dtl.SCNDR_CARRIER_ID%TYPE;
      r_Protection_Level_Id   rg_lane_dtl.PROTECTION_LEVEL_ID%TYPE;
	  vVoyage				  rg_lane_dtl.VOYAGE%TYPE;
      shippingParamsMatch     NUMBER;

      CURSOR TRANSFER_LANE_DTL_CURSOR
      IS
         SELECT rg_lane_dtl_seq,
                carrier_id,
                mot_id,
                equipment_id,
                service_level_id,
                SCNDR_CARRIER_ID,
                protection_level_id,
                tier_id,
                RANK,
                rep_tp_flag,
                yearly_capacity,
                monthly_capacity,
                weekly_capacity,
                daily_capacity,
                capacity_sun,
                capacity_mon,
                capacity_tue,
                capacity_wed,
                capacity_thu,
                capacity_fri,
                capacity_sat,
                yearly_commitment,
                monthly_commitment,
                weekly_commitment,
                daily_commitment,
                commitment_sun,
                commitment_mon,
                commitment_tue,
                commitment_wed,
                commitment_thu,
                commitment_fri,
                commitment_sat,
                yearly_commit_pct,
                monthly_commit_pct,
                weekly_commit_pct,
                daily_commit_pct,
                commit_pct_sun,
                commit_pct_mon,
                commit_pct_tue,
                commit_pct_wed,
                commit_pct_thu,
                commit_pct_fri,
                commit_pct_sat,
                is_preferred,
                effective_dt,
                expiration_dt,
                import_rg_dtl_status,
                tt_tolerance_factor,
                package_id,
                package_name,
                size_uom_id,
                OVERRIDE_CODE,
                PREFERENCE_BONUS_VALUE,
                CUBING_INDICATOR,
                SP_LPN_TYPE,
                SP_MIN_LPN_COUNT,
                SP_MAX_LPN_COUNT,
                SP_MIN_WEIGHT,
                SP_MAX_WEIGHT,
                SP_MIN_VOLUME,
                SP_MAX_VOLUME,
                SP_MIN_LINEAR_FEET,
                SP_MAX_LINEAR_FEET,
                SP_MIN_MONETARY_VALUE,
                SP_MAX_MONETARY_VALUE,
                SP_CURRENCY_CODE,
				VOYAGE
           FROM IMPORT_RG_LANE_DTL
          WHERE     tc_company_id = vTCCompanyId
                AND lane_id = vImportLaneId
                AND lane_dtl_status = 1;
   BEGIN
      OPEN TRANSFER_LANE_DTL_CURSOR;

     <<TRANSFER_LANE_DTL_LOOP>>
      LOOP
         FETCH TRANSFER_LANE_DTL_CURSOR
         INTO vImpRGLaneDtlSeq,
              r_Carrier_Id,
              r_Mot_Id,
              r_Equipment_Id,
              r_Service_Level_Id,
              r_scndr_carrier_Id,
              r_Protection_Level_Id,
              vTier,
              vRank,
              vRepTPFlag,
              vYearlyCapacity,
              vMonthlyCapacity,
              vWeeklyCapacity,
              vDailyCapacity,
              vCapacitySun,
              vCapacityMon,
              vCapacityTue,
              vCapacityWed,
              vCapacityThu,
              vCapacityFri,
              vCapacitySat,
              vYearlyCommitment,
              vMonthlyCommitment,
              vWeeklyCommitment,
              vDailyCommitment,
              vCommitmentSun,
              vCommitmentMon,
              vCommitmentTue,
              vCommitmentWed,
              vCommitmentThu,
              vCommitmentFri,
              vCommitmentSat,
              vYearlyCommitPct,
              vMonthlyCommitPct,
              vWeeklyCommitPct,
              vDailyCommitPct,
              vCommitPctSun,
              vCommitPctMon,
              vCommitPctTue,
              vCommitPctWed,
              vCommitPctThu,
              vCommitPctFri,
              vCommitPctSat,
              vIsPreferred,
              vEffectiveDT,
              vExpirationDT,
              vImpRGDtlStatus,
              vToleranceFactor,
              vPackageId,
              vPackageName,
              vSizeUOMId,
              vOverrideCode,
              vPreferenceBonusValue,
              vCubingIndicator,
              vLPNType,
              vMinLPNcount,
              vMaxLPNcount,
              vMinWeight,
              vMaxWeight,
              vMinVolume,
              vMaxVolume,
              vMinLength,
              vMaxLength,
              vMinMonetaryValue,
              vMaxMonetaryValue,
              vMVCurrencyCode,
			  vVoyage;

         EXIT TRANSFER_LANE_DTL_LOOP WHEN TRANSFER_LANE_DTL_CURSOR%NOTFOUND;

         shippingParamsMatch := -1;

         shippingParamsMatch :=
            VALIDATE_OVERLAP_SHIP_PARAMS (vTCCompanyId,
                                          vImportLaneId,
                                          vLaneId,
                                          vImpRGLaneDtlSeq,
                                          r_Carrier_Id,
                                          r_Mot_Id,
                                          r_Equipment_Id,
                                          r_Service_Level_Id,
                                          r_scndr_carrier_Id,
                                          r_Protection_Level_Id,
                                          vMinVolume,
                                          vMaxVolume,
                                          vMinWeight,
                                          vMaxWeight,
                                          vMinLength,
                                          vMaxLength,
                                          vMinLPNcount,
                                          vMaxLPNcount,
                                          vLPNType,
                                          vMinMonetaryValue,
                                          vMaxMonetaryValue,
                                          vMVCurrencyCode,
                                          vEffectiveDT,
                                          vExpirationDT,
										  vVoyage);
										  
										  -- In case of exactly one record shippingParamsMatch is the vLaneDtlSeq (a value >= 0)
		 -- In case of more than one record matching shippingParamsMatch is -2
		 -- In case of no record matching shippingParamsMatch is - 1


         IF (shippingParamsMatch = -1)
         THEN
            vRGLaneDtlSeq :=
               INSERT_RG_LANE_DTL (vTCCompanyId,
                                   vLaneId,
                                   r_Carrier_Id,
                                   r_Mot_Id,
                                   r_Equipment_Id,
                                   r_service_Level_Id,
                                   r_scndr_carrier_Id,
                                   r_Protection_Level_Id,
                                   vTier,
                                   vRank,
                                   vRepTPFlag,
                                   vYearlyCapacity,
                                   vMonthlyCapacity,
                                   vWeeklyCapacity,
                                   vDailyCapacity,
                                   vCapacitySun,
                                   vCapacityMon,
                                   vCapacityTue,
                                   vCapacityWed,
                                   vCapacityThu,
                                   vCapacityFri,
                                   vCapacitySat,
                                   vYearlyCommitment,
                                   vMonthlyCommitment,
                                   vWeeklyCommitment,
                                   vDailyCommitment,
                                   vCommitmentSun,
                                   vCommitmentMon,
                                   vCommitmentTue,
                                   vCommitmentWed,
                                   vCommitmentThu,
                                   vCommitmentFri,
                                   vCommitmentSat,
                                   vYearlyCommitPct,
                                   vMonthlyCommitPct,
                                   vWeeklyCommitPct,
                                   vDailyCommitPct,
                                   vCommitPctSun,
                                   vCommitPctMon,
                                   vCommitPctTue,
                                   vCommitPctWed,
                                   vCommitPctThu,
                                   vCommitPctFri,
                                   vCommitPctSat,
                                   vIsPreferred,
                                   vEffectiveDT,
                                   vExpirationDT,
                                   vImpRGDtlStatus,
                                   vToleranceFactor,
                                   vPackageId,
                                   vPackageName,
                                   vSizeUOMId,
                                   vOverrideCode,
                                   vPreferenceBonusValue,
                                   vCubingIndicator,
                                   vLPNType,
                                   vMinLPNcount,
                                   vMaxLPNcount,
                                   vMinWeight,
                                   vMaxWeight,
                                   vMinVolume,
                                   vMaxVolume,
                                   vMinLength,
                                   vMaxLength,
                                   vMinMonetaryValue,
                                   vMaxMonetaryValue,
                                   vMVCurrencyCode,
								   vVoyage);

            TRANSFER_SURGE_CAPACITY (vTCCompanyId,
                                     vImportLaneID,
                                     vLaneId,
                                     vImpRGLaneDtlSeq,
                                     vRGLaneDtlSeq);

            TRANSFER_DAILY_UTILIZATION (vTCCompanyId,
                                        vImportLaneID,
                                        vLaneId,
                                        vImpRGLaneDtlSeq,
                                        vRGLaneDtlSeq);

            DELETE FROM IMPORT_RG_LANE_DTL
                  WHERE     TC_COMPANY_ID = vTCCompanyId
                        AND LANE_ID = vImportLaneId
                        AND RG_LANE_DTL_SEQ = vImpRGLaneDtlSeq
                        AND lane_dtl_status != 4;

            ADJUST_EFF_EXP_DT (vTCCompanyId,
                               vLaneId,
                               vRGLaneDtlSeq,
                               vEffectiveDT,
                               vExpirationDT,
                               r_Carrier_Id,
                               r_Mot_Id,
                               r_Equipment_Id,
                               r_service_Level_Id,
                               r_scndr_carrier_Id,
                               r_Protection_Level_Id,
							   vVoyage);

            COMMIT;
			
			ELSE
			
			IF (shippingParamsMatch >= 0)
			THEN
			
				vRGLaneDtlSeq := shippingParamsMatch;
						

					UPDATE comb_lane_dtl 
					SET effective_dt = vEffectiveDT,
					  expiration_dt = vExpirationDT,
					  sp_min_volume = vMinVolume,
					  sp_max_volume = vMaxVolume,
					  sp_min_weight = vMinWeight,
					  sp_max_weight = vMaxWeight,
					  sp_min_linear_feet = vMinLength,
					  sp_max_linear_feet = vMaxLength,
					  sp_min_lpn_count = vMinLpnCount,
					  sp_max_lpn_count = vMaxLpnCount,
					  sp_lpn_type = vLpnType,
					  sp_min_monetary_value = vMinMonetaryValue,
					  sp_max_monetary_value = vMaxMonetaryValue,
					  sp_currency_code = vMVCurrencyCode,
				      tier_id =   vTier,  
					  rank =   vRank,
					  rep_tp_flag =   vRepTPFlag,
					  yearly_capacity =   vYearlyCapacity,
					  montly_capacity =   vMonthlyCapacity,
					  weekly_capacity =   vWeeklyCapacity,
					  daily_capacity =   vDailyCapacity,
					  capacity_sun =   vCapacitySun,
					  capacity_mon =   vCapacityMon,
					  capacity_tue =   vCapacityTue,
					  capacity_wed =   vCapacityWed,
					  capacity_thu =   vCapacityThu,
					  capacity_fri =   vCapacityFri,
					  capacity_sat =   vCapacitySat,
					  yearly_commitment =   vYearlyCommitment,
					  montly_commitment =   vMonthlyCommitment,
					  weekly_commitment =   vWeeklyCommitment,
					  daily_commitment =   vDailyCommitment,
					  commitment_sun =   vCommitmentSun,
					  commitment_mon =   vCommitmentMon,
					  commitment_tue =   vCommitmentTue,
					  commitment_wed =   vCommitmentWed,
					  commitment_thu =   vCommitmentThu,
					  commitment_fri =   vCommitmentFri,
					  commitment_sat =   vCommitmentSat,
					  yearly_commit_pct =   vYearlyCommitPct,
					  montly_commit_pct =   vMonthlyCommitPct,
					  weekly_commit_pct =   vWeeklyCommitPct,
					  daily_commit_pct =   vDailyCommitPct,
					  commit_pct_sun =   vCommitPctSun,
					  commit_pct_mon =   vCommitPctMon,
					  commit_pct_tue =   vCommitPctTue,
					  commit_pct_wed =   vCommitPctWed,
					  commit_pct_thu =   vCommitPctThu,
					  commit_pct_fri =   vCommitPctFri,
					  commit_pct_sat =   vCommitPctSat,
					  is_preferred =   vIsPreferred,
					  tt_tolerance_factor =   vToleranceFactor,
					  package_id =   vPackageId,
					  package_name =   vPackageName, 
					  size_uom_id =   vSizeUOMId,
					  override_code =   vOverrideCode,
					  preference_bonus_value =   vPreferenceBonusValue,
					  cubing_indicator =   vCubingIndicator,
					  last_updated_source_type = 3,
					  last_updated_source = 'OMS', 
					  last_updated_dttm = sysdate
					  WHERE lane_id = vLaneId 
					  AND lane_dtl_seq = vRGLaneDtlSeq;
					
				DELETE FROM SURGE_CAPACITY 
					WHERE LANE_ID = vLaneId
					AND RG_LANE_DTL_SEQ = vRGLaneDtlSeq;
					
				TRANSFER_SURGE_CAPACITY (vTCCompanyId,
                                     vImportLaneID,
                                     vLaneId,
                                     vImpRGLaneDtlSeq,
                                     vRGLaneDtlSeq);
									 
				DELETE FROM DAILY_UTILIZATION
					WHERE LANE_ID = vLaneId
					AND RG_LANE_DTL_SEQ = vRGLaneDtlSeq;

				TRANSFER_DAILY_UTILIZATION (vTCCompanyId,
											vImportLaneID,
											vLaneId,
											vImpRGLaneDtlSeq,
											vRGLaneDtlSeq);

				DELETE FROM IMPORT_RG_LANE_DTL
					  WHERE     TC_COMPANY_ID = vTCCompanyId
							AND LANE_ID = vImportLaneId
							AND RG_LANE_DTL_SEQ = vImpRGLaneDtlSeq
							AND lane_dtl_status != 4;
							
				COMMIT;
				
			END IF;
			
         END IF;
      END LOOP TRANSFER_LANE_DTL_LOOP;

      CLOSE TRANSFER_LANE_DTL_CURSOR;
   END TRANSFER_IMPORT_LANE_DTL;

   PROCEDURE TRANSFER_SURGE_CAPACITY (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vImportLaneId      IN surge_capacity.LANE_ID%TYPE,
      vLaneId            IN surge_capacity.LANE_ID%TYPE,
      vImpRGLaneDtlSeq   IN surge_capacity.RG_LANE_DTL_SEQ%TYPE,
      vRGLaneDtlSeq      IN surge_capacity.RG_LANE_DTL_SEQ%TYPE)
   AS
      vEffectiveDT       surge_capacity.EFFECTIVE_DT%TYPE;
      vExpirationDT      surge_capacity.EXPIRATION_DT%TYPE;
      vYearlyCapacity    surge_capacity.YEARLY_SURGE_CAPACITY%TYPE;
      vMonthlyCapacity   surge_capacity.MONTHLY_SURGE_CAPACITY%TYPE;
      vWeeklyCapacity    surge_capacity.WEEKLY_SURGE_CAPACITY%TYPE;
      vDailyCapacity     surge_capacity.DAILY_SURGE_CAPACITY%TYPE;
      vCapacitySun       surge_capacity.SURGE_CAPACITY_SUN%TYPE;
      vCapacityMon       surge_capacity.SURGE_CAPACITY_MON%TYPE;
      vCapacityTue       surge_capacity.SURGE_CAPACITY_TUE%TYPE;
      vCapacityWed       surge_capacity.SURGE_CAPACITY_WED%TYPE;
      vCapacityThu       surge_capacity.SURGE_CAPACITY_THU%TYPE;
      vCapacityFri       surge_capacity.SURGE_CAPACITY_FRI%TYPE;
      vCapacitySat       surge_capacity.SURGE_CAPACITY_SAT%TYPE;

      CURSOR IMPORTED_SURGE_CURSOR
      IS
         SELECT EFFECTIVE_DT,
                EXPIRATION_DT,
                YEARLY_SURGE_CAPACITY,
                MONTHLY_SURGE_CAPACITY,
                WEEKLY_SURGE_CAPACITY,
                DAILY_SURGE_CAPACITY,
                SURGE_CAPACITY_SUN,
                SURGE_CAPACITY_MON,
                SURGE_CAPACITY_TUE,
                SURGE_CAPACITY_WED,
                SURGE_CAPACITY_THU,
                SURGE_CAPACITY_FRI,
                SURGE_CAPACITY_SAT
           FROM IMPORT_SURGE_CAPACITY
          WHERE     TC_COMPANY_ID = vTCCompanyId
                AND LANE_ID = vImportLaneId
                AND RG_LANE_DTL_SEQ = vImpRGLaneDtlSeq;
   BEGIN
      OPEN IMPORTED_SURGE_CURSOR;

     <<IMPORTED_SURGE_LOOP>>
      LOOP
         FETCH IMPORTED_SURGE_CURSOR
         INTO vEffectiveDT,
              vExpirationDT,
              vYearlyCapacity,
              vMonthlyCapacity,
              vWeeklyCapacity,
              vDailyCapacity,
              vCapacitySun,
              vCapacityMon,
              vCapacityTue,
              vCapacityWed,
              vCapacityThu,
              vCapacityFri,
              vCapacitySat;

         EXIT IMPORTED_SURGE_LOOP WHEN IMPORTED_SURGE_CURSOR%NOTFOUND;

         /*    DBMS_OUTPUT.PUT_LINE('Inserting Import Surge Capacity...');
             DBMS_OUTPUT.PUT_LINE('vImportLaneId: ' || TO_CHAR(vImportLaneId)) ;
             DBMS_OUTPUT.PUT_LINE('vLaneId: ' || TO_CHAR(vLaneId) || ' vRGLaneDtlSeq: ' || TO_CHAR(vRGLaneDtlSeq));  */

         INSERT_SURGE_CAPACITY (vTCCompanyId,
                                vLaneId,
                                vRGLaneDtlSeq,
                                vEffectiveDT,
                                vExpirationDT,
                                vYearlyCapacity,
                                vMonthlyCapacity,
                                vWeeklyCapacity,
                                vDailyCapacity,
                                vCapacitySun,
                                vCapacityMon,
                                vCapacityTue,
                                vCapacityWed,
                                vCapacityThu,
                                vCapacityFri,
                                vCapacitySat);
      END LOOP IMPORTED_SURGE_LOOP;

      CLOSE IMPORTED_SURGE_CURSOR;

      -- S Lal 05/22/02   Delete the data from the IMPORT_SURGE_CAPACITY table

      DELETE FROM import_surge_capacity
            WHERE     tc_company_id = vTCCompanyId
                  AND lane_id = vImportLaneId
                  AND RG_LANE_DTL_SEQ = vImpRGLaneDtlSeq;
   END TRANSFER_SURGE_CAPACITY;

   PROCEDURE TRANSFER_DAILY_UTILIZATION (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vImportLaneId      IN surge_capacity.LANE_ID%TYPE,
      vLaneId            IN surge_capacity.LANE_ID%TYPE,
      vImpRGLaneDtlSeq   IN surge_capacity.RG_LANE_DTL_SEQ%TYPE,
      vRGLaneDtlSeq      IN surge_capacity.RG_LANE_DTL_SEQ%TYPE)
   AS
      vUtilizationDT      daily_utilization.UTILIZATION_DATE%TYPE;
      vDailyUtilization   daily_utilization.DAILY_UTILIZATION%TYPE;

      CURSOR IMPORTED_DAILY_CURSOR
      IS
         SELECT UTILIZATION_DATE, DAILY_UTILIZATION
           FROM IMPORT_DAILY_UTILIZATION
          WHERE     TC_COMPANY_ID = vTCCompanyId
                AND LANE_ID = vImportLaneId
                AND RG_LANE_DTL_SEQ = vImpRGLaneDtlSeq;
   BEGIN
      OPEN IMPORTED_DAILY_CURSOR;

     <<IMPORTED_DAILY_LOOP>>
      LOOP
         FETCH IMPORTED_DAILY_CURSOR
         INTO vUtilizationDT, vDailyUtilization;

         EXIT IMPORTED_DAILY_LOOP WHEN IMPORTED_DAILY_CURSOR%NOTFOUND;
         INSERT_DAILY_UTILIZATION (vTCCompanyId,
                                   vUtilizationDT,
                                   vLaneId,
                                   vRGLaneDtlSeq,
                                   vDailyUtilization);
      END LOOP IMPORTED_DAILY_LOOP;

      CLOSE IMPORTED_DAILY_CURSOR;
   END TRANSFER_DAILY_UTILIZATION;

   PROCEDURE VALIDATE_IMPORT_LANES (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vFirstBatchId   IN rg_lane.LANE_ID%TYPE,
      vLastBatchId    IN rg_lane.LANE_ID%TYPE)
   AS
      vLaneId   rg_lane.LANE_ID%TYPE;

      CURSOR IMPORTED_LANES_CURSOR
      IS
         SELECT LANE_ID
           FROM IMPORT_RG_LANE
          WHERE     TC_COMPANY_ID = vTCCompanyId
                AND LANE_STATUS = 3
                AND BATCH_ID >= vFirstBatchId
                AND BATCH_ID <= vLastBatchId;
   BEGIN
      OPEN IMPORTED_LANES_CURSOR;

     <<IMPORTED_LANES_LOOP>>
      LOOP
         FETCH IMPORTED_LANES_CURSOR INTO vLaneId;

         EXIT IMPORTED_LANES_LOOP WHEN IMPORTED_LANES_CURSOR%NOTFOUND;

         VALIDATE_LANE (vTCCompanyId, vLaneId);
      END LOOP IMPORTED_LANES_LOOP;

      CLOSE IMPORTED_LANES_CURSOR;

      TRANSFER_IMPORT_LANES (vTCCompanyId, vFirstBatchId, vLastBatchId);
   END VALIDATE_IMPORT_LANES;

   PROCEDURE VALIDATE_LANE (vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
                            vLaneId        IN rg_lane.LANE_ID%TYPE)
   AS
      vBusinessUnit             BUSINESS_UNIT.BUSINESS_UNIT%TYPE;
      vOLocType                 rg_lane.O_LOC_TYPE%TYPE;
      vOFacilityId              rg_lane.O_FACILITY_ID%TYPE;
      vOFacilityAliasId         rg_lane.O_FACILITY_ALIAS_ID%TYPE;
      vOFacilityBU              BUSINESS_UNIT.BUSINESS_UNIT%TYPE;
      vOCity                    rg_lane.O_CITY%TYPE;
      vOStateProv               rg_lane.O_STATE_PROV%TYPE;
      vOCounty                  rg_lane.O_COUNTY%TYPE;
      vOPostal                  rg_lane.O_POSTAL_CODE%TYPE;
      vOCountry                 rg_lane.O_COUNTRY_CODE%TYPE;
      vOZoneId                  rg_lane.O_ZONE_ID%TYPE;
      vOZoneName                import_rg_lane.O_ZONE_NAME%TYPE;
      vDLocType                 rg_lane.D_LOC_TYPE%TYPE;
      vDFacilityId              rg_lane.D_FACILITY_ID%TYPE;
      vDFacilityAliasId         rg_lane.D_FACILITY_ALIAS_ID%TYPE;
      vDFacilityBU              BUSINESS_UNIT.BUSINESS_UNIT%TYPE;
      vDCity                    rg_lane.D_CITY%TYPE;
      vDStateProv               rg_lane.D_STATE_PROV%TYPE;
      vDCounty                  rg_lane.D_COUNTY%TYPE;
      vDPostal                  rg_lane.D_POSTAL_CODE%TYPE;
      vDCountry                 rg_lane.D_COUNTRY_CODE%TYPE;
      vDZoneId                  rg_lane.D_ZONE_ID%TYPE;
      vDZoneName                import_rg_lane.D_ZONE_NAME%TYPE;

      vRgLaneId                 IMPORT_RG_LANE.RG_LANE_ID%TYPE;

      vPassFail                 NUMBER;
      vFailure                  NUMBER;
      vRgQualifier              rg_lane.rg_qualifier%TYPE;
      vFrequency                rg_lane.FREQUENCY%TYPE;
      vCustomerId               rg_lane.CUSTOMER_ID%TYPE;
      vCustomerCode             import_rg_lane.CUSTOMER_CODE%TYPE;
      vCustomerCodeBU           import_rg_lane.CUSTOMER_CODE_BU%TYPE;
      vBillingMethod            rg_lane.BILLING_METHOD%TYPE;
      vBillingMethodCode        import_rg_lane.BILLING_METHOD_CODE%TYPE;
      vIncotermId               rg_lane.INCOTERM_ID%TYPE;
      vIncotermName             import_rg_lane.INCOTERM_NAME%TYPE;
      vIncotermNameBU           import_rg_lane.INCOTERM_NAME_BU%TYPE;
      vLaneName                 rg_lane.LANE_NAME%TYPE;
      vRouteTo			import_rg_lane.ROUTE_TO%TYPE;
      vRouteType1		import_rg_lane.ROUTE_TYPE_1%TYPE;
      vRouteType2		import_rg_lane.ROUTE_TYPE_2%TYPE;
	  vUseEpi			import_rg_lane.USE_EPI%TYPE;
	  vEpiServiceGroup	import_rg_lane.EPI_SERVICE_GROUP%TYPE;
      vTempDuplicateLaneCount   NUMBER;
      vCount                    NUMBER;
      VERRORMSGDESC1            VARCHAR2 (1000);
      VERRORID                  NUMBER;
      VERRORLIST                VARCHAR2 (255);

      CURSOR LANE_CURSOR
      IS
         SELECT BUSINESS_UNIT,
                O_LOC_TYPE,
                O_FACILITY_ID,
                O_FACILITY_ALIAS_ID,
                O_FACILITY_BU,
                O_CITY,
                O_STATE_PROV,
                O_COUNTY,
                O_POSTAL_CODE,
                O_COUNTRY_CODE,
                O_ZONE_ID,
                O_ZONE_NAME,
                D_LOC_TYPE,
                D_FACILITY_ID,
                D_FACILITY_ALIAS_ID,
                D_FACILITY_BU,
                D_CITY,
                D_STATE_PROV,
                D_COUNTY,
                D_POSTAL_CODE,
                DECODE (D_COUNTRY_CODE, '', NULL, D_COUNTRY_CODE)
                   D_COUNTRY_CODE,
                D_ZONE_ID,
                D_ZONE_NAME,
                rg_qualifier,
                FREQUENCY,
                CUSTOMER_ID,
                CUSTOMER_CODE,
                CUSTOMER_CODE_BU,
                BILLING_METHOD,
                BILLING_METHOD_CODE,
                INCOTERM_ID,
                INCOTERM_NAME,
                INCOTERM_NAME_BU,
                LANE_NAME,
                RG_LANE_ID,
                                ROUTE_TO,
		                ROUTE_TYPE_1,
                ROUTE_TYPE_2,
				USE_EPI,
				EPI_SERVICE_GROUP 
           FROM IMPORT_RG_LANE
          WHERE TC_COMPANY_ID = vTCCompanyId AND LANE_ID = vLaneId;
   BEGIN
      OPEN LANE_CURSOR;

     <<LANES_LOOP>>
      LOOP
         FETCH LANE_CURSOR
         INTO vBusinessUnit,
              vOLocType,
              vOFacilityId,
              vOFacilityAliasId,
              vOFacilityBU,
              vOCity,
              vOStateProv,
              vOCounty,
              vOPostal,
              vOCountry,
              vOZoneId,
              vOZoneName,
              vDLocType,
              vDFacilityId,
              vDFacilityAliasId,
              vDFacilityBU,
              vDCity,
              vDStateProv,
              vDCounty,
              vDPostal,
              vDCountry,
              vDZoneId,
              vDZoneName,
              vRgQualifier,
              vFrequency,
              vCustomerId,
              vCustomerCode,
              vCustomerCodeBU,
              vBillingMethod,
              vBillingMethodCode,
              vIncotermId,
              vIncotermName,
              vIncotermNameBU,
              vLaneName,
              vRgLaneId,
                            vRouteTo,
	                    vRouteType1,
              vRouteType2,
			  vUseEpi,
			  vEpiServiceGroup;

         EXIT LANES_LOOP WHEN LANE_CURSOR%NOTFOUND;

         /* Moved to validate_lane_ui

         DELETE FROM RG_LANE_ERRORS
          WHERE TC_COMPANY_ID = vTCCompanyId
              AND LANE_ID = vLaneId;

       */

         COMMIT;

         /*
         vFailure := 0;

         vPassFail := VALIDATE_LANE_ADDRESSES(vTCCompanyId, vLaneId, vOPostal,
                                              vOStateProv, vOCountry);

       IF (vPassFail = 1) THEN
             vFailure := 1;
       END IF;
       */

         vPassFail :=
            VALIDATE_LANE_BASE_DATA (vTCCompanyId,
                                     vLaneId,
                                     vBusinessUnit,
                                     vOFacilityId,
                                     vOFacilityAliasId,
                                     vOFacilityBU,
                                     vOStateProv,
                                     vOPostal,
                                     vOCountry,
                                     vOZoneId,
                                     vOZoneName,
                                     vDFacilityId,
                                     vDFacilityAliasId,
                                     vDFacilityBU,
                                     vDStateProv,
                                     vDPostal,
                                     vDCountry,
                                     vDZoneId,
                                     vDZoneName,
                                     vCustomerId,
                                     vCustomerCode,
                                     vCustomerCodeBU,
                                     vBillingMethod,
                                     vBillingMethodCode,
                                     vIncotermId,
                                     vIncotermName,
                                     vIncotermNameBU);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
         END IF;

         IF (vFrequency IS NULL)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_ERROR (vTCCompanyId,
                                  vLaneId,
                                  'Frequency is required.',
                                  4720002,
                                  NULL);
         ELSE
            SELECT COUNT (FREQUENCY)
              INTO vCount
              FROM FREQUENCY
             WHERE FREQUENCY = vFrequency;

            IF (vCount < 1)
            THEN
               vFailure := 1;
               INSERT_RG_LANE_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  'Frequency ' || vFrequency || ' is invalid.',
                  4720003,
                  vFrequency);
            END IF;
         END IF;

         -- Validate Parent level of Lane

         vPassFail :=
            VALIDATE_HIERARCHY (vTCCompanyId       => vTCCompanyId,
                                vLaneId            => vLaneId,
                                oFacilityId        => vOFacilityId,
                                oFacilityAliasId   => vOFacilityAliasId,
                                oCity              => vOCity,
                                oState             => vOStateProv,
                                oCounty            => vOCounty,
                                oPostal            => vOPostal,
                                oCountry           => vOCountry,
                                oZoneId            => vOZoneId,
                                oZoneName          => vOZoneName,
                                dFacilityId        => vDFacilityId,
                                dFacilityAliasId   => vDFacilityAliasId,
                                dState             => vDStateProv,
                                dCity              => vDCity,
                                dCounty            => vDCounty,
                                dCountry           => vDCountry,
                                dPostal            => vDPostal,
                                dZoneId            => vDZoneId,
                                dZoneName          => vDZoneName,
				routeTo	   	   => vRouteTo,
				routeType1	   => vRouteType1,
				routeType2	   => vRouteType2);


         IF (vPassFail = 1)
         THEN
            vFailure := 1;
         END IF;

         -- Validate the Routing Guide Qualifier if present

         vPassFail :=
            validate_rg_qualifier (vTCCompanyId, vLaneId, vRgQualifier);
		
		 IF (vEpiServiceGroup IS NOT NULL)
		 THEN
			vPassFail := VALIDATE_EPI_SERVICE_GROUP(vTCCompanyId, vLaneId, vUseEpi, vEpiServiceGroup);
			IF (vPassFail = 1)
			THEN
				vFailure := 1;
			END IF;
		 END IF;         

         IF (vFailure = 1)
         THEN
            UPDATE IMPORT_RG_LANE
               SET LANE_STATUS = 4,
                   LAST_UPDATED_DTTM = SYSDATE,
                   LAST_UPDATED_SOURCE_TYPE = 3,
                   LAST_UPDATED_SOURCE = 'OMS'
             WHERE TC_COMPANY_ID = vTCCompanyId AND LANE_ID = vLaneId;

            COMMIT;
         ELSE
            UPDATE IMPORT_RG_LANE
               SET LANE_STATUS = 1,
                   LAST_UPDATED_DTTM = SYSDATE,
                   LAST_UPDATED_SOURCE_TYPE = 3,
                   LAST_UPDATED_SOURCE = 'OMS'
             WHERE TC_COMPANY_ID = vTCCompanyId AND LANE_ID = vLaneId;

            COMMIT;
         END IF;

         VALIDATE_LANE_DTL (vTcCompanyId, vLaneId);
      END LOOP LANES_LOOP;

      CLOSE LANE_CURSOR;
   END VALIDATE_LANE;

   PROCEDURE VALIDATE_LANE_UI                 -- Call from the UI for TT 13815
                              (vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
                               vRgLaneId      IN rg_lane.LANE_ID%TYPE)
   IS
      CURSOR lane_list
      IS
         SELECT tc_company_id, lane_id, batch_id
           FROM import_rg_lane
          WHERE -- tc_company_id = vTCCompanyId AND
                rg_lane_id = vRgLaneId;
   BEGIN
      FOR c_lane_list IN lane_list
      LOOP
         INITIALIZE_LANE_DTL_STATUS (c_lane_list.tc_company_id,
                                     c_lane_list.lane_id);

         DELETE FROM RG_LANE_ERRORS
               WHERE TC_COMPANY_ID = c_lane_list.tc_company_id
                     AND LANE_ID = c_lane_list.lane_id;

         validate_lane (c_lane_list.tc_company_id, c_lane_list.lane_id);
         TRANSFER_IMPORT_LANES (c_lane_list.tc_company_id,
                                c_lane_list.batch_id,
                                c_lane_list.batch_id);
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE ('Procedure VALIDATE_LANE_UI: ' || SQLERRM);
         RAISE;
   END VALIDATE_LANE_UI;

   PROCEDURE INITIALIZE_LANE_DTL_STATUS (
      vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
      vLaneId        IN rg_lane.LANE_ID%TYPE)
   AS
   BEGIN
      UPDATE IMPORT_RG_LANE_DTL
         SET LANE_DTL_STATUS = 3                                -- 3=imported.
       WHERE TC_COMPANY_ID = vTCCompanyId AND LANE_ID = vLaneId;

      COMMIT;
   END INITIALIZE_LANE_DTL_STATUS;

   PROCEDURE VALIDATE_LANE_DTL (vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
                                vLaneId        IN rg_lane.lane_id%TYPE /* SL  05/22/02  */
                                                                      )
   AS
      vCarrierCode               IMPORT_RG_LANE_DTL.CARRIER_CODE%TYPE;
      vMode                      IMPORT_RG_LANE_DTL.MOT%TYPE;
      vServiceLevel              IMPORT_RG_LANE_DTL.SERVICE_LEVEL%TYPE;
      vSizeUOM                   IMPORT_RG_LANE_DTL.SIZE_UOM%TYPE;
      vEquipment                 IMPORT_RG_LANE_DTL.EQUIPMENT_CODE%TYPE;
      vProtectionLevel           IMPORT_RG_LANE_DTL.PROTECTION_LEVEL%TYPE;

      vCarrierCodeBU             IMPORT_RG_LANE_DTL.TP_CODE_BU%TYPE;
      vModeBU                    IMPORT_RG_LANE_DTL.MOT_BU%TYPE;
      vServiceLevelBU            IMPORT_RG_LANE_DTL.SERVICE_LEVEL_BU%TYPE;
      vSizeUOMBU                 IMPORT_RG_LANE_DTL.SIZE_UOM_BU%TYPE;
      vEquipmentBU               IMPORT_RG_LANE_DTL.EQUIPMENT_BU%TYPE;
      vProtectionLevelBU         IMPORT_RG_LANE_DTL.PROTECTION_LEVEL_BU%TYPE;

      vPassFail                  NUMBER;
      vFailure                   NUMBER;
      vRGLaneDtlSeq              rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE;
      vEffectiveDt               rg_lane_dtl.effective_dt%TYPE;
      vExpirationDt              rg_lane_dtl.expiration_dt%TYPE;
      vRepTpFlag                 rg_lane_dtl.rep_tp_flag%TYPE;
      l_scndr_carrier_code       IMPORT_RG_LANE_DTL.SCNDR_CARRIER_CODE%TYPE;
      l_scndr_carrier_codeBU     IMPORT_RG_LANE_DTL.SECONDARY_CARRIER_CODE_BU%TYPE;

      vCarrierCodeBUID           COMPANY.COMPANY_ID%TYPE;
      vModeBUID                  COMPANY.COMPANY_ID%TYPE;
      vServiceLevelBUID          COMPANY.COMPANY_ID%TYPE;
      vSizeUOMBUID               IMPORT_RG_LANE_DTL.SIZE_UOM_ID%TYPE;
      vEquipmentBUID             COMPANY.COMPANY_ID%TYPE;
      vProtectionLevelBUID       COMPANY.COMPANY_ID%TYPE;
      l_scndr_carrier_codeBUID   COMPANY.COMPANY_ID%TYPE;
      vErrorCount                PLS_INTEGER;                --[TT #45901 fix]
      vRootCompanyId             COMPANY.COMPANY_ID%TYPE;
      vCarrierId                 CARRIER_CODE.CARRIER_ID%TYPE;

      vUsePreferenceBonus        IMPORT_RG_LANE.USE_PREFERENCE_BONUS%TYPE;
      vPrefBonusValue            IMPORT_RG_LANE_DTL.Preference_Bonus_Value%TYPE;

      vLPNType                   rg_lane_dtl.SP_LPN_TYPE%TYPE;
      vMinLPNcount               rg_lane_dtl.SP_MIN_LPN_COUNT%TYPE;
      vMaxLPNcount               rg_lane_dtl.SP_MAX_LPN_COUNT%TYPE;
      vMinWeight                 rg_lane_dtl.SP_MIN_WEIGHT%TYPE;
      vMaxWeight                 rg_lane_dtl.SP_MAX_WEIGHT%TYPE;
      vMinVolume                 rg_lane_dtl.SP_MIN_VOLUME%TYPE;
      vMaxVolume                 rg_lane_dtl.SP_MAX_VOLUME%TYPE;
      vMinLength                 rg_lane_dtl.SP_MIN_LINEAR_FEET%TYPE;
      vMaxLength                 rg_lane_dtl.SP_MAX_LINEAR_FEET%TYPE;
      vMinMonetaryValue          rg_lane_dtl.SP_MIN_MONETARY_VALUE%TYPE;
      vMaxMonetaryValue          rg_lane_dtl.SP_MAX_MONETARY_VALUE%TYPE;
      vMVCurrencyCode            rg_lane_dtl.SP_CURRENCY_CODE%TYPE;
	  vRank              IMPORT_RG_LANE_DTL.RANK%TYPE;
      vCarrierCodeCount          SMALLINT;
	  vUseEpi					 IMPORT_RG_LANE.USE_EPI%TYPE;
	  vEpiServiceGroup			 IMPORT_RG_LANE.EPI_SERVICE_GROUP%TYPE;
	  vCurrDate					 DATE;

      CURSOR LANE_DTL_CURSOR
      IS
         SELECT CARRIER_CODE,
                TP_CODE_BU,
                MOT,
                MOT_BU,
                SERVICE_LEVEL,
                SERVICE_LEVEL_BU,
                EQUIPMENT_CODE,
                EQUIPMENT_BU,
                PROTECTION_LEVEL,
                PROTECTION_LEVEL_BU,
                RG_LANE_DTL_SEQ,
                EFFECTIVE_DT,
                EXPIRATION_DT,                              /* SL 05/27/02  */
                REP_TP_FLAG,                                    -- SL 07/11/02
                SCNDR_CARRIER_CODE,
                SECONDARY_CARRIER_CODE_BU,
                SIZE_UOM,
                SIZE_UOM_BU,
                CARRIER_ID,
                PREFERENCE_BONUS_VALUE,
                SP_LPN_TYPE,
                SP_MIN_LPN_COUNT,
                SP_MAX_LPN_COUNT,
                SP_MIN_WEIGHT,
                SP_MAX_WEIGHT,
                SP_MIN_VOLUME,
                SP_MAX_VOLUME,
                SP_MIN_LINEAR_FEET,
                SP_MAX_LINEAR_FEET,
                SP_MIN_MONETARY_VALUE,
                SP_MAX_MONETARY_VALUE,
                SP_CURRENCY_CODE,
				RANK
           FROM IMPORT_RG_LANE_DTL
          WHERE     TC_COMPANY_ID = vTCCompanyId
                AND LANE_ID = vLaneId
                AND LANE_DTL_STATUS = 3;
   --AND LANE_DTL_STATUS in (3, 4);
   BEGIN
      OPEN LANE_DTL_CURSOR;

     <<LANE_DTL_LOOP>>
      LOOP
         FETCH LANE_DTL_CURSOR
         INTO vCarrierCode,
              vCarrierCodeBU,
              vMode,
              vModeBU,
              vServiceLevel,
              vServiceLevelBU,
              vEquipment,
              vEquipmentBU,
              vProtectionLevel,
              vProtectionLevelBU,
              vRGLaneDtlSeq,
              vEffectiveDt,
              vExpirationDt,
              vRepTpFlag,
              l_scndr_carrier_code,
              l_scndr_carrier_codeBU,
              vSizeUOM,
              vSizeUOMBU,
              vCarrierId,
              vPrefBonusValue,
              vLPNType,
              vMinLPNcount,
              vMaxLPNcount,
              vMinWeight,
              vMaxWeight,
              vMinVolume,
              vMaxVolume,
              vMinLength,
              vMaxLength,
              vMinMonetaryValue,
              vMaxMonetaryValue,
              vMVCurrencyCode,
			  vRank;

         EXIT LANE_DTL_LOOP WHEN LANE_DTL_CURSOR%NOTFOUND;

         vPassFail := 0;
         vFailure := 0;

         SELECT ROOTCOMPANYID
           INTO vRootCompanyId
           FROM IMPORT_RG_LANE
          WHERE LANE_ID = vLaneId;

         SELECT USE_PREFERENCE_BONUS, USE_EPI, EPI_SERVICE_GROUP 
           INTO vUsePreferenceBonus, vUseEpi, vEpiServiceGroup 
           FROM IMPORT_RG_LANE
          WHERE LANE_ID = vLaneId AND TC_COMPANY_ID = vTCCompanyId;

         IF (vCarrierCodeBU IS NULL)
         THEN
            vCarrierCodeBUID := vRootCompanyId;
         ELSE
            vCarrierCodeBUID :=
               GET_BUID_FROM_BUSINESS_UNIT (vRootCompanyId, vCarrierCodeBU);
         END IF;

         IF (vModeBU IS NULL)
         THEN
            vModeBUID := vRootCompanyId;
         ELSE
            vModeBUID := GET_BUID_FROM_BUSINESS_UNIT (vRootCompanyId, vModeBU);
         END IF;

         IF (vServiceLevelBU IS NULL)
         THEN
            vServiceLevelBUID := vRootCompanyId;
         ELSE
            vServiceLevelBUID :=
               GET_BUID_FROM_BUSINESS_UNIT (vRootCompanyId, vServiceLevelBU);
         END IF;

         IF (vEquipmentBU IS NULL)
         THEN
            vEquipmentBUID := vRootCompanyId;
         ELSE
            vEquipmentBUID :=
               GET_BUID_FROM_BUSINESS_UNIT (vRootCompanyId, vEquipmentBU);
         END IF;

         IF (vProtectionLevelBU IS NULL)
         THEN
            vProtectionLevelBUID := vRootCompanyId;
         ELSE
            vProtectionLevelBUID :=
               GET_BUID_FROM_BUSINESS_UNIT (vRootCompanyId,
                                            vProtectionLevelBU);
         END IF;

         IF (l_scndr_carrier_codeBU IS NULL)
         THEN
            l_scndr_carrier_codeBUID := vRootCompanyId;
         ELSE
            l_scndr_carrier_codeBUID :=
               GET_BUID_FROM_BUSINESS_UNIT (vRootCompanyId,
                                            l_scndr_carrier_codeBU);
         END IF;

         IF (vSizeUOMBU IS NULL)
         THEN
            vSizeUOMBUID := vRootCompanyId;
         ELSE
            vSizeUOMBUID :=
               GET_BUID_FROM_BUSINESS_UNIT (vRootCompanyId, vSizeUOMBU);
         END IF;

         vPassFail :=
            VALIDATE_DETAIL_BASE_DATA (vTCCompanyId,
                                       vLaneId,
                                       vRGLaneDtlSeq,
                                       vCarrierCode,
                                       vCarrierCodeBUID,
                                       vMode,
                                       vModeBUID,
                                       vServiceLevel,
                                       vServiceLevelBUID,
                                       vEquipment,
                                       vEquipmentBUID,
                                       vProtectionLevel,
                                       vProtectionLevelBUID,
                                       l_scndr_carrier_code,
                                       l_scndr_carrier_codeBUID,
                                       vSizeUOM,
                                       vSizeUOMBUID,
									   vRank);

         IF (vPassFail = 1)
         THEN
            DBMS_OUTPUT.put_line ('Validate Detail Base Data Failed');
            vFailure := 1;
         ELSE
            vPassFail :=
               VALIDATE_TP_EQUIP_SERV_MODE (vTCCompanyId,
                                            vLaneId,
                                            vRGLaneDtlSeq,
                                            vCarrierCode,
                                            vEquipment,
                                            vServiceLevel,
                                            vMode,
                                            l_scndr_carrier_code,
                                            vRepTpFlag);

            IF (vPassFail = 1)
            THEN
               DBMS_OUTPUT.put_line ('Validate TP EQUIP SERV Mode Failed');
               vFailure := 1;
            END IF;
         END IF;

         -- 07/11/02
         -- Per TT 13743
         -- Carrier code must have a mode that is not null to be selected as a Rep Carrier

         IF (vCarrierCode IS NOT NULL)
         THEN
            IF vRepTpFlag = 1 AND vMode IS NULL
            THEN
               vFailure := 1;

               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGLaneDtlSeq,
                  'Carrier Code: ' || vCarrierCode
                  || ' must have a mode specified to be selected as Rep Carrier',
                  4720032,
                  vCarrierCode);
            END IF;

            IF (vCarrierId IS NULL AND vCarrierCode IS NOT NULL)
            THEN
               SELECT COUNT (CARRIER_ID)
                 INTO vCarrierCodeCount
                 FROM CARRIER_CODE
                WHERE CARRIER_CODE = vCarrierCode
                      AND TC_COMPANY_ID = vCarrierCodeBUID
					  AND mark_for_deletion = 0;

               IF (vCarrierCodeCount > 0)
               THEN
                  SELECT CARRIER_ID
                    INTO vCarrierId
                    FROM CARRIER_CODE
                   WHERE CARRIER_CODE = vCarrierCode
                         AND TC_COMPANY_ID = vCarrierCodeBUID
						 AND mark_for_deletion = 0;
               END IF;
            END IF;

            vPassFail :=
               VALIDATE_MULTIPLE_REP_TP_FLAGS (vTCCompanyId,
                                               vLaneId,
                                               vCarrierId,
                                               vRepTpFlag,
                                               vMode,
                                               l_scndr_carrier_code,
                                               vEffectiveDt,
                                               vExpirationDt);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;

               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGLaneDtlSeq,
                  'Representative carrier already exists for this lane, rep_tp_flag for carrier '
                  || vCarrierCode
                  || ' is invalid',
                  4720033,
                  vCarrierCode);
            END IF;

            IF (vEquipment IS NOT NULL)
            THEN
               vPassFail :=
                  VALIDATE_FEASIBLE (vTCCompanyId,
                                     vLaneId,
                                     vRGLaneDtlSeq,
                                     vCarrierCode,
                                     vCarrierCodeBUID,
                                     vEquipment,
                                     vEquipmentBUID,
                                     vProtectionLevel,
                                     vProtectionLevelBUID,
                                     l_scndr_carrier_code,
                                     l_scndr_carrier_codeBUID);

               IF (vPassFail = 1)
               THEN
                  DBMS_OUTPUT.put_line ('Validate Feasible Failed');
                  vFailure := 1;
               END IF;
            END IF;

            vPassFail :=
               VALIDATE_FAC_CARR_FEASIBLE (vTCCompanyId,
                                           vLaneId,
                                           vRGLaneDtlSeq,
                                           vCarrierCode,
                                           vCarrierCodeBUID,
                                           l_scndr_carrier_code,
                                           l_scndr_carrier_codeBUID);

            IF (vPassFail = 1)
            THEN
               DBMS_OUTPUT.put_line ('Validate Feasible Failed');
               vFailure := 1;
            END IF;

            vPassFail :=
               VALIDATE_CAPACITY_COMMITMENT (vTCCompanyId,
                                             vLaneId,
                                             vRGLaneDtlSeq,
                                             vEffectiveDt,
                                             vExpirationDt);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               DBMS_OUTPUT.put_line ('Validate Capacity Commitment Failed');
            END IF;

            /*
                SL 05/27/02

                Check Whether the effective date for the detail records is greater
                than the expiration record. If it is true, insert into the
                error log table
            */

            IF vEffectiveDt > vExpirationDt
            THEN
               vFailure := 1;

               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGLaneDtlSeq,
                  'Effective Date is greater than the Expiration Date',
                  4720034,
                  NULL);
            END IF;

            vPassFail :=
               VALIDATE_CARRIER_MODE_UOM (vTCCompanyId,
                                          vLaneId,
                                          vRGLaneDtlSeq,
                                          vCarrierCode,
                                          vCarrierCodeBUID,
                                          vMode,
                                          vModeBUID,
                                          vSizeUOM,
                                          vSizeUOMBUID);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               DBMS_OUTPUT.put_line (
                  'Validate Carrier Mode UOM Capicity Failed');
            END IF;

            IF (vUsePreferenceBonus = 0 AND vPrefBonusValue IS NOT NULL)
            THEN
               vFailure := 1;
               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGLaneDtlSeq,
                  'Use Preference Bonus flag must be checked if Preference Bonus Value is given.',
                  4720078,
                  NULL);
            END IF;

            IF (vUsePreferenceBonus = 1 AND vPrefBonusValue IS NULL)
            THEN
               vFailure := 1;
               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGLaneDtlSeq,
                  'Preference Bonus value is required field, if Use Preference Bonus flag is checked.',
                  4720079,
                  NULL);
            END IF;

            IF (vPrefBonusValue IS NOT NULL)
            THEN
               IF (vPrefBonusValue < -1 OR vPrefBonusValue > 1)
               THEN
                  vFailure := 1;
                  INSERT_RG_LANE_DTL_ERROR (
                     vTCCompanyId,
                     vLaneId,
                     vRGLaneDtlSeq,
                     'Preference Bonus Value should be between -1 and 1',
                     4720077,
                     NULL);
               END IF;
            END IF;
			
			SELECT GETDATE() INTO vCurrDate FROM DUAL;
			
			IF(vCarrierId IS NOT NULL AND vExpirationDt > vCurrDate)
			THEN
				vPassFail :=    VALIDATE_EPI_FOR_LANE_DTL (vTCCompanyId,
                                          vLaneId,
                                          vRGLaneDtlSeq,
                                          vCarrierId,
                                          vUseEpi,
                                          vEpiServiceGroup);
										  
				 IF (vPassFail = 1)
				 THEN
						vFailure := 1;
						DBMS_OUTPUT.put_line (
						'Validate EPI flags Failed');
				 END IF;
			END IF;

            vPassFail :=
               VALIDATE_CUST_CARR_MOT_FEAS (vTCCompanyId,
                                            vLaneId,
                                            vRGLaneDtlSeq,
                                            vCarrierCode,
                                            vMode);
         END IF;

         vPassFail :=
            VALIDATE_SHIPPING_PARAMETERS (vTCCompanyId,
                                          vLaneId,
                                          vRGLaneDtlSeq,
                                          vLPNType,
                                          vMinLPNcount,
                                          vMaxLPNcount,
                                          vMinWeight,
                                          vMaxWeight,
                                          vMinVolume,
                                          vMaxVolume,
                                          vMinLength,
                                          vMaxLength,
                                          vMinMonetaryValue,
                                          vMaxMonetaryValue,
                                          vMVCurrencyCode);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            DBMS_OUTPUT.put_line ('Validate Shipping Parameters Failed');
         END IF;

         --[TT #45901 fix]
         BEGIN
            SELECT COUNT (*)
              INTO vErrorCount
              FROM RG_LANE_ERRORS
             WHERE     tc_company_id = vTCCompanyId
                   AND LANE_ID = vLaneId
                   AND RG_LANE_DTL_SEQ = vRGLaneDtlSeq
                   AND BATCH_ID =
                          (SELECT BATCH_ID
                             FROM IMPORT_RG_LANE
                            WHERE LANE_ID = vLaneId
                                  AND TC_COMPANY_ID = vTCCompanyId);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               vErrorCount := 0;
         END;

         --[TT #45901 fix] If lane has errors in this batch then do not set lane status as 1.
         IF (vFailure = 1 OR vErrorCount > 0)
         THEN
            UPDATE IMPORT_RG_LANE_DTL
               SET lane_dtl_status = 4
             WHERE     TC_COMPANY_ID = vTCCompanyId
                   AND LANE_ID = vLaneId
                   AND RG_LANE_DTL_SEQ = vRGLaneDtlSeq;

            -- CR 16942
            --Updating the Parent Lane to
            --Invalid When the Lane details are
            --Invalid
            /*
            UPDATE IMPORT_RG_LANE
             SET lane_status = 4
             WHERE TC_COMPANY_ID = vTCCompanyId
             AND LANE_ID = vLaneId;
             */

            COMMIT;
         ELSE
            UPDATE IMPORT_RG_LANE_DTL
               SET lane_dtl_status = 1
             WHERE     TC_COMPANY_ID = vTCCompanyId
                   AND LANE_ID = vLaneId
                   AND RG_LANE_DTL_SEQ = vRGLaneDtlSeq;

            COMMIT;
         END IF;
      END LOOP LANE_DTL_LOOP;

      CLOSE LANE_DTL_CURSOR;
   END VALIDATE_LANE_DTL;

   FUNCTION VALIDATE_SHIPPING_PARAMETERS (
      vTCCompanyId        IN company.COMPANY_ID%TYPE,
      vLaneId             IN rg_lane.LANE_ID%TYPE,
      vRgLaneDtlSeq       IN rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      vLPNType            IN rg_lane_dtl.SP_LPN_TYPE%TYPE,
      vMinLPNcount        IN rg_lane_dtl.SP_MIN_LPN_COUNT%TYPE,
      vMaxLPNcount        IN rg_lane_dtl.SP_MAX_LPN_COUNT%TYPE,
      vMinWeight          IN rg_lane_dtl.SP_MIN_WEIGHT%TYPE,
      vMaxWeight          IN rg_lane_dtl.SP_MAX_WEIGHT%TYPE,
      vMinVolume          IN rg_lane_dtl.SP_MIN_VOLUME%TYPE,
      vMaxVolume          IN rg_lane_dtl.SP_MAX_VOLUME%TYPE,
      vMinLength          IN rg_lane_dtl.SP_MIN_LINEAR_FEET%TYPE,
      vMaxLength          IN rg_lane_dtl.SP_MAX_LINEAR_FEET%TYPE,
      vMinMonetaryValue   IN rg_lane_dtl.SP_MIN_MONETARY_VALUE%TYPE,
      vMaxMonetaryValue   IN rg_lane_dtl.SP_MAX_MONETARY_VALUE%TYPE,
      vMVCurrencyCode     IN rg_lane_dtl.SP_CURRENCY_CODE%TYPE)
      RETURN NUMBER
   IS
      vFailure   NUMBER;
   BEGIN
      vFailure := 0;

    IF(vLPNType = -1)
    THEN
	vFailure := 1;
	   INSERT_RG_LANE_DTL_ERROR (
	      vTCCompanyId,
	      vLaneId,
	      vRGLaneDtlSeq,
	      'Imported LPN Type is not valid',
	      99991024,
	      NULL);
     ELSE

      -- BEGIN LPN count validation
      IF (vLPNType IS NOT NULL)
      THEN
         IF (vMinLPNcount IS NOT NULL OR vMaxLPNcount IS NOT NULL)
         THEN
            IF (vMinLPNcount IS NOT NULL AND vMaxLPNcount IS NULL)
            THEN
               vFailure := 1;
               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGLaneDtlSeq,
                  'LPN count - Maximum is required when Minimum is given',
                  99991002,
                  NULL);
            END IF;

            IF (vMinLPNcount IS NULL AND vMaxLPNcount IS NOT NULL)
            THEN
               vFailure := 1;
               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGLaneDtlSeq,
                  'LPN count - Minimum is required when Maximum is given',
                  99991003,
                  NULL);
            END IF;

            IF (vMinLPNcount < 0)
            THEN
               vFailure := 1;
               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGLaneDtlSeq,
                  'SP MIN LPN COUNT cannot have a negative value.',
                  4000003,
                  NULL);
            END IF;

            IF (vMaxLPNcount < 0)
            THEN
               vFailure := 1;
               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGLaneDtlSeq,
                  'SP MAX LPN COUNT cannot have a negative value.',
                  4000003,
                  NULL);
            END IF;

            IF (vMinLPNcount > vMaxLPNcount)
            THEN
               vFailure := 1;
               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGLaneDtlSeq,
                  'LPN count - Minimum cannot be greater than Maximum',
                  99991001,
                  NULL);
            END IF;
         ELSE
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGLaneDtlSeq,
               'LPN count - LPN Count Values are required when LPN Type is given',
               99991002,
               NULL);
         END IF;
      ELSE
         IF (vMinLPNcount IS NOT NULL OR vMaxLPNcount IS NOT NULL)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (vTCCompanyId,
                                      vLaneId,
                                      vRGLaneDtlSeq,
                                      'SP LPN TYPE is a required field.',
                                      99991003,
                                      NULL);
         END IF;
      END IF;      -- END of LPN count validation
      END IF;

      -- BEGIN Weight parameter validation
      IF (vMinWeight IS NOT NULL OR vMaxWeight IS NOT NULL)
      THEN
         IF (vMinWeight IS NOT NULL AND vMaxWeight IS NULL)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGLaneDtlSeq,
               'Max Weight is required when Min Weight is given',
               99991016,
               NULL);
         END IF;

         IF (vMinWeight IS NULL AND vMaxWeight IS NOT NULL)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGLaneDtlSeq,
               'Min Weight is required when Max Weight is given',
               99991006,
               NULL);
         END IF;

         IF (vMinWeight < 0)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGLaneDtlSeq,
               'SP MIN WEIGHT cannot have a negative value.',
               4000003,
               NULL);
         END IF;

         IF (vMaxWeight < 0)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGLaneDtlSeq,
               'SP MAX WEIGHT cannot have a negative value.',
               4000003,
               NULL);
         END IF;

         IF (vMinWeight > vMaxWeight)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGLaneDtlSeq,
               'Min Weight must not be greater than Max Weight',
               99991004,
               NULL);
         END IF;
      END IF;

      -- END of Weight parameter validation

      -- BEGIN Volume parameter validation
      IF (vMinVolume IS NOT NULL OR vMaxVolume IS NOT NULL)
      THEN
         IF (vMinVolume IS NOT NULL AND vMaxVolume IS NULL)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGLaneDtlSeq,
               'Max Volume is required when Min Volume is given',
               99991008,
               NULL);
         END IF;

         IF (vMinVolume IS NULL AND vMaxVOLUME IS NOT NULL)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGLaneDtlSeq,
               'Min Volume is required when Max Volume is given',
               99991009,
               NULL);
         END IF;

         IF (vMinVolume < 0)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGLaneDtlSeq,
               'SP MIN VOLUME cannot have a negative value.',
               4000003,
               NULL);
         END IF;

         IF (vMaxVolume < 0)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGLaneDtlSeq,
               'SP MAXVOLUME cannot have a negative value.',
               4000003,
               NULL);
         END IF;

         IF (vMinVolume > vMaxVolume)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGLaneDtlSeq,
               'Min Volume must not be greater than Max Volume',
               99991007,
               NULL);
         END IF;
      END IF;

      -- END of Volume parameter validation

      -- BEGIN Length parameter validation
      IF (vMinLength IS NOT NULL OR vMaxLength IS NOT NULL)
      THEN
         IF (vMinLength IS NOT NULL AND vMaxLength IS NULL)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGLaneDtlSeq,
               'Max Linear Feet is required when Min Linear Feet is given',
               99991011,
               NULL);
         END IF;

         IF (vMinLength IS NULL AND vMaxLength IS NOT NULL)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGLaneDtlSeq,
               'Min Linear Feet is required when Max Linear Feet is given',
               99991012,
               NULL);
         END IF;

         IF (vMinLength < 0)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGLaneDtlSeq,
               'SP MIN LINEAR FEET cannot have a negative value.',
               4000003,
               NULL);
         END IF;

         IF (vMaxLength < 0)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGLaneDtlSeq,
               'SP MAX LINEAR FEET cannot have a negative value.',
               4000003,
               NULL);
         END IF;

         IF (vMinLength > vMaxLength)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGLaneDtlSeq,
               'Min Linear Feet must not be greater than Max Linear Feet',
               99991010,
               NULL);
         END IF;
      END IF;

      -- END of Length parameter validation

      -- BEGIN Monetary Value validation
      IF (vMVCurrencyCode IS NOT NULL)
      THEN
         IF (vMinMonetaryValue IS NOT NULL OR vMaxMonetaryValue IS NOT NULL)
         THEN
            IF (vMinMonetaryValue IS NOT NULL AND vMaxMonetaryValue IS NULL)
            THEN
               vFailure := 1;
               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGLaneDtlSeq,
                  'Max Monetary Value is required when Min Monetary Value is given',
                  99991014,
                  NULL);
            END IF;

            IF (vMinMonetaryValue IS NULL AND vMaxMonetaryValue IS NOT NULL)
            THEN
               vFailure := 1;
               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGLaneDtlSeq,
                  'Min Monetary Value is required when Max Monetary Value is given',
                  99991015,
                  NULL);
            END IF;

            IF (vMinMonetaryValue < 0)
            THEN
               vFailure := 1;
               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGLaneDtlSeq,
                  'SP MIN MONETARY VALUE cannot have a negative value.',
                  4000003,
                  NULL);
            END IF;

            IF (vMaxMonetaryValue < 0)
            THEN
               vFailure := 1;
               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGLaneDtlSeq,
                  'SP MAX MONETARY VALUE cannot have a negative value.',
                  4000003,
                  NULL);
            END IF;

            IF (vMinMonetaryValue > vMaxMonetaryValue)
            THEN
               vFailure := 1;
               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGLaneDtlSeq,
                  'Min Monetary Value must not be greater than Max Monetary Value',
                  99991013,
                  NULL);
            END IF;
         ELSE
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGLaneDtlSeq,
               'Min/Max Monetary Value is required when Monetary Currency Code is given',
               99991013,
               NULL);
         END IF;
      ELSE
         IF (vMinMonetaryValue IS NOT NULL OR vMaxMonetaryValue IS NOT NULL)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGLaneDtlSeq,
               'SP CURRENCY CODE is a required field.',
               99991003,
               NULL);
         END IF;
      END IF;

      -- END of Monetary Validation

      RETURN vFailure;
   END VALIDATE_SHIPPING_PARAMETERS;

   FUNCTION VALIDATE_MULTIPLE_REP_TP_FLAGS (
      vTCCompanyId           IN company.COMPANY_ID%TYPE,
      vLaneId                IN rg_lane.LANE_ID%TYPE,
      vCarrierId             IN carrier_code.CARRIER_ID%TYPE,
      vRepTpFlag             IN rg_lane_dtl.REP_TP_FLAG%TYPE,
      vMode                  IN MOT.mot%TYPE,
      p_scndr_carrier_code   IN carrier_code.CARRIER_CODE%TYPE,
      p_effective_dt         IN RG_LANE_DTL.EFFECTIVE_DT%TYPE,
      P_expiration_dt        IN RG_LANE_DTL.EXPIRATION_DT%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
      num         NUMBER;
      vMOT_ID     NUMBER;
   BEGIN
      num := 0;

      IF vRepTpFlag = 1
      THEN
         IF vMode IS NOT NULL
         THEN
            SELECT MOT_ID
              INTO vMOT_ID
              FROM MOT
             WHERE MOT = vMode AND TC_COMPANY_ID = vTCCompanyId;

            SELECT COUNT (*)
              INTO num
              FROM (SELECT 1
                      FROM import_rg_lane_dtl
                     WHERE     rep_tp_flag = 1
                           /* and             carrier_code != vCarrierCode */
                           AND carrier_id != vCarrierId
                           AND mot = vMode
                           AND lane_id = vLaneId
                           AND EFFECTIVE_DT <= EXPIRATION_DT
                           AND p_effective_dt <= p_expiration_dt
                           AND EFFECTIVE_DT <= p_expiration_dt
                           AND p_effective_dt <= EXPIRATION_DT
                           AND lane_dtl_status = 1             -- 1 is new row
                    UNION
                    SELECT 1
                      FROM rg_lane_dtl
                     WHERE     rep_tp_flag = 1
                           /* and             carrier_code != vCarrierCode */
                           AND carrier_id != vCarrierId
                           AND mot_id = vMOT_ID
                           -- and             mot = vMode
                           AND lane_id = (SELECT rg_lane_id
                                            FROM import_rg_lane
                                           WHERE lane_id = vLaneId)
                           AND EFFECTIVE_DT <= EXPIRATION_DT
                           AND p_effective_dt <= p_expiration_dt
                           AND EFFECTIVE_DT <= p_expiration_dt
                           AND p_effective_dt <= EXPIRATION_DT);
         -- end TT 51818

         END IF;
      END IF;

      IF num > 0
      THEN
         vPassFail := 1;
      ELSE
         vPassFail := 0;
      END IF;

      --              end if;

      IF num > 0
      THEN
         vPassFail := 1;
      ELSE
         vPassFail := 0;
      END IF;

      RETURN vPassFail;
   END VALIDATE_MULTIPLE_REP_TP_FLAGS;

   /*--------------------------------------------------------------------------
    *
    *      S.Lal (05/17/02)  -  Made changes according to the USE CASE 1508
    *                           Rules 1 and 4 do not overlap, so they are
    *                           simple inserts. Rule 3 is not in the Used Case.
    *-------------------------------------------------------------------------*/

   PROCEDURE ADJUST_EFF_EXP_DT (
      vTCCompanyId           IN company.COMPANY_ID%TYPE,
      vLaneId                IN comb_lane.LANE_ID%TYPE,
      vRGDtlSeq              IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vEffectiveDT           IN comb_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN comb_lane_dtl.EXPIRATION_DT%TYPE,
      vCarrierCode           IN comb_lane_dtl.CARRIER_ID%TYPE,
      vMot                   IN comb_lane_dtl.MOT_ID%TYPE,
      vEquipment             IN comb_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN comb_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN comb_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN comb_lane_dtl.PROTECTION_LEVEL_ID%TYPE,
	  vVoyage				 IN comb_lane_dtl.VOYAGE%TYPE)
   IS
      eff_exp_sql            VARCHAR2 (2000);
      eff_exp_cursor         ref_curtype;
      vCurrentRGDtlSeq       comb_lane_dtl.lane_id%TYPE;
      vEffDT                 comb_lane_dtl.effective_dt%TYPE;
      vExpDT                 comb_lane_dtl.expiration_dt%TYPE;
      vExpDTTemp             comb_lane_dtl.expiration_dt%TYPE;
      vIsRating              comb_lane_dtl.IS_RATING%TYPE;
      vIsRouting             comb_lane_dtl.IS_ROUTING%TYPE;
      vIsSailing             comb_lane_dtl.IS_SAILING%TYPE;
      isFirstRow             INT;
      isInsertAtEnd          INT;
      isChanged              INT;
      vMotTemp               comb_lane_dtl.MOT_ID%TYPE;
      vEquipmentTemp         comb_lane_dtl.EQUIPMENT_ID%TYPE;
      vServiceLevelTemp      comb_lane_dtl.SERVICE_LEVEL_ID%TYPE;
      vProtectionLevelTemp   comb_lane_dtl.PROTECTION_LEVEL_ID%TYPE;
   BEGIN
      eff_exp_sql :=
         ' SELECT lane_dtl_seq, effective_dt, expiration_dt, is_rating, is_routing, is_sailing '
         || ' FROM comb_lane_dtl '
         || /* ' WHERE tc_company_id = :vTCCompanyId' ||*/
            ' WHERE lane_id = :vLaneId'
         || ' AND lane_dtl_seq != :vRGDtlSeq'
         || ' AND lane_dtl_status = 0 '
         || ' AND HAS_SHIPPING_PARAM = 0 '
         || ' AND effective_dt <= expiration_dt '
         || ' AND expiration_dt >= :vEffectiveDT '
         || ' AND effective_dt <= :vExpirationDT '
		 || ' AND CUSTOM_TEXT1 IS NULL '
         || ' AND CUSTOM_TEXT2 IS NULL '
         || ' AND CUSTOM_TEXT3 IS NULL '
         || ' AND CUSTOM_TEXT4 IS NULL '
         || ' AND CUSTOM_TEXT5 IS NULL ';

      vMotTemp := vMot;
      vEquipmentTemp := vEquipment;
      vServiceLevelTemp := vServiceLevel;
      vProtectionLevelTemp := vProtectionLevel;

      /*
      IF( vMot = -1 ) THEN
       vMotTemp := null;
      END IF;

      IF( vEquipment = -1 ) THEN
       vEquipmentTemp := null;
      END IF;

      IF( vServiceLevel = -1) THEN
       vServiceLevelTemp := null;
      END IF;

      IF( vProtectionLevel = -1 ) THEN
       vProtectionLevelTemp := null;
      END IF;
      */
      eff_exp_sql :=
         eff_exp_sql
         || GET_RESOURCE_WHERE_CLAUSE (vCarrierCode,
                                       vMotTemp,
                                       vEquipmentTemp,
                                       vServiceLevelTemp,
                                       p_scndr_carrier_code,
                                       vProtectionLevelTemp,
									   vVoyage);

      eff_exp_sql := eff_exp_sql || ' ORDER BY EFFECTIVE_DT';

      isInsertAtEnd := 1;
      isFirstRow := 0;
      isChanged := 0;

      OPEN eff_exp_cursor FOR eff_exp_sql
         USING vLaneId,
               vRGDtlSeq,
               vEffectiveDT,
               vExpirationDT;

      LOOP
         FETCH eff_exp_cursor
         INTO vCurrentRGDtlSeq,
              vEffDT,
              vExpDT,
              vIsRating,
              vIsRouting,
              vIsSailing;

         EXIT WHEN eff_exp_cursor%NOTFOUND;

         -- Start of Merge with other lane detail
         IF (vEffectiveDT <= vEffDt)
         THEN
            IF (isFirstRow = 0)
            THEN
               IF (vEffectiveDT <> vEffDt)
               THEN
                  -- create row from input effective date to effective date of current row
                  CREATE_NEW_ROW (vTCCompanyId,
                                  vLaneId,
                                  vRGDtlSeq,
                                  vEffectiveDT,
                                  vEffDt - 1);
               END IF;
            ELSE
               IF (vExpDTTemp <> vEffDt)
               THEN
                  -- create row from prev expiration date to effective date of current row
                  CREATE_NEW_ROW (vTCCompanyId,
                                  vLaneId,
                                  vRGDtlSeq,
                                  vExpDTTemp,
                                  vEffDt - 1);
               END IF;
            END IF;

            IF (vExpirationDT < vExpDT)
            THEN
               --replicate row from input expiration date to exp date of current row with rating flag turned off
               REPLICATE_ROW (vTCCompanyId,
                              vLaneId,
                              -1,
                              vCurrentRGDtlSeq,
                              vExpirationDT + 1,
                              vExpDT,
                              vIsRating,
                              vIsRouting,
                              vIsSailing);

               UPDATE_ROW (vTCCompanyId,
                           vLaneId,
                           vRGDtlSeq,
                           vCurrentRGDtlSeq,
                           vExpirationDT);

               /*UPDATE comb_lane_dtl
                              SET is_routing = 1,
                expiration_dt = vExpirationDT,
                                  last_updated_source_type = 3,
                                  last_updated_source = 'OMS',
                last_updated_dttm = getdate()
                              WHERE tc_company_id = vTCCompanyId
                              AND lane_id = vLaneId
                              AND lane_dtl_seq = vCurrentRGDtlSeq;  */
               --COPY_CHILD_ROW( vTCCompanyId, vLaneId, vRGDtlSeq, vCurrentRGDtlSeq,
               -- vCurrentRGDtlSeq, 0, 1, 0 );

               isInsertAtEnd := 1;
            ELSE
               UPDATE_ROW (vTCCompanyId,
                           vLaneId,
                           vRGDtlSeq,
                           vCurrentRGDtlSeq,
                           vExpDT);

               /* UPDATE comb_lane_dtl
                              SET is_routing = 1,
                last_updated_source_type = 3,
                                  last_updated_source = 'OMS',
                last_updated_dttm = getdate()
                              WHERE tc_company_id = vTCCompanyId
                              AND lane_id = vLaneId
                              AND lane_dtl_seq = vCurrentRGDtlSeq; */
               --COPY_CHILD_ROW( vTCCompanyId, vLaneId, vRGDtlSeq, vCurrentRGDtlSeq,
               --   vCurrentRGDtlSeq, 0, 1, 0 );

               isInsertAtEnd := 0;
            END IF;

            IF (vIsRouting = 1)
            THEN
               DELETE_CHILD_ROW (vTCCompanyId, vLaneId, vCurrentRGDtlSeq);
            END IF;

            COPY_CHILD_ROW (vTCCompanyId,
                            vLaneId,
                            vRGDtlSeq,
                            vCurrentRGDtlSeq,
                            vCurrentRGDtlSeq,
                            0,
                            1,
                            0);
         ELSE
            UPDATE comb_lane_dtl
               SET expiration_dt = vEffectiveDT - 1,
                   last_updated_source_type = 3,
                   last_updated_source = 'OMS',
                   last_updated_dttm = getdate ()
             /*  WHERE tc_company_id = vTCCompanyId */
             WHERE lane_id = vLaneId AND lane_dtl_seq = vCurrentRGDtlSeq;

            IF (vExpirationDT < vExpDT)
            THEN
               --replicate row from input effective date to input exp date with rating flag turned on
               REPLICATE_ROW (vTCCompanyId,
                              vLaneId,
                              vRGDtlSeq,
                              vCurrentRGDtlSeq,
                              vEffectiveDT,
                              vExpirationDT,
                              vIsRating,
                              1,
                              vIsSailing);
               --replicate row from input expiration date to exp date of current row with rating flag turned off
               REPLICATE_ROW (vTCCompanyId,
                              vLaneId,
                              -1,
                              vCurrentRGDtlSeq,
                              vExpirationDT + 1,
                              vExpDT,
                              vIsRating,
                              vIsRouting,
                              vIsSailing);
               isInsertAtEnd := 1;
            ELSE
               --replicate row from input eff date to exp date of current row with rating flag turned on
               REPLICATE_ROW (vTCCompanyId,
                              vLaneId,
                              vRGDtlSeq,
                              vCurrentRGDtlSeq,
                              vEffectiveDT,
                              vExpDT,
                              vIsRating,
                              1,
                              vIsSailing);
               isInsertAtEnd := 0;
            END IF;
         END IF;

         vExpDTTemp := vExpDT + 1;
         isFirstRow := 1;
      END LOOP;

      IF (isInsertAtEnd = 0)
      THEN
         IF (vExpDT <> vExpirationDT)
         THEN
            -- create row from exp date of last row to input expiration date
            CREATE_NEW_ROW (vTCCompanyId,
                            vLaneId,
                            vRGDtlSeq,
                            vExpDT + 1,
                            vExpirationDT);
         END IF;
      END IF;

      IF (isFirstRow = 1)
      THEN
         --UPDATE comb_lane_dtl
         --SET lane_dtl_status = 2,
         --    last_updated_source_type = 3,
         --   last_updated_source = 'OMS',
         -- last_updated_dttm = getdate()
         DELETE_CHILD_ROW (vTCCompanyId, vLaneId, vRGDtlSeq);

         DELETE FROM comb_lane_dtl
               /* WHERE tc_company_id = vTCCompanyId */
               WHERE lane_id = vLaneId AND lane_dtl_seq = vRGDtlSeq;
      END IF;

      COMMIT;

      CLOSE eff_exp_cursor;
   END ADJUST_EFF_EXP_DT;

   PROCEDURE ADJUST_EFF_EXP_DT (
      vTCCompanyId           IN company.COMPANY_ID%TYPE,
      vLaneId                IN comb_lane.LANE_ID%TYPE,
      vRGDtlSeq              IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vRGDtlSeqUpd           IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vEffectiveDT           IN comb_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT          IN comb_lane_dtl.EXPIRATION_DT%TYPE,
      vCarrierCode           IN comb_lane_dtl.CARRIER_ID%TYPE,
      vMot                   IN comb_lane_dtl.MOT_ID%TYPE,
      vEquipment             IN comb_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN comb_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN comb_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN comb_lane_dtl.PROTECTION_LEVEL_ID%TYPE,
	  vVoyage				 IN comb_lane_dtl.voyage%TYPE)
   IS
      eff_exp_sql            VARCHAR2 (2000);
      eff_exp_cursor         ref_curtype;
      vCurrentRGDtlSeq       comb_lane_dtl.lane_id%TYPE;
      vEffDT                 comb_lane_dtl.effective_dt%TYPE;
      vExpDT                 comb_lane_dtl.expiration_dt%TYPE;
      vExpDTTemp             comb_lane_dtl.expiration_dt%TYPE;
      vIsRating              comb_lane_dtl.IS_RATING%TYPE;
      vIsRouting             comb_lane_dtl.IS_ROUTING%TYPE;
      vIsSailing             comb_lane_dtl.IS_SAILING%TYPE;
      isFirstRow             INT;
      isInsertAtEnd          INT;
      isChanged              INT;
      tempCount              INT;
      vMotTemp               comb_lane_dtl.MOT_ID%TYPE;
      vEquipmentTemp         comb_lane_dtl.EQUIPMENT_ID%TYPE;
      vServiceLevelTemp      comb_lane_dtl.SERVICE_LEVEL_ID%TYPE;
      vProtectionLevelTemp   comb_lane_dtl.PROTECTION_LEVEL_ID%TYPE;
   BEGIN
      eff_exp_sql :=
         ' SELECT lane_dtl_seq, effective_dt, expiration_dt, is_rating, is_routing, is_sailing '
         || ' FROM comb_lane_dtl '
         || ' WHERE lane_id = :vLaneId'
         || ' AND lane_dtl_seq != :vRGDtlSeq'
         || ' AND lane_dtl_status = 0 '
         || ' AND HAS_SHIPPING_PARAM = 0 '
         || ' AND effective_dt <= expiration_dt '
         || ' AND expiration_dt >= :vEffectiveDT '
         || ' AND effective_dt <= :vExpirationDT '
		 || ' AND CUSTOM_TEXT1 IS NULL '
         || ' AND CUSTOM_TEXT2 IS NULL '
         || ' AND CUSTOM_TEXT3 IS NULL '
         || ' AND CUSTOM_TEXT4 IS NULL '
         || ' AND CUSTOM_TEXT5 IS NULL ';
		 
      vMotTemp := vMot;
      vEquipmentTemp := vEquipment;
      vServiceLevelTemp := vServiceLevel;
      vProtectionLevelTemp := vProtectionLevel;

      IF (vMot = -1)
      THEN
         vMotTemp := NULL;
      END IF;

      IF (vEquipment = -1)
      THEN
         vEquipmentTemp := NULL;
      END IF;

      IF (vServiceLevel = -1)
      THEN
         vServiceLevelTemp := NULL;
      END IF;

      IF (vProtectionLevel = -1)
      THEN
         vProtectionLevelTemp := NULL;
      END IF;

      tempCount := 0;

      IF (vRGDtlSeqUpd IS NOT NULL)
      THEN
         --tempCount := MERGE_DRAFT( vTCCompanyId, vLaneId, vRGDtlSeq, vRGDtlSeqUpd );
         SELECT COUNT (*)
           INTO tempCount
           FROM COMB_LANE_DTL
          WHERE     LANE_ID = vLaneId
                AND LANE_DTL_SEQ = vRGDtlSeqUpd
                AND IS_SAILING = 0
                AND IS_RATING = 0;

         IF (tempCount > 0)
         THEN
            eff_exp_sql :=
               eff_exp_sql || ' AND lane_dtl_seq != ' || vRGDtlSeqUpd;
         END IF;
      END IF;

      eff_exp_sql :=
         eff_exp_sql
         || GET_RESOURCE_WHERE_CLAUSE (vCarrierCode,
                                       vMotTemp,
                                       vEquipmentTemp,
                                       vServiceLevelTemp,
                                       p_scndr_carrier_code,
                                       vProtectionLevelTemp,
									   vVoyage);

      eff_exp_sql := eff_exp_sql || ' ORDER BY EFFECTIVE_DT';

      isInsertAtEnd := 1;
      isFirstRow := 0;
      isChanged := 0;

      OPEN eff_exp_cursor FOR eff_exp_sql
         USING vLaneId,
               vRGDtlSeq,
               vEffectiveDT,
               vExpirationDT;

      LOOP
         FETCH eff_exp_cursor
         INTO vCurrentRGDtlSeq,
              vEffDT,
              vExpDT,
              vIsRating,
              vIsRouting,
              vIsSailing;

         EXIT WHEN eff_exp_cursor%NOTFOUND;

         -- Start of Merge with other lane detail
         IF (vEffectiveDT <= vEffDt)
         THEN
            IF (isFirstRow = 0)
            THEN
               IF (vEffectiveDT <> vEffDt)
               THEN
                  IF ( (tempCount > 0) AND (isChanged = 0))
                  THEN
                     isChanged := 1;

                     tempCount :=
                        MERGE_DRAFT (vTCCompanyId,
                                     vLaneId,
                                     vRGDtlSeq,
                                     vRGDtlSeqUpd);

                     -- Update the existing row
                     UPDATE comb_lane_dtl
                        SET effective_dt = vEffectiveDT,
                            expiration_dt = vEffDt - 1,
                            last_updated_source_type = 3,
                            last_updated_source = 'OMS',
                            last_updated_dttm = getdate ()
                      WHERE lane_id = vLaneId AND lane_dtl_seq = vRGDtlSeqUpd;
                  ELSE
                     -- create row from input effective date to effective date of current row
                     CREATE_NEW_ROW (vTCCompanyId,
                                     vLaneId,
                                     vRGDtlSeq,
                                     vEffectiveDT,
                                     vEffDt - 1);
                  END IF;
               END IF;
            ELSE
               IF (vExpDTTemp <> vEffDt)
               THEN
                  IF ( (tempCount > 0) AND (isChanged = 0))
                  THEN
                     isChanged := 1;
                     tempCount :=
                        MERGE_DRAFT (vTCCompanyId,
                                     vLaneId,
                                     vRGDtlSeq,
                                     vRGDtlSeqUpd);

                     -- Update the existing row
                     UPDATE comb_lane_dtl
                        SET effective_dt = vExpDTTemp,
                            expiration_dt = vEffDt - 1,
                            last_updated_source_type = 3,
                            last_updated_source = 'OMS',
                            last_updated_dttm = getdate ()
                      WHERE lane_id = vLaneId AND lane_dtl_seq = vRGDtlSeqUpd;
                  ELSE
                     -- create row from prev expiration date to effective date of current row
                     CREATE_NEW_ROW (vTCCompanyId,
                                     vLaneId,
                                     vRGDtlSeq,
                                     vExpDTTemp,
                                     vEffDt - 1);
                  END IF;
               END IF;
            END IF;

            IF (vExpirationDT < vExpDT)
            THEN
               --replicate row from input expiration date to exp date of current row with rating flag turned off
               REPLICATE_ROW (vTCCompanyId,
                              vLaneId,
                              -1,
                              vCurrentRGDtlSeq,
                              vExpirationDT + 1,
                              vExpDT,
                              vIsRating,
                              vIsRouting,
                              vIsSailing);

               UPDATE_ROW (vTCCompanyId,
                           vLaneId,
                           vRGDtlSeq,
                           vCurrentRGDtlSeq,
                           vExpirationDT);

               /*UPDATE comb_lane_dtl
                              SET is_routing = 1,
                expiration_dt = vExpirationDT,
                                  last_updated_source_type = 3,
                                  last_updated_source = 'OMS',
                last_updated_dttm = getdate()
                              WHERE lane_id = vLaneId
                              AND lane_dtl_seq = vCurrentRGDtlSeq;  */
               --COPY_CHILD_ROW( vTCCompanyId, vLaneId, vRGDtlSeq, vCurrentRGDtlSeq,
               -- vCurrentRGDtlSeq, 0, 1, 0 );

               isInsertAtEnd := 1;
            ELSE
               UPDATE_ROW (vTCCompanyId,
                           vLaneId,
                           vRGDtlSeq,
                           vCurrentRGDtlSeq,
                           vExpDT);

               /* UPDATE comb_lane_dtl
                              SET is_routing = 1,
                last_updated_source_type = 3,
                                  last_updated_source = 'OMS',
                last_updated_dttm = getdate()
                              WHERE lane_id = vLaneId
                              AND lane_dtl_seq = vCurrentRGDtlSeq; */
               --COPY_CHILD_ROW( vTCCompanyId, vLaneId, vRGDtlSeq, vCurrentRGDtlSeq,
               --   vCurrentRGDtlSeq, 0, 1, 0 );

               isInsertAtEnd := 0;
            END IF;

            IF (vIsRouting = 1)
            THEN
               DELETE_CHILD_ROW (vTCCompanyId, vLaneId, vCurrentRGDtlSeq);
            END IF;

            COPY_CHILD_ROW (vTCCompanyId,
                            vLaneId,
                            vRGDtlSeq,
                            vCurrentRGDtlSeq,
                            vCurrentRGDtlSeq,
                            0,
                            1,
                            0);
         ELSE
            UPDATE comb_lane_dtl
               SET expiration_dt = vEffectiveDT - 1,
                   last_updated_source_type = 3,
                   last_updated_source = 'OMS',
                   last_updated_dttm = getdate ()
             WHERE lane_id = vLaneId AND lane_dtl_seq = vCurrentRGDtlSeq;

            IF (vExpirationDT < vExpDT)
            THEN
               --replicate row from input effective date to input exp date with rating flag turned on
               REPLICATE_ROW (vTCCompanyId,
                              vLaneId,
                              vRGDtlSeq,
                              vCurrentRGDtlSeq,
                              vEffectiveDT,
                              vExpirationDT,
                              vIsRating,
                              1,
                              vIsSailing);
               --replicate row from input expiration date to exp date of current row with rating flag turned off
               REPLICATE_ROW (vTCCompanyId,
                              vLaneId,
                              -1,
                              vCurrentRGDtlSeq,
                              vExpirationDT + 1,
                              vExpDT,
                              vIsRating,
                              vIsRouting,
                              vIsSailing);
               isInsertAtEnd := 1;
            ELSE
               --replicate row from input eff date to exp date of current row with rating flag turned on
               REPLICATE_ROW (vTCCompanyId,
                              vLaneId,
                              vRGDtlSeq,
                              vCurrentRGDtlSeq,
                              vEffectiveDT,
                              vExpDT,
                              vIsRating,
                              1,
                              vIsSailing);
               isInsertAtEnd := 0;
            END IF;
         END IF;

         vExpDTTemp := vExpDT + 1;
         isFirstRow := 1;
      END LOOP;

      IF (isInsertAtEnd = 0)
      THEN
         IF (vExpDT <> vExpirationDT)
         THEN
            IF ( (tempCount > 0) AND (isChanged = 0))
            THEN
               isChanged := 1;
               tempCount :=
                  MERGE_DRAFT (vTCCompanyId,
                               vLaneId,
                               vRGDtlSeq,
                               vRGDtlSeqUpd);

               -- Update the existing row
               UPDATE comb_lane_dtl
                  SET effective_dt = vExpDT + 1,
                      expiration_dt = vExpirationDT,
                      last_updated_source_type = 3,
                      last_updated_source = 'OMS',
                      last_updated_dttm = getdate ()
                WHERE lane_id = vLaneId AND lane_dtl_seq = vRGDtlSeqUpd;
            ELSE
               -- create row from exp date of last row to input expiration date
               CREATE_NEW_ROW (vTCCompanyId,
                               vLaneId,
                               vRGDtlSeq,
                               vExpDT + 1,
                               vExpirationDT);
            END IF;
         END IF;
      END IF;

      IF (isFirstRow = 1)
      THEN
         --UPDATE comb_lane_dtl
         --SET lane_dtl_status = 2,
         --    last_updated_source_type = 3,
         --    last_updated_source = 'OMS',
         --   last_updated_dttm = getdate()
         DELETE_CHILD_ROW (vTCCompanyId, vLaneId, vRGDtlSeq);

         DELETE FROM comb_lane_dtl
               WHERE lane_id = vLaneId AND lane_dtl_seq = vRGDtlSeq;
      END IF;

      --For Update flow
      IF (tempCount > 0)
      THEN
         -- If the draft is not merged with row which is getting updated
         IF ( (isChanged = 0) AND (isFirstRow = 1))
         THEN
            UPDATE comb_lane_dtl
               SET expiration_dt = effective_dt - 1,
                   last_updated_source_type = 3,
                   last_updated_source = 'OMS',
                   last_updated_dttm = getdate ()
             WHERE lane_id = vLaneId AND lane_dtl_seq = vRGDtlSeqUpd;
         -- If the draft is not merged with any row
         ELSIF (isFirstRow = 0)
         THEN
            tempCount :=
               MERGE_DRAFT (vTCCompanyId,
                            vLaneId,
                            vRGDtlSeq,
                            vRGDtlSeqUpd);
            DELETE_CHILD_ROW (vTCCompanyId, vLaneId, vRGDtlSeq);

            DELETE FROM comb_lane_dtl
                  WHERE lane_id = vLaneId AND lane_dtl_seq = vRGDtlSeq;
         END IF;
      END IF;

      COMMIT;

      CLOSE eff_exp_cursor;
   END ADJUST_EFF_EXP_DT;

   -- Procedure COPY CARRIERS for updation of relavent carriers with the lane details changes when modified  SR1, 4R1 change, build92,  TT31182 [BEGIN]
   PROCEDURE COPY_CARRIERS (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                            vLaneId        IN comb_lane.LANE_ID%TYPE,
                            vOldLaneId     IN comb_lane.LANE_ID%TYPE)
   IS
      select_sql             VARCHAR2 (2000);
      select_cursor          ref_curtype;
      vLaneDtlSeq            comb_lane_dtl.LANE_DTL_SEQ%TYPE;
      vLaneDtlSeqNew         comb_lane_dtl.LANE_DTL_SEQ%TYPE;
      vEffectiveDT           comb_lane_dtl.EFFECTIVE_DT%TYPE;
      vExpirationDT          comb_lane_dtl.EXPIRATION_DT%TYPE;
      vCarrierCode           comb_lane_dtl.CARRIER_ID%TYPE;
      vMot                   comb_lane_dtl.MOT_ID%TYPE;
      vEquipment             comb_lane_dtl.EQUIPMENT_ID%TYPE;
      vServiceLevel          comb_lane_dtl.SERVICE_LEVEL_ID%TYPE;
      p_scndr_carrier_code   comb_lane_dtl.SCNDR_CARRIER_ID%TYPE;
      vProtectionLevel       comb_lane_dtl.PROTECTION_LEVEL_ID%TYPE;
      vLaneDtlStatus         comb_lane_dtl.LANE_DTL_STATUS%TYPE;
      vMasterLaneId          comb_lane_dtl.MASTER_LANE_ID%TYPE;
	  vVoyage				 comb_lane_dtl.VOYAGE%TYPE;
   BEGIN
      SELECT MASTER_LANE_ID
        INTO vMasterLaneId
        FROM comb_lane
       WHERE lane_id = vLaneId;

      select_sql :=
            ' SELECT LANE_DTL_SEQ, LANE_DTL_STATUS, '
         || ' EFFECTIVE_DT, EXPIRATION_DT, '
         || ' CARRIER_ID, MOT_ID, '
         || ' EQUIPMENT_ID, SERVICE_LEVEL_ID, '
         || ' PROTECTION_LEVEL_ID, SCNDR_CARRIER_ID, VOYAGE '
         || ' FROM COMB_LANE_DTL '
         || ' WHERE lane_id = :vOldLaneId'
         || ' AND is_routing = 1'
         || ' AND lane_dtl_status < 2';

      OPEN select_cursor FOR select_sql USING vOldLaneId;

      LOOP
         FETCH select_cursor
         INTO vLaneDtlSeq,
              vLaneDtlStatus,
              vEffectiveDT,
              vExpirationDT,
              vCarrierCode,
              vMot,
              vEquipment,
              vServiceLevel,
              vProtectionLevel,
              p_scndr_carrier_code,
			  vVoyage;

         EXIT WHEN select_cursor%NOTFOUND;

         SELECT NVL (MAX (LANE_DTL_SEQ), 0) + 1
           INTO vLaneDtlSeqNew
           FROM COMB_LANE_DTL
          WHERE LANE_ID = vLaneID;

         INSERT INTO COMB_LANE_DTL (TC_COMPANY_ID,
                                    LANE_ID,
                                    LANE_DTL_SEQ,
                                    LANE_DTL_STATUS,
                                    IS_ROUTING,
                                    IS_PREFERRED,
                                    REP_TP_FLAG,
                                    LAST_UPDATED_SOURCE_TYPE,
                                    LAST_UPDATED_SOURCE,
                                    LAST_UPDATED_DTTM,
                                    CARRIER_ID,
                                    MOT_ID,
                                    EQUIPMENT_ID,
                                    SERVICE_LEVEL_ID,
                                    SCNDR_CARRIER_ID,
                                    PROTECTION_LEVEL_ID,
									VOYAGE,
                                    EFFECTIVE_DT,
                                    EXPIRATION_DT,
                                    TIER_ID,
                                    RANK,
                                    YEARLY_CAPACITY,
                                    MONTLY_CAPACITY,
                                    WEEKLY_CAPACITY,
                                    DAILY_CAPACITY,
                                    CAPACITY_SUN,
                                    CAPACITY_MON,
                                    CAPACITY_TUE,
                                    CAPACITY_WED,
                                    CAPACITY_THU,
                                    CAPACITY_FRI,
                                    CAPACITY_SAT,
                                    YEARLY_COMMITMENT,
                                    MONTLY_COMMITMENT,
                                    WEEKLY_COMMITMENT,
                                    DAILY_COMMITMENT,
                                    COMMITMENT_SUN,
                                    COMMITMENT_MON,
                                    COMMITMENT_TUE,
                                    COMMITMENT_WED,
                                    COMMITMENT_THU,
                                    COMMITMENT_FRI,
                                    COMMITMENT_SAT,
                                    YEARLY_COMMIT_PCT,
                                    MONTLY_COMMIT_PCT,
                                    WEEKLY_COMMIT_PCT,
                                    DAILY_COMMIT_PCT,
                                    COMMIT_PCT_SUN,
                                    COMMIT_PCT_MON,
                                    COMMIT_PCT_TUE,
                                    COMMIT_PCT_WED,
                                    COMMIT_PCT_THU,
                                    COMMIT_PCT_FRI,
                                    COMMIT_PCT_SAT,
                                    MASTER_LANE_ID,
                                    TT_TOLERANCE_FACTOR,
                                    SP_LPN_TYPE,
                                    SP_MIN_LPN_COUNT,
                                    SP_MAX_LPN_COUNT,
                                    SP_MIN_WEIGHT,
                                    SP_MAX_WEIGHT,
                                    SP_MIN_VOLUME,
                                    SP_MAX_VOLUME,
                                    SP_MIN_LINEAR_FEET,
                                    SP_MAX_LINEAR_FEET,
                                    SP_MIN_MONETARY_VALUE,
                                    SP_MAX_MONETARY_VALUE,
                                    SP_CURRENCY_CODE,
                                    HAS_SHIPPING_PARAM,
                                    CUBING_INDICATOR,
                                    OVERRIDE_CODE,
                                    PREFERENCE_BONUS_VALUE,
				    REJECT_FURTHER_SHIPMENTS,
				    CARRIER_REJECT_PERIOD)
            SELECT TC_COMPANY_ID,
                   vLaneId,
                   vLaneDtlSeqNew,
                   1,
                   1,
                   IS_PREFERRED,
                   REP_TP_FLAG,
                   3,
                   'OMS',
                   GETDATE (),
                   CARRIER_ID,
                   MOT_ID,
                   EQUIPMENT_ID,
                   SERVICE_LEVEL_ID,
                   SCNDR_CARRIER_ID,
                   PROTECTION_LEVEL_ID,
				   VOYAGE,
                   EFFECTIVE_DT,
                   EXPIRATION_DT,
                   TIER_ID,
                   RANK,
                   YEARLY_CAPACITY,
                   MONTLY_CAPACITY,
                   WEEKLY_CAPACITY,
                   DAILY_CAPACITY,
                   CAPACITY_SUN,
                   CAPACITY_MON,
                   CAPACITY_TUE,
                   CAPACITY_WED,
                   CAPACITY_THU,
                   CAPACITY_FRI,
                   CAPACITY_SAT,
                   YEARLY_COMMITMENT,
                   MONTLY_COMMITMENT,
                   WEEKLY_COMMITMENT,
                   DAILY_COMMITMENT,
                   COMMITMENT_SUN,
                   COMMITMENT_MON,
                   COMMITMENT_TUE,
                   COMMITMENT_WED,
                   COMMITMENT_THU,
                   COMMITMENT_FRI,
                   COMMITMENT_SAT,
                   YEARLY_COMMIT_PCT,
                   MONTLY_COMMIT_PCT,
                   WEEKLY_COMMIT_PCT,
                   DAILY_COMMIT_PCT,
                   COMMIT_PCT_SUN,
                   COMMIT_PCT_MON,
                   COMMIT_PCT_TUE,
                   COMMIT_PCT_WED,
                   COMMIT_PCT_THU,
                   COMMIT_PCT_FRI,
                   COMMIT_PCT_SAT,
                   vMasterLaneId,
                   TT_TOLERANCE_FACTOR,
                   SP_LPN_TYPE,
                   SP_MIN_LPN_COUNT,
                   SP_MAX_LPN_COUNT,
                   SP_MIN_WEIGHT,
                   SP_MAX_WEIGHT,
                   SP_MIN_VOLUME,
                   SP_MAX_VOLUME,
                   SP_MIN_LINEAR_FEET,
                   SP_MAX_LINEAR_FEET,
                   SP_MIN_MONETARY_VALUE,
                   SP_MAX_MONETARY_VALUE,
                   SP_CURRENCY_CODE,
                   HAS_SHIPPING_PARAM,
                   CUBING_INDICATOR,
                   OVERRIDE_CODE,
                   PREFERENCE_BONUS_VALUE,
		   REJECT_FURTHER_SHIPMENTS,
		   CARRIER_REJECT_PERIOD 
              FROM COMB_LANE_DTL
             WHERE LANE_ID = vOldLaneId AND LANE_DTL_SEQ = vLaneDtlSeq;

         INSERT INTO SURGE_CAPACITY (SURGE_CAPACITY_ID,
                                     TC_COMPANY_ID,
                                     LANE_ID,
                                     RG_LANE_DTL_SEQ,
                                     EFFECTIVE_DT,
                                     EXPIRATION_DT,
                                     YEARLY_SURGE_CAPACITY,
                                     MONTHLY_SURGE_CAPACITY,
                                     WEEKLY_SURGE_CAPACITY,
                                     DAILY_SURGE_CAPACITY,
                                     SURGE_CAPACITY_SUN,
                                     SURGE_CAPACITY_MON,
                                     SURGE_CAPACITY_TUE,
                                     SURGE_CAPACITY_WED,
                                     SURGE_CAPACITY_THU,
                                     SURGE_CAPACITY_FRI,
                                     SURGE_CAPACITY_SAT)
            SELECT SEQ_SURGE_CAPACITY_ID.NEXTVAL,
                   TC_COMPANY_ID,
                   vLaneId,
                   vLaneDtlSeqNew,
                   EFFECTIVE_DT,
                   EXPIRATION_DT,
                   YEARLY_SURGE_CAPACITY,
                   MONTHLY_SURGE_CAPACITY,
                   WEEKLY_SURGE_CAPACITY,
                   DAILY_SURGE_CAPACITY,
                   SURGE_CAPACITY_SUN,
                   SURGE_CAPACITY_MON,
                   SURGE_CAPACITY_TUE,
                   SURGE_CAPACITY_WED,
                   SURGE_CAPACITY_THU,
                   SURGE_CAPACITY_FRI,
                   SURGE_CAPACITY_SAT
              FROM SURGE_CAPACITY
             WHERE LANE_ID = vOldLaneId AND RG_LANE_DTL_SEQ = vLaneDtlSeq;

         IF (vLaneDtlStatus = 0)
         THEN
            ADJUST_EFF_EXP_DT (vTCCompanyId,
                               vLaneId,
                               vLaneDtlSeqNew,
                               NULL,
                               vEffectiveDT,
                               vExpirationDT,
                               vCarrierCode,
                               vMot,
                               vEquipment,
                               vServiceLevel,
                               p_scndr_carrier_code,
                               vProtectionLevel,
							   vVoyage);

            UPDATE COMB_LANE_DTL
               SET LANE_DTL_STATUS = 0, LAST_UPDATED_DTTM = GETDATE ()
             WHERE     LANE_ID = vLaneId
                   AND LANE_DTL_SEQ = vLaneDtlSeqNew
                   AND LANE_DTL_STATUS = 1;
         END IF;
      END LOOP;

      CLOSE select_cursor;
   END COPY_CARRIERS;

   -- SR1 , 4R1 change, build92,  TT31182 [END]

   PROCEDURE MERGE_DRAFT (
      vTCCompanyId     IN     company.COMPANY_ID%TYPE,
      vLaneId          IN     comb_lane.LANE_ID%TYPE,
      vLaneDtlSeq      IN     comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vLaneDtlSeqUpd   IN     comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      REC_CNT             OUT NUMBER)
   AS
   BEGIN
      REC_CNT :=
         MERGE_DRAFT (vTCCompanyId,
                      vLaneId,
                      vLaneDtlSeq,
                      vLaneDtlSeqUpd);
   END MERGE_DRAFT;

   FUNCTION MERGE_DRAFT (vTCCompanyId     IN company.COMPANY_ID%TYPE,
                         vLaneId          IN comb_lane.LANE_ID%TYPE,
                         vLaneDtlSeq      IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
                         vLaneDtlSeqUpd   IN comb_lane_dtl.LANE_DTL_SEQ%TYPE)
      RETURN INT
   IS
      tempCount   INT;
   BEGIN
      tempCount := 0;

      SELECT COUNT (*)
        INTO tempCount
        FROM COMB_LANE_DTL
       WHERE     LANE_ID = vLaneId
             AND LANE_DTL_SEQ = vLaneDtlSeqUpd
             AND IS_SAILING = 0
             AND IS_RATING = 0;

      IF (tempCount > 0)
      THEN
         DELETE_CHILD_ROW (vTCCompanyId, vLaneId, vLaneDtlSeqUpd);
         COPY_CHILD_ROW (vTCCompanyId,
                         vLaneId,
                         -1,
                         vLaneDtlSeq,
                         vLaneDtlSeqUpd,
                         0,
                         1,
                         0);

         FOR CUR_DTL_UPD
            IN (SELECT EFFECTIVE_DT,
                       EXPIRATION_DT,
                       CARRIER_ID,
                       MOT_ID,
                       SERVICE_LEVEL_ID,
                       SCNDR_CARRIER_ID,
                       PROTECTION_LEVEL_ID,
                       EQUIPMENT_ID,
					   VOYAGE,
                       IS_PREFERRED,
                       REP_TP_FLAG,
                       TIER_ID,
                       RANK,
                       YEARLY_CAPACITY,
                       MONTLY_CAPACITY,
                       WEEKLY_CAPACITY,
                       DAILY_CAPACITY,
                       CAPACITY_SUN,
                       CAPACITY_MON,
                       CAPACITY_TUE,
                       CAPACITY_WED,
                       CAPACITY_THU,
                       CAPACITY_FRI,
                       CAPACITY_SAT,
                       YEARLY_COMMITMENT,
                       MONTLY_COMMITMENT,
                       WEEKLY_COMMITMENT,
                       DAILY_COMMITMENT,
                       COMMITMENT_SUN,
                       COMMITMENT_MON,
                       COMMITMENT_TUE,
                       COMMITMENT_WED,
                       COMMITMENT_THU,
                       COMMITMENT_FRI,
                       COMMITMENT_SAT,
                       YEARLY_COMMIT_PCT,
                       MONTLY_COMMIT_PCT,
                       WEEKLY_COMMIT_PCT,
                       DAILY_COMMIT_PCT,
                       COMMIT_PCT_SUN,
                       COMMIT_PCT_MON,
                       COMMIT_PCT_TUE,
                       COMMIT_PCT_WED,
                       COMMIT_PCT_THU,
                       COMMIT_PCT_FRI,
                       COMMIT_PCT_SAT,
                       TT_TOLERANCE_FACTOR,
                       SP_LPN_TYPE,
                       SP_MIN_LPN_COUNT,
                       SP_MAX_LPN_COUNT,
                       SP_MIN_WEIGHT,
                       SP_MAX_WEIGHT,
                       SP_MIN_VOLUME,
                       SP_MAX_VOLUME,
                       SP_MIN_LINEAR_FEET,
                       SP_MAX_LINEAR_FEET,
                       SP_MIN_MONETARY_VALUE,
                       SP_MAX_MONETARY_VALUE,
                       SP_CURRENCY_CODE,
                       HAS_SHIPPING_PARAM,
                       CUBING_INDICATOR,
                       OVERRIDE_CODE,
                       PREFERENCE_BONUS_VALUE,
                       REJECT_FURTHER_SHIPMENTS,
                       CARRIER_REJECT_PERIOD
                  FROM COMB_LANE_DTL
                 WHERE LANE_ID = vLaneId AND LANE_DTL_SEQ = vLaneDtlSeq)
         LOOP
            UPDATE comb_lane_dtl
               SET LAST_UPDATED_SOURCE_TYPE = 3,
                   LAST_UPDATED_SOURCE = 'OMS',
                   LAST_UPDATED_DTTM = getdate (),
                   EFFECTIVE_DT = CUR_DTL_UPD.EFFECTIVE_DT,
                   EXPIRATION_DT = CUR_DTL_UPD.EXPIRATION_DT,
                   CARRIER_ID = CUR_DTL_UPD.CARRIER_ID,
                   MOT_ID = CUR_DTL_UPD.MOT_ID,
                   SERVICE_LEVEL_ID = CUR_DTL_UPD.SERVICE_LEVEL_ID,
                   SCNDR_CARRIER_ID = CUR_DTL_UPD.SCNDR_CARRIER_ID,
                   PROTECTION_LEVEL_ID = CUR_DTL_UPD.PROTECTION_LEVEL_ID,
                   EQUIPMENT_ID = CUR_DTL_UPD.EQUIPMENT_ID,
				   VOYAGE = CUR_DTL_UPD.VOYAGE,
                   IS_PREFERRED = CUR_DTL_UPD.IS_PREFERRED,
                   REP_TP_FLAG = CUR_DTL_UPD.REP_TP_FLAG,
                   TIER_ID = CUR_DTL_UPD.TIER_ID,
                   RANK = CUR_DTL_UPD.RANK,
                   YEARLY_CAPACITY = CUR_DTL_UPD.YEARLY_CAPACITY,
                   MONTLY_CAPACITY = CUR_DTL_UPD.MONTLY_CAPACITY,
                   WEEKLY_CAPACITY = CUR_DTL_UPD.WEEKLY_CAPACITY,
                   DAILY_CAPACITY = CUR_DTL_UPD.DAILY_CAPACITY,
                   CAPACITY_SUN = CUR_DTL_UPD.CAPACITY_SUN,
                   CAPACITY_MON = CUR_DTL_UPD.CAPACITY_MON,
                   CAPACITY_TUE = CUR_DTL_UPD.CAPACITY_TUE,
                   CAPACITY_WED = CUR_DTL_UPD.CAPACITY_WED,
                   CAPACITY_THU = CUR_DTL_UPD.CAPACITY_THU,
                   CAPACITY_FRI = CUR_DTL_UPD.CAPACITY_FRI,
                   CAPACITY_SAT = CUR_DTL_UPD.CAPACITY_SAT,
                   YEARLY_COMMITMENT = CUR_DTL_UPD.YEARLY_COMMITMENT,
                   MONTLY_COMMITMENT = CUR_DTL_UPD.MONTLY_COMMITMENT,
                   WEEKLY_COMMITMENT = CUR_DTL_UPD.WEEKLY_COMMITMENT,
                   DAILY_COMMITMENT = CUR_DTL_UPD.DAILY_COMMITMENT,
                   COMMITMENT_SUN = CUR_DTL_UPD.COMMITMENT_SUN,
                   COMMITMENT_MON = CUR_DTL_UPD.COMMITMENT_MON,
                   COMMITMENT_TUE = CUR_DTL_UPD.COMMITMENT_TUE,
                   COMMITMENT_WED = CUR_DTL_UPD.COMMITMENT_WED,
                   COMMITMENT_THU = CUR_DTL_UPD.COMMITMENT_THU,
                   COMMITMENT_FRI = CUR_DTL_UPD.COMMITMENT_FRI,
                   COMMITMENT_SAT = CUR_DTL_UPD.COMMITMENT_SAT,
                   YEARLY_COMMIT_PCT = CUR_DTL_UPD.YEARLY_COMMIT_PCT,
                   MONTLY_COMMIT_PCT = CUR_DTL_UPD.MONTLY_COMMIT_PCT,
                   WEEKLY_COMMIT_PCT = CUR_DTL_UPD.WEEKLY_COMMIT_PCT,
                   DAILY_COMMIT_PCT = CUR_DTL_UPD.DAILY_COMMIT_PCT,
                   COMMIT_PCT_SUN = CUR_DTL_UPD.COMMIT_PCT_SUN,
                   COMMIT_PCT_MON = CUR_DTL_UPD.COMMIT_PCT_MON,
                   COMMIT_PCT_TUE = CUR_DTL_UPD.COMMIT_PCT_TUE,
                   COMMIT_PCT_WED = CUR_DTL_UPD.COMMIT_PCT_WED,
                   COMMIT_PCT_THU = CUR_DTL_UPD.COMMIT_PCT_THU,
                   COMMIT_PCT_FRI = CUR_DTL_UPD.COMMIT_PCT_FRI,
                   COMMIT_PCT_SAT = CUR_DTL_UPD.COMMIT_PCT_SAT,
                   TT_TOLERANCE_FACTOR = CUR_DTL_UPD.TT_TOLERANCE_FACTOR,
                   SP_LPN_TYPE = CUR_DTL_UPD.SP_LPN_TYPE,
                   SP_MIN_LPN_COUNT = CUR_DTL_UPD.SP_MIN_LPN_COUNT,
                   SP_MAX_LPN_COUNT = CUR_DTL_UPD.SP_MAX_LPN_COUNT,
                   SP_MIN_WEIGHT = CUR_DTL_UPD.SP_MIN_WEIGHT,
                   SP_MAX_WEIGHT = CUR_DTL_UPD.SP_MAX_WEIGHT,
                   SP_MIN_VOLUME = CUR_DTL_UPD.SP_MIN_VOLUME,
                   SP_MAX_VOLUME = CUR_DTL_UPD.SP_MAX_VOLUME,
                   SP_MIN_LINEAR_FEET = CUR_DTL_UPD.SP_MIN_LINEAR_FEET,
                   SP_MAX_LINEAR_FEET = CUR_DTL_UPD.SP_MAX_LINEAR_FEET,
                   SP_MIN_MONETARY_VALUE = CUR_DTL_UPD.SP_MIN_MONETARY_VALUE,
                   SP_MAX_MONETARY_VALUE = CUR_DTL_UPD.SP_MAX_MONETARY_VALUE,
                   SP_CURRENCY_CODE = CUR_DTL_UPD.SP_CURRENCY_CODE,
                   HAS_SHIPPING_PARAM = CUR_DTL_UPD.HAS_SHIPPING_PARAM,
                   CUBING_INDICATOR = CUR_DTL_UPD.CUBING_INDICATOR,
                   OVERRIDE_CODE = CUR_DTL_UPD.OVERRIDE_CODE,
                   PREFERENCE_BONUS_VALUE = CUR_DTL_UPD.PREFERENCE_BONUS_VALUE,
                   REJECT_FURTHER_SHIPMENTS = CUR_DTL_UPD.REJECT_FURTHER_SHIPMENTS,
                   CARRIER_REJECT_PERIOD = CUR_DTL_UPD.CARRIER_REJECT_PERIOD
             WHERE lane_id = vLaneId AND lane_dtl_seq = vLaneDtlSeqUpd;
         END LOOP;
      END IF;

      RETURN tempCount;
   END MERGE_DRAFT;

   PROCEDURE UPDATE_ROW (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vLaneId            IN comb_lane.LANE_ID%TYPE,
      vRGDtlSeq          IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vCurrentRGDtlSeq   IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vExpirationDT      IN comb_lane_dtl.EXPIRATION_DT%TYPE)
   IS
      vRGDtlSeqNew   comb_lane_dtl.LANE_DTL_SEQ%TYPE;
   BEGIN
      FOR CUR_DTL IN (SELECT IS_PREFERRED,
                             REP_TP_FLAG,
                             EQUIPMENT_ID,
                             PROTECTION_LEVEL_ID,
							 VOYAGE,
                             TIER_ID,
                             RANK,
                             YEARLY_CAPACITY,
                             MONTLY_CAPACITY,
                             WEEKLY_CAPACITY,
                             DAILY_CAPACITY,
                             CAPACITY_SUN,
                             CAPACITY_MON,
                             CAPACITY_TUE,
                             CAPACITY_WED,
                             CAPACITY_THU,
                             CAPACITY_FRI,
                             CAPACITY_SAT,
                             YEARLY_COMMITMENT,
                             MONTLY_COMMITMENT,
                             WEEKLY_COMMITMENT,
                             DAILY_COMMITMENT,
                             COMMITMENT_SUN,
                             COMMITMENT_MON,
                             COMMITMENT_TUE,
                             COMMITMENT_WED,
                             COMMITMENT_THU,
                             COMMITMENT_FRI,
                             COMMITMENT_SAT,
                             YEARLY_COMMIT_PCT,
                             MONTLY_COMMIT_PCT,
                             WEEKLY_COMMIT_PCT,
                             DAILY_COMMIT_PCT,
                             COMMIT_PCT_SUN,
                             COMMIT_PCT_MON,
                             COMMIT_PCT_TUE,
                             COMMIT_PCT_WED,
                             COMMIT_PCT_THU,
                             COMMIT_PCT_FRI,
                             COMMIT_PCT_SAT,
                             TT_TOLERANCE_FACTOR,
                             SP_LPN_TYPE,
                             SP_MIN_LPN_COUNT,
                             SP_MAX_LPN_COUNT,
                             SP_MIN_WEIGHT,
                             SP_MAX_WEIGHT,
                             SP_MIN_VOLUME,
                             SP_MAX_VOLUME,
                             SP_MIN_LINEAR_FEET,
                             SP_MAX_LINEAR_FEET,
                             SP_MIN_MONETARY_VALUE,
                             SP_MAX_MONETARY_VALUE,
                             SP_CURRENCY_CODE,
                             HAS_SHIPPING_PARAM,
                             CUBING_INDICATOR,
                             OVERRIDE_CODE,
                             PREFERENCE_BONUS_VALUE,
                             REJECT_FURTHER_SHIPMENTS,
                             CARRIER_REJECT_PERIOD
                        FROM COMB_LANE_DTL
                       WHERE LANE_ID = vLaneId AND LANE_DTL_SEQ = vRGDtlSeq)
      LOOP
         UPDATE comb_lane_dtl
            SET IS_ROUTING = 1,
                EXPIRATION_DT = vExpirationDT,
                LAST_UPDATED_SOURCE_TYPE = 3,
                LAST_UPDATED_SOURCE = 'OMS',
                LAST_UPDATED_DTTM = getdate (),
                IS_PREFERRED = CUR_DTL.IS_PREFERRED,
                REP_TP_FLAG = CUR_DTL.REP_TP_FLAG,
                EQUIPMENT_ID = CUR_DTL.EQUIPMENT_ID,
                PROTECTION_LEVEL_ID = CUR_DTL.PROTECTION_LEVEL_ID,
				VOYAGE = CUR_DTL.VOYAGE,
                TIER_ID = CUR_DTL.TIER_ID,
                RANK = CUR_DTL.RANK,
                YEARLY_CAPACITY = CUR_DTL.YEARLY_CAPACITY,
                MONTLY_CAPACITY = CUR_DTL.MONTLY_CAPACITY,
                WEEKLY_CAPACITY = CUR_DTL.WEEKLY_CAPACITY,
                DAILY_CAPACITY = CUR_DTL.DAILY_CAPACITY,
                CAPACITY_SUN = CUR_DTL.CAPACITY_SUN,
                CAPACITY_MON = CUR_DTL.CAPACITY_MON,
                CAPACITY_TUE = CUR_DTL.CAPACITY_TUE,
                CAPACITY_WED = CUR_DTL.CAPACITY_WED,
                CAPACITY_THU = CUR_DTL.CAPACITY_THU,
                CAPACITY_FRI = CUR_DTL.CAPACITY_FRI,
                CAPACITY_SAT = CUR_DTL.CAPACITY_SAT,
                YEARLY_COMMITMENT = CUR_DTL.YEARLY_COMMITMENT,
                MONTLY_COMMITMENT = CUR_DTL.MONTLY_COMMITMENT,
                WEEKLY_COMMITMENT = CUR_DTL.WEEKLY_COMMITMENT,
                DAILY_COMMITMENT = CUR_DTL.DAILY_COMMITMENT,
                COMMITMENT_SUN = CUR_DTL.COMMITMENT_SUN,
                COMMITMENT_MON = CUR_DTL.COMMITMENT_MON,
                COMMITMENT_TUE = CUR_DTL.COMMITMENT_TUE,
                COMMITMENT_WED = CUR_DTL.COMMITMENT_WED,
                COMMITMENT_THU = CUR_DTL.COMMITMENT_THU,
                COMMITMENT_FRI = CUR_DTL.COMMITMENT_FRI,
                COMMITMENT_SAT = CUR_DTL.COMMITMENT_SAT,
                YEARLY_COMMIT_PCT = CUR_DTL.YEARLY_COMMIT_PCT,
                MONTLY_COMMIT_PCT = CUR_DTL.MONTLY_COMMIT_PCT,
                WEEKLY_COMMIT_PCT = CUR_DTL.WEEKLY_COMMIT_PCT,
                DAILY_COMMIT_PCT = CUR_DTL.DAILY_COMMIT_PCT,
                COMMIT_PCT_SUN = CUR_DTL.COMMIT_PCT_SUN,
                COMMIT_PCT_MON = CUR_DTL.COMMIT_PCT_MON,
                COMMIT_PCT_TUE = CUR_DTL.COMMIT_PCT_TUE,
                COMMIT_PCT_WED = CUR_DTL.COMMIT_PCT_WED,
                COMMIT_PCT_THU = CUR_DTL.COMMIT_PCT_THU,
                COMMIT_PCT_FRI = CUR_DTL.COMMIT_PCT_FRI,
                COMMIT_PCT_SAT = CUR_DTL.COMMIT_PCT_SAT,
                TT_TOLERANCE_FACTOR = CUR_DTL.TT_TOLERANCE_FACTOR,
                SP_LPN_TYPE = CUR_DTL.SP_LPN_TYPE,
                SP_MIN_LPN_COUNT = CUR_DTL.SP_MIN_LPN_COUNT,
                SP_MAX_LPN_COUNT = CUR_DTL.SP_MAX_LPN_COUNT,
                SP_MIN_WEIGHT = CUR_DTL.SP_MIN_WEIGHT,
                SP_MAX_WEIGHT = CUR_DTL.SP_MAX_WEIGHT,
                SP_MIN_VOLUME = CUR_DTL.SP_MIN_VOLUME,
                SP_MAX_VOLUME = CUR_DTL.SP_MAX_VOLUME,
                SP_MIN_LINEAR_FEET = CUR_DTL.SP_MIN_LINEAR_FEET,
                SP_MAX_LINEAR_FEET = CUR_DTL.SP_MAX_LINEAR_FEET,
                SP_MIN_MONETARY_VALUE = CUR_DTL.SP_MIN_MONETARY_VALUE,
                SP_MAX_MONETARY_VALUE = CUR_DTL.SP_MAX_MONETARY_VALUE,
                SP_CURRENCY_CODE = CUR_DTL.SP_CURRENCY_CODE,
                HAS_SHIPPING_PARAM = CUR_DTL.HAS_SHIPPING_PARAM,
                CUBING_INDICATOR = CUR_DTL.CUBING_INDICATOR,
                OVERRIDE_CODE = CUR_DTL.OVERRIDE_CODE,
                PREFERENCE_BONUS_VALUE = CUR_DTL.PREFERENCE_BONUS_VALUE,
                REJECT_FURTHER_SHIPMENTS = CUR_DTL.REJECT_FURTHER_SHIPMENTS,
                CARRIER_REJECT_PERIOD = CUR_DTL.CARRIER_REJECT_PERIOD
          WHERE lane_id = vLaneId AND lane_dtl_seq = vCurrentRGDtlSeq;
      END LOOP;
   END UPDATE_ROW;

   PROCEDURE CREATE_NEW_ROW (
      vTCCompanyId    IN company.COMPANY_ID%TYPE,
      vLaneId         IN comb_lane.LANE_ID%TYPE,
      vRGDtlSeq       IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vEffectiveDT    IN comb_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT   IN comb_lane_dtl.EXPIRATION_DT%TYPE)
   IS
      vRGDtlSeqNew   comb_lane_dtl.LANE_DTL_SEQ%TYPE;
   BEGIN
      SELECT MAX (LANE_DTL_SEQ) + 1
        INTO vRGDtlSeqNew
        FROM COMB_LANE_DTL
       WHERE LANE_ID = vLaneID;

      INSERT INTO COMB_LANE_DTL (TC_COMPANY_ID,
                                 LANE_ID,
                                 LANE_DTL_SEQ,
                                 LANE_DTL_STATUS,
                                 IS_ROUTING,
                                 IS_PREFERRED,
                                 REP_TP_FLAG,
                                 LAST_UPDATED_SOURCE_TYPE,
                                 LAST_UPDATED_SOURCE,
                                 LAST_UPDATED_DTTM,
                                 CARRIER_ID,
                                 MOT_ID,
                                 EQUIPMENT_ID,
                                 SERVICE_LEVEL_ID,
                                 SCNDR_CARRIER_ID,
                                 PROTECTION_LEVEL_ID,
								 VOYAGE,
                                 EFFECTIVE_DT,
                                 EXPIRATION_DT,
                                 TIER_ID,
                                 RANK,
                                 YEARLY_CAPACITY,
                                 MONTLY_CAPACITY,
                                 WEEKLY_CAPACITY,
                                 DAILY_CAPACITY,
                                 CAPACITY_SUN,
                                 CAPACITY_MON,
                                 CAPACITY_TUE,
                                 CAPACITY_WED,
                                 CAPACITY_THU,
                                 CAPACITY_FRI,
                                 CAPACITY_SAT,
                                 YEARLY_COMMITMENT,
                                 MONTLY_COMMITMENT,
                                 WEEKLY_COMMITMENT,
                                 DAILY_COMMITMENT,
                                 COMMITMENT_SUN,
                                 COMMITMENT_MON,
                                 COMMITMENT_TUE,
                                 COMMITMENT_WED,
                                 COMMITMENT_THU,
                                 COMMITMENT_FRI,
                                 COMMITMENT_SAT,
                                 YEARLY_COMMIT_PCT,
                                 MONTLY_COMMIT_PCT,
                                 WEEKLY_COMMIT_PCT,
                                 DAILY_COMMIT_PCT,
                                 COMMIT_PCT_SUN,
                                 COMMIT_PCT_MON,
                                 COMMIT_PCT_TUE,
                                 COMMIT_PCT_WED,
                                 COMMIT_PCT_THU,
                                 COMMIT_PCT_FRI,
                                 COMMIT_PCT_SAT,
                                 MASTER_LANE_ID,
                                 TT_TOLERANCE_FACTOR,
                                 SP_LPN_TYPE,
                                 SP_MIN_LPN_COUNT,
                                 SP_MAX_LPN_COUNT,
                                 SP_MIN_WEIGHT,
                                 SP_MAX_WEIGHT,
                                 SP_MIN_VOLUME,
                                 SP_MAX_VOLUME,
                                 SP_MIN_LINEAR_FEET,
                                 SP_MAX_LINEAR_FEET,
                                 SP_MIN_MONETARY_VALUE,
                                 SP_MAX_MONETARY_VALUE,
                                 SP_CURRENCY_CODE,
                                 HAS_SHIPPING_PARAM,
                                 CUBING_INDICATOR,
                                 OVERRIDE_CODE,
                                 PREFERENCE_BONUS_VALUE,
                                 REJECT_FURTHER_SHIPMENTS,
                                 CARRIER_REJECT_PERIOD)
         SELECT TC_COMPANY_ID,
                LANE_ID,
                vRGDtlSeqNew,
                0,
                1,
                IS_PREFERRED,
                REP_TP_FLAG,
                3,
                'OMS',
                GETDATE (),
                CARRIER_ID,
                MOT_ID,
                EQUIPMENT_ID,
                SERVICE_LEVEL_ID,
                SCNDR_CARRIER_ID,
                PROTECTION_LEVEL_ID,
				VOYAGE,
                vEffectiveDT,
                vExpirationDT,
                TIER_ID,
                RANK,
                YEARLY_CAPACITY,
                MONTLY_CAPACITY,
                WEEKLY_CAPACITY,
                DAILY_CAPACITY,
                CAPACITY_SUN,
                CAPACITY_MON,
                CAPACITY_TUE,
                CAPACITY_WED,
                CAPACITY_THU,
                CAPACITY_FRI,
                CAPACITY_SAT,
                YEARLY_COMMITMENT,
                MONTLY_COMMITMENT,
                WEEKLY_COMMITMENT,
                DAILY_COMMITMENT,
                COMMITMENT_SUN,
                COMMITMENT_MON,
                COMMITMENT_TUE,
                COMMITMENT_WED,
                COMMITMENT_THU,
                COMMITMENT_FRI,
                COMMITMENT_SAT,
                YEARLY_COMMIT_PCT,
                MONTLY_COMMIT_PCT,
                WEEKLY_COMMIT_PCT,
                DAILY_COMMIT_PCT,
                COMMIT_PCT_SUN,
                COMMIT_PCT_MON,
                COMMIT_PCT_TUE,
                COMMIT_PCT_WED,
                COMMIT_PCT_THU,
                COMMIT_PCT_FRI,
                COMMIT_PCT_SAT,
                MASTER_LANE_ID,
                TT_TOLERANCE_FACTOR,
                SP_LPN_TYPE,
                SP_MIN_LPN_COUNT,
                SP_MAX_LPN_COUNT,
                SP_MIN_WEIGHT,
                SP_MAX_WEIGHT,
                SP_MIN_VOLUME,
                SP_MAX_VOLUME,
                SP_MIN_LINEAR_FEET,
                SP_MAX_LINEAR_FEET,
                SP_MIN_MONETARY_VALUE,
                SP_MAX_MONETARY_VALUE,
                SP_CURRENCY_CODE,
                HAS_SHIPPING_PARAM,
                CUBING_INDICATOR,
                OVERRIDE_CODE,
                PREFERENCE_BONUS_VALUE,
                REJECT_FURTHER_SHIPMENTS,
                CARRIER_REJECT_PERIOD
           FROM COMB_LANE_DTL
          WHERE LANE_ID = vLaneId AND LANE_DTL_SEQ = vRGDtlSeq;

      COPY_CHILD_ROW (vTCCompanyId,
                      vLaneId,
                      vRGDtlSeq,
                      vRGDtlSeqNew,
                      vRGDtlSeqNew,
                      0,
                      1,
                      0);
   END CREATE_NEW_ROW;

   PROCEDURE REPLICATE_ROW (
      vTCCompanyId     IN company.COMPANY_ID%TYPE,
      vLaneId          IN comb_lane.LANE_ID%TYPE,
      vOverlappedSeq   IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vRGDtlSeq        IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vEffectiveDT     IN comb_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT    IN comb_lane_dtl.EXPIRATION_DT%TYPE,
      vIsRating        IN comb_lane_dtl.IS_RATING%TYPE,
      vIsRouting       IN comb_lane_dtl.IS_ROUTING%TYPE,
      vIsSailing       IN comb_lane_dtl.IS_SAILING%TYPE)
   IS
      vRGDtlSeqNew         comb_lane_dtl.LANE_DTL_SEQ%TYPE;
      vRGDtlSeqTemp        comb_lane_dtl.LANE_DTL_SEQ%TYPE;
      vIsBudgetedTemp      comb_lane_dtl.IS_BUDGETED%TYPE;
      vTransitUOMTemp      comb_lane_dtl.FIXED_TRANSIT_STANDARD_UOM%TYPE;
      vTransitValueTemp    comb_lane_dtl.FIXED_TRANSIT_VALUE%TYPE;
      vFrequencyTypeTemp   comb_lane_dtl.SAILING_FREQUENCY_TYPE%TYPE;
      vScheduleNameTemp    comb_lane_dtl.SAILING_SCHEDULE_NAME%TYPE;
      vIsSailingTemp       comb_lane_dtl.IS_SAILING%TYPE;
      vIsRatingTemp        comb_lane_dtl.IS_RATING%TYPE;
   BEGIN
      SELECT MAX (LANE_DTL_SEQ) + 1
        INTO vRGDtlSeqNew
        FROM COMB_LANE_DTL
       WHERE LANE_ID = vLaneID;

      SELECT IS_BUDGETED,
             FIXED_TRANSIT_STANDARD_UOM,
             FIXED_TRANSIT_VALUE,
             SAILING_FREQUENCY_TYPE,
             SAILING_SCHEDULE_NAME,
             IS_SAILING,
             IS_RATING
        INTO vIsBudgetedTemp,
             vTransitUOMTemp,
             vTransitValueTemp,
             vFrequencyTypeTemp,
             vScheduleNameTemp,
             vIsSailingTemp,
             vIsRatingTemp
        FROM COMB_LANE_DTL
       WHERE LANE_ID = vLaneId AND LANE_DTL_SEQ = vRGDtlSeq;

      IF (vOverlappedSeq = -1)
      THEN
         vRGDtlSeqTemp := vRGDtlSeq;
      ELSE
         vRGDtlSeqTemp := vOverlappedSeq;
      END IF;

      INSERT INTO COMB_LANE_DTL (TC_COMPANY_ID,
                                 LANE_ID,
                                 LANE_DTL_SEQ,
                                 LANE_DTL_STATUS,
                                 IS_RATING,
                                 IS_ROUTING,
                                 IS_BUDGETED,
                                 IS_PREFERRED,
                                 REP_TP_FLAG,
                                 LAST_UPDATED_SOURCE_TYPE,
                                 LAST_UPDATED_SOURCE,
                                 LAST_UPDATED_DTTM,
                                 CARRIER_ID,
                                 MOT_ID,
                                 EQUIPMENT_ID,
                                 SERVICE_LEVEL_ID,
                                 SCNDR_CARRIER_ID,
                                 PROTECTION_LEVEL_ID,
								 VOYAGE,
                                 EFFECTIVE_DT,
                                 EXPIRATION_DT,
                                 TIER_ID,
                                 RANK,
                                 YEARLY_CAPACITY,
                                 MONTLY_CAPACITY,
                                 WEEKLY_CAPACITY,
                                 DAILY_CAPACITY,
                                 CAPACITY_SUN,
                                 CAPACITY_MON,
                                 CAPACITY_TUE,
                                 CAPACITY_WED,
                                 CAPACITY_THU,
                                 CAPACITY_FRI,
                                 CAPACITY_SAT,
                                 YEARLY_COMMITMENT,
                                 MONTLY_COMMITMENT,
                                 WEEKLY_COMMITMENT,
                                 DAILY_COMMITMENT,
                                 COMMITMENT_SUN,
                                 COMMITMENT_MON,
                                 COMMITMENT_TUE,
                                 COMMITMENT_WED,
                                 COMMITMENT_THU,
                                 COMMITMENT_FRI,
                                 COMMITMENT_SAT,
                                 YEARLY_COMMIT_PCT,
                                 MONTLY_COMMIT_PCT,
                                 WEEKLY_COMMIT_PCT,
                                 DAILY_COMMIT_PCT,
                                 COMMIT_PCT_SUN,
                                 COMMIT_PCT_MON,
                                 COMMIT_PCT_TUE,
                                 COMMIT_PCT_WED,
                                 COMMIT_PCT_THU,
                                 COMMIT_PCT_FRI,
                                 COMMIT_PCT_SAT,
                                 FIXED_TRANSIT_STANDARD_UOM,
                                 FIXED_TRANSIT_VALUE,
                                 IS_SAILING,
                                 SAILING_FREQUENCY_TYPE,
                                 SAILING_SCHEDULE_NAME,
                                 MASTER_LANE_ID,
                                 O_SHIP_VIA,
                                 D_SHIP_VIA,
                                 CONTRACT_NUMBER,
                                 CUSTOM_TEXT1,
                                 CUSTOM_TEXT2,
                                 CUSTOM_TEXT3,
                                 CUSTOM_TEXT4,
                                 CUSTOM_TEXT5,
                                 TT_TOLERANCE_FACTOR,
                                 SP_LPN_TYPE,
                                 SP_MIN_LPN_COUNT,
                                 SP_MAX_LPN_COUNT,
                                 SP_MIN_WEIGHT,
                                 SP_MAX_WEIGHT,
                                 SP_MIN_VOLUME,
                                 SP_MAX_VOLUME,
                                 SP_MIN_LINEAR_FEET,
                                 SP_MAX_LINEAR_FEET,
                                 SP_MIN_MONETARY_VALUE,
                                 SP_MAX_MONETARY_VALUE,
                                 SP_CURRENCY_CODE,
                                 HAS_SHIPPING_PARAM,
                                 CUBING_INDICATOR,
                                 OVERRIDE_CODE,
                                 PREFERENCE_BONUS_VALUE,
                                 REJECT_FURTHER_SHIPMENTS,
                                 CARRIER_REJECT_PERIOD)
         SELECT TC_COMPANY_ID,
                LANE_ID,
                vRGDtlSeqNew,
                0,
                vIsRatingTemp,
                vIsRouting,
                vIsBudgetedTemp,
                IS_PREFERRED,
                REP_TP_FLAG,
                3,
                'OMS',
                GETDATE (),
                CARRIER_ID,
                MOT_ID,
                EQUIPMENT_ID,
                SERVICE_LEVEL_ID,
                SCNDR_CARRIER_ID,
                PROTECTION_LEVEL_ID,
				VOYAGE,
                vEffectiveDT,
                vExpirationDT,
                TIER_ID,
                RANK,
                YEARLY_CAPACITY,
                MONTLY_CAPACITY,
                WEEKLY_CAPACITY,
                DAILY_CAPACITY,
                CAPACITY_SUN,
                CAPACITY_MON,
                CAPACITY_TUE,
                CAPACITY_WED,
                CAPACITY_THU,
                CAPACITY_FRI,
                CAPACITY_SAT,
                YEARLY_COMMITMENT,
                MONTLY_COMMITMENT,
                WEEKLY_COMMITMENT,
                DAILY_COMMITMENT,
                COMMITMENT_SUN,
                COMMITMENT_MON,
                COMMITMENT_TUE,
                COMMITMENT_WED,
                COMMITMENT_THU,
                COMMITMENT_FRI,
                COMMITMENT_SAT,
                YEARLY_COMMIT_PCT,
                MONTLY_COMMIT_PCT,
                WEEKLY_COMMIT_PCT,
                DAILY_COMMIT_PCT,
                COMMIT_PCT_SUN,
                COMMIT_PCT_MON,
                COMMIT_PCT_TUE,
                COMMIT_PCT_WED,
                COMMIT_PCT_THU,
                COMMIT_PCT_FRI,
                COMMIT_PCT_SAT,
                vTransitUOMTemp,
                vTransitValueTemp,
                vIsSailingTemp,
                vFrequencyTypeTemp,
                vScheduleNameTemp,
                MASTER_LANE_ID,
                O_SHIP_VIA,
                D_SHIP_VIA,
                CONTRACT_NUMBER,
                CUSTOM_TEXT1,
                CUSTOM_TEXT2,
                CUSTOM_TEXT3,
                CUSTOM_TEXT4,
                CUSTOM_TEXT5,
                TT_TOLERANCE_FACTOR,
                SP_LPN_TYPE,
                SP_MIN_LPN_COUNT,
                SP_MAX_LPN_COUNT,
                SP_MIN_WEIGHT,
                SP_MAX_WEIGHT,
                SP_MIN_VOLUME,
                SP_MAX_VOLUME,
                SP_MIN_LINEAR_FEET,
                SP_MAX_LINEAR_FEET,
                SP_MIN_MONETARY_VALUE,
                SP_MAX_MONETARY_VALUE,
                SP_CURRENCY_CODE,
                HAS_SHIPPING_PARAM,
                CUBING_INDICATOR,
                OVERRIDE_CODE,
                PREFERENCE_BONUS_VALUE,
                REJECT_FURTHER_SHIPMENTS,
                CARRIER_REJECT_PERIOD
           FROM COMB_LANE_DTL
          WHERE LANE_ID = vLaneId AND LANE_DTL_SEQ = vRGDtlSeqTemp;

      COPY_CHILD_ROW (vTCCompanyId,
                      vLaneId,
                      vOverlappedSeq,
                      vRGDtlSeq,
                      vRGDtlSeqNew,
                      vIsRating,
                      vIsRouting,
                      vIsSailing);
   END REPLICATE_ROW;

   PROCEDURE DELETE_CHILD_ROW (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vLaneId        IN comb_lane.LANE_ID%TYPE,
      vRGDtlSeq      IN comb_lane_dtl.LANE_DTL_SEQ%TYPE)
   IS
      vRGDtlSeqNew   comb_lane_dtl.LANE_DTL_SEQ%TYPE;
   BEGIN
      DELETE FROM SURGE_CAPACITY
            WHERE lane_id = vLaneId AND rg_lane_dtl_seq = vRGDtlSeq;
   END DELETE_CHILD_ROW;

   PROCEDURE COPY_CHILD_ROW (
      vTCCompanyId     IN company.COMPANY_ID%TYPE,
      vLaneId          IN comb_lane.LANE_ID%TYPE,
      vOverlappedSeq   IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vRGDtlSeq        IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vRGDtlSeqNew     IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vIsRating        IN comb_lane_dtl.IS_RATING%TYPE,
      vIsRouting       IN comb_lane_dtl.IS_ROUTING%TYPE,
      vIsSailing       IN comb_lane_dtl.IS_SAILING%TYPE)
   IS
      vRGDtlSeqTemp     COMB_LANE_DTL.LANE_DTL_SEQ%TYPE;
      week_day_sql      VARCHAR2 (2000);
      week_day_cursor   ref_curtype;
	  vcnt                     NUMBER;
      vWeekId           SAILING_SCHDL_BY_WEEK.SAIL_SCHED_BY_WEEK_ID%TYPE;
	  v_lane_acces_id_seq      lane_accessorial.lane_accessorial_id%TYPE;
   BEGIN
      IF (vIsRating = 1)
      THEN
         INSERT INTO rating_lane_dtl_rate (TC_COMPANY_ID,
                                           LANE_ID,
                                           RATING_LANE_DTL_SEQ,
                                           RLD_RATE_SEQ,
                                           MINIMUM_SIZE,
                                           MAXIMUM_SIZE,
                                           SIZE_UOM_ID,
                                           RATE_CALC_METHOD,
                                           RATE_UOM,
                                           RATE,
                                           MINIMUM_RATE,
                                           CURRENCY_CODE,
                                           TARIFF_CODE_ID,
                                           MIN_DISTANCE,
                                           MAX_DISTANCE,
                                           DISTANCE_UOM,
                                           SUPPORTS_MSTL,
                                           HAS_BH,
                                           HAS_RT,
                                           MIN_COMMODITY,
                                           MAX_COMMODITY,
                                           COMMODITY_CODE_ID,
                                           EXCESS_WT_RATE)
            SELECT TC_COMPANY_ID,
                   LANE_ID,
                   vRGDtlSeqNew,
                   RLD_RATE_SEQ,
                   MINIMUM_SIZE,
                   MAXIMUM_SIZE,
                   SIZE_UOM_ID,
                   RATE_CALC_METHOD,
                   RATE_UOM,
                   RATE,
                   MINIMUM_RATE,
                   CURRENCY_CODE,
                   TARIFF_CODE_ID,
                   MIN_DISTANCE,
                   MAX_DISTANCE,
                   DISTANCE_UOM,
                   SUPPORTS_MSTL,
                   HAS_BH,
                   HAS_RT,
                   MIN_COMMODITY,
                   MAX_COMMODITY,
                   COMMODITY_CODE_ID,
                   EXCESS_WT_RATE
              FROM RATING_LANE_DTL_RATE
             WHERE lane_id = vLaneId AND rating_lane_dtl_seq = vRGDtlSeq;

	FOR l_rec
            IN (SELECT lane_accessorial_id,
                       tc_company_id,
                       lane_id,
                       accessorial_id,
                       rate,
                       minimum_rate,
                       currency_code,
                       is_auto_approve,
                       effective_dt,
                       expiration_dt,
                       payee_carrier_id,
                       minimum_size,
                       maximum_size,
                       size_uom_id,
                       zone_id,
                       is_shipment_accessorial,
                       incoterm_id,
                       Min_Rate_Type,
                       vRGDtlSeqNew,
                       Maximum_Rate,
                       PACKAGE_TYPE_ID,
                       CUSTOMER_RATE,
                       Customer_Min_Charge,
                       Calculated_Rate,
                       Amount,
                       BASE_AMOUNT,
                       Base_Charge,
                       RANGE_MIN_AMOUNT,
                       RANGE_MAX_AMOUNT,
                       Increment_Val,
                       Mot_Id,
                       Equipment_Id,
                       Service_Level_Id,
                       PROTECTION_LEVEL_ID,
                       Billing_Method,
                       Min_Longest_Dimension,
                       Max_Longest_Dimension,
                       Min_Second_Longest_Dimension,
                       Max_Second_Longest_Dimension,
                       Min_Third_Longest_Dimension,
                       Max_Third_Longest_Dimension,
                       Min_Length_Plus_Grith,
                       Max_Length_Plus_Grith,
                       Min_Weight,
                       Max_Weight,
                       Is_Zone_Stopoff
                  FROM lane_accessorial
                 WHERE
                       lane_id = vlaneid
                       AND rating_lane_dtl_seq = vRGDtlSeq)
         LOOP
            INSERT INTO LANE_ACCESSORIAL (TC_COMPANY_ID,
                                          LANE_ID,
                                          RATING_LANE_DTL_SEQ,
                                          ACCESSORIAL_ID,
                                          RATE,
                                          MINIMUM_RATE,
                                          CURRENCY_CODE,
                                          IS_AUTO_APPROVE,
                                          EFFECTIVE_DT,
                                          EXPIRATION_DT,
                                          LANE_ACCESSORIAL_ID,
                                          PAYEE_CARRIER_ID,
                                          MINIMUM_SIZE,
                                          MAXIMUM_SIZE,
                                          SIZE_UOM_ID,
                                          ZONE_ID,
                                          IS_SHIPMENT_ACCESSORIAL,
                                          INCOTERM_ID,
                                          Min_Rate_Type, -- MACR00159593 starts
                                          Maximum_Rate,
                                          PACKAGE_TYPE_ID,
                                          CUSTOMER_RATE,
                                          Customer_Min_Charge,
                                          Calculated_Rate,
                                          Amount,
                                          BASE_AMOUNT,
                                          Base_Charge,
                                          RANGE_MIN_AMOUNT,
                                          RANGE_MAX_AMOUNT,
                                          Increment_Val,
                                          Mot_Id,
                                          Equipment_Id,
                                          Service_Level_Id,
                                          PROTECTION_LEVEL_ID,
                                          Billing_Method,
                                          Min_Longest_Dimension,
                                          Max_Longest_Dimension,
                                          Min_Second_Longest_Dimension,
                                          Max_Second_Longest_Dimension,
                                          Min_Third_Longest_Dimension,
                                          Max_Third_Longest_Dimension,
                                          Min_Length_Plus_Grith,
                                          Max_Length_Plus_Grith,
                                          Min_Weight,
                                          Max_Weight,
                                          Is_Zone_Stopoff -- MACR00159593 ends
                                                         )
                 VALUES (l_rec.TC_COMPANY_ID,
                         l_rec.LANE_ID,
                         l_rec.vRGDtlSeqNew,
                         l_rec.ACCESSORIAL_ID,
                         l_rec.RATE,
                         l_rec.MINIMUM_RATE,
                         l_rec.CURRENCY_CODE,
                         l_rec.IS_AUTO_APPROVE,
                         l_rec.EFFECTIVE_DT,
                         l_rec.EXPIRATION_DT,
                         LANE_ACCESSORIAL_ID_SEQ.NEXTVAL,
                         l_rec.PAYEE_CARRIER_ID,
                         l_rec.MINIMUM_SIZE,
                         l_rec.MAXIMUM_SIZE,
                         l_rec.SIZE_UOM_ID,
                         l_rec.ZONE_ID,
                         l_rec.IS_SHIPMENT_ACCESSORIAL,
                         l_rec.INCOTERM_ID,
                         l_rec.Min_Rate_Type,
                         l_rec.Maximum_Rate,
                         l_rec.PACKAGE_TYPE_ID,
                         l_rec.CUSTOMER_RATE,
                         l_rec.Customer_Min_Charge,
                         l_rec.Calculated_Rate,
                         l_rec.Amount,
                         l_rec.BASE_AMOUNT,
                         l_rec.Base_Charge,
                         l_rec.RANGE_MIN_AMOUNT,
                         l_rec.RANGE_MAX_AMOUNT,
                         l_rec.Increment_Val,
                         l_rec.Mot_Id,
                         l_rec.Equipment_Id,
                         l_rec.Service_Level_Id,
                         l_rec.PROTECTION_LEVEL_ID,
                         l_rec.Billing_Method,
                         l_rec.Min_Longest_Dimension,
                         l_rec.Max_Longest_Dimension,
                         l_rec.Min_Second_Longest_Dimension,
                         l_rec.Max_Second_Longest_Dimension,
                         l_rec.Min_Third_Longest_Dimension,
                         l_rec.Max_Third_Longest_Dimension,
                         l_rec.Min_Length_Plus_Grith,
                         l_rec.Max_Length_Plus_Grith,
                         l_rec.Min_Weight,
                         l_rec.Max_Weight,
                         l_rec.Is_Zone_Stopoff);


            SELECT MAX (lane_accessorial_id)
              INTO v_lane_acces_id_seq
              FROM lane_accessorial
             WHERE
                   lane_id = vlaneid
                   AND rating_lane_dtl_seq = vRGDtlSeqNew;


		 INSERT INTO lane_acc_feasible_incoterm (LANE_ACC_ID, INCOTERM_ID)
					SELECT v_lane_acces_id_seq, INCOTERM_ID
							FROM lane_acc_feasible_incoterm
							WHERE LANE_ACC_ID = l_rec.lane_accessorial_id;

            SELECT COUNT (*)
              INTO vcnt
              FROM rating_event
             WHERE     rating_lane_id = vlaneid
                   AND rating_lane_dtl_id = vRGDtlSeq
                   AND is_pending_auth = 1
                   AND rld_rate_id IS NULL
                   AND lane_accessorial_id = l_rec.lane_accessorial_id;

            INSERT INTO rating_event (rating_event_id,
                                      rating_lane_id,
                                      rating_lane_dtl_id,
                                      rld_rate_id,
                                      tc_company_id,
                                      change_id,
                                      is_pending_auth,
                                      field_name,
                                      old_value,
                                      new_value,
                                      comments,
                                      last_updated_src_type,
                                      last_updated_src,
                                      last_updated_dttm,
                                      auth_src_type,
                                      auth_src,
                                      auth_dttm,
                                      lane_accessorial_id,
                                      reason_id,
                                      SPLIT)
               SELECT seq_rating_event_id.NEXTVAL,
                      rating_lane_id,
                      vRGDtlSeqNew,
                      rld_rate_id,
                      tc_company_id,
                      change_id,
                      is_pending_auth,
                      field_name,
                      old_value,
                      new_value,
                      comments,
                      last_updated_src_type,
                      last_updated_src,
                      last_updated_dttm,
                      auth_src_type,
                      auth_src,
                      auth_dttm,
                      v_lane_acces_id_seq,
                      reason_id,
                      SPLIT
                 FROM rating_event
                WHERE     rating_lane_id = vlaneid
                      AND rating_lane_dtl_id = vRGDtlSeq
                      AND is_pending_auth = 1
                      AND rld_rate_id IS NULL
                      AND lane_accessorial_id = l_rec.lane_accessorial_id;
         END LOOP;

      END IF;

      IF (vIsRouting = 1)
      THEN
         IF (vOverlappedSeq = -1)
         THEN
            vRGDtlSeqTemp := vRGDtlSeq;
         ELSE
            vRGDtlSeqTemp := vOverlappedSeq;
         END IF;

         INSERT INTO SURGE_CAPACITY (SURGE_CAPACITY_ID,
                                     TC_COMPANY_ID,
                                     LANE_ID,
                                     RG_LANE_DTL_SEQ,
                                     EFFECTIVE_DT,
                                     EXPIRATION_DT,
                                     YEARLY_SURGE_CAPACITY,
                                     MONTHLY_SURGE_CAPACITY,
                                     WEEKLY_SURGE_CAPACITY,
                                     DAILY_SURGE_CAPACITY,
                                     SURGE_CAPACITY_SUN,
                                     SURGE_CAPACITY_MON,
                                     SURGE_CAPACITY_TUE,
                                     SURGE_CAPACITY_WED,
                                     SURGE_CAPACITY_THU,
                                     SURGE_CAPACITY_FRI,
                                     SURGE_CAPACITY_SAT)
            SELECT SEQ_SURGE_CAPACITY_ID.NEXTVAL,
                   TC_COMPANY_ID,
                   LANE_ID,
                   vRGDtlSeqNew,
                   EFFECTIVE_DT,
                   EXPIRATION_DT,
                   YEARLY_SURGE_CAPACITY,
                   MONTHLY_SURGE_CAPACITY,
                   WEEKLY_SURGE_CAPACITY,
                   DAILY_SURGE_CAPACITY,
                   SURGE_CAPACITY_SUN,
                   SURGE_CAPACITY_MON,
                   SURGE_CAPACITY_TUE,
                   SURGE_CAPACITY_WED,
                   SURGE_CAPACITY_THU,
                   SURGE_CAPACITY_FRI,
                   SURGE_CAPACITY_SAT
              FROM SURGE_CAPACITY
             WHERE LANE_ID = vLaneId AND RG_LANE_DTL_SEQ = vRGDtlSeqTemp;
      END IF;

      IF (vIsSailing = 1)
      THEN
         INSERT INTO SAILING_SCHDL_BY_DATE (TC_COMPANY_ID,
                                            LANE_ID,
                                            LANE_DTL_SEQ,
                                            SAIL_SCHED_BY_DATE_ID,
                                            DEPARTURE_DTTM,
                                            ARRIVAL_DTTM,
                                            CUTOFF_DTTM,
                                            PICKUP_DTTM,
                                            TRANSIT_TIME_VALUE,
                                            TRANSIT_TIME_STANDARD_UOM,
                                            RESOURCE_REF_EXTERNAL,
                                            RESOURCE_NAME_EXTERNAL,
                                            COMMENTS,
                                            DESCRIPTION)
            SELECT TC_COMPANY_ID,
                   LANE_ID,
                   vRGDtlSeqNew,
                   SEQ_SAIL_SCHED_BY_DATE_ID.NEXTVAL,
                   DEPARTURE_DTTM,
                   ARRIVAL_DTTM,
                   CUTOFF_DTTM,
                   PICKUP_DTTM,
                   TRANSIT_TIME_VALUE,
                   TRANSIT_TIME_STANDARD_UOM,
                   RESOURCE_REF_EXTERNAL,
                   RESOURCE_NAME_EXTERNAL,
                   COMMENTS,
                   DESCRIPTION
              FROM SAILING_SCHDL_BY_DATE
             WHERE LANE_ID = vLaneId AND LANE_DTL_SEQ = vRGDtlSeq;

         week_day_sql :=
               ' SELECT SAIL_SCHED_BY_WEEK_ID '
            || ' FROM SAILING_SCHDL_BY_WEEK '
            || ' WHERE lane_id = :vLaneId'
            || ' AND lane_dtl_seq = :vRGDtlSeq';

         OPEN week_day_cursor FOR week_day_sql USING vLaneId, vRGDtlSeq;

         LOOP
            FETCH week_day_cursor INTO vWeekId;

            EXIT WHEN week_day_cursor%NOTFOUND;

            INSERT INTO SAILING_SCHDL_BY_WEEK (TC_COMPANY_ID,
                                               LANE_ID,
                                               LANE_DTL_SEQ,
                                               SAIL_SCHED_BY_WEEK_ID,
                                               EFFECTIVE_DT,
                                               EXPIRATION_DT,
                                               RESOURCE_REF_EXTERNAL,
                                               RESOURCE_NAME_EXTERNAL,
                                               COMMENTS,
                                               DESCRIPTION)
               SELECT TC_COMPANY_ID,
                      LANE_ID,
                      vRGDtlSeqNew,
                      SEQ_SAIL_SCHED_BY_WEEK_ID.NEXTVAL,
                      EFFECTIVE_DT,
                      EXPIRATION_DT,
                      RESOURCE_REF_EXTERNAL,
                      RESOURCE_NAME_EXTERNAL,
                      COMMENTS,
                      DESCRIPTION
                 FROM SAILING_SCHDL_BY_WEEK
                WHERE     LANE_ID = vLaneId
                      AND LANE_DTL_SEQ = vRGDtlSeq
                      AND SAIL_SCHED_BY_WEEK_ID = vWeekId;

            INSERT INTO SAILING_SCHDL_BY_WEEK_DAY (TC_COMPANY_ID,
                                                   LANE_ID,
                                                   LANE_DTL_SEQ,
                                                   SAIL_SCHED_BY_WEEK_ID,
                                                   SAIL_SCHED_BY_WEEK_DAY_ID,
                                                   DEPARTURE_DAYOFWEEK,
                                                   DEPARTURE_TIME,
                                                   ARRIVAL_DAYOFWEEK,
                                                   ARRIVAL_TIME,
                                                   CUTOFF_DAYOFWEEK,
                                                   CUTOFF_TIME,
                                                   PICKUP_DAYOFWEEK,
                                                   PICKUP_TIME,
                                                   TRANSIT_TIME_VALUE,
                                                   TRANSIT_TIME_STANDARD_UOM,
                                                   TRANSIT_TIME_WEEK)
               SELECT TC_COMPANY_ID,
                      LANE_ID,
                      vRGDtlSeqNew,
                      SEQ_SAIL_SCHED_BY_WEEK_ID.CURRVAL,
                      SEQ_SAIL_SCHED_BY_WEEK_DAY_ID.NEXTVAL,
                      DEPARTURE_DAYOFWEEK,
                      DEPARTURE_TIME,
                      ARRIVAL_DAYOFWEEK,
                      ARRIVAL_TIME,
                      CUTOFF_DAYOFWEEK,
                      CUTOFF_TIME,
                      PICKUP_DAYOFWEEK,
                      PICKUP_TIME,
                      TRANSIT_TIME_VALUE,
                      TRANSIT_TIME_STANDARD_UOM,
                      TRANSIT_TIME_WEEK
                 FROM SAILING_SCHDL_BY_WEEK_DAY
                WHERE     LANE_ID = vLaneId
                      AND LANE_DTL_SEQ = vRGDtlSeq
                      AND SAIL_SCHED_BY_WEEK_ID = vWeekId;
         END LOOP;

         CLOSE week_day_cursor;
      END IF;
   END COPY_CHILD_ROW;

   PROCEDURE REPLICATE_LANE_DETAIL (
	  laneDtlId             OUT NUMBER,
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vLaneId        IN comb_lane.LANE_ID%TYPE,
      vRGDtlSeq      IN comb_lane_dtl.LANE_DTL_SEQ%TYPE)
   IS
      vRGDtlSeqNew   comb_lane_dtl.LANE_DTL_SEQ%TYPE;
   BEGIN
      SELECT MAX (LANE_DTL_SEQ) + 1
        INTO vRGDtlSeqNew
        FROM COMB_LANE_DTL
       WHERE LANE_ID = vLaneID;
	  laneDtlId:= vRGDtlSeqNew;
      INSERT INTO COMB_LANE_DTL (TC_COMPANY_ID,
                                 LANE_ID,
                                 LANE_DTL_SEQ,
                                 LANE_DTL_STATUS,
                                 IS_RATING,
                                 IS_ROUTING,
                                 IS_BUDGETED,
                                 LAST_UPDATED_SOURCE_TYPE,
                                 LAST_UPDATED_SOURCE,
                                 LAST_UPDATED_DTTM,
                                 CARRIER_ID,
                                 MOT_ID,
                                 EQUIPMENT_ID,
                                 SERVICE_LEVEL_ID,
                                 SCNDR_CARRIER_ID,
                                 PROTECTION_LEVEL_ID,
								 VOYAGE,
                                 EFFECTIVE_DT,
                                 EXPIRATION_DT,
                                 FIXED_TRANSIT_STANDARD_UOM,
                                 FIXED_TRANSIT_VALUE,
                                 IS_SAILING,
                                 SAILING_FREQUENCY_TYPE,
                                 SAILING_SCHEDULE_NAME,
                                 MASTER_LANE_ID,
                                 O_SHIP_VIA,
                                 D_SHIP_VIA,
                                 CONTRACT_NUMBER,
                                 CUSTOM_TEXT1,
                                 CUSTOM_TEXT2,
                                 CUSTOM_TEXT3,
                                 CUSTOM_TEXT4,
                                 CUSTOM_TEXT5,
                                 TT_TOLERANCE_FACTOR)
         SELECT TC_COMPANY_ID,
                LANE_ID,
                vRGDtlSeqNew,
                0,
                IS_RATING,
                0,
                IS_BUDGETED,
                3,
                'OMS',
                GETDATE (),
                CARRIER_ID,
                MOT_ID,
                EQUIPMENT_ID,
                SERVICE_LEVEL_ID,
                SCNDR_CARRIER_ID,
                PROTECTION_LEVEL_ID,
				VOYAGE,
                EFFECTIVE_DT,
                EXPIRATION_DT,
                FIXED_TRANSIT_STANDARD_UOM,
                FIXED_TRANSIT_VALUE,
                IS_SAILING,
                SAILING_FREQUENCY_TYPE,
                SAILING_SCHEDULE_NAME,
                MASTER_LANE_ID,
                O_SHIP_VIA,
                D_SHIP_VIA,
                CONTRACT_NUMBER,
                CUSTOM_TEXT1,
                CUSTOM_TEXT2,
                CUSTOM_TEXT3,
                CUSTOM_TEXT4,
                CUSTOM_TEXT5,
                TT_TOLERANCE_FACTOR
           FROM COMB_LANE_DTL
          WHERE LANE_ID = vLaneId AND LANE_DTL_SEQ = vRGDtlSeq;

      COPY_CHILD_ROW (vTCCompanyId,
                      vLaneId,
                      -1,
                      vRGDtlSeq,
                      vRGDtlSeqNew,
                      1,
                      0,
                      1);
   END REPLICATE_LANE_DETAIL;

   PROCEDURE INSERT_RG_LANE_ERROR (
      vTCCompanyId         IN company.COMPANY_ID%TYPE,
      vLaneId              IN rg_lane.LANE_ID%TYPE,
      vErrorMsgDesc        IN rg_lane_errors.ERROR_MSG_DESC%TYPE,
      p_error_msg_id       IN RG_LANE_ERRORS.ERROR_MSG_ID%TYPE DEFAULT NULL,
      p_error_param_list   IN RG_LANE_ERRORS.ERROR_PARAM_LIST%TYPE DEFAULT NULL)
   AS
      vBatchId   rg_lane_errors.BATCH_ID%TYPE;
   BEGIN
      SELECT BATCH_ID
        INTO vBatchId
        FROM IMPORT_RG_LANE
       WHERE LANE_ID = vLaneId AND TC_COMPANY_ID = vTCCompanyId;

      INSERT INTO RG_LANE_ERRORS (RG_LANE_ERROR,
                                  TC_COMPANY_ID,
                                  LANE_ID,
                                  ERROR_MSG_DESC,
                                  BATCH_ID,
                                  ERROR_MSG_ID,
                                  ERROR_PARAM_LIST)
           VALUES (SEQ_RG_LANE_ERROR.NEXTVAL,
                   vTCCompanyId,
                   vLaneId,
                   vErrorMsgDesc,
                   vBatchId,
                   p_error_msg_id,
                   p_error_param_list);

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE ('Procedure INSERT_RG_LANE_ERROR: ' || SQLERRM);
         RAISE;
   END INSERT_RG_LANE_ERROR;

   PROCEDURE UPDATE_RG_LANE_ERROR (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vBatchId       IN rg_lane_errors.BATCH_ID%TYPE)
   AS
      vLaneId   comb_lane.LANE_ID%TYPE;
   BEGIN
      SELECT LANE_ID
        INTO vLaneId
        FROM IMPORT_RG_LANE
       WHERE BATCH_ID = vBatchId AND TC_COMPANY_ID = vTCCompanyId;

      UPDATE RG_LANE_ERRORS
         SET LANE_ID = vLaneId
       WHERE     BATCH_ID = vBatchId
             AND TC_COMPANY_ID = vTCCompanyId
             AND LANE_ID = 0;

      COMMIT;
   END UPDATE_RG_LANE_ERROR;

   FUNCTION INSERT_RG_LANE (
      vTCCompanyId            IN company.COMPANY_ID%TYPE,
      vImportLaneId           IN import_rg_lane.LANE_ID%TYPE,
      /* vBusinessUnit    IN business_unit.BUSINESS_UNIT%TYPE,*/
      vLaneHierarchy          IN rg_lane.LANE_HIERARCHY%TYPE,
      vOLocType               IN rg_lane.O_LOC_TYPE%TYPE,
      vOFacilityId            IN facility.FACILITY_ID%TYPE,
      vOFacilityAlias         IN facility_alias.FACILITY_ALIAS_ID%TYPE,
      vOFacilityBU            IN import_rg_lane.O_FACILITY_BU%TYPE,
      vOCity                  IN rg_lane.O_CITY%TYPE,
      vOStateProv             IN state_prov.STATE_PROV%TYPE,
      vOCounty                IN rg_lane.O_COUNTY%TYPE,
      vOPostal                IN postal_code.POSTAL_CODE%TYPE,
      vOCountry               IN country.COUNTRY_CODE%TYPE,
      vOZone                  IN zone.ZONE_ID%TYPE,
      vDLocType               IN rg_lane.D_LOC_TYPE%TYPE,
      vDFacilityId            IN facility.FACILITY_ID%TYPE,
      vDFacilityAlias         IN facility_alias.FACILITY_ALIAS_ID%TYPE,
      vDFacilityBU            IN import_rg_lane.D_FACILITY_BU%TYPE,
      vDCity                  IN rg_lane.D_CITY%TYPE,
      vDStateProv             IN state_prov.STATE_PROV%TYPE,
      vDCounty                IN rg_lane.D_COUNTY%TYPE,
      vDPostal                IN postal_code.POSTAL_CODE%TYPE,
      vDCountry               IN country.COUNTRY_CODE%TYPE,
      vDZone                  IN zone.ZONE_ID%TYPE,
      vRGQualifier            IN rg_lane.RG_QUALIFIER%TYPE,
      vFrequency              IN rg_lane.FREQUENCY%TYPE,
      vCustomerId                rg_lane.CUSTOMER_ID%TYPE,
      vBillingMethod             rg_lane.BILLING_METHOD%TYPE,
      vIncotermId                rg_lane.INCOTERM_ID%TYPE,
      vRouteTo                   rg_lane.ROUTE_TO%TYPE,
      vRouteType1                rg_lane.ROUTE_TYPE_1%TYPE,
      vRouteType2                rg_lane.ROUTE_TYPE_2%TYPE,
      vLaneName                  rg_lane.LANE_NAME%TYPE,
      vNoRating                  rg_lane.NO_RATING%TYPE,
      vSelectFastestCarrier      rg_lane.USE_FASTEST%TYPE,
      vUsePreferenceBonus        rg_lane.USE_PREFERENCE_BONUS%TYPE,
	  vUseEpi				  IN rg_lane.USE_EPI%TYPE,
	  vEpiServiceGroup		  IN rg_lane.EPI_SERVICE_GROUP%TYPE,
      vRgLaneId               IN rg_lane.lane_id%TYPE,       /* SL 05/21/02 */
      vBatchId                IN import_rg_lane.BATCH_ID%TYPE /* RC 06/05/2003 */
                                                             )
      RETURN NUMBER
   IS
      vRealLaneId              comb_lane.lane_id%TYPE;
      vRealLaneFrequency       comb_lane.FREQUENCY%TYPE;
      vOFacilityBUID           company.COMPANY_ID%TYPE;
      vDFacilityBUID           company.COMPANY_ID%TYPE;
      vRootCompanyId           company.COMPANY_ID%TYPE;
      l_real_lane_is_routing   COMB_LANE.IS_ROUTING%TYPE;

      vWhereClause             VARCHAR2 (2000);
      vsqlStatement            VARCHAR2 (2000);
      vCount                   NUMBER;
	  vTempDuplicateLaneCount NUMBER;
   VERRORMSGDESC1 VARCHAR2(1000);
   VERRORID NUMBER;
   VERRORLIST varchar2(255);
   vPassFail                   NUMBER;
   BEGIN
      SELECT COUNT (ROOTCOMPANYID)
        INTO vCount
        FROM IMPORT_RG_LANE
       WHERE LANE_ID = vImportLaneId;

      IF (vCount > 0)
      THEN
         SELECT ROOTCOMPANYID
           INTO vRootCompanyId
           FROM IMPORT_RG_LANE
          WHERE LANE_ID = vImportLaneId;
      END IF;

      IF (vOFacilityBU IS NULL)
      THEN
         vOFacilityBUID := vRootCompanyId;
      ELSE
         vOFacilityBUID :=
            GET_BUID_FROM_BUSINESS_UNIT (vRootCompanyId, vOFacilityBU);
      END IF;

      IF (vDFacilityBU IS NULL)
      THEN
         vDFacilityBUID := vRootCompanyId;
      ELSE
         vDFacilityBUID :=
            GET_BUID_FROM_BUSINESS_UNIT (vRootCompanyId, vDFacilityBU);
      END IF;

      vsqlStatement := '/* INSERT_RG_LANE - SEARCH FOR EXISTING RG LANE */ ';
      vsqlStatement :=
         vsqlStatement || 'SELECT lane_id, frequency, is_routing FROM ( ';
      vsqlStatement :=
         vsqlStatement
         || 'SELECT lane_id, frequency, is_routing FROM COMB_LANE';
      vWhereClause := ' WHERE tc_company_id = ' || vTCCompanyId;
      vWhereClause := vWhereClause || ' and lane_status = 0 '; /* SL 05/21/02 Lane Status is Valid */
      /* vWhereClause := vWhereClause || ' and business_unit ';
       IF(vBusinessUnit IS NULL) THEN
        vWhereClause := vWhereClause || 'is null';
          ELSE
        vWhereClause := vWhereClause || '= ''' || vBusinessUnit || '''';
       END IF; */
      vWhereClause := vWhereClause || ' and o_facility_id ';

      IF (vOFacilityId IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause :=
            vWhereClause
            || '= ( select facility_id from facility_alias where tc_company_id = '
            || vOFacilityBUID
            || ' ';
         vWhereClause :=
               vWhereClause
            || 'and facility_alias_id = '''
            || vOFacilityAlias
            || ''' )';
      END IF;

      vWhereClause := vWhereClause || ' and o_city ';

      IF (vOCity IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause :=
            vWhereClause || '= ''' || REPLACE (vOCity, '''', '''''') || '''';
      END IF;

      vWhereClause := vWhereClause || ' and o_state_prov ';

      IF (vOStateProv IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vOStateProv || '''';
      END IF;

      vWhereClause := vWhereClause || ' and o_county ';

      IF (vOCounty IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vOCounty || '''';
      END IF;

      vWhereClause := vWhereClause || ' and o_postal_code ';

      IF (vOPostal IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vOPostal || '''';
      END IF;

      vWhereClause := vWhereClause || ' and o_country_code ';

      IF (vOCountry IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vOCountry || '''';
      END IF;

      vWhereClause := vWhereClause || ' and o_zone_id ';

      IF (vOZone IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ' || vOZone;
      END IF;

      vWhereClause := vWhereClause || ' and d_facility_id ';

      IF (vDFacilityId IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause :=
            vWhereClause
            || '= ( select facility_id from facility_alias where tc_company_id = '
            || vDFacilityBUID
            || ' ';
         vWhereClause :=
               vWhereClause
            || 'and facility_alias_id = '''
            || vDFacilityAlias
            || ''' )';
      END IF;

      vWhereClause := vWhereClause || ' and d_city ';

      IF (vDCity IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause :=
            vWhereClause || '= ''' || REPLACE (vDCity, '''', '''''') || '''';
      END IF;

      vWhereClause := vWhereClause || ' and d_state_prov ';

      IF (vDStateProv IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vDStateProv || '''';
      END IF;

      vWhereClause := vWhereClause || ' and d_county ';

      IF (vDCounty IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vDCounty || '''';
      END IF;

      vWhereClause := vWhereClause || ' and d_postal_code ';

      IF (vDPostal IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vDPostal || '''';
      END IF;

      vWhereClause := vWhereClause || ' and d_country_code ';

      IF (vDCountry IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vDCountry || '''';
      END IF;

      vWhereClause := vWhereClause || ' and d_zone_id ';

      IF (vDZone IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ' || vDZone;
      END IF;

      /**** Removed by C.R. TT 17331 ****

      vWhereClause := vWhereClause || ' and frequency ';
      IF( vFrequency IS NULL ) THEN
        vWhereClause := vWhereClause || 'is null';
      ELSE
        vWhereClause := vWhereClause || '= ''' || vFrequency || '''';
      END IF;
      /**** END: Removed by C.R. TT 17331 ****/

      vWhereClause := vWhereClause || ' and rg_qualifier ';

      IF (vRGQualifier IS NULL OR vRGQualifier = '')
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vRGQualifier || '''';
      END IF;

      vWhereClause := vWhereClause || ' and CUSTOMER_ID ';

      IF (vCustomerId IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vCustomerId || '''';
      END IF;

      vWhereClause := vWhereClause || ' and BILLING_METHOD ';

      IF (vBillingMethod IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vBillingMethod || '''';
      END IF;

      vWhereClause := vWhereClause || ' and INCOTERM_ID ';

      IF (vIncotermId IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vIncotermId || '''';
      END IF;

      vWhereClause := vWhereClause || ' and ROUTE_TO ';

      IF (vRouteTo IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vRouteTo || '''';
      END IF;

      vWhereClause := vWhereClause || ' and ROUTE_TYPE_1 ';

      IF (vRouteType1 IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vRouteType1 || '''';
      END IF;

      vWhereClause := vWhereClause || ' and ROUTE_TYPE_2 ';

      IF (vRouteType2 IS NULL)
      THEN
         vWhereClause := vWhereClause || 'is null';
      ELSE
         vWhereClause := vWhereClause || '= ''' || vRouteType2 || '''';
      END IF;

      vWhereClause := vWhereClause || ' and lane_status = 0 ';

      vsqlStatement := vsqlStatement || vWhereClause;
      vsqlStatement :=
         vsqlStatement
         || 'ORDER BY IS_ROUTING desc, is_rating desc, is_sailing desc ) ';
      vsqlStatement := vsqlStatement || 'where rownum = 1 ';

      COMMIT;

      /*
         DBMS_OUTPUT.PUT_LINE(substr(vsqlStatement, 1,    200));
         DBMS_OUTPUT.PUT_LINE(substr(vsqlStatement, 201,  400));
         DBMS_OUTPUT.PUT_LINE(substr(vsqlStatement, 401,  600));
         DBMS_OUTPUT.PUT_LINE(substr(vsqlStatement, 601,  800));
         DBMS_OUTPUT.PUT_LINE(substr(vsqlStatement, 801,  1000));
         DBMS_OUTPUT.PUT_LINE(substr(vsqlStatement, 1001, 1200));
         DBMS_OUTPUT.PUT_LINE(substr(vsqlStatement, 1201, 1400));
         DBMS_OUTPUT.PUT_LINE(substr(vsqlStatement, 1401, 1600));
         DBMS_OUTPUT.PUT_LINE(substr(vsqlStatement, 1601, 1800));
         DBMS_OUTPUT.PUT_LINE(substr(vsqlStatement, 1801, 2000));
      */

      /*
      SP_TEMP_MB_DEBUG_PLSQL
      ( 'INSERT_RG_LANE vsqlStatement',
       vsqlStatement,
       NULL
      )
      ;
      */

      -- insert into debug_log values (vsqlStatement);
      EXECUTE IMMEDIATE vsqlStatement
         INTO vRealLaneId, vRealLaneFrequency, l_real_lane_is_routing;

		 IF(vLaneName IS NOT NULL) THEN
            IF (vRealLaneId IS NOT NULL) THEN
				SELECT COUNT(LANE_ID) INTO vTempDuplicateLaneCount FROM RG_LANE
					WHERE LANE_NAME = vLaneName
					AND TC_COMPANY_ID = vTCCompanyId
					AND LANE_ID != vRealLaneId;
			ELSE
				SELECT COUNT(LANE_ID) INTO vTempDuplicateLaneCount FROM RG_LANE
					WHERE LANE_NAME = vLaneName
					AND TC_COMPANY_ID = vTCCompanyId;
			END IF;
            IF(vTempDuplicateLaneCount > 0) THEN
                VERRORMSGDESC1 := 'One or more lanes already exist for lane name ' || vLaneName;
                VERRORID := 1720030;
                VERRORLIST := NULL;
                INSERT_RG_LANE_ERROR(vTCCompanyId,vImportLaneId,VERRORMSGDESC1,VERRORID,VERRORLIST);
				RETURN -999;
            END IF;
		END IF;
		
		vPassFail := 0;
		IF (vRealLaneId IS NOT NULL)
		THEN
			vPassFail := VALIDATE_USE_EPI(vTCCompanyId, vRealLaneId, vImportLaneId, vUseEpi, vEpiServiceGroup);
			IF (vPassFail = 1)
			THEN 
				RETURN -999;
			END IF;
		END IF;

      IF (l_real_lane_is_routing = 1)
      THEN
         --CR 29508
         /* Commented the call to method VALIDATE_MULTIPLE_REP_TP_FLAGS and made a call to this method from validate_lane_dtl */

         -- TT42403 change (code added)
         FOR CUR_IMP_RG_LANE_DTL
            IN (SELECT CARRIER_CODE,
                       MOT,
                       SERVICE_LEVEL,
                       EQUIPMENT_CODE,
                       PROTECTION_LEVEL,
                       RG_LANE_DTL_SEQ,
                       EFFECTIVE_DT,
                       EXPIRATION_DT,                            --SL 05/27/02
                       REP_TP_FLAG,                             -- SL 07/11/02
                       SCNDR_CARRIER_CODE,
                       CARRIER_ID
                  FROM IMPORT_RG_LANE_DTL
                 WHERE     TC_COMPANY_ID = vTCCompanyId
                       AND LANE_ID = vImportLaneId
                       AND LANE_DTL_STATUS IN (1, 3))
         LOOP
            IF (VALIDATE_MULTIPLE_REP_TP_FLAGS (
                   vTCCompanyId,
                   vRealLaneId,          --this is the real one - for tt#42403
                   CUR_IMP_RG_LANE_DTL.CARRIER_ID,
                   CUR_IMP_RG_LANE_DTL.REP_TP_FLAG,
                   CUR_IMP_RG_LANE_DTL.MOT,
                   CUR_IMP_RG_LANE_DTL.SCNDR_CARRIER_CODE,
                   CUR_IMP_RG_LANE_DTL.EFFECTIVE_DT,
                   CUR_IMP_RG_LANE_DTL.EXPIRATION_DT) = 1)
            THEN
               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vImportLaneId,
                  CUR_IMP_RG_LANE_DTL.RG_LANE_DTL_SEQ,
                  'Representative carrier already exists for this lane for date range, rep_tp_flag for carrier '
                  || CUR_IMP_RG_LANE_DTL.CARRIER_CODE
                  || ' - hence is invalid',
                  4720033,
                  CUR_IMP_RG_LANE_DTL.CARRIER_CODE);

               UPDATE IMPORT_RG_LANE_DTL
                  SET LANE_DTL_STATUS = 4
                WHERE TC_COMPANY_ID = vTCCompanyId
                      AND LANE_ID = vImportLaneId
                      AND RG_LANE_DTL_SEQ =
                             CUR_IMP_RG_LANE_DTL.RG_LANE_DTL_SEQ;
            END IF;
         END LOOP;

         -- End of TT42403 change

         IF (vRealLaneFrequency <> vFrequency)
         THEN
            UPDATE COMB_LANE
               SET LANE_NAME = vLaneName,
                   last_updated_dttm = SYSDATE,
                   last_updated_source_type = 3,
                   last_updated_source = 'OMS',
                   frequency = vFrequency,
				   use_epi = vUseEpi,
				   epi_service_group = vEpiServiceGroup 
             WHERE tc_company_id = vTCCompanyId AND lane_id = vRealLaneId;

            RETURN vRealLaneId;
         ELSE
            UPDATE COMB_LANE
               SET LANE_NAME = vLaneName,
				   use_epi = vUseEpi,
				   epi_service_group = vEpiServiceGroup, 
                   last_updated_dttm = SYSDATE,
                   last_updated_source_type = 3,
                   last_updated_source = 'OMS'
             WHERE tc_company_id = vTCCompanyId AND lane_id = vRealLaneId;

            RETURN vRealLaneId;
         END IF;
      ELSE
         UPDATE COMB_LANE
            SET LANE_NAME = vLaneName, IS_ROUTING = 1, frequency = vFrequency,
			use_epi = vUseEpi, epi_service_group = vEpiServiceGroup 
          WHERE TC_COMPANY_ID = vTCCompanyId AND LANE_ID = vRealLaneId;

         RETURN vRealLaneId;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN

	  SELECT COUNT(LANE_ID) INTO vTempDuplicateLaneCount FROM RG_LANE
					WHERE LANE_NAME = vLaneName
					AND TC_COMPANY_ID = vTCCompanyId;

		IF(vTempDuplicateLaneCount > 0) THEN
			VERRORMSGDESC1 := 'One or more lanes already exist for lane name ' || vLaneName;
			VERRORID := 1720030;
			VERRORLIST := NULL;
			INSERT_RG_LANE_ERROR(vTCCompanyId,vImportLaneId,VERRORMSGDESC1,VERRORID,VERRORLIST);
			RETURN -999;
		END IF;

         DBMS_OUTPUT.PUT_LINE ('INSERTING NEW RECORD INTO RG_LANE TABLE');

         DBMS_OUTPUT.PUT_LINE (
               'RG LANE: - COMPANY_ID: '
            || TO_CHAR (vTCCompanyId)
            || ' Lane Id: '
            || TO_CHAR (vRgLaneId));

         /*  SL 05/21/02  Removed Select Max value    */

         INSERT INTO COMB_LANE (TC_COMPANY_ID,
                                LANE_ID,
                                IS_ROUTING,
                                LANE_HIERARCHY,
                                LANE_STATUS,
                                O_LOC_TYPE,
                                O_FACILITY_ID,
                                O_FACILITY_ALIAS_ID,
                                O_CITY,
                                O_STATE_PROV,
                                O_COUNTY,
                                O_POSTAL_CODE,
                                O_COUNTRY_CODE,
                                O_ZONE_ID,
                                D_LOC_TYPE,
                                D_FACILITY_ID,
                                D_FACILITY_ALIAS_ID,
                                D_CITY,
                                D_STATE_PROV,
                                D_COUNTY,
                                D_POSTAL_CODE,
                                D_COUNTRY_CODE,
                                D_ZONE_ID,
                                RG_QUALIFIER,
                                FREQUENCY,
                                CUSTOMER_ID,
                                BILLING_METHOD,
                                INCOTERM_ID,
                                ROUTE_TO,
                                ROUTE_TYPE_1,
                                ROUTE_TYPE_2,
                                LANE_NAME,
                                NO_RATING,
                                USE_FASTEST,
                                USE_PREFERENCE_BONUS,
								USE_EPI,
								EPI_SERVICE_GROUP,
                                CREATED_SOURCE_TYPE,
                                CREATED_SOURCE,
                                CREATED_DTTM,
                                LAST_UPDATED_SOURCE_TYPE,
                                LAST_UPDATED_SOURCE,
                                LAST_UPDATED_DTTM)
              VALUES (vTCCompanyId,
                      vBatchId,
                      1,
                      vLaneHierarchy,
                      0,
                      vOLocType,
                      vOFacilityId,
                      vOFacilityAlias,
                      vOCity,
                      vOStateProv,
                      vOCounty,
                      vOPostal,
                      vOCountry,
                      vOZone,
                      vDLocType,
                      vDFacilityId,
                      vDFacilityAlias,
                      vDCity,
                      vDStateProv,
                      vDCounty,
                      vDPostal,
                      vDCountry,
                      vDZone,
                      vRGQualifier,
                      vFrequency,
                      vCustomerId,
                      vBillingMethod,
                      vIncotermId,
                      vRouteTo,
                      vRouteType1,
                      vRouteType2,
                      vLaneName,
                      vNoRating,
                      vSelectFastestCarrier,
                      vUsePreferenceBonus,
					  vUseEpi,
					  vEpiServiceGroup,
                      3,
                      'OMS',
                      SYSDATE,
                      3,
                      'OMS',
                      SYSDATE);

         COMMIT;

         RETURN vBatchId;
      WHEN OTHERS
      THEN
         RAISE;
   END INSERT_RG_LANE;

   FUNCTION INSERT_RG_LANE_DTL (
      vTCCompanyId            IN company.COMPANY_ID%TYPE,
      vRgLaneId               IN rg_lane.LANE_ID%TYPE,
      vCarrierCode            IN rg_lane_dtl.CARRIER_ID%TYPE,
      vMot                    IN rg_lane_dtl.MOT_ID%TYPE,
      vEquipment              IN rg_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel           IN rg_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code    IN RG_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel        IN rg_lane_dtl.PROTECTION_LEVEL_ID%TYPE,
      vTier                   IN rg_lane_dtl.TIER_ID%TYPE,
      vRank                   IN rg_lane_dtl.RANK%TYPE,
      vRepTPFlag              IN rg_lane_dtl.REP_TP_FLAG%TYPE,
      vYearlyCapacity         IN rg_lane_dtl.YEARLY_CAPACITY%TYPE,
      vMonthlyCapacity        IN rg_lane_dtl.MONTHLY_CAPACITY%TYPE,
      vWeeklyCapacity         IN rg_lane_dtl.WEEKLY_CAPACITY%TYPE,
      vDailyCapacity          IN rg_lane_dtl.DAILY_CAPACITY%TYPE,
      vCapacitySun            IN rg_lane_dtl.CAPACITY_SUN%TYPE,
      vCapacityMon            IN rg_lane_dtl.CAPACITY_MON%TYPE,
      vCapacityTue            IN rg_lane_dtl.CAPACITY_TUE%TYPE,
      vCapacityWed            IN rg_lane_dtl.CAPACITY_WED%TYPE,
      vCapacityThu            IN rg_lane_dtl.CAPACITY_THU%TYPE,
      vCapacityFri            IN rg_lane_dtl.CAPACITY_FRI%TYPE,
      vCapacitySat            IN rg_lane_dtl.CAPACITY_SAT%TYPE,
      vYearlyCommitment       IN rg_lane_dtl.YEARLY_COMMITMENT%TYPE,
      vMonthlyCommitment      IN rg_lane_dtl.MONTHLY_COMMITMENT%TYPE,
      vWeeklyCommitment       IN rg_lane_dtl.WEEKLY_COMMITMENT%TYPE,
      vDailyCommitment        IN rg_lane_dtl.DAILY_COMMITMENT%TYPE,
      vCommitmentSun          IN rg_lane_dtl.COMMITMENT_SUN%TYPE,
      vCommitmentMon          IN rg_lane_dtl.COMMITMENT_MON%TYPE,
      vCommitmentTue          IN rg_lane_dtl.COMMITMENT_TUE%TYPE,
      vCommitmentWed          IN rg_lane_dtl.COMMITMENT_WED%TYPE,
      vCommitmentThu          IN rg_lane_dtl.COMMITMENT_THU%TYPE,
      vCommitmentFri          IN rg_lane_dtl.COMMITMENT_FRI%TYPE,
      vCommitmentSat          IN rg_lane_dtl.COMMITMENT_SAT%TYPE,
      vYearlyCommitPct        IN rg_lane_dtl.YEARLY_COMMIT_PCT%TYPE,
      vMonthlyCommitPct       IN rg_lane_dtl.MONTHLY_COMMIT_PCT%TYPE,
      vWeeklyCommitPct        IN rg_lane_dtl.WEEKLY_COMMIT_PCT%TYPE,
      vDailyCommitPct         IN rg_lane_dtl.DAILY_COMMIT_PCT%TYPE,
      vCommitPctSun           IN rg_lane_dtl.COMMIT_PCT_SUN%TYPE,
      vCommitPctMon           IN rg_lane_dtl.COMMIT_PCT_MON%TYPE,
      vCommitPctTue           IN rg_lane_dtl.COMMIT_PCT_TUE%TYPE,
      vCommitPctWed           IN rg_lane_dtl.COMMIT_PCT_WED%TYPE,
      vCommitPctThu           IN rg_lane_dtl.COMMIT_PCT_THU%TYPE,
      vCommitPctFri           IN rg_lane_dtl.COMMIT_PCT_FRI%TYPE,
      vCommitPctSat           IN rg_lane_dtl.COMMIT_PCT_SAT%TYPE,
      vIsPreferred            IN rg_lane_dtl.IS_PREFERRED%TYPE,
      vEffectiveDT            IN rg_lane_dtl.EFFECTIVE_DT%TYPE,
      vExpirationDT           IN rg_lane_dtl.EXPIRATION_DT%TYPE,
      vImpRGDtlStatus         IN import_rg_lane_dtl.IMPORT_RG_DTL_STATUS%TYPE,
      vToleranceFactor        IN rg_lane_dtl.TT_TOLERANCE_FACTOR%TYPE,
      vPackageId              IN import_rg_lane_dtl.PACKAGE_ID%TYPE,
      vPackageName            IN rg_lane_dtl.PACKAGE_NAME%TYPE,
      vSizeUOMId              IN rg_lane_dtl.SIZE_UOM_ID%TYPE,
      vOverrideCode           IN rg_lane_dtl.OVERRIDE_CODE%TYPE,
      vPreferenceBonusValue   IN rg_lane_dtl.PREFERENCE_BONUS_VALUE%TYPE,
      vCubingIndicator        IN rg_lane_dtl.CUBING_INDICATOR%TYPE,
      vLPNType                IN rg_lane_dtl.SP_LPN_TYPE%TYPE,
      vMinLPNcount            IN rg_lane_dtl.SP_MIN_LPN_COUNT%TYPE,
      vMaxLPNcount            IN rg_lane_dtl.SP_MAX_LPN_COUNT%TYPE,
      vMinWeight              IN rg_lane_dtl.SP_MIN_WEIGHT%TYPE,
      vMaxWeight              IN rg_lane_dtl.SP_MAX_WEIGHT%TYPE,
      vMinVolume              IN rg_lane_dtl.SP_MIN_VOLUME%TYPE,
      vMaxVolume              IN rg_lane_dtl.SP_MAX_VOLUME%TYPE,
      vMinLength              IN rg_lane_dtl.SP_MIN_LINEAR_FEET%TYPE,
      vMaxLength              IN rg_lane_dtl.SP_MAX_LINEAR_FEET%TYPE,
      vMinMonetaryValue       IN rg_lane_dtl.SP_MIN_MONETARY_VALUE%TYPE,
      vMaxMonetaryValue       IN rg_lane_dtl.SP_MAX_MONETARY_VALUE%TYPE,
      vMVCurrencyCode         IN rg_lane_dtl.SP_CURRENCY_CODE%TYPE,
	  vVoyage				  IN rg_lane_dtl.VOYAGE%TYPE)
      RETURN NUMBER
   IS
      max_lane_dtl_seq    rg_lane_dtl.rg_lane_dtl_seq%TYPE;
      rld_seq             rg_lane_dtl.rg_lane_dtl_seq%TYPE;
      rg_dtl_seq_cursor   ref_curtype;
      expire_dtl_sql      VARCHAR2 (2000);
      vHasShippingParam   rg_lane_dtl.has_shipping_param%TYPE;
   BEGIN
      IF (    VLPNTYPE IS NULL
          AND VMINLPNCOUNT IS NULL
          AND VMAXLPNCOUNT IS NULL
          AND VMINWEIGHT IS NULL
          AND VMAXWEIGHT IS NULL
          AND VMINVOLUME IS NULL
          AND VMAXVOLUME IS NULL
          AND VMINLENGTH IS NULL
          AND VMAXLENGTH IS NULL
          AND VMINMONETARYVALUE IS NULL
          AND VMAXMONETARYVALUE IS NULL
          AND VMVCURRENCYCODE IS NULL)
      THEN
         vHasShippingParam := 0;
      ELSE
         vHasShippingParam := 1;
      END IF;

      /*-----------------------------------------------------------------------
       # S.Lal     - 05/17/02
       #
       # Change the code to build the cursor dynamically
       #----------------------------------------------------------------------*/

      --  vImpRGDtlStatus is not the lane detail status. Possible values are:
      --  0 for Import as regular lane
      --  1 for expire this lane

      IF (vImpRGDtlStatus = 1)
      THEN
         expire_dtl_sql :=
               ' SELECT NVL(rg_lane_dtl_seq, 0) '
            || ' FROM RG_LANE_DTL '
            || ' WHERE TC_COMPANY_ID = '
            || vTCCompanyId
            || ' AND LANE_ID = '
            || vRgLaneId
            || ' AND EFFECTIVE_DT = '''
            || TO_CHAR (vEffectiveDT)
            || ''' '
            || ' AND EXPIRATION_DT = '''
            || TO_CHAR (vExpirationDT)
            || ''' ';

         expire_dtl_sql :=
            expire_dtl_sql
            || GET_RESOURCE_WHERE_CLAUSE (vCarrierCode,
                                          vMot,
                                          vEquipment,
                                          vServiceLevel,
                                          p_scndr_carrier_code,
                                          vProtectionLevel,
										  vVoyage);

         rld_seq := 0;

         OPEN rg_dtl_seq_cursor FOR expire_dtl_sql;

         LOOP
            FETCH rg_dtl_seq_cursor INTO rld_seq;

            EXIT WHEN rg_dtl_seq_cursor%NOTFOUND;

            UPDATE RG_LANE_DTL
               SET expiration_dt = effective_dt - 1,
                   last_updated_dttm = SYSDATE
             WHERE     tc_company_id = vTCCompanyId
                   AND lane_id = vRgLaneId
                   AND rg_lane_dtl_seq = rld_seq;
         END LOOP;

         CLOSE rg_dtl_seq_cursor;

         COMMIT;

         RETURN rld_seq;
      ELSE
         SELECT (NVL (MAX (lane_dtl_seq), 0) + 1)
           INTO max_lane_dtl_seq
           FROM comb_lane_dtl
          WHERE tc_company_id = vTCCompanyId AND lane_id = vRGLaneId;

         DBMS_OUTPUT.PUT_LINE ('INSERTING NEW RECORD INTO RG_LANE_DTL TABLE');

         DBMS_OUTPUT.PUT_LINE (
               'COMAPNY_ID: '
            || TO_CHAR (vTCCompanyId)
            || ' Lane Id: '
            || TO_CHAR (vRgLaneId));

         INSERT INTO COMB_LANE_DTL (TC_COMPANY_ID,
                                    LANE_ID,
                                    LANE_DTL_SEQ,
                                    IS_ROUTING,
                                    CARRIER_ID,
                                    MOT_ID,
                                    EQUIPMENT_ID,
                                    SERVICE_LEVEL_ID,
                                    PROTECTION_LEVEL_ID,
									VOYAGE,
                                    TIER_ID,
                                    RANK,
                                    REP_TP_FLAG,
                                    YEARLY_CAPACITY,
                                    MONTLY_CAPACITY,
                                    WEEKLY_CAPACITY,
                                    DAILY_CAPACITY,
                                    CAPACITY_SUN,
                                    CAPACITY_MON,
                                    CAPACITY_TUE,
                                    CAPACITY_WED,
                                    CAPACITY_THU,
                                    CAPACITY_FRI,
                                    CAPACITY_SAT,
                                    YEARLY_COMMITMENT,
                                    MONTLY_COMMITMENT,
                                    WEEKLY_COMMITMENT,
                                    DAILY_COMMITMENT,
                                    COMMITMENT_SUN,
                                    COMMITMENT_MON,
                                    COMMITMENT_TUE,
                                    COMMITMENT_WED,
                                    COMMITMENT_THU,
                                    COMMITMENT_FRI,
                                    COMMITMENT_SAT,
                                    YEARLY_COMMIT_PCT,
                                    MONTLY_COMMIT_PCT,
                                    WEEKLY_COMMIT_PCT,
                                    DAILY_COMMIT_PCT,
                                    COMMIT_PCT_SUN,
                                    COMMIT_PCT_MON,
                                    COMMIT_PCT_TUE,
                                    COMMIT_PCT_WED,
                                    COMMIT_PCT_THU,
                                    COMMIT_PCT_FRI,
                                    COMMIT_PCT_SAT,
                                    LAST_UPDATED_SOURCE_TYPE,
                                    LAST_UPDATED_SOURCE,
                                    LAST_UPDATED_DTTM,
                                    IS_PREFERRED,
                                    LANE_DTL_STATUS,
                                    EFFECTIVE_DT,
                                    EXPIRATION_DT,
                                    SCNDR_CARRIER_ID,
                                    TT_TOLERANCE_FACTOR,
                                    PACKAGE_ID,
                                    PACKAGE_NAME,
                                    SIZE_UOM_ID,
                                    OVERRIDE_CODE,
                                    PREFERENCE_BONUS_VALUE,
                                    CUBING_INDICATOR,
                                    SP_LPN_TYPE,
                                    SP_MIN_LPN_COUNT,
                                    SP_MAX_LPN_COUNT,
                                    SP_MIN_WEIGHT,
                                    SP_MAX_WEIGHT,
                                    SP_MIN_VOLUME,
                                    SP_MAX_VOLUME,
                                    SP_MIN_LINEAR_FEET,
                                    SP_MAX_LINEAR_FEET,
                                    SP_MIN_MONETARY_VALUE,
                                    SP_MAX_MONETARY_VALUE,
                                    SP_CURRENCY_CODE,
                                    HAS_SHIPPING_PARAM)
              VALUES (vTCCompanyId,
                      vRGLaneId,
                      max_lane_dtl_seq,
                      1,
                      vCarrierCode,
                      vMot,
                      vEquipment,
                      vServiceLevel,
                      vProtectionLevel,
					  vVoyage,
                      vTier,
                      vRank,
                      vRepTPFlag,
                      vYearlyCapacity,
                      vMonthlyCapacity,
                      vWeeklyCapacity,
                      vDailyCapacity,
                      vCapacitySun,
                      vCapacityMon,
                      vCapacityTue,
                      vCapacityWed,
                      vCapacityThu,
                      vCapacityFri,
                      vCapacitySat,
                      vYearlyCommitment,
                      vMonthlyCommitment,
                      vWeeklyCommitment,
                      vDailyCommitment,
                      vCommitmentSun,
                      vCommitmentMon,
                      vCommitmentTue,
                      vCommitmentWed,
                      vCommitmentThu,
                      vCommitmentFri,
                      vCommitmentSat,
                      vYearlyCommitPct,
                      vMonthlyCommitPct,
                      vWeeklyCommitPct,
                      vDailyCommitPct,
                      vCommitPctSun,
                      vCommitPctMon,
                      vCommitPctTue,
                      vCommitPctWed,
                      vCommitPctThu,
                      vCommitPctFri,
                      vCommitPctSat,
                      3,
                      'OMS',
                      SYSDATE,
                      vIsPreferred,
                      0,
                      vEffectiveDT,
                      vExpirationDT,
                      p_scndr_carrier_code,
                      vToleranceFactor,
                      vPackageId,
                      vPackageName,
                      vSizeUOMId,
                      VOVERRIDECODE,
                      VPREFERENCEBONUSVALUE,
                      VCUBINGINDICATOR,
                      VLPNTYPE,
                      VMINLPNCOUNT,
                      VMAXLPNCOUNT,
                      VMINWEIGHT,
                      VMAXWEIGHT,
                      VMINVOLUME,
                      VMAXVOLUME,
                      VMINLENGTH,
                      VMAXLENGTH,
                      VMINMONETARYVALUE,
                      VMAXMONETARYVALUE,
                      VMVCURRENCYCODE,
                      vHasShippingParam);

         COMMIT;

         RETURN max_lane_dtl_seq;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         SELECT (NVL (MAX (lane_dtl_seq), 0) + 1)
           INTO max_lane_dtl_seq
           FROM comb_lane_dtl
          WHERE tc_company_id = vTCCompanyId AND lane_id = vRGLaneId;

         INSERT INTO COMB_LANE_DTL (TC_COMPANY_ID,
                                    LANE_ID,
                                    LANE_DTL_SEQ,
                                    IS_ROUTING,
                                    CARRIER_ID,
                                    MOT_ID,
                                    EQUIPMENT_ID,
                                    SERVICE_LEVEL_ID,
                                    PROTECTION_LEVEL_ID,
									VOYAGE,
                                    TIER_ID,
                                    RANK,
                                    REP_TP_FLAG,
                                    YEARLY_CAPACITY,
                                    MONTLY_CAPACITY,
                                    WEEKLY_CAPACITY,
                                    DAILY_CAPACITY,
                                    CAPACITY_SUN,
                                    CAPACITY_MON,
                                    CAPACITY_TUE,
                                    CAPACITY_WED,
                                    CAPACITY_THU,
                                    CAPACITY_FRI,
                                    CAPACITY_SAT,
                                    YEARLY_COMMITMENT,
                                    MONTLY_COMMITMENT,
                                    WEEKLY_COMMITMENT,
                                    DAILY_COMMITMENT,
                                    COMMITMENT_SUN,
                                    COMMITMENT_MON,
                                    COMMITMENT_TUE,
                                    COMMITMENT_WED,
                                    COMMITMENT_THU,
                                    COMMITMENT_FRI,
                                    COMMITMENT_SAT,
                                    YEARLY_COMMIT_PCT,
                                    MONTLY_COMMIT_PCT,
                                    WEEKLY_COMMIT_PCT,
                                    DAILY_COMMIT_PCT,
                                    COMMIT_PCT_SUN,
                                    COMMIT_PCT_MON,
                                    COMMIT_PCT_TUE,
                                    COMMIT_PCT_WED,
                                    COMMIT_PCT_THU,
                                    COMMIT_PCT_FRI,
                                    COMMIT_PCT_SAT,
                                    LAST_UPDATED_SOURCE_TYPE,
                                    LAST_UPDATED_SOURCE,
                                    LAST_UPDATED_DTTM,
                                    IS_PREFERRED,
                                    LANE_DTL_STATUS,
                                    EFFECTIVE_DT,
                                    EXPIRATION_DT,
                                    SCNDR_CARRIER_ID,
                                    TT_TOLERANCE_FACTOR,
                                    PACKAGE_ID,
                                    PACKAGE_NAME,
                                    SIZE_UOM_ID,
                                    OVERRIDE_CODE,
                                    PREFERENCE_BONUS_VALUE,
                                    CUBING_INDICATOR,
                                    SP_LPN_TYPE,
                                    SP_MIN_LPN_COUNT,
                                    SP_MAX_LPN_COUNT,
                                    SP_MIN_WEIGHT,
                                    SP_MAX_WEIGHT,
                                    SP_MIN_VOLUME,
                                    SP_MAX_VOLUME,
                                    SP_MIN_LINEAR_FEET,
                                    SP_MAX_LINEAR_FEET,
                                    SP_MIN_MONETARY_VALUE,
                                    SP_MAX_MONETARY_VALUE,
                                    SP_CURRENCY_CODE,
                                    HAS_SHIPPING_PARAM)
              VALUES (vTCCompanyId,
                      vRGLaneId,
                      max_lane_dtl_seq,
                      1,
                      vCarrierCode,
                      vMot,
                      vEquipment,
                      vServiceLevel,
                      vProtectionLevel,
					  vVoyage,
                      vTier,
                      vRank,
                      vRepTPFlag,
                      vYearlyCapacity,
                      vMonthlyCapacity,
                      vWeeklyCapacity,
                      vDailyCapacity,
                      vCapacitySun,
                      vCapacityMon,
                      vCapacityTue,
                      vCapacityWed,
                      vCapacityThu,
                      vCapacityFri,
                      vCapacitySat,
                      vYearlyCommitment,
                      vMonthlyCommitment,
                      vWeeklyCommitment,
                      vDailyCommitment,
                      vCommitmentSun,
                      vCommitmentMon,
                      vCommitmentTue,
                      vCommitmentWed,
                      vCommitmentThu,
                      vCommitmentFri,
                      vCommitmentSat,
                      vYearlyCommitPct,
                      vMonthlyCommitPct,
                      vWeeklyCommitPct,
                      vDailyCommitPct,
                      vCommitPctSun,
                      vCommitPctMon,
                      vCommitPctTue,
                      vCommitPctWed,
                      vCommitPctThu,
                      vCommitPctFri,
                      vCommitPctSat,
                      3,
                      'OMS',
                      SYSDATE,
                      vIsPreferred,
                      0,
                      vEffectiveDT,
                      TRUNC (SYSDATE - 1),
                      p_scndr_carrier_code,
                      vToleranceFactor,
                      vPackageId,
                      vPackageName,
                      vSizeUOMId,
                      VOVERRIDECODE,
                      VPREFERENCEBONUSVALUE,
                      VCUBINGINDICATOR,
                      VLPNTYPE,
                      VMINLPNCOUNT,
                      VMAXLPNCOUNT,
                      VMINWEIGHT,
                      VMAXWEIGHT,
                      VMINVOLUME,
                      VMAXVOLUME,
                      VMINLENGTH,
                      VMAXLENGTH,
                      VMINMONETARYVALUE,
                      VMAXMONETARYVALUE,
                      VMVCURRENCYCODE,
                      vHasShippingParam);

         COMMIT;

         RETURN max_lane_dtl_seq;
   END INSERT_RG_LANE_DTL;

   PROCEDURE INSERT_SURGE_CAPACITY (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vRGLaneId          IN surge_capacity.LANE_ID%TYPE,
      vRGLaneDtlId       IN surge_capacity.RG_LANE_DTL_SEQ%TYPE,
      vEffectiveDT       IN surge_capacity.EFFECTIVE_DT%TYPE,
      vExpirationDT      IN surge_capacity.EXPIRATION_DT%TYPE,
      vYearlyCapacity    IN surge_capacity.YEARLY_SURGE_CAPACITY%TYPE,
      vMonthlyCapacity   IN surge_capacity.MONTHLY_SURGE_CAPACITY%TYPE,
      vWeeklyCapacity    IN surge_capacity.WEEKLY_SURGE_CAPACITY%TYPE,
      vDailyCapacity     IN surge_capacity.DAILY_SURGE_CAPACITY%TYPE,
      vCapacitySun       IN surge_capacity.SURGE_CAPACITY_SUN%TYPE,
      vCapacityMon       IN surge_capacity.SURGE_CAPACITY_MON%TYPE,
      vCapacityTue       IN surge_capacity.SURGE_CAPACITY_TUE%TYPE,
      vCapacityWed       IN surge_capacity.SURGE_CAPACITY_WED%TYPE,
      vCapacityThu       IN surge_capacity.SURGE_CAPACITY_THU%TYPE,
      vCapacityFri       IN surge_capacity.SURGE_CAPACITY_FRI%TYPE,
      vCapacitySat       IN surge_capacity.SURGE_CAPACITY_SAT%TYPE)
   AS
   BEGIN
      INSERT INTO SURGE_CAPACITY (SURGE_CAPACITY_ID,
                                  TC_COMPANY_ID,
                                  LANE_ID,
                                  RG_LANE_DTL_SEQ,
                                  EFFECTIVE_DT,
                                  EXPIRATION_DT,
                                  YEARLY_SURGE_CAPACITY,
                                  MONTHLY_SURGE_CAPACITY,
                                  WEEKLY_SURGE_CAPACITY,
                                  DAILY_SURGE_CAPACITY,
                                  SURGE_CAPACITY_SUN,
                                  SURGE_CAPACITY_MON,
                                  SURGE_CAPACITY_TUE,
                                  SURGE_CAPACITY_WED,
                                  SURGE_CAPACITY_THU,
                                  SURGE_CAPACITY_FRI,
                                  SURGE_CAPACITY_SAT)
           VALUES (SEQ_SURGE_CAPACITY_ID.NEXTVAL,
                   vTCCompanyId,
                   vRGLaneId,
                   vRGLaneDtlId,
                   vEffectiveDT,
                   vExpirationDT,
                   vYearlyCapacity,
                   vMonthlyCapacity,
                   vWeeklyCapacity,
                   vDailyCapacity,
                   vCapacitySun,
                   vCapacityMon,
                   vCapacityTue,
                   vCapacityWed,
                   vCapacityThu,
                   vCapacityFri,
                   vCapacitySat);

      COMMIT;
   END INSERT_SURGE_CAPACITY;

   PROCEDURE INSERT_DAILY_UTILIZATION (
      vTCCompanyId        IN company.COMPANY_ID%TYPE,
      vUtilizationDT      IN daily_utilization.UTILIZATION_DATE%TYPE,
      vRGLaneId           IN daily_utilization.LANE_ID%TYPE,
      vRGLaneDtlSeq       IN daily_utilization.RG_LANE_DTL_SEQ%TYPE,
      vDailyUtilization   IN daily_utilization.DAILY_UTILIZATION%TYPE)
   AS
      max_internal_seq   NUMBER (8);
   BEGIN
      SELECT (NVL (MAX (internal_seq), 0) + 1)
        INTO max_internal_seq
        FROM daily_utilization
       WHERE tc_company_id = vTCCompanyId AND lane_id = vRGLaneId;

      INSERT INTO DAILY_UTILIZATION (TC_COMPANY_ID,
                                     UTILIZATION_DATE,
                                     INTERNAL_SEQ,
                                     LANE_ID,
                                     RG_LANE_DTL_SEQ,
                                     DAILY_UTILIZATION)
           VALUES (vTCCompanyID,
                   vUtilizationDT,
                   max_internal_seq,
                   vRGLaneId,
                   vRGLaneDtlSeq,
                   vDailyUtilization);
   END INSERT_DAILY_UTILIZATION;

   PROCEDURE INSERT_RG_LANE_DTL_ERROR (
      vTCCompanyId         IN company.COMPANY_ID%TYPE,
      vLaneId              IN rg_lane.LANE_ID%TYPE,
      vRGLaneDtlSeq        IN rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      vErrorMsgDesc        IN rg_lane_errors.ERROR_MSG_DESC%TYPE,
      p_error_msg_id       IN RG_LANE_ERRORS.ERROR_MSG_ID%TYPE DEFAULT NULL,
      p_error_param_list   IN RG_LANE_ERRORS.ERROR_PARAM_LIST%TYPE DEFAULT NULL)
   AS
      vBatchId   rg_lane_errors.BATCH_ID%TYPE;
   BEGIN
      SELECT BATCH_ID
        INTO vBatchId
        FROM IMPORT_RG_LANE
       WHERE LANE_ID = vLaneId AND TC_COMPANY_ID = vTCCompanyId;

      INSERT INTO RG_LANE_ERRORS (RG_LANE_ERROR,
                                  TC_COMPANY_ID,
                                  LANE_ID,
                                  RG_LANE_DTL_SEQ,
                                  ERROR_MSG_DESC,
                                  BATCH_ID,
                                  ERROR_MSG_ID,
                                  ERROR_PARAM_LIST)
           VALUES (SEQ_RG_LANE_ERROR.NEXTVAL,
                   vTCCompanyId,
                   vLaneId,
                   vRGLaneDtlSeq,
                   vErrorMsgDesc,
                   vBatchId,
                   p_error_msg_id,
                   p_error_param_list);

      COMMIT;
   END INSERT_RG_LANE_DTL_ERROR;

   FUNCTION DELETE_IMP_RG_LANES (
      vTCCompanyId    IN company.COMPANY_ID%TYPE,
      vImportLaneID   IN import_rg_lane.LANE_ID%TYPE)
      RETURN NUMBER
   IS
   BEGIN
      DELETE FROM IMPORT_RG_LANE
            WHERE TC_COMPANY_ID = vTCCompanyId AND LANE_ID = vImportLaneID
                  AND NOT EXISTS
                             (SELECT 1
                                FROM IMPORT_RG_LANE_DTL
                               WHERE TC_COMPANY_ID = vTCCompanyId
                                     AND LANE_ID = vImportLaneID);

      COMMIT;
      RETURN 0;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE ('Procedure DELETE_IMP_RG_LANES: ' || SQLERRM);

         RETURN 1;
   END DELETE_IMP_RG_LANES;

   FUNCTION VALIDATE_LANE_ADDRESSES (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vLaneId        IN rg_lane.LANE_ID%TYPE,
      vPostalCode    IN rg_lane.O_POSTAL_CODE%TYPE,
      vState         IN rg_lane.O_STATE_PROV%TYPE,
      vCountry       IN rg_lane.O_COUNTRY_CODE%TYPE)
      RETURN NUMBER
   IS
      vPassFail      NUMBER;
      vStateCount    NUMBER := 0;
      vPostalCount   NUMBER := 0;
   BEGIN
      IF (vState IS NULL) OR (vPostalCode IS NULL) OR (vCountry IS NULL)
      THEN
         vPassFail := 1;

         INSERT_RG_LANE_ERROR (
            vTCCompanyId,
            vLaneId,
            'Invalid Address, State/Country/Postal Code Required.',
            4720005,
            NULL);
      END IF;

      SELECT COUNT (*)
        INTO vStateCount
        FROM postal_code
       WHERE state_prov = vState AND country_code = vCountry;

      IF vStateCount = 0
      THEN
         vPassFail := 1;

         INSERT_RG_LANE_ERROR (
            vTCCompanyId,
            vLaneId,
            'Invalid Address, Check State/Country/Postal Code.',
            4720006,
            NULL);
      ELSE
         SELECT COUNT (*)
           INTO vPostalCount
           FROM postal_code
          WHERE     state_prov = vState
                AND country_code = vCountry
                AND postal_code = vPostalCode;

         IF vStateCount = 0
         THEN
            vPassFail := 1;

            INSERT_RG_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               'Invalid Address, Check State/Country/Postal Code.',
               4720006,
               NULL);
         END IF;
      END IF;

      RETURN vPassFail;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Procedure VALIDATE_LANE_ADDRESSES: ' || SQLERRM);

         RETURN 1;
   END VALIDATE_LANE_ADDRESSES;

   FUNCTION VALIDATE_LANE_BASE_DATA (
      vTCCompanyId         IN company.COMPANY_ID%TYPE,
      vLaneId              IN rg_lane.LANE_ID%TYPE,
      vBusinessUnit        IN business_unit.BUSINESS_UNIT%TYPE,
      vOFacilityId         IN facility.FACILITY_ID%TYPE,
      vOFacilityAlias      IN facility_alias.FACILITY_ALIAS_ID%TYPE,
      vOFacilityBU         IN business_unit.BUSINESS_UNIT%TYPE,
      vOStateProv          IN state_prov.STATE_PROV%TYPE,
      vOPostal             IN postal_code.POSTAL_CODE%TYPE,
      vOCountry            IN country.COUNTRY_CODE%TYPE,
      vOZoneId             IN zone.ZONE_ID%TYPE,
      vOZoneName           IN import_rg_lane.O_ZONE_NAME%TYPE,
      vDFacilityId         IN facility.FACILITY_ID%TYPE,
      vDFacilityAlias      IN facility_alias.FACILITY_ALIAS_ID%TYPE,
      vDFacilityBU         IN business_unit.BUSINESS_UNIT%TYPE,
      vDStateProv          IN state_prov.STATE_PROV%TYPE,
      vDPostal             IN postal_code.POSTAL_CODE%TYPE,
      vDCountry            IN country.COUNTRY_CODE%TYPE,
      vDZoneId             IN zone.ZONE_ID%TYPE,
      vDZoneName           IN import_rg_lane.D_ZONE_NAME%TYPE,
      vCustomerId          IN rg_lane.CUSTOMER_ID%TYPE,
      vCustomerCode        IN import_rg_lane.CUSTOMER_CODE%TYPE,
      vCustomerCodeBU      IN import_rg_lane.CUSTOMER_CODE_BU%TYPE,
      vBillingMethod       IN rg_lane.BILLING_METHOD%TYPE,
      vBillingMethodCode   IN import_rg_lane.BILLING_METHOD_CODE%TYPE,
      vIncotermId          IN rg_lane.INCOTERM_ID%TYPE,
      vIncotermName        IN import_rg_lane.INCOTERM_NAME%TYPE,
      vIncotermNameBU      IN import_rg_lane.INCOTERM_NAME_BU%TYPE)
      RETURN NUMBER
   IS
      vPassFail            NUMBER;
      vFailure             NUMBER;
      vOTempFacility       NUMBER (8);
      vDTempFacility       NUMBER (8);
      vOFacilityBUID       NUMBER;
      vDFacilityBUID       NUMBER;
      vCustomerCodeBUID    NUMBER;
      vTempCustomerId      NUMBER;
      vIncotermNameBUID    NUMBER;
      vTempIncotermId      NUMBER;
      vTempBillingMethod   NUMBER;
      VTCID                NUMBER;
      vCount               NUMBER;
      VALLNULLS            NUMBER;
      VERRORMSGDESC        VARCHAR2 (100);
      VERRORID             NUMBER;
      vRootCompanyId       COMPANY.COMPANY_ID%TYPE;
      VERRORLIST           VARCHAR2 (10);
   BEGIN
      vFailure := 0;
      vCount := 0;
      VTCID := NULL;
      VALLNULLS := 1;

      IF (vBusinessUnit IS NOT NULL)
      THEN
         VALLNULLS := 0;
         vPassFail := 0;

         SELECT COUNT (PARENT_COMPANY_ID)
           INTO vCount
           FROM COMPANY
          WHERE COMPANY_ID = vTCCompanyId;

         IF (vCount > 0)
         THEN
            SELECT PARENT_COMPANY_ID
              INTO VTCID
              FROM COMPANY
             WHERE COMPANY_ID = vTCCompanyId;
         END IF;

         vPassFail :=
            RG_SUB_VALIDATION_PKG.VALIDATE_BUSINESS_UNIT (VTCID,
                                                          vBusinessUnit);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vBusinessUnit || ' is an invalid Business Unit',
               4720007,
               vBusinessUnit);
         END IF;
      END IF;

      SELECT ROOTCOMPANYID
        INTO vRootCompanyId
        FROM IMPORT_RG_LANE
       WHERE LANE_ID = vLaneId;

      IF (vOFacilityBU IS NULL)
      THEN
         vOFacilityBUID := vRootCompanyId;
      ELSE
         vOFacilityBUID :=
            GET_BUID_FROM_BUSINESS_UNIT (vRootCompanyId, vOFacilityBU);
      END IF;

      IF (vOFacilityId IS NOT NULL)
      THEN
         VALLNULLS := 0;
         vPassFail := 0;
         vPassFail :=
            RG_SUB_VALIDATION_PKG.VALIDATE_FACILITY (vOFacilityBUID,
                                                     vOFacilityId);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vOFacilityId || ' is an invalid Origin Facility',
               4720008,
               vOFacilityId);
         END IF;
      END IF;

      IF (vOFacilityAlias IS NOT NULL)
      THEN
         VALLNULLS := 0;
         vPassFail := 0;
         vPassFail :=
            RG_SUB_VALIDATION_PKG.VALIDATE_FACILITY_ALIAS (vOFacilityBUID,
                                                           vOFacilityAlias);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vOFacilityAlias || ' is an invalid Origin Facility Alias',
               4720009,
               vOFacilityAlias);
         ELSE
            IF (vOFacilityId IS NULL)
            THEN
               SELECT FACILITY_ID
                 INTO vOTempFacility
                 FROM FACILITY_ALIAS
                WHERE FACILITY_ALIAS_ID = vOFacilityAlias
                      AND TC_COMPANY_ID = vOFacilityBUID;

               UPDATE IMPORT_RG_LANE
                  SET O_FACILITY_ID = vOTempFacility
                WHERE O_FACILITY_ALIAS_ID = vOFacilityAlias
                      AND TC_COMPANY_ID = vTCCompanyId;

               COMMIT;
            END IF;
         END IF;
      END IF;

      IF (vOStateProv IS NOT NULL)
      THEN
         VALLNULLS := 0;
         vPassFail := 0;
         vPassFail := RG_SUB_VALIDATION_PKG.VALIDATE_STATE_PROV (vOStateProv);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vOStateProv || ' is an invalid Origin State/Prov',
               4720010,
               vOStateProv);
         END IF;
      END IF;

      IF (vOCountry IS NOT NULL)
      THEN
         VALLNULLS := 0;
         vPassFail := 0;
         vPassFail := RG_SUB_VALIDATION_PKG.VALIDATE_COUNTRY (vOCountry);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vOCountry || ' is an invalid Origin Country',
               4720011,
               vOCountry);
         END IF;
      END IF;

      IF (vOZoneName IS NOT NULL)
      THEN
         VALLNULLS := 0;

         IF (vOZoneId IS NULL)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_ERROR (vTCCompanyId,
                                  vLaneId,
                                  vOZoneName || ' is an invalid Origin Zone',
                                  4720012,
                                  vOZoneName);
         END IF;
      END IF;

      IF (vDFacilityBU IS NULL)
      THEN
         vDFacilityBUID := vRootCompanyId;
      ELSE
         vDFacilityBUID :=
            GET_BUID_FROM_BUSINESS_UNIT (vRootCompanyId, vDFacilityBU);
      END IF;

      IF (vDFacilityId IS NOT NULL)
      THEN
         VALLNULLS := 0;
         vPassFail := 0;
         vPassFail :=
            RG_SUB_VALIDATION_PKG.VALIDATE_FACILITY (vDFacilityBUID,
                                                     vDFacilityId);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vDFacilityId || ' is an invalid Destination Facility',
               4720013,
               vDFacilityId);
         END IF;
      END IF;

      IF (vDFacilityAlias IS NOT NULL)
      THEN
         VALLNULLS := 0;
         vPassFail := 0;
         vPassFail :=
            RG_SUB_VALIDATION_PKG.VALIDATE_FACILITY_ALIAS (vDFacilityBUID,
                                                           vDFacilityAlias);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vDFacilityAlias || ' is an invalid Destination Facility Alias',
               4720014,
               vDFacilityAlias);
         ELSE
            IF (vDFacilityId IS NULL)
            THEN
               SELECT FACILITY_ID
                 INTO vDTempFacility
                 FROM FACILITY_ALIAS
                WHERE FACILITY_ALIAS_ID = vDFacilityAlias
                      AND TC_COMPANY_ID = vDFacilityBUID;

               UPDATE IMPORT_RG_LANE
                  SET D_FACILITY_ID = vDTempFacility
                WHERE D_FACILITY_ALIAS_ID = vDFacilityAlias
                      AND TC_COMPANY_ID = vTCCompanyId;

               COMMIT;
            END IF;
         END IF;
      END IF;

      IF (vDStateProv IS NOT NULL)
      THEN
         VALLNULLS := 0;
         vPassFail := 0;
         vPassFail := RG_SUB_VALIDATION_PKG.VALIDATE_STATE_PROV (vDStateProv);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vDStateProv || ' is an invalid Destination State/Prov',
               4720015,
               vDStateProv);
         END IF;
      END IF;

      IF (vDCountry IS NOT NULL)
      THEN
         VALLNULLS := 0;
         vPassFail := 0;
         vPassFail := RG_SUB_VALIDATION_PKG.VALIDATE_COUNTRY (vDCountry);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vDCountry || ' is an invalid Destination Country',
               4720016,
               vDCountry);
            COMMIT;
         END IF;
      END IF;

      IF (vDZoneName IS NOT NULL)
      THEN
         VALLNULLS := 0;

         IF (vDZoneId IS NULL)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               vDZoneName || ' is an invalid Destination Zone',
               4720017,
               vDZoneName);
         END IF;
      END IF;

      IF (vCustomerCodeBU IS NULL)
      THEN
         vCustomerCodeBUID := vRootCompanyId;
      ELSE
         vCustomerCodeBUID :=
            GET_BUID_FROM_BUSINESS_UNIT (vRootCompanyId, vCustomerCodeBU);
      END IF;

      IF (vCustomerId IS NOT NULL)
      THEN
         VALLNULLS := 0;
         VPASSFAIL := 0;
         VPASSFAIL :=
            RG_SUB_VALIDATION_PKG.VALIDATE_CUSTOMER (vCustomerCodeBUID,
                                                     vCustomerId);

         IF (VPASSFAIL = 1)
         THEN
            VFAILURE := 1;
            VERRORMSGDESC := VCUSTOMERID || ' is an invalid Customer ID';
            VERRORID := 2012;
            VERRORLIST := vCustomerId;
            INSERT_RG_LANE_ERROR (VTCCOMPANYID,
                                  VLANEID,
                                  VERRORMSGDESC,
                                  VERRORID,
                                  VERRORLIST);
         END IF;
      END IF;

      IF (vCustomerCode IS NOT NULL)
      THEN
         VALLNULLS := 0;
         VPASSFAIL := 0;
         VPASSFAIL :=
            RG_SUB_VALIDATION_PKG.VALIDATE_CUSTOMER_CODE (vCustomerCodeBUID,
                                                          vCustomerCode);

         IF (VPASSFAIL = 1)
         THEN
            VFAILURE := 1;
            VERRORMSGDESC := VCUSTOMERCODE || ' is an invalid Customer Code';
            VERRORID := 4720181;
            VERRORLIST := vCustomerCode;
            INSERT_RG_LANE_ERROR (VTCCOMPANYID,
                                  VLANEID,
                                  VERRORMSGDESC,
                                  VERRORID,
                                  VERRORLIST);
         ELSE
            IF (vCustomerId IS NULL)
            THEN
               SELECT CUSTOMER_ID
                 INTO vTempCustomerId
                 FROM CUSTOMER
                WHERE CUSTOMER_CODE = vCustomerCode
                      AND TC_COMPANY_ID = vCustomerCodeBUID;

               UPDATE IMPORT_RG_LANE
                  SET CUSTOMER_ID = vTempCustomerId
                WHERE CUSTOMER_CODE = vCustomerCode
                      AND TC_COMPANY_ID = vTcCompanyId;
            END IF;
         END IF;
      END IF;

      IF (vIncotermNameBU IS NULL)
      THEN
         vIncotermNameBUID := vRootCompanyId;
      ELSE
         vIncotermNameBUID :=
            GET_BUID_FROM_BUSINESS_UNIT (vRootCompanyId, vIncotermNameBU);
      END IF;

      IF (vIncotermId IS NOT NULL)
      THEN
         VALLNULLS := 0;
         VPASSFAIL := 0;
         VPASSFAIL :=
            RG_SUB_VALIDATION_PKG.VALIDATE_INCOTERM (vIncotermNameBUID,
                                                     vIncotermId);

         IF (VPASSFAIL = 1)
         THEN
            VFAILURE := 1;
            VERRORMSGDESC := VINCOTERMID || ' is an invalid Incoterm ID';
            VERRORID := 1500195;
            VERRORLIST := vIncotermId;
            INSERT_RG_LANE_ERROR (VTCCOMPANYID,
                                  VLANEID,
                                  VERRORMSGDESC,
                                  VERRORID,
                                  VERRORLIST);
         END IF;
      END IF;

      IF (vIncotermName IS NOT NULL)
      THEN
         VALLNULLS := 0;
         VPASSFAIL := 0;
         VPASSFAIL :=
            RG_SUB_VALIDATION_PKG.VALIDATE_INCOTERM_NAME (vIncotermNameBUID,
                                                          vIncotermName);

         IF (VPASSFAIL = 1)
         THEN
            VFAILURE := 1;
            VERRORMSGDESC := VINCOTERMNAME || ' is an invalid Incoterm Code';
            VERRORID := 4720182;
            VERRORLIST := vIncotermName;
            INSERT_RG_LANE_ERROR (VTCCOMPANYID,
                                  VLANEID,
                                  VERRORMSGDESC,
                                  VERRORID,
                                  VERRORLIST);
         ELSE
            IF (vIncotermId IS NULL)
            THEN
               SELECT INCOTERM_ID
                 INTO vTempIncotermId
                 FROM INCOTERM
                WHERE INCOTERM_NAME = vIncotermName;

               UPDATE IMPORT_RG_LANE
                  SET INCOTERM_ID = vTempIncotermId
                WHERE INCOTERM_NAME = vIncotermName
                      AND TC_COMPANY_ID = vTcCompanyId;
            END IF;
         END IF;
      END IF;

      IF (vBillingMethod IS NOT NULL)
      THEN
         VALLNULLS := 0;

         SELECT BILLING_METHOD
           INTO vTempBillingMethod
           FROM BILLING_METHOD
          WHERE BILLING_METHOD = vBillingMethod;

         IF (vTempBillingMethod IS NULL)
         THEN
            VFAILURE := 1;
            VERRORMSGDESC :=
               vBillingMethod || ' is an invalid Billing Method Code';
            VERRORID := 4720183;
            VERRORLIST := vBillingMethod;
            INSERT_RG_LANE_ERROR (VTCCOMPANYID,
                                  VLANEID,
                                  VERRORMSGDESC,
                                  VERRORID,
                                  VERRORLIST);
         END IF;
      END IF;

      IF (vBillingMethodCode IS NOT NULL)
      THEN
         VALLNULLS := 0;

         SELECT BILLING_METHOD
           INTO vTempBillingMethod
           FROM BILLING_METHOD
          WHERE TO_CHAR (BILLING_METHOD) = vBillingMethodCode;

         IF (vTempBillingMethod IS NULL)
         THEN
            VFAILURE := 1;
            VERRORMSGDESC :=
               vBillingMethodCode || ' is an invalid Billing Method Code';
            VERRORID := 4720183;
            VERRORLIST := vBillingMethodCode;
            INSERT_RG_LANE_ERROR (VTCCOMPANYID,
                                  VLANEID,
                                  VERRORMSGDESC,
                                  VERRORID,
                                  VERRORLIST);
         ELSE
            UPDATE IMPORT_RG_LANE
               SET BILLING_METHOD = vTempBillingMethod
             WHERE BILLING_METHOD_CODE = vBillingMethodCode;
         END IF;
      END IF;

      IF (VALLNULLS = 1)
      THEN
         VFAILURE := 1;
         VERRORMSGDESC := 'Required values missing on RG_LANE';
         VERRORID := 9992111;
         INSERT_RG_LANE_ERROR (VTCCOMPANYID,
                               VLANEID,
                               VERRORMSGDESC,
                               VERRORID,
                               VERRORLIST);
      END IF;

      RETURN vFailure;
   END VALIDATE_LANE_BASE_DATA;

   /*Vaibhav adding code */

   FUNCTION CHECK_IS_BU_VALID (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                               vBUID          IN company.COMPANY_ID%TYPE)
      RETURN NUMBER
   IS
      vParentCompanyId   NUMBER;
      vTcCompanyId1      NUMBER;
      vPassFail          NUMBER;
      vCount1            NUMBER;
   BEGIN
      vPassFail := 1;
      vParentCompanyId := 0;
      vTcCompanyId1 := vTCCompanyId;

      IF (vTcCompanyId1 = vBUID)
      THEN
         vPassFail := 0;
      END IF;

      WHILE (vParentCompanyId != -1)
      LOOP
         SELECT PARENT_COMPANY_ID
           INTO vParentCompanyId
           FROM COMPANY
          WHERE COMPANY_ID = vTcCompanyId1;

         vTcCompanyId1 := vParentCompanyId;

         IF (vTcCompanyId1 != -1 AND vTcCompanyId1 = vBUID)
         THEN
            vPassFail := 0;
         END IF;
      END LOOP;

      RETURN vPassFail;
   END CHECK_IS_BU_VALID;

   FUNCTION VALIDATE_DETAIL_BASE_DATA (
      vTCCompanyId               IN company.COMPANY_ID%TYPE,
      vLaneId                    IN rg_lane.LANE_ID%TYPE,
      vRgDtlSeq                  IN rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      vCarrierCode               IN IMPORT_RG_LANE_DTL.CARRIER_CODE%TYPE,
      vCarrierCodeBUID           IN company.COMPANY_ID%TYPE,
      vMode                      IN IMPORT_RG_LANE_DTL.MOT%TYPE,
      vModeBUID                  IN company.COMPANY_ID%TYPE,
      vServiceLevel              IN IMPORT_RG_LANE_DTL.SERVICE_LEVEL%TYPE,
      vServiceLevelBUID          IN company.COMPANY_ID%TYPE,
      vEquipment                 IN IMPORT_RG_LANE_DTL.EQUIPMENT_CODE%TYPE,
      vEquipmentBUID             IN company.COMPANY_ID%TYPE,
      vProtectionLevel           IN IMPORT_RG_LANE_DTL.PROTECTION_LEVEL%TYPE,
      vProtectionLevelBUID       IN company.COMPANY_ID%TYPE,
      p_scndr_carrier_code       IN CARRIER_CODE.CARRIER_CODE%TYPE,
      p_scndr_carrier_codeBUID   IN company.COMPANY_ID%TYPE,
      vSizeUOM                   IN IMPORT_RG_LANE_DTL.SIZE_UOM%TYPE,
      vSizeUOMBUID               IN IMPORT_RG_LANE_DTL.SIZE_UOM_ID%TYPE,
	  vRank                      IN IMPORT_RG_LANE_DTL.RANK%TYPE)
      RETURN NUMBER
   IS
      vPassFail            NUMBER;
      vFailure             NUMBER;
      vBUId                NUMBER;
      vCarrierId           rg_lane_dtl.CARRIER_ID%TYPE;
      vModeId              rg_lane_dtl.MOT_ID%TYPE;
      vSizeUOMId           RG_LANE_DTL.SIZE_UOM_ID%TYPE;
      vEquipmentId         rg_lane_dtl.EQUIPMENT_ID%TYPE;
      vServiceLevelId      rg_lane_dtl.SERVICE_LEVEL_ID%TYPE;
      p_scndr_carrier_Id   rg_LANE_DTL.SCNDR_CARRIER_ID%TYPE;
      vProtectionLevelId   rg_lane_dtl.PROTECTION_LEVEL_ID%TYPE;
      vErrorMsg            VARCHAR2 (255);
   BEGIN
      vPassFail := 0;
      vFailure := 0;
      vCarrierId := 0;
      vModeId := 0;
      vEquipmentId := 0;
      vServiceLevelId := 0;
      p_scndr_carrier_Id := 0;
      vProtectionLevelId := 0;
      vSizeUOMId := 0;

      vPassFail := CHECK_IS_BU_VALID (vTCCompanyId, vCarrierCodeBUID);

      IF (vCarrierCode IS NULL)
      THEN
         vErrorMsg := 'Carrier Code is a required field';
      ELSE
         vErrorMsg := vCarrierCode || ' is an invalid Carrier Code';
      END IF;

	  VALIDATE_RANK(vTCCompanyId, vLaneId, vRgDtlSeq, vRank);

      IF (vPassFail = 1)
      THEN
         vFailure := 1;
         INSERT_RG_LANE_DTL_ERROR (vTCCompanyId,
                                   vLaneId,
                                   vRGDtlSeq,
                                   vErrorMsg,
                                   4720035,
                                   vCarrierCode);
      ELSE
         vPassFail :=
            RG_SUB_VALIDATION_PKG.VALIDATE_CARRIER_CODE (vCarrierCodeBUID,
                                                         vCarrierCode);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (vTCCompanyId,
                                      vLaneId,
                                      vRGDtlSeq,
                                      vErrorMsg,
                                      4720035,
                                      vCarrierCode);
         ELSE
            IF (vCarrierId = 0)
            THEN
               SELECT carrier_id
                 INTO vCarrierId
                 FROM carrier_code
                WHERE carrier_code = vCarrierCode
                      AND tc_company_id = vCarrierCodeBUID
					  AND mark_for_deletion = 0;

               UPDATE import_rg_lane_dtl
                  SET carrier_id = vCarrierId
                WHERE     carrier_code = vCarrierCode
                      AND tc_company_id = vTCCompanyId
                      AND lane_id = vLaneId
                      AND RG_LANE_DTL_SEQ = vRGDtlSeq;
            END IF;
         END IF;
      END IF;

      IF (vMode IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail := CHECK_IS_BU_VALID (vTCCompanyId, vModeBUID);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (vTCCompanyId,
                                      vLaneId,
                                      vRGDtlSeq,
                                      vMode || ' is an invalid Mode',
                                      4720036,
                                      vMode);
         ELSE
            vPassFail :=
               RG_SUB_VALIDATION_PKG.VALIDATE_MODE (vModeBUID, vMode);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RG_LANE_DTL_ERROR (vTCCompanyId,
                                         vLaneId,
                                         vRGDtlSeq,
                                         vMode || ' is an invalid Mode',
                                         4720036,
                                         vMode);
            ELSE
               IF (vModeId = 0)
               THEN
                  SELECT mot_id
                    INTO vModeId
                    FROM mot
                   WHERE mot = vMode AND tc_company_id = vModeBUID AND mark_for_deletion = 0;

                  UPDATE import_rg_lane_dtl
                     SET mot_id = vModeId
                   WHERE     mot = vMode
                         AND tc_company_id = vTCCompanyId
                         AND lane_id = vLaneId
                         AND RG_LANE_DTL_SEQ = vRGDtlSeq;
               END IF;
            END IF;
         END IF;
      END IF;

      IF (vServiceLevel IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail := CHECK_IS_BU_VALID (vTCCompanyId, vServiceLevelBUID);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGDtlSeq,
               vServiceLevel || ' is an invalid Service Level',
               4720037,
               vServiceLevel);
         ELSE
            vPassFail :=
               RG_SUB_VALIDATION_PKG.VALIDATE_SERVICE_LEVEL (
                  vServiceLevelBUID,
                  vServiceLevel);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGDtlSeq,
                  vServiceLevel || ' is an invalid Service Level',
                  4720037,
                  vServiceLevel);
            ELSE
               IF (vServiceLevelId = 0)
               THEN
                  SELECT service_level_id
                    INTO vServiceLevelId
                    FROM service_level
                   WHERE service_level = vServiceLevel
                         AND tc_company_id = vServiceLevelBUID
						 AND mark_for_deletion = 0;

                  UPDATE import_rg_lane_dtl
                     SET service_level_id = vServiceLevelId
                   WHERE     service_level = vServiceLevel
                         AND tc_company_id = vTCCompanyId
                         AND lane_id = vLaneId
                         AND RG_LANE_DTL_SEQ = vRGDtlSeq;
               END IF;
            END IF;
         END IF;
      END IF;

      IF (vEquipment IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail := CHECK_IS_BU_VALID (vTCCompanyId, vEquipmentBUID);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGDtlSeq,
               vEquipment || ' is an invalid Equipment Code',
               4720038,
               vEquipment);
         ELSE
            vPassFail :=
               RG_SUB_VALIDATION_PKG.VALIDATE_EQUIPMENT (vEquipmentBUID,
                                                         vEquipment);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGDtlSeq,
                  vEquipment || ' is an invalid Equipment Code',
                  4720038,
                  vEquipment);
            ELSE
               IF (vEquipmentId = 0)
               THEN
                  SELECT equipment_id
                    INTO vEquipmentId
                    FROM equipment
                   WHERE equipment_code = vEquipment
                         AND tc_company_id = vEquipmentBUID
						 AND mark_for_deletion = 0;

                  UPDATE import_rg_lane_dtl
                     SET equipment_id = vEquipmentId
                   WHERE     equipment_code = vEquipment
                         AND tc_company_id = vTCCompanyId
                         AND lane_id = vLaneId
                         AND RG_LANE_DTL_SEQ = vRGDtlSeq;
               END IF;
            END IF;
         END IF;
      END IF;

      IF (vProtectionLevel IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail := CHECK_IS_BU_VALID (vTCCompanyId, vProtectionLevelBUId);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGDtlSeq,
               vProtectionLevel || ' is an invalid Protection Level',
               4720039,
               vProtectionLevel);
         ELSE
            vPassFail :=
               RG_SUB_VALIDATION_PKG.VALIDATE_PROTECTION_LEVEL (
                  vProtectionLevelBUId,
                  vProtectionLevel);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGDtlSeq,
                  vProtectionLevel || ' is an invalid Protection Level',
                  4720039,
                  vProtectionLevel);
            ELSE
               IF (vProtectionLevelId = 0)
               THEN
                  SELECT protection_level_id
                    INTO vProtectionLevelId
                    FROM protection_level
                   WHERE protection_level = vProtectionLevel
                         AND tc_company_id = vProtectionLevelBUID
						 AND mark_for_deletion = 0;

                  UPDATE import_rg_lane_dtl
                     SET protection_level_id = vProtectionLevelId
                   WHERE     protection_level = vProtectionLevel
                         AND tc_company_id = vTCCompanyId
                         AND lane_id = vLaneId
                         AND RG_LANE_DTL_SEQ = vRGDtlSeq;
               END IF;
            END IF;
         END IF;
      END IF;

      IF (p_scndr_carrier_code IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail :=
            CHECK_IS_BU_VALID (vTCCompanyId, p_scndr_carrier_codeBUID);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGDtlSeq,
               p_scndr_carrier_code
               || ' is an invalid Secondary Carrier Code',
               4720066,
               p_scndr_carrier_code);
         ELSE
            vPassFail :=
               RG_SUB_VALIDATION_PKG.VALIDATE_CARRIER_CODE (
                  p_scndr_carrier_codeBUID,
                  p_scndr_carrier_code);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGDtlSeq,
                  p_scndr_carrier_code
                  || ' is an invalid Secondary Carrier Code',
                  4720066,
                  p_scndr_carrier_code);
            ELSE
               IF (p_scndr_carrier_Id = 0)
               THEN
                  SELECT CARRIER_ID
                    INTO p_scndr_carrier_Id
                    FROM carrier_code
                   WHERE carrier_code = p_scndr_carrier_code
                         AND tc_company_id = p_scndr_carrier_codeBUID
						 AND mark_for_deletion = 0;

                  UPDATE import_rg_lane_dtl
                     SET SCNDR_CARRIER_ID = p_scndr_carrier_Id
                   WHERE     SCNDR_CARRIER_CODE = p_scndr_carrier_code
                         AND tc_company_id = vTCCompanyId
                         AND lane_id = vLaneId
                         AND RG_LANE_DTL_SEQ = vRGDtlSeq;
               END IF;
            END IF;
         END IF;
      END IF;

      IF (vSizeUOM IS NOT NULL)
      THEN
         vPassFail := 0;
         vPassFail := CHECK_IS_BU_VALID (vTCCompanyId, vSizeUOMBUID);

         IF (vPassFail = 1)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (vTCCompanyId,
                                      vLaneId,
                                      vRGDtlSeq,
                                      vSizeUOM || ' is an invalid Size UOM ',
                                      4720130,
                                      vSizeUOM);
         ELSE
            vPassFail :=
               RG_SUB_VALIDATION_PKG.VALIDATE_SIZE_UOM (vSizeUOMBUID,
                                                        vSizeUOM);

            IF (vPassFail = 1)
            THEN
               vFailure := 1;
               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGDtlSeq,
                  vSizeUOM || ' is an invalid Size UOM ',
                  4720130,
                  vSizeUOM);
            ELSE
               IF (vSizeUOMId = 0)
               THEN
                  SELECT size_uom_id
                    INTO vSizeUOMId
                    FROM SIZE_UOM
                   WHERE SIZE_UOM = vSizeUOM AND tc_company_id = vSizeUOMBUID AND mark_for_deletion = 0;

                  UPDATE import_rg_lane_dtl
                     SET SIZE_UOM_ID = vSizeUOMId
                   WHERE     SIZE_UOM = vSizeUOM
                         AND tc_company_id = vTCCompanyId
                         AND lane_id = vLaneId
                         AND RG_LANE_DTL_SEQ = vRGDtlSeq;
               END IF;
            END IF;
         END IF;
      END IF;

      RETURN vFailure;
   END VALIDATE_DETAIL_BASE_DATA;

   /* End Vaibhav adding code*/

   FUNCTION GET_BUID_FROM_BUSINESS_UNIT (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vBUColumn         VARCHAR2)
      RETURN NUMBER
   IS
      vBUId    NUMBER;
      vCount   NUMBER;
   BEGIN
      SELECT COUNT (COMPANY_ID)
        INTO vCount
        FROM COMPANY
       WHERE COMPANY_NAME = vBUColumn;   -- and TC_COMPANY_ID = vTCCompanyId;

      IF (vCount > 0)
      THEN
         SELECT COMPANY_ID
           INTO vBUId
           FROM COMPANY
          WHERE COMPANY_NAME = vBUColumn; -- and TC_COMPANY_ID = vTCCompanyId;
      ELSE
         vBUId := vTCCompanyId;
      END IF;

      RETURN vBUId;
   END GET_BUID_FROM_BUSINESS_UNIT;

   FUNCTION VALIDATE_HIERARCHY (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vLaneId            IN rg_lane.LANE_ID%TYPE,
      oFacilityId        IN rg_lane.O_FACILITY_ID%TYPE,
      oFacilityAliasId   IN rg_lane.O_FACILITY_ALIAS_ID%TYPE,
      oCity              IN rg_lane.O_CITY%TYPE,
      oState             IN rg_lane.O_STATE_PROV%TYPE,
      oCounty            IN rg_lane.O_COUNTY%TYPE,
      oPostal            IN rg_lane.O_POSTAL_CODE%TYPE,
      oCountry           IN rg_lane.O_COUNTRY_CODE%TYPE,
      oZoneId            IN zone.ZONE_ID%TYPE,
      oZoneName          IN import_rg_lane.O_ZONE_NAME%TYPE,
      dFacilityId        IN rg_lane.D_FACILITY_ID%TYPE,
      dFacilityAliasId   IN rg_lane.D_FACILITY_ALIAS_ID%TYPE,
      dState             IN rg_lane.D_STATE_PROV%TYPE,
      dCity              IN rg_lane.D_CITY%TYPE,
      dCounty            IN rg_lane.D_COUNTY%TYPE,
      dCountry           IN rg_lane.D_COUNTRY_CODE%TYPE,
      dPostal            IN rg_lane.D_POSTAL_CODE%TYPE,
      dZoneId            IN zone.ZONE_ID%TYPE,
      dZoneName          IN import_rg_lane.D_ZONE_NAME%TYPE,
      routeTo		 IN import_rg_lane.ROUTE_TO%TYPE,
      routeType1	 IN import_rg_lane.ROUTE_TYPE_1%TYPE,
      routetype2	 IN import_rg_lane.ROUTE_TYPE_2%TYPE)
      RETURN NUMBER
   IS
      vPassFail        NUMBER;
      vDPassFail       NUMBER;
      vState           NUMBER;
      vOLocType        rating_lane.O_LOC_TYPE%TYPE;
      vOLocTypeLH      VARCHAR2 (5);
      vDLocType        rating_lane.D_LOC_TYPE%TYPE;
      vDLocTypeLH      VARCHAR2 (5);
      vOLocValue       rating_lane_hierarchy.ORIGIN_VALUE%TYPE;
      vDLocValue       rating_lane_hierarchy.DEST_VALUE%TYPE;
      vLHValue         NUMBER;
      vOLocError       error_message.ERROR_MSG_ID%TYPE;
      vDLocError       error_message.ERROR_MSG_ID%TYPE;
      vHierarchy       NUMBER;
      vOPostal         VARCHAR2 (5);
      vDPostal         VARCHAR2 (5);
      vAttributeType   NUMBER;
      vErrorMsg        VARCHAR2 (1000);
   BEGIN
      /* SL 05/21 Made changes as per the validation in the rating */

      vPassFail := 0;

      vDPassFail := 0;

      IF (oState IS NOT NULL AND oCountry IS NOT NULL)
         AND (    oCity IS NULL
              AND oPostal IS NULL
              AND oCounty IS NULL
              AND oFacilityAliasId IS NULL
              AND oZoneId IS NULL)
      THEN
         vOLocType := 'ST';
         vOLocTypeLH := 'ST';
      ELSIF (oCity IS NOT NULL AND oCountry IS NOT NULL)
            AND (    oPostal IS NULL
                 AND oFacilityAliasId IS NULL
                 AND oZoneId IS NULL)
      THEN
         SELECT HAS_STATE_PROV
           INTO vState
           FROM COUNTRY
          WHERE COUNTRY_CODE = oCountry;

         IF (vState = 1)
         THEN
            IF (oState IS NOT NULL)
            THEN
               vOLocType := 'CS';
               vOLocTypeLH := 'CS';
            ELSE                       /* oState is null but it is required */
               vPassFail := 1;
            END IF;
         ELSIF (oState IS NOT NULL)
         THEN
            vPassFail := 1;
         ELSE
            vOLocType := 'CS';
            vOLocTypeLH := 'CS';
         END IF;
      ELSIF (oPostal IS NOT NULL AND oCountry IS NOT NULL)
            AND (    oCity IS NULL
                 AND oState IS NULL
                 AND oCounty IS NULL
                 AND oFacilityAliasId IS NULL
                 AND oZoneId IS NULL)
      THEN
         SELECT DECODE (LENGTH (oPostal),
                        2, 'P2',
                        3, 'P3',
                        4, 'P4',
                        5, 'P5',
                        6, 'P6',
                        'Other')
           INTO vOPostal
           FROM DUAL;

         IF (vOPostal != 'Other')
         THEN
            vOLocType := vOPostal;
            vOLocTypeLH := vOPostal;
         ELSE
            vPassFail := 1;
         END IF;
      ELSIF (oZoneId IS NOT NULL)
            AND (    oCity IS NULL
                 AND oState IS NULL
                 AND oPostal IS NULL
                 AND oCounty IS NULL
                 AND oCountry IS NULL
                 AND oFacilityAliasId IS NULL)
      THEN
         vOLocType := 'ZN';

         SELECT COUNT (ATTRIBUTE_TYPE)
           INTO vAttributeType
           FROM ZONE
          WHERE TC_COMPANY_ID = vTCCompanyId AND ZONE_ID = oZoneId;

         IF (vAttributeType > 0)
         THEN
            SELECT DECODE (ATTRIBUTE_TYPE,
                           'ST', 'ZST',
                           'FC', 'ZFA',
                           'CT', 'ZCO',
                           'Z2', 'Z2',
                           'Z3', 'Z3',
                           'Z4', 'Z4',
                           'Z5', 'Z5',
                           'Z6', 'Z6',
                           'PR', 'PR',
                           'CI', 'ZCS',
                           'Other')
              INTO vOLocTypeLH
              FROM ZONE
             WHERE TC_COMPANY_ID = vTCCompanyId AND ZONE_ID = oZoneId;

            IF (vOLocTypeLH = 'Other')
            THEN
               vPassFail := 1;
            END IF;
         ELSE
            vPassFail := 1;
         END IF;
      ELSIF (oFacilityAliasId IS NOT NULL)
            AND (    oCity IS NULL
                 AND oState IS NULL
                 AND oPostal IS NULL
                 AND oCounty IS NULL
                 AND oCountry IS NULL
                 AND oZoneId IS NULL)
      THEN
         vOLocType := 'FA';
         vOLocTypeLH := 'FA';
      ELSIF (oCountry IS NOT NULL)
            AND (    oCity IS NULL
                 AND oState IS NULL
                 AND oPostal IS NULL
                 AND oCounty IS NULL
                 AND oFacilityAliasId IS NULL
                 AND oZoneId IS NULL)
      THEN
         vOLocType := 'CO';
         vOLocTypeLH := 'CO';
      ELSE
         vOLocError := 320000;
         vPassFail := 1;
      END IF;

      /*In case we missed an else case, we should check if the loc types have been determined*/
      IF (vOLocTypeLH IS NULL OR vOLocType IS NULL)
      THEN
         vPassFail := 1;
      END IF;

      IF (vPassFail = 1)
      THEN
         INSERT_RG_LANE_ERROR (vTCCompanyId,
                               vLaneId,
                               'Origin Definition is invalid',
                               4720018,
                               NULL);
         COMMIT;
      END IF;

      -- Destination validation

      IF (dState IS NOT NULL AND dCountry IS NOT NULL)
         AND (    dCity IS NULL
              AND dPostal IS NULL
              AND dCounty IS NULL
              AND dFacilityAliasId IS NULL
              AND dZoneId IS NULL)
      THEN
         vDLocType := 'ST';
         vDLocTypeLH := 'ST';
      ELSIF (dCity IS NOT NULL AND dCountry IS NOT NULL)
            AND (    dPostal IS NULL
                 AND dFacilityAliasId IS NULL
                 AND dZoneId IS NULL)
      THEN
         SELECT HAS_STATE_PROV
           INTO vState
           FROM COUNTRY
          WHERE COUNTRY_CODE = dCountry;

         IF (vState = 1)
         THEN
            IF (dState IS NOT NULL)
            THEN
               vDLocType := 'CS';
               vDLocTypeLH := 'CS';
            ELSE                       /* dState is null but it is required */
               vDPassFail := 1;
            END IF;
         ELSIF (dState IS NOT NULL)
         THEN
            vDPassFail := 1;
         ELSE
            vDLocType := 'CS';
            vDLocTypeLH := 'CS';
         END IF;
      ELSIF (dPostal IS NOT NULL AND dCountry IS NOT NULL)
            AND (    dCity IS NULL
                 AND dState IS NULL
                 AND dCounty IS NULL
                 AND dFacilityAliasId IS NULL
                 AND dZoneId IS NULL)
      THEN
         SELECT DECODE (LENGTH (dPostal),
                        2, 'P2',
                        3, 'P3',
                        4, 'P4',
                        5, 'P5',
                        6, 'P6',
                        'Other')
           INTO vDPostal
           FROM DUAL;

         IF (vDPostal != 'Other')
         THEN
            vDLocType := vDPostal;
            vDLocTypeLH := vDPostal;
         ELSE
            vDPassFail := 1;
         END IF;
      ELSIF (dZoneId IS NOT NULL)
            AND (    dCity IS NULL
                 AND dState IS NULL
                 AND dPostal IS NULL
                 AND dCounty IS NULL
                 AND dCountry IS NULL
                 AND dFacilityAliasId IS NULL)
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'DESTINATION IS ZONE !!!' || dZoneId || 'done');
         vDLocType := 'ZN';

         SELECT COUNT (ATTRIBUTE_TYPE)
           INTO vAttributeType
           FROM ZONE
          WHERE TC_COMPANY_ID = vTCCompanyId AND ZONE_ID = dZoneId;

         IF (vAttributeType > 0)
         THEN
            SELECT DECODE (ATTRIBUTE_TYPE,
                           'ST', 'ZST',
                           'FC', 'ZFA',
                           'CT', 'ZCO',
                           'Z2', 'Z2',
                           'Z3', 'Z3',
                           'Z4', 'Z4',
                           'Z5', 'Z5',
                           'Z6', 'Z6',
                           'PR', 'PR',
                           'CI', 'ZCS',
                           'Other')
              INTO vDLocTypeLH
              FROM ZONE
             WHERE TC_COMPANY_ID = vTCCompanyId AND ZONE_ID = dZoneId;

            IF (vDLocTypeLH = 'Other')
            THEN
               vDPassFail := 1;
            END IF;
         ELSE
            vDPassFail := 1;
         END IF;
      ELSIF (dFacilityAliasId IS NOT NULL)
            AND (    dCity IS NULL
                 AND dState IS NULL
                 AND dPostal IS NULL
                 AND dCounty IS NULL
                 AND dCountry IS NULL
                 AND dZoneId IS NULL)
      THEN
         vDLocType := 'FA';
         vDLocTypeLH := 'FA';
      ELSIF (dCountry IS NOT NULL)
            AND (    dCity IS NULL
                 AND dState IS NULL
                 AND dPostal IS NULL
                 AND dCounty IS NULL
                 AND dFacilityAliasId IS NULL
                 AND dZoneId IS NULL)
      THEN
         vDLocType := 'CO';
         vDLocTypeLH := 'CO';
      ELSE
         vDLocError := 320000;
         vDPassFail := 1;
      END IF;

      /*In case we missed an else case, we should check if the loc types have been determined*/
      IF (vDLocTypeLH IS NULL OR vDLocType IS NULL)
      THEN
         vDPassFail := 1;
      END IF;

      IF (vDPassFail = 1)
      THEN
         INSERT_RG_LANE_ERROR (vTCCompanyId,
                               vLaneId,
                               'Destination Definition is invalid',
                               4720019,
                               NULL);
         COMMIT;
      END IF;

      IF (vDPassFail = 1)
      THEN
         vPassFail := 1;
      END IF;

      IF (vPassFail = 0)
      THEN
         -- calculate lane hierarchy
         BEGIN
            SELECT ORIGIN_VALUE
              INTO vOLocValue
              FROM RG_LANE_HIERARCHY
             WHERE TC_COMPANY_ID = vTCCompanyId
                   AND LOCATION_TYPE = vOLocTypeLH;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               vOLocValue := NULL;
         END;

         BEGIN
            SELECT DEST_VALUE
              INTO vDLocValue
              FROM RG_LANE_HIERARCHY
             WHERE TC_COMPANY_ID = vTCCompanyId
                   AND LOCATION_TYPE = vDLocTypeLH;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               vDLocValue := NULL;
         END;

         IF (vOLocValue IS NOT NULL AND vDLocValue IS NOT NULL)
         THEN
            vLHValue := vOLocValue + vDLocValue;
         ELSE
            -- calculate default hierarchy
            SELECT DECODE (vOLocTypeLH,
                           'FA', 100,
                           'ZFA', 200,
                           'P6', 300,
                           'Z6', 400,
                           'P5', 500,
                           'Z5', 600,
                           'CS', 700,
                           'ZCS', 750,
                           'P4', 800,
                           'Z4', 900,
                           'P3', 1000,
                           'Z3', 1100,
                           'P2', 1200,
                           'Z2', 1300,
                           'ST', 1400,
                           'ZST', 1500,
                           'CO', 1600,
                           'ZCO', 1700,
                           'PR', 1800,
                           0)
              INTO vOLocValue
              FROM DUAL;

            SELECT DECODE (vDLocTypeLH,
                           'FA', 101,
                           'ZFA', 202,
                           'P6', 303,
                           'Z6', 404,
                           'P5', 505,
                           'Z5', 606,
                           'CS', 707,
                           'ZCS', 757,
                           'P4', 808,
                           'Z4', 909,
                           'P3', 1010,
                           'Z3', 1111,
                           'P2', 1212,
                           'Z2', 1313,
                           'ST', 1414,
                           'ZST', 1515,
                           'CO', 1616,
                           'ZCO', 1717,
                           'PR', 1818,
                           0)
              INTO vDLocValue
              FROM DUAL;

            IF (vOLocValue = 0 OR vDLocValue = 0)
            THEN
               vPassFail := 1;
               vLHValue := 0;
            ELSE
                vLHValue := vOLocValue + vDLocValue;
		IF (routeTo is null)
		THEN
		   vLHValue := vLHValue + 4000;
		END IF;

		IF(routeType1 is null)
		THEN
		   vLHValue := vLHValue + 4100;
		END IF;

		IF(routeType2 is null)
		THEN
		   vLHValue := vLHValue + 4200;
		END IF;

            END IF;
         END IF;

         -- update the entry in the database

         UPDATE IMPORT_RG_LANE
            SET LANE_HIERARCHY = vLHValue,
                O_LOC_TYPE = vOLocType,
                D_LOC_TYPE = vDLocType
          WHERE TC_COMPANY_ID = vTCCompanyId AND LANE_ID = vLaneId;

         IF (vPassFail = 1)
         THEN
            INSERT_RG_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               'An error prevented the Lane Hierarchy value from being calculated.',
               4720020,
               NULL);
         END IF;

         COMMIT;
      END IF;

      RETURN vPassFail;
   END VALIDATE_HIERARCHY;

   /* SL 05/29  Validate the RG Qualifier if present */

   FUNCTION VALIDATE_RG_QUALIFIER (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vLaneId        IN rg_lane.LANE_ID%TYPE,
      vRgQualifier   IN rg_lane.rg_qualifier%TYPE)
      RETURN NUMBER
   IS
      vParamValue   company_parameter.param_value%TYPE;
      vCount        NUMBER := 0;
      vPassFail     NUMBER := 0;
   BEGIN
      IF vRgQualifier IS NOT NULL
      THEN
         SELECT param_value
           INTO vParamValue
           FROM company_parameter
          WHERE     tc_company_id = vTCCompanyId
                AND param_def_id = 'RGQ'
                AND param_group_id = 'RG';

         IF vParamValue = 'EQ'
         THEN
            SELECT COUNT (*)
              INTO vCount
              FROM equipment
             /*   WHERE tc_company_id = vTCCompanyId
                  AND equipment_code = vRgQualifier; */
             WHERE equipment_id = TO_NUMBER (vRgQualifier);
         ELSIF vParamValue = 'SL'
         THEN
            SELECT COUNT (*)
              INTO vCount
              FROM service_level
             /* WHERE tc_company_id = vTCCompanyId
                      AND service_level = vRgQualifier; */
             WHERE service_level_id = TO_NUMBER (vRgQualifier);
         ELSIF vParamValue = 'MOT'
         THEN
            SELECT COUNT (*)
              INTO vCount
              FROM mot
             /*  WHERE tc_company_id = vTCCompanyId
                      AND mot = vRgQualifier; */
             WHERE mot_id = TO_NUMBER (vRgQualifier);
         ELSIF vParamValue = 'PL'
         THEN
            SELECT COUNT (*)
              INTO vCount
              FROM protection_level
             /* WHERE tc_company_id = vTCCompanyId
                 AND protection_level = vRgQualifier; */
             WHERE protection_level_id = TO_NUMBER (vRgQualifier);
         ELSIF vParamValue = 'BM'
         THEN
            SELECT COUNT (*)
              INTO vCount
              FROM billing_method
             WHERE billing_method = vRgQualifier;
         END IF;

         IF vCount = 0
         THEN
            vPassFail := 1;

            INSERT_RG_LANE_ERROR (
               vTCCompanyId,
               vLaneId,
               'Routing Guide Qualifier is not appropriate',
               4720021,
               NULL);
         END IF;
      END IF;

      RETURN vPassFail;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         INSERT_RG_LANE_ERROR (
            vTCCompanyId,
            vLaneId,
            'Routing Guide Qualifier is not in the company parameter table',
            4720022,
            NULL);

         RETURN 1;
   END VALIDATE_RG_QUALIFIER;

   FUNCTION VALIDATE_TP_EQUIP_SERV_MODE (
      vTCCompanyId           IN company.COMPANY_ID%TYPE,
      vLaneId                IN rg_lane.LANE_ID%TYPE,
      vRGDtlSeq              IN rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      vCarrierCode           IN carrier_code.CARRIER_CODE%TYPE,
      vEquipment             IN equipment.EQUIPMENT_CODE%TYPE,
      vServiceLevel          IN service_level.SERVICE_LEVEL%TYPE,
      vMode                  IN mot.MOT%TYPE,
      p_scndr_carrier_code   IN CARRIER_CODE.CARRIER_CODE%TYPE,
      vRepTpFlag             IN rg_lane_dtl.rep_tp_flag%TYPE)
      RETURN NUMBER
   IS
      vPassFail            NUMBER;
      vCount               NUMBER;
      feacarr              VARCHAR2 (100);

      vCarrierId           CARRIER_CODE.CARRIER_ID%TYPE;
      p_Scndr_Carrier_Id   CARRIER_CODE.CARRIER_ID%TYPE;
   BEGIN
      /*

         S.Lal  05/21/02

         If the carrier code is null (budgeted carrier) then no Validation is required.

      */

      vPassFail := 0;

      IF (vCarrierCode IS NOT NULL)
      THEN
         SELECT carrier_id, SCNDR_CARRIER_ID
           INTO vCarrierId, p_Scndr_Carrier_Id
           FROM IMPORT_RG_LANE_DTL
          WHERE lane_id = vLaneId AND RG_LANE_DTL_SEQ = vRGDtlSeq;

         feacarr := vCarrierCode;

         IF (p_scndr_carrier_code IS NOT NULL)
         THEN
            feacarr := vCarrierCode || ' or ' || p_scndr_carrier_code;
         END IF;

         IF (vEquipment IS NOT NULL)
         THEN
            SELECT COUNT (1)
              INTO vCount
              FROM TP_COMPANY_EQUIPMENT TCE, carrier_code cc, equipment eqp
             WHERE                          --TCE.TC_COMPANY_ID = vTCCompanyID
                       --and
                       TCE.carrier_id = cc.carrier_id
                   AND cc.CARRIER_ID IN (vCarrierId, p_Scndr_Carrier_Id)
                   AND eqp.EQUIPMENT_CODE = vEquipment
                   AND eqp.equipment_id = tce.equipment_id;

            IF (vCount < 1)
            THEN
               vPassFail := 1;

               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGDtlSeq,
                     'Carrier ( '
                  || feacarr
                  || ' ) and Equipment ( '
                  || vEquipment
                  || ' ) is not feasible ',
                  2460218,
                     feacarr
                  || '<sep>'
                  || vEquipment);
            END IF;
         END IF;

         IF (vServiceLevel IS NOT NULL)
         THEN
            SELECT COUNT (1)
              INTO vCount
              FROM TP_COMPANY_SERVICE_LEVEL TCS,
                   carrier_code cc,
                   SERVICE_LEVEL sl
             WHERE                          --TCS.TC_COMPANY_ID = vTCCompanyID
                       --and
                       cc.CARRIER_ID IN (vCarrierId, p_Scndr_Carrier_Id)
                   AND TCS.carrier_id = cc.carrier_id
                   AND sl.SERVICE_LEVEL = vServiceLevel
                   AND TCS.service_level_id = sl.service_level_id;

            IF (vCount < 1)
            THEN
               vPassFail := 1;

               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGDtlSeq,
                     'Carrier ( '
                  || feacarr
                  || ' ) and  ServiceLevel ( '
                  || vServiceLevel
                  || ' ) is not feasible ',
                  2460219,
                     feacarr
                  || '<sep>'
                  || vServiceLevel);
            END IF;
         END IF;

         IF (vMode IS NOT NULL)
         THEN
            SELECT COUNT (1)
              INTO vCount
              FROM CARRIER_CODE_MOT ccm, carrier_code cc, mot mt
             WHERE                          --ccm.TC_COMPANY_ID = vTCCompanyID
                       --and
                       cc.CARRIER_ID IN (vCarrierId, p_Scndr_Carrier_Id)
                   AND ccm.carrier_id = cc.carrier_id
                   AND mt.MOT = vMode
                   AND ccm.mot_id = mt.mot_id;

            IF (vCount < 1)
            THEN
               vPassFail := 1;

               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGDtlSeq,
                     'Carrier ( '
                  || feacarr
                  || ' ) and MOT( '
                  || vMode
                  || ' ) is not feasible',
                  2460220,
                     feacarr
                  || '<sep>'
                  || vMode);
            END IF;
         END IF;
      END IF;

      RETURN vPassFail;
   END VALIDATE_TP_EQUIP_SERV_MODE;

   FUNCTION VALIDATE_FEASIBLE (
      vTCCompanyId               IN company.COMPANY_ID%TYPE,
      vLaneId                    IN rg_lane.LANE_ID%TYPE,
      vRgDtlSeq                  IN rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      vCarrierCode               IN carrier_code.CARRIER_CODE%TYPE,
      vCarrierCodeBUID           IN company.COMPANY_ID%TYPE,
      vEquipment                 IN equipment.EQUIPMENT_CODE%TYPE,
      vEquipmentBUID             IN company.COMPANY_ID%TYPE,
      vProtectionLevel           IN protection_level.PROTECTION_LEVEL%TYPE,
      vProtectionLevelBUID       IN company.COMPANY_ID%TYPE,
      p_scndr_carrier_code       IN CARRIER_CODE.CARRIER_CODE%TYPE,
      p_scndr_carrier_codeBUID   IN company.COMPANY_ID%TYPE)
      RETURN NUMBER
   IS
      vCount               NUMBER;
      vPassFail            NUMBER;
      vCarrierId           NUMBER;
      vEquipmentId         NUMBER;
      vProtectionLevelId   NUMBER;
      p_scndr_carrierId    NUMBER;
   BEGIN
      vCount := 0;
      vPassFail := 0;

      IF (vProtectionLevel IS NOT NULL)
      THEN
         SELECT COUNT (protection_Level_Id)
           INTO vCount
           FROM Protection_Level
          WHERE tc_company_id = vProtectionLevelBUID
                AND Protection_Level = vProtectionLevel
				AND mark_for_deletion = 0;
      END IF;

      IF (vCount > 0)
      THEN
         SELECT protection_Level_Id
           INTO vProtectionLevelId
           FROM Protection_Level
          WHERE tc_company_id = vProtectionLevelBUID
                AND Protection_Level = vProtectionLevel
				AND mark_for_deletion = 0;
      END IF;

      vCount := 0;

      IF (vEquipment IS NOT NULL)
      THEN
         SELECT COUNT (equipment_Id)
           INTO vCount
           FROM equipment
          WHERE tc_company_id = vEquipmentBUID
                AND equipment_code = vEquipment
				AND mark_for_deletion = 0;
      END IF;

      IF (vCount > 0)
      THEN
         SELECT equipment_Id
           INTO vEquipmentId
           FROM equipment
          WHERE tc_company_id = vEquipmentBUID
                AND equipment_code = vEquipment
				AND mark_for_deletion = 0;
      END IF;

      vCount := 0;

      IF (vCarrierCode IS NOT NULL)
      THEN
         SELECT COUNT (carrier_id)
           INTO vCount
           FROM carrier_code
          WHERE tc_company_id = vCarrierCodeBUID
                AND carrier_code = vCarrierCode
				AND mark_for_deletion = 0;
      END IF;

      IF (vCount > 0)
      THEN
         SELECT carrier_id
           INTO vCarrierId
           FROM carrier_code
          WHERE tc_company_id = vCarrierCodeBUID
                AND carrier_code = vCarrierCode
				AND mark_for_deletion = 0;
      END IF;

      vCount := 0;

      IF (p_scndr_carrier_code IS NOT NULL)
      THEN
         SELECT COUNT (carrier_id)
           INTO vCount
           FROM carrier_code
          WHERE tc_company_id = p_scndr_carrier_codeBUID
                AND carrier_code = p_scndr_carrier_code
				AND mark_for_deletion = 0;
      END IF;

      IF (vCount > 0)
      THEN
         SELECT carrier_id
           INTO p_scndr_carrierId
           FROM carrier_code
          WHERE tc_company_id = p_scndr_carrier_codeBUID
                AND carrier_code = p_scndr_carrier_code
				AND mark_for_deletion = 0;
      END IF;

      IF (vProtectionLevel IS NOT NULL)
      THEN
         SELECT COUNT (INFEASIBILITY_ID)
           INTO vCount
           FROM INFEASIBILITY
          WHERE     INFEASIBILITY_TYPE = 'PLEQ'
                AND TC_COMPANY_ID = vTCCompanyId
                AND value1 = TO_CHAR (vProtectionLevelId)
                AND VALUE2 = TO_CHAR (vEquipmentId);

         IF (vCount > 0)
         THEN
            vCount := 0;

            SELECT COUNT (INFEASIBILITY_ID)
              INTO vCount
              FROM INFEASIBILITY
             WHERE     INFEASIBILITY_TYPE = 'FAPLEQ'
                   AND TC_COMPANY_ID = vTCCompanyId
                   AND VALUE1 = (SELECT O_FACILITY_ID
                                   FROM IMPORT_RG_LANE
                                  WHERE LANE_ID = vLaneId)
                   AND VALUE2 = TO_CHAR (vProtectionLevelId)
                   AND VALUE3 = TO_CHAR (vEquipmentId);

            IF (vCount < 1)
            THEN
               vPassFail := 1;
            END IF;
         ELSE
            /* S.Lal 05/21/02  If no data found in the infeasible table, it is valid */

            vPassFail := 0;
         END IF;

         IF (vPassFail = 1)
         THEN
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGDtlSeq,
                  vProtectionLevel
               || ' is infeasible'
               || ' with equipment type '
               || vEquipment,
               2460223,
               vProtectionLevel || '<sep>' || vEquipment);
         END IF;
      END IF;

      SELECT COUNT (INFEASIBILITY_ID)
        INTO vCount
        FROM INFEASIBILITY
       WHERE     INFEASIBILITY_TYPE = 'FAEQ'
             AND VALUE1 = (SELECT O_FACILITY_ID
                             FROM IMPORT_RG_LANE
                            WHERE LANE_ID = vLaneId)
             AND VALUE2 = TO_CHAR (vEquipmentId)
             AND TC_COMPANY_ID = vTCCompanyId;

      IF (vCount > 0)
      THEN
         vPassFail := 1;

         INSERT_RG_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRGDtlSeq,
            'Origin Facility is infeasible with equipment ' || vEquipment,
            4720044,
            vEquipment);
      END IF;

      SELECT COUNT (INFEASIBILITY_ID)
        INTO vCount
        FROM INFEASIBILITY
       WHERE     INFEASIBILITY_TYPE = 'FAEQ'
             AND VALUE1 = (SELECT D_FACILITY_ID
                             FROM IMPORT_RG_LANE
                            WHERE LANE_ID = vLaneId)
             AND VALUE2 = TO_CHAR (vEquipmentId)
             AND TC_COMPANY_ID = vTCCompanyId;

      IF (vCount > 0)
      THEN
         vPassFail := 1;

         INSERT_RG_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRGDtlSeq,
            'Destination Facility is infeasible with equipment '
            || vEquipment,
            4720045,
            vEquipment);
      END IF;

      RETURN vPassFail;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE ('Procedure VALIDATE_FEASIBLE: ' || SQLERRM);
         RAISE;
   END VALIDATE_FEASIBLE;

   FUNCTION VALIDATE_FAC_CARR_FEASIBLE (
      vTCCompanyId               IN company.COMPANY_ID%TYPE,
      vLaneId                    IN rg_lane.LANE_ID%TYPE,
      vRgDtlSeq                  IN rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      vCarrierCode               IN carrier_code.CARRIER_CODE%TYPE,
      vCarrierCodeBUID           IN company.COMPANY_ID%TYPE,
      p_scndr_carrier_code       IN CARRIER_CODE.CARRIER_CODE%TYPE,
      p_scndr_carrier_codeBUID   IN company.COMPANY_ID%TYPE)
      RETURN NUMBER
   IS
      vCount              NUMBER;
      vPassFail           NUMBER;
      vCarrierId          NUMBER;
      p_scndr_carrierId   NUMBER;
      vBUId               NUMBER;
      vInfeasibilitySQL   VARCHAR2 (20000);
      vBuList             VARCHAR2 (10000);
      errorMsg            VARCHAR2 (1000);
   BEGIN
      SELECT COUNT (*)
        INTO vCount
        FROM carrier_code
       WHERE tc_company_id = vCarrierCodeBUID AND carrier_code = vCarrierCode AND mark_for_deletion = 0;

      IF (vCount > 0)
      THEN
         SELECT carrier_id
           INTO vCarrierId
           FROM carrier_code
          WHERE tc_company_id = vCarrierCodeBUID
                AND carrier_code = vCarrierCode
				AND mark_for_deletion = 0;
      END IF;

      SELECT COUNT (*)
        INTO vCount
        FROM carrier_code
       WHERE tc_company_id = p_scndr_carrier_codeBUID
             AND carrier_code = p_scndr_carrier_code
			 AND mark_for_deletion = 0;

      IF (vCount > 0)
      THEN
         SELECT carrier_id
           INTO p_scndr_carrierId
           FROM carrier_code
          WHERE tc_company_id = p_scndr_carrier_codeBUID
                AND carrier_code = p_scndr_carrier_code
				AND mark_for_deletion = 0;
      END IF;

      IF (p_scndr_carrier_code != NULL)
      THEN
         errorMsg :=
               'is infeasible with carriers '
            || vCarrierCode
            || ' or '
            || p_scndr_carrier_code;
      ELSE
         errorMsg := 'is infeasible with carrier ' || vCarrierCode;
      END IF;

      /*   -- Get the Parent BU IDS as comma separated string
         vBUList := GET_PARENT_BU_IDS(vTCCompanyId);
         IF vBUList IS NOT NULL THEN
             BEGIN
             vInfeasibilitySQL := 'SELECT count(INFEASIBILITY_ID) ';
             vInfeasibilitySQL :=  vInfeasibilitySQL || ' FROM INFEASIBILITY ';
             vInfeasibilitySQL :=  vInfeasibilitySQL || ' WHERE INFEASIBILITY_TYPE = ''FATP'' ';
             vInfeasibilitySQL :=  vInfeasibilitySQL || ' AND VALUE1 = (SELECT FACILITY_ID ';
             vInfeasibilitySQL :=  vInfeasibilitySQL || ' FROM FACILITY_ALIAS ';
             vInfeasibilitySQL :=  vInfeasibilitySQL || ' WHERE FACILITY_ALIAS_ID = (SELECT O_FACILITY_ALIAS_ID ';
             vInfeasibilitySQL :=  vInfeasibilitySQL || ' FROM IMPORT_RG_LANE ';
             vInfeasibilitySQL :=  vInfeasibilitySQL || ' WHERE LANE_ID = '|| vLaneId || ' ';
             vInfeasibilitySQL :=  vInfeasibilitySQL || ' AND TC_COMPANY_ID = '|| vTCCompanyId || '  ) ';
             vInfeasibilitySQL :=  vInfeasibilitySQL || ' AND TC_COMPANY_ID IN ('|| vBUList || ')) ';
             vInfeasibilitySQL :=  vInfeasibilitySQL || ' AND value2 IN (''' || to_char(vCarrierId) || '''';
             IF p_scndr_carrierId IS NOT NULL THEN
                vInfeasibilitySQL :=  vInfeasibilitySQL || ' , ''' || to_char(p_scndr_carrierId) || '''';
             END IF;
             vInfeasibilitySQL :=  vInfeasibilitySQL || ' ) ';
             execute immediate vInfeasibilitySQL into vCount;
             END;
         ELSE */
      SELECT COUNT (INFEASIBILITY_ID)
        INTO vCount
        FROM INFEASIBILITY
       WHERE INFEASIBILITY_TYPE = 'FATP'
             AND VALUE1 = (SELECT O_FACILITY_ID
                             FROM IMPORT_RG_LANE
                            WHERE LANE_ID = vLaneId)
             AND value2 IN
                    (TO_CHAR (vCarrierId), TO_CHAR (p_scndr_carrierId))
             AND TC_COMPANY_ID = vTCCompanyId;

      --    END IF;
      IF (vCount > 0)
      THEN
         vPassFail := 1;
		 
		 IF (p_scndr_carrier_code != NULL)
		  THEN
			 INSERT_RG_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRGDtlSeq,
            'Origin Facility ' || errorMsg,
            4720048,
            vCarrierCode || '<sep>' || p_scndr_carrier_code);
		  ELSE
			 INSERT_RG_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRGDtlSeq,
            'Origin Facility ' || errorMsg,
            2460221,
            vCarrierCode);
		  END IF;

      END IF;

      /*        IF vBUList IS NOT NULL THEN
                  vInfeasibilitySQL := 'SELECT count(INFEASIBILITY_ID) ';
                  vInfeasibilitySQL :=  vInfeasibilitySQL || ' FROM INFEASIBILITY ';
                  vInfeasibilitySQL :=  vInfeasibilitySQL || ' WHERE INFEASIBILITY_TYPE = ''FATP'' ';
                  vInfeasibilitySQL :=  vInfeasibilitySQL || ' AND VALUE1 = (SELECT FACILITY_ID ';
                  vInfeasibilitySQL :=  vInfeasibilitySQL || ' FROM FACILITY_ALIAS ';
                  vInfeasibilitySQL :=  vInfeasibilitySQL || ' WHERE FACILITY_ALIAS_ID = (SELECT D_FACILITY_ALIAS_ID ';
                  vInfeasibilitySQL :=  vInfeasibilitySQL || ' FROM IMPORT_RG_LANE ';
                  vInfeasibilitySQL :=  vInfeasibilitySQL || ' WHERE LANE_ID = '|| vLaneId || ' ';
                  vInfeasibilitySQL :=  vInfeasibilitySQL || ' AND TC_COMPANY_ID = '|| vTCCompanyId || '  ) ';
                  vInfeasibilitySQL :=  vInfeasibilitySQL || ' AND TC_COMPANY_ID IN ('|| vBUList || ')) ';
                  vInfeasibilitySQL :=  vInfeasibilitySQL || ' AND value2 IN (''' || to_char(vCarrierId) || '''';
                  IF p_scndr_carrierId IS NOT NULL THEN
                     vInfeasibilitySQL :=  vInfeasibilitySQL || ' , ''' || to_char(p_scndr_carrierId) || '''';
                  END IF;
                  vInfeasibilitySQL :=  vInfeasibilitySQL || ' ) ';
                  execute immediate vInfeasibilitySQL into vCount;
              ELSE */
      SELECT COUNT (INFEASIBILITY_ID)
        INTO vCount
        FROM INFEASIBILITY
       WHERE INFEASIBILITY_TYPE = 'FATP'
             AND VALUE1 = (SELECT D_FACILITY_ID
                             FROM IMPORT_RG_LANE
                            WHERE LANE_ID = vLaneId)
             AND value2 IN
                    (TO_CHAR (vCarrierId), TO_CHAR (p_scndr_carrierId))
             AND TC_COMPANY_ID = vTCCompanyId;

      -- END IF;

      IF (vCount > 0)
      THEN
         vPassFail := 1;
		 
		 IF (p_scndr_carrier_code != NULL)
		  THEN
			 INSERT_RG_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRGDtlSeq,
            'Destination Facility ' || errorMsg,
            4720049,
            vCarrierCode || '<sep>' || p_scndr_carrier_code);
		  ELSE
			 INSERT_RG_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRGDtlSeq,
            'Destination Facility ' || errorMsg,
            2460222,
            vCarrierCode);
		  END IF;
		  
      END IF;

      RETURN vPassFail;
   END VALIDATE_FAC_CARR_FEASIBLE;

   FUNCTION VALIDATE_CAPACITY_COMMITMENT (
      vTCCompanyId    IN company.COMPANY_ID%TYPE,
      vLaneId         IN import_rg_lane.LANE_ID%TYPE,
      vRGLaneDtlSeq   IN import_rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      vEffectiveDt    IN import_rg_lane_dtl.effective_dt%TYPE,
      vExpirationDt   IN import_rg_lane_dtl.expiration_dt%TYPE)
      RETURN NUMBER
   IS
      vPassFail               NUMBER;
      vFailure                NUMBER;
	  vReturn1					NUMBER;
      vFrequency              import_rg_lane.FREQUENCY%TYPE;
      vYearlyCapacity         import_rg_lane_dtl.YEARLY_CAPACITY%TYPE;
      vYearlyCommitment       import_rg_lane_dtl.YEARLY_COMMITMENT%TYPE;
      vMonthlyCapacity        import_rg_lane_dtl.MONTHLY_CAPACITY%TYPE;
      vMonthlyCommitment      import_rg_lane_dtl.MONTHLY_COMMITMENT%TYPE;
      vWeeklyCapacity         import_rg_lane_dtl.WEEKLY_CAPACITY%TYPE;
      vWeeklyCommitment       import_rg_lane_dtl.WEEKLY_COMMITMENT%TYPE;
      vDailyCapacity          import_rg_lane_dtl.DAILY_CAPACITY%TYPE;
      vDailyCommitment        import_rg_lane_dtl.DAILY_COMMITMENT%TYPE;
      vDowCapacitySun         import_rg_lane_dtl.capacity_sun%TYPE;
      vDowCapacityMon         import_rg_lane_dtl.capacity_mon%TYPE;
      vDowCapacityTue         import_rg_lane_dtl.capacity_tue%TYPE;
      vDowCapacityWed         import_rg_lane_dtl.capacity_wed%TYPE;
      vDowCapacityThu         import_rg_lane_dtl.capacity_thu%TYPE;
      vDowCapacityFri         import_rg_lane_dtl.capacity_fri%TYPE;
      vDowCapacitySat         import_rg_lane_dtl.capacity_sat%TYPE;
      vDOWCapacity            NUMBER (8);
      vDowCommitmentSun       import_rg_lane_dtl.commitment_sun%TYPE;
      vDowCommitmentMon       import_rg_lane_dtl.commitment_mon%TYPE;
      vDowCommitmentTue       import_rg_lane_dtl.commitment_tue%TYPE;
      vDowCommitmentWed       import_rg_lane_dtl.commitment_wed%TYPE;
      vDowCommitmentThu       import_rg_lane_dtl.commitment_thu%TYPE;
      vDowCommitmentFri       import_rg_lane_dtl.commitment_fri%TYPE;
      vDowCommitmentSat       import_rg_lane_dtl.commitment_sat%TYPE;
      vYearlyCommitPCT        import_rg_lane_dtl.YEARLY_COMMIT_PCT%TYPE;
      vMonthlyCommitPCT       import_rg_lane_dtl.MONTHLY_COMMIT_PCT%TYPE;
      vWeeklyCommitPCT        import_rg_lane_dtl.WEEKLY_COMMIT_PCT%TYPE;
      vDailyCommitPCT         import_rg_lane_dtl.DAILY_COMMIT_PCT%TYPE;
      vDowCommitPctSun        import_rg_lane_dtl.commit_pct_sun%TYPE;
      vDowCommitPctMon        import_rg_lane_dtl.commit_pct_mon%TYPE;
      vDowCommitPctTue        import_rg_lane_dtl.commit_pct_tue%TYPE;
      vDowCommitPctWed        import_rg_lane_dtl.commit_pct_wed%TYPE;
      vDowCommitPctThu        import_rg_lane_dtl.commit_pct_thu%TYPE;
      vDowCommitPctFri        import_rg_lane_dtl.commit_pct_fri%TYPE;
      vDowCommitPctSat        import_rg_lane_dtl.commit_pct_sat%TYPE;
      vYearlySurgeCapacity    import_surge_capacity.YEARLY_SURGE_CAPACITY%TYPE;
      vMonthlySurgeCapacity   import_surge_capacity.MONTHLY_SURGE_CAPACITY%TYPE;
      vWeeklySurgeCapacity    import_surge_capacity.WEEKLY_SURGE_CAPACITY%TYPE;
      vDailySurgeCapacity     import_surge_capacity.DAILY_SURGE_CAPACITY%TYPE;
      vDowSurgeCapacitySun    import_surge_capacity.surge_capacity_sun%TYPE;
      vDowSurgeCapacityMon    import_surge_capacity.surge_capacity_mon%TYPE;
      vDowSurgeCapacityTue    import_surge_capacity.surge_capacity_tue%TYPE;
      vDowSurgeCapacityWed    import_surge_capacity.surge_capacity_wed%TYPE;
      vDowSurgeCapacityThu    import_surge_capacity.surge_capacity_thu%TYPE;
      vDowSurgeCapacityFri    import_surge_capacity.surge_capacity_fri%TYPE;
      vDowSurgeCapacitySat    import_surge_capacity.surge_capacity_sat%TYPE;
	vEffectiveDtSurgeCapacity import_surge_capacity.EFFECTIVE_DT%TYPE;
	vExpirationDtSurgeCapacity import_surge_capacity.EXPIRATION_DT%TYPE;
      vTrackCommitment        NUMBER (1);
      vSurgeCount             NUMBER;
      vMot                    import_rating_lane_dtl.MOT%TYPE;
	  v_stmt				  VARCHAR2 (4000);
	  surge_capacity_cursor	  ref_curtype;

   BEGIN
      /*  SL 05/28/02

          Made changes as per Use Case: 1508 - Import Routing Guide

      */

      DBMS_OUTPUT.PUT_LINE ('Procedure Validating Capacity Commitment');

		v_stmt :='Select EFFECTIVE_DT,'||
      'EXPIRATION_DT,'||
      'NVL(YEARLY_SURGE_CAPACITY,0) as YEARLY_SURGE_CAPACITY,'||
  		'NVL(MONTHLY_SURGE_CAPACITY,0) as MONTHLY_SURGE_CAPACITY,'||
  		'NVL(WEEKLY_SURGE_CAPACITY,0) as WEEKLY_SURGE_CAPACITY,'||
      'NVL(DAILY_SURGE_CAPACITY, 0) AS DAILY_SUREG_CAPACITY,'||
      'NVL(SURGE_CAPACITY_SUN, 0) AS SURGE_CAPACITY_SUN ,'||
      'NVL(SURGE_CAPACITY_MON, 0) AS SURGE_CAPACITY_MON,'||
      'NVL(SURGE_CAPACITY_TUE, 0) AS SURGE_CAPACITY_TUE,'||
      'NVL(SURGE_CAPACITY_WED, 0) AS SURGE_CAPACITY_WED,'||
      'NVL(SURGE_CAPACITY_THU, 0) AS SURGE_CAPACITY_THU,'||
      'NVL(SURGE_CAPACITY_FRI, 0) AS SURGE_CAPACITY_FRI,'||
      'NVL(SURGE_CAPACITY_SAT, 0) AS SURGE_CAPACITY_SAT '||
    'FROM import_surge_capacity '||
    'WHERE TC_COMPANY_ID = : vTCCompanyId '||
            'AND LANE_ID = :vLaneId '||
            'AND RG_LANE_DTL_SEQ = :vRGLaneDtlSeq';


      vFailure := 0;

      SELECT FREQUENCY
        INTO vFrequency
        FROM IMPORT_RG_LANE
       WHERE TC_COMPANY_ID = vTCCompanyId AND LANE_ID = vLaneId;
    
    Begin
      SELECT MOT,
             YEARLY_CAPACITY,
             YEARLY_COMMITMENT,
             MONTHLY_CAPACITY,
             MONTHLY_COMMITMENT,
             WEEKLY_CAPACITY,
             WEEKLY_COMMITMENT,
             DAILY_CAPACITY,
             DAILY_COMMITMENT,
             CAPACITY_SUN,
             CAPACITY_MON,
             CAPACITY_TUE,
             CAPACITY_WED,
             CAPACITY_THU,
             CAPACITY_FRI,
             CAPACITY_SAT,
             COMMITMENT_SUN,
             COMMITMENT_MON,
             COMMITMENT_TUE,
             COMMITMENT_WED,
             COMMITMENT_THU,
             COMMITMENT_FRI,
             COMMITMENT_SAT,
             YEARLY_COMMIT_PCT,
             MONTHLY_COMMIT_PCT,
             WEEKLY_COMMIT_PCT,
             DAILY_COMMIT_PCT,
             COMMIT_PCT_SUN,
             COMMIT_PCT_MON,
             COMMIT_PCT_TUE,
             COMMIT_PCT_WED,
             COMMIT_PCT_THU,
             COMMIT_PCT_FRI,
             COMMIT_PCT_SAT
        INTO vMot,
             vYearlyCapacity,
             vYearlyCommitment,
             vMonthlyCapacity,
             vMonthlyCommitment,
             vWeeklyCapacity,
             vWeeklyCommitment,
             vDailyCapacity,
             vDailyCommitment,
             vDowCapacitySun,
             vDowCapacityMon,
             vDowCapacityTue,
             vDowCapacityWed,
             vDowCapacityThu,
             vDowCapacityFri,
             vDowCapacitySat,
             vDowCommitmentSun,
             vDowCommitmentMon,
             vDowCommitmentTue,
             vDowCommitmentWed,
             vDowCommitmentThu,
             vDowCommitmentFri,
             vDowCommitmentSat,
             vYearlyCommitPCT,
             vMonthlyCommitPCT,
             vWeeklyCommitPCT,
             vDailyCommitPCT,
             vDowCommitPctSun,
             vDowCommitPctMon,
             vDowCommitPctTue,
             vDowCommitPctWed,
             vDowCommitPctThu,
             vDowCommitPctFri,
             vDowCommitPctSat
        FROM IMPORT_RG_LANE_DTL irld
       WHERE irld.TC_COMPANY_ID = vTCCompanyId
             AND LANE_ID = vLaneId
             AND RG_LANE_DTL_SEQ = vRGLaneDtlSeq;
     Exception
      When No_Data_Found Then
          DBMS_OUTPUT.PUT_LINE ('Procedure Validating Capacity Commitment: Excpetion:   No_Data_Found');
      End;
If (Vmot Is Not Null) Then
  Begin
   SELECT Track_Commitment into vTrackCommitment from MOT where Mot = Vmot AND TC_COMPANY_ID=vTCCompanyId;
   Exception
      When No_Data_Found Then
          Dbms_Output.Put_Line ('Mot does not exist.');
      End;  
END IF;
IF (vFrequency = 'Y')   THEN
	IF ( (vYearlyCapacity IS NOT NULL AND vYearlyCommitment IS NOT NULL) AND (vYearlyCapacity >= 0))THEN
		IF (vMot IS NOT NULL AND vTrackCommitment = 0)THEN
			IF (vYearlyCommitment != 0 OR vYearlyCommitPCT != 0)THEN
				vFailure := 1;
				INSERT_RG_LANE_DTL_ERROR (
					vTCCompanyId,
					vLaneId,
					vRGLaneDtlSeq,
					'Yearly Commitment or Yearly Commitment Pct is not zero for the given mode/frequency.',
					4720067,
					NULL);
			ELSE
				vPassFail := 0;
			END IF;
		END IF;
		IF (vYearlyCapacity >= vYearlyCommitment) AND (vYearlyCommitPCT >= 0)THEN
			vPassFail := 0;
		ELSE
			vFailure := 1;

			IF vYearlyCapacity < vYearlyCommitment THEN

				INSERT_RG_LANE_DTL_ERROR (
					vTCCompanyId,
					vLaneId,
					vRGLaneDtlSeq,
					'Yearly Capacity must be greater than or equal to Yearly Commitment for the given frequency',
					4720068,
					NULL);
			END IF;

			IF vYearlyCommitPCT < 0  THEN
				INSERT_RG_LANE_DTL_ERROR (
					vTCCompanyId,
					vLaneId,
					vRGLaneDtlSeq,
					'Yearly Commitment % should be greater than or equal to zero',
					4720069,
					NULL);
			END IF;
		END IF;
	ELSE
		vFailure := 1;

		IF ( (vYearlyCapacity IS NULL OR vYearlyCommitment IS NULL) OR (vYearlyCapacity <= 0)) THEN
			INSERT_RG_LANE_DTL_ERROR (
				vTCCompanyId,
				vLaneId,
				vRGLaneDtlSeq,
				'Yearly Commitment or Yearly Capacity is null or 0 for given frequency.',
				4720071,
				NULL);
		END IF;
	END IF;
ELSIF (vFrequency = 'M') THEN
	IF ( (vMonthlyCapacity IS NOT NULL AND vMonthlyCommitment IS NOT NULL) AND (vMonthlyCapacity >= 0)) THEN
		IF (vMot IS NOT NULL AND vTrackCommitment = 0)THEN
			IF (vMonthlyCommitment != 0 OR vMonthlyCommitPCT != 0) THEN
				vFailure := 1;
				INSERT_RG_LANE_DTL_ERROR (
					vTCCompanyId,
					vLaneId,
					vRGLaneDtlSeq,
					'Monthly Commitment or Monthly Commitment Pct is not zero for the given mode/frequency.',
					4720072,
					NULL);
			ELSE
				vPassFail := 0;
			END IF;
		END IF;

		IF (vMonthlyCapacity >= vMonthlyCommitment)AND (vMonthlyCommitPCT >= 0)THEN
			vPassFail := 0;
		ELSE
			vFailure := 1;

			IF vMonthlyCapacity < vMonthlyCommitment THEN
				INSERT_RG_LANE_DTL_ERROR (
					vTCCompanyId,
					vLaneId,
					vRGLaneDtlSeq,
					'Monthly Capacity must be greater than or equal to Monthly Commitment for the given frequency',
					4720073,
					NULL);
			END IF;

			IF vMonthlyCommitPCT < 0  THEN
				INSERT_RG_LANE_DTL_ERROR (
					vTCCompanyId,
					vLaneId,
					vRGLaneDtlSeq,
					'Monthly Commitment % should be greater than or equal to zero',
					4720074,
					NULL);
			END IF;
		END IF;
	ELSE
		vFailure := 1;

		IF ( (vMonthlyCapacity IS NULL OR vMonthlyCommitment IS NULL) OR (vMonthlyCapacity <= 0)) THEN
			INSERT_RG_LANE_DTL_ERROR (
				vTCCompanyId,
				vLaneId,
				vRGLaneDtlSeq,
				'Monthly Commitment or Monthly Capacity is null or 0 for given frequency.',
				4720076,
				NULL);
		END IF;
	END IF;
Elsif (Vfrequency = 'W') Then
	IF ( (vWeeklyCapacity IS NOT NULL AND vWeeklyCommitment IS NOT NULL) AND (vWeeklyCapacity >= 0)) THEN
		IF (vMot IS NOT NULL AND vTrackCommitment = 0) THEN
			IF (vWeeklyCommitment != 0 OR vWeeklyCommitPCT != 0) THEN
				vFailure := 1;

				INSERT_RG_LANE_DTL_ERROR (
					vTCCompanyId,
					vLaneId,
					vRGLaneDtlSeq,
					'Weekly Commitment or Weekly Commitment Pct is not zero for the given mode/frequency.',
					4720051,
					NULL);
			ELSE
				vPassFail := 0;
			END IF;
		END IF;

		IF (vWeeklyCapacity >= vWeeklyCommitment) AND (vWeeklyCommitPCT >= 0)THEN
			vPassFail := 0;
		ELSE
			vFailure := 1;

			IF vWeeklyCapacity < vWeeklyCommitment	THEN
				INSERT_RG_LANE_DTL_ERROR (
					vTCCompanyId,
					vLaneId,
					vRGLaneDtlSeq,
					'Weekly Capacity must be greater than or equal to Weekly Commitment for the given frequency',
					4720052,
					NULL);
			END IF;

			IF vWeeklyCommitPCT < 0   THEN
				INSERT_RG_LANE_DTL_ERROR (
					vTCCompanyId,
					vLaneId,
					vRGLaneDtlSeq,
					'Weekly Commitment % should be greater than or equal to zero',
					4720053,
					NULL);
			END IF;
		END IF;


	ELSE
		vFailure := 1;

		IF ( (vWeeklyCapacity IS NULL OR vWeeklyCommitment IS NULL) OR (vWeeklyCapacity <= 0)) THEN
			INSERT_RG_LANE_DTL_ERROR (
				vTCCompanyId,
				vLaneId,
				vRGLaneDtlSeq,
				'Weekly Commitment or Weekly Capacity is null or 0 for given frequency.',
				4720055,
				NULL);
		END IF;
	END IF;
ELSIF (vFrequency = 'D')THEN                                  /* Check for the Daily Capacity */
	IF ( (vDailyCapacity IS NOT NULL AND vDailyCommitment IS NOT NULL) AND (vDailyCapacity >= 0)) THEN
		IF (vMot IS NOT NULL AND vTrackCommitment = 0)THEN
			IF (vDailyCommitment != 0 OR vDailyCommitPCT != 0) THEN
				vFailure := 1;

				INSERT_RG_LANE_DTL_ERROR (
					vTCCompanyId,
					vLaneId,
					vRGLaneDtlSeq,
					'Daily Commitment or Daily Commitment Pct is not zero for the given mode/frequency.',
					4720056,
					NULL);
			ELSE
				vPassFail := 0;
			END IF;
		END IF;

		/* DBMS_OUTPUT.PUT_LINE (
		   'Daily Capacity: ' || TO_CHAR (vDailyCapacity));
		DBMS_OUTPUT.PUT_LINE (
		   'Daily Commit Pct: ' || TO_CHAR (vDailyCommitPCT));
		DBMS_OUTPUT.PUT_LINE (
		   'Daily Surge Capacity: ' || TO_CHAR (vDailySurgeCapacity));
 */
		IF ( (vDailyCapacity >= vDailyCommitment)AND (vDailyCommitPCT >= 0))THEN
		   -- and (vSurgeCount = 0 or (vSurgeCount > 0 and vDailySurgeCapacity >= vDailyCapacity))) THEN

			vPassFail := 0;
		ELSE
			vFailure := 1;

			IF vDailyCapacity < vDailyCommitment THEN
				INSERT_RG_LANE_DTL_ERROR (
					vTCCompanyId,
					vLaneId,
					vRGLaneDtlSeq,
					'Daily Capacity must be greater than or equal to Daily Commitment for the given frequency',
					4720057,
					NULL);
			END IF;

			IF vDailyCommitPCT < 0  THEN
				INSERT_RG_LANE_DTL_ERROR (
					vTCCompanyId,
					vLaneId,
					vRGLaneDtlSeq,
					'Daily Commitment % should be greater than or equal to zero',
					4720058,
					NULL);
			END IF;
		END IF;


	ELSE
		vFailure := 1;

		IF ( (vDailyCapacity IS NULL OR vDailyCommitment IS NULL) OR (vDailyCapacity <= 0)) THEN
			INSERT_RG_LANE_DTL_ERROR (
				vTCCompanyId,
				vLaneId,
				vRGLaneDtlSeq,
				'Daily Commitment or Daily Capacity is null or 0 for given frequency.',
				4720060,
				NULL);
		END IF;
	END IF;
END IF;

SELECT COUNT(*)
  INTO vSurgeCount
  FROM import_surge_capacity
      WHERE TC_COMPANY_ID = VTCCOMPANYID
            AND LANE_ID = VLANEID
            AND RG_LANE_DTL_SEQ = VRGLANEDTLSEQ;



	OPEN surge_capacity_cursor FOR v_stmt
         USING vTCCompanyId,
               vLaneId,
               vRGLaneDtlSeq;

	LOOP FETCH surge_capacity_cursor INTO
			 vEffectiveDtSurgeCapacity,
			 vExpirationDtSurgeCapacity,
			 vYearlySurgeCapacity,
             vMonthlySurgeCapacity,
             vWeeklySurgeCapacity,
             vDailySurgeCapacity,
             vDOWSurgeCapacitySun,
             vDOWSurgeCapacityMon,
             vDOWSurgeCapacityTue,
             vDOWSurgeCapacityWed,
             vDOWSurgeCapacityThu,
             vDOWSurgeCapacityFri,
             vDOWSurgeCapacitySat;

     EXIT WHEN surge_capacity_cursor%NOTFOUND;

      /*
         SL 05/27/02

         Check Whether the effective date for the detail records is greater
         than the expiration record. If it is true, insert into the
         error log table
      */
	  vReturn1:= 0;

      IF vEffectiveDtSurgeCapacity > vExpirationDtSurgeCapacity
      THEN
         vFailure := 1;

         INSERT_RG_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRGLaneDtlSeq,
            'Import Surge Capacity - Effective Date is greater than the Expiration Date',
            4720050,
            NULL);
      END IF;

      /*
        TT 44119
     Yearly and Monthly Validataion
      */

IF (vFrequency = 'Y')   THEN
                                   --Check for the Yearly Capacity
	IF ( (vYearlyCapacity IS NOT NULL AND vYearlyCommitment IS NOT NULL) AND (vYearlyCapacity >= 0))THEN
		IF (vSurgeCount > 0)THEN
			IF (vYearlySurgeCapacity <= vYearlyCapacity) THEN

				vPassFail := 1;
				vFailure := 1;

				INSERT_RG_LANE_DTL_ERROR (
					vTCCompanyId,
					vLaneId,
					vRGLaneDtlSeq,
					'Yearly Surge Capacity must be greater than or equal to Yearly Capacity for the given frequency',
					4720070,
					NULL);
			END IF;
		END IF;

	END IF;                                        -- End of Yearly Check
ELSIF (vFrequency = 'M') THEN                                   -- Check for the Monthly Capacity
	IF ( (vMonthlyCapacity IS NOT NULL AND vMonthlyCommitment IS NOT NULL) AND (vMonthlyCapacity >= 0)) THEN


		IF (vSurgeCount > 0)THEN
			IF (vMonthlySurgeCapacity <= vMonthlyCapacity) THEN

				vPassFail := 1;
				vFailure := 1;

				INSERT_RG_LANE_DTL_ERROR (
					vTCCompanyId,
					vLaneId,
					vRGLaneDtlSeq,
					'Monthly Surge Capacity must be greater than or equal to Monthly Capacity for the given frequency',
					4720075,
					NULL);
			END IF;
		END IF;

	END IF;                                       -- End of Monthly Check
ELSIF (vFrequency = 'W') THEN                                 /* Check for the Weekly Capacity */
	IF ( (vWeeklyCapacity IS NOT NULL AND vWeeklyCommitment IS NOT NULL) AND (vWeeklyCapacity >= 0)) THEN

		IF (vSurgeCount > 0)THEN
			IF (vWeeklySurgeCapacity <= vWeeklyCapacity)THEN

				vPassFail := 1;
				vFailure := 1;

				INSERT_RG_LANE_DTL_ERROR (
					vTCCompanyId,
					vLaneId,
					vRGLaneDtlSeq,
					'Weekly Surge Capacity must be greater than or equal to Weekly Capacity for the given frequency',
					4720054,
					NULL);
			END IF;
		END IF;

	END IF;
ELSIF (vFrequency = 'D')THEN                                  /* Check for the Daily Capacity */
	IF ( (vDailyCapacity IS NOT NULL AND vDailyCommitment IS NOT NULL) AND (vDailyCapacity >= 0)) THEN


		/* DBMS_OUTPUT.PUT_LINE (
		   'Daily Capacity: ' || TO_CHAR (vDailyCapacity));
		DBMS_OUTPUT.PUT_LINE (
		   'Daily Commit Pct: ' || TO_CHAR (vDailyCommitPCT));
		DBMS_OUTPUT.PUT_LINE (
		   'Daily Surge Capacity: ' || TO_CHAR (vDailySurgeCapacity));
 */

		IF (vSurgeCount > 0)THEN
			IF (vDailySurgeCapacity <= vDailyCapacity)THEN

				vPassFail := 1;
				vFailure := 1;

				INSERT_RG_LANE_DTL_ERROR (
					vTCCompanyId,
					vLaneId,
					vRGLaneDtlSeq,
					'Daily Surge Capacity must be greater than or equal to Daily Capacity for the given frequency',
					4720059,
					NULL);
			END IF;
		END IF;
	END IF;
 ELSIF (vFrequency = 'O') THEN                                         /* Day of the Week Check */
	 -- Check the Validation for each day of the week, Sun thr Sat

	vReturn1 := validate_dow_capacity_commit (vTCCompanyId,
											vLaneId,
											vRGLaneDtlSeq,
											vDowCapacitySun,
											vDowCommitmentSun,
											vDowCommitPctSun,
											vDowSurgeCapacitySun,
											'Sunday',
											vMot,
											vTrackCommitment,
											vSurgeCount);
	IF vReturn1 = 1
	 THEN
		vPassFail := 1;
	 END IF;

	vReturn1 := validate_dow_capacity_commit (vTCCompanyId,
											vLaneId,
											vRGLaneDtlSeq,
											vDowCapacityMon,
											vDowCommitmentMon,
											vDowCommitPctMon,
											vDowSurgeCapacityMon,
											'Monday',
											vMot,
											vTrackCommitment,
											vSurgeCount);
	IF vReturn1 = 1
	 THEN
		vPassFail := 1;
	 END IF;
	vReturn1 := validate_dow_capacity_commit (vTCCompanyId,
											vLaneId,
											vRGLaneDtlSeq,
											vDowCapacityTue,
											vDowCommitmentTue,
											vDowCommitPctTue,
											vDowSurgeCapacityTue,
											'Tuesday',
											vMot,
											vTrackCommitment,
											vSurgeCount);
	IF vReturn1 = 1
	 THEN
		vPassFail := 1;
	 END IF;
	vReturn1 := validate_dow_capacity_commit (vTCCompanyId,
											vLaneId,
											vRGLaneDtlSeq,
											vDowCapacityWed,
											vDowCommitmentWed,
											vDowCommitPctWed,
											vDowSurgeCapacityWed,
											'Wednesday',
											vMot,
											vTrackCommitment,
											vSurgeCount);
	IF vReturn1 = 1
	 THEN
		vPassFail := 1;
	 END IF;
	vReturn1 := validate_dow_capacity_commit (vTCCompanyId,
											vLaneId,
											vRGLaneDtlSeq,
											vDowCapacityThu,
											vDowCommitmentThu,
											vDowCommitPctThu,
											vDowSurgeCapacityThu,
											'Thrusday',
											vMot,
											vTrackCommitment,
											vSurgeCount);
	IF vReturn1 = 1
	 THEN
		vPassFail := 1;
	 END IF;
	vReturn1 := validate_dow_capacity_commit (vTCCompanyId,
											vLaneId,
											vRGLaneDtlSeq,
											vDowCapacityFri,
											vDowCommitmentFri,
											vDowCommitPctFri,
											vDowSurgeCapacityFri,
											'Friday',
											vMot,
											vTrackCommitment,
											vSurgeCount);
	IF vReturn1 = 1
	 THEN
		vPassFail := 1;
	 END IF;
	vReturn1 := validate_dow_capacity_commit (vTCCompanyId,
											vLaneId,
											vRGLaneDtlSeq,
											vDowCapacitySat,
											vDowCommitmentSat,
											vDowCommitPctSat,
											vDowSurgeCapacitySat,
											'Saturday',
											vMot,
											vTrackCommitment,
											vSurgeCount);
	IF vReturn1 = 1
	 THEN
		vPassFail := 1;
	 END IF;
 END IF;
	 IF vPassFail = 1
	 THEN
		vFailure := 1;
	 END IF;

	COMMIT;
	END LOOP;
      CLOSE surge_capacity_cursor;

  RETURN vFailure;
END VALIDATE_CAPACITY_COMMITMENT;

   FUNCTION validate_dow_capacity_commit (
      vTCCompanyId        IN company.COMPANY_ID%TYPE,
      vLaneId             IN import_rg_lane.LANE_ID%TYPE,
      vRGLaneDtlSeq       IN import_rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      vDowCapacity        IN import_rg_lane_dtl.commitment_sun%TYPE,
      vDowCommitment      IN import_rg_lane_dtl.commitment_sun%TYPE,
      vDowCommitPct       IN import_rg_lane_dtl.commitment_sun%TYPE,
      vDowSurgeCapacity   IN import_rg_lane_dtl.commitment_sun%TYPE,
      vDayOfWeek          IN VARCHAR2,
      vMot                IN mot.mot%TYPE,
      vTrackCommitment    IN mot.track_commitment%TYPE,
      vSurgeCount         IN NUMBER)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
      vFailure    NUMBER;
   BEGIN
      vFailure := 0;

      IF (    vDowCapacity IS NOT NULL
          AND vDowCommitment IS NOT NULL
          AND (vDowCapacity >= 0)
          AND (vDowCommitment >= 0))
      THEN
         IF (vMot IS NOT NULL AND vTrackCommitment = 0)
         THEN
            IF (vDowCommitment != 0 AND vDowCommitPct != 0)
            THEN
               vFailure := 1;

               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGLaneDtlSeq,
                  'Day of Week Commitment or Day of Week Commitment Pct is not zero for the given mode/frequency'
                  || '('
                  || vDayOfWeek
                  || ').',
                  4720061,
                  vDayOfWeek);
            ELSE
               vPassFail := 0;
            END IF;
         END IF;

         IF ( (vDowCapacity >= vDowCommitment) AND (vDowCommitPct >= 0))
         THEN
            --      and (vSurgeCount = 0 or (vSurgeCount > 0 and vDowSurgeCapacity >= vDowCapacity))) THEN

            vPassFail := 0;
         ELSE
            vFailure := 1;

            IF vDowCapacity < vDowCommitment
            THEN
               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGLaneDtlSeq,
                  'Day of Week (' || vDayOfWeek
                  || '): Capacity must be greater than or equal to Commitment for the given frequency',
                  4720062,
                  vDayOfWeek);
            END IF;

            IF vDowCommitPCT < 0
            THEN
               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGLaneDtlSeq,
                  'Day of Week (' || vDayOfWeek
                  || '): Commitment % should be greater than or equal to zero',
                  4720063,
                  vDayOfWeek);
            END IF;
         END IF;

         IF (vSurgeCount > 0)
         THEN
            IF (vDowSurgeCapacity >= vDowCapacity)
            THEN
               vPassFail := 0;
            ELSE
               vPassFail := 1;

               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGLaneDtlSeq,
                  'Day of Week (' || vDayOfWeek
                  || '): Surge Capacity must be greater than or equal to Capacity for the given frequency',
                  4720064,
                  vDayOfWeek);
            END IF;
         END IF;
      ELSE
         vFailure := 1;

         INSERT_RG_LANE_DTL_ERROR (
            vTCCompanyId,
            vLaneId,
            vRGLaneDtlSeq,
               'Day of Week ('
            || vDayOfWeek
            || '): Commitment or Capacity is null for given frequency.',
            4720065,
            vDayOfWeek);
      END IF;

      RETURN vFailure;
   END validate_dow_capacity_commit;

   FUNCTION GET_BUDG_RESOURCE_WHERE_CLAUSE (
      vMot                   IN rg_lane_dtl.MOT_ID%TYPE,
      vEquipment             IN rg_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN rg_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN RG_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN rg_lane_dtl.PROTECTION_LEVEL_ID%TYPE)
      RETURN VARCHAR2
   IS
      resource_sql   VARCHAR2 (2000);
   BEGIN
      resource_sql := '';

      IF (vMot IS NOT NULL)
      THEN
         resource_sql := resource_sql || ' AND mot_id = ' || vMot;
      ELSE
         resource_sql := resource_sql || ' AND mot_id IS Null ';
      END IF;

      IF (vEquipment IS NOT NULL)
      THEN
         resource_sql := resource_sql || ' AND equipment_id = ' || vEquipment;
      ELSE
         resource_sql := resource_sql || ' AND equipment_id IS Null ';
      END IF;

      IF (vServiceLevel IS NOT NULL)
      THEN
         resource_sql :=
            resource_sql || ' AND service_level_id = ' || vServiceLevel;
      ELSE
         resource_sql := resource_sql || ' AND service_level_id IS Null ';
      END IF;

      IF (vProtectionLevel IS NOT NULL)
      THEN
         resource_sql :=
            resource_sql || ' AND protection_level_id = ' || vProtectionLevel;
      ELSE
         resource_sql := resource_sql || ' AND protection_level_id IS Null ';
      END IF;

      IF (p_scndr_carrier_code IS NOT NULL)
      THEN
         resource_sql :=
               resource_sql
            || ' and SCNDR_CARRIER_ID = '
            || p_scndr_carrier_code;
      ELSE
         resource_sql := resource_sql || ' and SCNDR_CARRIER_ID is NULL ';
      END IF;

      RETURN resource_sql;
   END GET_BUDG_RESOURCE_WHERE_CLAUSE;

   FUNCTION GET_RESOURCE_WHERE_CLAUSE (
      vCarrierCode           IN rg_lane_dtl.CARRIER_ID%TYPE,
      vMot                   IN rg_lane_dtl.MOT_ID%TYPE,
      vEquipment             IN rg_lane_dtl.EQUIPMENT_ID%TYPE,
      vServiceLevel          IN rg_lane_dtl.SERVICE_LEVEL_ID%TYPE,
      p_scndr_carrier_code   IN RG_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
      vProtectionLevel       IN rg_lane_dtl.PROTECTION_LEVEL_ID%TYPE,
	  vVoyage				 IN rg_lane_dtl.VOYAGE%TYPE)
      RETURN VARCHAR2
   IS
      resource_sql   VARCHAR2 (2000);
   BEGIN
      resource_sql := '';

      IF (vCarrierCode IS NOT NULL)
      THEN
         resource_sql := resource_sql || ' AND carrier_id = ' || vCarrierCode;
      ELSE
         resource_sql := resource_sql || ' AND carrier_id IS Null ';
      END IF;

      IF (vVoyage IS NOT NULL)
      THEN
         resource_sql := resource_sql || ' AND voyage = ''' || vVoyage || ''' ';
      ELSE
         resource_sql := resource_sql || ' AND voyage IS Null ';
      END IF;

      resource_sql :=
         resource_sql || ' AND O_SHIP_VIA IS Null AND D_SHIP_VIA IS NULL ';
      resource_sql :=
         resource_sql
         || GET_BUDG_RESOURCE_WHERE_CLAUSE (vMot,
                                            vEquipment,
                                            vServiceLevel,
                                            p_scndr_carrier_code,
                                            vProtectionLevel);

      RETURN resource_sql;
   END GET_RESOURCE_WHERE_CLAUSE;

   FUNCTION GET_PARENT_BU_IDS (vTCCompanyId IN company.COMPANY_ID%TYPE)
      RETURN VARCHAR2
   IS
      vBUList            VARCHAR2 (10000);
      vParentCompanyId   company.COMPANY_ID%TYPE;
   BEGIN
      vBUList := '';
      vParentCompanyId := NULL;

      IF vTCCompanyId IS NULL
      THEN
         RETURN vBUList;
      END IF;

      BEGIN
         SELECT PARENT_COMPANY_ID
           INTO vParentCompanyId
           FROM COMPANY
          WHERE COMPANY_ID = vTcCompanyId;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            vParentCompanyId := NULL;
      END;

      IF (vParentCompanyId IS NULL)
      THEN
         vBUList := TO_CHAR (vTcCompanyId);
      ELSE
         vBUList :=
               TO_CHAR (vTcCompanyId)
            || ','
            || GET_PARENT_BU_IDS (vParentCompanyId);
      END IF;

      RETURN vBUList;
   END GET_PARENT_BU_IDS;

   FUNCTION VALIDATE_CARRIER_MODE_UOM (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vLaneId            IN import_rg_lane.LANE_ID%TYPE,
      vRGLaneDtlSeq      IN import_rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      vCarrierCode       IN import_rg_lane_dtl.CARRIER_CODE%TYPE,
      vCarrierCodeBUID   IN import_rg_lane_dtl.TP_CODE_BU%TYPE,
      vMode              IN import_rg_lane_dtl.MOT%TYPE,
      vModeBUID          IN import_rg_lane_dtl.MOT_BU%TYPE,
      vSizeUOM           IN import_rg_lane_dtl.SIZE_UOM%TYPE,
      vSizeUOMBUID       IN import_rg_lane_dtl.SIZE_UOM_BU%TYPE)
      RETURN NUMBER
   IS
      vCount             INT;
      vFailure           INT;
      vSizeUOMID         INT;
      vSizeUOMIDFromUI   INT;
   BEGIN
      vCount := 0;
      vFailure := 0;
      vSizeUOMID := 0;
      vSizeUOMIDFromUI := 0;

      IF (vMode IS NULL)
      THEN
         IF (vSizeUOM IS NOT NULL)
         THEN
            vFailure := 1;
            INSERT_RG_LANE_DTL_ERROR (
               vTCCompanyId,
               vLaneId,
               vRGLaneDtlSeq,
               'Mode mapped must be specified for using size UOM for capacity and commitments',
               4000032,
               vSizeUOM);
         END IF;
      ELSE
         IF (vSizeUOM IS NOT NULL)
         THEN
            SELECT COUNT (*) CNT
              INTO vCount
              FROM CARRIER_CODE_MOT_CAPCOMM
             WHERE ( (CARRIER_ID =
                         (SELECT CARRIER_ID
                            FROM CARRIER_CODE
                           WHERE CARRIER_CODE = vCarrierCode
                                 AND TC_COMPANY_ID = vCarrierCodeBUID AND mark_for_deletion = 0))
                    AND (MOT_ID =
                            (SELECT MOT_ID
                               FROM MOT
                              WHERE MOT = vMode AND TC_COMPANY_ID = vModeBUID))
                    AND (MARK_FOR_DELETION = 0));

            IF (vCount > 0)
            THEN
               SELECT CAP_COMM_SIZE_UOM_ID sizeUOM
                 INTO vSizeUOMID
                 FROM CARRIER_CODE_MOT_CAPCOMM
                WHERE ( (CARRIER_ID =
                            (SELECT CARRIER_ID
                               FROM CARRIER_CODE
                              WHERE CARRIER_CODE = vCarrierCode
                                    AND TC_COMPANY_ID = vCarrierCodeBUID AND mark_for_deletion = 0))
                       AND (MOT_ID =
                               (SELECT MOT_ID
                                  FROM MOT
                                 WHERE MOT = vMode
                                       AND TC_COMPANY_ID = vModeBUID))
                       AND (MARK_FOR_DELETION = 0));

               SELECT size_uom_id
                 INTO vSizeUOMIDFromUI
                 FROM size_uom
                WHERE size_uom = vSizeUOM AND tc_company_id = vTCCompanyId;

               IF (vSizeUOM != vSizeUOMID)
               THEN
                  vFailure := 1;
                  INSERT_RG_LANE_DTL_ERROR (
                     vTCCompanyId,
                     vLaneId,
                     vRGLaneDtlSeq,
                     'SizeUOM not valid for Carrier-Mode combination ',
                     9900026,
                     vSizeUOM);
               END IF;
            END IF;
         END IF;
      END IF;

      RETURN vFailure;
   END VALIDATE_CARRIER_MODE_UOM;

   PROCEDURE REPLICATE_RA_SA_LANE_DETAIL (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vLaneId        IN comb_lane.LANE_ID%TYPE,
      vRGDtlSeq      IN comb_lane_dtl.LANE_DTL_SEQ%TYPE,
      vIsRating      IN comb_lane_dtl.IS_RATING%TYPE,
      vIsSailing     IN comb_lane_dtl.IS_SAILING%TYPE)
   IS
      vRGDtlSeqNew   comb_lane_dtl.LANE_DTL_SEQ%TYPE;
   BEGIN
      SELECT MAX (LANE_DTL_SEQ) + 1
        INTO vRGDtlSeqNew
        FROM COMB_LANE_DTL
       WHERE LANE_ID = vLaneID;

      INSERT INTO COMB_LANE_DTL (TC_COMPANY_ID,
                                 LANE_ID,
                                 LANE_DTL_SEQ,
                                 LANE_DTL_STATUS,
                                 IS_RATING,
                                 IS_ROUTING,
                                 IS_BUDGETED,
                                 LAST_UPDATED_SOURCE_TYPE,
                                 LAST_UPDATED_SOURCE,
                                 LAST_UPDATED_DTTM,
                                 CARRIER_ID,
                                 MOT_ID,
                                 EQUIPMENT_ID,
                                 SERVICE_LEVEL_ID,
                                 SCNDR_CARRIER_ID,
                                 PROTECTION_LEVEL_ID,
                                 EFFECTIVE_DT,
                                 EXPIRATION_DT,
                                 FIXED_TRANSIT_STANDARD_UOM,
                                 FIXED_TRANSIT_VALUE,
                                 IS_SAILING,
                                 SAILING_FREQUENCY_TYPE,
                                 SAILING_SCHEDULE_NAME,
                                 MASTER_LANE_ID,
                                 O_SHIP_VIA,
                                 D_SHIP_VIA,
                                 CONTRACT_NUMBER,
                                 CUSTOM_TEXT1,
                                 CUSTOM_TEXT2,
                                 CUSTOM_TEXT3,
                                 CUSTOM_TEXT4,
                                 CUSTOM_TEXT5,
                                 TT_TOLERANCE_FACTOR)
         SELECT TC_COMPANY_ID,
                LANE_ID,
                vRGDtlSeqNew,
                0,
                IS_RATING,
                0,
                IS_BUDGETED,
                3,
                'OMS',
                GETDATE (),
                CARRIER_ID,
                MOT_ID,
                EQUIPMENT_ID,
                SERVICE_LEVEL_ID,
                SCNDR_CARRIER_ID,
                PROTECTION_LEVEL_ID,
                EFFECTIVE_DT,
                EXPIRATION_DT,
                FIXED_TRANSIT_STANDARD_UOM,
                FIXED_TRANSIT_VALUE,
                IS_SAILING,
                SAILING_FREQUENCY_TYPE,
                SAILING_SCHEDULE_NAME,
                MASTER_LANE_ID,
                O_SHIP_VIA,
                D_SHIP_VIA,
                CONTRACT_NUMBER,
                CUSTOM_TEXT1,
                CUSTOM_TEXT2,
                CUSTOM_TEXT3,
                CUSTOM_TEXT4,
                CUSTOM_TEXT5,
                TT_TOLERANCE_FACTOR
           FROM COMB_LANE_DTL
          WHERE LANE_ID = vLaneId AND LANE_DTL_SEQ = vRGDtlSeq;

      COPY_CHILD_ROW (vTCCompanyId,
                      vLaneId,
                      -1,
                      vRGDtlSeq,
                      vRGDtlSeqNew,
                      vIsRating,
                      0,
                      vIsSailing);
   END REPLICATE_RA_SA_LANE_DETAIL;

   FUNCTION VALI_MUL_R_TP_FLAG (
      vTCCompanyId           IN company.COMPANY_ID%TYPE,
      vLaneId                IN rg_lane.LANE_ID%TYPE,
      vCarrierCode           IN CARRIER_CODE.CARRIER_CODE%TYPE,
      vRepTpFlag             IN rg_lane_dtl.REP_TP_FLAG%TYPE,
      vMode                  IN mot.mot%TYPE,
      p_effective_dt         IN RG_LANE_DTL.EFFECTIVE_DT%TYPE,
      p_expiration_dt        IN RG_LANE_DTL.EXPIRATION_DT%TYPE,
      p_scndr_carrier_code   IN CARRIER_CODE.CARRIER_CODE%TYPE)
      RETURN NUMBER
   IS
      vPassFail               NUMBER;
      num                     NUMBER;
      v_O_LOC_TYPE            IMPORT_RG_LANE.O_LOC_TYPE%TYPE;
      v_O_FACILITY_ALIAS_ID   IMPORT_RG_LANE.O_FACILITY_ALIAS_ID%TYPE;
      v_O_CITY                IMPORT_RG_LANE.O_CITY%TYPE;
      v_O_COUNTY              IMPORT_RG_LANE.O_COUNTY%TYPE;
      v_O_STATE_PROV          IMPORT_RG_LANE.O_STATE_PROV%TYPE;
      v_O_POSTAL_CODE         IMPORT_RG_LANE.O_POSTAL_CODE%TYPE;
      v_O_COUNTRY_CODE        IMPORT_RG_LANE.O_COUNTRY_CODE%TYPE;
      v_O_ZONE_ID             IMPORT_RG_LANE.O_ZONE_ID%TYPE;
      v_D_LOC_TYPE            IMPORT_RG_LANE.D_LOC_TYPE%TYPE;
      v_D_FACILITY_ALIAS_ID   IMPORT_RG_LANE.D_FACILITY_ALIAS_ID%TYPE;
      v_D_CITY                IMPORT_RG_LANE.D_CITY%TYPE;
      v_D_COUNTY              IMPORT_RG_LANE.D_COUNTY%TYPE;
      v_D_STATE_PROV          IMPORT_RG_LANE.D_STATE_PROV%TYPE;
      v_D_POSTAL_CODE         IMPORT_RG_LANE.D_POSTAL_CODE%TYPE;
      v_D_COUNTRY_CODE        IMPORT_RG_LANE.D_COUNTRY_CODE%TYPE;
      v_D_ZONE_ID             IMPORT_RG_LANE.D_ZONE_ID%TYPE;
      vCombLaneId             RG_LANE.LANE_ID%TYPE;
   BEGIN
      NUM := 0;
      VPASSFAIL := 0;

      SELECT O_LOC_TYPE,
             O_FACILITY_ALIAS_ID,
             O_CITY,
             O_COUNTY,
             O_STATE_PROV,
             O_POSTAL_CODE,
             O_COUNTRY_CODE,
             O_ZONE_ID,
             D_LOC_TYPE,
             D_FACILITY_ALIAS_ID,
             D_CITY,
             D_COUNTY,
             D_STATE_PROV,
             D_POSTAL_CODE,
             D_COUNTRY_CODE,
             D_ZONE_ID
        INTO v_O_LOC_TYPE,
             v_O_FACILITY_ALIAS_ID,
             v_O_CITY,
             v_O_COUNTY,
             v_O_STATE_PROV,
             v_O_POSTAL_CODE,
             v_O_COUNTRY_CODE,
             v_O_ZONE_ID,
             v_D_LOC_TYPE,
             v_D_FACILITY_ALIAS_ID,
             v_D_CITY,
             v_D_COUNTY,
             v_D_STATE_PROV,
             v_D_POSTAL_CODE,
             v_D_COUNTRY_CODE,
             v_D_ZONE_ID
        FROM IMPORT_RG_LANE
       WHERE LANE_ID = vLaneId;

      SELECT lane_id
        INTO vCombLaneId
        FROM rg_lane
       WHERE (o_search_location = (SELECT LANE_LOCATION_PKG.fnGetLocationString (
                                             v_O_LOC_TYPE,
                                             (SELECT facility_id
                                                FROM facility_alias
                                               WHERE tc_company_id =
                                                        vTCCompanyId
                                                     AND FACILITY_ALIAS_ID =
                                                            v_O_FACILITY_ALIAS_ID),
                                             v_O_CITY,
                                             v_O_COUNTY,
                                             v_O_STATE_PROV,
                                             v_O_POSTAL_CODE,
                                             v_O_COUNTRY_CODE,
                                             TO_CHAR (v_O_ZONE_ID))
                                     FROM DUAL)
              AND d_search_location = (SELECT LANE_LOCATION_PKG.fnGetLocationString (
                                                 v_D_LOC_TYPE,
                                                 (SELECT facility_id
                                                    FROM facility_alias
                                                   WHERE tc_company_id =
                                                            vTCCompanyId
                                                         AND FACILITY_ALIAS_ID =
                                                                v_D_FACILITY_ALIAS_ID),
                                                 v_D_CITY,
                                                 v_D_COUNTY,
                                                 v_D_STATE_PROV,
                                                 v_D_POSTAL_CODE,
                                                 v_D_COUNTRY_CODE,
                                                 TO_CHAR (v_D_ZONE_ID))
                                         FROM DUAL));

      IF vRepTpFlag = 1
      THEN
         IF vMode IS NOT NULL
         THEN
            SELECT COUNT (*)
              INTO NUM
              FROM (SELECT 1
                      FROM import_rg_lane_dtl
                     WHERE     rep_tp_flag = 1
                           AND mot = vMode
                           AND lane_id = vLaneId
                           AND carrier_code != vCarrierCode
                           AND p_effective_dt <= p_expiration_dt
                           AND EFFECTIVE_DT <= p_effective_dt
                           AND EFFECTIVE_DT <= p_expiration_dt
                           AND EXPIRATION_DT >= p_effective_dt
                           AND EXPIRATION_DT >= p_expiration_dt
                    UNION ALL
                    SELECT 1
                      FROM rg_lane_dtl, mot, carrier_code
                     WHERE     rep_tp_flag = 1
                           AND mot.mot_id = rg_lane_dtl.mot_id
                           AND carrier_code.tc_company_id = vTCCompanyId
                           AND carrier_code.carrier_id =
                                  rg_lane_dtl.carrier_id
                           AND carrier_code != vCarrierCode
                           AND mot.mot = vMode
                           AND lane_id = vCombLaneId
                           AND p_effective_dt <= p_expiration_dt
                           AND EFFECTIVE_DT <= p_effective_dt
                           AND EFFECTIVE_DT <= p_expiration_dt
                           AND EXPIRATION_DT >= p_effective_dt
                           AND EXPIRATION_DT >= p_expiration_dt);
         END IF;
      END IF;

      IF NUM > 0
      THEN
         VPASSFAIL := 1;
      ELSE
         VPASSFAIL := 0;
      END IF;

      RETURN VPASSFAIL;
   END VALI_MUL_R_TP_FLAG;

   PROCEDURE CHECK_DUPLICATE_RG_LANE_DTL (
      VLANEID       IN     COMB_LANE.LANE_ID%TYPE,
      CARRIERID     IN     COMB_LANE_DTL.CARRIER_ID%TYPE,
      MOTID         IN     COMB_LANE_DTL.MOT_ID%TYPE,
      SLID          IN     COMB_LANE_DTL.SERVICE_LEVEL_ID%TYPE,
      EQID          IN     COMB_LANE_DTL.EQUIPMENT_ID%TYPE,
      PLID          IN     COMB_LANE_DTL.PROTECTION_LEVEL_ID%TYPE,
      SCCARRIERID   IN     COMB_LANE_DTL.SCNDR_CARRIER_ID%TYPE,
	  VVOYAGE		IN 	   COMB_LANE_DTL.VOYAGE%TYPE,
    VCOUNT           OUT INTEGER)
   IS
      -- vCount INTEGER;
      vMotTemp               COMB_LANE_DTL.MOT_ID%TYPE;
      vEquipmentTemp         COMB_LANE_DTL.EQUIPMENT_ID%TYPE;
      vServiceLevelTemp      COMB_LANE_DTL.SERVICE_LEVEL_ID%TYPE;
      vProtectionLevelTemp   COMB_LANE_DTL.PROTECTION_LEVEL_ID%TYPE;
      vSecCarrierIdTemp      COMB_LANE_DTL.SCNDR_CARRIER_ID%TYPE;

      vEffDT                 COMB_LANE_DTL.EFFECTIVE_DT%TYPE;
      vExpDT                 COMB_LANE_DTL.EXPIRATION_DT%TYPE;
      vLaneDtlSeq            COMB_LANE_DTL.LANE_DTL_SEQ%TYPE;

      EFF_EXP_SQL            VARCHAR2 (2000);
      OVRLAP_EFF_EXP_SQL     VARCHAR2 (2000);

      eff_exp_cursor         ref_curtype;
      OVRLAP_EFF_CURSOR      ref_curtype;
   BEGIN
      vCount := 0;
      eff_exp_sql :=
            ' SELECT LANE_DTL_SEQ, EFFECTIVE_DT, EXPIRATION_DT '
         || ' FROM comb_lane_dtl '
         || ' WHERE lane_id = :vLaneId'
         || ' AND IS_ROUTING = 1'
         || ' AND HAS_SHIPPING_PARAM = 0 '
         || ' AND lane_dtl_status = 0 '
         || ' AND effective_dt <= expiration_dt ';

      vMotTemp := MOTID;
      vEquipmentTemp := EQID;
      vServiceLevelTemp := SLID;
      vProtectionLevelTemp := PLID;
      vSecCarrierIdTemp := SCCARRIERID;

      IF (vMotTemp = -1)
      THEN
         vMotTemp := NULL;
      END IF;

      IF (vEquipmentTemp = -1)
      THEN
         vEquipmentTemp := NULL;
      END IF;

      IF (vServiceLevelTemp = -1)
      THEN
         vServiceLevelTemp := NULL;
      END IF;

      IF (vProtectionLevelTemp = -1)
      THEN
         vProtectionLevelTemp := NULL;
      END IF;

      IF (vSecCarrierIdTemp = -1)
      THEN
         vSecCarrierIdTemp := NULL;
      END IF;

      EFF_EXP_SQL :=
         (COALESCE (EFF_EXP_SQL, '')
          || COALESCE (GET_RESOURCE_WHERE_CLAUSE (CARRIERID,
                                                  VMOTTEMP,
                                                  VEQUIPMENTTEMP,
                                                  VSERVICELEVELTEMP,
                                                  vSecCarrierIdTemp,
                                                  VPROTECTIONLEVELTEMP,
												  VVOYAGE),
                       ''));
                       
     
      eff_exp_sql := eff_exp_sql || ' ORDER BY EFFECTIVE_DT';

      OVRLAP_EFF_EXP_SQL :=
            ' SELECT COUNT(*) '
         || ' FROM comb_lane_dtl '
         || ' WHERE lane_id = :vLaneId'
         || ' AND LANE_DTL_SEQ != :vLaneDtlSeq'
         || ' AND IS_ROUTING = 1 '
         || ' AND HAS_SHIPPING_PARAM = 0 '
         || ' AND lane_dtl_status = 0 '
         || ' AND effective_dt <= expiration_dt '
         || ' AND expiration_dt >= :vEffectDt '
         || ' AND effective_dt <=  :vExpirDt ';

      OVRLAP_EFF_EXP_SQL :=
         (COALESCE (OVRLAP_EFF_EXP_SQL, '')
          || COALESCE (GET_RESOURCE_WHERE_CLAUSE (CARRIERID,
                                                  VMOTTEMP,
                                                  VEQUIPMENTTEMP,
                                                  VSERVICELEVELTEMP,
                                                  vSecCarrierIdTemp,
                                                  VPROTECTIONLEVELTEMP,
												  VVOYAGE),
                       ''));

      OPEN eff_exp_cursor FOR eff_exp_sql USING VLANEID;

     <<LOOP_LABEL>>
      LOOP
         FETCH eff_exp_cursor
         INTO vLaneDtlSeq, vEffDT, vExpDT;

         EXIT LOOP_LABEL WHEN eff_exp_cursor%NOTFOUND;

         --  INSERT INTO DEBUG_SQL(SQLTEXT) VALUES(TO_CHAR(vLaneDtlSeq));

         OPEN OVRLAP_EFF_CURSOR FOR OVRLAP_EFF_EXP_SQL
            USING VLANEID,
                  vLaneDtlSeq,
                  vEffDT,
                  vExpDT;

         FETCH OVRLAP_EFF_CURSOR INTO vCount;

         EXIT WHEN OVRLAP_EFF_CURSOR%NOTFOUND;

         CLOSE OVRLAP_EFF_CURSOR;

         IF (vCount > 0)
         THEN
            INSERT INTO DEBUG_SQL (SQLTEXT)
                 VALUES ('FOUND DUPLICATE');

            EXIT LOOP_LABEL;
         END IF;
      END LOOP LOOP_LABEL;

      CLOSE eff_exp_cursor;
   END;

   FUNCTION GET_SHIP_PARAMS_WHERE_CLAUSE (
      vMinVolume          IN RG_LANE_DTL.SP_MIN_VOLUME%TYPE,
      vMaxVolume          IN RG_LANE_DTL.SP_MAX_VOLUME%TYPE,
      vMinWeight          IN RG_LANE_DTL.SP_MIN_WEIGHT%TYPE,
      vMaxWeight          IN RG_LANE_DTL.SP_MAX_WEIGHT%TYPE,
      vMinLength          IN RG_LANE_DTL.SP_MIN_LINEAR_FEET%TYPE,
      vMaxLength          IN RG_LANE_DTL.SP_MAX_LINEAR_FEET%TYPE,
      vMinLpnCount        IN RG_LANE_DTL.SP_MIN_LPN_COUNT%TYPE,
      vMaxLpnCount        IN RG_LANE_DTL.SP_MAX_LPN_COUNT%TYPE,
      vLpnType            IN RG_LANE_DTL.SP_LPN_TYPE%TYPE,
      vMinMonetary        IN RG_LANE_DTL.SP_MIN_MONETARY_VALUE%TYPE,
      vMaxMonetary        IN RG_LANE_DTL.SP_MAX_MONETARY_VALUE%TYPE,
      vMonetaryCurrency   IN RG_LANE_DTL.SP_CURRENCY_CODE%TYPE)
      RETURN VARCHAR2
   IS
      resource_sql   VARCHAR2 (2000);
      andQuery       VARCHAR2 (100);
   BEGIN
      resource_sql := '';

      andQuery := ' AND ( ';

      IF (vMinVolume IS NOT NULL AND vMaxVolume IS NOT NULL)
      THEN
         resource_sql := resource_sql || andQuery;
         resource_sql := resource_sql || ' ( SP_MIN_VOLUME <= ' || vMaxVolume;
         resource_sql :=
            resource_sql || ' AND SP_MAX_VOLUME >= ' || vMinVolume;
         resource_sql := resource_sql || ' ) ';
         andQuery := ' OR ';
      --  ELSE
      --  resource_sql := resource_sql || ' AND SP_MIN_VOLUME IS NULL AND SP_MAX_VOLUME IS NULL ';
      END IF;

      IF (vMinWeight IS NOT NULL AND vMaxWeight IS NOT NULL)
      THEN
         resource_sql := resource_sql || andQuery;
         resource_sql := resource_sql || ' ( SP_MIN_WEIGHT <= ' || vMaxWeight;
         resource_sql :=
            resource_sql || ' AND SP_MAX_WEIGHT >= ' || vMinWeight;
         resource_sql := resource_sql || ' ) ';
         andQuery := ' OR ';
      -- ELSE
      --  resource_sql := resource_sql || ' AND SP_MIN_WEIGHT IS NULL AND SP_MAX_WEIGHT IS NULL ';
      END IF;

      IF (vMinLength IS NOT NULL AND vMaxLength IS NOT NULL)
      THEN
         resource_sql := resource_sql || andQuery;
         resource_sql :=
            resource_sql || ' ( SP_MIN_LINEAR_FEET <= ' || vMaxLength;
         resource_sql :=
            resource_sql || ' AND SP_MAX_LINEAR_FEET >= ' || vMinLength;
         resource_sql := resource_sql || ' ) ';
         andQuery := ' OR ';
      -- ELSE
      -- resource_sql := resource_sql || ' AND SP_MIN_LINEAR_FEET IS NULL AND SP_MAX_LINEAR_FEET IS NULL ';
      END IF;

      IF (    vMinLpnCount IS NOT NULL
          AND vMaxLpnCount IS NOT NULL
          AND vLpnType IS NOT NULL)
      THEN
         resource_sql := resource_sql || andQuery;
         resource_sql :=
            resource_sql || ' ( SP_MIN_LPN_COUNT <= ' || vMaxLpnCount;
         resource_sql :=
            resource_sql || ' AND SP_MAX_LPN_COUNT >= ' || vMinLpnCount;
         resource_sql := resource_sql || ' AND SP_LPN_TYPE = ' || vLpnType;
         resource_sql := resource_sql || ' ) ';
         andQuery := ' OR ';
      -- ELSE
      -- resource_sql := resource_sql || ' AND SP_MIN_LPN_COUNT IS NULL AND SP_MAX_LPN_COUNT IS NULL ';
      END IF;

      IF (    vMinMonetary IS NOT NULL
          AND vMaxMonetary IS NOT NULL
          AND vMonetaryCurrency IS NOT NULL)
      THEN
         resource_sql := resource_sql || andQuery;
         resource_sql :=
            resource_sql || ' ( SP_MIN_MONETARY_VALUE <= ' || vMaxMonetary;
         resource_sql :=
            resource_sql || ' AND SP_MAX_MONETARY_VALUE >= ' || vMinMonetary;
         resource_sql :=
               resource_sql
            || ' AND SP_CURRENCY_CODE = '''
            || vMonetaryCurrency
            || '''';
         resource_sql := resource_sql || ' ) ';
      -- ELSE
      -- resource_sql := resource_sql || ' AND SP_MIN_MONETARY_VALUE IS NULL AND SP_MAX_MONETARY_VALUE IS NULL ';
      END IF;

      resource_sql := resource_sql || ' ) ';
      RETURN resource_sql;
   END GET_SHIP_PARAMS_WHERE_CLAUSE;
   
   FUNCTION GET_SHIP_PARAMS_UPDATE_CLAUSE (
      vMinVolume          IN RG_LANE_DTL.SP_MIN_VOLUME%TYPE,
      vMaxVolume          IN RG_LANE_DTL.SP_MAX_VOLUME%TYPE,
      vMinWeight          IN RG_LANE_DTL.SP_MIN_WEIGHT%TYPE,
      vMaxWeight          IN RG_LANE_DTL.SP_MAX_WEIGHT%TYPE,
      vMinLength          IN RG_LANE_DTL.SP_MIN_LINEAR_FEET%TYPE,
      vMaxLength          IN RG_LANE_DTL.SP_MAX_LINEAR_FEET%TYPE,
      vMinLpnCount        IN RG_LANE_DTL.SP_MIN_LPN_COUNT%TYPE,
      vMaxLpnCount        IN RG_LANE_DTL.SP_MAX_LPN_COUNT%TYPE,
      vLpnType            IN RG_LANE_DTL.SP_LPN_TYPE%TYPE,
      vMinMonetary        IN RG_LANE_DTL.SP_MIN_MONETARY_VALUE%TYPE,
      vMaxMonetary        IN RG_LANE_DTL.SP_MAX_MONETARY_VALUE%TYPE,
      vMonetaryCurrency   IN RG_LANE_DTL.SP_CURRENCY_CODE%TYPE)
      RETURN VARCHAR2
   IS
      resource_sql   VARCHAR2 (2000);
   BEGIN
      resource_sql := '';

      IF (vMinVolume IS NOT NULL AND vMaxVolume IS NOT NULL)
      THEN
		 resource_sql := resource_sql || 'SP_MIN_VOLUME = ' || vMinVolume || ', SP_MAX_VOLUME = ' || vMaxVolume;
      ELSE
         resource_sql := resource_sql || 'SP_MIN_VOLUME = NULL, SP_MAX_VOLUME = NULL';
      END IF;
	  
	  resource_sql := resource_sql || ', ';
	  
	  IF (vMinWeight IS NOT NULL AND vMaxWeight IS NOT NULL)
      THEN
		 resource_sql := resource_sql || 'SP_MIN_WEIGHT = ' || vMinWeight || ', SP_MAX_WEIGHT = ' || vMaxWeight;
      ELSE
         resource_sql := resource_sql || 'SP_MIN_WEIGHT = NULL, SP_MAX_WEIGHT = NULL';
      END IF;
	  
	  resource_sql := resource_sql || ', ';

      IF (vMinLength IS NOT NULL AND vMaxLength IS NOT NULL)
      THEN
		 resource_sql := resource_sql || 'SP_MIN_LINEAR_FEET = ' || vMinLength || ', SP_MAX_LINEAR_FEET = ' || vMaxLength;
      ELSE
         resource_sql := resource_sql || 'SP_MIN_LINEAR_FEET = NULL, SP_MAX_LINEAR_FEET = NULL';
      END IF;
	  
	  resource_sql := resource_sql || ', ';

	  IF (vMinLpnCount IS NOT NULL AND vMaxLpnCount IS NOT NULL AND vLpnType IS NOT NULL)
      THEN
		 resource_sql := resource_sql || 'SP_MIN_LPN_COUNT = ' || vMinLpnCount || ', SP_MAX_LPN_COUNT = ' || vMaxLpnCount || ', SP_LPN_TYPE = ' || vLpnType;
      ELSE
         resource_sql := resource_sql || 'SP_MIN_LPN_COUNT = NULL, SP_MAX_LPN_COUNT = NULL, SP_LPN_TYPE = NULL';
      END IF;
	  
	  resource_sql := resource_sql || ', ';

      IF (vMinMonetary IS NOT NULL AND vMaxMonetary IS NOT NULL AND vMonetaryCurrency IS NOT NULL)
      THEN
		 resource_sql := resource_sql || 'SP_MIN_MONETARY_VALUE = ' || vMinMonetary || ', SP_MAX_MONETARY_VALUE = ' || vMaxMonetary || ', SP_CURRENCY_CODE = ' || vMonetaryCurrency;
      ELSE
         resource_sql := resource_sql || 'SP_MIN_MONETARY_VALUE = NULL, SP_MAX_MONETARY_VALUE = NULL, SP_CURRENCY_CODE = NULL';
      END IF;

      RETURN resource_sql;
   END GET_SHIP_PARAMS_UPDATE_CLAUSE;

    FUNCTION VALIDATE_OVERLAP_SHIP_PARAMS
 (
  vTCCompanyId     IN company.COMPANY_ID%TYPE,
  vImportLaneId    IN rg_lane.LANE_ID%TYPE,
  vLaneId          IN rg_lane.LANE_ID%TYPE,
  vImpRGLaneDtlSeq IN rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
  carrier_Id       IN rg_lane_dtl.CARRIER_ID%TYPE,
  mot_Id           IN rg_lane_dtl.MOT_ID%TYPE,
  equipment_Id     IN rg_lane_dtl.EQUIPMENT_ID%TYPE,
  service_Level_Id IN rg_lane_dtl.SERVICE_LEVEL_ID%TYPE,
  scndr_carrier_Id IN rg_lane_dtl.SCNDR_CARRIER_ID%TYPE,
  protection_Level_Id  IN  rg_lane_dtl.PROTECTION_LEVEL_ID%TYPE,
  vMinVolume IN RG_LANE_DTL.SP_MIN_VOLUME%TYPE,
  vMaxVolume IN RG_LANE_DTL.SP_MAX_VOLUME%TYPE,
  vMinWeight IN RG_LANE_DTL.SP_MIN_WEIGHT%TYPE,
  vMaxWeight IN RG_LANE_DTL.SP_MAX_WEIGHT%TYPE,
  vMinLength IN RG_LANE_DTL.SP_MIN_LINEAR_FEET%TYPE,
  vMaxLength IN RG_LANE_DTL.SP_MAX_LINEAR_FEET%TYPE,
  vMinLpnCount IN RG_LANE_DTL.SP_MIN_LPN_COUNT%TYPE,
  vMaxLpnCount IN RG_LANE_DTL.SP_MAX_LPN_COUNT%TYPE,
  vLpnType IN RG_LANE_DTL.SP_LPN_TYPE%TYPE,
  vMinMonetary IN RG_LANE_DTL.SP_MIN_MONETARY_VALUE%TYPE,
  vMaxMonetary IN RG_LANE_DTL.SP_MAX_MONETARY_VALUE%TYPE,
  vMonetaryCurrency IN RG_LANE_DTL.SP_CURRENCY_CODE%TYPE,
  vEffectiveDT          IN rg_lane_dtl.EFFECTIVE_DT%TYPE,
  vExpirationDT         IN rg_lane_dtl.EXPIRATION_DT%TYPE,
  vVoyage				IN rg_lane_dtl.VOYAGE%TYPE)
 RETURN NUMBER
 IS
 eff_exp_sql     VARCHAR2(2000);
 res_whr_cls         VARCHAR2 (2000);
 shp_whr_cls         VARCHAR2 (2000);
 vCount          NUMBER;
 vHasShippingParam   rg_lane_dtl.has_shipping_param%TYPE;
 vLaneDtlSeq NUMBER;
 vRetVal     NUMBER;
 eff_exp_cursor         ref_curtype;
 
 BEGIN

  vCount := 0;
  vRetVal := -1;
   
    IF( VLPNTYPE IS NULL
        AND VMINLPNCOUNT IS NULL
        AND VMAXLPNCOUNT IS NULL
        AND VMINWEIGHT IS NULL
        AND VMAXWEIGHT IS NULL
        AND VMINVOLUME IS NULL
        AND VMAXVOLUME IS NULL
        AND VMINLENGTH IS NULL
        AND VMAXLENGTH IS NULL
        AND vMinMonetary IS NULL
        AND vMaxMonetary IS NULL
        AND vMonetaryCurrency IS NULL)
    THEN
        vHasShippingParam := 0;
    ELSE
        vHasShippingParam := 1;
    END IF;

	IF (vHasShippingParam = 1)
      THEN
         eff_exp_sql :=
               ' SELECT lane_dtl_seq '
            || ' FROM comb_lane_dtl '
            || ' WHERE lane_id = :vLaneId'
            || ' AND lane_dtl_status = 0 '
            || ' AND HAS_SHIPPING_PARAM = 1 '
            || ' AND effective_dt <= expiration_dt '
            || ' AND expiration_dt >= :vEffectiveDT '
            || ' AND effective_dt <= :vExpirationDT ';
			
		 res_whr_cls := GET_RESOURCE_WHERE_CLAUSE (carrier_Id,
                                          mot_Id,
                                          equipment_Id,
                                          service_Level_Id,
                                          scndr_carrier_Id,
                                          protection_Level_Id,
										  vVoyage);

		 eff_exp_sql :=
            eff_exp_sql
            || res_whr_cls;
			
		 shp_whr_cls := GET_SHIP_PARAMS_WHERE_CLAUSE (vMinVolume,
                                             vMaxVolume,
                                             vMinWeight,
                                             vMaxWeight,
                                             vMinLength,
                                             vMaxLength,
                                             vMinLpnCount,
                                             vMaxLpnCount,
                                             vLpnType,
                                             vMinMonetary,
                                             vMaxMonetary,
                                             vMonetaryCurrency); 

         eff_exp_sql :=
            eff_exp_sql
            || shp_whr_cls;
			
		 OPEN eff_exp_cursor FOR eff_exp_sql
			USING vLaneId, vEffectiveDT, vExpirationDT;
		 
		 LOOP
		 
			FETCH eff_exp_cursor INTO vLaneDtlSeq;
	
			EXIT WHEN eff_exp_cursor%NOTFOUND;
			
			vCount := vCount + 1;
		 
		 END LOOP;
		 
		 -- In case of exactly one record matching return the vLaneDtlSeq (a value >= 0)
		 -- In case of more than one record matching return -2
		 -- In case of no record matching return - 1

         IF (vCount > 1)
         THEN

            UPDATE IMPORT_RG_LANE_DTL
               SET lane_dtl_status = 4
             WHERE     TC_COMPANY_ID = vTCCompanyId
                   AND LANE_ID = vImportLaneId
                   AND RG_LANE_DTL_SEQ = vImpRGLaneDtlSeq;

            INSERT_RG_LANE_DTL_ERROR (

               vTCCompanyId,
               vImportLaneId,
               vImpRGLaneDtlSeq,
               'Overlap found in Shipping Parameters.',
               4500045,
               NULL);
			  
			vRetVal := -2;
			   
         END IF;
		 
		 IF (vCount = 1)
		 THEN
			vRetVal := vLaneDtlSeq;
		 END IF;
		 
		 IF (vCount = 0)
		 THEN
			vRetVal := -1;
		 END IF;
		 
      END IF;

      RETURN vRetVal;
 END VALIDATE_OVERLAP_SHIP_PARAMS;

   FUNCTION VALIDATE_CUST_CARR_MOT_FEAS (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vLaneId        IN rg_lane.LANE_ID%TYPE,
      vRgDtlSeq      IN rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      vCarrierCode   IN carrier_code.CARRIER_CODE%TYPE,
      vMot           IN MOT.MOT%TYPE)
      RETURN NUMBER
   IS
      vCount       NUMBER;
      vPassFail    NUMBER;
      vCustCode    IMPORT_RG_LANE.CUSTOMER_CODE%TYPE;
      vCustId      IMPORT_RG_LANE.CUSTOMER_ID%TYPE;
      vCarrierId   IMPORT_RG_LANE_DTL.CARRIER_ID%TYPE;
      vMotId       IMPORT_RG_LANE_DTL.MOT_ID%TYPE;
   BEGIN
      vPassFail := 0;

      SELECT CUSTOMER_CODE, CUSTOMER_ID
        INTO vCustCode, vCustId
        FROM IMPORT_RG_LANE
       WHERE LANE_ID = vLaneId;

      SELECT CARRIER_ID, MOT_ID
        INTO vCarrierId, vMotId
        FROM IMPORT_RG_LANE_DTL
       WHERE LANE_ID = vLaneId AND RG_LANE_DTL_SEQ = vRgDtlSeq;

      IF (vCustCode IS NOT NULL)
      THEN
         IF (vCarrierCode IS NOT NULL)
         THEN
            SELECT COUNT (INFEASIBILITY_ID)
              INTO vCount
              FROM INFEASIBILITY
             WHERE     INFEASIBILITY_TYPE = 'CUCA'
                   AND VALUE1 = (SELECT CUSTOMER_ID
                                   FROM IMPORT_RG_LANE
                                  WHERE LANE_ID = vLaneId)
                   AND VALUE2 = TO_CHAR (vCarrierId)
                   AND TC_COMPANY_ID = vTCCompanyId;

            IF (vCount > 0)
            THEN
               vPassFail := 1;

               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGDtlSeq,
                  vCustCode || ' is infeasible with ' || vCarrierCode,
                  4000036,
                  vCustCode || '<sep>' || vCarrierCode);
            END IF;
         END IF;

         IF (vMot IS NOT NULL)
         THEN
            SELECT COUNT (INFEASIBILITY_ID)
              INTO vCount
              FROM INFEASIBILITY
             WHERE     INFEASIBILITY_TYPE = 'CUMO'
                   AND VALUE1 = (SELECT CUSTOMER_ID
                                   FROM IMPORT_RG_LANE
                                  WHERE LANE_ID = vLaneId)
                   AND VALUE2 = TO_CHAR (vMotId)
                   AND TC_COMPANY_ID = vTCCompanyId;

            IF (vCount > 0)
            THEN
               vPassFail := 1;

               INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGDtlSeq,
                  vCustCode || ' is infeasible with ' || vMot,
                  4000036,
                  vCustCode || '<sep>' || vMot);
            END IF;
         END IF;
      END IF;

      RETURN vPassFail;
   END VALIDATE_CUST_CARR_MOT_FEAS;

   PROCEDURE VALIDATE_RANK (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vLaneId        IN IMPORT_RATING_LANE.LANE_ID%TYPE,
      vRGDtlSeq      IN IMPORT_RG_LANE_DTL.RG_LANE_DTL_SEQ%TYPE,
      vRank          IN IMPORT_RG_LANE_DTL.RANK%TYPE)

   IS
      vNoRating      NUMBER;
   BEGIN
      select no_rating INTO vNoRating from IMPORT_RG_LANE where tc_company_id=vTCCompanyId and lane_id=vLaneId;

      if(vNoRating > 0 and vRank is null) then
          INSERT_RG_LANE_DTL_ERROR (vTCCompanyId,
                                   vLaneId,
                                   vRGDtlSeq,
                                   'When No Rating is set to true, Rank is a required field.',
                                   2820001,
                                   vRank);
      end if;
   END VALIDATE_RANK;
   
   FUNCTION VALIDATE_USE_EPI (
   vTCCompanyId   IN company.COMPANY_ID%TYPE,
   vLaneId        IN IMPORT_RATING_LANE.LANE_ID%TYPE,
   vImportLaneId        IN IMPORT_RATING_LANE.LANE_ID%TYPE,
   vUseEpi    IN IMPORT_RG_LANE.USE_EPI%TYPE,
   vEpiServiceGroup    IN IMPORT_RG_LANE.EPI_SERVICE_GROUP%TYPE)
      RETURN NUMBER
   IS
      vPassFail        NUMBER;
      vEffectiveExtPclCarrCount   NUMBER;
	  vEffectiveNonExtPclCarrCount   NUMBER;
   BEGIN
      vPassFail := 0;
	  
	  IF (vUseEpi = 0)
	  THEN
			SELECT COUNT(CLD.CARRIER_ID)  INTO  vEffectiveExtPclCarrCount 
			FROM COMB_LANE_DTL CLD, CARRIER_CODE CC  
			WHERE CLD.LANE_ID= vLaneId AND CLD.LANE_DTL_STATUS = 0 
			AND CLD.EXPIRATION_DT > CLD.EFFECTIVE_DT AND CLD.EXPIRATION_DT > GETDATE() 
			AND CLD.CARRIER_ID = CC.CARRIER_ID AND CC.CARRIER_TYPE_ID=24;
			
			IF (vEffectiveExtPclCarrCount > 0)
			THEN
				vPassFail := 1;
				INSERT_RG_LANE_ERROR (vTCCompanyId,
                               vImportLaneId,
                               'External Parcel Integration cannot be disabled on a lane having External Parcel Carrier(s)',
                               4720195,
                               NULL);
			END IF;			
		
	  ELSE
			SELECT COUNT(CLD.CARRIER_ID)  INTO  vEffectiveNonExtPclCarrCount 
			FROM COMB_LANE_DTL CLD, CARRIER_CODE CC  
			WHERE CLD.LANE_ID= vLaneId AND CLD.LANE_DTL_STATUS = 0 
			AND CLD.EXPIRATION_DT > CLD.EFFECTIVE_DT AND CLD.EXPIRATION_DT > GETDATE() 
			AND CLD.CARRIER_ID = CC.CARRIER_ID AND CC.CARRIER_TYPE_ID IS NOT NULL AND CC.CARRIER_TYPE_ID!=24;
			
			IF (vEffectiveNonExtPclCarrCount > 0)
			THEN
				vPassFail := 1;
				INSERT_RG_LANE_ERROR (vTCCompanyId,
                               vImportLaneId,
                               'External Parcel Integration cannot be enabled on a lane having Native Parcel Carrier(s)',
                               4720194,
                               NULL);
			END IF;
	  
	  END IF;
	  
	  IF (vEpiServiceGroup IS NOT NULL)
	  THEN
			SELECT COUNT(CLD.CARRIER_ID)  INTO  vEffectiveExtPclCarrCount 
			FROM COMB_LANE_DTL CLD, CARRIER_CODE CC  
			WHERE CLD.LANE_ID= vLaneId AND CLD.LANE_DTL_STATUS = 0 
			AND CLD.EXPIRATION_DT > CLD.EFFECTIVE_DT AND CLD.EXPIRATION_DT > GETDATE() 
			AND CLD.CARRIER_ID = CC.CARRIER_ID AND CC.CARRIER_TYPE_ID=24;
	
			IF (vEffectiveExtPclCarrCount > 0)
			THEN
				vPassFail := 1;
				INSERT_RG_LANE_ERROR (vTCCompanyId,
				vImportLaneId,
				'EPI Service Group cannot be specified on a lane having external Parcel Carrier(s)',
				4720197,
				NULL);
			END IF;
	  END IF;
	  
	  

      RETURN vPassFail;
   END VALIDATE_USE_EPI;
   
   FUNCTION VALIDATE_EPI_SERVICE_GROUP (
   vTCCompanyId   IN company.COMPANY_ID%TYPE,
   vLaneId        IN IMPORT_RATING_LANE.LANE_ID%TYPE,
   vUseEpi    IN IMPORT_RG_LANE.USE_EPI%TYPE,
   vEpiServiceGroup    IN IMPORT_RG_LANE.EPI_SERVICE_GROUP%TYPE)
      RETURN NUMBER
   IS
      vPassFail        NUMBER;
      vEpiSGCount   NUMBER;
	  vEffectiveExtPclCarrCount   NUMBER;
   BEGIN
      vPassFail := 0;
	  
	  IF (vUseEpi = 0)
	  THEN
		vPassFail := 1;
		INSERT_RG_LANE_ERROR (vTCCompanyId,
                               vLaneId,
                               'External Parcel Integration cannot be disabled on a lane having EPI Service Group',
                               4720196,
                               NULL);
		
	  END IF;

      SELECT COUNT (CODE_ID)
        INTO vEpiSGCount
        FROM SYS_CODE
       WHERE REC_TYPE='S' AND CODE_TYPE='D29' AND 
	   CODE_ID = vEpiServiceGroup;

      IF (vEpiSGCount < 1)
      THEN
         vPassFail := 1;
		 INSERT_RG_LANE_ERROR (vTCCompanyId,
                               vLaneId,
                               vEpiServiceGroup || ' is an invalid EPI Service Group',
                               4720198,
                               vEpiServiceGroup);
      END IF;

      RETURN vPassFail;
   END VALIDATE_EPI_SERVICE_GROUP;
   
   FUNCTION VALIDATE_EPI_FOR_LANE_DTL (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vLaneId        IN rg_lane.LANE_ID%TYPE,
      vRgDtlSeq      IN rg_lane_dtl.RG_LANE_DTL_SEQ%TYPE,
      vCarrierId   IN IMPORT_RG_LANE_DTL.CARRIER_ID%TYPE,
      vUseEpi           IN IMPORT_RG_LANE.USE_EPI%TYPE,
	  vEpiServiceGroup           IN IMPORT_RG_LANE.EPI_SERVICE_GROUP%TYPE)
      RETURN NUMBER
   IS
      vCount       NUMBER;
      vPassFail    NUMBER;
   BEGIN
      vPassFail := 0;
	  
	  IF (vUseEpi = 0)
	  THEN
			SELECT COUNT(CC.CARRIER_ID) INTO vCount FROM CARRIER_CODE CC WHERE CC.CARRIER_ID = vCarrierId AND CC.CARRIER_TYPE_ID = 24;
			IF (vCount>0)
			THEN
				vPassFail := 1;
				INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGDtlSeq,
                  'Lane detail for external parcel carrier cannot be created when external parcel integration is disabled on the lane',
                  4720199,
                  NULL);
			
			END IF;
			
	  ELSE
			SELECT COUNT(CC.CARRIER_ID) INTO vCount FROM CARRIER_CODE CC WHERE CC.CARRIER_ID = vCarrierId AND CC.CARRIER_TYPE_ID IS NOT NULL AND CC.CARRIER_TYPE_ID != 24;
			IF (vCount>0)
			THEN
				vPassFail := 1;
				INSERT_RG_LANE_DTL_ERROR (
                  vTCCompanyId,
                  vLaneId,
                  vRGDtlSeq,
                  'Lane detail for native parcel carrier cannot be created when external parcel integration is enabled on the lane',
                  4720201,
                  NULL);
			
			END IF;
			
			IF(vEpiServiceGroup IS NOT NULL)
			THEN
				SELECT COUNT(CC.CARRIER_ID) INTO vCount FROM CARRIER_CODE CC WHERE CC.CARRIER_ID = vCarrierId AND CC.CARRIER_TYPE_ID = 24;
				IF (vCount>0)
				THEN
					vPassFail := 1;
					INSERT_RG_LANE_DTL_ERROR (
						vTCCompanyId,
						vLaneId,
						vRGDtlSeq,
						'Lane detail for external or native parcel carrier cannot be created when EPI Service Group is specified on the lane',
						4720200,
						NULL);
				END IF;
				
			END IF;
			
	  END IF;
      RETURN vPassFail;
	  
   END VALIDATE_EPI_FOR_LANE_DTL;
   
   PROCEDURE UPDATE_ROUT_STATUS_FLAGS (
      vLaneID    IN COMB_LANE.LANE_ID%TYPE)
   IS
	  vLaneDtlCount       	NUMBER;
	  vLaneStatus			COMB_LANE.LANE_STATUS%TYPE;
      vEpiServiceGroup    	COMB_LANE.EPI_SERVICE_GROUP%TYPE;
	  vIsRouting			COMB_LANE.IS_ROUTING%TYPE;
   BEGIN
		
		SELECT COMB_LANE.EPI_SERVICE_GROUP, COMB_LANE.IS_ROUTING, COUNT(COMB_LANE_DTL.LANE_DTL_SEQ) 
		INTO vEpiServiceGroup, vIsRouting, vLaneDtlCount
		FROM COMB_LANE LEFT OUTER JOIN COMB_LANE_DTL ON COMB_LANE_DTL.LANE_ID=COMB_LANE.LANE_ID AND COMB_LANE_DTL.LANE_DTL_STATUS=0 AND COMB_LANE_DTL.IS_ROUTING=1 
		WHERE COMB_LANE.LANE_ID=vLaneID  
		GROUP BY COMB_LANE.EPI_SERVICE_GROUP, COMB_LANE.IS_ROUTING;
		
		IF( vEpiServiceGroup IS NULL AND vLaneDtlCount=0) 
		THEN
			IF(vIsRouting = 1) THEN
				UPDATE COMB_LANE SET IS_ROUTING=0 WHERE LANE_ID=vLaneID;
				COMMIT;
			END IF;
		ELSE 
			IF (vIsRouting = 0) THEN
				UPDATE COMB_LANE SET IS_ROUTING=1 WHERE LANE_ID=vLaneID;
				COMMIT;
			END IF;
		END IF;
		
		SELECT COMB_LANE.EPI_SERVICE_GROUP, COMB_LANE.LANE_STATUS, COUNT(COMB_LANE_DTL.LANE_DTL_SEQ) 
		INTO vEpiServiceGroup, vLaneStatus, vLaneDtlCount
		FROM COMB_LANE LEFT OUTER JOIN COMB_LANE_DTL ON  COMB_LANE_DTL.LANE_ID=COMB_LANE.LANE_ID AND COMB_LANE_DTL.LANE_DTL_STATUS=0 
		WHERE  COMB_LANE.LANE_ID=vLaneID  
		GROUP BY COMB_LANE.EPI_SERVICE_GROUP, COMB_LANE.LANE_STATUS; 
		
		IF(vEpiServiceGroup IS NULL AND vLaneDtlCount = 0) THEN
			IF(vLaneStatus = 0) THEN
				UPDATE COMB_LANE SET LANE_STATUS=1 WHERE LANE_ID=vLaneID;
				COMMIT;
			END IF;
		ELSE 
			IF(vLaneStatus = 1) THEN
				UPDATE COMB_LANE SET LANE_STATUS=0 WHERE LANE_ID=vLaneID;
				COMMIT;
			END IF;
		END IF;

   END UPDATE_ROUT_STATUS_FLAGS;

END RG_LANE_VALIDATION_PKG;
/
create or replace procedure wm_diff_605_runs
(
    p_facility_id           in facility.facility_id%type,
    p_user_id               in pix_tran.user_id%type,
    p_begin_tran_nbr        in pix_tran.tran_nbr%type,
    p_end_tran_nbr          in pix_tran.tran_nbr%type,
    p_adj_check             in varchar2 default 'A',
    p_result_set            out types.cursortype
)
as
    v_tran_nbr_exists        number(1); 
    v_tran_type              pix_tran.tran_type%type := '605';   
    v_tran_code              pix_tran.tran_code%type
        := (case when p_adj_check = 'A' then '97' else '98' end);
    v_begin_create_date_time date;
    v_end_create_date_time   date;
    v_current_date           date := sysdate;
begin
    delete from gen605_run_diff;
    
    if (p_adj_check not in ('A', 'U'))
    then
        raise_application_error(-20004, 'Invalid Adjustment check code value was passed: ' 
            || p_adj_check || '.  Valid values are A-Allocatble/U-Unallocatable');
    end if;

    if p_begin_tran_nbr >= p_end_tran_nbr
    then
        raise_application_error(-20001, 'The Begin TRAN_NBR ' || p_begin_tran_nbr 
            || ' should be less than the End TRAN_NBR ' || p_end_tran_nbr);
    end if;

    select case when exists
    (
        select 1
        from pix_tran pt
        where pt.tran_type = v_tran_type and pt.tran_code in ('97', '98')
            and pt.tran_nbr = p_begin_tran_nbr
    ) then 1 else 0 end
    into v_tran_nbr_exists
    from dual;

    if (v_tran_nbr_exists = 0)
    then
        raise_application_error(-20002, 'The Begin TRAN_NBR ' || p_begin_tran_nbr 
            || ' does not exist in PIX_TRAN table for the 605-97/98 PIX');
    end if;

    select case when exists
    (
        select 1
        from pix_tran pt
        where pt.tran_type = v_tran_type and pt.tran_code in ('97', '98')
            and pt.tran_nbr = p_end_tran_nbr
    ) then 1 else 0 end
    into v_tran_nbr_exists
    from dual;

    if (v_tran_nbr_exists = 0)
    then
        raise_application_error(-20003, 'The End TRAN_NBR ' || p_end_tran_nbr
            || ' does not exist in PIX_TRAN table for the 605-97/98 PIX');
    end if;

    -- get snapshot times
    select coalesce(min(pt.create_date_time), (sysdate + 999))
    into v_begin_create_date_time
    from pix_tran pt
    where pt.tran_type = v_tran_type and pt.tran_code in ('97', '98')
        and pt.tran_nbr = p_begin_tran_nbr;

    select coalesce(max(pt.create_date_time), (sysdate + 999))
    into v_end_create_date_time
    from pix_tran pt
    where pt.tran_type = v_tran_type and pt.tran_code in ('97', '98') 
        and pt.tran_nbr = p_end_tran_nbr;

    if (p_adj_check = 'A')
    then
        insert into gen605_run_diff  
        (
            item_id, item_season, item_season_year, item_style, item_style_sfx, item_color, 
            item_color_sfx, item_second_dim, item_quality, item_size_desc, size_range_code,
            size_rel_posn_in_table, start_invn, pix_invn, crtn_invn, end_invn, run_date, 
            begin_tran_nbr, end_tran_nbr
        )
        select pt.item_id, max(pt.season), max(pt.season_yr), max(pt.style), max(pt.style_sfx), 
            max(pt.color), max(pt.color_sfx), max(pt.sec_dim), max(pt.qual), max(pt.size_desc), 
            max(pt.size_range_code), max(pt.size_rel_posn_in_table), 
            coalesce(sum(case when pt.tran_nbr = p_begin_tran_nbr 
                then (pt.invn_adjmt_qty * decode(pt.invn_adjmt_type, 'S', -1, 1)) 
                else 0 end), 0) start_invn,
            coalesce(max(pt2.qty), 0) pix_invn, coalesce(max(ocd.units_pakd), 0) crtn_invn,
            coalesce(sum(case when pt.tran_nbr = p_end_tran_nbr 
                then (pt.invn_adjmt_qty * decode(pt.invn_adjmt_type, 'S', -1, 1)) 
                else 0 end), 0) end_invn,
            v_current_date, p_begin_tran_nbr, p_end_tran_nbr
        from pix_tran pt
        left join
        (
            select old.item_id, coalesce(sum(old.size_value), 0) units_pakd
            from outpt_lpn_detail old
            where old.created_dttm between v_begin_create_date_time and v_end_create_date_time
            group by old.item_id
        ) ocd on ocd.item_id = pt.item_id
        left join
        (
            select p.item_id, 
                coalesce(sum(p.invn_adjmt_qty * decode(p.invn_adjmt_type, 'S', -1, 1)), 0) qty
            from pix_tran p
            where p.tran_type in ('100', '200', '300')
                and p.create_date_time between v_begin_create_date_time and v_end_create_date_time
            group by p.item_id
        ) pt2 on pt2.item_id = pt.item_id
        where pt.tran_type = v_tran_type and pt.tran_code = v_tran_code 
            and pt.tran_nbr in (p_begin_tran_nbr, p_end_tran_nbr)
        group by pt.item_id;

        open p_result_set for
        select /*+ALL_ROWS*/ t.item_id, t.item_season, t.item_season_year, t.item_style, 
            t.item_style_sfx, t.item_color, t.item_color_sfx, t.item_second_dim, t.item_quality, 
            t.item_size_desc, t.size_range_code, t.size_rel_posn_in_table, t.start_invn, t.pix_invn,
            t.crtn_invn, t.end_invn, (t.start_invn + t.pix_invn - t.crtn_invn - t.end_invn) balance_qty
        from gen605_run_diff   t
        where t.run_date = v_current_date 
            and (t.start_invn + t.pix_invn - t.crtn_invn - t.end_invn) != 0
        order by t.item_id;
    else
        insert into gen605_run_diff  
        (
            item_id, item_season, item_season_year, item_style, item_style_sfx, item_color,
            item_color_sfx, item_second_dim, item_quality, item_size_desc, size_range_code,
            size_rel_posn_in_table, start_invn, pix_invn, end_invn, run_date, begin_tran_nbr,
            end_tran_nbr
        )
        select /*+ALL_ROWS*/ pt.item_id, max(pt.season), max(pt.season_yr), max(pt.style), 
            max(pt.style_sfx), max(pt.color), max(pt.color_sfx), max(pt.sec_dim), max(pt.qual),
            max(pt.size_desc), max(pt.size_range_code), max(pt.size_rel_posn_in_table),
            coalesce(sum(case when pt.tran_nbr = p_begin_tran_nbr 
                then (pt.invn_adjmt_qty * decode(pt.invn_adjmt_type, 'S', -1, 1)) 
                else 0 end), 0),
            coalesce(max(pt2.qty), 0),
            coalesce(sum(case when pt.tran_nbr = p_end_tran_nbr 
                then (pt.invn_adjmt_qty * decode(pt.invn_adjmt_type, 'S', -1, 1))
                else 0 end), 0),
            v_current_date, p_begin_tran_nbr, p_end_tran_nbr
        from pix_tran pt
        left join
        (
            select p.item_id, 
                coalesce(sum(p.invn_adjmt_qty * decode(p.invn_adjmt_type, 'S', -1, 1)), 0) qty
            from pix_tran p
            where p.tran_type = '606' 
                and p.create_date_time between v_begin_create_date_time and v_end_create_date_time
            group by p.item_id
        ) pt2 on pt2.item_id = pt.item_id
        where pt.tran_type = v_tran_type and pt.tran_code = v_tran_code 
            and pt.tran_nbr in (p_begin_tran_nbr, p_end_tran_nbr)
        group by pt.item_id;

        open p_result_set for
        select /*+ALL_ROWS*/ t.item_id, t.item_season, t.item_season_year, t.item_style, 
            t.item_style_sfx, t.item_color, t.item_color_sfx, t.item_second_dim, t.item_quality, 
            t.item_size_desc, t.size_range_code, t.size_rel_posn_in_table, t.start_invn, t.pix_invn,
            t.end_invn, (t.start_invn + t.pix_invn - t.end_invn) balance_qty
        from gen605_run_diff   t
        where t.run_date = v_current_date and (t.start_invn + t.pix_invn - t.end_invn) != 0
        order by t.item_id;
    end if;

    insert into msg_log
    (
        msg_log_id, module, msg_id, pgm_id, log_date_time, msg, create_date_time, mod_date_time,
        user_id, whse, cd_master_id
    )
    select msg_log_id_seq.nextval, '605', '9999', 'wm_diff_605_runs', sysdate,
        '605 comparison tool completed', sysdate, sysdate, p_user_id, f.whse, f.tc_company_id
    from facility f
    where f.facility_id = p_facility_id;

    commit;
end;
/
show errors;

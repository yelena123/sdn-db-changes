CREATE OR REPLACE 
PROCEDURE DUPLICATERFP (I_RFPID       NUMBER,
                        O_RFPID       NUMBER,
                        NEWRFPNAME    VARCHAR2,
                        NEWUID        NUMBER)
IS
BEGIN
   --c_quantity := 50;
   INSERT INTO rfp (rfpid,
                    tccompanyid,
                    rfpname,
                    modeid,
                    rfpstatus,
                    contractstartdt,
                    contractenddt,
                    bidclosedttm,
                    expectedawarddttm,
                    contactname,
                    contacttelephonenumber,
                    contactfaxnumber,
                    contactemailaddress,
                    commmethodid,
                    currencycode,
                    tpviewoptionid,
                    timezoneid,
                    comments,
                    awardcomment,
                    cancelcomment,
                    lanecount,
                    participantcount,
                    responsecount,
                    lastupdateduid,
                    lastupdateddttm,
                    createddttm,
                    posteddttm,
                    rfptype,
                    exporttoratingrouting,
                    basedatamode,
                    laneattributeversion,
                    bidattributeversion,
                    allowpackages,
                    awardtype,
                    onewaypricing,
                    collectcapacity,
                    uploadshipmenthistory,
                    tariffid,
                    viewall,
                    viewavg,
                    viewlow,
                    viewrnk,
                    complex_optimization,
                    invcarrierbylane,
                    rating_only,
                    alertcarrdttm,
                    alertemailcarr,
                    completeemailnotification,
                    abortemailnotification,
                    completestatusemailaddress,
                    abortedstatusemailaddress,
                    allow_specify_discounts,
					external_transittime )
      SELECT o_rfpid,
             tccompanyid,
             newrfpname,
             modeid,
             0,
             contractstartdt,
             contractenddt,
             bidclosedttm,
             expectedawarddttm,
             contactname,
             contacttelephonenumber,
             contactfaxnumber,
             contactemailaddress,
             commmethodid,
             currencycode,
             tpviewoptionid,
             timezoneid,
             comments,
             CAST (NULL AS VARCHAR2 (1)),
             CAST (NULL AS VARCHAR2 (1)),
             0,
             0,
             0,
             newuid,
             getdate (),
             getdate (),
             CAST (NULL AS VARCHAR2 (1)),
             rfptype,
             exporttoratingrouting,
             basedatamode,
             laneattributeversion,
             bidattributeversion,
             allowpackages,
             awardtype,
             onewaypricing,
             collectcapacity,
             uploadshipmenthistory,
             tariffid,
             viewall,
             viewavg,
             viewlow,
             viewrnk,
             complex_optimization,
             invcarrierbylane,
             rating_only,
             alertcarrdttm,
             alertemailcarr,
             completeemailnotification,
             abortemailnotification,
             completestatusemailaddress,
             abortedstatusemailaddress,
             allow_specify_discounts,
             external_transittime
        FROM rfp
       WHERE rfpid = i_rfpid
             AND round_num = (SELECT MAX (round_num)
                                FROM rfp
                               WHERE rfpid = i_rfpid);

   INSERT INTO rfpcolproperties (rfpid,
                                 colname,
                                 datatable,
                                 displayname,
                                 minvalue,
                                 maxvalue,
                                 groupid,
                                 iscarrierviewable,
                                 isrequired,
                                 iscustom,
                                 datatype,
                                 coldisplayorder)
      SELECT o_rfpid,
             colname,
             datatable,
             displayname,
             minvalue,
             maxvalue,
             groupid,
             iscarrierviewable,
             isrequired,
             iscustom,
             datatype,
             coldisplayorder
        FROM rfpcolproperties rp1
       WHERE rfpid = i_rfpid
             AND round_num =
                    (SELECT MAX (round_num)
                       FROM rfpcolproperties
                      WHERE     rfpid = i_rfpid
                            AND colname = rp1.colname
                            AND datatable = rp1.datatable);

   INSERT INTO lanerfpcolproperties (RFPID,
                                     LANEID,
                                     COLNAME,
                                     DATATABLE,
                                     ROUND_NUM)
      SELECT o_rfpid,
             LANEID,
             COLNAME,
             DATATABLE,
             1
        FROM lanerfpcolproperties lrc
       WHERE rfpid = i_rfpid
             AND round_num =
                    (SELECT MAX (round_num)
                       FROM lanerfpcolproperties
                      WHERE     rfpid = i_rfpid
                            AND colname = lrc.colname
                            AND datatable = lrc.datatable);

   INSERT INTO rfplineitemcolproperties (RFPID,
                                         COLNAME,
                                         DATATABLE,
                                         ROUND_NUM,
                                         CONTRACT_TYPE,
                                         ISREQUIRED)
      SELECT o_rfpid,
             COLNAME,
             DATATABLE,
             1,
             CONTRACT_TYPE,
             ISREQUIRED
        FROM rfplineitemcolproperties rlc
       WHERE rfpid = i_rfpid
             AND round_num =
                    (SELECT MAX (round_num)
                       FROM rfplineitemcolproperties
                      WHERE     rfpid = i_rfpid
                            AND colname = rlc.colname
                            AND datatable = rlc.datatable);

   INSERT INTO rfpcustompicklist (rfpid,
                                  colname,
                                  datatable,
                                  VALUE,
                                  displayorder,
                                  round_num)
      SELECT o_rfpid,
             colname,
             datatable,
             rcp.VALUE,
             displayorder,
             1
        FROM rfpcustompicklist rcp
       WHERE rfpid = i_rfpid
             AND round_num =
                    (SELECT MAX (round_num)
                       FROM rfpcustompicklist rcp1
                      WHERE rfpid = i_rfpid
                            AND rcp1.datatable = rcp.datatable);

   INSERT INTO rfpspecreq (rfpid,
                           specreqid,
                           VALUE,
                           datatype,
                           isawarded)
      SELECT o_rfpid,
             specreqid,
             VALUE,
             datatype,
             isawarded
        FROM rfpspecreq
       WHERE rfpid = i_rfpid
             AND specreqid NOT IN (SELECT specreqid
                                     FROM specreqentry
                                    WHERE specreqheadingid = 1)
             AND NOT EXISTS
                        (SELECT 1
                           FROM specreqentry
                          WHERE specreqentry.specreqid = rfpspecreq.specreqid
                                AND specreqentry.deleted > 0);

   FOR C1
      IN (SELECT srepc2.specreqid AS newid,
                 srepc1.specreqid AS oldid,
                 srepc1.specreqdescription AS olddescription,
                 srepc1.tcdatatype AS tcdatatype,
                 srepc1.tpdatatype AS tpdatatype
            FROM specreqentryprofilecustom srepc1,
                 specreqentryprofilecustom srepc2
           WHERE     srepc1.rfpid = i_rfpid
                 AND srepc2.rfpid = o_rfpid
                 AND srepc1.customseq = srepc2.customseq)
   LOOP
      UPDATE specreqentryprofilecustom
         SET specreqdescription = C1.olddescription
       WHERE specreqid = C1.newid;

      UPDATE specreqentryprofilecustom
         SET tcdatatype = C1.tcdatatype
       WHERE specreqid = C1.newid;

      UPDATE specreqentryprofilecustom
         SET tpdatatype = C1.tpdatatype
       WHERE specreqid = C1.newid;

      UPDATE rfpspecreq
         SET specreqid = C1.newid
       WHERE specreqid = C1.oldid AND rfpid = o_rfpid;

      UPDATE rfpcustompicklist
         SET colname = RTRIM (TO_CHAR (C1.newid))
       WHERE     colname = TO_CHAR (C1.oldid)
             AND rfpid = o_rfpid
             AND UPPER (datatable) = UPPER ('sr');
   END LOOP;

   INSERT INTO rfpparticipant (rfpid,
                               obcarriercodeid,
                               tpcompanyid,
                               createduid,
                               createddttm,
                               incumbent,
                               is_additional)
      SELECT o_rfpid,
             obcarriercodeid,
             tpcompanyid,
             newuid,
             getdate (),
             incumbent,
             is_additional
        FROM rfpparticipant
       WHERE rfpid = i_rfpid
             AND round_num =
                    (SELECT MAX (round_num)
                       FROM rfpparticipant rp1
                      WHERE rp1.rfpid = i_rfpid
                            AND rp1.obcarriercodeid =
                                   rfpparticipant.obcarriercodeid);

   INSERT INTO RFPLANEPARTICIPANT (rfpid, obcarriercodeid, laneid)
      SELECT O_RFPID, obcarriercodeid, laneid
        FROM RFPLANEPARTICIPANT
       WHERE rfpid = I_RFPID
             AND round_num =
                    (SELECT MAX (round_num)
                       FROM RFPPARTICIPANT rp1
                      WHERE rp1.rfpid = I_RFPID
                            AND rp1.obcarriercodeid =
                                   RFPLANEPARTICIPANT.OBCARRIERCODEID);
								   
    INSERT INTO UNINVITEDCARRIERLIST (Rfpid, Tpcompanyid, Userid)
       SELECT O_Rfpid, Tpcompanyid, Userid
           FROM UNINVITEDCARRIERLIST
     WHERE rfpid = i_rfpid;    

   INSERT INTO lane (rfpid,
                     laneid,
                     originshipsite,
                     originshipsitecode,
                     originport,
                     origincity,
                     originstateprov,
                     origincountrycode,
                     originpostalcode,
                     destinationshipsite,
                     destinationshipsitecode,
                     destinationport,
                     destinationcity,
                     destinationstateprov,
                     destinationcountrycode,
                     destinationpostalcode,
                     customername,
                     commoditycode,
                     commoditycodedesc,
                     harmonizedcode,
                     ratetype,
                     numstops,
                     numcontainers,
                     isroundtrip,
                     averageweight,
                     volumefrequency,
                     volume,
                     weightbreak1,
                     weightbreak2,
                     weightbreak3,
                     weightbreak4,
                     weightbreak5,
                     weightbreak6,
                     weightbreak7,
                     weightbreak8,
                     weightbreak9,
                     weightuom,
                     weightbreakuom,
                     mileage,
                     distanceuom,
                     lclcubicspace,
                     lclcubicspaceuom,
                     pickuptype,
                     servicecriteria,
                     freightclass,
                     routingtype,
                     isconference,
                     transittime,
                     transittimeuom,
                     isoversized,
                     oversizednote,
                     ishazardous,
                     hazardousnote,
                     isperishable,
                     perishablenote,
                     other1,
                     other2,
                     other3,
                     iscancelled,
                     isdirty,
                     createduid,
                     createddttm,
                     lastupdateduid,
                     lastupdateddttm,
                     custtext1,
                     custtext2,
                     custtext3,
                     custtext4,
                     custtext5,
                     custtext6,
                     custtext7,
                     custtext8,
                     custtext9,
                     custtext10,
                     custint1,
                     custint2,
                     custint3,
                     custint4,
                     custint5,
                     custint6,
                     custint7,
                     custint8,
                     custint9,
                     custint10,
                     custdouble1,
                     custdouble2,
                     custdouble3,
                     custdouble4,
                     custdouble5,
                     custdouble6,
                     custdouble7,
                     custdouble8,
                     custdouble9,
                     custdouble10,
                     originfacilitycode,
                     originairport,
                     destinationfacilitycode,
                     destinationairport,
                     historicalcost,
                     weightbreakratetype,
                     originzonecode,
                     destinationzonecode,
                     lane_type,
                     comblaneid,
                     shipmentratedcost,
                     shipmentvolume,
                     shipmentvolumeerror,
                     season_id,
                     equipmenttype_id,
                     servicetype_id,
                     protectionlevel_id,
                     mot,
                     BUSINESS_UNIT_ID,
                     laneattributeversion,
					 Origincapacityareacode,
					 Destinationcapacityareacode)
      SELECT o_rfpid,
             laneid,
             originshipsite,
             originshipsitecode,
             originport,
             origincity,
             originstateprov,
             origincountrycode,
             originpostalcode,
             destinationshipsite,
             destinationshipsitecode,
             destinationport,
             destinationcity,
             destinationstateprov,
             destinationcountrycode,
             destinationpostalcode,
             customername,
             commoditycode,
             commoditycodedesc,
             harmonizedcode,
             ratetype,
             numstops,
             numcontainers,
             isroundtrip,
             averageweight,
             volumefrequency,
             volume,
             weightbreak1,
             weightbreak2,
             weightbreak3,
             weightbreak4,
             weightbreak5,
             weightbreak6,
             weightbreak7,
             weightbreak8,
             weightbreak9,
             weightuom,
             weightbreakuom,
             mileage,
             distanceuom,
             lclcubicspace,
             lclcubicspaceuom,
             pickuptype,
             servicecriteria,
             freightclass,
             routingtype,
             isconference,
             transittime,
             transittimeuom,
             isoversized,
             oversizednote,
             ishazardous,
             hazardousnote,
             isperishable,
             perishablenote,
             other1,
             other2,
             other3,
             CAST (NULL AS NUMBER),
             isdirty,
             newuid,
             getdate (),
             newuid,
             getdate (),
             custtext1,
             custtext2,
             custtext3,
             custtext4,
             custtext5,
             custtext6,
             custtext7,
             custtext8,
             custtext9,
             custtext10,
             custint1,
             custint2,
             custint3,
             custint4,
             custint5,
             custint6,
             custint7,
             custint8,
             custint9,
             custint10,
             custdouble1,
             custdouble2,
             custdouble3,
             custdouble4,
             custdouble5,
             custdouble6,
             custdouble7,
             custdouble8,
             custdouble9,
             custdouble10,
             originfacilitycode,
             originairport,
             destinationfacilitycode,
             destinationairport,
             historicalcost,
             weightbreakratetype,
             originzonecode,
             destinationzonecode,
             lane_type,
             comblaneid,
             shipmentratedcost,
             shipmentvolume,
             shipmentvolumeerror,
             season_id,
             equipmenttype_id,
             servicetype_id,
             protectionlevel_id,
             mot,
             BUSINESS_UNIT_ID,
             laneattributeversion,
			 Origincapacityareacode,
			 Destinationcapacityareacode
        FROM lane
       WHERE rfpid = i_rfpid
             AND round_num =
                    (SELECT MAX (round_num)
                       FROM lane l1
                      WHERE l1.rfpid = i_rfpid AND l1.laneid = lane.laneid)
             AND lane_type IN (0, 1);

   INSERT INTO lane_active (rfpid,
                            laneid,
                            round_num,
                            active)
      SELECT DISTINCT o_rfpid,
                      laneid,
                      1,
                      1
        FROM lane
       WHERE rfpid = o_rfpid AND lane_type IN (0, 1);

   INSERT INTO RFPMODE (tccompanyid,
                        rfpid,
                        transportmodeid,
                        description)
      SELECT tccompanyid,
             o_rfpid,
             transportmodeid,
             description
        FROM rfpmode
       WHERE rfpid = I_RFPID;

   INSERT INTO RFPBASEMODE (tccompanyid,
                            rfpid,
                            mot_code,
                            mot_id,
                            description)
      SELECT tccompanyid,
             o_rfpid,
             mot_code,
             mot_id,
             description
        FROM rfpbasemode
       WHERE rfpid = I_RFPID;

   INSERT INTO laneequipmenttype (laneequipmenttypeid,
                                  laneid,
                                  rfpid,
                                  equipmenttype_id,
                                  volume,
                                  frequency,
                                  servicetype_id,
                                  protectionlevel_id,
                                  mot,
                                  commoditycode_id,
                                  createduid,
                                  createddttm,
                                  lastupdateduid,
                                  lastupdateddttm)
      SELECT laneequipmenttype_sequence.NEXTVAL,
             le.laneid,
             o_rfpid,
             le.equipmenttype_id,
             le.volume,
             le.frequency,
             le.servicetype_id,
             le.protectionlevel_id,
             le.mot,
             le.commoditycode_id,
             newuid,
             getdate (),
             newuid,
             getdate ()
        FROM laneequipmenttype le, lane l
       WHERE     le.rfpid = i_rfpid
             AND l.rfpid = i_rfpid
             AND le.laneid = l.laneid
             AND l.lane_type IN (0, 1)
             AND l.round_num =
                    (SELECT MAX (round_num)
                       FROM lane l1
                      WHERE l1.rfpid = l.rfpid AND l1.laneid = l.laneid);
					  
	INSERT INTO EXTERNALTRANSITTIME (rfpid,
                            Laneid,
                            Carriercode,
                            Transittime,
                            Createddttm,
                            lastupdateddttm)
      SELECT o_rfpid,
             Laneid,
             Carriercode,
             Transittime,
             getdate (),
             getdate ()
        From EXTERNALTRANSITTIME
       WHERE rfpid = I_RFPID;

   INSERT INTO package (packageid,
                        rfpid,
                        obcarriercodeid,
                        packagenumber,
                        totalvolume,
                        awardstatus,
                        packagename)
      SELECT package_sequence.NEXTVAL,
             o_rfpid c_rfp,
             obcarriercodeid,
             packagenumber,
             totalvolume,
             awardstatus,
             packagename
        FROM package
       WHERE rfpid = i_rfpid AND obcarriercodeid IS NULL
             AND packageid NOT IN
                    (SELECT packageid
                       FROM packagelane pl, lane l
                      WHERE     pl.rfpid = l.rfpid
                            AND l.rfpid = i_rfpid
                            AND pl.laneid = l.laneid
                            AND l.lane_type = 2);

   INSERT INTO packagelane (packageid, rfpid, laneid)
      SELECT newp.packageid, o_rfpid, pl.laneid
        FROM packagelane pl, package p, package newp
       WHERE     p.packageid = pl.packageid
             AND p.rfpid = i_rfpid
             AND p.obcarriercodeid IS NULL
             AND newp.rfpid = o_rfpid
             AND (newp.packagename = p.packagename)
             AND laneid NOT IN (SELECT laneid
                                  FROM lane
                                 WHERE rfpid = i_rfpid AND lane_type = 2);

   INSERT INTO capacityselectionrfp (rfpid,
                                     weeklycapacityselected,
                                     surgecapacityselected,
                                     cust0name,
                                     cust0selected,
                                     cust1name,
                                     cust1selected,
                                     cust2name,
                                     cust2selected,
                                     cust3name,
                                     cust3selected,
                                     cust4name,
                                     cust4selected,
                                     cust5name,
                                     cust5selected,
                                     cust6name,
                                     cust6selected,
                                     cust7name,
                                     cust7selected,
                                     cust8name,
                                     cust8selected,
                                     cust9name,
                                     cust9selected)
      SELECT o_rfpid,
             weeklycapacityselected,
             surgecapacityselected,
             cust0name,
             cust0selected,
             cust1name,
             cust1selected,
             cust2name,
             cust2selected,
             cust3name,
             cust3selected,
             cust4name,
             cust4selected,
             cust5name,
             cust5selected,
             cust6name,
             cust6selected,
             cust7name,
             cust7selected,
             cust8name,
             cust8selected,
             cust9name,
             cust9selected
        FROM capacityselectionrfp
       WHERE rfpid = i_rfpid
             AND round_num = (SELECT MAX (round_num)
                                FROM capacityselectionrfp csr1
                               WHERE csr1.rfpid = i_rfpid);

   INSERT INTO capacityselectionfacility (rfpid,
                                          facilitycode,
                                          direction,
                                          weeklycapacity_sel,
                                          surgecapacity_sel,
                                          cust0selected,
                                          cust1selected,
                                          cust2selected,
                                          cust3selected,
                                          cust4selected,
                                          cust5selected,
                                          cust6selected,
                                          cust7selected,
                                          cust8selected,
                                          cust9selected,
										  areacode,
										  City,
										  State,
										  Postalcode,
										  Country)
      SELECT o_rfpid,
             facilitycode,
             direction,
             weeklycapacity_sel,
             surgecapacity_sel,
             cust0selected,
             cust1selected,
             cust2selected,
             cust3selected,
             cust4selected,
             cust5selected,
             cust6selected,
             cust7selected,
             cust8selected,
             cust9selected,
			 areacode,
			 City,
			 State,
			 Postalcode,
			 Country 
        FROM capacityselectionfacility
       WHERE rfpid = i_rfpid
             AND round_num =
                    (SELECT MAX (round_num)
                       FROM capacityselectionfacility csf1
                      WHERE csf1.rfpid = i_rfpid
                             AND (Csf1.Facilitycode =Capacityselectionfacility.Facilitycode OR Csf1.AreaCode =Capacityselectionfacility.AreaCode)
                            AND csf1.direction =
                                   capacityselectionfacility.direction);

   INSERT INTO laneaccessorialcolproperties (freightchargetypeid,
                                             rfpid,
                                             colname,
                                             displayname,
                                             minvalue,
                                             maxvalue,
                                             isinclusive,
                                             isrequired,
                                             iscustom,
                                             displayorder,
                                             round_num,
                                             accid)
      SELECT laneaccessorial_sequence.NEXTVAL,
             o_rfpid,
             colname,
             displayname,
             minvalue,
             maxvalue,
             isinclusive,
             isrequired,
             iscustom,
             displayorder,
             1 one,
             accid
        FROM laneaccessorialcolproperties lac
       WHERE rfpid = i_rfpid
             AND round_num =
                    (SELECT MAX (round_num)
                       FROM laneaccessorialcolproperties lac1
                      WHERE lac1.rfpid = lac.rfpid
                            AND lac1.colname = lac.colname);

   INSERT INTO rfpeffectiverate (rfpid,
                                 calculation_type,
                                 mot,
                                 rate_calculation,
                                 ruleset_id,
                                 ruleset_description)
      SELECT o_rfpid,
             calculation_type,
             mot,
             rate_calculation,
             ruleset_id,
             ruleset_description
        FROM rfpeffectiverate
       WHERE rfpid = i_rfpid;

   INSERT INTO effectiverate (rfpid,
                              calculation_method,
                              multiplier,
                              weight,
                              name,
                              isaccessorial,
                              mot)
      SELECT o_rfpid,
             calculation_method,
             multiplier,
             weight,
             name,
             isaccessorial,
             mot
        FROM effectiverate
       WHERE rfpid = i_rfpid;

   INSERT INTO ob200contract_type (contract_type,
                                   name,
                                   equipmenttype_id,
                                   servicetype_id,
                                   protectionlevel_id,
                                   mot,
                                   commoditycode_id,
                                   rfpid)
      SELECT contract_type,
             name,
             equipmenttype_id,
             servicetype_id,
             protectionlevel_id,
             mot,
             commoditycode_id,
             o_rfpid
        FROM ob200contract_type
       WHERE rfpid = i_rfpid;

   duplicate_historical_lanebids (i_rfpid, o_rfpid);

   DELETE FROM rfpparticipant
         WHERE     is_additional = 1
               AND obcarriercodeid NOT IN (SELECT obcarriercodeid
                                             FROM lanebid
                                            WHERE rfpid = o_rfpid)
               AND rfpid = o_rfpid;
END DUPLICATERFP;
/
create or replace procedure manh_wave_sku_invn
(
    p_sku_cnstr          in wave_parm.sku_cnstr%type,
    p_sku_sub            in wave_parm.sku_sub%type,
    p_pull_all_swc       in wave_parm.pull_all_swc%type,
    p_reject_distro_rule in wave_parm.reject_distro_rule%type,
    p_zero               in number,
    p_scale              in number,
    p_facility_id        in facility.facility_id%type
)
as
    v_qty_shortage          number(13,4) := 0;
    v_qty_found             number(13,4) := 0;
    v_qty_available         number(13,4) := 0;
    v_is_line_rejected      number(1) := 0;
    v_max_log_lvl           number(1) := wm_get_curr_log_lvl();
begin
    -- when order is vas-complete, unconditionally soft-allocate vas (complete)
    -- lines; no partial allocation is allowed for vas
-- todo: this is a low-cardinality column
    insert into tmp_ord_dtl_sku_invn
    (
        line_item_id, item_id, invn_type, prod_stat, cntry_of_orgn, batch_nbr,
        item_attr_1, item_attr_2, item_attr_3, item_attr_4, item_attr_5, 
        order_id, rng_shortage, is_flushed, qty_soft_alloc, is_split_line
    )
    select t.line_item_id, t.item_id, t.invn_type, t.prod_stat, t.cntry_of_orgn,
        t.batch_nbr, t.item_attr_1, t.item_attr_2, t.item_attr_3, t.item_attr_4,
        t.item_attr_5, t.order_id, 0 rng_shortage, 0 is_flushed,
        round(t.need_qty, p_scale) qty_soft_alloc,
        case t.do_dtl_status when 120 then 1 else 0 end is_split_line
    from tmp_wave_selected_orders t
    where t.perform_vas = 2;
    wm_cs_log('Bypassed SA for VAS complete lines ' || sql%rowcount, p_sql_log_level => 1);

    -- regular soft alloc process for non-vas or vas-pending lines
    for oli_rec in
    (
        select t.order_id order_id, t.line_item_id line_item_id, 
            t.order_qty, t.allocated_qty, t.need_qty need_qty,
            t.pre_pack_flag pre_pack_flag, t.ship_group_id ship_group_id,
            t.sku_sub_code_id sku_sub_code_id, t.invn_type invn_type,
            t.sku_sub_code_value sku_sub_code_value, t.item_attr_1 item_attr_1,
            t.do_dtl_status do_dtl_status, t.item_attr_2 item_attr_2,
            t.item_attr_3 item_attr_3, t.item_attr_4 item_attr_4, 
            t.item_attr_5 item_attr_5, t.batch_nbr batch_nbr, t.perform_vas,
            t.cntry_of_orgn cntry_of_orgn, t.prod_stat prod_stat,
            t.batch_requirement_type batch_reqmt_type, t.item_id item_id
        from tmp_wave_selected_orders t
        where t.perform_vas < 2
        order by t.id
    )
    loop
        -- check for prior group-based rejections; if so, skip this line
        v_is_line_rejected := 0;
        if (p_pull_all_swc = 'Y' and oli_rec.ship_group_id is not null)
        then
            select case when exists
            (
                select 1
                from tmp_wave_group_rejected_swcs t
                where t.ship_group_id = oli_rec.ship_group_id
            ) then 1 else 0 end
            into v_is_line_rejected
            from dual;
            if (v_max_log_lvl >= 2)
            then
                wm_cs_log('Line ' || oli_rec.line_item_id || ' rejected due to prior swc');
            end if;
        end if;

        if (v_is_line_rejected = 0 and p_reject_distro_rule = '1')
        then
            select case when exists
            (
                select 1
                from tmp_wave_group_rejected_orders t
                where t.order_id = oli_rec.order_id
            ) then 1 else 0 end
            into v_is_line_rejected
            from dual;
            if (v_max_log_lvl >= 2)
            then
                wm_cs_log('Line ' || oli_rec.line_item_id || ' rejected due to prior order');
            end if;

        elsif (v_is_line_rejected = 0 and p_reject_distro_rule = '2')
        then
            select case when exists
            (
                select 1
                from tmp_wave_group_rejected_items t
                where t.item_id = oli_rec.item_id
            ) then 1 else 0 end
            into v_is_line_rejected
            from dual;
            if (v_max_log_lvl >= 2)
            then
                wm_cs_log('Line ' || oli_rec.line_item_id || ' rejected due to prior item');
            end if;
        end if;

        if (v_is_line_rejected = 0)
        then
            v_qty_shortage := round(oli_rec.need_qty, p_scale);
            if (oli_rec.pre_pack_flag = 1)
            then
                manh_wave_soft_allocate_ppack(oli_rec.order_id,
                    oli_rec.line_item_id, p_pull_all_swc, p_sku_cnstr, 
                    oli_rec.ship_group_id, oli_rec.batch_reqmt_type, p_scale,
                    p_zero, v_max_log_lvl, v_qty_shortage);
            else
                v_qty_found := 0;
                manh_wave_get_sku_invn_qty(oli_rec.item_id, 
                    oli_rec.invn_type, oli_rec.prod_stat, 
                    oli_rec.batch_nbr, oli_rec.item_attr_1, 
                    oli_rec.item_attr_2, oli_rec.item_attr_3, 
                    oli_rec.item_attr_4, oli_rec.item_attr_5, 
                    oli_rec.cntry_of_orgn, oli_rec.batch_reqmt_type, 
                    v_qty_shortage, p_scale, p_zero, v_max_log_lvl, v_qty_available);

                if (v_qty_available > p_zero)
                then
                    if (v_qty_available - v_qty_shortage > p_zero)
                    then
                        v_qty_found := v_qty_shortage;
                        v_qty_shortage := 0;
                    else
                        v_qty_found := v_qty_available;
                        v_qty_shortage := v_qty_shortage - v_qty_available;
                    end if;

                    -- if the line was partially alloc to begin with, then split
                    -- it; this should work for regular/pre waves
                    insert into tmp_ord_dtl_sku_invn
                    (
                        line_item_id, item_id, invn_type, prod_stat, 
                        cntry_of_orgn, batch_nbr, item_attr_1, item_attr_2, 
                        item_attr_3, item_attr_4, item_attr_5, qty_soft_alloc,
                        order_id, rng_shortage, is_flushed, ship_group_id, 
                        is_split_line
                    )
                    values
                    (
                        oli_rec.line_item_id, oli_rec.item_id, 
                        oli_rec.invn_type, oli_rec.prod_stat, 
                        oli_rec.cntry_of_orgn, oli_rec.batch_nbr,
                        oli_rec.item_attr_1, oli_rec.item_attr_2, 
                        oli_rec.item_attr_3, oli_rec.item_attr_4, 
                        oli_rec.item_attr_5, v_qty_found,
                        oli_rec.order_id, v_qty_shortage, 0, 
                        oli_rec.ship_group_id,
                        case oli_rec.do_dtl_status when 120 then 1 else 0 end
                    );
                    if (v_max_log_lvl >= 2)
                    then
                        wm_cs_log('SA ' || v_qty_found || ' units of item ' 
                            || oli_rec.item_id || ' for line ' 
                            || oli_rec.line_item_id || ' against a reqmt of ' 
                            || to_char(oli_rec.need_qty));
                    end if;
                end if; -- orig sku qty is available

                -- if shortage still present, attempt to substitute
                if (v_qty_shortage > p_zero and p_sku_sub in ('1', '2'))
                then
                    manh_wave_sku_substitution(oli_rec.order_id, 
                        oli_rec.line_item_id, oli_rec.ship_group_id,
                        p_sku_sub, oli_rec.item_id, oli_rec.invn_type,
                        oli_rec.prod_stat, oli_rec.batch_nbr, 
                        oli_rec.item_attr_1, oli_rec.item_attr_2,
                        oli_rec.item_attr_3, oli_rec.item_attr_4,
                        oli_rec.item_attr_5, oli_rec.cntry_of_orgn, 
                        oli_rec.batch_reqmt_type, oli_rec.sku_sub_code_id,
                        oli_rec.sku_sub_code_value, p_scale, p_zero,
                        p_facility_id, v_max_log_lvl, v_qty_shortage);
                end if;
            end if; -- its a prepack line

            if (v_qty_shortage > p_zero)
            then
                if (p_pull_all_swc = 'Y' and oli_rec.ship_group_id is not null and p_sku_cnstr != 2)
                then
                    -- remove prior soft allocs for this entire ship grp
                    delete from tmp_ord_dtl_sku_invn t
                    where t.ship_group_id = oli_rec.ship_group_id;
                    if (v_max_log_lvl >= 2)
                    then
                        wm_cs_log('Removed ' || sql%rowcount || ' SA''s for line ' 
                            || oli_rec.line_item_id || ' with swc ' || oli_rec.ship_group_id
                            || ' due to a shortage of ' || v_qty_shortage || ', need was '
                            || to_char(oli_rec.need_qty));
                    end if;

                    -- we now know that this entire swc needs to be deselected
                    insert into tmp_wave_group_rejected_swcs
                    (
                        order_id, line_item_id, ship_group_id
                    )
                    values 
                    (
                        oli_rec.order_id, oli_rec.line_item_id, 
                        oli_rec.ship_group_id
                    );
                elsif (p_sku_cnstr = '1' or oli_rec.perform_vas > 0)
                then
                    -- this order line and any alloc/splits/subs needs to be 
                    -- deselected from the wave; free up qty for other orders
                    delete from tmp_ord_dtl_sku_invn t
                    where t.order_id = oli_rec.order_id
                        and t.line_item_id = oli_rec.line_item_id;
                    if (v_max_log_lvl >= 2)
                    then
                        wm_cs_log('Removed ' || sql%rowcount || ' SA''s for line ' 
                            || oli_rec.line_item_id || ' due to a shortage of ' 
                            || v_qty_shortage || ', need was '
                            || to_char(oli_rec.need_qty));
                    end if;
                end if;

                -- fulfillment for a vas-completed line is expected to be full
                -- (not partial); any deviation from that is a problem; so
                -- we deselect those lines too
                if (p_sku_cnstr = '1' or oli_rec.perform_vas > 0
                    or (round(oli_rec.need_qty, p_scale) - v_qty_shortage <= p_zero))
                then
                    -- we've 'failed' to allocate
                    if (p_reject_distro_rule = '1')
                    then
                        insert into tmp_wave_group_rejected_orders
                        (
                            order_id, line_item_id
                        )
                        values 
                        (
                            oli_rec.order_id, oli_rec.line_item_id
                        );

                        delete from tmp_ord_dtl_sku_invn t
                        where t.order_id = oli_rec.order_id;
                        if (v_max_log_lvl >= 2)
                        then
                            wm_cs_log('Removed ' || sql%rowcount || ' SA''s for order ' 
                                || oli_rec.order_id || ' due to a shortage of ' 
                                || v_qty_shortage || ' for line ' || oli_rec.line_item_id
                                || ', need was ' || to_char(oli_rec.need_qty));
                        end if;
                    elsif (p_reject_distro_rule = '2')
                    then
                        insert into tmp_wave_group_rejected_items
                        (
                            order_id, line_item_id, item_id
                        )
                        values 
                        (
                            oli_rec.order_id, oli_rec.line_item_id,
                            oli_rec.item_id
                        );

                        delete from tmp_ord_dtl_sku_invn t
                        where t.item_id = oli_rec.item_id;
                        if (v_max_log_lvl >= 2)
                        then
                            wm_cs_log('Removed ' || sql%rowcount || ' SA''s for item ' 
                                || oli_rec.item_id || ' due to a shortage of ' 
                                || v_qty_shortage || ' for line ' || oli_rec.line_item_id
                                || ', need was ' || to_char(oli_rec.need_qty));
                        end if;
                    end if;
                end if;

                -- if the line is going to be cancelled 'routinely' later, dont
                -- write a cancelled line for the shorted qty, below
                if (p_sku_cnstr = '2' and oli_rec.perform_vas = 0
                    and (oli_rec.allocated_qty > p_zero
                        or oli_rec.need_qty - v_qty_shortage > p_zero
                        or oli_rec.order_qty - oli_rec.allocated_qty - oli_rec.need_qty > p_zero))
                then
                    -- record the shorted qty on a special canceled split line
                    insert into tmp_ord_dtl_sku_invn
                    (
                        line_item_id, item_id, invn_type, prod_stat, 
                        cntry_of_orgn, batch_nbr, item_attr_1, item_attr_2, 
                        item_attr_3, item_attr_4, item_attr_5, qty_soft_alloc,
                        order_id, rng_shortage, is_flushed, ship_group_id, 
                        is_split_line
                    )
                    values
                    (
                        oli_rec.line_item_id, oli_rec.item_id, 
                        oli_rec.invn_type, oli_rec.prod_stat, 
                        oli_rec.cntry_of_orgn, oli_rec.batch_nbr,
                        oli_rec.item_attr_1, oli_rec.item_attr_2, 
                        oli_rec.item_attr_3, oli_rec.item_attr_4, 
                        oli_rec.item_attr_5, 0, oli_rec.order_id, v_qty_shortage,
                        0, oli_rec.ship_group_id, 4
                    );
                    if (v_max_log_lvl >= 2)
                    then
                        wm_cs_log('Shorted ' || v_qty_shortage || ' units of item '
                            || oli_rec.item_id || ' for line '
                            || oli_rec.line_item_id || ' against a reqmt of '
                            || to_char(oli_rec.need_qty));
                    end if;
                end if;
            end if; -- shortage exists
        end if; -- not rejected
    end loop; -- oli loop
end;
/
show errors;

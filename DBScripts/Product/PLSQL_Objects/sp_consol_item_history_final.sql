CREATE OR REPLACE PROCEDURE sp_consol_item_history_final
(
    p_whse    in locn_hdr.whse%type
)AS
   v_iloopcontrol           NUMBER (1, 0);
   v_inextrowid             NUMBER (19, 0);
   v_imaxrowid              NUMBER (19, 0);
   v_icurrentrowid          NUMBER (10, 0);
   v_slot_ship_unit         NUMBER (10, 0);
   v_item_ship_unit         NUMBER (10, 0);
   v_hist_id                NUMBER (19, 0);
   v_sku_id                 NUMBER (19, 0);
   v_start_date             TIMESTAMP (3);
   v_end_date               TIMESTAMP (3);
   v_hist_match             VARCHAR2 (40);
   v_mult_loc_grp           VARCHAR2 (40);
   v_min_slotitem_id        NUMBER (19, 0);
   v_no_of_slot_item_recs   NUMBER (10, 0);
   v_errcode                NUMBER (10, 0);
   v_if_exists              NUMBER (10, 0);
   swv_ret_stat             NUMBER (10, 0);
   swv_rcur                 SYS_REFCURSOR;
   swc_current              SYS_REFCURSOR;
   variable_t               VARCHAR2 (50);
   v_cnt                    PLS_INTEGER := 0;
   table_name               VARCHAR2 (50);
-- case_2 varchar2(50);
BEGIN
   v_errcode := 0;
   variable_t := 0;

   DBMS_OUTPUT.enable (buffer_size => NULL);                      -- slot-4302

   DELETE FROM tt_temp_slot_item_slotted;

   DELETE FROM tt_temp_slot_item_unslotted;

   DELETE FROM tt_temp_slot_item_interested;

   DELETE FROM tt_temp_item_history_unslotted;

   DELETE FROM tt_temp_item_history_smry;

   DELETE FROM tt_temp_item_history_inst;

   DELETE FROM tt_temp_neg_mvmt_slot_item;

   DELETE FROM tt_temp_neg_inv_slot_item;

   DELETE FROM tt_temp_neg_hits_slot_item;

   INSERT INTO tt_temp_neg_mvmt_slot_item (slotitem_id, start_date, end_date)
      SELECT ih.slotitem_id, ih.start_date, ih.end_date
        FROM item_history ih 
        left join slot_item si on si.SLOTITEM_ID = ih.SLOTITEM_ID 
        left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
      WHERE ih.movement IS NULL OR ih.movement < 0.00;

   INSERT INTO tt_temp_neg_inv_slot_item (slotitem_id, start_date, end_date)
      SELECT ih.slotitem_id, ih.start_date, ih.end_date
        FROM item_history ih
        left join slot_item si on si.SLOTITEM_ID = ih.SLOTITEM_ID 
        left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
      WHERE ih.inventory IS NULL OR ih.inventory < 0.00;

   INSERT INTO tt_temp_neg_hits_slot_item (slotitem_id, start_date, end_date)
      SELECT ih.slotitem_id, ih.start_date, ih.end_date
        FROM item_history ih
        left join slot_item si on si.SLOTITEM_ID = ih.SLOTITEM_ID 
        left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
       WHERE ih.nbr_of_picks IS NULL OR ih.nbr_of_picks < 0.00;

   DELETE item_history
    WHERE ship_unit NOT IN (1, 2, 4, 8);

   UPDATE item_history
      SET movement = 0.00, mod_date_time = SYSDATE, mod_user = 'consoldtd'
    WHERE (movement IS NULL OR movement < 0.00)
	AND ROWID IN
             (SELECT item_history.ROWID
                FROM slot_item si
				left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
               WHERE item_history.slotitem_id = si.slotitem_id);

   UPDATE item_history
      SET inventory = 0.00, mod_date_time = SYSDATE, mod_user = 'consoldtd'
    WHERE (inventory IS NULL OR inventory < 0.00)
	AND ROWID IN
             (SELECT item_history.ROWID
                FROM slot_item si
				left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
               WHERE item_history.slotitem_id = si.slotitem_id);

   UPDATE item_history
      SET nbr_of_picks = 0.00,
          mod_date_time = SYSDATE,
          mod_user = 'consoldtd'
    WHERE (nbr_of_picks IS NULL OR nbr_of_picks < 0.00)
	AND ROWID IN
             (SELECT item_history.ROWID
                FROM slot_item si
				left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
               WHERE item_history.slotitem_id = si.slotitem_id);

   DBMS_OUTPUT.put_line (
      'info: consolidating item history for skus that are currently both unslotted and slotted.');

   INSERT INTO tt_temp_slot_item_unslotted (sku_id,
                                            ship_unit,
                                            hist_match,
                                            mult_loc_grp,
                                            no_of_slot_item_recs,
                                            min_slotitem_id)
        SELECT DISTINCT si.sku_id,
                        NVL (si.ship_unit, 0) ship_unit,
                        NVL (si.hist_match, '0x') hist_match,
                        NVL (si.mult_loc_grp, '0x') mult_loc_grp,
                        COUNT (1) no_of_slot_item_recs,
                        MIN (si.slotitem_id) min_slotitem_id
          FROM slot_item si 
          left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
         WHERE     si.sku_id IS NOT NULL
               AND si.slot_id IS NULL
               AND si.delete_mult <> 1
      GROUP BY si.sku_id,
               si.ship_unit,
               si.hist_match,
               si.mult_loc_grp
      ORDER BY 1 NULLS FIRST;

   INSERT INTO tt_temp_slot_item_slotted (sku_id,
                                          ship_unit,
                                          hist_match,
                                          mult_loc_grp,
                                          no_of_slot_item_recs,
                                          min_slotitem_id)
        SELECT DISTINCT si.sku_id,
                        NVL (si.ship_unit, 0) ship_unit,
                        NVL (si.hist_match, '0x') hist_match,
                        NVL (si.mult_loc_grp, '0x') mult_loc_grp,
                        COUNT (1) no_of_slot_item_recs,
                        MIN (si.slotitem_id) min_slotitem_id
          FROM slot_item si
          left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
         WHERE     si.sku_id IS NOT NULL
               AND si.slot_id IS NOT NULL
               AND si.delete_mult <> 1
      GROUP BY si.sku_id,
               si.ship_unit,
               si.hist_match,
               si.mult_loc_grp
      ORDER BY 1 NULLS FIRST;

   INSERT INTO tt_temp_slot_item_interested (sku_id,
                                             ship_unit,
                                             hist_match,
                                             mult_loc_grp,
                                             no_of_slot_item_recs,
                                             min_slotitem_id)
      SELECT tsiu.sku_id,
             tsiu.ship_unit,
             tsiu.hist_match,
             tsiu.mult_loc_grp,
             tsis.no_of_slot_item_recs,
             tsiu.min_slotitem_id
        FROM    tt_temp_slot_item_unslotted tsiu
             INNER JOIN
                tt_temp_slot_item_slotted tsis
             ON     tsis.sku_id = tsiu.sku_id
                AND tsis.ship_unit = tsiu.ship_unit
                AND tsis.hist_match = tsiu.hist_match
                AND tsis.mult_loc_grp = tsiu.mult_loc_grp;

   v_iloopcontrol := 1;

   BEGIN
      SELECT MIN (irowid), MAX (irowid)
        INTO v_inextrowid, v_imaxrowid
        FROM tt_temp_slot_item_interested;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         NULL;
   END;

   IF NVL (v_inextrowid, 0) = 0
   THEN
      DBMS_OUTPUT.put_line (
         'warning: there is no group match for skus that are both slotted and unslotted.');
      GOTO case_2;
   END IF;

   BEGIN
      SELECT irowid,
             sku_id,
             ship_unit,
             hist_match,
             mult_loc_grp,
             no_of_slot_item_recs
        INTO v_icurrentrowid,
             v_sku_id,
             v_slot_ship_unit,
             v_hist_match,
             v_mult_loc_grp,
             v_no_of_slot_item_recs
        FROM tt_temp_slot_item_interested
       WHERE irowid = v_inextrowid AND ROWNUM <= 1;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         NULL;
   END;

   WHILE v_iloopcontrol = 1
   LOOP
      DBMS_OUTPUT.put_line (
            'processing sku group '
         || SUBSTR (CAST (v_icurrentrowid AS VARCHAR2), 1, 10)
         || ' of '
         || SUBSTR (CAST (v_imaxrowid AS VARCHAR2), 1, 10));

      sp_consol_item_hist_slotted (v_sku_id,
                                   p_whse,
                                   v_slot_ship_unit,
                                   v_hist_match,
                                   v_mult_loc_grp,
                                   v_no_of_slot_item_recs,
                                   v_errcode);

      IF (v_errcode <> 0)
      THEN
         DBMS_OUTPUT.put_line (
               'error: error processing sku group - sku: '
            || SUBSTR (CAST (v_sku_id AS VARCHAR2), 1, 10)
            || 'slot ship unit:  '
            || SUBSTR (CAST (v_slot_ship_unit AS VARCHAR2), 1, 4)
            || ' , '
            || 'hist match: '
            || SUBSTR (v_hist_match, 1, 12)
            || ' , '
            || 'mult locn grp: '
            || SUBSTR (v_mult_loc_grp, 1, 12));
         DBMS_OUTPUT.put_line ('warning: rolling back transaction....');
         ROLLBACK;
         GOTO case_2;
      END IF;

      v_inextrowid := NULL;

      BEGIN
         SELECT NVL (MIN (irowid), 0)
           INTO v_inextrowid
           FROM tt_temp_slot_item_interested
          WHERE irowid > v_icurrentrowid;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            NULL;
      END;

      IF NVL (v_inextrowid, 0) = 0
      THEN
         v_iloopcontrol := 0;
         EXIT;
      END IF;

      BEGIN
         SELECT irowid,
                sku_id,
                ship_unit,
                hist_match,
                mult_loc_grp,
                no_of_slot_item_recs
           INTO v_icurrentrowid,
                v_sku_id,
                v_slot_ship_unit,
                v_hist_match,
                v_mult_loc_grp,
                v_no_of_slot_item_recs
           FROM tt_temp_slot_item_interested
          WHERE irowid = v_inextrowid;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            NULL;
      END;
   END LOOP;

   -- commit;
   IF 'slot_item_slotted' IS NOT NULL
   THEN
      EXECUTE IMMEDIATE ' delete tt_temp_slot_item_slotted ';
   END IF;

   IF 'slot_item_unslotted' IS NOT NULL
   THEN
      EXECUTE IMMEDIATE ' delete tt_temp_slot_item_unslotted ';
   END IF;

   DBMS_OUTPUT.put_line (
      '.....................................................................................................');

  <<case_2>>
   DBMS_OUTPUT.put_line ('');
   DBMS_OUTPUT.put_line (
      'info: consolidating item history for skus that are currently all unslotted.');

   EXECUTE IMMEDIATE ' delete tt_temp_item_history_unslotted ';

   EXECUTE IMMEDIATE ' delete tt_temp_item_history_smry ';

   EXECUTE IMMEDIATE ' delete tt_temp_item_history_inst ';

   EXECUTE IMMEDIATE ' delete tt_temp_slot_item_interested ';

   v_errcode := 0;

   INSERT INTO tt_temp_slot_item_interested (sku_id,
                                             ship_unit,
                                             hist_match,
                                             mult_loc_grp,
                                             no_of_slot_item_recs,
                                             min_slotitem_id)
      SELECT t1.sku_id,
             t1.ship_unit,
             t1.hist_match,
             t1.mult_loc_grp,
             t1.no_of_slot_item_recs,
             t1.min_slotitem_id
        FROM    (  SELECT DISTINCT si.sku_id,
                                   NVL (si.ship_unit, 0) ship_unit,
                                   NVL (si.hist_match, '0x') hist_match,
                                   NVL (si.mult_loc_grp, '0x') mult_loc_grp,
                                   COUNT (1) no_of_slot_item_recs,
                                   MIN (si.slotitem_id) min_slotitem_id
                     FROM slot_item si
                     left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                    WHERE     si.sku_id IS NOT NULL
                          AND si.slot_id IS NULL
                          AND si.delete_mult <> 1
                 GROUP BY si.sku_id,
                          si.ship_unit,
                          si.hist_match,
                          si.mult_loc_grp
                   HAVING COUNT (1) > 1) t1
             LEFT OUTER JOIN
                (  SELECT DISTINCT si.sku_id,
                                   NVL (si.ship_unit, 0) ship_unit,
                                   NVL (si.hist_match, '0x') hist_match,
                                   NVL (si.mult_loc_grp, '0x') mult_loc_grp,
                                   COUNT (1) no_of_slot_item_recs,
                                   MIN (si.slotitem_id) min_slotitem_id
                     FROM slot_item si
                     left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                    WHERE     si.sku_id IS NOT NULL
                          AND si.slot_id IS NOT NULL
                          AND si.delete_mult <> 1
                          AND si.sku_id = 0
                 GROUP BY si.sku_id,
                          si.ship_unit,
                          si.hist_match,
                          si.mult_loc_grp) t2
             ON     t2.sku_id = t1.sku_id
                AND t2.ship_unit = t1.ship_unit
                AND t2.hist_match = t1.hist_match
                AND t2.mult_loc_grp = t1.mult_loc_grp;

   DBMS_OUTPUT.put_line (
      'printing all sku groups that are all unslotted and does not exists in slotted group');

   v_iloopcontrol := 1;

   BEGIN
      SELECT MIN (irowid), MAX (irowid)
        INTO v_inextrowid, v_imaxrowid
        FROM tt_temp_slot_item_interested;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         NULL;
   END;

   IF NVL (v_inextrowid, 0) = 0
   THEN
      DBMS_OUTPUT.put_line (
         'warning: there is no group match for skus that are all unslotted and does not exists in slotted group');
      v_errcode := 0;
      GOTO handle_error;
   END IF;

   BEGIN
      SELECT irowid,
             sku_id,
             ship_unit,
             hist_match,
             mult_loc_grp,
             min_slotitem_id,
             no_of_slot_item_recs
        INTO v_icurrentrowid,
             v_sku_id,
             v_slot_ship_unit,
             v_hist_match,
             v_mult_loc_grp,
             v_min_slotitem_id,
             v_no_of_slot_item_recs
        FROM tt_temp_slot_item_interested
       WHERE irowid = v_inextrowid AND ROWNUM <= 1;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         NULL;
   END;

   WHILE v_iloopcontrol = 1
   LOOP
      DBMS_OUTPUT.put_line (
            'processing sku group '
         || SUBSTR (CAST (v_icurrentrowid AS VARCHAR2), 1, 10)
         || ' of '
         || SUBSTR (CAST (v_imaxrowid AS VARCHAR2), 1, 10));
      sp_consol_item_hist_unslotted (v_sku_id,
                                     p_whse,
                                     v_slot_ship_unit,
                                     v_hist_match,
                                     v_mult_loc_grp,
                                     v_no_of_slot_item_recs,
                                     v_min_slotitem_id,
                                     v_errcode);

      IF (v_errcode <> 0)
      THEN
         DBMS_OUTPUT.put_line (
               'error: error processing skugroup - sku: '
            || SUBSTR (CAST (v_sku_id AS VARCHAR2), 1, 10)
            || ', slot ship unit: '
            || SUBSTR (CAST (v_slot_ship_unit AS VARCHAR2), 1, 4)
            || ' , '
            || 'hist match: '
            || SUBSTR (v_hist_match, 1, 12)
            || ' , '
            || 'mult locn grp: '
            || SUBSTR (v_mult_loc_grp, 1, 12));
         DBMS_OUTPUT.put_line ('warning: rolling back transaction....');
         ROLLBACK;
         GOTO cleanup;
      END IF;

      v_inextrowid := NULL;

      BEGIN
         SELECT NVL (MIN (irowid), 0)
           INTO v_inextrowid
           FROM tt_temp_slot_item_interested
          WHERE irowid > v_icurrentrowid;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            NULL;
      END;

      IF NVL (v_inextrowid, 0) = 0
      THEN
         v_iloopcontrol := 0;
         EXIT;
      END IF;

      BEGIN
         SELECT irowid,
                sku_id,
                ship_unit,
                hist_match,
                mult_loc_grp,
                min_slotitem_id,
                no_of_slot_item_recs
           INTO v_icurrentrowid,
                v_sku_id,
                v_slot_ship_unit,
                v_hist_match,
                v_mult_loc_grp,
                v_min_slotitem_id,
                v_no_of_slot_item_recs
           FROM tt_temp_slot_item_interested
          WHERE irowid = v_inextrowid;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            NULL;
      END;
   END LOOP;

  <<cleanup>>
   DELETE tt_temp_slot_item_interested;

   DELETE tt_temp_slot_item_slotted;

   DELETE tt_temp_slot_item_unslotted;

   DELETE tt_temp_item_history_unslotted;

   DELETE tt_temp_item_history_smry;

   DELETE tt_temp_item_history_inst;

   DELETE tt_temp_item_history_inst;

    update item_history ih
    set ih.movement = -999.99
    where ih.movement = 0.00
        and exists
        (
            select 1
            from tt_temp_neg_mvmt_slot_item tnms
            where tnms.slotitem_id = ih.slotitem_id and tnms.start_date = ih.start_date
                and tnms.end_date = ih.end_date
        );

    update item_history ih
    set ih.inventory = -999.99
    where ih.inventory = 0.00
        and exists
        (
            select 1
            from tt_temp_neg_inv_slot_item tnns
            where tnns.slotitem_id = ih.slotitem_id and tnns.start_date = ih.start_date
                and tnns.end_date = ih.end_date
        );
    
    update item_history ih
    set ih.nbr_of_picks = -999.99
    where ih.nbr_of_picks = 0.00
        and exists
        (
            select 1
            from tt_temp_neg_hits_slot_item tnhs
            where tnhs.slotitem_id = ih.slotitem_id and tnhs.start_date = ih.start_date
                and tnhs.end_date = ih.end_date
        );

   DELETE FROM tt_temp_neg_mvmt_slot_item;

   DELETE FROM tt_temp_neg_inv_slot_item;

   DELETE FROM tt_temp_neg_hits_slot_item;

  <<handle_error>>
   NULL;
END;
/
show errors;

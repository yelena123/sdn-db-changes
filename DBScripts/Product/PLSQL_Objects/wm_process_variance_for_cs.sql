create or replace procedure wm_process_variance_for_cs
(
    p_user_id             in user_profile.user_id%type,
    p_tc_company_id       in shipment.tc_company_id%type,
    p_shipment_id         in shipment.shipment_id%type,
    p_manifest_id         in shipment.manifest_id%type,
    p_caller_id           in varchar2 default null
)
as
    v_use_locking               number(1) := 0;
    v_exec_pre_cleanup          varchar2(1);
    v_max_log_lvl               number(1) := 0;
    v_commit_freq               number(5) := 99999;
    v_mode_id                   sys_code.code_id%type := 'CS';
    v_row_count                 number(9) := 0;
    v_act_trk_tran_nbr      prod_trkg_tran.tran_nbr%type;
    v_act_trk_rec_count     number(9) := 0;
    v_shipment_ref_code     varchar2(3) := '41';
    v_order_ref_code        varchar2(3) := '01';
    v_lpn_ref_code          varchar2(3) := '02';
    type t_lpn_id is        table of lpn.lpn_id%type index by binary_integer;
    va_lpn_id               t_lpn_id;
begin
    update shipment s
    set s.shipment_closed_indicator = 1, s.last_updated_dttm = sysdate,
        s.last_updated_source = p_user_id
    where s.shipment_id = p_shipment_id or s.manifest_id = p_manifest_id;
    commit;

    select /*+ result_cache */ to_number(coalesce(trim(substr(sc.misc_flags, 3, 5)), '99999')),
        case when substr(sc.misc_flags, 2, 1) = 'Y' then 1 else 0 end,
        to_number(coalesce(trim(substr(sc.misc_flags, 8, 1)), '0')),
        coalesce(substr(sc.misc_flags, 9, 1), 'N')
    into v_commit_freq, v_use_locking, v_max_log_lvl, v_exec_pre_cleanup
    from sys_code sc 
    where sc.rec_type = 'S' and sc.code_type = '520' and sc.code_id = v_mode_id;
    v_commit_freq := case v_commit_freq when 0 then 99999 else v_commit_freq end;

    delete from tmp_store_master;
    delete from tmp_order_splits_master;
    delete from tmp_log_parm;
    insert into tmp_log_parm
    (
        log_lvl, pgm_id, module, msg_id, ref_code_1, ref_value_1, user_id
    )
    values
    (
        v_max_log_lvl, 'wm_process_variance_for_cs', 'WAVE', '1094', v_mode_id,
        to_char(coalesce(p_shipment_id, p_manifest_id)), p_user_id
    );

    if (v_exec_pre_cleanup = 'Y')
    then
        wm_cs_log('Beginning custom sanitization');
        wm_pre_cs_cleanup();
    end if;

    if (p_shipment_id is not null)
    then
        select l.lpn_id
        bulk collect into va_lpn_id
        from lpn l
        where l.lpn_facility_status < 40
            and l.shipment_id =
            (
                select s.shipment_id
                from shipment s
                where s.shipment_id = p_shipment_id and s.static_route_id is not null
            );
        v_row_count := sql%rowcount;
        if (v_row_count > 0)
        then
            wm_cs_log('Updated ' || v_row_count || ' static retail routed lpns; returning.', p_sql_log_level => 0);
        end if;
    else
        select l.lpn_id
        bulk collect into va_lpn_id
        from lpn l
        where l.lpn_facility_status < 40
            and l.static_route_id is not null
            and l.shipment_id in
            (
                select s.shipment_id
                from shipment s
                where s.manifest_id= p_manifest_id and s.shipment_status < 80 
                    and s.is_cancelled = 0
            );

        v_row_count := sql%rowcount;
        if (v_row_count > 0)
        then
            wm_cs_log('Updated ' || v_row_count || ' static retail routed lpns; continuing.', p_sql_log_level => 0);
        end if;
    end if;
        wm_cs_log('Collected ' || va_lpn_id.count || ' static retail routed lpns;', p_sql_log_level => 1);
    
    if (va_lpn_id.count > 0)
    then
        -- activity tracking is at the origin facility level
        select decode(wp.trk_prod_flag, 'Y', prod_trkg_tran_id_seq.nextval, null)
        into v_act_trk_tran_nbr
        from whse_parameters wp
        join facility f on f.facility_id = wp.whse_master_id
        where f.facility_id =
        (
            select mh.o_facility_id
            from manifest_hdr mh
            where mh.manifest_id = p_manifest_id
            union all
            select s.o_facility_number
            from shipment s
            where s.shipment_id = p_shipment_id
        );
        
        if (v_act_trk_tran_nbr is not null)
        then
            delete from tmp_act_trk_lpns;
            forall i in 1..va_lpn_id.count
            insert into tmp_act_trk_lpns t (lpn_id)
            values (va_lpn_id(i));

            insert into prod_trkg_tran
            (
                prod_trkg_tran_id, tran_type, tran_code, tran_nbr, seq_nbr, whse,
                menu_optn_name, user_id, cd_master_id, ref_code_id_1, ref_code_id_2, 
                ref_code_id_3, ref_field_1, ref_field_2, ref_field_3, mod_date_time,
                create_date_time, module_name
            ) 
            select prod_trkg_tran_id_seq.nextval, '800', '013', v_act_trk_tran_nbr,
                v_act_trk_rec_count + rownum seq_nbr, f.whse, p_caller_id, p_user_id, 
                l.tc_company_id, v_shipment_ref_code, v_order_ref_code, v_lpn_ref_code,
                l.tc_shipment_id ref_field_1, l.tc_order_id ref_field_2, 
                l.tc_lpn_id ref_field_3, sysdate, sysdate, 'Shipping'
            from tmp_act_trk_lpns t
            join lpn l on l.lpn_id = t.lpn_id
            join facility f on f.facility_id = l.c_facility_id;
            v_act_trk_rec_count := v_act_trk_rec_count + sql%rowcount;
            wm_cs_log('Wrote act trk recs for lpns ' || v_act_trk_rec_count, p_sql_log_level => 1);
        end if;
        
        forall i in 1..va_lpn_id.count
        update lpn l
        set l.shipment_id = null, l.tc_shipment_id = null,
            l.last_updated_source = p_user_id, l.last_updated_dttm = sysdate  
        where l.lpn_id = va_lpn_id(i);
        wm_cs_log('updated ' || sql%rowcount || ' static retail routed lpns; returning.', p_sql_log_level => 1);
    end if;

    if ((p_shipment_id is not null or p_manifest_id is not null) and va_lpn_id.count > 0)
    then
        return;
    end if;

    if (v_use_locking = 1)
    then
        -- turn off for non-retail CM or CS
        if (p_manifest_id is not null)
        then
            select case when exists
            (
                select 1
                from manifested_lpn ml
                join lpn l on l.lpn_id = ml.lpn_id
                join orders o on o.order_id = l.order_id and o.d_facility_id is null
                where ml.manifest_id = p_manifest_id
            ) then 0 else 1 end
            into v_use_locking
            from dual;
        else
            select case when exists
                (
                    select 1
                    from orders o 
                    where o.shipment_id = p_shipment_id and o.d_facility_id is null
                )
                or exists
                (
                    select 1
                    from order_split os
                    join orders o on o.order_id = os.order_id
                        and o.d_facility_id is null
                    where os.shipment_id = p_shipment_id and os.is_cancelled = 0
                ) then 0 else 1 end
            into v_use_locking
            from dual;
        end if;

        if (v_use_locking = 0)
        then
            wm_cs_log('Locking turned off!', p_sql_log_level => 0);
        end if;
    end if;

    wm_cs_log('Beginning base sanitization');
    wm_sanitize_orphaned_lpns(p_user_id, p_shipment_id, p_manifest_id);
    commit;

    wm_cs_log('Closing shpmt/manifest', p_sql_log_level => 0);
    wm_load_splits_for_input_crit(v_use_locking, p_manifest_id => p_manifest_id,
        p_cs_shipment_id => p_shipment_id, p_mode => '_close_');

    wm_remove_variance_wrapper(p_user_id, v_use_locking, v_commit_freq, p_caller_id, p_shipment_id,
        p_manifest_id, v_act_trk_tran_nbr, v_act_trk_rec_count);

    -- recalc wt/vol after handling variance
    merge into shipment s
    using
    (
        select s.shipment_id, sum(coalesce(l.weight, l.estimated_weight, 0)) wt,
            sum(coalesce(l.actual_volume, l.estimated_volume, 0)) vol, sum(l.total_lpn_qty) qty
        from shipment s
        join lpn l on l.shipment_id = s.shipment_id
        where s.shipment_id = p_shipment_id or s.manifest_id = p_manifest_id
        group by s.shipment_id
    ) iv on (iv.shipment_id = s.shipment_id)
    when matched then
    update set s.last_updated_source = p_user_id, s.planned_weight = iv.wt,
        s.planned_volume = iv.vol, s.order_qty = iv.qty, s.last_updated_dttm = sysdate,
        s.qty_uom_id = 
        (
            select l.qty_uom_id_base 
            from lpn l 
            where l.shipment_id = s.shipment_id and l.qty_uom_id_base is not null and rownum < 2
        );
    wm_cs_log('Wt/vol recalculated for shpmts ' || sql%rowcount);

    commit;
    wm_cs_log('Closed shpmt/manifest', p_sql_log_level => 0);
end;
/

show errors;

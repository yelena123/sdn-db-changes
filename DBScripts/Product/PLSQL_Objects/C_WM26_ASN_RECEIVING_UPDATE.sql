create or replace
PROCEDURE C_WM26_ASN_RECEIVING_UPDATE ( p_tc_asn_id IN VARCHAR2 )
IS
      v_errorFlag NUMBER(2,0) := '0' ;
BEGIN
     IF p_tc_asn_id like 'ASN%' then 
         return;
     END IF;

      -- Insert records in TEMP table
      INSERT INTO C_WM26_TEMP_ASN_RCV_UPDATES
          SELECT LD.LPN_ID,min(LPN_DETAIL_ID),LD.ITEM_ID, SUM(LD.SIZE_VALUE), SUM(SHIPPED_QTY), SUM(INITIAL_QTY), MIN(LD.EXPIRATION_DATE)
             FROM LPN_DETAIL LD ,LPN WHERE LPN.LPN_ID = LD.LPN_ID AND LPN.TC_ASN_ID = p_tc_asn_id GROUP BY LD.LPN_ID, LD.ITEM_ID;

      Update C_WM26_TEMP_ASN_RCV_UPDATES TEMP set LPN_DETAIL_ID =
         ( SELECT min(LD.LPN_DETAIL_ID) from LPN_DETAIL LD, LPN where LPN.LPN_ID = TEMP.LPN_ID AND LD.LPN_ID = LPN.LPN_ID AND LD.EXPIRATION_DATE = TEMP.MIN_EXP_DATE );

      --Multiple item check
      UPDATE LPN SET LPN.ERROR_INDICATOR ='2' WHERE LPN_ID IN (
          SELECT LPN_ID FROM C_WM26_TEMP_ASN_RCV_UPDATES TEMP GROUP BY TEMP.LPN_ID HAVING COUNT(TEMP.LPN_ID)> '1');
      IF SQL%ROWCOUNT > 0
            THEN
            INSERT INTO ERROR_LOG (ERROR_ID,TC_COMPANY_ID,ERROR_MSG_ID, ERROR_TYPE,DESCRIPTION ,OBJECT_TYPE, LPN_ID)
                  SELECT E_ERROR_LOG_SEQ.NEXTVAL, RTEMP.* FROM (SELECT DISTINCT LPN.TC_COMPANY_ID, '5102601','1', (
                  SELECT MSG FROM MESSAGE_MASTER WHERE KEY = '5102601'),'LPN',
                  LPN.LPN_ID FROM LPN, C_WM26_TEMP_ASN_RCV_UPDATES TEMP WHERE LPN.ERROR_INDICATOR = '2' AND LPN.LPN_ID=TEMP.LPN_ID) RTEMP
                  WHERE NOT EXISTS (SELECT 1 FROM ERROR_LOG EL WHERE EL.LPN_ID = RTEMP.LPN_ID AND EL.ERROR_MSG_ID = '5102601');
      END IF ;

      --Missing Item Check

      UPDATE LPN SET LPN.ERROR_INDICATOR ='3' WHERE LPN_ID IN
          (SELECT LPN2.LPN_ID FROM LPN LPN2, LPN_DETAIL LD WHERE
              LPN2.TC_ASN_ID = p_tc_asn_id and
              LPN2.LPN_ID = LD.LPN_ID AND (LPN2.PURCHASE_ORDERS_ID iS NULL OR LD.ITEM_ID NOT IN (SELECT distinct POLI.SKU_ID FROM
            PURCHASE_ORDERS_LINE_ITEM POLI WHERE POLI.PURCHASE_ORDERS_ID = LD.purchase_orders_id )));


      IF SQL%ROWCOUNT > 0
            THEN
            INSERT INTO ERROR_LOG (ERROR_ID,TC_COMPANY_ID,ERROR_MSG_ID, ERROR_TYPE,DESCRIPTION ,OBJECT_TYPE, LPN_ID)
                  SELECT E_ERROR_LOG_SEQ.NEXTVAL, RTEMP.* FROM (SELECT DISTINCT LPN.TC_COMPANY_ID, '5102602','1', (
                  SELECT MSG FROM MESSAGE_MASTER WHERE KEY = '5102602'),'LPN',
                  LPN.LPN_ID FROM LPN, C_WM26_TEMP_ASN_RCV_UPDATES TEMP WHERE LPN.ERROR_INDICATOR = '3' AND LPN.LPN_ID=TEMP.LPN_ID) RTEMP
                  WHERE NOT EXISTS (SELECT 1 FROM ERROR_LOG EL WHERE EL.LPN_ID = RTEMP.LPN_ID AND EL.ERROR_MSG_ID = '5102602');
      END IF ;


     UPDATE LPN SET LPN.ERROR_INDICATOR = '1' where TC_ASN_ID = p_tc_asn_id
      AND ERROR_INDICATOR IN ('2', '3');

      UPDATE ASN SET HAS_IMPORT_ERROR ='1' WHERE ASN.TC_ASN_ID = p_tc_asn_id AND EXISTS (SELECT 1 FROM LPN
            WHERE LPN.TC_ASN_ID = ASN.TC_ASN_ID AND LPN.ERROR_INDICATOR = '1' );
      IF SQL%ROWCOUNT > 0
            THEN
            v_errorFlag := '1' ;
            INSERT INTO ERROR_LOG (ERROR_ID,TC_COMPANY_ID,ERROR_MSG_ID, ERROR_TYPE,DESCRIPTION ,OBJECT_TYPE,ASN_ID)
                  SELECT E_ERROR_LOG_SEQ.NEXTVAL, RTEMP.* FROM (SELECT DISTINCT ASN.TC_COMPANY_ID, '6100132','1', (
                  SELECT MSG FROM MESSAGE_MASTER WHERE KEY = '6100132'),'ADSN', ASN_ID
                  FROM ASN WHERE ASN.HAS_IMPORT_ERROR = '1' AND ASN.TC_ASN_ID = p_tc_asn_id) RTEMP
                  WHERE NOT EXISTS (SELECT 1 FROM ERROR_LOG EL WHERE EL.ASN_ID = RTEMP.ASN_ID AND EL.ERROR_MSG_ID = '6100132');
      END IF ;

      IF v_errorFlag = '0'
      THEN
            -- BATCH_MASTER updates
            INSERT INTO BATCH_MASTER (BATCH_MASTER_ID,BATCH_NBR, XPIRE_DATE, ITEM_ID, STAT_CODE, RCVD_DATE, XPIRE_FLAG, MFG_RECALL_FLAG, CREATE_DATE_TIME, MOD_DATE_TIME, ITEM_MASTER_ID )
               SELECT BATCH_MASTER_ID_SEQ.Nextval,BATCH_NBR, EXPIRATION_DATE, item_id, '10', null, 'N', 'N', sysdate, sysdate, 0 from
                 (select DISTINCT LD.BATCH_NBR, LD.EXPIRATION_DATE, LD.item_id, '10', getdate(), 'N', 'N' FROM LPN_DETAIL LD, LPN WHERE
                      LD.LPN_ID = LPN.LPN_ID AND LPN.TC_ASN_ID = p_tc_asn_id AND NOT EXISTS (SELECT 1 FROM BATCH_MASTER BM  WHERE bm.batch_nbr = ld.batch_nbr and bm.item_id = ld.item_id) ) ABC ;


            -- Custom Table C_ILPN_LOT_MAPPING updates
            DELETE FROM C_ILPN_LOT_MAPPING CILM WHERE EXISTS (SELECT 1 FROM LPN where tc_asn_id = p_tc_asn_id AND LPN.TC_LPN_ID = CILM.TC_LPN_ID);
            --commit;

            INSERT INTO C_ILPN_LOT_MAPPING (SELECT LD.BATCH_NBR, LD.EXPIRATION_DATE, LD.ITEM_NAME, LPN.TC_LPN_ID,
                getdate(), 'HOST', 'JDE','HOST','JDE',getdate(), null,  LD.MANUFACTURED_DTTM  FROM LPN_DETAIL LD, LPN WHERE LPN.LPN_ID = LD.LPN_ID AND LPN.tc_asn_id = p_tc_asn_id);


            --LPN_DETAIL updates
            UPDATE LPN_DETAIL LD SET (SIZE_VALUE, SHIPPED_QTY, INITIAL_QTY ) = (SELECT TOTAL_SIZE_VAL, TOTAL_SHIPPED_QTY, TOTAL_INITIAL_QTY FROM C_WM26_TEMP_ASN_RCV_UPDATES TEMP
                    WHERE TEMP.LPN_DETAIL_ID = LD.LPN_DETAIL_ID AND TEMP.LPN_ID = LD.LPN_ID )
              WHERE EXISTS (SELECT 1 from C_WM26_TEMP_ASN_RCV_UPDATES T where T.LPN_ID = LD.LPN_ID );

            ----- Delete the LPN detail which is a little redundant now.
            DELETE FROM LPN_DETAIL WHERE lpn_id in (select lpn_id from lpn where inbound_outbound_indicator = 'I' and tc_asn_id = p_tc_asn_id
             and lpn.lpn_facility_status != 99) and (shipped_qty is null or shipped_qty=0);

            --DELETE ASN_DETAILS corresponding to deleted LPN_DETAIL
            DELETE from asn_detail where asn_id in (select asn_id from asn where tc_asn_id = p_tc_asn_id)
             and not exists (select 1 from lpn_detail where lpn_detail.asn_dtl_id = asn_detail.asn_detail_id
             and lpn_id in (select lpn_id from lpn where inbound_outbound_indicator = 'I' and tc_asn_id = p_tc_asn_id
             and lpn.lpn_facility_status != 99));

            --UPDATE ASN_DETAILS to match LPN_DETAIL_UPDATES
            update asn_detail set (shipped_qty, shipped_lpn_count) = (select sum(shipped_qty), count(lpn_id) from lpn_detail
              where lpn_detail.asn_dtl_id = asn_detail.asn_detail_id
              and lpn_id in (select lpn_id from lpn where inbound_outbound_indicator = 'I' and tc_asn_id = p_tc_asn_id
              and lpn.lpn_facility_status != 99))
              where asn_id in (select asn_id from asn where tc_asn_id = p_tc_asn_id);



             COMMIT;
     END IF;


END C_WM26_ASN_RECEIVING_UPDATE;
/
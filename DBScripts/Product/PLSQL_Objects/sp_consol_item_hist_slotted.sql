CREATE OR REPLACE PROCEDURE sp_consol_item_hist_slotted (
   v_sku_id                 IN     NUMBER,
   p_whse                   IN     locn_hdr.whse%type,
   v_slot_ship_unit         IN     NUMBER,
   v_hist_match             IN     VARCHAR2,
   v_mult_loc_grp           IN     VARCHAR2,
   v_no_of_slot_item_recs   IN     NUMBER,
   v_errcode                IN OUT NUMBER)
AS
   v_i_ih_nextrowid                NUMBER (10, 0);
   v_i_ih_currowid                 NUMBER (10, 0);
   v_i_ih_loopctl                  NUMBER (1, 0);
   v_wh_ti                         NUMBER (10, 0);
   v_ven_ti                        NUMBER (10, 0);
   v_ord_ti                        NUMBER (10, 0);
   v_wh_hi                         NUMBER (10, 0);
   v_ven_hi                        NUMBER (10, 0);
   v_ord_hi                        NUMBER (10, 0);
   v_inn_per_cs                    NUMBER (10, 0);
   v_each_per_cs                   NUMBER (10, 0);
   v_each_per_inn                  NUMBER (10, 0);
   v_item_ship_unit                NUMBER (5, 0);
   v_start_date                    TIMESTAMP (3);
   v_end_date                      TIMESTAMP (3);
   v_pallete_pattern               NUMBER (10, 0);
   v_mov_pallet                    NUMBER (17, 2);
   v_mov_case                      NUMBER (17, 2);
   v_mov_each                      NUMBER (17, 2);
   v_mov_inner                     NUMBER (17, 2);
   v_inv_pallet                    NUMBER (17, 2);
   v_inv_case                      NUMBER (17, 2);
   v_inv_inner                     NUMBER (17, 2);
   v_inv_each                      NUMBER (17, 2);
   v_nbr_of_picks                  NUMBER (17, 2);
   v_hist_id                       NUMBER (19, 0);
   v_recs_updtd_tot                NUMBER (10, 0);
   v_recs_updtd_tmp                NUMBER (10, 0);
   v_bis_slotitem_good_to_delete   NUMBER (1, 0);
   v_strsqlst                      VARCHAR2 (1500);
   v_no_of_item_hist_recs          NUMBER (10, 0);
   v_rc_del                        NUMBER (10, 0);
   swv_rowcount                    NUMBER (10, 0);
   v_if_exists                     NUMBER (10, 0);
   v_if_exists2                    NUMBER (10, 0);
BEGIN
   v_bis_slotitem_good_to_delete := 0;
   v_errcode := 0;

   SELECT wh_hi,
          ven_hi,
          ord_hi,
          wh_ti,
          ven_ti,
          ord_ti,
          inn_per_cs,
          each_per_cs,
          each_per_inn
     INTO v_wh_hi,
          v_ven_hi,
          v_ord_hi,
          v_wh_ti,
          v_ven_ti,
          v_ord_ti,
          v_inn_per_cs,
          v_each_per_cs,
          v_each_per_inn
     FROM so_item_master
    WHERE sku_id = v_sku_id and whse_code = p_whse;

   IF (v_wh_ti IS NULL AND v_ven_ti IS NULL AND v_ord_ti IS NULL
       OR v_wh_hi IS NULL AND v_ven_hi IS NULL AND v_ord_hi IS NULL)
   THEN
      v_errcode := 0;
      GOTO handle_error;
   END IF;

   v_wh_hi := NVL (v_wh_hi, 1);
   v_ven_hi := NVL (v_ven_hi, 1);
   v_ord_hi := NVL (v_ord_hi, 1);
   v_wh_ti := NVL (v_wh_ti, 1);
   v_ven_ti := NVL (v_ven_ti, 1);
   v_ord_ti := NVL (v_ord_ti, 1);

   EXECUTE IMMEDIATE ' delete tt_temp_item_history_smry ';

   INSERT INTO tt_temp_item_history_smry (ship_unit,
                                          pallete_pattern,
                                          nbr_of_picks,
                                          mov1,
                                          mov2,
                                          mov4,
                                          mov8,
                                          inv1,
                                          inv2,
                                          inv4,
                                          inv8,
                                          start_date,
                                          end_date)
        SELECT t.ship_unit AS ship_unit,
               t.pallete_pattern AS pallete_pattern,
               SUM (t.nbr_of_picks) AS nbr_of_picks,
               SUM (t.mov1) AS mov_pallets,
               SUM (t.mov2) AS mov_cases,
               SUM (t.mov4) AS mov_inners,
               SUM (t.mov8) AS mov_eaches,
               SUM (t.inv1) AS inv_pallets,
               SUM (t.inv2) AS inv_cases,
               SUM (t.inv4) AS inv_inners,
               SUM (t.inv8) AS inv_eaches,
               start_date AS start_date,
               end_date AS end_date
          FROM (  SELECT ih.ship_unit,
                         si.pallete_pattern,
                         SUM (NVL (ih.nbr_of_picks, 0)) nbr_of_picks,
                         NVL (
                            CASE ih.ship_unit
                               WHEN 1 THEN SUM (NVL (ih.movement, 0))
                            END,
                            0)
                            AS mov1,
                         NVL (
                            CASE ih.ship_unit
                               WHEN 2 THEN SUM (NVL (ih.movement, 0))
                            END,
                            0)
                            AS mov2,
                         NVL (
                            CASE ih.ship_unit
                               WHEN 4 THEN SUM (NVL (ih.movement, 0))
                            END,
                            0)
                            AS mov4,
                         NVL (
                            CASE ih.ship_unit
                               WHEN 8 THEN SUM (NVL (ih.movement, 0))
                            END,
                            0)
                            AS mov8,
                         NVL (
                            CASE ih.ship_unit
                               WHEN 1 THEN SUM (NVL (ih.inventory, 0))
                            END,
                            0)
                            AS inv1,
                         NVL (
                            CASE ih.ship_unit
                               WHEN 2 THEN SUM (NVL (ih.inventory, 0))
                            END,
                            0)
                            AS inv2,
                         NVL (
                            CASE ih.ship_unit
                               WHEN 4 THEN SUM (NVL (ih.inventory, 0))
                            END,
                            0)
                            AS inv4,
                         NVL (
                            CASE ih.ship_unit
                               WHEN 8 THEN SUM (NVL (ih.inventory, 0))
                            END,
                            0)
                            AS inv8,
                         ih.start_date AS start_date,
                         ih.end_date AS end_date
                    FROM    item_history ih
                         INNER JOIN
                            slot_item si
                         ON si.slotitem_id = ih.slotitem_id
                         left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                   WHERE     si.sku_id = v_sku_id
                         AND si.ship_unit = v_slot_ship_unit
                         AND si.delete_mult <> 1
                         AND NVL (si.hist_match, '0x') = v_hist_match
                         AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                         AND si.slot_id IS NULL
                GROUP BY ih.ship_unit,
                         si.pallete_pattern,
                         ih.start_date,
                         ih.end_date) t
      GROUP BY t.ship_unit,
               t.pallete_pattern,
               t.start_date,
               t.end_date
      ORDER BY t.start_date NULLS FIRST, t.end_date NULLS FIRST;

   v_i_ih_loopctl := 1;

   SELECT MIN (i_ih_rowid)
     INTO v_i_ih_nextrowid
     FROM tt_temp_item_history_smry;

   IF (NVL (v_i_ih_nextrowid, 0) = 0)
   THEN
      DBMS_OUTPUT.put_line (
         'info: no data in item history table to consolidate for the selected sku group.');
      v_errcode := 0;
      v_bis_slotitem_good_to_delete := 1;

      GOTO last_call;
   END IF;

   SELECT i_ih_rowid,
          ship_unit,
          pallete_pattern,
          nbr_of_picks,
          mov1,
          mov2,
          mov4,
          mov8,
          inv1,
          inv2,
          inv4,
          inv8,
          start_date,
          end_date
     INTO v_i_ih_currowid,
          v_item_ship_unit,
          v_pallete_pattern,
          v_nbr_of_picks,
          v_mov_pallet,
          v_mov_case,
          v_mov_inner,
          v_mov_each,
          v_inv_pallet,
          v_inv_case,
          v_inv_inner,
          v_inv_each,
          v_start_date,
          v_end_date
     FROM tt_temp_item_history_smry
    WHERE i_ih_rowid = v_i_ih_nextrowid;


   WHILE v_i_ih_loopctl = 1
   LOOP
      SELECT COUNT (*)
        INTO v_if_exists
        FROM    item_history ih
             INNER JOIN
                slot_item si
             ON si.slotitem_id = ih.slotitem_id
             left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
       WHERE     si.sku_id = v_sku_id
             AND si.ship_unit = v_slot_ship_unit
             AND si.delete_mult <> 1
             AND NVL (si.hist_match, '0x') = v_hist_match
             AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
             AND si.slot_id IS NOT NULL
             AND ih.start_date = v_start_date
             AND ih.end_date = v_end_date
             AND ROWNUM <= 1;


      IF v_if_exists > 0
      THEN
         v_recs_updtd_tot := 0;
         v_recs_updtd_tmp := 0;

         SELECT COUNT (1)
           INTO v_no_of_item_hist_recs
           FROM    item_history ih
                INNER JOIN
                   slot_item si
                ON si.slotitem_id = ih.slotitem_id
                left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
          WHERE     si.sku_id = v_sku_id
                AND si.ship_unit = v_slot_ship_unit
                AND si.delete_mult <> 1
                AND NVL (si.hist_match, '0x') = v_hist_match
                AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                AND si.slot_id IS NOT NULL
                AND ih.start_date = v_start_date
                AND ih.end_date = v_end_date;

         BEGIN
         UPDATE item_history ih
         SET ih.nbr_of_picks = ih.nbr_of_picks + (v_nbr_of_picks /v_no_of_item_hist_recs),
             ih.mod_date_time = SYSTIMESTAMP, ih. mod_user = 'consoldtd'
         where ih.slotitem_id in ( select slotitem_id 
                                   from slot_item si  
                                   LEFT JOIN slot s ON s.slot_id = si.slot_id AND s.whse_code = p_whse
                                   where  si.sku_id = v_sku_id
                                       AND si.ship_unit = v_slot_ship_unit
                                       AND si.delete_mult <> 1
                                       AND NVL (si.hist_match, '0x') = v_hist_match
                                       AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                                       AND si.slot_id IS NOT NULL
                                       AND ih.start_date =v_start_date
                                       AND ih.end_date = v_end_date);        
            
         EXCEPTION
            WHEN OTHERS
            THEN
               v_errcode := SQLCODE;
               GOTO handle_error;
         END;

         swv_rowcount := SQL%ROWCOUNT;
         v_recs_updtd_tmp := swv_rowcount;


         v_recs_updtd_tot := v_recs_updtd_tmp + v_recs_updtd_tot;



         UPDATE item_history ih_upd
            SET (movement,
                 inventory,
                 mod_date_time,
                 mod_user) =
                   (SELECT CASE
                              WHEN v_item_ship_unit = 1
                              THEN
                                 ih.movement
                                 + (v_mov_pallet / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.movement
                                 + ( (v_mov_case / (v_wh_hi * v_wh_ti))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.movement
                                 + ( (v_mov_case / (v_ord_hi * v_ord_ti))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.movement
                                 + ( (v_mov_case / (v_ven_hi * v_ven_ti))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.movement
                                 + ( (v_mov_inner
                                      / (v_wh_hi * v_wh_ti * v_inn_per_cs))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.movement
                                 + ( (v_mov_inner
                                      / (v_ord_hi * v_ord_ti * v_inn_per_cs))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.movement
                                 + ( (v_mov_inner
                                      / (v_ven_hi * v_ven_ti * v_inn_per_cs))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.movement
                                 + ( (v_mov_each
                                      / (v_wh_hi * v_wh_ti * v_each_per_cs))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.movement
                                 + ( (v_mov_each
                                      / (v_ord_hi * v_ord_ti * v_each_per_cs))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.movement
                                 + ( (v_mov_each
                                      / (v_ven_hi * v_ven_ti * v_each_per_cs))
                                    / v_no_of_item_hist_recs)
                           END,
                           CASE
                              WHEN v_item_ship_unit = 1
                              THEN
                                 ih.inventory
                                 + (v_inv_pallet / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.inventory
                                 + ( (v_inv_case / (v_wh_hi * v_wh_ti))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.inventory
                                 + ( (v_inv_case / (v_ord_hi * v_ord_ti))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.inventory
                                 + ( (v_inv_case / (v_ven_hi * v_ven_ti))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.inventory
                                 + ( (v_inv_inner
                                      / (v_wh_hi * v_wh_ti * v_inn_per_cs))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.inventory
                                 + ( (v_inv_inner
                                      / (v_ord_hi * v_ord_ti * v_inn_per_cs))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.inventory
                                 + ( (v_inv_inner
                                      / (v_ven_hi * v_ven_ti * v_inn_per_cs))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.inventory
                                 + ( (v_inv_each
                                      / (v_wh_hi * v_wh_ti * v_each_per_cs))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.inventory
                                 + ( (v_inv_each
                                      / (v_ord_hi * v_ord_ti * v_each_per_cs))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.inventory
                                 + ( (v_inv_each
                                      / (v_ven_hi * v_ven_ti * v_each_per_cs))
                                    / v_no_of_item_hist_recs)
                           END,
                           SYSTIMESTAMP,
                           'consoldtd'
                      FROM    item_history ih
                           INNER JOIN
                              slot_item si
                           ON si.slotitem_id = ih.slotitem_id
                           left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                     WHERE (    si.sku_id = v_sku_id
                            AND si.ship_unit = v_slot_ship_unit
                            AND si.delete_mult <> 1
                            AND NVL (si.hist_match, '0x') = v_hist_match
                            AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                            AND si.slot_id IS NOT NULL
                            AND ih.start_date = v_start_date
                            AND ih.end_date = v_end_date
                            AND ih.ship_unit = 1)
                           AND ih.ROWID = ih_upd.ROWID)
          WHERE ROWID IN
                   (SELECT ih.ROWID
                      FROM    item_history ih
                           INNER JOIN
                              slot_item si
                           ON si.slotitem_id = ih.slotitem_id
                           left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                     WHERE     si.sku_id = v_sku_id
                           AND si.ship_unit = v_slot_ship_unit
                           AND si.delete_mult <> 1
                           AND NVL (si.hist_match, '0x') = v_hist_match
                           AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                           AND si.slot_id IS NOT NULL
                           AND ih.start_date = v_start_date
                           AND ih.end_date = v_end_date
                           AND ih.ship_unit = 1);

         swv_rowcount := SQL%ROWCOUNT;
         v_recs_updtd_tmp := swv_rowcount;
         v_recs_updtd_tot := v_recs_updtd_tmp + v_recs_updtd_tot;

         UPDATE item_history ih_upd
            SET (movement,
                 inventory,
                 mod_date_time,
                 mod_user) =
                   (SELECT CASE
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.movement
                                 + ( (v_mov_pallet * v_wh_hi * v_wh_ti)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.movement
                                 + ( (v_mov_pallet * v_ord_hi * v_ord_ti)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.movement
                                 + ( (v_mov_pallet * v_ven_hi * v_ven_ti)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                              THEN
                                 ih.movement
                                 + (v_mov_case / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                              THEN
                                 ih.movement
                                 + ( (v_mov_inner / v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                              THEN
                                 ih.movement
                                 + ( (v_mov_each / v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                           END,
                           CASE
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.inventory
                                 + ( (v_inv_pallet * v_wh_hi * v_wh_ti)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.inventory
                                 + ( (v_inv_pallet * v_ord_hi * v_ord_ti)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.inventory
                                 + ( (v_inv_pallet * v_ven_hi * v_ven_ti)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                              THEN
                                 ih.inventory
                                 + (v_inv_case / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                              THEN
                                 ih.inventory
                                 + ( (v_inv_inner / v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                              THEN
                                 ih.inventory
                                 + ( (v_inv_each / v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                           END,
                           SYSTIMESTAMP,
                           'consoldtd'
                      FROM    item_history ih
                           INNER JOIN
                              slot_item si
                           ON si.slotitem_id = ih.slotitem_id
                           left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                     WHERE (    si.sku_id = v_sku_id
                            AND si.ship_unit = v_slot_ship_unit
                            AND si.delete_mult <> 1
                            AND NVL (si.hist_match, '0x') = v_hist_match
                            AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                            AND si.slot_id IS NOT NULL
                            AND ih.start_date = v_start_date
                            AND ih.end_date = v_end_date
                            AND ih.ship_unit = 2)
                           AND ih.ROWID = ih_upd.ROWID)
          WHERE ROWID IN
                   (SELECT ih.ROWID
                      FROM    item_history ih
                           INNER JOIN
                              slot_item si
                           ON si.slotitem_id = ih.slotitem_id
                           left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                     WHERE     si.sku_id = v_sku_id
                           AND si.ship_unit = v_slot_ship_unit
                           AND si.delete_mult <> 1
                           AND NVL (si.hist_match, '0x') = v_hist_match
                           AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                           AND si.slot_id IS NOT NULL
                           AND ih.start_date = v_start_date
                           AND ih.end_date = v_end_date
                           AND ih.ship_unit = 2);

         swv_rowcount := SQL%ROWCOUNT;
         v_recs_updtd_tmp := swv_rowcount;
         v_recs_updtd_tot := v_recs_updtd_tmp + v_recs_updtd_tot;

         UPDATE item_history ih_upd
            SET (movement,
                 inventory,
                 mod_date_time,
                 mod_user) =
                   (SELECT CASE
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.movement
                                 + ( (  v_mov_pallet
                                      * v_wh_hi
                                      * v_wh_ti
                                      * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.movement
                                 + ( (  v_mov_pallet
                                      * v_ord_hi
                                      * v_ord_ti
                                      * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.movement
                                 + ( (  v_mov_pallet
                                      * v_ven_hi
                                      * v_ven_ti
                                      * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                              THEN
                                 ih.movement
                                 + ( (v_mov_case * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                              THEN
                                 ih.movement
                                 + (v_mov_inner / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                              THEN
                                 ih.movement
                                 + ( (v_mov_each / v_each_per_inn)
                                    / v_no_of_item_hist_recs)
                           END,
                           CASE
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.inventory
                                 + ( (  v_inv_pallet
                                      * v_wh_hi
                                      * v_wh_ti
                                      * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.inventory
                                 + ( (  v_inv_pallet
                                      * v_ord_hi
                                      * v_ord_ti
                                      * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.inventory
                                 + ( (  v_inv_pallet
                                      * v_ven_hi
                                      * v_ven_ti
                                      * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                              THEN
                                 ih.inventory
                                 + ( (v_inv_case * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                              THEN
                                 ih.inventory
                                 + (v_inv_inner / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                              THEN
                                 ih.inventory
                                 + ( (v_inv_each / v_each_per_inn)
                                    / v_no_of_item_hist_recs)
                           END,
                           SYSTIMESTAMP,
                           'consoldtd'
                      FROM    item_history ih
                           INNER JOIN
                              slot_item si
                           ON si.slotitem_id = ih.slotitem_id
                           left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                     WHERE (    si.sku_id = v_sku_id
                            AND si.ship_unit = v_slot_ship_unit
                            AND si.delete_mult <> 1
                            AND NVL (si.hist_match, '0x') = v_hist_match
                            AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                            AND si.slot_id IS NOT NULL
                            AND ih.start_date = v_start_date
                            AND ih.end_date = v_end_date
                            AND ih.ship_unit = 4)
                           AND ih.ROWID = ih_upd.ROWID)
          WHERE ROWID IN
                   (SELECT ih.ROWID
                      FROM    item_history ih
                           INNER JOIN
                              slot_item si
                           ON si.slotitem_id = ih.slotitem_id
                           left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                     WHERE     si.sku_id = v_sku_id
                           AND si.ship_unit = v_slot_ship_unit
                           AND si.delete_mult <> 1
                           AND NVL (si.hist_match, '0x') = v_hist_match
                           AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                           AND si.slot_id IS NOT NULL
                           AND ih.start_date = v_start_date
                           AND ih.end_date = v_end_date
                           AND ih.ship_unit = 4);

         swv_rowcount := SQL%ROWCOUNT;
         v_recs_updtd_tmp := swv_rowcount;
         v_recs_updtd_tot := v_recs_updtd_tmp + v_recs_updtd_tot;

         UPDATE item_history ih_upd
            SET (movement,
                 inventory,
                 mod_date_time,
                 mod_user) =
                   (SELECT CASE
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.movement
                                 + ( (  v_mov_pallet
                                      * v_wh_hi
                                      * v_wh_ti
                                      * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.movement
                                 + ( (  v_mov_pallet
                                      * v_ord_hi
                                      * v_ord_ti
                                      * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.movement
                                 + ( (  v_mov_pallet
                                      * v_ven_hi
                                      * v_ven_ti
                                      * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                              THEN
                                 ih.movement
                                 + ( (v_mov_case * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                              THEN
                                 ih.movement
                                 + ( (v_mov_inner * v_each_per_inn)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                              THEN
                                 ih.movement
                                 + (v_mov_each / v_no_of_item_hist_recs)
                           END,
                           CASE
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.inventory
                                 + ( (  v_inv_pallet
                                      * v_wh_hi
                                      * v_wh_ti
                                      * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.inventory
                                 + ( (  v_inv_pallet
                                      * v_ord_hi
                                      * v_ord_ti
                                      * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.inventory
                                 + ( (  v_inv_pallet
                                      * v_ven_hi
                                      * v_ven_ti
                                      * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                              THEN
                                 ih.inventory
                                 + ( (v_inv_case * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                              THEN
                                 ih.inventory
                                 + ( (v_inv_inner * v_each_per_inn)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                              THEN
                                 ih.inventory
                                 + (v_inv_each / v_no_of_item_hist_recs)
                           END,
                           SYSTIMESTAMP,
                           'consoldtd'
                      FROM    item_history ih
                           INNER JOIN
                              slot_item si
                           ON si.slotitem_id = ih.slotitem_id
                           left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                     WHERE (    si.sku_id = v_sku_id
                            AND si.ship_unit = v_slot_ship_unit
                            AND si.delete_mult <> 1
                            AND NVL (si.hist_match, '0x') = v_hist_match
                            AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                            AND si.slot_id IS NOT NULL
                            AND ih.start_date = v_start_date
                            AND ih.end_date = v_end_date
                            AND ih.ship_unit = 8)
                           AND ih.ROWID = ih_upd.ROWID)
          WHERE ROWID IN
                   (SELECT ih.ROWID
                      FROM    item_history ih
                           INNER JOIN
                              slot_item si
                           ON si.slotitem_id = ih.slotitem_id
                           left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                     WHERE     si.sku_id = v_sku_id
                           AND si.ship_unit = v_slot_ship_unit
                           AND si.delete_mult <> 1
                           AND NVL (si.hist_match, '0x') = v_hist_match
                           AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                           AND si.slot_id IS NOT NULL
                           AND ih.start_date = v_start_date
                           AND ih.end_date = v_end_date
                           AND ih.ship_unit = 8);

         swv_rowcount := SQL%ROWCOUNT;
         v_recs_updtd_tmp := swv_rowcount;
         v_recs_updtd_tot := v_recs_updtd_tmp + v_recs_updtd_tot;

         IF (v_recs_updtd_tot > 0)
         THEN
            v_strsqlst :=
               'delete from item_history '
               || ' where hist_id in  ( select hist_id from item_history ih'
               || ' inner join slot_item si on si.slotitem_id = ih.slotitem_id left join slot s on s.slot_id = si.slot_id and s.whse_code = '''
               || p_whse  
               || ''' where si.sku_id = '
               || v_sku_id
               || ' and si.ship_unit = '
               || v_slot_ship_unit
               || ' and si.delete_mult <> '
               || 1
               || ' and nvl(si.hist_match,''0x'') = '''
               || v_hist_match
               || ''' and nvl(si.mult_loc_grp,''0x'') = '''
               || v_mult_loc_grp
               || ''' and si.slot_id is null'
               || ' and ih.start_date = '''
               || v_start_date
               || ''' and ih.end_date = '''
               || v_end_date
               || ''' and ih.ship_unit = '''
               || v_item_ship_unit
               || ''')';



            BEGIN
               EXECUTE IMMEDIATE v_strsqlst;
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_errcode := SQLCODE;
                  GOTO handle_error;
            END;



            v_bis_slotitem_good_to_delete := 1;
         ELSE
            DBMS_OUTPUT.put_line (
               'error: oops!!! for some reasons none of the item history records are updated....rolling back transaction');
            v_errcode := -1;
            GOTO handle_error;
         END IF;
      END IF;

      v_i_ih_nextrowid := NULL;

      SELECT NVL (MIN (i_ih_rowid), 0)
        INTO v_i_ih_nextrowid
        FROM tt_temp_item_history_smry
       WHERE i_ih_rowid > v_i_ih_currowid;


      IF NVL (v_i_ih_nextrowid, 0) = 0
      THEN
         EXIT;
      END IF;


      SELECT i_ih_rowid,
             ship_unit,
             pallete_pattern,
             nbr_of_picks,
             mov1,
             mov2,
             mov4,
             mov8,
             inv1,
             inv2,
             inv4,
             inv8,
             start_date,
             end_date
        INTO v_i_ih_currowid,
             v_item_ship_unit,
             v_pallete_pattern,
             v_nbr_of_picks,
             v_mov_pallet,
             v_mov_case,
             v_mov_inner,
             v_mov_each,
             v_inv_pallet,
             v_inv_case,
             v_inv_inner,
             v_inv_each,
             v_start_date,
             v_end_date
        FROM tt_temp_item_history_smry
       WHERE i_ih_rowid = v_i_ih_nextrowid;
   END LOOP;

   EXECUTE IMMEDIATE ' delete tt_temp_item_history_smry ';

   INSERT INTO tt_temp_item_history_smry (ship_unit,
                                          pallete_pattern,
                                          nbr_of_picks,
                                          mov1,
                                          mov2,
                                          mov4,
                                          mov8,
                                          inv1,
                                          inv2,
                                          inv4,
                                          inv8,
                                          start_date,
                                          end_date)
        SELECT DISTINCT v_slot_ship_unit,
                        si.pallete_pattern,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        ih.start_date,
                        ih.end_date
          FROM    item_history ih
               INNER JOIN
                  slot_item si
               ON si.slotitem_id = ih.slotitem_id
               left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
         WHERE     si.sku_id = v_sku_id
               AND si.ship_unit = v_slot_ship_unit
               AND si.delete_mult <> 1
               AND NVL (si.hist_match, '0x') = v_hist_match
               AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
               AND si.slot_id IS NULL
      ORDER BY ih.start_date NULLS FIRST, si.pallete_pattern NULLS FIRST;

   v_i_ih_loopctl := 1;

   SELECT MIN (i_ih_rowid)
     INTO v_i_ih_nextrowid
     FROM tt_temp_item_history_smry;

   IF (NVL (v_i_ih_nextrowid, 0) = 0)
   THEN
      DBMS_OUTPUT.put_line (
         'info: there is no data in item history table to consolidate by inserting the slot_items that do not have a previous history');
      GOTO last_call;
   END IF;


   UPDATE tt_temp_item_history_smry tihs_upd
      SET (mov1,
           mov2,
           mov4,
           mov8,
           inv1,
           inv2,
           inv4,
           inv8,
           nbr_of_picks) =
             (SELECT tout.mov1,
                     tout.mov2,
                     tout.mov4,
                     tout.mov8,
                     tout.inv1,
                     tout.inv2,
                     tout.inv4,
                     tout.inv8,
                     tout.nbr_of_picks
                FROM    tt_temp_item_history_smry tihs
                     INNER JOIN
                        (  SELECT SUM (thist.mov1) mov1,
                                  SUM (thist.mov2) mov2,
                                  SUM (thist.mov4) mov4,
                                  SUM (thist.mov8) mov8,
                                  SUM (thist.inv1) inv1,
                                  SUM (thist.inv2) inv2,
                                  SUM (thist.inv4) inv4,
                                  SUM (thist.inv8) inv8,
                                  thist.start_date,
                                  thist.end_date,
                                  thist.pallete_pattern,
                                  SUM (thist.nbr_of_picks) nbr_of_picks
                             FROM (  SELECT NVL (
                                               CASE ih.ship_unit
                                                  WHEN 1
                                                  THEN
                                                     SUM (NVL (ih.movement, 0))
                                               END,
                                               0)
                                               AS mov1,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 2
                                                  THEN
                                                     SUM (NVL (ih.movement, 0))
                                               END,
                                               0)
                                               AS mov2,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 4
                                                  THEN
                                                     SUM (NVL (ih.movement, 0))
                                               END,
                                               0)
                                               AS mov4,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 8
                                                  THEN
                                                     SUM (NVL (ih.movement, 0))
                                               END,
                                               0)
                                               AS mov8,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 1
                                                  THEN
                                                     SUM (NVL (ih.inventory, 0))
                                               END,
                                               0)
                                               AS inv1,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 2
                                                  THEN
                                                     SUM (NVL (ih.inventory, 0))
                                               END,
                                               0)
                                               AS inv2,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 4
                                                  THEN
                                                     SUM (NVL (ih.inventory, 0))
                                               END,
                                               0)
                                               AS inv4,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 8
                                                  THEN
                                                     SUM (NVL (ih.inventory, 0))
                                               END,
                                               0)
                                               AS inv8,
                                            SUM (NVL (ih.nbr_of_picks, 0))
                                            / v_no_of_slot_item_recs
                                               AS nbr_of_picks,
                                            ih.start_date,
                                            ih.end_date,
                                            si.pallete_pattern
                                       FROM    item_history ih
                                            INNER JOIN
                                               slot_item si
                                            ON si.slotitem_id = ih.slotitem_id
                                            left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                                      WHERE     si.sku_id = v_sku_id
                                            AND si.ship_unit = v_slot_ship_unit
                                            AND si.delete_mult <> 1
                                            AND NVL (si.hist_match, '0x') =
                                                   v_hist_match
                                            AND NVL (si.mult_loc_grp, '0x') =
                                                   v_mult_loc_grp
                                            AND si.slot_id IS NULL
                                   --sqlways_eval# = '2008-08-07 00:00:00.000' and ih.end_date = '2008-08-13 00:00:00.000'
                                   --sqlways_eval# = 0
                                   GROUP BY ih.ship_unit,
                                            ih.start_date,
                                            ih.end_date,
                                            si.pallete_pattern) thist
                         GROUP BY thist.start_date,
                                  thist.end_date,
                                  thist.pallete_pattern) tout
                     ON     tout.start_date = tihs.start_date
                        AND tout.end_date = tihs.end_date
                        AND tout.pallete_pattern = tihs.pallete_pattern
               WHERE tihs.ROWID = tihs_upd.ROWID)
    WHERE ROWID IN
             (SELECT tihs.ROWID
                FROM    tt_temp_item_history_smry tihs
                     INNER JOIN
                        (  SELECT SUM (thist.mov1) mov1,
                                  SUM (thist.mov2) mov2,
                                  SUM (thist.mov4) mov4,
                                  SUM (thist.mov8) mov8,
                                  SUM (thist.inv1) inv1,
                                  SUM (thist.inv2) inv2,
                                  SUM (thist.inv4) inv4,
                                  SUM (thist.inv8) inv8,
                                  thist.start_date,
                                  thist.end_date,
                                  thist.pallete_pattern,
                                  SUM (thist.nbr_of_picks) nbr_of_picks
                             FROM (  SELECT NVL (
                                               CASE ih.ship_unit
                                                  WHEN 1
                                                  THEN
                                                     SUM (
                                                        NVL (ih.movement,
                                                             0))
                                               END,
                                               0)
                                               AS mov1,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 2
                                                  THEN
                                                     SUM (
                                                        NVL (ih.movement,
                                                             0))
                                               END,
                                               0)
                                               AS mov2,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 4
                                                  THEN
                                                     SUM (
                                                        NVL (ih.movement,
                                                             0))
                                               END,
                                               0)
                                               AS mov4,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 8
                                                  THEN
                                                     SUM (
                                                        NVL (ih.movement,
                                                             0))
                                               END,
                                               0)
                                               AS mov8,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 1
                                                  THEN
                                                     SUM (
                                                        NVL (ih.inventory,
                                                             0))
                                               END,
                                               0)
                                               AS inv1,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 2
                                                  THEN
                                                     SUM (
                                                        NVL (ih.inventory,
                                                             0))
                                               END,
                                               0)
                                               AS inv2,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 4
                                                  THEN
                                                     SUM (
                                                        NVL (ih.inventory,
                                                             0))
                                               END,
                                               0)
                                               AS inv4,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 8
                                                  THEN
                                                     SUM (
                                                        NVL (ih.inventory,
                                                             0))
                                               END,
                                               0)
                                               AS inv8,
                                            SUM (NVL (ih.nbr_of_picks, 0))
                                            / v_no_of_slot_item_recs
                                               AS nbr_of_picks,
                                            ih.start_date,
                                            ih.end_date,
                                            si.pallete_pattern
                                       FROM    item_history ih
                                            INNER JOIN
                                               slot_item si
                                            ON si.slotitem_id =
                                                  ih.slotitem_id
                                            left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                                      WHERE si.sku_id = v_sku_id
                                            AND si.ship_unit =
                                                   v_slot_ship_unit
                                            AND si.delete_mult <> 1
                                            AND NVL (si.hist_match, '0x') =
                                                   v_hist_match
                                            AND NVL (si.mult_loc_grp, '0x') =
                                                   v_mult_loc_grp
                                            AND si.slot_id IS NULL
                                   GROUP BY ih.ship_unit,
                                            ih.start_date,
                                            ih.end_date,
                                            si.pallete_pattern) thist
                         GROUP BY thist.start_date,
                                  thist.end_date,
                                  thist.pallete_pattern) tout
                     ON     tout.start_date = tihs.start_date
                        AND tout.end_date = tihs.end_date
                        AND tout.pallete_pattern = tihs.pallete_pattern);


   SELECT i_ih_rowid,
          ship_unit,
          pallete_pattern,
          nbr_of_picks,
          mov1,
          mov2,
          mov4,
          mov8,
          inv1,
          inv2,
          inv4,
          inv8,
          start_date,
          end_date
     INTO v_i_ih_currowid,
          v_item_ship_unit,
          v_pallete_pattern,
          v_nbr_of_picks,
          v_mov_pallet,
          v_mov_case,
          v_mov_inner,
          v_mov_each,
          v_inv_pallet,
          v_inv_case,
          v_inv_inner,
          v_inv_each,
          v_start_date,
          v_end_date
     FROM tt_temp_item_history_smry
    WHERE i_ih_rowid = v_i_ih_nextrowid;

   WHILE v_i_ih_loopctl = 1
   LOOP
      SELECT COUNT (*)
        INTO v_if_exists2
        FROM    item_history ih
             INNER JOIN
                slot_item si
             ON si.slotitem_id = ih.slotitem_id
             left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
       WHERE     si.sku_id = v_sku_id
             AND si.ship_unit = v_slot_ship_unit
             AND si.delete_mult <> 1
             AND NVL (si.hist_match, '0x') = v_hist_match
             AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
             AND si.slot_id IS NOT NULL
             AND ih.start_date = v_start_date
             AND ih.end_date = v_end_date;

      IF v_if_exists2 = 0
      THEN
         EXECUTE IMMEDIATE ' delete tt_temp_item_history_inst ';

         /*slot-4302
         select curr_nbr
           into v_hist_id
           from unique_ids
          where rec_type = 'item_history';

         v_hist_id := v_hist_id + 1;*/



         DBMS_OUTPUT.put_line ('inserting new records in item_history...');

         INSERT INTO tt_temp_item_history_inst (slotitem_id,
                                                start_date,
                                                end_date,
                                                ship_unit,
                                                nbr_of_picks,
                                                movement,
                                                inventory)
              SELECT si.slotitem_id,
                     v_start_date,
                     v_end_date,
                     v_slot_ship_unit,
                     v_nbr_of_picks,
                     0,
                     0
                FROM slot_item si
                left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
               WHERE     si.sku_id = v_sku_id
                     AND si.ship_unit = v_slot_ship_unit
                     AND si.delete_mult <> 1
                     AND NVL (si.hist_match, '0x') = v_hist_match
                     AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                     AND si.slot_id IS NOT NULL
            ORDER BY si.slotitem_id NULLS FIRST;



         UPDATE tt_temp_item_history_inst
            SET                              --hist_id = v_hist_id,--slot-4302
               movement =
                   CASE
                      WHEN v_slot_ship_unit = 8 AND v_pallete_pattern = 0
                      THEN
                         ( (v_mov_pallet * v_wh_hi * v_wh_ti * v_each_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_mov_case * v_each_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_inner * v_each_per_inn)
                            / v_no_of_slot_item_recs)
                         + (v_mov_each / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 8 AND v_pallete_pattern = 1
                      THEN
                         ( (  v_mov_pallet
                            * v_ord_hi
                            * v_ord_ti
                            * v_each_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_mov_case * v_each_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_inner * v_each_per_inn)
                            / v_no_of_slot_item_recs)
                         + (v_mov_each / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 8 AND v_pallete_pattern = 2
                      THEN
                         ( (  v_mov_pallet
                            * v_ven_hi
                            * v_ven_ti
                            * v_each_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_mov_case * v_each_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_inner * v_each_per_inn)
                            / v_no_of_slot_item_recs)
                         + (v_mov_each / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 4 AND v_pallete_pattern = 0
                      THEN
                         ( (v_mov_pallet * v_wh_hi * v_wh_ti * v_inn_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_mov_case * v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + (v_mov_inner / v_no_of_slot_item_recs)
                         + ( (v_mov_each / v_each_per_inn)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 4 AND v_pallete_pattern = 1
                      THEN
                         ( (v_mov_pallet * v_ord_hi * v_ord_ti * v_inn_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_mov_case * v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + (v_mov_inner / v_no_of_slot_item_recs)
                         + ( (v_mov_each / v_each_per_inn)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 4 AND v_pallete_pattern = 2
                      THEN
                         ( (v_mov_pallet * v_ven_hi * v_ven_ti * v_inn_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_mov_case * v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + (v_mov_inner / v_no_of_slot_item_recs)
                         + ( (v_mov_each / v_each_per_inn)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 2 AND v_pallete_pattern = 0
                      THEN
                         ( (v_mov_pallet * v_wh_hi * v_wh_ti)
                          / v_no_of_slot_item_recs)
                         + (v_mov_case / v_no_of_slot_item_recs)
                         + ( (v_mov_inner / v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_each / v_each_per_cs)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 2 AND v_pallete_pattern = 1
                      THEN
                         ( (v_mov_pallet * v_ord_hi * v_ord_ti)
                          / v_no_of_slot_item_recs)
                         + (v_mov_case / v_no_of_slot_item_recs)
                         + ( (v_mov_inner / v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_each / v_each_per_cs)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 2 AND v_pallete_pattern = 2
                      THEN
                         ( (v_mov_pallet * v_ven_hi * v_ven_ti)
                          / v_no_of_slot_item_recs)
                         + (v_mov_case / v_no_of_slot_item_recs)
                         + ( (v_mov_inner / v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_each / v_each_per_cs)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 1 AND v_pallete_pattern = 0
                      THEN
                         (v_mov_pallet / v_no_of_slot_item_recs)
                         + ( (v_mov_case / (v_wh_hi * v_wh_ti))
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_inner
                              / (v_wh_hi * v_wh_ti * v_inn_per_cs))
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_each
                              / (v_wh_hi * v_wh_ti * v_each_per_cs))
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 1 AND v_pallete_pattern = 1
                      THEN
                         (v_mov_pallet / v_no_of_slot_item_recs)
                         + ( (v_mov_case / (v_ord_hi * v_ord_ti))
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_inner
                              / (v_ord_hi * v_ord_ti * v_inn_per_cs))
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_each
                              / (v_ord_hi * v_ord_ti * v_each_per_cs))
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 1 AND v_pallete_pattern = 2
                      THEN
                         (v_mov_pallet / v_no_of_slot_item_recs)
                         + ( (v_mov_case / (v_ven_hi * v_ven_ti))
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_inner
                              / (v_ven_hi * v_ven_ti * v_inn_per_cs))
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_each
                              / (v_ven_hi * v_ven_ti * v_each_per_cs))
                            / v_no_of_slot_item_recs)
                   END,
                inventory =
                   CASE
                      WHEN v_slot_ship_unit = 8 AND v_pallete_pattern = 0
                      THEN
                         ( (v_inv_pallet * v_wh_hi * v_wh_ti * v_each_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_inv_case * v_each_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_inner * v_each_per_inn)
                            / v_no_of_slot_item_recs)
                         + (v_inv_each / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 8 AND v_pallete_pattern = 1
                      THEN
                         ( (  v_inv_pallet
                            * v_ord_hi
                            * v_ord_ti
                            * v_each_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_inv_case * v_each_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_inner * v_each_per_inn)
                            / v_no_of_slot_item_recs)
                         + (v_inv_each / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 8 AND v_pallete_pattern = 2
                      THEN
                         ( (  v_inv_pallet
                            * v_ven_hi
                            * v_ven_ti
                            * v_each_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_inv_case * v_each_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_inner * v_each_per_inn)
                            / v_no_of_slot_item_recs)
                         + (v_inv_each / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 4 AND v_pallete_pattern = 0
                      THEN
                         ( (v_inv_pallet * v_wh_hi * v_wh_ti * v_inn_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_inv_case * v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + (v_inv_inner / v_no_of_slot_item_recs)
                         + ( (v_inv_each / v_each_per_inn)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 4 AND v_pallete_pattern = 1
                      THEN
                         ( (v_inv_pallet * v_ord_hi * v_ord_ti * v_inn_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_inv_case * v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + (v_inv_inner / v_no_of_slot_item_recs)
                         + ( (v_inv_each / v_each_per_inn)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 4 AND v_pallete_pattern = 2
                      THEN
                         ( (v_inv_pallet * v_ven_hi * v_ven_ti * v_inn_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_inv_case * v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + (v_inv_inner / v_no_of_slot_item_recs)
                         + ( (v_inv_each / v_each_per_inn)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 2 AND v_pallete_pattern = 0
                      THEN
                         ( (v_inv_pallet * v_wh_hi * v_wh_ti)
                          / v_no_of_slot_item_recs)
                         + (v_inv_case / v_no_of_slot_item_recs)
                         + ( (v_inv_inner / v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_each / v_each_per_cs)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 2 AND v_pallete_pattern = 1
                      THEN
                         ( (v_inv_pallet * v_ord_hi * v_ord_ti)
                          / v_no_of_slot_item_recs)
                         + (v_inv_case / v_no_of_slot_item_recs)
                         + ( (v_inv_inner / v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_each / v_each_per_cs)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 2 AND v_pallete_pattern = 2
                      THEN
                         ( (v_inv_pallet * v_ven_hi * v_ven_ti)
                          / v_no_of_slot_item_recs)
                         + (v_inv_case / v_no_of_slot_item_recs)
                         + ( (v_inv_inner / v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_each / v_each_per_cs)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 1 AND v_pallete_pattern = 0
                      THEN
                         (v_inv_pallet / v_no_of_slot_item_recs)
                         + ( (v_inv_case / (v_wh_hi * v_wh_ti))
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_inner
                              / (v_wh_hi * v_wh_ti * v_inn_per_cs))
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_each
                              / (v_wh_hi * v_wh_ti * v_each_per_cs))
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 1 AND v_pallete_pattern = 1
                      THEN
                         (v_inv_pallet / v_no_of_slot_item_recs)
                         + ( (v_inv_case / (v_ord_hi * v_ord_ti))
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_inner
                              / (v_ord_hi * v_ord_ti * v_inn_per_cs))
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_each
                              / (v_ord_hi * v_ord_ti * v_each_per_cs))
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 1 AND v_pallete_pattern = 2
                      THEN
                         (v_inv_pallet / v_no_of_slot_item_recs)
                         + ( (v_inv_case / (v_ven_hi * v_ven_ti))
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_inner
                              / (v_ven_hi * v_ven_ti * v_inn_per_cs))
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_each
                              / (v_ven_hi * v_ven_ti * v_each_per_cs))
                            / v_no_of_slot_item_recs)
                   END;


         v_strsqlst :=
            'insert into  item_history (hist_id,slotitem_id,start_date,end_date,ship_unit,movement,inventory,nbr_of_picks)
      select hist_id,slotitem_id,start_date,end_date,ship_unit,movement,inventory,nbr_of_picks from tt_temp_item_history_inst';

         -- execute immediate v_strsqlst;

         BEGIN
            EXECUTE IMMEDIATE v_strsqlst;
         EXCEPTION
            WHEN OTHERS
            THEN
               v_errcode := SQLCODE;
               GOTO handle_error;
         END;

         swv_rowcount := SQL%ROWCOUNT;
         v_recs_updtd_tmp := swv_rowcount;


         v_recs_updtd_tmp := 0;

         /*slot-4302
         update unique_ids
            set curr_nbr = v_hist_id
          where rec_type = 'item_history';*/

         v_strsqlst :=
            'delete from item_history '
            || ' where hist_id in  ( select hist_id from item_history ih'
            || ' inner join slot_item si on si.slotitem_id = ih.slotitem_id  left join slot s on s.slot_id = si.slot_id and s.whse_code = '''
            || p_whse 
            || ''' where si.sku_id = '
            || v_sku_id
            || ' and si.ship_unit ='
            || v_slot_ship_unit
            || ' and si.delete_mult <> '
            || 1
            || ' and nvl(si.hist_match,''0x'') = '''
            || v_hist_match
            || ''' and nvl(si.mult_loc_grp,''0x'') = '''
            || v_mult_loc_grp
            || ''' and si.slot_id is null'
            || ' and ih.start_date = '''
            || v_start_date
            || ''' and ih.end_date = '''
            || v_end_date
            || ''')';



         BEGIN
            EXECUTE IMMEDIATE v_strsqlst;
         EXCEPTION
            WHEN OTHERS
            THEN
               v_errcode := SQLCODE;
               GOTO handle_error;
         END;


         v_bis_slotitem_good_to_delete := 1;
      END IF;

      v_i_ih_nextrowid := NULL;

      SELECT NVL (MIN (i_ih_rowid), 0)
        INTO v_i_ih_nextrowid
        FROM tt_temp_item_history_smry
       WHERE i_ih_rowid > v_i_ih_currowid;

      IF NVL (v_i_ih_nextrowid, 0) = 0
      THEN
         EXIT;
      END IF;


      SELECT i_ih_rowid,
             ship_unit,
             pallete_pattern,
             nbr_of_picks,
             mov1,
             mov2,
             mov4,
             mov8,
             inv1,
             inv2,
             inv4,
             inv8,
             start_date,
             end_date
        INTO v_i_ih_currowid,
             v_item_ship_unit,
             v_pallete_pattern,
             v_nbr_of_picks,
             v_mov_pallet,
             v_mov_case,
             v_mov_inner,
             v_mov_each,
             v_inv_pallet,
             v_inv_case,
             v_inv_inner,
             v_inv_each,
             v_start_date,
             v_end_date
        FROM tt_temp_item_history_smry
       WHERE i_ih_rowid = v_i_ih_nextrowid;
   END LOOP;

  <<last_call>>
   IF (v_bis_slotitem_good_to_delete = 1)
   THEN
      DBMS_OUTPUT.put_line ('    deleting records in slot_item_score...');

      DELETE FROM slot_item_score
            WHERE slot_item_score.slotitem_id IN
                     (SELECT DISTINCT si.slotitem_id
                        FROM slot_item si
                        left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                       WHERE     si.sku_id = v_sku_id
                             AND si.ship_unit = v_slot_ship_unit
                             AND si.delete_mult <> 1
                             AND NVL (si.hist_match, '0x') = v_hist_match
                             AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                             AND si.slot_id IS NULL);

      DBMS_OUTPUT.put_line ('    deleting records in hn_failure...');

      DELETE FROM hn_failure
            WHERE hn_failure.slotitem_id IN
                     (SELECT DISTINCT si.slotitem_id
                        FROM slot_item si
                        left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                       WHERE     si.sku_id = v_sku_id
                             AND si.ship_unit = v_slot_ship_unit
                             AND si.delete_mult <> 1
                             AND NVL (si.hist_match, '0x') = v_hist_match
                             AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                             AND si.slot_id IS NULL);

      DBMS_OUTPUT.put_line ('    deleting records in haves_needs...');

      DELETE FROM haves_needs
            WHERE haves_needs.WHSE_CODE = p_whse
                AND haves_needs.slot_item_id IN
                     (SELECT DISTINCT si.slotitem_id
                        FROM slot_item si
                       WHERE     si.sku_id = v_sku_id
                             AND si.ship_unit = v_slot_ship_unit
                             AND si.delete_mult <> 1
                             AND NVL (si.hist_match, '0x') = v_hist_match
                             AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                             AND si.slot_id IS NULL);

      DBMS_OUTPUT.put_line ('    deleting records in needs_results_dtl...');

      DELETE FROM needs_results_dtl
            WHERE needs_results_dtl.slotitem_id IN
                     (SELECT DISTINCT si.slotitem_id
                        FROM slot_item si
                        left join slot s on s.slot_id = si.slot_id and s.whse_code = p_whse
                       WHERE     si.sku_id = v_sku_id
                             AND si.ship_unit = v_slot_ship_unit
                             AND si.delete_mult <> 1
                             AND NVL (si.hist_match, '0x') = v_hist_match
                             AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                             AND si.slot_id IS NULL);


      DBMS_OUTPUT.put_line ('    deleting records in sl_failure_reasons...');

      DELETE FROM sl_failure_reasons
            WHERE sl_failure_reasons.WHSE_CODE = p_whse
                AND sl_failure_reasons.slotitem_id IN
                     (SELECT DISTINCT si.slotitem_id
                        FROM slot_item si
                       WHERE     si.sku_id = v_sku_id
                             AND si.ship_unit = v_slot_ship_unit
                             AND si.delete_mult <> 1
                             AND NVL (si.hist_match, '0x') = v_hist_match
                             AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                             AND si.slot_id IS NULL);
                             
      DBMS_OUTPUT.put_line ('    deleting records in move_list/move_list_hdr...');
      
      DELETE FROM move_list
      WHERE move_hdr_id IN (SELECT move_hdr_id
                              FROM move_list_hdr
                             WHERE whse_code = p_whse);
                     
      DELETE FROM move_list_hdr
      WHERE whse_code = p_whse;

      DBMS_OUTPUT.put_line (
         '    finally deleting all the unslotted records (where slot_id is null) for selected group in slot_item recs ...');
      v_strsqlst :=
            'delete from slot_item '
         || ' where slotitem_id in  ( select slotitem_id from slot_item si left join slot s on s.slot_id = si.slot_id and s.whse_code = '''
         || p_whse
         || ''' where si.sku_id = '
         || v_sku_id
         || ' and si.ship_unit ='
         || v_slot_ship_unit
         || ' and si.delete_mult <>'
         || 1
         || ' and nvl(si.hist_match,''0x'') = '''
         || v_hist_match
         || ''' and nvl(si.mult_loc_grp,''0x'') = '''
         || v_mult_loc_grp
         || ''' and si.slot_id is null ) ';

      BEGIN
         EXECUTE IMMEDIATE v_strsqlst;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_errcode := SQLCODE;
            GOTO handle_error;
      END;
   ELSE
      DBMS_OUTPUT.put_line (
         'warning: slot item records are not deleted for this sku group since there is no change made in item_history');
   END IF;

   v_errcode := 0;



  <<handle_error>>
   NULL;
END;
/

show errors;
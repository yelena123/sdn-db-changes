create or replace
PROCEDURE INS_APPOINTMENT_EVENT 
(
   vAPPOINTMENTID   IN ILM_APPOINTMENT_EVENTS.APPOINTMENT_ID%TYPE,
   vFieldName       IN ILM_APPOINTMENT_EVENTS.FIELD_NAME%TYPE,
   vOldValue        IN ILM_APPOINTMENT_EVENTS.OLD_VALUE%TYPE,
   vNewValue        IN ILM_APPOINTMENT_EVENTS.NEW_VALUE%TYPE,
   vSourceType      IN ILM_APPOINTMENT_EVENTS.CREATED_SOURCE_TYPE%TYPE,
   vSource          IN ILM_APPOINTMENT_EVENTS.CREATED_SOURCE%TYPE
   )
AS
   vEventSeq           NUMBER (8, 0);
   l_old_value         VARCHAR2 (500) := voldvalue;
   l_new_value         VARCHAR2 (500) := vnewValue;
   l_appt_type_desc    VARCHAR2 (50);
   l_appt_type_desc2   VARCHAR2 (50);
BEGIN
   SELECT NVL (MAX (EVENT_SEQ), 0) + 1
     INTO vEventSeq
     FROM ILM_APPOINTMENT_EVENTS
    WHERE APPOINTMENT_ID = vAPPOINTMENTID;

   IF vFieldName = 'APPT_TYPE'
   THEN
      SELECT DESCRIPTION
        INTO l_old_value
        FROM ILM_APPOINTMENT_TYPE
       WHERE APPT_TYPE = voldvalue;

      SELECT DESCRIPTION
        INTO l_new_value
        FROM ILM_APPOINTMENT_TYPE
       WHERE APPT_TYPE = vnewvalue;
   END IF;

   IF vFieldName = 'APPT_STATUS'
   THEN
      SELECT DESCRIPTION
        INTO l_old_value
        FROM ILM_APPOINTMENT_STATUS
       WHERE APPT_STATUS_CODE = voldvalue;

      SELECT DESCRIPTION
        INTO l_new_value
        FROM ILM_APPOINTMENT_STATUS
       WHERE APPT_STATUS_CODE = vnewvalue;
   END IF;

   IF vFieldName = 'CARRIER_CODE'
   THEN
      SELECT CARRIER_CODE
        INTO l_old_value
        FROM CARRIER_CODE
       WHERE CARRIER_ID = voldvalue;

      SELECT CARRIER_CODE
        INTO l_new_value
        FROM CARRIER_CODE
       WHERE CARRIER_ID = vnewvalue;
   END IF;

   IF vFieldName = 'EQUIPMENT_ID'
   THEN
      SELECT EQUIPMENT_CODE
        INTO l_old_value
        FROM EQUIPMENT
       WHERE EQUIPMENT_ID = voldvalue;

      SELECT EQUIPMENT_CODE
        INTO l_new_value
        FROM EQUIPMENT
       WHERE EQUIPMENT_ID = vnewvalue;
   END IF;

   IF vFieldName = 'LOADING_TYPE'
   THEN
      SELECT DESCRIPTION
        INTO l_old_value
        FROM LOADING_TYPE
       WHERE LOADING_TYPE = voldvalue;

      SELECT DESCRIPTION
        INTO l_new_value
        FROM LOADING_TYPE
       WHERE LOADING_TYPE = vnewvalue;
   END IF;

   IF vFieldName = 'REQUEST_COMM_TYPE'
   THEN
      SELECT DESCRIPTION
        INTO l_old_value
        FROM COMM_METHOD
       WHERE COMM_METHOD = voldvalue;

      SELECT DESCRIPTION
        INTO l_new_value
        FROM COMM_METHOD
       WHERE COMM_METHOD = vnewvalue;
   END IF;

   IF vFieldName = 'DRIVER_ID'
   THEN
      l_old_value := getdriver (voldvalue);
      l_new_value := getdriver (vnewvalue);
   END IF;

   INSERT INTO ILM_APPOINTMENT_EVENTS (APPOINTMENT_ID,
                                       EVENT_SEQ,
                                       FIELD_NAME,
                                       OLD_VALUE,
                                       NEW_VALUE,
                                       CREATED_SOURCE_TYPE,
                                       CREATED_SOURCE,
                                       CREATED_DTTM)
        VALUES (vAPPOINTMENTID,
                vEventSeq,
                vFieldName,
                l_old_value,
                l_new_value,
                vSourceType,
                vSource,
                SYSDATE);
END;
/

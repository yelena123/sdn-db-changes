create or replace procedure wm_assign_chutes_to_cartons
(
    p_whse                    in facility.whse%type,
    p_bal_alg                 in pack_wave_parm_hdr.bal_alg%type,
    p_curr_pack_wave_seq_nbr  in sorter_grp.sorter_grp%type default null
)
as
    v_chute_id                chute_master.chute_id%type;
    v_pack_wave_seq_nbr       pack_wave_parm_dtl.pack_wave_seq_nbr%type;
    v_chute_row_id            number(9);
    v_log_lvl                 number(1) := wm_get_curr_log_lvl();
begin
    for lpn_rec in 
    (
        select t.lpn_id, t.chute_assign_type, t.vol, t.units, t.wt, t.critcl_dim_1, t.critcl_dim_2, 
            t.critcl_dim_3, ',' || to_char(t.order_id) || ',' order_str
        from tmp_pack_wave_selected_lpns t
        where t.chute_row_id is null
        order by t.id
    )
    loop
        if (p_bal_alg = '1')
        then
            -- maximize chute capacity usage
            select min(t.chute_row_id) keep(dense_rank first
                order by cap.prty, t.sorter_seq_nbr, t.chute_id)
            into v_chute_row_id
            from tmp_pack_wave_selected_chutes t
            join chute_assign_prty cap on cap.chute_type = t.chute_type
            where cap.chute_assign_type = lpn_rec.chute_assign_type
                and cap.whse = p_whse and t.remaining_vol >= lpn_rec.vol
                and t.remaining_units >= lpn_rec.units
                and t.remaining_cartons > 0 and t.remaining_wt >= lpn_rec.wt
                and (t.remaining_orders > 0 or instr(t.order_list, lpn_rec.order_str, 1) > 0)
                and t.critcl_dim_1 >= lpn_rec.critcl_dim_1
                and t.critcl_dim_2 >= lpn_rec.critcl_dim_2
                and t.critcl_dim_3 >= lpn_rec.critcl_dim_3
                and t.pack_wave_seq_nbr = p_curr_pack_wave_seq_nbr;
        elsif (p_bal_alg = '2')
        then
            -- balance lpns across sorters within a pack wave
            select min(iv.chute_row_id) keep(dense_rank first
                order by iv.prty, iv.lpns_per_sorter desc)
            into v_chute_row_id
            from
            (
                select t.chute_row_id, cap.prty, sum(t.remaining_cartons)
                    over(partition by t.sorter_seq_nbr) lpns_per_sorter
                from tmp_pack_wave_selected_chutes t
                join chute_assign_prty cap on cap.chute_type = t.chute_type
                where cap.chute_assign_type = lpn_rec.chute_assign_type
                    and cap.whse = p_whse and t.remaining_vol >= lpn_rec.vol
                    and t.remaining_units >= lpn_rec.units
                    and t.remaining_cartons > 0 and t.remaining_wt >= lpn_rec.wt
                    and (t.remaining_orders > 0 or instr(t.order_list, lpn_rec.order_str, 1) > 0)
                    and t.critcl_dim_1 >= lpn_rec.critcl_dim_1
                    and t.critcl_dim_2 >= lpn_rec.critcl_dim_2
                    and t.critcl_dim_3 >= lpn_rec.critcl_dim_3
                    and t.pack_wave_seq_nbr = p_curr_pack_wave_seq_nbr
            ) iv;
        elsif (p_bal_alg = '4')
        then
            -- balance lpns across pack waves
            select min(iv.chute_row_id) keep(dense_rank first
                order by iv.prty, iv.lpns_per_pw desc, iv.pack_wave_seq_nbr)
            into v_chute_row_id
            from
            (
                select t.chute_row_id, t.pack_wave_seq_nbr, cap.prty, 
                    sum(t.remaining_cartons) over(partition by t.pack_wave_seq_nbr) lpns_per_pw
                from tmp_pack_wave_selected_chutes t
                join chute_assign_prty cap on cap.chute_type = t.chute_type
                where cap.chute_assign_type = lpn_rec.chute_assign_type
                    and cap.whse = p_whse and t.remaining_vol >= lpn_rec.vol
                    and t.remaining_units >= lpn_rec.units
                    and t.remaining_cartons > 0 and t.remaining_wt >= lpn_rec.wt
                    and (t.remaining_orders > 0 or instr(t.order_list, lpn_rec.order_str, 1) > 0)
                    and t.critcl_dim_1 >= lpn_rec.critcl_dim_1
                    and t.critcl_dim_2 >= lpn_rec.critcl_dim_2
                    and t.critcl_dim_3 >= lpn_rec.critcl_dim_3
            ) iv;
        else
            raise_application_error(-20050, 'Unsupported feature.');
        end if;

        if (v_chute_row_id is not null)
        then
            update tmp_pack_wave_selected_lpns t
            set t.chute_row_id = v_chute_row_id
            where t.lpn_id = lpn_rec.lpn_id;

            -- update chute capacities with that of the assigned lpn
            update tmp_pack_wave_selected_chutes t
            set t.remaining_vol = t.remaining_vol - lpn_rec.vol,
                t.remaining_units = t.remaining_units - lpn_rec.units,
                t.remaining_cartons = t.remaining_cartons - 1,
                t.remaining_wt = t.remaining_wt - lpn_rec.wt,
                t.remaining_orders = t.remaining_orders
                    - case when instr(t.order_list, lpn_rec.order_str, 1) > 0 then 0 else 1 end,
                t.order_list = case when instr(t.order_list, lpn_rec.order_str, 1) > 0
                    then t.order_list
                    when length(t.order_list || lpn_rec.order_str) <= 4000
                    then t.order_list || lpn_rec.order_str
                    else lpn_rec.order_str end
            where t.chute_row_id = v_chute_row_id
            returning t.chute_id, t.pack_wave_seq_nbr
            into v_chute_id, v_pack_wave_seq_nbr;

            if (v_log_lvl = 2)
            then
                wm_cs_log('Assigned lpn ' || lpn_rec.lpn_id || ' to chute ' || v_chute_id
                    || ' in PW ' || v_pack_wave_seq_nbr, p_sql_log_level => 2);
            end if;
        end if;
    end loop;
end;
/
show errors;

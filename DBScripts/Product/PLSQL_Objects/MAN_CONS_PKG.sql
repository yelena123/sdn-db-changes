create or replace
PACKAGE man_cons_pkg
IS
   -- ***********************
   -- Change Tracker
   -- Date:  By: Desc:
   -- ***********************
   -- 07/19/2004 MB added: LOAD_WORKSPACE2 (from 4R1)
   -- 16-Sep-05  Ganesh added: wave_id, move_type in SHIPMENT_LIST_SHP_REC_TYPE3 -- DBCR 5159, 5063
   -- 12-july-06 Abhishek Midha CR 29353 (Mohan K L)
   -- CT4: 25-jan-2011 Sriram Acharya: new MCW-UI Requirement to show additional sizes
   -- ***********************

   -- Returned from original shipment list method
   TYPE shipment_list_rec_type IS RECORD
   (
      row_num                NUMBER,
      shipment_id            shipment.shipment_id%TYPE,
      tc_shipment_id         shipment.tc_shipment_id%TYPE,
      shipment_status        shipment.shipment_status%TYPE,
      has_import_error       shipment.has_import_error%TYPE,
      has_soft_check_error   shipment.has_soft_check_error%TYPE,
      has_alerts             NUMBER,
      stop_seq               STOP.stop_seq%TYPE,
      stop_location_name     STOP.stop_location_name%TYPE,
      facility_id            STOP.facility_id%TYPE,
      arrival_start_dttm     STOP.arrival_start_dttm%TYPE,
      arrival_end_dttm       STOP.departure_end_dttm%TYPE,
      city                   STOP.city%TYPE,
      state_prov             STOP.state_prov%TYPE,
      stop_tz                STOP.stop_tz%TYPE,
      action_type            stop_action.action_type%TYPE,
      size1                  NUMBER,
      size2                  NUMBER,
      size3                  NUMBER,
      size4                  NUMBER,                                    -- CT4
      order_id               orders.order_id%TYPE,
      tc_order_id            orders.tc_order_id%TYPE
   );

   -- Shipment-only details for shipment list2
   TYPE shipment_list_shp_rec_type2 IS RECORD
   (
      row_num                       NUMBER,
      shipment_id                   shipment.shipment_id%TYPE,
      tc_shipment_id                shipment.tc_shipment_id%TYPE,
      shipment_status               shipment.shipment_status%TYPE,
      earned_income                 shipment.earned_income%TYPE,
      earned_income_currency_code   shipment.earned_income_currency_code%TYPE,
      has_import_error              shipment.has_import_error%TYPE,
      has_soft_check_error          shipment.has_soft_check_error%TYPE,
      has_alerts                    NUMBER,
      has_dependent_shipments       NUMBER
   );

   TYPE shipment_list_shp_rec_type3 IS RECORD
   (
      row_num                       NUMBER,
      shipment_id                   shipment.shipment_id%TYPE,
      tc_shipment_id                shipment.tc_shipment_id%TYPE,
      shipment_status               shipment.shipment_status%TYPE,
      earned_income                 shipment.earned_income%TYPE,
      earned_income_currency_code   shipment.earned_income_currency_code%TYPE,
      has_import_error              shipment.has_import_error%TYPE,
      has_soft_check_error          shipment.has_soft_check_error%TYPE,
      has_alerts                    NUMBER,
      has_dependent_shipments       NUMBER,
      estimated_savings             shipment.estimated_savings%TYPE,
      total_cost                    shipment.total_cost%TYPE,
      currency_code                 shipment.currency_code%TYPE,
      wave_id                       shipment.wave_id%TYPE,
      move_type                     shipment.move_type%TYPE,
      is_booking_required           shipment.is_booking_required%TYPE,
      booking_ref_shipper           shipment.booking_ref_shipper%TYPE,
      booking_id                    shipment.booking_id%TYPE
   );

   TYPE shipment_list_shp_rec_type4 IS RECORD
   (
      row_num                       NUMBER,
      shipment_id                   shipment.shipment_id%TYPE,
      tc_shipment_id                shipment.tc_shipment_id%TYPE,
      shipment_status               shipment.shipment_status%TYPE,
      earned_income                 shipment.earned_income%TYPE,
      earned_income_currency_code   shipment.earned_income_currency_code%TYPE,
      feasible_mot_id               shipment.feasible_mot_id%TYPE,
      feasible_equipment_id         shipment.feasible_equipment_id%TYPE,
      feasible_service_level_id     shipment.feasible_service_level_id%TYPE,
      feasible_carrier_id           shipment.feasible_carrier_id%TYPE,
      feasible_equipment2_id        shipment.feasible_equipment2_id%TYPE,
      feasible_driver_type          shipment.feasible_driver_type%TYPE,
      pickup_start_dttm             shipment.pickup_start_dttm%TYPE,
      pickup_end_dttm               shipment.pickup_end_dttm%TYPE,
      pickup_tz                     shipment.pickup_tz%TYPE,
      delivery_start_dttm           shipment.delivery_start_dttm%TYPE,
      delivery_end_dttm             shipment.delivery_end_dttm%TYPE,
      delivery_tz                   shipment.delivery_tz%TYPE,
      protection_level_id           shipment.protection_level_id%TYPE,
      o_facility_id                 shipment.o_facility_id%TYPE,
      o_address                     shipment.o_address%TYPE,
      o_city                        shipment.o_city%TYPE,
      o_state_prov                  shipment.o_state_prov%TYPE,
      o_postal_code                 shipment.o_postal_code%TYPE,
      o_country_code                shipment.o_country_code%TYPE,
      d_facility_id                 shipment.d_facility_id%TYPE,
      d_address                     shipment.d_address%TYPE,
      d_city                        shipment.d_city%TYPE,
      d_state_prov                  shipment.d_state_prov%TYPE,
      d_postal_code                 shipment.d_postal_code%TYPE,
      d_country_code                shipment.d_country_code%TYPE,
      has_import_error              shipment.has_import_error%TYPE,
      has_soft_check_error          shipment.has_soft_check_error%TYPE,
      has_alerts                    NUMBER,
      has_dependent_shipments       NUMBER,
      estimated_savings             shipment.estimated_savings%TYPE,
      total_cost                    shipment.total_cost%TYPE,
      currency_code                 shipment.currency_code%TYPE,
      wave_id                       shipment.wave_id%TYPE,
      move_type                     shipment.move_type%TYPE,
      is_booking_required           shipment.is_booking_required%TYPE,
      booking_ref_shipper           shipment.booking_ref_shipper%TYPE,
      booking_id                    shipment.booking_id%TYPE,
      size1                         NUMBER,
      size2                         NUMBER,
      size3                         NUMBER,
      size4                         NUMBER
   );

   -- Stop and stop sizes-only details for shipment list2
   TYPE shipment_list_stop_rec_type2 IS RECORD
   (
      row_num              NUMBER,
      shipment_id          shipment.shipment_id%TYPE,
      stop_seq             STOP.stop_seq%TYPE,
      stop_location_name   STOP.stop_location_name%TYPE,
      facility_id          STOP.facility_id%TYPE,
      arrival_start_dttm   STOP.arrival_start_dttm%TYPE,
      arrival_end_dttm     STOP.departure_end_dttm%TYPE,
      city                 STOP.city%TYPE,
      state_prov           STOP.state_prov%TYPE,
      stop_tz              STOP.stop_tz%TYPE,
      action_type          stop_action.action_type%TYPE,
      size1                NUMBER,
      size2                NUMBER,
      size3                NUMBER,
      size4                NUMBER                                       -- CT4
   );

   TYPE shipment_list_stop_rec_type3 IS RECORD
   (
      row_num                NUMBER,
      shipment_id            shipment.shipment_id%TYPE,
      stop_seq               STOP.stop_seq%TYPE,
      stop_location_name     STOP.stop_location_name%TYPE,
      facility_id            STOP.facility_id%TYPE,
      arrival_start_dttm     STOP.arrival_start_dttm%TYPE,
      arrival_end_dttm       STOP.arrival_end_dttm%TYPE,
      departure_start_dttm   STOP.departure_start_dttm%TYPE,
      departure_end_dttm     STOP.departure_end_dttm%TYPE,
      city                   STOP.city%TYPE,
      state_prov             STOP.state_prov%TYPE,
      stop_tz                STOP.stop_tz%TYPE,
      action_type            stop_action.action_type%TYPE,
      size1                  NUMBER,
      size2                  NUMBER,
      size3                  NUMBER,
      size4                  NUMBER
   );

   TYPE shipment_list_stop_rec_type4 IS RECORD
   (
      row_num                NUMBER,
      shipment_id            shipment.shipment_id%TYPE,
      stop_seq               STOP.stop_seq%TYPE,
      stop_location_name     STOP.stop_location_name%TYPE,
      facility_id            STOP.facility_id%TYPE,
      arrival_start_dttm     STOP.arrival_start_dttm%TYPE,
      arrival_end_dttm       STOP.arrival_end_dttm%TYPE,
      departure_start_dttm   STOP.departure_start_dttm%TYPE,
      departure_end_dttm     STOP.departure_end_dttm%TYPE,
      city                   STOP.city%TYPE,
      state_prov             STOP.state_prov%TYPE,
      stop_tz                STOP.stop_tz%TYPE,
      action_type            stop_action.action_type%TYPE,
      size1                  NUMBER,
      size2                  NUMBER,
      size3                  NUMBER,
      size4                  NUMBER,
      size5                  NUMBER,
      size6                  NUMBER,
      size7                  NUMBER,
      size8                  NUMBER
   );

   -- Order-only details at each stop for shipment list2
   TYPE shipment_list_order_rec_type2 IS RECORD
   (
      row_num       NUMBER,
      shipment_id   shipment.shipment_id%TYPE,
      stop_seq      STOP.stop_seq%TYPE,
      order_id      orders.order_id%TYPE,
      tc_order_id   orders.tc_order_id%TYPE
   );

   -- shipment custom attributes for  shipment list2 (vchillara)
   TYPE shipment_list_attrib_rec_type2 IS RECORD
   (
      row_num           NUMBER,
      shipment_id       shipment_attribute.shipment_id%TYPE,
      attribute_name    shipment_attribute.attribute_name%TYPE,
      attribute_value   shipment_attribute.attribute_value%TYPE
   );

   -- order custom attributes for  shipment list2 (vchillara)
   TYPE shipment_list_orat_rec_type2 IS RECORD
   (
      row_num           NUMBER,
      shipment_id       man_cons_shipment_list_gtt.shipment_id%TYPE,
      order_id          order_attribute.order_id%TYPE,
      attribute_name    order_attribute.attribute_name%TYPE,
      attribute_value   order_attribute.attribute_value%TYPE
   );

   TYPE cursor_type IS REF CURSOR;                     -- generic internal use

   -- returned to app (original list)
   TYPE shipment_cursor_type IS REF CURSOR
      RETURN shipment_list_rec_type;

   -- returned to app from shipment list2 sproc
   TYPE shipment_list_shp_cursor_type2 IS REF CURSOR
      RETURN shipment_list_shp_rec_type2;

   TYPE shipment_list_stp_cursor_type2 IS REF CURSOR
      RETURN shipment_list_stop_rec_type2;

   TYPE shipment_list_stp_cursor_type3 IS REF CURSOR
      RETURN shipment_list_stop_rec_type3;

   TYPE shipment_list_stp_cursor_type4 IS REF CURSOR
      RETURN shipment_list_stop_rec_type4;

   TYPE shipment_list_ord_cursor_type2 IS REF CURSOR
      RETURN shipment_list_order_rec_type2;

   TYPE shipment_list_shp_cursor_type3 IS REF CURSOR
      RETURN shipment_list_shp_rec_type3;

   TYPE shipment_list_shp_cursor_type4 IS REF CURSOR
      RETURN shipment_list_shp_rec_type4;

   --vchillara
   TYPE ship_list_attr_cursor_type2 IS REF CURSOR
      RETURN shipment_list_attrib_rec_type2;

   TYPE ship_list_orat_cursor_type2 IS REF CURSOR
      RETURN shipment_list_orat_rec_type2;

   TYPE int_array IS TABLE OF INTEGER;

   TYPE string_array IS TABLE OF VARCHAR (255);

   /******************************************************************************
      NAME:       MAN_CONS_PKG
      PURPOSE:    Manual consolidation functionality

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        7/10/2002   JDM           Created for man. shipment list
   ******************************************************************************/
   PROCEDURE get_shipment_list (
      vcoid                    IN     shipment.tc_company_id%TYPE,
      vcontactid               IN     facility_contact.facility_contact_id%TYPE,
      filter_where_clause      IN     VARCHAR2,
      filter_order_by_clause   IN     VARCHAR2,
      vminrow                  IN     NUMBER,
      vmaxrow                  IN     NUMBER,
      vsizeuom1                IN     size_uom.size_uom_id%TYPE,
      vsizeuom2                IN     size_uom.size_uom_id%TYPE,
      vsizeuom3                IN     size_uom.size_uom_id%TYPE,
      vsizeuom4                IN     size_uom.size_uom_id%TYPE,
      vtotal_count                OUT NUMBER,
      vshipment_list_refcur       OUT cursor_type);

   PROCEDURE get_shipment_list2 (
      --kishore vcoid IN shipment.tc_company_id%TYPE,
      vcoid                    IN     VARCHAR2,
      vcontactid               IN     facility_contact.facility_contact_id%TYPE,
      filter_where_clause      IN     VARCHAR2,
      filter_order_by_clause   IN     VARCHAR2,
      vminrow                  IN     NUMBER,
      vmaxrow                  IN     NUMBER,
      vsizeuom1                IN     size_uom.size_uom_id%TYPE,
      vsizeuom2                IN     size_uom.size_uom_id%TYPE,
      vsizeuom3                IN     size_uom.size_uom_id%TYPE,
      vsizeuom4                IN     size_uom.size_uom_id%TYPE,
      vtotal_count                OUT NUMBER,
      vshipment_refcur            OUT cursor_type,
      vstop_refcur                OUT cursor_type,
      vorder_refcur               OUT cursor_type);

   /*---------------------------------------------------------------------------------------
    #
    #   DATE               AUTHOR         DESCRIPTION
    #  ----------         ---------      ---------------------------------------------------
    #  11/19/03           Subhendhu     Procedure Get_Shipment_List3 has been added in line with
    #                                    usecase 432 changes for Tecate development( Harsh/Amit Team ).
    # ---------------------------------------------------------------------------------------- */
   PROCEDURE get_shipment_list3 (
      --kishore vcoid IN shipment.tc_company_id%TYPE,
      vcoid                    IN     VARCHAR2,
      vcontactid               IN     facility_contact.facility_contact_id%TYPE,
      filter_where_clause      IN     VARCHAR2,
      filter_order_by_clause   IN     VARCHAR2,
      vminrow                  IN     NUMBER,
      vmaxrow                  IN     NUMBER,
      vsizeuom1                IN     size_uom.size_uom_id%TYPE,
      vsizeuom2                IN     size_uom.size_uom_id%TYPE,
      vsizeuom3                IN     size_uom.size_uom_id%TYPE,
      vsizeuom4                IN     size_uom.size_uom_id%TYPE,
      vtotal_count                OUT NUMBER,
      vshipment_refcur            OUT cursor_type,
      vstop_refcur                OUT cursor_type,
      vorder_refcur               OUT cursor_type);

   -------------------------------------------------------------------------------------------
   -- 11/17/04   Vchillara Created to get shipment custom attributes
   --
   ----------------------------------------------------------------------------------------
   PROCEDURE get_shipment_list4 (
      vcoid                    IN     VARCHAR2,
      vcontactid               IN     facility_contact.facility_contact_id%TYPE,
      filter_where_clause      IN     VARCHAR2,
      filter_order_by_clause   IN     VARCHAR2,
      vminrow                  IN     NUMBER,
      vmaxrow                  IN     NUMBER,
      vsizeuom1                IN     size_uom.size_uom_id%TYPE,
      vsizeuom2                IN     size_uom.size_uom_id%TYPE,
      vsizeuom3                IN     size_uom.size_uom_id%TYPE,
      vsizeuom4                IN     size_uom.size_uom_id%TYPE,
      vtotal_count                OUT NUMBER,
      vshipment_refcur            OUT cursor_type,
      vstop_refcur                OUT cursor_type,
      vorder_refcur               OUT cursor_type,
      vcustattrib_refcur          OUT cursor_type,
      vorderattrib_refcur         OUT cursor_type);

   PROCEDURE add_orders_to_unassigned (ordersaddedcsv   OUT VARCHAR2,
                                       shipperid            INTEGER,
                                       cwsid                INTEGER,
                                       idlistcsv            VARCHAR2,
                                       issplit              INTEGER,
                                       size1uom             INTEGER,
                                       size2uom             INTEGER,
                                       --CR69340
                                       size3uom             INTEGER,
                                       -- CT4
                                       size4uom             INTEGER);

   PROCEDURE add_pln_orders_to_workspace (ordersaddedcsv   OUT VARCHAR2,
                                          shipperid            INTEGER,
                                          cwsid                INTEGER,
                                          idlistcsv            VARCHAR2,
                                          size1uom             INTEGER,
                                          size2uom             INTEGER,
                                          --CR69340
                                          size3uom             INTEGER,
                                          -- CT4
                                          size4uom             INTEGER,
                                          issplit              INTEGER);

   PROCEDURE add_shipments_to_workspace (shipmentsaddedcsv   OUT VARCHAR2,
                                         shipperid               INTEGER,
                                         cwsid                   INTEGER,
                                         idlistcsv               VARCHAR2,
                                         size1uom                INTEGER,
                                         size2uom                INTEGER,
                                         --CR69340
                                         size3uom                INTEGER,
                                         -- CT4
                                         size4uom                INTEGER);

   PROCEDURE cancel_workspace (shipperid       INTEGER,
                               cwsid           INTEGER,
                               cwsprocessid    INTEGER,
							   isFromCancel    INTEGER);

   /*   PROCEDURE create_Shipments_Ext_Orders(
           shipmentsAddedCsv OUT VARCHAR2,
        shipperId INTEGER,
        cwsId INTEGER,
        size1UOM INTEGER,
        size2UOM INTEGER,
           --CR69340
           size3UOM INTEGER,
        orderIdCsv VARCHAR2,
        placeHolderShipmentIdCsv VARCHAR2,
        movementTypeCsv VARCHAR2,
        pathSetIdCsv VARCHAR2,
        pathIdCsv VARCHAR2,
        orderShipmentSeqCsv VARCHAR2);

      PROCEDURE create_Shipments_Ext_Orders(
           shipmentsAddedCsv OUT VARCHAR2,
        shipperId INTEGER,
        cwsId INTEGER,
        size1UOM INTEGER,
        size2UOM INTEGER,
           --CR69340
           size3UOM INTEGER,
        orderIdCsv VARCHAR2,
        placeHolderShipmentIdCsv VARCHAR2,
        movementTypeCsv VARCHAR2,
        pathSetIdCsv VARCHAR2,
        pathIdCsv VARCHAR2,
        orderShipmentSeqCsv VARCHAR2,
        isRelayCsv VARCHAR2);*/
   PROCEDURE create_shipments_ext_orders (
      shipmentsaddedcsv          OUT VARCHAR2,
      shipperid                      INTEGER,
      cwsid                          INTEGER,
      size1uom                       INTEGER,
      size2uom                       INTEGER,
      --CR69340
      size3uom                       INTEGER,
      -- CT4
      size4uom                       INTEGER,
      orderidcsv                     VARCHAR2,
      ordersplitidcsv                VARCHAR2,
      placeholdershipmentidcsv       VARCHAR2,
      movementtypecsv                VARCHAR2,
      pathsetidcsv                   VARCHAR2,
      pathidcsv                      VARCHAR2,
      ordershipmentseqcsv            VARCHAR2,
      isrelaycsv                     VARCHAR2,
      compartmentnocsv               VARCHAR2,
      staticrouteid                  INTEGER,
      hassplit                       INTEGER);

   PROCEDURE assign_orders_to_drafts (ordersassignedboolcsv   OUT VARCHAR2,
                                      shipperid                   INTEGER,
                                      cwsid                       INTEGER,
                                      issplit                     INTEGER,
                                      orderidcsv                  VARCHAR2,
                                      ordersplitidcsv             VARCHAR2,
                                      cwsshipmentidcsv            VARCHAR2,
                                      omshipmentidcsv             VARCHAR2,
                                      pickupseqcsv                VARCHAR2,
                                      deliveryseqcsv              VARCHAR2,
                                      movementtypecsv             VARCHAR2,
                                      pathsetidcsv                VARCHAR2,
                                      pathidcsv                   VARCHAR2,
                                      ordershipmentseqcsv         VARCHAR2,
                                      isrelaycsv                  VARCHAR2,
                                      compartmentnocsv            VARCHAR2);

   PROCEDURE assign_orders_to_drafts (ordersassignedboolcsv   OUT VARCHAR2,
                                      shipperid                   INTEGER,
                                      cwsid                       INTEGER,
                                      orderidcsv                  VARCHAR2,
                                      cwsshipmentidcsv            VARCHAR2,
                                      omshipmentidcsv             VARCHAR2,
                                      pickupseqcsv                VARCHAR2,
                                      deliveryseqcsv              VARCHAR2,
                                      movementtypecsv             VARCHAR2,
                                      pathsetidcsv                VARCHAR2,
                                      pathidcsv                   VARCHAR2,
                                      ordershipmentseqcsv         VARCHAR2,
                                      isrelaycsv                  VARCHAR2);

   PROCEDURE assign_orders_to_drafts (ordersassignedboolcsv   OUT VARCHAR2,
                                      shipperid                   INTEGER,
                                      cwsid                       INTEGER,
                                      orderidcsv                  VARCHAR2,
                                      cwsshipmentidcsv            VARCHAR2,
                                      omshipmentidcsv             VARCHAR2,
                                      pickupseqcsv                VARCHAR2,
                                      deliveryseqcsv              VARCHAR2,
                                      movementtypecsv             VARCHAR2,
                                      pathsetidcsv                VARCHAR2,
                                      pathidcsv                   VARCHAR2,
                                      ordershipmentseqcsv         VARCHAR2,
                                      isrelaycsv                  VARCHAR2,
                                      compartmentnocsv            VARCHAR2);

   PROCEDURE assign_orders_to_drafts_sod (
      ordersassignedboolcsv   OUT VARCHAR2,
      shipperid                   INTEGER,
      cwsid                       INTEGER,
      orderidcsv                  VARCHAR2,
      cwsshipmentidcsv            VARCHAR2,
      omshipmentidcsv             VARCHAR2,
      pickupseqcsv                VARCHAR2,
      deliveryseqcsv              VARCHAR2,
      movementtypecsv             VARCHAR2,
      pathsetidcsv                VARCHAR2,
      pathidcsv                   VARCHAR2,
      ordershipmentseqcsv         VARCHAR2);

   PROCEDURE unassign_orders_from_drafts (
      ordersunassignedboolcsv   OUT VARCHAR2,
      shipperid                     INTEGER,
      cwsid                         INTEGER,
      orderidcsv                    VARCHAR2,
      splitordercsv                 VARCHAR2,
      cwsshipmentidcsv              VARCHAR2,
      cleanshipmentsbool            INTEGER,
      issplit                       INTEGER);

   PROCEDURE delete_empty_shipments (shipperid           INTEGER,
                                     cwsid               INTEGER,
                                     cwsshipmentidcsv    VARCHAR2);

   PROCEDURE get_out_of_synch_ids (ordersoutofsynch         OUT VARCHAR2,
                                   cwsshipmentsoutofsynch   OUT VARCHAR2,
                                   shipperid                    INTEGER,
                                   cwsid                        INTEGER);

   PROCEDURE get_out_of_synch_ids (ordersoutofsynch            OUT VARCHAR2,
                                   orderswithsplitoutofsynch   OUT VARCHAR2,
                                   cwsshipmentsoutofsynch      OUT VARCHAR2,
                                   shipperid                       INTEGER,
                                   cwsid                           INTEGER);

   PROCEDURE load_workspace (
      shipmentlistrefcur            OUT cursor_type,
      -- Cws shipment display info
      assignedorderlistrefcur       OUT cursor_type,
      -- Cws assigned order display info
      unassignedorderlistrefcur     OUT cursor_type,
      -- Cws unassigned order display info
      retcwsshipmentid              OUT INTEGER,
      -- current draft shipment id as determined by the sproc
      statcwsshipmentcount          OUT NUMBER,
      -- total # Cws shipments in workspace
      stattotalstopcount            OUT NUMBER,
      -- total # of stops across all shipments
      stattotalordermovementcount   OUT NUMBER,
      -- total # of order movements across all shipments
      statbaselinecost              OUT cws_shipment.baseline_cost%TYPE,
      -- total baseline cost for shipments
      statestimatedcost             OUT cws_shipment.estimated_cost%TYPE,
      -- total estimated cost for shipments
      statinitialcost               OUT cws_shipment.initial_cost%TYPE,
      -- total initial cost for shipments
      shipperid                         INTEGER,                 -- company id
      cwsid                             INTEGER,               -- workspace id
      currentcwsshipmentid              INTEGER,
      -- current draft shipment id
      shipmentlistorderby               VARCHAR2,
      -- order by clause for shipment list or empty string
      shipmentlistminrow                NUMBER,
      -- first row to retrieve (0-based)
      shipmentlistmaxrow                NUMBER,
      -- last row + 1 to retrieve (ex: 0..10 retrieves first 10 shipments)
      orderidmdfilter                   NUMBER,
      -- for MD link, if non-null, joins to cws_order_movement to restrict list of shipment to this order id
      assignedorderlistorderby          VARCHAR2,
      -- order by clause for assigned orders list or empty string
      unassignedorderlistorderby        VARCHAR2); -- order by clause for unassigned orders list or empty string

   PROCEDURE load_workspace_sod (
      shipmentlistrefcur            OUT cursor_type,
      -- Cws shipment display info
      assignedorderlistrefcur       OUT cursor_type,
      -- Cws assigned order display info
      unassignedorderlistrefcur     OUT cursor_type,
      -- Cws unassigned order display info
      movedorderlistrefcur          OUT cursor_type,   -- cws moded order list
      retcwsshipmentid              OUT INTEGER,
      -- current draft shipment id as determined by the sproc
      statcwsshipmentcount          OUT NUMBER,
      -- total # Cws shipments in workspace
      stattotalstopcount            OUT NUMBER,
      -- total # of stops across all shipments
      stattotalordermovementcount   OUT NUMBER,
      -- total # of order movements across all shipments
      statbaselinecost              OUT cws_shipment.baseline_cost%TYPE,
      -- total baseline cost for shipments
      statestimatedcost             OUT cws_shipment.estimated_cost%TYPE,
      -- total estimated cost for shipments
      statinitialcost               OUT cws_shipment.initial_cost%TYPE,
      -- total initial cost for shipments
      shipperid                         INTEGER,                 -- company id
      cwsid                             INTEGER,               -- workspace id
      currentcwsshipmentid              INTEGER,
      -- current draft shipment id
      shipmentlistorderby               VARCHAR2,
      -- order by clause for shipment list or empty string
      shipmentlistminrow                NUMBER,
      -- first row to retrieve (0-based)
      shipmentlistmaxrow                NUMBER,
      -- last row + 1 to retrieve (ex: 0..10 retrieves first 10 shipments)
      orderidmdfilter                   NUMBER,
      -- for MD link, if non-null, joins to cws_order_movement to restrict list of shipment to this order id
      assignedorderlistorderby          VARCHAR2,
      -- order by clause for assigned orders list or empty string
      unassignedorderlistorderby        VARCHAR2); -- order by clause for unassigned orders list or empty string

   PROCEDURE load_workspace2 (
      shipmentlistrefcur            OUT cursor_type,
      -- Cws shipment display info
      assignedorderlistrefcur       OUT cursor_type,
      -- Cws assigned order display info
      unassignedorderlistrefcur     OUT cursor_type,
      -- Cws unassigned order display info
      assignedstoplistrefcur        OUT cursor_type,
      -- Cws assigned stop display info
      retcwsshipmentid              OUT INTEGER,
      -- current draft shipment id as determined by the sproc
      statcwsshipmentcount          OUT NUMBER,
      -- total # Cws shipments in workspace
      stattotalstopcount            OUT NUMBER,
      -- total # of stops across all shipments
      stattotalordermovementcount   OUT NUMBER,
      -- total # of order movements across all shipments
      statbaselinecost              OUT cws_shipment.baseline_cost%TYPE,
      -- total baseline cost for shipments
      statestimatedcost             OUT cws_shipment.estimated_cost%TYPE,
      -- total estimated cost for shipments
      statinitialcost               OUT cws_shipment.initial_cost%TYPE,
      -- total initial cost for shipments
      modifiedminrow                OUT INTEGER,
      -- minimum row when modified
      modifiedmaxrow                OUT INTEGER,
      -- maximum row when modified
      shipperid                         INTEGER,                 -- company id
      cwsid                             INTEGER,               -- workspace id
      currentcwsshipmentid              INTEGER,
      -- current draft shipment id
      shipmentlistorderby               VARCHAR2,
      -- order by clause for shipment list or empty string
      shipmentlistminrow                NUMBER,
      -- first row to retrieve (0-based)
      shipmentlistmaxrow                NUMBER,
      -- last row + 1 to retrieve (ex: 0..10 retrieves first 10 shipments)
      orderidmdfilter                   NUMBER,
      -- for MD link, if non-null, joins to cws_order_movement to restrict list of shipment to this order id
      assignedorderlistorderby          VARCHAR2,
      -- order by clause for assigned orders list or empty string
      unassignedorderlistorderby        VARCHAR2,
      allowsplit                        NUMBER);

   PROCEDURE add_shipments_to_pre_workspace (
      shipmentsaddedcsv   OUT VARCHAR2,
      shipperid               INTEGER,
      cwsid                   INTEGER,
      idlistcsv               VARCHAR2,
      size1uom                INTEGER,
      size2uom                INTEGER,
      --CT4
      size3uom                INTEGER,
      size4uom                INTEGER);

   PROCEDURE assign_orders_to_pos_draft (
      ordersassignedboolcsv   OUT VARCHAR2,
      shipperid                   INTEGER,
      cwsid                       INTEGER,
      orderidcsv                  VARCHAR2,
      cwsshipmentidcsv            VARCHAR2,
      omshipmentidcsv             VARCHAR2,
      pickupseqcsv                VARCHAR2,
      deliveryseqcsv              VARCHAR2,
      movementtypecsv             VARCHAR2,
      pathsetidcsv                VARCHAR2,
      pathidcsv                   VARCHAR2,
      ordershipmentseqcsv         VARCHAR2);

   -- load info from possible shipment and possible movement
   PROCEDURE load_pre_workspace (
      shipmentlistrefcur            OUT cursor_type,
      -- Cws shipment display info
      assignedorderlistrefcur       OUT cursor_type,
      -- Cws assigned order display info
      unassignedorderlistrefcur     OUT cursor_type,
      -- Cws unassigned order display info
      movedorderlistrefcur          OUT cursor_type,   -- cws moded order list
      retcwsshipmentid              OUT INTEGER,
      -- current draft shipment id as determined by the sproc
      statcwsshipmentcount          OUT NUMBER,
      -- total # Cws shipments in workspace
      stattotalstopcount            OUT NUMBER,
      -- total # of stops across all shipments
      stattotalordermovementcount   OUT NUMBER,
      -- total # of order movements across all shipments
      statbaselinecost              OUT cws_pos_shipment.baseline_cost%TYPE,
      -- total baseline cost for shipments
      statestimatedcost             OUT cws_pos_shipment.estimated_cost%TYPE,
      -- total estimated cost for shipments
      statinitialcost               OUT cws_pos_shipment.initial_cost%TYPE,
      -- total initial cost for shipments
      shipperid                         INTEGER,                 -- company id
      cwsid                             INTEGER,               -- workspace id
      currentcwsshipmentid              INTEGER,
      -- current draft shipment id
      shipmentlistorderby               VARCHAR2,
      -- order by clause for shipment list or empty string
      shipmentlistminrow                NUMBER,
      -- first row to retrieve (0-based)
      shipmentlistmaxrow                NUMBER,
      -- last row + 1 to retrieve (ex: 0..10 retrieves first 10 shipments)
      orderidmdfilter                   NUMBER,
      -- for MD link, if non-null, joins to cws_order_movement to restrict list of shipment to this order id
      assignedorderlistorderby          VARCHAR2,
      -- order by clause for assigned orders list or empty string
      unassignedorderlistorderby        VARCHAR2); -- order by clause for unassigned orders list or empty string

   -- remove data from the possible shipment table and possible order movement table
   PROCEDURE clean_pre_workspace (shipperid INTEGER, cwsid INTEGER);

   PROCEDURE mark_shipments_for_refresh (shipperid    INTEGER,
                                         cwsid        INTEGER,
                                         idlistcsv    VARCHAR2);

   PROCEDURE set_shipment_promote_flag (shipmentsupdatedcsv   OUT VARCHAR2,
                                        shipperid                 INTEGER,
                                        cwsid                     INTEGER,
                                        idlistcsv                 VARCHAR2,
                                        dopromoteflag             NUMBER);

   PROCEDURE deconsolidate_draft_shipment (
      shipmentsdeconscsv             OUT VARCHAR2,
      shipperid                          INTEGER,
      cwsid                              INTEGER,
      idlistcsv                          VARCHAR2,
      keepordersinunassignedbucket       NUMBER);

   PROCEDURE load_draft_shipments (shipmentlistrefcur       OUT cursor_type, -- Cws shipment info
                                   movementlistrefcur       OUT cursor_type, -- Cws order movement info
                                   origmovementlistrefcur   OUT cursor_type, -- Cws order movement info
                                   stoplistrefcur           OUT cursor_type, -- Cws stop info
                                   ordersummarylistrefcur   OUT cursor_type, -- CwsOrderSummary info
                                   shipperid                    INTEGER, -- company id
                                   cwsid                        INTEGER, -- workspace id
                                   cwsshipmentlistcsv           VARCHAR2,
                                   -- specific list of draft shipments to load
                                   ldsoperationcode             NUMBER,
                                   -- a code # from the wrapper (see LDS_OP_*)
                                   ldsloaderflagword            NUMBER,
                                   -- a bitmask from the wrapper (see LDS_LOAD_*)
                                   cwsprocessid                 NUMBER);

   --  cws Porcess Id representing group of orders in progress for accepting.

   PROCEDURE synch_modified_details (shipperid               INTEGER,
                                     cwsid                   INTEGER,
                                     cwsshipmentidlistcsv    VARCHAR2,
                                     orderidlistcsv          VARCHAR2,
                                     ordersplitidcsv         VARCHAR2,
                                     size1uom                INTEGER,
                                     size2uom                INTEGER,
                                     size3uom                INTEGER,
                                     -- CT4
                                     size4uom                INTEGER);

   PROCEDURE remove_orders_from_workspace (ordersremovedcsv   OUT VARCHAR2,
                                           shipperid              INTEGER,
                                           cwsid                  INTEGER,
                                           idlistcsv              VARCHAR2,
                                           onlysplit              INTEGER,
                                           issplit                INTEGER);

   PROCEDURE remove_shipment_from_workspace (
      shipmentsremovedcsv   OUT VARCHAR2,
      shipperid                 INTEGER,
      cwsid                     INTEGER,
      idlistcsv                 VARCHAR2);

   FUNCTION p_add_order_to_unassigned (orderid      INTEGER,
                                       issplit      INTEGER,
                                       shipperid    INTEGER,
                                       cwsid        INTEGER,
                                       size1uom     INTEGER,
                                       size2uom     INTEGER,
                                       --CR69340
                                       size3uom     INTEGER,
                                       -- CT4
                                       size4uom     INTEGER)
      RETURN NUMBER;

   FUNCTION p_clean_shipments (cwsid            INTEGER,
                               shipperid        INTEGER,
                               wsshipmentarr    int_array)
      RETURN int_array;

   PROCEDURE p_mark_for_refresh (cwsid INTEGER, wsshipmentarr int_array);

   PROCEDURE p_reenable_comps (cwsid INTEGER, wsshipmentarr int_array);

   FUNCTION csv_to_int_array (csv_string VARCHAR2)
      RETURN int_array;

   FUNCTION int_array_to_csv (intarr int_array)
      RETURN VARCHAR;

   FUNCTION csv_to_string_array (csv_string VARCHAR2)
      RETURN string_array;

   --PROCEDURE prSetOrderStatus(aTcCompanyid    shipment.tc_company_id%type
   --       ,aShipmentId    shipment.SHIPMENT_id%type);
   FUNCTION getidarrindex (arr1    int_array,
                           arr2    int_array,
                           val1    INTEGER,
                           val2    INTEGER)
      RETURN INTEGER;

   PROCEDURE load_workspacestatistics (
      statcwsshipmentcount          OUT NUMBER,
      stattotalstopcount            OUT NUMBER,
      stattotalordermovementcount   OUT NUMBER,
      statbaselinecost              OUT cws_shipment.baseline_cost%TYPE,
      statestimatedcost             OUT cws_shipment.estimated_cost%TYPE,
      statinitialcost               OUT cws_shipment.initial_cost%TYPE,
      cwsid                             INTEGER);

   PROCEDURE load_workspaceassignedorders (
      assignedorderlistrefcur    OUT cursor_type,
      retcwsshipmentid               INTEGER,
      shipperid                      INTEGER,
      cwsid                          INTEGER,
      assignedorderlistorderby       VARCHAR2,
      allowsplit                     NUMBER,
      minrow                         NUMBER,
      maxrow                         NUMBER);

   PROCEDURE load_workspaceunassignedorders (
      unassignedorderlistrefcur    OUT cursor_type,
      totalrows                    OUT NUMBER,         -- total number of rows
      shipperid                        INTEGER,
      cwsid                            INTEGER,
      unassignedorderlistorderby       VARCHAR2,
      allowsplit                       NUMBER,
      minrow                           NUMBER,
      maxrow                           NUMBER,
      unassignedorderidlistcsv         VARCHAR2);

   PROCEDURE load_workspaceshipments (shipmentlistrefcur     OUT cursor_type,
                                      retcwsshipmentid       OUT INTEGER,
                                      modifiedminrow         OUT INTEGER,
                                      modifiedmaxrow         OUT INTEGER,
                                      statcwsshipmentcount   OUT NUMBER,
                                      shipperid                  INTEGER,
                                      cwsid                      INTEGER,
                                      currentcwsshipmentid       INTEGER,
                                      shipmentlistorderby        VARCHAR2,
                                      shipmentlistminrow         NUMBER,
                                      shipmentlistmaxrow         NUMBER,
                                      orderidmdfilter            NUMBER,
                                      ALLOWSPLIT                 NUMBER);
END MAN_CONS_PKG;
/

create or replace
PACKAGE BODY MAN_CONS_PKG AS
--    ***********************
--    Change Tracker
--    Date:        By:    Desc:
--    ***********************
--    07/19/2004    MB    added:    LOAD_WORKSPACE2 (from 4R1)
--    10/06/2004    DS    modified:    LOAD_WORKSPACE2
--    8-Dec-2004    DS    modified:    load_Draft_Shipments - added 2 new columns in OUT RefCursor (Raji S; 40476) and add_Shipments_To_Workspace
--    16-Dec-2004 DS    modified:    load_Draft_Shipments - added 2 new columns in OUT RefCursor shipment_list_cursor (Sridhar babu; 41029)
--    15-Sep-2005    Ganesh    added:  wave_id,move_type column (DBCR 5159, 5063)
--    21-Nov-2005    Ganesh    CR 10580 Modifed load_Draft_Shipments: FEASIBLE_EQUIPMENT_CODE2 to FEASIBLE_EQUIPMENT2_ID, DSG_EQUIPMENT_CODE2 to DSG_EQUIPMENT2_ID and PROTECTION_LEVEL to PROTECTION_LEVEL_ID
--    20-Dec-2005    Rajeev    CR 15245 (Sreejith Arat)
--    30-Jun-2006    Ganesh    CR 25655 (Sudipto Maulik)
--    06-july-2006    Abhishek CR 28921 (Mohan K.L)
--    07/25/2006 Abhishek Midha CR 30472
--    16/08/2006 Abhishek Midha CR 31676 (Mohan K.L.)
--      11/09/2006 Ganesh          CR 32890 (Balakrishnan N)
--    20/09/2006 Ganesh          CR 33601 (Shailesh K) ASP code merge
--    25/09/2006 Abhishek Midha      CR 33814 (Girish N)
--    24/10/2006 Roman          CR 35506 (Production change merged)
--    09-Nov-2006 Ganesh          CR 36416 Added distinct in get_shipment_list4 procedure
--    28-July-2008 Yogamani T          Kroger Enhancement  on MCW ShipmentListPage
--    CT18: 25-jan-2011 Sriram Acharya: new MCW-UI Requirement to show additional sizes
--    ***********************
PROCEDURE Get_Shipment_List(
    vcoid IN SHIPMENT.tc_company_id%TYPE,                                  -- company id
    vcontactid IN FACILITY_CONTACT.facility_contact_id%TYPE,         -- contact id
    filter_where_clause IN VARCHAR2,                                 -- where clause from filter or empty string
    filter_order_by_clause IN VARCHAR2,                                 -- order by clause from filter or empty string
    vminrow IN NUMBER,                                                      -- first row to retrieve (0-based)
    vmaxrow IN NUMBER,                                                 -- last row + 1 to retrieve (ex: 0..10 retrieves first 10 shipments)
    vsizeuom1 IN SIZE_UOM.size_uom_id%TYPE,                             -- configured display UOM or empty string
    vsizeuom2 IN SIZE_UOM.size_uom_id%TYPE,                             -- configured display UOM or empty string
    vsizeuom3 IN SIZE_UOM.size_uom_id%TYPE,                             -- configured display UOM or empty string
    vsizeuom4 IN SIZE_UOM.size_uom_id%TYPE,                             -- configured display UOM or empty string -- CT18
    vtotal_count OUT NUMBER,                                         -- total # records matching where clause
    vshipment_list_refcur OUT CURSOR_TYPE                             -- shipment list resultset
)
IS
   where_clause                 VARCHAR2(5000);
   sql_statement             VARCHAR2(5000);
   vshipment_id SHIPMENT.shipment_id%TYPE;
   vcurrow NUMBER;
   vidlist_done NUMBER;
   vgttcount NUMBER;
   total_count_cursor CURSOR_TYPE;
   shipment_id_cursor CURSOR_TYPE;
   shipment_list_cursor SHIPMENT_CURSOR_TYPE;
BEGIN
   -- total count for display on bottom of page (rows m to n of <total>).  Filter is applied, but
   -- no row count restrictions or order by clause.
   -- These restrictions were previously hardcoded in the SS bean method:
   -- creation_type is consolidated, is_cancelled is false, shipment_status not complete, not pool point shipment.
   where_clause :=
   ' where ((shipment.tc_company_id = :vcoid)
       AND  ((SHIPMENT.outbound_region_id IN
                 (SELECT CFR.region_id FROM CONTACT_FACILITY_REGION CFR WHERE CFR.tc_company_id = SHIPMENT.tc_company_id
               AND CFR.contact_id = :vcontactid AND CFR.DIRECTION = ''O''))
         OR  (SHIPMENT.inbound_region_id IN
              (SELECT CFR.region_id FROM CONTACT_FACILITY_REGION CFR WHERE CFR.tc_company_id = SHIPMENT.tc_company_id
               AND CFR.contact_id = :vcontactid AND CFR.DIRECTION = ''I''))))
       AND CREATION_TYPE = 20 AND IS_CANCELLED = 0 AND SHIPMENT_STATUS <> 90 AND PP_SHIPMENT_ID IS NULL '
       || filter_where_clause;
   sql_statement := 'select count(1) from shipment' || where_clause;
   /*
   DBMS_OUTPUT.put_line( 'Count query:');
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,   0, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3801, 200 ));
   DBMS_OUTPUT.put_line( 'END Count query' );
   */
   OPEN total_count_cursor FOR sql_statement USING vcoid, vcontactid, vcontactid;
   FETCH total_count_cursor INTO vtotal_count;
   CLOSE total_count_cursor;
   /* DBMS_OUTPUT.put_line( 'Total count: ' || to_char(vtotal_count) ); */
   -- shipment id list for page (restriction by row numbers and ordered by filter)
   DELETE FROM MAN_CONS_SHIPMENT_LIST_GTT;
   sql_statement := 'select shipment.shipment_id from shipment'
                    || where_clause
                 || ' '
                 || filter_order_by_clause;
   /*
   DBMS_OUTPUT.put_line( 'Shipment ID List query:');
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,   0, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3801, 200 ));
   DBMS_OUTPUT.put_line( 'END Shipment ID List query' );
   */
   OPEN shipment_id_cursor FOR sql_statement USING vcoid, vcontactid, vcontactid;
   -- Skip rows before minrow, starting at 0
   vcurrow := 0;
   vidlist_done := 0;
   <<c1_cursor_loop>>
   LOOP
          /*DBMS_OUTPUT.put_line('c1 loop, vcurrow = ' || to_char(vcurrow));*/
          EXIT WHEN vcurrow >= vminrow;
          FETCH shipment_id_cursor INTO vshipment_id;
       IF shipment_id_cursor%NOTFOUND THEN
             vidlist_done := 1;
          EXIT;
       END IF;
          /*DBMS_OUTPUT.put_line('c1 loop, fetched id = ' || to_char(vshipment_id));*/
       vcurrow := vcurrow + 1;
   END LOOP c1_cursor_loop;
   -- If any rows left, add these id's, up to maxrow-1
   IF vidlist_done = 0 THEN
       <<c2_cursor_loop>>
       LOOP
              /*DBMS_OUTPUT.put_line('c2 loop, vcurrow = ' || to_char(vcurrow));*/
              EXIT WHEN vcurrow >= vmaxrow;
              FETCH shipment_id_cursor INTO vshipment_id;
           EXIT WHEN shipment_id_cursor%NOTFOUND;
           /*DBMS_OUTPUT.put_line('c2 loop, fetched id = ' || to_char(vshipment_id));*/
           INSERT INTO MAN_CONS_SHIPMENT_LIST_GTT (ROW_NUM, SHIPMENT_ID) VALUES (vcurrow, vshipment_id);
           vcurrow := vcurrow + 1;
       END LOOP c2_cursor_loop;
       --COMMIT;
   END IF;
   CLOSE shipment_id_cursor;
   /* Debugging
   select count(1) into vgttcount from MAN_CONS_SHIPMENT_LIST_GTT;
   DBMS_OUTPUT.put_line('MAN_CONS_SHIPMENT_LIST_GTT count = ' || to_char(vgttcount));
   */
   -- ** ALL THE Man. shipment list data you could ever want **
   OPEN shipment_list_cursor FOR
SELECT SH.row_num, SH.shipment_id, SH.tc_shipment_id, SH.SHIPMENT_STATUS, SH.has_import_error, SH.has_soft_check_error,
       CASE WHEN ALERTS.alert_id IS NOT NULL THEN 1 END HAS_ALERTS,
       ST.stop_seq, ST.stop_location_name, ST.facility_id, ST.arrival_start_dttm, ST.arrival_end_dttm, ST.city, ST.STATE_PROV, ST.stop_tz, ST.ACTION_TYPE,
       SAO.SIZE1, SAO.SIZE2, SAO.SIZE3, SAO.SIZE4, -- CT18
       STORD.order_id,
       ORD.tc_order_id
       FROM
-- Shipment level
(SELECT /*+ ordered first_rows */ MAN_CONS_SHIPMENT_LIST_GTT.row_num,
        SHIPMENT.shipment_id, SHIPMENT.tc_shipment_id, SHIPMENT.SHIPMENT_STATUS, SHIPMENT.has_import_error, SHIPMENT.has_soft_check_error
    FROM MAN_CONS_SHIPMENT_LIST_GTT, SHIPMENT
    WHERE SHIPMENT.shipment_id=MAN_CONS_SHIPMENT_LIST_GTT.shipment_id) SH,
-- Shipment level alerts
AP_ALERT ALERTS,
-- Stop level
(SELECT /*+ ordered first_rows */ STOP.shipment_id, STOP.stop_seq, STOP.stop_location_name, STOP.facility_id, STOP.arrival_start_dttm, STOP.arrival_end_dttm, STOP.city, STOP.STATE_PROV, STOP.stop_tz,
       STOP_ACTION.ACTION_TYPE
    FROM MAN_CONS_SHIPMENT_LIST_GTT, STOP, STOP_ACTION
    WHERE (STOP.SHIPMENT_ID=MAN_CONS_SHIPMENT_LIST_GTT.shipment_id)
              AND (STOP_ACTION.shipment_id=MAN_CONS_SHIPMENT_LIST_GTT.shipment_id)
              AND (STOP_ACTION.stop_seq=STOP.stop_seq)) ST,
-- Stop size rollups
(SELECT /*+ ordered first_rows */ STOP_ACTION_ORDER.SHIPMENT_ID SAO_SHIPMENT_ID, STOP_ACTION_ORDER.STOP_SEQ SAO_STOP_SEQ,
        SUM(ORDER_LINE_ITEM_SIZE.size_value*DECODE(SIZE_UOM_ID,vsizeuom1,1,0)) SIZE1
       ,SUM(ORDER_LINE_ITEM_SIZE.size_value*DECODE(SIZE_UOM_ID,vsizeuom2,1,0)) SIZE2
       ,SUM(ORDER_LINE_ITEM_SIZE.size_value*DECODE(SIZE_UOM_ID,vsizeuom3,1,0)) SIZE3
       ,SUM(ORDER_LINE_ITEM_SIZE.size_value*DECODE(SIZE_UOM_ID,vsizeuom4,1,0)) SIZE4 -- CT18
    FROM MAN_CONS_SHIPMENT_LIST_GTT, STOP_ACTION_ORDER, ORDER_LINE_ITEM_SIZE
    WHERE           (STOP_ACTION_ORDER.SHIPMENT_ID=MAN_CONS_SHIPMENT_LIST_GTT.shipment_id)
              AND (ORDER_LINE_ITEM_SIZE.order_id=STOP_ACTION_ORDER.order_id)
              AND (ORDER_LINE_ITEM_SIZE.size_uom_id IN (vsizeuom1, vsizeuom2, vsizeuom3, vsizeuom4))
    GROUP BY STOP_ACTION_ORDER.SHIPMENT_ID,STOP_ACTION_ORDER.STOP_SEQ) SAO,
-- Any 1 order at the stop
(SELECT /*+ ordered first_rows */ STOP_ACTION_ORDER.shipment_id,STOP_ACTION_ORDER.stop_seq,MIN(STOP_ACTION_ORDER.order_id) ORDER_ID
           FROM MAN_CONS_SHIPMENT_LIST_GTT, STOP_ACTION_ORDER
        WHERE (STOP_ACTION_ORDER.shipment_id=MAN_CONS_SHIPMENT_LIST_GTT.shipment_id)
        GROUP BY STOP_ACTION_ORDER.shipment_id,STOP_ACTION_ORDER.stop_seq) STORD,
-- Add tc order id
(SELECT /*+ ordered first_rows */ ORDERS.order_id,ORDERS.tc_order_id FROM MAN_CONS_SHIPMENT_LIST_GTT,ORDERS
        WHERE (MAN_CONS_SHIPMENT_LIST_GTT.shipment_id=ORDERS.shipment_id)) ORD
-- Now join and order all of the above
 WHERE     (SH.shipment_id=ST.shipment_id (+))
        AND (SH.shipment_id=ALERTS.object_id (+) AND ALERTS.OBJECT_TYPE(+)='SHIPMENT' AND ALERTS.alert_status(+) = 1)
       AND (ST.shipment_id=SAO.sao_shipment_id (+))
       AND (ST.stop_seq=SAO.sao_stop_seq (+))
       AND (ST.shipment_id=STORD.shipment_id (+))
       AND (ST.stop_seq=STORD.stop_seq (+))
       AND (STORD.order_id=ORD.order_id (+))
ORDER BY SH.row_num, ST.stop_seq;
   -- set as return
   vshipment_list_refcur := shipment_list_cursor;
END Get_Shipment_List;
-- New version does not join from results with different cardinalities, but returns them
-- as separate cursors.  This makes the queries faster and returns less data.
PROCEDURE Get_Shipment_List2(
    --vcoid IN shipment.tc_company_id%TYPE,                                  -- company id
    vcoid IN VARCHAR2,                                                       -- comma separated company id list
    vcontactid IN FACILITY_CONTACT.facility_contact_id%TYPE,         -- contact id
    filter_where_clause IN VARCHAR2,                                 -- where clause from filter or empty string
    filter_order_by_clause IN VARCHAR2,                                 -- order by clause from filter or empty string
    vminrow IN NUMBER,                                                      -- first row to retrieve (0-based)
    vmaxrow IN NUMBER,                                                 -- last row + 1 to retrieve (ex: 0..10 retrieves first 10 shipments)
    vsizeuom1 IN SIZE_UOM.size_uom_id%TYPE,                             -- configured display UOM or empty string
    vsizeuom2 IN SIZE_UOM.size_uom_id%TYPE,                             -- configured display UOM or empty string
    vsizeuom3 IN SIZE_UOM.size_uom_id%TYPE,                             -- configured display UOM or empty string
    vsizeuom4 IN SIZE_UOM.size_uom_id%TYPE,                             -- configured display UOM or empty string -- CT18
    vtotal_count OUT NUMBER,                                         -- total # records matching where clause
    vshipment_refcur OUT CURSOR_TYPE,                                 -- shipment data resultset
    vstop_refcur OUT CURSOR_TYPE,                                     -- stop data resultset
    vorder_refcur OUT CURSOR_TYPE                                     -- order data resultset
)

IS
   where_clause                 VARCHAR2(5000);
   sql_statement             VARCHAR2(5000);
   vshipment_id SHIPMENT.shipment_id%TYPE;
   vcurrow NUMBER;
   vidlist_done NUMBER;
   vgttcount NUMBER;
   total_count_cursor CURSOR_TYPE;
   shipment_id_cursor CURSOR_TYPE;
   shipment_list_cursor SHIPMENT_LIST_SHP_CURSOR_TYPE2;
   stop_list_cursor SHIPMENT_LIST_STP_CURSOR_TYPE2;
   order_list_cursor SHIPMENT_LIST_ORD_CURSOR_TYPE2;
BEGIN
   -- total count for display on bottom of page (rows m to n of <total>).  Filter is applied, but
   -- no row count restrictions or order by clause.
   -- These restrictions were previously hardcoded in the SS bean method:
   -- creation_type is consolidated, is_cancelled is false, shipment_status not complete, not pool point shipment.
   where_clause :=
   --kishore ' where ((shipment.tc_company_id = :vcoid)
   ' where ((shipment.tc_company_id in (' || vcoid || '))
             AND CREATION_TYPE = 20 AND IS_CANCELLED = 0 AND SHIPMENT_STATUS <> 90 AND PP_SHIPMENT_ID IS NULL)
             AND NOT EXISTS    (SELECT    1
                FROM    CWS_SHIPMENT a
                WHERE    a.ORIG_SHIPMENT_ID = SHIPMENT.SHIPMENT_ID
           )'
       || filter_where_clause;
   sql_statement := 'select count(1) from shipment' || where_clause;
   /*
   DBMS_OUTPUT.put_line( 'Count query:');
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,   0, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3801, 200 ));
   DBMS_OUTPUT.put_line( 'END Count query' );
   */
   -- OPEN total_count_cursor FOR sql_statement USING vcoid;
   OPEN total_count_cursor FOR sql_statement;
   FETCH total_count_cursor INTO vtotal_count;
   CLOSE total_count_cursor;
   /* DBMS_OUTPUT.put_line( 'Total count: ' || to_char(vtotal_count) ); */
   -- shipment id list for page (restriction by row numbers and ordered by filter)
   DELETE FROM MAN_CONS_SHIPMENT_LIST_GTT;
   sql_statement := 'select shipment.shipment_id from shipment'
                    || where_clause
                 || ' '
                 || filter_order_by_clause;
   /*
   DBMS_OUTPUT.put_line( 'Shipment ID List query:');
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,   0, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3801, 200 ));
   DBMS_OUTPUT.put_line( 'END Shipment ID List query' );
   */
   --kishore OPEN shipment_id_cursor FOR sql_statement USING vcoid;
    OPEN shipment_id_cursor FOR sql_statement;
   -- Skip rows before minrow, starting at 0
   vcurrow := 0;
   vidlist_done := 0;
   <<c1_cursor_loop>>
   LOOP
          /*DBMS_OUTPUT.put_line('c1 loop, vcurrow = ' || to_char(vcurrow));*/
          EXIT WHEN vcurrow >= vminrow;
          FETCH shipment_id_cursor INTO vshipment_id;
       IF shipment_id_cursor%NOTFOUND THEN
             vidlist_done := 1;
          EXIT;
       END IF;
          /*DBMS_OUTPUT.put_line('c1 loop, fetched id = ' || to_char(vshipment_id));*/
       vcurrow := vcurrow + 1;
   END LOOP c1_cursor_loop;
   -- If any rows left, add these id's, up to maxrow-1
   IF vidlist_done = 0 THEN
       <<c2_cursor_loop>>
       LOOP
              /*DBMS_OUTPUT.put_line('c2 loop, vcurrow = ' || to_char(vcurrow));*/
              EXIT WHEN vcurrow >= vmaxrow;
              FETCH shipment_id_cursor INTO vshipment_id;
           EXIT WHEN shipment_id_cursor%NOTFOUND;
           /*DBMS_OUTPUT.put_line('c2 loop, fetched id = ' || to_char(vshipment_id));*/
           INSERT INTO MAN_CONS_SHIPMENT_LIST_GTT (ROW_NUM, SHIPMENT_ID) VALUES (vcurrow, vshipment_id);
           vcurrow := vcurrow + 1;
       END LOOP c2_cursor_loop;
       --COMMIT;
   END IF;
   CLOSE shipment_id_cursor;
   /* Debugging
   select count(1) into vgttcount from MAN_CONS_SHIPMENT_LIST_GTT;
   DBMS_OUTPUT.put_line('MAN_CONS_SHIPMENT_LIST_GTT count = ' || to_char(vgttcount));
   */
   -- Shipment data only
   OPEN shipment_list_cursor FOR
SELECT SH.row_num, SH.shipment_id, SH.tc_shipment_id, SH.SHIPMENT_STATUS, SH.earned_income, SH.earned_income_currency_code,
       SH.has_import_error, SH.has_soft_check_error,
       CASE WHEN ALERTS.alert_id IS NOT NULL THEN 1 END HAS_ALERTS,
       SH.has_dependent_shipments
       FROM
-- Shipment level
(SELECT /*+ ordered first_rows */ MAN_CONS_SHIPMENT_LIST_GTT.row_num,
        SHIPMENT.shipment_id, SHIPMENT.tc_shipment_id, SHIPMENT.SHIPMENT_STATUS, SHIPMENT.earned_income, SHIPMENT.earned_income_currency_code, SHIPMENT.has_import_error, SHIPMENT.has_soft_check_error,
(
SELECT (NVL((SELECT 1 FROM dual WHERE EXISTS(
SELECT om2.shipment_id FROM ORDER_MOVEMENT om1, ORDER_MOVEMENT om2 WHERE
om1.shipment_id = SHIPMENT.shipment_id AND
om1.order_id = om2.order_id AND
om2.shipment_id != om1.shipment_id)),0) ) FROM SHIPMENT
WHERE shipment_id = MAN_CONS_SHIPMENT_LIST_GTT.shipment_id
)has_dependent_shipments
    FROM MAN_CONS_SHIPMENT_LIST_GTT, SHIPMENT
    WHERE SHIPMENT.shipment_id=MAN_CONS_SHIPMENT_LIST_GTT.shipment_id) SH,
-- Shipment level alerts
AP_ALERT ALERTS
-- Now join and order all of the above
 WHERE (SH.shipment_id=ALERTS.object_id (+) AND ALERTS.OBJECT_TYPE(+)='SHIPMENT' AND ALERTS.alert_status(+) = 1)
ORDER BY SH.row_num;

-- Stop and size data only
   OPEN stop_list_cursor FOR
    SELECT /*+ ordered first_rows */
          MAN_CONS_SHIPMENT_LIST_GTT.row_num,
          STOP.shipment_id
          , STOP.stop_seq
          , STOP.stop_location_name
          , STOP.facility_id
          ,STOP.arrival_start_dttm, STOP.arrival_end_dttm, STOP.city, STOP.STATE_PROV
          , STOP.stop_tz
          ,STOP_ACTION.ACTION_TYPE
          ,(SELECT SUM(size_value)
              FROM ORDER_LINE_ITEM_SIZE
                   ,STOP_ACTION_ORDER
             WHERE ORDER_LINE_ITEM_SIZE.order_id = STOP_ACTION_ORDER.order_id
               AND STOP_ACTION_ORDER.shipment_id = STOP_ACTION.shipment_id
               AND STOP_ACTION_ORDER.stop_seq = STOP_ACTION.stop_seq
               AND STOP_ACTION_ORDER.stop_action_seq = STOP_ACTION.stop_action_seq
               AND size_uom_id = vsizeuom1
            )
          ,(SELECT SUM(size_value)
              FROM ORDER_LINE_ITEM_SIZE
                   ,STOP_ACTION_ORDER
             WHERE ORDER_LINE_ITEM_SIZE.order_id = STOP_ACTION_ORDER.order_id
               AND STOP_ACTION_ORDER.shipment_id = STOP_ACTION.shipment_id
               AND STOP_ACTION_ORDER.stop_seq = STOP_ACTION.stop_seq
               AND STOP_ACTION_ORDER.stop_action_seq = STOP_ACTION.stop_action_seq
               AND size_uom_id = vsizeuom2
            )
          ,(SELECT SUM(size_value)
              FROM ORDER_LINE_ITEM_SIZE
                   ,STOP_ACTION_ORDER
             WHERE ORDER_LINE_ITEM_SIZE.order_id = STOP_ACTION_ORDER.order_id
               AND STOP_ACTION_ORDER.shipment_id = STOP_ACTION.shipment_id
               AND STOP_ACTION_ORDER.stop_seq = STOP_ACTION.stop_seq
               AND STOP_ACTION_ORDER.stop_action_seq = STOP_ACTION.stop_action_seq
               AND size_uom_id = vsizeuom3
            )
          ,(SELECT SUM(size_value)
              FROM ORDER_LINE_ITEM_SIZE
                   ,STOP_ACTION_ORDER
             WHERE ORDER_LINE_ITEM_SIZE.order_id = STOP_ACTION_ORDER.order_id
               AND STOP_ACTION_ORDER.shipment_id = STOP_ACTION.shipment_id
               AND STOP_ACTION_ORDER.stop_seq = STOP_ACTION.stop_seq
               AND STOP_ACTION_ORDER.stop_action_seq = STOP_ACTION.stop_action_seq
               AND size_uom_id = vsizeuom4
            )
          FROM
          MAN_CONS_SHIPMENT_LIST_GTT
          ,STOP
          ,STOP_ACTION
      WHERE
          STOP.SHIPMENT_ID = MAN_CONS_SHIPMENT_LIST_GTT.shipment_id
      AND STOP_ACTION.shipment_id=STOP.shipment_id
      AND STOP_ACTION.stop_seq=STOP.stop_seq
      ORDER BY MAN_CONS_SHIPMENT_LIST_GTT.row_num,STOP.stop_seq
      ;
   -- Order data only at the stop
   OPEN order_list_cursor FOR
SELECT /*+ ordered first_rows */ MAN_CONS_SHIPMENT_LIST_GTT.row_num,
              STOP_ACTION_ORDER.shipment_id, STOP_ACTION_ORDER.stop_seq,
           STOP_ACTION_ORDER.order_id ORDER_ID, ORDERS.tc_order_id
           FROM MAN_CONS_SHIPMENT_LIST_GTT, STOP_ACTION_ORDER, ORDERS
        WHERE     (STOP_ACTION_ORDER.shipment_id=MAN_CONS_SHIPMENT_LIST_GTT.shipment_id)
              AND (ORDERS.order_id=STOP_ACTION_ORDER.order_id)
ORDER BY MAN_CONS_SHIPMENT_LIST_GTT.row_num, STOP_ACTION_ORDER.stop_seq, ORDERS.tc_order_id;
   -- set as returns
   vshipment_refcur := shipment_list_cursor;
   vstop_refcur := stop_list_cursor;
   vorder_refcur := order_list_cursor;
END Get_Shipment_List2;
--
-- Add orders to unassigned bucket from outside the workspace
PROCEDURE add_Orders_To_Unassigned(
  ordersAddedCsv OUT VARCHAR2,
  shipperId INTEGER,
  cwsId INTEGER,
  idListCsv VARCHAR2,
  isSplit INTEGER,
  size1UOM INTEGER,
  size2UOM INTEGER,
  --CR69340
  size3UOM INTEGER,
  size4UOM INTEGER -- CT18
  ) IS
  idArr INT_ARRAY;
  idAddedArr INT_ARRAY;
  bAdded NUMBER(1);
  bFound NUMBER(1);
  bStatusUnplanned NUMBER(1);
  orderId INTEGER;
  TYPE ORDER_ID_CURSOR IS REF CURSOR;
  order_cursor ORDER_ID_CURSOR;
  sql_statement VARCHAR2(10000);
  sql_statement1 VARCHAR(10000);
BEGIN
  idArr := csv_to_INT_ARRAY(idListCsv);
  idAddedArr := INT_ARRAY();
  -- Actual insert of order records.  We rollup the requested order sizes.
  -- We do not insert orders unless their status is unplanned (5) and they are not in the MCW.
  -- I would like to use bulk collect but these extra clauses make it impossible.
  sql_statement1 :='select distinct order_id  from order_Split ' ||
                'where order_split_id in (' || idListCsv || ')';
  FOR i IN idArr.FIRST..idArr.LAST LOOP
  IF isSplit =0 THEN
      SELECT COUNT(ORDER_STATUS) INTO bStatusUnplanned FROM ORDERS WHERE order_id=idArr(i) AND ORDER_STATUS=5;
      IF bStatusUnplanned=1 THEN
       --CR69340
         bAdded := p_add_Order_To_Unassigned(idArr(i), isSplit, shipperId, cwsId, size1UOM, size2UOM, size3UOM, size4UOM); -- CT18
         IF bAdded=1 THEN
           idAddedArr.EXTEND;
           idAddedArr(idAddedArr.LAST) := idArr(i);
         END IF;
    END IF;
  ELSE
      SELECT COUNT(order_split_status) INTO bStatusUnplanned FROM ORDER_SPLIT WHERE order_split_id=idArr(i) AND order_split_status=5;
      IF bStatusUnplanned=1 THEN
       --CR69340
         bAdded := p_add_Order_To_Unassigned(idArr(i), isSplit, shipperId, cwsId, size1UOM, size2UOM, size3UOM, size4UOM); -- CT18
         IF bAdded=1 THEN
           idAddedArr.EXTEND;
           idAddedArr(idAddedArr.LAST) := idArr(i);
         END IF;
    END IF;
  END IF;
  END LOOP;

  IF isSplit = 1 THEN
     OPEN order_cursor FOR sql_statement1;
     LOOP
     FETCH order_cursor INTO orderId;
     IF order_cursor%NOTFOUND
        THEN
          EXIT;
     END IF;
     SELECT COUNT(order_id) INTO bFound FROM
                       CWS_ORDER WHERE cons_workspace_id=cwsId AND order_id=orderId;
     IF bFound=0 THEN
          INSERT INTO CWS_ORDER
                (cons_workspace_id, order_id, last_refreshed_dttm,has_split,is_visible,
                --CR69340
                from_shipment_flag, size1, size1_uom_id, size2, size2_uom_id,size3, size3_uom_id,size4, size4_uom_id) -- CT18
          SELECT cwsId, orderId, SYSDATE, 1, 0,
          (CASE WHEN ORDERS.ORDER_STATUS=5 THEN 0 ELSE 1 END) AS FROM_SHIPMENT,
          SUM(ORDER_LINE_ITEM_SIZE.size_value*(CASE WHEN size_uom_id=size1UOM THEN 1 ELSE 0 END)) AS SIZE1, size1UOM,
          SUM(ORDER_LINE_ITEM_SIZE.size_value*(CASE WHEN SIZE_UOM_id=size2UOM THEN 1 ELSE 0 END)) AS SIZE2, size2UOM,
          --CR69340
          SUM(ORDER_LINE_ITEM_SIZE.size_value*(CASE WHEN SIZE_UOM_id=size3UOM THEN 1 ELSE 0 END)) AS SIZE3, size3UOM,
          SUM(ORDER_LINE_ITEM_SIZE.size_value*(CASE WHEN SIZE_UOM_id=size4UOM THEN 1 ELSE 0 END)) AS SIZE4, size4UOM -- CT18
          FROM ORDER_LINE_ITEM_SIZE  RIGHT OUTER JOIN ORDERS
               ON (ORDER_LINE_ITEM_SIZE.order_id=ORDERS.order_id AND ORDER_LINE_ITEM_SIZE.tc_company_id =ORDERS.tc_company_id)
          WHERE (ORDERS.order_id=orderId)
          GROUP BY ORDERS.ORDER_STATUS;
     ELSE
         UPDATE CWS_ORDER  CWSORD SET CWSORD.has_split = 1, CWSORD.is_visible = 0
           WHERE CWSORD.order_id = orderId AND CWSORD.cons_workspace_id=cwsId;
     END IF;
     UPDATE CWS_ORDER CWSORD SET CWSORD.is_partially_planned =
          (SELECT is_partially_planned FROM ORDERS WHERE order_id =orderId )
           WHERE CWSORD.order_id = orderId AND CWSORD.cons_workspace_id=cwsId;
    END LOOP;
    CLOSE order_cursor;

  END IF;



  ordersAddedCsv := int_array_to_csv(idAddedArr);
END add_Orders_To_Unassigned;
-- Add PLaNned or greater orders' shipments to the workspace.  This is a handy wrapper for adding
-- shipments to the workspace on the order list page.
PROCEDURE add_PLN_Orders_To_Workspace(
                 ordersAddedCsv OUT VARCHAR2,
              shipperId INTEGER,
              cwsId INTEGER,
              idListCsv VARCHAR2,
              size1UOM INTEGER,
              size2UOM INTEGER,
        --CR69340
        size3UOM INTEGER,
        size4UOM INTEGER, -- CT18
              isSplit INTEGER) IS
orderIdArr INT_ARRAY;
omShipmentIdArr INT_ARRAY;
addShipmentIdArr INT_ARRAY;
foundInAddList NUMBER(1);
shipmentsAdded VARCHAR(2000);
ordersAddedArr INT_ARRAY;
BEGIN
     -- Convert the [order] id list into a unique shipment id list
     orderIdArr :=  csv_to_INT_ARRAY(idListCsv);
     addShipmentIdArr := INT_ARRAY();
     ordersAddedArr := INT_ARRAY();
     FOR i IN orderIdArr.FIRST..orderIdArr.LAST LOOP
         IF isSplit = 0 THEN
             SELECT DISTINCT SHIPMENT_ID BULK COLLECT INTO omShipmentIdArr FROM (
               (SELECT ORDERS.SHIPMENT_ID FROM ORDERS
                WHERE (ORDERS.ORDER_ID=orderIdArr(i)) AND ORDERS.SHIPMENT_ID IS NOT NULL)
               UNION
               (SELECT ORDER_MOVEMENT.SHIPMENT_ID FROM ORDER_MOVEMENT
                    WHERE (ORDER_MOVEMENT.ORDER_ID=orderIdArr(i)))
             );
        ELSE
             SELECT DISTINCT SHIPMENT_ID BULK COLLECT INTO omShipmentIdArr FROM (
               (SELECT ORDER_SPLIT.SHIPMENT_ID FROM ORDER_SPLIT
                WHERE (ORDER_SPLIT.ORDER_SPLIT_ID=orderIdArr(i)) AND ORDER_SPLIT.SHIPMENT_ID IS NOT NULL)
               UNION
               (SELECT ORDER_MOVEMENT.SHIPMENT_ID FROM ORDER_MOVEMENT
                    WHERE (ORDER_MOVEMENT.ORDER_SPLIT_ID=orderIdArr(i)))
             );
        END IF;
         -- If any shipment id's, add to add list if not already present.
         IF omShipmentIdArr.COUNT > 0 THEN
             FOR j IN omShipmentIdArr.FIRST..omShipmentIdArr.LAST LOOP
                foundInAddList := 0;
                IF addShipmentIdArr.COUNT > 0 THEN
                    FOR k IN addShipmentIdArr.FIRST..addShipmentIdArr.LAST LOOP
                        IF omShipmentIdArr(j)=addShipmentIdArr(k) THEN
                           foundInAddList := 1;
                           EXIT;
                        END IF;
                    END LOOP;
                END IF;
                IF foundInAddList = 0 THEN
                    addShipmentIdArr.EXTEND;
                   addShipmentIdArr(addShipmentIdArr.LAST) := omShipmentIdArr(j);
                END IF;
            END LOOP;
         END IF;
     END LOOP;
     -- Add the shipments
     IF addShipmentIdArr.COUNT > 0 THEN
         add_Shipments_To_Workspace(shipmentsAdded, shipperId, cwsId,
             int_array_to_csv(addShipmentIdArr),
         --CR69340
             size1UOM, size2UOM, size3UOM, size4UOM); -- CT18
     END IF;
     -- Figure out what requested orders are now in the workspace - these must have been added by this user
     -- from this operation since the orders are planned.
     FOR i IN orderIdArr.FIRST..orderIdArr.LAST LOOP
       IF isSplit = 0 THEN
            SELECT COUNT(1) INTO foundInAddList FROM CWS_ORDER WHERE
            CWS_ORDER.CONS_WORKSPACE_ID=cwsId AND CWS_ORDER.ORDER_ID=orderIdarr(i);
       ELSE
            SELECT COUNT(1) INTO foundInAddList FROM CWS_ORDER_SPLIT WHERE
            CWS_ORDER_SPLIT.CONS_WORKSPACE_ID=cwsId AND CWS_ORDER_SPLIT.ORDER_SPLIT_ID=orderIdarr(i);
       END IF;
         IF foundInAddList=1 THEN
              ordersAddedArr.EXTEND;
              ordersAddedArr(i) := orderIdArr(i);
         END IF;
     END LOOP;
     ordersAddedCsv := int_array_to_csv(ordersAddedArr);
END add_PLN_Orders_To_Workspace;
-- Add external shipments to the workspace
PROCEDURE add_Shipments_To_Pre_workspace(
                 shipmentsAddedCsv OUT VARCHAR2,
              shipperId INTEGER,
              cwsId INTEGER,
              idListCsv VARCHAR2,
              size1UOM INTEGER,
              size2UOM INTEGER,
              -- CT4
              size3UOM INTEGER,
              size4UOM INTEGER) IS
  idArr INT_ARRAY;
  idAddedArr INT_ARRAY;
  orderIdOnShipment INT_ARRAY;
  bFound NUMBER(1);
  bAdded NUMBER(1);
  bIsMovement NUMBER(3);
  bIsNonMovementWithOrders NUMBER(3);
  wsShipmentId CWS_SHIPMENT.WS_SHIPMENT_ID%TYPE;
  stop_seq_cursor CURSOR_TYPE;
  shipmentId INTEGER;
  stopSeq1 STOP.STOP_SEQ%TYPE;
  stopAction1 STOP_ACTION.ACTION_TYPE%TYPE;
  orderId1 STOP_ACTION_ORDER.ORDER_ID%TYPE;
  stopSeq2 STOP.STOP_SEQ%TYPE;
  stopAction2 STOP_ACTION.ACTION_TYPE%TYPE;
  orderId2 STOP_ACTION_ORDER.ORDER_ID%TYPE;
BEGIN
  idArr := csv_to_INT_ARRAY(idListCsv);
  idAddedArr := INT_ARRAY();
  FOR i IN idArr.FIRST..idArr.LAST LOOP
    shipmentId := idArr(i);
    -- Do not add if already in po workspace.
      SELECT COUNT(orig_shipment_id) INTO bFound FROM
           CWS_POS_SHIPMENT WHERE cons_workspace_id=cwsId AND orig_shipment_id=idArr(i);
    IF bFound=0 THEN
       -- Generate unique WS shipment ID
       SELECT seq_ws_pos_shipment_id.NEXTVAL INTO wsShipmentId FROM dual;
   -- Insert the shipment record
           INSERT
           INTO CWS_POS_SHIPMENT
               (
               CONS_WORKSPACE_ID,
               WS_POS_SHIPMENT_ID,
               TC_COMPANY_ID,
               ORIG_SHIPMENT_ID,
               TC_SHIPMENT_ID,
               LAST_REFRESHED_DTTM,
               LAST_UPDATED_DTTM,
               NEEDS_REFRESH,
               SHIPMENT_COMPS_ENABLED,
               IS_HAZMAT,
               IS_PERISHABLE,
               PROMOTE_TO_AVAIL_FLAG,
               HAS_HARD_CHECK_ERROR,
               HAS_SOFT_CHECK_ERROR,
               O_ADDRESS,
               O_CITY,
               O_STATE_PROV,
               O_POSTAL_CODE,
               O_COUNTY,
               O_COUNTRY_CODE,
               D_ADDRESS,
               D_CITY,
               D_STATE_PROV,
               D_POSTAL_CODE,
               D_COUNTY,
               D_COUNTRY_CODE,
               FEASIBLE_MOT_ID,
               FEASIBLE_EQUIPMENT_ID,
               FEASIBLE_SERVICE_LEVEL_ID,
               FEASIBLE_CARRIER_ID,
               DELIVERY_REQ,
               DROPOFF_PICKUP,
               PACKAGING,
               SAVE_MOT_AS_DSG_FLAG,
               SAVE_EQUIP_AS_DSG_FLAG,
               SAVE_SL_AS_DSG_FLAG,
               SAVE_CC_AS_DSG_FLAG,
               PRODUCT_CLASS_ID,
               TRANS_RESP_CODE,
               PROTECTION_LEVEL_ID,
               BILLING_METHOD,
               -- BUSINESS_UNIT,
               COMMODITY_CLASS,
               DSG_MOT_ID,
               DSG_CARRIER_ID,
               DSG_EQUIPMENT_ID,
               DSG_SERVICE_LEVEL_ID,
               TOTAL_DISTANCE,
               DIRECT_DISTANCE,
               OUT_OF_ROUTE_DISTANCE,
               DISTANCE_UOM,
               SIZE1_VALUE,
               SIZE1_UOM_ID,
               SIZE2_VALUE,
               SIZE2_UOM_ID,
               -- CT18
               SIZE3_VALUE,
               SIZE3_UOM_ID,
               SIZE4_VALUE,
               SIZE4_UOM_ID,
               NUM_STOPS,
               PICKUP_START_DTTM,
               PICKUP_END_DTTM,
               PICKUP_TZ,
               DELIVERY_START_DTTM,
               DELIVERY_END_DTTM,
               DELIVERY_TZ,
               INITIAL_COST,
               BASELINE_COST,
               ESTIMATED_COST,
               XDOCK_SORT_COST,
               NEEDS_VALIDATION
               )
           SELECT
               cwsId,
               wsShipmentId,
               SHIPMENT.TC_COMPANY_ID,
               SHIPMENT.SHIPMENT_ID,
               SHIPMENT.TC_SHIPMENT_ID,
               SYSDATE,
               SHIPMENT.LAST_UPDATED_DTTM,
               0,
               1,
               SHIPMENT.IS_HAZMAT,
               SHIPMENT.IS_PERISHABLE,
               DECODE(SHIPMENT.SHIPMENT_STATUS,10,0,20,1,2),
               SHIPMENT.HAS_IMPORT_ERROR,
               SHIPMENT.HAS_SOFT_CHECK_ERROR,
               SHIPMENT.O_ADDRESS,
               SHIPMENT.O_CITY,
               SHIPMENT.O_STATE_PROV,
               SHIPMENT.O_POSTAL_CODE,
               SHIPMENT.O_COUNTY,
               SHIPMENT.O_COUNTRY_CODE,
               SHIPMENT.D_ADDRESS,
               SHIPMENT.D_CITY,
               SHIPMENT.D_STATE_PROV,
               SHIPMENT.D_POSTAL_CODE,
               SHIPMENT.D_COUNTY,
               SHIPMENT.D_COUNTRY_CODE,
               SHIPMENT.FEASIBLE_MOT_ID,
               SHIPMENT.FEASIBLE_EQUIPMENT_ID,
               SHIPMENT.FEASIBLE_SERVICE_LEVEL_ID,
               SHIPMENT.FEASIBLE_CARRIER_ID,
               SHIPMENT.DELIVERY_REQ,
               SHIPMENT.DROPOFF_PICKUP,
               SHIPMENT.PACKAGING,
               0,
               0,
               0,
               0,
               SHIPMENT.PRODUCT_CLASS_ID,
               SHIPMENT.TRANS_RESP_CODE,
               SHIPMENT.PROTECTION_LEVEL_ID,
               SHIPMENT.BILLING_METHOD,
               --shipment.BUSINESS_UNIT,
               SHIPMENT.COMMODITY_CLASS,
               SHIPMENT.DSG_MOT_ID,
               SHIPMENT.DSG_CARRIER_ID,
               SHIPMENT.DSG_EQUIPMENT_ID,
               SHIPMENT.DSG_SERVICE_LEVEL_ID,
               SHIPMENT.DISTANCE,
               SHIPMENT.DIRECT_DISTANCE,
               SHIPMENT.OUT_OF_ROUTE_DISTANCE,
               SHIPMENT.DISTANCE_UOM,
               SUM(SHIPMENT_COMMODITY.size_value*DECODE(SIZE_UOM_ID,size1UOM,1,0)),
               size1UOM,
               SUM(SHIPMENT_COMMODITY.size_value*DECODE(SIZE_UOM_ID,size2UOM,1,0)),
               size2UOM,
               -- CT18
               SUM(SHIPMENT_COMMODITY.size_value*DECODE(SIZE_UOM_ID,size3UOM,1,0)),
	       size3UOM,
	       SUM(SHIPMENT_COMMODITY.size_value*DECODE(SIZE_UOM_ID,size4UOM,1,0)),
               size4UOM,
               DECODE(SHIPMENT.NUM_STOPS, NULL, 0, 0, 0, SHIPMENT.NUM_STOPS+1),
               SHIPMENT.PICKUP_START_DTTM,
               SHIPMENT.PICKUP_END_DTTM,
               SHIPMENT.PICKUP_TZ,
               SHIPMENT.DELIVERY_START_DTTM,
               SHIPMENT.DELIVERY_END_DTTM,
               SHIPMENT.DELIVERY_TZ,
               SHIPMENT.ESTIMATED_COST,
               SHIPMENT.NORMALIZED_BASELINE_COST,
               SHIPMENT.ESTIMATED_COST,
               NULL, -- TODO - need to compute the xdock sort here???  or maybe CWS does this and remove this column?  think.
               0
           FROM SHIPMENT,
               SHIPMENT_COMMODITY
           WHERE SHIPMENT_COMMODITY.shipment_id(+)=SHIPMENT.shipment_id
               AND SHIPMENT.shipment_id=idArr(i)
               --AND shipment.tc_company_id=shipperId
           GROUP BY SHIPMENT.TC_COMPANY_ID, SHIPMENT.SHIPMENT_ID,
               SHIPMENT.TC_SHIPMENT_ID,
               SHIPMENT.LAST_UPDATED_DTTM,
               SHIPMENT.IS_HAZMAT,
               SHIPMENT.IS_PERISHABLE,
               SHIPMENT.SHIPMENT_STATUS,
               SHIPMENT.HAS_IMPORT_ERROR,
               SHIPMENT.HAS_SOFT_CHECK_ERROR,
               SHIPMENT.O_ADDRESS,
               SHIPMENT.O_CITY,
               SHIPMENT.O_STATE_PROV,
               SHIPMENT.O_POSTAL_CODE,
               SHIPMENT.O_COUNTY,
               SHIPMENT.O_COUNTRY_CODE,
               SHIPMENT.D_ADDRESS,
               SHIPMENT.D_CITY,
               SHIPMENT.D_STATE_PROV,
               SHIPMENT.D_POSTAL_CODE,
               SHIPMENT.D_COUNTY,
               SHIPMENT.D_COUNTRY_CODE,
               SHIPMENT.FEASIBLE_MOT_ID,
               SHIPMENT.FEASIBLE_EQUIPMENT_ID,
               SHIPMENT.FEASIBLE_SERVICE_LEVEL_ID,
               SHIPMENT.FEASIBLE_CARRIER_ID,
               SHIPMENT.DELIVERY_REQ,
               SHIPMENT.DROPOFF_PICKUP,
               SHIPMENT.PACKAGING,
               SHIPMENT.PRODUCT_CLASS_ID,
               SHIPMENT.TRANS_RESP_CODE,
               SHIPMENT.PROTECTION_LEVEL_ID,
               SHIPMENT.BILLING_METHOD,
               -- shipment.BUSINESS_UNIT,
               SHIPMENT.COMMODITY_CLASS,
               SHIPMENT.DSG_MOT_ID,
               SHIPMENT.DSG_CARRIER_ID,
               SHIPMENT.DSG_EQUIPMENT_ID,
               SHIPMENT.DSG_SERVICE_LEVEL_ID,
               SHIPMENT.DISTANCE,
               SHIPMENT.DIRECT_DISTANCE,
               SHIPMENT.OUT_OF_ROUTE_DISTANCE,
               SHIPMENT.DISTANCE_UOM,
               SHIPMENT.NUM_STOPS,
               SHIPMENT.PICKUP_START_DTTM,
               SHIPMENT.PICKUP_END_DTTM,
               SHIPMENT.PICKUP_TZ,
               SHIPMENT.DELIVERY_START_DTTM,
               SHIPMENT.DELIVERY_END_DTTM,
               SHIPMENT.DELIVERY_TZ,
               SHIPMENT.ESTIMATED_COST,
               SHIPMENT.NORMALIZED_BASELINE_COST,
               SHIPMENT.ESTIMATED_COST;
           idAddedArr.EXTEND;
         idAddedArr(idAddedArr.LAST) := idArr(i);
    END IF;
  END LOOP;

  shipmentsAddedCsv := int_array_to_csv(idAddedArr);
END add_Shipments_To_Pre_workspace;
-- It does this by adding order movements when possible.  The return is a Csv of 0's and 1's
-- indicating success or failure for each requested added movment.
PROCEDURE assign_Orders_To_Pos_Draft(
                 ordersAssignedBoolCsv OUT VARCHAR2,
              shipperId INTEGER,
              cwsId INTEGER,
              orderIdCsv VARCHAR2,
              cwsShipmentIdCsv VARCHAR2,
              omShipmentIdCsv VARCHAR2,
              pickupSeqCsv VARCHAR2,
              deliverySeqCsv VARCHAR2,
              movementTypeCsv VARCHAR2,
              pathSetIdCsv VARCHAR2,
              pathIdCsv VARCHAR2,
              orderShipmentSeqCsv VARCHAR2) IS
orderIdArr INT_ARRAY;
cwsShipmentIdArr INT_ARRAY;
omShipmentIdArr INT_ARRAY;
pickupSeqArr INT_ARRAY;
deliverySeqArr INT_ARRAY;
movementTypeArr STRING_ARRAY;
pathSetIdArr INT_ARRAY;
pathIdArr INT_ARRAY;
orderShipmentSeqArr INT_ARRAY;
ordersAssignedBoolArr INT_ARRAY;
orderMovementExists NUMBER(1);
wsShipmentInsertArr INT_ARRAY;
BEGIN
     -- Convert the input CSVs into the appropriate arrays
    orderIdArr := csv_to_int_array(orderIdCsv);
    cwsShipmentIdArr := csv_to_int_array(cwsShipmentIdCsv);
    omShipmentIdArr := csv_to_int_array(omShipmentIdCsv);
    pickupSeqArr := csv_to_int_array(pickupSeqCsv);
    deliverySeqArr := csv_to_int_array(deliverySeqCsv);
    movementTypeArr := csv_to_string_array(movementTypeCsv);
    pathSetIdArr := csv_to_int_array(pathSetIdCsv);
    pathIdArr := csv_to_int_array(pathIdCsv);
    orderShipmentSeqArr := csv_to_int_array(orderShipmentSeqCsv);
    ordersAssignedBoolArr := INT_ARRAY();
    wsShipmentInsertArr := INT_ARRAY();
    -- For each movment, test for existing violating PK or path constraints.
    -- If none, insert movement.
    FOR j IN orderIdArr.FIRST..orderIdArr.LAST LOOP
        ordersAssignedBoolArr.EXTEND;
        SELECT COUNT(1) INTO orderMovementExists FROM CWS_POS_ORDER_MOVEMENT CWSOM
                   WHERE ((CWSOM.CONS_WORKSPACE_ID=cwsId) AND (CWSOM.ORDER_ID=orderIdArr(j)))
                        AND
                       ((CWSOM.ORDER_SHIPMENT_SEQ=orderShipmentSeqArr(j)) AND
                        (CWSOM.WS_POS_SHIPMENT_ID=cwsShipmentIdArr(j)));
        IF orderMovementExists=0 THEN
              INSERT INTO CWS_POS_ORDER_MOVEMENT
                 (CONS_WORKSPACE_ID, ORDER_ID, WS_POS_SHIPMENT_ID, MOVEMENT_TYPE, ORDER_SHIPMENT_SEQ,
                  TC_COMPANY_ID, PATH_SET_ID, PATH_ID, PICKUP_STOP_SEQ, DELIVERY_STOP_SEQ)
                 VALUES
                 (cwsId, orderIdArr(j), cwsShipmentIdArr(j), movementTypeArr(j), orderShipmentSeqArr(j),
                  shipperId, pathSetIdArr(j), pathIdArr(j), pickupSeqArr(j), deliverySeqArr(j));
              ordersAssignedBoolArr(ordersAssignedBoolArr.LAST) := 1;
              wsShipmentInsertArr.EXTEND;
              wsShipmentInsertArr(wsShipmentInsertArr.LAST) := cwsShipmentIdArr(j);
        ELSE
              ordersAssignedBoolArr(ordersAssignedBoolArr.LAST) := 0;
        END IF;
     END LOOP;
     p_mark_For_Refresh(cwsId, wsShipmentInsertArr);
     p_ReEnable_Comps(cwsId,wsShipmentInsertArr);
     --COMMIT;
     ordersAssignedBoolCsv := int_array_to_csv(ordersAssignedBoolArr);
END assign_Orders_To_Pos_Draft;
PROCEDURE clean_Pre_Workspace(
              shipperId INTEGER,
              cwsId INTEGER) IS
BEGIN
     DELETE FROM CWS_POS_ORDER_MOVEMENT WHERE cons_workspace_id=cwsId;
     DELETE FROM CWS_POS_SHIPMENT WHERE cons_workspace_id=cwsId;
--     COMMIT;
END clean_Pre_Workspace;
PROCEDURE load_Pre_Workspace(
                 shipmentListRefcur OUT CURSOR_TYPE,        -- Cws shipment display info
              assignedOrderListRefcur OUT CURSOR_TYPE,    -- Cws assigned order display info
              unassignedOrderListRefcur OUT CURSOR_TYPE,-- Cws unassigned order display info
              movedOrderListRefcur OUT CURSOR_TYPE,     -- cws moded order list
              retCwsShipmentId OUT INTEGER,                  -- current draft shipment id as determined by the sproc
              statCwsShipmentCount OUT NUMBER,            -- total # Cws shipments in workspace
              statTotalStopCount OUT NUMBER,            -- total # of stops across all shipments
              statTotalOrderMovementCount OUT NUMBER,   -- total # of order movements across all shipments
              statBaselineCost OUT CWS_POS_SHIPMENT.BASELINE_COST%TYPE,   -- total baseline cost for shipments
              statEstimatedCost OUT CWS_POS_SHIPMENT.ESTIMATED_COST%TYPE, -- total estimated cost for shipments
              statInitialCost OUT CWS_POS_SHIPMENT.INITIAL_COST%TYPE,     -- total initial cost for shipments
              shipperId INTEGER,                            -- company id
              cwsId INTEGER,                                -- workspace id
              currentCwsShipmentId INTEGER,                  -- current draft shipment id
              shipmentListOrderBy VARCHAR2,             -- order by clause for shipment list or empty string
              shipmentListMinRow NUMBER,                  -- first row to retrieve (0-based)
              shipmentListMaxRow NUMBER,                -- last row + 1 to retrieve (ex: 0..10 retrieves first 10 shipments)
              orderIdMDFilter NUMBER,                    -- for MD link, if non-null, joins to cws_order_movement to restrict list of shipment to this order id
              assignedOrderListOrderBy VARCHAR2,         -- order by clause for assigned orders list or empty string
              unassignedOrderListOrderBy VARCHAR2)       -- order by clause for unassigned orders list or empty string
              IS
   sql_statement1 VARCHAR(2000);
   sql_statement2 VARCHAR(2000);
   sql_statement3 VARCHAR(2000);
   sql_statement4 VARCHAR(2000);
   shipment_id_cursor CURSOR_TYPE;
   vcurrow NUMBER;
   vidlist_done NUMBER;
   vShipmentId CWS_POS_SHIPMENT.WS_POS_SHIPMENT_ID%TYPE;
   orderCount NUMBER;
   vgttcount NUMBER;
   shipment_list_cursor CURSOR_TYPE;
   asg_order_list_cursor CURSOR_TYPE;
   una_order_list_cursor CURSOR_TYPE;
   mov_order_list_cursor CURSOR_TYPE;
   firstCwsShipmentId CWS_POS_SHIPMENT.WS_POS_SHIPMENT_ID%TYPE;
   foundCurrentShipmentId NUMBER(1);
   order_null_check    PLS_INTEGER    := 0;
    partial_shipment_network PLS_INTEGER := 0;
BEGIN
    -- First get the shipment id list for the given range in the correct order
    DELETE MAN_CONS_SHIPMENT_LIST_GTT;
    sql_statement1 := 'select WS_POS_SHIPMENT_ID, COUNT(*) SORT_ORDER'
                    ||' from    CWS_POS_ORDER_MOVEMENT '
                    ||' where    CONS_WORKSPACE_ID =:cwsId '
                    ||' group by WS_POS_SHIPMENT_ID   order by    sort_order desc ';
       OPEN shipment_id_cursor FOR sql_statement1 USING cwsId;
   -- Skip rows before minrow, starting at 0
   vcurrow := 0;
   vidlist_done := 0;
   foundCurrentShipmentId := 0;
   <<c1_cursor_loop>>
   LOOP
          /*DBMS_OUTPUT.put_line('c1 loop, vcurrow = ' || to_char(vcurrow));*/
          EXIT WHEN vcurrow >= shipmentListMinRow;
          FETCH shipment_id_cursor INTO vShipmentId,orderCount;
       IF shipment_id_cursor%NOTFOUND THEN
             vidlist_done := 1;
          EXIT;
       END IF;
          /*DBMS_OUTPUT.put_line('c1 loop, fetched id = ' || to_char(vshipment_id));*/
       vcurrow := vcurrow + 1;
   END LOOP c1_cursor_loop;
   -- If any rows left, add these id's, up to maxrow-1
   IF vidlist_done = 0 THEN
       <<c2_cursor_loop>>
       LOOP
              /*DBMS_OUTPUT.put_line('c2 loop, vcurrow = ' || to_char(vcurrow));*/
              EXIT WHEN vcurrow >= shipmentListMaxRow;
              FETCH shipment_id_cursor INTO vShipmentId, orderCount;
           EXIT WHEN shipment_id_cursor%NOTFOUND;
           /*DBMS_OUTPUT.put_line('c2 loop, fetched id = ' || to_char(vshipment_id));*/
           INSERT INTO MAN_CONS_SHIPMENT_LIST_GTT (ROW_NUM, SHIPMENT_ID) VALUES (vcurrow, vShipmentId);
           vcurrow := vcurrow + 1;
           -- Save first shipment id in case user has no context for current shipment
           IF firstCwsShipmentId IS NULL THEN
                 firstCwsShipmentId := vShipmentId;
           END IF;
           IF vShipmentId = currentCwsShipmentId THEN
                 foundCurrentShipmentId := 1;
           END IF;
       END LOOP c2_cursor_loop;
--       COMMIT;
   END IF;
   CLOSE shipment_id_cursor;
   /* Debugging
   select count(1) into vgttcount from MAN_CONS_SHIPMENT_LIST_GTT;
   DBMS_OUTPUT.put_line('MAN_CONS_SHIPMENT_LIST_GTT count = ' || to_char(vgttcount));
   */
   -- Materialize the resultset based on the id's and the order of the row #'s
   OPEN shipment_list_cursor FOR
          SELECT /*+ ordered first_rows */
          CWSSHP.TC_COMPANY_ID,
        CWSSHP.WS_POS_SHIPMENT_ID, CWSSHP.TC_SHIPMENT_ID, CWSSHP.ORIG_SHIPMENT_ID,
            CWSSHP.O_CITY, CWSSHP.O_STATE_PROV, -- origin
            CWSSHP.D_CITY, CWSSHP.D_STATE_PROV, -- dest
            CWSSHP.TOTAL_DISTANCE,
        CWSSHP.FEASIBLE_MOT_ID,  -- feasible mode
        CWSSHP.FEASIBLE_EQUIPMENT_ID,
        CWSSHP.SIZE1_VALUE, CWSSHP.SIZE1_UOM_ID, CWSSHP.SIZE2_VALUE, CWSSHP.SIZE2_UOM_ID,  -- size1+UOM1, size2+UOM2
        CWSSHP.SIZE3_VALUE, CWSSHP.SIZE3_UOM_ID, CWSSHP.SIZE4_VALUE, CWSSHP.SIZE4_UOM_ID, -- CT18
        CWSSHP.NUM_STOPS,  -- # stops
        CWSSHP.PICKUP_END_DTTM, CWSSHP.PICKUP_TZ,  -- pickup end + TZ
        CWSSHP.DELIVERY_END_DTTM, CWSSHP.DELIVERY_TZ,  -- delivery end + TZ
        CWSSHP.PROMOTE_TO_AVAIL_FLAG,  -- promotion flag
        CWSSHP.ESTIMATED_COST,  -- est. cost
        CWSSHP.HAS_HARD_CHECK_ERROR, CWSSHP.HAS_SOFT_CHECK_ERROR,  -- hard/soft check flags
        CWSSHP.NEEDS_VALIDATION, CWSSHP.BASELINE_COST, --# This has been included for 4r1 as baseline cost is not being fetched
        CWSSHP.DISTANCE_UOM,
        0
          FROM MAN_CONS_SHIPMENT_LIST_GTT MCSLGTT, CWS_POS_SHIPMENT CWSSHP
          WHERE CWSSHP.CONS_WORKSPACE_ID=cwsId
          AND    MCSLGTT.SHIPMENT_ID=CWSSHP.WS_POS_SHIPMENT_ID
          ORDER BY MCSLGTT.ROW_NUM;
   -- Assigned order list: If passed a current cws shipment id.  If not, use the first shipment id
   -- retrieved above and set this in retCwsShipmentId.  Use order by clause (this requires dynamic sql...).
   IF (currentCwsShipmentId IS NULL) THEN
         retCwsShipmentId := firstCwsShipmentId;
   ELSE
      retCwsShipmentId := currentCwsShipmentId;
   END IF;
   /*
   sql_statement2 := 'select'
           || ' ORD.TC_COMPANY_ID,'
        || ' ORD.ORDER_ID, ORD.TC_ORDER_ID,'
        || ' ORD.O_CITY, ORD.O_STATE_PROV,'
        || ' ORD.D_CITY, ORD.D_STATE_PROV,'
        || ' ORD.PICKUP_START_DTTM, ORD.PICKUP_END_DTTM, ORD.PICKUP_TZ,'
        || ' ORD.DELIVERY_START_DTTM, ORD.DELIVERY_END_DTTM, ORD.DELIVERY_TZ,'
        || ' ORD.NORMALIZED_BASELINE_COST,'
        || ' CWSORD.SIZE1, CWSORD.SIZE1_UOM_ID,CWSORD.SIZE2, CWSORD.SIZE2_UOM_ID,'
        || ' CWSORD.SIZE3, CWSORD.SIZE3_UOM_ID,CWSORD.SIZE4, CWSORD.SIZE4_UOM_ID,' -- CT18
        || ' CWSOM.PICKUP_STOP_SEQ, CWSOM.DELIVERY_STOP_SEQ , ORD.WAVE_ID, CWSORD.CWS_PROCESS_ID '
        || ' from CWS_ORDER_MOVEMENT CWSOM, CWS_ORDER CWSORD, ORDERS ORD'
        || ' where (CWSOM.CONS_WORKSPACE_ID=:cwsId)'
        || ' and   (CWSORD.CONS_WORKSPACE_ID=CWSOM.CONS_WORKSPACE_ID and CWSORD.ORDER_ID=CWSOM.ORDER_ID)'
        || ' and   (ORD.ORDER_ID=CWSORD.ORDER_ID)'
  || ' and   (CWSORD.IS_VISIBLE = 1)'
        || ' ' || assignedOrderListOrderBy;
   OPEN asg_order_list_cursor FOR sql_statement2 USING cwsId, retCwsShipmentId;
   */

    sql_statement2 := 'select'
                   || ' ORD.TC_COMPANY_ID,'
                   || ' ORD.ORDER_ID, ORD.TC_ORDER_ID,'
                   || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.O_FACILITY_ALIAS_ID AND TC_COMPANY_ID = ORD.TC_COMPANY_ID) O_FACILITY_NAME, '
                   || ' ORD.O_CITY, ORD.O_STATE_PROV,'
                   || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.D_FACILITY_ALIAS_ID AND TC_COMPANY_ID = ORD.TC_COMPANY_ID) D_FACILITY_NAME, '
                   || ' ORD.D_CITY, ORD.D_STATE_PROV,'
                   || ' ORD.PICKUP_START_DTTM, ORD.PICKUP_END_DTTM, ORD.PICKUP_TZ,'
                   || ' ORD.DELIVERY_START_DTTM, ORD.DELIVERY_END_DTTM, ORD.DELIVERY_TZ,'
                   || ' ORD.PROTECTION_LEVEL_ID,'
                   || ' ORD.NORMALIZED_BASELINE_COST,'
                   || ' 0, null,0, null,'
                   || ' 0, null,0, null,' -- CT18
                   || ' CWSOM.PICKUP_STOP_SEQ, CWSOM.DELIVERY_STOP_SEQ, '
				   || ' (SELECT DISTINCT WAVOPT.WAVE_ID FROM WAVE_OPTION_LIST WAVOPT WHERE WAVOPT.FACILITY_ID=ORD.D_FACILITY_ID AND WAVOPT.WAVE_ID=ORD.WAVE_ID AND WAVOPT.WAVE_OPTION_ID=ORD.WAVE_OPTION_ID AND WAVOPT.DOCK_ID=ORD.D_DOCK_ID AND WAVOPT.TC_COMPANY_ID=ORD.TC_COMPANY_ID AND WAVOPT.MARK_FOR_DELETION = 0) WAVE_ID, '
				   || ' 0 '
                   || ' from CWS_POS_ORDER_MOVEMENT CWSOM,  ORDERS ORD '
				   || ' where (CWSOM.CONS_WORKSPACE_ID=:cwsId )'
                   || ' and   (ORD.ORDER_ID=CWSOM.ORDER_ID)'
                   || ' ' || assignedOrderListOrderBy;

OPEN asg_order_list_cursor FOR sql_statement2 USING cwsId;

   -- Unassigned order list: Simple (I hope!).  Use order by clause (more dynamic sql ack).
   sql_statement3 := 'select'
           || ' ORD.TC_COMPANY_ID,'
        || ' ORD.ORDER_ID, ORD.TC_ORDER_ID,'
        || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.O_FACILITY_ALIAS_ID AND TC_COMPANY_ID = ORD.TC_COMPANY_ID) O_FACILITY_NAME, '
        || ' ORD.O_CITY, ORD.O_STATE_PROV,'
        || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.D_FACILITY_ALIAS_ID AND TC_COMPANY_ID = ORD.TC_COMPANY_ID) D_FACILITY_NAME, '
        || ' ORD.D_CITY, ORD.D_STATE_PROV,'
        || ' ORD.PICKUP_START_DTTM, ORD.PICKUP_END_DTTM, ORD.PICKUP_TZ,'
        || ' ORD.DELIVERY_START_DTTM, ORD.DELIVERY_END_DTTM, ORD.DELIVERY_TZ,'
        || ' ORD.PROTECTION_LEVEL_ID,'
        || ' ORD.NORMALIZED_BASELINE_COST,'
        || ' CWSORD.SIZE1, CWSORD.SIZE1_UOM_ID,CWSORD.SIZE2, CWSORD.SIZE2_UOM_ID,'
        || ' CWSORD.SIZE3, CWSORD.SIZE3_UOM_ID,CWSORD.SIZE4, CWSORD.SIZE4_UOM_ID,' -- CT18
        || ' null PICKUP_STOP_SEQ, null DELIVERY_STOP_SEQ, '
		|| ' (select ORD.WAVE_ID from ORDERS ORD, WAVE_OPTION_LIST WAOPTLIST, CWS_ORDER CWSORD where  ORD.ORDER_ID=CWSORD.ORDER_ID and WAOPTLIST.WAVE_ID = ORD.WAVE_ID AND WAOPTLIST.FACILITY_ID = ORD.D_FACILITY_ID AND WAOPTLIST.MARK_FOR_DELETION = 0) WAVE_ID, '
		|| ' 0 '
        || ' from CWS_ORDER CWSORD, ORDERS ORD '
        || ' where (CWSORD.CONS_WORKSPACE_ID=:cwsId)'
        || ' and (not exists'
        || '   (select 1 from CWS_ORDER_MOVEMENT CWSOM where CWSOM.CONS_WORKSPACE_ID=CWSORD.CONS_WORKSPACE_ID and CWSOM.ORDER_ID=CWSORD.ORDER_ID))'
        || ' and (ORD.ORDER_ID=CWSORD.ORDER_ID)'
        || ' ' || unassignedOrderListOrderBy;
   OPEN una_order_list_cursor FOR sql_statement3 USING cwsId;
   -- moved order list
   sql_statement4 := 'select'
           || ' ORD.CONS_WORKSPACE_ID,'
        || ' ORD.ORDER_ID,'
        || ' ORD.WS_POS_SHIPMENT_ID, ORD.MOVEMENT_TYPE,'
        || ' ORD.ORDER_SHIPMENT_SEQ, ORD.TC_COMPANY_ID,'
        || ' ORD.PATH_SET_ID, ORD.PATH_ID,'
        || ' ORD.PICkUP_STOP_SEQ, ORD.DELIVERY_STOP_SEQ '
        || ' from    CWS_POS_ORDER_MOVEMENT ORD,'
        || ' (    select WS_POS_SHIPMENT_ID, COUNT(*) SORT_ORDER '
        || '           from    CWS_POS_ORDER_MOVEMENT '
        || '           where    CONS_WORKSPACE_ID =:cwsId '
        || '           group by WS_POS_SHIPMENT_ID '
        || ' ) v_help '
        || ' where    ORD.CONS_WORKSPACE_ID =:cwsId '
        || ' and    ORD.WS_POS_SHIPMENT_ID = v_help.WS_POS_SHIPMENT_ID '
        || ' order by    v_help.sort_order desc, ORD.ORDER_ID';
   OPEN mov_order_list_cursor FOR sql_statement4 USING cwsId, cwsId;
   -- Compute statistics using 1 or more aggregate queries into output scalars
   -- Shipment count, Estimated cost (includes sorting cost), initial cost)
   SELECT COUNT(1),
             (SUM(CWS_POS_SHIPMENT.ESTIMATED_COST)+NVL(SUM(CWS_POS_SHIPMENT.XDOCK_SORT_COST),0)),
          SUM(CWS_POS_SHIPMENT.INITIAL_COST)
          INTO statCwsShipmentCount, statEstimatedCost, statInitialCost
          FROM CWS_POS_SHIPMENT
          WHERE CWS_POS_SHIPMENT.CONS_WORKSPACE_ID=cwsId;
   -- Baseline cost will now be computed as the sum of the assigned orders' baseline costs.  Otherwise,
   -- we might count the baseline cost multiple times if the order is on multiple shipments (per MichelleF).
   -- But! If any order baseline cost is null for a shipment, that shipment has no baseline cost.  And
   -- if any orders in a movement network have null baseline cost, the whole network does.  Got it?  Here's the way:
    -- Step 0: incase there are any orders with Baseline cost as null, that shipment should have the null baseline and hence workspace
    -- Step 1: Get list of orders assigned to shipments that have null baseline cost.
    -- Step 2: Get list of CWS shipments for step 1 orders.
    -- Step 3: Get list of CWS order movements for step 2 shipments - none of these can count in the total
    -- Step 4: Sum the baseline cost of the orders assigned to shipment MINUS the orders on the movements from step 3.
    SELECT    COUNT(ORD1.order_id)
    INTO        order_null_check
    FROM        ORDERS ORD1
    WHERE        ORD1.ORDER_ID IN    (    SELECT    CWSOM1.ORDER_ID
                            FROM        CWS_ORDER_MOVEMENT CWSOM1
                            WHERE        CWSOM1.CONS_WORKSPACE_ID=cwsId
                        )
    AND        ORD1.NORMALIZED_BASELINE_COST IS NULL
    ;
    --If there are orders whose all shipments are not  inside
    --we show baseline cost of workspace as null
    /*
    select count(*) into partial_shipment_network from
    (
                select cws_order.order_id from cws_order
                    where cons_workspace_id= cwsId and cws_order.is_partially_planned = 1
                union
                select cwsor1.order_id from cws_order cwsor1 where cons_workspace_id= cwsId and exists
                (
                            --for orders shipment_id can be there on order table or on order_movement
                            ( select shipment_id from orders  where order_id = cwsor1.order_id
                                and shipment_id is not null
                                union
                                select distinct shipment_id from order_movement  where order_id = cwsor1.order_id
                            )
                            minus --subtract inside shipments
                            select distinct orig_shipment_id from cws_shipment,cws_order_movement  where
                                cws_order_movement.order_id= cwsor1.order_id
                                and cws_shipment.ws_shipment_id= cws_order_movement.ws_shipment_id
                                and orig_shipment_id is not null
                )
    );
    IF    (order_null_check    = 0 AND partial_shipment_network =0) THEN*/
    IF ( order_null_check = 0 ) THEN
    SELECT SUM(ORD4.NORMALIZED_BASELINE_COST) INTO statBaselineCost FROM ORDERS ORD4
      WHERE ORD4.ORDER_ID IN (
      (SELECT DISTINCT CWSOM4.ORDER_ID FROM CWS_ORDER_MOVEMENT CWSOM4
                  WHERE CWSOM4.CONS_WORKSPACE_ID=cwsId)
      MINUS
      (SELECT DISTINCT CWSOM3.order_id FROM CWS_ORDER_MOVEMENT CWSOM3
             WHERE CWSOM3.CONS_WORKSPACE_ID=cwsId AND CWSOM3.WS_SHIPMENT_ID IN
        (SELECT DISTINCT CWSOM2.ws_shipment_id FROM CWS_ORDER_MOVEMENT CWSOM2
             WHERE CWSOM2.CONS_WORKSPACE_ID=cwsId AND CWSOM2.ORDER_ID IN
          (SELECT ORD1.order_id FROM ORDERS ORD1
                 WHERE ORD1.ORDER_ID IN
                  (SELECT DISTINCT CWSOM1.ORDER_ID FROM CWS_ORDER_MOVEMENT CWSOM1
                WHERE CWSOM1.CONS_WORKSPACE_ID=cwsId)
               AND (ORD1.NORMALIZED_BASELINE_COST IS NULL)))));
    END IF;
    -- Total stops
    SELECT COUNT(1) INTO statTotalStopCount FROM CWS_STOP
             WHERE CWS_STOP.CONS_WORKSPACE_ID=cwsId;
    -- Total order movements (orders on shipments)
    SELECT COUNT(1) INTO statTotalOrderMovementCount FROM CWS_ORDER_MOVEMENT
             WHERE CWS_ORDER_MOVEMENT.CONS_WORKSPACE_ID=cwsId;
   -- Set return ref cursors
   shipmentListRefcur := shipment_list_cursor;
   assignedOrderListRefcur := asg_order_list_cursor;
   unassignedOrderListRefcur := una_order_list_cursor;
   movedOrderListRefcur := mov_order_list_cursor;
END load_Pre_Workspace;
-- Add external shipments to the workspace
PROCEDURE add_Shipments_To_Workspace(
                 shipmentsAddedCsv OUT VARCHAR2,
              shipperId INTEGER,
              cwsId INTEGER,
              idListCsv VARCHAR2,
              size1UOM INTEGER,
              size2UOM INTEGER,
              --CR69340
              size3UOM INTEGER,
              size4UOM INTEGER -- CT18
        ) IS
  idArr INT_ARRAY;
  idAddedArr INT_ARRAY;
  orderIdOnShipment INT_ARRAY;
  compartmentNoOnOrder INT_ARRAY;
  pathSetIdArr INT_ARRAY;
  pathIdArr INT_ARRAY;
  orderSplitIdArr INT_ARRAY;
  loadingSeqArr INT_ARRAY;
  compartmentNo INTEGER;
  bFound NUMBER(1);
  bAdded NUMBER(1);
  bIsMovement NUMBER(3);
  pathSetId ORDERS.PATH_SET_ID%TYPE;
  pathId ORDERS.PATH_ID%TYPE;
  bIsNonMovementWithOrders NUMBER(3);
  wsShipmentId CWS_SHIPMENT.WS_SHIPMENT_ID%TYPE;
  stop_seq_cursor CURSOR_TYPE;
  shipmentId INTEGER;
  stopSeq1 STOP.STOP_SEQ%TYPE;
  initialStopSeq1 STOP.STOP_SEQ%TYPE;
  stopAction1 STOP_ACTION.ACTION_TYPE%TYPE;
  orderId1 STOP_ACTION_ORDER.ORDER_ID%TYPE;
  orderSplitId1 STOP_ACTION_ORDER.ORDER_SPLIT_ID%TYPE;
  loadingSeq NUMBER;
  isRelay NUMBER;
  stopSeq2 STOP.STOP_SEQ%TYPE;
  stopAction2 STOP_ACTION.ACTION_TYPE%TYPE;
  orderId2 STOP_ACTION_ORDER.ORDER_ID%TYPE;
  orderSplitId2 STOP_ACTION_ORDER.ORDER_SPLIT_ID%TYPE;
  tempIndex NUMBER;
  orderIdTemp NUMBER;
    compartmentNoTemp NUMBER;
    pathSetIdTemp NUMBER;
    pathIdTemp NUMBER;
  slNoTemp NUMBER;
  movementType VARCHAR(2);
  recno NUMBER DEFAULT 0;
  orderShipmentSeq INTEGER DEFAULT 0;

BEGIN
  idArr := csv_to_INT_ARRAY(idListCsv);
  idAddedArr := INT_ARRAY();
  FOR i IN idArr.FIRST..idArr.LAST LOOP
    shipmentId := idArr(i);
    -- Do not add if already in workspace.
      SELECT COUNT(orig_shipment_id) INTO bFound FROM
           CWS_SHIPMENT WHERE cons_workspace_id=cwsId AND orig_shipment_id=idArr(i);
    IF bFound=0 THEN
       -- Determine if shipment is a movement or not.  If a movement, we'll just copy the movement records.
       -- If not a movement, we create movement records corresponding to the order(s)-shipment relationship.
       --select count(order_id) into bIsNonMovementWithOrders from
               --  orders where (orders.shipment_id=idArr(i) and orders.shipment_id is not null);
       SELECT COUNT(1) INTO bIsMovement FROM
                 ORDER_MOVEMENT WHERE shipment_id=idArr(i);
       IF (bIsMovement > 0) THEN
             bIsNonMovementWithOrders := 0;
       ELSE
             bIsNonMovementWithOrders := 1;
       END IF;
       -- Generate unique WS shipment ID
       SELECT seq_ws_shipment_id.NEXTVAL INTO wsShipmentId FROM dual;
   -- Insert the shipment record
           INSERT INTO CWS_SHIPMENT
               (
               CONS_WORKSPACE_ID,
               WS_SHIPMENT_ID,
               TC_COMPANY_ID,
               ORIG_SHIPMENT_ID,
               TC_SHIPMENT_ID,
               LAST_REFRESHED_DTTM,
               LAST_UPDATED_DTTM,
               NEEDS_REFRESH,
               SHIPMENT_COMPS_ENABLED,
               IS_HAZMAT,
               IS_PERISHABLE,
               IS_FILO,
               IS_COOLER_AT_NOSE,
               PROMOTE_TO_AVAIL_FLAG,
               HAS_HARD_CHECK_ERROR,
               HAS_SOFT_CHECK_ERROR,
               O_ADDRESS,
               O_CITY,
               O_STATE_PROV,
               O_POSTAL_CODE,
               O_COUNTY,
               O_COUNTRY_CODE,
               D_ADDRESS,
               D_CITY,
               D_STATE_PROV,
               D_POSTAL_CODE,
               D_COUNTY,
               D_COUNTRY_CODE,
               FEASIBLE_MOT_ID,
               FEASIBLE_EQUIPMENT_ID,
               FEASIBLE_SERVICE_LEVEL_ID,
               FEASIBLE_CARRIER_ID,
               FEASIBLE_EQUIPMENT2_ID,
               FEASIBLE_DRIVER_TYPE,
               WAVE_ID,
               IS_WAVE_MAN_CHANGED,
               DSG_EQUIPMENT2_ID,
               DSG_DRIVER_TYPE,
               SAVE_TRACT_AS_DSG_FLAG,
               SAVE_DT_AS_DSG_FLAG,
               DELIVERY_REQ,
               DROPOFF_PICKUP,
               PACKAGING,
               SAVE_MOT_AS_DSG_FLAG,
               SAVE_EQUIP_AS_DSG_FLAG,
               SAVE_SL_AS_DSG_FLAG,
               SAVE_CC_AS_DSG_FLAG,
               PRODUCT_CLASS_ID,
               TRANS_RESP_CODE,
               PROTECTION_LEVEL_ID,
               BILLING_METHOD,
               --BUSINESS_UNIT,
               COMMODITY_CLASS,
               DSG_MOT_ID,
               DSG_CARRIER_ID,
               DSG_EQUIPMENT_ID,
               DSG_SERVICE_LEVEL_ID,
               TOTAL_DISTANCE,
               DIRECT_DISTANCE,
               OUT_OF_ROUTE_DISTANCE,
               DISTANCE_UOM,
               SIZE1_VALUE,
               SIZE1_UOM_ID,
               SIZE2_VALUE,
               SIZE2_UOM_ID,
               -- CT18
               SIZE3_VALUE,
               SIZE3_UOM_ID,
               SIZE4_VALUE,
               SIZE4_UOM_ID,
               NUM_STOPS,
               PICKUP_START_DTTM,
               PICKUP_END_DTTM,
               PICKUP_TZ,
               DELIVERY_START_DTTM,
               DELIVERY_END_DTTM,
               DELIVERY_TZ,
               INITIAL_COST,
               BASELINE_COST,
               ESTIMATED_COST,
               XDOCK_SORT_COST,
               NEEDS_VALIDATION,
               MOVE_TYPE,
               ASG_MOT_ID,
               BOOKING_ID,
               IS_TIME_FEAS_ENABLED,
               STATIC_ROUTE_ID,
        IS_LOADING_SEQ_MODIFIED
               )
           SELECT
               cwsId,
               wsShipmentId,
               SHIPMENT.TC_COMPANY_ID,
               SHIPMENT.SHIPMENT_ID,
               SHIPMENT.TC_SHIPMENT_ID,
               SYSDATE,
               SHIPMENT.LAST_UPDATED_DTTM,
               0,
               1,
               SHIPMENT.IS_HAZMAT,
               SHIPMENT.IS_PERISHABLE,
           SHIPMENT.IS_FILO,
                 SHIPMENT.IS_COOLER_AT_NOSE,
               DECODE(SHIPMENT.SHIPMENT_STATUS,10,0,20,1,2),
               SHIPMENT.HAS_IMPORT_ERROR,
               SHIPMENT.HAS_SOFT_CHECK_ERROR,
               SHIPMENT.O_ADDRESS,
               SHIPMENT.O_CITY,
               SHIPMENT.O_STATE_PROV,
               SHIPMENT.O_POSTAL_CODE,
               SHIPMENT.O_COUNTY,
               SHIPMENT.O_COUNTRY_CODE,
               SHIPMENT.D_ADDRESS,
               SHIPMENT.D_CITY,
               SHIPMENT.D_STATE_PROV,
               SHIPMENT.D_POSTAL_CODE,
               SHIPMENT.D_COUNTY,
               SHIPMENT.D_COUNTRY_CODE,
               SHIPMENT.FEASIBLE_MOT_ID,
               SHIPMENT.FEASIBLE_EQUIPMENT_ID,
               SHIPMENT.FEASIBLE_SERVICE_LEVEL_ID,
               SHIPMENT.FEASIBLE_CARRIER_ID,
               SHIPMENT.FEASIBLE_EQUIPMENT2_ID,
               SHIPMENT.FEASIBLE_DRIVER_TYPE,
               SHIPMENT.WAVE_ID,
               SHIPMENT.IS_WAVE_MAN_CHANGED,
               SHIPMENT.DESIGNATED_TRACTOR_CODE,
               SHIPMENT.DESIGNATED_DRIVER_TYPE,
               0,0,
               SHIPMENT.DELIVERY_REQ,
               SHIPMENT.DROPOFF_PICKUP,
               SHIPMENT.PACKAGING,
               0,
               0,
               0,
               0,
               SHIPMENT.PRODUCT_CLASS_ID,
               SHIPMENT.TRANS_RESP_CODE,
               SHIPMENT.PROTECTION_LEVEL_ID,
               SHIPMENT.BILLING_METHOD,
               -- shipment.BUSINESS_UNIT,
               SHIPMENT.COMMODITY_CLASS,
               SHIPMENT.DSG_MOT_ID,
               SHIPMENT.DSG_CARRIER_ID,
               SHIPMENT.DSG_EQUIPMENT_ID,
               SHIPMENT.DSG_SERVICE_LEVEL_ID,
               SHIPMENT.DISTANCE,
               SHIPMENT.DIRECT_DISTANCE,
               SHIPMENT.OUT_OF_ROUTE_DISTANCE,
               SHIPMENT.DISTANCE_UOM,
               SUM(SHIPMENT_COMMODITY.size_value*DECODE(SIZE_UOM_ID,size1UOM,1,0)),
               size1UOM,
               SUM(SHIPMENT_COMMODITY.size_value*DECODE(SIZE_UOM_ID,size2UOM,1,0)),
               size2UOM,
               -- CT18
               SUM(SHIPMENT_COMMODITY.size_value*DECODE(SIZE_UOM_ID,size3UOM,1,0)),
	       size3UOM,
	       SUM(SHIPMENT_COMMODITY.size_value*DECODE(SIZE_UOM_ID,size4UOM,1,0)),
               size4UOM,
               DECODE(SHIPMENT.NUM_STOPS, NULL, 0, 0, 0, SHIPMENT.NUM_STOPS+1),
               SHIPMENT.PICKUP_START_DTTM,
               SHIPMENT.PICKUP_END_DTTM,
               SHIPMENT.PICKUP_TZ,
               SHIPMENT.DELIVERY_START_DTTM,
               SHIPMENT.DELIVERY_END_DTTM,
               SHIPMENT.DELIVERY_TZ,
               SHIPMENT.ESTIMATED_COST,
               SHIPMENT.NORMALIZED_BASELINE_COST,
               SHIPMENT.ESTIMATED_COST,
               NULL, -- TODO - need to compute the xdock sort here???  or maybe CWS does this and remove this column?  think.
               0,
               SHIPMENT.MOVE_TYPE,
               SHIPMENT.ASSIGNED_MOT_ID,
               (SELECT booking_id FROM BOOKING_SHIPMENT bs WHERE bs.shipment_id=SHIPMENT.shipment_id) BOOKING_ID,
               SHIPMENT.IS_TIME_FEAS_ENABLED,
               SHIPMENT.STATIC_ROUTE_ID,
        SHIPMENT_EXTN_TLM.IS_LOADING_SEQ_MANUALLY_EDITED
           FROM SHIPMENT,
               SHIPMENT_COMMODITY,
               SHIPMENT_EXTN_TLM
           WHERE SHIPMENT_COMMODITY.shipment_id(+)=SHIPMENT.shipment_id
               AND SHIPMENT.shipment_id=idArr(i)
               AND SHIPMENT_EXTN_TLM.shipment_id(+)=SHIPMENT.shipment_id
           GROUP BY SHIPMENT.TC_COMPANY_ID, SHIPMENT.SHIPMENT_ID,
               SHIPMENT.TC_SHIPMENT_ID,
               SHIPMENT.LAST_UPDATED_DTTM,
               SHIPMENT.IS_HAZMAT,
               SHIPMENT.IS_PERISHABLE,
           SHIPMENT.IS_FILO,
           SHIPMENT.IS_COOLER_AT_NOSE,
               SHIPMENT.SHIPMENT_STATUS,
               SHIPMENT.HAS_IMPORT_ERROR,
               SHIPMENT.HAS_SOFT_CHECK_ERROR,
               SHIPMENT.O_ADDRESS,
               SHIPMENT.O_CITY,
               SHIPMENT.O_STATE_PROV,
               SHIPMENT.O_POSTAL_CODE,
               SHIPMENT.O_COUNTY,
               SHIPMENT.O_COUNTRY_CODE,
               SHIPMENT.D_ADDRESS,
               SHIPMENT.D_CITY,
               SHIPMENT.D_STATE_PROV,
               SHIPMENT.D_POSTAL_CODE,
               SHIPMENT.D_COUNTY,
               SHIPMENT.D_COUNTRY_CODE,
               SHIPMENT.FEASIBLE_MOT_ID,
               SHIPMENT.FEASIBLE_EQUIPMENT_ID,
               SHIPMENT.FEASIBLE_SERVICE_LEVEL_ID,
               SHIPMENT.FEASIBLE_CARRIER_ID,
               SHIPMENT.FEASIBLE_EQUIPMENT2_ID,
               SHIPMENT.FEASIBLE_DRIVER_TYPE,
               SHIPMENT.WAVE_ID,
               SHIPMENT.IS_WAVE_MAN_CHANGED,
               SHIPMENT.DESIGNATED_TRACTOR_CODE,
               SHIPMENT.DESIGNATED_DRIVER_TYPE,
               SHIPMENT.DELIVERY_REQ,
               SHIPMENT.DROPOFF_PICKUP,
               SHIPMENT.PACKAGING,
               SHIPMENT.PRODUCT_CLASS_ID,
               SHIPMENT.TRANS_RESP_CODE,
               SHIPMENT.PROTECTION_LEVEL_ID,
               SHIPMENT.BILLING_METHOD,
             --  shipment.BUSINESS_UNIT,
               SHIPMENT.COMMODITY_CLASS,
               SHIPMENT.DSG_MOT_ID,
               SHIPMENT.DSG_CARRIER_ID,
               SHIPMENT.DSG_EQUIPMENT_ID,
               SHIPMENT.DSG_SERVICE_LEVEL_ID,
               SHIPMENT.DISTANCE,
               SHIPMENT.DIRECT_DISTANCE,
               SHIPMENT.OUT_OF_ROUTE_DISTANCE,
               SHIPMENT.DISTANCE_UOM,
               SHIPMENT.NUM_STOPS,
               SHIPMENT.PICKUP_START_DTTM,
               SHIPMENT.PICKUP_END_DTTM,
               SHIPMENT.PICKUP_TZ,
               SHIPMENT.DELIVERY_START_DTTM,
               SHIPMENT.DELIVERY_END_DTTM,
               SHIPMENT.DELIVERY_TZ,
               SHIPMENT.ESTIMATED_COST,
               SHIPMENT.NORMALIZED_BASELINE_COST,
               SHIPMENT.ESTIMATED_COST,
               MOVE_TYPE,
               SHIPMENT.ASSIGNED_MOT_ID,
               SHIPMENT.BOOKING_REF_SHIPPER,
               SHIPMENT.IS_TIME_FEAS_ENABLED,
               SHIPMENT.STATIC_ROUTE_ID,
        SHIPMENT_EXTN_TLM.IS_LOADING_SEQ_MANUALLY_EDITED;

         idAddedArr.EXTEND;
         idAddedArr(idAddedArr.LAST) := idArr(i);

		 -- Insert Virtual Trip tables
		 INSERT INTO CWS_ORIGINAL_VIRTUAL_TRIP
               (
               CONS_WORKSPACE_ID,
               VIRTUAL_TRIP_ID,
               VIRTUAL_TRIP_SEQUENCE,
               WS_SHIPMENT_ID,
               DEAD_HEAD_DISTANCE,
			   DISTANCE_UOM,
               DEAD_HEAD_TRANSIT_TIME,
               IS_TANDEM
               )
           SELECT
               cwsId,
               VIRTUAL_TRIP_ID,
               VIRTUAL_TRIP_SEQUENCE,
               wsShipmentId,
               DEAD_HEAD_DISTANCE,
			   DISTANCE_UOM,
               DEAD_HEAD_TRANSIT_TIME,
               IS_TANDEM
           FROM VIRTUAL_TRIP WHERE  SHIPMENT_ID=idArr(i);


           INSERT INTO CWS_VIRTUAL_TRIP
               (
               CONS_WORKSPACE_ID,
               VIRTUAL_TRIP_ID,
               VIRTUAL_TRIP_SEQUENCE,
               WS_SHIPMENT_ID,
               DEAD_HEAD_DISTANCE,
			   DISTANCE_UOM,
               DEAD_HEAD_TRANSIT_TIME,
               IS_TANDEM
               )
           SELECT
               cwsId,
               VIRTUAL_TRIP_ID,
               VIRTUAL_TRIP_SEQUENCE,
               wsShipmentId,
               DEAD_HEAD_DISTANCE,
			   DISTANCE_UOM,
               DEAD_HEAD_TRANSIT_TIME,
               IS_TANDEM

           FROM VIRTUAL_TRIP WHERE  SHIPMENT_ID=idArr(i);

           -- Insert the shipments' stops.

         INSERT INTO CWS_STOP
           (CONS_WORKSPACE_ID,WS_SHIPMENT_ID,STOP_SEQ,
            STOP_COMPS_ENABLED,TIME_WINDOW_COMPS_ENABLED,
            STOP_LOCATION_NAME,FACILITY_ID,FACILITY_ALIAS_ID,DOCK_ID,ADDRESS,ADDRESS_2,ADDRESS_3,CITY,STATE_PROV,POSTAL_CODE,COUNTY,COUNTRY_CODE,
            HANDLER,DROP_HOOK_INDICATOR,IS_APPT_REQD,STOP_TZ,
            ARRIVAL_START_DTTM,ARRIVAL_END_DTTM,DEPARTURE_START_DTTM,DEPARTURE_END_DTTM,
            CONTACT_FIRST_NAME,CONTACT_SURNAME,CONTACT_PHONE_NUMBER,
            DISTANCE,DISTANCE_UOM,size_uom_id, capacity,wave_id, wave_option_list_id,wave_option_id, IS_WAVE_MAN_CHANGED,SEGMENT_ID,SEGMENT_STOP_SEQ,
     STOP_TIME_MODIFIED, ORDER_WINDOW_VIOLATION, DOCK_HOUR_VIOLATION, WAVE_OPTION_VIOLATION,STOP_ACTION,HITCH_TIME,ON_STATIC_ROUTE)
          SELECT cwsId, wsShipmentId, STOP.STOP_SEQ,
                   1, 1,
                 STOP.STOP_LOCATION_NAME, STOP.FACILITY_ID, STOP.FACILITY_ALIAS_ID, STOP_ACTION.DOCK_ID,STOP.ADDRESS_1,STOP.ADDRESS_2,STOP.ADDRESS_3, STOP.CITY, STOP.STATE_PROV, STOP.POSTAL_CODE, STOP.COUNTY, STOP.COUNTRY_CODE,
                 STOP.HANDLER, STOP.DROP_HOOK_INDICATOR, STOP.IS_APPT_REQD, STOP.STOP_TZ,
                 STOP.ARRIVAL_START_DTTM, STOP.ARRIVAL_END_DTTM, STOP.DEPARTURE_START_DTTM, STOP.DEPARTURE_END_DTTM,
                 STOP.CONTACT_FIRST_NAME, STOP.CONTACT_SURNAME, STOP.CONTACT_PHONE_NUMBER,
                 STOP.DISTANCE, STOP.DISTANCE_UOM, STOP.size_uom_id, STOP.capacity, STOP.wave_id, STOP.wave_option_list_id, STOP.wave_option_id,
   STOP.IS_WAVE_MAN_CHANGED,STOP.SEGMENT_ID,STOP.SEGMENT_STOP_SEQ,
   STOP.STOP_TIME_MODIFIED, TPE_STOP_EXTN.ORDER_WINDOW_VIOLATION, TPE_STOP_EXTN.DOCK_HOUR_VIOLATION,  TPE_STOP_EXTN.WAVE_OPTION_VIOLATION, STOP_ACTION.Action_Type,STOP.HITCH_TIME,STOP.ON_STATIC_ROUTE
          FROM  STOP_ACTION, STOP
   LEFT JOIN TPE_STOP_EXTN
   ON TPE_STOP_EXTN.SHIPMENT_ID = STOP.SHIPMENT_ID AND TPE_STOP_EXTN.STOP_SEQ = STOP.STOP_SEQ
          WHERE STOP.SHIPMENT_ID=idArr(i)
          AND STOP_ACTION.shipment_id=STOP.shipment_id AND STOP_ACTION.STOP_SEQ=STOP.STOP_SEQ AND STOP_ACTION.STOP_ACTION_SEQ=1;
         -- Insert the shipments' orders - if they are not already in the workspace.
         -- Insert the movement records (either create them on the fly or copy them if they exist).  We create
         -- the same records in the original and current ws movement tables, except for the shipment id.
         IF bIsNonMovementWithOrders>0 THEN
             -- Orders
         SELECT order_id, order_split_id, path_id,
                path_set_id, compartment_no, order_loading_seq
         BULK COLLECT INTO orderidonshipment, ordersplitidarr,
               pathidarr, pathsetidarr,compartmentnoonorder, loadingseqarr
           FROM (SELECT ORDERS.order_id, order_split_id, path_id, path_set_id,
                        ORDERS.compartment_no, ORDERS.order_loading_seq
                   FROM ORDERS LEFT OUTER JOIN ORDER_SPLIT ON ORDERS.order_id = ORDER_SPLIT.order_id
                  WHERE ORDERS.shipment_id = idarr (i)
                 UNION
                 SELECT ORDERS.order_id, order_split_id, path_id, path_set_id,
                        ORDERS.compartment_no, ORDERS.order_loading_seq
                   FROM ORDERS LEFT OUTER JOIN ORDER_SPLIT ON ORDERS.order_id = ORDER_SPLIT.order_id
                  WHERE ORDER_SPLIT.shipment_id = idarr (i));
          IF (orderIdOnShipment.COUNT > 0) THEN
            FOR j IN orderIdOnShipment.FIRST..orderIdOnShipment.LAST LOOP
         --CR69340
               bAdded := p_add_Order_To_Unassigned(orderIdOnShipment(j), 0, shipperId, cwsId, size1UOM, size2UOM,size3UOM,size4UOM); -- CT18
         IF(orderSplitIdArr(j) IS NOT NULL) THEN
                  --CR69340
                  bAdded := p_add_Order_To_Unassigned(orderSplitIdArr(j), 1, shipperId, cwsId, size1UOM, size2UOM,size3UOM,size4UOM); -- CT18
                  UPDATE CWS_ORDER CWSORD SET CWSORD.has_split = 1, CWSORD.is_visible =0
                  WHERE CWSORD.order_id = orderIdOnShipment(j) AND CWSORD.cons_workspace_id=cwsId;
         END IF;
            END LOOP;
            END IF;
            -- Movements - we synthesize basic movement records with pickup/deliver stops since none actually exist
            -- We do this by reading the stop and related tables.

-- modified for ASP Issue CR 78998. Ashwini
    compartmentNo := NULL;
    pathSetId := NULL;
    pathId := NULL;

    FOR stop_seq_cursor IN (SELECT STOP.stop_seq stop_seq,
                       DECODE(STOP_ACTION.ACTION_TYPE,'PA','PU','DA','DL','PU','PU','DL','DL', 'RD', 'RD', 'RP', 'RP') STOP_ACTION,
                       STOP_ACTION_ORDER.ORDER_ID ORDER_ID,
                       STOP_ACTION_ORDER.ORDER_SPLIT_ID ORDER_SPLIT_ID
                FROM STOP,STOP_ACTION,STOP_ACTION_ORDER
                WHERE STOP.shipment_id=shipmentId
                AND STOP_ACTION.shipment_id=STOP.shipment_id
                AND STOP_ACTION.stop_seq=STOP.stop_seq
                AND STOP_ACTION_ORDER.shipment_id=STOP.shipment_id
                AND STOP_ACTION_ORDER.stop_seq=STOP.stop_seq
                AND STOP_ACTION_ORDER.stop_action_seq=1
                ORDER BY order_id, order_split_id, stop_seq)
              LOOP

    recno := recno+1;
       IF MOD(recno,2) = 1
       THEN
          stopSeq1 := stop_seq_cursor.stop_seq;
          orderid1 := stop_seq_cursor.order_id;
          orderSplitid1 := COALESCE(stop_seq_cursor.order_split_id,-1);

          IF (orderIdOnShipment.COUNT > 0)
          THEN
                    FOR j IN orderIdOnShipment.FIRST..orderIdOnShipment.LAST
                    LOOP
                         EXIT WHEN compartmentNo IS NOT NULL AND pathSetId IS NOT NULL AND pathId IS NOT NULL;
                         IF( orderId1 = orderIdOnShipment(j) )
                         THEN
                              compartmentNo := compartmentNoOnOrder(j);
                              pathSetId := pathSetIdArr(j);
                              pathId := pathIdArr(j);
                         END IF;
                    END LOOP;
                END IF;

       ELSE
          stopSeq2 := stop_seq_cursor.stop_seq;
          orderid2 := stop_seq_cursor.order_id;
          orderSplitid1 := stop_seq_cursor.order_split_id;
		  IF (stop_seq_cursor.STOP_ACTION = 'RD' OR stop_seq_cursor.STOP_ACTION = 'RP')
          THEN -- CR# 54375
            isRelay := 1;
		  END IF;
	  IF (pathId IS NOT NULL AND pathSetId IS NOT NULL) THEN -- [CR 28801 fix]
               movementType := 'WW';
	       -- retrieve order shipment sequence from db.
	       SELECT MIN(PATH_WAYPOINT.PATH_WAYPOINT_SEQ) into orderShipmentSeq
			FROM PATH_WAYPOINT, ORDERS
			WHERE ORDERS.ORDER_ID = orderId1
			AND PATH_WAYPOINT.PATH_ID = ORDERS.PATH_ID
			AND PATH_WAYPOINT.PATH_SET_ID = ORDERS.PATH_SET_ID
			AND PATH_WAYPOINT.FACILITY_ID IN (
			    SELECT STOP.FACILITY_ID
					FROM STOP_ACTION_ORDER
						JOIN STOP ON STOP_ACTION_ORDER.SHIPMENT_ID = STOP.SHIPMENT_ID
							AND  STOP_ACTION_ORDER.STOP_SEQ = STOP.STOP_SEQ
						JOIN STOP_ACTION ON STOP_ACTION_ORDER.SHIPMENT_ID = STOP_ACTION.SHIPMENT_ID
							AND  STOP_ACTION.STOP_SEQ = STOP_ACTION_ORDER.STOP_SEQ
					WHERE STOP_ACTION_ORDER.ORDER_ID = ORDERS.ORDER_ID
						AND STOP_ACTION_ORDER.SHIPMENT_ID = ORDERS.SHIPMENT_ID
						AND STOP_ACTION.ACTION_TYPE IN ('PU', 'PA', 'RP'));
		IF orderShipmentSeq IS NULL
			THEN orderShipmentSeq := 0;
		END IF;
           ELSE
               movementType := 'OD';
           END IF;
            INSERT INTO CWS_ORIGINAL_MOVEMENT
             (CWS_ORIGINAL_MOVEMENT_ID, CONS_WORKSPACE_ID, ORDER_ID, ORIG_SHIPMENT_ID, MOVEMENT_TYPE, ORDER_SHIPMENT_SEQ,
             TC_COMPANY_ID, PATH_SET_ID, PATH_ID, PICKUP_STOP_SEQ, DELIVERY_STOP_SEQ, ORDER_LOADING_SEQ, COMPARTMENT_NO, ORDER_SPLIT_ID, IS_RELAY)
             VALUES
             (SEQ_CWS_ORIGINAL_MOVEMENT_ID.NEXTVAL, cwsId, orderId1, shipmentId, 'OD', orderShipmentSeq,
             shipperId, pathSetId, pathId, stopSeq1, stopSeq2, loadingSeq, compartmentNoTemp, orderSplitid1, isRelay);
          INSERT INTO CWS_ORDER_MOVEMENT
          (CWS_ORDER_MOVEMENT_ID,CONS_WORKSPACE_ID, ORDER_ID, WS_SHIPMENT_ID, MOVEMENT_TYPE, ORDER_SHIPMENT_SEQ,
          TC_COMPANY_ID, PATH_SET_ID, PATH_ID, PICKUP_STOP_SEQ, DELIVERY_STOP_SEQ, ORDER_LOADING_SEQ, COMPARTMENT_NO, ORDER_SPLIT_ID, IS_RELAY )
          VALUES
          ( SEQ_CWS_ORDER_MOVEMENT_ID.NEXTVAL,cwsId, orderId1, wsShipmentId, movementType, orderShipmentSeq,
          shipperId, pathSetId, pathId, stopSeq1, stopSeq2, loadingSeq, compartmentNoTemp, orderSplitid1, isRelay);

         END IF;
    END LOOP;
    END IF;

         -- This is a real movement, just copy the existing movement records.
         IF bIsMovement>0 THEN
         --Orders that are on just this shipment
         SELECT order_id, order_split_id, path_id,
                path_set_id, compartment_no, order_loading_seq
         BULK COLLECT INTO orderidonshipment, ordersplitidarr,
               pathidarr, pathsetidarr,compartmentnoonorder, loadingseqarr
           FROM (SELECT ORDERS.order_id, order_split_id, path_id, path_set_id,
                        ORDERS.compartment_no, ORDERS.order_loading_seq
               FROM ORDERS LEFT OUTER JOIN ORDER_SPLIT ON ORDERS.order_id = ORDER_SPLIT.order_id
                  WHERE ORDERS.shipment_id = idarr (i)
            UNION
                 SELECT ORDERS.order_id, order_split_id, path_id, path_set_id,
                        ORDERS.compartment_no, ORDERS.order_loading_seq
               FROM ORDERS LEFT OUTER JOIN ORDER_SPLIT ON ORDERS.order_id = ORDER_SPLIT.order_id
                  WHERE ORDER_SPLIT.shipment_id = idarr (i));

            IF (orderIdOnShipment.COUNT > 0) THEN
            FOR j1 IN orderIdOnShipment.FIRST..orderIdOnShipment.LAST LOOP
      --CR69340
               bAdded := p_add_Order_To_Unassigned(orderIdOnShipment(j1), 0, shipperId, cwsId, size1UOM, size2UOM,size3UOM,size4UOM); -- CT18
            -- Movements - we synthesize basic movement records with pickup/deliver stops since none actually exist
            -- We do this by reading the stop and related tables.
         IF(orderSplitIdArr(j1) IS NOT NULL ) THEN
              --CR69340
              bAdded := p_add_Order_To_Unassigned(orderSplitIdArr(j1), 1, shipperId, cwsId, size1UOM, size2UOM, size3UOM, size4UOM); -- CT18
              UPDATE CWS_ORDER CWSORD SET CWSORD.has_split = 1, CWSORD.is_visible =0
                        WHERE CWSORD.order_id = orderIdOnShipment(j1) AND CWSORD.cons_workspace_id=cwsId;
         END IF ;
            OPEN stop_seq_cursor FOR
                SELECT STOP.stop_seq,
                       DECODE(STOP_ACTION.ACTION_TYPE,'PA','PU','DA','DL','PU','PU','DL','DL') STOP_ACTION,
                       STOP_ACTION_ORDER.ORDER_ID,
             STOP_ACTION_ORDER.ORDER_SPLIT_ID
                FROM STOP,STOP_ACTION,STOP_ACTION_ORDER
                WHERE STOP.shipment_id=shipmentId
                AND STOP_ACTION.shipment_id=STOP.shipment_id
                AND STOP_ACTION.stop_seq=STOP.stop_seq
                AND STOP_ACTION_ORDER.shipment_id=STOP.shipment_id
                AND STOP_ACTION_ORDER.stop_seq=STOP.stop_seq
                AND STOP_ACTION_ORDER.stop_action_seq=1
                AND STOP_ACTION_ORDER.order_id=orderIdOnShipment(j1)
				AND (STOP_ACTION_ORDER.ORDER_SPLIT_ID IS NULL OR STOP_ACTION_ORDER.ORDER_SPLIT_ID=ordersplitidarr(j1))
                ORDER BY order_id,stop_seq;
            <<c2_cursor_loop>>
            LOOP
               EXIT WHEN stop_seq_cursor%NOTFOUND;
                  FETCH stop_seq_cursor INTO stopSeq1, stopAction1, orderId1, orderSplitId1;
               EXIT WHEN stop_seq_cursor%NOTFOUND;
                  FETCH stop_seq_cursor INTO stopSeq2, stopAction2, orderId2, orderSplitId2;

               IF (orderIdOnShipment.COUNT > 0) THEN
                FOR j IN orderIdOnShipment.FIRST..orderIdOnShipment.LAST
                    LOOP
                         EXIT WHEN compartmentNo IS NOT NULL AND pathSetId IS NOT NULL AND pathId IS NOT NULL;
                     IF( orderId1 = orderIdOnShipment(j) )
                     THEN
                          compartmentNo := compartmentNoOnOrder(j);
                          pathSetId := pathSetIdArr(j);
                          pathId := pathIdArr(j);
                         END IF;
                    END LOOP;
                END IF;

               INSERT INTO CWS_ORIGINAL_MOVEMENT
                       (CWS_ORIGINAL_MOVEMENT_ID, CONS_WORKSPACE_ID, ORDER_ID, ORIG_SHIPMENT_ID, MOVEMENT_TYPE, ORDER_SHIPMENT_SEQ,
                        TC_COMPANY_ID, PATH_SET_ID, PATH_ID, PICKUP_STOP_SEQ, DELIVERY_STOP_SEQ, ORDER_SPLIT_ID )
                       VALUES
                       (SEQ_CWS_ORIGINAL_MOVEMENT_ID.NEXTVAL, cwsId, orderId1, shipmentId, 'OD', 0,
                        shipperId, NULL, NULL, stopSeq1, stopSeq2, orderSplitId1);

               IF (pathId IS NOT NULL AND pathSetId IS NOT NULL) THEN -- [CR 28801 fix]
                       movementType := 'WW';
                   ELSE
                       movementType := 'OD';
                   END IF;

               INSERT INTO CWS_ORDER_MOVEMENT
                       ( CWS_ORDER_MOVEMENT_ID,CONS_WORKSPACE_ID, ORDER_ID, WS_SHIPMENT_ID, MOVEMENT_TYPE, ORDER_SHIPMENT_SEQ,
                        TC_COMPANY_ID, PATH_SET_ID, PATH_ID, PICKUP_STOP_SEQ, DELIVERY_STOP_SEQ, ORDER_SPLIT_ID )
                       VALUES
                       (SEQ_CWS_ORDER_MOVEMENT_ID.NEXTVAL,cwsId, orderId1, wsShipmentId, movementType, 0,
                        shipperId, pathSetId, pathId, stopSeq1, stopSeq2, orderSplitId1);
            END LOOP c2_cursor_loop;
            CLOSE stop_seq_cursor;
            END LOOP;
            END IF;
            -- Orders that are on other shipments also
             SELECT order_id, order_split_id BULK COLLECT INTO orderIdOnShipment, orderSplitIdArr
                   FROM ORDER_MOVEMENT WHERE shipment_id=idArr(i);
            FOR j IN orderIdOnShipment.FIRST..orderIdOnShipment.LAST LOOP
         --CR69340
               bAdded := p_add_Order_To_Unassigned(orderIdOnShipment(j), 0, shipperId, cwsId, size1UOM, size2UOM, size3UOM, size4UOM); -- CT18
         IF(orderSplitIdArr(j) IS NOT NULL) THEN
            --CR69340
            bAdded := p_add_Order_To_Unassigned(orderSplitIdArr(j), 1, shipperId, cwsId, size1UOM, size2UOM,size3UOM, size4UOM); -- CT18
            UPDATE CWS_ORDER CWSORD SET CWSORD.has_split = 1, CWSORD.is_visible =0
                        WHERE CWSORD.order_id = orderIdOnShipment(j) AND CWSORD.cons_workspace_id=cwsId;
         END IF;
            END LOOP;
            -- Original Movement
            INSERT INTO CWS_ORIGINAL_MOVEMENT
                   (CWS_ORIGINAL_MOVEMENT_ID, CONS_WORKSPACE_ID, ORDER_ID, ORIG_SHIPMENT_ID, MOVEMENT_TYPE, ORDER_SHIPMENT_SEQ,
                    TC_COMPANY_ID, PATH_SET_ID, PATH_ID, PICKUP_STOP_SEQ, DELIVERY_STOP_SEQ, ORDER_LOADING_SEQ, IS_RELAY, COMPARTMENT_NO, ORDER_SPLIT_ID )
                   SELECT SEQ_CWS_ORIGINAL_MOVEMENT_ID.NEXTVAL, cwsId, ORDER_MOVEMENT.ORDER_ID, ORDER_MOVEMENT.SHIPMENT_ID, ORDER_MOVEMENT.MOVEMENT_TYPE, ORDER_MOVEMENT.ORDER_SHIPMENT_SEQ,
                             ORDER_MOVEMENT.TC_COMPANY_ID, ORDER_MOVEMENT.PATH_SET_ID, ORDER_MOVEMENT.PATH_ID, ORDER_MOVEMENT.PICKUP_STOP_SEQ, ORDER_MOVEMENT.DELIVERY_STOP_SEQ,
                          ORDER_MOVEMENT.ORDER_LOADING_SEQ, ORDER_MOVEMENT.IS_RELAY, ORDER_MOVEMENT.COMPARTMENT_NO, ORDER_MOVEMENT.ORDER_SPLIT_ID
                   FROM ORDER_MOVEMENT
                   WHERE ORDER_MOVEMENT.SHIPMENT_ID=idArr(i);
            -- Current Movement (same as original but for the WS shipment instead of real shipment in 3rd column).
            INSERT INTO CWS_ORDER_MOVEMENT
                   (CWS_ORDER_MOVEMENT_ID,CONS_WORKSPACE_ID, ORDER_ID, WS_SHIPMENT_ID, MOVEMENT_TYPE, ORDER_SHIPMENT_SEQ,
                    TC_COMPANY_ID, PATH_SET_ID, PATH_ID, PICKUP_STOP_SEQ, DELIVERY_STOP_SEQ, ORDER_LOADING_SEQ, IS_RELAY, COMPARTMENT_NO, ORDER_SPLIT_ID )
                   SELECT SEQ_CWS_ORDER_MOVEMENT_ID.NEXTVAL,cwsId, ORDER_MOVEMENT.ORDER_ID, wsShipmentId, ORDER_MOVEMENT.MOVEMENT_TYPE, ORDER_MOVEMENT.ORDER_SHIPMENT_SEQ,
                             ORDER_MOVEMENT.TC_COMPANY_ID, ORDER_MOVEMENT.PATH_SET_ID, ORDER_MOVEMENT.PATH_ID, ORDER_MOVEMENT.PICKUP_STOP_SEQ, ORDER_MOVEMENT.DELIVERY_STOP_SEQ,
                          ORDER_MOVEMENT.ORDER_LOADING_SEQ, ORDER_MOVEMENT.IS_RELAY, ORDER_MOVEMENT.COMPARTMENT_NO, ORDER_MOVEMENT.ORDER_SPLIT_ID
                   FROM ORDER_MOVEMENT
                   WHERE ORDER_MOVEMENT.SHIPMENT_ID=idArr(i);
         END IF;
    END IF;
  END LOOP;

  shipmentsAddedCsv := int_array_to_csv(idAddedArr);
END add_Shipments_To_Workspace;

-- Cancel the workspace by removing all entries in all tables.
--  22/06/2004    RAM        Modified for PE enahancements  SR1 , 4R1 change, build92,  TT32740 [BEGIN]
PROCEDURE cancel_Workspace(
     shipperId INTEGER,
     cwsId INTEGER,
     cwsProcessId INTEGER,
     isFromCancel INTEGER) IS
bIsAggregated NUMBER(3);
orderId INT_ARRAY;
oldOrderParentId INT_ARRAY;
processId INTEGER := 0;
BEGIN

  IF (cwsProcessId > 0 ) THEN
    processId := cwsProcessId;
  END IF;

  DELETE FROM CWS_ORIGINAL_MOVEMENT WHERE cons_workspace_id = cwsId AND order_id IN (SELECT order_id FROM CWS_ORDER WHERE cws_process_id =processId);
  DELETE FROM CWS_ORDER_MOVEMENT WHERE cons_workspace_id = cwsId AND order_id IN (SELECT order_id FROM CWS_ORDER WHERE cws_process_id =processId);

  /*This has been added to handle aggregated orders in manual consolidation workspace    */
  DELETE CANCEL_WORKSPACE_ORDER_ID_GTT;
  INSERT INTO CANCEL_WORKSPACE_ORDER_ID_GTT(order_id,cws_process_id) (SELECT order_id,cws_process_id FROM CWS_ORDER WHERE cons_workspace_id = cwsId AND is_aggregated_in_cws = 1 AND cws_process_id=processId);

  SELECT COUNT(1) INTO bIsAggregated FROM CANCEL_WORKSPACE_ORDER_ID_GTT ;
  IF (bIsAggregated > 0) THEN
    IF( isFromCancel = 1) THEN
       DELETE FROM ORDER_LINE_ITEM_SIZE WHERE order_id IN (SELECT order_id FROM CANCEL_WORKSPACE_ORDER_ID_GTT);
       DELETE FROM ORDER_LINE_ITEM WHERE order_id IN (SELECT order_id FROM CANCEL_WORKSPACE_ORDER_ID_GTT);
       DELETE FROM ORDER_BASELINE_COST WHERE order_id IN (SELECT order_id FROM CANCEL_WORKSPACE_ORDER_ID_GTT);
       DELETE FROM ORDER_EVENT WHERE order_id IN (SELECT order_id FROM CANCEL_WORKSPACE_ORDER_ID_GTT);
       SELECT order_id, old_order_parent_id
          BULK COLLECT INTO
          orderId, oldOrderParentId FROM CWS_ORDER
       WHERE new_order_parent_id IN (SELECT order_id FROM CANCEL_WORKSPACE_ORDER_ID_GTT);
       FOR i IN orderId.FIRST..orderId.LAST LOOP
          UPDATE ORDERS SET parent_order_id = oldOrderParentId(i) WHERE order_id = orderId(i);
       END LOOP;

       DELETE FROM ORDER_MASTER_ORDER WHERE order_id IN (SELECT order_id FROM CANCEL_WORKSPACE_ORDER_ID_GTT);
       DELETE FROM ORDER_ATTRIBUTE WHERE order_id IN (SELECT order_id FROM CANCEL_WORKSPACE_ORDER_ID_GTT);
       DELETE FROM ORDERS WHERE order_id IN (SELECT order_id FROM CANCEL_WORKSPACE_ORDER_ID_GTT);
     END IF;

   DELETE FROM CWS_ORDER WHERE cons_workspace_id=cwsId AND cws_process_id =processId;
   DELETE FROM CWS_ORDER_SPLIT WHERE cons_workspace_id=cwsId AND order_id IN(SELECT order_id FROM CWS_ORDER WHERE cws_process_id =processId);
   --68375

  END IF;
  DELETE FROM CANCEL_WORKSPACE_ORDER_ID_GTT WHERE cws_process_id =processId;
  DELETE FROM CWS_ORDER_SPLIT WHERE cons_workspace_id=cwsId AND order_id IN (SELECT order_id FROM CWS_ORDER WHERE cws_process_id = processId);
  DELETE FROM CWS_ORDER WHERE cons_workspace_id=cwsId AND cws_process_id =processId;
  DELETE FROM CWS_STOP WHERE cons_workspace_id=cwsId AND ws_shipment_id IN (SELECT ws_shipment_id FROM CWS_SHIPMENT WHERE cws_process_id =processId);
  DELETE FROM CWS_ORIGINAL_VIRTUAL_TRIP WHERE CONS_WORKSPACE_ID=CWSID AND WS_SHIPMENT_ID IN (SELECT WS_SHIPMENT_ID FROM CWS_SHIPMENT WHERE CWS_PROCESS_ID =PROCESSID);
  DELETE FROM CWS_VIRTUAL_TRIP WHERE CONS_WORKSPACE_ID=CWSID AND WS_SHIPMENT_ID IN (SELECT WS_SHIPMENT_ID FROM CWS_SHIPMENT WHERE CWS_PROCESS_ID =PROCESSID);
  DELETE FROM CWS_SHIPMENT WHERE cons_workspace_id=cwsId AND cws_process_id =processId ;
  DELETE FROM CWS_PROCESS WHERE cons_workspace_id=cwsId AND cws_process_id =processId;
--  COMMIT;
END cancel_Workspace;
--  SR1 , 4R1 change, build92,  TT32740 [END]
-- Take orders that are external to the workspace, add them in if they are not already there, then
-- create placeholder shipments and order movement records.  These will require immediate refresh to
-- fill in all the remaining info (stops, rollups, costs, etc.).
/*PROCEDURE create_Shipments_Ext_Orders(
                 shipmentsAddedCsv OUT VARCHAR2,
              shipperId INTEGER,
              cwsId INTEGER,
              size1UOM INTEGER,
              size2UOM INTEGER,
              --CR69340
              size3UOM INTEGER,
              orderIdCsv VARCHAR2,
              placeHolderShipmentIdCsv VARCHAR2,
              movementTypeCsv VARCHAR2,
              pathSetIdCsv VARCHAR2,
              pathIdCsv VARCHAR2,
              orderShipmentSeqCsv VARCHAR2) IS
orderIdArr INT_ARRAY;
placeHolderShipmentIdArr INT_ARRAY;
movementTypeArr STRING_ARRAY;
pathSetIdArr INT_ARRAY;
pathIdArr INT_ARRAY;
orderShipmentSeqArr INT_ARRAY;
shipmentsAddedArr INT_ARRAY;
curShipmentStartIdx INTEGER;
curShipmentEndIdx INTEGER;
curShipmentPlaceHolderId INTEGER;
wasOrderAdded NUMBER(1);
wsShipmentId CWS_SHIPMENT.WS_SHIPMENT_ID%TYPE;
wasAnyOrderMovementAdded NUMBER(1); orderMovementExists NUMBER(1);
hasSplit INTEGER;
BEGIN
 -- Convert the input CSVs into the appropriate arrays
    orderIdArr := csv_to_int_array(orderIdCsv);
    placeHolderShipmentIdArr := csv_to_int_array(placeHolderShipmentIdCsv);
    movementTypeArr := csv_to_string_array(movementTypeCsv);
    pathSetIdArr := csv_to_int_array(pathSetIdCsv);
    pathIdArr := csv_to_int_array(pathIdCsv);
    orderShipmentSeqArr := csv_to_int_array(orderShipmentSeqCsv);
    shipmentsAddedArr := INT_ARRAY();
    -- Outer loop over all array entries, but inner loop to deal with each placeholder shipment.
    -- Watch: All arrays must be of equal length for this to work properly.  Caller must ensure this!
    -- Watch: Every grouping of shipments must have a contiguous set of equal placeholder values to be grouped properly.
    -- Watch: This loop goes from the first shipment + 1 to the last + 1!
   curShipmentStartIdx := 1;
   curShipmentPlaceHolderId := placeHolderShipmentIdArr(curShipmentStartIdx);
   FOR i IN placeHolderShipmentIdArr.FIRST+1..placeHolderShipmentIdArr.LAST+1 LOOP
        -- Process this range of shipments if at last or new placeholder
        IF (i > placeHolderShipmentIdArr.LAST) OR (curShipmentPlaceHolderId <> placeHolderShipmentIdArr(i)) THEN
           curShipmentEndIdx := i-1;
          -- Add all missing orders first.
             FOR j IN curShipmentStartIdx..curShipmentEndIdx LOOP
          --CR69340
                wasOrderAdded := p_add_Order_To_Unassigned(
                 orderIdArr(j), hasSplit, shipperId, cwsId, size1UOM, size2UOM,size3UOM);
          END LOOP;
          -- Now add a shipment and movement records
            SELECT seq_ws_shipment_id.NEXTVAL INTO wsShipmentId FROM dual;
          INSERT INTO CWS_SHIPMENT
             (CONS_WORKSPACE_ID,WS_SHIPMENT_ID,TC_COMPANY_ID,TC_SHIPMENT_ID,
              LAST_REFRESHED_DTTM,LAST_UPDATED_DTTM,NEEDS_REFRESH, NEEDS_VALIDATION)
             VALUES
              (cwsId, wsShipmentId, shipperId, 'NEW', SYSDATE, SYSDATE, 1, 1);
          -- Add 1 partial movement record for each order (no pickup/delivery stops).
          -- Note that there are no original movement records in this case.
          -- There is a test for duplicate order movements which is not allowed to happen
          -- (the sequence #'s should be different at least).  No need to test the PK for
          -- uniqueness because the ws shipment id is new.
          wasAnyOrderMovementAdded := 0;
          FOR j IN curShipmentStartIdx..curShipmentEndIdx LOOP
                shipmentsAddedArr.EXTEND;
              SELECT COUNT(1) INTO orderMovementExists FROM CWS_ORDER_MOVEMENT CWSOM
                       WHERE (CWSOM.CONS_WORKSPACE_ID=cwsId)
                     AND (CWSOM.ORDER_ID=orderIdArr(j))
                     AND (CWSOM.ORDER_SHIPMENT_SEQ=orderShipmentSeqArr(j));
              IF orderMovementExists=0 THEN
                  INSERT INTO CWS_ORDER_MOVEMENT
                     ( CWS_ORDER_MOVEMENT_ID,CONS_WORKSPACE_ID, ORDER_ID, WS_SHIPMENT_ID, MOVEMENT_TYPE, ORDER_SHIPMENT_SEQ,
                      TC_COMPANY_ID, PATH_SET_ID, PATH_ID)
                     VALUES
                     (SEQ_CWS_ORDER_MOVEMENT_ID.NEXTVAL,cwsId, orderIdArr(j), wsShipmentId, movementTypeArr(j), orderShipmentSeqArr(j),
                      shipperId, pathSetIdArr(j), pathIdArr(j));
                  wasAnyOrderMovementAdded := 1;
                  shipmentsAddedArr(shipmentsAddedArr.LAST) := wsShipmentId;
              ELSE
                    shipmentsAddedArr(shipmentsAddedArr.LAST) := NULL;
              END IF;
          END LOOP;
          -- If all order movements were duplicated, no new shipment!  Otherwise, keep it.
          IF wasAnyOrderMovementAdded != 1 THEN
                ROLLBACK;
          END IF;
              -- Setup for next run of placeholders if not at end
              IF i <= placeHolderShipmentIdArr.LAST THEN
                curShipmentStartIdx := i;
              curShipmentPlaceHolderId := placeHolderShipmentIdArr(i);
           END IF;
        END IF; -- Process this range of orders
    END LOOP; -- outermost loop
    shipmentsAddedCsv := int_array_to_csv(shipmentsAddedArr);
END create_Shipments_Ext_Orders;*/



/*PROCEDURE create_Shipments_Ext_Orders(
                 shipmentsAddedCsv OUT VARCHAR2,
              shipperId INTEGER,
              cwsId INTEGER,
              size1UOM INTEGER,
              size2UOM INTEGER,
              --CR69340
              size3UOM INTEGER,
              orderIdCsv VARCHAR2,
              placeHolderShipmentIdCsv VARCHAR2,
              movementTypeCsv VARCHAR2,
              pathSetIdCsv VARCHAR2,
              pathIdCsv VARCHAR2,
              orderShipmentSeqCsv VARCHAR2,
              isRelayCsv VARCHAR2) IS
orderIdArr INT_ARRAY;
placeHolderShipmentIdArr INT_ARRAY;
movementTypeArr STRING_ARRAY;
pathSetIdArr INT_ARRAY;
pathIdArr INT_ARRAY;
isRelayArr INT_ARRAY;
orderShipmentSeqArr INT_ARRAY;
shipmentsAddedArr INT_ARRAY;
curShipmentStartIdx INTEGER;
curShipmentEndIdx INTEGER;
curShipmentPlaceHolderId INTEGER;
wasOrderAdded NUMBER(1);
wsShipmentId CWS_SHIPMENT.WS_SHIPMENT_ID%TYPE;
wasAnyOrderMovementAdded NUMBER(1); orderMovementExists NUMBER(1);
hasSplit INTEGER; -- added to pass to p_add_Order_To_Unassigned without any value assignment
BEGIN
     -- Convert the input CSVs into the appropriate arrays
    orderIdArr := csv_to_int_array(orderIdCsv);
    placeHolderShipmentIdArr := csv_to_int_array(placeHolderShipmentIdCsv);
    movementTypeArr := csv_to_string_array(movementTypeCsv);
    pathSetIdArr := csv_to_int_array(pathSetIdCsv);
    pathIdArr := csv_to_int_array(pathIdCsv);
    orderShipmentSeqArr := csv_to_int_array(orderShipmentSeqCsv);
    isRelayArr := csv_to_int_array(isRelayCsv);
    shipmentsAddedArr := INT_ARRAY();
    -- Outer loop over all array entries, but inner loop to deal with each placeholder shipment.
    -- Watch: All arrays must be of equal length for this to work properly.  Caller must ensure this!
    -- Watch: Every grouping of shipments must have a contiguous set of equal placeholder values to be grouped properly.
    -- Watch: This loop goes from the first shipment + 1 to the last + 1!
   curShipmentStartIdx := 1;
   curShipmentPlaceHolderId := placeHolderShipmentIdArr(curShipmentStartIdx);
   FOR i IN placeHolderShipmentIdArr.FIRST+1..placeHolderShipmentIdArr.LAST+1 LOOP
        -- Process this range of shipments if at last or new placeholder
        IF (i > placeHolderShipmentIdArr.LAST) OR (curShipmentPlaceHolderId <> placeHolderShipmentIdArr(i)) THEN
           curShipmentEndIdx := i-1;
          -- Add all missing orders first.
             FOR j IN curShipmentStartIdx..curShipmentEndIdx LOOP
          --CR69340
                wasOrderAdded := p_add_Order_To_Unassigned(
                 orderIdArr(j), hasSplit, shipperId, cwsId, size1UOM, size2UOM,size3UOM);
          END LOOP;
          -- Now add a shipment and movement records
            SELECT seq_ws_shipment_id.NEXTVAL INTO wsShipmentId FROM dual;
          INSERT INTO CWS_SHIPMENT
             (CONS_WORKSPACE_ID,WS_SHIPMENT_ID,TC_COMPANY_ID,TC_SHIPMENT_ID,
              LAST_REFRESHED_DTTM,LAST_UPDATED_DTTM,NEEDS_REFRESH, NEEDS_VALIDATION)
             VALUES
              (cwsId, wsShipmentId, shipperId, 'NEW', SYSDATE, SYSDATE, 1, 1);
          -- Add 1 partial movement record for each order (no pickup/delivery stops).
          -- Note that there are no original movement records in this case.
          -- There is a test for duplicate order movements which is not allowed to happen
          -- (the sequence #'s should be different at least).  No need to test the PK for
          -- uniqueness because the ws shipment id is new.
          wasAnyOrderMovementAdded := 0;
          FOR j IN curShipmentStartIdx..curShipmentEndIdx LOOP
                shipmentsAddedArr.EXTEND;
              SELECT COUNT(1) INTO orderMovementExists FROM CWS_ORDER_MOVEMENT CWSOM
                       WHERE (CWSOM.CONS_WORKSPACE_ID=cwsId)
                     AND (CWSOM.ORDER_ID=orderIdArr(j))
                     AND (CWSOM.ORDER_SHIPMENT_SEQ=orderShipmentSeqArr(j));
              IF orderMovementExists=0 THEN
                  INSERT INTO CWS_ORDER_MOVEMENT
                     ( CWS_ORDER_MOVEMENT_ID,CONS_WORKSPACE_ID, ORDER_ID, WS_SHIPMENT_ID, MOVEMENT_TYPE, ORDER_SHIPMENT_SEQ,
                      TC_COMPANY_ID, PATH_SET_ID, PATH_ID, IS_RELAY)
                     VALUES
                     (SEQ_CWS_ORDER_MOVEMENT_ID.NEXTVAL,cwsId, orderIdArr(j), wsShipmentId, movementTypeArr(j), orderShipmentSeqArr(j),
                      shipperId, pathSetIdArr(j), pathIdArr(j), isRelayArr(j));
                  wasAnyOrderMovementAdded := 1;
                  shipmentsAddedArr(shipmentsAddedArr.LAST) := wsShipmentId;
              ELSE
                    shipmentsAddedArr(shipmentsAddedArr.LAST) := NULL;
              END IF;
          END LOOP;
          -- If all order movements were duplicated, no new shipment!  Otherwise, keep it.
          IF wasAnyOrderMovementAdded != 1 THEN
                ROLLBACK;

          END IF;
              -- Setup for next run of placeholders if not at end
              IF i <= placeHolderShipmentIdArr.LAST THEN
                curShipmentStartIdx := i;
              curShipmentPlaceHolderId := placeHolderShipmentIdArr(i);
           END IF;
        END IF; -- Process this range of orders
    END LOOP; -- outermost loop
    shipmentsAddedCsv := int_array_to_csv(shipmentsAddedArr);
END create_Shipments_Ext_Orders;*/

PROCEDURE create_Shipments_Ext_Orders(
                 shipmentsAddedCsv OUT VARCHAR2,
              shipperId INTEGER,
              cwsId INTEGER,
              size1UOM INTEGER,
              size2UOM INTEGER,
              --CR69340
              size3UOM INTEGER,
              size4UOM INTEGER, -- CT18
              orderIdCsv VARCHAR2,
                    orderSplitIdCsv VARCHAR2,
              placeHolderShipmentIdCsv VARCHAR2,
              movementTypeCsv VARCHAR2,
              pathSetIdCsv VARCHAR2,
              pathIdCsv VARCHAR2,
              orderShipmentSeqCsv VARCHAR2,
              isRelayCsv VARCHAR2,
              compartmentNoCsv VARCHAR2,
                staticRouteId INTEGER,
                hasSplit INTEGER ) IS
orderIdArr INT_ARRAY;
orderSplitIdArr INT_ARRAY;
placeHolderShipmentIdArr INT_ARRAY;
movementTypeArr STRING_ARRAY;
pathSetIdArr INT_ARRAY;
pathIdArr INT_ARRAY;
isRelayArr INT_ARRAY;
compartmentNoArr INT_ARRAY;
orderShipmentSeqArr INT_ARRAY;
shipmentsAddedArr INT_ARRAY;
curShipmentStartIdx INTEGER;
curShipmentEndIdx INTEGER;
curShipmentPlaceHolderId INTEGER;
wasOrderAdded NUMBER(1);
wsShipmentId CWS_SHIPMENT.WS_SHIPMENT_ID%TYPE;
wasAnyOrderMovementAdded NUMBER(1); orderMovementExists NUMBER(1);
BEGIN
     -- Convert the input CSVs into the appropriate arrays
    orderIdArr := csv_to_int_array(orderIdCsv);
      orderSplitIdArr := csv_to_int_array(orderSplitIdCsv);
    placeHolderShipmentIdArr := csv_to_int_array(placeHolderShipmentIdCsv);
    movementTypeArr := csv_to_string_array(movementTypeCsv);
    pathSetIdArr := csv_to_int_array(pathSetIdCsv);
    pathIdArr := csv_to_int_array(pathIdCsv);
    orderShipmentSeqArr := csv_to_int_array(orderShipmentSeqCsv);
    isRelayArr := csv_to_int_array(isRelayCsv);
    compartmentNoArr := csv_to_int_array(compartmentNoCsv);
    shipmentsAddedArr := INT_ARRAY();
    -- Outer loop over all array entries, but inner loop to deal with each placeholder shipment.
    -- Watch: All arrays must be of equal length for this to work properly.  Caller must ensure this!
    -- Watch: Every grouping of shipments must have a contiguous set of equal placeholder values to be grouped properly.
    -- Watch: This loop goes from the first shipment + 1 to the last + 1!
   curShipmentStartIdx := 1;
   curShipmentPlaceHolderId := placeHolderShipmentIdArr(curShipmentStartIdx);
   FOR i IN placeHolderShipmentIdArr.FIRST+1..placeHolderShipmentIdArr.LAST+1 LOOP
        -- Process this range of shipments if at last or new placeholder
        IF (i > placeHolderShipmentIdArr.LAST) OR (curShipmentPlaceHolderId <> placeHolderShipmentIdArr(i)) THEN

           curShipmentEndIdx := i-1;
          -- Add all missing orders first.
             FOR j IN curShipmentStartIdx..curShipmentEndIdx LOOP
          --CR69340
                wasOrderAdded := p_add_Order_To_Unassigned(
                 orderIdArr(j), hasSplit, shipperId, cwsId, size1UOM, size2UOM,size3UOM,size4UOM); -- CT18

          END LOOP;
          -- Now add a shipment and movement records
            SELECT seq_ws_shipment_id.NEXTVAL INTO wsShipmentId FROM dual;
          INSERT INTO CWS_SHIPMENT
             (CONS_WORKSPACE_ID,WS_SHIPMENT_ID,TC_COMPANY_ID,TC_SHIPMENT_ID,
              LAST_REFRESHED_DTTM,LAST_UPDATED_DTTM,NEEDS_REFRESH, NEEDS_VALIDATION, STATIC_ROUTE_ID)
             VALUES
              (cwsId, wsShipmentId, shipperId, 'NEW', SYSDATE, SYSDATE, 1, 1, staticRouteId);
          -- Add 1 partial movement record for each order (no pickup/delivery stops).
          -- Note that there are no original movement records in this case.
          -- There is a test for duplicate order movements which is not allowed to happen
          -- (the sequence #'s should be different at least).  No need to test the PK for
          -- uniqueness because the ws shipment id is new.
          wasAnyOrderMovementAdded := 0;
          FOR j IN curShipmentStartIdx..curShipmentEndIdx LOOP
                shipmentsAddedArr.EXTEND;

              SELECT COUNT(1) INTO orderMovementExists FROM CWS_ORDER_MOVEMENT CWSOM
                       WHERE (CWSOM.CONS_WORKSPACE_ID=cwsId)
                     AND (CWSOM.ORDER_ID=orderIdArr(j))
                     AND (CWSOM.ORDER_SPLIT_ID= orderSplitIdArr(j))
                     AND (CWSOM.ORDER_SHIPMENT_SEQ=orderShipmentSeqArr(j));
              IF orderMovementExists=0 THEN
                  IF (hasSplit = 1 AND orderSplitIdArr(j) <> 0) THEN -- if split indicator is true then consider split order ids
                        INSERT INTO CWS_ORDER_MOVEMENT
                                 (CWS_ORDER_MOVEMENT_ID,CONS_WORKSPACE_ID, ORDER_ID, ORDER_SPLIT_ID, WS_SHIPMENT_ID, MOVEMENT_TYPE, ORDER_SHIPMENT_SEQ,
                                TC_COMPANY_ID, PATH_SET_ID, PATH_ID, IS_RELAY, COMPARTMENT_NO)
                             VALUES
                             (SEQ_CWS_ORDER_MOVEMENT_ID.NEXTVAL,cwsId, orderIdArr(j), orderSplitIdArr(j), wsShipmentId, movementTypeArr(j), orderShipmentSeqArr(j),
                                shipperId, pathSetIdArr(j), pathIdArr(j), isRelayArr(j), compartmentNoArr(j));
                    ELSE -- normal operation if split indicator is false; split id will be null
                        INSERT INTO CWS_ORDER_MOVEMENT
                             (CWS_ORDER_MOVEMENT_ID,CONS_WORKSPACE_ID, ORDER_ID, WS_SHIPMENT_ID, MOVEMENT_TYPE, ORDER_SHIPMENT_SEQ,
                            TC_COMPANY_ID, PATH_SET_ID, PATH_ID, IS_RELAY, COMPARTMENT_NO)
                         VALUES
                         (SEQ_CWS_ORDER_MOVEMENT_ID.NEXTVAL,cwsId, orderIdArr(j), wsShipmentId, movementTypeArr(j), orderShipmentSeqArr(j),
                            shipperId, pathSetIdArr(j), pathIdArr(j), isRelayArr(j), compartmentNoArr(j));
                END IF;
                  wasAnyOrderMovementAdded := 1;
                  shipmentsAddedArr(shipmentsAddedArr.LAST) := wsShipmentId;


              ELSE

                   shipmentsAddedArr(shipmentsAddedArr.LAST) := NULL;
              END IF;
          END LOOP;
          -- If all order movements were duplicated, no new shipment!  Otherwise, keep it.
          IF wasAnyOrderMovementAdded != 1 THEN

                ROLLBACK;
          END IF;
              -- Setup for next run of placeholders if not at end
              IF i <= placeHolderShipmentIdArr.LAST THEN
                curShipmentStartIdx := i;
              curShipmentPlaceHolderId := placeHolderShipmentIdArr(i);
           END IF;
        END IF; -- Process this range of orders
    END LOOP; -- outermost loop
    shipmentsAddedCsv := int_array_to_csv(shipmentsAddedArr);
END create_Shipments_Ext_Orders;

-- Workspace function to assign orders already in the workspace to existing draft shipments.
-- It does this by adding order movements when possible.  The return is a Csv of 0's and 1's
-- indicating success or failure for each requested added movment.
PROCEDURE assign_Orders_To_Drafts(
                 ordersAssignedBoolCsv OUT VARCHAR2,
              shipperId INTEGER,
              cwsId INTEGER,
              isSplit INTEGER,
              orderIdCsv VARCHAR2,
              orderSplitIdCsv VARCHAR2,
              cwsShipmentIdCsv VARCHAR2,
              omShipmentIdCsv VARCHAR2,
              pickupSeqCsv VARCHAR2,
              deliverySeqCsv VARCHAR2,
              movementTypeCsv VARCHAR2,
              pathSetIdCsv VARCHAR2,
              pathIdCsv VARCHAR2,
              orderShipmentSeqCsv VARCHAR2,
              isRelayCsv VARCHAR2,
              compartmentNoCsv VARCHAR2) IS
orderIdArr INT_ARRAY;
orderSplitIdArr INT_ARRAY;
cwsShipmentIdArr INT_ARRAY;
omShipmentIdArr INT_ARRAY;
pickupSeqArr INT_ARRAY;
deliverySeqArr INT_ARRAY;
movementTypeArr STRING_ARRAY;
pathSetIdArr INT_ARRAY;
pathIdArr INT_ARRAY;
orderShipmentSeqArr INT_ARRAY;
isRelayArr INT_ARRAY;
compartmentNoArr INT_ARRAY;
ordersAssignedBoolArr INT_ARRAY;
orderMovementExists NUMBER(1);
wsShipmentInsertArr INT_ARRAY;
BEGIN
     -- Convert the input CSVs into the appropriate arrays
    orderIdArr := csv_to_int_array(orderIdCsv);
    orderSplitIdArr := csv_to_int_array(orderSplitIdCsv);
    cwsShipmentIdArr := csv_to_int_array(cwsShipmentIdCsv);
    omShipmentIdArr := csv_to_int_array(omShipmentIdCsv);
    pickupSeqArr := csv_to_int_array(pickupSeqCsv);
    deliverySeqArr := csv_to_int_array(deliverySeqCsv);
    movementTypeArr := csv_to_string_array(movementTypeCsv);
    pathSetIdArr := csv_to_int_array(pathSetIdCsv);
    pathIdArr := csv_to_int_array(pathIdCsv);
    orderShipmentSeqArr := csv_to_int_array(orderShipmentSeqCsv);
    isRelayArr := csv_to_int_array(isRelayCsv);
    compartmentNoArr := csv_to_int_array(compartmentNoCsv);
    ordersAssignedBoolArr := INT_ARRAY();
    wsShipmentInsertArr := INT_ARRAY();
    -- For each movment, test for existing violating PK or path constraints.
    -- If none, insert movement.
    FOR j IN orderIdArr.FIRST..orderIdArr.LAST LOOP
        ordersAssignedBoolArr.EXTEND;
        IF(isSPlit =0) THEN
            SELECT COUNT(1) INTO orderMovementExists FROM CWS_ORDER_MOVEMENT CWSOM
                   WHERE ((CWSOM.CONS_WORKSPACE_ID=cwsId) AND (CWSOM.ORDER_ID=orderIdArr(j)))
                        AND
                       ((CWSOM.ORDER_SHIPMENT_SEQ=orderShipmentSeqArr(j)) OR
                        (CWSOM.WS_SHIPMENT_ID=cwsShipmentIdArr(j)));
        ELSE

            SELECT COUNT(1) INTO orderMovementExists FROM CWS_ORDER_MOVEMENT CWSOM
                 WHERE ((CWSOM.CONS_WORKSPACE_ID=cwsId) AND (CWSOM.ORDER_ID=orderIdArr(j)))
                       AND
                       ((CWSOM.ORDER_SHIPMENT_SEQ=orderShipmentSeqArr(j)) OR
                        (CWSOM.WS_SHIPMENT_ID=cwsShipmentIdArr(j)))
                       AND
                       (CWSOM.ORDER_SPLIT_ID = orderSplitIdArr(j));
        END IF;

        IF orderMovementExists=0 THEN
            IF(isSplit =0) THEN
              INSERT INTO CWS_ORDER_MOVEMENT
              (CWS_ORDER_MOVEMENT_ID,CONS_WORKSPACE_ID, ORDER_ID, WS_SHIPMENT_ID, MOVEMENT_TYPE, ORDER_SHIPMENT_SEQ,
                TC_COMPANY_ID, PATH_SET_ID, PATH_ID, PICKUP_STOP_SEQ, DELIVERY_STOP_SEQ, IS_RELAY, COMPARTMENT_NO)
                VALUES
                (SEQ_CWS_ORDER_MOVEMENT_ID.NEXTVAL,cwsId, orderIdArr(j), cwsShipmentIdArr(j), movementTypeArr(j), orderShipmentSeqArr(j),
                shipperId, pathSetIdArr(j), pathIdArr(j), pickupSeqArr(j), deliverySeqArr(j), isRelayArr(j), compartmentNoArr(j));

            ELSE
              INSERT INTO CWS_ORDER_MOVEMENT
          (CWS_ORDER_MOVEMENT_ID,
          CONS_WORKSPACE_ID,
          ORDER_ID,
          ORDER_SPLIT_ID,
          WS_SHIPMENT_ID,
          MOVEMENT_TYPE,
          ORDER_SHIPMENT_SEQ,
          TC_COMPANY_ID,
          PATH_SET_ID,
          PATH_ID,
          PICKUP_STOP_SEQ,
          DELIVERY_STOP_SEQ,
          IS_RELAY,
          COMPARTMENT_NO)
               VALUES
              (SEQ_CWS_ORDER_MOVEMENT_ID.NEXTVAL,
                cwsId,
                orderIdArr(j),
                orderSplitIdArr(j),
                cwsShipmentIdArr(j),
                movementTypeArr(j),
                orderShipmentSeqArr(j),
                shipperId,
                pathSetIdArr(j),
                pathIdArr(j),
                pickupSeqArr(j),
                deliverySeqArr(j),
                isRelayArr(j),
                compartmentNoArr(j)
              );
      END IF;
      ordersAssignedBoolArr(ordersAssignedBoolArr.LAST) := 1;
      wsShipmentInsertArr.EXTEND;
      wsShipmentInsertArr(wsShipmentInsertArr.LAST) := cwsShipmentIdArr(j);
    ELSE
              ordersAssignedBoolArr(ordersAssignedBoolArr.LAST) := 0;
    END IF;
    END LOOP;
    p_mark_For_Refresh(cwsId, wsShipmentInsertArr);
    p_ReEnable_Comps(cwsId,wsShipmentInsertArr);

    ordersAssignedBoolCsv := int_array_to_csv(ordersAssignedBoolArr);
END assign_Orders_To_Drafts;

PROCEDURE assign_Orders_To_Drafts(
                 ordersAssignedBoolCsv OUT VARCHAR2,
              shipperId INTEGER,
              cwsId INTEGER,
              orderIdCsv VARCHAR2,
              cwsShipmentIdCsv VARCHAR2,
              omShipmentIdCsv VARCHAR2,
              pickupSeqCsv VARCHAR2,
              deliverySeqCsv VARCHAR2,
              movementTypeCsv VARCHAR2,
              pathSetIdCsv VARCHAR2,
              pathIdCsv VARCHAR2,
              orderShipmentSeqCsv VARCHAR2,
              isRelayCsv VARCHAR2 ) IS
orderIdArr INT_ARRAY;
cwsShipmentIdArr INT_ARRAY;
omShipmentIdArr INT_ARRAY;
pickupSeqArr INT_ARRAY;
deliverySeqArr INT_ARRAY;
movementTypeArr STRING_ARRAY;
pathSetIdArr INT_ARRAY;
pathIdArr INT_ARRAY;
orderShipmentSeqArr INT_ARRAY;
isRelayArr INT_ARRAY;
ordersAssignedBoolArr INT_ARRAY;
orderMovementExists NUMBER(1);
wsShipmentInsertArr INT_ARRAY;
BEGIN
     -- Convert the input CSVs into the appropriate arrays
    orderIdArr := csv_to_int_array(orderIdCsv);
    cwsShipmentIdArr := csv_to_int_array(cwsShipmentIdCsv);
    omShipmentIdArr := csv_to_int_array(omShipmentIdCsv);
    pickupSeqArr := csv_to_int_array(pickupSeqCsv);
    deliverySeqArr := csv_to_int_array(deliverySeqCsv);
    movementTypeArr := csv_to_string_array(movementTypeCsv);
    pathSetIdArr := csv_to_int_array(pathSetIdCsv);
    pathIdArr := csv_to_int_array(pathIdCsv);
    orderShipmentSeqArr := csv_to_int_array(orderShipmentSeqCsv);
    isRelayArr := csv_to_int_array(isRelayCsv);
    ordersAssignedBoolArr := INT_ARRAY();
    wsShipmentInsertArr := INT_ARRAY();
    -- For each movment, test for existing violating PK or path constraints.
    -- If none, insert movement.
    FOR j IN orderIdArr.FIRST..orderIdArr.LAST LOOP
        ordersAssignedBoolArr.EXTEND;
        SELECT COUNT(1) INTO orderMovementExists FROM CWS_ORDER_MOVEMENT CWSOM
                   WHERE ((CWSOM.CONS_WORKSPACE_ID=cwsId) AND (CWSOM.ORDER_ID=orderIdArr(j)))
                        AND
                       ((CWSOM.ORDER_SHIPMENT_SEQ=orderShipmentSeqArr(j)) OR
                        (CWSOM.WS_SHIPMENT_ID=cwsShipmentIdArr(j)));
        IF orderMovementExists=0 THEN
              INSERT INTO CWS_ORDER_MOVEMENT
                 (CWS_ORDER_MOVEMENT_ID,CONS_WORKSPACE_ID, ORDER_ID, WS_SHIPMENT_ID, MOVEMENT_TYPE, ORDER_SHIPMENT_SEQ,
                  TC_COMPANY_ID, PATH_SET_ID, PATH_ID, PICKUP_STOP_SEQ, DELIVERY_STOP_SEQ, IS_RELAY)
                 VALUES
                 (SEQ_CWS_ORDER_MOVEMENT_ID.NEXTVAL,cwsId, orderIdArr(j), cwsShipmentIdArr(j), movementTypeArr(j), orderShipmentSeqArr(j),
                  shipperId, pathSetIdArr(j), pathIdArr(j), pickupSeqArr(j), deliverySeqArr(j), isRelayArr(j) );
              ordersAssignedBoolArr(ordersAssignedBoolArr.LAST) := 1;
              wsShipmentInsertArr.EXTEND;
              wsShipmentInsertArr(wsShipmentInsertArr.LAST) := cwsShipmentIdArr(j);
        ELSE
              ordersAssignedBoolArr(ordersAssignedBoolArr.LAST) := 0;
        END IF;
     END LOOP;
     p_mark_For_Refresh(cwsId, wsShipmentInsertArr);
     p_ReEnable_Comps(cwsId,wsShipmentInsertArr);

     ordersAssignedBoolCsv := int_array_to_csv(ordersAssignedBoolArr);
END assign_Orders_To_Drafts;

PROCEDURE assign_Orders_To_Drafts(
                 ordersAssignedBoolCsv OUT VARCHAR2,
              shipperId INTEGER,
              cwsId INTEGER,
              orderIdCsv VARCHAR2,
              cwsShipmentIdCsv VARCHAR2,
              omShipmentIdCsv VARCHAR2,
              pickupSeqCsv VARCHAR2,
              deliverySeqCsv VARCHAR2,
              movementTypeCsv VARCHAR2,
              pathSetIdCsv VARCHAR2,
              pathIdCsv VARCHAR2,
              orderShipmentSeqCsv VARCHAR2,
              isRelayCsv VARCHAR2,
              compartmentNoCsv VARCHAR2 ) IS
orderIdArr INT_ARRAY;
cwsShipmentIdArr INT_ARRAY;
omShipmentIdArr INT_ARRAY;
pickupSeqArr INT_ARRAY;
deliverySeqArr INT_ARRAY;
movementTypeArr STRING_ARRAY;
pathSetIdArr INT_ARRAY;
pathIdArr INT_ARRAY;
orderShipmentSeqArr INT_ARRAY;
isRelayArr INT_ARRAY;
compartmentNoArr INT_ARRAY;
ordersAssignedBoolArr INT_ARRAY;
orderMovementExists NUMBER(1);
wsShipmentInsertArr INT_ARRAY;
BEGIN
     -- Convert the input CSVs into the appropriate arrays
    orderIdArr := csv_to_int_array(orderIdCsv);
    cwsShipmentIdArr := csv_to_int_array(cwsShipmentIdCsv);
    omShipmentIdArr := csv_to_int_array(omShipmentIdCsv);
    pickupSeqArr := csv_to_int_array(pickupSeqCsv);
    deliverySeqArr := csv_to_int_array(deliverySeqCsv);
    movementTypeArr := csv_to_string_array(movementTypeCsv);
    pathSetIdArr := csv_to_int_array(pathSetIdCsv);
    pathIdArr := csv_to_int_array(pathIdCsv);
    orderShipmentSeqArr := csv_to_int_array(orderShipmentSeqCsv);
    isRelayArr := csv_to_int_array(isRelayCsv);
    compartmentNoArr := csv_to_int_array(compartmentNoCsv);
    ordersAssignedBoolArr := INT_ARRAY();
    wsShipmentInsertArr := INT_ARRAY();
    -- For each movment, test for existing violating PK or path constraints.
    -- If none, insert movement.
    FOR j IN orderIdArr.FIRST..orderIdArr.LAST LOOP
        ordersAssignedBoolArr.EXTEND;
        SELECT COUNT(1) INTO orderMovementExists FROM CWS_ORDER_MOVEMENT CWSOM
                   WHERE ((CWSOM.CONS_WORKSPACE_ID=cwsId) AND (CWSOM.ORDER_ID=orderIdArr(j)))
                        AND
                       ((CWSOM.ORDER_SHIPMENT_SEQ=orderShipmentSeqArr(j)) OR
                        (CWSOM.WS_SHIPMENT_ID=cwsShipmentIdArr(j)));
        IF orderMovementExists=0 THEN
              INSERT INTO CWS_ORDER_MOVEMENT
                 (CWS_ORDER_MOVEMENT_ID,CONS_WORKSPACE_ID, ORDER_ID, WS_SHIPMENT_ID, MOVEMENT_TYPE, ORDER_SHIPMENT_SEQ,
                  TC_COMPANY_ID, PATH_SET_ID, PATH_ID, PICKUP_STOP_SEQ, DELIVERY_STOP_SEQ, IS_RELAY, COMPARTMENT_NO)
                 VALUES
                 (SEQ_CWS_ORDER_MOVEMENT_ID.NEXTVAL,cwsId, orderIdArr(j), cwsShipmentIdArr(j), movementTypeArr(j), orderShipmentSeqArr(j),
                  shipperId, pathSetIdArr(j), pathIdArr(j), pickupSeqArr(j), deliverySeqArr(j), isRelayArr(j), compartmentNoArr(j) );
              ordersAssignedBoolArr(ordersAssignedBoolArr.LAST) := 1;
              wsShipmentInsertArr.EXTEND;
              wsShipmentInsertArr(wsShipmentInsertArr.LAST) := cwsShipmentIdArr(j);
        ELSE
              ordersAssignedBoolArr(ordersAssignedBoolArr.LAST) := 0;
        END IF;
     END LOOP;
     p_mark_For_Refresh(cwsId, wsShipmentInsertArr);
     p_ReEnable_Comps(cwsId,wsShipmentInsertArr);

     ordersAssignedBoolCsv := int_array_to_csv(ordersAssignedBoolArr);
END assign_Orders_To_Drafts;

-- Workspace function to assign orders already in the workspace to existing draft shipments.
-- It does this by adding order movements when possible.  The return is a Csv of 0's and 1's
-- indicating success or failure for each requested added movment.
PROCEDURE assign_Orders_To_Drafts_SOD(
                 ordersAssignedBoolCsv OUT VARCHAR2,
              shipperId INTEGER,
              cwsId INTEGER,
              orderIdCsv VARCHAR2,
              cwsShipmentIdCsv VARCHAR2,
              omShipmentIdCsv VARCHAR2,
              pickupSeqCsv VARCHAR2,
              deliverySeqCsv VARCHAR2,
              movementTypeCsv VARCHAR2,
              pathSetIdCsv VARCHAR2,
              pathIdCsv VARCHAR2,
              orderShipmentSeqCsv VARCHAR2) IS
orderIdArr INT_ARRAY;
cwsShipmentIdArr INT_ARRAY;
omShipmentIdArr INT_ARRAY;
pickupSeqArr INT_ARRAY;
deliverySeqArr INT_ARRAY;
movementTypeArr STRING_ARRAY;
pathSetIdArr INT_ARRAY;
pathIdArr INT_ARRAY;
orderShipmentSeqArr INT_ARRAY;
ordersAssignedBoolArr INT_ARRAY;
orderMovementExists NUMBER(1);
wsShipmentInsertArr INT_ARRAY;
BEGIN
     -- Convert the input CSVs into the appropriate arrays
    orderIdArr := csv_to_int_array(orderIdCsv);
    cwsShipmentIdArr := csv_to_int_array(cwsShipmentIdCsv);
    omShipmentIdArr := csv_to_int_array(omShipmentIdCsv);
    pickupSeqArr := csv_to_int_array(pickupSeqCsv);
    deliverySeqArr := csv_to_int_array(deliverySeqCsv);
    movementTypeArr := csv_to_string_array(movementTypeCsv);
    pathSetIdArr := csv_to_int_array(pathSetIdCsv);
    pathIdArr := csv_to_int_array(pathIdCsv);
    orderShipmentSeqArr := csv_to_int_array(orderShipmentSeqCsv);
    ordersAssignedBoolArr := INT_ARRAY();
    wsShipmentInsertArr := INT_ARRAY();
    -- For each movment, test for existing violating PK or path constraints.
    -- If none, insert movement.
    FOR j IN orderIdArr.FIRST..orderIdArr.LAST LOOP
        ordersAssignedBoolArr.EXTEND;
        SELECT COUNT(1) INTO orderMovementExists FROM CWS_ORDER_MOVEMENT CWSOM
                   WHERE ((CWSOM.CONS_WORKSPACE_ID=cwsId) AND (CWSOM.ORDER_ID=orderIdArr(j)))
                        AND
                       ((CWSOM.ORDER_SHIPMENT_SEQ=orderShipmentSeqArr(j)) AND
                        (CWSOM.WS_SHIPMENT_ID=cwsShipmentIdArr(j)));
        IF orderMovementExists=0 THEN
              INSERT INTO CWS_ORDER_MOVEMENT
                 (CWS_ORDER_MOVEMENT_ID,CONS_WORKSPACE_ID, ORDER_ID, WS_SHIPMENT_ID, MOVEMENT_TYPE, ORDER_SHIPMENT_SEQ,
                  TC_COMPANY_ID, PATH_SET_ID, PATH_ID, PICKUP_STOP_SEQ, DELIVERY_STOP_SEQ)
                 VALUES
                 (SEQ_CWS_ORDER_MOVEMENT_ID.NEXTVAL,cwsId, orderIdArr(j), cwsShipmentIdArr(j), movementTypeArr(j), orderShipmentSeqArr(j),
                  shipperId, pathSetIdArr(j), pathIdArr(j), pickupSeqArr(j), deliverySeqArr(j));
              ordersAssignedBoolArr(ordersAssignedBoolArr.LAST) := 1;
              wsShipmentInsertArr.EXTEND;
              wsShipmentInsertArr(wsShipmentInsertArr.LAST) := cwsShipmentIdArr(j);
        ELSE
              ordersAssignedBoolArr(ordersAssignedBoolArr.LAST) := 0;
        END IF;
     END LOOP;
     p_mark_For_Refresh(cwsId, wsShipmentInsertArr);
     p_ReEnable_Comps(cwsId,wsShipmentInsertArr);
--     COMMIT;
     ordersAssignedBoolCsv := int_array_to_csv(ordersAssignedBoolArr);
END assign_Orders_To_Drafts_SOD;
-- Workspace function to unassign orders already in the workspace and assigned to existing draft shipments.
-- It does this by removing order movements when possible.  The return is a Csv of 0's and 1's
-- indicating success or failure for each requested removed movment.
-- cleanShipmentsBool should be set to true if shipments should get cleaned now.
PROCEDURE unassign_Orders_From_Drafts(
                                      ordersUnassignedBoolCsv OUT VARCHAR2,
                                      shipperId INTEGER,
                                      cwsId INTEGER,
                                      orderIdCsv VARCHAR2,
                                      splitOrderCsv VARCHAR2,
                                      cwsShipmentIdCsv VARCHAR2,
                                      cleanShipmentsBool INTEGER,
                                      isSplit INTEGER
                                      ) IS
orderIdArr INT_ARRAY;
splitOrderIdArr INT_ARRAY;
cwsShipmentIdArr INT_ARRAY;
ordersUnassignedBoolArr INT_ARRAY;
wsShipmentInsertArr INT_ARRAY;
wsShipmentFinalInsertArr INT_ARRAY;
BEGIN
             -- Convert the input CSVs into the appropriate arrays
            orderIdArr := csv_to_int_array(orderIdCsv);
            splitOrderIdArr := csv_to_int_array(splitOrderCsv);
            cwsShipmentIdArr := csv_to_int_array(cwsShipmentIdCsv);
            ordersUnassignedBoolArr := INT_ARRAY();
            wsShipmentInsertArr := INT_ARRAY();

            -- For each movment, try to delete it.
            IF (orderIdCsv != ' ')
            THEN

            FOR j IN orderIdArr.FIRST..orderIdArr.LAST LOOP
                        ordersUnassignedBoolArr.EXTEND;
                        DELETE CWS_ORDER_MOVEMENT CWSOM WHERE
                                                (CWSOM.CONS_WORKSPACE_ID=cwsId)
                                    AND (CWSOM.ORDER_ID=orderIdArr(j))
                                    AND (CWSOM.WS_SHIPMENT_ID=cwsShipmentIdArr(j));
                        IF SQL%ROWCOUNT = 1 THEN
                           ordersUnassignedBoolArr(ordersUnassignedBoolArr.LAST) := 1;
                           wsShipmentInsertArr.EXTEND;
                           wsShipmentInsertArr(wsShipmentInsertArr.LAST) := cwsShipmentIdArr(j);
                        ELSE
                           ordersUnassignedBoolArr(ordersUnassignedBoolArr.LAST) := 0;
                        END IF;
             END LOOP;

         END IF;

             IF(isSplit = 1) THEN
              IF (splitOrderCsv != ' ')
               THEN
               FOR j IN splitOrderIdArr.FIRST..splitOrderIdArr.LAST LOOP
                                    ordersUnassignedBoolArr.EXTEND;
                                    DELETE CWS_ORDER_MOVEMENT CWSOM WHERE
                                                            (CWSOM.CONS_WORKSPACE_ID=cwsId)
                                               AND (CWSOM.ORDER_SPLIT_ID=splitOrderIdArr(j))
                                                AND (CWSOM.WS_SHIPMENT_ID=cwsShipmentIdArr(j));
                                    IF SQL%ROWCOUNT = 1 THEN
                                       ordersUnassignedBoolArr(ordersUnassignedBoolArr.LAST) := 1;
                                       wsShipmentInsertArr.EXTEND;
                                       wsShipmentInsertArr(wsShipmentInsertArr.LAST) := cwsShipmentIdArr(j);
                                    ELSE
                                       ordersUnassignedBoolArr(ordersUnassignedBoolArr.LAST) := 0;
                                    END IF;
               END LOOP;
             END IF;
             END IF;

            /* loop over the shipments to refresh -
             * if any of them have no orders left,
             * remove them from the workspace.
             */
            IF cleanShipmentsBool=1 THEN
                        wsShipmentFinalInsertArr := p_clean_shipments(cwsId, shipperId, wsShipmentInsertArr);
            ELSE
                wsShipmentFinalInsertArr := wsShipmentInsertArr;
            END IF;
             p_mark_For_Refresh(cwsId, wsShipmentFinalInsertArr);
             p_ReEnable_Comps(cwsId,wsShipmentFinalInsertArr);

             ordersUnassignedBoolCsv := int_array_to_csv(ordersUnassignedBoolArr);
END unassign_Orders_From_Drafts;

PROCEDURE delete_empty_shipments(
              shipperId INTEGER,
              cwsId INTEGER,
              cwsShipmentIdCsv VARCHAR2
              ) IS
cwsShipmentIdArr INT_ARRAY;
wsShipmentFinalArr INT_ARRAY;
BEGIN
     -- Convert the input CSVs into the appropriate arrays
    cwsShipmentIdArr := csv_to_int_array(cwsShipmentIdCsv);
    wsShipmentFinalArr := p_clean_shipments(cwsId, shipperId, cwsShipmentIdArr);
--    COMMIT;
END delete_empty_shipments;
-- Get list of out of synch orders and shipments based on MCW last refresh dttm vs. object's last update dttm
PROCEDURE get_Out_Of_Synch_Ids(
                 ordersOutOfSynch OUT VARCHAR2,
                 cwsShipmentsOutOfSynch OUT VARCHAR2,
              shipperId INTEGER,
              cwsId INTEGER) IS
orderIdArr INT_ARRAY;
cwsShipmentIdArr INT_ARRAY;
BEGIN
  SELECT ORDERS.order_id BULK COLLECT INTO orderIdArr FROM CWS_ORDER, ORDERS
    WHERE (CWS_ORDER.CONS_WORKSPACE_ID = cwsId) AND
          (CWS_ORDER.ORDER_ID = ORDERS.ORDER_ID) AND
          (CWS_ORDER.LAST_REFRESHED_DTTM IS NULL OR CWS_ORDER.LAST_REFRESHED_DTTM < ORDERS.LAST_UPDATED_DTTM);
  SELECT CWS_SHIPMENT.ws_shipment_id BULK COLLECT INTO cwsShipmentIdArr FROM CWS_SHIPMENT, SHIPMENT
    WHERE (CWS_SHIPMENT.CONS_WORKSPACE_ID = cwsId) AND
          (CWS_SHIPMENT.ORIG_SHIPMENT_ID = SHIPMENT.SHIPMENT_ID) AND
          (CWS_SHIPMENT.LAST_REFRESHED_DTTM IS NULL OR CWS_SHIPMENT.LAST_REFRESHED_DTTM < SHIPMENT.LAST_UPDATED_DTTM);
  ordersOutOfSynch := int_array_to_csv(orderIdArr);
  cwsShipmentsOutOfSynch := int_array_to_csv(cwsShipmentIdArr);
END get_Out_Of_Synch_Ids;

-- Get list of out of synch orders and shipments based on MCW last refresh dttm vs. object's last update dttm
PROCEDURE get_Out_Of_Synch_Ids(
                 ordersOutOfSynch OUT VARCHAR2,
          ordersWithSplitOutOfSynch OUT VARCHAR2,
                 cwsShipmentsOutOfSynch OUT VARCHAR2,
              shipperId INTEGER,
              cwsId INTEGER) IS
orderIdArr INT_ARRAY;
orderSplitIdArr INT_ARRAY;
cwsShipmentIdArr INT_ARRAY;
BEGIN

  SELECT ORDERS.order_id BULK COLLECT INTO orderIdArr FROM CWS_ORDER, ORDERS
    WHERE (CWS_ORDER.CONS_WORKSPACE_ID = cwsId) AND
          (CWS_ORDER.ORDER_ID = ORDERS.ORDER_ID) AND
          (CWS_ORDER.LAST_REFRESHED_DTTM IS NULL OR CWS_ORDER.LAST_REFRESHED_DTTM < ORDERS.LAST_UPDATED_DTTM);


 SELECT ORDERS.order_id  BULK COLLECT INTO orderSplitIdArr FROM CWS_ORDER, ORDERS
                 WHERE ORDERS.HAS_SPLIT = 1
                 AND (CWS_ORDER.CONS_WORKSPACE_ID = cwsId)
                 AND CWS_ORDER.ORDER_ID = ORDERS.ORDER_ID
                 AND (CWS_ORDER.LAST_REFRESHED_DTTM IS NULL
                    OR CWS_ORDER.LAST_REFRESHED_DTTM < ORDERS.LAST_UPDATED_DTTM) ;

  SELECT CWS_SHIPMENT.ws_shipment_id BULK COLLECT INTO cwsShipmentIdArr FROM CWS_SHIPMENT, SHIPMENT
    WHERE (CWS_SHIPMENT.CONS_WORKSPACE_ID = cwsId) AND
          (CWS_SHIPMENT.ORIG_SHIPMENT_ID = SHIPMENT.SHIPMENT_ID) AND
          (CWS_SHIPMENT.LAST_REFRESHED_DTTM IS NULL OR CWS_SHIPMENT.LAST_REFRESHED_DTTM < SHIPMENT.LAST_UPDATED_DTTM);

  ordersOutOfSynch := int_array_to_csv(orderIdArr);
  cwsShipmentsOutOfSynch := int_array_to_csv(cwsShipmentIdArr);
END get_Out_Of_Synch_Ids;

-- Load all data required for display of the workspace at once.
PROCEDURE load_Workspace(
                 shipmentListRefcur OUT CURSOR_TYPE,        -- Cws shipment display info
              assignedOrderListRefcur OUT CURSOR_TYPE,    -- Cws assigned order display info
              unassignedOrderListRefcur OUT CURSOR_TYPE,-- Cws unassigned order display info
              retCwsShipmentId OUT INTEGER,                  -- current draft shipment id as determined by the sproc
              statCwsShipmentCount OUT NUMBER,            -- total # Cws shipments in workspace
              statTotalStopCount OUT NUMBER,            -- total # of stops across all shipments
              statTotalOrderMovementCount OUT NUMBER,   -- total # of order movements across all shipments
              statBaselineCost OUT CWS_SHIPMENT.BASELINE_COST%TYPE,   -- total baseline cost for shipments
              statEstimatedCost OUT CWS_SHIPMENT.ESTIMATED_COST%TYPE, -- total estimated cost for shipments
              statInitialCost OUT CWS_SHIPMENT.INITIAL_COST%TYPE,     -- total initial cost for shipments
              shipperId INTEGER,                            -- company id
              cwsId INTEGER,                                -- workspace id
              currentCwsShipmentId INTEGER,                  -- current draft shipment id
              shipmentListOrderBy VARCHAR2,             -- order by clause for shipment list or empty string
              shipmentListMinRow NUMBER,                  -- first row to retrieve (0-based)
              shipmentListMaxRow NUMBER,                -- last row + 1 to retrieve (ex: 0..10 retrieves first 10 shipments)
              orderIdMDFilter NUMBER,                    -- for MD link, if non-null, joins to cws_order_movement to restrict list of shipment to this order id
              assignedOrderListOrderBy VARCHAR2,         -- order by clause for assigned orders list or empty string
              unassignedOrderListOrderBy VARCHAR2)       -- order by clause for unassigned orders list or empty string
              IS
   sql_statement1 VARCHAR(2000);
   sql_statement2 VARCHAR(2000);
   sql_statement3 VARCHAR(2000);
   shipment_id_cursor CURSOR_TYPE;
   vcurrow NUMBER;
   vidlist_done NUMBER;
   vShipmentId CWS_SHIPMENT.WS_SHIPMENT_ID%TYPE;
   vgttcount NUMBER;
   shipment_list_cursor CURSOR_TYPE;
   asg_order_list_cursor CURSOR_TYPE;
   una_order_list_cursor CURSOR_TYPE;
   firstCwsShipmentId CWS_SHIPMENT.WS_SHIPMENT_ID%TYPE;
   foundCurrentShipmentId NUMBER(1);
   order_null_check    PLS_INTEGER    := 0;
    partial_shipment_network PLS_INTEGER := 0;
BEGIN
    -- First get the shipment id list for the given range in the correct order
    DELETE MAN_CONS_SHIPMENT_LIST_GTT;
    sql_statement1 := 'select cws_shipment.ws_shipment_id from cws_shipment'
                      || '  where cws_shipment.cons_workspace_id=:cwsId';
    -- Dynamically build and bind the select for just the id's
    IF orderIdMDFilter IS NOT NULL THEN
       sql_statement1 := sql_statement1
                    || ' and cws_shipment.ws_shipment_id in'
                 || ' (select distinct cws_order_movement.ws_shipment_id from cws_order_movement where'
                 || '  cws_order_movement.cons_workspace_id=:cwsId and'
                 || '  cws_order_movement.order_id=:orderIdMD)';
    END IF;
    sql_statement1 := sql_statement1 || ' '  || shipmentListOrderBy;
    IF orderIdMDFilter IS NOT NULL THEN
       OPEN shipment_id_cursor FOR sql_statement1 USING cwsId, cwsId, orderIdMDFilter;
    ELSE
       OPEN shipment_id_cursor FOR sql_statement1 USING cwsId;
    END IF;
   -- Skip rows before minrow, starting at 0
   vcurrow := 0;
   vidlist_done := 0;
   foundCurrentShipmentId := 0;
   <<c1_cursor_loop>>
   LOOP
          /*DBMS_OUTPUT.put_line('c1 loop, vcurrow = ' || to_char(vcurrow));*/
          EXIT WHEN vcurrow >= shipmentListMinRow;
          FETCH shipment_id_cursor INTO vShipmentId;
       IF shipment_id_cursor%NOTFOUND THEN
             vidlist_done := 1;
          EXIT;
       END IF;
          /*DBMS_OUTPUT.put_line('c1 loop, fetched id = ' || to_char(vshipment_id));*/
       vcurrow := vcurrow + 1;
   END LOOP c1_cursor_loop;
   -- If any rows left, add these id's, up to maxrow-1
   IF vidlist_done = 0 THEN
       <<c2_cursor_loop>>
       LOOP
              /*DBMS_OUTPUT.put_line('c2 loop, vcurrow = ' || to_char(vcurrow));*/
              EXIT WHEN vcurrow >= shipmentListMaxRow;
              FETCH shipment_id_cursor INTO vShipmentId;
           EXIT WHEN shipment_id_cursor%NOTFOUND;
           /*DBMS_OUTPUT.put_line('c2 loop, fetched id = ' || to_char(vshipment_id));*/
           INSERT INTO MAN_CONS_SHIPMENT_LIST_GTT (ROW_NUM, SHIPMENT_ID) VALUES (vcurrow, vShipmentId);
           vcurrow := vcurrow + 1;
           -- Save first shipment id in case user has no context for current shipment
           IF firstCwsShipmentId IS NULL THEN
                 firstCwsShipmentId := vShipmentId;
           END IF;
           IF vShipmentId = currentCwsShipmentId THEN
                 foundCurrentShipmentId := 1;
           END IF;
       END LOOP c2_cursor_loop;
--       COMMIT;
   END IF;
   CLOSE shipment_id_cursor;
   /* Debugging
   select count(1) into vgttcount from MAN_CONS_SHIPMENT_LIST_GTT;
   DBMS_OUTPUT.put_line('MAN_CONS_SHIPMENT_LIST_GTT count = ' || to_char(vgttcount));
   */    -- Materialize the resultset based on the id's and the order of the row #'s
   OPEN shipment_list_cursor FOR
          SELECT /*+ ordered first_rows */
          CWSSHP.TC_COMPANY_ID,
        CWSSHP.WS_SHIPMENT_ID, CWSSHP.TC_SHIPMENT_ID, CWSSHP.ORIG_SHIPMENT_ID,
        CWSSHP.O_CITY, CWSSHP.O_STATE_PROV, -- origin
        CWSSHP.D_CITY, CWSSHP.D_STATE_PROV, -- dest
            CWSSHP.TOTAL_DISTANCE,
        CWSSHP.FEASIBLE_MOT_ID,  -- feasible mode
        CWSSHP.FEASIBLE_EQUIPMENT_ID,
        CWSSHP.SIZE1_VALUE, CWSSHP.SIZE1_UOM_ID, CWSSHP.SIZE2_VALUE, CWSSHP.SIZE2_UOM_ID,  -- size1+UOM1, size2+UOM2
        CWSSHP.SIZE3_VALUE, CWSSHP.SIZE3_UOM_ID, CWSSHP.SIZE4_VALUE, CWSSHP.SIZE4_UOM_ID, CWSSHP.NUM_STOPS,  -- # stops
        CWSSHP.PICKUP_END_DTTM, CWSSHP.PICKUP_TZ,  -- pickup end + TZ
        CWSSHP.DELIVERY_END_DTTM, CWSSHP.DELIVERY_TZ,  -- delivery end + TZ
        CWSSHP.PROMOTE_TO_AVAIL_FLAG,  -- promotion flag
        CWSSHP.ESTIMATED_COST,  -- est. cost
        CWSSHP.HAS_HARD_CHECK_ERROR, CWSSHP.HAS_SOFT_CHECK_ERROR,  -- hard/soft check flags
        CWSSHP.NEEDS_VALIDATION,CWSSHP.BASELINE_COST, --# This has been included for 4r1 as baseline cost is not being fetched
        CWSSHP.DISTANCE_UOM,
        CWSSHP.CWS_PROCESS_ID,
        CWSSHP.IS_TIME_FEAS_ENABLED
          FROM MAN_CONS_SHIPMENT_LIST_GTT MCSLGTT, CWS_SHIPMENT CWSSHP
          WHERE CWSSHP.CONS_WORKSPACE_ID=cwsId
          AND    MCSLGTT.SHIPMENT_ID=CWSSHP.WS_SHIPMENT_ID
          ORDER BY MCSLGTT.ROW_NUM;
   -- Assigned order list: If passed a current cws shipment id.  If not, use the first shipment id
   -- retrieved above and set this in retCwsShipmentId.  Use order by clause (this requires dynamic sql...).
   IF (currentCwsShipmentId IS NULL) THEN
         retCwsShipmentId := firstCwsShipmentId;
   ELSE
      retCwsShipmentId := currentCwsShipmentId;
   END IF;
   sql_statement2 := 'select'
           || ' ORD.TC_COMPANY_ID,'
        || ' ORD.ORDER_ID, ORD.TC_ORDER_ID,'
        || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.O_FACILITY_ALIAS_ID AND TC_COMPANY_ID = ORD.TC_COMPANY_ID) O_FACILITY_NAME, '
        || ' ORD.O_CITY, ORD.O_STATE_PROV,'
        || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.D_FACILITY_ALIAS_ID AND TC_COMPANY_ID = ORD.TC_COMPANY_ID) D_FACILITY_NAME, '
        || ' ORD.D_CITY, ORD.D_STATE_PROV,'
        || ' ORD.PICKUP_START_DTTM, ORD.PICKUP_END_DTTM, ORD.PICKUP_TZ,'
        || ' ORD.DELIVERY_START_DTTM, ORD.DELIVERY_END_DTTM, ORD.DELIVERY_TZ,'
        || ' ORD.PROTECTION_LEVEL_ID,'
        || ' ORD.NORMALIZED_BASELINE_COST,'
        || ' CWSORD.SIZE1, CWSORD.SIZE1_UOM_ID,CWSORD.SIZE2, CWSORD.SIZE2_UOM_ID,'
        --CR69340
        || ' CWSORD.SIZE3, CWSORD.SIZE3_UOM_ID,CWSORD.SIZE4, CWSORD.SIZE4_UOM_ID,' -- CT18
        || ' ORD.PRODUCT_CLASS_ID ,'
        || ' CWSOM.PICKUP_STOP_SEQ, CWSOM.DELIVERY_STOP_SEQ, '
		|| ' (SELECT DISTINCT WAVOPT.WAVE_ID FROM WAVE_OPTION_LIST WAVOPT WHERE WAVOPT.FACILITY_ID=ORD.D_FACILITY_ID AND WAVOPT.WAVE_ID=ORD.WAVE_ID AND WAVOPT.WAVE_OPTION_ID=ORD.WAVE_OPTION_ID AND WAVOPT.DOCK_ID=ORD.D_DOCK_ID AND WAVOPT.TC_COMPANY_ID=ORD.TC_COMPANY_ID AND WAVOPT.MARK_FOR_DELETION = 0) WAVE_ID, '
		|| ' ORD.IS_PARTIALLY_PLANNED '
        || ' from CWS_ORDER_MOVEMENT CWSOM, CWS_ORDER CWSORD, ORDERS ORD'
		|| ' where (CWSOM.CONS_WORKSPACE_ID=:cwsId and CWSOM.WS_SHIPMENT_ID=:retCwsShipmentId)'
        || ' and   (CWSORD.CONS_WORKSPACE_ID=CWSOM.CONS_WORKSPACE_ID and CWSORD.ORDER_ID=CWSOM.ORDER_ID)'
        || ' and   (ORD.ORDER_ID=CWSORD.ORDER_ID)'
  || ' and   (CWSORD.IS_VISIBLE = 1)'
        || ' ' || assignedOrderListOrderBy;
   OPEN asg_order_list_cursor FOR sql_statement2 USING cwsId, retCwsShipmentId;
   -- Unassigned order list: Simple (I hope!).  Use order by clause (more dynamic sql ack).
   sql_statement3 := 'select'
           || ' ORD.TC_COMPANY_ID,'
        || ' ORD.ORDER_ID, ORD.TC_ORDER_ID,'
        || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.O_FACILITY_ALIAS_ID AND TC_COMPANY_ID = ORD.TC_COMPANY_ID) O_FACILITY_NAME, '
        || ' ORD.O_CITY, ORD.O_STATE_PROV,'
        || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.D_FACILITY_ALIAS_ID AND TC_COMPANY_ID = ORD.TC_COMPANY_ID) D_FACILITY_NAME, '
        || ' ORD.D_CITY, ORD.D_STATE_PROV,'
        || ' ORD.PICKUP_START_DTTM, ORD.PICKUP_END_DTTM, ORD.PICKUP_TZ,'
        || ' ORD.DELIVERY_START_DTTM, ORD.DELIVERY_END_DTTM, ORD.DELIVERY_TZ,'
        || ' ORD.PROTECTION_LEVEL_ID,'
        || ' ORD.NORMALIZED_BASELINE_COST,'
        || ' CWSORD.SIZE1, CWSORD.SIZE1_UOM_ID,CWSORD.SIZE2, CWSORD.SIZE2_UOM_ID,'
        --CR69340
        || ' CWSORD.SIZE3, CWSORD.SIZE3_UOM_ID,CWSORD.SIZE4, CWSORD.SIZE4_UOM_ID,' -- CT18
        || ' ORD.PRODUCT_CLASS_ID ,'
        || ' null PICKUP_STOP_SEQ, null DELIVERY_STOP_SEQ, '
		|| ' (SELECT DISTINCT WAVOPT.WAVE_ID FROM WAVE_OPTION_LIST WAVOPT WHERE WAVOPT.FACILITY_ID=ORD.D_FACILITY_ID AND WAVOPT.WAVE_ID=ORD.WAVE_ID AND WAVOPT.WAVE_OPTION_ID=ORD.WAVE_OPTION_ID AND WAVOPT.DOCK_ID=ORD.D_DOCK_ID AND WAVOPT.TC_COMPANY_ID=ORD.TC_COMPANY_ID AND WAVOPT.MARK_FOR_DELETION = 0) WAVE_ID, '
		|| ' ORD.IS_PARTIALLY_PLANNED '
        || ' from CWS_ORDER CWSORD, ORDERS ORD '
		|| ' where (CWSORD.CONS_WORKSPACE_ID=:cwsId)'
        || ' and (not exists'
        || '   (select 1 from CWS_ORDER_MOVEMENT CWSOM where CWSOM.CONS_WORKSPACE_ID=CWSORD.CONS_WORKSPACE_ID and CWSOM.ORDER_ID=CWSORD.ORDER_ID))'
        || ' and (ORD.ORDER_ID=CWSORD.ORDER_ID)'
  || ' and   (CWSORD.IS_VISIBLE = 1)'
        || ' ' || unassignedOrderListOrderBy;
   OPEN una_order_list_cursor FOR sql_statement3 USING cwsId;
   -- Compute statistics using 1 or more aggregate queries into output scalars
   -- Shipment count, Estimated cost (includes sorting cost), initial cost)
   SELECT COUNT(1),
             (NVL(SUM(CWS_SHIPMENT.ESTIMATED_COST),0)+NVL(SUM(CWS_SHIPMENT.XDOCK_SORT_COST),0)),
          SUM(CWS_SHIPMENT.INITIAL_COST)
          INTO statCwsShipmentCount, statEstimatedCost, statInitialCost
          FROM CWS_SHIPMENT
          WHERE CWS_SHIPMENT.CONS_WORKSPACE_ID=cwsId;
   -- Baseline cost will now be computed as the sum of the assigned orders' baseline costs.  Otherwise,
   -- we might count the baseline cost multiple times if the order is on multiple shipments (per MichelleF).
   -- But! If any order baseline cost is null for a shipment, that shipment has no baseline cost.  And
   -- if any orders in a movement network have null baseline cost, the whole network does.  Got it?  Here's the way:
    -- Step 0: incase there are any orders with Baseline cost as null, that shipment should have the null baseline and hence workspace
    -- Step 1: Get list of orders assigned to shipments that have null baseline cost.
    -- Step 2: Get list of CWS shipments for step 1 orders.
    -- Step 3: Get list of CWS order movements for step 2 shipments - none of these can count in the total
    -- Step 4: Sum the baseline cost of the orders assigned to shipment MINUS the orders on the movements from step 3.
    SELECT    COUNT(ORD1.order_id)
    INTO        order_null_check
    FROM        ORDERS ORD1
    WHERE        ORD1.ORDER_ID IN    (    SELECT    CWSOM1.ORDER_ID
                            FROM        CWS_ORDER_MOVEMENT CWSOM1
                            WHERE        CWSOM1.CONS_WORKSPACE_ID=cwsId
                        )
    AND        ORD1.NORMALIZED_BASELINE_COST IS NULL
    ;
    --If there are orders whose all shipments are not  inside
    --we show baseline cost of workspace as null
    SELECT COUNT(*) INTO partial_shipment_network FROM
    (
                SELECT CWS_ORDER.order_id FROM CWS_ORDER
                    WHERE cons_workspace_id= cwsId AND CWS_ORDER.is_partially_planned = 1
                UNION
                SELECT cwsor1.order_id FROM CWS_ORDER cwsor1 WHERE cons_workspace_id= cwsId AND EXISTS
                (
                            --for orders shipment_id can be there on order table or on order_movement
                            ( SELECT shipment_id FROM ORDERS  WHERE order_id = cwsor1.order_id
                                AND shipment_id IS NOT NULL
                                UNION
                                SELECT DISTINCT shipment_id FROM ORDER_MOVEMENT  WHERE order_id = cwsor1.order_id
                            )
                            MINUS --subtract inside shipments
                            SELECT DISTINCT orig_shipment_id FROM CWS_SHIPMENT,CWS_ORDER_MOVEMENT  WHERE
                                CWS_ORDER_MOVEMENT.order_id= cwsor1.order_id
                                AND CWS_SHIPMENT.ws_shipment_id= CWS_ORDER_MOVEMENT.ws_shipment_id
                                AND orig_shipment_id IS NOT NULL
                )
    );
    IF    (order_null_check    = 0 AND partial_shipment_network=0) THEN
    --SMPLIFYING THE BELOW QUERY TO AVOID UNNECESSARY INNER LOOPS.
    --select SUM(ORD4.NORMALIZED_BASELINE_COST) into statBaselineCost from ORDERS ORD4
    --  where ORD4.ORDER_ID in (
    --  (select distinct CWSOM4.ORDER_ID from CWS_ORDER_MOVEMENT CWSOM4
    --             where CWSOM4.CONS_WORKSPACE_ID=cwsId )
    --  MINUS
    --  (select distinct CWSOM3.order_id from CWS_ORDER_MOVEMENT CWSOM3
    --         where CWSOM3.CONS_WORKSPACE_ID=cwsId and CWSOM3.WS_SHIPMENT_ID in
    --    (select distinct CWSOM2.ws_shipment_id from CWS_ORDER_MOVEMENT CWSOM2
    --         where CWSOM2.CONS_WORKSPACE_ID=cwsId and CWSOM2.ORDER_ID in
    --      (select ORD1.order_id from orders ORD1
    --             where ORD1.ORDER_ID in
    --              (select distinct CWSOM1.ORDER_ID from CWS_ORDER_MOVEMENT CWSOM1
    --            where CWSOM1.CONS_WORKSPACE_ID=cwsId )
    --           and (ORD1.NORMALIZED_BASELINE_COST IS NULL)))));


      SELECT SUM(ORD4.NORMALIZED_BASELINE_COST) INTO statBaselineCost FROM ORDERS ORD4
      WHERE ORD4.ORDER_ID IN (
      (SELECT DISTINCT CWSOM4.ORDER_ID FROM CWS_ORDER_MOVEMENT CWSOM4
                  WHERE CWSOM4.CONS_WORKSPACE_ID=cwsId )
      ) AND ORD4.NORMALIZED_BASELINE_COST IS NULL;

    END IF;
    -- Total stops
    SELECT COUNT(1) INTO statTotalStopCount FROM CWS_STOP
             WHERE CWS_STOP.CONS_WORKSPACE_ID=cwsId ;
    -- Total order movements (orders on shipments)
    SELECT COUNT(1) INTO statTotalOrderMovementCount FROM CWS_ORDER_MOVEMENT
             WHERE CWS_ORDER_MOVEMENT.CONS_WORKSPACE_ID=cwsId ;
   -- Set return ref cursors
   shipmentListRefcur := shipment_list_cursor;
   assignedOrderListRefcur := asg_order_list_cursor;
   unassignedOrderListRefcur := una_order_list_cursor;
END load_Workspace;

-- Load all data required for display of the workspace at once.
PROCEDURE load_Workspace_SOD(
                 shipmentListRefcur OUT CURSOR_TYPE,        -- Cws shipment display info
              assignedOrderListRefcur OUT CURSOR_TYPE,    -- Cws assigned order display info
              unassignedOrderListRefcur OUT CURSOR_TYPE,-- Cws unassigned order display info
              movedOrderListRefcur OUT CURSOR_TYPE,     -- cws moded order list
              retCwsShipmentId OUT INTEGER,                  -- current draft shipment id as determined by the sproc
              statCwsShipmentCount OUT NUMBER,            -- total # Cws shipments in workspace
              statTotalStopCount OUT NUMBER,            -- total # of stops across all shipments
              statTotalOrderMovementCount OUT NUMBER,   -- total # of order movements across all shipments
              statBaselineCost OUT CWS_SHIPMENT.BASELINE_COST%TYPE,   -- total baseline cost for shipments
              statEstimatedCost OUT CWS_SHIPMENT.ESTIMATED_COST%TYPE, -- total estimated cost for shipments
              statInitialCost OUT CWS_SHIPMENT.INITIAL_COST%TYPE,     -- total initial cost for shipments
              shipperId INTEGER,                            -- company id
              cwsId INTEGER,                                -- workspace id
              currentCwsShipmentId INTEGER,                  -- current draft shipment id
              shipmentListOrderBy VARCHAR2,             -- order by clause for shipment list or empty string
              shipmentListMinRow NUMBER,                  -- first row to retrieve (0-based)
              shipmentListMaxRow NUMBER,                -- last row + 1 to retrieve (ex: 0..10 retrieves first 10 shipments)
              orderIdMDFilter NUMBER,                    -- for MD link, if non-null, joins to cws_order_movement to restrict list of shipment to this order id
              assignedOrderListOrderBy VARCHAR2,         -- order by clause for assigned orders list or empty string
              unassignedOrderListOrderBy VARCHAR2)       -- order by clause for unassigned orders list or empty string
              IS
   sql_statement1 VARCHAR(2000);
   sql_statement2 VARCHAR(2000);
   sql_statement3 VARCHAR(2000);
   sql_statement4 VARCHAR(2000);
   shipment_id_cursor CURSOR_TYPE;
   vcurrow NUMBER;
   vidlist_done NUMBER;
   vShipmentId CWS_SHIPMENT.WS_SHIPMENT_ID%TYPE;
   orderCount NUMBER;
   vgttcount NUMBER;
   shipment_list_cursor CURSOR_TYPE;
   asg_order_list_cursor CURSOR_TYPE;
   una_order_list_cursor CURSOR_TYPE;
   mov_order_list_cursor CURSOR_TYPE;
   firstCwsShipmentId CWS_SHIPMENT.WS_SHIPMENT_ID%TYPE;
   foundCurrentShipmentId NUMBER(1);
BEGIN
    -- First get the shipment id list for the given range in the correct order
    DELETE MAN_CONS_SHIPMENT_LIST_GTT;
    sql_statement1 := 'select WS_SHIPMENT_ID, COUNT(*) SORT_ORDER'
                    ||' from    CWS_ORDER_MOVEMENT '
                    ||' where    CONS_WORKSPACE_ID =:cwsId and order_id not in ( select order_id from cws_original_movement )'
                    ||' group by WS_SHIPMENT_ID   order by    sort_order desc ';
       OPEN shipment_id_cursor FOR sql_statement1 USING cwsId;
   -- Skip rows before minrow, starting at 0
   vcurrow := 0;
   vidlist_done := 0;
   foundCurrentShipmentId := 0;
   <<c1_cursor_loop>>
   LOOP
          /*DBMS_OUTPUT.put_line('c1 loop, vcurrow = ' || to_char(vcurrow));*/
          EXIT WHEN vcurrow >= shipmentListMinRow;
          FETCH shipment_id_cursor INTO vShipmentId,orderCount;
       IF shipment_id_cursor%NOTFOUND THEN
             vidlist_done := 1;
          EXIT;
       END IF;
          /*DBMS_OUTPUT.put_line('c1 loop, fetched id = ' || to_char(vshipment_id));*/
       vcurrow := vcurrow + 1;
   END LOOP c1_cursor_loop;
   -- If any rows left, add these id's, up to maxrow-1
   IF vidlist_done = 0 THEN
       <<c2_cursor_loop>>
       LOOP
              /*DBMS_OUTPUT.put_line('c2 loop, vcurrow = ' || to_char(vcurrow));*/
              EXIT WHEN vcurrow >= shipmentListMaxRow;
              FETCH shipment_id_cursor INTO vShipmentId, orderCount;
           EXIT WHEN shipment_id_cursor%NOTFOUND;
           /*DBMS_OUTPUT.put_line('c2 loop, fetched id = ' || to_char(vshipment_id));*/
           INSERT INTO MAN_CONS_SHIPMENT_LIST_GTT (ROW_NUM, SHIPMENT_ID) VALUES (vcurrow, vShipmentId);
           vcurrow := vcurrow + 1;
           -- Save first shipment id in case user has no context for current shipment
           IF firstCwsShipmentId IS NULL THEN
                 firstCwsShipmentId := vShipmentId;
           END IF;
           IF vShipmentId = currentCwsShipmentId THEN
                 foundCurrentShipmentId := 1;
           END IF;
       END LOOP c2_cursor_loop;
--       COMMIT;
   END IF;
   CLOSE shipment_id_cursor;
   /* Debugging
   select count(1) into vgttcount from MAN_CONS_SHIPMENT_LIST_GTT;
   DBMS_OUTPUT.put_line('MAN_CONS_SHIPMENT_LIST_GTT count = ' || to_char(vgttcount));
   */
   -- Materialize the resultset based on the id's and the order of the row #'s
   OPEN shipment_list_cursor FOR
          SELECT /*+ ordered first_rows */
          CWSSHP.TC_COMPANY_ID,
        CWSSHP.WS_SHIPMENT_ID, CWSSHP.TC_SHIPMENT_ID, CWSSHP.ORIG_SHIPMENT_ID,
        CWSSHP.O_CITY, CWSSHP.O_STATE_PROV, -- origin
        CWSSHP.D_CITY, CWSSHP.D_STATE_PROV, -- dest
        CWSSHP.FEASIBLE_MOT_ID,  -- feasible mode
        CWSSHP.SIZE1_VALUE, CWSSHP.SIZE1_UOM_ID, CWSSHP.SIZE2_VALUE, CWSSHP.SIZE2_UOM_ID,  -- size1+UOM1, size2+UOM2
        -- CT18
        CWSSHP.SIZE3_VALUE, CWSSHP.SIZE3_UOM_ID, CWSSHP.SIZE4_VALUE, CWSSHP.SIZE4_UOM_ID, CWSSHP.NUM_STOPS,  -- # stops
        CWSSHP.PICKUP_END_DTTM, CWSSHP.PICKUP_TZ,  -- pickup end + TZ
        CWSSHP.DELIVERY_END_DTTM, CWSSHP.DELIVERY_TZ,  -- delivery end + TZ
        CWSSHP.PROMOTE_TO_AVAIL_FLAG,  -- promotion flag
        CWSSHP.ESTIMATED_COST,  -- est. cost
        CWSSHP.HAS_HARD_CHECK_ERROR, CWSSHP.HAS_SOFT_CHECK_ERROR,  -- hard/soft check flags
        CWSSHP.NEEDS_VALIDATION,
        CWSSHP.IS_TIME_FEAS_ENABLED
          FROM MAN_CONS_SHIPMENT_LIST_GTT MCSLGTT, CWS_SHIPMENT CWSSHP
          WHERE CWSSHP.CONS_WORKSPACE_ID=cwsId
          AND    MCSLGTT.SHIPMENT_ID=CWSSHP.WS_SHIPMENT_ID
          ORDER BY MCSLGTT.ROW_NUM;
   -- Assigned order list: If passed a current cws shipment id.  If not, use the first shipment id
   -- retrieved above and set this in retCwsShipmentId.  Use order by clause (this requires dynamic sql...).
   IF (currentCwsShipmentId IS NULL) THEN
         retCwsShipmentId := firstCwsShipmentId;
   ELSE
      retCwsShipmentId := currentCwsShipmentId;
   END IF;
   sql_statement2 := 'select'
           || ' ORD.TC_COMPANY_ID,'
        || ' ORD.ORDER_ID, ORD.TC_ORDER_ID,'
        || ' ORD.O_CITY, ORD.O_STATE_PROV,'
        || ' ORD.D_CITY, ORD.D_STATE_PROV,'
        || ' ORD.PICKUP_START_DTTM, ORD.PICKUP_END_DTTM, ORD.PICKUP_TZ,'
        || ' ORD.DELIVERY_START_DTTM, ORD.DELIVERY_END_DTTM, ORD.DELIVERY_TZ,'
        || ' ORD.NORMALIZED_BASELINE_COST,'
        || ' CWSORD.SIZE1, CWSORD.SIZE1_UOM_ID,CWSORD.SIZE2, CWSORD.SIZE2_UOM_ID,'
        --CR69340
        || ' CWSORD.SIZE3, CWSORD.SIZE3_UOM_ID,CWSORD.SIZE4, CWSORD.SIZE4_UOM_ID,' -- CT18
        || ' ORD.PRODUCT_CLASS_ID ,'
        || ' CWSOM.PICKUP_STOP_SEQ, CWSOM.DELIVERY_STOP_SEQ, '
		|| ' (SELECT DISTINCT WAVOPT.WAVE_ID FROM WAVE_OPTION_LIST WAVOPT WHERE WAVOPT.FACILITY_ID=ORD.D_FACILITY_ID AND WAVOPT.WAVE_ID=ORD.WAVE_ID AND WAVOPT.WAVE_OPTION_ID=ORD.WAVE_OPTION_ID AND WAVOPT.DOCK_ID=ORD.D_DOCK_ID AND WAVOPT.TC_COMPANY_ID=ORD.TC_COMPANY_ID AND WAVOPT.MARK_FOR_DELETION = 0) WAVE_ID, '
		|| ' ORD.IS_PARTIALLY_PLANNED '
        || ' from CWS_ORDER_MOVEMENT CWSOM, CWS_ORDER CWSORD, ORDERS ORD '
		|| ' where (CWSOM.CONS_WORKSPACE_ID=:cwsId and CWSOM.WS_SHIPMENT_ID=:retCwsShipmentId)'
        || ' and   (CWSORD.CONS_WORKSPACE_ID=CWSOM.CONS_WORKSPACE_ID and CWSORD.ORDER_ID=CWSOM.ORDER_ID)'
        || ' and   (ORD.ORDER_ID=CWSORD.ORDER_ID)'
  || ' and   (CWSORD.IS_VISIBLE = 1)'
        || ' ' || assignedOrderListOrderBy;
   OPEN asg_order_list_cursor FOR sql_statement2 USING cwsId, retCwsShipmentId;
   -- Unassigned order list: Simple (I hope!).  Use order by clause (more dynamic sql ack).
   sql_statement3 := 'select'
           || ' ORD.TC_COMPANY_ID,'
        || ' ORD.ORDER_ID, ORD.TC_ORDER_ID,'
        || ' ORD.O_CITY, ORD.O_STATE_PROV,'
        || ' ORD.D_CITY, ORD.D_STATE_PROV,'
        || ' ORD.PICKUP_START_DTTM, ORD.PICKUP_END_DTTM, ORD.PICKUP_TZ,'
        || ' ORD.DELIVERY_START_DTTM, ORD.DELIVERY_END_DTTM, ORD.DELIVERY_TZ,'
        || ' ORD.NORMALIZED_BASELINE_COST,'
        || ' CWSORD.SIZE1, CWSORD.SIZE1_UOM_ID,CWSORD.SIZE2, CWSORD.SIZE2_UOM_ID,'
        --CR69340
        || ' CWSORD.SIZE3, CWSORD.SIZE3_UOM_ID,CWSORD.SIZE4, CWSORD.SIZE4_UOM_ID,' -- CT18
        || ' ORD.PRODUCT_CLASS_ID ,'
        || ' null PICKUP_STOP_SEQ, null DELIVERY_STOP_SEQ, '
		|| ' (SELECT DISTINCT WAVOPT.WAVE_ID FROM WAVE_OPTION_LIST WAVOPT WHERE WAVOPT.FACILITY_ID=ORD.D_FACILITY_ID AND WAVOPT.WAVE_ID=ORD.WAVE_ID AND WAVOPT.WAVE_OPTION_ID=ORD.WAVE_OPTION_ID AND WAVOPT.DOCK_ID=ORD.D_DOCK_ID AND WAVOPT.TC_COMPANY_ID=ORD.TC_COMPANY_ID AND WAVOPT.MARK_FOR_DELETION = 0) WAVE_ID, '
		|| ' ORD.IS_PARTIALLY_PLANNED '
        || ' from CWS_ORDER CWSORD, ORDERS ORD '
		|| ' where (CWSORD.CONS_WORKSPACE_ID=:cwsId)'
        || ' and (not exists'
        || '   (select 1 from CWS_ORDER_MOVEMENT CWSOM where CWSOM.CONS_WORKSPACE_ID=CWSORD.CONS_WORKSPACE_ID and CWSOM.ORDER_ID=CWSORD.ORDER_ID))'
        || ' and (ORD.ORDER_ID=CWSORD.ORDER_ID)'
  || ' and   (CWSORD.IS_VISIBLE = 1)'
        || ' ' || unassignedOrderListOrderBy;
   OPEN una_order_list_cursor FOR sql_statement3 USING cwsId;
   -- moded order list
   sql_statement4 := 'select'
           || ' ORD.CONS_WORKSPACE_ID,'
        || ' ORD.ORDER_ID,'
        || ' ORD.WS_SHIPMENT_ID, ORD.MOVEMENT_TYPE,'
        || ' ORD.ORDER_SHIPMENT_SEQ, ORD.TC_COMPANY_ID,'
        || ' ORD.PATH_SET_ID, ORD.PATH_ID,'
        || ' ORD.PICkUP_STOP_SEQ, ORD.DELIVERY_STOP_SEQ '
        || ' from    CWS_ORDER_MOVEMENT ORD,'
        || ' (    select WS_SHIPMENT_ID, COUNT(*) SORT_ORDER '
        || '           from    CWS_ORDER_MOVEMENT '
        || '           where    CONS_WORKSPACE_ID =:cwsId and order_id not in ( select order_id from cws_original_movement )'
        || '           group by WS_SHIPMENT_ID '
        || ' ) v_help '
        || ' where    ORD.CONS_WORKSPACE_ID =:cwsId and order_id not in ( select order_id from cws_original_movement )'
        || ' and    ORD.WS_SHIPMENT_ID = v_help.WS_SHIPMENT_ID '
        || ' order by    v_help.sort_order desc, ORD.ORDER_ID';
   OPEN mov_order_list_cursor FOR sql_statement4 USING cwsId, cwsId;
   -- Compute statistics using 1 or more aggregate queries into output scalars
   -- Shipment count, Estimated cost (includes sorting cost), initial cost)
   SELECT COUNT(1),
             (SUM(CWS_SHIPMENT.ESTIMATED_COST)+NVL(SUM(CWS_SHIPMENT.XDOCK_SORT_COST),0)),
          SUM(CWS_SHIPMENT.INITIAL_COST)
          INTO statCwsShipmentCount, statEstimatedCost, statInitialCost
          FROM CWS_SHIPMENT
          WHERE CWS_SHIPMENT.CONS_WORKSPACE_ID=cwsId;
   -- Baseline cost will now be computed as the sum of the assigned orders' baseline costs.  Otherwise,
   -- we might count the baseline cost multiple times if the order is on multiple shipments (per MichelleF).
   -- But! If any order baseline cost is null for a shipment, that shipment has no baseline cost.  And
   -- if any orders in a movement network have null baseline cost, the whole network does.  Got it?  Here's the way:
    -- Step 1: Get list of orders assigned to shipments that have null baseline cost.
    -- Step 2: Get list of CWS shipments for step 1 orders.
    -- Step 3: Get list of CWS order movements for step 2 shipments - none of these can count in the total
    -- Step 4: Sum the baseline cost of the orders assigned to shipment MINUS the orders on the movements from step 3.
    SELECT SUM(ORD4.NORMALIZED_BASELINE_COST) INTO statBaselineCost FROM ORDERS ORD4
      WHERE ORD4.ORDER_ID IN (
      (SELECT DISTINCT CWSOM4.ORDER_ID FROM CWS_ORDER_MOVEMENT CWSOM4
                  WHERE CWSOM4.CONS_WORKSPACE_ID=cwsId)
      MINUS
      (SELECT DISTINCT CWSOM3.order_id FROM CWS_ORDER_MOVEMENT CWSOM3
             WHERE CWSOM3.CONS_WORKSPACE_ID=cwsId AND CWSOM3.WS_SHIPMENT_ID IN
        (SELECT DISTINCT CWSOM2.ws_shipment_id FROM CWS_ORDER_MOVEMENT CWSOM2
             WHERE CWSOM2.CONS_WORKSPACE_ID=cwsId AND CWSOM2.ORDER_ID IN
          (SELECT ORD1.order_id FROM ORDERS ORD1
                 WHERE ORD1.ORDER_ID IN
                  (SELECT DISTINCT CWSOM1.ORDER_ID FROM CWS_ORDER_MOVEMENT CWSOM1
                WHERE CWSOM1.CONS_WORKSPACE_ID=cwsId)
               AND (ORD1.NORMALIZED_BASELINE_COST IS NULL)))));
    -- Total stops
    SELECT COUNT(1) INTO statTotalStopCount FROM CWS_STOP
             WHERE CWS_STOP.CONS_WORKSPACE_ID=cwsId;
    -- Total order movements (orders on shipments)
    SELECT COUNT(1) INTO statTotalOrderMovementCount FROM CWS_ORDER_MOVEMENT
             WHERE CWS_ORDER_MOVEMENT.CONS_WORKSPACE_ID=cwsId;
   -- Set return ref cursors
   shipmentListRefcur := shipment_list_cursor;
   assignedOrderListRefcur := asg_order_list_cursor;
   unassignedOrderListRefcur := una_order_list_cursor;
   movedOrderListRefcur := mov_order_list_cursor;
END load_Workspace_SOD;
        PROCEDURE load_Workspace2 ( shipmentListRefcur OUT CURSOR_TYPE,
                                    -- Cws shipment display info
                    assignedOrderListRefcur OUT CURSOR_TYPE,
                                    -- Cws assigned order display info
                    unassignedOrderListRefcur OUT CURSOR_TYPE,
                                    -- Cws unassigned order display info
                    assignedStopListRefcur OUT CURSOR_TYPE,
                                    -- Cws assigned stop display info
                    retCwsShipmentId OUT INTEGER,
                                    -- current draft shipment id as determined by the sproc
                    statCwsShipmentCount OUT NUMBER,
                                    -- total # Cws shipments in workspace
                    statTotalStopCount OUT NUMBER,
                                    -- total # of stops across all shipments
                    statTotalOrderMovementCount OUT NUMBER,
                                    -- total # of order movements across all shipments
                    statBaselineCost OUT CWS_SHIPMENT.BASELINE_COST%TYPE,
                                    -- total baseline cost for shipments
                    statEstimatedCost OUT CWS_SHIPMENT.ESTIMATED_COST%TYPE,
                                    -- total estimated cost for shipments
                    statInitialCost OUT CWS_SHIPMENT.INITIAL_COST%TYPE,
                                    -- total initial cost for shipments
                    modifiedMinRow OUT INTEGER,
                    modifiedMaxRow OUT INTEGER,
                    shipperId INTEGER,          -- company id
                    cwsId INTEGER,              -- workspace id
                    currentCwsShipmentId INTEGER,
                                    -- current draft shipment id
                    shipmentListOrderBy VARCHAR2,
                                    -- order by clause for shipment list or empty string
                    shipmentListMinRow NUMBER,  -- first row to retrieve (0-based)
                    shipmentListMaxRow NUMBER,  -- last row + 1 to retrieve (ex: 0..10 retrieves first 10 shipments)
                    orderIdMDFilter NUMBER,     -- for MD link, if non-null, joins to cws_order_movement to restrict list of shipment to this order id
                    assignedOrderListOrderBy VARCHAR2,
                                    -- order by clause for assigned orders list or empty string
                    unassignedOrderListOrderBy VARCHAR2,
            allowSplit NUMBER ) IS
        -- order by clause for unassigned orders list or empty string
        sql_statement1 VARCHAR ( 2000 );
        sql_statement2 VARCHAR ( 4000 );
        sql_statement3 VARCHAR ( 4000 );
        sql_statement4 VARCHAR ( 2000 );
        sql_statement5 VARCHAR ( 2000 );
        shipment_id_cursor CURSOR_TYPE;
        vcurrow NUMBER;
        vidlist_done NUMBER;
        vShipmentId CWS_SHIPMENT.WS_SHIPMENT_ID%TYPE;
        vgttcount NUMBER;
        shipment_list_cursor CURSOR_TYPE;
        asg_order_list_cursor CURSOR_TYPE;
        una_order_list_cursor CURSOR_TYPE;
        asg_stop_list_cursor CURSOR_TYPE;
        firstCwsShipmentId CWS_SHIPMENT.WS_SHIPMENT_ID%TYPE;
        foundCurrentShipmentId NUMBER ( 1 );
        order_null_check PLS_INTEGER := 0;
        partial_shipment_network PLS_INTEGER := 0;
        position_count_cursor CURSOR_TYPE;
        position_of_current_shipment NUMBER;
        min_max_difference NUMBER;
        shipmentListMinRow_local NUMBER;
        shipmentListMaxRow_local NUMBER;
        NoOfShipHavingEstimatedCost NUMBER;
        BEGIN
        -- First get the shipment id list for the given range in the correct order
        DELETE MAN_CONS_SHIPMENT_LIST_GTT;
        --sshrivastava>>copy min row and max row variable to local variables
        shipmentListMinRow_local := shipmentListMinRow;
        shipmentListMaxRow_local := shipmentListMaxRow;
        sql_statement4 := 'select cws_shipment.ws_shipment_id from cws_shipment' || ' where cws_shipment.cons_workspace_id=:cwsId ';
        IF orderIdMDFilter IS NOT NULL THEN
            sql_statement4 := sql_statement4 || ' and cws_shipment.ws_shipment_id in' || ' (select distinct cws_order_movement.ws_shipment_id from cws_order_movement where' || '  cws_order_movement.cons_workspace_id=:cwsId and' || '  cws_order_movement.order_id=:orderIdMD)';
        END IF;
        sql_statement4 := sql_statement4 || ' ' || shipmentListOrderBy;
        sql_statement4 := 'select rownum row_num, ws_shipment_id from (' || sql_statement4 || ')';
        --sshrivastava>>If current shipment Id has been passed to the procedure then find out the position of the current cws shipment first
        IF currentCwsShipmentId IS NOT NULL THEN
            sql_statement4 := 'select a.row_num from (' || sql_statement4 || ') a where a.ws_shipment_id = :currentCwsShipmentId';
            IF orderIdMDFilter IS NOT NULL THEN
             OPEN position_count_cursor
              FOR sql_statement4
            USING cwsId,
                  cwsId,
                  orderIdMDFilter,
                  currentCwsShipmentId;
            ELSE
             OPEN position_count_cursor
              FOR sql_statement4
            USING cwsId,
                  currentCwsShipmentId;
            END IF;
            FETCH position_count_cursor INTO position_of_current_shipment;
            CLOSE position_count_cursor;
            --sshrivastava>>min_max_difference will give number of records to be shown on the page
            min_max_difference := shipmentListMaxRow_local - shipmentListMinRow_local;
            IF position_of_current_shipment <= 0 THEN
            position_of_current_shipment := 0;
            --ELSE
            --position_of_current_shipment := position_of_current_shipment - 1;
            END IF;
            --increment or decrement min row and max row, till position of current shipment fall between them
            WHILE NOT ( position_of_current_shipment >= shipmentListMinRow_local AND position_of_current_shipment <= shipmentListMaxRow_local ) LOOP
            IF position_of_current_shipment > shipmentListMinRow_local THEN
                shipmentListMinRow_local := shipmentListMinRow_local + min_max_difference;
                shipmentListMaxRow_local := shipmentListMaxRow_local + min_max_difference;
                ELSIF position_of_current_shipment < shipmentListMinRow_local THEN shipmentListMinRow_local := shipmentListMinRow_local - min_max_difference;
                shipmentListMaxRow_local := shipmentListMaxRow_local - min_max_difference;
            END IF;
            END LOOP;
            --sshrivastava>>copy the min row and max row to modified min row and max row out parameters
            --these parameters will be used later to update the page number on list page
            modifiedMinRow := shipmentListMinRow_local;
            modifiedMaxRow := shipmentListMaxRow_local;
        END IF;
        sql_statement1 := 'select cws_shipment.ws_shipment_id from cws_shipment' || '  where cws_shipment.cons_workspace_id=:cwsId';
        -- Dynamically build and bind the select for just the id's
        IF orderIdMDFilter IS NOT NULL THEN
            sql_statement1 := sql_statement1 || ' and cws_shipment.ws_shipment_id in' || ' (select distinct cws_order_movement.ws_shipment_id from cws_order_movement where' || '  cws_order_movement.cons_workspace_id=:cwsId and' || '  cws_order_movement.order_id=:orderIdMD)';
        END IF;
        sql_statement1 := sql_statement1 || ' ' || shipmentListOrderBy;
        IF orderIdMDFilter IS NOT NULL THEN
             OPEN shipment_id_cursor
              FOR sql_statement1
            USING cwsId,
              cwsId,
              orderIdMDFilter;
        ELSE
             OPEN shipment_id_cursor
              FOR sql_statement1
            USING cwsId;
        END IF;
        -- Skip rows before minrow, starting at 0
        vcurrow := 0;
        vidlist_done := 0;
        foundCurrentShipmentId := 0;
        <<c1_cursor_loop>>
     LOOP
            /*DBMS_OUTPUT.put_line('c1 loop, vcurrow = ' || to_char(vcurrow));*/
            EXIT WHEN vcurrow >= shipmentListMinRow_local;
            FETCH shipment_id_cursor INTO vShipmentId;
            IF shipment_id_cursor%NOTFOUND THEN
            vidlist_done := 1;
            EXIT;
            END IF;
            /*DBMS_OUTPUT.put_line('c1 loop, fetched id = ' || to_char(vshipment_id));*/
            vcurrow := vcurrow + 1;
        END LOOP c1_cursor_loop;
        -- If any rows left, add these id's, up to maxrow-1
        IF vidlist_done = 0 THEN
            <<c2_cursor_loop>>
     LOOP
            /*DBMS_OUTPUT.put_line('c2 loop, vcurrow = ' || to_char(vcurrow));*/
            EXIT WHEN vcurrow >= shipmentListMaxRow_local;
            FETCH shipment_id_cursor INTO vShipmentId;
            EXIT WHEN shipment_id_cursor%NOTFOUND;
            /*DBMS_OUTPUT.put_line('c2 loop, fetched id = ' || to_char(vshipment_id));*/
            INSERT INTO MAN_CONS_SHIPMENT_LIST_GTT ( ROW_NUM,
                                 SHIPMENT_ID )
            VALUES ( vcurrow,
                 vShipmentId );
            vcurrow := vcurrow + 1;
            -- Save first shipment id in case user has no context for current shipment
            IF firstCwsShipmentId IS NULL THEN
                firstCwsShipmentId := vShipmentId;
            END IF;
            IF vShipmentId = currentCwsShipmentId THEN
                foundCurrentShipmentId := 1;
            END IF;
            END LOOP c2_cursor_loop;
--            COMMIT;
        END IF;
        CLOSE shipment_id_cursor;
        /* Debugging
        select count(1) into vgttcount from MAN_CONS_SHIPMENT_LIST_GTT;
        DBMS_OUTPUT.put_line('MAN_CONS_SHIPMENT_LIST_GTT count = ' || to_char(vgttcount));
         */
        -- Materialize the resultset based on the id's and the order of the row #'s
         OPEN shipment_list_cursor
          FOR SELECT /*+ ordered first_rows */ CWSSHP.TC_COMPANY_ID,
              CWSSHP.WS_SHIPMENT_ID,
              CWSSHP.TC_SHIPMENT_ID,
              CWSSHP.ORIG_SHIPMENT_ID,
              CWSSHP.O_CITY,
              CWSSHP.O_STATE_PROV,                          -- origin
              CWSSHP.D_CITY,
              CWSSHP.D_STATE_PROV,                          -- dest
              CWSSHP.TOTAL_DISTANCE,                        -- total distance
              CWSSHP.FEASIBLE_MOT_ID,                          -- feasible mode
              CWSSHP.FEASIBLE_EQUIPMENT_ID,                 -- feasible equipment
              CWSSHP.SIZE1_VALUE,
              CWSSHP.SIZE1_UOM_ID,
              CWSSHP.SIZE2_VALUE,
              CWSSHP.SIZE2_UOM_ID,                             -- size1+UOM1, size2+UOM2
              -- CT18
              CWSSHP.SIZE3_VALUE,
	      CWSSHP.SIZE3_UOM_ID,
	      CWSSHP.SIZE4_VALUE,
              CWSSHP.SIZE4_UOM_ID,
              CWSSHP.NUM_STOPS,                             -- # stops
              CWSSHP.PICKUP_END_DTTM,
              CWSSHP.PICKUP_TZ,                             -- pickup end + TZ
              CWSSHP.DELIVERY_END_DTTM,
              CWSSHP.DELIVERY_TZ,                           -- delivery end + TZ
              CWSSHP.PROMOTE_TO_AVAIL_FLAG,                 -- promotion flag
              CWSSHP.ESTIMATED_COST,                        -- est. cost
              CWSSHP.HAS_HARD_CHECK_ERROR,
              CWSSHP.HAS_SOFT_CHECK_ERROR,                  -- hard/soft check flags
              CWSSHP.NEEDS_VALIDATION,
              CWSSHP.BASELINE_COST,                          --# This has been included for 4r1 as baseline cost is not being fetched
              CWSSHP.CWS_PROCESS_ID,
              CWSSHP.IS_TIME_FEAS_ENABLED,
              CWSSHP.DISTANCE_UOM
         FROM MAN_CONS_SHIPMENT_LIST_GTT MCSLGTT,
              CWS_SHIPMENT CWSSHP
        WHERE CWSSHP.CONS_WORKSPACE_ID = cwsId
          AND MCSLGTT.SHIPMENT_ID = CWSSHP.WS_SHIPMENT_ID
        ORDER BY MCSLGTT.ROW_NUM;
        -- Assigned order list: If passed a current cws shipment id.  If not, use the first shipment id
        -- retrieved above and set this in retCwsShipmentId.  Use order by clause (this requires dynamic sql...).

        IF ( currentCwsShipmentId IS NULL ) THEN
            retCwsShipmentId := firstCwsShipmentId;
        ELSE
            retCwsShipmentId := currentCwsShipmentId;
        END IF;

        sql_statement2 := 'select'
            || ' ORD.TC_COMPANY_ID,'
            || ' ORD.ORDER_ID, ORD.TC_ORDER_ID,'
            || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.O_FACILITY_ALIAS_ID AND FACILITY_ID = ORD.O_FACILITY_ID) O_FACILITY_NAME, '
            || ' ORD.O_CITY, ORD.O_STATE_PROV,'
            || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.D_FACILITY_ALIAS_ID AND FACILITY_ID = ORD.D_FACILITY_ID) D_FACILITY_NAME, '
            || ' ORD.D_CITY, ORD.D_STATE_PROV,'
            || ' ORD.PICKUP_START_DTTM, ORD.PICKUP_END_DTTM, ORD.PICKUP_TZ,'
            || ' ORD.DELIVERY_START_DTTM, ORD.DELIVERY_END_DTTM, ORD.DELIVERY_TZ,'
            || ' ORD.PROTECTION_LEVEL_ID,'
            || ' ORD.NORMALIZED_BASELINE_COST,'
            || ' CWSORD.SIZE1, CWSORD.SIZE1_UOM_ID,CWSORD.SIZE2, CWSORD.SIZE2_UOM_ID,'
            --CR69340
            || ' CWSORD.SIZE3, CWSORD.SIZE3_UOM_ID,CWSORD.SIZE4, CWSORD.SIZE4_UOM_ID,' -- CT18
            || ' ORD.PRODUCT_CLASS_ID ,'
            || ' CWSOM.PICKUP_STOP_SEQ, CWSOM.DELIVERY_STOP_SEQ, ORD.WAVE_ID,CWSORD.CWS_PROCESS_ID, 0 , 0 '
            || ' from CWS_ORDER_MOVEMENT CWSOM, CWS_ORDER CWSORD, ORDERS ORD'
            || ' where (CWSOM.CONS_WORKSPACE_ID=:cwsId and CWSOM.WS_SHIPMENT_ID=:retCwsShipmentId)'
            || ' and   (CWSORD.CONS_WORKSPACE_ID=CWSOM.CONS_WORKSPACE_ID and CWSORD.ORDER_ID=CWSOM.ORDER_ID)'
            || ' and   (ORD.ORDER_ID=CWSORD.ORDER_ID)'
            || ' and   (CWSORD.IS_VISIBLE = 1)' || ' ' ;

        IF(allowSplit = 1) THEN
        sql_statement2 :=  sql_statement2
            || ' UNION '
            || ' select'
            || ' ORD.TC_COMPANY_ID,'
            || ' ORD.ORDER_ID, ORD.TC_ORDER_ID,'
            || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.O_FACILITY_ALIAS_ID AND FACILITY_ID = ORD.O_FACILITY_ID) O_FACILITY_NAME, '
            || ' ORD.O_CITY, ORD.O_STATE_PROV,'
            || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.D_FACILITY_ALIAS_ID AND FACILITY_ID = ORD.D_FACILITY_ID) D_FACILITY_NAME, '
            || ' ORD.D_CITY, ORD.D_STATE_PROV,'
            || ' ORD.PICKUP_START_DTTM, ORD.PICKUP_END_DTTM, ORD.PICKUP_TZ,'
            || ' ORD.DELIVERY_START_DTTM, ORD.DELIVERY_END_DTTM, ORD.DELIVERY_TZ,'
            || ' ORD.PROTECTION_LEVEL_ID,'
            || ' ORD.NORMALIZED_BASELINE_COST,'
            || ' CWSORDSPLIT.SIZE1, CWSORDSPLIT.SIZE1_UOM_ID,CWSORDSPLIT.SIZE2, CWSORDSPLIT.SIZE2_UOM_ID,'
            --CR69340
            || ' CWSORDSPLIT.SIZE3, CWSORDSPLIT.SIZE3_UOM_ID,CWSORDSPLIT.SIZE4, CWSORDSPLIT.SIZE4_UOM_ID,' -- CT18
            || ' ORD.PRODUCT_CLASS_ID ,'
            || ' CWSOM.PICKUP_STOP_SEQ, CWSOM.DELIVERY_STOP_SEQ, ORD.WAVE_ID, CWSORDSPLIT.ORDER_SPLIT_ID,ORDSPLIT.ORDER_SPLIT_SEQ '
            || ' (select cws_process_id from cws_shipment where ws_shipment_id = :retCwsShipmentId), '
            || ' CWSORDSPLIT.ORDER_SPLIT_ID,ORDSPLIT.ORDER_SPLIT_SEQ '
            || ' from CWS_ORDER_MOVEMENT CWSOM, CWS_ORDER_SPLIT CWSORDSPLIT,ORDER_SPLIT ORDSPLIT, ORDERS ORD'
            || ' where (CWSOM.CONS_WORKSPACE_ID=:cwsId and CWSOM.WS_SHIPMENT_ID=:retCwsShipmentId)'
            || ' and   (CWSORDSPLIT.CONS_WORKSPACE_ID=CWSOM.CONS_WORKSPACE_ID and CWSORDSPLIT.ORDER_SPLIT_ID=CWSOM.ORDER_SPLIT_ID)'
            || ' and   (ORD.ORDER_ID=CWSORDSPLIT.ORDER_ID)'
            || ' and (CWSORDSPLIT.ORDER_SPLIT_ID=ORDSPLIT.ORDER_SPLIT_ID)' || ' ' ;
        END IF;
        sql_statement2 := sql_statement2 || assignedOrderListOrderBy;

        IF(allowSplit = 1) THEN
            OPEN asg_order_list_cursor FOR sql_statement2    USING cwsId, retCwsShipmentId, retCwsShipmentId, cwsId, retCwsShipmentId;
        ELSE
            OPEN asg_order_list_cursor FOR sql_statement2 USING cwsId, retCwsShipmentId;
        END IF;

        -- Unassigned order list: Simple (I hope!).  Use order by clause (more dynamic sql ack).
        sql_statement3 := 'select'
            || ' ORD.TC_COMPANY_ID,'
            || ' ORD.ORDER_ID, ORD.TC_ORDER_ID,'
            || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.O_FACILITY_ALIAS_ID AND FACILITY_ID = ORD.O_FACILITY_ID) O_FACILITY_NAME, '
            || ' ORD.O_CITY, ORD.O_STATE_PROV,'
            || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.D_FACILITY_ALIAS_ID AND FACILITY_ID = ORD.D_FACILITY_ID) D_FACILITY_NAME, '
            || ' ORD.D_CITY, ORD.D_STATE_PROV,'
            || ' ORD.PICKUP_START_DTTM, ORD.PICKUP_END_DTTM, ORD.PICKUP_TZ,'
            || ' ORD.DELIVERY_START_DTTM, ORD.DELIVERY_END_DTTM, ORD.DELIVERY_TZ,'
            || ' ORD.PROTECTION_LEVEL_ID,'
            || ' ORD.NORMALIZED_BASELINE_COST,'
            || ' CWSORD.SIZE1, CWSORD.SIZE1_UOM_ID,CWSORD.SIZE2, CWSORD.SIZE2_UOM_ID,'
            --CR69340
            || ' CWSORD.SIZE3, CWSORD.SIZE3_UOM_ID,CWSORD.SIZE4, CWSORD.SIZE4_UOM_ID,' -- CT18
            || ' ORD.PRODUCT_CLASS_ID ,'
            || ' null PICKUP_STOP_SEQ, null DELIVERY_STOP_SEQ, ORD.WAVE_ID, 0, null, null '
            || ' from CWS_ORDER CWSORD, ORDERS ORD'
            || ' where (CWSORD.CONS_WORKSPACE_ID=:cwsId )'
            || ' and (not exists'
            || '   (select 1 from CWS_ORDER_MOVEMENT CWSOM where CWSOM.CONS_WORKSPACE_ID=CWSORD.CONS_WORKSPACE_ID and CWSOM.ORDER_ID=CWSORD.ORDER_ID))'
            || ' and (ORD.ORDER_ID=CWSORD.ORDER_ID)'
            || ' and   (CWSORD.IS_VISIBLE = 1)'
            || ' and   (ORD.PARENT_ORDER_ID is NULL)'
            || ' ' ;

        IF (allowSplit = 1) THEN
        sql_statement3 :=  sql_statement3
            || ' UNION '
            || 'select'
            || ' ORD.TC_COMPANY_ID,'
            || ' ORD.ORDER_ID, ORD.TC_ORDER_ID,'
            || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.O_FACILITY_ALIAS_ID AND FACILITY_ID = ORD.O_FACILITY_ID) O_FACILITY_NAME, '
            || ' ORD.O_CITY, ORD.O_STATE_PROV,'
            || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.D_FACILITY_ALIAS_ID AND FACILITY_ID = ORD.D_FACILITY_ID) D_FACILITY_NAME, '
            || ' ORD.D_CITY, ORD.D_STATE_PROV,'
            || ' ORD.PICKUP_START_DTTM, ORD.PICKUP_END_DTTM, ORD.PICKUP_TZ,'
            || ' ORD.DELIVERY_START_DTTM, ORD.DELIVERY_END_DTTM, ORD.DELIVERY_TZ,'
            || ' ORD.PROTECTION_LEVEL_ID,'
            || ' ORD.NORMALIZED_BASELINE_COST,'
            || ' CWSORDSPLIT.SIZE1, CWSORDSPLIT.SIZE1_UOM_ID,CWSORDSPLIT.SIZE2, CWSORDSPLIT.SIZE2_UOM_ID,'
            --CR69340
            || ' CWSORDSPLIT.SIZE3, CWSORDSPLIT.SIZE3_UOM_ID,CWSORDSPLIT.SIZE4, CWSORDSPLIT.SIZE4_UOM_ID,' -- CT18
            || ' ORD.PRODUCT_CLASS_ID ,'
            || ' 0 PICKUP_STOP_SEQ, 0 DELIVERY_STOP_SEQ,ORD.WAVE_ID, 0, CWSORDSPLIT.ORDER_SPLIT_ID,ORDSPLIT.ORDER_SPLIT_SEQ'
            || ' from CWS_ORDER_SPLIT CWSORDSPLIT,ORDER_SPLIT ORDSPLIT, ORDERS ORD'
            || ' where (CWSORDSPLIT.CONS_WORKSPACE_ID=:cwsId)'
            || ' and (not exists'
            || '   (select 1 from CWS_ORDER_MOVEMENT CWSOM where CWSOM.CONS_WORKSPACE_ID=CWSORDSPLIT.CONS_WORKSPACE_ID and CWSOM.ORDER_SPLIT_ID=CWSORDSPLIT.ORDER_SPLIT_ID))'
            || ' and (ORD.ORDER_ID=CWSORDSPLIT.ORDER_ID)'
            || ' and (CWSORDSPLIT.ORDER_SPLIT_ID=ORDSPLIT.ORDER_SPLIT_ID)'
            || ' and (ORD.PARENT_ORDER_ID is NULL)' || ' ';
        END IF;
        sql_statement2 := sql_statement2 || unassignedOrderListOrderBy;

        IF (allowSplit = 1) THEN
            OPEN una_order_list_cursor FOR sql_statement3    USING cwsId, cwsId;
        ELSE
            OPEN una_order_list_cursor FOR sql_statement3    USING cwsId;
        END IF;

    sql_statement5 := 'select CWS_STOP.WS_SHIPMENT_ID, CWS_STOP.STOP_SEQ, CWS_STOP.FACILITY_ALIAS_ID, '
        || 'CWS_STOP.DOCK_ID, CWS_STOP.ADDRESS,CWS_STOP.ADDRESS_2,CWS_STOP.ADDRESS_3, CWS_STOP.CITY, CWS_STOP.STATE_PROV, CWS_STOP.STOP_TZ, '
        || 'CWS_STOP.ARRIVAL_START_DTTM, CWS_STOP.ARRIVAL_END_DTTM, '
        || 'CWS_STOP.DEPARTURE_START_DTTM, CWS_STOP.DEPARTURE_END_DTTM, CWS_STOP.DISTANCE, '
        || '(select SUM(SIZE1) from cws_order where order_id in (select order_id from cws_order_movement '
        || ' where ((PICKUP_STOP_SEQ = cws_stop.stop_seq) OR ((DELIVERY_STOP_SEQ = cws_stop.stop_seq))) '
        || 'and CWS_STOP.WS_SHIPMENT_ID = WS_SHIPMENT_ID AND CONS_WORKSPACE_ID = CWS_STOP.CONS_WORKSPACE_ID)) SIZE1, '
        || '(select SUM(SIZE2) from cws_order where order_id in (select order_id from cws_order_movement '
        || ' where ((PICKUP_STOP_SEQ = cws_stop.stop_seq) OR ((DELIVERY_STOP_SEQ = cws_stop.stop_seq))) '
        || 'and CWS_STOP.WS_SHIPMENT_ID = WS_SHIPMENT_ID AND CONS_WORKSPACE_ID = CWS_STOP.CONS_WORKSPACE_ID)) SIZE2 , '
        || '(select SUM(SIZE3) from cws_order where order_id in (select order_id from cws_order_movement '
        || ' where ((PICKUP_STOP_SEQ = cws_stop.stop_seq) OR ((DELIVERY_STOP_SEQ = cws_stop.stop_seq))) '
        || 'and CWS_STOP.WS_SHIPMENT_ID = WS_SHIPMENT_ID AND CONS_WORKSPACE_ID = CWS_STOP.CONS_WORKSPACE_ID)) SIZE3  '
        || 'FROM CWS_STOP WHERE WS_SHIPMENT_ID =:retCwsShipmentId AND CONS_WORKSPACE_ID =:cwsId order by CWS_STOP.STOP_SEQ';

        -- NOT NEEDED IF retCwsShipmentId IS NOT NULL THEN
        OPEN asg_stop_list_cursor FOR sql_statement5 USING retCwsShipmentId, cwsId;
        --NOT NEEDED END IF;

        -- Compute statistics using 1 or more aggregate queries into output scalars
        -- Shipment count, Estimated cost (includes sorting cost), initial cost)
        SELECT COUNT ( 1 ),
               ( NVL ( SUM ( CWS_SHIPMENT.ESTIMATED_COST ),
                   0 ) + NVL ( SUM ( CWS_SHIPMENT.XDOCK_SORT_COST ),
                       0 ) ),
               SUM ( CWS_SHIPMENT.INITIAL_COST )
          INTO statCwsShipmentCount,
               statEstimatedCost,
               statInitialCost
          FROM CWS_SHIPMENT
         WHERE CWS_SHIPMENT.CONS_WORKSPACE_ID = cwsId;
             -- TT :: 48314
        SELECT COUNT ( 1 )
          INTO NoOfShipHavingEstimatedCost
          FROM CWS_SHIPMENT
         WHERE CWS_SHIPMENT.CONS_WORKSPACE_ID = cwsId AND CWS_SHIPMENT.ESTIMATED_COST IS NOT NULL;
        IF (NoOfShipHavingEstimatedCost != statCwsShipmentCount ) THEN
            statEstimatedCost := NULL;
        END IF;
        -- Baseline cost will now be computed as the sum of the assigned orders' baseline costs.  Otherwise,
        -- we might count the baseline cost multiple times if the order is on multiple shipments (per MichelleF).
        -- But! If any order baseline cost is null for a shipment, that shipment has no baseline cost.  And
        -- if any orders in a movement network have null baseline cost, the whole network does.  Got it?  Here's the way:
        -- Step 0: incase there are any orders with Baseline cost as null, that shipment should have the null baseline and hence workspace
        -- Step 1: Get list of orders assigned to shipments that have null baseline cost.
        -- Step 2: Get list of CWS shipments for step 1 orders.
        -- Step 3: Get list of CWS order movements for step 2 shipments - none of these can count in the total
        -- Step 4: Sum the baseline cost of the orders assigned to shipment MINUS the orders on the movements from step 3.
        SELECT COUNT ( ORD1.order_id )
          INTO order_null_check
          FROM ORDERS ORD1
         WHERE ORD1.ORDER_ID IN ( SELECT CWSOM1.ORDER_ID
                        FROM CWS_ORDER_MOVEMENT CWSOM1
                       WHERE CWSOM1.CONS_WORKSPACE_ID = cwsId )
           AND ORD1.NORMALIZED_BASELINE_COST IS NULL;
        --If there are orders whose all shipments are not  inside
        --we show baseline cost of workspace as null

        --simplifying below queries to avoid unnecessary inner loops.
        --IF  (order_null_check = 0) THEN
        --    SELECT SUM ( ORD4.NORMALIZED_BASELINE_COST )
        --      INTO statBaselineCost
        --      FROM ORDERS ORD4
        --     WHERE ORD4.ORDER_ID IN ( ( SELECT DISTINCT CWSOM4.ORDER_ID
        --                  FROM CWS_ORDER_MOVEMENT CWSOM4
        --                 WHERE CWSOM4.CONS_WORKSPACE_ID = cwsId ) MINUS ( SELECT DISTINCT CWSOM3.order_id
        --                                            FROM CWS_ORDER_MOVEMENT CWSOM3
        --                                           WHERE CWSOM3.CONS_WORKSPACE_ID = cwsId
        --                                             AND CWSOM3.WS_SHIPMENT_ID IN ( SELECT DISTINCT CWSOM2.ws_shipment_id
        --                                                              FROM CWS_ORDER_MOVEMENT CWSOM2
        --                                                             WHERE CWSOM2.CONS_WORKSPACE_ID = cwsId
        --                                                               AND CWSOM2.ORDER_ID IN ( SELECT ORD1.order_id
        --                                                                          FROM orders ORD1
        --                                                                         WHERE ORD1.ORDER_ID IN ( SELECT DISTINCT CWSOM1.ORDER_ID
        --                                                                                        FROM CWS_ORDER_MOVEMENT CWSOM1
        --                                                                                       WHERE CWSOM1.CONS_WORKSPACE_ID = cwsId )
        --                                                                           AND ( ORD1.NORMALIZED_BASELINE_COST IS NULL ) ) ) ) );

        IF  (order_null_check = 0) THEN
            SELECT SUM ( ORD4.NORMALIZED_BASELINE_COST )
              INTO statBaselineCost
              FROM ORDERS ORD4
             WHERE ORD4.ORDER_ID IN ( ( SELECT DISTINCT CWSOM4.ORDER_ID
                          FROM CWS_ORDER_MOVEMENT CWSOM4
                         WHERE CWSOM4.CONS_WORKSPACE_ID = cwsId ) )
                         AND ORD4.NORMALIZED_BASELINE_COST IS NOT NULL ;    ----- Added Dharmendra For Testing


        END IF;
        -- Total stops
        SELECT COUNT ( 1 )
          INTO statTotalStopCount
          FROM CWS_STOP
         WHERE CWS_STOP.CONS_WORKSPACE_ID = cwsId;
        -- Total order movements (orders on shipments)
        SELECT COUNT ( 1 )
          INTO statTotalOrderMovementCount
          FROM CWS_ORDER_MOVEMENT
         WHERE CWS_ORDER_MOVEMENT.CONS_WORKSPACE_ID = cwsId;
        -- Set return ref cursors
        shipmentListRefcur := shipment_list_cursor;
        assignedOrderListRefcur := asg_order_list_cursor;
        unassignedOrderListRefcur := una_order_list_cursor;
        assignedStopListRefcur := asg_stop_list_cursor;
        END load_Workspace2;

-- Mark the list of draft shipments as requiring refresh.
PROCEDURE mark_Shipments_For_Refresh(
              shipperId INTEGER,
              cwsId INTEGER,
              idListCsv VARCHAR2) IS
wsShipmentIdArr INT_ARRAY;
BEGIN
    wsShipmentIdArr := csv_to_int_array(idListCsv);
    p_mark_For_Refresh(cwsId, wsShipmentIdArr);
--    COMMIT;
END mark_Shipments_For_Refresh;
-- Update each WS shipment promote to available flag to the value passed in.
PROCEDURE set_Shipment_Promote_Flag(
                shipmentsUpdatedCsv OUT VARCHAR2,
              shipperId INTEGER,
              cwsId INTEGER,
              idListCsv VARCHAR2,
              doPromoteFlag NUMBER) IS
wsShipmentIdArr INT_ARRAY;
updatedIdArr INT_ARRAY;
BEGIN
    wsShipmentIdArr := csv_to_int_array(idListCsv);
    updatedIdArr := INT_ARRAY();
    IF wsShipmentIdArr.COUNT > 0 THEN
         FOR i IN wsShipmentIdArr.FIRST..wsShipmentIdArr.LAST LOOP
               UPDATE CWS_SHIPMENT SET PROMOTE_TO_AVAIL_FLAG=doPromoteFlag WHERE
                         cons_workspace_id=cwsId AND
                         ws_shipment_id=wsShipmentIdArr(i) AND PROMOTE_TO_AVAIL_FLAG <> 2;
               IF SQL%ROWCOUNT=1 THEN
                     updatedIdArr.EXTEND;
                  updatedIdArr(updatedIdArr.LAST) := wsShipmentIdArr(i);
               END IF;
        END LOOP;
    END IF;
--    COMMIT;
    shipmentsUpdatedCsv := int_array_to_csv(updatedIdArr);
END set_Shipment_Promote_Flag;
-- For draft shipments only, deconsolidate them.  This removes the order movements, stops, and the shipment
-- if it was not an existing OM shipment (if it is, then it requires an update).
-- Any original movements are left alone.  The flag to keep
-- the orders in the unassigned bucket means the orders stay in the workspace.  If the flag is false and
-- the orders are in use by other draft order movements, the order cannot be removed.  If the shipment is
-- an existing OM shipment, the orders must remain in the workspace.
-- @param keepOrdersInUnassignedBucket
--           0=don't keep if this is a orders-only draft shipment and not on other shipments in the workspace.
--           1=keep the orders in the workspace unassiged bucket regardless.
--          2=don't keep the orders even if a real OM shipment (for removing the shipment) unless they are
--            on another shipment in the workspace, Also -
--            this causes the real shipment to be removed and the original movements also.  This value is
--            generated internally only.
PROCEDURE deconsolidate_Draft_Shipment(
                 shipmentsDeconsCsv OUT VARCHAR2,
              shipperId INTEGER,
              cwsId INTEGER,
              idListCsv VARCHAR2,
              keepOrdersInUnassignedBucket NUMBER) IS
wsShipmentIdArr INT_ARRAY;
deconsIdArr INT_ARRAY;
orderOnShipmentArr INT_ARRAY;
orderSplitOnShipmentArr INT_ARRAY;
shipmentExists NUMBER(1);
omShipmentId CWS_SHIPMENT.ORIG_SHIPMENT_ID%TYPE;
KEEP_FLAG_REMOVE_IF_DRAFT NUMBER := 0;
KEEP_FLAG_KEEP NUMBER := 1;
KEEP_FLAG_REMOVE_SHIPMENT NUMBER := 2;

BEGIN
    wsShipmentIdArr := csv_to_int_array(idListCsv);
    deconsIdArr := INT_ARRAY();
    IF wsShipmentIdArr.COUNT > 0 THEN
         FOR i IN wsShipmentIdArr.FIRST..wsShipmentIdArr.LAST LOOP
            shipmentExists := NULL;


			BEGIN
				SELECT 1, ORIG_SHIPMENT_ID INTO shipmentExists, omShipmentId FROM CWS_SHIPMENT CWSSHP
                   WHERE CWSSHP.CONS_WORKSPACE_ID=cwsId
                   AND     CWSSHP.WS_SHIPMENT_ID=wsShipmentIdArr(i);
				EXCEPTION
				   WHEN NO_DATA_FOUND THEN
				   CONTINUE;
			END;



            IF shipmentExists IS NOT NULL THEN
                SELECT DISTINCT CWSOM.ORDER_ID BULK COLLECT INTO orderOnShipmentArr FROM CWS_ORDER_MOVEMENT CWSOM
                       WHERE CWSOM.CONS_WORKSPACE_ID=cwsId
                       AND     CWSOM.WS_SHIPMENT_ID=wsShipmentIdArr(i);
                SELECT DISTINCT CWSOM.ORDER_SPLIT_ID BULK COLLECT INTO orderSplitOnShipmentArr FROM CWS_ORDER_MOVEMENT CWSOM
                       WHERE CWSOM.CONS_WORKSPACE_ID=cwsId
                       AND     CWSOM.WS_SHIPMENT_ID=wsShipmentIdArr(i);
                DELETE FROM CWS_ORDER_MOVEMENT CWSOM
                       WHERE CWSOM.CONS_WORKSPACE_ID=cwsId
                       AND   CWSOM.WS_SHIPMENT_ID=wsShipmentIdArr(i);
                DELETE FROM CWS_STOP CWSST
                       WHERE CWSST.CONS_WORKSPACE_ID=cwsId
                       AND     CWSST.WS_SHIPMENT_ID=wsShipmentIdArr(i);
                -- Shipment record is not needed for true orders-only draft shipment or for remove operation.
                IF (omShipmentId IS NULL) OR
                   (keepOrdersInUnassignedBucket = KEEP_FLAG_REMOVE_SHIPMENT) THEN
					DELETE FROM CWS_VIRTUAL_TRIP CWSVT
                           WHERE CWSVT.CONS_WORKSPACE_ID=cwsId
                           AND  CWSVT.WS_SHIPMENT_ID=wsShipmentIdArr(i);

                    DELETE FROM CWS_ORIGINAL_VIRTUAL_TRIP CWSOVT
                           WHERE CWSOVT.CONS_WORKSPACE_ID=cwsId
                           AND  CWSOVT.WS_SHIPMENT_ID=wsShipmentIdArr(i);

                    DELETE FROM CWS_SHIPMENT CWSSHP
                           WHERE CWSSHP.CONS_WORKSPACE_ID=cwsId
                           AND     CWSSHP.WS_SHIPMENT_ID=wsShipmentIdArr(i);
                ELSE
                    UPDATE CWS_SHIPMENT SET NEEDS_REFRESH=1 WHERE
                         cons_workspace_id=cwsId AND
                         ws_shipment_id=wsShipmentIdArr(i);
                END IF;
                -- Remove original order movements if existing shipment being removed.
                IF (omShipmentId IS NOT NULL) AND
                   (keepOrdersInUnassignedBucket = KEEP_FLAG_REMOVE_SHIPMENT) THEN
                   DELETE FROM CWS_ORIGINAL_MOVEMENT CWSOM
                             WHERE CWSOM.CONS_WORKSPACE_ID=cwsId
                          AND    CWSOM.ORIG_SHIPMENT_ID=omShipmentId;
                END IF;
                deconsIdArr.EXTEND;
                deconsIdArr(deconsIdArr.LAST) := wsShipmentIdArr(i);
                -- If user wants orders out of workspace, also verify that no other draft shipments are using them.
                -- This flag is ignored for an existing OM shipment unless that shipment is being removed.
                IF (((keepOrdersInUnassignedBucket = KEEP_FLAG_REMOVE_IF_DRAFT) AND
                     (omShipmentId IS NULL)) OR
                    (keepOrdersInUnassignedBucket = KEEP_FLAG_REMOVE_SHIPMENT)) AND
                      ((orderOnShipmentArr.COUNT > 0) OR (orderSplitOnShipmentArr.COUNT > 0)) THEN
                          FORALL j IN orderOnShipmentArr.FIRST..orderOnShipmentArr.LAST
                         DELETE FROM CWS_ORDER CWSORD
                               WHERE CWSORD.CONS_WORKSPACE_ID=cwsId
                               AND     CWSORD.ORDER_ID=orderOnShipmentArr(j)
                               AND
                               ((SELECT COUNT(1) FROM CWS_ORDER_MOVEMENT CWSOM
                                          WHERE CWSOM.CONS_WORKSPACE_ID=cwsId
                                       AND   CWSOM.ORDER_ID=orderOnShipmentArr(j)) = 0);

                  FORALL j IN orderSplitOnShipmentArr.FIRST..orderSplitOnShipmentArr.LAST
                           DELETE FROM CWS_ORDER_SPLIT CWSORDSPLIT
                               WHERE CWSORDSPLIT.CONS_WORKSPACE_ID=cwsId
                               AND     CWSORDSPLIT.ORDER_SPLIT_ID=orderSplitOnShipmentArr(j)
                               AND
                               ((SELECT COUNT(1) FROM CWS_ORDER_MOVEMENT CWSOM
                                          WHERE CWSOM.CONS_WORKSPACE_ID=cwsId
                                       AND   CWSOM.ORDER_SPLIT_ID=orderSplitOnShipmentArr(j)) = 0);

               END IF;
            END IF;
--            COMMIT;
        END LOOP;
    END IF;
    shipmentsDeconsCsv := int_array_to_csv(deconsIdArr);
END deconsolidate_Draft_Shipment;
-- Load any data required for a draft shipment at once, based on operation flag and loader flag word.
-- Full OptiManage orders are not loaded here, that uses the order loader.
-- All lists are guaranteed to be ordered to make knitting faster, as follows:
-- Shipment List: WS Shipment Id
-- Movement List: WS Shipment Id, Order Id
-- Original Movement List: WS Shipment Id, Order Id
-- Stop List: WS Shipment Id, Stop Seq #
-- Display Order List: WS Shipment Id, Order Pickup Stop #, Order Delivery Stop #



PROCEDURE load_Draft_Shipments(
                 shipmentListRefcur OUT CURSOR_TYPE,        -- Cws shipment info
                 movementListRefcur OUT CURSOR_TYPE,        -- Cws order movement info
                 origMovementListRefcur OUT CURSOR_TYPE,    -- Cws order movement info
                 stopListRefcur OUT CURSOR_TYPE,        -- Cws stop info
                 orderSummaryListRefcur OUT CURSOR_TYPE,    -- CwsOrderSummary info
              shipperId INTEGER,                  -- company id
              cwsId INTEGER,                  -- workspace id
              cwsShipmentListCsv VARCHAR2,              -- specific list of draft shipments to load
              ldsOperationCode NUMBER,            -- a code # from the wrapper (see LDS_OP_*)
              ldsLoaderFlagWord NUMBER,            -- a bitmask from the wrapper (see LDS_LOAD_*)
              cwsProcessId NUMBER                -- cws Porcess Id representing group of orders in progress for accepting.
              )
              IS
   cwsShipmentIdArr INT_ARRAY;
   shipment_list_cursor CURSOR_TYPE;
   movement_list_cursor CURSOR_TYPE;
   origMovment_list_cursor CURSOR_TYPE;
   stop_list_cursor CURSOR_TYPE;
   order_summary_cursor CURSOR_TYPE;
   -- Operation codes (only 1 value may be used)
   LDS_OP_GET_SHIPMENT_ID_LIST INTEGER := 1;    -- Load given list of shipment id's
   LDS_OP_GET_NEEDS_REFRESH INTEGER := 2;        -- Load shipments needing refresh
   LDS_OP_GET_ALL_IN_WORKSPACE INTEGER := 3;    -- Load all shipments in workspace
   -- Load flags that can be OR'd together
   LDS_LOAD_SHIPMENT INTEGER := 0;                     -- CwsShipment's; must be on!  bitwhacking:
   LDS_LOAD_MOVEMENTS INTEGER := 1;                -- CwsOrderMovement's (current)
   LDS_LOAD_STOPS INTEGER := 2;                     -- CwsStop's
   LDS_LOAD_DISPLAY_ORDERS INTEGER := 4;        -- CwsOrderSummary's
   LDS_LOAD_REAL_ORDERS INTEGER := 8;            -- Order's (these are not loaded here)
   LDS_LOAD_ORIGINAL_MOVEMENTS INTEGER := 16;   -- CwsOrderMovement's (original)
   processId INTEGER := 0;
BEGIN
    IF (cwsProcessId > 0) THEN
        processId := cwsProcessId;
    END IF;

    -- First get the shipment id list for the specified operation, in the correct order, into the global temp table.
    DELETE MAN_CONS_SHIPMENT_LIST_GTT;
    IF ldsOperationCode = LDS_OP_GET_ALL_IN_WORKSPACE THEN
       INSERT INTO MAN_CONS_SHIPMENT_LIST_GTT (ROW_NUM, SHIPMENT_ID)
                 SELECT ROWNUM, CWSSHP.ws_shipment_id FROM CWS_SHIPMENT CWSSHP
                       WHERE CWSSHP.CONS_WORKSPACE_ID=cwsId AND CWSSHP.cws_process_id =processId
                     ORDER BY CWSSHP.WS_SHIPMENT_ID;
    ELSIF ldsOperationCode = LDS_OP_GET_NEEDS_REFRESH THEN
       INSERT INTO MAN_CONS_SHIPMENT_LIST_GTT (ROW_NUM, SHIPMENT_ID)
                 SELECT ROWNUM, CWSSHP.ws_shipment_id FROM CWS_SHIPMENT CWSSHP
                       WHERE CWSSHP.CONS_WORKSPACE_ID=cwsId AND CWSSHP.CWS_PROCESS_ID = processId
                     AND   CWSSHP.NEEDS_REFRESH=1
                     ORDER BY CWSSHP.WS_SHIPMENT_ID;
    ELSIF ldsOperationCode = LDS_OP_GET_SHIPMENT_ID_LIST THEN
        cwsShipmentIdArr := csv_to_int_array(cwsShipmentListCsv);
        FORALL i IN cwsShipmentIdArr.FIRST..cwsShipmentIdArr.LAST
               INSERT INTO MAN_CONS_SHIPMENT_LIST_GTT VALUES (cwsShipmentIdArr(i), cwsShipmentIdArr(i));
    ELSE
        NULL;  -- no shipments!
    END IF;
--    COMMIT;
   -- Materialize the shipment resultset based on the id's and the order of the row #'s
   -- WATCH: Keep in synch with CwsShipment ResultSet ctor
   OPEN shipment_list_cursor FOR
                  SELECT /*+ ORDERED FIRST_ROWS */
                CWS_SHIPMENT.CONS_WORKSPACE_ID CONS_WORKSPACE_ID,
                CWS_SHIPMENT.WS_SHIPMENT_ID,
                TC_COMPANY_ID,
                ORIG_SHIPMENT_ID,
                TC_SHIPMENT_ID,
                LAST_REFRESHED_DTTM,
                LAST_UPDATED_DTTM,
                NEEDS_REFRESH,
                NEEDS_RESEQUENCE,
                SHIPMENT_COMPS_ENABLED,
                SHIPMENT_COMPS_ENABLED,
                IS_HAZMAT,
                IS_PERISHABLE,
                   IS_FILO,
                   IS_COOLER_AT_NOSE,
                PROMOTE_TO_AVAIL_FLAG,
                HAS_HARD_CHECK_ERROR,
                HAS_SOFT_CHECK_ERROR,
                O_ADDRESS,
                O_CITY,
                O_STATE_PROV,
                O_POSTAL_CODE,
                O_COUNTY,
                O_COUNTRY_CODE,
                D_ADDRESS,
                D_CITY,
                D_STATE_PROV,
                D_POSTAL_CODE,
                D_COUNTY,
                D_COUNTRY_CODE,
                FEASIBLE_MOT_ID,
                FEASIBLE_EQUIPMENT_ID,
                FEASIBLE_SERVICE_LEVEL_ID,
                FEASIBLE_CARRIER_ID,
                DELIVERY_REQ,
                DROPOFF_PICKUP,
                PACKAGING,
                DSG_MOT_ID,
                DSG_EQUIPMENT_ID,
                DSG_SERVICE_LEVEL_ID,
                DSG_CARRIER_ID,
                SAVE_MOT_AS_DSG_FLAG,
                SAVE_EQUIP_AS_DSG_FLAG,
                SAVE_SL_AS_DSG_FLAG,
                SAVE_CC_AS_DSG_FLAG,
                PRODUCT_CLASS_ID,
                TRANS_RESP_CODE,
                BILLING_METHOD,
                PROTECTION_LEVEL_ID,
                COMMODITY_CLASS,
                INITIAL_COST,
                BASELINE_COST,
                ESTIMATED_COST,
                XDOCK_SORT_COST,
                TOTAL_DISTANCE,
                DIRECT_DISTANCE,
                OUT_OF_ROUTE_DISTANCE,
                CWS_SHIPMENT.DISTANCE_UOM,
                SIZE1_VALUE,
                SIZE1_UOM_ID,
                SIZE2_VALUE,
                SIZE2_UOM_ID,
                -- CT18
                SIZE3_VALUE,
                SIZE3_UOM_ID,
                SIZE4_VALUE,
                SIZE4_UOM_ID,
                NUM_STOPS,
                PICKUP_START_DTTM,
                PICKUP_END_DTTM,
                PICKUP_TZ,
                DELIVERY_START_DTTM,
                DELIVERY_END_DTTM,
                DELIVERY_TZ ,
                NEEDS_VALIDATION,
                FEASIBLE_DRIVER_TYPE,
                FEASIBLE_EQUIPMENT2_ID,
                WAVE_ID,
                DSG_EQUIPMENT2_ID,
                DSG_DRIVER_TYPE,
                SAVE_TRACT_AS_DSG_FLAG,
                SAVE_DT_AS_DSG_FLAG,
                IS_WAVE_MAN_CHANGED,
                MOVE_TYPE,
                ASG_MOT_ID,
                BOOKING_ID,
                FEASIBLE_CARRIER_CODE,
                IS_TIME_FEAS_ENABLED,
                DSG_CARRIER_CODE,
                STATIC_ROUTE_ID,
                CWS_PROCESS_ID,
                TRANSIT_TIME_MINUTES,
                IS_LOADING_SEQ_MODIFIED,
				(SELECT O_ST.FACILITY_ALIAS_ID FROM CWS_STOP O_ST WHERE O_ST.WS_SHIPMENT_ID=CWS_SHIPMENT.WS_SHIPMENT_ID AND O_ST.STOP_SEQ = 1)  O_FACILITY_ALIAS_ID,
				(SELECT D_ST.FACILITY_ALIAS_ID FROM CWS_STOP D_ST WHERE D_ST.WS_SHIPMENT_ID=CWS_SHIPMENT.WS_SHIPMENT_ID AND D_ST.STOP_SEQ = (SELECT MAX(ST.STOP_SEQ) FROM CWS_STOP ST WHERE ST.WS_SHIPMENT_ID=CWS_SHIPMENT.WS_SHIPMENT_ID))  D_FACILITY_ALIAS_ID,
                V_TRIP_ID,
                 V_TRIP_SEQ
              	FROM MAN_CONS_SHIPMENT_LIST_GTT MCSLGTT,
                CWS_SHIPMENT LEFT OUTER JOIN SHIPMENT_EXTN_TLM EXT
                ON EXT.SHIPMENT_ID= CWS_SHIPMENT.ORIG_SHIPMENT_ID
            WHERE CWS_SHIPMENT.CONS_WORKSPACE_ID=CWSID  AND CWS_SHIPMENT.CWS_PROCESS_ID = processId
                AND MCSLGTT.SHIPMENT_ID=CWS_SHIPMENT.WS_SHIPMENT_ID
            ORDER BY MCSLGTT.ROW_NUM;


   -- Materialize the current order movement resultset
   -- WATCH: Keep in synch with CwsOrderMovement ResultSet ctor
   IF BITAND(ldsLoaderFlagWord, LDS_LOAD_MOVEMENTS) <> 0 THEN
       OPEN movement_list_cursor FOR
          SELECT /*+ ORDERED FIRST_ROWS */
           CWS_ORDER_MOVEMENT.CONS_WORKSPACE_ID CONS_WORKSPACE_ID,
           ORDER_ID,
           WS_SHIPMENT_ID,
           MOVEMENT_TYPE,
           ORDER_SHIPMENT_SEQ,
           TC_COMPANY_ID,
           PATH_SET_ID,
           PATH_ID,
           PICKUP_STOP_SEQ,
           DELIVERY_STOP_SEQ,
           ORDER_LOADING_SEQ,
           IS_RELAY,
           COMPARTMENT_NO,
           ORDER_SPLIT_ID
            FROM MAN_CONS_SHIPMENT_LIST_GTT MCSLGTT, CWS_ORDER_MOVEMENT
           WHERE CWS_ORDER_MOVEMENT.CONS_WORKSPACE_ID = CWSID
             AND MCSLGTT.SHIPMENT_ID = CWS_ORDER_MOVEMENT.WS_SHIPMENT_ID
           ORDER BY MCSLGTT.ROW_NUM, CWS_ORDER_MOVEMENT.ORDER_ID;
     ELSE
        OPEN movement_list_cursor FOR SELECT NULL FROM dual;
    END IF;
   -- Materialize the original order movement resultset
   -- WATCH: Keep in synch with CwsOrderMovement ResultSet ctor
   IF BITAND(ldsLoaderFlagWord, LDS_LOAD_ORIGINAL_MOVEMENTS) <> 0 THEN
       OPEN origMovment_list_cursor FOR
             SELECT /*+ ORDERED FIRST_ROWS */
             CWS_ORIGINAL_MOVEMENT.CONS_WORKSPACE_ID CONS_WORKSPACE_ID,
             ORDER_ID,
             CWS_ORIGINAL_MOVEMENT.ORIG_SHIPMENT_ID ORIG_SHIPMENT_ID,
             MOVEMENT_TYPE,
             ORDER_SHIPMENT_SEQ,
             CWS_ORIGINAL_MOVEMENT.TC_COMPANY_ID TC_COMPANY_ID,
             PATH_SET_ID,
             PATH_ID,
             PICKUP_STOP_SEQ,
             DELIVERY_STOP_SEQ,
             ORDER_LOADING_SEQ,
             IS_RELAY,
             COMPARTMENT_NO,
             ORDER_SPLIT_ID
              FROM MAN_CONS_SHIPMENT_LIST_GTT MCSLGTT,
                   CWS_SHIPMENT,
                   CWS_ORIGINAL_MOVEMENT
             WHERE CWS_SHIPMENT.CONS_WORKSPACE_ID = CWSID AND CWS_SHIPMENT.CWS_PROCESS_ID = processId
               AND MCSLGTT.SHIPMENT_ID = CWS_SHIPMENT.WS_SHIPMENT_ID
               AND CWS_ORIGINAL_MOVEMENT.CONS_WORKSPACE_ID = CWSID
               AND CWS_ORIGINAL_MOVEMENT.ORIG_SHIPMENT_ID =
                   CWS_SHIPMENT.ORIG_SHIPMENT_ID
             ORDER BY MCSLGTT.ROW_NUM, CWS_ORIGINAL_MOVEMENT.ORDER_ID;
    ELSE
        OPEN origMovment_list_cursor FOR SELECT NULL FROM dual;
    END IF;
   -- Materialize the stop resultset
   -- WATCH: Keep in synch with CwsStop ResultSet ctor
   IF BITAND(ldsLoaderFlagWord, LDS_LOAD_STOPS) <> 0 THEN
       OPEN stop_list_cursor FOR
            SELECT /*+ ORDERED FIRST_ROWS */
             CONS_WORKSPACE_ID,
             WS_SHIPMENT_ID,
             STOP_SEQ,
             ON_STATIC_ROUTE,
             STOP_COMPS_ENABLED,
             TIME_WINDOW_COMPS_ENABLED,
             STOP_LOCATION_NAME,
             FACILITY_ID,
             FACILITY_ALIAS_ID,
             DOCK_ID,
             ADDRESS,
			 ADDRESS_2,
             ADDRESS_3,
             CITY,
             STATE_PROV,
             COUNTY,
             POSTAL_CODE,
             COUNTRY_CODE,
                   CWS_STOP.HANDLER,
             DROP_HOOK_INDICATOR,
             IS_APPT_REQD,
             STOP_TZ,
             ARRIVAL_START_DTTM,
             ARRIVAL_END_DTTM,
             DEPARTURE_START_DTTM,
             DEPARTURE_END_DTTM,
             CONTACT_FIRST_NAME,
             CONTACT_SURNAME,
             CONTACT_PHONE_NUMBER,
             DISTANCE,
             DISTANCE_UOM,
             SIZE_UOM_ID,
             CAPACITY,
             WAVE_ID,
             WAVE_OPTION_LIST_ID,
             WAVE_OPTION_ID,
	     (select WAVE_NAME from WAVE wa where wa.WAVE_ID = CWS_STOP.WAVE_ID AND (CWS_STOP.STOP_ACTION = 'DL' OR CWS_STOP.STOP_ACTION = 'RD') ) WAVE_NAME,
	     (select WAVE_OPTION_NAME from WAVE_OPTION wo where wo.WAVE_OPTION_ID = CWS_STOP.WAVE_OPTION_ID AND (CWS_STOP.STOP_ACTION = 'DL' OR CWS_STOP.STOP_ACTION = 'RD') ) WAVE_OPTION_NAME,
             IS_WAVE_MAN_CHANGED,
             SEGMENT_ID,
             SEGMENT_STOP_SEQ,
                STOP_ACTION,
               HITCH_TIME,
            STOP_TIME_MODIFIED,
            ORDER_WINDOW_VIOLATION,
            DOCK_HOUR_VIOLATION,
            WAVE_OPTION_VIOLATION
              FROM MAN_CONS_SHIPMENT_LIST_GTT MCSLGTT, CWS_STOP
             WHERE CWS_STOP.CONS_WORKSPACE_ID = CWSID
               AND MCSLGTT.SHIPMENT_ID = CWS_STOP.WS_SHIPMENT_ID
             ORDER BY MCSLGTT.ROW_NUM, CWS_STOP.STOP_SEQ;
    ELSE
        OPEN stop_list_cursor FOR SELECT NULL FROM dual;
   END IF;
   -- Materialize the display orders resultset
   -- WATCH: Keep in synch with CwsOrderSummary ResultSet ctor
   IF BITAND(ldsLoaderFlagWord, LDS_LOAD_DISPLAY_ORDERS) <> 0 THEN
       OPEN order_summary_cursor FOR
               SELECT /*+ ordered first_rows */
               ORD.ORDER_ID,
               ORD.TC_ORDER_ID,
               ORD.O_FACILITY_ALIAS_ID O_FACILITY_NAME,
               ORD.O_CITY,
               ORD.O_STATE_PROV,
               ORD.D_FACILITY_ALIAS_ID D_FACILITY_NAME,
               ORD.D_CITY,
               ORD.D_STATE_PROV,
               ORD.PICKUP_START_DTTM,
               ORD.PICKUP_END_DTTM,
               ORD.PICKUP_TZ,
               ORD.DELIVERY_START_DTTM,
               ORD.DELIVERY_END_DTTM,
               ORD.DELIVERY_TZ,
               ORD.PROTECTION_LEVEL_ID,
               ORD.NORMALIZED_BASELINE_COST NORMALIZED_BASELINE_COST,
               CWSORD.SIZE1,
               CWSORD.SIZE1_UOM_ID,
               CWSORD.SIZE2,
               CWSORD.SIZE2_UOM_ID,
               --CR69340
               CWSORD.SIZE3,
               CWSORD.SIZE3_UOM_ID,
               -- CT18
               CWSORD.SIZE4,
               CWSORD.SIZE4_UOM_ID,
               ORD.PRODUCT_CLASS_ID,
               CWSOM.PICKUP_STOP_SEQ,
               CWSOM.DELIVERY_STOP_SEQ,
               (CASE WHEN (SELECT COUNT(WAVOPT.WAVE_ID) FROM WAVE_OPTION_LIST WAVOPT WHERE WAVOPT.FACILITY_ID=ORD.D_FACILITY_ID AND WAVOPT.WAVE_ID=ORD.WAVE_ID AND WAVOPT.WAVE_OPTION_ID=ORD.WAVE_OPTION_ID AND WAVOPT.DOCK_ID=ORD.D_DOCK_ID AND WAVOPT.TC_COMPANY_ID=ORD.TC_COMPANY_ID AND WAVOPT.MARK_FOR_DELETION = 0)> 0 THEN ORD.WAVE_ID ELSE NULL END) WAVE_ID,
               ORD.IS_PARTIALLY_PLANNED,
               (CASE WHEN (SELECT COUNT(WAVOPT.WAVE_OPTION_ID) FROM WAVE_OPTION_LIST WAVOPT WHERE WAVOPT.FACILITY_ID=ORD.D_FACILITY_ID AND WAVOPT.WAVE_ID=ORD.WAVE_ID AND WAVOPT.WAVE_OPTION_ID=ORD.WAVE_OPTION_ID AND WAVOPT.DOCK_ID=ORD.D_DOCK_ID AND WAVOPT.TC_COMPANY_ID=ORD.TC_COMPANY_ID AND WAVOPT.MARK_FOR_DELETION = 0) > 0 THEN ORD.WAVE_OPTION_ID ELSE NULL END) WAVE_OPTION_ID,
               ORD.COMPARTMENT_NO,
               0,
               0, CWSOM.WS_SHIPMENT_ID
                FROM MAN_CONS_SHIPMENT_LIST_GTT MCSLGTT,
                     CWS_ORDER_MOVEMENT         CWSOM,
                     CWS_ORDER                  CWSORD,
                     ORDERS                     ORD
               WHERE (CWSOM.CONS_WORKSPACE_ID = cwsId AND CWSORD.CWS_PROCESS_ID =processId)
                 AND (MCSLGTT.SHIPMENT_ID = CWSOM.WS_SHIPMENT_ID)
                 AND (CWSORD.CONS_WORKSPACE_ID = CWSOM.CONS_WORKSPACE_ID)
                 AND (CWSORD.ORDER_ID = CWSOM.ORDER_ID)
               --  and (CWSORD.IS_VISIBLE = 1)
                 AND (ORD.ORDER_ID = CWSORD.ORDER_ID)
                 AND (CWSOM.ORDER_SPLIT_ID IS NULL)
              UNION
              SELECT ORD.ORDER_ID,
                     ORD.TC_ORDER_ID,
                     ORD.O_FACILITY_ALIAS_ID O_FACILITY_NAME,
                     ORD.O_CITY,
                     ORD.O_STATE_PROV,
                     ORD.D_FACILITY_ALIAS_ID D_FACILITY_NAME,
                     ORD.D_CITY,
                     ORD.D_STATE_PROV,
                     ORD.PICKUP_START_DTTM,
                     ORD.PICKUP_END_DTTM,
                     ORD.PICKUP_TZ,
                     ORD.DELIVERY_START_DTTM,
                     ORD.DELIVERY_END_DTTM,
                     ORD.DELIVERY_TZ,
                     ORD.PROTECTION_LEVEL_ID,
                     OS.BASELINE_COST NORMALIZED_BASELINE_COST,
                     CWSOS.SIZE1,
                     CWSOS.SIZE1_UOM_ID,
                     CWSOS.SIZE2,
                     CWSOS.SIZE2_UOM_ID,
                     --CR69340
                     CWSOS.SIZE3,
                     CWSOS.SIZE3_UOM_ID,
                     -- CT18
                     CWSOS.SIZE4,
                     CWSOS.SIZE4_UOM_ID,
                     ORD.PRODUCT_CLASS_ID,
                     CWSOM.PICKUP_STOP_SEQ,
                     CWSOM.DELIVERY_STOP_SEQ,
                     (CASE WHEN (SELECT COUNT(WAVOPT.WAVE_ID) FROM WAVE_OPTION_LIST WAVOPT WHERE WAVOPT.FACILITY_ID=ORD.D_FACILITY_ID AND WAVOPT.WAVE_ID=ORD.WAVE_ID AND WAVOPT.WAVE_OPTION_ID=ORD.WAVE_OPTION_ID AND WAVOPT.DOCK_ID=ORD.D_DOCK_ID AND WAVOPT.TC_COMPANY_ID=ORD.TC_COMPANY_ID AND WAVOPT.MARK_FOR_DELETION = 0)> 0 THEN ORD.WAVE_ID ELSE NULL END) WAVE_ID,
                     ORD.IS_PARTIALLY_PLANNED,
                     (CASE WHEN (SELECT COUNT(WAVOPT.WAVE_OPTION_ID) FROM WAVE_OPTION_LIST WAVOPT WHERE WAVOPT.FACILITY_ID=ORD.D_FACILITY_ID AND WAVOPT.WAVE_ID=ORD.WAVE_ID AND WAVOPT.WAVE_OPTION_ID=ORD.WAVE_OPTION_ID AND WAVOPT.DOCK_ID=ORD.D_DOCK_ID AND WAVOPT.TC_COMPANY_ID=ORD.TC_COMPANY_ID AND WAVOPT.MARK_FOR_DELETION = 0) > 0 THEN ORD.WAVE_OPTION_ID ELSE NULL END) WAVE_OPTION_ID,
                     OS.COMPARTMENT_NO,
                     CWSOS.ORDER_SPLIT_ID,
                     OS.ORDER_SPLIT_SEQ, CWSOM.WS_SHIPMENT_ID
                FROM MAN_CONS_SHIPMENT_LIST_GTT MCSLGTT,
                     CWS_ORDER_MOVEMENT         CWSOM,
                     CWS_ORDER                  CWSORD,
                     ORDER_SPLIT                OS,
                     CWS_ORDER_SPLIT            CWSOS,
                     ORDERS                     ORD
               WHERE (CWSOM.CONS_WORKSPACE_ID = cwsId AND CWSORD.CWS_PROCESS_ID = processId)
                 AND (MCSLGTT.SHIPMENT_ID = CWSOM.WS_SHIPMENT_ID)
                 AND (CWSORD.CONS_WORKSPACE_ID = CWSOM.CONS_WORKSPACE_ID)
                 AND (CWSORD.ORDER_ID = CWSOM.ORDER_ID)
                 AND (OS.ORDER_SPLIT_ID = CWSOM.ORDER_SPLIT_ID)
                 AND (CWSOS.ORDER_SPLIT_ID = CWSOM.ORDER_SPLIT_ID)
                 AND (ORD.ORDER_ID = CWSORD.ORDER_ID)
               ORDER BY PICKUP_STOP_SEQ, DELIVERY_STOP_SEQ, TC_ORDER_ID;
    ELSE
        OPEN order_summary_cursor FOR SELECT NULL FROM dual;
   END IF;
   -- Set return ref cursors
   shipmentListRefcur := shipment_list_cursor;
   movementListRefcur := movement_list_cursor;
   origMovementListRefcur := origMovment_list_cursor;
   stopListRefcur := stop_list_cursor;
   orderSummaryListRefcur := order_summary_cursor;
END load_Draft_Shipments;
-- External synchronization required for shipments and or orders.  Perform the task and shipments
-- will be refreshed.
PROCEDURE synch_Modified_Details(
              shipperId INTEGER,
              cwsId INTEGER,
              cwsShipmentIdListCsv VARCHAR2,
              orderIdListCsv VARCHAR2,
              orderSplitIdCsv VARCHAR2,
              size1UOM INTEGER,
              size2UOM INTEGER,
              size3UOM INTEGER,
              size4UOM INTEGER) IS -- CT18
   cwsShipmentIdArr INT_ARRAY;
   orderIdArr INT_ARRAY;
   orderSplitIdArr INT_ARRAY;
BEGIN
   cwsShipmentIdArr := csv_to_int_array(cwsShipmentIdListCsv);
   orderIdArr := csv_to_int_array(orderIdListCsv);
   orderSplitIdArr := csv_to_int_array(orderSplitIdCsv);
   -- Orders are simple: copy appropriate sizes and set last refreshed dttm.
   IF orderIdArr.COUNT > 0 THEN
         -- First do scalars
         FORALL i IN orderIdArr.FIRST..orderIdArr.LAST
         UPDATE CWS_ORDER CWSORD
            SET CWSORD.last_refreshed_dttm=SYSDATE,
                CWSORD.size1_uom_id=size1UOM,
                CWSORD.size2_uom_id=size2UOM,
                CWSORD.size3_uom_id=size3UOM,
                CWSORD.size4_uom_id=size4UOM -- CT18
            WHERE CWSORD.CONS_WORKSPACE_ID=cwsId AND CWSORD.ORDER_ID=orderIdArr(i);
      -- Then size rollups
         FORALL i IN orderIdArr.FIRST..orderIdArr.LAST
         UPDATE CWS_ORDER CWSORD
           SET (CWSORD.size1,CWSORD.size2,CWSORD.size3,CWSORD.size4) =
               (SELECT SUM(ORDER_LINE_ITEM_SIZE_VIEW.size_value*DECODE(SIZE_UOM_ID,size1UOM,1,0)) SIZE1,
                       SUM(ORDER_LINE_ITEM_SIZE_VIEW.size_value*DECODE(SIZE_UOM_ID,size2UOM,1,0)) SIZE2,
                       SUM(ORDER_LINE_ITEM_SIZE_VIEW.size_value*DECODE(SIZE_UOM_ID,size3UOM,1,0)) SIZE3,
                       SUM(ORDER_LINE_ITEM_SIZE_VIEW.size_value*DECODE(SIZE_UOM_ID,size4UOM,1,0)) SIZE4 -- CT18
                   FROM ORDER_LINE_ITEM_SIZE_VIEW
                   WHERE (ORDER_LINE_ITEM_SIZE_VIEW.order_id (+)=CWSORD.ORDER_ID)
                   AND     (ORDER_LINE_ITEM_SIZE_VIEW.order_id =orderIdArr(i))
            )
            WHERE CWSORD.CONS_WORKSPACE_ID=cwsId AND CWSORD.ORDER_ID=orderIdArr(i);
   END IF;

   IF(orderSplitIdArr.COUNT > 0) THEN
       FORALL i IN orderSplitIdArr.FIRST..orderSplitIdArr.LAST
               UPDATE CWS_ORDER  CWSORD
                SET CWSORD.last_refreshed_dttm=SYSDATE
                WHERE CWSORD.CONS_WORKSPACE_ID=cwsId AND CWSORD.ORDER_ID IN (SELECT ORDER_ID FROM CWS_ORDER_SPLIT WHERE CWS_ORDER_SPLIT.ORDER_SPLIT_ID=orderSplitIdArr(i));
    FORALL i IN orderSplitIdArr.FIRST..orderSplitIdArr.LAST
            UPDATE CWS_ORDER_SPLIT CWSORDSPLIT
                SET CWSORDSPLIT.last_refreshed_dttm=SYSDATE,
                    CWSORDSPLIT.size1_uom_id=size1UOM,
                    CWSORDSPLIT.size2_uom_id=size2UOM,
                    CWSORDSPLIT.size3_uom_id=size3UOM,
                    CWSORDSPLIT.size4_uom_id=size4UOM -- CT18
                WHERE CWSORDSPLIT.CONS_WORKSPACE_ID=cwsId AND CWSORDSPLIT.ORDER_SPLIT_ID=orderSplitIdArr(i);
    FORALL i IN orderSplitIdArr.FIRST..orderSplitIdArr.LAST
            UPDATE CWS_ORDER_SPLIT CWSORDSPLIT
                SET (size1,size2,size3,size4)= -- CT18
                (SELECT  SUM(ordsplitsize.split_size_value*(CASE WHEN ordsplitsize.split_size_uom_id=size1UOM THEN 1 ELSE 0 END)) size1,
                    SUM(ordsplitsize.split_size_value*(CASE WHEN ordsplitsize.split_size_uom_id=size2UOM THEN 1 ELSE 0 END)) size2,
                    SUM(ordsplitsize.split_size_value*(CASE WHEN ordsplitsize.split_size_uom_id=size3UOM THEN 1 ELSE 0 END)) size3,
                    SUM(ordsplitsize.split_size_value*(CASE WHEN ordsplitsize.split_size_uom_id=size4UOM THEN 1 ELSE 0 END)) size4 -- CT18
                FROM ORDER_SPLIT_SIZE_VIEW ordsplitsize WHERE ordsplitsize.order_split_id=cwsordsplit.order_split_id)
            WHERE CWSORDSPLIT.cons_workspace_id=cwsid AND CWSORDSPLIT.order_SPLIT_id=orderSplitIdArr(i);
   END IF;

    -- Shipments are more complex.  Certain fields need to be copied back over, all computation flags
    -- must be enabled, refresh needs to be *cleared* as well as last refreshed dttm.
    IF cwsShipmentIdArr.COUNT > 0 THEN
         -- First do scalars
         FORALL i IN cwsShipmentIdArr.FIRST..cwsShipmentIdArr.LAST
         UPDATE CWS_SHIPMENT CWSSHP
           SET CWSSHP.LAST_REFRESHED_DTTM=SYSDATE,
                  CWSSHP.SHIPMENT_COMPS_ENABLED=1,
               CWSSHP.NEEDS_REFRESH=0,
               CWSSHP.NEEDS_VALIDATION=0,
               CWSSHP.SIZE1_UOM_ID=size1UOM,
               CWSSHP.SIZE2_UOM_ID=size2UOM,
               -- CT18
               CWSSHP.SIZE3_UOM_ID=size3UOM,
               CWSSHP.SIZE4_UOM_ID=size4UOM
           WHERE CWSSHP.CONS_WORKSPACE_ID=cwsId AND CWSSHP.WS_SHIPMENT_ID=cwsShipmentIdArr(i);
         -- Then copy relevant values from the shipment record.
         FORALL i IN cwsShipmentIdArr.FIRST..cwsShipmentIdArr.LAST
         UPDATE CWS_SHIPMENT CWSSHP
        SET (CWSSHP.IS_HAZMAT,
        CWSSHP.IS_PERISHABLE,
        CWSSHP.IS_FILO,
        CWSSHP.IS_COOLER_AT_NOSE,
        CWSSHP.HAS_HARD_CHECK_ERROR,
        CWSSHP.HAS_SOFT_CHECK_ERROR,
        CWSSHP.FEASIBLE_MOT_ID,
        CWSSHP.FEASIBLE_EQUIPMENT_ID,
        CWSSHP.FEASIBLE_SERVICE_LEVEL_ID,
        CWSSHP.FEASIBLE_CARRIER_ID,
        CWSSHP.DELIVERY_REQ,
        CWSSHP.DROPOFF_PICKUP,
        CWSSHP.PACKAGING,
        CWSSHP.DSG_MOT_ID,
        CWSSHP.DSG_EQUIPMENT_ID,
        CWSSHP.DSG_SERVICE_LEVEL_ID,
        CWSSHP.DSG_CARRIER_ID,
        CWSSHP.PRODUCT_CLASS_ID,
        CWSSHP.TRANS_RESP_CODE,
        CWSSHP.PROTECTION_LEVEL_ID,
        CWSSHP.BILLING_METHOD,
        CWSSHP.COMMODITY_CLASS,
        CWSSHP.DISTANCE_UOM,
        CWSSHP.PICKUP_TZ,
        CWSSHP.DELIVERY_TZ,
        CWSSHP.INITIAL_COST,
        CWSSHP.BASELINE_COST,
        CWSSHP.ESTIMATED_COST,
        CWSSHP.XDOCK_SORT_COST) =
            (SELECT SHP.IS_HAZMAT,
            SHP.IS_PERISHABLE,
            SHP.IS_FILO,
        SHP.IS_COOLER_AT_NOSE,
            SHP.HAS_IMPORT_ERROR,
            SHP.HAS_SOFT_CHECK_ERROR,
             SHP.FEASIBLE_MOT_ID,
             SHP.FEASIBLE_EQUIPMENT_ID,
             SHP.FEASIBLE_SERVICE_LEVEL_ID,
             SHP.FEASIBLE_CARRIER_ID,
        SHP.DELIVERY_REQ,
        SHP.DROPOFF_PICKUP,
        SHP.PACKAGING,
        SHP.DSG_MOT_ID,
        SHP.DSG_EQUIPMENT_ID,
        SHP.DSG_SERVICE_LEVEL_ID,
        SHP.DSG_CARRIER_ID,
        SHP.PRODUCT_CLASS_ID,
        SHP.TRANS_RESP_CODE,
        SHP.PROTECTION_LEVEL_ID,
        SHP.BILLING_METHOD,
        SHP.COMMODITY_CLASS,
        SHP.DISTANCE_UOM,
        SHP.PICKUP_TZ,
        SHP.DELIVERY_TZ,
        SHP.ESTIMATED_COST,
        SHP.NORMALIZED_BASELINE_COST,
        SHP.ESTIMATED_COST,
        NULL -- TODO - need to compute the xdock sort here???  or maybe CWS does this and remove this column?  think.
        FROM SHIPMENT SHP
        WHERE SHP.shipment_id=CWSSHP.ORIG_SHIPMENT_ID
        )
           WHERE CWSSHP.CONS_WORKSPACE_ID=cwsId AND CWSSHP.WS_SHIPMENT_ID=cwsShipmentIdArr(i);

     --TT48808 : added
                 --update TimeWindow TF component of shipment
                 FORALL i IN cwsShipmentIdArr.FIRST..cwsShipmentIdArr.LAST
                   UPDATE CWS_SHIPMENT CWSSHP
                      SET CWSSHP.IS_TIME_FEAS_ENABLED = 0
                      WHERE CWSSHP.CONS_WORKSPACE_ID=cwsId
                       AND  CWSSHP.WS_SHIPMENT_ID=cwsShipmentIdArr(i)
                       AND  (
                          CWSSHP.IS_TIME_FEAS_ENABLED = 0
                          OR EXISTS                        --for shipemnt ouside MCW
                          (
                            SELECT 1
                            FROM SHIPMENT SHHP
                            WHERE SHHP.SHIPMENT_ID=cwsShipmentIdArr(i)
                              AND SHHP.IS_TIME_FEAS_ENABLED = 0
                          )
                          OR  EXISTS    --- if any stop's TF compomrnt id 0 shipemnt TF should be 0
                          ( SELECT 1
                              FROM  CWS_STOP CWSSTOP
                              WHERE CWSSTOP.WS_SHIPMENT_ID = cwsShipmentIdArr(i)
                           AND  CWSSTOP.TIME_WINDOW_COMPS_ENABLED = 0
                          )
                    );
      --TT48808 : added
      --update shipment Arrival/Departure DateTime only if Shipment TF is ON.
      FORALL i IN cwsShipmentIdArr.FIRST..cwsShipmentIdArr.LAST
          UPDATE CWS_SHIPMENT CWSSHP
           SET( CWSSHP.PICKUP_START_DTTM, CWSSHP.PICKUP_END_DTTM,
              CWSSHP.DELIVERY_START_DTTM, CWSSHP.DELIVERY_END_DTTM,
              CWSSHP.IS_TIME_FEAS_ENABLED ) =
            (SELECT  SHP.PICKUP_START_DTTM, SHP.PICKUP_END_DTTM,
                 SHP.DELIVERY_START_DTTM, SHP.DELIVERY_END_DTTM,
                 SHP.IS_TIME_FEAS_ENABLED
              FROM SHIPMENT SHP
              WHERE SHP.shipment_id = CWSSHP.ORIG_SHIPMENT_ID
            )
           WHERE CWSSHP.CONS_WORKSPACE_ID=cwsId AND CWSSHP.WS_SHIPMENT_ID=cwsShipmentIdArr(i)
         AND CWSSHP.IS_TIME_FEAS_ENABLED = 1;

         -- Then update the size rollups from the shipment commodity sizes.
         FORALL i IN cwsShipmentIdArr.FIRST..cwsShipmentIdArr.LAST
         UPDATE CWS_SHIPMENT CWSSHP
           SET (CWSSHP.SIZE1_VALUE, CWSSHP.SIZE2_VALUE, CWSSHP.SIZE3_VALUE, CWSSHP.SIZE4_VALUE) =
           (SELECT SUM(SHIPMENT_COMMODITY.size_value*DECODE(SIZE_UOM_ID,size1UOM,1,0)) SIZE1,
                   SUM(SHIPMENT_COMMODITY.size_value*DECODE(SIZE_UOM_ID,size2UOM,1,0)) SIZE2,
                   -- CT18
                   SUM(SHIPMENT_COMMODITY.size_value*DECODE(SIZE_UOM_ID,size3UOM,1,0)) SIZE3,
                   SUM(SHIPMENT_COMMODITY.size_value*DECODE(SIZE_UOM_ID,size4UOM,1,0)) SIZE4
            FROM SHIPMENT_COMMODITY
            WHERE SHIPMENT_COMMODITY.shipment_id=CWSSHP.ORIG_SHIPMENT_ID
            AND SHIPMENT_COMMODITY.shipment_id=cwsShipmentIdArr(i)
           )
           WHERE CWSSHP.CONS_WORKSPACE_ID=cwsId AND CWSSHP.WS_SHIPMENT_ID=cwsShipmentIdArr(i);
      -- TT48808 : modified
      -- Update Stop DateTime only if Shipment TF is ON
         -- Then do stop-level scalars to reenable computations and time window cals
         FORALL i IN cwsShipmentIdArr.FIRST..cwsShipmentIdArr.LAST
               UPDATE CWS_STOP CWSST
                 SET ( CWSST.STOP_COMPS_ENABLED,
                   CWSST.TIME_WINDOW_COMPS_ENABLED,
                   CWSST.ARRIVAL_START_DTTM, CWSST.ARRIVAL_END_DTTM,
                   CWSST.DEPARTURE_START_DTTM, CWSST.DEPARTURE_END_DTTM
                     ) =
                     ( SELECT  1,1,ST.ARRIVAL_START_DTTM, ST.ARRIVAL_END_DTTM,
                           ST.DEPARTURE_START_DTTM, ST.DEPARTURE_END_DTTM
                    FROM CWS_SHIPMENT CWSSHP, STOP ST
                    WHERE CWSSHP.CONS_WORKSPACE_ID=cwsId
                     AND  CWSSHP.WS_SHIPMENT_ID=cwsShipmentIdArr(i)
                     AND  CWSST.CONS_WORKSPACE_ID=CWSSHP.CONS_WORKSPACE_ID
                     AND  CWSST.WS_SHIPMENT_ID=CWSSHP.WS_SHIPMENT_ID
                     AND  ST.SHIPMENT_ID=CWSSHP.ORIG_SHIPMENT_ID
                     AND  ST.STOP_SEQ=CWSST.STOP_SEQ
                  -- and  CWSSHP.IS_TIME_FEAS_ENABLED = 1 -- The flag will be turned off on edit dates in shipment section
                  )
                      WHERE EXISTS
                    (  SELECT 1
                          FROM  CWS_SHIPMENT CWSSHP
                          WHERE CWSSHP.CONS_WORKSPACE_ID = cwsId
                            AND CWSSHP.WS_SHIPMENT_ID = cwsShipmentIdArr(i)
                         -- and CWSSHP.IS_TIME_FEAS_ENABLED = 1
                    )
                    AND CWSST.CONS_WORKSPACE_ID=cwsId
                        AND CWSST.WS_SHIPMENT_ID=cwsShipmentIdArr(i);


      -- TT48808 : modified
         -- Then do stop-level relevant values from the stop records
         FORALL i IN cwsShipmentIdArr.FIRST..cwsShipmentIdArr.LAST
         UPDATE CWS_STOP CWSST
           SET (CWSST.HANDLER, CWSST.DROP_HOOK_INDICATOR, CWSST.IS_APPT_REQD, CWSST.STOP_TZ,
                       CWSST.CONTACT_FIRST_NAME, CWSST.CONTACT_SURNAME, CWSST.CONTACT_PHONE_NUMBER,
                CWSST.DISTANCE, CWSST.DISTANCE_UOM) =
           (SELECT ST.HANDLER, ST.DROP_HOOK_INDICATOR, ST.IS_APPT_REQD, ST.STOP_TZ,
                       ST.CONTACT_FIRST_NAME, ST.CONTACT_SURNAME, ST.CONTACT_PHONE_NUMBER,
                ST.DISTANCE, ST.DISTANCE_UOM
                   FROM CWS_SHIPMENT CWSSHP, STOP ST
                WHERE CWSSHP.CONS_WORKSPACE_ID=cwsId
                AND      CWSSHP.WS_SHIPMENT_ID=cwsShipmentIdArr(i)
                AND      CWSST.CONS_WORKSPACE_ID=CWSSHP.CONS_WORKSPACE_ID
                AND      CWSST.WS_SHIPMENT_ID=CWSSHP.WS_SHIPMENT_ID
                AND      ST.SHIPMENT_ID=CWSSHP.ORIG_SHIPMENT_ID
                AND      ST.STOP_SEQ=CWSST.STOP_SEQ
            )
            WHERE CWSST.CONS_WORKSPACE_ID=cwsId AND CWSST.WS_SHIPMENT_ID=cwsShipmentIdArr(i);
    END IF;
--    COMMIT;
END synch_Modified_Details;
-- This is for unassigned orders only.  It removes them from the workspace and returns which
-- ones were removed.
PROCEDURE remove_Orders_From_Workspace(
                 ordersRemovedCsv OUT VARCHAR2,
              shipperId INTEGER,
              cwsId INTEGER,
              idListCsv VARCHAR2,
              onlySplit INTEGER,
              isSplit INTEGER) IS
   orderIdArr INT_ARRAY;
   orderRemovedIdArr INT_ARRAY := INT_ARRAY();
   isAggregated INTEGER(1);
   order_id_cursor CURSOR_TYPE;
   sql_statement1 VARCHAR(10000);
   orderId INTEGER;
   bFound INTEGER(1);
BEGIN
   orderIdArr := csv_to_int_array(idListCsv);
   sql_statement1 :='select distinct order_id  from order_Split ' ||
         'where order_split_id in (' || IDLISTCSV || ')';
   FOR i IN orderIdArr.FIRST..orderIdArr.LAST LOOP
       IF isSplit = 0 THEN
			SELECT COUNT(*) INTO bFound FROM CWS_ORDER
            WHERE CWS_ORDER.CONS_WORKSPACE_ID=cwsId AND CWS_ORDER.ORDER_ID=orderIdArr(i);

            if bFound =0  then
              isAggregated :=0;
            else
              SELECT CWS_ORDER.is_aggregated_in_cws INTO isAggregated FROM CWS_ORDER
              WHERE CWS_ORDER.CONS_WORKSPACE_ID=cwsId AND CWS_ORDER.ORDER_ID=orderIdArr(i);
            end if;
      ELSE
         isAggregated := 0;
      END IF;
       IF isAggregated = 0 THEN
           IF onlySplit = 0 THEN
            IF isSplit = 0 THEN
                DELETE FROM CWS_ORDER WHERE
                CWS_ORDER.CONS_WORKSPACE_ID=cwsId AND CWS_ORDER.ORDER_ID=orderIdArr(i);
            ELSE
                DELETE FROM CWS_ORDER_SPLIT WHERE
                CWS_ORDER_SPLIT.CONS_WORKSPACE_ID=cwsId AND CWS_ORDER_SPLIT.ORDER_SPLIT_ID=orderIdArr(i);
            END IF;
        ELSE
            DELETE FROM CWS_ORDER_SPLIT WHERE
                CWS_ORDER_SPLIT.CONS_WORKSPACE_ID=cwsId AND CWS_ORDER_SPLIT.ORDER_ID=orderIdArr(i);
            UPDATE CWS_ORDER SET has_split=0,is_visible=1 WHERE
                CWS_ORDER.CONS_WORKSPACE_ID=cwsId AND CWS_ORDER.ORDER_ID=orderIdArr(i);
        END IF;

       ELSE
         UPDATE CWS_ORDER SET is_visible = 0 WHERE
            CWS_ORDER.CONS_WORKSPACE_ID=cwsId AND CWS_ORDER.ORDER_ID=orderIdArr(i);
       END IF;
       IF SQL%ROWCOUNT = 1 THEN
             orderRemovedIdArr.EXTEND;
          orderRemovedIdArr(orderRemovedIdArr.LAST) := orderIdArr(i);
       END IF;
   END LOOP;
   IF isSplit = 1 THEN
       OPEN order_id_cursor FOR sql_statement1;
       FETCH order_id_cursor INTO orderId;
       SELECT COUNT(order_id) INTO bFound FROM
               CWS_ORDER_SPLIT WHERE cons_workspace_id=cwsId AND order_id=orderId;
            IF bFound=0 THEN
               DELETE FROM CWS_ORDER WHERE
         CWS_ORDER.CONS_WORKSPACE_ID=cwsId AND CWS_ORDER.ORDER_ID=orderId;
            END IF;
       CLOSE order_id_cursor;
   END IF;
--   COMMIT;
   ordersRemovedCsv := int_array_to_csv(orderRemovedIdArr);
END remove_Orders_From_Workspace;
-- Removes draft shipment(s) and all related info possible
PROCEDURE remove_Shipment_From_Workspace(
                 ShipmentsRemovedCsv OUT VARCHAR2,
              shipperId INTEGER,
              cwsId INTEGER,
              idListCsv VARCHAR2) IS
   keepOrdersInUnassignedBucket NUMBER := 2;     -- signify remove operation
BEGIN
     deconsolidate_Draft_Shipment(
                 ShipmentsRemovedCsv,
              shipperId,
              cwsId,
              idListCsv,
              keepOrdersInUnassignedBucket);
END remove_Shipment_From_Workspace;
-- PRIVATE Add external orders to the workspace if not already in WS
FUNCTION p_add_Order_To_Unassigned(
              orderId INTEGER,
              isSplit INTEGER,
               shipperId INTEGER,
              cwsId INTEGER,
              size1UOM INTEGER,
              size2UOM INTEGER,
              --CR69340
              size3UOM INTEGER,
              size4UOM INTEGER -- CT18
        ) RETURN NUMBER IS
  bFound NUMBER(1);
  bAdded NUMBER(1);
  hasSplit NUMBER(1);
  bActualAdded NUMBER(1);
BEGIN
    bAdded := 0;
    bActualAdded := 0;

    IF isSplit = 0 THEN
        SELECT COUNT(order_id) INTO bFound FROM
           CWS_ORDER WHERE cons_workspace_id=cwsId AND order_id=orderId;
        IF bFound=0 THEN
                INSERT INTO CWS_ORDER
             (cons_workspace_id, order_id, last_refreshed_dttm,
              --CR69340
                from_shipment_flag, size1, size1_uom_id, size2, SIZE2_UOM_ID,size3, SIZE3_UOM_ID,size4, SIZE4_UOM_ID) -- CT18
             SELECT cwsId, orderId, SYSDATE,
                         DECODE(ORDERS.ORDER_STATUS, 5, 0, 1) FROM_SHIPMENT,
                         SUM(ORDER_LINE_ITEM_SIZE_VIEW.size_value*DECODE(SIZE_UOM_ID,size1UOM,1,0)) SIZE1, size1UOM,
                         SUM(ORDER_LINE_ITEM_SIZE_VIEW.size_value*DECODE(SIZE_UOM_ID,size2UOM,1,0)) SIZE2, size2UOM,
                         --CR69340
                         SUM(ORDER_LINE_ITEM_SIZE_VIEW.size_value*DECODE(SIZE_UOM_ID,size3UOM,1,0)) SIZE3, size3UOM,
                         SUM(ORDER_LINE_ITEM_SIZE_VIEW.size_value*DECODE(SIZE_UOM_ID,size4UOM,1,0)) SIZE4, size4UOM -- CT18

                 FROM ORDERS, ORDER_LINE_ITEM_SIZE_VIEW, ORDER_LINE_ITEM -- not joining with order_baseline_cost (TT17523)
                 WHERE (ORDERS.order_id=orderId)
                     AND (ORDER_LINE_ITEM_SIZE_VIEW.order_id (+)=ORDERS.order_id)
                     --AND (ORDER_LINE_ITEM_SIZE.tc_company_id (+)=ORDERS.tc_company_id)
                             AND ORDER_LINE_ITEM.LINE_ITEM_ID (+)= ORDER_LINE_ITEM_SIZE_VIEW.LINE_ITEM_ID
                                  AND (ORDER_LINE_ITEM.IS_CANCELLED = 0 OR ORDER_LINE_ITEM.IS_CANCELLED IS NULL)
                     -- (TT17523 - not joining with order_baseline_cost) and (order_baseline_cost.order_id (+)=orders.order_id) and (order_baseline_cost.tc_company_id (+)=orders.tc_company_id)
                     GROUP BY ORDERS.ORDER_STATUS;
                --TT 24770 fix. Update the partially_planned flag from orders
                 UPDATE CWS_ORDER CWSORD SET CWSORD.is_partially_planned =
                (SELECT is_partially_planned FROM ORDERS WHERE order_id =orderId )
                WHERE CWSORD.order_id = orderId AND CWSORD.cons_workspace_id=cwsId;
            SELECT COUNT(ORDER_ID) INTO bActualAdded FROM CWS_ORDER WHERE ORDER_ID = orderId and cons_workspace_id = cwsId;

            IF bActualAdded > 0 THEN
                    bAdded := 1;
                    END IF;
      END IF;

    ELSE
        SELECT COUNT(order_split_id) INTO bFound FROM
                   CWS_ORDER_SPLIT WHERE cons_workspace_id=cwsId AND order_split_id=orderId;
                IF bFound=0 THEN
                        INSERT INTO CWS_ORDER_SPLIT
                     (cons_workspace_id, order_split_id, order_id, last_refreshed_dttm,
                         --CR69340
                        size1, size1_uom_id, size2, SIZE2_UOM_ID, size3, SIZE3_UOM_ID,size4, SIZE4_UOM_ID) -- CT18
                     SELECT cwsId, orderId, ORDER_SPLIT.order_id, SYSDATE,
                        SUM(ORDER_SPLIT_SIZE_VIEW.split_size_value*DECODE(split_SIZE_UOM_id,size1UOM,1,0)) AS SIZE1, size1UOM,
                        SUM(ORDER_SPLIT_SIZE_VIEW.split_size_value*DECODE(split_SIZE_UOM_id,size2UOM,1,0)) AS SIZE2, size2UOM, --CR69340
                        SUM(ORDER_SPLIT_SIZE_VIEW.split_size_value*DECODE(split_SIZE_UOM_id,size3UOM,1,0)) AS SIZE3, size3UOM,
                        SUM(ORDER_SPLIT_SIZE_VIEW.split_size_value*DECODE(split_SIZE_UOM_id,size4UOM,1,0)) AS SIZE4, size4UOM  -- CT18
                     FROM ORDER_SPLIT, ORDER_SPLIT_SIZE_VIEW
                  WHERE (ORDER_SPLIT.order_split_id=orderId)
                        AND ORDER_SPLIT_SIZE_VIEW.order_split_id (+)= ORDER_SPLIT.order_split_id
                        and order_split.is_cancelled = 0
                                     GROUP BY ORDER_SPLIT.order_split_status,ORDER_SPLIT.order_id;
                    bAdded := 1;
      END IF;
  END IF;



  RETURN bAdded;
END p_add_Order_To_Unassigned;
-- PRIVATE Returns 1 if any orders in the give order id array are found in the workspace.
FUNCTION p_Check_Orders_In_Workspace(
         cwsId INTEGER,
         orderIdArr INT_ARRAY,
         beginIdx INTEGER,
         endIdx INTEGER) RETURN NUMBER IS
bFound NUMBER(1);
BEGIN
     bFound := 0;
     FOR i IN beginIdx..endIdx LOOP
            SELECT COUNT(order_id) INTO bFound FROM
           CWS_ORDER WHERE cons_workspace_id=cwsId AND order_id=orderIdArr(i);
         IF bFound = 1 THEN
             EXIT;
         END IF;
     END LOOP;
     RETURN bFound;
END p_Check_Orders_In_Workspace;
-- Delete shipments that have no orders and do not exist outside the workspace
-- Private method does not do a commit, caller must.
FUNCTION p_clean_shipments(cwsId INTEGER, shipperId INTEGER, wsShipmentArr INT_ARRAY)
RETURN INT_ARRAY
IS
    wsShipmentFinalArr INT_ARRAY;
    v_num_orders_on_shipment INTEGER;
    v_shipments_removed VARCHAR2(1000);
    v_existing_shipment INTEGER;
BEGIN
  wsShipmentFinalArr := INT_ARRAY();
  IF wsShipmentArr.COUNT > 0 THEN
    FOR j IN wsShipmentArr.FIRST..wsShipmentArr.LAST LOOP
        SELECT COUNT(1) INTO v_num_orders_on_shipment FROM CWS_ORDER_MOVEMENT WHERE
                (cons_workspace_id = cwsid)
            AND (ws_shipment_id = wsShipmentArr(j));
        SELECT COUNT(1) INTO v_existing_shipment FROM CWS_SHIPMENT WHERE
                (cons_workspace_id = cwsid)
            AND (ws_shipment_id = wsShipmentArr(j))
            AND (orig_shipment_id IS NOT NULL);
         IF v_num_orders_on_shipment > 0 OR
           v_existing_shipment = 1 THEN
            wsShipmentFinalArr.EXTEND;
            wsShipmentFinalArr(wsShipmentFinalArr.LAST) := wsShipmentArr(j);
        ELSE
            -- check if the shipment still exists - the list might have had duplicates
            SELECT COUNT(1) INTO v_existing_shipment FROM CWS_SHIPMENT WHERE
                    (cons_workspace_id = cwsid)
                AND (ws_shipment_id = wsShipmentArr(j));
            IF v_existing_shipment>0 THEN
                remove_Shipment_From_Workspace(v_shipments_removed,
                                           shipperId,
                                           cwsId,
                                           wsShipmentArr(j)
                                           );
            END IF;
        END IF;
    END LOOP;
  END IF;
  RETURN wsShipmentFinalArr;
END;
-- Mark shipment(s) for refresh.  Private method does not do a commit, caller must.
PROCEDURE p_mark_For_Refresh(cwsId INTEGER, wsShipmentArr INT_ARRAY) IS
BEGIN
    IF wsShipmentArr.COUNT > 0 THEN
         FORALL i IN wsShipmentArr.FIRST..wsShipmentArr.LAST
               UPDATE CWS_SHIPMENT SET NEEDS_REFRESH=1 WHERE
                         cons_workspace_id=cwsId AND
                         ws_shipment_id=wsShipmentArr(i);
    END IF;
END;
PROCEDURE p_ReEnable_Comps(cwsId INTEGER, wsShipmentArr INT_ARRAY) IS
BEGIN
    IF wsShipmentArr.COUNT > 0 THEN
         FORALL i IN wsShipmentArr.FIRST..wsShipmentArr.LAST
               UPDATE CWS_SHIPMENT SET SHIPMENT_COMPS_ENABLED=1 WHERE
                         cons_workspace_id=cwsId AND
                         ws_shipment_id=wsShipmentArr(i);
    END IF;
END;
FUNCTION csv_to_int_array(csv_string VARCHAR2)
RETURN INT_ARRAY
IS
  crt_position  NUMBER;
  next_position NUMBER;
  crt_string        VARCHAR2(30000) := NULL;
  new_csv_string    VARCHAR2(30000) := NULL;
  cnt NUMBER;
  ret_int_array INT_ARRAY;
BEGIN
  ret_int_array := INT_ARRAY();
  -- remove the apostrophes
  new_csv_string := REPLACE( csv_string, '''' );
  IF new_csv_string IS NULL OR LENGTH( new_csv_string ) = 0
  THEN
      ret_int_array.EXTEND;
        RETURN ret_int_array;
  END IF;
  -- initialization
  crt_position  := 1;
  next_position := INSTR( new_csv_string, ',', crt_position );
  cnt := 0;
  -- get all matches
  WHILE ( next_position <> 0 )
  LOOP
        crt_string := LTRIM( RTRIM( SUBSTR( new_csv_string, crt_position, next_position - crt_position ) ) );
      IF LENGTH(crt_string) <> 0 THEN
           ret_int_array.EXTEND;
         cnt := cnt + 1;
         IF crt_string='<null>' THEN
            ret_int_array(cnt) := NULL;
         ELSE
            ret_int_array(cnt) := TO_NUMBER(crt_string);
         END IF;
      END IF;
      crt_position := next_position + 1;
      next_position := INSTR( new_csv_string, ',', crt_position );
  END LOOP;
  -- get the last piece after the last comma
  crt_string := LTRIM( RTRIM( SUBSTR( new_csv_string, crt_position ) ) );
  IF LENGTH( crt_string ) <> 0 THEN
       ret_int_array.EXTEND;
     cnt := cnt + 1;
     IF crt_string='<null>' THEN
        ret_int_array(cnt) := NULL;
     ELSE
        ret_int_array(cnt) := TO_NUMBER(crt_string);
     END IF;
  END IF;
  RETURN ret_int_array;
END csv_to_int_array;
FUNCTION int_array_to_csv(intArr INT_ARRAY)
RETURN VARCHAR
IS
  strRet VARCHAR(30000);
BEGIN
  strRet := '';
  IF intArr.COUNT > 0 THEN
      FOR i IN intArr.FIRST..intArr.LAST LOOP
            IF i > 1 THEN
               strRet := strRet || ',';
          END IF;
          IF intArr(i) IS NULL THEN
             strRet := strRet || '<null>';
          ELSE
               strRet := strRet || TO_CHAR(intArr(i));
          END IF;
      END LOOP;
  END IF;
  RETURN strRet;
END int_array_to_csv;
FUNCTION csv_to_string_array(csv_string VARCHAR2)
RETURN STRING_ARRAY
IS
  crt_position  NUMBER;
  next_position NUMBER;
  crt_string        VARCHAR2(5000) := NULL;
  new_csv_string    VARCHAR2(5000) := NULL;
  cnt NUMBER;
  ret_str_array STRING_ARRAY;
BEGIN
  ret_str_array := STRING_ARRAY();
  -- remove the apostrophes
  new_csv_string := REPLACE( csv_string, '''' );
  IF new_csv_string IS NULL OR LENGTH( new_csv_string ) = 0
  THEN
        RETURN ret_str_array;
  END IF;
  -- initialization
  crt_position  := 1;
  next_position := INSTR( new_csv_string, ',', crt_position );
  cnt := 0;
  -- get all matches
  WHILE ( next_position <> 0 )
  LOOP
        crt_string := LTRIM( RTRIM( SUBSTR( new_csv_string, crt_position, next_position - crt_position ) ) );
      IF LENGTH(crt_string) <> 0 THEN
           ret_str_array.EXTEND;
         cnt := cnt + 1;
         IF crt_string='<null>' THEN
            ret_str_array(cnt) := NULL;
         ELSE
            ret_str_array(cnt) := crt_string;
         END IF;
      END IF;
      crt_position := next_position + 1;
      next_position := INSTR( new_csv_string, ',', crt_position );
  END LOOP;
  -- get the last piece after the last comma
  crt_string := LTRIM( RTRIM( SUBSTR( new_csv_string, crt_position ) ) );
  IF LENGTH( crt_string ) <> 0 THEN
       ret_str_array.EXTEND;
     cnt := cnt + 1;
     IF crt_string='<null>' THEN
        ret_str_array(cnt) := NULL;
     ELSE
        ret_str_array(cnt) := crt_string;
     END IF;
  END IF;
  RETURN ret_str_array;
END csv_to_string_array;
PROCEDURE Get_Shipment_List3(
    --vcoid IN shipment.tc_company_id%TYPE,                                  -- company id
    vcoid IN VARCHAR2,                                                       -- comma separated company id list
    vcontactid IN FACILITY_CONTACT.facility_contact_id%TYPE,         -- contact id
    filter_where_clause IN VARCHAR2,                                 -- where clause from filter or empty string
    filter_order_by_clause IN VARCHAR2,                                 -- order by clause from filter or empty string
    vminrow IN NUMBER,                                                      -- first row to retrieve (0-based)
    vmaxrow IN NUMBER,                                                 -- last row + 1 to retrieve (ex: 0..10 retrieves first 10 shipments)
    vsizeuom1 IN SIZE_UOM.size_uom_id%TYPE,                             -- configured display UOM or empty string
    vsizeuom2 IN SIZE_UOM.size_uom_id%TYPE,                             -- configured display UOM or empty string
    vsizeuom3 IN SIZE_UOM.size_uom_id%TYPE,                             -- configured display UOM or empty string
    vsizeuom4 IN SIZE_UOM.size_uom_id%TYPE,                             -- configured display UOM or empty string -- CT18
    vtotal_count OUT NUMBER,                                         -- total # records matching where clause
    vshipment_refcur OUT CURSOR_TYPE,                                 -- shipment data resultset
    vstop_refcur OUT CURSOR_TYPE,                                     -- stop data resultset
    vorder_refcur OUT CURSOR_TYPE                                     -- order data resultset
)
IS
   where_clause                 VARCHAR2(5000);
   sql_statement             VARCHAR2(5000);
   vshipment_id SHIPMENT.shipment_id%TYPE;
   vcurrow NUMBER;
   vidlist_done NUMBER;
   vgttcount NUMBER;
   total_count_cursor CURSOR_TYPE;
   shipment_id_cursor CURSOR_TYPE;
   shipment_list_cursor SHIPMENT_LIST_SHP_CURSOR_TYPE3;
   stop_list_cursor SHIPMENT_LIST_STP_CURSOR_TYPE2;
   order_list_cursor SHIPMENT_LIST_ORD_CURSOR_TYPE2;
BEGIN
/*    -- total count for display on bottom of page (rows m to n of <total>).  Filter is applied, but
   -- no row count restrictions or order by clause.
   -- These restrictions were previously hardcoded in the SS bean method:
   -- creation_type is consolidated, is_cancelled is false, shipment_status not complete, not pool point shipment. */
   where_clause :=
   ' where ((shipment.tc_company_id in (' || vcoid || '))
             AND CREATION_TYPE = 20 AND IS_CANCELLED = 0 AND SHIPMENT_STATUS <> 90 AND PP_SHIPMENT_ID IS NULL)
             AND NOT EXISTS    (SELECT    1
                FROM    CWS_SHIPMENT a
                WHERE    a.ORIG_SHIPMENT_ID = SHIPMENT.SHIPMENT_ID
           )'
       || filter_where_clause;
   sql_statement := 'select count(1) from shipment' || where_clause;
   /*
   DBMS_OUTPUT.put_line( 'Count query:');
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,   0, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3801, 200 ));
   DBMS_OUTPUT.put_line( 'END Count query' );
   */
   -- OPEN total_count_cursor FOR sql_statement USING vcoid;
   OPEN total_count_cursor FOR sql_statement;
   FETCH total_count_cursor INTO vtotal_count;
   CLOSE total_count_cursor;
   /* DBMS_OUTPUT.put_line( 'Total count: ' || to_char(vtotal_count) ); */
   -- shipment id list for page (restriction by row numbers and ordered by filter)
   DELETE FROM MAN_CONS_SHIPMENT_LIST_GTT;
   sql_statement := 'select shipment.shipment_id from shipment'
                    || where_clause
                 || ' '
                 || filter_order_by_clause;
   /*
   DBMS_OUTPUT.put_line( 'Shipment ID List query:');
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,   0, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3801, 200 ));
   DBMS_OUTPUT.put_line( 'END Shipment ID List query' );
   */
   --kishore OPEN shipment_id_cursor FOR sql_statement USING vcoid;
    OPEN shipment_id_cursor FOR sql_statement;
   -- Skip rows before minrow, starting at 0
   vcurrow := 0;
   vidlist_done := 0;
   <<c1_cursor_loop>>
   LOOP
          /*DBMS_OUTPUT.put_line('c1 loop, vcurrow = ' || to_char(vcurrow));*/
          EXIT WHEN vcurrow >= vminrow;
          FETCH shipment_id_cursor INTO vshipment_id;
       IF shipment_id_cursor%NOTFOUND THEN
             vidlist_done := 1;
          EXIT;
       END IF;
          /*DBMS_OUTPUT.put_line('c1 loop, fetched id = ' || to_char(vshipment_id));*/
       vcurrow := vcurrow + 1;
   END LOOP c1_cursor_loop;
   -- If any rows left, add these id's, up to maxrow-1
   IF vidlist_done = 0 THEN
       <<c2_cursor_loop>>
       LOOP
              /*DBMS_OUTPUT.put_line('c2 loop, vcurrow = ' || to_char(vcurrow));*/
              EXIT WHEN vcurrow >= vmaxrow;
              FETCH shipment_id_cursor INTO vshipment_id;
           EXIT WHEN shipment_id_cursor%NOTFOUND;
           /*DBMS_OUTPUT.put_line('c2 loop, fetched id = ' || to_char(vshipment_id));*/
           INSERT INTO MAN_CONS_SHIPMENT_LIST_GTT (ROW_NUM, SHIPMENT_ID) VALUES (vcurrow, vshipment_id);
           vcurrow := vcurrow + 1;
       END LOOP c2_cursor_loop;
--       COMMIT;
   END IF;
   CLOSE shipment_id_cursor;
   /* Debugging
   select count(1) into vgttcount from MAN_CONS_SHIPMENT_LIST_GTT;
   DBMS_OUTPUT.put_line('MAN_CONS_SHIPMENT_LIST_GTT count = ' || to_char(vgttcount));
   */
   -- Shipment data only
   OPEN shipment_list_cursor FOR
SELECT SH.row_num, SH.shipment_id, SH.tc_shipment_id, SH.SHIPMENT_STATUS, SH.earned_income, SH.earned_income_currency_code,
       SH.has_import_error, SH.has_soft_check_error,
       CASE WHEN ALERTS.alert_id IS NOT NULL THEN 1 END HAS_ALERTS,
       SH.has_dependent_shipments,SH.estimated_savings,SH.total_cost,SH.currency_code,
       SH.WAVE_ID,SH.MOVE_TYPE,SH.IS_BOOKING_REQUIRED,SH.BOOKING_REF_SHIPPER,BOOKING_ID
       FROM
-- Shipment level
(SELECT /*+ ordered first_rows */ MAN_CONS_SHIPMENT_LIST_GTT.row_num,
        SHIPMENT.shipment_id, SHIPMENT.tc_shipment_id, SHIPMENT.SHIPMENT_STATUS, SHIPMENT.earned_income, SHIPMENT.earned_income_currency_code, SHIPMENT.has_import_error, SHIPMENT.has_soft_check_error,
(
SELECT (NVL((SELECT 1 FROM dual WHERE EXISTS(
SELECT om2.shipment_id FROM ORDER_MOVEMENT om1, ORDER_MOVEMENT om2 WHERE
om1.shipment_id = SHIPMENT.shipment_id AND
om1.order_id = om2.order_id AND
om2.shipment_id != om1.shipment_id)),0) ) FROM SHIPMENT
WHERE shipment_id = MAN_CONS_SHIPMENT_LIST_GTT.shipment_id
)has_dependent_shipments,
SHIPMENT.ESTIMATED_SAVINGS,SHIPMENT.TOTAL_COST,SHIPMENT.CURRENCY_CODE, SHIPMENT.WAVE_ID,SHIPMENT.MOVE_TYPE,SHIPMENT.IS_BOOKING_REQUIRED,SHIPMENT.BOOKING_REF_SHIPPER,(SELECT booking_id FROM BOOKING_SHIPMENT, MAN_CONS_SHIPMENT_LIST_GTT WHERE BOOKING_SHIPMENT.shipment_id = MAN_CONS_SHIPMENT_LIST_GTT.SHIPMENT_ID) BOOKING_ID
    FROM MAN_CONS_SHIPMENT_LIST_GTT, SHIPMENT
    WHERE SHIPMENT.shipment_id=MAN_CONS_SHIPMENT_LIST_GTT.shipment_id) SH,
-- Shipment level alerts
AP_ALERT ALERTS
-- Now join and order all of the above
 WHERE (SH.shipment_id=ALERTS.object_id (+) AND ALERTS.OBJECT_TYPE(+)='SHIPMENT' AND ALERTS.alert_status(+) = 1)
ORDER BY SH.row_num;

-- Stop and size data only
   OPEN stop_list_cursor FOR
    SELECT /*+ ordered first_rows */
          MAN_CONS_SHIPMENT_LIST_GTT.row_num,
          STOP.shipment_id
          , STOP.stop_seq
          , STOP.stop_location_name
          , STOP.facility_id
          ,STOP.arrival_start_dttm, STOP.arrival_end_dttm, STOP.city, STOP.STATE_PROV
          , STOP.stop_tz
          ,STOP_ACTION.ACTION_TYPE
          ,(SELECT SUM(size_value)
              FROM ORDER_LINE_ITEM_SIZE
                   ,STOP_ACTION_ORDER
             WHERE ORDER_LINE_ITEM_SIZE.order_id = STOP_ACTION_ORDER.order_id
               AND STOP_ACTION_ORDER.shipment_id = STOP_ACTION.shipment_id
               AND STOP_ACTION_ORDER.stop_seq = STOP_ACTION.stop_seq
               AND STOP_ACTION_ORDER.stop_action_seq = STOP_ACTION.stop_action_seq
               AND size_uom_id = vsizeuom1
            )
          ,(SELECT SUM(size_value)
              FROM ORDER_LINE_ITEM_SIZE
                   ,STOP_ACTION_ORDER
             WHERE ORDER_LINE_ITEM_SIZE.order_id = STOP_ACTION_ORDER.order_id
               AND STOP_ACTION_ORDER.shipment_id = STOP_ACTION.shipment_id
               AND STOP_ACTION_ORDER.stop_seq = STOP_ACTION.stop_seq
               AND STOP_ACTION_ORDER.stop_action_seq = STOP_ACTION.stop_action_seq
               AND size_uom_id = vsizeuom2
            )
          ,(SELECT SUM(size_value)
              FROM ORDER_LINE_ITEM_SIZE
                   ,STOP_ACTION_ORDER
             WHERE ORDER_LINE_ITEM_SIZE.order_id = STOP_ACTION_ORDER.order_id
               AND STOP_ACTION_ORDER.shipment_id = STOP_ACTION.shipment_id
               AND STOP_ACTION_ORDER.stop_seq = STOP_ACTION.stop_seq
               AND STOP_ACTION_ORDER.stop_action_seq = STOP_ACTION.stop_action_seq
               AND size_uom_id = vsizeuom3
            )
          -- CT18
          ,(SELECT SUM(size_value)
              FROM ORDER_LINE_ITEM_SIZE
                   ,STOP_ACTION_ORDER
             WHERE ORDER_LINE_ITEM_SIZE.order_id = STOP_ACTION_ORDER.order_id
               AND STOP_ACTION_ORDER.shipment_id = STOP_ACTION.shipment_id
               AND STOP_ACTION_ORDER.stop_seq = STOP_ACTION.stop_seq
               AND STOP_ACTION_ORDER.stop_action_seq = STOP_ACTION.stop_action_seq
               AND size_uom_id = vsizeuom4
            )
          FROM
          MAN_CONS_SHIPMENT_LIST_GTT
          ,STOP
          ,STOP_ACTION
      WHERE
          STOP.SHIPMENT_ID = MAN_CONS_SHIPMENT_LIST_GTT.shipment_id
      AND STOP_ACTION.shipment_id=STOP.shipment_id
      AND STOP_ACTION.stop_seq=STOP.stop_seq
      ORDER BY MAN_CONS_SHIPMENT_LIST_GTT.row_num,STOP.stop_seq
      ;
   -- Order data only at the stop
   OPEN order_list_cursor FOR
SELECT /*+ ordered first_rows */ MAN_CONS_SHIPMENT_LIST_GTT.row_num,
              STOP_ACTION_ORDER.shipment_id, STOP_ACTION_ORDER.stop_seq,
           STOP_ACTION_ORDER.order_id ORDER_ID, ORDERS.tc_order_id
           FROM MAN_CONS_SHIPMENT_LIST_GTT, STOP_ACTION_ORDER, ORDERS
        WHERE     (STOP_ACTION_ORDER.shipment_id=MAN_CONS_SHIPMENT_LIST_GTT.shipment_id)
              AND (ORDERS.order_id=STOP_ACTION_ORDER.order_id)
ORDER BY MAN_CONS_SHIPMENT_LIST_GTT.row_num, STOP_ACTION_ORDER.stop_seq, ORDERS.tc_order_id;
   -- set as returns
   vshipment_refcur := shipment_list_cursor;
   vstop_refcur := stop_list_cursor;
   vorder_refcur := order_list_cursor;
END Get_Shipment_List3;
--
PROCEDURE Get_Shipment_List4(
    --vcoid IN shipment.tc_company_id%TYPE,                                               -- company id
    vcoid IN VARCHAR2,                                                                                 -- comma separated company id list
       vcontactid IN FACILITY_CONTACT.facility_contact_id%TYPE,             -- contact id
       filter_where_clause IN VARCHAR2,                                                  -- where clause from filter or empty string
       filter_order_by_clause IN VARCHAR2,                                                      -- order by clause from filter or empty string
       vminrow IN NUMBER,                                                                                -- first row to retrieve (0-based)
       vmaxrow IN NUMBER,                                                                              -- last row + 1 to retrieve (ex: 0..10 retrieves first 10 shipments)
       vsizeuom1 IN SIZE_UOM.size_uom_id%TYPE,                                           -- configured display UOM or empty string
       vsizeuom2 IN SIZE_UOM.size_uom_id%TYPE,                                           -- configured display UOM or empty string
       vsizeuom3 IN SIZE_UOM.size_uom_id%TYPE,                                           -- configured display UOM or empty string
       vsizeuom4 IN SIZE_UOM.size_uom_id%TYPE,                                           -- configured display UOM or empty string
       vtotal_count OUT NUMBER,                                                                 -- total # records matching where clause
       vshipment_refcur OUT CURSOR_TYPE,                                                 -- shipment data resultset
       vstop_refcur OUT CURSOR_TYPE,                                                            -- stop data resultset
       vorder_refcur OUT CURSOR_TYPE,                                                           -- order data resultset
       vcustattrib_refcur OUT CURSOR_TYPE,                                                             -- custom attribute data resultset
       vorderattrib_refcur OUT CURSOR_TYPE                                                             -- custom attribute data resultset
)
IS
   where_clause                   VARCHAR2(10000);
   sql_statement            VARCHAR2(10000);
   vshipment_id SHIPMENT.shipment_id%TYPE;
   vcurrow NUMBER;
   vidlist_done NUMBER;
   vgttcount NUMBER;
   total_count_cursor CURSOR_TYPE;
   shipment_id_cursor CURSOR_TYPE;
   shipment_list_cursor SHIPMENT_LIST_SHP_CURSOR_TYPE4;
   stop_list_cursor SHIPMENT_LIST_STP_CURSOR_TYPE4;
   order_list_cursor SHIPMENT_LIST_ORD_CURSOR_TYPE2;
   cust_attrib_cursor SHIP_LIST_ATTR_CURSOR_TYPE2;
   order_attrib_cursor SHIP_LIST_ORAT_CURSOR_TYPE2;
BEGIN
/*    -- total count for display on bottom of page (rows m to n of <total>).  Filter is applied, but
   -- no row count restrictions or order by clause.
   -- These restrictions were previously hardcoded in the SS bean method:
   -- creation_type is consolidated, is_cancelled is false, shipment_status not complete, not pool point shipment. */
   where_clause :=
   ' where (creation_type = 20 AND IS_CANCELLED = 0 AND SHIPMENT_STATUS <> 90 AND PP_SHIPMENT_ID IS NULL)
                 AND NOT EXISTS    (SELECT       1
                           FROM   CWS_SHIPMENT a
                           WHERE  a.ORIG_SHIPMENT_ID = SHIPMENT.SHIPMENT_ID
                 )'
          || filter_where_clause;
   sql_statement := 'select count(1) from shipment' || where_clause;
   /*
   DBMS_OUTPUT.put_line( 'Count query:');
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,   0, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3801, 200 ));
   DBMS_OUTPUT.put_line( 'END Count query' );
   */
   -- OPEN total_count_cursor FOR sql_statement USING vcoid;
   OPEN total_count_cursor FOR sql_statement;
   FETCH total_count_cursor INTO vtotal_count;
   CLOSE total_count_cursor;
   /* DBMS_OUTPUT.put_line( 'Total count: ' || to_char(vtotal_count) ); */
   -- shipment id list for page (restriction by row numbers and ordered by filter)
   DELETE FROM MAN_CONS_SHIPMENT_LIST_GTT;
   sql_statement := 'select shipment.shipment_id from shipment'
                            || where_clause
                             || ' '
                            || filter_order_by_clause;
   /*
   DBMS_OUTPUT.put_line( 'Shipment ID List query:');
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,   0, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement, 801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,1801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,2801, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3001, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3201, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3401, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3601, 200 ));
   DBMS_OUTPUT.PUT_LINE( substr(sql_statement,3801, 200 ));
   DBMS_OUTPUT.put_line( 'END Shipment ID List query' );
   */
   --kishore OPEN shipment_id_cursor FOR sql_statement USING vcoid;
       OPEN shipment_id_cursor FOR sql_statement;
   -- Skip rows before minrow, starting at 0
   vcurrow := 0;
   vidlist_done := 0;
   <<c1_cursor_loop>>
   LOOP
          /*DBMS_OUTPUT.put_line('c1 loop, vcurrow = ' || to_char(vcurrow));*/
          EXIT WHEN vcurrow >= vminrow;
          FETCH shipment_id_cursor INTO vshipment_id;
       IF shipment_id_cursor%NOTFOUND THEN
                vidlist_done := 1;
                EXIT;
          END IF;
          /*DBMS_OUTPUT.put_line('c1 loop, fetched id = ' || to_char(vshipment_id));*/
          vcurrow := vcurrow + 1;
   END LOOP c1_cursor_loop;
   -- If any rows left, add these id's, up to maxrow-1
   IF vidlist_done = 0 THEN
          <<c2_cursor_loop>>
          LOOP
                 /*DBMS_OUTPUT.put_line('c2 loop, vcurrow = ' || to_char(vcurrow));*/
                 EXIT WHEN vcurrow >= vmaxrow;
                 FETCH shipment_id_cursor INTO vshipment_id;
              EXIT WHEN shipment_id_cursor%NOTFOUND;
                 /*DBMS_OUTPUT.put_line('c2 loop, fetched id = ' || to_char(vshipment_id));*/
                 INSERT INTO MAN_CONS_SHIPMENT_LIST_GTT (ROW_NUM, SHIPMENT_ID) VALUES (vcurrow, vshipment_id);
                 vcurrow := vcurrow + 1;
          END LOOP c2_cursor_loop;
--          COMMIT;
   END IF;
   CLOSE shipment_id_cursor;
   /* Debugging
   select count(1) into vgttcount from MAN_CONS_SHIPMENT_LIST_GTT;
   DBMS_OUTPUT.put_line('MAN_CONS_SHIPMENT_LIST_GTT count = ' || to_char(vgttcount));
   */
   -- Shipment data only
   OPEN  shipment_list_cursor FOR
       SELECT DISTINCT SH.row_num,
       SH.shipment_id,
       SH.tc_shipment_id,
       SH.SHIPMENT_STATUS,
       SH.earned_income,
       SH.earned_income_currency_code,
       SH.feasible_mot_id,
       SH.feasible_equipment_id,
       SH.feasible_service_level_id,
       SH.feasible_carrier_id,
       SH.feasible_equipment2_id,
       SH.feasible_driver_type,
  SH.pickup_start_dttm,
  SH.pickup_end_dttm,
  SH.pickup_tz,
  SH.delivery_start_dttm,
  SH.delivery_end_dttm,
  SH.delivery_tz,
  SH.protection_level_id,
  SH.o_facility_id,
  SH.o_address,
  SH.o_city,
  SH.o_state_prov,
  SH.o_postal_code,
  SH.o_country_code,
  SH.d_facility_id,
  SH.d_address,
  SH.d_city,
  SH.d_state_prov,
  SH.d_postal_code,
  SH.d_country_code,
       SH.has_import_error,
       SH.has_soft_check_error,
       CASE WHEN ALERTS.alert_id IS NOT NULL THEN 1 END HAS_ALERTS,
       SH.has_dependent_shipments,
       SH.estimated_savings,
       SH.total_cost,
       SH.currency_code,
       SH.WAVE_ID,
       SH.MOVE_TYPE,
       SH.IS_BOOKING_REQUIRED,
       SH.BOOKING_REF_SHIPPER,
       SH.BOOKING_ID,
  SH.SIZE1,
  SH.SIZE2,
  SH.SIZE3,
  SH.SIZE4

          FROM
-- Shipment level
(SELECT /*+ ordered first_rows */ MAN_CONS_SHIPMENT_LIST_GTT.row_num,
              SHIPMENT.shipment_id, SHIPMENT.tc_shipment_id, SHIPMENT.SHIPMENT_STATUS,
    SHIPMENT.earned_income, SHIPMENT.earned_income_currency_code, SHIPMENT.feasible_mot_id,
    SHIPMENT.feasible_equipment_id, SHIPMENT.feasible_service_level_id,
    SHIPMENT.feasible_carrier_id, SHIPMENT.feasible_equipment2_id,
    SHIPMENT.feasible_driver_type,
    SHIPMENT.pickup_start_dttm, SHIPMENT.pickup_end_dttm, SHIPMENT.pickup_tz,
    SHIPMENT.delivery_start_dttm, SHIPMENT.delivery_end_dttm, SHIPMENT.delivery_tz, SHIPMENT.protection_level_id,
    SHIPMENT.o_facility_id, SHIPMENT.o_address, SHIPMENT.o_city, SHIPMENT.o_state_prov, SHIPMENT.o_postal_code,
    SHIPMENT.o_country_code, SHIPMENT.d_facility_id, SHIPMENT.d_address, SHIPMENT.d_city, SHIPMENT.d_state_prov,
    SHIPMENT.d_postal_code, SHIPMENT.d_country_code,
    SHIPMENT.has_import_error, SHIPMENT.has_soft_check_error,
(
SELECT (NVL((SELECT 1 FROM dual WHERE EXISTS(
SELECT om2.shipment_id FROM ORDER_MOVEMENT om1, ORDER_MOVEMENT om2 WHERE
om1.shipment_id = SHIPMENT.shipment_id AND
om1.order_id = om2.order_id AND
om2.shipment_id != om1.shipment_id)),0) ) FROM SHIPMENT
WHERE shipment_id = MAN_CONS_SHIPMENT_LIST_GTT.shipment_id
)has_dependent_shipments,
SHIPMENT.ESTIMATED_SAVINGS,SHIPMENT.TOTAL_COST,SHIPMENT.CURRENCY_CODE, SHIPMENT.WAVE_ID, SHIPMENT.MOVE_TYPE,SHIPMENT.IS_BOOKING_REQUIRED,SHIPMENT.BOOKING_REF_SHIPPER,
SHIPMENT.BOOKING_ID, (
         SELECT SUM(SIZE_VALUE)  FROM SHIPMENT_COMMODITY
         WHERE SHIPMENT_COMMODITY.SHIPMENT_ID = SHIPMENT.SHIPMENT_ID
         AND SHIPMENT_COMMODITY.SIZE_UOM_ID = ( SELECT SIZE_UOM_ID FROM SIZE_UOM_DISPLAY
         WHERE TC_COMPANY_ID = SHIPMENT.TC_COMPANY_ID AND SUMMARY_VIEW_FLAG=0)
) AS SIZE1,
(
         SELECT SUM(SIZE_VALUE)  FROM SHIPMENT_COMMODITY
         WHERE SHIPMENT_COMMODITY.SHIPMENT_ID = SHIPMENT.SHIPMENT_ID
         AND SHIPMENT_COMMODITY.SIZE_UOM_ID = ( SELECT SIZE_UOM_ID FROM SIZE_UOM_DISPLAY
         WHERE TC_COMPANY_ID = SHIPMENT.TC_COMPANY_ID AND SUMMARY_VIEW_FLAG=1)
) AS SIZE2,
(
         SELECT SUM(SIZE_VALUE)  FROM SHIPMENT_COMMODITY
         WHERE SHIPMENT_COMMODITY.SHIPMENT_ID = SHIPMENT.SHIPMENT_ID
         AND SHIPMENT_COMMODITY.SIZE_UOM_ID = ( SELECT SIZE_UOM_ID FROM SIZE_UOM_DISPLAY
         WHERE TC_COMPANY_ID = SHIPMENT.TC_COMPANY_ID AND SUMMARY_VIEW_FLAG=2)
) AS SIZE3,
(
         SELECT SUM(SIZE_VALUE)  FROM SHIPMENT_COMMODITY
         WHERE SHIPMENT_COMMODITY.SHIPMENT_ID = SHIPMENT.SHIPMENT_ID
         AND SHIPMENT_COMMODITY.SIZE_UOM_ID = ( SELECT SIZE_UOM_ID FROM SIZE_UOM_DISPLAY
         WHERE TC_COMPANY_ID = SHIPMENT.TC_COMPANY_ID AND SUMMARY_VIEW_FLAG=3)
) AS SIZE4
       FROM MAN_CONS_SHIPMENT_LIST_GTT, SHIPMENT
       WHERE SHIPMENT.shipment_id=MAN_CONS_SHIPMENT_LIST_GTT.shipment_id) SH,
-- Shipment level alerts
AP_ALERT ALERTS
-- Now join and order all of the above
-- where (SH.shipment_id=ALERTS.shipment_id (+))
WHERE (SH.shipment_id=ALERTS.object_id (+) AND ALERTS.OBJECT_TYPE(+)='SHIPMENT' AND ALERTS.alert_status(+) = 1)
ORDER BY SH.row_num;
-- Stop and size data only
   OPEN stop_list_cursor FOR
       SELECT /*+ ordered first_rows */
             MAN_CONS_SHIPMENT_LIST_GTT.row_num,
             STOP.shipment_id
                , STOP.stop_seq
                , STOP.stop_location_name
                , STOP.facility_id
                , STOP.arrival_start_dttm
                , STOP.arrival_end_dttm
                , STOP.departure_start_dttm
                , STOP.departure_end_dttm
                , STOP.city, STOP.STATE_PROV
                , STOP.stop_tz
                ,STOP_ACTION.ACTION_TYPE
                ,(SELECT SUM(size_value)
                    FROM ORDER_LINE_ITEM_SIZE
                            ,STOP_ACTION_ORDER
                      WHERE ORDER_LINE_ITEM_SIZE.order_id = STOP_ACTION_ORDER.order_id
                        AND STOP_ACTION_ORDER.shipment_id = STOP_ACTION.shipment_id
                        AND STOP_ACTION_ORDER.stop_seq = STOP_ACTION.stop_seq
                        AND STOP_ACTION_ORDER.stop_action_seq = STOP_ACTION.stop_action_seq
                        AND STOP_ACTION_ORDER.order_split_id IS NULL
                        AND size_uom_id = vsizeuom1
                     ),
                     (SELECT SUM (split_size_value)
                            FROM ORDER_SPLIT_SIZE,STOP_ACTION_ORDER
                            WHERE
                            ORDER_SPLIT_SIZE.order_id = STOP_ACTION_ORDER.order_id
                            AND ORDER_SPLIT_SIZE.order_split_id = STOP_ACTION_ORDER.order_split_id
                                AND STOP_ACTION_ORDER.shipment_id = STOP_ACTION.shipment_id
                                AND STOP_ACTION_ORDER.stop_seq = STOP_ACTION.stop_seq
                                AND STOP_ACTION_ORDER.stop_action_seq = STOP_ACTION.stop_action_seq
                                AND STOP_ACTION_ORDER.order_split_id IS NOT NULL
                         AND split_size_uom_id  = vsizeuom1
                     )
                ,(SELECT SUM(size_value)
                    FROM ORDER_LINE_ITEM_SIZE
                            ,STOP_ACTION_ORDER
                      WHERE ORDER_LINE_ITEM_SIZE.order_id = STOP_ACTION_ORDER.order_id
                        AND STOP_ACTION_ORDER.shipment_id = STOP_ACTION.shipment_id
                        AND STOP_ACTION_ORDER.stop_seq = STOP_ACTION.stop_seq
                        AND STOP_ACTION_ORDER.stop_action_seq = STOP_ACTION.stop_action_seq
                        AND STOP_ACTION_ORDER.order_split_id IS NULL
                        AND size_uom_id = vsizeuom2
                     ),
                     (SELECT SUM (split_size_value)
                            FROM ORDER_SPLIT_SIZE,STOP_ACTION_ORDER
                            WHERE
                            ORDER_SPLIT_SIZE.order_id = STOP_ACTION_ORDER.order_id
                            AND ORDER_SPLIT_SIZE.order_split_id = STOP_ACTION_ORDER.order_split_id
                                AND STOP_ACTION_ORDER.shipment_id = STOP_ACTION.shipment_id
                                AND STOP_ACTION_ORDER.stop_seq = STOP_ACTION.stop_seq
                                AND STOP_ACTION_ORDER.stop_action_seq = STOP_ACTION.stop_action_seq
                                AND STOP_ACTION_ORDER.order_split_id IS NOT NULL
                         AND split_size_uom_id  = vsizeuom2
                     )
                ,(SELECT SUM(size_value)
                    FROM ORDER_LINE_ITEM_SIZE
                            ,STOP_ACTION_ORDER
                      WHERE ORDER_LINE_ITEM_SIZE.order_id = STOP_ACTION_ORDER.order_id
                        AND STOP_ACTION_ORDER.shipment_id = STOP_ACTION.shipment_id
                        AND STOP_ACTION_ORDER.stop_seq = STOP_ACTION.stop_seq
                        AND STOP_ACTION_ORDER.stop_action_seq = STOP_ACTION.stop_action_seq
                        AND STOP_ACTION_ORDER.order_split_id IS NULL
                        AND size_uom_id = vsizeuom3
                     ),
                     (SELECT SUM (split_size_value)
                            FROM ORDER_SPLIT_SIZE,STOP_ACTION_ORDER
                            WHERE
                            ORDER_SPLIT_SIZE.order_id = STOP_ACTION_ORDER.order_id
                            AND ORDER_SPLIT_SIZE.order_split_id = STOP_ACTION_ORDER.order_split_id
                                AND STOP_ACTION_ORDER.shipment_id = STOP_ACTION.shipment_id
                                AND STOP_ACTION_ORDER.stop_seq = STOP_ACTION.stop_seq
                                AND STOP_ACTION_ORDER.stop_action_seq = STOP_ACTION.stop_action_seq
                                AND STOP_ACTION_ORDER.order_split_id IS NOT NULL
                         AND split_size_uom_id  = vsizeuom3
                     )
                ,(SELECT SUM(size_value)
                    FROM ORDER_LINE_ITEM_SIZE
                            ,STOP_ACTION_ORDER
                      WHERE ORDER_LINE_ITEM_SIZE.order_id = STOP_ACTION_ORDER.order_id
                        AND STOP_ACTION_ORDER.shipment_id = STOP_ACTION.shipment_id
                        AND STOP_ACTION_ORDER.stop_seq = STOP_ACTION.stop_seq
                        AND STOP_ACTION_ORDER.stop_action_seq = STOP_ACTION.stop_action_seq
                        AND STOP_ACTION_ORDER.order_split_id IS NULL
                        AND size_uom_id = vsizeuom4
                     ),
                     (SELECT SUM (split_size_value)
                            FROM ORDER_SPLIT_SIZE,STOP_ACTION_ORDER
                            WHERE
                            ORDER_SPLIT_SIZE.order_id = STOP_ACTION_ORDER.order_id
                            AND ORDER_SPLIT_SIZE.order_split_id = STOP_ACTION_ORDER.order_split_id
                                AND STOP_ACTION_ORDER.shipment_id = STOP_ACTION.shipment_id
                                AND STOP_ACTION_ORDER.stop_seq = STOP_ACTION.stop_seq
                                AND STOP_ACTION_ORDER.stop_action_seq = STOP_ACTION.stop_action_seq
                                AND STOP_ACTION_ORDER.order_split_id IS NOT NULL
                         AND split_size_uom_id  = vsizeuom4
                     )
                FROM
             MAN_CONS_SHIPMENT_LIST_GTT
             ,STOP
                ,STOP_ACTION
         WHERE
             STOP.SHIPMENT_ID = MAN_CONS_SHIPMENT_LIST_GTT.shipment_id
         AND STOP_ACTION.shipment_id=STOP.shipment_id
         AND STOP_ACTION.stop_seq=STOP.stop_seq
         ORDER BY MAN_CONS_SHIPMENT_LIST_GTT.row_num,STOP.stop_seq
         ;
   -- Order data only at the stop
   OPEN order_list_cursor FOR
SELECT /*+ ordered first_rows */ MAN_CONS_SHIPMENT_LIST_GTT.row_num,
                 STOP_ACTION_ORDER.shipment_id, STOP_ACTION_ORDER.stop_seq,
                 STOP_ACTION_ORDER.order_id ORDER_ID, ORDERS.tc_order_id
              FROM MAN_CONS_SHIPMENT_LIST_GTT, STOP_ACTION_ORDER, ORDERS
           WHERE     (STOP_ACTION_ORDER.shipment_id=MAN_CONS_SHIPMENT_LIST_GTT.shipment_id)
                       AND (ORDERS.order_id=STOP_ACTION_ORDER.order_id)
ORDER BY MAN_CONS_SHIPMENT_LIST_GTT.row_num, STOP_ACTION_ORDER.stop_seq, ORDERS.tc_order_id;
-- Custom Attributes for the shipment
   OPEN cust_attrib_cursor FOR
SELECT MAN_CONS_SHIPMENT_LIST_GTT.row_num,
                 SHIPMENT_ATTRIBUTE.shipment_id, SHIPMENT_ATTRIBUTE.attribute_name,
                 SHIPMENT_ATTRIBUTE.attribute_value
              FROM MAN_CONS_SHIPMENT_LIST_GTT, SHIPMENT_ATTRIBUTE
           WHERE  SHIPMENT_ATTRIBUTE.shipment_id=MAN_CONS_SHIPMENT_LIST_GTT.shipment_id
ORDER BY MAN_CONS_SHIPMENT_LIST_GTT.row_num;
-- Custom Attributes for orders on the shipment
   OPEN order_attrib_cursor FOR
SELECT MAN_CONS_SHIPMENT_LIST_GTT.row_num, MAN_CONS_SHIPMENT_LIST_GTT.shipment_id, order_id, attribute_name,attribute_value
              FROM MAN_CONS_SHIPMENT_LIST_GTT, ORDER_ATTRIBUTE
           WHERE  order_id IN (  SELECT DISTINCT order_id FROM STOP_ACTION_ORDER , MAN_CONS_SHIPMENT_LIST_GTT
           WHERE STOP_ACTION_ORDER.shipment_id = MAN_CONS_SHIPMENT_LIST_GTT.shipment_id ) ORDER BY order_id ;
   -- set as returns
   vshipment_refcur := shipment_list_cursor;
   vstop_refcur := stop_list_cursor;
   vorder_refcur := order_list_cursor;
   vcustattrib_refcur  := cust_attrib_cursor;
   vorderattrib_refcur  := order_attrib_cursor;
END Get_Shipment_List4;
--
FUNCTION getIdArrIndex(arr1 INT_ARRAY , arr2 INT_ARRAY , val1 INTEGER, val2 INTEGER)
RETURN INTEGER IS
  ret_index INTEGER ;
 BEGIN
    ret_index:=0;
    FOR i IN arr1.FIRST..arr1.LAST LOOP
    IF arr1(i)=val1 AND arr2(i)=val2 THEN
    ret_index := i ;
    END IF;
    END LOOP;
  RETURN(ret_index);
END getIdArrIndex;

PROCEDURE load_WorkspaceStatistics (
                statCwsShipmentCount OUT NUMBER,
                                    -- total # Cws shipments in workspace
                    statTotalStopCount OUT NUMBER,
                                    -- total # of stops across all shipments
                    statTotalOrderMovementCount OUT NUMBER,
                                    -- total # of order movements across all shipments
                    statBaselineCost OUT CWS_SHIPMENT.BASELINE_COST%TYPE,
                                    -- total baseline cost for shipments
                    statEstimatedCost OUT CWS_SHIPMENT.ESTIMATED_COST%TYPE,
                                    -- total estimated cost for shipments
                    statInitialCost OUT CWS_SHIPMENT.INITIAL_COST%TYPE,
                                    -- total initial cost for shipments

                    cwsId INTEGER               -- workspace id
                     ) IS
        -- order by clause for unassigned orders list or empty string
        order_null_check PLS_INTEGER := 0;
        NoOfShipHavingEstimatedCost NUMBER;
        NoOfNonDeconsolidatedShipments NUMBER;
        BEGIN

        -- Compute statistics using 1 or more aggregate queries into output scalars
        -- Shipment count, Estimated cost (includes sorting cost), initial cost)

        SELECT COUNT ( 1 ),
               ( NVL ( SUM ( CWS_SHIPMENT.ESTIMATED_COST ),
                   0 ) + NVL ( SUM ( CWS_SHIPMENT.XDOCK_SORT_COST ),
                       0 ) ),
               SUM ( CWS_SHIPMENT.INITIAL_COST )
          INTO statCwsShipmentCount,
               statEstimatedCost,
               statInitialCost
          FROM CWS_SHIPMENT
         WHERE CWS_SHIPMENT.CONS_WORKSPACE_ID = cwsId;


        SELECT COUNT ( 1 )
          INTO NoOfShipHavingEstimatedCost
          FROM CWS_SHIPMENT
         WHERE CWS_SHIPMENT.CONS_WORKSPACE_ID = cwsId AND CWS_SHIPMENT.ESTIMATED_COST IS NOT NULL;


    SELECT COUNT ( 1 )
            INTO NoOfNonDeconsolidatedShipments
            FROM CWS_SHIPMENT
              WHERE CWS_SHIPMENT.CONS_WORKSPACE_ID = cwsId AND CWS_SHIPMENT.NUM_STOPS > 0;

        IF (NoOfShipHavingEstimatedCost != NoOfNonDeconsolidatedShipments ) THEN
            statEstimatedCost := NULL;
        END IF;

        -- Baseline cost will now be computed as the sum of the assigned orders' baseline costs.  Otherwise,
        -- we might count the baseline cost multiple times if the order is on multiple shipments (per MichelleF).
        -- But! If any order baseline cost is null for a shipment, that shipment has no baseline cost.  And
        -- if any orders in a movement network have null baseline cost, the whole network does.  Got it?  Here's the way:
        -- Step 0: incase there are any orders with Baseline cost as null, that shipment should have the null baseline and hence workspace
        -- Step 1: Get list of orders assigned to shipments that have null baseline cost.
        -- Step 2: Get list of CWS shipments for step 1 orders.
        -- Step 3: Get list of CWS order movements for step 2 shipments - none of these can count in the total
        -- Step 4: Sum the baseline cost of the orders assigned to shipment MINUS the orders on the movements from step 3.
        SELECT COUNT ( ORD1.order_id )
          INTO order_null_check
          FROM ORDERS ORD1
         WHERE ORD1.ORDER_ID IN ( SELECT CWSOM1.ORDER_ID
                        FROM CWS_ORDER_MOVEMENT CWSOM1
                       WHERE CWSOM1.CONS_WORKSPACE_ID = cwsId )
           AND ORD1.NORMALIZED_BASELINE_COST IS NULL;

        IF  (order_null_check = 0) THEN
            SELECT SUM ( ORD4.NORMALIZED_BASELINE_COST )
              INTO statBaselineCost
              FROM ORDERS ORD4
             WHERE ORD4.ORDER_ID IN ( ( SELECT DISTINCT CWSOM4.ORDER_ID
                          FROM CWS_ORDER_MOVEMENT CWSOM4
                         WHERE CWSOM4.CONS_WORKSPACE_ID = cwsId ) )
                         AND ORD4.NORMALIZED_BASELINE_COST IS NOT NULL ;


        END IF;

        -- Total stops
        SELECT COUNT ( 1 )
          INTO statTotalStopCount
          FROM CWS_STOP
         WHERE CWS_STOP.CONS_WORKSPACE_ID = cwsId;

        -- Total order movements (orders on shipments)
        SELECT COUNT ( 1 )
          INTO statTotalOrderMovementCount
          FROM CWS_ORDER_MOVEMENT
         WHERE CWS_ORDER_MOVEMENT.CONS_WORKSPACE_ID = cwsId;

END load_WorkspaceStatistics;

PROCEDURE load_WorkspaceAssignedOrders (
                    assignedOrderListRefcur OUT CURSOR_TYPE,
                                    -- Cws assigned order display info
                    retCwsShipmentId INTEGER,
                                    -- current draft shipment id as determined by the sproc
                    shipperId INTEGER,          -- company id
                    cwsId INTEGER,              -- workspace id
                    assignedOrderListOrderBy VARCHAR2,
                                    -- order by clause for assigned orders list or empty string
                    allowSplit NUMBER,
                    minRow NUMBER,           -- page to page param min row.
                    maxRow NUMBER) IS           -- page to page param max row.
        sql_statement2 VARCHAR ( 10000 );
        asg_order_list_cursor CURSOR_TYPE;
        BEGIN


    sql_statement2 := ' SELECT '
        || ' ASIGNORD.TC_COMPANY_ID, '
        || ' ASIGNORD.ORDER_ID, ASIGNORD.TC_ORDER_ID, '
        || ' O_FACILITY_NAME,  '
        || ' ASIGNORD.O_CITY, ASIGNORD.O_STATE_PROV, '
        || ' D_FACILITY_NAME,  '
        || ' ASIGNORD.D_CITY, ASIGNORD.D_STATE_PROV, '
        || ' ASIGNORD.PICKUP_START_DTTM, ASIGNORD.PICKUP_END_DTTM, ASIGNORD.PICKUP_TZ, '
        || ' ASIGNORD.DELIVERY_START_DTTM, ASIGNORD.DELIVERY_END_DTTM, ASIGNORD.DELIVERY_TZ, '
        || ' ASIGNORD.PROTECTION_LEVEL_ID, '
        || ' ASIGNORD.NORMALIZED_BASELINE_COST, '
        || ' ASIGNORD.SIZE1, ASIGNORD.SIZE1_UOM_ID,ASIGNORD.SIZE2, ASIGNORD.SIZE2_UOM_ID, '
        || ' ASIGNORD.SIZE3, ASIGNORD.SIZE3_UOM_ID,ASIGNORD.SIZE4, ASIGNORD.SIZE4_UOM_ID, ' -- CT18
        || ' ASIGNORD.PICKUP_STOP_SEQ, ASIGNORD.DELIVERY_STOP_SEQ, ASIGNORD.WAVE_ID,ASIGNORD.CWS_PROCESS_ID, ORDER_SPLIT_ID , ORDER_SPLIT_SEQ'
        || '  FROM ( ';


        sql_statement2 :=  sql_statement2 || 'select'
            || ' ORD.TC_COMPANY_ID,'
            || ' ORD.ORDER_ID, ORD.TC_ORDER_ID,'
            || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.O_FACILITY_ALIAS_ID AND FACILITY_ID = ORD.O_FACILITY_ID) O_FACILITY_NAME, '
            || ' ORD.O_CITY, ORD.O_STATE_PROV,'
            || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.D_FACILITY_ALIAS_ID AND FACILITY_ID = ORD.D_FACILITY_ID) D_FACILITY_NAME, '
            || ' ORD.D_CITY, ORD.D_STATE_PROV,'
            || ' ORD.PICKUP_START_DTTM, ORD.PICKUP_END_DTTM, ORD.PICKUP_TZ,'
            || ' ORD.DELIVERY_START_DTTM, ORD.DELIVERY_END_DTTM, ORD.DELIVERY_TZ,'
            || ' ORD.PROTECTION_LEVEL_ID,'
            || ' ORD.NORMALIZED_BASELINE_COST,'
            || ' CWSORD.SIZE1, CWSORD.SIZE1_UOM_ID,CWSORD.SIZE2, CWSORD.SIZE2_UOM_ID,'
            || ' CWSORD.SIZE3, CWSORD.SIZE3_UOM_ID,CWSORD.SIZE4, CWSORD.SIZE4_UOM_ID,' -- CT18
            || ' CWSOM.PICKUP_STOP_SEQ, CWSOM.DELIVERY_STOP_SEQ, '
			|| ' (SELECT DISTINCT WAVOPT.WAVE_ID FROM WAVE_OPTION_LIST WAVOPT WHERE WAVOPT.FACILITY_ID=ORD.D_FACILITY_ID AND WAVOPT.WAVE_ID=ORD.WAVE_ID AND WAVOPT.WAVE_OPTION_ID=ORD.WAVE_OPTION_ID AND WAVOPT.DOCK_ID=ORD.D_DOCK_ID AND WAVOPT.TC_COMPANY_ID=ORD.TC_COMPANY_ID AND WAVOPT.MARK_FOR_DELETION = 0) WAVE_ID, '
			|| ' ORD.IS_PARTIALLY_PLANNED, CWSORD.CWS_PROCESS_ID, 0 ORDER_SPLIT_ID , 0 ORDER_SPLIT_SEQ, '
            || ' rownum row_num '
            || ' from CWS_ORDER_MOVEMENT CWSOM, CWS_ORDER CWSORD, ORDERS ORD'
            || ' where (CWSOM.CONS_WORKSPACE_ID=:cwsId and CWSOM.WS_SHIPMENT_ID=:retCwsShipmentId)'
            || ' and   (CWSORD.CONS_WORKSPACE_ID=CWSOM.CONS_WORKSPACE_ID and CWSORD.ORDER_ID=CWSOM.ORDER_ID)'
            || ' and   (ORD.ORDER_ID=CWSORD.ORDER_ID)'
            || ' and   (CWSORD.IS_VISIBLE = 1)' || ' ' ;

        IF(allowSplit = 1) THEN
        sql_statement2 :=  sql_statement2
            || ' UNION '
            || ' select'
            || ' ORD.TC_COMPANY_ID,'
            || ' ORD.ORDER_ID, ORD.TC_ORDER_ID,'
            || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.O_FACILITY_ALIAS_ID AND FACILITY_ID = ORD.O_FACILITY_ID) O_FACILITY_NAME, '
            || ' ORD.O_CITY, ORD.O_STATE_PROV,'
            || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.D_FACILITY_ALIAS_ID AND FACILITY_ID = ORD.D_FACILITY_ID) D_FACILITY_NAME, '
            || ' ORD.D_CITY, ORD.D_STATE_PROV,'
            || ' ORD.PICKUP_START_DTTM, ORD.PICKUP_END_DTTM, ORD.PICKUP_TZ,'
            || ' ORD.DELIVERY_START_DTTM, ORD.DELIVERY_END_DTTM, ORD.DELIVERY_TZ,'
            || ' ORD.PROTECTION_LEVEL_ID,'
            || ' ORD.NORMALIZED_BASELINE_COST,'
            || ' CWSORDSPLIT.SIZE1, CWSORDSPLIT.SIZE1_UOM_ID,CWSORDSPLIT.SIZE2, CWSORDSPLIT.SIZE2_UOM_ID,'
            || ' CWSORDSPLIT.SIZE3, CWSORDSPLIT.SIZE3_UOM_ID,CWSORDSPLIT.SIZE4, CWSORDSPLIT.SIZE4_UOM_ID,' -- CT18
            || ' CWSOM.PICKUP_STOP_SEQ, CWSOM.DELIVERY_STOP_SEQ, '
			|| ' (SELECT DISTINCT WAVOPT.WAVE_ID FROM WAVE_OPTION_LIST WAVOPT WHERE WAVOPT.FACILITY_ID=ORD.D_FACILITY_ID AND WAVOPT.WAVE_ID=ORD.WAVE_ID AND WAVOPT.WAVE_OPTION_ID=ORD.WAVE_OPTION_ID AND WAVOPT.DOCK_ID=ORD.D_DOCK_ID AND WAVOPT.TC_COMPANY_ID=ORD.TC_COMPANY_ID AND WAVOPT.MARK_FOR_DELETION = 0) WAVE_ID, '
			|| ' ORD.IS_PARTIALLY_PLANNED, '
            || ' (select cws_process_id from cws_shipment where ws_shipment_id = :retCwsShipmentId), '
            || ' CWSORDSPLIT.ORDER_SPLIT_ID,ORDSPLIT.ORDER_SPLIT_SEQ, '
            || ' rownum row_num '
            || ' from CWS_ORDER_MOVEMENT CWSOM, CWS_ORDER_SPLIT CWSORDSPLIT,ORDER_SPLIT ORDSPLIT, ORDERS ORD'
            || ' where (CWSOM.CONS_WORKSPACE_ID=:cwsId and CWSOM.WS_SHIPMENT_ID=:retCwsShipmentId)'
            || ' and   (CWSORDSPLIT.CONS_WORKSPACE_ID=CWSOM.CONS_WORKSPACE_ID and CWSORDSPLIT.ORDER_SPLIT_ID=CWSOM.ORDER_SPLIT_ID)'
            || ' and   (ORD.ORDER_ID=CWSORDSPLIT.ORDER_ID)'
            || ' and (CWSORDSPLIT.ORDER_SPLIT_ID=ORDSPLIT.ORDER_SPLIT_ID)' || ' ' ;
        END IF;
        sql_statement2 := sql_statement2 || assignedOrderListOrderBy;
        sql_statement2 := sql_statement2 || ' )ASIGNORD  where row_num >= :minRow and row_num <= :maxRow ';  -- pagination parameter.

        IF(allowSplit = 1) THEN
            OPEN asg_order_list_cursor FOR sql_statement2    USING cwsId, retCwsShipmentId, retCwsShipmentId, cwsId, retCwsShipmentId,minRow,maxRow;
        ELSE
            OPEN asg_order_list_cursor FOR sql_statement2 USING cwsId, retCwsShipmentId,minRow,maxRow;
        END IF;
        assignedOrderListRefcur := asg_order_list_cursor;

    END load_WorkspaceAssignedOrders;

PROCEDURE load_WorkspaceUnassignedOrders (
                    unassignedOrderListRefcur OUT CURSOR_TYPE,
                    totalRows OUT NUMBER,        -- total number of rows
                                    -- Cws unassigned order display info
                    shipperId INTEGER,          -- company id
                    cwsId INTEGER,              -- workspace id
                    unassignedOrderListOrderBy VARCHAR2,
                       allowSplit NUMBER,
                       minRow NUMBER,           -- page to page param min row.
                    maxRow NUMBER,              -- page to page param max row.
                    unassignedOrderIdListCsv VARCHAR2) IS   -- speicific unassignedOrderList
        -- order by clause for unassigned orders list or empty string
        sql_statement1 VARCHAR ( 30000 );
        sql_statement3 VARCHAR ( 30000 );
        total_count_cursor CURSOR_TYPE;
        una_order_list_cursor CURSOR_TYPE;
        BEGIN

        -- Unassigned order list: Simple (I hope!).  Use order by clause (more dynamic sql ack).

    sql_statement1 := ' select count(1) from ( ';
    sql_statement1 := sql_statement1
        || ' select ORD.ORDER_ID, 0 '
        || ' from CWS_ORDER CWSORD, ORDERS ORD'
        || ' where (CWSORD.CONS_WORKSPACE_ID=:cwsId )'
        || ' and (not exists'
        || '   (select 1 from CWS_ORDER_MOVEMENT CWSOM where CWSOM.CONS_WORKSPACE_ID=CWSORD.CONS_WORKSPACE_ID and CWSOM.ORDER_ID=CWSORD.ORDER_ID))'
        || ' and (ORD.ORDER_ID=CWSORD.ORDER_ID)'
        || ' and   (CWSORD.IS_VISIBLE = 1)'
        || ' and   (ORD.PARENT_ORDER_ID is NULL)';
    IF(unassignedOrderIdListCsv IS NOT NULL)THEN
        sql_statement1 := sql_statement1
            || 'and CWSORD.ORDER_ID in ('
            ||unassignedOrderIdListCsv
            ||')';
    END IF;
    IF (allowSplit = 1) THEN
        sql_statement1 :=  sql_statement1
        || ' UNION '
        || ' select ORD.ORDER_ID, CWSORDSPLIT.ORDER_SPLIT_ID '
        || ' from  CWS_ORDER_SPLIT CWSORDSPLIT,ORDER_SPLIT ORDSPLIT, ORDERS ORD'
        || ' where (CWSORDSPLIT.CONS_WORKSPACE_ID=:cwsId)'
        || ' and (not exists'
        || '   (select 1 from CWS_ORDER_MOVEMENT CWSOM where CWSOM.CONS_WORKSPACE_ID=CWSORDSPLIT.CONS_WORKSPACE_ID and CWSOM.ORDER_SPLIT_ID=CWSORDSPLIT.ORDER_SPLIT_ID))'
        || ' and (ORD.ORDER_ID=CWSORDSPLIT.ORDER_ID)'
        || ' and (CWSORDSPLIT.ORDER_SPLIT_ID=ORDSPLIT.ORDER_SPLIT_ID)'
        || ' and (ORD.PARENT_ORDER_ID is NULL)';
 IF(unassignedOrderIdListCsv IS NOT NULL)THEN
            sql_statement1 := sql_statement1
                || 'and CWSORDSPLIT.ORDER_ID in ('
                || unassignedOrderIdListCsv
                ||')';
        END IF;
    END IF;
    sql_statement1 := sql_statement1 || ' ) ';

    sql_statement3 := ' select '
        || ' UNASGNORD.TC_COMPANY_ID, '
        || ' UNASGNORD.TC_SHIPMENT_ID, '
        || ' UNASGNORD.WS_SHIPMENT_ID, '
        || ' UNASGNORD.ORDER_ID, '
        || ' UNASGNORD.TC_ORDER_ID, '
        || ' UNASGNORD.O_FACILITY_NAME,  '
        || ' UNASGNORD.O_CITY, '
        || ' UNASGNORD.O_STATE_PROV, '
        || ' UNASGNORD.D_FACILITY_NAME,  '
        || ' UNASGNORD.D_CITY, '
        || ' UNASGNORD.D_STATE_PROV, '
        || ' UNASGNORD.PICKUP_START_DTTM, '
        || ' UNASGNORD.PICKUP_END_DTTM, '
        || ' UNASGNORD.PICKUP_TZ, '
        || ' UNASGNORD.DELIVERY_START_DTTM, '
        || ' UNASGNORD.DELIVERY_END_DTTM, '
        || ' UNASGNORD.DELIVERY_TZ, '
        || ' UNASGNORD.PROTECTION_LEVEL_ID, '
        || ' UNASGNORD.NORMALIZED_BASELINE_COST, '
        || ' UNASGNORD.SIZE1, '
        || ' UNASGNORD.SIZE1_UOM_ID, '
        || ' UNASGNORD.SIZE2, '
        || ' UNASGNORD.SIZE2_UOM_ID, '
        || ' UNASGNORD.SIZE3, '
        || ' UNASGNORD.SIZE3_UOM_ID, '
        -- CT18
        || ' UNASGNORD.SIZE4, '
        || ' UNASGNORD.SIZE4_UOM_ID, '
        || ' UNASGNORD.PRODUCT_CLASS_ID, '
        || ' UNASGNORD.PICKUP_STOP_SEQ, '
        || ' UNASGNORD.DELIVERY_STOP_SEQ, '
        || ' UNASGNORD.WAVE_ID, '
        || ' UNASGNORD.IS_PARTIALLY_PLANNED, '
	|| ' UNASGNORD.WAVE_OPTION_ID, '
        || ' 0, '
        || ' UNASGNORD.ORDER_SPLIT_ID, '
        || ' UNASGNORD.ORDER_SPLIT_SEQ, '
        || ' UNASGNORD.REF_FIELD_1,'
        || ' UNASGNORD.REF_FIELD_2,'
        || ' UNASGNORD.REF_FIELD_3,'
        || ' UNASGNORD.REF_FIELD_4,'
        || ' UNASGNORD.REF_FIELD_5,'
        || ' UNASGNORD.RTE_SWC_NBR,'
        || ' UNASGNORD.DSG_STATIC_ROUTE_ID,'
		|| ' UNASGNORD.DO_STATUS,'
		|| ' UNASGNORD.DSG_HUB_LOCATION_ID,'
		|| ' UNASGNORD.O_POSTAL_CODE, '
		|| ' UNASGNORD.D_POSTAL_CODE, '
		|| ' UNASGNORD.O_ADDRESS_1, '
		|| ' UNASGNORD.D_ADDRESS_1, '
		|| ' UNASGNORD.ORDER_REF_FIELD_1,'
        || ' UNASGNORD.ORDER_REF_FIELD_2,'
        || ' UNASGNORD.ORDER_REF_FIELD_3,'
        || ' UNASGNORD.ORDER_REF_FIELD_4,'
        || ' UNASGNORD.ORDER_REF_FIELD_5'
        || ' from( ';

        sql_statement3 := sql_statement3
            || 'select'
            || ' ORD.TC_COMPANY_ID,'
            || ' null TC_SHIPMENT_ID, null WS_SHIPMENT_ID, '
            || ' ORD.ORDER_ID, ORD.TC_ORDER_ID,'
            || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.O_FACILITY_ALIAS_ID AND FACILITY_ID = ORD.O_FACILITY_ID) O_FACILITY_NAME, '
            || ' ORD.O_CITY, ORD.O_STATE_PROV,'
            || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.D_FACILITY_ALIAS_ID AND FACILITY_ID = ORD.D_FACILITY_ID) D_FACILITY_NAME, '
            || ' ORD.D_CITY, ORD.D_STATE_PROV,'
            || ' ORD.PICKUP_START_DTTM, ORD.PICKUP_END_DTTM, ORD.PICKUP_TZ,'
            || ' ORD.DELIVERY_START_DTTM, ORD.DELIVERY_END_DTTM, ORD.DELIVERY_TZ,'
            || ' ORD.PROTECTION_LEVEL_ID,'
            || ' ORD.NORMALIZED_BASELINE_COST,'
            || ' CWSORD.SIZE1, CWSORD.SIZE1_UOM_ID, CWSORD.SIZE2, CWSORD.SIZE2_UOM_ID,'
            || ' CWSORD.SIZE3, CWSORD.SIZE3_UOM_ID, CWSORD.SIZE4, CWSORD.SIZE4_UOM_ID, ORD.PRODUCT_CLASS_ID, ' -- CT18
            || ' null PICKUP_STOP_SEQ, null DELIVERY_STOP_SEQ, '
			|| ' (SELECT DISTINCT WAVOPT.WAVE_ID FROM WAVE_OPTION_LIST WAVOPT WHERE WAVOPT.FACILITY_ID=ORD.D_FACILITY_ID AND WAVOPT.WAVE_ID=ORD.WAVE_ID AND WAVOPT.WAVE_OPTION_ID=ORD.WAVE_OPTION_ID AND WAVOPT.DOCK_ID=ORD.D_DOCK_ID AND WAVOPT.TC_COMPANY_ID=ORD.TC_COMPANY_ID AND WAVOPT.MARK_FOR_DELETION = 0) WAVE_ID, '
			|| ' ORD.IS_PARTIALLY_PLANNED, (SELECT DISTINCT WAVOPT.WAVE_OPTION_ID FROM WAVE_OPTION_LIST WAVOPT WHERE WAVOPT.FACILITY_ID=ORD.D_FACILITY_ID AND WAVOPT.WAVE_ID=ORD.WAVE_ID AND WAVOPT.WAVE_OPTION_ID=ORD.WAVE_OPTION_ID AND WAVOPT.DOCK_ID=ORD.D_DOCK_ID AND WAVOPT.TC_COMPANY_ID=ORD.TC_COMPANY_ID AND WAVOPT.MARK_FOR_DELETION = 0) WAVE_OPTION_ID, 0, null ORDER_SPLIT_ID, '
            || ' null ORDER_SPLIT_SEQ, '
            || ' (select note from facility_note where facility_id=ORD.D_FACILITY_ID and facility_note_type=1 AND dock_id IS NULL AND is_reference = 1) REF_FIELD_1, '
            || ' (select note from facility_note where facility_id=ORD.D_FACILITY_ID and facility_note_type=2 AND dock_id IS NULL AND is_reference = 1) REF_FIELD_2, '
            || ' (select note from facility_note where facility_id=ORD.D_FACILITY_ID and facility_note_type=3 AND dock_id IS NULL AND is_reference = 1) REF_FIELD_3, '
            || ' (select note from facility_note where facility_id=ORD.D_FACILITY_ID and facility_note_type=4 AND dock_id IS NULL AND is_reference = 1) REF_FIELD_4, '
            || ' (select note from facility_note where facility_id=ORD.D_FACILITY_ID and facility_note_type=5 AND dock_id IS NULL AND is_reference = 1) REF_FIELD_5, '
            || ' ORD.RTE_SWC_NBR, '
            || ' ORD.DSG_STATIC_ROUTE_ID,'
			|| ' ORD.DO_STATUS,'
			|| ' ORD.DSG_HUB_LOCATION_ID,'
			|| ' ORD.O_POSTAL_CODE,ORD.D_POSTAL_CODE,'
			|| ' ORD.O_ADDRESS_1,ORD.D_ADDRESS_1,'
			|| ' ORD.REF_FIELD_1 ORDER_REF_FIELD_1,'
            || ' ORD.REF_FIELD_2 ORDER_REF_FIELD_2,'
            || ' ORD.REF_FIELD_3 ORDER_REF_FIELD_3,'
            || ' ORD.REF_FIELD_4 ORDER_REF_FIELD_4,'
            || ' ORD.REF_FIELD_5 ORDER_REF_FIELD_5,'
            || ' rownum row_num '
            || ' from CWS_ORDER CWSORD, ORDERS ORD'
            || ' where (CWSORD.CONS_WORKSPACE_ID=:cwsId )'
            || ' and (not exists'
            || '   (select 1 from CWS_ORDER_MOVEMENT CWSOM where CWSOM.CONS_WORKSPACE_ID=CWSORD.CONS_WORKSPACE_ID and CWSOM.ORDER_ID=CWSORD.ORDER_ID))'
            || ' and (ORD.ORDER_ID=CWSORD.ORDER_ID)'
            || ' and   (CWSORD.IS_VISIBLE = 1)'
            || ' and   (ORD.PARENT_ORDER_ID is NULL)'
            || ' ' ;
 IF(unassignedOrderIdListCsv IS NOT NULL)THEN
            sql_statement3 := sql_statement3
                || 'and CWSORD.ORDER_ID in ('
                || unassignedOrderIdListCsv
                ||')';
        END IF;

        IF (allowSplit = 1) THEN
        sql_statement3 :=  sql_statement3
            || ' UNION '
            || 'select'
            || ' ORD.TC_COMPANY_ID,'
            || ' null TC_SHIPMENT_ID, null WS_SHIPMENT_ID, '
            || ' ORD.ORDER_ID, ORD.TC_ORDER_ID,'
            || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.O_FACILITY_ALIAS_ID AND FACILITY_ID = ORD.O_FACILITY_ID) O_FACILITY_NAME, '
            || ' ORD.O_CITY, ORD.O_STATE_PROV,'
            || ' (select FACILITY_NAME from FACILITY_ALIAS where FACILITY_ALIAS_ID = ORD.D_FACILITY_ALIAS_ID AND FACILITY_ID = ORD.D_FACILITY_ID) D_FACILITY_NAME, '
            || ' ORD.D_CITY, ORD.D_STATE_PROV,'
            || ' ORD.PICKUP_START_DTTM, ORD.PICKUP_END_DTTM, ORD.PICKUP_TZ,'
            || ' ORD.DELIVERY_START_DTTM, ORD.DELIVERY_END_DTTM, ORD.DELIVERY_TZ,'
            || ' ORD.PROTECTION_LEVEL_ID,'
            || ' ORDSPLIT.BASELINE_COST,'
            || ' CWSORDSPLIT.SIZE1, CWSORDSPLIT.SIZE1_UOM_ID, CWSORDSPLIT.SIZE2, CWSORDSPLIT.SIZE2_UOM_ID,'
            || ' CWSORDSPLIT.SIZE3, CWSORDSPLIT.SIZE3_UOM_ID, CWSORDSPLIT.SIZE4, CWSORDSPLIT.SIZE4_UOM_ID, ORD.PRODUCT_CLASS_ID, ' -- CT18
            || ' 0 PICKUP_STOP_SEQ, 0 DELIVERY_STOP_SEQ, '
            || ' (SELECT DISTINCT WAVOPT.WAVE_ID FROM WAVE_OPTION_LIST WAVOPT WHERE WAVOPT.FACILITY_ID=ORD.D_FACILITY_ID AND WAVOPT.WAVE_ID=ORD.WAVE_ID AND WAVOPT.WAVE_OPTION_ID=ORD.WAVE_OPTION_ID AND WAVOPT.DOCK_ID=ORD.D_DOCK_ID AND WAVOPT.TC_COMPANY_ID=ORD.TC_COMPANY_ID AND WAVOPT.MARK_FOR_DELETION = 0) WAVE_ID, '
            || ' ORD.IS_PARTIALLY_PLANNED, (SELECT DISTINCT WAVOPT.WAVE_OPTION_ID FROM WAVE_OPTION_LIST WAVOPT WHERE WAVOPT.FACILITY_ID=ORD.D_FACILITY_ID AND WAVOPT.WAVE_ID=ORD.WAVE_ID AND WAVOPT.WAVE_OPTION_ID=ORD.WAVE_OPTION_ID AND WAVOPT.DOCK_ID=ORD.D_DOCK_ID AND WAVOPT.TC_COMPANY_ID=ORD.TC_COMPANY_ID AND WAVOPT.MARK_FOR_DELETION = 0) WAVE_OPTION_ID, 0, CWSORDSPLIT.ORDER_SPLIT_ID,ORDSPLIT.ORDER_SPLIT_SEQ, '
            || ' (select note from facility_note where facility_id=ORD.D_FACILITY_ID and facility_note_type=1 AND dock_id IS NULL AND is_reference = 1) REF_FIELD_1, '
            || ' (select note from facility_note where facility_id=ORD.D_FACILITY_ID and facility_note_type=2 AND dock_id IS NULL AND is_reference = 1) REF_FIELD_2, '
            || ' (select note from facility_note where facility_id=ORD.D_FACILITY_ID and facility_note_type=3 AND dock_id IS NULL AND is_reference = 1) REF_FIELD_3, '
            || ' (select note from facility_note where facility_id=ORD.D_FACILITY_ID and facility_note_type=4 AND dock_id IS NULL AND is_reference = 1) REF_FIELD_4, '
            || ' (select note from facility_note where facility_id=ORD.D_FACILITY_ID and facility_note_type=5 AND dock_id IS NULL AND is_reference = 1) REF_FIELD_5, '

            || ' ORD.RTE_SWC_NBR, '
            || ' ORD.DSG_STATIC_ROUTE_ID,'
			|| ' ORD.DO_STATUS,'
			|| ' ORD.DSG_HUB_LOCATION_ID,'
			|| ' ORD.O_POSTAL_CODE,ORD.D_POSTAL_CODE,'
			|| ' ORD.O_ADDRESS_1,ORD.D_ADDRESS_1,'
			|| ' ORD.REF_FIELD_1 ORDER_REF_FIELD_1, '
            || ' ORD.REF_FIELD_2 ORDER_REF_FIELD_2, '
            || ' ORD.REF_FIELD_3 ORDER_REF_FIELD_3, '
            || ' ORD.REF_FIELD_4 ORDER_REF_FIELD_4, '
            || ' ORD.REF_FIELD_5 ORDER_REF_FIELD_5, '
            || ' rownum row_num '
            || ' from CWS_ORDER_SPLIT CWSORDSPLIT,ORDER_SPLIT ORDSPLIT, ORDERS ORD'
            || ' where (CWSORDSPLIT.CONS_WORKSPACE_ID=:cwsId)'
            || ' and (not exists'
            || '   (select 1 from CWS_ORDER_MOVEMENT CWSOM where CWSOM.CONS_WORKSPACE_ID=CWSORDSPLIT.CONS_WORKSPACE_ID and CWSOM.ORDER_SPLIT_ID=CWSORDSPLIT.ORDER_SPLIT_ID))'
            || ' and (ORD.ORDER_ID=CWSORDSPLIT.ORDER_ID)'
            || ' and (CWSORDSPLIT.ORDER_SPLIT_ID=ORDSPLIT.ORDER_SPLIT_ID)'
            || ' and (ORD.PARENT_ORDER_ID is NULL)' || ' ';
     IF(unassignedOrderIdListCsv IS NOT NULL)THEN
                sql_statement3 := sql_statement3
                    || 'and CWSORDSPLIT.ORDER_ID in ('
                    || unassignedOrderIdListCsv
                    ||')';
            END IF;
        END IF;
        sql_statement3 := sql_statement3 || unassignedOrderListOrderBy;
        sql_statement3 := sql_statement3 || ' )UNASGNORD where row_num >= :minRow and row_num <= :maxRow';

    IF (allowSplit = 1) THEN
        OPEN total_count_cursor FOR sql_statement1    USING cwsId, cwsId;
            FETCH total_count_cursor INTO totalRows;
        CLOSE total_count_cursor;
    ELSE
        OPEN total_count_cursor FOR sql_statement1    USING cwsId;
        FETCH total_count_cursor INTO totalRows;
        CLOSE total_count_cursor;
        END IF;

        IF (allowSplit = 1) THEN
            OPEN una_order_list_cursor FOR sql_statement3    USING cwsId, cwsId, minRow, maxRow;
        ELSE
            OPEN una_order_list_cursor FOR sql_statement3    USING cwsId, minRow, maxRow;
        END IF;
        unassignedOrderListRefcur := una_order_list_cursor;
  END load_WorkspaceUnassignedOrders;

PROCEDURE load_WorkspaceShipments( shipmentListRefcur OUT CURSOR_TYPE,
                                    -- Cws shipment display info
                    retCwsShipmentId OUT INTEGER,
                                    -- current draft shipment id as determined by the sproc
                    modifiedMinRow OUT INTEGER,
                    modifiedMaxRow OUT INTEGER,
                    statCwsShipmentCount OUT NUMBER,
                    shipperId INTEGER,          -- company id
                    cwsId INTEGER,              -- workspace id
                    currentCwsShipmentId INTEGER,
                                    -- current draft shipment id
                    shipmentListOrderBy VARCHAR2,
                                    -- order by clause for shipment list or empty string
                    shipmentListMinRow NUMBER,  -- first row to retrieve (0-based)
                    shipmentListMaxRow NUMBER,  -- last row + 1 to retrieve (ex: 0..10 retrieves first 10 shipments)
                    orderIdMDFilter NUMBER,     -- for MD link, if non-null, joins to cws_order_movement to restrict list of shipment to this order id
                        allowSplit NUMBER ) IS
        -- order by clause for unassigned orders list or empty string
        sql_statement1 VARCHAR ( 2000 );
        sql_statement4 VARCHAR ( 2000 );
        shipment_id_cursor CURSOR_TYPE;
        vcurrow NUMBER;
        vidlist_done NUMBER;
        vShipmentId CWS_SHIPMENT.WS_SHIPMENT_ID%TYPE;
        vgttcount NUMBER;
        shipment_list_cursor CURSOR_TYPE;


        firstCwsShipmentId CWS_SHIPMENT.WS_SHIPMENT_ID%TYPE;
        foundCurrentShipmentId NUMBER ( 1 );
        position_count_cursor CURSOR_TYPE;
        position_of_current_shipment NUMBER;
        min_max_difference NUMBER;
        shipmentListMinRow_local NUMBER;
        shipmentListMaxRow_local NUMBER;
        BEGIN
        -- First get the shipment id list for the given range in the correct order
        DELETE MAN_CONS_SHIPMENT_LIST_GTT;
        --sshrivastava>>copy min row and max row variable to local variables
        shipmentListMinRow_local := shipmentListMinRow;
        shipmentListMaxRow_local := shipmentListMaxRow;
        sql_statement4 := 'select cws_shipment.ws_shipment_id from cws_shipment' || ' where cws_shipment.cons_workspace_id=:cwsId ';
        IF orderIdMDFilter IS NOT NULL THEN
            sql_statement4 := sql_statement4 || ' and cws_shipment.ws_shipment_id in' || ' (select distinct cws_order_movement.ws_shipment_id from cws_order_movement where' || '  cws_order_movement.cons_workspace_id=:cwsId and' || '  cws_order_movement.order_id=:orderIdMD)';
        END IF;
        sql_statement4 := sql_statement4 || ' ' || shipmentListOrderBy;
        sql_statement4 := 'select rownum row_num, ws_shipment_id from (' || sql_statement4 || ')';
        --sshrivastava>>If current shipment Id has been passed to the procedure then find out the position of the current cws shipment first
        IF currentCwsShipmentId IS NOT NULL THEN
            sql_statement4 := 'select a.row_num from (' || sql_statement4 || ') a where a.ws_shipment_id = :currentCwsShipmentId';
            IF orderIdMDFilter IS NOT NULL THEN
             OPEN position_count_cursor
              FOR sql_statement4
            USING cwsId,
                  cwsId,
                  orderIdMDFilter,
                  currentCwsShipmentId;
            ELSE
             OPEN position_count_cursor
              FOR sql_statement4
            USING cwsId,
                  currentCwsShipmentId;
            END IF;
            FETCH position_count_cursor INTO position_of_current_shipment;
            CLOSE position_count_cursor;
            --sshrivastava>>min_max_difference will give number of records to be shown on the page
            min_max_difference := shipmentListMaxRow_local - shipmentListMinRow_local;
            IF position_of_current_shipment <= 0 THEN
            position_of_current_shipment := 0;
            --ELSE
            --position_of_current_shipment := position_of_current_shipment - 1;
            END IF;
            --increment or decrement min row and max row, till position of current shipment fall between them
            WHILE NOT ( position_of_current_shipment >= shipmentListMinRow_local AND position_of_current_shipment <= shipmentListMaxRow_local ) LOOP
            IF position_of_current_shipment > shipmentListMinRow_local THEN
                shipmentListMinRow_local := shipmentListMinRow_local + min_max_difference;
                shipmentListMaxRow_local := shipmentListMaxRow_local + min_max_difference;
                ELSIF position_of_current_shipment < shipmentListMinRow_local THEN shipmentListMinRow_local := shipmentListMinRow_local - min_max_difference;
                shipmentListMaxRow_local := shipmentListMaxRow_local - min_max_difference;
            END IF;
            END LOOP;
            --sshrivastava>>copy the min row and max row to modified min row and max row out parameters
            --these parameters will be used later to update the page number on list page
            modifiedMinRow := shipmentListMinRow_local;
            modifiedMaxRow := shipmentListMaxRow_local;
        END IF;
        sql_statement1 := 'select cws_shipment.ws_shipment_id from cws_shipment' || '  where cws_shipment.cons_workspace_id=:cwsId';
        -- Dynamically build and bind the select for just the id's
        IF orderIdMDFilter IS NOT NULL THEN
            sql_statement1 := sql_statement1 || ' and cws_shipment.ws_shipment_id in' || ' (select distinct cws_order_movement.ws_shipment_id from cws_order_movement where' || '  cws_order_movement.cons_workspace_id=:cwsId and' || '  cws_order_movement.order_id=:orderIdMD)';
        END IF;
        sql_statement1 := sql_statement1 || ' ' || shipmentListOrderBy;
        IF orderIdMDFilter IS NOT NULL THEN
             OPEN shipment_id_cursor
              FOR sql_statement1
            USING cwsId,
              cwsId,
              orderIdMDFilter;
        ELSE
             OPEN shipment_id_cursor
              FOR sql_statement1
            USING cwsId;
        END IF;
        -- Skip rows before minrow, starting at 0
        vcurrow := 0;
        vidlist_done := 0;
        foundCurrentShipmentId := 0;
        <<c1_cursor_loop>>
     LOOP
            /*DBMS_OUTPUT.put_line('c1 loop, vcurrow = ' || to_char(vcurrow));*/
            EXIT WHEN vcurrow >= shipmentListMinRow_local;
            FETCH shipment_id_cursor INTO vShipmentId;
            IF shipment_id_cursor%NOTFOUND THEN
            vidlist_done := 1;
            EXIT;
            END IF;
            /*DBMS_OUTPUT.put_line('c1 loop, fetched id = ' || to_char(vshipment_id));*/
            vcurrow := vcurrow + 1;
        END LOOP c1_cursor_loop;
        -- If any rows left, add these id's, up to maxrow-1
        IF vidlist_done = 0 THEN
            <<c2_cursor_loop>>
     LOOP
            /*DBMS_OUTPUT.put_line('c2 loop, vcurrow = ' || to_char(vcurrow));*/
            EXIT WHEN vcurrow >= shipmentListMaxRow_local;
            FETCH shipment_id_cursor INTO vShipmentId;
            EXIT WHEN shipment_id_cursor%NOTFOUND;
            /*DBMS_OUTPUT.put_line('c2 loop, fetched id = ' || to_char(vshipment_id));*/
            INSERT INTO MAN_CONS_SHIPMENT_LIST_GTT ( ROW_NUM,
                                 SHIPMENT_ID )
            VALUES ( vcurrow,
                 vShipmentId );
            vcurrow := vcurrow + 1;
            -- Save first shipment id in case user has no context for current shipment
            IF firstCwsShipmentId IS NULL THEN
                firstCwsShipmentId := vShipmentId;
            END IF;
            IF vShipmentId = currentCwsShipmentId THEN
                foundCurrentShipmentId := 1;
            END IF;
            END LOOP c2_cursor_loop;
--            COMMIT;
        END IF;
        CLOSE shipment_id_cursor;
        /* Debugging
        select count(1) into vgttcount from MAN_CONS_SHIPMENT_LIST_GTT;
        DBMS_OUTPUT.put_line('MAN_CONS_SHIPMENT_LIST_GTT count = ' || to_char(vgttcount));
         */
        -- Materialize the resultset based on the id's and the order of the row #'s
         OPEN shipment_list_cursor
          FOR SELECT /*+ ordered first_rows */ CWSSHP.TC_COMPANY_ID,
              CWSSHP.WS_SHIPMENT_ID,
              CWSSHP.TC_SHIPMENT_ID,
              CWSSHP.ORIG_SHIPMENT_ID,
              CWSSHP.O_CITY,
              CWSSHP.O_STATE_PROV,                          -- origin
              CWSSHP.D_CITY,
              CWSSHP.D_STATE_PROV,                          -- dest
              CWSSHP.TOTAL_DISTANCE,
              CWSSHP.FEASIBLE_MOT_ID,                          -- feasible mode
              CWSSHP.FEASIBLE_EQUIPMENT_ID,            -- feasible equipment
              CWSSHP.SIZE1_VALUE,
              CWSSHP.SIZE1_UOM_ID,
              CWSSHP.SIZE2_VALUE,
              CWSSHP.SIZE2_UOM_ID,                             -- size1+UOM1, size2+UOM2
              -- CT18
              CWSSHP.SIZE3_VALUE,
              CWSSHP.SIZE3_UOM_ID,
              CWSSHP.SIZE4_VALUE,
              CWSSHP.SIZE4_UOM_ID,
              CWSSHP.NUM_STOPS,                             -- # stops
              CWSSHP.PICKUP_END_DTTM,
              CWSSHP.PICKUP_TZ,                             -- pickup end + TZ
              CWSSHP.DELIVERY_END_DTTM,
              CWSSHP.DELIVERY_TZ,                           -- delivery end + TZ
              CWSSHP.PROMOTE_TO_AVAIL_FLAG,                 -- promotion flag
              CWSSHP.ESTIMATED_COST,                        -- est. cost
              CWSSHP.HAS_HARD_CHECK_ERROR,
              CWSSHP.HAS_SOFT_CHECK_ERROR,                  -- hard/soft check flags
              CWSSHP.NEEDS_VALIDATION,
              CWSSHP.BASELINE_COST,                          --# This has been included for 4r1 as baseline cost is not being fetched
              CWSSHP.IS_TIME_FEAS_ENABLED,
              CWSSHP.DISTANCE_UOM,
              CWSSHP.CWS_PROCESS_ID
         FROM MAN_CONS_SHIPMENT_LIST_GTT MCSLGTT,
              CWS_SHIPMENT CWSSHP
        WHERE CWSSHP.CONS_WORKSPACE_ID = cwsId
          AND MCSLGTT.SHIPMENT_ID = CWSSHP.WS_SHIPMENT_ID
        ORDER BY MCSLGTT.ROW_NUM;
        -- Assigned order list: If passed a current cws shipment id.  If not, use the first shipment id
        -- retrieved above and set this in retCwsShipmentId.  Use order by clause (this requires dynamic sql...).

        IF ( currentCwsShipmentId IS NULL ) THEN
            retCwsShipmentId := firstCwsShipmentId;
        ELSE
            retCwsShipmentId := currentCwsShipmentId;
        END IF;

        -- Set return ref cursors
        shipmentListRefcur := shipment_list_cursor;

         SELECT COUNT ( 1 ) INTO statCwsShipmentCount
         FROM CWS_SHIPMENT
         WHERE CWS_SHIPMENT.CONS_WORKSPACE_ID = cwsId;

   END load_WorkspaceShipments;

end MAN_CONS_PKG;
/
create or replace procedure wm_unplan_splits_intrnl
(
    p_user_id          in user_profile.user_id%type,
    p_is_cs_or_cm      in number default 0,
    p_mode             in varchar2 default null
)
as
    type t_shipment_id is table of shipment.shipment_id%type index by binary_integer;
    va_shpmt_id        t_shipment_id;
    v_row_count        number(9) := 0;
begin
    if (p_mode = '_unplan_')
    then
        -- do not unplan a split if any lpn is manifested or above
        delete from tmp_unplan_splits t
        where exists
        (
            select 1
            from lpn l
            where l.order_id = t.order_id
                and coalesce(l.order_split_id, -1) = coalesce(t.order_split_id, -1)
                and l.lpn_type = 1 and l.inbound_outbound_indicator = 'O'
                and l.lpn_facility_status between 40 and 90
        );
        v_row_count := sql%rowcount;
        wm_cs_log('Splits removed from unplanning due to invalid lpn status '
            || v_row_count, p_sql_log_level => case when v_row_count > 0 then 0 else 1 end);
    end if;

    if (p_is_cs_or_cm = 0)
    then
        -- CS currently unassigns lpns separately; this is in order to be
        -- able to consolidate them under an unplanned split
        update lpn l
        set l.tc_shipment_id = null, l.shipment_id = null, l.plan_load_id = null,
            l.ship_via = null, l.tracking_nbr = null, l.alt_tracking_nbr = null,
            l.last_updated_dttm = sysdate, l.last_updated_source = p_user_id,
            l.last_updated_source_type = 1, l.init_ship_via = null,
            l.distribution_leg_carrier_id = null, l.distribution_leg_mode_id = null,
            l.distribution_lev_svce_level_id = null
        where exists
        (
            select 1
            from tmp_unplan_splits t
            where t.order_id = l.order_id
                and coalesce(t.order_split_id, -1) = coalesce(l.order_split_id, -1)
        );
        wm_cs_log('Lpns unassigned ' || sql%rowcount);
    end if;

    insert into tmp_stop_action
    (
        shipment_id, stop_seq, stop_action_seq, static_route_id
    )
    select distinct sao.shipment_id, sao.stop_seq, sao.stop_action_seq, s.static_route_id
    from stop_action_order sao
    join shipment s on s.shipment_id = sao.shipment_id
    where exists
    (
        select 1
        from tmp_unplan_splits t
        where t.order_id = sao.order_id
            and coalesce(t.order_split_id, -1) = coalesce(sao.order_split_id, -1)
    );
    wm_cs_log('Collected stop action rows ' || sql%rowcount, p_sql_log_level => 2);

    delete from order_movement om
    where exists
    (
        select 1
        from tmp_unplan_splits t
        where t.order_id = om.order_id
            and coalesce(t.order_split_id, -1) = coalesce(om.order_split_id, -1)
    );
    wm_cs_log('Deleted OM ' || sql%rowcount);

    delete from stop_action_order sao
    where exists
    (
        select 1
        from tmp_unplan_splits t
        where t.order_id = sao.order_id
            and coalesce(t.order_split_id, -1) = coalesce(sao.order_split_id, -1)
    );
    wm_cs_log('Deleted SAO ' || sql%rowcount);

    delete from stop_action sa
    where exists
        (
            select 1
            from tmp_stop_action t
            where t.shipment_id = sa.shipment_id and t.stop_seq = sa.stop_seq
                and t.stop_action_seq = sa.stop_action_seq
                and t.static_route_id is null
        )
        and not exists
        (
            select 1
            from stop_action_order sao
            where sao.shipment_id = sa.shipment_id
                and sao.stop_seq = sa.stop_seq
                and sao.stop_action_seq = sa.stop_action_seq
        );
    wm_cs_log('Deleted stop action ' || sql%rowcount);

    -- Added call to delete records from STOP EXTN tables.
    te_remove_stop_extn_data;

    delete from shipment_note sn
    where exists
        (
            select 1
            from tmp_stop_action t
            where t.shipment_id = sn.shipment_id and t.stop_seq = sn.stop_seq
        )
        and not exists
        (
            select 1
            from stop_action sa
            where sa.shipment_id = sn.shipment_id and sa.stop_seq = sn.stop_seq
        );
    wm_cs_log('Deleted shipment notes ' || sql%rowcount);

    delete from stop s
    where exists
        (
            select 1
            from tmp_stop_action t
            where t.shipment_id = s.shipment_id and t.stop_seq = s.stop_seq
        )
        and not exists
        (
            select 1
            from stop_action sa
            where sa.shipment_id = s.shipment_id and sa.stop_seq = s.stop_seq
        );
    wm_cs_log('Deleted stops ' || sql%rowcount);

    -- cancel splits in CS/CM flows as we are recreating and consolidating them
    update order_split os
    set os.order_split_status = 5, os.last_updated_dttm = sysdate,
        os.last_updated_source = p_user_id, os.last_updated_source_type = 1,
        os.shipment_id = null, os.assigned_mot_id = null,
        os.assigned_service_level_id = null, os.assigned_carrier_id = null,
        os.assigned_equipment_id = null, os.dynamic_routing_reqd = null,
        os.line_haul_ship_via = null, os.zone_skip_hub_location_id = null,
        os.distribution_ship_via = null, os.baseline_cost = null,
        os.baseline_cost_currency_code = null, 
        os.is_cancelled = decode(p_is_cs_or_cm, 1, 1, 0),
        os.original_assigned_ship_via = decode(p_is_cs_or_cm, 1, null, os.original_assigned_ship_via)
    where exists
    (
        select 1
        from tmp_unplan_splits t
        where t.order_split_id = os.order_split_id
    );
    wm_cs_log('Splits ' || case p_is_cs_or_cm when 1 then 'cancelled ' else 'unplanned ' end || sql%rowcount);

    merge into orders o
    using
    (
        select t.order_id, coalesce(min(os.order_split_status), 5) order_split_status
        from tmp_unplan_splits t
        left join order_split os on os.order_id = t.order_id and os.is_cancelled = 0
        group by t.order_id
    ) iv on (iv.order_id = o.order_id)
    when matched then
    update set o.actual_cost = null, o.actual_cost_currency_code = null,
        o.assigned_carrier_id = null, o.assigned_mot_id = null,
        o.assigned_service_level_id = null, o.assigned_equipment_id = null,
        o.baseline_cost = null, o.baseline_cost_currency_code = null,
        o.distribution_ship_via = null, o.dynamic_routing_reqd = null,
        o.last_updated_dttm = sysdate, o.last_updated_source = p_user_id,
        o.last_updated_source_type = 1, o.line_haul_ship_via = null,
        o.shipment_id = null, o.tc_shipment_id = null,
        o.zone_skip_hub_location_id = null, 
        o.path_id = null, o.path_set_id = null, o.plan_d_facility_alias_id = (case when o.path_set_id is not null then null else o.plan_d_facility_alias_id end),
        o.plan_d_facility_id = (case when o.path_set_id is not null then null else o.plan_d_facility_id end),
        o.order_status = iv.order_split_status
    where iv.order_split_status != o.order_status or o.shipment_id is not null;
    wm_cs_log('Orders needing unplanning ' || sql%rowcount);

    if (p_is_cs_or_cm = 0)
    then
        -- assumption: a shipment is never empty when closed, so complete 
        -- unplanning isnt realistic for CS/CM; in other flows, delete/cancel 
        -- shpmts if all orders are completely unplanned
        select distinct t.shipment_id
        bulk collect into va_shpmt_id
        from tmp_stop_action t
        where not exists
        (
            select 1
            from stop_action_order sao
            where sao.shipment_id = t.shipment_id
        );
        wm_cs_log('Stop action rows to remove ' || sql%rowcount, p_sql_log_level => 2);
    
        forall i in 1..va_shpmt_id.count
        delete from shipment_accessorial sa
        where sa.shipment_id = va_shpmt_id(i);
        wm_cs_log('Deleted accessorials ' || sql%rowcount);
        
        forall i in 1..va_shpmt_id.count
        delete from shipment_commodity sc
        where sc.shipment_id = va_shpmt_id(i);
        wm_cs_log('Deleted commodities ' || sql%rowcount);
        
        forall i in 1..va_shpmt_id.count
        delete from shipment_size ss
        where ss.shipment_id = va_shpmt_id(i);
        wm_cs_log('Deleted shpmt sizes ' || sql%rowcount);
    
        forall i in 1..va_shpmt_id.count
        update shipment s
        set s.is_cancelled = 1, s.shipment_status = 120, s.o_address = null,
           s.o_city = null, s.o_country_code = null, s.o_county = null,
           s.o_facility_id = null, s.o_postal_code = null, s.o_state_prov = null,
           s.d_address = null, s.d_city = null, s.d_country_code = null,
           s.d_county = null, s.d_facility_id = null, s.d_facility_number = null,
           s.d_postal_code = null, s.d_state_prov = null, s.baseline_cost = null,
           s.baseline_cost_currency_code = null, s.total_cost = null,
           s.linehaul_cost = null, s.num_stops = 0, s.num_docks = 0,
           s.direct_distance = null, s.distance = null,
           s.out_of_route_distance = null, s.estimated_cost = null,
           s.earned_income = null, s.assigned_carrier_id = null,
           s.assigned_service_level_id = null, s.assigned_mot_id = null,
           s.assigned_equipment_id = null, s.assigned_ship_via = null,
           s.last_updated_dttm = sysdate, s.last_updated_source = p_user_id,
           s.last_updated_source_type = 1
        where s.shipment_id = va_shpmt_id(i);
        wm_cs_log('Shipments cancelled ' || sql%rowcount);
    end if;
end;
/
show errors;

create or replace procedure wm_add_misc_to_shpmt
(
    p_user_id             in user_profile.user_id%type,
    p_tc_company_id       in shipment.tc_company_id%type,
    p_caller_id           in varchar2 default null
)
as
    v_use_locking           number(1) := 0;
    v_exec_pre_cleanup      varchar2(1);
    v_max_log_lvl           number(1) := 0;
    v_commit_freq           number(5) := 99999;
    v_code_id               sys_code.code_id%type := 'ADD';
    v_ref_value_1           msg_log.ref_value_1%type := 'Misc';
begin
    select /*+ result_cache */ to_number(coalesce(trim(substr(sc.misc_flags, 3, 5)), '99999')),
        case when substr(sc.misc_flags, 2, 1) = 'Y' then 1 else 0 end,
        to_number(coalesce(trim(substr(sc.misc_flags, 8, 1)), '0')),
        coalesce(substr(sc.misc_flags, 9, 1), 'N')
    into v_commit_freq, v_use_locking, v_max_log_lvl, v_exec_pre_cleanup
    from sys_code sc 
    where sc.rec_type = 'S' and sc.code_type = '520' and sc.code_id = v_code_id;
    v_commit_freq := case v_commit_freq when 0 then 99999 else v_commit_freq end;

    -- the app is expected to delete prior to populating the tmp tables
    delete from tmp_store_master;
    delete from tmp_log_parm;
    insert into tmp_log_parm
    (
        log_lvl, pgm_id, module, msg_id, ref_code_1, ref_value_1, user_id
    )
    values
    (
        v_max_log_lvl, 'wm_add_to_shpmt_wrapper', 'WAVE', '1094', v_code_id,
        v_ref_value_1, p_user_id
    );

    if (v_exec_pre_cleanup = 'Y')
    then
        wm_cs_log('Beginning cleanup');
        wm_pre_add_cleanup(p_mode => '_assign_misc_');
    end if;

    wm_cs_log('Beginning assignment of misc splits');
    
    wm_add_to_shpmt_wrapper(p_user_id, p_tc_company_id, v_use_locking, v_commit_freq, p_caller_id, 
        p_trk_prod_flag=>null, p_shipment_id => null, p_om_shipment_id => null, 
        p_static_route_id => null, p_lpn_id => null, p_order_id => null, p_order_split_id => null,
        p_cons_run_id => null, p_load_split_data => 0, p_mode => '_assign_misc_');

    wm_cs_log('Completed assignment of misc splits to shpmts');
end;
/
show errors;

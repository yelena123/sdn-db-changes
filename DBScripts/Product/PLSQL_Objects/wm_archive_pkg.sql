

CREATE OR REPLACE package wm_archive_pkg
as
    -- procedure called by the WM UI
    -- (p_purge_config_ids => 'purge_config_id1,..,purge_config_idN'
    c_msg_module            CONSTANT varchar2(10):= 'SYSCONTROL';  
    c_msg_id_inform         CONSTANT varchar2(9) := '9999'; 
    c_msg_id_error          CONSTANT varchar2(4) := '8888';
    g_whse_id               whse_master.whse_master_id%type;
    gv_whse                 whse_master.whse%type;
    gv_company              company.company_name%type;
    
    TYPE tIDs IS TABLE OF number(38) INDEX BY binary_integer;
    TYPE tvIDs IS TABLE OF varchar2(50) INDEX BY binary_integer;
    
    procedure ui_purge
    (
        p_purge_config_ids  in  varchar2,
        p_user_id           in  varchar2 default 'PURGE'
    );
    
    PROCEDURE create_arch_gtmp_table (
                                        p_archive_table_name IN VARCHAR2,
                                        p_table_type         IN VARCHAR2 default 'D',
                                        P_PURGE_CD              IN VARCHAR2
                                     );
    PROCEDURE get_forall_range (
                             pstart         in out integer
                            ,pstop          in out integer
                            ,pcommit_size   in pls_integer
                            ,parr_size      in integer
                            );
    
    procedure purge_table
        (
            p_table_name    in varchar2,
            p_column_name   in varchar2,
            p_arrids        in tids,
            p_commit_size   in pls_integer,
            p_start_time    in timestamp,
            p_archive_ind   in char,
            p_purge_cd      in varchar2,
            p_comp_id       in pls_integer,
            p_warehouse_id  in pls_integer,
            p_rowcount      out pls_integer,
            p_error_msg     out varchar2);
            
    procedure purge_table
        (
            p_table_name     in varchar2,
            p_column_name    in varchar2,
            p_varrids        in tvids,
            p_commit_size    in pls_integer,
            p_start_time     in timestamp,
            p_archive_ind    in char,
            p_purge_cd       in varchar2,
            p_comp_id        in pls_integer,
            p_warehouse_id   in pls_integer,
            p_rowcount       out pls_integer,
            p_error_msg      out varchar2);            

    procedure purge_table_where
    (
        p_table_name        in  varchar2,
        p_where_clause      in  varchar2,
        p_commit_size       in  pls_integer,
        p_start_time        in  timestamp,
        p_archive_ind       in  char,
        p_purge_cd          in  varchar2,
        p_comp_id           in  pls_integer,
        p_warehouse_id      in  pls_integer,
        p_rowcount          out pls_integer,
        p_error_msg         out varchar2
    );

    -- record for processing message log entries
    type rt_msglog is record
    (
         r_module               msg_log.module%type := 'SYSCONTROL'
        ,r_msg_id               msg_log.msg_id%type -- 8888 (failed) or 9999 (info)
        ,r_lang_id              message_display.to_lang%type := 'en'
        ,r_whse_id              whse_master.whse_master_id%type
        ,r_cd_master_id         msg_log.cd_master_id%type
        ,r_stat_cd              msg_log.ref_value_1%type
        ,r_locn_parm_id         msg_log.ref_value_5%type
        ,r_start_dttm           timestamp(6)
        ,r_end_dttm             timestamp(6)
        ,r_rows_archived        pls_integer
        ,r_rows_deleted         pls_integer
        ,r_table_name           msg_log.msg%type
        ,r_purge_code           purge_config.purge_code%type
        ,r_company_id           msg_log.cd_master_id%type
    );
    rec_msglog  rt_msglog;

    procedure log_msg
    (
        p_rec_msglog        in  rt_msglog
        ,p_whse             in  whse_master.whse%type default gv_whse
        ,p_company          in  company.company_name%type default gv_company
    ) ;
    
    PROCEDURE msg_log_insert (
                         p_msg_parms     in VARCHAR2
                         ,p_module       in msg_log.module%TYPE DEFAULT c_msg_module
                         ,p_msg_id       in msg_log.msg_id%TYPE DEFAULT c_msg_id_inform
                         ,p_cd_master_id in purge_config.cd_master_id%TYPE DEFAULT NULL
                         ,p_warehouse    in purge_config.whse_master_id%TYPE DEFAULT g_whse_id
                         );   
    FUNCTION get_col_list  (
                                p_table_name IN VARCHAR2,
                                p_alias      IN VARCHAR2 DEFAULT 'N'
                            ) RETURN CLOB;
    FUNCTION get_archive_table_mapping (p_table_name varchar2) RETURN varchar2; 
    --PROCEDURE ins_archive_long_table_mapping;    
end wm_archive_pkg;
/


create or replace procedure extended_wm_archive_pkg(p_company_id      IN purge_config.cd_master_id%type
                        ,p_whse_id          IN purge_config.whse_master_id%type
                        ,p_commit_size      IN purge_config.nbr_rows_to_commit%type
                        ,p_days             IN purge_config.nbr_days_old%type
                        ,p_purge_cd         IN purge_config.purge_code%type
                        ,p_archive_flag     IN purge_config.archive_flag%type
                        ,p_child_comp_flag  IN purge_config.include_child_company_flag%type
                        ,p_misc_parm_1      IN purge_config.misc_parm_1%type
                        ,p_misc_parm_2      IN purge_config.misc_parm_2%type
                        ,p_misc_parm_3      IN purge_config.misc_parm_3%type
                        ,p_delink_child     IN purge_config.delink_child%type
                        ,p_invalid_purgecode OUT varchar2
                        ,p_rows_purged      OUT pls_integer
                        ,p_error_msg        OUT varchar2)
IS
  dstart                  timestamp(6);
  v_error_msg             purge_hist.message%type default null;
  i_rc                    pls_integer default 0;
  i_rows_purged           pls_integer default 0;
  arrIDs                  wm_archive_pkg.tIDs;
  rec_msglog              wm_archive_pkg.rt_msglog;

begin
   -- sample code that can be replaced with custom purges to purge the data dynamically using purge_table , purge_table_where procedures
   /* case when p_purge_cd = '92' then  -- C_LPN  (MD08) custom purge
         dstart := systimestamp;
         i_rows_purged := 0;

         select LPN_ID
         bulk collect into arrIDs
         from C_LPN CL
         WHERE NOT EXISTS (SELECT 1 FROM ORDERS O WHERE O.TC_ORDER_ID = CL.TC_ORDER_ID);
         
         wm_archive_pkg.purge_table ( p_table_name  => 'C_LPN'
                     , p_column_name => 'LPN_ID'
                     , p_arrids      => arrIDs
                     , p_commit_size => p_commit_size
                     , p_start_time  => dstart
                     , p_archive_ind => p_archive_flag
                     , p_purge_cd    => p_purge_cd
                     , p_comp_id     => p_company_id
                     , p_warehouse_id=> p_whse_id
                     , p_rowcount    => i_rc
                     , p_error_msg   => v_error_msg);
         i_rows_purged := i_rows_purged + i_rc;

     when p_purge_cd = '93' then  -- C_MD05_LOCK   (MD05) custom purge
         dstart := systimestamp;
         i_rows_purged := 0;

         wm_archive_pkg.purge_table_where ( p_table_name  => 'C_MD05_LOCK'
                           , p_where_clause=> 'WHERE STAT_CODE = 5 and MOD_DATE_TIME <= (current_timestamp - ' ||p_days||')'
                           , p_commit_size => p_commit_size
                           , p_start_time  => dstart
                           , p_archive_ind => p_archive_flag
                           , p_purge_cd    => p_purge_cd
                           , p_comp_id     => p_company_id
                           , p_warehouse_id=> p_whse_id
                           , p_rowcount    => i_rc
                           , p_error_msg   => v_error_msg);
         i_rows_purged := i_rows_purged + i_rc;
         --if v_error_msg is not null then raise e_StopPurge; end if;
     else
         p_invalid_purgecode := 'Y';   
     end case; */
     
     p_rows_purged := i_rows_purged;
     p_error_msg   := v_error_msg;    
EXCEPTION    
WHEN OTHERS 
THEN
    wm_archive_pkg.msg_log_insert(p_msg_parms => 'extended_wm_archive_pkg(): Exception! -> ' || substr(sqlerrm,1,200));    
     p_rows_purged := i_rows_purged;
     p_error_msg   := substr(sqlerrm,1,200);
end;
/

create or replace package body wm_archive_pkg
as

    c_pkg_name              CONSTANT varchar2(35) := 'wm_archive_pkg';
    c_debug_yn              CONSTANT char(1) := 'n';
    c_debug_logging_yn      CONSTANT char(1) := 'y';  -- write to arch_debug_log table
    gv_user_id              msg_log.user_id%TYPE;
    gv_purge_desc           sys_code.code_desc%TYPE default '';

    v_msg                   varchar2(500);
    
    -- DEFINE ARCHIVE INDICATORS ----------------------------------
    -- Note - Values for ALL and NONE correspond to the value in column
    --        PURGE_CONFIG.ARCHIVE_FLAG (i.e. Y, N)

    -- NORMAL:  archive transaction tables, don't archive transient
    c_arch_norm             CONSTANT char(1) := 'D';
    -- ALL:  archive all tables
    c_arch_all              CONSTANT char(1) := 'Y';
    -- NONE: archive no tables
    c_arch_none             CONSTANT char(1) := 'N';

    -- DEFINE MESSAGING CONSTANTS
    c_created_source        CONSTANT varchar2(12):= 'ErrorMessage';
    c_msg_lang              CONSTANT varchar2(2) := 'en';
    c_purge_status_complete CONSTANT sys_code.code_desc%type := 'Completed';
    c_purge_status_inproc   CONSTANT sys_code.code_desc%type := 'In process';
    c_purge_status_failed   CONSTANT sys_code.code_desc%type := 'Failed';
    c_purge_status_except   CONSTANT sys_code.code_desc%type := 'Exception';
    c_shipment_purge_status CONSTANT company_parameter.param_def_id%type := 'shipment_purge_status';
    c_o_lpn_stat_code       CONSTANT whse_parameters.purge_case_beyond_stat_code%type := 90;


    e_arch_purge_failed     exception;

    -- MACR00848065 modified to number(38) from pls_integer

    arrIDs      tIDs;
    arrIDs_1    tIDs;
    arrIDs_2    tIDs;
    arrIDs_3    tIDs;
    arrIDs_4    tIDs;
    arrIDs_5    tIDs;
    arrIDs_6    tIDs;


    varrIDs      tvIDs;

    TYPE Hist_Array IS VARRAY(20) OF VARCHAR2(50);

    -- CURRENT WAREHOUSE NAME BEING PROCESSED (SEE EXEC_PURGE)
    gv_success_result            msg_log.msg%TYPE;
    gv_error_result              msg_log.msg%TYPE;
	gv_count					 number(10);
    gv_DbLinkSynCnt              pls_integer;
    gvTempTableforBulkProc       varchar2(30);

    gi_bulk_except_ind           pls_integer;
    gi_purge_exception_count     pls_integer := 0;
	gv_commit_freq               purge_config.nbr_rows_to_commit%TYPE := 100000;

    /* BEGIN LOCAL PROCEDURES ************************************************/
    ----------------------------------------------------------------------------------------
    -- PROCEDURE Msg_Log_Insert
    ----------------------------------------------------------------------------------------
    PROCEDURE msg_log_insert (
                         p_msg_parms     VARCHAR2
                         ,p_module       msg_log.module%TYPE DEFAULT c_msg_module
                         ,p_msg_id       msg_log.msg_id%TYPE DEFAULT c_msg_id_inform
                         ,p_cd_master_id purge_config.cd_master_id%TYPE DEFAULT NULL
                         ,p_warehouse    purge_config.whse_master_id%TYPE DEFAULT g_whse_id
                         )
    IS
        PRAGMA AUTONOMOUS_TRANSACTION;
        v_msg           msg_log.msg%TYPE;
        v_list          varchar2(500);
        v_name          varchar2(5);
        v_value         varchar2(150);        

    BEGIN
        if ( instr(p_msg_parms,'Exception!')=0 )  and  (  instr(p_msg_parms,'UI_PURGE()')=0 )  then
            -- do the parameter substitution
           if p_msg_id = '8888'
           then
              v_msg := gv_error_result;
           elsif p_msg_id = '9999'
           then
              v_msg := gv_success_result;
           else 
              v_msg := null;
           end if;            
        
           v_list := p_msg_parms;
           -- substitute placeholders in message string with values
           WHILE v_list is not null LOOP
               v_name := substr(v_list, 1, instr(v_list,'^')-1);
               if instr(v_list,'|') > 0 then
                   v_value := substr(v_list, instr(v_list,'^')+1, (instr(v_list,'|')-instr(v_list,'^')-1));
               else
                   v_value := substr(v_list, instr(v_list,'^')+1);
               end if;
               -- replace the parameter name with the value
               v_msg := replace(v_msg, v_name, v_value);
               -- remove the processed name/value pair from the list
               -- nvl function added 3/8/12 jaf
               v_list := substr(v_list, length(v_name)+nvl(length(v_value),0)+3);
           END LOOP;

        else
            -- log the exception handler error message
            v_msg := p_msg_parms;
        end if;
        
        -- note: whse is set globally in exec_purge
        INSERT INTO msg_log
                  (msg_log_id, module, msg_id, pgm_id,SUB_PGM_NAME,
                   msg, create_date_time, user_id, whse,
                   cd_master_id,class, log_date_time, mod_date_time
                  )
            VALUES (msg_log_id_seq.nextval, p_module, p_msg_id, c_pkg_name,gv_purge_desc||' Purge',
                   v_msg, SYSDATE, gv_user_id, gv_whse,
                   p_cd_master_id,'SYSTEM', SYSDATE, SYSDATE
                  );
        commit;

    EXCEPTION
        WHEN OTHERS THEN
            raise_application_error
                      (-20121, 'MSG_LOG_INSERT():  Failed to log exception information into MSG_LOG '
                       || CHR (10) || SQLERRM);
    END;

    ----------------------------------------------------------------------
    -- called by all archive/purge procedures for messaging
    ----------------------------------------------------------------------
    PROCEDURE log_msg (p_rec_MsgLog   IN rt_MsgLog        
                       ,p_whse        in  whse_master.whse%type default gv_whse
                       ,p_company     in  company.company_name%type default gv_company )
    IS 
        v_parm_list     varchar2(500);
        vmsg            varchar2(1500);

    BEGIN
        -- set up the parameter list for substitution
        v_parm_list := '{0}^' || upper(p_rec_MsgLog.r_table_name) ||
                        '|{1}^' || p_rec_MsgLog.r_purge_code ||
                        '|{2}^' || p_company ||            -- MACR00557025
                        '|{3}^' || p_whse ||               -- MACR00557025
                        '|{4}^' || p_rec_MsgLog.r_rows_deleted ||
                        '|{5}^' || p_rec_MsgLog.r_rows_archived;
        -- get the message
        msg_log_insert (
                       p_msg_parms    => v_parm_list
                       ,p_module       => p_rec_MsgLog.r_module
                       ,p_msg_id       => p_rec_MsgLog.r_msg_id
                       ,p_cd_master_id => p_rec_MsgLog.r_cd_master_id
                       ,p_warehouse    => p_rec_MsgLog.r_whse_id
                       );

    EXCEPTION
        when others then
             raise_application_error
                    (-20121, 'LOG_MSG():  Failed to log exception information into MSG_LOG '
                    || CHR (10) || SQLERRM);
    END;

    -- MACR00440675
    procedure set_taskhdr_origtaskid_to_null (p_arrIDs in tIDs)
    is
    begin
        -- set other orig_task_ids to null for tasks being purged
        forall i in 1..p_arrIDs.count
            update task_hdr o
            set orig_task_id = null
            where exists (
                        select 1
                        from task_hdr i
                        where task_hdr_id = p_arrIDs(i)
                        and i.task_id = o.orig_task_id
                        );
    exception when others then
        v_msg := substr(' set_taskhdr_origtaskid_to_null() -> Exception! -> '  || sqlerrm,1,500) ; 
        msg_log_insert(p_msg_parms => v_msg);
    end;
    
    -- WM-39034 modified to purge to move the data from _HISTORY tables if the history tables contain any data.
    procedure is_table_history_configured(p_hist_array  Hist_Array, p_is_config out varchar2)
    is
      v_cnt            pls_integer := 0;
      l_hist_cnt       pls_integer := 0;
      v_sql            varchar2(4000);
      table_not_found  EXCEPTION;
      PRAGMA EXCEPTION_INIT(table_not_found, -942);
    begin
       for i IN p_hist_array.first .. p_hist_array.last 
       loop
          v_sql := 'select count(*) from ' || p_hist_array(i) || ' where rownum = 1';
          execute immediate v_sql into v_cnt;
          l_hist_cnt := l_hist_cnt + v_cnt ;
       end loop;
       if l_hist_cnt > 0
       then
          p_is_config := 'Y';
       else
          p_is_config := 'N';
       end if;
    exception
    when table_not_found
    then
       p_is_config := 'N';
    when others
    then
        v_msg := substr('is_table_history_configured() -> Exception! -> '  || sqlerrm,1,500) ; 
        msg_log_insert(p_msg_parms => v_msg);    
    END;
    ----------------------------------------------------------------------
    -- Determine start/stop indexes for the next commit range
    ----------------------------------------------------------------------
    PROCEDURE get_forall_range (
                             pstart         in out integer
                            ,pstop          in out integer
                            ,pcommit_size   in pls_integer
                            ,parr_size      in integer
                            )
    IS

    BEGIN
        pstart := pstop + 1;
        if (parr_size < (pstop + pcommit_size)) or
           (pcommit_size = -1) then
            -- commit size is less than array size or
            -- we're told to skip incremental commits (pcommit_size=-1)
            pstop := parr_size;
        else
            pstop := pstop + pcommit_size;
        end if;
    END;

    --------------------------------------------------------------------------
    -- update the status in purge_config for the purge code being purged
    --------------------------------------------------------------------------
    PROCEDURE update_purge_config_status (
                                     p_id       purge_config.purge_config_id%type
                                    ,p_status   purge_config.purge_status%type
                                    )
    IS
        pragma autonomous_transaction;

    BEGIN
        update purge_config
        set purge_status = (
                           select code_id
                           from sys_code
                           where rec_type = 'S'
                           and code_type = '960'
                           and lower(code_desc) = lower(p_status)
                           )
            ,mod_date_time      = sysdate
			,nbr_rows_to_commit = gv_commit_freq
            ,date_last_purged   = decode(p_status, c_purge_status_complete, sysdate, date_last_purged)
        where purge_config_id   = p_id;
        commit;

    EXCEPTION
        WHEN OTHERS THEN
             v_msg := substr('update_config_status -> Exception! => '  || sqlerrm,1,500) ; 
             msg_log_insert(p_msg_parms => v_msg);
            rollback;
    END;

    --------------------------------------------------------------------------
    -- write the info for the purge_config_id just purged into purge_hist
    --------------------------------------------------------------------------
    PROCEDURE insert_purge_hist (p_purge_config_rec IN purge_config%rowtype
                            ,p_purge_start_dttm     IN timestamp
                            ,p_purge_end_dttm       IN timestamp
                            ,p_source               IN purge_hist.source%type
                            ,p_status               IN purge_hist.purge_status%type
                            ,p_message              IN purge_hist.message%type
                            ,p_nbr_rows_purged      IN purge_hist.nbr_rows_purged%type
                            )
    IS
        pragma autonomous_transaction;

    BEGIN
        msg_log_insert(p_msg_parms => '--> ' || 'UI_PURGE()' || ' p_rows_purged => ' || p_nbr_rows_purged, p_cd_master_id => p_purge_config_rec.CD_MASTER_ID ); 

        insert into purge_hist (
                            PURGE_HIST_ID
                            ,USER_ID
                            ,PURGE_CODE
                            ,CD_MASTER_ID
                            ,WHSE_MASTER_ID
                            ,NBR_DAYS_OLD
                            ,NBR_ROWS_TO_COMMIT
                            ,ARCHIVE_FLAG
                            ,PURGE_START_DTTM
                            ,PURGE_END_DTTM
                            ,PURGE_STATUS
                            ,NBR_ROWS_PURGED
                            ,SOURCE
                            ,MESSAGE
                            )
        values (
                --p_purge_config_rec.PURGE_HIST_ID
                 seq_purge_hist_id.nextval
                ,gv_user_id
                ,p_purge_config_rec.PURGE_CODE
                ,p_purge_config_rec.CD_MASTER_ID
                ,p_purge_config_rec.WHSE_MASTER_ID
                ,p_purge_config_rec.NBR_DAYS_OLD
                ,p_purge_config_rec.NBR_ROWS_TO_COMMIT
                ,p_purge_config_rec.ARCHIVE_FLAG
                ,p_purge_start_dttm
                ,p_purge_end_dttm
                ,decode(gi_purge_exception_count, 0, p_status, c_purge_status_except)
                ,p_nbr_rows_purged
                ,p_source
                ,decode(gi_purge_exception_count, 0, p_message, 'See table MSG_LOG.MSG like ''ORA-%'' for details.')
                );       
        commit;
           

    EXCEPTION
        WHEN OTHERS THEN
             v_msg := substr('insert_purge_hist -> Exception! '   || sqlerrm,1,500) ; 
             msg_log_insert(p_msg_parms => v_msg);
            rollback;
    END;
    
    --------------------------------------------------------------------------
    -- Create GTMP table to do the insert "for all"
    --------------------------------------------------------------------------
    PROCEDURE create_arch_gtmp_table (
                                        p_archive_table_name IN VARCHAR2,
                                        p_table_type         IN VARCHAR2 default 'D',
                                        P_PURGE_CD 			 IN VARCHAR2

                                     )
    IS
        i_cnt pls_integer;
    begin
        select count(*) into i_cnt
        from user_tables
        where table_name = 'GTMP_ARCHIVE_PURGE_'||P_PURGE_CD;
        if i_cnt = 1 then
            execute immediate 'DROP TABLE GTMP_ARCHIVE_PURGE_'||P_PURGE_CD;
        end if;

        begin
            IF p_table_type = 'D'
            then
				execute immediate 'CREATE GLOBAL TEMPORARY TABLE GTMP_ARCHIVE_PURGE_'||P_PURGE_CD||' ON COMMIT DELETE ROWS AS SELECT * FROM '||p_archive_table_name||' WHERE 1=2';
            else
				execute immediate 'CREATE GLOBAL TEMPORARY TABLE GTMP_ARCHIVE_PURGE_'||P_PURGE_CD||' ON COMMIT PRESERVE ROWS AS SELECT * FROM '||p_archive_table_name||' WHERE 1=2';
            end if;   
        exception
            when others then
            v_msg := substr('create_arch_gtmp_table > Exception! => ' || sqlerrm,1,500) ; 
            msg_log_insert(p_msg_parms => v_msg);
            raise e_arch_purge_failed;
        end;
    END;

    --------------------------------------------------------------------------
    -- Generate dynamic SQL for getting col lists
    --------------------------------------------------------------------------
    FUNCTION get_col_list  (
                                p_table_name IN VARCHAR2,
                                p_alias      IN VARCHAR2 DEFAULT 'N'
                            )
    RETURN CLOB
    IS
        v_col_list CLOB;
        v_tab_exists NUMBER:=0;
        v_owner VARCHAR2(30);
        CURSOR cur_get_col_list (p_owner_name IN VARCHAR2)
        IS
            select column_name,column_id from all_tab_columns
            where table_name = upper(p_table_name)
            and owner = p_owner_name order by column_id;

    BEGIN

        select count(*) into v_tab_exists from user_tables where table_name = upper(p_table_name);

        if ( v_tab_exists != 0 )
        then
            select username into v_owner from user_users ;
        else
            select table_owner into v_owner from user_synonyms where table_name = upper(p_table_name) and synonym_name not like 'ARCH_%';
        end if;

        FOR currec in cur_get_col_list(v_owner)
        LOOP
            if ( currec.column_id = 1 )
            then
                if p_alias = 'Y'
                then
                   v_col_list := 't.'|| currec.column_name;
                else
                   v_col_list := currec.column_name;
                end if;
            else
                if p_alias = 'Y'
                then
                   v_col_list := v_col_list ||',t.'|| currec.column_name;
                else
                   v_col_list := v_col_list ||','|| currec.column_name;
                end if;   
            end if;
        END LOOP;
        RETURN v_col_list;
    EXCEPTION
        WHEN OTHERS THEN
             v_msg := substr('get_col_list > Exception! => '   || sqlerrm,1,500) ; 
             msg_log_insert(p_msg_parms => v_msg);
             raise e_arch_purge_failed;
    END;

    -- WM-39034 adding stand-alone purges(CL_MESSAGE,om_sched_event,LRF_SCHED_EVENT_RPT) to base archive package
    /*PROCEDURE ins_archive_long_table_mapping
    is
       v_sql_statement   VARCHAR2 (10000);
       v_cnt             NUMBER (1) := 0;
       cursor c1 IS
       select 'E_PLAN_TRANSACTION_ACT_ZONE' TABLE_NAME ,'ARCH_E_PLAN_TRANSACTION_ACT_ZO' ARCHIVE_TABLE_NAME  from dual  union
       select 'SMARTLABEL_CARTON_HDR_HIST' TABLE_NAME ,'ARCH_SMARTLABEL_CRTN_HDR_HIST'         ARCHIVE_TABLE_NAME  from dual  union
       select 'QUESTION_ANS_FILTER_HISTORY' TABLE_NAME ,'ARCH_QUESTION_ANS_FILTER_HIST' ARCHIVE_TABLE_NAME  from dual  union
       select 'PURCHASE_ORDERS_LINE_ITEM_SIZE' TABLE_NAME ,'ARCH_PURCHASE_ORDERS_LN_ITM_SZ' ARCHIVE_TABLE_NAME  from dual  union
       select 'PO_LINE_ITEM_ATTR_TEMPLATE' TABLE_NAME ,'ARCH_PO_LINE_ITEM_ATTR_TMPLT' ARCHIVE_TABLE_NAME  from dual  union
       select 'OUTPT_ORDER_LINE_ITEM_SIZE' TABLE_NAME ,'ARCH_OUTPT_ORDER_LINE_ITEM_SZ' ARCHIVE_TABLE_NAME  from dual  union
       select 'OUTPT_LPN_MINOR_SERIAL_NBR' TABLE_NAME ,'ARCH_OUTPT_LPN_MINOR_SRL_NBR' ARCHIVE_TABLE_NAME  from dual  union
       select 'ILM_APPOINTMENTS_CHARGEBACK' TABLE_NAME ,'ARCH_ILM_APPTS_CHARGEBACK' ARCHIVE_TABLE_NAME  from dual  union
       select 'SHIPMENT_ACCESSORIAL_TEMPLATE' TABLE_NAME ,'ARCH_SHPMNT_ACCSSRL_TEMPLATE' ARCHIVE_TABLE_NAME  from dual  union
       select 'SHIPMENT_ATTRIBUTE_TEMPLATE' TABLE_NAME ,'ARCH_SHPMNT_ATTR_TEMPLATE' ARCHIVE_TABLE_NAME  from dual  union
       select 'SHIPMENT_COMMODITY_TEMPLATE' TABLE_NAME ,'ARCH_SHPMNT_COMMDTY_TEMPLATE' ARCHIVE_TABLE_NAME  from dual  union
       select 'FACILITY_SHIPPING_CALENDAR' TABLE_NAME , 'ARCH_FACILITY_SHP_CAL' ARCHIVE_TABLE_NAME  from dual  union
       select 'SLOT_TOTAL_CAP_USAGE_DETAIL'  TABLE_NAME , 'ARCH_SLOT_TOTAL_CAP_USAGE_DTL' ARCHIVE_TABLE_NAME  from dual  union
       select 'ORDER_ACCESSORIAL_OPTION_GRP'  TABLE_NAME ,'ARCH_ORDER_ACCESSORIAL_OPT_GRP' ARCHIVE_TABLE_NAME  from dual  union
       select 'DO_LINE_CLASSIFICATION_CODE' TABLE_NAME ,'ARCH_DO_LINE_CLSIFICATION_CODE' ARCHIVE_TABLE_NAME  from dual union
       select 'PO_LINE_CLASSIFICATION_CODE' TABLE_NAME ,'ARCH_PO_LINE_CLSIFICATION_CODE' ARCHIVE_TABLE_NAME  from dual  union  
       select 'E_AVG_HRLY_LABOR_COST_RATE' TABLE_NAME ,'ARCH_E_AVG_HRLY_LABOR_COST_RTE' ARCHIVE_TABLE_NAME  from dual  union 
       select 'LRF_REPORT_PARAM_VALUE_DTL' TABLE_NAME ,'ARCH_LRF_REPORT_PARM_VALUE_DTL' ARCHIVE_TABLE_NAME  from dual  union          
       select 'CL_ENDPOINT_QUEUE_HISTORY' TABLE_NAME ,'ARCH_CL_ENDPOINT_QUEUE' ARCHIVE_TABLE_NAME  from dual  union
       select 'CL_MESSAGE_HISTORY' TABLE_NAME ,'ARCH_CL_MESSAGE' ARCHIVE_TABLE_NAME  from dual  union
       select 'CL_MESSAGE_KEYS_HISTORY'  TABLE_NAME ,'ARCH_CL_MESSAGE_KEYS' ARCHIVE_TABLE_NAME  from dual  union
       select 'LRF_EVENT_HISTORY' TABLE_NAME ,'ARCH_LRF_EVENT' ARCHIVE_TABLE_NAME  from dual  union
       select 'LRF_REPORT_AUDIT_HISTORY' TABLE_NAME ,'ARCH_LRF_REPORT_AUDIT' ARCHIVE_TABLE_NAME  from dual  union
       select 'LRF_REPORT_HISTORY' TABLE_NAME ,'ARCH_LRF_REPORT' ARCHIVE_TABLE_NAME  from dual  union
       select 'LRF_REPORT_PARAM_VALUE_HISTORY' TABLE_NAME ,'ARCH_LRF_REPORT_PARAM_VALUE' ARCHIVE_TABLE_NAME  from dual  union
       select 'LRF_RPT_PARM_VALUE_DTL_HISTORY' TABLE_NAME ,'ARCH_LRF_REPORT_PARM_VALUE_DTL' ARCHIVE_TABLE_NAME  from dual  union
       select 'LRF_SCHED_EVENT_REPORT_HISTORY' TABLE_NAME ,'ARCH_LRF_SCHED_EVENT_REPORT' ARCHIVE_TABLE_NAME  from dual  union
       select 'LRF_REP_SCHD_PARAM_HISTORY' TABLE_NAME ,'ARCH_LRF_REP_SCHD_PARAM' ARCHIVE_TABLE_NAME  from dual  union
       select 'LRF_REP_SCHD_PARAM_DTL_HISTORY' TABLE_NAME ,'ARCH_LRF_REP_SCHD_PARAM_DTL' ARCHIVE_TABLE_NAME  from dual  union
       select 'OM_SCHED_EVENT_HISTORY' TABLE_NAME ,'ARCH_OM_SCHED_EVENT' ARCHIVE_TABLE_NAME  from dual  union
       select 'SO_ORDER_CHG_VALUE_HISTORY' TABLE_NAME ,'ARCH_SO_ORDER_CHG_VALUE' ARCHIVE_TABLE_NAME  from dual  union
       select 'CONSTEMP_SCHED_EVENT_HISTORY' TABLE_NAME ,'ARCH_CONSTEMP_SCHED_EVENT' ARCHIVE_TABLE_NAME  from dual  union
       select 'EVENT_MESSAGE_HISTORY' TABLE_NAME ,'ARCH_EVENT_MESSAGE' ARCHIVE_TABLE_NAME  from dual  union
       select 'RECV_MSG_DATA_HISTORY' TABLE_NAME ,'ARCH_RECV_MSG_DATA' ARCHIVE_TABLE_NAME  from dual;    
    BEGIN
       SELECT COUNT (*)
         INTO v_cnt
         FROM user_cons_columns
        WHERE constraint_name = 'ARCHIVE_LONG_TABLE_MAPPING_PK'
          AND column_name IN ('ARCHIVE_TABLE_NAME', 'TABLE_NAME');
       IF (v_cnt <> 2)
       THEN
          begin
             EXECUTE IMMEDIATE
                'alter table archive_long_table_mapping drop constraint archive_long_table_mapping_pk CASCADE';
          EXCEPTION
          WHEN OTHERS
          THEN
          NULL;
          end;          
          EXECUTE IMMEDIATE
                'alter table archive_long_table_mapping add constraint archive_long_table_mapping_pk primary key (archive_table_name,table_name)';
       END IF;
       FOR I IN C1
       LOOP
          v_sql_statement :=
                'merge into archive_long_table_mapping alt'
             || ' using (select ''' ||I.TABLE_NAME || ''' table_name, ''' ||  I.ARCHIVE_TABLE_NAME || ''' archive_table_name from dual) d '
             || ' on (alt.table_name = d.table_name and alt.archive_table_name = d.archive_table_name) '
             || ' when not matched then '
             || ' insert (alt.table_name, alt.archive_table_name) values ('''
             || I.TABLE_NAME ||''','''||I.ARCHIVE_TABLE_NAME ||''')';

          EXECUTE IMMEDIATE v_sql_statement;
       END LOOP;
       commit;
    EXCEPTION
    WHEN OTHERS
       THEN
          v_msg := substr('ins_archive_long_table_mapping() -> Exception! -> '  || sqlerrm,1,500) ; 
          msg_log_insert(p_msg_parms => v_msg);          
          raise e_arch_purge_failed;          
    END; */
 
    FUNCTION get_archive_table_mapping (p_table_name varchar2)
      RETURN varchar2
    IS
        lv_tab_name varchar2(30) default '';
    BEGIN

        -- check table name length -------------------------------   
            begin
              /*select archive_table_name
                into lv_tab_name
              from archive_long_table_mapping
              where upper(table_name) = upper(p_table_name); */
              
              select DISTINCT archive_table_name
              into lv_tab_name
              from MANH_PRODUCT_PURGING_TABLES
              where upper(table_name) = upper(p_table_name);
              
            exception
       when no_data_found 
       then
          lv_tab_name := 'ARCH_'||p_table_name;
       when others then
          null;
       end;

        -- confirm that the archive table exists
        begin
            select synonym_name into lv_tab_name
            from user_synonyms
            where upper(synonym_name)=upper(lv_tab_name);
            return lv_tab_name;
        exception
            when others then
                return 'Not Found';
        end;
    END;
    
    --DB-701
    PROCEDURE archive_purge_log_dpndt_data ( p_archive_ind     IN purge_config.archive_flag%type )
    IS
       PRAGMA AUTONOMOUS_TRANSACTION;
       v_data_exist          varchar2(1);
    BEGIN
       IF p_archive_ind in (c_arch_norm, c_arch_all)   
       THEN
          EXECUTE IMMEDIATE  'INSERT INTO arch_purge_log_dpndt_data ('
                            ||get_col_list('purge_log_dpndt_data')
                            ||') SELECT '
                            ||get_col_list('purge_log_dpndt_data')
                            ||' FROM purge_log_dpndt_data' ; 
                                                 
          DELETE FROM purge_log_dpndt_data;      
       ELSE 
          INSERT INTO purge_log_dpndt_data_hist
          SELECT * FROM purge_log_dpndt_data;
       
          DELETE FROM purge_log_dpndt_data;        
       END IF;
      
       select decode(count(*),0,'N','Y')
         into v_data_exist
         from purge_log_dpndt_data_hist
        where run_time <= current_timestamp - 10;

        if v_data_exist ='Y' 
        THEN
           delete from purge_log_dpndt_data_hist where run_time <= current_timestamp - 10;
        END IF;
        
        COMMIT;

    EXCEPTION
        WHEN OTHERS THEN
          v_msg := substr('archive_purge_log_dpndt_data -> Exception! =>  '  || sqlerrm,1,500) ; 
          msg_log_insert(p_msg_parms => v_msg);          
          raise e_arch_purge_failed; 
    END;    
    --------------------------------------------------------------------------
    --  Purge records based on dynamic inputs
    --------------------------------------------------------------------------
    PROCEDURE purge_table
        (
            p_table_name    IN VARCHAR2,
            p_column_name   IN VARCHAR2,
            P_ARRIDS        IN TIDS,
            P_COMMIT_SIZE   IN PLS_INTEGER,
            P_START_TIME    IN TIMESTAMP,
            P_ARCHIVE_IND   IN CHAR,
            P_PURGE_CD      IN VARCHAR2,
            P_COMP_ID       IN PLS_INTEGER,
            P_WAREHOUSE_ID  IN PLS_INTEGER,
            P_ROWCOUNT      OUT PLS_INTEGER,
            P_ERROR_MSG     OUT VARCHAR2)
    IS
        istart                      pls_integer default 0;
        istop                       pls_integer default 0;
        i_arc_rc                    pls_integer default 0;
        i_del_rc                    pls_integer default 0;
        v_msg                       varchar2(250);
        v_column_list               clob;
        v_archive_table_name        varchar2(30);
        e_ArchTableMappingNotFound  exception;
        i_err_count                 pls_integer;

    BEGIN
        rec_MsgLog.r_start_dttm     := p_start_time;
        rec_MsgLog.r_cd_master_id   := p_comp_id;
        rec_MsgLog.r_purge_code     := p_purge_cd;
        rec_MsgLog.r_whse_id        := p_warehouse_id;
        rec_MsgLog.r_table_name     := p_table_name;

        if p_archive_ind in (c_arch_norm, c_arch_all) and lower(c_debug_yn)='n' then  
           -- check table name length -------------------------------
           v_archive_table_name:=get_archive_table_mapping(p_table_name);
           if v_archive_table_name='Not Found' then
               raise e_ArchTableMappingNotFound;
           end if;
       
           if gv_DbLinkSynCnt > 0 then
               create_arch_gtmp_table (v_archive_table_name,'D',p_purge_cd);
              gvTempTableforBulkProc := 'GTMP_ARCHIVE_PURGE_'||p_purge_cd;
           else
              gvTempTableforBulkProc := v_archive_table_name;
           end if;
           
        end if;

        while istop < p_arrIDs.count loop

            get_forall_range (istart, istop, p_commit_size, p_arrIDs.count);

            if c_debug_yn='n' then
                if p_archive_ind in (c_arch_norm, c_arch_all) then
                
                    forall i in istart..istop
                    EXECUTE IMMEDIATE
                        'INSERT INTO '||gvTempTableforBulkProc||' ('
                        ||get_col_list(p_table_name)
                        ||') SELECT '
                        ||get_col_list(p_table_name)
                        ||' FROM '
                        ||p_table_name ||' t '
                        ||' WHERE '
                        ||p_column_name
                        ||' = :B1'
                        USING p_arrIDs(i);

                       if gv_DbLinkSynCnt > 0
                       then
                    -- Bulk insert into archive using the gtmp table
					EXECUTE IMMEDIATE 'INSERT INTO '||v_archive_table_name||' SELECT * FROM GTMP_ARCHIVE_PURGE_'||p_purge_cd;
                       end if;
                    -- increment archive count
                    i_arc_rc := i_arc_rc + sql%rowcount;
                end if; -- archive_ind
            end if;
            
		  if p_table_name in ('ITEM_CBO','PURCHASE_ORDERS','ORDERS','LPN','SHIPMENT','ASN','TRAN_LOG') then  	
            begin
            forall i in istart..istop 
               EXECUTE IMMEDIATE 'insert into SOLR_ARCHIVE_INFO values (:1,:2,:3)' using p_table_name, p_column_name ,p_arrIDs(i)  ;
            exception 
            when others then
                         
                v_msg := substr('purge_table('''||p_table_name||'''), purge code=('|| p_purge_cd||'): Exception while inserting to SOLR_ARCHIVE_INFO table ! -> '|| sqlerrm,1,500) ; 
            
                 msg_log_insert (p_msg_parms => v_msg
                                 ,p_cd_master_id => p_comp_id
                                 ,p_warehouse => p_warehouse_id);            
            end;            
          end if;
            -- MACR00773167
            -- CAPTURE BULK ERRORS, ALLOWING ALL REMAINING TO PROCESS
            begin
                -- delete the same
                forall i in istart..istop save exceptions
                    EXECUTE IMMEDIATE 'DELETE FROM '||p_table_name||' WHERE '||p_column_name||' = :B2' USING p_arrIDs(i);

                -- increment delete count
                i_del_rc := i_del_rc + sql%rowcount;

            exception when others then
                -- increment delete count
                i_del_rc := i_del_rc + sql%rowcount;

                -- CAPTURE AND REPORT
                i_err_count:=SQL%BULK_EXCEPTIONS.count;
                
                for i in 1..i_err_count loop
                
                    gi_bulk_except_ind := (istart + SQL%BULK_EXCEPTIONS(i).error_index) - 1;
                    
				    -- back out the failures from solr table       
                  if p_table_name in ('ITEM_CBO','PURCHASE_ORDERS','ORDERS','LPN','SHIPMENT','ASN') then 					
                    EXECUTE IMMEDIATE ' delete from SOLR_ARCHIVE_INFO where table_name =  :1 and pk_column = :2 and archived_pk_value = :3 ' 
                                            using p_table_name,p_column_name,p_arrIDs(gi_bulk_except_ind) ;
				  end if;	

                    -- log the error detail  
                    v_msg := sqlerrm(-SQL%BULK_EXCEPTIONS(i).error_code) 
                             ||' Exception! for table '||  p_table_name || '.'||p_column_name|| ' = ' || p_arrIDs(gi_bulk_except_ind);    
                             
                                 
                    msg_log_insert (p_msg_parms => substr(v_msg,1,500)
                           ,p_cd_master_id => p_comp_id
                           ,p_warehouse => p_warehouse_id);                              

                    gi_purge_exception_count := gi_purge_exception_count + 1;
                 
                end loop;
                
                if p_archive_ind in (c_arch_norm, c_arch_all) then  
                   -- #MACR00858713
                   EXECUTE IMMEDIATE 'delete from '|| v_archive_table_name ||' where ' || p_column_name || ' in (select ' || p_column_name || ' from ' || p_table_name || ')' ;
                   
                   -- decrement archive count
                   i_arc_rc := i_arc_rc - sql%rowcount; 
                end if;
            end;

            if  p_archive_ind in (c_arch_norm, c_arch_all) and gv_DbLinkSynCnt > 0 then -- CR MACR00866380 added p_archive_ind check
                EXECUTE IMMEDIATE 'truncate table GTMP_ARCHIVE_PURGE_'||p_purge_cd;
            end if;
            
            if lower(c_debug_yn)='n' then 
                commit;
            end if;

        end loop;

        -- set new values and log the message
        rec_MsgLog.r_msg_id         := c_msg_id_inform;
        rec_MsgLog.r_rows_archived  := i_arc_rc;
        rec_MsgLog.r_rows_deleted   := i_del_rc;
        rec_MsgLog.r_end_dttm       := systimestamp;
        log_msg (p_rec_MsgLog       => rec_MsgLog);
        -- return the number of deleted rows
        p_rowcount := i_del_rc;

    EXCEPTION
        WHEN E_ARCHTABLEMAPPINGNOTFOUND THEN
            msg_log_insert (p_msg_parms => 'purge_table('''||p_table_name||'''), purge code=('|| p_purge_cd||
                           '): Exception! -> Archive Table Not Found for '||p_table_name
                           ,p_msg_id => c_msg_id_error
                           ,p_cd_master_id => p_comp_id
                           ,p_warehouse => p_warehouse_id);
                           
            -- set new values and log the message
            rec_MsgLog.r_msg_id := c_msg_id_error;
            rec_MsgLog.r_rows_archived := i_arc_rc;
            rec_MsgLog.r_rows_deleted := i_del_rc;
            rec_MsgLog.r_end_dttm := systimestamp;
            log_msg (p_rec_MsgLog => rec_MsgLog);                           

            raise e_arch_purge_failed;
    
        WHEN OTHERS THEN
            msg_log_insert (p_msg_parms => 'purge_table('''||p_table_name||'''), purge code=('|| p_purge_cd||
                           '): Exception! -> '||substr(sqlerrm,1,200)
                           ,p_msg_id => c_msg_id_error
                           ,p_cd_master_id => p_comp_id
                           ,p_warehouse => p_warehouse_id);
                           
            -- set new values and log the message
            rec_MsgLog.r_msg_id := c_msg_id_error;
            rec_MsgLog.r_rows_archived := i_arc_rc;
            rec_MsgLog.r_rows_deleted := i_del_rc;
            rec_MsgLog.r_end_dttm := systimestamp;
            log_msg (p_rec_MsgLog => rec_MsgLog);  

            if  p_archive_ind in (c_arch_norm, c_arch_all) and gv_DbLinkSynCnt > 0 then 
                EXECUTE IMMEDIATE 'truncate table GTMP_ARCHIVE_PURGE_'||p_purge_cd;
            end if;            

            raise e_arch_purge_failed;
    END;
    
    -- overloaded for tvID arrays
    PROCEDURE PURGE_TABLE
        (
            p_table_name     IN VARCHAR2,
            p_column_name    IN VARCHAR2,
            P_vARRIDS        IN tvIDS,
            P_COMMIT_SIZE    IN PLS_INTEGER,
            P_START_TIME     IN TIMESTAMP,
            P_ARCHIVE_IND    IN CHAR,
            P_PURGE_CD       IN VARCHAR2,
            P_COMP_ID        IN PLS_INTEGER,
            P_WAREHOUSE_ID   IN PLS_INTEGER,
            P_ROWCOUNT       OUT PLS_INTEGER,
            P_ERROR_MSG      OUT VARCHAR2)
    IS
        istart                      pls_integer default 0;
        istop                       pls_integer default 0;
        i_arc_rc                    pls_integer default 0;
        i_del_rc                    pls_integer default 0;
        v_msg                       varchar2(250);
        v_column_list               clob;
        v_archive_table_name        varchar2(30);
        e_ArchTableMappingNotFound  exception;
        i_err_count                 pls_integer;

    BEGIN
        rec_MsgLog.r_start_dttm     := p_start_time;
        rec_MsgLog.r_cd_master_id   := p_comp_id;
        rec_MsgLog.r_purge_code     := p_purge_cd;
        rec_MsgLog.r_whse_id        := p_warehouse_id;
        rec_MsgLog.r_table_name     := p_table_name;

        if p_archive_ind in (c_arch_norm, c_arch_all) and lower(c_debug_yn)='n' then 
           -- check table name length -------------------------------
           v_archive_table_name:=get_archive_table_mapping(p_table_name);
           if v_archive_table_name='Not Found' then
               raise e_ArchTableMappingNotFound;
           end if;

           if gv_DbLinkSynCnt > 0 then
               create_arch_gtmp_table (v_archive_table_name,'D',p_purge_cd);
              gvTempTableforBulkProc := 'GTMP_ARCHIVE_PURGE_'||p_purge_cd;
           else
              gvTempTableforBulkProc := v_archive_table_name;
           end if;

        end if;

        while istop < p_varrIDs.count loop
            get_forall_range (istart, istop, p_commit_size, p_varrIDs.count);

            if c_debug_yn='n' then
                if p_archive_ind in (c_arch_norm, c_arch_all) then
                    forall i in istart..istop
                        EXECUTE IMMEDIATE
                         'INSERT INTO '||gvTempTableforBulkProc||' ('
                            ||get_col_list(p_table_name)
                            ||') SELECT '
                            ||get_col_list(p_table_name)
                            ||' FROM '
                            ||p_table_name
                            ||' WHERE '
                            ||p_column_name
                            ||' = :B1'
                            USING p_varrIDs(i);

                    -- Bulk insert into archive using the gtmp table
                       if gv_DbLinkSynCnt > 0
                       then
                          -- Bulk insert into archive using the gtmp table
                    EXECUTE IMMEDIATE 'INSERT INTO '||v_archive_table_name||' SELECT * FROM GTMP_ARCHIVE_PURGE_'||p_purge_cd;
                       end if;
                    -- increment archive count
                    i_arc_rc := i_arc_rc + sql%rowcount;
                end if; -- archive_ind
            end if;

            -- MACR00773167
            -- CAPTURE BULK ERRORS, ALLOWING ALL REMAINING TO PROCESS
            begin
                -- delete the same
                forall i in istart..istop save exceptions
                    EXECUTE IMMEDIATE 'DELETE FROM '||p_table_name||' WHERE '||p_column_name||' = :B2' USING p_varrIDs(i);

                -- increment delete count
                i_del_rc := i_del_rc + sql%rowcount;

            exception when others then
                -- increment delete count
                i_del_rc := i_del_rc + sql%rowcount;

                -- CAPTURE AND REPORT
                i_err_count:=SQL%BULK_EXCEPTIONS.count;
                for i in 1..i_err_count loop

                    gi_bulk_except_ind := (istart + SQL%BULK_EXCEPTIONS(i).error_index) - 1;

                    -- log the error detail  
                    v_msg := sqlerrm(-SQL%BULK_EXCEPTIONS(i).error_code) 
                             ||' Exception! for table '||  p_table_name || '.'||p_column_name|| ' = ' || p_varrIDs(gi_bulk_except_ind);    
                             
                   v_msg := substr(v_msg,1,500) ; 
         
                    msg_log_insert (p_msg_parms => v_msg
                           ,p_cd_master_id => p_comp_id
                           ,p_warehouse => p_warehouse_id);                                            

                    gi_purge_exception_count := gi_purge_exception_count + 1;
                    
                end loop;
                
                if p_archive_ind in (c_arch_norm, c_arch_all) then 
                   -- #MACR00858713
                   EXECUTE IMMEDIATE 'delete from '|| v_archive_table_name||' where ' || p_column_name || ' in (select ' || p_column_name || ' from ' || p_table_name || ')' ;
                   
                   -- decrement archive count
                   i_arc_rc := i_arc_rc - sql%rowcount;                   
                end if;
                
            end;

            if  p_archive_ind in (c_arch_norm, c_arch_all) and gv_DbLinkSynCnt > 0 then -- CR MACR00866380 added p_archive_ind check
                EXECUTE IMMEDIATE 'truncate table GTMP_ARCHIVE_PURGE_'||p_purge_cd;
            end if;
            
            if lower(c_debug_yn)='n' then 
                commit;
            end if;
        end loop;

        -- set new values and log the message
        rec_MsgLog.r_msg_id         := c_msg_id_inform;
        rec_MsgLog.r_rows_archived  := i_arc_rc;
        rec_MsgLog.r_rows_deleted   := i_del_rc;
        rec_MsgLog.r_end_dttm       := systimestamp;
        log_msg (p_rec_MsgLog       => rec_MsgLog);
        -- return the number of deleted rows
        p_rowcount := i_del_rc;

    EXCEPTION
        WHEN E_ARCHTABLEMAPPINGNOTFOUND THEN
            msg_log_insert (p_msg_parms => 'purge_table('''||p_table_name||'''), purge code=('|| p_purge_cd||
                           '): Exception! -> Archive Table Not Found for '||p_table_name
                           ,p_msg_id => c_msg_id_error
                           ,p_cd_master_id => p_comp_id
                           ,p_warehouse => p_warehouse_id);
                           
            -- set new values and log the message
            rec_MsgLog.r_msg_id := c_msg_id_error;
            rec_MsgLog.r_rows_archived := i_arc_rc;
            rec_MsgLog.r_rows_deleted := i_del_rc;
            rec_MsgLog.r_end_dttm := systimestamp;
            log_msg (p_rec_MsgLog => rec_MsgLog);                           

            raise e_arch_purge_failed;

        WHEN OTHERS THEN
            msg_log_insert (p_msg_parms => 'purge_table('''||p_table_name||'''), purge code=('|| p_purge_cd||
                           '): Exception! -> '||substr(sqlerrm,1,200)
                           ,p_msg_id => c_msg_id_error
                           ,p_cd_master_id => p_comp_id
                           ,p_warehouse => p_warehouse_id);
                           
            -- set new values and log the message
            rec_MsgLog.r_msg_id := c_msg_id_error;
            rec_MsgLog.r_rows_archived := i_arc_rc;
            rec_MsgLog.r_rows_deleted := i_del_rc;
            rec_MsgLog.r_end_dttm := systimestamp;
            log_msg (p_rec_MsgLog => rec_MsgLog); 

            if  p_archive_ind in (c_arch_norm, c_arch_all) and gv_DbLinkSynCnt > 0 then -- CR MACR00866380 added p_archive_ind check
                EXECUTE IMMEDIATE 'truncate table GTMP_ARCHIVE_PURGE_'||p_purge_cd;
            end if;

            raise e_arch_purge_failed;
    END;    

   PROCEDURE purge_table_where     -- use the where clause instead of array of ids
        (
            p_table_name    in varchar2,
            p_where_clause  in varchar2,
            p_commit_size   in pls_integer,
            p_start_time    in timestamp,
            p_archive_ind   in char,
            p_purge_cd      in varchar2,
            p_comp_id       in pls_integer,
            p_warehouse_id  in pls_integer,
            p_rowcount      out pls_integer,
            p_error_msg     out varchar2)
    IS
        istart                      pls_integer default 0;
        istop                       pls_integer default 0;    
        i_arc_rc                    pls_integer default 0;
        i_del_rc                    pls_integer default 0;
        v_msg                       varchar2(250);
        v_table_clause              varchar2(65);
        v_column_list               clob;
        v_archive_table_name        varchar2(30);
        e_ArchTableMappingNotFound  exception;
        i_id_count                  number(20) default 0;
        v_columns_names             varchar2(250);
        v_sql                       varchar2(32000);
        
    BEGIN
        rec_MsgLog.r_start_dttm     := p_start_time;
        rec_MsgLog.r_cd_master_id   := p_comp_id;
        rec_MsgLog.r_purge_code     := p_purge_cd;
        rec_MsgLog.r_whse_id        := p_warehouse_id;
        rec_MsgLog.r_table_name     := p_table_name;

        if p_archive_ind in (c_arch_norm, c_arch_all) and lower(c_debug_yn)='n' then
        
           -- check table name length -------------------------------
           v_archive_table_name:=get_archive_table_mapping(p_table_name);
           if length(v_archive_table_name)=0 then
               raise e_ArchTableMappingNotFound;
           end if;

           create_arch_gtmp_table (v_archive_table_name,'P',p_purge_cd);
           
           EXECUTE IMMEDIATE 'alter table GTMP_ARCHIVE_PURGE_'||P_PURGE_CD||' add gtmp_rowid number(20)';
           
           BEGIN
                SELECT LISTAGG (ucc.column_name, ',')
                          WITHIN GROUP (ORDER BY ucc.position)
                  INTO v_columns_names
                  FROM user_cons_columns ucc, user_constraints uc
                 WHERE ucc.table_name = uc.table_name
                   AND uc.constraint_type = 'P'
                   AND ucc.constraint_name = uc.constraint_name
                   AND ucc.table_name = upper(p_table_name)
              GROUP BY ucc.constraint_name;
           EXCEPTION
              WHEN OTHERS
              THEN
                BEGIN
                SELECT LISTAGG (ucc.column_name, ',')
                          WITHIN GROUP (ORDER BY ucc.position)
                  INTO v_columns_names
                  FROM all_cons_columns ucc, all_constraints uc
                 WHERE ucc.owner=uc.owner
                   AND ucc.owner in (select table_owner from user_synonyms where table_name = upper(p_table_name) and synonym_name not like 'ARCH_%')
                   AND ucc.table_name = uc.table_name
                   AND uc.constraint_type = 'P'
                   AND ucc.constraint_name = uc.constraint_name
                   AND ucc.table_name = upper(p_table_name)
              GROUP BY ucc.constraint_name; 
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     NULL;
           END; 
           END;

           v_sql := 'insert into gtmp_archive_purge_'||p_purge_cd||' ('
                       ||get_col_list(p_table_name)
                       ||',archive_dttm) SELECT '
                       ||get_col_list(p_table_name,'Y')
                       ||',systimestamp FROM '||p_table_name||' t '
                           ||p_where_clause;
    
           if p_purge_cd > 100
           then
             EXECUTE IMMEDIATE v_sql using p_start_time;
           else 
           EXECUTE IMMEDIATE v_sql;
           end if;

           -- set the rowid values for the commit frequency
           EXECUTE IMMEDIATE 'update GTMP_ARCHIVE_PURGE_'||p_purge_cd||'  set gtmp_rowid = rownum'; 
           
           -- get the number of ids to process]
           v_sql := 'SELECT count(1) FROM GTMP_ARCHIVE_PURGE_'||p_purge_cd;
           EXECUTE IMMEDIATE v_sql INTO i_id_count;           
           
        end if;

        while istop < i_id_count loop

           get_forall_range (istart, istop, p_commit_size, i_id_count);    
           if c_debug_yn='n' and p_archive_ind in (c_arch_norm, c_arch_all) 
           then
               -- Insert into archive table
               execute immediate 'INSERT INTO '||v_archive_table_name||' ('
                 ||get_col_list(p_table_name)
                 ||') SELECT '
                 ||get_col_list(p_table_name)
                 ||' FROM gtmp_archive_purge_'||p_purge_cd||' WHERE gtmp_rowid BETWEEN ' || to_char(istart) || ' AND ' || to_char(istop);

               -- increment archive count
               I_ARC_RC := I_ARC_RC + SQL%ROWCOUNT;
               
           end if; -- archive_ind
        
		   if v_columns_names is not null
           then
              v_sql := 'DELETE FROM '||p_table_name|| ' WHERE (' || v_columns_names || ') IN  ( SELECT ' || v_columns_names || ' FROM GTMP_ARCHIVE_PURGE_'||p_purge_cd||' WHERE gtmp_rowid BETWEEN ' || to_char(istart) || ' AND ' || to_char(istop) || ')';
           else
              v_sql := 'DELETE FROM '||p_table_name||' t '||p_where_clause;           
           end if;  
           
           if p_purge_cd > 100 and v_columns_names is null
           then
             EXECUTE IMMEDIATE v_sql using p_start_time;
           else 
             EXECUTE IMMEDIATE v_sql;
           end if;

           -- increment delete count
           i_del_rc := i_del_rc + sql%rowcount;
           
           if lower(c_debug_yn)='n' then
              commit;
           end if;
        
        end loop;

        if lower(c_debug_yn)='n' and p_archive_ind in (c_arch_norm, c_arch_all) then -- CR MACR00866380 added p_archive_ind
           EXECUTE IMMEDIATE 'truncate table GTMP_ARCHIVE_PURGE_'||p_purge_cd;
        end if;
        
        -- set new values and log the message
        rec_MsgLog.r_msg_id         := c_msg_id_inform;
        rec_MsgLog.r_rows_archived  := i_arc_rc;
        rec_MsgLog.r_rows_deleted   := i_del_rc;
        rec_MsgLog.r_end_dttm       := systimestamp;
        log_msg (p_rec_MsgLog       => rec_MsgLog);
        -- return the number of deleted rows
        p_rowcount := i_del_rc;

    EXCEPTION
        WHEN E_ARCHTABLEMAPPINGNOTFOUND THEN
            p_error_msg := 'Table mapping expected but not found';
            msg_log_insert(p_msg_parms => 'purge_table_where('''||p_table_name||'''), purge_code=' || p_purge_cd||': Exception! -> ' || p_error_msg,p_cd_master_id => p_comp_id
                           ,p_warehouse => p_warehouse_id);
            -- set new values and log the message
            rec_MsgLog.r_msg_id := c_msg_id_error;
            rec_MsgLog.r_rows_archived := i_arc_rc;
            rec_MsgLog.r_rows_deleted := i_del_rc;
            rec_MsgLog.r_end_dttm := systimestamp;
            log_msg (p_rec_MsgLog => rec_MsgLog);
            
            raise e_arch_purge_failed;
            
        WHEN OTHERS THEN
            p_error_msg := substr(sqlerrm,1,200);
            msg_log_insert(p_msg_parms => 'purge_table_where('''||p_table_name||'''), purge_code=' || p_purge_cd||': Exception! -> ' || p_error_msg,p_cd_master_id => p_comp_id
                           ,p_warehouse => p_warehouse_id);
            -- set new values and log the message
            rec_MsgLog.r_msg_id := c_msg_id_error;
            rec_MsgLog.r_rows_archived := i_arc_rc;
            rec_MsgLog.r_rows_deleted := i_del_rc;
            rec_MsgLog.r_end_dttm := systimestamp;
            log_msg (p_rec_MsgLog => rec_MsgLog);
            
           if lower(c_debug_yn)='n' and p_archive_ind in (c_arch_norm, c_arch_all) then -- CR MACR00866380 added p_archive_ind
              EXECUTE IMMEDIATE 'truncate table GTMP_ARCHIVE_PURGE_'||p_purge_cd;
           end if;
            
            raise e_arch_purge_failed;
    END;

    /* BEGIN PRIVATE SUB-ROUTINES FOR EXEC_PURGE */
    -- called by exec_purge
    
      -- WM-40829: Created procedure for call from Shipments purge
    procedure purge_appointments (
                                 pa_company_id       IN purge_config.cd_master_id%type
                                ,pa_whse_id          IN purge_config.whse_master_id%type
                                ,pa_commit_size      IN purge_config.nbr_rows_to_commit%type
                                ,pa_days             IN purge_config.nbr_days_old%type
                                ,pa_purge_cd         IN purge_config.purge_code%type
                                ,pa_archive_flag     IN purge_config.archive_flag%type
                                ,pa_child_comp_flag  IN purge_config.include_child_company_flag%type
                                ,pa_start_time       IN timestamp
                                ,pa_rows_purged      OUT pls_integer
                                )
    is
        dstart_dttm             timestamp(6);
        i_rc                    pls_integer default 0;
        i_rows_purged           pls_integer default 0;
        v_msg                   varchar2(250);
        v_error_msg             purge_hist.message%type;

    begin
        delete from tmp_wm_id_1;
        dstart_dttm := sysdate;
        -- select purge-able appointment_ids
        insert into tmp_wm_id_1 (i_id)
        select appointment_id
        from ilm_appointments ia
        where ia.last_updated_dttm <= pa_start_time - pa_days
        and ia.appt_status in (1,9,10)
        and ia.facility_id = pa_whse_id
        and ia.company_id = pa_company_id
		and not exists (select 1 from TRAILER_VISIT_DETAIL tvd where tvd.appointment_id=ia.appointment_id) ;--MACR00355920

        -- select purge-able appointment chargeback ids
        select chargeback_id
        bulk collect into arrIDs_1
        from ilm_appointments_chargeback, tmp_wm_id_1 t
        where appointment_id = i_id; 

        -- get the appointment_ids
        select i_id
        bulk collect into arrIDs
        from tmp_wm_id_1;

               -- CR MACR00865176 changes
        update EQUIPMENT_INSTANCE 
        set CURRENT_APPOINTMENT_ID = null
        where CURRENT_APPOINTMENT_ID in  (select i_id from tmp_wm_id_1);

        -- corrected the table as part of MACR00858284
        purge_table (p_table_name    => 'ILM_APPOINTMENTS_CHARGEBACK'
                   , p_column_name   => 'CHARGEBACK_ID'
                   , p_arrids        => arrIDs_1
                   , p_commit_size   => pa_commit_size
                   , p_start_time    => dstart_dttm
                   , p_archive_ind   => pa_archive_flag
                   , p_purge_cd      => pa_purge_cd
                   , p_comp_id       => pa_company_id
                   , p_warehouse_id  => pa_whse_id
                   , p_rowcount      => i_rc
                   , p_error_msg     => v_error_msg);
        i_rows_purged := i_rows_purged + i_rc; 
		
        for CURREC in
          (
            select 'ILM_APPOINTMENT_EVENTS' TAB_NAME from dual
            UNION ALL
            select 'ILM_APPT_CUSTOM_ATTRIBUTE' TAB_NAME  from dual
            UNION ALL
            select 'ILM_APPT_LOAD_CONFIG' TAB_NAME  from dual
            UNION ALL
            select 'ILM_APPT_NOTES' TAB_NAME  from dual
            UNION ALL
            select 'ILM_APPT_SLOTS' TAB_NAME  from dual
            UNION ALL
            select 'ILM_APPT_DOCKS' TAB_NAME  from dual
            UNION ALL
            select 'ILM_APPT_EQUIPMENTS' TAB_NAME  from dual
            UNION ALL
            select 'ILM_APPT_SIZES' TAB_NAME  from dual
            UNION ALL
            select 'ILM_APPOINTMENT_OBJECTS' TAB_NAME  from dual
            UNION ALL
            select 'ILM_YARD_ACTIVITY' TAB_NAME  from dual
            UNION ALL
            select 'DOCK_DOOR_REF' TAB_NAME  from dual
            UNION ALL
            select 'TRAILER_VISIT_DETAIL' TAB_NAME  from dual
            UNION ALL
            select 'ILM_APPOINTMENTS' TAB_NAME  from dual
          )
        loop
            purge_table ( p_table_name  => currec.tab_name
                        , p_column_name => 'APPOINTMENT_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => pa_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pa_archive_flag
                        , p_purge_cd    => pa_purge_cd
                        , p_comp_id     => pa_company_id
                        , p_warehouse_id=> pa_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;
        end loop;

        pa_rows_purged := i_rows_purged;

    exception
        when others then
            v_msg := v_msg || ' ' || substr(sqlerrm,1,250);
            msg_log_insert('purge_appointments(): Exception! -> ' || v_msg);
            pa_rows_purged := i_rows_purged;

            raise e_arch_purge_failed;
    end;
	
	
-- start archive c_ilpn_lot_mapping.  Case-3324988
	
	PROCEDURE arch_c_ilpn_lot_mapping (
                             p_arrIDs       IN tIDs
                            ,p_commit_size  IN pls_integer
                            ,p_start_time   IN timestamp
                            ,p_archive_ind  IN char
                            ,p_purge_cd     IN varchar2
                            ,p_comp_id      IN pls_integer
                            ,p_warehouse_id IN pls_integer
                            ,p_rowcount     OUT pls_integer
                            )
    IS
        istart      pls_integer default 0;
        istop       pls_integer default 0;
        i_arc_rc    pls_integer default 0;
        i_del_rc    pls_integer default 0;
        v_msg       varchar2(250);

    BEGIN
        rec_MsgLog.r_start_dttm := p_start_time;
        rec_MsgLog.r_cd_master_id := p_comp_id;
        rec_MsgLog.r_purge_code := p_purge_cd;
        rec_MsgLog.r_whse_id := p_warehouse_id;
        rec_MsgLog.r_table_name := 'C_ILPN_LOT_MAPPING';

		WHILE ISTOP < P_ARRIDS.COUNT LOOP
            -- get the index range to process
            get_forall_range (istart, istop, p_commit_size, p_arrIDs.count);
            if p_archive_ind in (c_arch_norm, c_arch_all) then
                -- archive the data
                FOR I IN ISTART..ISTOP LOOP
				DECLARE
					lpnCount NUMBER(3);
					tcLpnId VARCHAR2(20 CHAR);
				begin
          select nvl(tc_lpn_id,0) INTO tcLpnId
					from (select distinct tc_lpn_id 
						FROM C_ILPN_LOT_MAPPING WHERE TC_LPN_ID= (select tc_lpn_id from lpn where lpn_id=P_ARRIDS(I)));
--						FROM C_ILPN_LOT_MAPPING WHERE TC_LPN_ID=LPAD(to_char(P_ARRIDS(I)),20,'0');

							INSERT INTO ARCH_C_ILPN_LOT_MAPPING (
													BATCH_NBR,
													XPIRE_DATE,
													ITEM_NAME,
													TC_LPN_ID,
													CREATED_DTTM,
													CREATED_SOURCE,
													CREATED_USER_ID,
													LAST_MODIFIED_SOURCE,
													LAST_MODIFIED_BY,
													LAST_UPDATED_DTTM)
							select BATCH_NBR,
													XPIRE_DATE,
													ITEM_NAME,
													TC_LPN_ID,
													CREATED_DTTM,
													CREATED_SOURCE,
													CREATED_USER_ID,
													LAST_MODIFIED_SOURCE,
													LAST_MODIFIED_BY,
													LAST_UPDATED_DTTM from C_ILPN_LOT_MAPPING where tc_lpn_id = tcLpnId;
								-- increment archive count
							I_ARC_RC := I_ARC_RC + SQL%ROWCOUNT;
				END;
                end loop;
            end if;  -- archive_ind
            -- delete the same
            FORALL I IN ISTART..ISTOP
				delete from C_ILPN_LOT_MAPPING WHERE TC_LPN_ID= (select tc_lpn_id from lpn where lpn_id=P_ARRIDS(I));
                --delete from C_ILPN_LOT_MAPPING where tc_lpn_id = LPAD(to_char(P_ARRIDS(I)),20,'0');
                -- increment delete count
                I_DEL_RC := I_DEL_RC + SQL%ROWCOUNT;
            if lower(c_debug_yn)='n' then
                commit;
            end if;
        end loop;
        --write_debug_output(p_msg_label=>'ARCH_C_ILPN_LOT_MAPPING()', p_arrCompID => p_comp_id, p_rows_archived => i_arc_rc, p_rows_purged => i_del_rc);
        -- set new values and log the message
        rec_MsgLog.r_msg_id := c_msg_id_inform;
        rec_MsgLog.r_rows_archived := i_arc_rc;
        rec_MsgLog.r_rows_deleted := i_del_rc;
        rec_MsgLog.r_end_dttm := systimestamp;
        log_msg (p_rec_MsgLog => rec_MsgLog);
        -- return the number of deleted rows
        p_rowcount := i_del_rc;

    EXCEPTION
        WHEN OTHERS THEN
            v_msg := substr(sqlerrm,1,250);
            msg_log_insert('ARCH_C_ILPN_LOT_MAPPING(): Exception! -> ' || v_msg);
            -- set new values and log the message
            rec_MsgLog.r_msg_id := c_msg_id_error;
            rec_MsgLog.r_rows_archived := i_arc_rc;
            rec_MsgLog.r_rows_deleted := i_del_rc;
            rec_MsgLog.r_end_dttm := systimestamp;
            log_msg (p_rec_MsgLog => rec_MsgLog);

            raise e_arch_purge_failed;
    END;

--End archive c_ilpn_lot_mapping
    
    PROCEDURE purge_lpns(
                         pl_company_id       IN purge_config.cd_master_id%type
                        ,pl_whse_id          IN purge_config.whse_master_id%type
                        ,pl_commit_size      IN purge_config.nbr_rows_to_commit%type
                        ,pl_days             IN purge_config.nbr_days_old%type
                        ,pl_purge_cd         IN purge_config.purge_code%type
                        ,pl_archive_flag     IN purge_config.archive_flag%type
                        ,pl_child_comp_flag  IN purge_config.include_child_company_flag%type
                        ,pl_rows_purged      OUT pls_integer
                        )
    IS
        dstart_dttm             timestamp(6);
        i_rc                    pls_integer default 0;
        i_rows_purged           pls_integer default 0;
        v_msg                   varchar2(250);
        v_error_msg             purge_hist.message%type;

    BEGIN
        msg_log_insert('BEGIN UI_PURGE() ----> purge_lpns()');
        dstart_dttm := systimestamp;

            delete from tmp_wm_arch_lpn_1 t
            where i_id in ( select s.lpn_id  from srl_nbr_track s where s.lpn_id = t.i_id and s.stat_code < 90
                            union
                            select ol.lpn_id from lpn ol 
                            where ol.TC_REFERENCE_LPN_ID = t.v_id
                            and ol.inbound_outbound_indicator = 'O' 
                            and ol.lpn_facility_status < c_o_lpn_stat_code);
			    
	    -- c_ilpn_lot_mapping purge
		msg_log_insert('Begin c_ilpn_lot_mapping purge');
        select distinct t1.i_id bulk collect into arrIDs from C_ILPN_LOT_MAPPING cilm, tmp_wm_arch_lpn_1 t1
        where cilm.tc_lpn_id = t1.v_id and t1.i_id is not null;
        
        arch_c_ilpn_lot_mapping (arrIDs     -- tc_lpn_id
                            , pl_commit_size
                            , dstart_dttm
                            , pl_archive_flag
                            , pl_purge_cd
                            , pl_company_id
                            , pl_whse_id
                            , i_rc);
        i_rows_purged := i_rows_purged + i_rc;
          msg_log_insert('End c_ilpn_lot_mapping purge');

		-- end c_ilpn_lot_mapping purge

        select distinct s.srl_nbr_trk_id , minr.srl_nbr_trk_id , fri.srl_nbr_trk_id
        bulk collect into arrIDs , arrIDs_1, arrIDs_2
        from srl_nbr_track s
             inner join tmp_wm_arch_lpn_1 t1
             on (s.lpn_id = t1.i_id)
             left outer join minor_srl_track minr
             on (s.srl_nbr_trk_id = minr.srl_nbr_trk_id)
             left outer join frozn_invn_srl_nbr fri
             on ( s.srl_nbr_trk_id = fri.srl_nbr_trk_id)
        where s.stat_code >= 90;    
		
		    purge_table ( p_table_name  => 'frozn_invn_srl_nbr' 
                        , p_column_name => 'srl_nbr_trk_id'
                            , p_arrids      => arrIDs_2
                            , p_commit_size => pl_commit_size
                            , p_start_time  => dstart_dttm
                            , p_archive_ind => pl_archive_flag
                            , p_purge_cd    => pl_purge_cd
                            , p_comp_id     => pl_company_id
                            , p_warehouse_id=> pl_whse_id
                            , p_rowcount    => i_rc
                            , p_error_msg   => v_error_msg); 
            i_rows_purged := i_rows_purged + i_rc;    


		    purge_table ( p_table_name  => 'minor_srl_track' 
                        , p_column_name => 'srl_nbr_trk_id'
                        , p_arrids      => arrIDs_1 
                        , p_commit_size => pl_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pl_archive_flag
                        , p_purge_cd    => pl_purge_cd
                        , p_comp_id     => pl_company_id
                        , p_warehouse_id=> pl_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;    

                            
			
		    purge_table ( p_table_name  => 'srl_nbr_track' 
                        , p_column_name => 'srl_nbr_trk_id'
                        , p_arrids      => arrIDs 
                        , p_commit_size => pl_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pl_archive_flag
                        , p_purge_cd    => pl_purge_cd
                        , p_comp_id     => pl_company_id
                        , p_warehouse_id=> pl_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
                i_rows_purged := i_rows_purged + i_rc;
                      
            select i.frozn_invn_dtl_id
            bulk collect into arrIDs
            from frozn_invn_hdr s, tmp_wm_arch_lpn_1 t1, frozn_invn_dtl i
            where s.lpn_id = t1.i_id
            and s.frozn_invn_hdr_id = i.frozn_invn_hdr_id; 
            
            purge_table ( p_table_name  => 'frozn_invn_srl_nbr' 
                        , p_column_name => 'frozn_invn_dtl_id'
                        , p_arrids      => arrIDs 
                        , p_commit_size => pl_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pl_archive_flag
                        , p_purge_cd    => pl_purge_cd
                        , p_comp_id     => pl_company_id
                        , p_warehouse_id=> pl_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;    
            
            select frozn_invn_hdr_id
            bulk collect into arrIDs
            from frozn_invn_hdr s, tmp_wm_arch_lpn_1 t1
            where s.lpn_id = t1.i_id;    
            
            for currec in (
                            select 'FROZN_INVN_DTL' TAB_NAME from dual
                            union all
                            select 'FROZN_INVN_HDR' TAB_NAME from dual
                          )
            loop              
                 purge_table ( p_table_name  => currec.tab_name
                            , p_column_name => 'FROZN_INVN_HDR_ID'
                            , p_arrids      => arrIDs
                            , p_commit_size => pl_commit_size
                            , p_start_time  => dstart_dttm
                            , p_archive_ind => pl_archive_flag
                            , p_purge_cd    => pl_purge_cd
                            , p_comp_id     => pl_company_id
                            , p_warehouse_id=> pl_whse_id
                            , p_rowcount    => i_rc
                            , p_error_msg   => v_error_msg); 
                            
                i_rows_purged := i_rows_purged + i_rc;
            end loop;              

            select i.cnt_invn_dtl_id
            bulk collect into arrIDs
            from cnt_invn_hdr s, tmp_wm_arch_lpn_1 t1, cnt_invn_dtl i 
            where s.lpn_id = t1.i_id
            and s.cnt_invn_hdr_id = i.cnt_invn_hdr_id ;
            
            
            purge_table ( p_table_name  => 'CNT_INVN_SRL_NBR' 
                        , p_column_name => 'CNT_INVN_DTL_ID'
                        , p_arrids      => arrIDs 
                        , p_commit_size => pl_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pl_archive_flag
                        , p_purge_cd    => pl_purge_cd
                        , p_comp_id     => pl_company_id
                        , p_warehouse_id=> pl_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;    
            
            select cnt_invn_hdr_id  
            bulk collect into arrIDs
            from cnt_invn_hdr s, tmp_wm_arch_lpn_1 t1
            where s.lpn_id = t1.i_id;

            for currec in (
                            select 'CNT_INVN_DTL' TAB_NAME from dual
                            union all
                            select 'CNT_INVN_HDR' TAB_NAME from dual
                          )
            loop              
                 purge_table ( p_table_name  => currec.tab_name
                            , p_column_name => 'CNT_INVN_HDR_ID'
                            , p_arrids      => arrIDs
                            , p_commit_size => pl_commit_size
                            , p_start_time  => dstart_dttm
                            , p_archive_ind => pl_archive_flag
                            , p_purge_cd    => pl_purge_cd
                            , p_comp_id     => pl_company_id
                            , p_warehouse_id=> pl_whse_id
                            , p_rowcount    => i_rc
                            , p_error_msg   => v_error_msg); 
                            
                i_rows_purged := i_rows_purged + i_rc;
            end loop;             

            -- select the wm_inventory rows to purge using lpn_id and lpn_detail_id combo
            select w1.wm_inventory_id
            bulk collect into arrIDs
            from wm_inventory w1, lpn_detail d, tmp_wm_arch_lpn_1 t1
            where d.lpn_id          = t1.i_id
            and t1.i_id             = w1.lpn_id
            and d.lpn_id            = w1.lpn_id
            and w1.lpn_detail_id    = d.lpn_detail_id;

            -- wm_inventory
            purge_table ( p_table_name  => 'WM_INVENTORY' 
                        , p_column_name => 'WM_INVENTORY_ID'
                        , p_arrids      => arrIDs 
                        , p_commit_size => pl_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pl_archive_flag
                        , p_purge_cd    => pl_purge_cd
                        , p_comp_id     => pl_company_id
                        , p_warehouse_id=> pl_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;            

            -- lpn_accessorial
            select distinct la.lpn_accessorial_id
            bulk collect into arrIDs
            from lpn_accessorial la, tmp_wm_arch_lpn_1 t1
            where la.lpn_id = t1.i_id;
            
            purge_table ( p_table_name  => 'LPN_ACCESSORIAL'
                        , p_column_name => 'LPN_ACCESSORIAL_ID'
                        , p_arrids      => arrIDs 
                        , p_commit_size => pl_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pl_archive_flag
                        , p_purge_cd    => pl_purge_cd
                        , p_comp_id     => pl_company_id
                        , p_warehouse_id=> pl_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;            

            -- manifest tables
            select mh.manifest_id
            bulk collect into arrIDs
            from manifest_hdr mh
            where mh.manifest_status_id >= 90 -- closed
            and exists (
                        select 1
                        from manifested_lpn ml, tmp_wm_arch_lpn_1
                        where ml.lpn_id = i_id
                        and ml.manifest_id = mh.manifest_id
                        );
                        
            for currec in (
                            select 'MANIFESTED_LPN' TAB_NAME from dual
                            union all
                            select 'MANIFEST_HDR' TAB_NAME from dual
                          )
            loop              
                 purge_table ( p_table_name  => currec.tab_name
                            , p_column_name => 'MANIFEST_ID'
                            , p_arrids      => arrIDs
                            , p_commit_size => pl_commit_size
                            , p_start_time  => dstart_dttm
                            , p_archive_ind => pl_archive_flag
                            , p_purge_cd    => pl_purge_cd
                            , p_comp_id     => pl_company_id
                            , p_warehouse_id=> pl_whse_id
                            , p_rowcount    => i_rc
                            , p_error_msg   => v_error_msg); 
                            
                i_rows_purged := i_rows_purged + i_rc;
            end loop;                          
  
            -- fedex_lpn
            select distinct fedex_lpn_id
            bulk collect into arrIDs
            from fedex_lpn, tmp_wm_arch_lpn_1
            where lpn_id = i_id;

            purge_table ( p_table_name  => 'FEDEX_LPN'
                        , p_column_name => 'FEDEX_LPN_ID'
                        , p_arrids      => arrIDs 
                        , p_commit_size => pl_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pl_archive_flag
                        , p_purge_cd    => pl_purge_cd
                        , p_comp_id     => pl_company_id
                        , p_warehouse_id=> pl_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;

            -- vas_carton
            select distinct vas_carton_id
            bulk collect into arrIDs
            from vas_carton, tmp_wm_arch_lpn_1
            where carton_nbr = v_id;
            
            purge_table ( p_table_name  => 'VAS_CARTON'
                        , p_column_name => 'VAS_CARTON_ID'
                        , p_arrids      => arrIDs 
                        , p_commit_size => pl_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pl_archive_flag
                        , p_purge_cd    => pl_purge_cd
                        , p_comp_id     => pl_company_id
                        , p_warehouse_id=> pl_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;
            
            -- vocollect_active_carton
            select /*+ dynamic_sampling (tmp_wm_arch_lpn_1 2) */ distinct vocollect_active_carton_id -- added distinct #MACR00859795
            bulk collect into arrIDs
            from vocollect_active_carton, tmp_wm_arch_lpn_1
            where active_carton = v_id;
            
            purge_table ( p_table_name  => 'VOCOLLECT_ACTIVE_CARTON'
                        , p_column_name => 'VOCOLLECT_ACTIVE_CARTON_ID'
                        , p_arrids      => arrIDs 
                        , p_commit_size => pl_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pl_archive_flag
                        , p_purge_cd    => pl_purge_cd
                        , p_comp_id     => pl_company_id
                        , p_warehouse_id=> pl_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;            

            -- epc_track #MACR00867115 PE changes added.
            select distinct epc_track_id
            bulk collect into arrIDs
            from (select epc_track_id
                  from epc_track, tmp_wm_arch_lpn_1
                  where v_id = case_nbr
                  UNION
                  select epc_track_id
                  from epc_track, tmp_wm_arch_lpn_1
                  where v_id = carton_nbr);
            
            purge_table ( p_table_name  => 'EPC_TRACK'
                        , p_column_name => 'EPC_TRACK_ID'
                        , p_arrids      => arrIDs 
                        , p_commit_size => pl_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pl_archive_flag
                        , p_purge_cd    => pl_purge_cd
                        , p_comp_id     => pl_company_id
                        , p_warehouse_id=> pl_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;                

            -- MACR00624890: Removed the pix_tran purge based on the discussion with Ravi.

            -- immd_needs
            select distinct immd_needs_id
            bulk collect into arrIDs
            from immd_needs, tmp_wm_arch_lpn_1
            where case_nbr = v_id;
            
            purge_table ( p_table_name  => 'IMMD_NEEDS'
                        , p_column_name => 'IMMD_NEEDS_ID'
                        , p_arrids      => arrIDs 
                        , p_commit_size => pl_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pl_archive_flag
                        , p_purge_cd    => pl_purge_cd
                        , p_comp_id     => pl_company_id
                        , p_warehouse_id=> pl_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;                

            -- vend_perf_tran
            select distinct vend_perf_tran_id
            bulk collect into arrIDs
            from vend_perf_tran, tmp_wm_arch_lpn_1
            where case_nbr = v_id;
            
            purge_table ( p_table_name  => 'VEND_PERF_TRAN'
                        , p_column_name => 'VEND_PERF_TRAN_ID'
                        , p_arrids      => arrIDs 
                        , p_commit_size => pl_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pl_archive_flag
                        , p_purge_cd    => pl_purge_cd
                        , p_comp_id     => pl_company_id
                        , p_warehouse_id=> pl_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;            

            -- divrt_hist
            select distinct divrt_hist_id
            bulk collect into arrIDs
            from divrt_hist, tmp_wm_arch_lpn_1
            where cntr_nbr = v_id;
            
            purge_table ( p_table_name  => 'DIVRT_HIST'
                        , p_column_name => 'DIVRT_HIST_ID'
                        , p_arrids      => arrIDs 
                        , p_commit_size => pl_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pl_archive_flag
                        , p_purge_cd    => pl_purge_cd
                        , p_comp_id     => pl_company_id
                        , p_warehouse_id=> pl_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;                

            -- task_hdr_id/task_dtl_id-------------------------------
            delete from tmp_wm_arch_lpn_2;

-- MACR00440675
            insert into tmp_wm_arch_lpn_2 (i_hdr_id, i_dtl_id)
                with purgeable_tasks as
                  (
                    select task_hdr_id, task_dtl_id, task_id
                    from task_dtl td, tmp_wm_arch_lpn_1 t
                    where ( (t.v_id =td.carton_nbr and t.io_indicator = 'O')
                    or
                     (t.v_id=td.cntr_nbr and  td.carton_nbr is null and t.io_indicator  = 'I'))  --   -- WM-26717 added condition to check OLPN eligibility.
				    and td.mod_date_time <= dstart_dttm - pl_days -- WM-98278	 
                  )
                select o2.task_hdr_id, o2.task_dtl_id
                from task_hdr o1, purgeable_tasks o2
                where o1.task_hdr_id = o2.task_hdr_id
                and
                -- if orig_task_id is not null check for purgable tasks
                (o1.orig_task_id is null
                  or
                  exists (-- select the purge-able task_ids
                          select 1
                          from purgeable_tasks i1
                          where o1.orig_task_id = i1.task_id  -- orig_task_id of purge candidate is a purgeable task_id
                          )
                 );

            select distinct i_dtl_id
            bulk collect into arrIDs
            from tmp_wm_arch_lpn_2;

            purge_table ( p_table_name  => 'TASK_DTL'
                        , p_column_name => 'TASK_DTL_ID'
                        , p_arrids      => arrIDs 
                        , p_commit_size => pl_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pl_archive_flag
                        , p_purge_cd    => pl_purge_cd
                        , p_comp_id     => pl_company_id
                        , p_warehouse_id=> pl_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;

            -- BEGIN MACR00532267
            -- prod_trkg_tran  MOVED UP because is a child of TASK_HDR
            select /*+ leading(tmp_wm_arch_lpn_2) */ distinct prod_trkg_tran_id
            bulk collect into arrIDs
            from (select distinct prod_trkg_tran_id from prod_trkg_tran p, task_hdr t, tmp_wm_arch_lpn_2
            where   p.task_id = t.task_id
            and     t.task_hdr_id = i_hdr_id
			UNION
            select distinct p.prod_trkg_tran_id
            from prod_trkg_tran p, tmp_wm_arch_lpn_1,lpn l
            where p.cntr_nbr = v_id
			and l.lpn_id=i_id
			and (
			  (l.inbound_outbound_indicator='I' and p.wave_nbr is null)
			   or (l.inbound_outbound_indicator='O' and nvl(p.wave_nbr,0)=nvl(l.wave_nbr,0))));

            purge_table ( p_table_name  => 'PROD_TRKG_TRAN'
                        , p_column_name => 'PROD_TRKG_TRAN_ID'
                        , p_arrids      => arrIDs 
                        , p_commit_size => pl_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pl_archive_flag
                        , p_purge_cd    => pl_purge_cd
                        , p_comp_id     => pl_company_id
                        , p_warehouse_id=> pl_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);          
            i_rows_purged := i_rows_purged + i_rc;
            
        -- task_hdr_ids where all task_dtl records were removed . Modified with MACR00532267
            select distinct i_hdr_id
            bulk collect into arrIDs
            from tmp_wm_arch_lpn_2
            where not exists (
                            select 1
                            from task_dtl
                            where task_hdr_id = i_hdr_id
                            )
            and not exists (
                            select 1
                            from prod_trkg_tran p, task_hdr t
                            where p.task_id = t.task_id
                            and t.task_hdr_id = i_hdr_id
                            );

            set_taskhdr_origtaskid_to_null(arrIDs);
            purge_table ( p_table_name  => 'TASK_HDR'
                        , p_column_name => 'TASK_HDR_ID'
                        , p_arrids      => arrIDs 
                        , p_commit_size => pl_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pl_archive_flag
                        , p_purge_cd    => pl_purge_cd
                        , p_comp_id     => pl_company_id
                        , p_warehouse_id=> pl_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);          
            i_rows_purged := i_rows_purged + i_rc;
            -- END MACR00532267

            -- alloc_invn_dtl
            -- WM-26717 Modified the below SQL not to consider the AID's with OLPN not in completed or cancelled status. 
            select distinct /*+ index (tmp_wm_arch_lpn_1 IDX_GTMP_ARCH_LPN_ID_V_ID)
                       index (alloc_invn_dtl ALOCINVNDTL_CRTNNBR_IDX)
                       index (alloc_invn_dtl ALOCINVNDTL_CNTRNBR_IDX) */ alloc_invn_dtl_id
            bulk collect into arrIDs
            from tmp_wm_arch_lpn_1 t, alloc_invn_dtl aid
            where  ( (t.v_id =aid.carton_nbr and t.io_indicator = 'O')
            or
             (t.v_id=aid.cntr_nbr and  aid.carton_nbr is null and t.io_indicator = 'I')) ;  -- WM-26717 added condition to check OLPN eligibility.

            purge_table ( p_table_name  => 'ALLOC_INVN_DTL'
                        , p_column_name => 'ALLOC_INVN_DTL_ID'
                        , p_arrids      => arrIDs 
                        , p_commit_size => pl_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pl_archive_flag
                        , p_purge_cd    => pl_purge_cd
                        , p_comp_id     => pl_company_id
                        , p_warehouse_id=> pl_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;

            -- TE-2576 removing ws_order_lpn as the table is dropped in 2014.
            
            -- added a join to eliminate empty executions
            select /*+ leading(l) */ distinct i_id
            bulk collect into arrIDs
            from tmp_wm_arch_lpn_1 l, lpn_address ad
            where l.i_id = ad.lpn_id;
            
            purge_table ( p_table_name  => 'LPN_ADDRESS'
                        , p_column_name => 'LPN_ID'
                        , p_arrids      => arrIDs 
                        , p_commit_size => pl_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pl_archive_flag
                        , p_purge_cd    => pl_purge_cd
                        , p_comp_id     => pl_company_id
                        , p_warehouse_id=> pl_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;                

            select distinct lpn_event_id
            bulk collect into arrIDs_1
            from lpn_event le, lpn_detail d, tmp_wm_arch_lpn_1 t1
            where d.lpn_id       = t1.i_id
            and d.lpn_id         = le.lpn_id
            and le.lpn_id        = t1.i_id
            and le.lpn_detail_id = d.lpn_detail_id;

            purge_table ( p_table_name  => 'LPN_EVENT'
                        , p_column_name => 'LPN_EVENT_ID'
                        , p_arrids      => arrIDs_1
                        , p_commit_size => pl_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pl_archive_flag
                        , p_purge_cd    => pl_purge_cd
                        , p_comp_id     => pl_company_id
                        , p_warehouse_id=> pl_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;    

            -- added a join to eliminate empty executions -- Upendra
            select /*+ leading(l) */ distinct i_id
            bulk collect into arrIDs
            from tmp_wm_arch_lpn_1 l, lpn_movement lm
            where l.i_id = lm.lpn_id;

            purge_table ( p_table_name  => 'LPN_MOVEMENT'
                        , p_column_name => 'LPN_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => pl_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pl_archive_flag
                        , p_purge_cd    => pl_purge_cd
                        , p_comp_id     => pl_company_id
                        , p_warehouse_id=> pl_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;            

            -- added a join to eliminate empty executions -- Upendra
            select /*+ leading(l) */ distinct i_id
            bulk collect into arrIDs
            from tmp_wm_arch_lpn_1 l, lpn_lock ll
            where l.i_id = ll.lpn_id;
            
            purge_table ( p_table_name  => 'LPN_LOCK'
                        , p_column_name => 'LPN_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => pl_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pl_archive_flag
                        , p_purge_cd    => pl_purge_cd
                        , p_comp_id     => pl_company_id
                        , p_warehouse_id=> pl_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;                

         -- Added to eliminate the FK violation while doing LPN purges -- need cr

            select invn_vari_hdr_id
            bulk collect into arrIDs_1
            from invn_vari_hdr, tmp_wm_arch_lpn_1 t
            where lpn_id = t.i_id;
            
            for currec in (
                            select 'INVN_VARI_SRL_NBR' TAB_NAME from dual
                            union all
                            select 'INVN_VARI_HDR' TAB_NAME from dual
                          )
            loop              
                 purge_table ( p_table_name  => currec.tab_name
                            , p_column_name => 'INVN_VARI_HDR_ID'
                            , p_arrids      => arrIDs_1
                            , p_commit_size => pl_commit_size
                            , p_start_time  => dstart_dttm
                            , p_archive_ind => pl_archive_flag
                            , p_purge_cd    => pl_purge_cd
                            , p_comp_id     => pl_company_id
                            , p_warehouse_id=> pl_whse_id
                            , p_rowcount    => i_rc
                            , p_error_msg   => v_error_msg); 
                            
                i_rows_purged := i_rows_purged + i_rc;
            end loop;             

         -- cr needed

            select lpn_catch_weight_id
            bulk collect into arrIDs_1
            from lpn_catch_weight l, lpn_detail d, tmp_wm_arch_lpn_1 t1
            where d.lpn_id      = t1.i_id
            and d.lpn_id        = l.lpn_id
            and l.lpn_detail_id = d.lpn_detail_id;
            
            purge_table ( p_table_name  => 'LPN_CATCH_WEIGHT'
                        , p_column_name => 'LPN_CATCH_WEIGHT_ID'
                        , p_arrids      => arrIDs_1
                        , p_commit_size => pl_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pl_archive_flag
                        , p_purge_cd    => pl_purge_cd
                        , p_comp_id     => pl_company_id
                        , p_warehouse_id=> pl_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;                

            select /*+ leading(l) */ distinct i_id
            bulk collect into arrIDs
            from tmp_wm_arch_lpn_1 l, lpn_notes ln
            where l.i_id = ln.lpn_id;
            
            purge_table ( p_table_name  => 'LPN_NOTES'
                        , p_column_name => 'LPN_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => pl_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pl_archive_flag
                        , p_purge_cd    => pl_purge_cd
                        , p_comp_id     => pl_company_id
                        , p_warehouse_id=> pl_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;            

            -- Added query here to avoid issues with the previous change - No rows returned for the LPN purge -- Upendra
            -- MACR00829473:  Order by to purge parent lpns last to avoid FK constraint violations.
            select i_id
            bulk collect into arrIDs
            from tmp_wm_arch_lpn_1, lpn
            where lpn_id=i_id
            order by decode(parent_lpn_id||split_lpn_id,null,2,1);
            
            for currec in (
                            select 'LPN_DETAIL_ATTRIBUTE' TAB_NAME from dual
                            union all
                            select 'LPN_DETAIL' TAB_NAME from dual
                            union all
                            select 'LPN_ATTRIBUTE' TAB_NAME from dual  -- MACR00690847
                            union all
                            select 'LPN' TAB_NAME from dual
                           )
            loop              
                 purge_table ( p_table_name  => currec.tab_name
                            , p_column_name => 'LPN_ID'
                            , p_arrids      => arrIDs
                            , p_commit_size => pl_commit_size
                            , p_start_time  => dstart_dttm
                            , p_archive_ind => pl_archive_flag
                            , p_purge_cd    => pl_purge_cd
                            , p_comp_id     => pl_company_id
                            , p_warehouse_id=> pl_whse_id
                            , p_rowcount    => i_rc
                            , p_error_msg   => v_error_msg); 
                            
                i_rows_purged := i_rows_purged + i_rc;
            end loop;             

          pl_rows_purged := i_rows_purged;
          
          msg_log_insert('END UI_PURGE() ----> purge_lpns()');

    EXCEPTION
        WHEN OTHERS THEN
            v_msg := v_msg || ' ' || substr(sqlerrm,1,250);
            msg_log_insert(p_msg_parms => 'purge_lpns(): Exception! -> ' || v_msg);
            pl_rows_purged := i_rows_purged;

            raise e_arch_purge_failed;
    END;

    -- adding pruning agg orders procedure to base archive-purge package    #CR MACR00865613
    -- called by exec_purge
    PROCEDURE purge_agg_orders(
                         p_company_id       IN purge_config.cd_master_id%type
                        ,p_whse_id          IN purge_config.whse_master_id%type
                        ,p_commit_size      IN purge_config.nbr_rows_to_commit%type
                        ,p_days             IN purge_config.nbr_days_old%type
                        ,p_purge_cd         IN purge_config.purge_code%type
                        ,p_archive_flag     IN purge_config.archive_flag%type
                        ,p_child_comp_flag  IN purge_config.include_child_company_flag%type
                        ,p_start_time       IN timestamp
                        ,p_rows_purged      OUT pls_integer
                        )
    IS
        dstart_dttm             timestamp(6);
        v_where_clause          varchar2(4000) default null;
        i_rc                    pls_integer default 0;
        i_rows_purged           pls_integer default 0;
        v_msg                   varchar2(250);
        v_error_msg             purge_hist.message%type;

    BEGIN
         --- copy agg lines to perpetual agg orders
        delete from tmp_wm_id_4;
      insert into tmp_wm_id_4(i_id1, i_id2)
        select oli.order_id, oli.line_item_id
        from order_line_item oli
      join orders o on (o.order_id = oli.order_id and o.is_original_order = 0
          and ( o.o_facility_id = p_whse_id or o.o_facility_id is null))
        where oli.last_updated_dttm < p_start_time - p_days
            and oli.do_dtl_status >= 190
            and not exists
            (
                select 1
                from order_line_item orig_oli
                where orig_oli.reference_order_id = oli.order_id
                    and orig_oli.reference_line_item_id = oli.line_item_id
                    and orig_oli.do_dtl_status < 190
            )
            and not exists
            (
                select 1
                from lpn_detail ld 
                join lpn l on l.lpn_id = ld.lpn_id
                where ld.distribution_order_dtl_id = oli.line_item_id
				and lpn_status <> 99
            );

        if sql%rowcount > 0
        then
            msg_log_insert('BEGIN UI_PURGE() ----> purge_agg_orders()');
            dstart_dttm := systimestamp;
            
            -- lps have their distribution_order_dtl_id overwritten after they are
            -- invoiced so we do not have to nullify anything there
            -- remove vas_pkt data
            v_where_clause:=' WHERE exists (
                                        select 1
                                        from tmp_wm_id_4 
                                        where i_id1 = t.order_id and i_id2 = t.line_item_id)';
            purge_table_where ( p_table_name  => 'vas_pkt'
                              , p_where_clause=> v_where_clause
                              , p_commit_size => p_commit_size
                              , p_start_time  => dstart_dttm
                              , p_archive_ind => p_archive_flag
                              , p_purge_cd    => p_purge_cd
                              , p_comp_id     => p_company_id
                              , p_warehouse_id=> p_whse_id
                              , p_rowcount    => i_rc
                              , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;

            v_where_clause:=' WHERE exists (
                                        select 1
                                        from tmp_wm_id_4 
                                        where i_id1 = t.reference_order_id and i_id2 = t.reference_line_item_id)';
            purge_table_where ( p_table_name  => 'pkt_dtl'
                              , p_where_clause=> v_where_clause
                              , p_commit_size => p_commit_size
                              , p_start_time  => dstart_dttm
                              , p_archive_ind => p_archive_flag
                              , p_purge_cd    => p_purge_cd
                              , p_comp_id     => p_company_id
                              , p_warehouse_id=> p_whse_id
                              , p_rowcount    => i_rc
                              , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;
        
            -- remove osli across all shpmts; all associated shpmts should be closed            
            v_where_clause:=' WHERE exists (
                                        select 1
                                        from tmp_wm_id_4 
                                        where i_id1 = t.order_id and i_id2 = t.line_item_id)';
            purge_table_where ( p_table_name  => 'order_split_line_item'
                              , p_where_clause=> v_where_clause
                              , p_commit_size => p_commit_size
                              , p_start_time  => dstart_dttm
                              , p_archive_ind => p_archive_flag
                              , p_purge_cd    => p_purge_cd
                              , p_comp_id     => p_company_id
                              , p_warehouse_id=> p_whse_id
                              , p_rowcount    => i_rc
                              , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;
            
            -- remove olis records
            v_where_clause:=' WHERE exists (
                                        select 1
                                        from tmp_wm_id_4 
                                          where i_id1 = t.order_id and i_id2 = t.line_item_id)';
            purge_table_where ( p_table_name  => 'order_line_item_size'
                              , p_where_clause=> v_where_clause
                              , p_commit_size => p_commit_size
                              , p_start_time  => dstart_dttm
                              , p_archive_ind => p_archive_flag
                              , p_purge_cd    => p_purge_cd
                              , p_comp_id     => p_company_id
                              , p_warehouse_id=> p_whse_id
                              , p_rowcount    => i_rc
                              , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;    
            
            -- let orig oli point to deleted agg oli for history purposes; no fk    
            -- let DO_LINE_WMPROCESSINFO Records -- WM-39564    
            v_where_clause:=' WHERE exists (
                                        select 1
                                        from tmp_wm_id_4 
                                        where i_id1 = t.order_id and i_id2 = t.line_item_id)';
            purge_table_where ( p_table_name  => 'DO_LINE_WMPROCESSINFO'
                              , p_where_clause=> v_where_clause
                              , p_commit_size => p_commit_size
                              , p_start_time  => dstart_dttm
                              , p_archive_ind => p_archive_flag
                              , p_purge_cd    => p_purge_cd
                              , p_comp_id     => p_company_id
                              , p_warehouse_id=> p_whse_id
                              , p_rowcount    => i_rc
                              , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;   
            
            --WM-54393 added order_split_size
            v_where_clause:=' WHERE exists (
                                        select 1
                                        from tmp_wm_id_4 
                                        where i_id1 = t.order_id and i_id2 = t.line_item_id)';
 
            purge_table_where ( p_table_name  => 'order_split_size'
                              , p_where_clause=> v_where_clause
                              , p_commit_size => p_commit_size
                              , p_start_time  => dstart_dttm
                              , p_archive_ind => p_archive_flag
                              , p_purge_cd    => p_purge_cd
                              , p_comp_id     => p_company_id
                              , p_warehouse_id=> p_whse_id
                              , p_rowcount    => i_rc
                              , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;   
            
            --WM-60080 added SRL_NBR_TRACK
            v_where_clause:=' WHERE exists (
                                        select 1
                                        from tmp_wm_id_4 
                                        where i_id1 = t.DISTRIBUTION_ORDER and i_id2 = t.DISTRIBUTION_ORDER_LINE_ID)';
 
            purge_table_where ( p_table_name  => 'SRL_NBR_TRACK'
                              , p_where_clause=> v_where_clause
                              , p_commit_size => p_commit_size
                              , p_start_time  => dstart_dttm
                              , p_archive_ind => p_archive_flag
                              , p_purge_cd    => p_purge_cd
                              , p_comp_id     => p_company_id
                              , p_warehouse_id=> p_whse_id
                              , p_rowcount    => i_rc
                              , p_error_msg   => v_error_msg);               
            i_rows_purged := i_rows_purged + i_rc;   
		
            -- let orig oli point to deleted agg oli for history purposes; no fk    
            v_where_clause:=' WHERE exists (
                                        select 1
                                        from tmp_wm_id_4 
                                        where i_id1 = t.order_id and i_id2 = t.order_line_id)';
            purge_table_where ( p_table_name  => 'ORDER_REASON_CODE'
                              , p_where_clause=> v_where_clause
                              , p_commit_size => p_commit_size
                              , p_start_time  => dstart_dttm
                              , p_archive_ind => p_archive_flag
                              , p_purge_cd    => p_purge_cd
                              , p_comp_id     => p_company_id
                              , p_warehouse_id=> p_whse_id
                              , p_rowcount    => i_rc
                              , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;
			
			v_where_clause:=' WHERE exists (
                                        select 1
                                        from tmp_wm_id_4 
                                        where i_id1 = t.order_id and i_id2 = t.line_item_id)';
           
             purge_table_where ( p_table_name  => 'order_line_item'
                              , p_where_clause=> v_where_clause
                              , p_commit_size => p_commit_size
                              , p_start_time  => dstart_dttm
                              , p_archive_ind => p_archive_flag
                              , p_purge_cd    => p_purge_cd
                              , p_comp_id     => p_company_id
                              , p_warehouse_id=> p_whse_id
                              , p_rowcount    => i_rc
                              , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;        
        end if;
        p_rows_purged := i_rows_purged;   
    EXCEPTION
        WHEN OTHERS THEN
            v_msg := v_msg || ' ' || substr(sqlerrm,1,250);
            msg_log_insert(p_msg_parms => 'purge_agg_orders(): Exception! -> ' || v_msg);
            p_rows_purged := i_rows_purged;

            raise e_arch_purge_failed;
    END;    

    -- called by exec_purge
    PROCEDURE purge_asns(
                         pa_arr_asn_ids      IN tIDs
                        ,pa_company_id       IN purge_config.cd_master_id%type
                        ,pa_whse_id          IN purge_config.whse_master_id%type
                        ,pa_commit_size      IN purge_config.nbr_rows_to_commit%type
                        ,pa_days             IN purge_config.nbr_days_old%type
                        ,pa_purge_cd         IN purge_config.purge_code%type
                        ,pa_archive_flag     IN purge_config.archive_flag%type
                        ,pa_child_comp_flag  IN purge_config.include_child_company_flag%type
                        ,pa_rows_purged      OUT pls_integer
                        )
    IS
        dstart_dttm     timestamp(6);
        i_rc            pls_integer default 0;
        i_rows_purged   pls_integer default 0;
        v_msg           varchar2(250);
        v_error_msg     purge_hist.message%type;

    BEGIN
        msg_log_insert('BEGIN UI_PURGE() ----> purge_asns()');

        dstart_dttm := systimestamp;
        delete from tmp_wm_arch_asn_2;

        -- select the asn_ids to purge
        forall i in 1..pa_arr_asn_ids.count
            insert into tmp_wm_arch_asn_2 (i_id, v_id)
            select asn_id, tc_asn_id
            from asn
            where asn_id = pa_arr_asn_ids(i)
			and NOT EXISTS (select 1 from srl_nbr_track s where s.asn = asn.asn_id and s.stat_code < 90);

            -- MACR00391268: Removed if pa_arr_asn_ids.count > 0 test
            
            -- begin asn purge process
            select distinct immd_needs_id
            bulk collect into arrIDs
            from immd_needs, tmp_wm_arch_asn_2
            where tc_asn_id = v_id;
            
            purge_table ( p_table_name  => 'IMMD_NEEDS'
                        , p_column_name => 'IMMD_NEEDS_ID'
                        , p_arrids      => arrIDs 
                        , p_commit_size => pa_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pa_archive_flag
                        , p_purge_cd    => pa_purge_cd
                        , p_comp_id     => pa_company_id
                        , p_warehouse_id=> pa_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;    
            
        
            select distinct srl_nbr_trk_id
            bulk collect into arrIDs
            from srl_nbr_track s, tmp_wm_arch_asn_2 t
            where s.asn = t.i_id
            and s.stat_code >= 90;  
            
            for currec in (
                            select 'FROZN_INVN_SRL_NBR' TAB_NAME from dual
                            union all
                            select 'MINOR_SRL_TRACK' TAB_NAME from dual
                            union all
                            select 'SRL_NBR_TRACK' TAB_NAME from dual
                          )
            loop              
                 purge_table ( p_table_name  => currec.tab_name
                            , p_column_name => 'SRL_NBR_TRK_ID'
                            , p_arrids      => arrIDs
                            , p_commit_size => pa_commit_size
                            , p_start_time  => dstart_dttm
                            , p_archive_ind => pa_archive_flag
                            , p_purge_cd    => pa_purge_cd
                            , p_comp_id     => pa_company_id
                            , p_warehouse_id=> pa_whse_id
                            , p_rowcount    => i_rc
                            , p_error_msg   => v_error_msg); 
                            
                i_rows_purged := i_rows_purged + i_rc;
            end loop;             

            -- WM-71477
            if pa_purge_cd = '32'
            then
               select distinct ada.asn_detail_id
               bulk collect into arrIDs
               from asn_detail_attribute ada, TMP_WM_ID_4 t
               where ada.asn_detail_id = t.i_id2;
            else
               select distinct ada.asn_detail_id
               bulk collect into arrIDs
               from asn_detail_attribute ada, tmp_wm_arch_asn_2 t, asn_detail ad
               where ada.asn_id = t.i_id 
               and ada.asn_id      = ad.asn_id
               and ada.asn_detail_id = ad.asn_detail_id;
            end if;                    

            purge_table ( p_table_name  => 'ASN_DETAIL_ATTRIBUTE'
                        , p_column_name => 'asn_detail_id'
                        , p_arrids      => arrIDs
                        , p_commit_size => pa_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pa_archive_flag
                        , p_purge_cd    => pa_purge_cd
                        , p_comp_id     => pa_company_id
                        , p_warehouse_id=> pa_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;                

            -- asn attribute (purge by asn_id)
            select distinct asn_id
            bulk collect into arrIDs
            from asn_attribute, tmp_wm_arch_asn_2
            where asn_id = i_id;
            
            purge_table ( p_table_name  => 'ASN_ATTRIBUTE'
                         , p_column_name => 'ASN_ID'
                         , p_arrids      => arrIDs
                         , p_commit_size => pa_commit_size
                         , p_start_time  => dstart_dttm
                         , p_archive_ind => pa_archive_flag
                         , p_purge_cd    => pa_purge_cd
                         , p_comp_id     => pa_company_id
                         , p_warehouse_id=> pa_whse_id
                         , p_rowcount    => i_rc
                         , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;                

            -- asn note (purge by asn_id)
            select distinct asn_id
            bulk collect into arrIDs
            from asn_note, tmp_wm_arch_asn_2
            where asn_id = i_id;
            
            purge_table ( p_table_name  => 'ASN_NOTE'
                         , p_column_name => 'ASN_ID'
                         , p_arrids      => arrIDs
                         , p_commit_size => pa_commit_size
                         , p_start_time  => dstart_dttm
                         , p_archive_ind => pa_archive_flag
                         , p_purge_cd    => pa_purge_cd
                         , p_comp_id     => pa_company_id
                         , p_warehouse_id=> pa_whse_id
                         , p_rowcount    => i_rc
                         , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;            

             -- BEGIN MACR00530507 (purge by asn_id)
             select distinct asn_id
             bulk collect into arrIDs
             from asn_seal_number, tmp_wm_arch_asn_2
             where asn_id = i_id;

             purge_table ( p_table_name  => 'ASN_SEAL_NUMBER'
                         , p_column_name => 'ASN_ID'
                         , p_arrids      => arrIDs
                         , p_commit_size => pa_commit_size
                         , p_start_time  => dstart_dttm
                         , p_archive_ind => pa_archive_flag
                         , p_purge_cd    => pa_purge_cd
                         , p_comp_id     => pa_company_id
                         , p_warehouse_id=> pa_whse_id
                         , p_rowcount    => i_rc
                         , p_error_msg   => v_error_msg);
             i_rows_purged := i_rows_purged + i_rc;
             -- END MACR00530507

            --WM-71477
            if pa_purge_cd = '32'
            then
               select distinct t.i_id2 asn_detail_id
               bulk collect into arrIDs
               from TMP_WM_ID_4 t;
            else
               select distinct asn_detail_id
               bulk collect into arrIDs
               from asn_detail, tmp_wm_arch_asn_2
               where asn_id = i_id;
            end if;               

            purge_table ( p_table_name  => 'ASN_DETAIL'
                        , p_column_name => 'asn_detail_id'
                        , p_arrids      => arrIDs
                        , p_commit_size => pa_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => pa_archive_flag
                        , p_purge_cd    => pa_purge_cd
                        , p_comp_id     => pa_company_id
                        , p_warehouse_id=> pa_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;

            -- asn
            select i_id
            bulk collect into arrIDs
            from tmp_wm_arch_asn_2;
            
            for currec in (
                            select 'ASN_UNIT_DETAIL' TAB_NAME from dual
                            union all
                            select 'DOCK_DOOR_REF' TAB_NAME from dual
                            union all
                            select 'TRAILER_CONTENTS' TAB_NAME from dual
                            union all
                            select 'ASN' TAB_NAME from dual
                          )
            loop              
                 purge_table ( p_table_name  => currec.tab_name
                            , p_column_name => 'ASN_ID'
                            , p_arrids      => arrIDs
                            , p_commit_size => pa_commit_size
                            , p_start_time  => dstart_dttm
                            , p_archive_ind => pa_archive_flag
                            , p_purge_cd    => pa_purge_cd
                            , p_comp_id     => pa_company_id
                            , p_warehouse_id=> pa_whse_id
                            , p_rowcount    => i_rc
                            , p_error_msg   => v_error_msg); 
                            
                i_rows_purged := i_rows_purged + i_rc;
            end loop;              
            
            pa_rows_purged := i_rows_purged;

            msg_log_insert('END UI_PURGE() ----> purge_asns()');

    EXCEPTION
        WHEN OTHERS THEN
            v_msg := substr(sqlerrm,1,250);
            msg_log_insert(p_msg_parms => 'purge_asns(): Exception! -> ' || v_msg);
            pa_rows_purged := i_rows_purged;

            raise e_arch_purge_failed;
    END;


    -- called by exec_purge
    PROCEDURE purge_orders(po_company_id       IN purge_config.cd_master_id%type
                        ,po_whse_id          IN purge_config.whse_master_id%type
                        ,po_commit_size      IN purge_config.nbr_rows_to_commit%type
                        ,po_days             IN purge_config.nbr_days_old%type
                        ,po_purge_cd         IN purge_config.purge_code%type
                        ,po_archive_flag     IN purge_config.archive_flag%type
                        ,po_child_comp_flag  IN purge_config.include_child_company_flag%type
                        ,po_start_time       IN timestamp
                        ,po_rows_purged      OUT pls_integer
                        )
    IS
        dstart_dttm     timestamp(6);
        i_rc            pls_integer default 0;
        i_rows_purged   pls_integer default 0;
        v_msg           varchar2(250);
        v_error_msg     purge_hist.message%type;

    BEGIN
        msg_log_insert('BEGIN UI_PURGE() ----> purge_orders()');
        dstart_dttm := systimestamp;

         insert into gtmp_purge_orders (col_name, i_id, vc_id)
         with purgeable_tasks as
           (
             select task_hdr_id, task_dtl_id, task_id
             from task_dtl
             where tc_order_id in (select V_ID
                                   from TMP_WM_ID_1)
			 and mod_date_time <= dstart_dttm - po_days -- WM-98278					   
           )
         select distinct 'tc_order_id', null, V_ID
         from TMP_WM_ID_1
         union all
         select distinct 'task_dtl_id', o2.task_dtl_id, null
         from task_hdr o1, purgeable_tasks o2
         where o1.task_hdr_id = o2.task_hdr_id
         and (exists (  -- select the purge-able task_ids
                     select 1
                     from purgeable_tasks i1
                     where o1.orig_task_id = i1.task_id  -- orig_task_id of purge candidate is a purgeable task_id
                     ) or o1.orig_task_id is null ) --WM-45572
             union all
         select distinct 'task_hdr_id', o2.task_hdr_id, null
         from task_hdr o1, purgeable_tasks o2
         where o1.task_hdr_id = o2.task_hdr_id
         and (exists (  -- select the purge-able task_ids
                     select 1
                     from purgeable_tasks i1
                     where o1.orig_task_id = i1.task_id  -- orig_task_id of purge candidate is a purgeable task_id
                     ) or o1.orig_task_id is null ) --WM-45572
             union all
       select 'smartlabel', smartlabel_carton_hdr_hist_id, smartlabel_brcd
         from smartlabel_carton_hdr_hist
         where pkt_ctrl_nbr in (select V_ID
                                   from TMP_WM_ID_1   
           );                
 
 --WM-68318 
        BEGIN
		DBMS_STATS.gather_table_stats(USER,'GTMP_PURGE_ORDERS');
		EXCEPTION
		WHEN OTHERS	
		THEN 
		NULL;
		END;     

            select vc_id bulk collect into varrIDs
          from gtmp_purge_orders where col_name = 'smartlabel';
            
            purge_table ( p_table_name  => 'SMARTLABEL_SKU_HIST'
                        , p_column_name => 'SMARTLABEL_BRCD'
                        , p_varrids      => varrIDs 
                        , p_commit_size => po_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => po_archive_flag
                        , p_purge_cd    => po_purge_cd
                        , p_comp_id     => po_company_id
                        , p_warehouse_id=> po_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;    

            select d.smartlabel_arn_event_hdr_id
            bulk collect into varrIDs
            from SMARTLABEL_ARN_EVENT_DTL d
            WHERE EXISTS (
                            SELECT 1
                            FROM smartlabel_arn_event_hdr h ,gtmp_purge_orders g
                            WHERE g.col_name = 'smartlabel_brcd'
                            AND h.smartlabel_brcd = g.vc_id
                            AND h.smartlabel_arn_event_hdr_id = d.smartlabel_arn_event_hdr_id
                            );    
            
            purge_table ( p_table_name  => 'SMARTLABEL_ARN_EVENT_DTL'
                        , p_column_name => 'smartlabel_arn_event_hdr_id'
                        , p_varrids      => varrIDs 
                        , p_commit_size => po_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => po_archive_flag
                        , p_purge_cd    => po_purge_cd
                        , p_comp_id     => po_company_id
                        , p_warehouse_id=> po_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;    
    
            select vc_id bulk collect into varrIDs
          from gtmp_purge_orders where col_name = 'smartlabel';
            
            purge_table ( p_table_name  => 'SMARTLABEL_ARN_EVENT_HDR'
                        , p_column_name => 'SMARTLABEL_BRCD'
                        , p_varrids      => varrIDs 
                        , p_commit_size => po_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => po_archive_flag
                        , p_purge_cd    => po_purge_cd
                        , p_comp_id     => po_company_id
                        , p_warehouse_id=> po_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;                

            select i_id bulk collect into arrIDs
          from gtmp_purge_orders where col_name = 'smartlabel';

            purge_table ( p_table_name  => 'SMARTLABEL_CARTON_HDR_HIST'
                        , p_column_name => 'SMARTLABEL_CARTON_HDR_HIST_ID'
                        , p_arrids      => arrIDs 
                        , p_commit_size => po_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => po_archive_flag
                        , p_purge_cd    => po_purge_cd
                        , p_comp_id     => po_company_id
                        , p_warehouse_id=> po_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;    
            
            -- purge records from prod_trkg_tran with the same task_hdr_ids
            select distinct ptt.prod_trkg_tran_id bulk collect into arrIDs
            from prod_trkg_tran ptt , gtmp_purge_orders gpo ,task_hdr th 
            where  ptt.task_id = th.task_id --WM-45572 changed to task_id
            and th.task_hdr_id = gpo.i_id 
            and gpo.col_name = 'task_hdr_id';

            -- purge records from prod_trkg_tran with the same task_hdr_ids
            purge_table ( p_table_name  => 'PROD_TRKG_TRAN'
                        , p_column_name => 'PROD_TRKG_TRAN_ID'
                        , p_arrids      => arrIDs 
                        , p_commit_size => po_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => po_archive_flag
                        , p_purge_cd    => po_purge_cd
                        , p_comp_id     => po_company_id
                        , p_warehouse_id=> po_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);          
            i_rows_purged := i_rows_purged + i_rc;
            
            -- task_dtl
            select distinct i_id bulk collect into arrIDs
            from gtmp_purge_orders where col_name = 'task_dtl_id';

            purge_table ( p_table_name  => 'TASK_DTL'
                        , p_column_name => 'TASK_DTL_ID'
                        , p_arrids      => arrIDs 
                        , p_commit_size => po_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => po_archive_flag
                        , p_purge_cd    => po_purge_cd
                        , p_comp_id     => po_company_id
                        , p_warehouse_id=> po_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;

          /* Select only those task hdr ids that do not have task dtl ids */

          select distinct i_id bulk collect into arrIDs
          from gtmp_purge_orders o where o.col_name = 'task_hdr_id'
          and not exists ( select 1
                        from task_dtl d
                        where d.task_hdr_id = o.i_id
                        );

            -- MACR00440675
            set_taskhdr_origtaskid_to_null(arrIDs);
            purge_table ( p_table_name  => 'TASK_HDR'
                        , p_column_name => 'TASK_HDR_ID'
                        , p_arrids      => arrIDs 
                        , p_commit_size => po_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => po_archive_flag
                        , p_purge_cd    => po_purge_cd
                        , p_comp_id     => po_company_id
                        , p_warehouse_id=> po_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;

            -- allocation inventory detail
            select /*+ index (gtmp_purge_orders GIDX_TPO_VC_ID)
                       index (alloc_invn_dtl AID_TCORDIDSTAT_IDX) */ alloc_invn_dtl_id
            bulk collect into arrIDs
            from gtmp_purge_orders, alloc_invn_dtl
            where tc_order_id = vc_id
            and col_name = 'tc_order_id';  -- tc_order_id

            purge_table ( p_table_name  => 'ALLOC_INVN_DTL'
                        , p_column_name => 'ALLOC_INVN_DTL_ID'
                        , p_arrids      => arrIDs 
                        , p_commit_size => po_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => po_archive_flag
                        , p_purge_cd    => po_purge_cd
                        , p_comp_id     => po_company_id
                        , p_warehouse_id=> po_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;

            select snt.srl_nbr_trk_id
            bulk collect into arrIDs
            from srl_nbr_track snt
            where snt.stat_code >= 90  
              and exists
            (
                select 1
                from gtmp_purge_orders t
                where t.i_id = snt.distribution_order
                    and t.col_name = 'order_id'
            );

            for currec in (
                            select 'MINOR_SRL_TRACK' TAB_NAME from dual
                            union all
                            select 'SRL_NBR_TRACK' TAB_NAME from dual
                          )
            loop              
                 purge_table ( p_table_name  => currec.tab_name
                            , p_column_name => 'SRL_NBR_TRK_ID'
                            , p_arrids      => arrIDs
                            , p_commit_size => po_commit_size
                            , p_start_time  => dstart_dttm
                            , p_archive_ind => po_archive_flag
                            , p_purge_cd    => po_purge_cd
                            , p_comp_id     => po_company_id
                            , p_warehouse_id=> po_whse_id
                            , p_rowcount    => i_rc
                            , p_error_msg   => v_error_msg); 
                            
                i_rows_purged := i_rows_purged + i_rc;
            end loop; 

            -- stop action order
            select stop_action_order_id bulk collect into arrIDs
            from stop_action_order, gtmp_purge_orders
            where order_id = i_id
            and col_name = 'order_id';

            purge_table ( p_table_name  => 'STOP_ACTION_ORDER'
                        , p_column_name => 'STOP_ACTION_ORDER_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => po_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => po_archive_flag
                        , p_purge_cd    => po_purge_cd
                        , p_comp_id     => po_company_id
                        , p_warehouse_id=> po_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;

            -- MACR00489838
            -- Integrity constraint violation in delete from ORDER_LINE_ITEM.

            -- order split line item order
            select order_split_line_item_id bulk collect into arrIDs
            from order_split_line_item, gtmp_purge_orders
            where order_id = i_id
            and col_name = 'order_id';
            
            purge_table ( p_table_name  => 'ORDER_SPLIT_LINE_ITEM'
                        , p_column_name => 'ORDER_SPLIT_LINE_ITEM_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => po_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => po_archive_flag
                        , p_purge_cd    => po_purge_cd
                        , p_comp_id     => po_company_id
                        , p_warehouse_id=> po_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;

            select order_split_size_id bulk collect into arrIDs
            from order_split_size, gtmp_purge_orders
            where order_id = i_id
            and col_name = 'order_id';
            
            purge_table ( p_table_name  => 'ORDER_SPLIT_SIZE'
                        , p_column_name => 'ORDER_SPLIT_SIZE_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => po_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => po_archive_flag
                        , p_purge_cd    => po_purge_cd
                        , p_comp_id     => po_company_id
                        , p_warehouse_id=> po_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;                
            -- END MACR00489838

            -- get the order_ids to purge
            select i_id bulk collect into arrIDs
            from gtmp_purge_orders where col_name = 'order_id';
            
            for currec in (
                            select 'ORDER_MOVEMENT' TAB_NAME from dual
                            union all
                            select 'ORDER_LINE_ITEM_SIZE' TAB_NAME from dual
                            union all
                            select 'ORDER_LINE_ITEM_ATTRIBUTE' TAB_NAME from dual
                            union all
                            select 'ORDER_ATTRIBUTE' TAB_NAME from dual
                            union all
                            select 'ORDER_SRL_NBR' TAB_NAME from dual
                            union all
                            select 'ORDER_BASELINE_COST' TAB_NAME from dual
							union all
                            select 'ORDER_SCHED_ACTION' TAB_NAME from dual
                          )
            loop              
                 purge_table ( p_table_name  => currec.tab_name
                            , p_column_name => 'ORDER_ID'
                            , p_arrids      => arrIDs
                            , p_commit_size => po_commit_size
                            , p_start_time  => dstart_dttm
                            , p_archive_ind => po_archive_flag
                            , p_purge_cd    => po_purge_cd
                            , p_comp_id     => po_company_id
                            , p_warehouse_id=> po_whse_id
                            , p_rowcount    => i_rc
                            , p_error_msg   => v_error_msg); 
                            
                i_rows_purged := i_rows_purged + i_rc;
            end loop;             
             
            select cons_order_path_id bulk collect into arrIDs
            from cons_order_path, gtmp_purge_orders
            where order_id = i_id
            and col_name = 'order_id';
            
            purge_table ( p_table_name  => 'CONS_ORDER_PATH'
                        , p_column_name => 'CONS_ORDER_PATH_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => po_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => po_archive_flag
                        , p_purge_cd    => po_purge_cd
                        , p_comp_id     => po_company_id
                        , p_warehouse_id=> po_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;            

        -- END MACR00489838
           -- TE-2576 removing WS_ORDER_LPN,WS_ORDER_LINE_ITEM,WS_ORDER tables, as they are dropped in 2014.
            
            -- END MACR00489838

           -- get the tc_order_ids to purge
           --modified for  MACR00845322 - Shipment  porting 
           --modified for  MACR00851932 
         
         delete from tmp_wm_id_1;

            insert into tmp_wm_id_1(i_id,v_id)                          
            select h.pkt_hdr_id,h.pkt_ctrl_nbr  from pkt_hdr h ,gtmp_purge_orders 
            where h.original_tc_order_id = vc_id and col_name = 'tc_order_id' 
            union all
            select h.pkt_hdr_id,h.pkt_ctrl_nbr  from pkt_hdr h 
            where  h.original_tc_order_id is null  
            and coalesce(h.mod_date_time, h.create_date_time) < po_start_time - po_days;

         select  v_id bulk collect into varrIDs from tmp_wm_id_1;        
      
            -- MACR00773167
            purge_table ( p_table_name  => 'PKT_CONSOL_ATTR_SUMM'
                        , p_column_name => 'PKT_CTRL_NBR'
                        , p_varrids      => varrIDs
                        , p_commit_size => po_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => po_archive_flag
                        , p_purge_cd    => po_purge_cd
                        , p_comp_id     => po_company_id
                        , p_warehouse_id=> po_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_msg);
            i_rows_purged := i_rows_purged + i_rc;    

            -- get the order_ids to purge
            select i_id bulk collect into arrIDs
            from gtmp_purge_orders where col_name = 'order_id';
            
            for currec in (
                            select 'ORDER_WORK_TYPE' TAB_NAME from dual
                            union all
                            select 'ORDER_NOTE' TAB_NAME from dual                      
                          )
            loop              
                 purge_table ( p_table_name  => currec.tab_name
                            , p_column_name => 'ORDER_ID'
                            , p_arrids      => arrIDs
                            , p_commit_size => po_commit_size
                            , p_start_time  => dstart_dttm
                            , p_archive_ind => po_archive_flag
                            , p_purge_cd    => po_purge_cd
                            , p_comp_id     => po_company_id
                            , p_warehouse_id=> po_whse_id
                            , p_rowcount    => i_rc
                            , p_error_msg   => v_error_msg); 
                            
                i_rows_purged := i_rows_purged + i_rc;
            end loop;              
          
            -- removing the redundant query as the same varrIDs is calculated above prior to PKT_CONSOL_ATTR_SUMM       
            purge_table ( p_table_name  => 'VAS_PKT'
                        , p_column_name => 'PKT_CTRL_NBR'
                        , p_varrids      => varrIDs
                        , p_commit_size => po_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => po_archive_flag
                        , p_purge_cd    => po_purge_cd
                        , p_comp_id     => po_company_id
                        , p_warehouse_id=> po_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_msg);
            i_rows_purged := i_rows_purged + i_rc;               
            
            --modified for  MACR00845322 - Shipment  porting 

		select distinct i_id
        bulk collect into arrIDs
        from tmp_wm_id_1 t
        where exists ( select 1 from pkt_dtl d
                         where t.i_id = d.pkt_hdr_id
                           and t.v_id = d.pkt_ctrl_nbr);

          
        purge_table ( p_table_name  => 'pkt_dtl'
                    , p_column_name => 'pkt_hdr_id'
                            , p_arrids      => arrIDs
                            , p_commit_size => po_commit_size
                            , p_start_time  => dstart_dttm
                            , p_archive_ind => po_archive_flag
                            , p_purge_cd    => po_purge_cd
                            , p_comp_id     => po_company_id
                            , p_warehouse_id=> po_whse_id
                            , p_rowcount    => i_rc
                    , p_error_msg   => v_msg);
            i_rows_purged := i_rows_purged + i_rc;	
                            
        select distinct i_id
        bulk collect into arrIDs        
        from tmp_wm_id_1 t;
     
        purge_table ( p_table_name  => 'pkt_hdr'
                    , p_column_name => 'pkt_hdr_id'
                    , p_arrids      => arrIDs
                    , p_commit_size => po_commit_size
                    , p_start_time  => dstart_dttm
                    , p_archive_ind => po_archive_flag
                    , p_purge_cd    => po_purge_cd
                    , p_comp_id     => po_company_id
                    , p_warehouse_id=> po_whse_id
                    , p_rowcount    => i_rc
                    , p_error_msg   => v_msg);
                i_rows_purged := i_rows_purged + i_rc;

            --   Upendra begine changes for do_line_wmprocessinfo
            select oli.line_item_id bulk collect into arrIDs
            from gtmp_purge_orders tmp, order_line_item oli
            where tmp.col_name = 'order_id'
            and  tmp.i_id = oli.order_id;

            purge_table ( p_table_name  => 'DO_LINE_WMPROCESSINFO'
                        , p_column_name => 'LINE_ITEM_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => po_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => po_archive_flag
                        , p_purge_cd    => po_purge_cd
                        , p_comp_id     => po_company_id
                        , p_warehouse_id=> po_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;
            --   Upendra end changes for do_line_wmprocessinfo

            purge_table ( p_table_name  => 'PICKING_SHORT_ITEM'
                        , p_column_name => 'LINE_ITEM_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => po_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => po_archive_flag
                        , p_purge_cd    => po_purge_cd
                        , p_comp_id     => po_company_id
                        , p_warehouse_id=> po_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;
            
            --Upendra begin changes for do_wmprocessinfo
            select i_id bulk collect into arrIDs
            from gtmp_purge_orders tmp, do_wmprocessinfo dowm
            where tmp.col_name = 'order_id'
            and  tmp.i_id = dowm.order_id;

            purge_table ( p_table_name  => 'DO_WMPROCESSINFO'
                        , p_column_name => 'ORDER_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => po_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => po_archive_flag
                        , p_purge_cd    => po_purge_cd
                        , p_comp_id     => po_company_id
                        , p_warehouse_id=> po_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;

            select i_id bulk collect into arrIDs
            from gtmp_purge_orders where col_name = 'order_id';
            
            for currec in (
                            select 'ORDER_ACCESSORIAL_OPTION_GRP' TAB_NAME from dual -- MACR00720452 adding the ORDER_ACCESSORIAL_OPTION_GRP table
                            union all                            
                            select 'DO_LINE_CLASSIFICATION_CODE' TAB_NAME from dual -- WM-45168
                            union all
                            select 'ORDER_SPLIT' TAB_NAME from dual    --MACR00489838
                            union all
                            select 'ORDER_REASON_CODE' TAB_NAME from dual
                            union all
                            select 'ORDER_LINE_ITEM' TAB_NAME from dual
                            union all
                            select 'ORDER_EVENT' TAB_NAME from dual  -- MACR00773167
                            union all                                              
                            select 'ORDER_REASON_CODE' TAB_NAME from dual
                            union all
                            select 'ORDERS_TEMPLATE_RUN' TAB_NAME from dual
                            union all
                            select 'ORDER_MASTER_ORDER' TAB_NAME from dual
                            union all
                            select 'ORDERS' TAB_NAME from dual
                           )
            loop              
                 purge_table ( p_table_name  => currec.tab_name
                            , p_column_name => 'ORDER_ID'
                            , p_arrids      => arrIDs
                            , p_commit_size => po_commit_size
                            , p_start_time  => dstart_dttm
                            , p_archive_ind => po_archive_flag
                            , p_purge_cd    => po_purge_cd
                            , p_comp_id     => po_company_id
                            , p_warehouse_id=> po_whse_id
                            , p_rowcount    => i_rc
                            , p_error_msg   => v_error_msg); 
                            
                i_rows_purged := i_rows_purged + i_rc;
            end loop;              

            po_rows_purged := i_rows_purged;

            execute immediate 'truncate table gtmp_purge_orders';

            msg_log_insert('END UI_PURGE() ----> purge_orders()');
    
    EXCEPTION
        WHEN OTHERS THEN
            v_msg := substr(sqlerrm,1,250);
            msg_log_insert(p_msg_parms => 'purge_orders(): Exception! -> ' || v_msg);
            po_rows_purged := i_rows_purged;

            raise e_arch_purge_failed;
    END;
	
		--WM01 def starts

--C_LOAD_DIAGRAM_SUMMARY

	PROCEDURE ARCH_C_LOAD_DIAGRAM_SUMMARY (
                             p_arrIDs       IN tIDs
                            ,p_commit_size  IN pls_integer
                            ,p_start_time   IN timestamp
                            ,p_archive_ind  IN char
                            ,p_purge_cd     IN varchar2
                            ,p_comp_id      IN pls_integer
                            ,p_warehouse_id IN pls_integer
                            ,p_rowcount     OUT pls_integer
                            )
    IS
        istart      pls_integer default 0;
        istop       pls_integer default 0;
        i_arc_rc    pls_integer default 0;
        i_del_rc    pls_integer default 0;
        v_msg       varchar2(250);

    BEGIN
        rec_MsgLog.r_start_dttm := p_start_time;
        rec_MsgLog.r_cd_master_id := p_comp_id;
        rec_MsgLog.r_purge_code := p_purge_cd;
        rec_MsgLog.r_whse_id := p_warehouse_id;
        rec_MsgLog.r_table_name := 'C_LOAD_DIAGRAM_SUMMARY';

		while istop < p_arrIDs.count loop
            -- get the index range to process
            get_forall_range (istart, istop, p_commit_size, p_arrIDs.count);
            if p_archive_ind in (c_arch_norm, c_arch_all) then
                -- archive the data
                for i in istart..istop loop
				DECLARE
					status NUMBER(3);
					tcShipmentId VARCHAR2(50);
				BEGIN
					select tc_shipment_id INTO tcShipmentId
						from shipment where
						shipment_id = p_arrIDs(i);

							INSERT INTO ARCH_C_LOAD_DIAGRAM_SUMMARY (
													LOAD_DIAGRAM_ID,
													ROUTE_ID,
													TC_SHIPMENT_ID,
													TRLR_TYPE,
													NUM_STOPS,
													TOTAL_CASES,
													TOTAL_WGT,
													TOTAL_VOL,
													ASSIGNED_CASES,
													OVERFLOW_CASES,
													EDIT_USER_ID,
													CREATED_DTTM,
													LAST_UPDATED_DTTM,
													USER_ID)
							select LOAD_DIAGRAM_ID,
													ROUTE_ID,
													TC_SHIPMENT_ID,
													TRLR_TYPE,
													NUM_STOPS,
													TOTAL_CASES,
													TOTAL_WGT,
													TOTAL_VOL,
													ASSIGNED_CASES,
													OVERFLOW_CASES,
													EDIT_USER_ID,
													CREATED_DTTM,
													LAST_UPDATED_DTTM,
													USER_ID from C_LOAD_DIAGRAM_SUMMARY where LOAD_DIAGRAM_ID = (select load_diagram_id from C_LOAD_DIAGRAM_SUMMARY
										where tc_shipment_id = tcShipmentId);
								-- increment archive count
							i_arc_rc := i_arc_rc + sql%rowcount;
				END;
                end loop;
            end if;  -- archive_ind
            -- delete the same
            forall i in istart..istop
                delete from C_LOAD_DIAGRAM_SUMMARY where LOAD_DIAGRAM_ID = (select load_diagram_id from C_LOAD_DIAGRAM_SUMMARY
										where tc_shipment_id = (select tc_shipment_id
							from shipment where
							shipment_id = p_arrIDs(i)));
                -- increment delete count
                i_del_rc := i_del_rc + sql%rowcount;
            if lower(c_debug_yn)='n' then
                commit;
            end if;
        end loop;
        --write_debug_output(p_msg_label=>'ARCH_C_LOAD_DIAGRAM_SUMMARY()', p_arrCompID => p_comp_id, p_rows_archived => i_arc_rc, p_rows_purged => i_del_rc);
        -- set new values and log the message
        rec_MsgLog.r_msg_id := c_msg_id_inform;
        rec_MsgLog.r_rows_archived := i_arc_rc;
        rec_MsgLog.r_rows_deleted := i_del_rc;
        rec_MsgLog.r_end_dttm := systimestamp;
        log_msg (p_rec_MsgLog => rec_MsgLog);
        -- return the number of deleted rows
        p_rowcount := i_del_rc;

    EXCEPTION
        WHEN OTHERS THEN
            v_msg := substr(sqlerrm,1,250);
            msg_log_insert('ARCH_C_LOAD_DIAGRAM_SUMMARY(): Exception! -> ' || v_msg);
            -- set new values and log the message
            rec_MsgLog.r_msg_id := c_msg_id_error;
            rec_MsgLog.r_rows_archived := i_arc_rc;
            rec_MsgLog.r_rows_deleted := i_del_rc;
            rec_MsgLog.r_end_dttm := systimestamp;
            log_msg (p_rec_MsgLog => rec_MsgLog);

            raise e_arch_purge_failed;
    END;


--C_LOAD_DIAGRAM_ITEM_DETAIL

	PROCEDURE ARCH_C_LOAD_DIAGRAM_ITEM_DTL (
                             p_arrIDs       IN tIDs
                            ,p_commit_size  IN pls_integer
                            ,p_start_time   IN timestamp
                            ,p_archive_ind  IN char
                            ,p_purge_cd     IN varchar2
                            ,p_comp_id      IN pls_integer
                            ,p_warehouse_id IN pls_integer
                            ,p_rowcount     OUT pls_integer
                            )
    IS
        istart      pls_integer default 0;
        istop       pls_integer default 0;
        i_arc_rc    pls_integer default 0;
        i_del_rc    pls_integer default 0;
        v_msg       varchar2(250);

    BEGIN
        rec_MsgLog.r_start_dttm := p_start_time;
        rec_MsgLog.r_cd_master_id := p_comp_id;
        rec_MsgLog.r_purge_code := p_purge_cd;
        rec_MsgLog.r_whse_id := p_warehouse_id;
        rec_MsgLog.r_table_name := 'C_LOAD_DIAGRAM_ITEM_DETAIL';

		while istop < p_arrIDs.count loop
            -- get the index range to process
            get_forall_range (istart, istop, p_commit_size, p_arrIDs.count);
            if p_archive_ind in (c_arch_norm, c_arch_all) then
                -- archive the data
                for i in istart..istop loop

					DECLARE
						status NUMBER(3);
						tcShipmentId VARCHAR2(50);
					BEGIN
						select tc_shipment_id INTO tcShipmentId
							from shipment where
							shipment_id = p_arrIDs(i);

								INSERT INTO ARCH_C_LOAD_DIAGRAM_ITEM_DTL (
														LOAD_DIAGRAM_ITEM_DETAIL_ID,
														LOAD_DIAGRAM_ID,
														TC_ORDER_ID,
														STOP_NUMBER,
														LINE_ITEM_ID,
														ITEM_ID,
														ITEM_NAME,
														LOAD_TYPE,
														STORAGE_TYPE,
														COMPARTMENT,
														TRAILER_POSITION,
														NUM_CASES,
														ALLOCATED_CASES,
														SHORTED_CASES,
														VOLUME,
														WEIGHT,
														CREATED_DTTM,
														LAST_UPDATED_DTTM,
														USER_ID,
														PRODUCT_TYPE,
														PARENT_ID)
								select LOAD_DIAGRAM_ITEM_DETAIL_ID,
														LOAD_DIAGRAM_ID,
														TC_ORDER_ID,
														STOP_NUMBER,
														LINE_ITEM_ID,
														ITEM_ID,
														ITEM_NAME,
														LOAD_TYPE,
														STORAGE_TYPE,
														COMPARTMENT,
														TRAILER_POSITION,
														NUM_CASES,
														ALLOCATED_CASES,
														SHORTED_CASES,
														VOLUME,
														WEIGHT,
														CREATED_DTTM,
														LAST_UPDATED_DTTM,
														USER_ID,
														PRODUCT_TYPE,
														PARENT_ID from C_LOAD_DIAGRAM_ITEM_DETAIL
									where LOAD_DIAGRAM_ID = (select load_diagram_id from C_LOAD_DIAGRAM_SUMMARY
										where tc_shipment_id = tcShipmentId);
									-- increment archive count
								i_arc_rc := i_arc_rc + sql%rowcount;
					END;
                end loop;
            end if;  -- archive_ind
            -- delete the same
            forall i in istart..istop
                delete from C_LOAD_DIAGRAM_ITEM_DETAIL
                where LOAD_DIAGRAM_ID = (select load_diagram_id from C_LOAD_DIAGRAM_SUMMARY
										where tc_shipment_id = (select tc_shipment_id
							from shipment where
							shipment_id = p_arrIDs(i)));

                -- increment delete count
                i_del_rc := i_del_rc + sql%rowcount;
            if lower(c_debug_yn)='n' then
                commit;
            end if;
        end loop;
        --write_debug_output(p_msg_label=>'ARCH_C_LOAD_DIAGRAM_ITEM_DTL()', p_arrCompID => p_comp_id, p_rows_archived => i_arc_rc, p_rows_purged => i_del_rc);
        -- set new values and log the message
        rec_MsgLog.r_msg_id := c_msg_id_inform;
        rec_MsgLog.r_rows_archived := i_arc_rc;
        rec_MsgLog.r_rows_deleted := i_del_rc;
        rec_MsgLog.r_end_dttm := systimestamp;
        log_msg (p_rec_MsgLog => rec_MsgLog);
        -- return the number of deleted rows
        p_rowcount := i_del_rc;

    EXCEPTION
        WHEN OTHERS THEN
            v_msg := substr(sqlerrm,1,250);
            msg_log_insert('ARCH_C_LOAD_DIAGRAM_ITEM_DTL(): Exception! -> ' || v_msg);
            -- set new values and log the message
            rec_MsgLog.r_msg_id := c_msg_id_error;
            rec_MsgLog.r_rows_archived := i_arc_rc;
            rec_MsgLog.r_rows_deleted := i_del_rc;
            rec_MsgLog.r_end_dttm := systimestamp;
            log_msg (p_rec_MsgLog => rec_MsgLog);

            raise e_arch_purge_failed;
    END;



--C_LOAD_DIAGRAM_POSN

	PROCEDURE ARCH_C_LOAD_DIAGRAM_POSN (
                             p_arrIDs       IN tIDs
                            ,p_commit_size  IN pls_integer
                            ,p_start_time   IN timestamp
                            ,p_archive_ind  IN char
                            ,p_purge_cd     IN varchar2
                            ,p_comp_id      IN pls_integer
                            ,p_warehouse_id IN pls_integer
                            ,p_rowcount     OUT pls_integer
                            )
    IS
        istart      pls_integer default 0;
        istop       pls_integer default 0;
        i_arc_rc    pls_integer default 0;
        i_del_rc    pls_integer default 0;
        v_msg       varchar2(250);

    BEGIN
        rec_MsgLog.r_start_dttm := p_start_time;
        rec_MsgLog.r_cd_master_id := p_comp_id;
        rec_MsgLog.r_purge_code := p_purge_cd;
        rec_MsgLog.r_whse_id := p_warehouse_id;
        rec_MsgLog.r_table_name := 'C_LOAD_DIAGRAM_POSN';

		while istop < p_arrIDs.count loop
            -- get the index range to process
            get_forall_range (istart, istop, p_commit_size, p_arrIDs.count);
            if p_archive_ind in (c_arch_norm, c_arch_all) then
                -- archive the data
                for i in istart..istop loop

					DECLARE
						status NUMBER(3);
						tcShipmentId VARCHAR2(50);
					BEGIN
						select tc_shipment_id INTO tcShipmentId
							from shipment where
							shipment_id = p_arrIDs(i);

								INSERT INTO ARCH_C_LOAD_DIAGRAM_POSN (
													   LOAD_DIAGRAM_POSN_ID,
													   LOAD_DIAGRAM_ID,
													   TRAILER_POSITION,
													   BULK_HEAD_TOP,
													   BULK_HEAD_LEFT,
													   BULK_HEAD_RIGHT,
													   BULK_HEAD_BOTTOM,
													   EQUIP_POSN,
													   CREATED_DTTM,
													   LAST_UPDATED_DTTM,
													   USER_ID)
								select LOAD_DIAGRAM_POSN_ID,
													   LOAD_DIAGRAM_ID,
													   TRAILER_POSITION,
													   BULK_HEAD_TOP,
													   BULK_HEAD_LEFT,
													   BULK_HEAD_RIGHT,
													   BULK_HEAD_BOTTOM,
													   EQUIP_POSN,
													   CREATED_DTTM,
													   LAST_UPDATED_DTTM,
													   USER_ID from C_LOAD_DIAGRAM_POSN
									where LOAD_DIAGRAM_ID = (select load_diagram_id from C_LOAD_DIAGRAM_SUMMARY
										where tc_shipment_id = tcShipmentId);
									-- increment archive count
								i_arc_rc := i_arc_rc + sql%rowcount;
					END;
                end loop;
            end if;  -- archive_ind
            -- delete the same
            forall i in istart..istop
                delete from C_LOAD_DIAGRAM_POSN
                where LOAD_DIAGRAM_ID = (select load_diagram_id from C_LOAD_DIAGRAM_SUMMARY
										where tc_shipment_id = (select tc_shipment_id
							from shipment where
							shipment_id = p_arrIDs(i)));
                -- increment delete count
                i_del_rc := i_del_rc + sql%rowcount;
            if lower(c_debug_yn)='n' then
                commit;
            end if;
        end loop;
        --write_debug_output(p_msg_label=>'ARCH_C_LOAD_DIAGRAM_POSN()', p_arrCompID => p_comp_id, p_rows_archived => i_arc_rc, p_rows_purged => i_del_rc);
        -- set new values and log the message
        rec_MsgLog.r_msg_id := c_msg_id_inform;
        rec_MsgLog.r_rows_archived := i_arc_rc;
        rec_MsgLog.r_rows_deleted := i_del_rc;
        rec_MsgLog.r_end_dttm := systimestamp;
        log_msg (p_rec_MsgLog => rec_MsgLog);
        -- return the number of deleted rows
        p_rowcount := i_del_rc;

    EXCEPTION
        WHEN OTHERS THEN
            v_msg := substr(sqlerrm,1,250);
            msg_log_insert('ARCH_C_LOAD_DIAGRAM_POSN(): Exception! -> ' || v_msg);
            -- set new values and log the message
            rec_MsgLog.r_msg_id := c_msg_id_error;
            rec_MsgLog.r_rows_archived := i_arc_rc;
            rec_MsgLog.r_rows_deleted := i_del_rc;
            rec_MsgLog.r_end_dttm := systimestamp;
            log_msg (p_rec_MsgLog => rec_MsgLog);

            raise e_arch_purge_failed;
    END;

-- ====================================================
	----WM01 def ends

    PROCEDURE purge_shipments(
                         ps_arr_ship_ids     IN tIDs
                        ,ps_company_id       IN purge_config.cd_master_id%type
                        ,ps_whse_id          IN purge_config.whse_master_id%type
                        ,ps_commit_size      IN purge_config.nbr_rows_to_commit%type
                        ,ps_days             IN purge_config.nbr_days_old%type
                        ,ps_purge_cd         IN purge_config.purge_code%type
                        ,ps_archive_flag     IN purge_config.archive_flag%type
                        ,ps_child_comp_flag  IN purge_config.include_child_company_flag%type
                        ,ps_rows_purged      OUT pls_integer
                        )
    IS
        dstart_dttm     timestamp(6);
        i_rc            pls_integer default 0;
        i_rows_purged   pls_integer default 0;
        v_msg           varchar2(250);
        v_error_msg     purge_hist.message%type;

    BEGIN
        msg_log_insert('BEGIN UI_PURGE() ----> purge_shipments()');
        dstart_dttm := systimestamp;

            -- MACR00391268: Removed if ps_arr_ship_ids.count > 0 test
            -- purge tracking message
            select tracking_msg_id bulk collect into arrIDs
            from tracking_message, gtmp_arch_id_list
            where shipment_id = i_id
            and col_name = 'shipment_id';
            
            purge_table ( p_table_name  => 'tracking_message'
                        , p_column_name => 'tracking_msg_id'
                        , p_arrids      => arrIDs
                        , p_commit_size => ps_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => ps_archive_flag
                        , p_purge_cd    => ps_purge_cd
                        , p_comp_id     => ps_company_id
                        , p_warehouse_id=> ps_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := nvl(i_rows_purged,0) + i_rc;--MACR00386769            

            -- shipment ids
            select i_id bulk collect into arrIDs
            from gtmp_arch_id_list
            where col_name = 'shipment_id';
            
            for currec in (
                            select 'STOP_COMMODITY_TEMPLATE' TAB_NAME from dual 
                            union all
                            select 'stop_sched_action' TAB_NAME from dual   
                            union all
                            select 'STOP_TEMPLATE' TAB_NAME from dual
                            union all
                            select 'STOP_COMMODITY' TAB_NAME from dual  -- MACR00386769
                            union all
                            select 'stop_action_template' TAB_NAME from dual -- MACR00386769
                            union all
                            select 'STOP_ACTION_SHIPMENT' TAB_NAME from dual -- MACR00386769
                          )
            loop              
                 purge_table ( p_table_name  => currec.tab_name
                            , p_column_name => 'shipment_id'
                            , p_arrids      => arrIDs
                            , p_commit_size => ps_commit_size
                            , p_start_time  => dstart_dttm
                            , p_archive_ind => ps_archive_flag
                            , p_purge_cd    => ps_purge_cd
                            , p_comp_id     => ps_company_id
                            , p_warehouse_id=> ps_whse_id
                            , p_rowcount    => i_rc
                            , p_error_msg   => v_error_msg); 
                            
                i_rows_purged := nvl(i_rows_purged,0) + i_rc;
            end loop;                     

            -- select stop_action_order_ids
            select stop_action_order_id bulk collect into arrIDs
            from stop_action_order, gtmp_arch_id_list
            where shipment_id = i_id
            and col_name = 'shipment_id';

            
            purge_table ( p_table_name  => 'STOP_ACTION_ORDER'
                        , p_column_name => 'STOP_ACTION_ORDER_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => ps_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => ps_archive_flag
                        , p_purge_cd    => ps_purge_cd
                        , p_comp_id     => ps_company_id
                        , p_warehouse_id=> ps_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := nvl(i_rows_purged,0) + i_rc;

            -- TE-2576 removing WS_SHIPMENT,WS_STOP tables, as they are dropped in 2014.
            -- select original_shipment_ids

            -- Modify to add conditions to stop empty executions
            -- shipment ids
            select distinct i_id bulk collect into arrIDs
            from gtmp_arch_id_list, stop_action
            where col_name = 'shipment_id'
            and   shipment_id=i_id;
            
            purge_table ( p_table_name  => 'stop_action'
                        , p_column_name => 'SHIPMENT_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => ps_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => ps_archive_flag
                        , p_purge_cd    => ps_purge_cd
                        , p_comp_id     => ps_company_id
                        , p_warehouse_id=> ps_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := nvl(i_rows_purged,0) + i_rc;
            
            select distinct i_id
            bulk collect into arrIDs
            from gtmp_arch_id_list, shipment_note
            where col_name = 'shipment_id'
            and   shipment_id=i_id;

            purge_table ( p_table_name  => 'SHIPMENT_NOTE'
                        , p_column_name => 'SHIPMENT_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => ps_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => ps_archive_flag
                        , p_purge_cd    => ps_purge_cd
                        , p_comp_id     => ps_company_id
                        , p_warehouse_id=> ps_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := nvl(i_rows_purged,0) + i_rc;

            select distinct i_id bulk collect into arrIDs
            from gtmp_arch_id_list, stop
            where col_name = 'shipment_id'
            and   shipment_id=i_id;

            purge_table ( p_table_name  => 'STOP'
                        , p_column_name => 'SHIPMENT_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => ps_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => ps_archive_flag
                        , p_purge_cd    => ps_purge_cd
                        , p_comp_id     => ps_company_id
                        , p_warehouse_id=> ps_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;

            select distinct i_id bulk collect into arrIDs
            from gtmp_arch_id_list, shipment_tracking
            where col_name = 'shipment_id'
            and   shipment_id=i_id;
            
            purge_table ( p_table_name  => 'SHIPMENT_TRACKING'
                        , p_column_name => 'SHIPMENT_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => ps_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => ps_archive_flag
                        , p_purge_cd    => ps_purge_cd
                        , p_comp_id     => ps_company_id
                        , p_warehouse_id=> ps_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := nvl(i_rows_purged,0) + i_rc;            

            select distinct i_id bulk collect into arrIDs
            from gtmp_arch_id_list, shipment_size_override
            where col_name = 'shipment_id'
            and   shipment_id=i_id;
            
            purge_table ( p_table_name  => 'SHIPMENT_SIZE_OVERRIDE'
                        , p_column_name => 'SHIPMENT_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => ps_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => ps_archive_flag
                        , p_purge_cd    => ps_purge_cd
                        , p_comp_id     => ps_company_id
                        , p_warehouse_id=> ps_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := nvl(i_rows_purged,0) + i_rc;                

            select distinct i_id bulk collect into arrIDs
            from gtmp_arch_id_list, shipment_size
            where col_name = 'shipment_id'
            and   shipment_id=i_id;
            
            purge_table ( p_table_name  => 'SHIPMENT_SIZE'
                        , p_column_name => 'SHIPMENT_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => ps_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => ps_archive_flag
                        , p_purge_cd    => ps_purge_cd
                        , p_comp_id     => ps_company_id
                        , p_warehouse_id=> ps_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := nvl(i_rows_purged,0) + i_rc;                

            select distinct i_id bulk collect into arrIDs
            from gtmp_arch_id_list, shipment_seal_number
            where col_name = 'shipment_id'
            and   shipment_id=i_id;
            
            purge_table ( p_table_name  => 'SHIPMENT_SEAL_NUMBER'
                        , p_column_name => 'SHIPMENT_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => ps_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => ps_archive_flag
                        , p_purge_cd    => ps_purge_cd
                        , p_comp_id     => ps_company_id
                        , p_warehouse_id=> ps_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := nvl(i_rows_purged,0) + i_rc;            

            select distinct i_id bulk collect into arrIDs
            from gtmp_arch_id_list, shipment_sched_action
            where col_name = 'shipment_id'
            and   shipment_id=i_id;
            
            purge_table ( p_table_name  => 'SHIPMENT_SCHED_ACTION'
                        , p_column_name => 'SHIPMENT_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => ps_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => ps_archive_flag
                        , p_purge_cd    => ps_purge_cd
                        , p_comp_id     => ps_company_id
                        , p_warehouse_id=> ps_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := nvl(i_rows_purged,0) + i_rc;    

            select distinct i_id bulk collect into arrIDs
            from gtmp_arch_id_list, shipment_event
            where col_name = 'shipment_id'
            and   shipment_id=i_id;
            
            purge_table ( p_table_name  => 'SHIPMENT_EVENT'
                        , p_column_name => 'SHIPMENT_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => ps_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => ps_archive_flag
                        , p_purge_cd    => ps_purge_cd
                        , p_comp_id     => ps_company_id
                        , p_warehouse_id=> ps_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := nvl(i_rows_purged,0) + i_rc;    

            select distinct i_id bulk collect into arrIDs
            from gtmp_arch_id_list, shipment_accessorial
            where col_name = 'shipment_id'
            and   shipment_id=i_id;

            purge_table ( p_table_name  => 'SHIPMENT_ACCESSORIAL'
                        , p_column_name => 'SHIPMENT_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => ps_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => ps_archive_flag
                        , p_purge_cd    => ps_purge_cd
                        , p_comp_id     => ps_company_id
                        , p_warehouse_id=> ps_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := nvl(i_rows_purged,0) + i_rc;
            
            -- BEGIN MACR00551671
            select distinct i_id bulk collect into arrIDs
            from gtmp_arch_id_list, shipment_commodity
            where col_name = 'shipment_id'
            and   shipment_id=i_id;

            purge_table ( p_table_name  => 'SHIPMENT_COMMODITY'
                        , p_column_name => 'SHIPMENT_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => ps_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => ps_archive_flag
                        , p_purge_cd    => ps_purge_cd
                        , p_comp_id     => ps_company_id
                        , p_warehouse_id=> ps_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;

            select distinct i_id bulk collect into arrIDs
            from gtmp_arch_id_list, shipment_commodity_template
            where col_name = 'shipment_id'
            and   shipment_id=i_id;

            purge_table ( p_table_name  => 'SHIPMENT_COMMODITY_TEMPLATE'
                        , p_column_name => 'SHIPMENT_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => ps_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => ps_archive_flag
                        , p_purge_cd    => ps_purge_cd
                        , p_comp_id     => ps_company_id
                        , p_warehouse_id=> ps_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;


            select distinct i_id bulk collect into arrIDs
            from gtmp_arch_id_list, shipment_attribute
            where col_name = 'shipment_id'
            and   shipment_id=i_id;

            purge_table ( p_table_name  => 'SHIPMENT_ATTRIBUTE'
                        , p_column_name => 'SHIPMENT_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => ps_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => ps_archive_flag
                        , p_purge_cd    => ps_purge_cd
                        , p_comp_id     => ps_company_id
                        , p_warehouse_id=> ps_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;


            select distinct i_id bulk collect into arrIDs
            from gtmp_arch_id_list, shipment_attribute_template
            where col_name = 'shipment_id'
            and   shipment_id=i_id;

            purge_table ( p_table_name  => 'SHIPMENT_ATTRIBUTE_TEMPLATE'
                        , p_column_name => 'SHIPMENT_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => ps_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => ps_archive_flag
                        , p_purge_cd    => ps_purge_cd
                        , p_comp_id     => ps_company_id
                        , p_warehouse_id=> ps_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;


            select distinct i_id bulk collect into arrIDs
            from gtmp_arch_id_list, shipment_accessorial_template
            where col_name = 'shipment_id'
            and   shipment_id=i_id;

            purge_table ( p_table_name  => 'SHIPMENT_ACCESSORIAL_TEMPLATE'
                        , p_column_name => 'SHIPMENT_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => ps_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => ps_archive_flag
                        , p_purge_cd    => ps_purge_cd
                        , p_comp_id     => ps_company_id
                        , p_warehouse_id=> ps_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;


            select distinct i_id bulk collect into arrIDs
            from gtmp_arch_id_list, shipment_note_template
            where col_name = 'shipment_id'
            and   shipment_id=i_id;

            purge_table ( p_table_name  => 'SHIPMENT_NOTE_TEMPLATE'
                        , p_column_name => 'SHIPMENT_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => ps_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => ps_archive_flag
                        , p_purge_cd    => ps_purge_cd
                        , p_comp_id     => ps_company_id
                        , p_warehouse_id=> ps_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;


            select distinct i_id bulk collect into arrIDs
            from gtmp_arch_id_list, shipment_size_template
            where col_name = 'shipment_id'
            and   shipment_id=i_id;

            purge_table ( p_table_name  => 'SHIPMENT_SIZE_TEMPLATE'
                        , p_column_name => 'SHIPMENT_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => ps_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => ps_archive_flag
                        , p_purge_cd    => ps_purge_cd
                        , p_comp_id     => ps_company_id
                        , p_warehouse_id=> ps_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;

            select distinct i_id bulk collect into arrIDs
            from gtmp_arch_id_list, shipment_template
            where col_name = 'shipment_id'
            and   shipment_id=i_id;
			
			for currec in (
                            select 'STOP_ADDITIONAL_SIZE_TEMPLATE' TAB_NAME from dual  -- MACR00386769
                            union all
                            select 'STOP_SIZE_TEMPLATE' TAB_NAME from dual -- MACR00386769
                            union all
                            select 'SHIPMENT_TEMPLATE' TAB_NAME from dual -- MACR00386769
                          )
            loop 

            purge_table ( p_table_name  => currec.tab_name
                        , p_column_name => 'SHIPMENT_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => ps_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => ps_archive_flag
                        , p_purge_cd    => ps_purge_cd
                        , p_comp_id     => ps_company_id
                        , p_warehouse_id=> ps_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;
			
			end loop;

            -- #MACR00732039 SHIPMENT_EXTN_TLM table named corrected as part of cbo 2012.0.12.1 scripts. 
            select distinct i_id bulk collect into arrIDs
            from gtmp_arch_id_list, shipment_extn_tlm
            where col_name = 'shipment_id'
            and   shipment_id=i_id;

            purge_table ( p_table_name  => 'SHIPMENT_EXTN_TLM'
                        , p_column_name => 'SHIPMENT_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => ps_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => ps_archive_flag
                        , p_purge_cd    => ps_purge_cd
                        , p_comp_id     => ps_company_id
                        , p_warehouse_id=> ps_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := i_rows_purged + i_rc;
			

		/* WM01 call Begins */

		select i_id bulk collect into arrIDs
				from gtmp_arch_id_list
				where col_name = 'shipment_id';

				ARCH_C_LOAD_DIAGRAM_ITEM_DTL ( arrIDs
										, ps_commit_size
										, dstart_dttm
										, ps_archive_flag
										, ps_purge_cd
										, ps_company_id
										, ps_whse_id
										, i_rc);
				/*
				purge_table ( p_table_name  => 'C_LOAD_DIAGRAM_ITEM_DETAIL'
									, p_column_name => 'LOAD_DIAGRAM_ID'
									, p_arrids      => arrIDs_1
									, p_commit_size => ps_commit_size
									, p_start_time  => dstart_dttm
									, p_archive_ind => ps_archive_flag
									, p_purge_cd    => ps_purge_cd
									, p_comp_id     => ps_company_id
									, p_warehouse_id=> ps_whse_id
									, p_rowcount    => i_rc
									, p_error_msg   => v_error_msg);
						i_rows_purged := i_rows_purged + i_rc;
				*/
			
				ARCH_C_LOAD_DIAGRAM_POSN ( arrIDs
					, ps_commit_size
					, dstart_dttm
					, ps_archive_flag
					, ps_purge_cd
					, ps_company_id
					, ps_whse_id
					, i_rc);
				/*
				purge_table ( p_table_name  => 'C_LOAD_DIAGRAM_POSN'
									, p_column_name => 'LOAD_DIAGRAM_ID'
									, p_arrids      => arrIDs_1
									, p_commit_size => ps_commit_size
									, p_start_time  => dstart_dttm
									, p_archive_ind => ps_archive_flag
									, p_purge_cd    => ps_purge_cd
									, p_comp_id     => ps_company_id
									, p_warehouse_id=> ps_whse_id
									, p_rowcount    => i_rc
									, p_error_msg   => v_error_msg);
						i_rows_purged := i_rows_purged + i_rc;
				*/
				ARCH_C_LOAD_DIAGRAM_SUMMARY ( arrIDs
										, ps_commit_size
										, dstart_dttm
										, ps_archive_flag
										, ps_purge_cd
										, ps_company_id
										, ps_whse_id
										, i_rc );
				/*
				purge_table ( p_table_name  => 'C_LOAD_DIAGRAM_SUMMARY'
									, p_column_name => 'LOAD_DIAGRAM_ID'
									, p_arrids      => arrIDs_1
									, p_commit_size => ps_commit_size
									, p_start_time  => dstart_dttm
									, p_archive_ind => ps_archive_flag
									, p_purge_cd    => ps_purge_cd
									, p_comp_id     => ps_company_id
									, p_warehouse_id=> ps_whse_id
									, p_rowcount    => i_rc
									, p_error_msg   => v_error_msg);
						i_rows_purged := i_rows_purged + i_rc;
					*/
			/*  WM01 call ENDS */

            select i_id bulk collect into arrIDs
            from gtmp_arch_id_list
            where col_name = 'shipment_id';

	        FOR CURREC in
                    (
                      select 'STOP_SIZE' tab_name from dual
                      UNION ALL
                      select 'DOCK_DOOR_REF' tab_name from dual
                      UNION ALL
                      select 'TRAILER_CONTENTS' tab_name from dual
                      UNION ALL
					  select 'STOP_ADDITIONAL_SIZE' tab_name from dual
                      UNION ALL
                      select 'SHIPMENT' tab_name  from dual
                    )
                  LOOP
                      purge_table ( p_table_name  => currec.tab_name
                        , p_column_name => 'SHIPMENT_ID'
                        , p_arrids      => arrIDs
                        , p_commit_size => ps_commit_size
                        , p_start_time  => dstart_dttm
                        , p_archive_ind => ps_archive_flag
                        , p_purge_cd    => ps_purge_cd
                        , p_comp_id     => ps_company_id
                        , p_warehouse_id=> ps_whse_id
                        , p_rowcount    => i_rc
                        , p_error_msg   => v_error_msg);
            i_rows_purged := nvl(i_rows_purged,0) + i_rc;
                  END LOOP;

        ps_rows_purged := i_rows_purged;

        msg_log_insert('END UI_PURGE() ----> purge_shipments()');
    EXCEPTION
        WHEN OTHERS THEN
            v_msg := substr(sqlerrm,1,250);
            msg_log_insert(p_msg_parms => 'purge_shipments(): Exception! -> ' || v_msg);
            ps_rows_purged := i_rows_purged;

            raise e_arch_purge_failed;
    END;

    /* Purge PROCEDURE called by PROCEDURE UI_PURGE */
    PROCEDURE exec_purge (p_company_id      IN purge_config.cd_master_id%type
                        ,p_whse_id          IN purge_config.whse_master_id%type
                        ,p_commit_size      IN purge_config.nbr_rows_to_commit%type
                        ,p_days             IN purge_config.nbr_days_old%type
                        ,p_purge_cd         IN purge_config.purge_code%type
                        ,p_archive_flag     IN purge_config.archive_flag%type
                        ,p_child_comp_flag  IN purge_config.include_child_company_flag%type
                        ,p_misc_parm_1      IN purge_config.misc_parm_1%type
                        ,p_misc_parm_2      IN purge_config.misc_parm_2%type
                        ,p_misc_parm_3      IN purge_config.misc_parm_3%type
                        ,p_delink_child     IN purge_config.delink_child%type
                        ,p_custom_config    IN purge_config.custom_config%type
                        ,p_rows_purged      OUT pls_integer
                        ,p_error_msg        OUT purge_hist.message%type)
    IS
        dstart                  timestamp(6);
        v_msg                   varchar2(250);
        v_error_msg             purge_hist.message%type;
        v_where_clause          varchar2(4000) default null;
        v_arch_starts           constant char (15):='ARCHIVE STARTS';
        v_arch_ends             constant char (15):='ARCHIVE ENDS';
        lv_recv_msg             varchar2(30);
        lv_event_message        varchar2(30);
        lv_cl_message                  varchar2(30);
        lv_cl_endpoint_queue           varchar2(30); 
        lv_cl_message_keys             varchar2(30); 
        lv_om_sched_event              varchar2(30);
        lv_so_order_chg_value          varchar2(30); 
        lv_constemp_sched_event        varchar2(30);
        lv_lrf_report_param_value_dtl  varchar2(30); 
        lv_lrf_report_param_value      varchar2(30); 
        lv_lrf_report                  varchar2(30); 
        lv_lrf_event                   varchar2(30); 
        lv_lrf_rep_schd_param_dtl      varchar2(30); 
        lv_lrf_rep_schd_param          varchar2(30); 
        lv_lrf_sched_event_report      varchar2(30); 
        lv_lrf_report_audit            varchar2(30); 
        v_sql                          varchar2(9000);
        v_hist_array                   Hist_Array;
        v_is_config                    varchar2(1) default 'N';
        i_event_rows            pls_integer;
        i_olpn_rows             pls_integer;
        i_lpn_rows              pls_integer;
        i_ord_rows              pls_integer;
        i_task_rows             pls_integer;
        i_rule_rows             pls_integer;
        i_num_rows              pls_integer;
        i_rc                    pls_integer default 0;
        i_rows_purged           pls_integer default 0;
        i_asn_stat_code         pls_integer;
        i_lpn_stat_code         pls_integer;
        i_ship_stat_code        pls_integer;
        e_InvalidPurgeCode      exception;
        e_NoShipPurgeStatus     exception;
        e_StopPurge             exception;
        e_InvalidWhseID         exception;      
        v_invalid_purgecode     varchar2(1);

        c_default_asn_status    CONSTANT whse_parameters.purge_asn_stat_code%type := 40;

    BEGIN
        -- set global WHSE variable
        begin
            select whse
            into gv_whse
            from whse_master
            where whse_master_id = p_whse_id;
        exception
            when others then
                v_error_msg := 'exec_purge(): Warehouse ID Not Found --> ' || to_char(p_whse_id);
                raise e_InvalidWhseID;
        end;
        g_whse_id :=p_whse_id;

        -- MACR00557025: use company name for message logging
        select company_name
        into gv_company
        from company
        where company_id = p_company_id;
      
        gi_purge_exception_count := 0; -- WM-37179
        --ins_archive_long_table_mapping();
        
        dstart := systimestamp;
	    if (p_archive_flag <> c_arch_none) 
        then
			v_sql:='select count(1) 
			from archive_run_detail
			where PURGE_CODE='||p_purge_cd ||' and STATUS='''||v_arch_starts||'''';
			
			execute immediate v_sql into gv_count;
	        if ( gv_count=0 ) then
               
			   v_sql:='insert into archive_run_detail(run_id,PURGE_CODE,COMPANY_ID, START_TIME,STATUS)
                       select '||archive_run_log_SEQ.nextval||','|| p_purge_cd||','||p_company_id||','''||dstart||''','''||v_arch_starts||''' from dual';
			   execute immediate v_sql;
			end if;
		end if;
        if p_custom_config = 'N'
        then		        
           case -- p_purge_cd
               when p_purge_cd = '1' then    -- Activity Tracking Transactions
                   dstart := systimestamp;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Activity Tracking Purge', action_name => 'Archiving and Purging');
                   select prod_trkg_tran_id
                   bulk collect into arrIDs
                   from prod_trkg_tran
                   where cd_master_id = p_company_id
                   and whse = gv_whse
                   and mod_date_time <= dstart - p_days;
                   
                   purge_table ( p_table_name  => 'PROD_TRKG_TRAN'
                               , p_column_name => 'PROD_TRKG_TRAN_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);          
                   i_rows_purged := i_rows_purged + i_rc;
               
   
               when p_purge_cd = '2' then    -- Allocations
                   dstart := systimestamp;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Allocations Purge', action_name => 'Archiving and Purging');
                   select aid.alloc_invn_dtl_id
                   bulk collect into arrIDs
                   from alloc_invn_dtl aid
                   where aid.cd_master_id = p_company_id
                   and aid.whse = gv_whse
                   and ( aid.tc_order_id is NULL or 
                         not exists (select NULL from orders o1 where o1.tc_order_id = aid.tc_order_id and o1.DO_STATUS < 200 )) --WM-46790 
                   and aid.mod_date_time <= systimestamp - p_days
                   and aid.stat_code >= 90  -- processed
                   and not exists (select 1 from task_dtl td,task_hdr th  where td.alloc_invn_dtl_id = aid.alloc_invn_dtl_id and td.task_hdr_id = th.task_hdr_id and th.stat_code < 90 ) ;   --WM-34255 : Not to consider open tasks
   
                   purge_table ( p_table_name  => 'ALLOC_INVN_DTL'
                               , p_column_name => 'ALLOC_INVN_DTL_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
   
               when p_purge_cd = '3' then -- Batch Master Purge (Lot Master)
                   dstart := systimestamp;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Lot Master Purge', action_name => 'Archiving and Purging');
                   delete from TMP_WM_ID_1;
                   insert into TMP_WM_ID_1 ( v_id, i_id)
                       select  b.batch_nbr, b.item_id
                       from batch_master b, item_cbo i
                       where b.item_id = i.item_id
                       and i.company_id = p_company_id --#WM-54534 removed stat_code=99 check
                       and b.mod_date_time <= dstart - p_days
                       and not exists (
                                       select 1
                                       from pick_locn_dtl p
                                       where p.batch_nbr = b.batch_nbr
                                       and p.item_id = b.item_id --#WM-49438
                                       )
                       and not exists (  -- MACR00783503 (no quantity-on-hand inventory for lot)
                                       select distinct batch_nbr
                                       from wm_inventory w
                                       where on_hand_qty <> 0
                                       and w.batch_nbr = b.batch_nbr
                                       and w.item_id = b.item_id 
                                       )
                       and not exists (  -- MACR00783503 (no in-transit inventory for lot)
                                       select distinct d.batch_nbr
                                       from asn a, asn_detail d
                                       where d.batch_nbr IS NOT NULL
                                       AND d.asn_id = a.asn_id
                                       and a.asn_status='20'
                                       and d.shipped_qty <> 0
                                       and d.batch_nbr = b.batch_nbr
                                       and d.sku_id = b.item_id 
                                      );
                                      
                   select distinct t.v_id
                   bulk collect into varrIDs
                   from batch_hist_stat, TMP_WM_ID_1 t
                   where item_id = t.i_id;
                   
                   purge_table ( p_table_name  => 'batch_hist_stat'
                               , p_column_name => 'batch_nbr'
                               , p_varrids      => varrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
   
                   select distinct t.v_id
                   bulk collect into varrIDs
                   from batch_hist_ship, TMP_WM_ID_1 t
                   where item_id = t.i_id;
                   
                   purge_table ( p_table_name  => 'batch_hist_ship'
                               , p_column_name => 'batch_nbr'
                               , p_varrids      => varrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
                   
                   select distinct t.v_id
                   bulk collect into varrIDs
                   from batch_hist_work_ord, TMP_WM_ID_1 t
                   where item_id = t.i_id;                
   
                   purge_table ( p_table_name  => 'batch_hist_work_ord'
                               , p_column_name => 'batch_nbr'
                               , p_varrids      => varrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;                    
      
                   v_where_clause:=' WHERE exists (
                                           select 1
                                           from TMP_WM_ID_1 
                                           where v_id = t.batch_nbr and i_id = t.item_id)';
   
   				   purge_table_where ( p_table_name  => 'batch_master'
                                 , p_where_clause=> v_where_clause
                                 , p_commit_size => p_commit_size
                                 , p_start_time  => dstart
                                 , p_archive_ind => p_archive_flag
                                 , p_purge_cd    => p_purge_cd
                                 , p_comp_id     => p_company_id
                                 , p_warehouse_id=> p_whse_id
                                 , p_rowcount    => i_rc
                                 , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
   
               when p_purge_cd = '4' then -- Divert History Records
                   dstart := systimestamp;
   
   				DBMS_APPLICATION_INFO.set_module(module_name => 'Divert History Purge', action_name => 'Archiving and Purging');
                   select divrt_hist_id
                   bulk collect into arrIDs
                   from divrt_hist
                   where cd_master_id = p_company_id
                   and whse = gv_whse
                   and mod_date_time <= dstart - p_days;
                   
                   purge_table ( p_table_name  => 'DIVRT_HIST'
                               , p_column_name => 'DIVRT_HIST_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;                        
   
               when p_purge_cd = '5' then -- Dynamic Routing Requests
                   dstart := systimestamp;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Dynamic Routing Requests Purge', action_name => 'Archiving and Purging');
                   select dynamic_route_id
                   bulk collect into arrIDs
                   from dynamic_route
                   where tc_company_id = p_company_id
                   and facility_id = p_whse_id
                   and stat_code in (90,99)  -- completed,Cancelled
                   and last_updated_dttm <= dstart - p_days;
                   
                   for currec in (
                                   select 'DYNAMIC_ROUTE_DETAIL' TAB_NAME from dual
                                   union all
                                   select 'DYNAMIC_ROUTE' TAB_NAME from dual
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'DYNAMIC_ROUTE_ID'
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;                 
                                  
   
               when p_purge_cd = '6' then -- EPC Tag Reads
                   dstart := systimestamp;
   
   				DBMS_APPLICATION_INFO.set_module(module_name => 'EPC Tag Reads Purge', action_name => 'Archiving and Purging');
                   select epc_tag_reads_id
                   bulk collect into arrIDs
                   from epc_tag_reads
                   where whse = gv_whse
                   and mod_date_time <= dstart - p_days;
                   
                   purge_table ( p_table_name  => 'epc_tag_reads'
                               , p_column_name => 'EPC_TAG_READS_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;                    
                   
               when p_purge_cd = '7' then -- EPC Tracking Records Purge
                   dstart := systimestamp;
   
   				DBMS_APPLICATION_INFO.set_module(module_name => 'EPC Track Purge', action_name => 'Archiving and Purging');
   
                   select epc_track_id
                   bulk collect into arrIDs
                   from epc_track
                   where mod_date_time <= dstart - p_days;
                   
                   purge_table ( p_table_name  => 'EPC_TRACK'
                               , p_column_name => 'EPC_TRACK_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;                    
   
               when p_purge_cd = '8' then -- Event Message Purge
                   dstart := systimestamp;
   
   				DBMS_APPLICATION_INFO.set_module(module_name => 'Event Message Purge', action_name => 'Archiving and Purging');
   
                   -- WM-41705 modified to purge to move the data from _HISTORY tables if the history tables contain any data.
                   v_hist_array := Hist_Array('RECV_MSG_DATA_HISTORY','EVENT_MESSAGE_HISTORY');
   
                   is_table_history_configured(p_hist_array => v_hist_array
                                               ,p_is_config => v_is_config);
   
                   if v_is_config = 'Y'
                   then
                      lv_recv_msg := 'RECV_MSG_DATA_HISTORY';
                      lv_event_message := 'EVENT_MESSAGE_HISTORY';
                   else
                      lv_recv_msg := 'RECV_MSG_DATA';
                      lv_event_message := 'EVENT_MESSAGE';
                   end if;
   
                   v_sql := 'select event_message_id ' 
                   ||' from '
                   || lv_event_message 
                   || ' where cd_master_id =  ' 
                   || p_company_id
                   || ' and whse =  '''
                   || gv_whse
                   || ''' and stat_code >= 90 '  -- completed --MACR00353738
                   || ' and mod_date_time <= sysdate - ' || p_days;
   
                   execute immediate v_sql bulk collect into arrIDs;
   
                   purge_table ( p_table_name  => lv_recv_msg
                               , p_column_name => 'MESSAGE_ID'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rc;
                   
                   purge_table ( p_table_name  => lv_event_message
                               , p_column_name => 'EVENT_MESSAGE_ID'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   p_error_msg := v_error_msg;
   
               when p_purge_cd = '9' then -- FedEx Transactions
                   dstart := systimestamp;
   
   			    DBMS_APPLICATION_INFO.set_module(module_name => 'Fedex Transactions Purge', action_name => 'Archiving and Purging');
                   select fedex_tran_id
                   bulk collect into arrIDs
                   from fedex_tran
                   where last_updated_dttm <= dstart - p_days;
                   
                   purge_table ( p_table_name  => 'FEDEX_TRAN'
                               , p_column_name => 'FEDEX_TRAN_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
   
               when p_purge_cd = '10' then -- Immediate Needs
                   dstart := systimestamp;
   
   				DBMS_APPLICATION_INFO.set_module(module_name => 'Immediate Needs Purge', action_name => 'Archiving and Purging');
                   select immd_needs_id
                   bulk collect into arrIDs
                   from immd_needs
                   where cd_master_id = p_company_id
                   and whse = gv_whse
                   and stat_code >= 90  -- completed
                   and mod_date_time <= dstart - p_days;
                   
                   purge_table ( p_table_name  => 'IMMD_NEEDS'
                               , p_column_name => 'IMMD_NEEDS_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
               
               when p_purge_cd = '11' then -- Message Log
                   dstart := systimestamp;
   
   				DBMS_APPLICATION_INFO.set_module(module_name => 'Message Log Purge', action_name => 'Archiving and Purging');
                   select p.proc_mntr_id
                   bulk collect into arrIDs
                   from proc_mntr p
                   where exists (
                               select 1
                               from msg_log m
                               where m.cd_master_id = p_company_id
                               and m.whse = gv_whse
                               and m.mod_date_time <= dstart - p_days
                               and m.msg_log_id = p.msg_log_id
                               );
                               
                   for currec in (
                                   select 'PROC_MNTR_DTL' TAB_NAME from dual
                                   union all
                                   select 'PROC_MNTR' TAB_NAME from dual
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'PROC_MNTR_ID'
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;                              
   
                   select msg_log_id
                   bulk collect into arrIDs
                   from msg_log m
                   where m.mod_date_time <= dstart - p_days; -- removed whse check #MACR00866257
                   
                   purge_table ( p_table_name  => 'MSG_LOG'
                               , p_column_name => 'MSG_LOG_ID'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
   
               when p_purge_cd = '12' then -- Output Tables
                   dstart := systimestamp;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Output Tables Purge', action_name => 'Archiving and Purging');
                   delete from tmp_wm_id_1;
                   delete from tmp_wm_id_2;
                   delete from tmp_wm_id_3;
                   -- get outpt_orders eligible to archive/purge
                   insert into tmp_wm_id_1 ( i_id, v_id)
                       select outpt_orders_id, tc_order_id
                       from outpt_orders
                       where tc_company_id = p_company_id
                       and proc_stat_code >= 90
                       and last_updated_dttm <= dstart - p_days
                       and o_facility_id = p_whse_id;  --- added as part of MACR00866261. Jira : WM-25989 
                   i_ord_rows := sql%rowcount;
   
   -- MACR00495778
                   -- get output lpn ids eligible to archive/purge
                   insert into tmp_wm_id_2 ( i_id, v_id)
                   with t_invc_batch as (
                       select distinct invc_batch_nbr
                       from tmp_wm_id_1, outpt_orders
                       where outpt_orders_id = i_id
                       )
                       select  l.outpt_lpn_id, l.tc_lpn_id
                       from outpt_lpn l, t_invc_batch ibn
                       where l.tc_company_id = p_company_id
                       and l.proc_stat_code >= 90
                       and l.invc_batch_nbr = ibn.invc_batch_nbr
                       and l.last_updated_dttm <= dstart - p_days;
   -- END MACR00495778
                   i_olpn_rows := sql%rowcount;
   
   -- MACR00495778
                   -- get lpn_ids from lpn table for outpt_lpn_serial_nbr tables
                   insert into tmp_wm_id_3 (lpn_id, invc_batch_nbr)
                       select l.lpn_id, ol.invc_batch_nbr
                       from lpn l, outpt_lpn ol
                       where l.tc_lpn_id = ol.tc_lpn_id
                       and exists (select 1
                                   from tmp_wm_id_2 t
                                   where t.i_id = ol.outpt_lpn_id);
   -- END MACR00495778
   
                   i_lpn_rows := sql%rowcount;
                   
                   select sn.srl_nbr
                   bulk collect into varrIDs
                   from OUTPT_LPN_SERIAL_NBR sn , TMP_WM_ID_3 t , OUTPT_LPN_MINOR_SERIAL_NBR msn
                   where sn.lpn_id = t.lpn_id
                   and sn.seq_nbr = msn.seq_nbr;    
   
                   purge_table ( p_table_name  => 'OUTPT_LPN_MINOR_SERIAL_NBR'
                               , p_column_name => 'srl_nbr'
                               , p_varrids      => varrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;        
                   
                   select distinct t.LPN_ID
                   bulk collect into arrIDs
                   from OUTPT_LPN_SERIAL_NBR o, TMP_WM_ID_3 t
                   where o.INVC_BATCH_NBR = t.INVC_BATCH_NBR;                    
           
                   purge_table ( p_table_name  => 'OUTPT_LPN_SERIAL_NBR'
                               , p_column_name => 'LPN_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
                   
                   select  distinct t.v_id
                   bulk collect into varrIDs
                   from outpt_lpn l , tmp_wm_id_2 t, outpt_lpn_detail d
                   where l.outpt_lpn_id = t.i_id
                   and l.invc_batch_nbr = d.invc_batch_nbr  
                   and l.tc_company_id = d.tc_company_id;                
   
                   purge_table ( p_table_name  => 'outpt_lpn_detail'
                               , p_column_name => 'tc_lpn_id'
                               , p_varrids      => varrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
       
                   select distinct t.v_id
                   bulk collect into varrIDs
                   from outpt_lpn l , tmp_wm_id_2 t , outpt_lpn_catch_weight o
                   where l.outpt_lpn_id = t.i_id
                   and l.invc_batch_nbr = o.invc_batch_nbr ;
                   
                   purge_table ( p_table_name  => 'outpt_lpn_catch_weight'
                               , p_column_name => 'tc_lpn_id'
                               , p_varrids      => varrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;                    
                   
                   -- get outpt_order_line_item_size_ids only for the tc_order_id/company combos
                   select i_id
                   bulk collect into arrIDs
                   from TMP_WM_ID_2;
                
                   purge_table ( p_table_name  => 'OUTPT_LPN'
                               , p_column_name => 'OUTPT_LPN_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
                   
                   select i_id
                   bulk collect into arrIDs
                   from tmp_wm_id_1;                
       
                   purge_table ( p_table_name  => 'OUTPT_ORDERS'
                               , p_column_name => 'outpt_orders_id'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
       
                   -- get outpt_order_line_item_ids only for the tc_order_id/company combos
                   select outpt_order_line_item_id
                   bulk collect into arrIDs
                   from outpt_order_line_item
                   where tc_company_id = p_company_id
                   and last_updated_dttm <= dstart - p_days
                   and exists (
                               select 1
                               from tmp_wm_id_1
                               where v_id = tc_order_id
                               );
                               
                   purge_table ( p_table_name  => 'OUTPT_ORDER_LINE_ITEM'
                               , p_column_name => 'OUTPT_ORDER_LINE_ITEM_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
   
                   -- Archive/purge any remaining records from outpt_order_line_item
                   -- orphaned by other processes as identified by tc_order_id                
                   select outpt_order_line_item_id
                   bulk collect into arrIDs
                   from outpt_order_line_item ool
                   where not exists 
                   (
                     select 1
                     from outpt_orders oo
                     where oo.TC_ORDER_ID = ool.TC_ORDER_ID
                   );
                                   
                   purge_table ( p_table_name  => 'OUTPT_ORDER_LINE_ITEM'
                               , p_column_name => 'OUTPT_ORDER_LINE_ITEM_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
                   
   
                   -- get outpt_order_line_item_size_ids only for the tc_order_id/company combos
                   select outpt_order_line_item_size_id
                   bulk collect into arrIDs
                   from outpt_order_line_item_size
                   where tc_company_id = p_company_id
                   and exists (
                               select 1
                               from tmp_wm_id_1
                               where v_id = tc_order_id
                               );
                               
                   purge_table ( p_table_name  => 'OUTPT_ORDER_LINE_ITEM_SIZE'
                               , p_column_name => 'outpt_order_line_item_size_id'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
                   
                   -- clear orphans from outpt_order_line_item_size
                   select outpt_order_line_item_size_id
                   bulk collect into arrIDs
                   from outpt_order_line_item_size ool
                   where not exists (
                               select 1
                               from OUTPT_ORDERS oo
                               where oo.tc_order_id = ool.tc_order_id
                               );
                               
                   purge_table ( p_table_name  => 'OUTPT_ORDER_LINE_ITEM_SIZE'
                               , p_column_name => 'outpt_order_line_item_size_id'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
   
                   dstart := systimestamp;
                   -- get output shipment_return ids to archive/purge
                   select outpt_shipment_retn_id
                   bulk collect into arrIDs
                   from outpt_shipment_retn
                   where proc_stat_code >= 90
                   and last_updated_dttm <= dstart - p_days;
                   
                   purge_table ( p_table_name  => 'OUTPT_SHIPMENT_RETN'
                               , p_column_name => 'OUTPT_SHIPMENT_RETN_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
                   
                   dstart := systimestamp;
                   select outpt_delv_notification_id
                   bulk collect into arrIDs
                   from outpt_delv_notification
                   where proc_stat_code >= 90
                   and last_updated_dttm <= dstart - p_days;
                   
                   purge_table ( p_table_name  => 'OUTPT_DELV_NOTIFICATION'
                               , p_column_name => 'OUTPT_DELV_NOTIFICATION_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
   
               when p_purge_cd = '13' then -- Parcel Manifest Purge
                   dstart := systimestamp;
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Parcel Manifest Purge', action_name => 'Archiving and Purging');
   
                   select manifest_id
                   bulk collect into arrIDs
                   from manifest_hdr
                   where tc_company_id = p_company_id
                   and manifest_status_id = 90    -- closed manifests only
                   and last_updated_dttm <= dstart - p_days;
                   
                   for currec in (
                                   select 'MANIFESTED_LPN' TAB_NAME from dual
                                   union all
                                   select 'MANIFEST_HDR' TAB_NAME from dual
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'MANIFEST_ID'
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;                  
                   
               when p_purge_cd = '14' then -- Physical Count Tables
                   dstart := systimestamp;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Physical Count Purge', action_name => 'Archiving and Purging');
                   delete from TMP_WM_ID_1;
                   -- select the physical inventory header ids to purge
                   insert into TMP_WM_ID_1 (i_id)
                      /* select phys_invn_hdr_id
                       from phys_invn_hdr
                       where facility_id = p_whse_id
                       and stat_code >= 90  -- booked
                       and (
                           (bkd_date_time <= dstart - p_days)
                           or
                           ((mod_date_time <= dstart - p_days) and bkd_date_time is null)
                           );*/
      
						select p.phys_invn_hdr_id
                    from phys_invn_hdr p
                    where p.facility_id = 1
                    and p.stat_code >= 90  -- booked
                    and (
                        (p.bkd_date_time <= dstart - p_days)
                        or
                        ((p.mod_date_time <= dstart - p_days) and bkd_date_time is null)
                        )                    
                    and
                    (
                    exists (select 1 from FROZN_INVN_HDR fh,FROZN_INVN_DTL fd,ITEM_CBO i where fh.phys_invn_hdr_id = p.phys_invn_hdr_id
                            and fh.FROZN_INVN_HDR_ID = fd.FROZN_INVN_HDR_ID
                            and fd.item_id = i.item_id
                            and i.company_id = p_company_id)
                            or 
                            exists (
                            select 1 from cnt_invn_hdr ch, cnt_invn_dtl cd , item_cbo i 
                            where ch.phys_invn_hdr_id = p.phys_invn_hdr_id
                            and ch.CNT_INVN_HDR_ID = cd.CNT_INVN_HDR_ID
                            and cd.item_id = i.item_id
                            and i.company_id = p_company_id)
                            or
                            exists(
                            select 1 from cnt_invn_hdr ch, cnt_invn_dtl cd , item_cbo i 
                            where ch.phys_invn_hdr_id = p.phys_invn_hdr_id
                            and ch.CNT_INVN_HDR_ID = cd.CNT_INVN_HDR_ID
                            and cd.item_id = i.item_id
                            and i.company_id = p_company_id)
                            or
                            exists(
                            select 1 from FROZN_INVN_HDR fh, lpn l 
                            where fh.phys_invn_hdr_id = p.phys_invn_hdr_id
                            and fh.lpn_id = l.lpn_id
                            and l.tc_company_id = p_company_id)
                            or
                            exists(
                            select 1 from CNT_INVN_HDR ch, lpn l 
                            where ch.phys_invn_hdr_id = p.phys_invn_hdr_id
                            and ch.lpn_id = l.lpn_id
                            and l.tc_company_id = p_company_id)
                            );
	  
              -- get related cnt invn header recs to archive/purge
                   select cnt_invn_hdr_id
                   bulk collect into arrIDs_1
                   from cnt_invn_hdr, tmp_wm_id_1 t
                   where phys_invn_hdr_id = t.i_id;
                   
                   -- get physical inventory header ids
                   select i_id
                   bulk collect into arrIDs_2
                   from tmp_wm_id_1 t;
                   
                   -- get related frozen invn header recs and archive/purge                
                   select i.FROZN_INVN_DTL_ID
                   bulk collect into arrIDs
                   from frozn_invn_hdr s, tmp_wm_id_1 t1,FROZN_INVN_DTL i
                   where s.phys_invn_hdr_id = t1.i_id
                   and s.FROZN_INVN_HDR_ID = i.frozn_invn_hdr_id;
                   
                   purge_table ( p_table_name  => 'FROZN_INVN_SRL_NBR' 
                               , p_column_name => 'FROZN_INVN_DTL_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
               
                   -- get related frozen invn header recs and archive/purge
                   select frozn_invn_hdr_id
                   bulk collect into arrIDs
                   from frozn_invn_hdr, tmp_wm_id_1 t
                   where phys_invn_hdr_id = t.i_id;    
                   
                   for currec in (
                                   select 'FROZN_INVN_DTL' TAB_NAME from dual
                                   union all
                                   select 'FROZN_INVN_HDR' TAB_NAME from dual
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'FROZN_INVN_HDR_ID'
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;                  
   
                   select i.CNT_INVN_DTL_ID
                   bulk collect into arrIDs
                   from cnt_invn_hdr s, tmp_wm_id_1 t1, CNT_INVN_DTL i 
                   where s.phys_invn_hdr_id = t1.i_id
                   and s.CNT_INVN_HDR_ID = i.cnt_invn_hdr_id;
                   
                   
                   purge_table ( p_table_name  => 'CNT_INVN_SRL_NBR' 
                               , p_column_name => 'CNT_INVN_DTL_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
                   
                   for currec in (
                                   select 'CNT_INVN_DTL' TAB_NAME from dual
                                   union all
                                   select 'CNT_INVN_HDR' TAB_NAME from dual
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'CNT_INVN_HDR_ID'
                                   , p_arrids      => arrIDs_1
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;                   
                                  
                   -- get related invn vari header recs to archive/purge --MACR00390319
                   select invn_vari_hdr_id
                   bulk collect into arrIDs_1
                   from invn_vari_hdr, tmp_wm_id_1 t
                   where phys_invn_hdr_id = t.i_id;
                   
                   for currec in (
                                   select 'INVN_VARI_SRL_NBR' TAB_NAME from dual
                                   union all
                                   select 'INVN_VARI_HDR' TAB_NAME from dual
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'INVN_VARI_HDR_ID'
                                   , p_arrids      => arrIDs_1
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;                 
                   
                   for currec in (
                                   select 'FROZN_INVN_CONFIG' TAB_NAME from dual
                                   union all
                                   select 'PHYS_INVN_HDR' TAB_NAME from dual
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'PHYS_INVN_HDR_ID'
                                   , p_arrids      => arrIDs_2
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;                        
                     
               when p_purge_cd = '15' then -- Pick Location Details (dynamic)
                   dstart := systimestamp;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Pick Location Details Purge', action_name => 'Archiving and Purging');
                   select pick_locn_dtl_id
                   bulk collect into arrIDs
                   from pick_locn_dtl p
                   where exists (
                               select 1
                               from item_cbo i
                               where i.item_id = p.item_id
                               and i.company_id = p_company_id
                               )
                   and exists (
                               select 1
                               from locn_hdr i
                               where i.locn_id = p.locn_id
                               and i.locn_class in ('A','C')
                               and sku_dedctn_type = 'T'   -- T => Temporary
                               )
                   and ltst_sku_assign = 'N'
                   and mod_date_time <= dstart - p_days;
                   
                   purge_table ( p_table_name  => 'PICK_LOCN_DTL'
                               , p_column_name => 'PICK_LOCN_DTL_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;  
                   
               when p_purge_cd = '16' then -- PIX History
                   dstart := systimestamp;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'PIX History Purge', action_name => 'Archiving and Purging');
                   select pix_hist_id
                   bulk collect into arrIDs
                   from pix_hist p
                   where exists (
                               select 1
                               from item_cbo i
                               where i.item_id = p.item_id
                               and i.company_id = p_company_id
                               )
                   and whse = gv_whse
                   and mod_date_time <= dstart - p_days;
                   
                   purge_table ( p_table_name  => 'PIX_HIST'
                               , p_column_name => 'PIX_HIST_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;  
   
               when p_purge_cd = '17' then -- PIX Summary
                   dstart := systimestamp;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'PIX Summary Purge', action_name => 'Archiving and Purging');
                   select pix_summ_id
                   bulk collect into arrIDs
                   from pix_summ p
                   where exists (
                               select 1
                               from item_cbo i
                               where i.item_id = p.item_id
                               and i.company_id = p_company_id
                               )
                   and whse = gv_whse
                   and mod_date_time <= dstart - p_days;
                   
                   purge_table ( p_table_name  => 'PIX_SUMM'
                               , p_column_name => 'PIX_SUMM_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;  
   
               when p_purge_cd = '18' then -- PIXs
                   dstart := systimestamp;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'PIXs Purge', action_name => 'Archiving and Purging');
   
                   -- get list of purgable srl_pix_tran records to be purged. Performance improvement MACR00624890
                   select distinct spt.pix_Tran_id
                   bulk collect into arrIDs
                   from srl_pix_tran spt, pix_tran pt
                   where pt.tc_company_id = p_company_id
                   and pt.whse = gv_whse
                   and pt.proc_stat_code >= 90  -- processed
                   and pt.mod_date_time <= dstart - p_days
                   and pt.pix_tran_id = spt.pix_tran_id;
   
   
                   
                   purge_table ( p_table_name  => 'SRL_PIX_TRAN'
                               , p_column_name => 'PIX_TRAN_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;  
                   
                   -- get list of purgable srl_pix_tran records to be purged. changed MACR00624890
                   select pix_tran_id
                   bulk collect into arrIDs
                   from pix_tran
                   where tc_company_id = p_company_id
                   and whse = gv_whse
                   and proc_stat_code >= 90  -- processed
                   and mod_date_time <= dstart - p_days;
   
                   purge_table ( p_table_name  => 'PIX_TRAN'
                               , p_column_name => 'PIX_TRAN_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;  
   
               when p_purge_cd = '19' then -- Question/Answer Table
                   dstart := systimestamp;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Question Answer Purge', action_name => 'Archiving and Purging');
                   select question_ans_id
                   bulk collect into arrIDs
                   from question_ans
                   where company_id = p_company_id
                   and whse = gv_whse
                   and stat_code >= 90  -- answered
                   and audit_last_updated_dttm <= dstart - p_days;
                   
                   for currec in (
                                   select 'QUESTION_ANS_FILTER_HISTORY' TAB_NAME,'QUE_ANS_ID' COL_NAME from dual
                                   union all
                                   select 'QUESTION_ANS' TAB_NAME,'QUESTION_ANS_ID' COL_NAME from dual
                                 )
                   loop           
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => currec.col_name
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;                   
   
               when p_purge_cd = '20' then -- Question Master
                   dstart := systimestamp;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Question Master Purge', action_name => 'Archiving and Purging');
                   select q.question_ans_id
                   bulk collect into arrIDs
                   from question_ans q
                   where exists (
                               select 1
                               from question_master m
                               where m.ques_master_id = q.ques_master_id
                               and m.company_id = p_company_id
                               and ( m.mark_for_deletion = 1
                                   or
                                   audit_last_updated_dttm <= systimestamp - p_days)
                               );
                               
                   for currec in (
                                   select 'QUESTION_ANS_FILTER_HISTORY' TAB_NAME,'QUE_ANS_ID' COL_NAME from dual
                                   union all
                                   select 'QUESTION_ANS' TAB_NAME,'QUESTION_ANS_ID' COL_NAME from dual
                                 )
                   loop           
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => currec.col_name
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;                               
   
                   select ques_master_id
                   bulk collect into arrIDs
                   from question_master
                   where company_id = p_company_id
                   and (
                       mark_for_deletion = 1
                       and
                       audit_last_updated_dttm <= systimestamp - p_days
                       );
                       
                   for currec in (
                                   select 'QUESTION_DETRM' TAB_NAME from dual
                                   union all
                                   select 'QUESTION_MASTER' TAB_NAME from dual
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'QUES_MASTER_ID'
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;                      
   
               when p_purge_cd = '21' then -- Returnables
                   -- in-bound
                   dstart := systimestamp;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Returnables Purge', action_name => 'Archiving and Purging');
                   select retn_ib_hist_id
                   bulk collect into arrIDs
                   from retn_ib_hist
                   where whse = gv_whse
                   and mod_date_time <= dstart - p_days
                   and not exists (
                                   select 1
                                   from asn
                                   where tc_asn_id = shpmt_nbr
                                   );
                                   
                   purge_table ( p_table_name  => 'RETN_IB_HIST'
                               , p_column_name => 'RETN_IB_HIST_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
                   
                   -- out-bound
                   dstart := systimestamp;
                   select retn_ob_hist_id
                   bulk collect into arrIDs
                   from retn_ob_hist o
                   where whse = gv_whse
                   and mod_date_time <= dstart - p_days
                   and not exists (
                                   select 1
                                   from shipment i
                                   where tc_shipment_id = o.load_nbr
                                   );
                                   
                   purge_table ( p_table_name  => 'RETN_OB_HIST'
                               , p_column_name => 'RETN_OB_HIST_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
   
               when p_purge_cd = '22' then -- RF Message
                   dstart := systimestamp;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'RF Message Purge', action_name => 'Archiving and Purging');
                   select rf_msg_id
                   bulk collect into arrIDs
                   from rf_message
                   where tc_company_id = p_company_id
                   and whse = gv_whse
                   and mod_date_time <= dstart - p_days;
                   
                   for currec in (
                                   select 'RF_MESSAGE_RESPONSE' TAB_NAME from dual
                                   union all
                                   select 'RF_MESSAGE' TAB_NAME from dual
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'RF_MSG_ID'
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;                  
    
               when p_purge_cd = '23' then -- Serial Numbers
                   dstart := systimestamp;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Serial Numbers Purge', action_name => 'Archiving and Purging');
                   -- get record ids to archive
                   select o.srl_nbr_trk_id
                   bulk collect into arrIDs
                   from srl_nbr_track o
                   where exists (
                               select 1
                               from item_cbo i
                               where i.item_id = o.item_id
                               and i.company_id = p_company_id
                               )
                   and facility_id = p_whse_id
                   and stat_code >= 90  -- canceled
                   and last_updated_dttm <= dstart - p_days;
                   
                   for currec in (
                                   select 'FROZN_INVN_SRL_NBR' TAB_NAME from dual
                                   union all
                                   select 'MINOR_SRL_TRACK' TAB_NAME from dual
                                   union all
                                   select 'SRL_NBR_TRACK' TAB_NAME from dual
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'SRL_NBR_TRK_ID'
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;                  
       
               when p_purge_cd = '24' then -- SmartLabel
                   dstart := sysdate;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'SmartLabel Purge', action_name => 'Archiving and Purging');
   
                   -- use smartlabel_brcd to archive/purge dependencies
                      
                   delete from tmp_wm_id_1;
                   insert into tmp_wm_id_1(v_id)
                   select distinct smartlabel_brcd
                   from smartlabel_carton_hdr_hist
                   where cd_master_id = p_company_id
                   and whse = gv_whse
                   and mod_date_time <= dstart - p_days;
                   
                   select SMARTLABEL_BRCD
                   bulk collect into varrIDs
                   from SMARTLABEL_SKU_HIST,tmp_wm_id_1
                   where SMARTLABEL_BRCD = v_id
                   and not exists
                   (
                     SELECT 1
                     FROM ORDERS i
                     WHERE PKT_CTRL_NBR = to_char(i.ORDER_ID)
                   );                
                   
                   purge_table ( p_table_name  => 'SMARTLABEL_SKU_HIST'
                               , p_column_name => 'SMARTLABEL_BRCD'
                               , p_varrids      => varrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
                   
                   select d.smartlabel_arn_event_hdr_id
                   bulk collect into varrIDs
                   from SMARTLABEL_ARN_EVENT_DTL d,tmp_wm_id_1
                   WHERE EXISTS (
                                   SELECT 1
                                   FROM smartlabel_arn_event_hdr h
                                   WHERE h.smartlabel_brcd = v_id
                                   AND h.smartlabel_arn_event_hdr_id = d.smartlabel_arn_event_hdr_id
                                   );    
   
                   purge_table ( p_table_name  => 'SMARTLABEL_ARN_EVENT_DTL'
                               , p_column_name => 'smartlabel_arn_event_hdr_id'
                               , p_varrids      => varrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;                                    
                   
                   select v_id
                   bulk collect into varrIDs
                   from tmp_wm_id_1;
                   
                   purge_table ( p_table_name  => 'SMARTLABEL_ARN_EVENT_HDR'
                               , p_column_name => 'SMARTLABEL_BRCD'
                               , p_varrids      => varrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;                
   
                   -- now use primary key to archive/purge from parent table
                   dstart := sysdate;
                   
                   select smartlabel_carton_hdr_hist_id
                   bulk collect into arrIDs
                   from smartlabel_carton_hdr_hist,tmp_wm_id_1
                   where smartlabel_brcd=v_id;
   
                   purge_table ( p_table_name  => 'SMARTLABEL_CARTON_HDR_HIST'
                               , p_column_name => 'SMARTLABEL_CARTON_HDR_HIST_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;                    
   
                   i_rows_purged := i_rows_purged + i_rc;
                   -- end smartlabel tables
   
               when p_purge_cd = '25' then -- Task Parm/Event Parm/Rule Tables
                   -- task_parm tables
                   dstart := systimestamp;
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Task Parm Purge', action_name => 'Archiving and Purging');
   
                   -- generate id list
                   insert into gtmp_arch_id_list (col_name, i_id)
                   with task_parm_with as
                               (
                               select t.task_parm_id
                               from task_parm t
                               where t.whse = gv_whse
                               and t.rec_type = 'N'
                               and t.mod_date_time <= dstart - p_days
                               and not exists (select 1 from task_hdr th where th.task_parm_id = t.task_parm_id)
                               ),
                        event_parm_with as
                               (
                               select event_parm_id
                               from event_parm
                               where whse = gv_whse
                               and rec_type = 'A'
                               and mod_date_time <= dstart - p_days
                               )
                   select 'task_parm_id', task_parm_id
                   from task_parm_with
                       union all
                   select 'tp_rule_id', r.rule_id  -- rule ids for task rule parm
                   from rule_hdr r, task_rule_parm otrp, task_parm_with t
                   where r.rule_id = otrp.rule_id
                   and t.task_parm_id = otrp.task_parm_id  -- rule belongs to a purge-able task_parms
                   and otrp.mod_date_time <= dstart - p_days
                       union all
                   select 'event_parm_id', event_parm_id
                   from event_parm_with
                       union all
                   select 'ep_rule_id', r.rule_id  -- rule ids for event rule parm
                   from rule_hdr r, event_rule_parm oerp, event_parm_with t
                   where r.rule_id = oerp.rule_id
                   and t.event_parm_id = oerp.event_parm_id  -- rule belongs to purge-able event_parms
                   and oerp.mod_date_time <= dstart - p_days;
                                   
                   -- arch_prod_trkg_tran ,arch_task_dtl ,arch_task_hdr needs to be removed from purge code 25
                   -- as these are taken care in Purge code 27 Tasks Purge #MACR00797191
                   
                   select i_id bulk collect into arrIDs
                   from gtmp_arch_id_list where col_name = 'event_parm_id';
   
                   
                   for currec in ( select 'EVENT_RULE_ATTRIB' TAB_NAME from dual
								   union all
                                   select 'EVENT_RULE_PARM' TAB_NAME from dual
                                   union all
                                   select 'EVENT_PARM' TAB_NAME from dual
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'EVENT_PARM_ID'
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;                  
   
                   -- get ids where task_parm_id is in task_rule_parm
                   select distinct task_prt_sort_dtl_id bulk collect into arrIDs
                   from gtmp_arch_id_list, task_rule_parm r, task_prt_sort_dtl d
                   where col_name = 'task_parm_id'
                   and d.rule_id = r.rule_id
                   and r.task_parm_id = i_id;
   
                   purge_table ( p_table_name  => 'task_prt_sort_dtl'
                               , p_column_name => 'task_prt_sort_dtl_id'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
   
                   select i_id bulk collect into arrIDs
                   from gtmp_arch_id_list where col_name = 'task_parm_id';
                   
                   for currec in (
                                   select 'task_rule_parm' TAB_NAME from dual
                                   union all
                                   select 'task_parm' TAB_NAME from dual
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'task_parm_id'
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;                  
   
                   -- Commented the select statement below for "MACR00386759" and added a new one with "in" clause to avoid calling the procedure twice.
                   --select i_id bulk collect into arrIDs
                   --from gtmp_arch_id_list where col_name = 'tp_rule_id';
                   select i_id bulk collect into arrIDs
                   from gtmp_arch_id_list where col_name in ('tp_rule_id','ep_rule_id')  --MACR00390319 Changed the string 'task_parm_id' which was incorrect to 'ep_rule_id'.
                   and i_id not in (select RULE_HDR_ID from task_rule_parm); --WM-40677
                   
                   
                   for currec in (
                                   select 'rule_sort_dtl' TAB_NAME from dual
                                   union all
                                   select 'rule_sel_dtl' TAB_NAME from dual
                                   union all
                                   select 'OUTWARD_EVENT_SUBSCRIBERS' TAB_NAME from dual
                                   union all
                                   select 'rule_hdr' TAB_NAME from dual
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'rule_id'
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;                 
   
               when p_purge_cd = '26' then -- Frozen Inventory
                   dstart := systimestamp;
                   -- get frozn record ids to archive
                   
   				DBMS_APPLICATION_INFO.set_module(module_name => 'Frozen Inventory Purge', action_name => 'Archiving and Purging');
                   
                   delete from tmp_wm_id_1;
                   insert into tmp_wm_id_1 (i_id)
                   select frozn_invn_hdr_id
                   from frozn_invn_hdr
                   where whse = gv_whse
                   and stat_code = 99
                   and mod_date_time <= dstart-p_days;
   
                   select i.FROZN_INVN_DTL_ID
                   bulk collect into arrIDs
                   from tmp_wm_id_1 t , FROZN_INVN_DTL i
                   where i.FROZN_INVN_HDR_ID = t.i_id;
                   
                   purge_table ( p_table_name  => 'FROZN_INVN_SRL_NBR' 
                               , p_column_name => 'FROZN_INVN_DTL_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
                   
                   select i_id
                   bulk collect into arrIDs
                   from tmp_wm_id_1;     
                   
                   for currec in (
                                   select 'FROZN_INVN_DTL' TAB_NAME from dual
                                   union all
                                   select 'FROZN_INVN_HDR' TAB_NAME from dual
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'FROZN_INVN_HDR_ID'
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;                  
   
               when p_purge_cd = '27' then -- Tasks and Allocations
                   dstart := systimestamp;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Tasks Purge', action_name => 'Archiving and Purging');
                   -- get purge-able task_hdr ids
                   delete from tmp_wm_id_4; --WM-55939 adding task_id to temp table.
                   
                   insert into tmp_wm_id_4 ( i_id1, i_id2)
                   with purgeable_tasks as
                     (
                     select o.task_hdr_id, o.task_id, o.orig_task_id
                      from task_hdr o
                      left outer join task_dtl d
                     on d.task_hdr_id = o.task_hdr_id 
                     where whse = gv_whse
                     and o.stat_code >= 90  -- completed
                     and o.mod_date_time <= dstart - p_days
                     and ( d.tc_order_id is NULL 
                           or not exists (select NULL from orders o1 where o1.tc_order_id = d.tc_order_id and DO_STATUS < 200 ))      --WM-46790
                     ) -- WM-44251  
                     select task_hdr_id,task_id
                     from purgeable_tasks o1
                     where exists (  -- the orig_task_ids must be purge-able
                                   select 1
                                   from purgeable_tasks i1
                                   where i1.task_id = o1.orig_task_id
                                   )
                     or o1.orig_task_id is null;
   
                   -- WM-44251 
                   select distinct(p.prod_trkg_tran_id)
                   bulk collect into arrIDs
                   from prod_trkg_tran p
                       ,tmp_wm_id_4 t
                   where p.task_id = t.i_id2;       -- purge-able task_hdr_ids   
              
                   
                   purge_table ( p_table_name  => 'PROD_TRKG_TRAN'
                               , p_column_name => 'PROD_TRKG_TRAN_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);          
                   i_rows_purged := i_rows_purged + i_rc;                
   
                   -- task_dtl
                 select distinct d.task_dtl_id
                 bulk collect into arrIDs
                 from task_dtl d
                    join tmp_wm_id_4 t
                    on (d.task_hdr_id = t.i_id1 or d.task_id = t.i_id2)
                    where  d.tc_order_id is null or  not exists (select NULL from orders o1 where o1.tc_order_id = d.tc_order_id and DO_STATUS < 200 ); 
   
                   purge_table ( p_table_name  => 'TASK_DTL'
                               , p_column_name => 'TASK_DTL_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   
                   -- task_hdr
                   select distinct(i_id1)
                   bulk collect into arrIDs
                   from tmp_wm_id_4;
   
                   -- MACR00440675
                   set_taskhdr_origtaskid_to_null(arrIDs);
                   purge_table ( p_table_name  => 'TASK_HDR'
                               , p_column_name => 'TASK_HDR_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
   
               when p_purge_cd = '28' then -- UPS PLD Transmission Responses
                   dstart := systimestamp;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'UPS PLD Transmission Response Purge', action_name => 'Archiving and Purging');
                   select ups_emt_upload_rpt_id
                   bulk collect into arrIDs
                   from ups_emt_upload_rpt
                   where mod_date_time <= dstart - p_days;
                   
                   purge_table ( p_table_name  => 'UPS_EMT_UPLOAD_RPT'
                               , p_column_name => 'ups_emt_upload_rpt_id'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   
               when p_purge_cd = '29' then -- Vendor Performance Tran Purge
                   dstart := systimestamp;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Vendor Performance Purge', action_name => 'Archiving and Purging');
                   select vend_perf_tran_id
                   bulk collect into arrIDs
                   from vend_perf_tran
                   where cd_master_id = p_company_id
                   and whse = gv_whse
                   and stat_code >= 90  -- processed
                   and mod_date_time <= dstart - p_days;
                   
                   purge_table ( p_table_name  => 'VEND_PERF_TRAN'
                               , p_column_name => 'VEND_PERF_TRAN_ID'
                               , p_arrids      => arrIDs 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
               
               when p_purge_cd = '30' then -- Wave Related Data
                   dstart := systimestamp;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Wave Purge', action_name => 'Archiving and Purging');
                   -- ship_wave_parm_ids
                   delete from tmp_wm_id_1;
                   insert into tmp_wm_id_1 (i_id)
                       select ship_wave_parm_id
                       from ship_wave_parm
                       where whse = gv_whse
                       and rec_type <> 'T'
                       and mod_date_time <= dstart - p_days
                       and stat_code >= 90
					   and exists(select 1 from lpn l where ship_wave_nbr = l.wave_nbr 
						   and l.tc_company_id = p_company_id)
					   and not exists (select 1 from picking_short_item p where p.stat_code < 90 
					   and p.wave_nbr = ship_wave_parm.SHIP_WAVE_NBR);
   
                   -- get the ship_wave_parm_ids
                   select i_id
                   bulk collect into arrIDs
                   from tmp_wm_id_1;
   
                   -- get the purge-able rule_ids
                   with wp_ids
                   as
                       (
                       select rule_id
                       from wave_rule_parm, tmp_wm_id_1
                       where i_id = wave_parm_id
                       )
                   select distinct w.rule_id
                   bulk collect into arrIDs_4
                   from wp_ids w
                   where not exists (  -- exclude rule_ids with child rows
                                   select 1
                                   from (
                                       select rule_id from task_rule_parm
                                       union
                                       select rule_id from event_rule_parm
                                       union
                                       select rule_id from outward_event_subscribers
                                       union
                                       select rule_id from rule_sel_col_dtl
                                       union
                                       select rule_id from task_prt_sort_dtl
                                       union
                                       select rule_id from mhe_parm_dtl
                                       union
                                       select rule_id from wave_rule_parm wrp
                                       where not exists (select 1 from tmp_wm_id_1 where i_id = wrp.wave_parm_id)
                                       ) rid
                                   where rid.rule_id = w.rule_id
                                   );
   
                   -- get wave_queue_ids
                   select wave_queue_id
                   bulk collect into arrIDs_2
                   from wave_queue, tmp_wm_id_1
                   where ship_wave_parm_id = i_id
                   and wave_queue_id is not null;
   
                   -- get wave_nbrs for pkt_consol_locn
                   select distinct ship_wave_nbr
                   bulk collect into varrIDs
                   from ship_wave_parm s , tmp_wm_id_1 t, pkt_consol_locn p
                   where s.ship_wave_parm_id = t.i_id
                   and s.ship_wave_nbr is not null     
                   and s.ship_wave_nbr    = p.WAVE_NBR 
                   and p.REC_TYPE = 'W';
   
                   -- get the pnh_parm_ids associated with target ship_wave_parm_ids
                   select distinct pnh_parm_id
                   bulk collect into arrIDs_3
                   from ship_wave_parm o, tmp_wm_id_1
                   where ship_wave_parm_id = i_id
                   and pnh_parm_id is not null
                   and not exists (
                                   select 1        -- but no others
                                   from ship_wave_parm i
                                   where not exists (
                                                       select 1
                                                       from tmp_wm_id_1
                                                       where i_id = i.ship_wave_parm_id
                                                       )
                                   and i.pnh_parm_id = o.pnh_parm_id
                                   );
   
                   -- wave_work_type_stats
                   purge_table ( p_table_name  => 'wave_work_type_stats' -- wave_work_type_stats.wave_parm_id=ship_wave_parm.ship_wave_parm_id
                               , p_column_name => 'WAVE_PARM_ID'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
   
                   -- get the ship_wave_parm_ids
                   select distinct i_id
                   bulk collect into arrIDs_6
                   from tmp_wm_id_1 t, wave_task_crit w
                   where t.i_id = w.wave_parm_id  
                   and w.WAVE_NBR is not null -- adding the changes related to MACR00845314 WAVE purge issues
                   and w.REC_TYPE <> 'T'; 
                   
                   -- wave_task_crit
                   purge_table ( p_table_name  => 'wave_task_crit' 
                               , p_column_name => 'wave_parm_id'
                               , p_arrids      => arrIDs_6
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;                
   
                   -- pack_wave_parm_hdr and pack_wave_parm_dtl 
                 -- WM-43274 wave purge deleting active records from pack_wave_parm_hdr and pack_wave_parm_dtl
                   select distinct h.PACK_WAVE_PARM_HDR_ID
                   bulk collect into arrIDs_5
                   from ship_wave_parm sw, tmp_wm_id_1 t , wave_parm w ,pack_wave_parm_hdr h
                   where sw.ship_wave_parm_id = t.i_id
                   and sw.wave_parm_id = w.wave_parm_id
                   and w.pack_wave_parm_id = h.PACK_WAVE_PARM_ID
                   and h.rec_type <> 'T'; --WM-46528  
                   
                   purge_table ( p_table_name  => 'pack_wave_parm_dtl'
                               , p_column_name => 'PACK_WAVE_PARM_HDR_ID'
                               , p_arrids      => arrIDs_5 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
                   
   
                   purge_table ( p_table_name  => 'pack_wave_parm_hdr'
                               , p_column_name => 'PACK_WAVE_PARM_HDR_ID'
                               , p_arrids      => arrIDs_5 
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
   
                   -- purge wave_queue data
                   for currec in (
                                   select 'wave_queue_work_ord' TAB_NAME from dual
                                   union all
                                   select 'wave_queue_pkt' TAB_NAME from dual
                                   union all
                                   select 'wave_queue_load' TAB_NAME from dual
                                   union all
                                   select 'wave_queue' TAB_NAME from dual
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'wave_queue_id'
                                   , p_arrids      => arrIDs_2 -- !! wave_rule_parm.wave_parm_id = ship_wave_parm_id !!
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;                  
                   
                   -- pkt_consol_locn
                   purge_table ( p_table_name  => 'pkt_consol_locn'
                               , p_column_name => 'WAVE_NBR'
                               , p_varrids      => varrIDs -- wave number ids
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;    
   
   
                   -- wave_rule_parm
                   purge_table ( p_table_name  => 'wave_rule_parm'
                               , p_column_name => 'wave_parm_id'
                               , p_arrids      => arrIDs -- !! wave_rule_parm.wave_parm_id = ship_wave_parm_id !!
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   
                    -- wave rule parm rule_hdr ids
                   for currec in (
                                   select 'rule_sort_dtl' TAB_NAME from dual
                                   union all
                                   select 'rule_sel_dtl' TAB_NAME from dual
                                   union all
                                   select 'OUTWARD_EVENT_SUBSCRIBERS' TAB_NAME from dual
                                   union all
                                   select 'rule_hdr' TAB_NAME from dual
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'rule_id'
                                   , p_arrids      => arrIDs_4
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;  
                   
   				
                   -- get the wave_parm_ids to purge wave_parm
                   select distinct wave_parm_id
                   bulk collect into arrIDs_1
                   from ship_wave_parm w, tmp_wm_id_1 t
                   where w.ship_wave_parm_id = t.i_id
                   and wave_parm_id is not null;
   				
                   -- ship_wave_parm and pnh_parm_ids
                   purge_table ( p_table_name  => 'ship_wave_parm'
                               , p_column_name => 'ship_wave_parm_id'
                               , p_arrids      => arrIDs -- ship wave parm ids
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;                    
                   
                   -- pnh_parms
                   purge_table ( p_table_name  => 'pnh_parm'
                               , p_column_name => 'pnh_parm_id'
                               , p_arrids      => arrIDs_3 -- pnh parm ids
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;   
                   
                   -- wave_parm (and rule tables?)
                   purge_table ( p_table_name  => 'wave_parm'
                               , p_column_name => 'wave_parm_id'
                               , p_arrids      => arrIDs_1 -- wave parm ids
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
   
                   --invc_parm related data
                                   
                   dstart := systimestamp;
                   -- get invc_parm_ids
                   delete from tmp_wm_id_1;
                   insert into tmp_wm_id_1 (i_id)
                   select invc_parm_id
                   from invc_parm
                   where whse = gv_whse
                   and rec_type = 'A'
                   and mod_date_time <= dstart - p_days;
   
                   -- get the invc_parm_ids
                   select i_id
                   bulk collect into arrIDs
                   from tmp_wm_id_1;
   
                   -- get the purge-able rule_ids
                   select distinct w.rule_id
                   bulk collect into arrIDs_4          
                   from invc_rule_parm w
                   join tmp_wm_id_1 t on t.i_id = w.invc_parm_id
                   where not exists (  -- exclude rule_ids with child rows
                                   select 1
                                   from (
                                       select rule_id from task_rule_parm
                                       union
                                       select rule_id from event_rule_parm
                                       union
                                       select rule_id from outward_event_subscribers
                                       union
                                       select rule_id from rule_sel_col_dtl
                                       union
                                       select rule_id from task_prt_sort_dtl
                                       union
                                       select rule_id from mhe_parm_dtl
                                       union
                                       select rule_id from wave_rule_parm wrp
                                       where not exists (select 1 from tmp_wm_id_1 where i_id = wrp.wave_parm_id)
                                       ) rid
                                   where rid.rule_id = w.rule_id
                                   );
   
                   -- invc_parm_ids invc_parm table and invc_rule_pa
                   for currec in (
                                   select 'invc_rule_parm' TAB_NAME from dual
                                   union all
                                   select 'invc_parm' TAB_NAME from dual
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'invc_parm_id'
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;                  
   
                   -- invc rule parm rule_hdr ids   
                   for currec in (
                                   select 'rule_sort_dtl' TAB_NAME from dual
                                   union all
                                   select 'rule_sel_dtl' TAB_NAME from dual
                                   union all
                                   select 'OUTWARD_EVENT_SUBSCRIBERS' TAB_NAME from dual
                                   union all
								   --select 'RULE_PARM' TAB_NAME from dual
                                   --union all -- not applicable to 2016
                                   select 'rule_hdr' TAB_NAME from dual
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'rule_id'
                                   , p_arrids      => arrIDs_4
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;      
   
                   -- (WM-33980) adding CONS_RUN / CONS_RUN_DTL tables.
                   select CONS_RUN_ID 
                   bulk collect into arrIDs
                   from CONS_RUN 
                   where last_updated_dttm  <= dstart - p_days
                   and tc_company_id =  p_company_id
                   and cons_run_status in ('Complete','Failed','Rejected');
                   
                   purge_table ( p_table_name  => 'CONS_RUN_DTL'
                               , p_column_name => 'CONS_RUN_ID'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
   
                   purge_table ( p_table_name  => 'CONS_RUN'
                               , p_column_name => 'CONS_RUN_ID'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
   
               when p_purge_cd = '31' then -- Work Order Purge
                   dstart := sysdate;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Work Order Purge', action_name => 'Archiving and Purging');
                   select work_ord_nbr
                   bulk collect into varrIDs
                   from work_ord_hdr
                   where cd_master_id = p_company_id
                   and whse = gv_whse
                   and stat_code >= 90  -- closed
                   and create_date_time <= dstart - p_days;
                   
                   for currec in (
                                   select 'WORK_ORD_DTL' TAB_NAME from dual
                                   union all
                                   select 'WORK_ORD_VAS' TAB_NAME from dual
                                   union all
                                   select 'WORK_ORD_WORK_TYPE' TAB_NAME from dual
                                   union all
                                   select 'WORK_ORD_CMNT' TAB_NAME from dual
                                   union all
                                   select 'WAVE_QUEUE_WORK_ORD' TAB_NAME from dual
                                   union all
                                   select 'BATCH_HIST_WORK_ORD' TAB_NAME from dual    
                                   union all
                                   select 'WORK_ORD_HDR' TAB_NAME from dual 
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'WORK_ORD_NBR'
                                   , P_vARRIDS      => varrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;                  
   
               when p_purge_cd = '32' then -- Purchase Order Purge
                   dstart := sysdate;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Purchase Order Purge', action_name => 'Archiving and Purging');
   
                   archive_purge_log_dpndt_data(p_archive_flag);  
                   delete from gtmp_arch_id_list;
                   insert into gtmp_arch_id_list (col_name, i_id,vc_id)
                       select 'purchase_orders_id', purchase_orders_id,tc_purchase_orders_id
                       from (
                               -- select pos eligible to purge
                               with po_base
                               as
                                   (
                                   select purchase_orders_id, tc_purchase_orders_id
                                   from purchase_orders
                                   where purchase_orders_status >= 940
                                   and tc_company_id = p_company_id
                                   and LAST_UPDATED_DTTM  <= dstart - p_days   --WM-60782
                                   )
                               select pb.purchase_orders_id,tc_purchase_orders_id
                               from po_base pb
                               where not exists 
                               (  -- Jira: WM-40672 exclude purchase_order_id associated with OPEN APPOINTMENTS 
                                  select 1
                                  from ILM_APPOINTMENT_OBJECTS
                                  where ILM_APPOINTMENT_OBJECTS.appt_obj_id = pb.purchase_orders_id
                                  and ILM_APPOINTMENT_OBJECTS.appt_obj_type=40 
                                )
                               minus
                               -- exclude pos where asn status is < receiving verified
                               select distinct pb.purchase_orders_id,pb.tc_purchase_orders_id
                               from po_base pb
                                   ,asn_detail ad
                                   ,asn a
                               where pb.purchase_orders_id = ad.purchase_orders_id
                               and a.asn_id = ad.asn_id
                               and a.asn_status < 40
                               minus
                               -- exclude pos that have orders
                               select purchase_orders_id,tc_purchase_orders_id
                               from po_base pb
                               where exists (
                                           select 1
                                           from orders o
                                           where o.purchase_order_id = pb.purchase_orders_id
                                           )
                               );
   
                   -- WM-40829: Purge ilm appointments first
                   purge_appointments (pa_company_id       => p_company_id
                                      ,pa_whse_id          => p_whse_id
                                      ,pa_commit_size      => p_commit_size
                                      ,pa_days             => p_days
                                      ,pa_purge_cd         => p_purge_cd
                                      ,pa_archive_flag     => p_archive_flag
                                      ,pa_child_comp_flag  => p_child_comp_flag
                                      ,pa_start_time       => dstart
                                      ,pa_rows_purged      => i_rc);
                   i_rows_purged := i_rows_purged + i_rc;
   
                   -- get the tc_purchase_order_ids
                      
                   -- select lpn ids to purge associated with eligible purchase_orders
                   -- get the purge-able lpns based on POs above

                               
                   delete from tmp_wm_arch_lpn_1;                                      
                   insert into tmp_wm_arch_lpn_1 (i_id, v_id,io_indicator)
                   select distinct l.lpn_id,l.tc_lpn_id,l.inbound_outbound_indicator
                   from gtmp_arch_id_list,lpn_detail ld, lpn l
                   where ld.tc_purchase_orders_id = vc_id
                   and l.lpn_id = ld.lpn_id
                               and l.order_id is null
                               and ( (l.tc_purchase_orders_id = vc_id or l.tc_purchase_orders_id  is null) and l.tc_company_id=p_company_id) -- WM-35115 PO purge to purge LPNs which has null tc_puchase_orders_id
                               and l.inbound_outbound_indicator = 'I'  --MACR00786428
                               and (l.lpn_facility_status < 10 or l.lpn_facility_status >= 92)
                   and l.last_updated_dttm <= (dstart - p_days );
                                              
   
                   purge_lpns(pl_company_id       => p_company_id
                           ,pl_whse_id          => p_whse_id
                           ,pl_commit_size      => p_commit_size
                           ,pl_days             => p_days
                           ,pl_purge_cd         => p_purge_cd
                           ,pl_archive_flag     => p_archive_flag
                           ,pl_child_comp_flag  => p_child_comp_flag
                           ,pl_rows_purged      => i_rc);
                   i_rows_purged := i_rows_purged + i_rc;
                   
                   --DB-701
                   merge into purge_log_dpndt_data pld 
                   using ( select 'LPN' object_dpndt,l.lpn_id object_pk,l.asn_id asn_id ,l.tc_asn_id tc_asn_id,l.purchase_orders_id purchase_orders_id
                             from asn_detail ad
                                  ,gtmp_arch_id_list
                                  ,lpn l
                            where ad.tc_purchase_orders_id = vc_id
                   and l.asn_id = ad.asn_id ) t
                   on (pld.object_dpndt = t.object_dpndt
                       and pld.OBJECT_PK = t.object_pk)
                   when matched
                   then
                      update 
                      set pld.asn_id = t.asn_id,
                          pld.tc_asn_id = t.tc_asn_id,
                          pld.purchase_orders_id = t.purchase_orders_id,
                          pld.is_nullfied = p_delink_child,
                          pld.PURGE_CODE = p_purge_cd,
                          pld.run_time = dstart
                  when not matched
                  then
                     insert (OBJECT_DPNDT,OBJECT_PK,ASN_ID,tc_asn_id,PURCHASE_ORDERS_ID,is_nullfied,PURGE_CODE,RUN_TIME)  
                     values (t.object_dpndt,t.object_pk,t.asn_id,t.tc_asn_id,t.purchase_orders_id,p_delink_child,p_purge_cd,dstart);    
   
                   if p_delink_child ='Y'
                   then
                   --WM-39208 Update ASN_ID to NULL for non-purgable LPNs associated with purgable ASNs.
                      update lpn l
                      set l.asn_id = null,
                          l.last_updated_dttm = sysdate,
                          l.last_updated_source = 'wm_archive_pkg' 
                      where exists
                      (select null
                          from asn_detail ad
                              ,gtmp_arch_id_list
                          where ad.tc_purchase_orders_id = vc_id
                          and l.asn_id = ad.asn_id);                    
      
                      --WM-55934 Update ASN_ID to NULL for non-purgable LPN_MOVEMENTs associated with purgable ASNs.
                      update LPN_MOVEMENT lm
                      set lm.asn_id = null,
                          lm.last_updated_dttm = sysdate,
                          lm.last_updated_source = 'wm_archive_pkg' 
                      where exists
                      (select null
                          from asn_detail ad
                              ,gtmp_arch_id_list
                          where ad.tc_purchase_orders_id = vc_id
                          and lm.asn_id = ad.asn_id);
						  
				      -- Update ASN_DETAIL to NULL for non-purgable LPN's in the LPN_DETAIL table	--WM-110355
                      update LPN_DETAIL ld
                      set ld.asn_dtl_id = NULL,
                          ld.last_updated_dttm = sysdate,
                          ld.last_updated_source = 'wm_archive_pkg'
                      where exists (
                                    select NULL
                                    from gtmp_arch_id_list tmp, asn_detail ad
                                    where ld.asn_dtl_id = ad.asn_detail_id
                                    and ad.tc_purchase_orders_id = tmp.vc_id);
                          
                    else
                       delete from gtmp_arch_id_list
                       where exists 
                              ( select ad.asn_id
                                 from asn_detail ad
                                     ,lpn l
                                where ad.tc_purchase_orders_id = vc_id
                                and l.asn_id = ad.asn_id 
                                union all
                                select ad.asn_id
                                 from asn_detail ad
                                     ,LPN_MOVEMENT lm
                                where ad.tc_purchase_orders_id = vc_id
                                and lm.asn_id = ad.asn_id );                   
                    end if;              
                       
                   -- WM-71477    
                 -- get the asn ids to purge
                 
                   delete from TMP_WM_ID_4;
                   insert into TMP_WM_ID_4(i_id1,i_id2)       
                   select asn_id, asn_detail_id
                    from asn_detail ad
                        ,gtmp_arch_id_list
                    where ad.tc_purchase_orders_id = vc_id;                    
   
                   -- get the asn ids to purge
                   select distinct asn_id bulk collect into arrIDs
                   from (
                       select asn_id
                       from asn_detail ad
                           ,gtmp_arch_id_list
                       where ad.tc_purchase_orders_id = vc_id
                       and ad.tc_purchase_orders_id is not null
                       minus 
                       select ad.asn_id 
                       from asn a , asn_detail ad , TMP_WM_ID_4 t
                       where a.asn_id = ad.asn_id
                       and ad.asn_id = t.i_id1
                       and not exists ( select 1 from TMP_WM_ID_4 ti
                                        where ad.asn_detail_id = ti.i_id2)
                       );
   
                   purge_asns ( pa_arr_asn_ids     => arrIDs
                               ,pa_company_id      => p_company_id
                               ,pa_whse_id         => p_whse_id
                               ,pa_commit_size     => p_commit_size
                               ,pa_days            => p_days
                               ,pa_purge_cd        => p_purge_cd
                               ,pa_archive_flag    => p_archive_flag
                               ,pa_child_comp_flag => p_child_comp_flag
                               ,pa_rows_purged     => i_rc);
                   i_rows_purged := i_rows_purged + i_rc;
                   
                   merge into purge_log_dpndt_data pld 
                   using ( select 'LPN' object_dpndt,l.lpn_id object_pk,l.asn_id asn_id,l.purchase_orders_id purchase_orders_id,l.tc_purchase_orders_id tc_purchase_orders_id 
                             from gtmp_arch_id_list t
                                  ,lpn l
                            where l.tc_purchase_orders_id = t.vc_id) t
                   on (pld.object_dpndt = t.object_dpndt
                       and pld.OBJECT_PK = t.object_pk)
                   when matched
                   then
                      update 
                      set pld.asn_id = t.asn_id,
                          pld.purchase_orders_id = t.purchase_orders_id,
                          pld.tc_purchase_orders_id = t.tc_purchase_orders_id, 
                          pld.is_nullfied = p_delink_child,
                          pld.PURGE_CODE = p_purge_cd,
                          pld.run_time = dstart
                  when not matched
                  then
                     insert(OBJECT_DPNDT,OBJECT_PK,ASN_ID,PURCHASE_ORDERS_ID,tc_purchase_orders_id,is_nullfied,PURGE_CODE,RUN_TIME)  
                     values(t.object_dpndt,t.object_pk,t.asn_id,t.purchase_orders_id,t.tc_purchase_orders_id,p_delink_child,p_purge_cd,dstart);      
                     
                   if p_delink_child ='Y'
                   then                
                      --WM-39208 Update purchase_orders_id to NULL for non-purgable LPNs associated with purgable PO's.                    
                      update lpn_detail ld
                      set ld.purchase_orders_id = null,
                          ld.last_updated_dttm = sysdate,
                          ld.last_updated_source = 'wm_archive_pkg' 
                      where ld.purchase_orders_id is not null and exists
                      (select null
                          from gtmp_arch_id_list t
                          where ld.tc_purchase_orders_id = t.vc_id);
                          
                      --WM-54240    
                      update lpn_movement lm
                      set lm.purchase_orders_id = null,
                          lm.last_updated_dttm = sysdate,
                          lm.last_updated_source = 'wm_archive_pkg' 
                      where lm.purchase_orders_id is not null and exists
                      (select null
                          from gtmp_arch_id_list t
                          where lm.tc_purchase_orders_id = t.vc_id);                     
       
                      update lpn l
                      set l.purchase_orders_id = null,
                          l.last_updated_dttm = sysdate,
                          l.last_updated_source = 'wm_archive_pkg' 
                      where l.purchase_orders_id is not null and exists
                      (select null
                          from gtmp_arch_id_list t
                          where l.tc_purchase_orders_id = t.vc_id);   
                   else
                       delete from gtmp_arch_id_list t
                       where exists 
                              (select 1
                                 from lpn l
                                where l.tc_purchase_orders_id = t.vc_id 
                               union all
                               select 1
                                 from lpn_movement lm
                                where lm.tc_purchase_orders_id = t.vc_id);   
                   end if;                   
   
                   select poli.purchase_orders_line_item_id bulk collect into arrIDs
                 from gtmp_arch_id_list tmp, purchase_orders_line_item poli
                   where tmp.i_id = poli.purchase_orders_id;
   
                   for currec in (
                                   select 'PO_LINE_WMPROCESSINFO' TAB_NAME ,'PURCHASE_ORDERS_LINE_ITEM_ID' COL_NAME from dual
                                   union all
                                   select 'PO_LINE_CLASSIFICATION_CODE' TAB_NAME ,'PO_LINE_ID' COL_NAME from dual --#WM-54533
                                 )
                   loop           
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => currec.col_name
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                                   
                   i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;              

				   select pobp.PURCHASE_ORDER_BP_ID bulk collect into arrIDs
                   from gtmp_arch_id_list tmp, purchase_order_bp pobp
                   where tmp.i_id = pobp.purchase_orders_id;
             
                   purge_table ( p_table_name  => 'PURCHASE_ORDER_BP_CONTACT'
                               , p_column_name => 'PURCHASE_ORDER_BP_ID'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                                   
                   i_rows_purged := i_rows_purged + i_rc;
   
                   select i_id bulk collect into arrIDs
                 from gtmp_arch_id_list;                      
   
                       for currec in (
                                   select 'PO_WMPROCESSINFO' TAB_NAME,'PURCHASE_ORDERS_ID' COL_NAME from dual
                                   union all                    
                                       select 'PO_SRL_NBR' TAB_NAME,'PURCHASE_ORDERS_ID' COL_NAME from dual
                                       union all
                                       select 'PO_LINE_ITEM_REF_FIELDS' TAB_NAME,'PURCHASE_ORDERS_ID' COL_NAME from dual
                                       union all
                                       select 'PO_REF_FIELDS' TAB_NAME,'PURCHASE_ORDERS_ID' COL_NAME from dual
                                       union all
                                       select 'PO_LINE_ITEM_ATTR_TEMPLATE' TAB_NAME,'PURCHASE_ORDERS_ID' COL_NAME from dual
                                       union all   
                                       select 'PO_LINE_ITEM_ATTRIBUTE' TAB_NAME,'PURCHASE_ORDERS_ID' COL_NAME from dual
                                       union all
                                       select 'PURCHASE_ORDERS_NOTE' TAB_NAME,'PURCHASE_ORDERS_ID' COL_NAME from dual
                                       union all  
                                       select 'PURCHASE_ORDERS_EVENT' TAB_NAME,'PURCHASE_ORDERS_ID' COL_NAME from dual
                                       union all
                                       select 'PURCHASE_ORDERS_ATTRIBUTE' TAB_NAME,'PURCHASE_ORDERS_ID' COL_NAME from dual
                                       union all   
                                       select 'PURCHASE_ORDERS_LINE_ITEM_SIZE' TAB_NAME,'PURCHASE_ORDERS_ID' COL_NAME from dual
                                       union all
                                       select 'PURCHASE_ORDERS_LINE_ITEM' TAB_NAME,'PURCHASE_ORDERS_ID' COL_NAME from dual
                                       union all  
                                       select 'RTS_LINE_ITEM' TAB_NAME,'ORDER_ID' COL_NAME from dual
                                       union all   
                                       select 'PURCHASE_ORDERS_EXTN_TLM' TAB_NAME,'PURCHASE_ORDERS_ID' COL_NAME from dual -- #MACR00732039 added as PURCHASE_ORDERS_EXTN_TLM table has a dependency on PURCHASE_ORDERS
                                       union all
									   select 'PURCHASE_ORDER_BP' TAB_NAME,'PURCHASE_ORDERS_ID' COL_NAME from dual 
                                       union all
                                       select 'TRAILER_CONTENTS' TAB_NAME,'PO_ID' COL_NAME from dual
                                       union all
									   select 'PURCHASE_ORDER_LINE_COMPONENTS' TAB_NAME,'PURCHASE_ORDERS_ID' COL_NAME from dual
                                       union all
                                       select 'PURCHASE_ORDERS' TAB_NAME,'PURCHASE_ORDERS_ID' COL_NAME from dual                                      
                                     )
                       loop           
                            purge_table ( p_table_name  => currec.tab_name
                                       , p_column_name => currec.col_name
                                       , p_arrids      => arrIDs
                                       , p_commit_size => p_commit_size
                                       , p_start_time  => dstart
                                       , p_archive_ind => p_archive_flag
                                       , p_purge_cd    => p_purge_cd
                                       , p_comp_id     => p_company_id
                                       , p_warehouse_id=> p_whse_id
                                       , p_rowcount    => i_rc
                                       , p_error_msg   => v_error_msg); 
                                       
                           i_rows_purged := i_rows_purged + i_rc;
                           if v_error_msg is not null then raise e_StopPurge; end if;
                       end loop;                       
   
             when p_purge_cd in ('33') then -- Order Purge
                   dstart := sysdate;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Order Purge', action_name => 'Archiving and Purging');
   
                   delete from tmp_wm_id_4;
                   -- select purge-able order/lpn combos
                   insert into tmp_wm_id_4 (i_id1, i_id2)
                 with purgable_order as (select order_id
                from orders o
                where o.do_status         >= 190
                and o.last_updated_dttm <= dstart - p_days
                   and o.tc_company_id       in (select company_id from company where company_id = p_company_id or parent_company_id = p_company_id)
                   and o.o_facility_id = p_whse_id
                   and o.purchase_order_id is null    -- order has no purchase orders
                 and not exists (select 1 from srl_nbr_track s where s.distribution_order = o.order_id and s.stat_code < 90)
                ) 
                   select l.order_id, l.lpn_id
                 from purgable_order o, lpn l
                    where o.order_id = l.order_id
                   and l.tc_purchase_orders_id is null
                   and l.tc_asn_id is null
                   and l.inbound_outbound_indicator = 'O'
                   and l.last_updated_dttm <= (dstart - p_days) -- modified to check the data condition on LPN table
                   and not exists (  -- MACR00773167/MACR00831029: exclude orders/lpns with unclosed manifests
                                   select ml.order_id
                                   from manifested_lpn ml, manifest_hdr mh
                                   where ml.manifest_id = mh.manifest_id
                                   and mh.manifest_status_id < 90  -- not closed
                                   and ml.order_id = o.order_id
                                   )
                   union all
                   -- select purge-able orders with no related lpns
                   select o.order_id, null
                 from purgable_order o
                 where not exists (
                                   select 1
                                   from lpn l
                                   where o.order_id = l.order_id
                                 );
   
                   -- WM-39341 Order split removal changes -- deleting the orders with the open shipments.
                   delete from tmp_wm_id_4 t
                   where exists
                   (select order_id from lpn
                    where lpn.order_id = t.i_id1
                    and lpn.shipment_id is not null);
   
                   -- query modified to improve performance.
                   delete from tmp_wm_id_4 t
                   where t.i_id1 in 
                   (select distinct o.order_id
                                    from (select order_id, line_item_id, reference_order_id, reference_line_item_id, do_dtl_status
                                        from order_line_item
                                        where reference_order_id is not null
                                             and reference_line_item_id is not null) o, 
                                         (select order_id, line_item_id, reference_order_id, reference_line_item_id, do_dtl_status
                                            from order_line_item) a
                                   where o.reference_order_id = a.order_id
                                     and o.reference_line_item_id = a.line_item_id
                                     and (a.do_dtl_status < 190
                                          or exists (
                                                     select 1
                                     from lpn_detail ld
                                     join lpn l on l.lpn_id = ld.lpn_id
                                     where ld.distribution_order_dtl_id = a.line_item_id
                                         and l.lpn_facility_status < 90
                                           )
                                           )); 
                                         
                   -- select lpn_ids into temp table for processing
                   delete from tmp_wm_arch_lpn_1;                                      
                   insert into tmp_wm_arch_lpn_1 (i_id, v_id,io_indicator)
                         select l.lpn_id , l.tc_lpn_id,l.inbound_outbound_indicator
                         from tmp_wm_id_4 t,lpn l
                         where l.lpn_id = t.i_id2
                         order by decode(l.parent_lpn_id||l.split_lpn_id,null,2,1);                        
   
                   -- purge lpns associated with the purge-able asns
                   purge_lpns (pl_company_id       => p_company_id
                           ,pl_whse_id          => p_whse_id
                           ,pl_commit_size      => p_commit_size
                           ,pl_days             => p_days
                           ,pl_purge_cd         => p_purge_cd
                           ,pl_archive_flag     => p_archive_flag
                           ,pl_child_comp_flag  => p_child_comp_flag
                           ,pl_rows_purged      => i_rc);
                   i_rows_purged := i_rc;
   
                   execute immediate 'truncate table gtmp_purge_orders'; -- WM-62011  PE changes
    
                   delete from TMP_WM_ID_1;
                   
                   delete from tmp_wm_id_4
                   where exists (select null from lpn l where order_id=i_id1);
           
                   insert into gtmp_purge_orders (col_name, i_id, vc_id)
                   select distinct 'order_id',i_id1,null from tmp_wm_id_4;
                   
                 --  log_auton_msg('Number of eligible records in gtmp_purge_orders table: order_ids: '|| sql%rowcount );    
                   
                   insert into TMP_WM_ID_1(V_ID)
                   select tc_order_id
                   from orders, tmp_wm_id_4
                   where order_id = i_id1;
   
                   purge_orders (po_company_id       => p_company_id
                           ,po_whse_id          => p_whse_id
                           ,po_commit_size      => p_commit_size
                           ,po_days             => p_days
                           ,po_purge_cd         => p_purge_cd
                           ,po_archive_flag     => p_archive_flag
                           ,po_child_comp_flag  => p_child_comp_flag
                           ,po_start_time       => dstart
                           ,po_rows_purged      => i_rc);
                   i_rows_purged := i_rows_purged + i_rc;
   
               when p_purge_cd = '34' then -- Shipment Purge
               --if p_purge_cd in ('34')
               --then
			   
			       dstart := sysdate;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Shipment Purge', action_name => 'Archiving and Purging');
   
                   begin
                       select param_value
                       into i_ship_stat_code
                       from company_parameter
                       where param_def_id = c_shipment_purge_status
                       and  tc_company_id = ( select case when parent_company_id is null or parent_company_id = -1
                                                         then company_id
                                                         else  parent_company_id
                                                         end
                                                         from company where company_id = p_company_id);  -- WM-65689 Shipment failing at BU level   
                   exception
                       when others then
                           -- raise an error for missing company/param_def entry
                           raise e_NoShipPurgeStatus;
                   end;
                   -- select lpn status code
                   begin
                       select nvl(purge_case_beyond_stat_code, 92)
                       into i_lpn_stat_code
                       from whse_parameters
                       where whse_master_id = p_whse_id;
                   exception
                       when others then
                           i_lpn_stat_code := 92;
                   end;
   
                   -- begin MACR00530224: asn default status
                   -- select asn status code
                   begin
                       select nvl(purge_asn_stat_code, c_default_asn_status)
                       into i_asn_stat_code
                       from whse_parameters
                       where whse_master_id = p_whse_id;
                   exception
                       when others then
                           i_asn_stat_code := c_default_asn_status;
                   end;
                   
                   archive_purge_log_dpndt_data(p_archive_flag); 
   
                   
                   delete from tmp_wm_id_1;
                   insert into tmp_wm_id_1 (i_id)  
                   select lpn_id
                   from lpn l
                   where
                       -- Changed for including destination and origin facility id after discussion with Ravi (Upendra)
                     (l.o_facility_id       = p_whse_id or l.d_facility_id       = p_whse_id or l.c_facility_id       = p_whse_id )
                   and l.tc_company_id       in (select company_id from company where company_id = p_company_id or parent_company_id = p_company_id) --WM-65572
                   -- MACR00592405 Canceled LPNs are purge-able.
                   and (l.last_updated_dttm  <= (dstart - p_days ) or l.lpn_facility_status=99)
                   and (
                         -- Changed to constant for outbound facility status
                         (l.lpn_facility_status   >= c_o_lpn_stat_code and l.inbound_outbound_indicator = 'O')
                            or  -- changed to include condition for inbound facility status
                        (l.lpn_facility_status   >= i_lpn_stat_code and l.inbound_outbound_indicator = 'I')
                         or
                         l.lpn_facility_status   = 1
                         ) -- not rcvd but shipment verified
                   and not exists (  -- MACR00773167: exclude lpns with unclosed manifests
                                   select 1
                                   from manifested_lpn ml, manifest_hdr mh
                                   where ml.manifest_id = mh.manifest_id
                                   and mh.manifest_status_id < 90  -- not closed
                                   and ml.lpn_id = l.lpn_id
                                 )
                   -- exclude those that have non-purgeable children              
                   minus
                   select lpn_id from lpn l
                   where exists (
                                  select 1
                                  from lpn p
                                  where l.lpn_id in (p.parent_lpn_id)
                                  and (
                                      p.last_updated_dttm > dstart - p_days 
                                      or (
                                             (p.lpn_facility_status < i_lpn_stat_code and p.inbound_outbound_indicator = 'I')
                                          or (p.lpn_facility_status < c_o_lpn_stat_code and p.inbound_outbound_indicator = 'O')
                                          or p.lpn_facility_status <> 1 -- not rcvd but shipment verified
                                         )
                                      or (p.order_id is not null and lpn_facility_status != 99) -- lpn has orders
                                      or p.asn_id is not null -- lpn has asns
                                      )          
                                  )   
                   -- exclude those that have non-purgeable children              
                   minus
                   select lpn_id from lpn l
                   where exists (
                                  select 1
                                  from lpn p
                                  where l.lpn_id in (p.split_lpn_id)
                                  and (
                                      p.last_updated_dttm > dstart - p_days 
                                      or (
                                             (p.lpn_facility_status < i_lpn_stat_code and p.inbound_outbound_indicator = 'I')
                                          or (p.lpn_facility_status < c_o_lpn_stat_code and p.inbound_outbound_indicator = 'O')
                                          or p.lpn_facility_status <> 1 -- not rcvd but shipment verified
                                         )
                                      or (p.order_id is not null and lpn_facility_status != 99) -- lpn has orders
                                      or p.asn_id is not null -- lpn has asns
                                      )          
                                  );                               
                   
                   delete from gtmp_arch_lpn_ids;
                   insert into gtmp_arch_lpn_ids(lpn_id, tc_lpn_id,order_id, shipment_id,io_indicator)
                   select lpn_id,tc_lpn_id, order_id, shipment_id,l.inbound_outbound_indicator
                   from lpn l , tmp_wm_id_1 t
                   where l.lpn_id = t.i_id
                   order by decode(parent_lpn_id||split_lpn_id,null,2,1); -- MACR00772156 (child lpns first)
   
   
                   -- select initial purgeable order_id list
                   delete from gtmp_arch_order_ids;
                   insert into gtmp_arch_order_ids(order_id, tc_order_id, shipment_id)
                       -- the 'distinct' is defensive; it really should not be necessary
                       select distinct o.order_id, o.tc_order_id, g.shipment_id  -- WM-39341 Order split changes
                       from orders o , gtmp_arch_lpn_ids g
                       where o.do_status         >= 190  -- shipped
                       and (
                           o.last_updated_dttm   <= dstart - p_days -- modified to check for last_updated_dttm, remove cancel_dttm and actual_shipped_dttm
                           )
                       and o.tc_company_id       in (select company_id from company where company_id = p_company_id or parent_company_id = p_company_id)
                       and (
                           -- changed to check for both inbound and outbound facility after discussing with Ravi -- change to facility_id
                           o.o_facility_id = p_whse_id
                           or
                           o.d_facility_id = p_whse_id
                           )
                       and o.purchase_order_id is null
                       and o.order_id = g.order_id
                       and g.shipment_id is not null
                       -- MACR00831029:  exclude any orders on manifested_lpn associated with lpns on unclosed manifests
                       and not exists (
                                       select 1 from manifested_lpn l, manifest_hdr h
                                       where l.manifest_id = h.manifest_id
                                       and h.manifest_status_id < 90
                                       and l.order_id = o.order_id)
                       and NOT EXISTS (select 1 from srl_nbr_track s where s.distribution_order = o.order_id and s.stat_code < 90);
                       
                   -- select initial purgeable shipment_id list
                   delete from gtmp_arch_ship_ids;
                   insert into gtmp_arch_ship_ids(shipment_id)
                       select s.shipment_id
                       from shipment s
                       where s.tc_company_id     = p_company_id
                       and s.last_updated_dttm   <= dstart - p_days
                       and (
                           s.o_facility_number = p_whse_id -- Changed from o_facility_id to o_facility_number
                           or
                           s.d_facility_number = p_whse_id -- changed from d_facility_id to d_facility_number
                           )
                       and (
                           -- s.shipment_status >= i_ship_stat_code
                          (s.shipment_status >= i_ship_stat_code
                               --MACR00829574 
                               and ((s.shipment_closed_indicator = 1 and s.lpn_assignment_stopped = 'Y') or s.is_cancelled = 1))                         
                           or
                           s.shipment_closed_indicator = 1  --MACR00561597
                           or
                           --- added condition for parcel shipments that do not go to closed status
                           (s.shipment_status >= 60 and assigned_ship_via in (select ship_via
                                                                              from ship_via sv, mot m
                                                                              where m.mot_id = sv.mot_id
                                                                              and m.is_parcel = 1))
                           or  --CR MACR00870478 UI inbound shipment purge - should take in account stop status and not only shipment status Jira : WM-25986
                           (s.shipment_status = 20 and exists ( select 1 from stop st
                                                                where st.shipment_id =s.shipment_id
                                                                and st.stop_status> 65) 
                           ))
                       and not exists (  -- MACR00773167 and MACR00799294: exclude shipment ids related to lpns with unclosed manifests
                                       select 1
                                       from manifested_lpn ml, manifest_hdr mh
                                       where ml.manifest_id = mh.manifest_id
                                       and mh.manifest_status_id < 90  -- not closed
                                       and ml.shipment_id = s.shipment_id
                                      )
                       and not exists (  -- Jira: WM-22584 exclude shipment_ids associated with OPEN APPOINTMENTS 
                                       select 1
                                       from ILM_APPOINTMENT_OBJECTS,ILM_APPOINTMENTS
                                       where ILM_APPOINTMENT_OBJECTS.appt_obj_id = s.shipment_id
                                       and ILM_APPOINTMENT_OBJECTS.appt_obj_type=30 
                                       and ILM_APPOINTMENTS.APPOINTMENT_ID=ILM_APPOINTMENT_OBJECTS.APPOINTMENT_ID
                                       and ILM_APPOINTMENTS.APPT_STATUS not in ('1','9','10') --(Invalid, Complete, Cancelled)
                                      );
   
                   -- MACR00551671: Select the purgeable orders with non-purgeable shipment ids
                   delete from tmp_wm_id_4;
                   insert into tmp_wm_id_4(i_id1, i_id2)
                   select order_id, shipment_id
                   from (
                         -- orders with non-purgeable shipment_ids
                         select o.order_id, o.shipment_id
                         from gtmp_arch_order_ids o
                         where not exists (
                                          select 1
                                          from gtmp_arch_ship_ids i
                                          where i.shipment_id = o.shipment_id
                                          )
                         union all
                         -- orders with non-purgeable shipment_ids in order_split table
                         select o.order_id, o.shipment_id
                         from order_split o, gtmp_arch_order_ids o1
                         where o.order_id = o1.order_id
                         and not exists (
                                         select 1
                                         from gtmp_arch_ship_ids i
                                         where i.shipment_id = o.shipment_id
                                         )
                         union all
                         -- orders with non-purgeable shipment_ids in order_movement table
                         select o.order_id, o.shipment_id
                         from order_movement o, gtmp_arch_order_ids o1
                         where o.order_id = o1.order_id
                         and not exists (
                                         select 1
                                         from gtmp_arch_ship_ids i
                                         where i.shipment_id = o.shipment_id
                                         )
                         )
                   where shipment_id is not null;
   
                   -- MACR00551671: remove orders with non-purgeable shipment_ids
                   delete from gtmp_arch_order_ids o
                     where exists (select 1
                                   from tmp_wm_id_4 i
                                   where i.i_id1 = o.order_id);
   
    
                     -- WM-70017 remove lpns with non-purgeable orders
                     delete from gtmp_arch_lpn_ids li
                     where not exists
                     (
                       select 1
                       from gtmp_arch_order_ids o
                       where o.order_id = li.order_id
                     );
   
                   -- select the ids to purge
                     delete from gtmp_arch_id_list;
                   insert into gtmp_arch_id_list (col_name, i_id, vc_id)
                     select 'shipment_id', s.shipment_id, null
                     from gtmp_arch_ship_ids s
                         union all
                     select distinct 'manifest_id', h.manifest_id, null
                     from manifested_lpn l, gtmp_arch_ship_ids s, manifest_hdr h -- Changed to get list from manifested_lpn and not manifest_hdr after discussion with Ravi
                     where l.shipment_id = s.shipment_id
                     and   l.manifest_id = h.manifest_id
                     and   h.manifest_status_id >= 90  -- closed
                         union all
                     select 'asn_id', a.asn_id, null
                     from asn a, gtmp_arch_ship_ids s
                     where a.destination_facility_alias_id   = gv_whse
                     and a.tc_company_id                     in (select company_id from company where company_id = p_company_id or parent_company_id = p_company_id)
                     and a.last_updated_dttm                 <= dstart - p_days
                     and a.asn_status  >= i_asn_stat_code
                     and a.shipment_id = s.shipment_id
                     /*and not exists (
                                     select 1
                                     from lpn l
                                     where l.asn_id = a.asn_id
                                     )*/ -- WM-46025

                      ;
   
                   select i_id bulk collect into arrIDs
                   from gtmp_arch_id_list where col_name = 'manifest_id';
   
                   -- purge manifest
                   for currec in (
                                   select 'MANIFESTED_LPN' TAB_NAME from dual
                                   union all
                                   select 'MANIFEST_HDR' TAB_NAME from dual
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'MANIFEST_ID'
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;  
                    
                   -- WM-40829: Purge ilm appointments first
                   purge_appointments (pa_company_id       => p_company_id
                                      ,pa_whse_id          => p_whse_id
                                      ,pa_commit_size      => p_commit_size
                                      ,pa_days             => p_days
                                      ,pa_purge_cd         => p_purge_cd
                                      ,pa_archive_flag     => p_archive_flag
                                      ,pa_child_comp_flag  => p_child_comp_flag
                                      ,pa_start_time       => dstart
                                      ,pa_rows_purged      => i_rc);
                   i_rows_purged := i_rows_purged + i_rc;
                   
                   delete from tmp_wm_arch_lpn_1;                                      
                   insert into tmp_wm_arch_lpn_1 (i_id, v_id,io_indicator)
                     select li.lpn_id, li.tc_lpn_id,io_indicator
                     from gtmp_arch_lpn_ids li
                     where exists
                     (
                       select 1
                       from gtmp_arch_ship_ids s
                       where s.shipment_id = li.shipment_id
                     )
                     or exists  
                     (
                       select 1
                       from gtmp_arch_order_ids o
                       where o.order_id = li.order_id
                     );                   
   
                   -- purge lpns associated with the purge-able asns
                   purge_lpns(pl_company_id       => p_company_id
                           ,pl_whse_id          => p_whse_id
                           ,pl_commit_size      => p_commit_size
                           ,pl_days             => p_days
                           ,pl_purge_cd         => p_purge_cd
                           ,pl_archive_flag     => p_archive_flag
                           ,pl_child_comp_flag  => p_child_comp_flag
                           ,pl_rows_purged      => i_rc);
                   i_rows_purged := i_rows_purged + i_rc;
                   
                   merge into purge_log_dpndt_data pld 
                   using ( select 'LPN' object_dpndt,l.lpn_id object_pk,l.asn_id asn_id ,l.tc_asn_id tc_asn_id,null purchase_orders_id
                             from gtmp_arch_id_list tmp
                                  ,lpn l
                            where l.asn_id = tmp.i_id
                              and col_name = 'asn_id' ) t
                   on (pld.object_dpndt = t.object_dpndt
                       and pld.OBJECT_PK = t.object_pk)
                   when matched
                   then
                      update 
                      set pld.asn_id = t.asn_id,
                          pld.tc_asn_id = t.tc_asn_id,
                          pld.purchase_orders_id = t.purchase_orders_id,
                          pld.is_nullfied = p_delink_child,
                          pld.PURGE_CODE = p_purge_cd,
                          pld.run_time = dstart
                  when not matched
                  then
                     insert(OBJECT_DPNDT,OBJECT_PK,ASN_ID,tc_asn_id,PURCHASE_ORDERS_ID,is_nullfied,PURGE_CODE,RUN_TIME)  
                     values(t.object_dpndt,t.object_pk,t.asn_id,t.tc_asn_id,t.purchase_orders_id,p_delink_child,p_purge_cd,dstart);                   
    
                   if p_delink_child ='Y'
                   then 
                      -- -- WM-46025 Update ASN_ID to NULL for non-purgable LPNs associated with purgable ASNs.
                      update LPN l
                      set l.asn_id = NULL
                      where  exists (
                                    select NULL
                                    from gtmp_arch_id_list tmp
                                    where col_name = 'asn_id'
                                    and l.asn_id = tmp.i_id);
      
                      -- Update ASN_ID to NULL for non-purgable LPN_MOVEMENT records based on the selection above -- MACR00624959.
                      update LPN_MOVEMENT lm
                      set lm.asn_id = NULL
                      where  exists (
                                    select NULL
                                    from gtmp_arch_id_list tmp
                                    where col_name = 'asn_id'
                                    and lm.asn_id = tmp.i_id);
									
					  -- Update ASN_DETAIL to NULL for non-purgable LPN's in the LPN_DETAIL table	--WM-110355
				
				      update LPN_DETAIL ld
                      set ld.asn_dtl_id = NULL,
                          ld.last_updated_dttm = sysdate,
                          ld.last_updated_source = 'wm_archive_pkg'
                      where exists (
                                    select NULL
                                    from gtmp_arch_id_list tmp, asn_detail ad
                                    where tmp.col_name = 'asn_id'
				      			  and ld.asn_dtl_id = ad.asn_detail_id
                                    and ad.asn_id = tmp.i_id);
                                    
                      -- Update ASN_ID to NULL for non-purgable LPN's in the SRL_NBR_TRACK table
                      update SRL_NBR_TRACK snt
                      set snt.asn = NULL
                      where  exists (
                                    select NULL
                                    from gtmp_arch_id_list tmp
                                    where col_name = 'asn_id'
                                    and snt.asn = tmp.i_id );
                   else
                      delete from  gtmp_arch_id_list tmp
                      where exists
                            (select null
                                 from LPN l
                                where l.asn_id = tmp.i_id
                                and col_name = 'asn_id'
                             union all
                             select null
                                 from lpn_movement l
                                where l.asn_id = tmp.i_id
                                and col_name = 'asn_id'                          
                                
                             );
                   end if;  
   
                   select distinct i_id bulk collect into arrIDs --MACR00353059
                   from gtmp_arch_id_list where col_name = 'asn_id';
   
                   purge_asns ( pa_arr_asn_ids     => arrIDs
                               ,pa_company_id      => p_company_id
                               ,pa_whse_id         => p_whse_id
                               ,pa_commit_size     => p_commit_size
                               ,pa_days            => p_days
                               ,pa_purge_cd        => p_purge_cd
                               ,pa_archive_flag    => p_archive_flag
                               ,pa_child_comp_flag => p_child_comp_flag
                               ,pa_rows_purged     => i_rc);
                   i_rows_purged := i_rows_purged + i_rc;
   
                   execute immediate 'truncate table gtmp_purge_orders'; -- WM-62011  PE changes
                   
                   
                    -- Remove orders with non purgable LPNS
                    delete from gtmp_arch_order_ids o
                    where exists (select null from lpn l where o.order_id=l.order_id);
                    
                    insert into gtmp_purge_orders (col_name, i_id, vc_id)
                    select distinct 'order_id',order_id,null from gtmp_arch_order_ids ;
                    
                 --   log_auton_msg('Number of eligible records in gtmp_purge_orders table: order_ids: '|| sql%rowcount );    


                    delete from TMP_WM_ID_1;

                    insert into TMP_WM_ID_1(V_ID)
                    select tc_order_id
                    from gtmp_arch_order_ids ;
   
                   purge_orders (po_company_id       => p_company_id
                           ,po_whse_id          => p_whse_id
                           ,po_commit_size      => p_commit_size
                           ,po_days             => p_days
                           ,po_purge_cd         => p_purge_cd
                           ,po_archive_flag     => p_archive_flag
                           ,po_child_comp_flag  => p_child_comp_flag
                           ,po_start_time      => dstart
                           ,po_rows_purged      => i_rc);
                   i_rows_purged := i_rows_purged + i_rc;
                   
                   merge into purge_log_dpndt_data pld 
                   using ( select 'ORDERS' object_dpndt,o.order_id object_pk,o.shipment_id shipment_id
                             from gtmp_arch_id_list tmp
                                  ,ORDERS o
                            where col_name = 'shipment_id'
                              and tmp.i_id = o.shipment_id) t
                   on (pld.object_dpndt = t.object_dpndt
                       and pld.OBJECT_PK = t.object_pk)
                   when matched
                   then
                      update 
                      set pld.shipment_id = t.shipment_id,
                          pld.is_nullfied = p_delink_child,
                          pld.PURGE_CODE = p_purge_cd,
                          pld.run_time = dstart
                  when not matched
                  then
                     insert(OBJECT_DPNDT,OBJECT_PK,shipment_id,is_nullfied,PURGE_CODE,RUN_TIME)  
                     values(t.object_dpndt,t.object_pk,t.shipment_id,p_delink_child,p_purge_cd,dstart);  
   
                   merge into purge_log_dpndt_data pld 
                   using ( select 'LPN' object_dpndt,l.lpn_id object_pk,l.shipment_id shipment_id 
                             from gtmp_arch_id_list tmp
                                  ,lpn l
                            where l.shipment_id = tmp.i_id
                              and col_name = 'shipment_id') t
                   on (pld.object_dpndt = t.object_dpndt
                       and pld.OBJECT_PK = t.object_pk)
                   when matched
                   then
                      update 
                      set pld.shipment_id = t.shipment_id,
                          pld.is_nullfied = p_delink_child,
                          pld.PURGE_CODE = p_purge_cd,
                          pld.run_time = dstart
                  when not matched
                  then
                     insert(OBJECT_DPNDT,OBJECT_PK,shipment_id,is_nullfied,PURGE_CODE,RUN_TIME)  
                     values(t.object_dpndt,t.object_pk,t.shipment_id,p_delink_child,p_purge_cd,dstart);                   
                      
                   if p_delink_child ='Y'
                   then
                       update ORDERS o
                       set o.shipment_id = NULL,
                         o.last_updated_dttm = sysdate,
                         o.last_updated_source = 'wm_archive_pkg' 
                       where exists (
                                     select NULL
                                     from gtmp_arch_id_list tmp  
                                     where col_name = 'shipment_id'
                                       and o.shipment_id = tmp.i_id);
   
                       update ORDER_SPLIT os
                       set os.shipment_id = NULL,
                         os.last_updated_dttm = sysdate,
                         os.last_updated_source = 'wm_archive_pkg' 
                       where exists (
                                     select NULL
                                     from gtmp_arch_id_list tmp  
                                     where col_name = 'shipment_id'
                                       and os.shipment_id = tmp.i_id);  
                                     
                       update ORDER_MOVEMENT om
                       set om.shipment_id = NULL
                       where  exists (
                                     select NULL
                                     from gtmp_arch_id_list tmp  
                                     where col_name = 'shipment_id'
                                       and om.shipment_id = tmp.i_id);
    
                    -- Update shipment id of lpn's to NULL whose associated shipment can be purged
                     update lpn li
                     set li.shipment_id = null,
                         li.last_updated_dttm = sysdate,
                         li.last_updated_source = 'wm_archive_pkg' 
                     where  exists
                     (
                       select NULL
                         from gtmp_arch_id_list tmp  
                        where col_name = 'shipment_id'
                          and li.shipment_id = tmp.i_id
                     );
   
                     update LPN_MOVEMENT lm
                     set lm.shipment_id = null,
                         lm.last_updated_dttm = sysdate,
                         lm.last_updated_source = 'wm_archive_pkg' 
                     where exists
                     (
                      select NULL
                        from gtmp_arch_id_list tmp  
                       where col_name = 'shipment_id'
                         and lm.shipment_id = tmp.i_id
                     );                          
                                 
                   else
                       delete from gtmp_arch_id_list s
                       where exists (
                                         select 1 
                                         from ORDERS o
                                         where col_name = 'shipment_id'
                                         and s.i_id = o.shipment_id
                                         union all
                                         select 1 
                                         from ORDER_MOVEMENT o
                                         where col_name = 'shipment_id'
                                         and s.i_id = o.shipment_id
                                         union all
                                         select 1 
                                         from ORDER_SPLIT o
                                         where col_name = 'shipment_id'
                                         and s.i_id = o.shipment_id
                                         );   
     
                       delete from gtmp_arch_id_list s
                       where exists
                       (
                         select 1
                         from lpn l
                         where col_name = 'shipment_id'
                         and s.i_id = l.shipment_id
                         union all
                         select 1
                         from LPN_MOVEMENT lm
                         where col_name = 'shipment_id'
                         and s.i_id = lm.shipment_id
                       );
                   end if;                       
   
   
                   select i_id bulk collect into arrIDs
                   from gtmp_arch_id_list where col_name = 'shipment_id';
                             
   
                   -- purge shipments
                   purge_shipments (ps_arr_ship_ids    => arrIDs
                           ,ps_company_id              => p_company_id
                           ,ps_whse_id                 => p_whse_id
                           ,ps_commit_size             => p_commit_size
                           ,ps_days                    => p_days
                           ,ps_purge_cd                => p_purge_cd
                           ,ps_archive_flag            => p_archive_flag
                           ,ps_child_comp_flag         => p_child_comp_flag
                           ,ps_rows_purged             => i_rc);
                   --MACR00386769
                   i_rows_purged := nvl(i_rows_purged,0) + nvl(i_rc,0);
   
   
               when p_purge_cd = '35' then -- ASN Purge
   
                   dstart := sysdate;
                   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'ASN Purge', action_name => 'Archiving and Purging');
   
                   -- begin MACR00530224: asn default status
                   begin
                       select nvl(purge_asn_stat_code, c_default_asn_status)
                       into i_asn_stat_code
                       from whse_parameters
                       where whse_master_id = p_whse_id;
                   exception
                       when others then
                           i_asn_stat_code := c_default_asn_status;
                   end;
   
                   begin
                       select nvl(purge_case_beyond_stat_code, 92)
                       into i_lpn_stat_code
                       from whse_parameters
                       where whse_master_id = p_whse_id;
                   exception
                       when others then
                           i_lpn_stat_code := 92;
                   end;
                   
                   archive_purge_log_dpndt_data(p_archive_flag);
   
                   delete from gtmp_arch_id_list;
                   insert into gtmp_arch_id_list (col_name, i_id)
                       with asn_ids as (
                                        select a.asn_id, a.tc_asn_id
                                       from asn a
                                       where a.destination_facility_id = p_whse_id
                                       and a.tc_company_id             = p_company_id
                                       and a.last_updated_dttm         <= dstart - p_days
                                       and a.asn_status                >= i_asn_stat_code
                                       and ( (a.tc_shipment_id is null)  or  (a.tc_shipment_id not in (select tc_shipment_id from shipment)))  -- added as part of MACR00865443
                                       and not exists 
                                        (  -- Jira:WM-40672 exclude asn_ids associated with OPEN APPOINTMENTS 
                                          select 1
                                          from ILM_APPOINTMENT_OBJECTS
                                          where ILM_APPOINTMENT_OBJECTS.appt_obj_id = a.asn_id
                                          and ILM_APPOINTMENT_OBJECTS.appt_obj_type=10 
                                        ) 
                                       )
                       -- asns begin purged with lpns
                       select distinct 'asn_id', a.asn_id
                       from asn_ids a, asn_detail ad, lpn l
                       where a.asn_id = ad.asn_id
                       and a.tc_asn_id = l.tc_asn_id -- changed to tc_asn_id as part of MACR00865443
                       and l.order_id is null
                    /*-- Modified below condition to check for ASN with PO's, with no corresponding record in the PO table
                       and
                       (ad.purchase_orders_id is null  -- no purchase orders
                          or not exists
                          (select NULL
                          from purchase_orders po
                          where po.purchase_orders_id = ad.purchase_orders_id
                          )
                       ) */ -- WM-61784
                       union all
                       -- asns with no lpns
                       select 'asn_id', a.asn_id
                       from asn_ids a
                       where not exists (
                                       select 1
                                       from lpn l
                                       where l.asn_id = a.asn_id
                                       );
   
                   -- select the asn_ids/lpn_ids to purge
                   delete from tmp_wm_id_4;
   
                   insert into tmp_wm_id_4 (i_id1, i_id2,V_ID,io_indicator)
                     select a.i_id asn_id, l.lpn_id,l.tc_lpn_id,l.inbound_outbound_indicator
                     from gtmp_arch_id_list a join asn 
                     on (a.i_id = asn.asn_id)
                     left outer join lpn l on (asn.tc_asn_id = l.tc_asn_id and asn.tc_company_id=l.tc_company_id) -- changed to tc_asn_id as part of MACR00865443
                     where col_name = 'asn_id'
                     and l.order_id is null    -- asn has no orders in related lpn
                     union -- fetch asn's associated with child data in lm, but not with lpn
                     select a.i_id asn_id, lm.lpn_id,lm.tc_lpn_id,null
                     from gtmp_arch_id_list a
                     join lpn_movement lm on lm.asn_id = a.i_id
                     where a.col_name = 'asn_id'
                     and exists
                     (
                      select 1
                      from lpn l
                      where l.lpn_id = lm.lpn_id and l.asn_id is null and l.order_id is null
                      );
   
                  /* delete from tmp_wm_id_4
                   where exists (
                               select 1
                               from asn_detail ad
                               where ad.asn_id = i_id1
                               and exists
                               (select NULL
                               from purchase_orders po
                               where po.purchase_orders_id = ad.purchase_orders_id
                               )
                               );*/ -- WM-61784
   
   
                   delete from tmp_wm_id_4
                   where exists (
                               select 1
                               from lpn
                               where asn_id = i_id1
                               and order_id is not null
                               );
   
   
                   -- update status of purge-able lpns from in-transit to 1
                   update lpn l
                   set l.lpn_facility_status = 1
                   where exists (
                                   select 1
                                   from tmp_wm_id_4
                                   where i_id2 = l.lpn_id
                                   )
                   and l.lpn_facility_status = 0
                   and l.order_id is null;
   
                   -- Begin MACR00610845
                   -- Get list of non-purgable LPN_IDs from list of ASN/LPN combos
                   delete from gtmp_arch_lpn_ids;
                   insert into gtmp_arch_lpn_ids(lpn_id)
                   select /*+ leading(tmp) use_nl(l) */ l.lpn_id
                   from lpn l, tmp_wm_id_4 tmp
                   where l.lpn_id = tmp.i_id2
                   and l.c_facility_id = p_whse_id  -- added as part of MACR00865443
                   and l.tc_company_id = p_company_id
                   and (
                          (l.last_updated_dttm > dstart - p_days)
                          or
                          (
                               (l.lpn_facility_status < c_o_lpn_stat_code and l.inbound_outbound_indicator = 'O')
                               or
                               -- Added condition to check for olpn and ilpn -- Upendra
                               (l.lpn_facility_status < i_lpn_stat_code and l.inbound_outbound_indicator = 'I'
                               and l.lpn_facility_status <> 1 )
                          )
                   );
   
                   -- WM-40829: Purge ilm appointments first
                   purge_appointments (pa_company_id       => p_company_id
                                      ,pa_whse_id          => p_whse_id
                                      ,pa_commit_size      => p_commit_size
                                      ,pa_days             => p_days
                                      ,pa_purge_cd         => p_purge_cd
                                      ,pa_archive_flag     => p_archive_flag
                                      ,pa_child_comp_flag  => p_child_comp_flag
                                      ,pa_start_time       => dstart
                                      ,pa_rows_purged      => i_rc);
                   i_rows_purged := i_rows_purged + i_rc;
   
                   -- Remove non-purgable LPN's from the list of purgable ASN/LPN combinations
                   update tmp_wm_id_4 set i_id2 = NULL 
                   where exists (
                                 select NULL
                                 from gtmp_arch_lpn_ids tmp
                                 where tmp.lpn_id = i_id2);                
                   
                   -- create array of purge-able lpns
                   delete from tmp_wm_arch_lpn_1;                                      
                   insert into tmp_wm_arch_lpn_1 (i_id, v_id,io_indicator)
                         select i_id2, v_id,io_indicator
                   from tmp_wm_id_4;
   
   
                   -- purge lpns associated with the purge-able asns
                   purge_lpns(pl_company_id       => p_company_id
                           ,pl_whse_id          => p_whse_id
                           ,pl_commit_size      => p_commit_size
                           ,pl_days             => p_days
                           ,pl_purge_cd         => p_purge_cd
                           ,pl_archive_flag     => p_archive_flag
                           ,pl_child_comp_flag  => p_child_comp_flag
                           ,pl_rows_purged      => i_rc);
                   i_rows_purged := i_rows_purged + i_rc;
   
                   merge into purge_log_dpndt_data pld 
                   using ( select 'LPN' object_dpndt,l.lpn_id object_pk,l.asn_id asn_id,l.tc_asn_id tc_asn_id
                             from gtmp_arch_lpn_ids tmp
                                  ,lpn l
                            where tmp.lpn_id = l.lpn_id ) t
                   on (pld.object_dpndt = t.object_dpndt
                       and pld.OBJECT_PK = t.object_pk)
                   when matched
                   then
                      update 
                      set pld.asn_id = t.asn_id,
                          pld.tc_asn_id = t.tc_asn_id,
                          pld.is_nullfied = p_delink_child,
                          pld.PURGE_CODE = p_purge_cd,
                          pld.run_time = dstart
                   when not matched
                   then
                      insert(OBJECT_DPNDT,OBJECT_PK,ASN_ID,TC_ASN_ID,is_nullfied,PURGE_CODE,RUN_TIME)  
                      values(t.object_dpndt,t.object_pk,t.asn_id,t.tc_asn_id,p_delink_child,p_purge_cd,dstart);   
                
                   
                   if p_delink_child ='Y'
                   then
                      -- Update ASN_ID to NULL for non-purgable LPNs associated with purgable ASNs.
                      update LPN l
                      set l.asn_id = NULL,
                          l.last_updated_dttm = sysdate,
                          l.last_updated_source = 'wm_archive_pkg' 
                      where exists (
                                    select NULL
                                    from tmp_wm_id_4 tmp
                                    where l.asn_id = tmp.i_id1);
      
                      -- Update ASN_ID to NULL for non-purgable LPN_MOVEMENT records based on the selection above -- MACR00624959.
                      update LPN_MOVEMENT lm
                      set lm.asn_id = NULL,
                          lm.last_updated_dttm = sysdate,
                          lm.last_updated_source = 'wm_archive_pkg' 
                      where exists (
                                    select NULL
                                    from tmp_wm_id_4 tmp
                                    where lm.asn_id = tmp.i_id1);
									
					-- Update ASN_DETAIL to NULL for non-purgable LPN's in the LPN_DETAIL table	--WM-110355
                      
                   update LPN_DETAIL ld
                   set ld.asn_dtl_id = NULL,
                       ld.last_updated_dttm = sysdate,
                       ld.last_updated_source = 'wm_archive_pkg'
                   where exists (
                                 select NULL
                                 from tmp_wm_id_4 tmp, asn_detail ad
                                 where ld.asn_dtl_id = ad.asn_detail_id
                                 and ad.asn_id = tmp.i_id1
                                 );
                                    
                      -- Update ASN_ID to NULL for non-purgable LPN's in the SRL_NBR_TRACK table
                      update SRL_NBR_TRACK snt
                      set snt.asn = NULL,
                          snt.last_updated_dttm = sysdate,
                          snt.last_updated_source = 'wm_archive_pkg' 
                      where exists (
                                    select NULL
                                    from tmp_wm_id_4 tmp
                                    where snt.asn = tmp.i_id1);
                      -- End MACR00624959
         
                   else
                      delete from tmp_wm_id_4 tmp
                      where exists 
                              (select NULL
                                 from LPN l
                                where l.asn_id = tmp.i_id1
                               union all
                               select NULL
                                 from LPN_MOVEMENT lm
                                where lm.asn_id = tmp.i_id1
                                union all
                                select NULL
                                 from SRL_NBR_TRACK snt
                                where snt.asn = tmp.i_id1);                   
                   end if;                  
                   
                   -- create array of purge-able asn ids
                   select distinct i_id1
                   bulk collect into arrIDs
                   from tmp_wm_id_4;
   
                   purge_asns ( pa_arr_asn_ids     => arrIDs
                               ,pa_company_id      => p_company_id
                               ,pa_whse_id         => p_whse_id
                               ,pa_commit_size     => p_commit_size
                               ,pa_days            => p_days
                               ,pa_purge_cd        => p_purge_cd
                               ,pa_archive_flag    => p_archive_flag
                               ,pa_child_comp_flag => p_child_comp_flag
                               ,pa_rows_purged     => i_rc);
                   i_rows_purged := i_rows_purged + i_rc;
   
   
               when p_purge_cd = '36' then -- LPN Purge
                   dstart := sysdate;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'LPN Purge', action_name => 'Archiving and Purging');
                   begin
                       select nvl(purge_case_beyond_stat_code, 92)
                       into i_lpn_stat_code
                       from whse_parameters
                       where whse_master_id = p_whse_id;
                   exception
                       when others then
                           i_lpn_stat_code := 92;
                   end;
   
                   delete from tmp_wm_id_1;
                   insert into tmp_wm_id_1 (i_id)
                   select lpn_id 
                   from lpn l
                   where l.c_facility_id = p_whse_id and l.tc_company_id = p_company_id
                   and l.last_updated_dttm <= (dstart - p_days )
                   and (
                          (l.lpn_facility_status   >= i_lpn_stat_code and l.inbound_outbound_indicator = 'I')
                       or (l.lpn_facility_status   >= c_o_lpn_stat_code and l.inbound_outbound_indicator = 'O')
                       or  l.lpn_facility_status   = 1
                       or (l.lpn_type=2  and not exists (select 1 from lpn t where t.tc_parent_lpn_id=l.tc_lpn_id))--# MACR00867094 Consider the Empty Pallets in LPN purge
                       )
                   and order_id is null    -- lpn has no orders, WM-70017 removed cancelled lpn check.
                   and asn_id is null      -- lpn has no asns
                   and not exists (  -- MACR00773167: exclude lpns with unclosed manifests
                                 select 1
                                 from manifested_lpn ml, manifest_hdr mh
                                 where ml.manifest_id = mh.manifest_id
                                 and mh.manifest_status_id < 90  -- not closed
                                 and ml.lpn_id = l.lpn_id
                                  )
                   and not exists (  -- JCRW CUSTOM: exclude lpns with open AIDs #MACR00866794
                              select 1
                              from alloc_invn_dtl aid
                              where aid.invn_need_type = 60
                              and aid.stat_code < 90
                              and aid.cntr_nbr = l.tc_lpn_id
                              )                                
                   union
                   --  adding logic to purge Shipped LPNs while Aggregate order is still open #CR MACR00865613
                   select l.lpn_id
                   from lpn l
                   join orders agg on agg.order_id = l.order_id and agg.is_original_order = 0
                       and agg.do_type = 10
                   where l.c_facility_id = p_whse_id and l.tc_company_id = p_company_id
                       and l.last_updated_dttm <= dstart - p_days
                       and l.lpn_facility_status >= c_o_lpn_stat_code 
                       and l.inbound_outbound_indicator = 'O'
                   minus -- candidates that have open data or some bad data
                   select distinct l.lpn_id
                   from lpn l
                   join orders agg on agg.order_id = l.order_id and agg.is_original_order = 0
                       and agg.do_type = 10
                   join lpn_detail ld on ld.lpn_id = l.lpn_id
                   join order_line_item oli on oli.line_item_id = ld.distribution_order_dtl_id -- this should be orig oli, can be agg oli on account of bad data
                   join orders o on o.order_id = oli.order_id -- should be orig order, can be agg order on account of bad data
                   left join order_line_item agg_oli on agg_oli.line_item_id = oli.reference_line_item_id -- should be agg oli or null
                   where l.c_facility_id = p_whse_id and l.tc_company_id = p_company_id
                       and l.last_updated_dttm <= dstart - p_days
                       and l.lpn_facility_status >= c_o_lpn_stat_code
                       and l.inbound_outbound_indicator = 'O'
					   and l.lpn_status = 99
                       and (o.do_status < 190 or coalesce(agg_oli.do_dtl_status, 0) < 190)                                   
                   -- exclude those that have non-purgeable children
                   minus
                   select lpn_id from lpn l
                   where exists (
                                  select 1
                                  from lpn p
                                  where l.lpn_id in (p.parent_lpn_id)
                                  and (
                                      p.last_updated_dttm > dstart - p_days 
                                      or (
                                             (p.lpn_facility_status < i_lpn_stat_code and p.inbound_outbound_indicator = 'I')
                                          or (p.lpn_facility_status < c_o_lpn_stat_code and p.inbound_outbound_indicator = 'O')
                                          or p.lpn_facility_status <> 1 -- not rcvd but shipment verified
                                         )
                                      or (p.order_id is not null and lpn_facility_status != 99) -- lpn has orders
                                      or p.asn_id is not null -- lpn has asns
                                      )          
                                  ) 
                   -- exclude those that have non-purgeable children
                   minus
                   select lpn_id from lpn l
                   where exists (
                                  select 1
                                  from lpn p
                                  where l.lpn_id in (p.split_lpn_id)
                                  and (
                                      p.last_updated_dttm > dstart - p_days 
                                      or (
                                             (p.lpn_facility_status < i_lpn_stat_code and p.inbound_outbound_indicator = 'I')
                                          or (p.lpn_facility_status < c_o_lpn_stat_code and p.inbound_outbound_indicator = 'O')
                                          or p.lpn_facility_status <> 1 -- not rcvd but shipment verified
                                         )
                                      or (p.order_id is not null and lpn_facility_status != 99) -- lpn has orders
                                      or p.asn_id is not null -- lpn has asns
                                      )          
                                  );                                 
                   
                   -- MACR00772156:  Select all lpns valid for purge and sort parents to bottom
                   --                to avoid FK constraint violations against LPN table
                    delete from tmp_wm_arch_lpn_1;                                      
                   insert into tmp_wm_arch_lpn_1 (i_id, v_id,io_indicator)
                   select lpn_id ,tc_lpn_id, l.inbound_outbound_indicator
                   from lpn l, tmp_wm_id_1 t 
                   where l.lpn_id = t.i_id
                   order by decode(parent_lpn_id||split_lpn_id,null,2,1);
   
   
                   purge_lpns (pl_company_id       => p_company_id
                               ,pl_whse_id          => p_whse_id
                               ,pl_commit_size      => p_commit_size
                               ,pl_days             => p_days
                               ,pl_purge_cd         => p_purge_cd
                               ,pl_archive_flag     => p_archive_flag
                               ,pl_child_comp_flag  => p_child_comp_flag
                               ,pl_rows_purged      => i_rc);
                   i_rows_purged := i_rc;
                   
                   purge_agg_orders (p_company_id       => p_company_id
                               ,p_whse_id          => p_whse_id
                               ,p_commit_size      => p_commit_size
                               ,p_days             => p_days
                               ,p_purge_cd         => p_purge_cd
                               ,p_archive_flag     => p_archive_flag
                               ,p_child_comp_flag  => p_child_comp_flag
                               ,p_start_time       => dstart
                               ,p_rows_purged      => i_rc);
                   i_rows_purged := i_rows_purged + i_rc; --WM-57774                       
   
               when p_purge_cd = '37' then -- Appointment Purge
                   dstart := sysdate;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Appointments Purge', action_name => 'Archiving and Purging');
                   i_rows_purged := 0;
   
                   -- WM-40829: Created procedure for call from Shipments purge. 
                   purge_appointments (pa_company_id       => p_company_id
                                      ,pa_whse_id          => p_whse_id
                                      ,pa_commit_size      => p_commit_size
                                      ,pa_days             => p_days
                                      ,pa_purge_cd         => p_purge_cd
                                      ,pa_archive_flag     => p_archive_flag
                                      ,pa_child_comp_flag  => p_child_comp_flag
                                      ,pa_start_time       => dstart
                                      ,pa_rows_purged      => i_rc);
                   i_rows_purged := i_rc;
   
   -- BEGIN MACR00516459 - 3 Additional Purges Support for 2010 and 2011
               when p_purge_cd = '38' then  -- PDIF (EIS) Purge - Device Integration Tables
                   dstart := systimestamp;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'DIF Purge', action_name => 'Archiving and Purging');
                   i_rows_purged := 0;
   
                   -- WM-39034 modified to purge to move the data from _HISTORY tables if the history tables contain any data.
                   v_hist_array := Hist_Array('CL_MESSAGE_HISTORY','CL_ENDPOINT_QUEUE_HISTORY','CL_MESSAGE_KEYS_HISTORY');
   
                   is_table_history_configured(p_hist_array => v_hist_array
                                               ,p_is_config => v_is_config);
                   
                   if v_is_config = 'Y'
                   then
                      lv_cl_message := 'CL_MESSAGE_HISTORY';
                      lv_cl_endpoint_queue := 'CL_ENDPOINT_QUEUE_HISTORY';
                      lv_cl_message_keys :=  'CL_MESSAGE_KEYS_HISTORY';
                   else
                      lv_cl_message := 'CL_MESSAGE';
                      lv_cl_endpoint_queue := 'CL_ENDPOINT_QUEUE';
                      lv_cl_message_keys :=  'CL_MESSAGE_KEYS';
                   end if;
                   
                   v_sql := 'select endpoint_queue_id ' 
                   ||' from '
                   || lv_cl_endpoint_queue 
                   || ' where when_status_changed <= :dstart - ' || p_days
                   || ' and status not in (2,10) '; -- busy
   
                   execute immediate v_sql bulk collect into arrIDs using dstart; --WM-50444
   
                   delete from tmp_wm_id_1;
   
                   -- select purge-able msg ids for cl_message and cl_message_keys
                   v_sql := 'insert into tmp_wm_id_1(i_id)'
                   ||' select msg_id ' 
                   ||' from '
                   ||' ( select distinct msg_id from '
                   || lv_cl_endpoint_queue 
                   || ' o where when_status_changed <= :dstart - ' || p_days 
                   || ' and status not in (2,10) ' -- busy
                   || ' union '
                   || ' select msg_id from '
                   || lv_cl_message 
                   || ' cm where not exists ( select 1 from '
                   || lv_cl_endpoint_queue
                   || ' ceq where ceq.msg_id = cm.msg_id ))';
   
                 execute immediate v_sql using  dstart; --WM-50444
   
                   purge_table ( p_table_name  => lv_cl_endpoint_queue
                               , p_column_name => 'ENDPOINT_QUEUE_ID'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                     v_sql:='select i_id '
                     ||' from tmp_wm_id_1'
                     ||' where exists (select 1 from '||lv_cl_message_keys||' where MSG_ID=i_id)';
                     
                     execute immediate v_sql bulk collect into arrIDs;
                     
                     
                     purge_table ( p_table_name  => lv_cl_message_keys
                                   , p_column_name => 'MSG_ID'
                                 , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg);
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                     
                     select i_id bulk collect into arrIDs
                     from tmp_wm_id_1;
 
                     purge_table ( p_table_name  => lv_cl_message
                                 , p_column_name => 'MSG_ID'
                                 , p_arrids      => arrIDs
                                 , p_commit_size => p_commit_size
                                 , p_start_time  => dstart
                                 , p_archive_ind => p_archive_flag
                                 , p_purge_cd    => p_purge_cd
                                 , p_comp_id     => p_company_id
                                 , p_warehouse_id=> p_whse_id
                                 , p_rowcount    => i_rc
                                 , p_error_msg   => v_error_msg);
                     i_rows_purged := i_rows_purged + i_rc;
                     if v_error_msg is not null then raise e_StopPurge; end if;
   
               when p_purge_cd = '39' then  -- Carrier Data Purge
                   if (p_misc_parm_1 is null)
                   then
                       v_error_msg := 'purge_config.misc_parm_1 is null; please '
                           || 'set it to a valid carrier_code.';
                       raise e_StopPurge;
                   end if;
   
                   dstart := systimestamp;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Carrier Tables Purge', action_name => 'Archiving and Purging');
                   i_rows_purged := 0;
   
                   delete from tmp_wm_id_1;
                   -- WM-59110 adding changes to purge all carrier's
                   
                   if upper(p_misc_parm_1) = 'ALL'
                   then
                      v_sql := 'insert into tmp_wm_id_1 (i_id,V_ID) '
                            || ' select carrier_id,carrier_code '
                            || ' from carrier_code  '
                            || ' where tc_company_id =  ' || p_company_id
							|| ' and mark_for_deletion = 1'
                            || ' and last_updated_dttm <= :dstart - ' || p_days;
                   else
                      v_sql := 'insert into tmp_wm_id_1 (i_id) '
                            || ' select carrier_id '
                            || ' from carrier_code  '
                            || ' where tc_company_id =  ' || p_company_id
							|| ' and mark_for_deletion = 1'
                            || ' and carrier_code = ''' ||p_misc_parm_1  -- carrier_code 
                            || ''' and last_updated_dttm <= :dstart - ' || p_days;               
                   end if;
                   
                   execute immediate v_sql using dstart;      
   
                   select i_id bulk collect into arrIDs
                   from tmp_wm_id_1;
                   
                   for currec in (
                                   select 'carrier_code_contact' TAB_NAME from dual
                                   union all
                                   select 'carrier_label' TAB_NAME from dual
                                   union all
                                   select 'carrier_code_mot_facility' TAB_NAME from dual
                                   union all
                                   select 'carrier_code_mot_capcomm' TAB_NAME from dual
                                   union all  
                                   select 'CARRIER_SCHDL_REG_DAY' TAB_NAME from dual
                                   union all  
                                   select 'CARRIER_SCHDL_HOLIDAY' TAB_NAME from dual   
                                   union all  
                                   select 'user_carrier_map' TAB_NAME from dual  
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'CARRIER_ID'
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;  
                                   
   
                   delete from tmp_wm_id_2;
                   insert into tmp_wm_id_2 (i_id)
                   select fsc.shipping_calendar_id
                   from facility_shipping_calendar fsc
                   where exists
                   (
                      select 1
                      from tmp_wm_id_1 t
                      where t.i_id = fsc.carrier_id
                   );
   
                   select i_id bulk collect into arrIDs
                   from tmp_wm_id_2;
                   
                   for currec in (
                                   select 'fac_shp_cal_schdl' TAB_NAME from dual
                                   union all
                                   select 'fac_shp_cal_schdl_exc' TAB_NAME from dual
                                   union all
                                   select 'facility_shipping_calendar' TAB_NAME from dual
   
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'shipping_calendar_id'
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;                 
   
                   select min_density_param_id bulk collect into arrIDs
                   from tmp_wm_id_1, min_density_param
                   where i_id = carrier_id
                   and tc_company_id = p_company_id;
                   
                   purge_table ( p_table_name  => 'MIN_DENSITY_PARAM'
                               , p_column_name => 'MIN_DENSITY_PARAM_ID'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                   -- zone_attribute
                   if upper(p_misc_parm_1) = 'ALL'
                   then
                   v_where_clause:=' WHERE t.tc_company_id = ' || p_company_id || '
                                       AND t.zone_id in  ( with carrier_ids as (select distinct ''%'' || V_ID || ''%'' carrier_code from tmp_wm_id_1) select zone_id from zone s, carrier_ids c where s.zone_name like c.carrier_code )';     
                   else
                      v_where_clause:=' WHERE t.tc_company_id = ' || p_company_id || '
                                       AND t.zone_id in (select zone_id from zone
                                                       where zone_name like ''%' || p_misc_parm_1 || '%'' )';
                   end if; 
                   
                   purge_table_where ( p_table_name  => 'ZONE_ATTRIBUTE'
                                     , p_where_clause=> v_where_clause
                                     , p_commit_size => p_commit_size
                                     , p_start_time  => dstart
                                     , p_archive_ind => p_archive_flag
                                     , p_purge_cd    => p_purge_cd
                                     , p_comp_id     => p_company_id
                                     , p_warehouse_id=> p_whse_id
                                     , p_rowcount    => i_rc
                                     , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                   if upper(p_misc_parm_1) = 'ALL'
                   then
                       v_sql := ' select tariff_transit_time_id  from TARIFF_TRANSIT_TIME  s  where' 
                               ||' s.tc_company_id =  '|| p_company_id ||' and  s.o_zone_id in'
                               ||' (   with carrier_ids as (select distinct ''%'' || V_ID || ''%'' carrier_code from tmp_wm_id_1)'   
                               ||' select zone_id from zone s, carrier_ids c where s.zone_name like c.carrier_code ) '
                               ||' or s.d_zone_id in  (   with carrier_ids as (select distinct ''%'' || V_ID || ''%'' carrier_code from tmp_wm_id_1)' 
                               ||' select zone_id from zone s, carrier_ids c where s.zone_name like c.carrier_code  ) ';         
                   else
                      v_sql := ' select tariff_transit_time_id ' 
                            || ' from TARIFF_TRANSIT_TIME '
                            || ' where tc_company_id =  '|| p_company_id
                            || ' and ( o_zone_id in (select zone_id from zone where zone_name like ''%'||p_misc_parm_1||'%'')'
                            || '   or d_zone_id in (select zone_id from zone where zone_name like ''%'||p_misc_parm_1||'%''))'; -- busy
                   end if; 
   
                   execute immediate v_sql bulk collect into arrIDs; 
   
                   purge_table ( p_table_name  => 'TARIFF_TRANSIT_TIME'
                               , p_column_name => 'TARIFF_TRANSIT_TIME_ID'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                   -- tariff_lane
                   if upper(p_misc_parm_1) = 'ALL'
                   then
                   v_where_clause := ' WHERE t.tc_company_id = ' || p_company_id || '
                       AND (
                           t.o_zone_id in ( with carrier_ids as (select distinct ''%'' || V_ID || ''%'' carrier_code from tmp_wm_id_1) select zone_id from zone s, carrier_ids c where s.zone_name like c.carrier_code )  
                           OR
                           t.d_zone_id in ( with carrier_ids as (select distinct ''%'' || V_ID || ''%'' carrier_code from tmp_wm_id_1) select zone_id from zone s, carrier_ids c where s.zone_name like c.carrier_code  )            
                           )';
                   else
                   v_where_clause := ' WHERE t.tc_company_id = ' || p_company_id || '
                       AND (
                           t.o_zone_id in (select zone_id from zone where zone_name like ''%' || p_misc_parm_1 || '%'')
                           OR
                           t.d_zone_id in (select zone_id from zone where zone_name like ''%' || p_misc_parm_1 || '%'')
                           )';
                   end if;   
                   
                   purge_table_where ( p_table_name  => 'TARIFF_LANE'
                                     , p_where_clause=> v_where_clause
                                     , p_commit_size => p_commit_size
                                     , p_start_time  => dstart
                                     , p_archive_ind => p_archive_flag
                                     , p_purge_cd    => p_purge_cd
                                     , p_comp_id     => p_company_id
                                     , p_warehouse_id=> p_whse_id
                                     , p_rowcount    => i_rc
                                     , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                   -- accessorial_rate
                   if upper(p_misc_parm_1) = 'ALL'
                   then
                   v_where_clause:=' WHERE t.tc_company_id = ' || p_company_id || '
                                     AND t.zone_id in ( with carrier_ids as (select distinct ''%'' || V_ID || ''%'' carrier_code from tmp_wm_id_1) select zone_id from zone s, carrier_ids c where s.zone_name like c.carrier_code ) ';            
                   else
                   v_where_clause:=' WHERE t.tc_company_id = ' || p_company_id || '
                                       AND t.zone_id in (select zone_id from zone
                                                       where zone_name like ''%' || p_misc_parm_1 || '%'' )';
                   end if;   
                   
                   purge_table_where ( p_table_name  => 'ACCESSORIAL_RATE'
                                     , p_where_clause=> v_where_clause
                                     , p_commit_size => p_commit_size
                                     , p_start_time  => dstart
                                     , p_archive_ind => p_archive_flag
                                     , p_purge_cd    => p_purge_cd
                                     , p_comp_id     => p_company_id
                                     , p_warehouse_id=> p_whse_id
                                     , p_rowcount    => i_rc
                                     , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                   -- zone
                   if upper(p_misc_parm_1) = 'ALL'
                   then
                      v_sql := ' with carrier_ids as (select distinct ''%'' || V_ID || ''%'' carrier_code from tmp_wm_id_1) select zone_id ' 
                            || ' from zone s, carrier_ids c '
                            || ' where s.tc_company_id =  '|| p_company_id
                            || 'and  s.zone_name like c.carrier_code ' ; 
                   else
                      v_sql := ' select zone_id ' 
                            || ' from ZONE '
                            || ' where tc_company_id =  '|| p_company_id
                            || ' and zone_name like ''%'||p_misc_parm_1||'%'''; -- busy
                   end if;                      
   
                   execute immediate v_sql bulk collect into arrIDs;                 
   
                   
                   purge_table ( p_table_name  => 'ZONE'
                               , p_column_name => 'ZONE_ID'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                   -- lane_accessorial
                   select lane_accessorial_id
                   bulk collect into arrIDs
                   from LANE_ACCESSORIAL, tmp_wm_id_1
                   where tc_company_id = p_company_id
                   and lane_id in (select lane_id
                                   from comb_lane_dtl
                                   where tc_company_id = p_company_id
                                   and carrier_id = i_id)
                   and rating_lane_dtl_seq in (select lane_dtl_seq
                                               from comb_lane_dtl
                                               where tc_company_id = p_company_id
                                               and carrier_id = i_id);
                                               
                   purge_table ( p_table_name  => 'LANE_ACCESSORIAL'
                               , p_column_name => 'LANE_ACCESSORIAL_ID'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                   -- surge_capacity
                   select surge_capacity_id
                   bulk collect into arrIDs
                   from surge_capacity, tmp_wm_id_1
                   where tc_company_id = p_company_id
                   and lane_id in (select lane_id
                                   from comb_lane_dtl
                                   where tc_company_id = p_company_id
                                   and carrier_id = i_id)
                   and rg_lane_dtl_seq in (select lane_dtl_seq
                                           from comb_lane_dtl
                                           where tc_company_id = p_company_id
                                           and carrier_id = i_id);
                                           
                   purge_table ( p_table_name  => 'SURGE_CAPACITY'
                               , p_column_name => 'SURGE_CAPACITY_ID'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                   -- rating_lane_dtl_rate
                   v_where_clause:=' where t.tc_company_id = ' || p_company_id || '
                   and t.lane_id in (select lane_id
                                   from comb_lane_dtl
                                   where tc_company_id = ' || p_company_id || '
                                   and exists (select 1 from tmp_wm_id_1 where i_id=carrier_id))
                   and t.rating_lane_dtl_seq in (select lane_dtl_seq
                                               from comb_lane_dtl
                                               where tc_company_id = ' || p_company_id || '
                                               and exists (select 1 from tmp_wm_id_1 where i_id=carrier_id))';
                                               
                   purge_table_where ( p_table_name  => 'RATING_LANE_DTL_RATE'
                                     , p_where_clause=> v_where_clause
                                     , p_commit_size => p_commit_size
                                     , p_start_time  => dstart
                                     , p_archive_ind => p_archive_flag
                                     , p_purge_cd    => p_purge_cd
                                     , p_comp_id     => p_company_id
                                     , p_warehouse_id=> p_whse_id
                                     , p_rowcount    => i_rc
                                     , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                   -- tariff_zone_rate_tier
                   if upper(p_misc_parm_1) = 'ALL'
                   then
                   v_where_clause:=' where t.tc_company_id = ' || p_company_id || '
                   and t.tariff_zone_id in (
                               select tariff_zone_id
                               from TARIFF_ZONE
                               where tc_company_id = ' || p_company_id || '
                               and tariff_code_id in (
                                       (
                                       select tariff_code_id
                                       from tariff_code  s , (select distinct ''%'' || V_ID || ''%'' carrier_code from tmp_wm_id_1)  c 
                                       where s.tariff_code like c.carrier_code                                     
                                       and s.tc_company_id = ' || p_company_id || ' ) ))';       
                   else
                   v_where_clause:=' where t.tc_company_id = ' || p_company_id || '
                   and t.tariff_zone_id in (
                               select tariff_zone_id
                               from TARIFF_ZONE
                               where tc_company_id = ' || p_company_id || '
                               and tariff_code_id in (
                                       select tariff_code_id
                                       from tariff_code
                                       where tc_company_id = ' || p_company_id || '
                                       and tariff_code like ''%' || p_misc_parm_1 || '%''
                                       )
                               )';
                   end if;                  
                   
                   purge_table_where ( p_table_name  => 'TARIFF_ZONE_RATE_TIER'
                                     , p_where_clause=> v_where_clause
                                     , p_commit_size => p_commit_size
                                     , p_start_time  => dstart
                                     , p_archive_ind => p_archive_flag
                                     , p_purge_cd    => p_purge_cd
                                     , p_comp_id     => p_company_id
                                     , p_warehouse_id=> p_whse_id
                                     , p_rowcount    => i_rc
                                     , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                   -- tariff_zone_rate
                   -- where clause same as for TARIFF_ZONE_RATE_TIER
                   purge_table_where ( p_table_name  => 'TARIFF_ZONE_RATE'
                                     , p_where_clause=> v_where_clause
                                     , p_commit_size => p_commit_size
                                     , p_start_time  => dstart
                                     , p_archive_ind => p_archive_flag
                                     , p_purge_cd    => p_purge_cd
                                     , p_comp_id     => p_company_id
                                     , p_warehouse_id=> p_whse_id
                                     , p_rowcount    => i_rc
                                     , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                   -- tariff_lane
                   -- where clause same as for TARIFF_ZONE_RATE_TIER
                   purge_table_where ( p_table_name  => 'TARIFF_LANE'
                                     , p_where_clause=> v_where_clause
                                     , p_commit_size => p_commit_size
                                     , p_start_time  => dstart
                                     , p_archive_ind => p_archive_flag
                                     , p_purge_cd    => p_purge_cd
                                     , p_comp_id     => p_company_id
                                     , p_warehouse_id=> p_whse_id
                                     , p_rowcount    => i_rc
                                     , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                   -- tariff_zone
                   if upper(p_misc_parm_1) = 'ALL'
                   then
                   v_where_clause := ' where t.tc_company_id = ' || p_company_id || '
                           and t.tariff_code_id in (
                                   with carrier_ids as (select distinct ''%'' || V_ID || ''%'' carrier_code from tmp_wm_id_1) 
                                   select tariff_code_id
                                   from tariff_code s, carrier_ids c 
                                   where s.tariff_code like c.carrier_code 
                                   and s.tc_company_id = ' || p_company_id || ' )';         
                   else
                   v_where_clause := ' where t.tc_company_id = ' || p_company_id || '
                           and t.tariff_code_id in (
                                   select tariff_code_id
                                   from tariff_code
                                   where tc_company_id = ' || p_company_id || '
                                   and tariff_code like ''%' || p_misc_parm_1 || '%'')';
                   end if;   
                                   
                   purge_table_where ( p_table_name  => 'TARIFF_ZONE'
                                     , p_where_clause=> v_where_clause
                                     , p_commit_size => p_commit_size
                                     , p_start_time  => dstart
                                     , p_archive_ind => p_archive_flag
                                     , p_purge_cd    => p_purge_cd
                                     , p_comp_id     => p_company_id
                                     , p_warehouse_id=> p_whse_id
                                     , p_rowcount    => i_rc
                                     , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                   -- tariff_code
                  if upper(p_misc_parm_1) = 'ALL'
                   then
                      with carrier_ids as (select distinct '%' || V_ID || '%' carrier_code from tmp_wm_id_1)                             
                      select tariff_code_id
                      bulk collect into arrIDs
                      from tariff_code s , carrier_ids c 
                      where s.tariff_code like c.carrier_code  
                      and s.tc_company_id = p_company_id ;                                  
                   else
                      select tariff_code_id
                      bulk collect into arrIDs
                      from tariff_code s 
                      where s.tc_company_id = p_company_id  
                      and s.tariff_code like '%' || p_misc_parm_1 || '%';  
                   end if;                 
   
                   for currec in (
                                   select 'RATING_LANE_DTL_RATE' TAB_NAME from dual                
                                   union all
                                   select 'TARIFF_CODE' TAB_NAME from dual				
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                               , p_column_name => 'TARIFF_CODE_ID'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);                
   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;  
   
                   select sr.static_route_id
                   bulk collect into arrIDs
                   from ship_via s, tmp_wm_id_1 t , static_route sr
                   where s.carrier_id = t.i_id
                   and  s.tc_company_id = p_company_id
                   and s.ship_via_id= sr.ship_via_id;
                   
                   for currec in (
                                   select 'STATIC_ROUTE_CALENDAR' TAB_NAME from dual
                                   union all 
                                   select 'STATIC_ROUTE_STOP' TAB_NAME from dual                
                                   union all
                                   select 'STATIC_RULE_ROUTE' TAB_NAME from dual				
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                               , p_column_name => 'STATIC_ROUTE_ID'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);                
   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;  
   
                   -- ship_via and ship_via_incl_accessorial
                   select ship_via_id
                   bulk collect into arrIDs
                   from ship_via s, tmp_wm_id_1 t
                   where s.carrier_id = t.i_id
                   and   s.tc_company_id = p_company_id;
                   
                   for currec in (
                                   select 'SHIP_VIA_INCL_ACCESSORIAL' TAB_NAME from dual
                                   union all
   				select 'STATIC_ROUTE' TAB_NAME from dual				
                                   union all
                                   select 'SHIP_VIA' TAB_NAME from dual
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'SHIP_VIA_ID'
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;  
                                   
                   -- removing accessorial_code, DEP_ACCESSORIAL_CODE_MAP and DELIVERY_AREA_SURCHARGE tables from carrier data purge #MACR00867115 
                   -- bulk_rating_rules
                   select bulk_rating_rules_id
                   bulk collect into arrIDs
                   from bulk_rating_rules, tmp_wm_id_1
                   where tc_company_id = p_company_id
                   and carrier_id = i_id;
                   
                   purge_table ( p_table_name  => 'BULK_RATING_RULES'
                               , p_column_name => 'BULK_RATING_RULES_ID'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                   -- parcel_origin_attr
                   select parcel_origin_attr_id
                   bulk collect into arrIDs
                   from parcel_origin_attr, tmp_wm_id_1
                   where carrier_id = i_id;
                   
                   purge_table ( p_table_name  => 'PARCEL_ORIGIN_ATTR'
                               , p_column_name => 'PARCEL_ORIGIN_ATTR_ID'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                   -- tp_company_service_level
                   v_where_clause := ' where exists (select 1 from tmp_wm_id_1 where i_id = carrier_id)';
                   
                   purge_table_where ( p_table_name  => 'TP_COMPANY_SERVICE_LEVEL'
                                     , p_where_clause=> v_where_clause
                                     , p_commit_size => p_commit_size
                                     , p_start_time  => dstart
                                     , p_archive_ind => p_archive_flag
                                     , p_purge_cd    => p_purge_cd
                                     , p_comp_id     => p_company_id
                                     , p_warehouse_id=> p_whse_id
                                     , p_rowcount    => i_rc
                                     , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                   -- carrier_parameter_event
                   v_where_clause := ' where t.accessorial_param_set_id in (
                                               select accessorial_param_set_id
                                               from ACCESSORIAL_PARAM_SET
                                               where tc_company_id = ' || p_company_id || '
                                               and exists (select 1 from tmp_wm_id_1 where i_id = carrier_id))';
                                               
                   purge_table_where ( p_table_name  => 'CARRIER_PARAMETER_EVENT'
                                     , p_where_clause=> v_where_clause
                                     , p_commit_size => p_commit_size
                                     , p_start_time  => dstart
                                     , p_archive_ind => p_archive_flag
                                     , p_purge_cd    => p_purge_cd
                                     , p_comp_id     => p_company_id
                                     , p_warehouse_id=> p_whse_id
                                     , p_rowcount    => i_rc
                                     , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                   -- accessorial_param_set
                   select accessorial_param_set_id
                   bulk collect into arrIDs
                   from accessorial_param_set, tmp_wm_id_1
                   where tc_company_id = p_company_id
                   and carrier_id = i_id;
                   
                   purge_table ( p_table_name  => 'ACCESSORIAL_PARAM_SET'
                               , p_column_name => 'ACCESSORIAL_PARAM_SET_ID'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                   -- carrier_code_mot
                   v_where_clause := ' where exists (select 1 from tmp_wm_id_1 where i_id = carrier_id)';
                   purge_table_where ( p_table_name  => 'CARRIER_CODE_MOT'
                                     , p_where_clause=> v_where_clause
                                     , p_commit_size => p_commit_size
                                     , p_start_time  => dstart
                                     , p_archive_ind => p_archive_flag
                                     , p_purge_cd    => p_purge_cd
                                     , p_comp_id     => p_company_id
                                     , p_warehouse_id=> p_whse_id
                                     , p_rowcount    => i_rc
                                     , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                   -- comb_lane_dtl
                   delete from tmp_wm_id_2;
                   insert into tmp_wm_id_2 (i_id)
                   select distinct cld.lane_id
                   from comb_lane_dtl cld
                   where cld.tc_company_id = p_company_id
                       and exists
                       (
                           select 1
                           from tmp_wm_id_1 t
                           where t.i_id = cld.carrier_id
                       );
   
                   v_where_clause := ' where t.tc_company_id = ' || p_company_id || '
                                       and exists (select 1 from tmp_wm_id_1 where i_id = carrier_id)';
                   purge_table_where ( p_table_name  => 'COMB_LANE_DTL'
                                     , p_where_clause=> v_where_clause
                                     , p_commit_size => p_commit_size
                                     , p_start_time  => dstart
                                     , p_archive_ind => p_archive_flag
                                     , p_purge_cd    => p_purge_cd
                                     , p_comp_id     => p_company_id
                                     , p_warehouse_id=> p_whse_id
                                     , p_rowcount    => i_rc
                                     , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                   -- comb_lane
                   v_where_clause := ' where exists (select 1 from tmp_wm_id_2 tmp where tmp.i_id = t.lane_id)'
                       || ' and not exists (select 1 from comb_lane_dtl cld where cld.lane_id = t.lane_id)';
                       
                   purge_table_where ( p_table_name  => 'COMB_LANE'
                                     , p_where_clause=> v_where_clause
                                     , p_commit_size => p_commit_size
                                     , p_start_time  => dstart
                                     , p_archive_ind => p_archive_flag
                                     , p_purge_cd    => p_purge_cd
                                     , p_comp_id     => p_company_id
                                     , p_warehouse_id=> p_whse_id
                                     , p_rowcount    => i_rc
                                     , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                    -- manifest tables                
                   select mh.manifest_id
                   bulk collect into arrIDs
                   from tmp_wm_id_1,manifest_hdr mh
                   where mh.CARRIER_ID=i_id;             
                   
                   purge_table ( p_table_name  => 'manifested_lpn'
                               , p_column_name => 'manifest_id'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;               
                   
                   purge_table ( p_table_name  => 'MANIFEST_HDR'
                               , p_column_name => 'manifest_id'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;                
   
                   -- mot and carrier_code
                   select i_id
                   bulk collect into arrIDs
                   from tmp_wm_id_1;
                   
                   for currec in (
                                   select 'MOT' TAB_NAME from dual
                                   union all
                                   select 'CARRIER_CODE' TAB_NAME from dual
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'CARRIER_ID'
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;  
    
               when p_purge_cd = '40' then  -- Items Marked for Deletion Purge
                   dstart := sysdate;
                   i_rows_purged := 0;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Items marked for Deletion Purge', action_name => 'Archiving and Purging');
   
                   delete from tmp_wm_id_1;
                   -- select purge-able item id's
                   insert into tmp_wm_id_1 (i_id)
                     select ITEM_ID
                     from ITEM_CBO
                     where company_id = p_company_id
                     AND   MARK_FOR_DELETION = 1
                     AND AUDIT_LAST_UPDATED_DTTM <= (dstart - p_days)
                     AND NOT EXISTS
                       (SELECT NULL FROM ORDER_LINE_ITEM WHERE ORDER_LINE_ITEM.ITEM_ID = ITEM_CBO.ITEM_ID)
                     AND NOT EXISTS
                       (SELECT NULL FROM PICK_LOCN_DTL WHERE PICK_LOCN_DTL.ITEM_ID   = ITEM_CBO.ITEM_ID)
                     AND NOT EXISTS
                       (SELECT NULL FROM ALLOC_INVN_DTL WHERE ALLOC_INVN_DTL.ITEM_ID   = ITEM_CBO.ITEM_ID)
                     AND NOT EXISTS
                       (SELECT NULL FROM FROZN_INVN_DTL WHERE FROZN_INVN_DTL.ITEM_ID   = ITEM_CBO.ITEM_ID) 
                     AND NOT EXISTS
                        (SELECT NULL FROM CNT_INVN_DTL WHERE CNT_INVN_DTL.ITEM_ID   = ITEM_CBO.ITEM_ID) 
                     AND NOT EXISTS
                        (SELECT NULL FROM WM_INVENTORY WHERE WM_INVENTORY.ITEM_ID   = ITEM_CBO.ITEM_ID) 
                     AND NOT EXISTS
                        (SELECT NULL FROM INVN_VARI_HDR WHERE INVN_VARI_HDR.ITEM_ID   = ITEM_CBO.ITEM_ID)
                     AND NOT EXISTS
                        (SELECT NULL FROM LPN_DETAIL WHERE LPN_DETAIL.ITEM_ID   = ITEM_CBO.ITEM_ID)
                     AND NOT EXISTS
                        (SELECT NULL FROM LPN WHERE LPN.ITEM_ID   = ITEM_CBO.ITEM_ID) 
                     AND NOT EXISTS
                        (SELECT NULL FROM SKU_LOCATION WHERE SKU_LOCATION.SKU_ID   = ITEM_CBO.ITEM_ID) 
                     AND NOT EXISTS
                        (SELECT NULL FROM ITEM_CLASSIFICATION_CODE WHERE ITEM_CLASSIFICATION_CODE.ITEM_ID   = ITEM_CBO.ITEM_ID);                    
   
                     -- WM-52025 including the MARK_FOR_DELETION = 1 check 
                     select ITEM_BOM_HEADER_ID 
                     bulk collect into arrIDs
                     from ( select ITEM_BOM_HEADER_ID 
                     from ITEM_BOM_HEADER_WMS ibhw, tmp_wm_id_1 t 
                            where ibhw.ITEM_ID = t.i_id
                            union
                            select ITEM_BOM_HEADER_ID 
                            from ITEM_BOM_HEADER_WMS
                            where MARK_FOR_DELETION = 1
                            and AUDIT_LAST_UPDATED_DTTM <= (dstart - p_days)); 
                     
                     FOR CURREC in
                       (
                         select 'ITEM_BOM_DETAIL_WMS' tab_name from dual
                         UNION ALL
                         select 'ITEM_BOM_HEADER_WMS' tab_name  from dual
                       )
                     LOOP
                         purge_table ( p_table_name  => currec.tab_name
                                     , p_column_name => 'ITEM_BOM_HEADER_ID'
                                     , p_arrids      => arrIDs
                                     , p_commit_size => p_commit_size
                                     , p_start_time  => dstart
                                     , p_archive_ind => p_archive_flag
                                     , p_purge_cd    => p_purge_cd
                                     , p_comp_id     => p_company_id
                                     , p_warehouse_id=> p_whse_id
                                     , p_rowcount    => i_rc
                                     , p_error_msg   => v_error_msg);
                         i_rows_purged := i_rows_purged + i_rc;
                         if v_error_msg is not null then raise e_StopPurge; end if;
                     END LOOP;
   
   
                   -- Start archive Purge for all item related tables
   
                   FOR CURREC in
                   (
                     select 'ITEM_SUBSTITUTION_DTL_CBO' TAB_NAME,'SUB_ITEM_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_SUBSTITUTION_CBO' TAB_NAME,'BASE_ITEM_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_PACKAGE_CBO' TAB_NAME,'ITEM_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_PRICE_CBO' TAB_NAME,'ITEM_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_COST_CBO' TAB_NAME,'ITEM_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_BUDGETED_COST_TMS' TAB_NAME,'ITEM_ID' COL_NAME from dual
                     UNION ALL
                   --  select 'ITEM_AUDIT_CBO' TAB_NAME,'ITEM_ID' COL_NAME from dual  -- Ticket WM-40055 ITEM_AUDIT_CBO removed 
               --      UNION ALL
                     select 'ITEM_VENDOR' TAB_NAME,'ITEM_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_EPC_PREFIX_WMS' TAB_NAME,'ITEM_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_CNTRY_CMMDTY_CODE' TAB_NAME,'ITEM_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_FACILITY_MAPPING_WMS' TAB_NAME,'ITEM_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_SUPPLIER_XREF_CBO' TAB_NAME,'ITEM_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_SIZE_TMS' TAB_NAME,'ITEM_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_TMS' TAB_NAME,'ITEM_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_GROUP_WMS' TAB_NAME,'ITEM_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_WMS' TAB_NAME,'ITEM_ID' COL_NAME from dual
                     UNION ALL
					 select 'ITEM_MISC_INSTR' TAB_NAME,'ITEM_ID' COL_NAME from dual
                     UNION ALL
					 select 'ITEM_VAS_APPLICABLE' TAB_NAME,'ITEM_ID' COL_NAME from dual
					 UNION ALL
                     select 'ITEM_CBO' TAB_NAME,'ITEM_ID' COL_NAME from dual
                   )
                   LOOP
                      execute immediate 'select distinct '||currec.col_name||
                                        ' from '||currec.tab_name||
                                        ' where '||currec.col_name||' in (select i_id from tmp_wm_id_1)'
                                        bulk collect into arrIDs;
   
                       if currec.tab_name = 'ITEM_FACILITY_MAPPING_WMS' then 
                                   BEGIN
                                        select distinct ITEM_FACILITY_MAPPING_ID bulk collect into arrIDs_3 from
                                        ( select  IFMW.ITEM_FACILITY_MAPPING_ID ITEM_FACILITY_MAPPING_ID from 
                                          ITEM_FACILITY_MAPPING_WMS IFMW , tmp_wm_id_1 tmp
                                          where tmp.i_id = IFMW.ITEM_ID
                                          UNION ALL
                                          select  ITEM_FACILITY_MAPPING_ID ITEM_FACILITY_MAPPING_ID from 
                                          ITEM_FACILITY_MAPPING_WMS IFMW where 
                                          IFMW.MARK_FOR_DELETION = 1 and IFMW.AUDIT_LAST_UPDATED_DTTM <=  dstart - P_days    );
                                        select distinct SLOTITEM_ID  BULK COLLECT INTO arrIDs_2 from
                                        (select  SI.SLOTITEM_ID SLOTITEM_ID from 
                                        ITEM_FACILITY_MAPPING_WMS IFMW , tmp_wm_id_1 tmp , SLOT_ITEM SI
                                        where tmp.i_id = IFMW.ITEM_ID and SI.SKU_ID = IFMW.ITEM_FACILITY_MAPPING_ID
                                        UNION ALL
                                        select  SI.SLOTITEM_ID SLOTITEM_ID from  ITEM_FACILITY_MAPPING_WMS IFMW,SLOT_ITEM SI
                                        where SI.SKU_ID = IFMW.ITEM_FACILITY_MAPPING_ID and 
                                        IFMW.MARK_FOR_DELETION = 1 and IFMW.AUDIT_LAST_UPDATED_DTTM <=  dstart - P_days);
                                       FOR CURREC2 in
                                                   (
                                                     select 'HAVES_NEEDS' TAB_NAME,'SLOT_ITEM_ID' COL_NAME from dual
                                                     UNION ALL
                                                     select 'HN_FAILURE' TAB_NAME,'SLOTITEM_ID' COL_NAME from dual
                                                     UNION ALL
                                                     select 'ITEM_HISTORY' TAB_NAME,'SLOTITEM_ID' COL_NAME from dual
                                                     UNION ALL
                                                     select 'SLOT_ITEM_SCORE' TAB_NAME,'SLOTITEM_ID' COL_NAME from dual
                                                     UNION ALL
                                                     select 'SL_FAILURE_REASONS' TAB_NAME,'SLOTITEM_ID' COL_NAME from dual
                                                   )
                                                    LOOP
                                                     purge_table ( p_table_name  => currec2.tab_name
                                                                 , p_column_name => currec2.col_name
                                                                 , p_arrids      => arrIDs_2
                                                                 , p_commit_size => p_commit_size
                                                                 , p_start_time  => dstart
                                                                 , p_archive_ind => p_archive_flag
                                                                 , p_purge_cd    => p_purge_cd
                                                                 , p_comp_id     => p_company_id
                                                                 , p_warehouse_id=> p_whse_id
                                                                 , p_rowcount    => i_rc
                                                                 , p_error_msg   => v_error_msg); 
                                                     i_rows_purged := i_rows_purged + i_rc;
                                                   END LOOP;     
                                        FOR CURREC3 in
                                                   (
                                                     select 'BIN_XREF' TAB_NAME,'SKU_ID' COL_NAME from dual
                                                     UNION ALL
                                                     select 'ITEM_CAT_XREF' TAB_NAME,'SKU_ID' COL_NAME from dual
                                                     UNION ALL
                                                     select 'MOVE_LIST' TAB_NAME,'ITEM_ID' COL_NAME from dual
                                                     UNION ALL
                                                     select 'ORDER_DTL' TAB_NAME,'SKU_ID' COL_NAME from dual
                                                     UNION ALL
                                                     select 'PALLET_XREF' TAB_NAME,'SKU_ID' COL_NAME from dual
                                                     UNION ALL
                                                     select 'RESERVE_BIN_XREF' TAB_NAME,'RES_SKU_ID' COL_NAME from dual
                                                     UNION ALL
                                                     select 'RESERVE_PALLET_XREF' TAB_NAME,'RES_SKU_ID' COL_NAME from dual
                                                     UNION ALL
                                                     select 'SHELF_SLOT_XREF' TAB_NAME,'SKU_ID' COL_NAME from dual
                                                     UNION ALL
                                                     select 'SLOT_ITEM' TAB_NAME,'SKU_ID' COL_NAME from dual
                                                     UNION ALL
                                                     select 'WA_DETAIL' TAB_NAME,'SKU_ID' COL_NAME from dual
                                                   )  
                                                   LOOP
                                                     purge_table ( p_table_name  => currec3.tab_name
                                                                 , p_column_name => currec3.col_name
                                                                 , p_arrids      => arrIDs_3
                                                                 , p_commit_size => p_commit_size
                                                                 , p_start_time  => dstart
                                                                 , p_archive_ind => p_archive_flag
                                                                 , p_purge_cd    => p_purge_cd
                                                                 , p_comp_id     => p_company_id
                                                                 , p_warehouse_id=> p_whse_id
                                                                 , p_rowcount    => i_rc
                                                                 , p_error_msg   => v_error_msg); 
                                                     i_rows_purged := i_rows_purged + i_rc;
                                                   END LOOP;
                                       exception
                                           when others then
                                               v_msg := v_msg || ' ' || substr(sqlerrm,1,250);
                                               msg_log_insert('PURGE_SLOTTING_CHILD_TABLES(): Exception! -> ' || v_msg);
                                              raise e_arch_purge_failed;
                                   END;  
                           i_rows_purged := i_rows_purged + i_rc;        
                       end if;                    
                      purge_table ( p_table_name  => currec.tab_name
                                 , p_column_name => currec.col_name
                                 , p_arrids      => arrIDs
                                 , p_commit_size => p_commit_size
                                 , p_start_time  => dstart
                                 , p_archive_ind => p_archive_flag
                                 , p_purge_cd    => p_purge_cd
                                 , p_comp_id     => p_company_id
                                 , p_warehouse_id=> p_whse_id
                                 , p_rowcount    => i_rc
                                 , p_error_msg   => v_error_msg); 
                                 
                     i_rows_purged := i_rows_purged + i_rc;
                     if v_error_msg is not null then raise e_StopPurge; end if;
   
                   END LOOP;
                   
                   --WM-52025 including the MARK_FOR_DELETION = 1 check in all child tables.
                   FOR CURREC in
                   (
                     select 'ITEM_SUBSTITUTION_DTL_CBO' TAB_NAME,'ITEM_SUB_HDR_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_SUBSTITUTION_CBO' TAB_NAME,'ITEM_SUB_HDR_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_PACKAGE_CBO' TAB_NAME,'ITEM_PACKAGE_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_PRICE_CBO' TAB_NAME,'ITEM_PRICE_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_COST_CBO' TAB_NAME,'ITEM_COST_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_BUDGETED_COST_TMS' TAB_NAME,'ITEM_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_VENDOR' TAB_NAME,'ITEM_VENDOR_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_EPC_PREFIX_WMS' TAB_NAME,'ITEM_EPC_PREFIX_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_FACILITY_MAPPING_WMS' TAB_NAME,'ITEM_FACILITY_MAPPING_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_SUPPLIER_XREF_CBO' TAB_NAME,'ITEM_SUPPLIER_XREF_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_SIZE_TMS' TAB_NAME,'ITEM_SIZE_ID' COL_NAME from dual
                     UNION ALL
                     select 'ITEM_TMS' TAB_NAME,'ITEM_ID' COL_NAME from dual
                     UNION ALL
                     SELECT 'ITEM_GROUP_WMS' TAB_NAME,'ITEM_GROUP_ID' COL_NAME FROM dual
                     UNION ALL
                     select 'ITEM_WMS' TAB_NAME,'ITEM_ID' COL_NAME from dual
                   )
                   LOOP
   
                    if currec.tab_name  in  ('ITEM_VENDOR') 
                    then 
                       execute immediate                    
                                       '       select ' || currec.col_name
                                       || '       from ' || currec.tab_name
                                       || '       where MARK_FOR_DELETION = 1 '  
                                      bulk collect into arrIDs; 
                     else
                        execute immediate  '       select ' || currec.col_name
                                       || '       from ' || currec.tab_name
                                       || '       where MARK_FOR_DELETION = 1 '            
                                       || '       and AUDIT_LAST_UPDATED_DTTM <=  :dstart - ' ||p_days
                                      bulk collect into arrIDs using dstart ;  
                     end if;
   
                     if sql%rowcount > 0
                     then
                        purge_table ( p_table_name  => currec.tab_name
                                       , p_column_name => currec.col_name
                                       , p_arrids      => arrIDs
                                       , p_commit_size => p_commit_size
                                       , p_start_time  => dstart
                                       , p_archive_ind => p_archive_flag
                                       , p_purge_cd    => p_purge_cd
                                       , p_comp_id     => p_company_id
                                       , p_warehouse_id=> p_whse_id
                                       , p_rowcount    => i_rc
                                       , p_error_msg   => v_error_msg);
                                 
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                     end if;
   
                   END LOOP;
   -- end item purge
   --
               when p_purge_cd = '41' then  -- Error Log Purge
                   dstart := systimestamp;
                   i_rows_purged := 0;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Import Error Logs Purge', action_name => 'Archiving and Purging');
   
                   select error_id
                   bulk collect into arrIDs
                   from error_log
                   where tc_company_id = p_company_id
                   and created_dttm <= dstart - p_days;
				   
				   for currec in (
                                   select 'ERROR_LOG_ARGS' TAB_NAME from dual
                                   union all
                                   select 'ERROR_LOG' TAB_NAME from dual
                                 )
					loop
                   purge_table ( p_table_name  => currec.tab_name
                               , p_column_name => 'ERROR_ID'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
				   
				   end loop;
   
                   select distinct tran_log_id  -- added distinct WM-37179
                   bulk collect into arrIDs
                   from tran_log
                   where created_dttm <= dstart - p_days;
                   
                   -- added as part of CR MACR00852318
                   select distinct tl.message_id
                   bulk collect into arrIDs_1
                   from tran_log tl, tran_log_response_message tlrm
                   where tl.message_id = tlrm.message_id  
                   and created_dttm <= dstart - p_days
                   union all
                   select distinct tlrm.message_id
                   from tran_log_response_message tlrm
                   where not exists (
                                     select 1
                                     from tran_log tl
                                     where tl.message_id = tlrm.message_id 
                                     );    
                                     
                   for currec in (
                                   select 'TRAN_LOG' TAB_NAME from dual
                                   union all
                                   select 'TRAN_LOG_MESSAGE' TAB_NAME from dual                            
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'TRAN_LOG_ID'
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;  
   
                   -- added as part of CR MACR00852318   
                   purge_table ( p_table_name  => 'TRAN_LOG_RESPONSE_MESSAGE'
                               , p_column_name => 'MESSAGE_ID'
                               , p_arrids      => arrIDs_1
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;       
                   
               when p_purge_cd = '42' then  -- Scheduler Event Purge
                   dstart := systimestamp;
                   i_rows_purged := 0;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Scheduler Event Queue Purge', action_name => 'Archiving and Purging');
   
                   -- WM-39034 modified to purge to move the data from _HISTORY tables if the history tables contain any data.
                   v_hist_array := Hist_Array('OM_SCHED_EVENT_HISTORY','SO_ORDER_CHG_VALUE_HISTORY','CONSTEMP_SCHED_EVENT_HISTORY');
   
                   is_table_history_configured(p_hist_array => v_hist_array
                                               ,p_is_config => v_is_config);
                   
                   if v_is_config = 'Y'
                   then
                      lv_om_sched_event := 'OM_SCHED_EVENT_HISTORY';
                      lv_so_order_chg_value := 'SO_ORDER_CHG_VALUE_HISTORY';
                      lv_constemp_sched_event := 'CONSTEMP_SCHED_EVENT_HISTORY';
                   else
                      lv_om_sched_event := 'OM_SCHED_EVENT';
                      lv_so_order_chg_value := 'SO_ORDER_CHG_VALUE';
                      lv_constemp_sched_event := 'CONSTEMP_SCHED_EVENT';
                   end if;
   
                   -- capture master list of purge-able event_id's
                   delete from tmp_wm_id_1;
                   v_sql := 'insert into tmp_wm_id_1 (i_id) ' 
                   ||' select event_id from '
                   || lv_om_sched_event 
                   || ' where scheduled_dttm <= systimestamp - ' || p_days
                   || ' and is_executed = 1 '; 
   
                   execute immediate v_sql;
   
                   v_sql := 'select socv.a_identity ' 
                   ||' from '
                   || lv_so_order_chg_value 
                   || ' socv where exists '
                   || ' (select 1 from tmp_wm_id_1 t where t.i_id = socv.event_id )'; 
   
                   execute immediate v_sql bulk collect into arrIDs;
   
                   purge_table ( p_table_name  => lv_so_order_chg_value
                               , p_column_name => 'a_identity'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                               
                   i_rows_purged := i_rc;            
                   if v_error_msg is not null then raise e_StopPurge; end if;
                   
                   select i_id bulk collect into arrIDs
                   from tmp_wm_id_1;
                   
                   for currec in (
                                   select lv_constemp_sched_event TAB_NAME from dual
                                   union all
                                   select lv_om_sched_event TAB_NAME from dual
                                 )
                   loop              
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'EVENT_ID'
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;  
   
           when p_purge_cd = '43' then  -- Report Queue Purge
                   dstart := systimestamp;
                   i_rows_purged := 0;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Report Queues Purge', action_name => 'Archiving and Purging');
   
                   -- WM-39034 modified to purge to move the data from _HISTORY tables if the history tables contain any data.
                   v_hist_array := Hist_Array('LRF_RPT_PARM_VALUE_DTL_HISTORY','LRF_REPORT_PARAM_VALUE_HISTORY','LRF_REPORT_HISTORY','LRF_EVENT_HISTORY',
                                              'LRF_REP_SCHD_PARAM_DTL_HISTORY','LRF_REP_SCHD_PARAM_HISTORY','LRF_SCHED_EVENT_REPORT_HISTORY','LRF_REPORT_AUDIT_HISTORY');
   
                   is_table_history_configured(p_hist_array => v_hist_array
                                               ,p_is_config => v_is_config);
                   
   
                   if v_is_config = 'Y'
                   then
                      lv_lrf_report_param_value_dtl := 'LRF_RPT_PARM_VALUE_DTL_HISTORY';
                      lv_lrf_report_param_value     := 'LRF_REPORT_PARAM_VALUE_HISTORY';
                      lv_lrf_report                 := 'LRF_REPORT_HISTORY';
                      lv_lrf_event                  := 'LRF_EVENT_HISTORY';
                      lv_lrf_rep_schd_param_dtl     := 'LRF_REP_SCHD_PARAM_DTL_HISTORY';
                      lv_lrf_rep_schd_param         := 'LRF_REP_SCHD_PARAM_HISTORY';
                      lv_lrf_sched_event_report     := 'LRF_SCHED_EVENT_REPORT_HISTORY';
                      lv_lrf_report_audit           := 'LRF_REPORT_AUDIT_HISTORY';
                   else
                      lv_lrf_report_param_value_dtl := 'LRF_REPORT_PARAM_VALUE_DTL';
                      lv_lrf_report_param_value     := 'LRF_REPORT_PARAM_VALUE';
                      lv_lrf_report                 := 'LRF_REPORT';
                      lv_lrf_event                  := 'LRF_EVENT';
                      lv_lrf_rep_schd_param_dtl     := 'LRF_REP_SCHD_PARAM_DTL';
                      lv_lrf_rep_schd_param         := 'LRF_REP_SCHD_PARAM';
                      lv_lrf_sched_event_report     := 'LRF_SCHED_EVENT_REPORT';
                      lv_lrf_report_audit           := 'LRF_REPORT_AUDIT';
                   end if;
   
                   delete from tmp_wm_id_1;
                   v_sql := 'insert into tmp_wm_id_1 (i_id) ' 
                   ||' select lser.event_id from '
                   || lv_lrf_sched_event_report 
                   || ' lser  where lser.executed_dttm <=  systimestamp - ' || p_days
                   || ' and lser.is_executed = 1 '; 
   
                   execute immediate v_sql;
   
                   delete from tmp_wm_id_2;
                   v_sql := 'insert into tmp_wm_id_2 (i_id) ' 
                   ||' select le.lrf_event_id from '
                   || lv_lrf_event 
                   || ' le  where exists ( select 1 from tmp_wm_id_1 t '
                   || '  where t.i_id = le.event_id ) or le.event_id is null ';
   
                   execute immediate v_sql;
   
                   v_sql := ' select l.report_param_value_dtl_id ' 
                   ||' from '
                   || lv_lrf_report_param_value_dtl 
                   || ' l where exists '
                   || ' ( select 1 from '
                   || lv_lrf_report_param_value
                   || ' lrpv where lrpv.rep_param_id = l.rep_param_id and lrpv.report_id = l.report_id '
                   || ' and lrpv.company_id = l.company_id'
                   || ' and exists ('
                   || ' select 1 from '
                   || lv_lrf_report
                   || ' lr where lr.report_id = lrpv.report_id and lr.company_id = lrpv.company_id '
                   || ' and exists ( select 1 from tmp_wm_id_2 t where t.i_id = lr.event_id)))';
   
                   execute immediate v_sql bulk collect into arrIDs;
   
                   purge_table ( p_table_name  => lv_lrf_report_param_value_dtl
                               , p_column_name => 'report_param_value_dtl_id'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
                                   
                   v_where_clause := 'where exists (select 1 from '||lv_lrf_report||' lr,tmp_wm_id_2 tmp '
                                                || ' where lr.report_id = t.report_id'
                                                || '   and lr.company_id = t.company_id'
                                                || '   and tmp.i_id = lr.event_id)'; 
                                      
                   purge_table_where ( p_table_name  => lv_lrf_report_param_value
                                     , p_where_clause=> v_where_clause
                                     , p_commit_size => p_commit_size
                                     , p_start_time  => dstart
                                     , p_archive_ind => p_archive_flag
                                     , p_purge_cd    => p_purge_cd
                                     , p_comp_id     => p_company_id
                                     , p_warehouse_id=> p_whse_id
                                     , p_rowcount    => i_rc
                                     , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
                   
                   v_where_clause := 'where exists (select 1 from tmp_wm_id_2 tmp where tmp.i_id = t.event_id)';
                   purge_table_where ( p_table_name  => lv_lrf_report
                                     , p_where_clause=> v_where_clause
                                     , p_commit_size => p_commit_size
                                     , p_start_time  => dstart
                                     , p_archive_ind => p_archive_flag
                                     , p_purge_cd    => p_purge_cd
                                     , p_comp_id     => p_company_id
                                     , p_warehouse_id=> p_whse_id
                                     , p_rowcount    => i_rc
                                     , p_error_msg   => v_error_msg);
                                     
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;    
   
                   v_where_clause := 'where t.event_id is null and t.MODIFIED_DTTM <= (current_timestamp - ' ||p_days||')';-- WM-36762 adding date criteria to lrf_report.
                   purge_table_where ( p_table_name  => lv_lrf_report
                                     , p_where_clause=> v_where_clause
                                     , p_commit_size => p_commit_size
                                     , p_start_time  => dstart
                                     , p_archive_ind => p_archive_flag
                                     , p_purge_cd    => p_purge_cd
                                     , p_comp_id     => p_company_id
                                     , p_warehouse_id=> p_whse_id
                                     , p_rowcount    => i_rc
                                     , p_error_msg   => v_error_msg);
                                     
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;              
   
                   select i_id
                   bulk collect into arrIDs
                   from tmp_wm_id_2;
   
                   purge_table ( p_table_name  => lv_lrf_event
                               , p_column_name => 'lrf_event_id'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                   v_sql := ' select l.rep_schd_param_dtl_id' 
                   ||' from '
                   || lv_lrf_rep_schd_param_dtl 
                   || ' l where exists '
                   || ' ( select 1 from tmp_wm_id_1 t where t.i_id = l.rep_schd_id)';
   
                   execute immediate v_sql bulk collect into arrIDs;
   
                   purge_table ( p_table_name  => lv_lrf_rep_schd_param_dtl
                               , p_column_name => 'rep_schd_param_dtl_id'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
                   
                   select i_id
                   bulk collect into arrIDs
                   from tmp_wm_id_1;   
                   
                   for currec in (
                                   select lv_lrf_rep_schd_param TAB_NAME,'rep_schd_id' COL_NAME from dual
                                   union all
                                   select lv_lrf_sched_event_report TAB_NAME,'event_id' COL_NAME from dual
                                 )
                   loop           
                        purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => currec.col_name
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg); 
                                   
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;                  
   
                   -- adding table LRF_REPORT_AUDIT related MACR00852788 
                   v_sql := ' select lrf_report_audit_id' 
                   ||' from '
                   || lv_lrf_report_audit
                   || ' where created_dttm <= systimestamp - ' || p_days ;
                   
                   execute immediate v_sql bulk collect into arrIDs;
   
                   purge_table ( p_table_name  => lv_lrf_report_audit
                               , p_column_name => 'LRF_REPORT_AUDIT_ID'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
           when p_purge_cd = '44' then  -- YMS TRANSACTIONAL Data Marked for Deletion Purge
                   dstart := sysdate;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Yard Management Purge', action_name => 'Archiving and Purging');
   
                   delete from tmp_wm_id_1;
                     
                   -- select purge-able APPOINTMENT id's
                   -- modified the logic to not to select parent tasks for which the child taks are in status not equal to (60,80)
                   -- MACR00760773  
                   insert into tmp_wm_id_1 (i_id)  -- modified the logic as part of WM-35281
                   with ttask as (
                       Select TASK_ID, parent_task_id, status, decode(parent_task_id,null,rownum,rownum-level+1) group_num,level task_level
                           from ILM_TASKS
                       where last_updated_dttm <= dstart - p_days -- WM-37473 added date criteria.
                       and company_id = p_company_id -- company being purged
                       start with (parent_task_id is null)
                       connect by (parent_task_id = prior task_id)
                       )
                   select task_id
                   from ttask o
                   where not exists (
                                   -- exclude groups with non-pureable status
                                   select 1
                                   from ttask i
                                   where status not in (60,70,80)
                                   and i.group_num = o.group_num
                                   )
                   order by task_level desc;      
                   
                   select i_id
                   bulk collect into arrIDs
                   from tmp_wm_id_1;
   
                   -- NO validation mentioned for Appointment purge
                   -- Start archive Purge for all item related tables
                   for currec in (
                                   select 'ILM_TASK_HISTORY' TAB_NAME from dual
                                   union all
                                   select 'ILM_YARD_ACTIVITY' TAB_NAME from dual
                                   union all
                                   select 'ILM_YARD_AUDIT' TAB_NAME from dual
                                   union all
                                   select 'ILM_TASK_EVENTS' TAB_NAME from dual
                                   union all
                                   select 'YARD_RECURRING_TASK' TAB_NAME from dual
                                   union all
								   --select 'YARD_AUDIT_TASK' TAB_NAME from dual
                                   --union all -- not applicable to 2016
                                   select 'ILM_TASKS' TAB_NAME from dual
                                 )
                   loop
                       purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'TASK_ID'
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg);
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;
   
                   for currec in (
                                   select 'ILM_TRAILER_EVENTS' TAB_NAME,'CREATED_DTTM' COL_NAME from dual
                                   union all
                                   select 'ILM_DOCK_DOOR_EVENTS' TAB_NAME,'CREATED_DTTM' COL_NAME from dual
                                   union all
                                   select 'ILM_APPT_UTIL_NEW' TAB_NAME,'END_DTTM' COL_NAME from dual
                                   union all
                                   select 'ILM_APPT_SLOT_UTILIZATION' TAB_NAME,'END_DTTM' COL_NAME from dual
                                   union all
                                   select 'SLOT_USAGE_DETAIL' TAB_NAME,'SLOT_END_TIME' COL_NAME from dual
                                   union all
                                   select 'SLOT_TOTAL_CAP_USAGE_DETAIL' TAB_NAME,'SCHEDULE_DAY' COL_NAME from dual
                                 )
                   loop
                       purge_table_where ( p_table_name  => currec.tab_name
                                         , p_where_clause=> 'WHERE t.'||currec.col_name||' <= (current_timestamp - ' ||p_days||')'
                                         , p_commit_size => p_commit_size
                                         , p_start_time  => dstart
                                         , p_archive_ind => p_archive_flag
                                         , p_purge_cd    => p_purge_cd
                                         , p_comp_id     => p_company_id
                                         , p_warehouse_id=> p_whse_id
                                         , p_rowcount    => i_rc
                                         , p_error_msg   => v_error_msg);
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   end loop;
   
                   --Removing OBJECT_TYPE filter in AP_ALERT purge # WM-41025
                   select alert_id
                   bulk collect into arrIDs
                   from ap_alert
                   where last_updated_dttm <= dstart - p_days
                   and alert_status in (2, 3);
                  
                   -- adding AP_ALERT_ACTIVITY_DETAILS,AP_ALERT_ARGS tables. #WM-48911
                   FOR currec in
                     (
                       select 'AP_ALERT_ACTIVITY_DETAILS' tab_name from dual
                       UNION ALL
                       select 'AP_ALERT_ARGS' tab_name  from dual
                       UNION ALL
                       select 'AP_ALERT' tab_name  from dual
                     )
                   LOOP
                       purge_table ( p_table_name  => currec.tab_name
                                   , p_column_name => 'ALERT_ID'
                                   , p_arrids      => arrIDs
                                   , p_commit_size => p_commit_size
                                   , p_start_time  => dstart
                                   , p_archive_ind => p_archive_flag
                                   , p_purge_cd    => p_purge_cd
                                   , p_comp_id     => p_company_id
                                   , p_warehouse_id=> p_whse_id
                                   , p_rowcount    => i_rc
                                   , p_error_msg   => v_error_msg);
                       i_rows_purged := i_rows_purged + i_rc;
                       if v_error_msg is not null then raise e_StopPurge; end if;
                   END LOOP; 
   
                   delete from tmp_wm_id_1;
   
                   -- select purge-able APPOINTMENT id's
                   insert into tmp_wm_id_1 (i_id)
                       select SO_ORDER_CHG_VALUE.EVENT_ID
                       from SO_ORDER_CHG_VALUE, OM_SCHED_EVENT
                       where SO_ORDER_CHG_VALUE.EVENT_ID = OM_SCHED_EVENT.EVENT_ID AND OM_SCHED_EVENT.IS_EXECUTED = 1
                       union all
                       select CONSTEMP_SCHED_EVENT.EVENT_ID
                       from CONSTEMP_SCHED_EVENT, OM_SCHED_EVENT
                       where CONSTEMP_SCHED_EVENT.EVENT_ID = OM_SCHED_EVENT.EVENT_ID AND OM_SCHED_EVENT.IS_EXECUTED = 1
                       union all
                       select LRF_EVENT.EVENT_ID
                       from LRF_EVENT, OM_SCHED_EVENT
                       where LRF_EVENT.EVENT_ID = OM_SCHED_EVENT.EVENT_ID AND OM_SCHED_EVENT.IS_EXECUTED = 1;
                       
                   select EVENT_ID
                   bulk collect into arrIDs
                   from OM_SCHED_EVENT o, TMP_WM_ID_1 t
                   where o.EXECUTED_DTTM <= dstart - p_days
                   and o.EVENT_ID = t.I_ID;   
                   
                   purge_table ( p_table_name  => 'OM_SCHED_EVENT'
                              , p_column_name => 'EVENT_ID'
                              , p_arrids      => arrIDs
                              , p_commit_size => p_commit_size
                              , p_start_time  => dstart
                              , p_archive_ind => p_archive_flag
                              , p_purge_cd    => p_purge_cd
                              , p_comp_id     => p_company_id
                              , p_warehouse_id=> p_whse_id
                              , p_rowcount    => i_rc
                              , p_error_msg   => v_error_msg);                  
   
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
                 --CR : WM-44132 Removing Eligible records of EQUIPMENT_INSTANCE table  
                   select eq.EQUIPMENT_INSTANCE_ID
                   bulk collect into arrIDs
                   from EQUIPMENT_INSTANCE eq
                   where eq.LAST_UPDATED_DTTM <= dstart - p_days
   				   and eq.mark_for_deletion = 1
                   and  
				   not exists (select 1 from ILM_APPT_EQUIPMENTS a where a.EQUIPMENT_INSTANCE_ID = eq.EQUIPMENT_INSTANCE_ID
                             union
                             select 1 from ILM_YARD_ACTIVITY b where (b.EQUIPMENT_ID1 = eq.EQUIPMENT_INSTANCE_ID or b.EQUIPMENT_ID2 = eq.EQUIPMENT_INSTANCE_ID)
                             union
                             select 1 from ILM_TASKS c where c.TRAILER_ID = eq.EQUIPMENT_INSTANCE_ID
                             union
                             select 1 from ILM_YARD_AUDIT d where d.EQUIPMENT_INSTANCE_ID = eq.EQUIPMENT_INSTANCE_ID
                             union
                             select 1 from ILM_TRAILER_EVENTS e where e.TRAILER_ID = eq.EQUIPMENT_INSTANCE_ID 
				             union
                             select 1 from TRAILER_REF t where t.TRAILER_ID = eq.EQUIPMENT_INSTANCE_ID
                             union
                             select 1 from TRAILER_VISIT tr where tr.TRAILER_ID = eq.EQUIPMENT_INSTANCE_ID );
                   
                   purge_table ( p_table_name  => 'EQUIPMENT_INSTANCE'
                              , p_column_name => 'EQUIPMENT_INSTANCE_ID'
                              , p_arrids      => arrIDs
                              , p_commit_size => p_commit_size
                              , p_start_time  => dstart
                              , p_archive_ind => p_archive_flag
                              , p_purge_cd    => p_purge_cd
                              , p_comp_id     => p_company_id
                              , p_warehouse_id=> p_whse_id
                              , p_rowcount    => i_rc
                              , p_error_msg   => v_error_msg);                 
   
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
				   
				   delete from tmp_wm_id_1;
				
				insert into tmp_wm_id_1(i_id)
				select VISIT_ID 
				from TRAILER_VISIT
				where LAST_UPDATED_DTTM <= dstart - p_days
				and FACILITY_ID=p_whse_id
                and not exists(select 1 from TRAILER_REF where ACTIVE_VISIT_ID=VISIT_ID);
				
				select VISIT_DETAIL_ID 
				bulk collect into arrIDs
				from tmp_wm_id_1,TRAILER_VISIT_DETAIL
				where i_id=VISIT_ID
				and not exists(select 1 from TRAILER_REF TF where ACTIVE_VISIT_DETAIL_ID=VISIT_DETAIL_ID);
				
				for CURREC in
				(
					select 'TRAILER_CONTENTS' TAB_NAME  from dual
					UNION ALL
					select 'ILM_YARD_ACTIVITY' TAB_NAME  from dual
				)
				loop
				purge_table ( p_table_name  => currec.tab_name
							, p_column_name => 'VISIT_DETAIL_ID'
							, p_arrids      => arrIDs
							, p_commit_size => p_commit_size
							, p_start_time  => dstart
							, p_archive_ind => p_archive_flag
							, p_purge_cd    => p_purge_cd
							, p_comp_id     => p_company_id
							, p_warehouse_id=> p_whse_id
							, p_rowcount    => i_rc
							, p_error_msg   => v_error_msg);
				i_rows_purged := i_rows_purged + i_rc;
				if v_error_msg is not null then raise e_StopPurge; end if;
				end loop;
			
				select i_id 
				bulk collect into arrIDs
				from tmp_wm_id_1;
				
				for CURREC in
				(
					select 'TRAILER_VISIT_DETAIL' TAB_NAME  from dual
					UNION ALL
					select 'TRAILER_VISIT' TAB_NAME  from dual
				)
				loop
				purge_table ( p_table_name  => currec.tab_name
							, p_column_name => 'VISIT_ID'
							, p_arrids      => arrIDs
							, p_commit_size => p_commit_size
							, p_start_time  => dstart
							, p_archive_ind => p_archive_flag
							, p_purge_cd    => p_purge_cd
							, p_comp_id     => p_company_id
							, p_warehouse_id=> p_whse_id
							, p_rowcount    => i_rc
							, p_error_msg   => v_error_msg);
				i_rows_purged := i_rows_purged + i_rc;
				if v_error_msg is not null then raise e_StopPurge; end if;
				end loop;
                
                delete from tmp_wm_id_4;
                insert into tmp_wm_id_4(I_ID1,I_ID2)
                select CARRIER_ID,DRIVER_ID
                from driver d
                where d.is_active=0
                    and d.driver_status=8
                    and d.LAST_UPDATED_DTTM <= dstart - p_days
                    and not exists (select 1 from ilm_appointments ia where ia.driver_id=d.driver_id)
                    and not exists (select 1 from TRAILER_VISIT_DETAIL tv where tv.driver_id=d.driver_id);
                
                v_where_clause:=' WHERE exists (
                                        select 1
                                        from tmp_wm_id_4 
                                            where i_id1 = t.CARRIER_ID and i_id2 = t.DRIVER_ID)';
                purge_table_where ( p_table_name  => 'driver'
                                  , p_where_clause=> v_where_clause
                                  , p_commit_size => p_commit_size
                                  , p_start_time  => dstart
                                  , p_archive_ind => p_archive_flag
                                  , p_purge_cd    => p_purge_cd
                                  , p_comp_id     => p_company_id
                                  , p_warehouse_id=> p_whse_id
                                  , p_rowcount    => i_rc
                                  , p_error_msg   => v_error_msg);

                i_rows_purged := i_rows_purged + i_rc;
                if v_error_msg is not null then raise e_StopPurge; end if;
   
           when p_purge_cd = '45' then  -- LPN Audit Results Purge
                   dstart := systimestamp;
                   i_rows_purged := 0;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Outbound Audit Results Purge', action_name => 'Archiving and Purging');
   
                   select lpn_audit_results_id
                   bulk collect into arrIDs
                   from lpn_audit_results
                   where last_updated_dttm <= dstart - p_days
                   and facility_id = p_whse_id
                   and tc_company_id = p_company_id;
   
                   purge_table ( p_table_name  => 'LPN_AUDIT_RESULTS'
                               , p_column_name => 'lpn_audit_results_id'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
   
           when p_purge_cd = '46' then  -- JOB_HIST table purge#WM-54617
                   dstart := systimestamp;
                   i_rows_purged := 0;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'JOB History Purge', action_name => 'Archiving and Purging');
   
                   select job_hist_id 
                   bulk collect into arrIDs
                   from JOB_HIST
                   where created_dttm  <= dstart - p_days
                   and facility_id = p_whse_id
                   and tc_company_id = p_company_id;
   
                   purge_table ( p_table_name  => 'JOB_HIST'
                               , p_column_name => 'job_hist_id'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;                 
   
                   when p_purge_cd = '47' then  -- OUTWARD_GEN_EVENTS 
                   dstart := systimestamp;
                   i_rows_purged := 0;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'Outward Gen Events Purge', action_name => 'Archiving and Purging');
   
                   select EVENT_GUID
                   bulk collect into arrIDs
                   from OUTWARD_GEN_EVENTS
                   where EVENT_THROWN_TIME <= dstart - p_days;
                  
                   purge_table ( p_table_name  => 'OUTWARD_GEN_EVENTS'
                               , p_column_name => 'EVENT_GUID'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
           				
   		   when p_purge_cd = '48' then  -- EPI Transaction Log 
                   dstart := systimestamp;
                   i_rows_purged := 0;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'EPI Transaction Log Purge', action_name => 'Archiving and Purging');
   
                   select EPI_TRANSACTION_LOG_ID
                   bulk collect into arrIDs
                   from EPI_TRANSACTION_LOG
                   where CREATED_DTTM <= dstart - p_days;
                  
                   purge_table ( p_table_name  => 'EPI_TRANSACTION_LOG'
                               , p_column_name => 'EPI_TRANSACTION_LOG_ID'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
			when p_purge_cd = '49' then  -- EPI END OF DAY 
                   dstart := systimestamp;
                   i_rows_purged := 0;
   
                   DBMS_APPLICATION_INFO.set_module(module_name => 'EPI END OF DAY Purge', action_name => 'Archiving and Purging');
   
                   select EOD_REQUEST_ID
                   bulk collect into arrIDs
                   from EPI_END_OF_DAY
                   where CREATED_DTTM <= dstart - p_days
				   and O_FACILITY_ID=p_whse_id
				   and TC_COMPANY_ID=p_company_id
				   and EOD_STATUS<>'20';
                  
                   purge_table ( p_table_name  => 'EPI_END_OF_DAY'
                               , p_column_name => 'EOD_REQUEST_ID'
                               , p_arrids      => arrIDs
                               , p_commit_size => p_commit_size
                               , p_start_time  => dstart
                               , p_archive_ind => p_archive_flag
                               , p_purge_cd    => p_purge_cd
                               , p_comp_id     => p_company_id
                               , p_warehouse_id=> p_whse_id
                               , p_rowcount    => i_rc
                               , p_error_msg   => v_error_msg);
                   i_rows_purged := i_rows_purged + i_rc;
                   if v_error_msg is not null then raise e_StopPurge; end if;
			else
                   raise e_InvalidPurgeCode;
           end case;
       else
           extended_wm_archive_pkg(p_company_id          => p_company_id
                                ,p_whse_id             => p_whse_id      
                                ,p_commit_size         => p_commit_size
                                ,p_days                => p_days
                                ,p_purge_cd            => p_purge_cd
                                ,p_archive_flag        => p_archive_flag
                                ,p_child_comp_flag     => p_child_comp_flag 
                                ,p_misc_parm_1         => p_misc_parm_1
                                ,p_misc_parm_2         => p_misc_parm_2
                                ,p_misc_parm_3         => p_misc_parm_3
                                ,p_delink_child        => p_delink_child
                                ,p_invalid_purgecode   => v_invalid_purgecode
                                ,p_rows_purged         => i_rc
                                ,p_error_msg           => v_error_msg ); 
                 
            if v_invalid_purgecode = 'Y'
            then  
               raise e_InvalidPurgeCode;
            end if;               
            i_rows_purged := i_rc;
            if v_error_msg is not null then raise e_StopPurge; end if;    
        end if;

           -- set the return value
        p_rows_purged := i_rows_purged;

        -- MACR00773167: Check Table purge_exceptions for the purge_code/run date
        if gi_purge_exception_count >= 1 then
            v_error_msg:='One or more records were failed to purge.  See table MSG_LOG.MSG like ''ORA-%'' for details.';
             v_error_msg := substr(v_error_msg,1,500) ; 
            msg_log_insert (p_msg_parms => 'exec_purge(): Exception! -> Purge Code('||to_char(p_purge_cd)||') ' || v_error_msg
                           ,p_cd_master_id => p_company_id
                           ,p_warehouse => p_whse_id);
		end if;
		
		if( p_archive_flag <> c_arch_none ) then
			v_sql:='update ARCHIVE_RUN_DETAIL
			set STATUS='''||v_arch_ends||''', END_TIME = SYSTIMESTAMP
			where purge_code='||p_purge_cd||' and STATUS='''||v_arch_starts||'''';
      
        execute immediate v_sql;
		end if;
		        
        if lower(c_debug_yn)='y' then
            rollback;
        end if;

    EXCEPTION
        WHEN e_InvalidPurgeCode THEN
            msg_log_insert(p_msg_parms => 'Exception! exec_purge() -> InvalidPurge');
            raise_application_error (-20001,'ERROR: ' || c_pkg_name || '.exec_purge -> Invalid Purge Code.  Call Failed');

        WHEN e_NoShipPurgeStatus THEN
            msg_log_insert(p_msg_parms => 'Exception! exec_purge() -> NoShipPurgeStatus');
            raise_application_error (-20002,'ERROR: ' || c_pkg_name || '.exec_purge -> COMPANY_PARAMETER.PARAM_DEF=''shipment_purge_status'' not found.  Call Failed');

        WHEN e_arch_purge_failed THEN
            rollback;
            msg_log_insert(p_msg_parms => 'Exception! exec_purge() -> arch_purge_failed');
            raise_application_error (-20003,'ERROR: ' || c_pkg_name || '.exec_purge -> Archive/Purge Failed.');
            
        WHEN e_InvalidWhseID THEN
            msg_log_insert(p_msg_parms => 'Exception! exec_purge() -> e_InvalidWhseID');
            raise_application_error (-20007,'Warehouse ID '|| to_char(p_whse_id)||' not found.');              

        WHEN e_StopPurge THEN
            rollback;
            msg_log_insert (p_msg_parms => 'Exception! exec_purge() -> Purge Code('||to_char(p_purge_cd)||') ' || v_error_msg
                           ,p_msg_id => c_msg_id_error
                           ,p_cd_master_id => p_company_id
                           ,p_warehouse => p_whse_id);
            p_error_msg := v_error_msg;

        WHEN OTHERS THEN
            rollback;
            msg_log_insert(p_msg_parms => 'Exception! exec_purge() -> others ' || substr(sqlerrm,1,250));
            v_msg := substr(sqlerrm,250);
            rec_MsgLog.r_start_dttm := dstart;
            rec_MsgLog.r_cd_master_id := p_company_id;
            rec_MsgLog.r_purge_code := p_purge_cd;
            rec_MsgLog.r_whse_id := p_whse_id;
            rec_MsgLog.r_table_name := null;
            rec_MsgLog.r_msg_id := c_msg_id_error;
            rec_MsgLog.r_rows_archived := 0;
            rec_MsgLog.r_rows_deleted := 0;
            rec_MsgLog.r_end_dttm := systimestamp;
            log_msg (p_rec_MsgLog => rec_MsgLog);
            raise_application_error (-20004,'ERROR: ' || c_pkg_name || '.exec_purge -> others. '|| substr(sqlerrm,1,250));
    END;

    /* BEGIN public PROCEDUREs ************************************************/
    /* Called by Purge UI
           Example:  exec ui_purge('1');
                     or
                     exec ui_purge('1,2,3,4,5');
           Note:     Company id = -1 indicates the job must be run for all companies
    */
    PROCEDURE ui_purge (
                        p_purge_config_ids  varchar2,
                        p_user_id varchar2 default 'PURGE'
                        )
    IS
        rec_purge_config    purge_config%rowtype;
        rec_purge_hist      purge_hist%rowtype;
        i_purge_config_id   purge_config.purge_config_id%type;
        v_id_string         varchar2(500);
    --  
        v_error_msg         purge_hist.message%type;
        v_purge_status      sys_code.code_desc%type;

        i_rows_purged       pls_integer;
        i_ttl_rows_purged   pls_integer default 0;
        d_start_dttm        timestamp(6);
        arrCompIDs          tIDs;
        arrConfig_IDs       tIDs;
        i_num               pls_integer default 0;

        e_PurgeInProgress   exception;
        e_NoDataFound       exception;

        v_lock_procname     varchar2(60);
        v_lock_handle       varchar2(128);
        v_related_purgecodes varchar2(300);
        v_module_name       varchar2(64) default null;
        v_action_name       varchar2(64) default null;
        i_lock_result       pls_integer;

        e_purgeFailure      exception;
        v_purge_failed      char(1) := 'N';
        v_err_config_id     varchar2(50) := NULL;
        v_fail_count        pls_integer := 0;
        i_curr_comp_id      pls_integer;
        v_sql               varchar2(32000);
        v_count 			number;
        v_commit_freq      CONSTANT number := 100000;

    BEGIN

        dbms_session.set_identifier(client_id => 'wm_archive_pkg');  
        
        DBMS_APPLICATION_INFO.READ_MODULE(v_module_name,v_action_name);

        gv_user_id := p_user_id;

	--WM-59405
	v_sql := 'SELECT disp_value FROM message_display WHERE disp_key= :B1 AND to_lang=''en'' AND created_source=''ErrorMessage''';
        
        execute immediate v_sql into gv_success_result using '111809999';
        
        execute immediate v_sql into gv_error_result using '111808888';         

        SELECT COUNT(SYNONYM_NAME) 
        into gv_DbLinkSynCnt
        FROM USER_SYNONYMS US , USER_DB_LINKS UD
        where UD.DB_LINK LIKE 'WMSADMIN_DBLINK%' AND US.DB_LINK = UD.DB_LINK AND synonym_name like 'ARCH_%';        
        
            -- loop through the purge configuration id(s)
            v_id_string := replace(p_purge_config_ids,' ','');
            while v_id_string is not null loop
            begin
                v_purge_failed :='N';
                -- get the next purge_config_id to process
                if instr(v_id_string,',') > 0 then
                    i_purge_config_id := substr(v_id_string, 1, instr(v_id_string,',')-1);
                    -- remove it from the list
                    v_id_string := substr(v_id_string, instr(v_id_string,',')+1);
                else
                    i_purge_config_id := v_id_string;
                    v_id_string := null;
                end if;

                -- retrieve purge parameters from purge_config table
                begin
                    select *
                    into rec_purge_config
                    from purge_config
                    where purge_config_id = i_purge_config_id;
                exception
                    when no_data_found then
                        rec_purge_config.purge_code         := 'n/f';
                        rec_purge_config.nbr_days_old       := 0;
                        rec_purge_config.nbr_rows_to_commit := 0;
                        rec_purge_config.archive_flag       := '-';
                        v_msg := 'Invalid Purge Config ID --> ' || to_char(i_purge_config_id);
                        raise e_NoDataFound;
                end;
                if rec_purge_config.nbr_rows_to_commit < v_commit_freq then
                    rec_purge_config.nbr_rows_to_commit:= v_commit_freq;
                end if;
                -- retrieve purge parameters from purge_config table
                begin

					select code_desc 
                    into gv_purge_desc 
                    from sys_code 
                    where rec_type ='S' and code_type='708' and code_id =rec_purge_config.purge_code;

                exception
                    when no_data_found then
                        rec_purge_config.purge_code         := 'n/f';
                        rec_purge_config.nbr_days_old       := 0;
                        rec_purge_config.nbr_rows_to_commit := 0;
                        rec_purge_config.archive_flag       := '-';
                        v_msg := 'Invalid Purge Config ID --> ' || to_char(i_purge_config_id);
                        raise e_NoDataFound;
                end;           

                msg_log_insert(p_msg_parms => 'BEGIN UI_PURGE() -------------------------------------------------------------------------------------------------------');
                msg_log_insert(p_msg_parms => '--> UI_PURGE() purge_config_id -> ' || i_purge_config_id);                   
                
				v_lock_procname:='wm_archive_pkg.'||lower(user)||'.'||rec_purge_config.purge_code;
				
                SELECT LISTAGG(related_purge_codes, ',') WITHIN GROUP (ORDER BY purge_code) "related_codes"
                  into v_related_purgecodes
                  FROM WM_PURGE_DEPENDENCY
                 where purge_code =rec_purge_config.purge_code;



                select count(*)
                  into v_count                
                  from dba_locks dl, sys.dbms_lock_allocated dla 
                 where  dla.lockid=dl.lock_id1
                   and dla.name in (select 'wm_archive_pkg' || '.' ||lower(user)||'.'|| trim(RELATED_PURGE_CODES) 
                                   from WM_PURGE_DEPENDENCY where purge_code=rec_purge_config.purge_code and nvl(RELATED_PURGE_CODES,'-') <> '-')
                   and dl.lock_type='PL/SQL User Lock';
                
				if v_count>0 then
					raise e_PurgeInProgress;
				else
					dbms_lock.allocate_unique(v_lock_procname, v_lock_handle);
					i_lock_result := dbms_lock.request(v_lock_handle, dbms_lock.x_mode, 0, false);
					if i_lock_result<>0 then
						raise e_PurgeInProgress;
					end if;
				end if;                 

                -- now check the parameters and find the company_ids to process
                if nvl(rec_purge_config.whse_master_id,0) = 0 then
                    begin
                        -- get default whse ids the user is auth'd to process
                        select uu.default_whse_region_id
                        into rec_purge_config.whse_master_id
                        from access_control ac, ucl_user uu
                        where ac.UCL_USER_ID = uu.ucl_user_id
                        and uu.user_name = gv_user_id;

                    exception
                        when no_data_found then
                            v_msg := 'Default Warehouse ID Required but Not Found. Purge Config ID --> ' || to_char(i_purge_config_id);
                            raise e_NoDataFound;
                    end;
                end if;
                if nvl(rec_purge_config.cd_master_id,0) = 0 then
                    begin
                        -- get default company the user is auth'd to process (excludes child companies)
                        select uu.default_business_unit_id
                        into  rec_purge_config.cd_master_id
                        from access_control ac, ucl_user uu
                        where ac.UCL_USER_ID = uu.ucl_user_id
                        and uu.user_name = gv_user_id;

                        arrCompIDs(1) := rec_purge_config.cd_master_id;
                    exception
                        when no_data_found then
                            v_msg := 'Default Company ID Required but Not Found. Purge Config ID --> ' || to_char(i_purge_config_id);
                            raise e_NoDataFound;
                    end;

                elsif rec_purge_config.include_child_company_flag = 'Y' then
                    -- get all child companies for the selected company id
                    select a.company_id
                    bulk collect into arrCompIDs
                    from (
                        select company_id
                        from company
                        start with (company_id = rec_purge_config.cd_master_id) -- company being purged
                        connect by (parent_company_id = prior company_id)
                        order by 1
                        ) a;
                else
                    -- just process the company passed in
                    arrCompIDs(1) := rec_purge_config.cd_master_id;
                end if;

                -- update purge_config to signal start of job
                update_purge_config_status (
                                            p_id     => i_purge_config_id
                                           ,p_status => c_purge_status_inproc
                                            );

                -- get start time for purge_hist insert later
                d_start_dttm := systimestamp;
                -- set row counter for current purge_config_id
                i_ttl_rows_purged := 0;

                -- call the purge job for each of the purge_config_id's purge_type/company combinations
                for i_num in 1..arrCompIDs.count loop
                    -- call the PROCEDURE that does the purge
                    i_rows_purged := 0;
                    i_curr_comp_id := arrCompIDs(i_num);
                                        
                    msg_log_insert(p_msg_parms => '--> UI_PURGE() p_company_id  => ' || arrCompIDs(i_num), p_cd_master_id => arrCompIDs(i_num) );
                    msg_log_insert(p_msg_parms => '--> UI_PURGE() p_whse_id => ' || rec_purge_config.whse_master_id , p_cd_master_id => arrCompIDs(i_num) );
                    msg_log_insert(p_msg_parms => '--> UI_PURGE() p_commit_size => ' || rec_purge_config.nbr_rows_to_commit, p_cd_master_id => arrCompIDs(i_num) );
                    msg_log_insert(p_msg_parms => '--> UI_PURGE() p_days => ' || rec_purge_config.nbr_days_old, p_cd_master_id => arrCompIDs(i_num) );
                    msg_log_insert(p_msg_parms => '--> UI_PURGE() p_purge_cd => ' || rec_purge_config.purge_code, p_cd_master_id => arrCompIDs(i_num) );
                    msg_log_insert(p_msg_parms => '--> UI_PURGE() p_archive_flag => ' || rec_purge_config.archive_flag, p_cd_master_id => arrCompIDs(i_num) );
                    msg_log_insert(p_msg_parms => '--> UI_PURGE() p_include_child_company_flag => ' || rec_purge_config.include_child_company_flag, p_cd_master_id => arrCompIDs(i_num) );
                    msg_log_insert(p_msg_parms => '--> UI_PURGE() p_user_id => ' || gv_user_id, p_cd_master_id => arrCompIDs(i_num) );                                        
                                        

                    rec_purge_config.purge_code := trim(rec_purge_config.purge_code);

                    --*************
                    --Modified WMArchive code to execute conditionally for purge codes <= 100
                    if  rec_purge_config.purge_code <= 100 then
                        -- calling wm_archive_pkg.exec_purge
                        exec_purge (p_company_id        => arrCompIDs(i_num)
                                    ,p_whse_id          => rec_purge_config.whse_master_id
                                    ,p_commit_size      => coalesce(nullif(rec_purge_config.nbr_rows_to_commit, 0), -1)
                                    ,p_days             => rec_purge_config.nbr_days_old
                                    ,p_purge_cd         => rec_purge_config.purge_code
                                    ,p_archive_flag     => rec_purge_config.archive_flag
                                    ,p_child_comp_flag  => rec_purge_config.include_child_company_flag
                                    ,p_misc_parm_1      => rec_purge_config.misc_parm_1
                                    ,p_misc_parm_2      => rec_purge_config.misc_parm_2
                                    ,p_misc_parm_3      => rec_purge_config.misc_parm_3
                                    ,p_delink_child     => rec_purge_config.delink_child
                                    ,p_custom_config    => rec_purge_config.custom_config
                                    ,p_rows_purged      => i_rows_purged
                                    ,p_error_msg        => v_error_msg);

                    else
                        --*************
                        --LMArchive Code Start , calling lm_archive_pkg.exec_purge
                        lm_archive_pkg.exec_purge (p_company_id    => arrCompIDs(i_num)
                                                  ,p_whse_id          => rec_purge_config.whse_master_id
                                                  ,p_commit_size      => coalesce(nullif(rec_purge_config.nbr_rows_to_commit, 0), -1)
                                                  ,p_days             => rec_purge_config.nbr_days_old
                                                  ,p_purge_cd         => rec_purge_config.purge_code
                                                  ,p_archive_flag     => rec_purge_config.archive_flag
                                                  ,p_child_comp_flag  => rec_purge_config.include_child_company_flag
                                                  ,p_user_id          => gv_user_id
                                                  ,p_misc_parm_1      => rec_purge_config.misc_parm_1
                                                  ,p_misc_parm_2      => rec_purge_config.misc_parm_2
                                                  ,p_misc_parm_3      => rec_purge_config.misc_parm_3
                                                  ,p_rows_purged      => i_rows_purged
                                                  ,p_error_msg        => v_error_msg);
                     end if;
                    --*************
                    --LMArchive Code End

                    if v_error_msg is null then
                        -- keep a running total of rows purged for purge_hist
                        i_ttl_rows_purged := nvl(i_ttl_rows_purged,0) + i_rows_purged;
                        v_purge_status := c_purge_status_complete;
                    else
                        v_purge_status := c_purge_status_failed;
                        rollback;
                        exit;
                    end if;
                end loop;  -- next company_id
                -- create a purge history record for each purge_config_id
                insert_purge_hist(
                                 p_purge_config_rec  => rec_purge_config
                                ,p_purge_start_dttm => d_start_dttm
                                ,p_purge_end_dttm   => systimestamp+1/86400
                                ,p_source           => 'UI'
                                ,p_status           => v_purge_status
                                ,p_message          => v_error_msg
                                ,p_nbr_rows_purged  => nvl(i_ttl_rows_purged,0)
                                );

                -- update purge_config to signal job completion
                update_purge_config_status (
                                            p_id     => rec_purge_config.purge_config_id
                                           ,p_status => c_purge_status_complete
                                            );
            exception
                when e_PurgeInProgress then
                   -- release the lock
                    i_lock_result := dbms_lock.release (v_lock_handle);
        
                    msg_log_insert(p_msg_parms => 'Exception! ui_purge(): Unable to execute Purge Job '||rec_purge_config.purge_code||' because one of the following is in progress: '||v_related_purgecodes||'');
                    
                    -- update purge_config with purge failure
                    update_purge_config_status (
                                                p_id     => rec_purge_config.purge_config_id
                                               ,p_status => c_purge_status_failed
                                                );                    
                    -- create a purge history record where processing failed
                    insert_purge_hist(
                                    p_purge_config_rec  => rec_purge_config
                                    ,p_purge_start_dttm => systimestamp
                                    ,p_purge_end_dttm   => systimestamp
                                    ,p_source           => 'UI'
                                    ,p_status           => c_purge_status_failed
                                    ,p_message          => 'Unable to execute Purge Job '||rec_purge_config.purge_code||' because one of the following is in progress: '||v_related_purgecodes||' '
                                    ,p_nbr_rows_purged  => i_ttl_rows_purged
                                    );                                     
                                    
                 when e_NoDataFound then
                     rollback;
                     -- release the lock
                     i_lock_result := dbms_lock.release (v_lock_handle);                     
                     v_msg := substr(v_msg,1,400) ; 
                     msg_log_insert(p_msg_parms => 'Exception! UI_PURGE(): ' || v_msg);
                     
                     -- update purge_config with purge failure
                     update_purge_config_status (
                                                 p_id     => rec_purge_config.purge_config_id
                                                ,p_status => c_purge_status_failed
                                                 );
                     -- create a purge history record where processing failed
                     insert_purge_hist(
                                     p_purge_config_rec  => rec_purge_config
                                     ,p_purge_start_dttm => d_start_dttm
                                     ,p_purge_end_dttm   => systimestamp+1/86400
                                     ,p_source           => 'UI'
                                     ,p_status           => c_purge_status_failed
                                     ,p_message          => 'ui_purge(): '||v_msg
                                     ,p_nbr_rows_purged  => nvl(i_ttl_rows_purged,0)
                                     );    
                                     
                     v_purge_failed := 'Y';
                     v_fail_count := v_fail_count + 1;
                     v_err_config_id := v_err_config_id ||' '||rec_purge_config.purge_config_id;                               
              

                 when others then
                     rollback;
                    -- release the lock
                    i_lock_result := dbms_lock.release (v_lock_handle);                    
                      v_msg := substr(sqlerrm,1,400) ; 
                     msg_log_insert(p_msg_parms => '--> Exception! UI_PURGE() -> ' || v_msg);
                    -- update purge_config with purge failure
                     update_purge_config_status (
                                                 p_id     => rec_purge_config.purge_config_id
                                                ,p_status => c_purge_status_failed
                                                 );
                     -- create a purge history record where processing failed
                     insert_purge_hist(
                                 p_purge_config_rec  => rec_purge_config
                                 ,p_purge_start_dttm => d_start_dttm
                                 ,p_purge_end_dttm   => systimestamp+1/86400
                                 ,p_source           => 'UI'
                                 ,p_status           => c_purge_status_failed
                                 ,p_message          => 'ui_purge(): '||sqlerrm
                                 ,p_nbr_rows_purged  => i_ttl_rows_purged
                                 );
                     v_purge_failed := 'Y';
                     v_fail_count := v_fail_count + 1;
                     v_err_config_id := v_err_config_id ||' '||rec_purge_config.purge_config_id;

                     v_msg := substr(sqlerrm,250);
                     msg_log_insert (p_msg_parms => 'Exception! -> wm_archive_pkg.ui_purge('||to_char(i_purge_config_id)||') ' || v_msg
                                                        ,p_msg_id => c_msg_id_error
                                                        ,p_cd_master_id => i_curr_comp_id 
                                                        ,p_warehouse => rec_purge_config.whse_master_id);

            end;
                -- release the lock
               i_lock_result := dbms_lock.release (v_lock_handle);   
            end loop;  -- next purge_config_id

            msg_log_insert(p_msg_parms => 'END UI_PURGE()  -------------------------------------------------------------------------------------------------------');


            if lower(c_debug_yn)='y' then
                rollback;
            else
                commit;
            end if;

            if ( v_purge_failed = 'Y') then
                raise e_purgeFailure;
            end if;
    
        DBMS_APPLICATION_INFO.set_module(module_name => v_module_name, action_name => v_action_name);
        
        dbms_session.clear_identifier;     

    exception
 
        when e_purgeFailure then
                -- release the lock
				rollback;
                i_lock_result := dbms_lock.release (v_lock_handle);
                if ( v_fail_count > 1 ) then
                    raise_application_error (-20006,'ERROR: ' || c_pkg_name || '.ui_purge -> purge configs '
                                            ||v_err_config_id||' failed. Check purge_status/msg_log table');
                else
                    raise_application_error (-20006,'ERROR: ' || c_pkg_name || '.ui_purge -> purge config '
                                            ||v_err_config_id||' failed. Check purge_status/msg_log table');
                end if;

        when others then
           -- release the lock
            i_lock_result := dbms_lock.release (v_lock_handle);

            v_msg := substr(sqlerrm,250);
            msg_log_insert(p_msg_parms => '--> ui_purge() Exception! -> ' || v_msg);

            -- update purge_config to signal start of job
            update_purge_config_status (
                                        p_id     => rec_purge_config.purge_config_id
                                       ,p_status => c_purge_status_failed
                                        );
            -- create a purge history record where processing failed
            insert_purge_hist(
                            p_purge_config_rec  => rec_purge_config
                            ,p_purge_start_dttm => d_start_dttm
                            ,p_purge_end_dttm   => systimestamp+1/86400
                            ,p_source           => 'UI'
                            ,p_status           => c_purge_status_failed
                            ,p_message          => 'ui_purge(): '||sqlerrm
                            ,p_nbr_rows_purged  => i_ttl_rows_purged
                            );
            rec_MsgLog.r_start_dttm := d_start_dttm;
            rec_MsgLog.r_cd_master_id := rec_purge_config.cd_master_id;
            rec_MsgLog.r_purge_code := rec_purge_config.purge_code;
            rec_MsgLog.r_whse_id := rec_purge_config.whse_master_id;
            rec_MsgLog.r_table_name := v_msg;
            rec_MsgLog.r_msg_id := c_msg_id_error;
            rec_MsgLog.r_rows_archived := 0;
            rec_MsgLog.r_rows_deleted := 0;
            rec_MsgLog.r_end_dttm := systimestamp;
            log_msg (p_rec_MsgLog => rec_MsgLog);

            rollback;
    END;
END wm_archive_pkg;
/
show errors;

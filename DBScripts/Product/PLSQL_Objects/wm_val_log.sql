create or replace procedure wm_val_log
(
    p_msg           in msg_log.msg%type,
    p_sql_log_lvl   in number default 1,
    p_module        in msg_log.module%type default null,
    p_msg_id        in msg_log.msg_id%type default null,
    p_ref_code_2    in msg_log.ref_code_2%type default null,
    p_ref_value_2   in msg_log.ref_value_2%type default null
)
as
    v_pgm_id        msg_log.pgm_id%type;
    v_user_id       user_profile.user_id%type;
    v_max_log_lvl   number(1);
    v_module        msg_log.module%type;
    v_msg_id        msg_log.msg_id%type;
    v_ref_code_1    msg_log.ref_code_1%type;
    v_ref_value_1   msg_log.ref_value_1%type;
    v_ref_code_2    msg_log.ref_code_2%type;
    v_ref_value_2   msg_log.ref_value_2%type;
begin
    select t.log_lvl, t.pgm_id, t.user_id, coalesce(p_module, t.module, 'WAVE'), 
        coalesce(p_msg_id, t.msg_id, '1094'), t.ref_code_1, 
        to_char(t.parent_txn_id) || '(' || to_char(t.txn_id) || ')',
        coalesce(p_ref_code_2, t.ref_code_2), coalesce(p_ref_value_2, t.ref_value_2)
    into v_max_log_lvl, v_pgm_id, v_user_id, v_module, v_msg_id, v_ref_code_1, v_ref_value_1, 
        v_ref_code_2, v_ref_value_2
    from tmp_val_parm t;

    if (p_sql_log_lvl <= v_max_log_lvl)
    then
        wm_val_auton_log(p_msg, v_module, v_msg_id, v_ref_code_1, v_ref_value_1, v_user_id, 
            v_pgm_id, v_ref_code_2, v_ref_value_2);
    end if;    
end;
/
show errors;

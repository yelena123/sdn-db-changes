create or replace procedure wm_unplan_pakd_orders_in_list
(
    p_user_id       in  ucl_user.user_name%type,
    pa_order_list   in  t_num
)
as
    v_cancel_shpmt  number(1);
    va_shpmt_list   t_num;
begin
    if (pa_order_list is null)
    then
        return;
    end if;
    
    select distinct o.shipment_id
    bulk collect into va_shpmt_list
    from orders o
    join table(pa_order_list) t on t.column_value = o.order_id
    where o.is_original_order = 1 and o.do_status >= 150
        and o.shipment_id is not null
        and not exists
        (
            select 1
            from lpn l
            where l.shipment_id = o.shipment_id and l.lpn_facility_status < 99
        )
        and not exists
        (
            select 1
            from orders o1
            where o1.shipment_id = o.shipment_id and o1.do_status < 150 
                and o1.parent_order_id is null
                and o1.order_id != o.order_id
        );

    forall i in 1..pa_order_list.count
    update orders o
    set o.shipment_id = null, o.tc_shipment_id = null, o.order_status = 5,
        o.assigned_mot_id = null, o.assigned_carrier_id = null, 
        o.assigned_service_level_id = null, o.line_haul_ship_via = null, 
        o.last_updated_source = p_user_id, o.last_updated_dttm = sysdate
    where o.order_id = pa_order_list(i) 
        and (o.is_original_order = 1 or o.major_minor_order = 'M')
        and o.do_status >= 150 and o.shipment_id is not null;

    wm_cleanup_stale_shpmts(p_user_id, va_shpmt_list);
end;
/
show errors;

/* INSERT Script for WM47 RFREYEPutawayContainer.xhtml*/

---- INSERT INTO NAVIGATION ---
MERGE INTO NAVIGATION N
USING (SELECT '/wm/putaway/ui/RFREYEPutawayContainer.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/wm/putaway/ui/RFREYEPutawayContainer.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----

MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/putaway/ui/RFREYEPutawayContainer.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/putaway/ui/RFREYEPutawayContainer.xhtml'),
(select app_id from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/putaway/ui/RFPutawayContainer.xhtml')),
(select module_id from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/putaway/ui/RFPutawayContainer.xhtml')),
(select permission_id from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/putaway/ui/RFPutawayContainer.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---

MERGE INTO RESOURCES R
USING (SELECT '/wm/putaway/ui/RFREYEPutawayContainer.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/wm/putaway/ui/RFREYEPutawayContainer.xhtml','ACM',1,NULL);

---- INSERT INTO RESOURCE_PERMISSION ---

MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/putaway/ui/RFREYEPutawayContainer.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/putaway/ui/RFREYEPutawayContainer.xhtml'),(select permission_code from RESOURCE_PERMISSION where resource_id = (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/putaway/ui/RFPutawayContainer.xhtml')));
commit;



/* INSERT Script for WM47 Custom Error Message(Invalid barcode for Tunnel Location scannned) into MESSAGE_MASTER Table */

MERGE INTO MESSAGE_MASTER M
USING (SELECT '110304701' KEY 
FROM DUAL) D
ON (M.KEY = D.KEY)
WHEN NOT MATCHED THEN
INSERT VALUES(SEQ_MESSAGE_MASTER_ID.NEXTVAL,'4701','110304701','WM','CUST','Invalid barcode for Tunnel Location scannned','ErrorMessage','USER','ERROR',NULL,'Y',SYSDATE,SYSDATE);

commit;

/* INSERT Script for WM47 RFREYEPutawayLocnLockEnter.xhtml*/

---- INSERT INTO NAVIGATION ---
MERGE INTO NAVIGATION N
USING (SELECT '/wm/putaway/ui/RFREYEPutawayLocnLockEnter.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/wm/putaway/ui/RFREYEPutawayLocnLockEnter.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

commit;

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----

MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/putaway/ui/RFREYEPutawayLocnLockEnter.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/putaway/ui/RFREYEPutawayLocnLockEnter.xhtml'),
(select app_id from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/putaway/ui/RFPutawayLocnLockEnter.xhtml')),
(select module_id from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/putaway/ui/RFPutawayLocnLockEnter.xhtml')),
(select permission_id from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/putaway/ui/RFPutawayLocnLockEnter.xhtml')),
SYSDATE,null);

commit;

---- INSERT INTO RESOURCES ---

MERGE INTO RESOURCES R
USING (SELECT '/wm/putaway/ui/RFREYEPutawayLocnLockEnter.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/wm/putaway/ui/RFREYEPutawayLocnLockEnter.xhtml','ACM',1,NULL);

commit;

---- INSERT INTO RESOURCE_PERMISSION ---

MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/putaway/ui/RFREYEPutawayLocnLockEnter.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/putaway/ui/RFREYEPutawayLocnLockEnter.xhtml'),(select permission_code from RESOURCE_PERMISSION where resource_id = (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/putaway/ui/RFPutawayLocnLockEnter.xhtml')));
commit;







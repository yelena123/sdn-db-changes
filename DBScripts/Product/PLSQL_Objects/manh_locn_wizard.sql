create or replace procedure manh_locn_wizard
(
    p_locn_parm_id in number,
    p_tc_company_id in number,
    p_user in varchar2,
    p_sucs out number,
    p_req_locn out number,
    p_crt_upd_locn out number,
    p_nbr_lbl out number
) 
as
cursor lwh_cur(locn_parm in number) is
select locn_seq_nbr, whse, locn_class, from_area, to_area, area_incr,
    from_zone, to_zone, zone_incr, from_aisle, to_aisle, aisle_incr,
    from_bay, to_bay, bay_incr, from_lvl, to_lvl, lvl_incr, from_posn,
    to_posn, posn_incr, stat_code, error_seq_nbr, proc_stat_code
from locn_wiz_hdr
where locn_parm_id = locn_parm
order by 1 desc;

cursor area_cur is
select comp_value area
from locn_wizard_locn_comp_tmp
where comp_type = 'A';

cursor zone_cur is
select comp_value zone
from locn_wizard_locn_comp_tmp
where comp_type = 'Z';

cursor aisle_cur is
select comp_value aisle
from locn_wizard_locn_comp_tmp
where comp_type = 'I';

cursor bay_cur is
select comp_value bay
from locn_wizard_locn_comp_tmp
where comp_type = 'B';

cursor lvl_cur is
select comp_value lvl
from locn_wizard_locn_comp_tmp
where comp_type = 'L';

cursor posn_cur is
select comp_value posn
from locn_wizard_locn_comp_tmp
where comp_type = 'P';

sql_errm                    varchar2(512);
incr_cntr                   number:=0;
repl_incr_cntr              number:=0;
v_incr_value                nxt_up_cnt.incr_value%type;
v_use_locn_id_as_brcd       whse_parameters.use_locn_id_as_brcd%type;
p_whse                      locn_wiz_hdr.whse%type;
insert_stat_code            number :=0;
msg_locn_brcd_1             locn_hdr.locn_brcd%type:= null;
cmt_frq                     number := 1000;
x_coord_chk                 varchar2(6) := 'N';
y_coord_chk                 varchar2(6) := 'N';
z_coord_chk                 varchar2(6) := 'N';
repl_x_coord_chk            varchar2(6) := 'N';
repl_y_coord_chk            varchar2(6) := 'N';
repl_z_coord_chk            varchar2(6) := 'N';
v_nbr_lbl                   number;
v_locn_id                   locn_hdr.locn_id%type;
v_locn_id_tmp               locn_hdr.locn_id%type;
v_locn_brcd                 locn_hdr.locn_brcd%type;
v_locn_pick_seq             locn_hdr.locn_pick_seq%type;
v_sku_dedctn_type           locn_hdr.sku_dedctn_type%type;
v_slot_type                 locn_hdr.slot_type%type;
v_putwy_zone                locn_hdr.putwy_zone%type;
v_pull_zone                 locn_hdr.pull_zone%type;
v_pick_detrm_zone           locn_hdr.pick_detrm_zone%type;
v_len                       locn_hdr.len%type;
v_width                     locn_hdr.width%type;
v_ht                        locn_hdr.ht%type;
v_x_coord                   locn_hdr.x_coord%type;
v_y_coord                   locn_hdr.y_coord%type;
v_z_coord                   locn_hdr.z_coord%type;
v_work_grp                  locn_hdr.work_grp%type;
v_work_area                 locn_hdr.work_area%type;
v_last_frozn_date_time      locn_hdr.last_frozn_date_time%type;
v_last_cnt_date_time        locn_hdr.last_cnt_date_time%type;
v_cycle_cnt_pending         locn_hdr.cycle_cnt_pending%type;
v_prt_label_flag            locn_hdr.prt_label_flag%type;
v_travel_aisle              locn_hdr.travel_aisle%type;
v_travel_zone               locn_hdr.travel_zone%type;
v_storage_uom               locn_hdr.storage_uom%type;
v_pick_uom                  locn_hdr.pick_uom%type;
v_slot_unusable             locn_hdr.slot_unusable%type;
v_create_date_time          locn_hdr.create_date_time%type;
v_mod_date_time             locn_hdr.mod_date_time%type;
v_user_id                   locn_hdr.user_id%type;
v_check_digit               locn_hdr.check_digit%type;
v_locn_size_type            resv_locn_hdr.locn_size_type%type;
v_dedctn_item_id            resv_locn_hdr.dedctn_item_id%type;
v_locn_putaway_lock         resv_locn_hdr.locn_putaway_lock%type;
v_invn_lock_code            resv_locn_hdr.invn_lock_code%type;
v_curr_wt                   resv_locn_hdr.curr_wt%type;
v_dirct_wt_rlh              resv_locn_hdr.dirct_wt%type;
v_max_wt_rlh                resv_locn_hdr.max_wt%type;
v_curr_vol_rlh              resv_locn_hdr.curr_vol%type;
v_dirct_vol_rlh             resv_locn_hdr.dirct_vol%type;
v_max_vol_rlh               resv_locn_hdr.max_vol%type;
v_curr_uom_qty_rlh          resv_locn_hdr.curr_uom_qty%type;
v_dirct_uom_qty_rlh         resv_locn_hdr.dirct_uom_qty%type;
v_max_uom_qty_rlh           resv_locn_hdr.max_uom_qty%type;
v_pack_zone_rlh             resv_locn_hdr.pack_zone%type;
v_sort_locn_flag            resv_locn_hdr.sort_locn_flag%type;
v_stag_locn_flag            resv_locn_hdr.INBD_STAGING_FLAG%type;
v_repl_locn_brcd            pick_locn_hdr.repl_locn_brcd%type;
v_repl_check_digit          pick_locn_hdr.repl_check_digit%type;
v_repl_x_coord              pick_locn_hdr.repl_x_coord%type;
v_repl_y_coord              pick_locn_hdr.repl_y_coord%type;
v_repl_z_coord              pick_locn_hdr.repl_z_coord%type;
v_repl_travel_aisle         pick_locn_hdr.repl_travel_aisle%type;
v_repl_travel_zone          pick_locn_hdr.repl_travel_zone%type;
v_putwy_type                pick_locn_hdr.putwy_type%type;
v_max_nbr_of_sku            pick_locn_hdr.max_nbr_of_sku%type;
v_repl_flag                 pick_locn_hdr.repl_flag%type;
v_pick_locn_assign_zone     pick_locn_hdr.pick_locn_assign_zone%type;
v_pick_to_light_flg         pick_locn_hdr.pick_to_light_flag%type;
v_pick_locn_assign_type     pick_locn_hdr.pick_locn_assign_type%type;
v_xcess_wave_need_proc_type pick_locn_hdr.xcess_wave_need_proc_type%type;
v_suppr_pr40_repl           pick_locn_hdr.suppr_pr40_repl%type;
v_comb_4050_repl            pick_locn_hdr.comb_4050_repl%type;
v_rec_type                  pkt_consol_locn.rec_type%type;
v_wave_nbr                  pkt_consol_locn.wave_nbr%type;
v_prty_seq_nbr              pkt_consol_locn.prty_seq_nbr%type;
v_locn_stat_code            pkt_consol_locn.locn_stat_code%type;
v_max_nbr_of_pkts           pkt_consol_locn.max_nbr_of_pkts%type;
v_curr_nbr_of_pkts          pkt_consol_locn.curr_nbr_of_pkts%type;
v_max_nbr_of_swc            pkt_consol_locn.max_nbr_of_swc%type;
v_curr_nbr_of_swc           pkt_consol_locn.curr_nbr_of_swc%type;
v_curr_wt_pcl               pkt_consol_locn.curr_wt%type;
v_max_wt_pcl                pkt_consol_locn.max_wt%type;
v_curr_vol                  pkt_consol_locn.curr_vol%type;
v_max_vol_pcl               pkt_consol_locn.max_vol%type;
v_capcty_uom                pkt_consol_locn.capcty_uom%type;
v_max_uom_qty               pkt_consol_locn.max_uom_qty%type;
v_curr_uom_qty              pkt_consol_locn.curr_uom_qty%type;
v_dirct_wt                  pkt_consol_locn.dirct_wt%type;
v_dirct_vol                 pkt_consol_locn.dirct_vol%type;
v_dirct_uom_qty             pkt_consol_locn.dirct_uom_qty%type;
v_dirct_nbr_of_swc          pkt_consol_locn.dirct_nbr_of_swc%type;
v_dirct_pkts                pkt_consol_locn.dirct_pkts%type;
v_lock_pkt_consol_colm_1    pkt_consol_locn.lock_pkt_consol_colm_1%type;
v_lock_pkt_consol_colm_2    pkt_consol_locn.lock_pkt_consol_colm_2%type;
v_lock_pkt_consol_colm_3    pkt_consol_locn.lock_pkt_consol_colm_3%type;
v_lock_pkt_consol_colm_4    pkt_consol_locn.lock_pkt_consol_colm_4%type;
v_lock_pkt_consol_colm_5    pkt_consol_locn.lock_pkt_consol_colm_5%type;
v_lock_pkt_consol_colm_6    pkt_consol_locn.lock_pkt_consol_colm_6%type;
v_lock_pkt_consol_colm_7    pkt_consol_locn.lock_pkt_consol_colm_7%type;
v_lock_pkt_consol_colm_8    pkt_consol_locn.lock_pkt_consol_colm_8%type;
v_lock_pkt_consol_colm_9    pkt_consol_locn.lock_pkt_consol_colm_9%type;
v_lock_pkt_consol_colm_10   pkt_consol_locn.lock_pkt_consol_colm_10%type;
v_pkt_consol_value_1        pkt_consol_locn.pkt_consol_value_1%type;
v_pkt_consol_value_2        pkt_consol_locn.pkt_consol_value_2%type;
v_pkt_consol_value_3        pkt_consol_locn.pkt_consol_value_3%type;
v_pkt_consol_value_4        pkt_consol_locn.pkt_consol_value_4%type;
v_pkt_consol_value_5        pkt_consol_locn.pkt_consol_value_5%type;
v_pkt_consol_value_6        pkt_consol_locn.pkt_consol_value_6%type;
v_pkt_consol_value_7        pkt_consol_locn.pkt_consol_value_7%type;
v_pkt_consol_value_8        pkt_consol_locn.pkt_consol_value_8%type;
v_pkt_consol_value_9        pkt_consol_locn.pkt_consol_value_9%type;
v_pkt_consol_value_10       pkt_consol_locn.pkt_consol_value_10%type;
v_dock_door_brcd            pkt_consol_locn.dock_door_brcd%type;
v_pkt_consol_attr           pkt_consol_locn.pkt_consol_attr%type;
v_store_nbr                 store_pack_locn_hdr.store_nbr%type;
v_pack_zone                 store_pack_locn_hdr.pack_zone%type;
v_assign_zone               store_pack_locn_hdr.assign_zone%type;
v_max_vendors               store_pack_locn_hdr.max_vendors%type;
v_max_wt                    store_pack_locn_hdr.max_wt%type;
v_max_vol                   store_pack_locn_hdr.max_vol%type;
v_max_items                 store_pack_locn_hdr.max_items%type;
v_max_cost                  store_pack_locn_hdr.max_cost%type;
v_grp_type                  locn_grp.grp_type%type;
v_grp_attr                  locn_grp.grp_attr%type;
--v_msg_log_id                msg_log.msg_log_id%type;

v_temp_locn_insert varchar2(512) := 'insert into locn_wizard_locn_tmp(whse,
    locn_class, area, zone, aisle, bay, lvl, posn) '
    || 'values (:whse, :locn_class, :area, :zne, :aisle, :bay, :lvl, :posn)';

v_process boolean;
v_msg msg_log.msg%type;
v_lang_id user_master.lang_id%type := 'en';

-- whse_locn_mask parameters
v_locn_mask         whse_locn_mask.locn_mask%type;
v_locn_offset_mask  whse_locn_mask.locn_offset_mask%type;
v_area_mask         whse_locn_mask.area_mask%type;
v_area_septr        whse_locn_mask.area_septr%type;
v_area_offset       whse_locn_mask.area_offset%type;
v_zone_mask         whse_locn_mask.zone_mask%type;
v_zone_septr        whse_locn_mask.zone_septr%type;
v_zone_offset       whse_locn_mask.zone_offset%type;
v_aisle_septr       whse_locn_mask.aisle_septr%type;
v_aisle_mask        whse_locn_mask.aisle_mask%type;
v_aisle_offset      whse_locn_mask.aisle_offset%type;
v_bay_septr         whse_locn_mask.bay_septr%type;
v_bay_mask          whse_locn_mask.bay_mask%type;
v_bay_offset        whse_locn_mask.bay_offset%type;
v_lvl_septr         whse_locn_mask.lvl_septr%type;
v_lvl_mask          whse_locn_mask.lvl_mask%type;
v_lvl_offset        whse_locn_mask.lvl_offset%type;
v_posn_septr        whse_locn_mask.posn_septr%type;
v_posn_mask         whse_locn_mask.posn_mask%type;
v_posn_offset       whse_locn_mask.posn_offset%type;


v_whse           varchar2(3);
v_rec_type_id    varchar2(3);
v_pfx_field      varchar2(10);
v_pfx_len        number(9);
v_start_nbr      number(9);
v_end_nbr        number(9);
v_nbr_len        number(9);
--v_incre_value     number(9);
v_nxt_start_nbr  number(9);
v_nxt_end_nbr    number(9);
v_chk_digit_type varchar2(5);
v_chk_digit_len  number(9);
v_repeat_range   varchar2(1);
v_cd_master_id   number(9);
v_nxt_up_cnt_id  number(9);
v_rc             number(9);
v_pfx_type nxt_up_cnt.pfx_type%type;
v_rev_order nxt_up_cnt.rev_order%type;
    
begin

    begin
        select misc_flags
        into cmt_frq
        from sys_code
        where rec_type = 'S'
            and code_type = '116'
            and code_id = '006';
    exception
        when no_data_found then
            cmt_frq := 01000;
    end;

    -- session level parameter changes for functional indexes
    execute immediate ' alter session set query_rewrite_enabled  = true ';
    execute immediate ' alter session set query_rewrite_integrity  = trusted ';

    select nvl(lang_id, 'en')
    into v_lang_id
    from user_master
    where login_user_id = p_user;

    p_req_locn := 0;
    p_crt_upd_locn :=  0;

    for lwh in lwh_cur(p_locn_parm_id)
    loop
        incr_cntr :=  0 ;
        repl_incr_cntr :=  0 ;
        v_incr_value :=  0 ;
        insert_stat_code :=  0 ;
        msg_locn_brcd_1 :=  null ;

        x_coord_chk := 'N';
        y_coord_chk := 'N';
        z_coord_chk := 'N';
        repl_x_coord_chk := 'N';
        repl_y_coord_chk := 'N';
        repl_z_coord_chk := 'N';

        -- log start info
        if lwh.stat_code in (10,50)
        then
            msg_log_insert('INVMGMT', '2039', p_user, lwh.whse, p_tc_company_id,
                lwh.stat_code, p_locn_parm_id, v_lang_id);
        else
            msg_log_insert('INVMGMT', '1690', p_user, lwh.whse, p_tc_company_id,
                lwh.stat_code, p_locn_parm_id, v_lang_id);
        end if;

        update locn_wiz_hdr
        set proc_stat_code = 10
        where locn_parm_id = p_locn_parm_id
            and locn_seq_nbr = lwh.locn_seq_nbr;

        p_whse := lwh.whse;
        v_create_date_time := sysdate;
        v_mod_date_time := sysdate;
        v_user_id := p_user;

        -- for locn_brcd gen
        select wp.use_locn_id_as_brcd
        into v_use_locn_id_as_brcd
        from whse_parameters wp
        join whse_master wm on wm.whse_master_id = wp.whse_master_id
        join locn_wiz_hdr lh on lh.whse = wm.whse
        where lh.locn_parm_id = p_locn_parm_id
        and lh.locn_seq_nbr = lwh.locn_seq_nbr;

       --status code not of type delete, enable and disable
        if lwh.stat_code not in (84, 80, 81)
        then
            -- variables for locn_hdr
            v_sku_dedctn_type := substr(
                get_locn_prop_value(p_locn_parm_id,lwh.locn_seq_nbr,'08',
                p_stat_code => lwh.stat_code,
                p_stat_flag =>
                    case when lwh.stat_code = 83 then 'T' else 'N' end), 1, 1);

            v_slot_type := get_locn_prop_value(p_locn_parm_id, lwh.locn_seq_nbr,
                '22',p_stat_code => lwh.stat_code) ;
            v_putwy_zone := get_locn_prop_value(p_locn_parm_id, lwh.locn_seq_nbr,
                '35',p_stat_code => lwh.stat_code) ;
            v_pull_zone := get_locn_prop_value(p_locn_parm_id, lwh.locn_seq_nbr,
                '33',p_stat_code => lwh.stat_code) ;
            v_pick_detrm_zone := get_locn_prop_value(p_locn_parm_id,
                lwh.locn_seq_nbr,'25',p_stat_code => lwh.stat_code) ;
            v_len := get_locn_prop_value(p_locn_parm_id, lwh.locn_seq_nbr,'09',
                '10',p_stat_code => lwh.stat_code);--, p_stat_flag => 0);
            v_width := get_locn_prop_value(p_locn_parm_id, lwh.locn_seq_nbr,'09',
                '11',p_stat_code => lwh.stat_code);--, p_stat_flag => 0);
            v_ht := get_locn_prop_value(p_locn_parm_id, lwh.locn_seq_nbr,'09',
                '12',p_stat_code => lwh.stat_code);--, p_stat_flag => 0);

            --nvl(get_locn_prop_value(p_locn_parm_id, lwh.locn_seq_nbr,
            -- 'XCORD'), 0);
            --nvl(get_locn_prop_value(p_locn_parm_id, lwh.locn_seq_nbr,
            -- 'YCORD'), 0);
            --nvl(get_locn_prop_value(p_locn_parm_id, lwh.locn_seq_nbr,
            -- 'ZCORD'), 0);

            v_x_coord := 0;
            v_y_coord := 0;
            v_z_coord := 0;

            v_work_grp := get_locn_prop_value(p_locn_parm_id, lwh.locn_seq_nbr,
                '13','14',p_stat_code => lwh.stat_code);
            v_work_area := get_locn_prop_value(p_locn_parm_id, lwh.locn_seq_nbr,
                '13','15',p_stat_code => lwh.stat_code);

            v_last_frozn_date_time := null; /*default*/
            v_last_cnt_date_time := null; /*default*/
            v_cycle_cnt_pending := 'N';
            v_prt_label_flag :=
                case to_number(nvl(get_locn_prop_value(p_locn_parm_id,
                    lwh.locn_seq_nbr, '23'), 0))
                    when 0 then 'N'  else 'Y'
                end;
            /*ask get_locn_prop_value(p_locn_parm_id,lwh.locn_seq_nbr,)*/
            v_travel_aisle := get_locn_prop_value(p_locn_parm_id, lwh.locn_seq_nbr,
                 '19', '21', p_stat_code => lwh.stat_code);
            v_travel_zone := get_locn_prop_value(p_locn_parm_id, lwh.locn_seq_nbr,
                 '19', '20', p_stat_code => lwh.stat_code);
            v_storage_uom := null; /*default*/
            v_pick_uom := null; /*default*/
            v_slot_unusable := 'N'; /*default*/

            v_nbr_lbl := to_number(nvl(get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '23'),0));
            p_nbr_lbl := nvl(p_nbr_lbl, 0) + nvl(v_nbr_lbl, 0);

            -- variables for resv_locn_hdr
            v_locn_size_type := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '34', p_stat_code => lwh.stat_code);
            v_dedctn_item_id  := null; /*default*/
            v_locn_putaway_lock := null; /*default*/
            v_invn_lock_code := null; /*default*/
            v_curr_wt := 0;   /*default*/
            v_dirct_wt_rlh := 0; /*default*/ --2
            v_max_wt_rlh := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '29', '30', p_stat_code => lwh.stat_code,
                 p_stat_flag => 999999999.9999);

            v_curr_vol_rlh := 0; /*default*/--2
            v_dirct_vol_rlh := 0; /*default*/--2
            v_max_vol_rlh := get_locn_prop_value(p_locn_parm_id, lwh.locn_seq_nbr,
                 '29', '31',p_stat_code => lwh.stat_code,
                 p_stat_flag => 999999999.9999);

            v_curr_uom_qty_rlh := 0; /*default*/--2
            v_dirct_uom_qty_rlh := 0; /*default*/--2
            v_max_uom_qty_rlh := get_locn_prop_value(p_locn_parm_id, lwh.locn_seq_nbr,
                 '29', '32', p_stat_code => lwh.stat_code,
                 p_stat_flag => 9999999999.99999);

            v_pack_zone_rlh := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '55', p_stat_code => lwh.stat_code); --2
            v_sort_locn_flag := get_locn_prop_value(p_locn_parm_id, lwh.locn_seq_nbr,
                 '59', p_stat_code => lwh.stat_code,
                 p_stat_flag => 0); /*ask sorting loc?*/ --2
            v_stag_locn_flag := case when get_locn_prop_value(p_locn_parm_id, lwh.locn_seq_nbr,
                 '83', p_stat_code => lwh.stat_code,
                 p_stat_flag => 0) = 'Y' then 'Y' when  get_locn_prop_value(p_locn_parm_id, lwh.locn_seq_nbr,
                 '83', p_stat_code => lwh.stat_code,
                 p_stat_flag => 0) = 'N' then 'N' Else NULL END;

            -- variables for pick_locn_hdr
            v_repl_check_digit := null;/*need to be regenrated or? ask*/
            v_check_digit := 0;

            --nvl(get_locn_prop_value(p_locn_parm_id,
            -- lwh.locn_seq_nbr, 'REPL_X_COO'), 0);
            --nvl(get_locn_prop_value(p_locn_parm_id,
            -- lwh.locn_seq_nbr, 'REPL_Y_COO'), 0);
            --nvl(get_locn_prop_value(p_locn_parm_id,
            -- lwh.locn_seq_nbr, 'REPL_Z_COO'), 0);
            v_repl_x_coord := 0;
            v_repl_y_coord := 0;
            v_repl_z_coord := 0;
            v_repl_travel_aisle := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '75', '68', p_stat_code => lwh.stat_code);
            v_repl_travel_zone := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '75', '67', p_stat_code => lwh.stat_code);
            v_putwy_type := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '28', p_stat_code => lwh.stat_code);
            v_max_nbr_of_sku := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '29', '24', p_stat_code => lwh.stat_code,
                 p_stat_flag => 1);
            v_repl_flag := substr(get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '27', p_stat_code => lwh.stat_code,
                 p_stat_flag => 'N'), 1, 1);
            v_pick_locn_assign_zone := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '26', p_stat_code => lwh.stat_code);
            v_pick_to_light_flg := substr(get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '76', p_stat_code => lwh.stat_code), 1, 1);

            v_pick_locn_assign_type := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '60', p_stat_code => lwh.stat_code);
            v_xcess_wave_need_proc_type := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '27', '61', p_stat_code => lwh.stat_code,
                 p_stat_flag =>0);
            v_suppr_pr40_repl := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '27', '62', p_stat_code => lwh.stat_code,
                 p_stat_flag =>0);
            v_comb_4050_repl := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '27', '63', p_stat_code => lwh.stat_code,
                 p_stat_flag =>0);

            -- variables for pkt_consol_locn
            v_rec_type := substr(get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '36', p_stat_code => lwh.stat_code,
                 p_stat_flag =>'P'), 1, 1);
            v_wave_nbr := 0;
            v_prty_seq_nbr := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '38', p_stat_code => lwh.stat_code,
                 p_stat_flag =>100);
            v_locn_stat_code := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '37', p_stat_code => lwh.stat_code,
                 p_stat_flag =>'00');
            v_max_nbr_of_pkts := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '29', '39', p_stat_code => lwh.stat_code,
                 p_stat_flag =>999999999);
            v_curr_nbr_of_pkts := 0;
            v_max_nbr_of_swc := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '29', '41', p_stat_code => lwh.stat_code,
                 p_stat_flag =>999999);
            v_curr_nbr_of_swc := 0;
            v_curr_wt_pcl := 0;
            v_max_wt_pcl := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '29', '30', p_stat_code => lwh.stat_code,
                 p_stat_flag =>999999999.9999); --2
            v_curr_vol := 0;
            v_max_vol_pcl := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '29', '31', p_stat_code => lwh.stat_code,
                 p_stat_flag =>999999999.9999); --2
            v_capcty_uom := substr(get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '29', '40', p_stat_code => lwh.stat_code,
                 p_stat_flag =>'C'),1,1);
            v_max_uom_qty := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '29', '32', p_stat_code => lwh.stat_code,
                 p_stat_flag =>9999999999.99999);
            v_curr_uom_qty := 0;
            v_dirct_wt := 0;
            v_dirct_vol := 0;
            v_dirct_uom_qty := 0;
            v_dirct_nbr_of_swc := 0;
            v_dirct_pkts := 0;

            v_lock_pkt_consol_colm_1 := case(get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '44', '45', p_stat_code => lwh.stat_code,
                 p_stat_flag =>'N')) when '-9' then 'N' else 'Y' end;
            v_lock_pkt_consol_colm_2 := case(get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '44', '46', p_stat_code => lwh.stat_code,
                 p_stat_flag =>'N')) when '-9' then 'N' else 'Y' end;
            v_lock_pkt_consol_colm_3 := case(get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '44', '47',p_stat_code => lwh.stat_code,
                 p_stat_flag =>'N')) when '-9' then 'N' else 'Y' end;
            v_lock_pkt_consol_colm_4 := case(get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '44', '48', p_stat_code => lwh.stat_code,
                 p_stat_flag =>'N')) when '-9' then 'N' else 'Y' end;
            v_lock_pkt_consol_colm_5 := case(get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '44', '49', p_stat_code => lwh.stat_code,
                 p_stat_flag =>'N')) when '-9' then 'N' else 'Y' end;
            v_lock_pkt_consol_colm_6 := case(get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '44', '50', p_stat_code => lwh.stat_code,
                 p_stat_flag =>'N')) when '-9' then 'N' else 'Y' end;
            v_lock_pkt_consol_colm_7 := case(get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '44', '51', p_stat_code => lwh.stat_code,
                 p_stat_flag =>'N')) when '-9' then 'N' else 'Y' end;
            v_lock_pkt_consol_colm_8 := case(get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '44', '52', p_stat_code => lwh.stat_code,
                 p_stat_flag =>'N')) when '-9' then 'N' else 'Y' end;
            v_lock_pkt_consol_colm_9 := case(get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '44', '53', p_stat_code => lwh.stat_code,
                 p_stat_flag =>'N')) when '-9' then 'N' else 'Y' end;
            v_lock_pkt_consol_colm_10 :=case(get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '44', '54', p_stat_code => lwh.stat_code,
                 p_stat_flag =>'N')) when '-9' then 'N' else 'Y' end;
            v_pkt_consol_value_1 := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '44', '45', p_stat_code => lwh.stat_code);
            v_pkt_consol_value_2 := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '44', '46', p_stat_code => lwh.stat_code);
            v_pkt_consol_value_3 := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '44', '47', p_stat_code => lwh.stat_code);
            v_pkt_consol_value_4 := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '44', '48', p_stat_code => lwh.stat_code);
            v_pkt_consol_value_5 := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '44', '49', p_stat_code => lwh.stat_code);
            v_pkt_consol_value_6 := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '44', '50', p_stat_code => lwh.stat_code);
            v_pkt_consol_value_7 := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '44', '51', p_stat_code => lwh.stat_code);
            v_pkt_consol_value_8 := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '44', '52', p_stat_code => lwh.stat_code);
            v_pkt_consol_value_9 := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '44', '53', p_stat_code => lwh.stat_code);
            v_pkt_consol_value_10 := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '44', '54', p_stat_code => lwh.stat_code);
            v_dock_door_brcd := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '42', p_stat_code => lwh.stat_code);
            v_pkt_consol_attr := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '43', p_stat_code => lwh.stat_code);

            -- variables for store_pack_locn_hdr
            v_store_nbr := null;
            v_pack_zone := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '55', p_stat_code => lwh.stat_code);
            v_assign_zone := null;
            v_max_vendors := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '29', '56', p_stat_code => lwh.stat_code,
                 p_stat_flag =>99999);
            v_max_wt := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '29', '30', p_stat_code => lwh.stat_code,
                 p_stat_flag =>999999999.9999);
            v_max_vol := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '29', '31', p_stat_code => lwh.stat_code,
                 p_stat_flag =>999999999.9999);
            v_max_items := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '29', '57', p_stat_code => lwh.stat_code,
                 p_stat_flag =>99999999999.99);
            v_max_cost := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '29', '58', p_stat_code => lwh.stat_code,
                 p_stat_flag =>99999999999.99);

            -- variables for locn_grp
            --update
            begin
                v_grp_type := get_locn_prop_value(p_locn_parm_id,
                    lwh.locn_seq_nbr, '16', '17', p_stat_code => lwh.stat_code);
            exception
                when others then
                    v_grp_type := null;
            end;
        end if;

        if v_grp_type is not null
        then
            v_grp_attr := get_locn_prop_value(p_locn_parm_id,
                 lwh.locn_seq_nbr, '16', '18', p_stat_code => lwh.stat_code);
        end if;

        -- get whse_locn_mask config for the locn_class specified on the lwh
        select locn_mask, locn_offset_mask, area_mask, area_septr, area_offset,
            zone_mask, zone_septr, zone_offset, aisle_septr, aisle_mask,
            aisle_offset, bay_septr, bay_mask, bay_offset, lvl_septr, lvl_mask,
            lvl_offset, posn_septr, posn_mask, posn_offset
        into v_locn_mask, v_locn_offset_mask, v_area_mask, v_area_septr,
            v_area_offset, v_zone_mask, v_zone_septr, v_zone_offset,
            v_aisle_septr, v_aisle_mask, v_aisle_offset, v_bay_septr, v_bay_mask,
            v_bay_offset, v_lvl_septr, v_lvl_mask, v_lvl_offset, v_posn_septr,
            v_posn_mask, v_posn_offset
        from whse_locn_mask
        where whse = lwh.whse
            and locn_class = lwh.locn_class;

        delete locn_wizard_locn_comp_tmp;

        -- gen area between from and to value
        if lwh.from_area is not null and lwh.to_area is not null
            and v_area_mask is not null
        then
            gen_locn_comp(lwh.from_area, lwh.to_area, nvl(lwh.area_incr, 1), 'A', v_area_mask);
        else
            insert into locn_wizard_locn_comp_tmp(comp_type, comp_value)
            values('A', null);
        end if;

        -- gen zone between from and to value
        if lwh.from_zone is not null and lwh.to_zone is not null
            and v_zone_mask is not null
        then
            gen_locn_comp(lwh.from_zone, lwh.to_zone,nvl(lwh.zone_incr, 1), 'Z', v_zone_mask);
        else
            insert into locn_wizard_locn_comp_tmp(comp_type, comp_value)
            values('Z', null);
        end if;

        -- gen aisle between from and to value
        if lwh.from_aisle is not null and lwh.to_aisle is not null
            and v_aisle_mask is not null
        then
            gen_locn_comp(lwh.from_aisle, lwh.to_aisle, nvl(lwh.aisle_incr, 1), 'I', v_aisle_mask);
        else
            insert into locn_wizard_locn_comp_tmp(comp_type, comp_value)
            values('I', null);
        end if;

        -- gen bay between from and to value
        if lwh.from_bay is not null and lwh.to_bay is not null
            and v_bay_mask is not null
        then
            gen_locn_comp(lwh.from_bay, lwh.to_bay,nvl(lwh.bay_incr, 1), 'B', v_bay_mask);
        else
            insert into locn_wizard_locn_comp_tmp(comp_type, comp_value)
            values('B', null);
        end if;

        -- gen level between from and to value
        if lwh.from_lvl is not null and lwh.to_lvl is not null
            and v_lvl_mask is not null
        then
            gen_locn_comp(lwh.from_lvl, lwh.to_lvl,nvl(lwh.lvl_incr, 1), 'L', v_lvl_mask);
        else
            insert into locn_wizard_locn_comp_tmp(comp_type, comp_value)
            values('L', null);
        end if;

        -- gen posn between from and to value
        if lwh.from_posn is not null and lwh.to_posn is not null
            and v_posn_mask is not null
        then
            gen_locn_comp(lwh.from_posn, lwh.to_posn, nvl(lwh.posn_incr, 1), 'P', v_posn_mask);
        else
           insert into locn_wizard_locn_comp_tmp(comp_type, comp_value)
           values('P', null);
        end if;

        setup_xyz_coordinates(p_locn_parm_id, lwh.locn_seq_nbr, lwh.stat_code,
            lwh.locn_class, x_coord_chk, y_coord_chk, z_coord_chk, v_x_coord,
            v_y_coord, v_z_coord, repl_x_coord_chk, repl_y_coord_chk,
            repl_z_coord_chk, v_repl_x_coord, v_repl_y_coord, v_repl_z_coord);

        p_req_locn := 0;

        delete locn_wizard_locn_tmp;

        --status code of type delete, enable, disable, active and reserve
        if lwh.stat_code in (84,80,81,82,83,85)
        then
            for i in area_cur
            loop
              for j in zone_cur
              loop
                  for k in aisle_cur
                  loop
                      for l in bay_cur
                      loop
                          for m in lvl_cur
                          loop
                              for n in posn_cur
                              loop
                                  v_locn_brcd := rpad(i.area, length(v_area_mask),' ')||v_area_septr||
                                      rpad(j.zone,length(v_zone_mask),' ')||v_zone_septr||
                                      rpad(k.aisle,length(v_aisle_mask),' ')||v_aisle_septr||
                                      rpad(l.bay,length(v_bay_mask),' ')||v_bay_septr||
                                      rpad(m.lvl,length(v_lvl_mask),' ')||v_lvl_septr||
                                      rpad(n.posn,length(v_posn_mask),' ')||v_posn_septr;

                                  if msg_locn_brcd_1 is null
                                  then
                                      msg_locn_brcd_1 := rpad(i.area,length(v_area_mask),' ')||v_area_septr||
                                          rpad(j.zone,length(v_zone_mask),' ')||v_zone_septr||
                                          rpad(k.aisle,length(v_aisle_mask),' ')||v_aisle_septr||
                                          rpad(l.bay,length(v_bay_mask),' ')||v_bay_septr||
                                          rpad(m.lvl,length(v_lvl_mask),' ')||v_lvl_septr||
                                          rpad(n.posn,length(v_posn_mask),' ')||v_posn_septr;
                                  end if;
                                  execute immediate v_temp_locn_insert using lwh.whse,
                                      case lwh.stat_code
                                          when '82' then 'A'
					                      when '85' then 'A'
                                          when '83' then 'R'
                                          else lwh.locn_class
                                      end, i.area, j.zone, k.aisle, l.bay, m.lvl, n.posn;
                              end loop;
                          end loop;
                      end loop;
                  end loop;
              end loop;
            end loop;

            update locn_wizard_locn_tmp tl set (tl.locn_id,tl.dsp_locn)
              = (
                    select locn_hdr.locn_id, locn_hdr.dsp_locn
                    from locn_hdr
                    where nvl(locn_hdr.area,'$') = nvl(tl.area,'$')
                        and nvl(locn_hdr.zone,'$') = nvl(tl.zone,'$')
                        and nvl(locn_hdr.aisle,'$') = nvl(tl.aisle,'$')
                        and nvl(locn_hdr.bay,'$') = nvl(tl.bay,'$')
                        and nvl(locn_hdr.lvl,'$') = nvl(tl.lvl,'$')
                        and nvl(locn_hdr.posn,'$') = nvl(tl.posn,'$')
                        --and locn_hdr.locn_class =
                        --    case lwh.stat_code
                        --        when 83 then 'R'
                        --        when 82 then 'A'
                        --        else tl.locn_class
                        --    end
                        and ( (locn_hdr.locn_class in ('A','C')
                                    and lwh.stat_code in  (82,85) )
                              or (locn_hdr.locn_class = 'R'
                                    and lwh.stat_code = 83 )
                              or ( locn_hdr.locn_class = tl.locn_class
                                    and lwh.stat_code not in (82,83,85))
                            )
                        and locn_hdr.whse = tl.whse
                )
            where tl.locn_id is null;

            select count(1)
            into p_req_locn
            from locn_wizard_locn_tmp
            where locn_id is not null;

            case lwh.stat_code
            -- delete locn
            when 84 then
                v_process := true;

                --validate delete locations
                validate_delete_locn(p_locn_parm_id, p_whse, p_tc_company_id,
                    v_lang_id, p_user, v_create_date_time, lwh.locn_class,
                    v_process);

                if v_process
                then
                    --delete the locations
                    delete_locns(p_user, lwh.whse, p_tc_company_id, v_lang_id,
                        v_create_date_time, lwh.locn_class, p_locn_parm_id,
                        p_crt_upd_locn);
                end if;

            -- disable locn
            when 81 then
                v_process := true;
				
				delete from locn_wizard_locn_tmp t
                where not exists
                (
                    select 1
                    from locn_hdr
                    where coalesce(area, '$') = coalesce(t.area, '$')
                        and coalesce(zone, '$') = coalesce(t.zone, '$')
                        and coalesce(aisle, '$') = coalesce(t.aisle, '$')
                        and coalesce(bay, '$') = coalesce(t.bay, '$')
                        and coalesce(lvl, '$') = coalesce(t.lvl, '$')
                        and coalesce(posn, '$') = coalesce(t.posn, '$')
                        and locn_class = t.locn_class
                        and dsp_locn = t.dsp_locn
                        and whse = p_whse
                )
                and locn_class in ('R', 'A', 'C');

                --validate disable locations
                validate_disable_locn(p_locn_parm_id, p_whse, p_tc_company_id,
                    v_lang_id, p_user, v_create_date_time, lwh.locn_class,
                    v_process);

                if v_process
                then
                    p_crt_upd_locn := 0;

                    --update disable locations
                    update_disable_locn(lwh.locn_class, v_mod_date_time,
                        p_crt_upd_locn);

                end if;

            -- enable locn
            when 80 then
                v_process := true;
				
				 delete from locn_wizard_locn_tmp t
                where not exists
                (
                    select 1
                    from locn_hdr
                    where coalesce(area, '$') = coalesce(t.area, '$')
                        and coalesce(zone, '$') = coalesce(t.zone, '$')
                        and coalesce(aisle, '$') = coalesce(t.aisle, '$')
                        and coalesce(bay, '$') = coalesce(t.bay, '$')
                        and coalesce(lvl, '$') = coalesce(t.lvl, '$')
                        and coalesce(posn, '$') = coalesce(t.posn, '$')
                        and locn_class = t.locn_class
                        and dsp_locn = t.dsp_locn
                        and whse = p_whse
                )
                and locn_class in ('0', '1', '2');

                --validate enable locations
                validate_locn_comp_exists(p_locn_parm_id, p_whse, p_tc_company_id,
                    v_lang_id, p_user, v_create_date_time, v_process);

                if v_process
                then
                    p_crt_upd_locn := 0;

                    --update enable locations
                    update_enable_locn(lwh.locn_class, v_mod_date_time,
                        p_crt_upd_locn);

                end if;

            -- tranfer from A -> R
            when 82 then
                v_process := true;

                --validate active locations
                validate_active_locn(p_locn_parm_id, p_whse, p_tc_company_id,
                    v_lang_id, p_user, v_create_date_time, v_process);

                if v_process
                then
                    --delete pick_locn_dtl and pick_locn_hdr
                    delete pick_locn_dtl pld
                    where exists
                    (
                        select 1
                        from locn_wizard_locn_tmp tl
                        where tl.locn_id = pld.locn_id
                    );

                    delete pick_locn_hdr plh
                    where exists
                    (
                        select 1
                        from locn_wizard_locn_tmp tl
                        where tl.locn_id = plh.locn_id
                    );

                    --delete locn_grp records
                    delete locn_grp lg
                    where lg.locn_id  in
                    (
                        select locn_id
                        from locn_wizard_locn_tmp
                    );

                    --update locn_hdr.locn_class = 'R'
                    update locn_hdr
                    set locn_class = 'R'
                    where locn_id in
                    (
                        select locn_id
                        from locn_wizard_locn_tmp
                    );

                    insert into resv_locn_hdr
                        (locn_id, locn_size_type, dedctn_item_id,
                        locn_putaway_lock, invn_lock_code, curr_wt, dirct_wt,
                        max_wt, curr_vol, dirct_vol, max_vol, curr_uom_qty,
                        dirct_uom_qty, max_uom_qty, create_date_time,
                        mod_date_time, user_id, pack_zone, sort_locn_flag,INBD_STAGING_FLAG,
                        resv_locn_hdr_id, locn_hdr_id)
                    select tl.locn_id, v_locn_size_type, v_dedctn_item_id,
                        v_locn_putaway_lock, v_invn_lock_code, v_curr_wt,
                        v_dirct_wt_rlh, v_max_wt_rlh, v_curr_vol_rlh,
                        v_dirct_vol_rlh, v_max_vol_rlh, v_curr_uom_qty_rlh,
                        v_dirct_uom_qty_rlh, v_max_uom_qty_rlh,
                        v_create_date_time, v_mod_date_time, v_user_id,
                        v_pack_zone_rlh, v_sort_locn_flag,v_stag_locn_flag, resv_locn_hdr_id_seq.nextval, locn_hdr.locn_hdr_id
                    from locn_wizard_locn_tmp tl, locn_hdr
                    where tl.locn_id = locn_hdr.locn_id and tl.locn_id is not null;

                    -- p_crt_upd_locn := sql%rowcount;
                    if v_grp_type is not null
                    then
                        merge into locn_grp lg
                        using
                        (
                            select *
                            from locn_wizard_locn_tmp tl
                            where locn_id is not null
                        ) tl
                        on (lg.locn_id = tl.locn_id)
                        when matched then
                            update
                            set grp_type = replace(v_grp_type,'-9',grp_type),
                                grp_attr = replace(v_grp_attr,'-9',grp_attr),
                                mod_date_time = v_mod_date_time,
                                user_id = v_user_id
                        when not matched then
                            insert (locn_grp_id,grp_type, locn_id, grp_attr,
                                create_date_time, mod_date_time, user_id)
                            values (LOCN_GRP_ID_SEQ.nextval,v_grp_type, tl.locn_id, v_grp_attr,
                                v_create_date_time, v_mod_date_time, v_user_id);
                    elsif v_grp_type is null
                    then
                        delete locn_grp
                        where locn_id = v_locn_id;
                    end if;
                end if;

            -- transfer from R -> A,C
            when 83 then
                v_process := true;

                --validate reserve locations
                validate_reserve_locn(p_locn_parm_id, p_whse, p_tc_company_id,
                    v_lang_id, p_user, v_create_date_time, v_process);

                if v_process
                then
                    --delete resv_locn_hdr and locn_grp records
                    delete resv_locn_hdr rlh where exists
                    (
                        select 1
                        from locn_wizard_locn_tmp tl
                        where tl.locn_id = rlh.locn_id
                    );

                    delete locn_grp lg
                    where exists
                    (
                        select 1
                        from locn_wizard_locn_tmp tl
                        where tl.locn_id = lg.locn_id
                    );

                    --update locn_hdr locn_class = 'A' or 'C'
                    update locn_hdr
                    set locn_class = lwh.locn_class
                    where locn_id in
                    (
                        select locn_id
                        from locn_wizard_locn_tmp
                    );
					
					if v_repl_x_coord is not null then 
                      v_repl_x_coord := get_xyz_coord_value(p_locn_parm_id,
                                lwh.locn_seq_nbr, 'R', 1, 'X',
                                0);
                    end if;
                     
                    if v_repl_y_coord is not null then 
                     v_repl_y_coord := get_xyz_coord_value(p_locn_parm_id,
                                lwh.locn_seq_nbr, 'R', 1, 'Y',
                                0);
                    end if;
                     
                    if v_repl_z_coord is not null then 
                     v_repl_z_coord := get_xyz_coord_value(p_locn_parm_id,
                                lwh.locn_seq_nbr, 'R', 1, 'Z',
                                0);
                    end if;

                    insert into pick_locn_hdr
                        (pick_locn_hdr_id,locn_id, repl_locn_brcd, repl_check_digit,
                        repl_x_coord, repl_y_coord, repl_z_coord,
                        repl_travel_aisle, repl_travel_zone, putwy_type,
                        max_nbr_of_sku, repl_flag, pick_locn_assign_zone,
                        create_date_time, mod_date_time, pick_to_light_flag,
                        user_id, pick_locn_assign_type, suppr_pr40_repl,
                        comb_4050_repl, xcess_wave_need_proc_type, max_vol,
                        max_wt, locn_hdr_id)
                    select pick_locn_hdr_id_seq.nextval, tl.locn_id,
                        v_repl_locn_brcd, v_check_digit, nvl(v_repl_x_coord,0),
                        nvl(v_repl_y_coord,0), nvl(v_repl_z_coord,0),
                        v_repl_travel_aisle, v_repl_travel_zone, v_putwy_type,
                        v_max_nbr_of_sku, v_repl_flag, v_pick_locn_assign_zone,
                        v_create_date_time, v_mod_date_time,
                        v_pick_to_light_flg, v_user_id,
                        v_pick_locn_assign_type, v_suppr_pr40_repl,
                        v_comb_4050_repl, v_xcess_wave_need_proc_type, v_max_vol,
                        v_max_wt, locn_hdr.locn_hdr_id
                    from locn_wizard_locn_tmp tl, locn_hdr 
                    where tl.locn_id = locn_hdr.locn_id and tl.locn_id is not null;
					
                    -- p_crt_upd_locn := sql%rowcount;
                    if v_grp_type is not null
                    then
                        merge into locn_grp lg
                        using
                        (
                            select *
                            from locn_wizard_locn_tmp tl
                            where locn_id is not null
                        ) tl
                        on (lg.locn_id = tl.locn_id)
                        when matched then
                            update
                            set grp_type = replace(v_grp_type, '-9', grp_type),
                                grp_attr = replace(v_grp_attr,'-9',grp_attr),
                                mod_date_time = v_mod_date_time,
                                user_id = v_user_id
                        when not matched then
                            insert (locn_grp_id,grp_type, locn_id, grp_attr,
                                create_date_time, mod_date_time, user_id)
                            values (LOCN_GRP_ID_SEQ.nextval,v_grp_type, tl.locn_id, v_grp_attr,
                                v_create_date_time, v_mod_date_time, v_user_id);
                    elsif v_grp_type is null
                    then
                        delete locn_grp
                        where locn_id = v_locn_id;
                    end if;
                end if;
				
            -- transfer from A -> C or C -> A
            when 85 then
                v_process := true;

                --validate active locations
                validate_active_locn(p_locn_parm_id, p_whse, p_tc_company_id,
                    v_lang_id, p_user, v_create_date_time, v_process);

                if v_process
                then
                    --update locn_hdr locn_class = 'A' or 'C'
                    update locn_hdr
                    set locn_class = lwh.locn_class
                    where locn_id in
                    (
                        select locn_id
                        from locn_wizard_locn_tmp
                    );
                end if;				
				
            end case;
        end if;

        --status code not of type delete, enable and disable
        if lwh.stat_code not in (84,80,81)
        then
            p_req_locn := 0;
            p_crt_upd_locn :=  0;

            -- for loop for locn comp
            for i in area_cur
            loop
                if i.area is not null
                then
                    if x_coord_chk = 'AREA'
                    then
                        v_x_coord := get_xyz_coord_value(p_locn_parm_id,
                            lwh.locn_seq_nbr, 'N', area_cur%rowcount, 'X',
                            v_x_coord);
                    end if;

                    if y_coord_chk = 'AREA'
                    then
                        v_y_coord := get_xyz_coord_value(p_locn_parm_id,
                            lwh.locn_seq_nbr, 'N', area_cur%rowcount, 'Y',
                            v_y_coord);
                    end if;

                    if z_coord_chk = 'AREA'
                    then
                        v_z_coord := get_xyz_coord_value(p_locn_parm_id,
                            lwh.locn_seq_nbr, 'N', area_cur%rowcount, 'Z',
                            v_z_coord);
                    end if;

                    if lwh.locn_class in ('A','C')
                    then
                        if repl_x_coord_chk = 'AREA'
                        then
                            v_repl_x_coord := get_xyz_coord_value(p_locn_parm_id,
                                lwh.locn_seq_nbr, 'R', area_cur%rowcount, 'X',
                                v_repl_x_coord);
                        end if;

                        if repl_y_coord_chk = 'AREA'
                        then
                            v_repl_y_coord := get_xyz_coord_value(p_locn_parm_id,
                                lwh.locn_seq_nbr, 'R', area_cur%rowcount, 'Y',
                                v_repl_y_coord);
                        end if;

                        if repl_z_coord_chk = 'AREA'
                        then
                            v_repl_z_coord := get_xyz_coord_value(p_locn_parm_id,
                                lwh.locn_seq_nbr, 'R', area_cur%rowcount, 'Z',
                                v_repl_z_coord);
                        end if;
                    end if;
                end if;            -- end i.area NN
                for j in zone_cur
                loop
                    if j.zone is not null
                    then
                        if x_coord_chk = 'ZONE'
                        then
                            v_x_coord := get_xyz_coord_value(p_locn_parm_id,
                                lwh.locn_seq_nbr, 'N', zone_cur%rowcount, 'X',
                                v_x_coord);
                        end if;

                        if y_coord_chk = 'ZONE'
                        then
                            v_y_coord := get_xyz_coord_value(p_locn_parm_id,
                                lwh.locn_seq_nbr, 'N', zone_cur%rowcount, 'Y',
                                v_y_coord);
                        end if;

                        if z_coord_chk = 'ZONE'
                        then
                            v_z_coord := get_xyz_coord_value(p_locn_parm_id,
                                lwh.locn_seq_nbr, 'N', zone_cur%rowcount, 'Z',
                                v_z_coord);
                         end if;

                        if lwh.locn_class in ('A','C')
                        then
                            if repl_x_coord_chk = 'ZONE'
                            then
                                v_repl_x_coord := get_xyz_coord_value(p_locn_parm_id,
                                    lwh.locn_seq_nbr, 'R', zone_cur%rowcount, 'X', v_repl_x_coord);
                            end if;

                            if repl_y_coord_chk = 'ZONE'
                            then
                                v_repl_y_coord := get_xyz_coord_value(p_locn_parm_id,
                                    lwh.locn_seq_nbr, 'R', zone_cur%rowcount, 'Y', v_repl_y_coord);
                             end if;

                             if repl_z_coord_chk = 'ZONE'
                             then
                                 v_repl_z_coord := get_xyz_coord_value(p_locn_parm_id,
                                    lwh.locn_seq_nbr, 'R', zone_cur%rowcount, 'Z', v_repl_z_coord);
                             end if;
                        end if;
                    end if;

                    for k in aisle_cur
                    loop
                        if k.aisle is not null
                        then
                            if x_coord_chk = 'AISLE'
                            then
                                v_x_coord := get_xyz_coord_value(p_locn_parm_id,
                                    lwh.locn_seq_nbr, 'N', aisle_cur%rowcount, 'X', v_x_coord);
                            end if;

                            if y_coord_chk = 'AISLE'
                            then
                                v_y_coord := get_xyz_coord_value(p_locn_parm_id,
                                    lwh.locn_seq_nbr, 'N', aisle_cur%rowcount, 'Y', v_y_coord);
                            end if;

                            if z_coord_chk = 'AISLE'
                            then
                                v_z_coord := get_xyz_coord_value(p_locn_parm_id,
                                    lwh.locn_seq_nbr, 'N', aisle_cur%rowcount, 'Z', v_z_coord);
                            end if;

                            if lwh.locn_class in ('A','C')
                            then
                                if repl_x_coord_chk = 'AISLE'
                                then
                                    v_repl_x_coord := get_xyz_coord_value(p_locn_parm_id,
                                        lwh.locn_seq_nbr, 'R', aisle_cur%rowcount, 'X', v_repl_x_coord);
                                end if;

                                if repl_y_coord_chk = 'AISLE'
                                then
                                    v_repl_y_coord := get_xyz_coord_value(p_locn_parm_id,
                                        lwh.locn_seq_nbr, 'R', aisle_cur%rowcount, 'Y', v_repl_y_coord);
                                end if;

                                if repl_z_coord_chk = 'AISLE'
                                then
                                    v_repl_z_coord := get_xyz_coord_value(p_locn_parm_id,
                                        lwh.locn_seq_nbr, 'R', aisle_cur%rowcount, 'Z', v_repl_z_coord);
                                end if;
                            end if;
                        end if;

                        for l in bay_cur
                        loop
                            if l.bay is not null
                            then
                                if x_coord_chk = 'BAY'
                                then
                                    v_x_coord := get_xyz_coord_value(p_locn_parm_id,
                                        lwh.locn_seq_nbr, 'N', bay_cur%rowcount, 'X', v_x_coord);
                                end if;

                                if y_coord_chk = 'BAY'
                                then
                                   v_y_coord := get_xyz_coord_value(p_locn_parm_id,
                                        lwh.locn_seq_nbr, 'N', bay_cur%rowcount, 'Y', v_y_coord);
                                end if;

                                if z_coord_chk = 'BAY'
                                then
                                    v_z_coord := get_xyz_coord_value(p_locn_parm_id,
                                        lwh.locn_seq_nbr, 'N', bay_cur%rowcount, 'Z', v_z_coord);
                                end if;

                                if lwh.locn_class in ('A','C')
                                then
                                    if repl_x_coord_chk = 'BAY'
                                    then
                                        v_repl_x_coord := get_xyz_coord_value(p_locn_parm_id,
                                            lwh.locn_seq_nbr, 'R', bay_cur%rowcount, 'X', v_repl_x_coord);
                                    end if;

                                    if repl_y_coord_chk = 'BAY'
                                    then
                                        v_repl_y_coord := get_xyz_coord_value(p_locn_parm_id,
                                            lwh.locn_seq_nbr, 'R', bay_cur%rowcount, 'Y', v_repl_y_coord);
                                    end if;

                                    if repl_z_coord_chk = 'BAY'
                                    then
                                        v_repl_z_coord := get_xyz_coord_value(p_locn_parm_id,
                                            lwh.locn_seq_nbr, 'R', bay_cur%rowcount, 'Z', v_repl_z_coord);
                                    end if;
                                end if;
                            end if;

                            for m in lvl_cur
                            loop
                                if m.lvl is not null
                                then
                                    if x_coord_chk = 'LVL'
                                    then
                                        v_x_coord := get_xyz_coord_value(p_locn_parm_id,
                                            lwh.locn_seq_nbr, 'N', lvl_cur%rowcount, 'X', v_x_coord);
                                    end if;

                                    if y_coord_chk = 'LVL'
                                    then
                                        v_y_coord := get_xyz_coord_value(p_locn_parm_id,
                                            lwh.locn_seq_nbr, 'N', lvl_cur%rowcount, 'Y', v_y_coord);
                                    end if;

                                    if z_coord_chk = 'LVL'
                                    then
                                        v_z_coord := get_xyz_coord_value(p_locn_parm_id,
                                            lwh.locn_seq_nbr, 'N', lvl_cur%rowcount, 'Z', v_z_coord);
                                    end if;

                                    if lwh.locn_class in ('A','C')
                                    then
                                        if repl_x_coord_chk = 'LVL'
                                        then
                                            v_repl_x_coord := get_xyz_coord_value(p_locn_parm_id,
                                                lwh.locn_seq_nbr, 'R', lvl_cur%rowcount, 'X', v_repl_x_coord);
                                        end if;

                                        if repl_y_coord_chk = 'LVL'
                                        then
                                            v_repl_y_coord := get_xyz_coord_value(p_locn_parm_id,
                                                lwh.locn_seq_nbr, 'R', lvl_cur%rowcount, 'Y', v_repl_y_coord);
                                        end if;

                                        if repl_z_coord_chk = 'LVL'
                                        then
                                            v_repl_z_coord := get_xyz_coord_value(p_locn_parm_id,
                                                lwh.locn_seq_nbr, 'R', lvl_cur%rowcount, 'Z', v_repl_z_coord);
                                        end if;
                                    end if;
                                end if;

                                for n in posn_cur
                                loop
                                    if n.posn is not null
                                    then
                                        if x_coord_chk = 'POSN'
                                        then
                                            v_x_coord := get_xyz_coord_value(p_locn_parm_id,
                                                lwh.locn_seq_nbr, 'N', posn_cur%rowcount, 'X', v_x_coord);
                                        end if;

                                        if y_coord_chk = 'POSN'
                                        then
                                           v_y_coord := get_xyz_coord_value(p_locn_parm_id,
                                                lwh.locn_seq_nbr, 'N', posn_cur%rowcount, 'Y', v_y_coord);
                                        end if;

                                        if z_coord_chk = 'POSN'
                                        then
                                            v_z_coord := get_xyz_coord_value(p_locn_parm_id,
                                                lwh.locn_seq_nbr, 'N', posn_cur%rowcount, 'Z', v_z_coord);
                                        end if;

                                        if lwh.locn_class in ('A','C')
                                        then
                                            if repl_x_coord_chk = 'POSN'
                                            then
                                                v_repl_x_coord := get_xyz_coord_value(p_locn_parm_id,
                                                    lwh.locn_seq_nbr, 'R', posn_cur%rowcount, 'X', v_repl_x_coord);
                                            end if;

                                            if repl_y_coord_chk = 'POSN'
                                            then
                                                v_repl_y_coord := get_xyz_coord_value(p_locn_parm_id,
                                                    lwh.locn_seq_nbr, 'R', posn_cur%rowcount, 'Y', v_repl_y_coord);
                                            end if;

                                            if repl_z_coord_chk = 'POSN'
                                            then
                                                v_repl_z_coord := get_xyz_coord_value(p_locn_parm_id,
                                                    lwh.locn_seq_nbr, 'R', posn_cur%rowcount, 'Z', v_repl_z_coord);
                                            end if;
                                        end if;
                                    end if;

                                    p_req_locn := p_req_locn+1;

                                    if ( mod(p_req_locn, cmt_frq) = 0 and lwh.stat_code in (10, 50) )
                                    then
                                        dbms_application_info.set_module ( 'L_C_W','EP...'|| p_req_locn);
                                        commit;
                                    end if;

                                    -- check for location existence if exists
                                    -- add one entry to message log
                                    -- otherwise get incr value form nxt up cntr
                                    -- to get location id
                                    insert_stat_code := 0;
                                    if lwh.stat_code = 10
                                    then
                                        begin
                                            v_locn_id := null;
                                            select /*+index(locn_hdr, locn_hdr_ind_15) */ locn_id
                                            into v_locn_id
                                            from locn_hdr
                                            where nvl(locn_hdr.area,'$') = nvl(i.area,'$')
                                                and nvl(locn_hdr.zone,'$') = nvl(j.zone,'$')
                                                and nvl(locn_hdr.aisle,'$') = nvl(k.aisle,'$')
                                                and nvl(locn_hdr.bay,'$') = nvl(l.bay,'$')
                                                and nvl(locn_hdr.lvl,'$') = nvl(m.lvl,'$')
                                                and nvl(locn_hdr.posn,'$') = nvl(n.posn,'$')
                                                --and locn_hdr.locn_class = lwh.locn_class
                                                and locn_hdr.whse = lwh.whse;

                                            insert_stat_code := 0;

                                            sql_errm := substr('warning :location already exist for :
                                                whse/area/zone/aisle/bay/lvl/posn(insert mode) -'||lwh.whse||'/'||i.area||'/'
                                                ||j.zone||'/'||k.aisle||'/'||l.bay||'/'||m.lvl||'/'||n.posn|| chr(13)
                                                ||sqlerrm, 1, 512);

                                            insert into msg_log(msg_log_id, module, msg_id, pgm_id, msg, create_date_time, mod_date_time,
                                                user_id, whse, cd_master_id, log_date_time, ref_code_1, ref_value_1, ref_code_5, ref_value_5)
                                            values(msg_log_id_seq.nextval,'INVMGMT','2039', 'L-C-W_CRT_MNTN_LOC', substr(sql_errm,1,500),
                                                sysdate,sysdate,p_user, p_whse, p_tc_company_id,sysdate,'96',lwh.stat_code,'95',p_locn_parm_id);
                                        exception
                                            when no_data_found then
                                            begin
                                                p_whse := '*';
                                                select incr_value+incr_cntr
                                                into v_incr_value
                                                from nxt_up_cnt
                                                where whse = '*'  --lwh.whse ask
                                                    and rec_type_id = '002';

                                                if v_incr_value = 0
                                                then
                                                    v_incr_value := 1;
                                                end if;

                                                -- get next location id
                                                 manh_get_nxt_up_cnt_id(null,null,'002',30,NULL,v_incr_value,v_whse,v_rec_type_id,
						            v_pfx_field,v_pfx_len,v_start_nbr,v_end_nbr,v_locn_id_tmp,
						            v_nbr_len,v_incr_value,v_nxt_start_nbr,v_nxt_end_nbr,
						            v_chk_digit_type,v_chk_digit_len,v_repeat_range,
                                                            v_cd_master_id,v_nxt_up_cnt_id, v_pfx_type, v_rev_order, v_Rc);
                                                v_locn_id := lpad(v_locn_id_tmp + 1, 9, '0'); -- lwh.whse ask
                                                insert_stat_code := 10;
                                            exception
                                                when no_data_found then
                                                    p_whse := lwh.whse;

                                                    begin
                                                        select incr_value+incr_cntr
                                                        into v_incr_value
                                                        from nxt_up_cnt
                                                        where whse = lwh.whse -- ask
                                                            and rec_type_id = '002';

                                                        if v_incr_value = 0
                                                        then
                                                            v_incr_value := 1;
                                                        end if;

                                                        -- get next location id
                                                        -- lwh.whse ask
                                                        manh_get_nxt_up_cnt_id(null,null,'002',30,NULL,v_incr_value,v_whse,v_rec_type_id,
							v_pfx_field,v_pfx_len,v_start_nbr,v_end_nbr,v_locn_id_tmp,
							v_nbr_len,v_incr_value,v_nxt_start_nbr,v_nxt_end_nbr,
							v_chk_digit_type,v_chk_digit_len,v_repeat_range,
                                                        v_cd_master_id,v_nxt_up_cnt_id, v_pfx_type, v_rev_order, v_Rc);
                                                        v_locn_id := lpad(v_locn_id_tmp + 1, 9, '0'); -- lwh.whse ask

                                                        insert_stat_code := 10;
                                                    exception
                                                        when others then
                                                            rollback;
                                                            p_sucs := 0;
                                                            raise_application_error(-20951,
                                                                'error in retrieving nxt_up_cnt.incr_value for whse '|| lwh.whse
                                                                ||' / rec_type_id - 002 ' || chr(13) || sqlerrm);
                                                    end;
                                                when others then
                                                    rollback;
                                                    p_sucs := 0;
                                                    raise_application_error(-20952,
                                                        'error in retrieving nxt_up_cnt.incr_value for whse - * / rec_type_id - 002 '
                                                        || chr(13) || sqlerrm);
                                            end;
                                            when others then
                                                rollback;
                                                p_sucs := 0;
                                                raise_application_error(-20971,
                                                    'error in retrieving loc_hdr.loc_id for whse/locn_class/area/zone/aisle/bay/lvl/posn(insert mode) -'
                                                    ||lwh.whse||'/'||lwh.locn_class||'/'||i.area||'/'||j.zone||'/'||k.aisle||'/'||l.bay||'/'||m.lvl||'/'
                                                    ||n.posn|| chr(13) || sqlerrm);
                                        end;
                                        -- for location maintenance
                                        -- get location id from locn_hdr
                                    elsif lwh.stat_code in (50,82,83,85)
                                    then
                                        begin
                                            select /*+index(locn_hdr, locn_hdr_ind_15) */ locn_id
                                            into v_locn_id
                                            from locn_hdr
                                            where nvl(locn_hdr.area,'$') = nvl(i.area,'$')
                                                and nvl(locn_hdr.zone,'$') = nvl(j.zone,'$')
                                                and nvl(locn_hdr.aisle,'$') = nvl(k.aisle,'$')
                                                and nvl(locn_hdr.bay,'$') = nvl(l.bay,'$')
                                                and nvl(locn_hdr.lvl,'$') = nvl(m.lvl,'$')
                                                and nvl(locn_hdr.posn,'$') = nvl(n.posn,'$')
                                                --and locn_hdr.locn_class =
                                                --case lwh.stat_code when 82 then 'A' when 83 then 'R' else lwh.locn_class end
                                                and ( (locn_hdr.locn_class='R' and lwh.stat_code = 82 )
                                                        or (locn_hdr.locn_class in ('A','C') and lwh.stat_code in (83,85) )
                                                        or ( locn_hdr.locn_class = lwh.locn_class and lwh.stat_code not in (82,83,85))
                                                    )
                                                and locn_hdr.whse = lwh.whse;

                                            insert_stat_code := 33;
                                        exception
                                            when no_data_found then
                                                insert_stat_code := 66;

                                                sql_errm := substr('warning :failure in maintaining record:no existing recoreds for
                                                    whse/locn_class/area/zone/aisle/bay/lvl/posn(update mode) -'||lwh.whse||'/'
                                                    ||lwh.locn_class||'/'||i.area||'/'||j.zone||'/'||k.aisle||'/'||l.bay||'/'
                                                    ||m.lvl||'/'||n.posn|| chr(13)||sqlerrm, 1, 512);
                                                insert into msg_log(msg_log_id, module, msg_id, pgm_id, msg, create_date_time, mod_date_time,
                                                    user_id, whse, cd_master_id,log_date_time,ref_code_1, ref_value_1,
                                                    ref_code_5, ref_value_5)
                                                values(msg_log_id_seq.nextval, 'INVMGMT','2039', 'L-C-W_CRT_MNTN_LOC', substr(sql_errm,1,500),
                                                    sysdate,sysdate,p_user, p_whse, p_tc_company_id,sysdate,'96', lwh.stat_code,
                                                    '95', p_locn_parm_id );
                                            when others then
                                                rollback;
                                                p_sucs := 0;
                                                raise_application_error(-20953,
                                                'error in retrieving loc_hdr.loc_id for whse/locn_class/area/zone/aisle/bay/lvl/posn(update mode) -'
                                                ||lwh.whse||'/'||lwh.locn_class||'/'||i.area||'/'||j.zone||'/'||k.aisle||'/'||l.bay||'/'||m.lvl||'/'
                                                ||n.posn|| chr(13) || sqlerrm);
                                        end;
                                    end if; -- end stat_code if

                                    if lwh.stat_code in (10,50)
                                    then
                                        v_check_digit := gnrt_location_ckdgt(lwh.whse);
                                        if v_use_locn_id_as_brcd = 1
                                        then
                                            v_locn_brcd := v_locn_id;
                                        else
                                            v_locn_brcd := substr(i.area,1,length(i.area)-nvl(v_area_offset,0))||
                                                substr(j.zone,1,length(j.zone)-nvl(v_zone_offset,0))||
                                                substr(k.aisle,1,length(k.aisle)-nvl(v_aisle_offset,0))||
                                                substr(l.bay,1,length(l.bay)-nvl(v_bay_offset,0))||
                                                substr(m.lvl,1,length(m.lvl)-nvl(v_lvl_offset,0))||
                                                substr(n.posn,1,length(n.posn)-nvl(v_posn_offset,0));
                                        end if; -- end v_use_locn_id_as_brcd if

                                        v_locn_pick_seq := rpad(nvl(i.area,' '),4,' ')||
                                            rpad(nvl(j.zone,' '),4,' ')||
                                            rpad(nvl(k.aisle,' '),4,' ')||
                                            rpad(nvl(l.bay,' '),4,' ')||
                                            rpad(nvl(m.lvl,' '),4,' ')||
                                            rpad(nvl(n.posn,' '),4,' ');

                                        if msg_locn_brcd_1 is null
                                        then
                                            msg_locn_brcd_1 := v_locn_brcd;
                                        end if;
                                    end if;

                                    if lwh.stat_code = 10  and insert_stat_code = 10
                                    then
                                        insert into locn_hdr
                                          (locn_id, whse, locn_class, locn_brcd, area, zone, aisle, bay, lvl, posn,
                                           locn_pick_seq, sku_dedctn_type, slot_type, putwy_zone, pull_zone, pick_detrm_zone,
                                           len, width, ht, x_coord, y_coord, z_coord, work_grp, work_area,last_frozn_date_time,
                                           last_cnt_date_time, cycle_cnt_pending, prt_label_flag, travel_aisle, travel_zone,
                                           storage_uom, pick_uom, slot_unusable, create_date_time, mod_date_time, user_id, check_digit)
                                        values (v_locn_id, lwh.whse, lwh.locn_class, v_locn_brcd, i.area, j.zone,
                                           k.aisle,l.bay, m.lvl, n.posn, v_locn_pick_seq, v_sku_dedctn_type, v_slot_type,
                                           v_putwy_zone, v_pull_zone, v_pick_detrm_zone, v_len, v_width, v_ht, v_x_coord,
                                           v_y_coord, v_z_coord, v_work_grp, v_work_area, v_last_frozn_date_time, v_last_cnt_date_time,
                                           v_cycle_cnt_pending, v_prt_label_flag, v_travel_aisle, v_travel_zone, v_storage_uom, v_pick_uom,
                                           v_slot_unusable, v_create_date_time, v_mod_date_time, v_user_id, v_check_digit);

                                        p_crt_upd_locn :=  p_crt_upd_locn + 1;
                                    elsif (lwh.stat_code = 50 and insert_stat_code = 33)
                                    then
                                        update locn_hdr
                                        set sku_dedctn_type    = replace(v_sku_dedctn_type,'-',sku_dedctn_type),
                                            slot_type          = replace(v_slot_type,'-9',slot_type),
                                            putwy_zone         = replace(v_putwy_zone,'-9',putwy_zone),
                                            pull_zone          = replace(v_pull_zone,'-9',pull_zone),
                                            pick_detrm_zone    = replace(v_pick_detrm_zone,'-9',pick_detrm_zone),
                                            len                = nvl(replace(v_len,'-9',len),0),
                                            width              = nvl(replace(v_width,'-9',width),0),
                                            ht                 = nvl(replace(v_ht,'-9',ht),0),
                                            x_coord            = nvl(replace(v_x_coord,-9,x_coord),0),
                                            y_coord            = nvl(replace(v_y_coord,-9,y_coord),0),
                                            z_coord            = nvl(replace(v_z_coord,-9,z_coord),0),
                                            work_grp           = replace(v_work_grp,'-9',work_grp),
                                            work_area          = replace(v_work_area,'-9',work_area),
                                            travel_aisle       = replace(v_travel_aisle,'-9',travel_aisle),
                                            travel_zone        = replace(v_travel_zone,'-9',travel_zone),
                                            --check_digit        = v_check_digit,
                                            mod_date_time      = v_mod_date_time,
                                            user_id            = v_user_id
                                        where locn_id = v_locn_id;
                                        p_crt_upd_locn :=  p_crt_upd_locn + 1;
                                    elsif (lwh.stat_code in (82,83,85) and insert_stat_code = 33 and v_process)
                                    then
                                        update locn_hdr
                                        set sku_dedctn_type    = NVL(replace(v_sku_dedctn_type,'-',sku_dedctn_type),sku_dedctn_type),
                                            slot_type          = NVL(replace(v_slot_type,'-9',slot_type),slot_type),
                                            putwy_zone         = NVL(replace(v_putwy_zone,'-9',putwy_zone),putwy_zone),
                                            pull_zone          = NVL(replace(v_pull_zone,'-9',pull_zone),pull_zone),
                                            pick_detrm_zone    = NVL(replace(v_pick_detrm_zone,'-9',pick_detrm_zone),pick_detrm_zone),
                                            len                    = nvl(replace(v_len,-9,len),len),
                                            width                = nvl(replace(v_width,-9,width),width),
                                            ht                     = nvl(replace(v_ht,-9,ht),ht),
                                            x_coord            = nvl(replace(v_x_coord,-9,x_coord),x_coord),
                                            y_coord            = nvl(replace(v_y_coord,-9,y_coord),y_coord),
                                            z_coord            = nvl(replace(v_z_coord,-9,z_coord),z_coord),
                                            work_grp           = nvl(replace(v_work_grp,'-9',work_grp),work_grp),
                                            work_area          = NVL(replace(v_work_area,'-9',work_area),work_area),
                                            prt_label_flag     = v_prt_label_flag,
                                            travel_aisle       = NVL(replace(v_travel_aisle,'-9',travel_aisle),travel_aisle),
                                            travel_zone        = nvl(replace(v_travel_zone,'-9',travel_zone),travel_zone),                                            
                                            mod_date_time      = v_mod_date_time,
                                            user_id            = v_user_id
                                        where locn_id = v_locn_id;
                                        -- Added another condition to move the coordinates from locn_wiz_dtl to locn_hdr during manual updates from UI
                                        if (x_coord_chk = 'N') then
                                            update locn_hdr
                                            set x_coord            = (select nvl(min(value),x_coord) from locn_wiz_dtl where locn_parm_id = p_locn_parm_id and locn_seq_nbr = lwh.locn_seq_nbr and
                                                                      locn_prop_type = ( select trim(max(misc_flags)) from sys_code where rec_type = 'S' and code_type = '313' and code_id = '01' ) and 
                                                                      coalesce(locn_pro_attr_type,'X') = coalesce( ( select trim(max(misc_flags)) from sys_code where rec_type = 'S' and code_type = '313' and code_id = '04' ), 'X' )),
                                                y_coord            = (select nvl(min(value),y_coord)  from locn_wiz_dtl where locn_parm_id = p_locn_parm_id and locn_seq_nbr = lwh.locn_seq_nbr and
                                                                      locn_prop_type = ( select trim(max(misc_flags)) from sys_code where rec_type = 'S' and code_type = '313' and code_id = '02' ) and 
                                                                      coalesce(locn_pro_attr_type,'X') = coalesce( ( select trim(max(misc_flags)) from sys_code where rec_type = 'S' and code_type = '313' and code_id = '04' ), 'X' )),
                                                z_coord            = (select nvl(min(value),z_coord)  from locn_wiz_dtl where locn_parm_id = p_locn_parm_id and locn_seq_nbr = lwh.locn_seq_nbr and
                                                                      locn_prop_type = ( select trim(max(misc_flags)) from sys_code where rec_type = 'S' and code_type = '313' and code_id = '03' ) and 
                                                                      coalesce(locn_pro_attr_type,'X') = coalesce( ( select trim(max(misc_flags)) from sys_code where rec_type = 'S' and code_type = '313' and code_id = '04' ), 'X' ))
                                            where locn_id = v_locn_id;
                                        p_crt_upd_locn :=  p_crt_upd_locn + 1;
										end if;
                                    end if;  -- stat_code if

                                    if lwh.locn_class in ('R','S','P','Y','0')
                                    then
                                        -- insert / update resv_locn_hdr also
                                        if (lwh.stat_code = 10  and insert_stat_code = 10)
                                        then
                                            insert into resv_locn_hdr
                                                (locn_id, locn_size_type, dedctn_item_id, locn_putaway_lock, invn_lock_code, curr_wt,
                                                dirct_wt, max_wt, curr_vol, dirct_vol, max_vol, curr_uom_qty, dirct_uom_qty,
                                                max_uom_qty, create_date_time, mod_date_time, user_id, pack_zone, sort_locn_flag,INBD_STAGING_FLAG,
                                                resv_locn_hdr_id)
                                            values (v_locn_id, v_locn_size_type, v_dedctn_item_id, v_locn_putaway_lock,
                                                v_invn_lock_code, v_curr_wt, v_dirct_wt_rlh, v_max_wt_rlh, v_curr_vol_rlh,
                                                v_dirct_vol_rlh, v_max_vol_rlh, v_curr_uom_qty_rlh, v_dirct_uom_qty_rlh,
                                                v_max_uom_qty_rlh, v_create_date_time, v_mod_date_time, v_user_id, v_pack_zone_rlh,
                                                v_sort_locn_flag,v_stag_locn_flag, resv_locn_hdr_id_seq.nextval);
                                        elsif lwh.stat_code = 50 and insert_stat_code = 33
                                        then
										   if  v_max_wt  = 999999999.9999  then v_max_wt := NULL; end if;
                                           if  v_max_vol = 999999999.9999  then v_max_vol := NULL; end if;
                                            update resv_locn_hdr
                                            set locn_size_type = replace(v_locn_size_type, '-9', locn_size_type),
                                                max_wt = nvl(replace(v_max_wt_rlh, '-9', max_wt), max_wt),
                                                max_vol = nvl(replace(v_max_vol_rlh, '-9', max_vol), max_vol),
                                                max_uom_qty = nvl(replace(v_max_uom_qty_rlh, '-9', max_uom_qty), max_uom_qty),
                                                pack_zone = replace(v_pack_zone_rlh,'-9',pack_zone),
                                                sort_locn_flag = nvl(replace(v_sort_locn_flag,'-9',sort_locn_flag),0),
                                                INBD_STAGING_FLAG =nvl(replace(v_stag_locn_flag,'-9',INBD_STAGING_FLAG),INBD_STAGING_FLAG),
                                                mod_date_time = v_mod_date_time,
                                                user_id = v_user_id
                                            where locn_id = v_locn_id;
                                        end if;  -- stat_code if
                                    end if;  -- locn_clss if

                                    if lwh.locn_class in ('A','C','1','2')
                                    then
                                        -- insert / update pick_locn_hdr also
                                        -- ## reqd for both insert and update of pick_locn_hdr
                                        if v_use_locn_id_as_brcd = 1
                                        then
                                            -- get incr value for nxt up cntr
                                            begin
                                                p_whse := '*';

                                                select incr_value+repl_incr_cntr
                                                into v_incr_value
                                                from nxt_up_cnt
                                                where whse = '*' --lwh.whse ask
                                                    and rec_type_id = '104';
                                            exception
                                                when no_data_found then
                                                    p_whse := lwh.whse;

                                                    begin
                                                        select incr_value + repl_incr_cntr
                                                        into v_incr_value
                                                        from nxt_up_cnt
                                                        where whse = lwh.whse -- ask
                                                            and rec_type_id = '104';
                                                    exception
                                                        when others then
                                                            rollback;
                                                            p_sucs := 0;
                                                            raise_application_error(-20958,
                                                                'error in retrieving nxt_up_cnt.incr_value for whse '|| lwh.whse
                                                                ||' / rec_type_id - 104 ' || chr(13) || sqlerrm);
                                                    end;
                                                when others then
                                                    rollback;
                                                    p_sucs := 0;
                                                    raise_application_error(-20959,
                                                        'error in retrieving nxt_up_cnt.incr_value for whse - * / rec_type_id - 104 '
                                                        || chr(13) || sqlerrm);
                                            end;

                                            -- get next repl locn brcd
                                            manh_get_nxt_up_cnt_id(null,null,'104',30,NULL,v_incr_value,v_whse,v_rec_type_id,
					    v_pfx_field,v_pfx_len,v_start_nbr,v_end_nbr,v_repl_locn_brcd,
					    v_nbr_len,v_incr_value,v_nxt_start_nbr,v_nxt_end_nbr,
					    v_chk_digit_type,v_chk_digit_len,v_repeat_range,
                                            v_cd_master_id,v_nxt_up_cnt_id, v_pfx_type, v_rev_order, v_Rc);

                                            v_repl_locn_brcd := v_repl_locn_brcd +1;
                                            --v_check_digit := gnrt_location_ckdgt(lwh.whse);
                                        else
                                            v_repl_locn_brcd := substr(i.area,1,length(i.area)-nvl(v_area_offset,0))||
                                                substr(j.zone,1,length(j.zone)-nvl(v_zone_offset,0))||
                                                substr(k.aisle,1,length(k.aisle)-nvl(v_aisle_offset,0))||
                                                substr(l.bay,1,length(l.bay)-nvl(v_bay_offset,0))||
                                                substr(m.lvl,1,length(m.lvl)-nvl(v_lvl_offset,0))||
                                                substr(n.posn,1,length(n.posn)-nvl(v_posn_offset,0));
                                        end if;   -- end of use locn as brcd if

                                        if (lwh.stat_code = 10 and insert_stat_code = 10)/* or (lwh.stat_code = 83 and v_process) */
                                        then
                                            if lwh.stat_code = 83
                                            then
                                                select locn_id
                                                into v_locn_id
                                                from locn_wizard_locn_tmp tl
                                                where nvl(tl.area,'$') = nvl(i.area,'$')
                                                    and nvl(tl.zone,'$') = nvl(j.zone,'$')
                                                    and nvl(tl.aisle,'$') = nvl(k.aisle,'$')
                                                    and nvl(tl.bay,'$') = nvl(l.bay,'$')
                                                    and nvl(tl.lvl,'$') = nvl(m.lvl,'$')
                                                    and nvl(tl.posn,'$') = nvl(n.posn,'$')
                                                    and tl.whse = lwh.whse;

                                                --v_check_digit := gnrt_location_ckdgt(lwh.whse);
                                            end if;

                                            insert into pick_locn_hdr
                                                (pick_locn_hdr_id, locn_id,repl_locn_brcd,repl_check_digit,repl_x_coord, repl_y_coord,
                                                repl_z_coord, repl_travel_aisle, repl_travel_zone,putwy_type,max_nbr_of_sku,
                                                repl_flag, pick_locn_assign_zone, create_date_time, mod_date_time,
                                                pick_to_light_flag,user_id, pick_locn_assign_type, suppr_pr40_repl,
                                                comb_4050_repl, xcess_wave_need_proc_type, max_vol, max_wt)
                                            values (pick_locn_hdr_id_seq.nextval, v_locn_id, v_repl_locn_brcd, v_check_digit, nvl(v_repl_x_coord,0),
                                                nvl(v_repl_y_coord,0), nvl(v_repl_z_coord,0), v_repl_travel_aisle, v_repl_travel_zone,
                                                v_putwy_type, v_max_nbr_of_sku, v_repl_flag, v_pick_locn_assign_zone, v_create_date_time,
                                                v_mod_date_time, v_pick_to_light_flg, v_user_id, v_pick_locn_assign_type,
                                                v_suppr_pr40_repl, v_comb_4050_repl, v_xcess_wave_need_proc_type, v_max_vol, v_max_wt);
                                                
                                        elsif (lwh.stat_code = 50 and insert_stat_code = 33)  OR (lwh.stat_code = 85 and v_process) 
                                        then
										  if  v_repl_x_coord  = 0 then v_repl_x_coord:= NULL; end if;
                                          if  v_repl_y_coord  = 0 then v_repl_y_coord:= NULL; end if;
                                          if  v_repl_z_coord  = 0 then v_repl_z_coord:= NULL; end if;
                                          if  v_max_wt  = 999999999.9999  then v_max_wt := NULL; end if;
                                          if   v_max_vol = 999999999.9999  then v_max_vol := NULL; end if;

                                            update pick_locn_hdr
                                            set --repl_check_digit      = v_check_digit,
                                                repl_x_coord          = nvl(replace(v_repl_x_coord,-9,repl_x_coord),repl_x_coord),
                                                repl_y_coord          = nvl(replace(v_repl_y_coord,-9,repl_y_coord),repl_y_coord),
                                                repl_z_coord          = nvl(replace(v_repl_z_coord,-9,repl_z_coord),repl_z_coord),
                                                repl_travel_aisle     = replace(v_repl_travel_aisle,'-9',repl_travel_aisle),
                                                repl_travel_zone      = replace(v_repl_travel_zone,'-9',repl_travel_zone),
                                                putwy_type            = replace(v_putwy_type,'-9',putwy_type),
                                                max_nbr_of_sku        = nvl(replace(v_max_nbr_of_sku,'-9',max_nbr_of_sku),0),
                                                repl_flag             = replace(v_repl_flag,'-',repl_flag),
                                                pick_locn_assign_zone = replace(v_pick_locn_assign_zone,'-9',pick_locn_assign_zone),
                                                pick_to_light_flag    = replace(v_pick_to_light_flg,'-',pick_to_light_flag),
                                                pick_locn_assign_type = replace(v_pick_locn_assign_type,'-9',pick_locn_assign_type),
                                                suppr_pr40_repl       = replace(v_suppr_pr40_repl,'-9',suppr_pr40_repl),
                                                comb_4050_repl        = replace(v_comb_4050_repl,'-9',comb_4050_repl),
                                                xcess_wave_need_proc_type = nvl(replace(v_xcess_wave_need_proc_type,
                                                                                '-9', xcess_wave_need_proc_type),xcess_wave_need_proc_type),
                                                max_vol               = nvl(replace(v_max_vol,'-9',max_vol),max_vol),
                                                max_wt                = nvl(replace(v_max_wt,'-9',max_wt),max_wt),
                                                mod_date_time         = v_mod_date_time,
                                                user_id               = v_user_id
                                            where locn_id = v_locn_id;
                                        end if; -- stat code if
                                    end if; -- locn_clss if

                                    if lwh.locn_class = 'O'
                                    then
                                        -- insert/ update pkt_consol_locn
                                       if lwh.stat_code = 10  and insert_stat_code = 10
                                       then
                                           insert into pkt_consol_locn
                                                (rec_type, locn_id, wave_nbr,prty_seq_nbr, locn_stat_code, max_nbr_of_pkts,
                                                curr_nbr_of_pkts, max_nbr_of_swc, curr_nbr_of_swc, curr_wt, max_wt,
                                                curr_vol, max_vol, capcty_uom, max_uom_qty, curr_uom_qty, dirct_wt, dirct_vol,
                                                dirct_uom_qty, dirct_nbr_of_swc, dirct_pkts, lock_pkt_consol_colm_1, lock_pkt_consol_colm_2,
                                                lock_pkt_consol_colm_3, lock_pkt_consol_colm_4, lock_pkt_consol_colm_5,
                                                lock_pkt_consol_colm_6, lock_pkt_consol_colm_7, lock_pkt_consol_colm_8,
                                                lock_pkt_consol_colm_9, lock_pkt_consol_colm_10, pkt_consol_value_1,
                                                pkt_consol_value_2, pkt_consol_value_3, pkt_consol_value_4, pkt_consol_value_5,
                                                pkt_consol_value_6, pkt_consol_value_7, pkt_consol_value_8, pkt_consol_value_9,
                                                pkt_consol_value_10, dock_door_brcd, pkt_consol_attr, create_date_time, mod_date_time,
                                                user_id)
                                           values (v_rec_type, v_locn_id, v_wave_nbr, v_prty_seq_nbr, v_locn_stat_code, v_max_nbr_of_pkts,
                                                v_curr_nbr_of_pkts, v_max_nbr_of_swc, v_curr_nbr_of_swc, v_curr_wt_pcl, v_max_wt_pcl,
                                                v_curr_vol, v_max_vol_pcl,v_capcty_uom, v_max_uom_qty, v_curr_uom_qty, v_dirct_wt,
                                                v_dirct_vol, v_dirct_uom_qty,v_dirct_nbr_of_swc, v_dirct_pkts, v_lock_pkt_consol_colm_1,
                                                v_lock_pkt_consol_colm_2, v_lock_pkt_consol_colm_3, v_lock_pkt_consol_colm_4,
                                                v_lock_pkt_consol_colm_5, v_lock_pkt_consol_colm_6, v_lock_pkt_consol_colm_7,
                                                v_lock_pkt_consol_colm_8, v_lock_pkt_consol_colm_9, v_lock_pkt_consol_colm_10,
                                                v_pkt_consol_value_1, v_pkt_consol_value_2, v_pkt_consol_value_3, v_pkt_consol_value_4,
                                                v_pkt_consol_value_5, v_pkt_consol_value_6, v_pkt_consol_value_7, v_pkt_consol_value_8,
                                                v_pkt_consol_value_9, v_pkt_consol_value_10, v_dock_door_brcd, v_pkt_consol_attr,
                                                v_create_date_time, v_mod_date_time, v_user_id);
                                       elsif lwh.stat_code = 50 and insert_stat_code = 33
                                       then
                                           update pkt_consol_locn
                                           set rec_type = nvl(replace(v_rec_type,'-',rec_type),'P'),
                                               prty_seq_nbr = nvl(replace(v_prty_seq_nbr,'-9',prty_seq_nbr),100),
                                               locn_stat_code = nvl(replace(v_locn_stat_code,'-9',locn_stat_code),'00'),
                                               max_nbr_of_pkts = nvl(replace(v_max_nbr_of_pkts,'-9',max_nbr_of_pkts),0),
                                               max_nbr_of_swc = nvl(replace(v_max_nbr_of_swc,'-9',max_nbr_of_swc),0),
                                               max_wt = nvl(replace(v_max_wt_pcl,'-9',max_wt),0),
                                               max_vol = nvl(replace(v_max_vol_pcl,'-9',max_vol),0),
                                               capcty_uom = replace(v_capcty_uom,'-',capcty_uom),
                                               max_uom_qty = nvl(replace(v_max_uom_qty,'-9',max_uom_qty),0),
                                               lock_pkt_consol_colm_1 = replace(v_lock_pkt_consol_colm_1,'-',lock_pkt_consol_colm_1),
                                               lock_pkt_consol_colm_2 = replace(v_lock_pkt_consol_colm_2,'-',lock_pkt_consol_colm_2),
                                               lock_pkt_consol_colm_3 = replace(v_lock_pkt_consol_colm_3,'-',lock_pkt_consol_colm_3),
                                               lock_pkt_consol_colm_4 = replace(v_lock_pkt_consol_colm_4,'-',lock_pkt_consol_colm_4),
                                               lock_pkt_consol_colm_5 = replace(v_lock_pkt_consol_colm_5,'-',lock_pkt_consol_colm_5),
                                               lock_pkt_consol_colm_6 = replace(v_lock_pkt_consol_colm_6,'-',lock_pkt_consol_colm_6),
                                               lock_pkt_consol_colm_7 = replace(v_lock_pkt_consol_colm_7,'-',lock_pkt_consol_colm_7),
                                               lock_pkt_consol_colm_8 = replace(v_lock_pkt_consol_colm_8,'-',lock_pkt_consol_colm_8),
                                               lock_pkt_consol_colm_9 = replace(v_lock_pkt_consol_colm_9,'-',lock_pkt_consol_colm_9),
                                               lock_pkt_consol_colm_10 = replace(v_lock_pkt_consol_colm_10,'-',lock_pkt_consol_colm_10),
                                               pkt_consol_value_1 = replace(v_pkt_consol_value_1,'-9',pkt_consol_value_1),
                                               pkt_consol_value_2 = replace(v_pkt_consol_value_2,'-9',pkt_consol_value_2),
                                               pkt_consol_value_3 = replace(v_pkt_consol_value_3,'-9',pkt_consol_value_3),
                                               pkt_consol_value_4 = replace(v_pkt_consol_value_4,'-9',pkt_consol_value_4),
                                               pkt_consol_value_5 = replace(v_pkt_consol_value_5,'-9',pkt_consol_value_5),
                                               pkt_consol_value_6 = replace(v_pkt_consol_value_6,'-9',pkt_consol_value_6),
                                               pkt_consol_value_7 = replace(v_pkt_consol_value_7,'-9',pkt_consol_value_7),
                                               pkt_consol_value_8 = replace(v_pkt_consol_value_8,'-9',pkt_consol_value_8),
                                               pkt_consol_value_9 = replace(v_pkt_consol_value_8,'-9',pkt_consol_value_9),
                                               pkt_consol_value_10 = replace(v_pkt_consol_value_10,'-9',pkt_consol_value_10),
                                               dock_door_brcd = replace(v_dock_door_brcd,'-9',dock_door_brcd),
                                               mod_date_time = v_mod_date_time,
                                               user_id = v_user_id
                                           where locn_id = v_locn_id;
                                       end if; -- stat_code  if
                                   end if; -- locn_clss if

                                   if lwh.locn_class = 'J'
                                   then
                                       -- insert/ update store_pack_locn_hdr
                                       if lwh.stat_code = 10  and insert_stat_code = 10
                                       then
                                           insert into  store_pack_locn_hdr
                                               (locn_id, store_nbr, pack_zone, assign_zone, max_vendors, max_wt, max_vol, max_items, max_cost,
                                                create_date_time, mod_date_time, user_id)
                                           values (v_locn_id, v_store_nbr, v_pack_zone, v_assign_zone, v_max_vendors, v_max_wt,
                                               v_max_vol, v_max_items, v_max_cost, v_create_date_time, v_mod_date_time, v_user_id);
                                       elsif lwh.stat_code = 50 and insert_stat_code = 33
                                       then
                                           update store_pack_locn_hdr
                                               set pack_zone = replace(v_pack_zone,'-9',pack_zone),
                                                   max_vendors = nvl(replace(v_max_vendors,'-9',max_vendors),0),
                                                   max_wt = nvl(replace(v_max_wt,'-9',max_wt),0),
                                                   max_vol = nvl(replace(v_max_vol,'-9',max_vol),0),
                                                   max_items = nvl(replace(v_max_items,'-9',max_items),0),
                                                   max_cost = nvl(replace(v_max_cost,'-9',max_cost),0),
                                                   mod_date_time = v_mod_date_time,
                                                   user_id = v_user_id
                                           where locn_id = v_locn_id;
                                       end if;  -- stat_code
                                   end if;  -- locn_clss if

                                   if v_grp_type is not null
                                   then
                                       if lwh.stat_code = 10  and insert_stat_code = 10
                                       then
                                           insert into locn_grp (grp_type, locn_id, grp_attr, create_date_time, mod_date_time, user_id)
                                           values (v_grp_type, v_locn_id, v_grp_attr, v_create_date_time, v_mod_date_time, v_user_id);
                                       elsif (lwh.stat_code = 50 and insert_stat_code = 33) or (lwh.stat_code = 83)
                                       then
                                           update locn_grp
                                               set grp_type = replace(v_grp_type,'-9',grp_type),
                                                   grp_attr = replace(v_grp_attr,'-9',grp_attr),
                                                   mod_date_time = v_mod_date_time,
                                                   user_id = v_user_id
                                           where locn_id = v_locn_id
                                               and grp_type = v_grp_type;

                                           if sql%rowcount = 0
                                           then
                                                insert into locn_grp
                                                (
                                                    grp_type, locn_id, grp_attr, create_date_time, mod_date_time, user_id
                                                )
                                                values
                                                (
                                                    v_grp_type, v_locn_id, v_grp_attr, v_create_date_time, v_mod_date_time, v_user_id
                                                );
                                           end if;
                                       end if;  -- stat_code
                                   elsif v_grp_type is null and lwh.stat_code in (50,83)
                                   then
                                       delete locn_grp
                                       where locn_id = v_locn_id;
                                   end if;  -- grp typ
                               end loop;  -- posn
                           end loop;  -- lvl
                       end loop;  -- bay
                   end loop;  -- aisle
               end loop;  -- zone
           end loop;  -- area
       end if;

        if lwh.stat_code in (10,50)
        then
            insert into msg_log(msg_log_id, module, msg_id, pgm_id, msg,
                create_date_time, mod_date_time, user_id, whse, cd_master_id,log_date_time,
                ref_code_1,ref_value_2,ref_code_5,ref_value_5)
            values(msg_log_id_seq.nextval, 'INVMGMT','2039', 'L-C-W_CRT_MNTN_LOC',
                'info : execution end time : '
                ||to_char(sysdate,'dd/mm/yy : hh24:mi:ss')
                ||'number of locations created/updated/deleted = '
                ||p_crt_upd_locn||' , number of locations skipped = '
                ||(p_req_locn-p_crt_upd_locn)||' for range '
                ||msg_locn_brcd_1||' to '||v_locn_brcd, sysdate, sysdate,
                p_user, p_whse, p_tc_company_id,sysdate,
                '96',lwh.stat_code,'95',p_locn_parm_id);
        else
            v_msg := get_msg_info('INVMGMT', '1691', v_lang_id);
            insert into msg_log(msg_log_id, module, msg_id, pgm_id, msg,
                create_date_time, mod_date_time, user_id, whse, cd_master_id,log_date_time,
                ref_code_1,ref_value_2,ref_code_5,ref_value_5)
            values(msg_log_id_seq.nextval, 'INVMGMT','1691', 'L-C-W_CRT_MNTN_LOC',
                v_msg||': number of locations created/updated/deleted = '
                ||p_crt_upd_locn||' , number of locations skipped = '
                ||(p_req_locn-p_crt_upd_locn)||' for range '
                ||msg_locn_brcd_1||' to '||v_locn_brcd, sysdate, sysdate,
                p_user, p_whse, p_tc_company_id,sysdate,'97',p_crt_upd_locn,
                '95',p_locn_parm_id);
        end if;

        dbms_output.put_line(' number of locations created/updated = '
            ||p_crt_upd_locn||' , number of locations skipped = '
            ||(p_req_locn-p_crt_upd_locn)||' for range '
            ||msg_locn_brcd_1||' to '||v_locn_brcd);

        --delete locn_wiz_dtl
        --where locn_parm_id = p_locn_parm_id
        --    and locn_seq_nbr = lwh.locn_seq_nbr;
        --delete locn_wiz_hdr
        --where locn_parm_id = p_locn_parm_id
        --    and locn_seq_nbr = lwh.locn_seq_nbr;

        delete from locn_wizard_locn_comp_tmp;

        delete from locn_wizard_locn_tmp;

        commit;
    -- check: we should not allow this lwh loop to continue on error
    end loop; -- lwh_cur for loop

    p_sucs := 1;
exception
    when others then
        rollback;

        p_sucs := 0;
        sql_errm := substr('execution term time : '
            ||to_char(sysdate,'dd/mm/yy : hh24 : mi : ss')||sqlerrm,1,500);

        insert into msg_log(msg_log_id, module, msg_id, pgm_id, msg,
            create_date_time, mod_date_time, user_id, whse, cd_master_id,log_date_time,
            ref_code_5,ref_value_5)
        values(msg_log_id_seq.nextval, 'INVMGMT','2039', 'L-C-W_CRT_MNTN_LOC',sql_errm,
            sysdate, sysdate, p_user, p_whse, p_tc_company_id,sysdate,'95',p_locn_parm_id);
        commit;
end;
/

show errors;

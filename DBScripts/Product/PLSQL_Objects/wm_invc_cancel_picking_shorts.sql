create or replace procedure wm_invc_cancel_picking_shorts
(
    p_user_id           in user_profile.user_id%type
)
as
begin
     -- collect the picking_short_item records for oli whose do_dtl_status = 150
     -- these lpn processed are not part of invoicing
     insert into tmp_pck_shrt_item
     (
         lpn_id, lpn_detail_id, short_qty
     )
     select ld.lpn_id, ld.lpn_detail_id, 
         sum(coalesce(psi.short_qty, 0)) short_qty
     from picking_short_item psi
     join lpn l on l.tc_lpn_id = psi.tc_lpn_id and l.tc_company_id = psi.tc_company_id
     join lpn_detail ld on ld.lpn_detail_id = psi.lpn_detail_id and ld.lpn_id = l.lpn_id
     join tmp_invc_orig_oli t on coalesce(t.reference_line_item_id, t.line_item_id) = psi.line_item_id   
     join order_line_item orig on orig.line_item_id = t.line_item_id
     where orig.do_dtl_status = 150 and psi.tc_lpn_id is not null
         and orig.user_canceled_qty > 0 and psi.stat_code < 90
         and not exists
         (
              select 1
              from tmp_invc_lpn_list t2
              where t2.lpn_id = ld.lpn_id
         )
      group by ld.lpn_id, ld.lpn_detail_id;
      wm_cs_log('picking short item records with lpn details collected for clean up: ' || sql%rowcount);
     
     merge into lpn_detail ld
     using
     (
         select t.lpn_detail_id, t.short_qty short_qty_per_dtl
         from tmp_pck_shrt_item t
     ) iv on (iv.lpn_detail_id = ld.lpn_detail_id)
     when matched then 
     update set 
         ld.initial_qty = 
             case 
                 when (coalesce(ld.initial_qty, 0) - iv.short_qty_per_dtl) > 0 then 
                     coalesce(ld.initial_qty, 0) - iv.short_qty_per_dtl
                 else 0
             end,
         ld.lpn_detail_status = 90,
         ld.last_updated_source = p_user_id, ld.last_updated_dttm = sysdate;
    wm_cs_log('lpn detail updated for picking_shorts of packed oli: ' || sql%rowcount);

    delete from wm_inventory wi
    where exists
    (
         select 1
         from lpn_detail ld
         join tmp_pck_shrt_item t on t.lpn_detail_id = ld.lpn_detail_id 
             and t.lpn_id = ld.lpn_id
         where ld.size_value = 0 and ld.initial_qty = 0
             and wi.lpn_id = t.lpn_id and wi.lpn_detail_id = t.lpn_detail_id
    );
    wm_cs_log('clean up the wm inventory for psi related lpn: '|| sql%rowcount);
  
    merge into lpn l
    using
    (
        select ld.lpn_id, 
            case
                when coalesce(min(ld.lpn_detail_status), 0) = 0 
                    and coalesce(max(ld.lpn_detail_status), 0) = 90 then 15
                when coalesce(min(ld.lpn_detail_status), 0) = 90 
                    and coalesce(sum(ld.size_value), 0) > 0 then 20
                else 99
            end lpn_facility_status
        from lpn_detail ld
        where exists
        (
            select 1
            from tmp_pck_shrt_item t
            where t.lpn_id = ld.lpn_id
        )
        group by ld.lpn_id
    ) iv on (iv.lpn_id = l.lpn_id)
    when matched then
    update set  
        l.lpn_facility_status = iv.lpn_facility_status,
        l.parent_lpn_id = decode(iv.lpn_facility_status, 99, null, l.parent_lpn_id),
        l.last_updated_source = p_user_id, l.last_updated_dttm = sysdate;
    wm_cs_log('psi related lpn updates: '|| sql%rowcount);
    
    -- cancel the picking_short_item records
    update picking_short_item psi
    set psi.stat_code = 99, 
        psi.last_updated_source = p_user_id, psi.last_updated_dttm = sysdate
    where psi.stat_code < 90 
        and exists
        (
            select 1
            from tmp_invc_orig_oli t
            join order_line_item oli on coalesce(t.reference_line_item_id, t.line_item_id) = oli.line_item_id
            where psi.line_item_id = oli.line_item_id
                and oli.do_dtl_status = 150 and oli.user_canceled_qty > 0
        );
    wm_cs_log('picking_short_item records cancelled: ' || sql%rowcount);
end;
/
show errors;

create or replace procedure manh_add_joins_for_lm_rule

(
    p_facility_id     in facility.facility_id%type,
    p_whse            in facility.whse%type,
    p_msg_insert_sql  in out varchar2
)
as
begin
    -- assumption: the UI will enforce access to only the relevant tables
-- todo: work out the design issue to distinguish between ilpns and olpns
    if instr(p_msg_insert_sql, 'ITEM_CBO', 1, 1) > 0
    then
        p_msg_insert_sql := replace(p_msg_insert_sql, 'where ',
            ' join item_cbo on item_cbo.item_id = t.item_id where ');
    end if;

    if instr(p_msg_insert_sql, 'ITEM_WMS', 1, 1) > 0
    then
        p_msg_insert_sql := replace(p_msg_insert_sql, 'where ',
            ' join item_wms on item_wms.item_id = t.item_id where ');
    end if;

    if (instr(p_msg_insert_sql, 'ITEM_FACILITY_MAPPING_WMS', 1, 1) > 0)
    then
        p_msg_insert_sql := replace(p_msg_insert_sql, 'where ',
            ' join item_facility_mapping_wms on '
            || ' item_facility_mapping_wms.item_id = t.item_id '
            || ' and item_facility_mapping_wms.facility_id = ' 
            || to_char(p_facility_id) || ' where ');
    end if;

-- check: need fk indexes from t to the respective tables below (esp. aid)?
    if instr(p_msg_insert_sql, 'ALLOC_INVN_DTL', 1, 1) > 0
    then
        p_msg_insert_sql := replace(p_msg_insert_sql, 'where ',
            ' join alloc_invn_dtl on alloc_invn_dtl.alloc_invn_dtl_id'
            || ' = t.alloc_invn_dtl_id where ');
    end if;

    if instr(p_msg_insert_sql, 'TASK_DTL', 1, 1) > 0
    then
        p_msg_insert_sql := replace(p_msg_insert_sql, 'where ',
            ' join task_dtl on task_dtl.task_id = t.task_id '
            || ' and task_dtl.task_seq_nbr = t.task_seq_nbr where ');
    end if;

    if instr(p_msg_insert_sql, 'TASK_HDR', 1, 1) > 0
    then
        p_msg_insert_sql := replace(p_msg_insert_sql, 'where ',
            ' join task_hdr on task_hdr.task_id = t.task_id where ');
    end if;

    if instr(p_msg_insert_sql, 'LOCN_HDR', 1, 1) > 0
    then
        p_msg_insert_sql := replace(p_msg_insert_sql, 'where ',
            ' join locn_hdr on locn_hdr.locn_id = t.pull_locn_id where ');
    end if;

    if instr(p_msg_insert_sql, 'RESV_LOCN_HDR', 1, 1) > 0
    then
        p_msg_insert_sql := replace(p_msg_insert_sql, 'where ',
            ' join resv_locn_hdr on resv_locn_hdr.locn_id = t.pull_locn_id'
            ||' where ');
    end if;

    -- pack carton joins
    if (instr(p_msg_insert_sql, 'PKT_HDR', 1, 1) > 0
        or instr(p_msg_insert_sql, 'PKT_DTL', 1, 1) > 0 )
    then
        p_msg_insert_sql := replace(p_msg_insert_sql, 'where ',
            ' join pkt_hdr on pkt_hdr.pkt_ctrl_nbr = t.pkt_ctrl_nbr '
            || ' and pkt_hdr.whse = ' || p_whse || ' where ');
    end if;

    if instr(p_msg_insert_sql, 'PKT_DTL', 1, 1) > 0
    then
        p_msg_insert_sql := replace(p_msg_insert_sql, 'where ',
            ' join pkt_dtl on pkt_dtl.pkt_ctrl_nbr = t.pkt_ctrl_nbr '
            || ' and pkt_dtl.pkt_seq_nbr = t.pkt_seq_nbr '
            || ' and pkt_dtl.pkt_hdr_id = pkt_hdr.pkt_hdr_id where ');
    end if;

    if instr(p_msg_insert_sql, 'PICK_LOCN_HDR', 1, 1) > 0
    then
        p_msg_insert_sql := replace(p_msg_insert_sql, 'where ',
            ' join pick_locn_hdr on pick_locn_hdr.locn_id = t.pull_locn_id'
            || ' where ');
    end if;

-- check: sku attributes?
    if instr(p_msg_insert_sql, 'PICK_LOCN_DTL', 1, 1) > 0
    then
        p_msg_insert_sql := replace(p_msg_insert_sql, 'where ',
            ' join pick_locn_dtl on pick_locn_dtl.locn_id = t.pull_locn_id'
            || ' and pick_locn_dtl.sku_id = t.sku_id where ');
    end if;
end;
/

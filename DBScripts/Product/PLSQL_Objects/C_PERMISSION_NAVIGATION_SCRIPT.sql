---- INSERT INTO NAVIGATION ---
MERGE INTO NAVIGATION N
USING (SELECT '/wm/outbound/ui/RFREYEPCDVerifyItem.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/wm/outbound/ui/RFREYEPCDVerifyItem.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/RFREYEPCDVerifyItem.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/RFREYEPCDVerifyItem.xhtml'),
(select app_id from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/RFPCDVerifyItem.xhtml')),
(select module_id from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/RFPCDVerifyItem.xhtml')),
(select permission_id from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/RFPCDVerifyItem.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/wm/outbound/ui/RFREYEPCDVerifyItem.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/wm/outbound/ui/RFREYEPCDVerifyItem.xhtml',
(select module from RESOURCES where uri ='/wm/outbound/ui/RFPCDVerifyItem.xhtml'),1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/outbound/ui/RFREYEPCDVerifyItem.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/outbound/ui/RFREYEPCDVerifyItem.xhtml'),
(select permission_code from RESOURCE_PERMISSION where resource_id = (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/outbound/ui/RFPCDVerifyItem.xhtml')));

/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/basedata/popup/REYEAssignTrailer.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/basedata/popup/REYEAssignTrailer.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/basedata/popup/REYEAssignTrailer.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/basedata/popup/REYEAssignTrailer.xhtml'),
(select min(app_id) from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/basedata/popup/*')),
(select min(module_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/basedata/popup/*')),
(select min(permission_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/basedata/popup/*')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/basedata/popup/REYEAssignTrailer.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/basedata/popup/REYEAssignTrailer.xhtml',
(select module from RESOURCES where uri ='/basedata/popup/*'),1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/basedata/popup/REYEAssignTrailer.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/basedata/popup/REYEAssignTrailer.xhtml'),
(select min(permission_code) from RESOURCE_PERMISSION where resource_id = (SELECT RESOURCE_ID FROM RESOURCES WHERE URI LIKE '/basedata/popup/*')));


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/basedata/popup/REYEErrorMsg.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/basedata/popup/REYEErrorMsg.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/basedata/popup/REYEErrorMsg.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/basedata/popup/REYEErrorMsg.xhtml'),
(select min(app_id) from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/basedata/popup/*')),
(select min(module_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/basedata/popup/*')),
(select min(permission_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/basedata/popup/*')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/basedata/popup/REYEErrorMsg.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/basedata/popup/REYEErrorMsg.xhtml',
(select module from RESOURCES where uri ='/basedata/popup/*'),1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/basedata/popup/REYEErrorMsg.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/basedata/popup/REYEErrorMsg.xhtml'),
(select permission_code from RESOURCE_PERMISSION where resource_id = (SELECT RESOURCE_ID FROM RESOURCES WHERE URI LIKE '/wm/outbound/ui/ShipWaveTemplateList.xhtml')));


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/wm/ld/ui/REYECustProdTypeOverride.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/wm/ld/ui/REYECustProdTypeOverride.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYECustProdTypeOverride.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYECustProdTypeOverride.xhtml'),
(select min(app_id) from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(module_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(permission_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/wm/ld/ui/REYECustProdTypeOverride.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/wm/ld/ui/REYECustProdTypeOverride.xhtml',
'ACM',1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYECustProdTypeOverride.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYECustProdTypeOverride.xhtml'), 'WMARUNWV');


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/basedata/popup/REYEOrderLineItemListUserShort.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/basedata/popup/REYEOrderLineItemListUserShort.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/basedata/popup/REYEOrderLineItemListUserShort.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/basedata/popup/REYEOrderLineItemListUserShort.xhtml'),
(select min(app_id) from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/basedata/popup/*')),
(select min(module_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/basedata/popup/*')),
(select min(permission_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/basedata/popup/*')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/basedata/popup/REYEOrderLineItemListUserShort.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/basedata/popup/REYEOrderLineItemListUserShort.xhtml',
(select module from RESOURCES where uri ='/wm/ld/ui/REYECustProdTypeOverride.xhtml'),1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/basedata/popup/REYEOrderLineItemListUserShort.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/basedata/popup/REYEOrderLineItemListUserShort.xhtml'),
(select permission_code from RESOURCE_PERMISSION where resource_id = (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYECustProdTypeOverride.xhtml')));


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/basedata/popup/REYEOrderLineItemListWaveShort.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/basedata/popup/REYEOrderLineItemListWaveShort.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/basedata/popup/REYEOrderLineItemListWaveShort.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/basedata/popup/REYEOrderLineItemListWaveShort.xhtml'),
(select min(app_id) from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/basedata/popup/*')),
(select min(module_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/basedata/popup/*')),
(select min(permission_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/basedata/popup/*')),
SYSDATE,null);

-- ---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/basedata/popup/REYEOrderLineItemListWaveShort.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/basedata/popup/REYEOrderLineItemListWaveShort.xhtml',
(select module from RESOURCES where uri ='/wm/ld/ui/REYECustProdTypeOverride.xhtml'),1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/basedata/popup/REYEOrderLineItemListWaveShort.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/basedata/popup/REYEOrderLineItemListWaveShort.xhtml'),
(select permission_code from RESOURCE_PERMISSION where resource_id = (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYECustProdTypeOverride.xhtml')));


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/wm/receiving/ui/RFREYEBlindLPNScan.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/wm/receiving/ui/RFREYEBlindLPNScan.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/receiving/ui/RFREYEBlindLPNScan.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/receiving/ui/RFREYEBlindLPNScan.xhtml'),
(select app_id from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/receiving/ui/RFRecvLPN.xhtml')),
(select module_id from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/receiving/ui/RFRecvLPN.xhtml')),
(select permission_id from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/receiving/ui/RFRecvLPN.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/wm/receiving/ui/RFREYEBlindLPNScan.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/wm/receiving/ui/RFREYEBlindLPNScan.xhtml',
(select module from RESOURCES where uri ='/wm/receiving/ui/RFRecvLPN.xhtml'),1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/receiving/ui/RFREYEBlindLPNScan.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/receiving/ui/RFREYEBlindLPNScan.xhtml'),
(select permission_code from RESOURCE_PERMISSION where resource_id = (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/receiving/ui/RFRecvLPN.xhtml')));


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/wm/receiving/ui/RFREYERecvBatchDates.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/wm/receiving/ui/RFREYERecvBatchDates.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/receiving/ui/RFREYERecvBatchDates.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/receiving/ui/RFREYERecvBatchDates.xhtml'),
(select app_id from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/receiving/ui/RFRecvLPNDates.xhtml')),
(select module_id from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/receiving/ui/RFRecvLPNDates.xhtml')),
(select permission_id from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/receiving/ui/RFRecvLPNDates.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/wm/receiving/ui/RFREYERecvBatchDates.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/wm/receiving/ui/RFREYERecvBatchDates.xhtml',
(select module from RESOURCES where uri ='/wm/receiving/ui/RFRecvLPNDates.xhtml'),1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/receiving/ui/RFREYERecvBatchDates.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/receiving/ui/RFREYERecvBatchDates.xhtml'),
(select permission_code from RESOURCE_PERMISSION where resource_id = (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/receiving/ui/RFRecvLPNDates.xhtml')));


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/cbo/transactional/asn/REYEQuestionAnswerEntry.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/cbo/transactional/asn/REYEQuestionAnswerEntry.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/asn/REYEQuestionAnswerEntry.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/asn/REYEQuestionAnswerEntry.xhtml'),
(select app_id from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/asn/ASNList.jsflps')),
(select module_id from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/asn/ASNList.jsflps')),
(select permission_id from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/asn/ASNList.jsflps')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/cbo/transactional/asn/REYEQuestionAnswerEntry.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/cbo/transactional/asn/REYEQuestionAnswerEntry.xhtml',
'ACM',1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/cbo/transactional/asn/REYEQuestionAnswerEntry.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/cbo/transactional/asn/REYEQuestionAnswerEntry.xhtml'), 'WMAWMASN');


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/cbo/transactional/asn/REYEASNList.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/cbo/transactional/asn/REYEASNList.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/asn/REYEASNList.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/asn/REYEASNList.xhtml'),
(select app_id from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/asn/ASNList.jsflps')),
(select module_id from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/asn/ASNList.jsflps')),
(select permission_id from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/asn/ASNList.jsflps')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/cbo/transactional/asn/REYEASNList.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/cbo/transactional/asn/REYEASNList.xhtml',
'ACM',1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/cbo/transactional/asn/REYEASNList.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/cbo/transactional/asn/REYEASNList.xhtml'), 'WMAWMASN');


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/cbo/transactional/lpn/view/REYECLPNLotMapping.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/cbo/transactional/lpn/view/REYECLPNLotMapping.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/lpn/view/REYECLPNLotMapping.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/lpn/view/REYECLPNLotMapping.xhtml'),
(select app_id from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/lpn/view/EditLPNHeaderMain.xhtml')),
(select module_id from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/lpn/view/EditLPNHeaderMain.xhtml')),
(select permission_id from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/lpn/view/EditLPNHeaderMain.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/cbo/transactional/lpn/view/REYECLPNLotMapping.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/cbo/transactional/lpn/view/REYECLPNLotMapping.xhtml',
(select module from RESOURCES where uri ='/cbo/transactional/lpn/view/EditLPNHeaderMain.xhtml'),1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/cbo/transactional/lpn/view/REYECLPNLotMapping.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/cbo/transactional/lpn/view/REYECLPNLotMapping.xhtml'), (select permission_code from RESOURCE_PERMISSION where resource_id = (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/cbo/transactional/lpn/view/EditLPNHeaderMain.xhtml')));


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/cbo/transactional/lpn/view/REYECustomCLPNLotScreen.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/cbo/transactional/lpn/view/REYECustomCLPNLotScreen.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/lpn/view/REYECustomCLPNLotScreen.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/lpn/view/REYECustomCLPNLotScreen.xhtml'),
(select app_id from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/lpn/view/EditLPNHeaderMain.xhtml')),
(select module_id from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/lpn/view/EditLPNHeaderMain.xhtml')),
(select permission_id from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/lpn/view/EditLPNHeaderMain.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/cbo/transactional/lpn/view/REYECustomCLPNLotScreen.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/cbo/transactional/lpn/view/REYECustomCLPNLotScreen.xhtml',
(select module from RESOURCES where uri ='/cbo/transactional/lpn/view/EditLPNHeaderMain.xhtml'),1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/cbo/transactional/lpn/view/REYECustomCLPNLotScreen.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/cbo/transactional/lpn/view/REYECustomCLPNLotScreen.xhtml'), (select permission_code from RESOURCE_PERMISSION where resource_id = (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/cbo/transactional/lpn/view/EditLPNHeaderMain.xhtml')));


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/cbo/transactional/shipment/view/REYEShipmentList.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/cbo/transactional/shipment/view/REYEShipmentList.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/shipment/view/REYEShipmentList.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/shipment/view/REYEShipmentList.xhtml'),
(select min(app_id) from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/shipment/view/ShipmentList.xhtml')),
(select min(module_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/shipment/view/ShipmentList.xhtml')),
(select min(permission_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/shipment/view/ShipmentList.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/cbo/transactional/shipment/view/REYEShipmentList.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/cbo/transactional/shipment/view/REYEShipmentList.xhtml',
(select module from RESOURCES where uri ='/cbo/transactional/shipment/view/ShipmentList.xhtml'),1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/cbo/transactional/shipment/view/REYEShipmentList.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/cbo/transactional/shipment/view/REYEShipmentList.xhtml'), 'ASH');


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/cbo/transactional/shipment/view/REYEShortageShipmentOrderList.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/cbo/transactional/shipment/view/REYEShortageShipmentOrderList.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/shipment/view/REYEShortageShipmentOrderList.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/shipment/view/REYEShortageShipmentOrderList.xhtml'),
(select min(app_id) from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/shipment/view/ShipmentList.xhtml')),
(select min(module_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/shipment/view/ShipmentList.xhtml')),
(select min(permission_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/cbo/transactional/shipment/view/ShipmentList.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/cbo/transactional/shipment/view/REYEShortageShipmentOrderList.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/cbo/transactional/shipment/view/REYEShortageShipmentOrderList.xhtml',
(select module from RESOURCES where uri ='/cbo/transactional/shipment/view/ShipmentList.xhtml'),1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/cbo/transactional/shipment/view/REYEShortageShipmentOrderList.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/cbo/transactional/shipment/view/REYEShortageShipmentOrderList.xhtml'), 'ASH');


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/wm/ld/ui/REYECustProdTypeOverrideAssignment.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/wm/ld/ui/REYECustProdTypeOverrideAssignment.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYECustProdTypeOverrideAssignment.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYECustProdTypeOverrideAssignment.xhtml'),
(select min(app_id) from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(module_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(permission_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/wm/ld/ui/REYECustProdTypeOverrideAssignment.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/wm/ld/ui/REYECustProdTypeOverrideAssignment.xhtml',
'ACM',1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYECustProdTypeOverrideAssignment.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYECustProdTypeOverrideAssignment.xhtml'), 'WMARUNWV');


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/wm/ld/ui/REYEEquipment.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/wm/ld/ui/REYEEquipment.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYEEquipment.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYEEquipment.xhtml'),
(select min(app_id) from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(module_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(permission_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/wm/ld/ui/REYEEquipment.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/wm/ld/ui/REYEEquipment.xhtml',
'ACM',1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYEEquipment.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYEEquipment.xhtml'), 'WMARUNWV');


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/wm/ld/ui/REYEEquipmentAssignment.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/wm/ld/ui/REYEEquipmentAssignment.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYEEquipmentAssignment.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYEEquipmentAssignment.xhtml'),
(select min(app_id) from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(module_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(permission_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/wm/ld/ui/REYEEquipmentAssignment.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/wm/ld/ui/REYEEquipmentAssignment.xhtml',
'ACM',1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYEEquipmentAssignment.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYEEquipmentAssignment.xhtml'), 'WMARUNWV');


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/wm/ld/ui/REYEEquipmentCompartment.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/wm/ld/ui/REYEEquipmentCompartment.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYEEquipmentCompartment.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYEEquipmentCompartment.xhtml'),
(select min(app_id) from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(module_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(permission_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/wm/ld/ui/REYEEquipmentCompartment.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/wm/ld/ui/REYEEquipmentCompartment.xhtml',
'ACM',1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYEEquipmentCompartment.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYEEquipmentCompartment.xhtml'), 'WMARUNWV');


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/wm/ld/ui/REYEEquipmentCompartmentAllocSeq.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/wm/ld/ui/REYEEquipmentCompartmentAllocSeq.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYEEquipmentCompartmentAllocSeq.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYEEquipmentCompartmentAllocSeq.xhtml'),
(select min(app_id) from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(module_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(permission_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/wm/ld/ui/REYEEquipmentCompartmentAllocSeq.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/wm/ld/ui/REYEEquipmentCompartmentAllocSeq.xhtml',
'ACM',1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYEEquipmentCompartmentAllocSeq.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYEEquipmentCompartmentAllocSeq.xhtml'), 'WMARUNWV');


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/wm/ld/ui/REYEEquipmentCompartmentDetails.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/wm/ld/ui/REYEEquipmentCompartmentDetails.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYEEquipmentCompartmentDetails.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYEEquipmentCompartmentDetails.xhtml'),
(select min(app_id) from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(module_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(permission_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/wm/ld/ui/REYEEquipmentCompartmentDetails.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/wm/ld/ui/REYEEquipmentCompartmentDetails.xhtml',
'ACM',1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYEEquipmentCompartmentDetails.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYEEquipmentCompartmentDetails.xhtml'), 'WMARUNWV');


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/wm/ld/ui/REYEGrid.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/wm/ld/ui/REYEGrid.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYEGrid.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYEGrid.xhtml'),
(select min(app_id) from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(module_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(permission_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/wm/ld/ui/REYEGrid.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/wm/ld/ui/REYEGrid.xhtml',
'ACM',1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYEGrid.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYEGrid.xhtml'), 'WMARUNWV');


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/wm/ld/ui/REYEItemCat.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/wm/ld/ui/REYEItemCat.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYEItemCat.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYEItemCat.xhtml'),
(select min(app_id) from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(module_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(permission_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/wm/ld/ui/REYEItemCat.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/wm/ld/ui/REYEItemCat.xhtml',
'ACM',1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYEItemCat.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYEItemCat.xhtml'), 'WMARUNWV');


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/wm/ld/ui/REYEItemCatLoadType.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/wm/ld/ui/REYEItemCatLoadType.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYEItemCatLoadType.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYEItemCatLoadType.xhtml'),
(select min(app_id) from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(module_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(permission_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/wm/ld/ui/REYEItemCatLoadType.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/wm/ld/ui/REYEItemCatLoadType.xhtml',
'ACM',1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYEItemCatLoadType.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYEItemCatLoadType.xhtml'), 'WMARUNWV');


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/wm/ld/ui/REYEItemCatMapping.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/wm/ld/ui/REYEItemCatMapping.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYEItemCatMapping.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYEItemCatMapping.xhtml'),
(select min(app_id) from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(module_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(permission_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/wm/ld/ui/REYEItemCatMapping.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/wm/ld/ui/REYEItemCatMapping.xhtml',
'ACM',1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYEItemCatMapping.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYEItemCatMapping.xhtml'), 'WMARUNWV');


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/wm/ld/ui/REYEItemCatProdType.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/wm/ld/ui/REYEItemCatProdType.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYEItemCatProdType.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYEItemCatProdType.xhtml'),
(select min(app_id) from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(module_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(permission_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/wm/ld/ui/REYEItemCatProdType.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/wm/ld/ui/REYEItemCatProdType.xhtml',
'ACM',1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYEItemCatProdType.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYEItemCatProdType.xhtml'), 'WMARUNWV');


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/wm/ld/ui/REYEItemCatStorType.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/wm/ld/ui/REYEItemCatStorType.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYEItemCatStorType.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYEItemCatStorType.xhtml'),
(select min(app_id) from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(module_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(permission_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/wm/ld/ui/REYEItemCatStorType.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/wm/ld/ui/REYEItemCatStorType.xhtml',
'ACM',1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYEItemCatStorType.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYEItemCatStorType.xhtml'), 'WMARUNWV');


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/wm/ld/ui/REYELoadDiagram.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/wm/ld/ui/REYELoadDiagram.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYELoadDiagram.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYELoadDiagram.xhtml'),
(select min(app_id) from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(module_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(permission_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/wm/ld/ui/REYELoadDiagram.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/wm/ld/ui/REYELoadDiagram.xhtml',
'ACM',1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYELoadDiagram.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYELoadDiagram.xhtml'), 'WMARUNWV');


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/wm/ld/ui/REYELoadDiagramDetailListOne.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/wm/ld/ui/REYELoadDiagramDetailListOne.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYELoadDiagramDetailListOne.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYELoadDiagramDetailListOne.xhtml'),
(select min(app_id) from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(module_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(permission_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/wm/ld/ui/REYELoadDiagramDetailListOne.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/wm/ld/ui/REYELoadDiagramDetailListOne.xhtml',
'ACM',1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYELoadDiagramDetailListOne.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYELoadDiagramDetailListOne.xhtml'), 'WMARUNWV');


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/wm/ld/ui/REYELoadDiagramDetailListTwo.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/wm/ld/ui/REYELoadDiagramDetailListTwo.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYELoadDiagramDetailListTwo.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYELoadDiagramDetailListTwo.xhtml'),
(select min(app_id) from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(module_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(permission_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
SYSDATE,null);

---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/wm/ld/ui/REYELoadDiagramDetailListTwo.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT MAX(RESOURCE_ID) From RESOURCES)+1,'/wm/ld/ui/REYELoadDiagramDetailListTwo.xhtml',
'ACM',1,NULL); 

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYELoadDiagramDetailListTwo.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYELoadDiagramDetailListTwo.xhtml'), 'WMARUNWV');


/* ################################################################################################################### */


MERGE INTO NAVIGATION N
USING (SELECT '/wm/ld/ui/REYELoadDiagramMain.xhtml' URL FROM DUAL) D
ON (N.URL = D.URL)
WHEN NOT MATCHED THEN
INSERT VALUES((SELECT Max(NAVIGATION_ID) From NAVIGATION)+1,NULL,NULL,'/wm/ld/ui/REYELoadDiagramMain.xhtml',-1,'F',NULL,NULL,SYSDATE,NULL);

---- Insert into NAVIGATION_APP_MOD_PERM, using same values from parent UI ----
MERGE INTO NAVIGATION_APP_MOD_PERM N
USING (SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYELoadDiagramMain.xhtml') D
ON (N.NAVIGATION_ID = D.NAVIGATION_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/ld/ui/REYELoadDiagramMain.xhtml'),
(select min(app_id) from NAVIGATION_APP_MOD_PERM where navigation_id = 
(SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(module_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
(select min(permission_id) from NAVIGATION_APP_MOD_PERM where navigation_id = (
SELECT NAVIGATION_ID FROM NAVIGATION WHERE URL = '/wm/outbound/ui/ShipWaveParmTemplate.xhtml')),
SYSDATE,null);

EXEC SEQUPDT('RESOURCES', 'RESOURCE_ID', 'SEQ_RESOURCE_ID');

-- ---- INSERT INTO RESOURCES ---
MERGE INTO RESOURCES R
USING (SELECT '/wm/ld/ui/REYELoadDiagramMain.xhtml' URI FROM DUAL)D
ON (R.URI = D.URI)
WHEN NOT MATCHED THEN
INSERT VALUES (SEQ_RESOURCE_ID.NEXTVAL,'/wm/ld/ui/REYELoadDiagramMain.xhtml','ACM',1,NULL);   

---- INSERT INTO RESOURCE_PERMISSION ---
MERGE INTO RESOURCE_PERMISSION R
USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYELoadDiagramMain.xhtml')D
ON (R.RESOURCE_ID = D.RESOURCE_ID)
WHEN NOT MATCHED THEN
INSERT VALUES ((SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/ld/ui/REYELoadDiagramMain.xhtml'),'WMARUNWV');
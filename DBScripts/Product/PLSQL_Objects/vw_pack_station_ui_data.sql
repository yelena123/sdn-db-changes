create or replace force view vw_pack_station_ui_data
as 
select iv.item_id, iv.sku_attr_1, iv.sku_attr_2, iv.sku_attr_3, iv.sku_attr_4, iv.sku_attr_5, 
    iv.cntry_of_orgn, iv.batch_nbr, iv.prod_stat, iv.pull_locn_id, iv.dest_locn_id, iv.pikr_nbr, 
    iv.carton_nbr, iv.pkt_ctrl_nbr, iv.cntr_nbr, iv.tote_nbr, iv.tc_order_id, iv.line_item_id, 
    iv.whse, iv.cd_master_id, sum(iv.qty_alloc) units_ordered, sum(iv.qty_pulld) units_packed,
    iv.invn_type,
    iv.item_id || iv.sku_attr_1 || iv.sku_attr_2 || iv.sku_attr_3 || iv.sku_attr_4 || iv.sku_attr_5
         || iv.cntry_of_orgn || iv.batch_nbr || iv.prod_stat || iv.pull_locn_id || iv.dest_locn_id
         || iv.pikr_nbr || iv.carton_nbr || iv.pkt_ctrl_nbr || iv.cntr_nbr || iv.tote_nbr
         || iv.tc_order_id || iv.line_item_id || iv.whse || iv.cd_master_id || iv.invn_type row_str
from 
(
    select aid.item_id, aid.sku_attr_1, aid.sku_attr_2, aid.sku_attr_3, aid.sku_attr_4, 
        aid.sku_attr_5, aid.cntry_of_orgn, aid.batch_nbr, aid.prod_stat, aid.pull_locn_id,
        aid.dest_locn_id, aid.pikr_nbr, aid.carton_nbr, aid.pkt_ctrl_nbr, aid.cntr_nbr, 
        aid.tote_nbr, aid.tc_order_id, aid.line_item_id, aid.whse, aid.cd_master_id, aid.qty_alloc, 
        aid.qty_pulld, aid.invn_type
    from alloc_invn_dtl aid
    where aid.invn_need_type = 52 and aid.stat_code < 90 and coalesce(aid.alloc_uom, ' ') != 'P'
    union all
    select td.item_id, td.sku_attr_1, td.sku_attr_2, td.sku_attr_3, td.sku_attr_4, td.sku_attr_5, 
        td.cntry_of_orgn, td.batch_nbr, td.prod_stat, td.pull_locn_id, td.dest_locn_id, td.pikr_nbr, 
        td.carton_nbr, td.pkt_ctrl_nbr, td.cntr_nbr, td.tote_nbr, td.tc_order_id, td.line_item_id, 
        th.whse, td.cd_master_id, td.qty_alloc, td.qty_pulld, td.invn_type
    from task_hdr th
    join task_dtl td on td.task_id = th.task_id and td.invn_need_type = 52 and td.stat_code < 90 
        and coalesce(td.alloc_uom, ' ') != 'P'
    where th.invn_need_type = 52 and th.stat_code < 90
) iv
group by iv.item_id, iv.sku_attr_1, iv.sku_attr_2, iv.sku_attr_3, iv.sku_attr_4, iv.sku_attr_5, 
    iv.cntry_of_orgn, iv.batch_nbr, iv.prod_stat, iv.pull_locn_id, iv.dest_locn_id, iv.pikr_nbr, 
    iv.carton_nbr, iv.pkt_ctrl_nbr, iv.cntr_nbr, iv.tote_nbr, iv.tc_order_id, iv.line_item_id, 
    iv.whse, iv.cd_master_id, iv.invn_type
/
show errors;

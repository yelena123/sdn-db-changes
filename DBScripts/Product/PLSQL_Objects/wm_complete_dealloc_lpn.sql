create or replace procedure wm_complete_dealloc_lpn
(
    p_user_id           in user_profile.user_id%type,
    p_tc_company_id     in company.company_id%type,
    p_facility_id       in facility.facility_id%type,
    p_whse              in facility.whse%type
)
as
    v_proc_stat_code        pix_tran.proc_stat_code%type := 0;
    v_xml_group_attr        pix_tran.xml_group_id%type;
    v_ref_codes_insert_list varchar2(1000);
    v_ref_codes_select_list varchar2(1000);
    v_insert_sql            varchar2(4000);
    v_is_601_pix_configured number(1) := 0;
begin

    -- the do status informational pix can also be turned off in S501
    select case when exists
    (
        select 1
        from sys_code sc
        where sc.rec_type = 'S' and sc.code_type = '501'
            and sc.code_id >= '120' and substr(sc.misc_flags, 1, 1) = '1'
    ) then 1 else 0 end
    into v_is_601_pix_configured
    from dual;
    
    manh_get_pix_config_data  ('601', '001', null, p_tc_company_id,
        v_proc_stat_code, v_xml_group_attr);
        
    -- gen do status change pix
    if (v_proc_stat_code != 91 and v_is_601_pix_configured = 1)
    then
        manh_get_pix_ref_code_col_list('601', '001', null, p_tc_company_id,
            v_ref_codes_insert_list, v_ref_codes_select_list);

        v_insert_sql :=
               'insert into pix_tran'
            || '('
            || '    tran_type, tran_code, user_id, create_date_time, mod_date_time,'
            || '    pix_seq_nbr, tran_nbr, proc_stat_code, facility_id, whse,'
            || '    tc_company_id, pix_tran_id, xml_group_id'
            ||      v_ref_codes_insert_list
            || ') '
            || 'select ''601'', ''001'', :user_id, sysdate, sysdate, 1 pix_seq_nbr,'
            || '    pix_tran_id_seq.nextval, :proc_stat_code, :p_facility_id,'
            || '    :p_whse, orders.tc_company_id, pix_tran_id_seq.nextval, '''
            ||      v_xml_group_attr || '''' || v_ref_codes_select_list || ' '
            || 'from orders '
            || 'join tmp_old_ord_status t on t.order_id = orders.order_id '
            || 'where t.do_status != orders.do_status '
            || '    and exists'
            || '    ('
            || '        select 1'
            || '        from sys_code sc'
            || '        where sc.rec_type = ''S'' and sc.code_type = ''501'''
            || '            and substr(sc.misc_flags, 1, 1) = ''1'''
            || '            and to_number(sc.code_id) = orders.do_status'
            || '    )';

        execute immediate v_insert_sql using p_user_id, v_proc_stat_code,
            p_facility_id, p_whse;
        wm_cs_log('order status change pix - 601 001, generated ' || sql%rowcount);
    end if;

    -- gen decrement of allocations summary pix 
    manh_get_pix_config_data('620', '09', '01', p_tc_company_id, v_proc_stat_code,
        v_xml_group_attr);

    if (v_proc_stat_code != 91)
    then
	     insert into pix_tran 
	     (
	        invn_adjmt_qty, proc_stat_code, cntry_of_orgn, item_id, 
	        uom, sku_attr_1, sku_attr_2, sku_attr_3, sku_attr_4, 
	        sku_attr_5, invn_adjmt_type, invn_type, prod_stat, 
	        batch_nbr, tc_company_id, company_code, season, season_yr, 
	        style, style_sfx,color, color_sfx, sec_dim,qual, 
	        size_desc, size_range_code, size_rel_posn_in_table, 
	        item_name, tran_type, tran_code, tran_nbr, pix_seq_nbr, 
	        whse, actn_code, create_date_time, mod_date_time, 
	        user_id, wm_version_id, facility_id, xml_group_id 
	     )
	     with giv as 
	     ( 
            select t.item_id, sum(t.qty_alloc - t.qty_pulld) invn_adjmt_qty,
                t.invn_type, t.prod_stat, t.cntry_of_orgn, t.batch_nbr, 
                t.sku_attr_1, t.sku_attr_2, t.sku_attr_3, t.sku_attr_4, 
                t.sku_attr_5, t.alloc_uom, t.cd_master_id 
            from tmp_lpn_alloc_dtl t
            join invn_need_type int on (int.invn_need_type = t.invn_need_type
                and int.cd_master_id = t.cd_master_id
                and int.task_cmpl_corr_upd = 6)
            where exists
            (
                select 1
                from tmp_orig_oli_qty t2 
                where t2.reference_line_item_id = t.line_item_id
            )
            group by t.item_id, t.invn_type, t.prod_stat, t.cntry_of_orgn, 
                t.batch_nbr, t.sku_attr_1, t.sku_attr_2, t.sku_attr_3, 
                t.sku_attr_4, t.sku_attr_5, t.alloc_uom, t.cd_master_id 
	     ) 
	     select iv.invn_adjmt_qty, iv.pix_create_stat_code, 
	         iv.cntry_of_orgn, iv.item_id, iv.alloc_uom,
	         iv.sku_attr_1, iv.sku_attr_2, iv.sku_attr_3,
	         iv.sku_attr_4, iv.sku_attr_5, 'S' invn_adjmt_type, 
	         iv.invn_type, iv.prod_stat, iv.batch_nbr, iv.cd_master_id,
	         company.company_code, item_cbo.item_season,
	         item_cbo.item_season_year, item_cbo.item_style,
	         item_cbo.item_style_sfx, item_cbo.item_color,
	         item_cbo.item_color_sfx, item_cbo.item_second_dim,
	         item_cbo.item_quality, item_cbo.item_size_desc,
	         item_wms.size_range_code, item_wms.size_rel_posn_in_table,
	         item_cbo.item_name, '620', '09', pix_tran_id_seq.nextval , 
	         rownum pix_seq_nbr, p_whse, '01', sysdate, sysdate, p_user_id, 1,  
           p_facility_id , 
	         ( case when v_xml_group_attr is null then null else 
             v_xml_group_attr end ) 
	     from 
       ( 
	         select giv.cntry_of_orgn, giv.item_id, giv.alloc_uom, 
	             giv.sku_attr_1, giv.sku_attr_2,giv.sku_attr_3,
	             giv.sku_attr_4, giv.sku_attr_5, giv.invn_type,
	             giv.prod_stat, giv.batch_nbr,
	             giv.cd_master_id, giv.invn_adjmt_qty,
	             row_number() over(partition by giv.cntry_of_orgn,
	             giv.item_id, giv.alloc_uom, giv.sku_attr_1, giv.sku_attr_2,
	             giv.sku_attr_3, giv.sku_attr_4, giv.sku_attr_5, 
	             giv.invn_type, giv.prod_stat, 
	             giv.batch_nbr, giv.cd_master_id 
	                 order by pix_tran_code.cd_master_id nulls last) priority,
	             coalesce(pix_tran_code.pix_create_stat_code, 10) 
	             pix_create_stat_code
	         from giv 
	         left join pix_tran_code on coalesce(pix_tran_code.cd_master_id,
	             giv.cd_master_id) = giv.cd_master_id
	         and pix_tran_code.tran_type = '620'
	         and pix_tran_code.tran_code = '09'
	         and pix_tran_code.actn_code = '01'
       ) iv 
	     join item_cbo on item_cbo.item_id = iv.item_id
	     join item_wms on item_wms.item_id = item_cbo.item_id
	     join company on company.company_id = iv.cd_master_id
	     where iv.priority < 2;
       wm_cs_log('allocations pix - 620 09 01 generated ' || sql%rowcount);
    end if;

    for cur_pix in
    (
        select '620' tran_type, '02' tran_code, '02' actn_code from dual
        union all
        select '620' tran_type, '03' tran_code, '02' actn_code from dual
    )
    loop
        -- gen Receiving flow-thru de-allocation pix
        manh_get_pix_config_data  (cur_pix.tran_type, cur_pix.tran_code, cur_pix.actn_code, p_tc_company_id,
            v_proc_stat_code, v_xml_group_attr);
        
        if (v_proc_stat_code != 91)
        then
            v_ref_codes_insert_list := '';
            v_ref_codes_select_list := '';
        
            manh_get_pix_ref_code_col_list(cur_pix.tran_type, cur_pix.tran_code, cur_pix.actn_code, p_tc_company_id,
                v_ref_codes_insert_list, v_ref_codes_select_list);    
        
            if (v_ref_codes_select_list is null and p_tc_company_id is not null)
            then
                manh_get_pix_ref_code_col_list(cur_pix.tran_type, cur_pix.tran_code, cur_pix.actn_code, null,
                    v_ref_codes_insert_list, v_ref_codes_select_list);  
            end if;
        
            v_insert_sql :=
                   'insert into pix_tran '
                || '('
                || '   item_name, pix_tran_id, tran_type, tran_code, tran_nbr,'
                || '   pix_seq_nbr, proc_stat_code, whse, season, season_yr, style,'
                || '   style_sfx, color, color_sfx, sec_dim, qual, size_desc,'
                || '   size_range_code,size_rel_posn_in_table,invn_type, prod_stat,'
                || '   batch_nbr, sku_attr_1, sku_attr_2, sku_attr_3, sku_attr_4,'
                || '   sku_attr_5, cntry_of_orgn, invn_adjmt_qty, invn_adjmt_type,'
                || '   uom, actn_code, create_date_time, mod_date_time, user_id,'
                || '   wm_version_id, item_id, facility_id, tc_company_id,'
                || '   company_code, xml_group_id '
                ||      v_ref_codes_insert_list
                || ') '
                || 'select item_cbo.item_name, pix_tran_id_seq.nextval,'''
                ||      cur_pix.tran_type ||''', '''||cur_pix.tran_code
                || '''  , pix_tran_id_seq.nextval,'
                || '    rownum, '
                || coalesce(v_proc_stat_code, '10')
                || '    ,:p_whse, '
                || '    item_cbo.item_season, item_cbo.item_season_year,'
                || '    item_cbo.item_style, item_cbo.item_style_sfx,'
                || '    item_cbo.item_color, item_cbo.item_color_sfx,'
                || '    item_cbo.item_second_dim, item_cbo.item_quality,'
                || '    item_cbo.item_size_desc, item_wms.size_range_code,'
                || '    item_wms.size_rel_posn_in_table, order_line_item.invn_type,'
                || '    order_line_item.prod_stat,order_line_item.batch_nbr, '
                || '    order_line_item.item_attr_1, order_line_item.item_attr_2,'
                || '    order_line_item.item_attr_3, order_line_item.item_attr_4,'
                || '    order_line_item.item_attr_5, order_line_item.cntry_of_orgn,'
                || '    iv.decrement_qty, ''S'', size_uom.size_uom,'''
                ||      cur_pix.actn_code
                || '''  , current_timestamp, current_timestamp, '
                || '    :p_user_id, 1, order_line_item.item_id, :p_facility_id, '   
                || '    order_line_item.tc_company_id, '
                || '    company.company_code, ''' 
                || case when v_xml_group_attr is null then 'null' else v_xml_group_attr end
                || '''' || v_ref_codes_select_list || ' '
                || 'from order_line_item '
                || 'join '
                || '( '
                || '    select '
                || '        ( '
                || '             case when t1.parent_line_item_id is null ' 
                || '                 then t1.line_item_id '
                || '             else t1.parent_line_item_id '
                || '             end '
                || '        ) line_item_id, '            
                || '        ( '
                || '             case when t1.rng_avail_qty <= t2.qty_to_dealloc ' 
                || '                 then t1.available_qty '
                || '             else t2.qty_to_dealloc - t1.lag_rng_avail_qty '
                || '             end '
                || '         ) decrement_qty '
                || '    from tmp_orig_oli_qty t1 '
                || '    join tmp_dealloc_oli t2 on t2.line_item_id = t1.reference_line_item_id '
                || ') iv on (iv.line_item_id = order_line_item.line_item_id )'
                || 'join orders on (orders.order_id =order_line_item.order_id '
                || '   and order_line_item.line_item_id = iv.line_item_id '
                || '   and orders.is_original_order = 1 '
                || '   and orders.o_facility_id = :p_facility_id ) '
                || 'join item_cbo on item_cbo.item_id = order_line_item.item_id '
                || 'join item_wms on item_wms.item_id = item_cbo.item_id '
                || 'join size_uom on size_uom.size_uom_id = order_line_item.qty_uom_id_base '
                || 'join company on company.company_id = orders.tc_company_id '
                || '    and order_line_item.fulfillment_type= '
                || case when cur_pix.tran_code = '02' then '''2''' else '''3''' end;   

            execute immediate v_insert_sql using p_whse, p_user_id, p_facility_id, p_facility_id;
            wm_cs_log('620 '|| cur_pix.tran_code ||' 02 pix generated ' || sql%rowcount);   
        end if;
    end loop;
end;
/
show errors;

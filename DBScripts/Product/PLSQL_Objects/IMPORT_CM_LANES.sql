CREATE OR REPLACE
FUNCTION "IMPORT_CM_LANES" (
    vtc_company_id IMPORT_CM_LANE.tc_company_id%TYPE,
    vrfp_id  IMPORT_CM_LANE.rfp_id%TYPE,
    vround_num  IMPORT_CM_LANE.round_num%TYPE,
    vscenario_id IMPORT_CM_LANE.scenario_id%TYPE,
    vimport_type IMPORT_CM_LANE.import_type%TYPE,
    vhistoricalaward INT
  )
RETURN CHAR
AS
  retval   CHAR(7);
  v_count  SMALLINT;
  v_comb_lane_id   IMPORT_CM_LANE.comb_lane_id%TYPE;
  v_error_code     IMPORT_CM_LANE.error_code%TYPE;
  v_good_lane      CHAR(1);
  cl_lane_id   COMB_LANE.lane_id%TYPE;
  cl_o_city   COMB_LANE.o_city%TYPE;
  cl_o_state_prov  COMB_LANE.o_state_prov%TYPE;
  cl_o_country_code  COMB_LANE.o_country_code%TYPE;
  cl_o_postal_code  COMB_LANE.o_postal_code%TYPE;
  cl_o_facility_id  COMB_LANE.o_facility_id%TYPE;
  cl_o_facility_alias_id  COMB_LANE.o_facility_alias_id%TYPE;
  cl_o_zone_id   COMB_LANE.o_zone_id%TYPE;
  cl_o_zone_code   ZONE.zone_name%TYPE;
  cl_d_city   COMB_LANE.d_city%TYPE;
  cl_d_state_prov  COMB_LANE.d_state_prov%TYPE;
  cl_d_country_code  COMB_LANE.d_country_code%TYPE;
  cl_d_postal_code  COMB_LANE.d_postal_code%TYPE;
  cl_d_facility_id  COMB_LANE.d_facility_id%TYPE;
  cl_d_zone_id   COMB_LANE.d_zone_id%TYPE;
  cl_d_zone_code   ZONE.zone_name%TYPE;
  cl_d_facility_alias_id  COMB_LANE.d_facility_alias_id%TYPE;
  cl_frequency   COMB_LANE.FREQUENCY%TYPE;
  cl_tc_company_id  COMB_LANE.tc_company_id%TYPE;
  cl_is_rating   COMB_LANE.is_rating%TYPE;
  v_currencycode   RFP.currencycode%TYPE;
  rfp_tccompanyid  RFP.tccompanyid%TYPE;
  rfp_basedatamode  RFP.basedatamode%TYPE;
  rfp_status   RFP.RFPSTATUS%TYPE;
  rfp_allowpackages  RFP.allowpackages%TYPE;
  rfp_exporttoratingrouting RFP.exporttoratingrouting%TYPE;
  rfpbu_business_unit  RFP_BUSINESSUNIT.BUSINESS_UNIT%TYPE;
  let_equipmenttypeid  LANEEQUIPMENTTYPE.equipmenttype_id%TYPE;
  let_servicetype  LANEEQUIPMENTTYPE.servicetype_id%TYPE;
  let_protectionlevel  LANEEQUIPMENTTYPE.protectionlevel_id%TYPE;
  let_mot                     LANEEQUIPMENTTYPE.MOT%TYPE;
  let_commoditycode  LANEEQUIPMENTTYPE.commoditycode_id%TYPE;
  cld_obcarriercodeid  CARRIER_CODE.CARRIER_CODE%TYPE;
  cld_carrierid  CARRIER_CODE.carrier_id%TYPE;
  --cld_historicalaward  COMB_LANE_DTL.is_preferred%TYPE;
  cld_lane_dtl_seq  COMB_LANE_DTL.lane_dtl_seq%TYPE;
  cld_equipment_code  EQUIPMENT.equipment_id%TYPE;
  cld_service_level  SERVICE_LEVEL.service_level_id%TYPE;
  cld_protection_level  PROTECTION_LEVEL.protection_level_id%TYPE;
  cld_mot    MOT.MOT%TYPE;
  cld_commoditycode  COMMODITY_CODE.commodity_code_id%TYPE;
  cld_is_rating   COMB_LANE_DTL.is_rating%TYPE;
  cld_weekly_commitment   COMB_LANE_DTL.weekly_commitment%TYPE;
  cld_daily_commitment   COMB_LANE_DTL.daily_commitment%TYPE;
  cld_montly_commitment   COMB_LANE_DTL.montly_commitment%TYPE;
  cld_yearly_commitment  COMB_LANE_DTL.yearly_commitment%TYPE;
  cld_weekly_capacity     COMB_LANE_DTL.weekly_capacity%TYPE;
  cld_daily_capacity   COMB_LANE_DTL.daily_capacity%TYPE;
  cld_montly_capacity   COMB_LANE_DTL.montly_capacity%TYPE;
  cld_yearly_capacity  COMB_LANE_DTL.yearly_capacity%TYPE;
  cld_package_id          COMB_LANE_DTL.package_id%TYPE;
  cld_package_name        COMB_LANE_DTL.package_name%TYPE;
  rlds_rate   RATING_LANE_DTL_RATE.rate%TYPE;
  l_rec_processed SMALLINT;
  v_lanebid_lanecapacity    LANEBID.lanecapacity%TYPE;
  v_lanebid_surgecapacity   LANEBID.surgecapacity%TYPE;
  v_rateperdistance  LANEBID.rateperdistance%TYPE;
  v_flatcharge   LANEBID.flatcharge%TYPE;
  v_minimumcharge  LANEBID.minimumcharge%TYPE;
  t_laneid                LANE.laneid%TYPE;
  t_tp_company_id  CARRIER_CODE.tp_company_id%TYPE;
  t_obcarriercodeid  OBCARRIERCODE.obcarriercodeid%TYPE;
  l_count                INT;
  l_laneequipmenttypeid  LANEEQUIPMENTTYPE.laneequipmenttypeid%TYPE;
  t_package_id  COMB_LANE_DTL.package_id%TYPE;
  l_package_id  COMB_LANE_DTL.package_id%TYPE;
  t_sum_lanecapacity INT;
  l_error_yn  CHAR(1);
  v_lanevolume  DECIMAL(14,3);
  v_laneid               COMB_LANE.lane_id%TYPE;
  l_totalvolume  PACKAGE.totalvolume%TYPE;
  v_packageid  PACKAGE.packageid%TYPE;
  v_sum_lanecapacity INT;
  v_obcarriercodeid INT;
  v_lanecapacity  DECIMAL(14,3);
  v_package_id  INT;
  v_newoverrideid        INT;
  s_package_id  INT;
  v_isimported  INT;
  v_package_lane_failed_count INT;
  v_package_failed CHAR(1);
  v_package_id_seq INT;
  v_equipmenttypeid  LANEEQUIPMENTTYPE.equipmenttype_id%TYPE;
  v_servicetype  LANEEQUIPMENTTYPE.servicetype_id%TYPE;
  v_protectionlevel LANEEQUIPMENTTYPE.protectionlevel_id%TYPE;
  v_mot   LANEEQUIPMENTTYPE.MOT%TYPE;
  v_commoditycode LANEEQUIPMENTTYPE.commoditycode_id%TYPE;
  l_laneequipmenttype_volume LANEEQUIPMENTTYPE.volume%TYPE;
  l_contract_type INT;
  v_contract_type INT;
  v_colname  RFPCOLPROPERTIES.colname%TYPE;
  v_col_required  RFPCOLPROPERTIES.isrequired%TYPE;
  l_error_no  CHAR(2);
  v_laneattributeversion RFP.laneattributeversion%TYPE;
  rfp_laneattributeversion RFP.laneattributeversion%TYPE;
  v_is_fc_selected CHAR(1);
  v_is_minc_selected CHAR(1);
  v_is_rpd_selected CHAR(1);
  s_package_name  PACKAGE.packagename%TYPE;
  v_max_allowed_errors_const INT;
  is_rating_routing_ok CHAR(1);
 /* select all the temp packages, used in c2, c6 */
  CURSOR cur_package_gtt IS
  SELECT package_id, package_name
  FROM PACKAGE_GTT;
 /* cursor to get all the import_cm_lane entries for looping */
   CURSOR cur_comb_lane(
    ll_tc_company_id  INTEGER,
    ll_rfp_id INTEGER,
    ll_round_num INTEGER,
    ll_scenario_id INTEGER,
    ll_import_type INTEGER
  ) IS
  SELECT comb_lane_id, error_code
  FROM  IMPORT_CM_LANE
  WHERE tc_company_id = ll_tc_company_id
   AND   rfp_id  = ll_rfp_id
   AND   round_num = ll_round_num
   AND   scenario_id = ll_scenario_id
   AND   import_type = ll_import_type
   AND error_code IS NULL;
 /* cursor for comblane details, used as c5 */
       CURSOR cur_lane_details(
    ll_tc_company_id  INTEGER,
    ll_comb_lane_id INTEGER,
    ll_rfp_basedatamode RFP.basedatamode%TYPE
   ) IS
   SELECT carr.CARRIER_CODE, carr.carrier_id, dtl.is_preferred,        dtl.lane_dtl_seq,
    equip.equipment_id ,       ser.service_level_id,       pro.protection_level_id,  MOT.MOT, comm.commodity_code_id, dtl.is_rating,
    dtl.weekly_commitment,     dtl.daily_commitment,    dtl.montly_commitment,    dtl.yearly_commitment,
    dtl.weekly_capacity,       dtl.daily_capacity,      dtl.montly_capacity,      dtl.yearly_capacity,
    NVL(dtl.package_id,0) package_id, dtl.package_name
   FROM   COMB_LANE_DTL dtl
    LEFT OUTER JOIN CARRIER_CODE carr ON dtl.carrier_id = carr.carrier_id AND carr.tc_company_id = ll_tc_company_id
    LEFT OUTER JOIN EQUIPMENT equip ON dtl.equipment_id = equip.equipment_id AND equip.tc_company_id = ll_tc_company_id
    LEFT OUTER JOIN SERVICE_LEVEL ser ON dtl.service_level_id = ser.service_level_id AND ser.tc_company_id = ll_tc_company_id
    LEFT OUTER JOIN PROTECTION_LEVEL pro ON dtl.protection_level_id = pro.protection_level_id AND pro.tc_company_id = ll_tc_company_id
    LEFT OUTER JOIN COMMODITY_CODE comm ON dtl.commodity_code_id = comm.commodity_code_id AND comm.tc_company_id = ll_tc_company_id
    LEFT OUTER JOIN MOT MOT ON MOT.mot_id = dtl.mot_id AND MOT.tc_company_id = ll_tc_company_id
   WHERE  carr.tc_company_id  = ll_tc_company_id
    AND lane_id = ll_comb_lane_id  AND
    --(mot.mot = ll_rfp_basedatamode  or (ll_rfp_basedatamode is null or ll_rfp_basedatamode = '' ) )
    (MOT.MOT IN (SELECT mot_code FROM RFPBASEMODE WHERE rfpid= vRfp_id))
    --AND dtl.is_preferred = 1
    AND is_rating = 1 AND
    (package_id IS NULL OR package_id IN (SELECT package_id FROM PACKAGE_GTT) )and effective_dt <= getdate() and expiration_dt >= getdate();
 /* cursor to fetch the lanes from temp table, used as c7 */
 CURSOR cur_lane_gtt IS SELECT laneid FROM LANE_GTT;
 /* cursor to fetch temp lanebids for a given lane id, used as c8 */
 CURSOR cur_lanebid_gtt(ll_laneid INTEGER) IS
  SELECT obcarriercodeid, lanecapacity, packageid
  FROM LANEBID_GTT
  WHERE laneid = ll_laneid;
 /* cursor to fetch the fully valid packages from temp table, used as c9 */
 CURSOR cur_packages_imported IS
  SELECT package_id
  FROM PACKAGE_GTT
  WHERE isimported = 1;
 /* contract types used as c10 */
 CURSOR cur_contract_types IS
  SELECT DISTINCT
  equipmenttype_id, servicetype_id, protectionlevel_id, MOT, commoditycode_id
  FROM LANEEQUIPMENTTYPE_GTT;
 /* cursor for lanebid customization checks, used as c11 */
 CURSOR cur_lanebid_customization (
    ll_rfp_id INTEGER,
    ll_round_num INTEGER
   )IS
  SELECT colname, isrequired
  FROM RFPCOLPROPERTIES
  WHERE datatable = 'LB' AND
   rfpid = ll_rfp_id AND
   round_num = ll_round_num ;
 /* cursor used to fetch the packages related to the lanes explicitly selected by user,
 to get the total list of packages to be imported , used as c12
 */
 CURSOR cur_rebid_lane_packages IS
  SELECT pl.packageid
  FROM PACKAGE p, PACKAGELANE pl
  WHERE  p.rfpid = pl.rfpid AND
   p.packageid = pl.packageid AND
   p.rfpid = vRfp_id AND
   p.round_num = vRound_num AND
   pl.laneid IN
   (
    SELECT laneid
    FROM LANE
    WHERE rfpid = vRfp_id AND
     round_num = vRound_num AND
     lane_type = 1
   );
BEGIN

 --trace('Started import_cm_lanes.........');
 DELETE FROM LANE_GTT;
 DELETE FROM LANEBID_GTT;
 DELETE FROM LANEEQUIPMENTTYPE_GTT;
 DELETE FROM OBCARRIERCODE_GTT;
 DELETE FROM PACKAGE_GTT;
 DELETE FROM PACKAGE_LANE_GTT;
 v_max_allowed_errors_const := 100;
 l_rec_processed      := 0;
 RetVal               := 'SUCCESS';
 v_good_lane          := 'Y';
 v_comb_lane_id       := NULL;
 v_error_code         := NULL;
 rfp_tccompanyid      := NULL;
 rfp_basedatamode     := NULL;
 rfp_exporttoratingrouting := NULL;
 rfp_status   := 3;
 rfp_allowpackages    := NULL;
 rfpbu_business_unit  := NULL;
 cl_o_facility_id     := NULL;
 cl_d_facility_id     := NULL;
 cld_equipment_code   := NULL;
 cld_service_level    := NULL;
 cld_protection_level := NULL;
 cld_mot := '';
 cld_commoditycode := NULL;
 cld_commoditycode := NULL;
 v_lanebid_lanecapacity  := NULL;
 t_laneid                := 0;
 t_tp_company_id      := NULL;
 t_obcarriercodeid    := NULL;
 l_error_yn           := 'N';
 t_package_id         := NULL;
 t_sum_lanecapacity   := 0.00;
 v_packageid          := NULL;
 v_sum_lanecapacity   := 0.00;
 v_laneattributeversion := 0;
 rfp_laneattributeversion := 0;
 v_is_fc_selected := 'N';
 v_is_minc_selected := 'N';
 v_is_rpd_selected := 'N';
 v_package_failed := 'N';
 /* get the details of the rfp */
 SELECT tccompanyid,     basedatamode  , RFPSTATUS, allowpackages, exporttoratingrouting , laneattributeversion
        INTO   rfp_tccompanyid, rfp_basedatamode  , rfp_status, rfp_allowpackages, rfp_exporttoratingrouting, rfp_laneattributeversion
        FROM   RFP
        WHERE  rfpid      = vrfp_id
        AND    round_num  = vround_num;
 v_laneattributeversion := rfp_laneattributeversion;
 /* Delete lanes and package lanes for the additional lanes */
 IF( vImport_type = 2) THEN
  /* delete packagelanes */
  DELETE FROM PACKAGELANE WHERE rfpid = vRfp_id AND laneid IN
   (SELECT lane_id FROM SCENARIO_ADDITIONAL_LANES WHERE scenario_id = vScenario_id AND rfp_id = vRfp_id)
   AND laneid NOT IN (SELECT lane_id FROM SCENARIO_ADDITIONAL_LANES WHERE scenario_id <> vScenario_id AND rfp_id = vRfp_id);
  /* delete packages */
  DELETE FROM PACKAGE WHERE rfpid = vRfp_id AND round_num = vRound_num AND packageid NOT IN
   (SELECT packageid FROM PACKAGELANE WHERE rfpid = vRfp_id);
  /* delete laneequipmenttype */
  DELETE FROM LANEEQUIPMENTTYPE WHERE rfpid = vRfp_id AND laneid IN
   (SELECT lane_id FROM SCENARIO_ADDITIONAL_LANES WHERE scenario_id = vScenario_id AND rfp_id = vRfp_id)
   AND laneid NOT IN (SELECT lane_id FROM SCENARIO_ADDITIONAL_LANES WHERE scenario_id <> vScenario_id AND rfp_id = vRfp_id);
  /* delete lanebid */
  DELETE FROM LANEBID WHERE rfpid = vRfp_id AND round_num = vRound_num AND laneid IN
   (SELECT lane_id FROM SCENARIO_ADDITIONAL_LANES WHERE scenario_id = vScenario_id AND rfp_id = vRfp_id)
   AND laneid NOT IN (SELECT lane_id FROM SCENARIO_ADDITIONAL_LANES WHERE scenario_id <> vScenario_id AND rfp_id = vRfp_id);
  /* delete laneactive */
  DELETE FROM LANE_ACTIVE WHERE rfpid = vRfp_id AND round_num = vRound_num AND laneid IN
   (SELECT lane_id FROM SCENARIO_ADDITIONAL_LANES WHERE scenario_id = vScenario_id AND rfp_id = vRfp_id)
   AND laneid NOT IN (SELECT lane_id FROM SCENARIO_ADDITIONAL_LANES WHERE scenario_id <> vScenario_id AND rfp_id = vRfp_id);
  /* delete lane entries */
  DELETE FROM LANE WHERE rfpid = vRfp_id AND round_num = vRound_num AND lane_type = 2 AND laneid IN
   (SELECT lane_id FROM SCENARIO_ADDITIONAL_LANES WHERE scenario_id = vScenario_id AND rfp_id = vRfp_id)
   AND laneid NOT IN (SELECT lane_id FROM SCENARIO_ADDITIONAL_LANES WHERE scenario_id <> vScenario_id AND rfp_id = vRfp_id);
  /* delete scenarioadjustment */
  DELETE FROM SCENARIOADJUSTMENT WHERE rfpid = vRfp_id AND scenarioid = vScenario_id AND laneid IN
   (SELECT lane_id FROM SCENARIO_ADDITIONAL_LANES WHERE scenario_id = vScenario_id AND rfp_id = vRfp_id);
  /* delete the mapping entry */
  DELETE FROM SCENARIO_ADDITIONAL_LANES WHERE rfp_id = vrfp_id AND scenario_id = vscenario_id;
  /* delete constraints */
  DELETE FROM OB200OVERRIDE_ADV WHERE rfpid=vRfp_id AND scenarioid=vScenario_id AND comments = 'SYS_ADD_LANE_LOCK';
 ELSE
  /* delete for the rebid lanes in draft and preview status for the current round */
  IF( (rfp_status = 0) OR (rfp_status = 6) ) THEN
   /* delete the packages and packagelanes which have rebid lanes */
   FOR c12 IN cur_rebid_lane_packages LOOP
    s_package_id := c12.packageid;
    DELETE FROM PACKAGELANE WHERE packageid = s_package_id;
   END LOOP;
   /* delete packagelanes */
   DELETE FROM PACKAGELANE WHERE rfpid = vrfp_id AND laneid IN
    (SELECT laneid FROM LANE WHERE rfpid = vrfp_id AND round_num = vround_num AND lane_type = 1);
   /* delete packages */
   DELETE FROM PACKAGE WHERE rfpid = vrfp_id AND round_num = vround_num AND packageid NOT IN
    (SELECT packageid FROM PACKAGELANE WHERE rfpid = vrfp_id);
   /* delete laneequipmenttype */
   DELETE FROM LANEEQUIPMENTTYPE WHERE rfpid = vrfp_id AND laneid IN
    (SELECT laneid FROM LANE WHERE rfpid = vrfp_id AND round_num = vround_num AND lane_type = 1);
   /* delete lanebid */
   DELETE FROM LANEBID WHERE rfpid = vrfp_id AND round_num = vround_num AND laneid IN
    (SELECT laneid FROM LANE WHERE rfpid = vrfp_id AND round_num = vround_num AND lane_type = 1);
   /* delete laneactive */
   DELETE FROM LANE_ACTIVE WHERE rfpid = vrfp_id AND laneid IN
    (SELECT laneid FROM LANE WHERE rfpid = vrfp_id AND round_num = vround_num AND lane_type = 1);
   /* delete lane */
   DELETE FROM LANE WHERE rfpid = vrfp_id AND round_num = vround_num AND lane_type = 1;
   /* delete rfpparticiapant */
   DELETE FROM RFPPARTICIPANT WHERE rfpid=vrfp_id AND round_num=vround_num AND is_additional = 1;
  END IF;
 END IF;

 /* if packages are allowed in the rfp, then get the current package lanes as rebid - One level nesting supported */
 IF(rfp_allowpackages = 1) THEN
  /* Get all the packages for all the selected laneid */
  INSERT INTO PACKAGE_GTT(package_id, package_name, cm_package_id)
   SELECT  DISTINCT package_id, package_name, package_id
   FROM COMB_LANE_DTL dtl
   WHERE package_id IS NOT NULL AND
    --is_preferred = 1 AND
    dtl.lane_id IN
    (
    SELECT comb_lane_id
    FROM IMPORT_CM_LANE
    WHERE
     tc_company_id = vtc_company_id
     AND   rfp_id        = vrfp_id
     AND   round_num     = vround_num
     AND   scenario_id  = vscenario_id
     AND   import_type   = vimport_type
    );
  /* For each distinct package in the above set */
  FOR c2 IN cur_package_gtt LOOP
   /* get in local variable - done for each maintainance across ports */
   s_package_id := c2.package_id;
   s_package_name := c2.package_name;
   /* get all the comb_lane_id associated with the package, and insert into package_gtt_lane if not already present */
   INSERT INTO PACKAGE_LANE_GTT(package_id, lane_id, comb_lane_id)
    SELECT  DISTINCT package_id, lane_id, lane_id
    FROM COMB_LANE_DTL dtl
    WHERE package_id = s_package_id;
   /* Check for duplicate package name */
   v_count := 0;
   SELECT COUNT(*) INTO v_count
   FROM PACKAGE
   WHERE rfpid = vRfp_id AND
    UPPER(packagename) = UPPER(s_package_name) AND
    packageid IN (
     SELECT DISTINCT p.packageid
     FROM PACKAGE p, PACKAGELANE pl, LANE l
     WHERE p.rfpid = pl.rfpid AND p.rfpid = l.rfpid AND p.rfpid = vRfp_id AND
      p.packageid = pl.packageid AND pl.laneid = l.laneid AND
      l.lane_type <> 2 AND UPPER(p.packagename) = UPPER(s_package_name)
    );
   IF(v_count IS NULL OR v_count > 0 ) THEN
    UPDATE IMPORT_CM_LANE
    SET error_code = '24'
    WHERE tc_company_id = vTc_company_id
     AND rfp_id        = vrfp_id
     AND round_num     = vround_num
     AND scenario_id  = vscenario_id
     AND import_type   = vimport_type
     AND comb_lane_id IN (
      SELECT lane_id FROM PACKAGE_LANE_GTT WHERE package_id = s_package_id
      );
          /* Verifying if any record got failed */
    IF SQL%FOUND THEN
	--INSERT INTO TRACK(PROC_NAME,PROC_CTIME,PROC_COMMENT) VALUES('1',1,'FAIL-01');
     RetVal := 'FAIL';
    END IF;
   END IF;
   /* insert into import_cm_lane the additonal lanes of the packages, with isimplicit = 1 */
   INSERT INTO IMPORT_CM_LANE(
    tc_company_id, rfp_id, round_num, scenario_id,
    comb_lane_id, import_type, isimplicit
   )
   SELECT  DISTINCT vtc_company_id, vrfp_id, vround_num, vscenario_id,
    lane_id, vimport_type, 1
   FROM  COMB_LANE_DTL
   WHERE  tc_company_id = vtc_company_id AND
    lane_id IN (SELECT comb_lane_id FROM PACKAGE_LANE_GTT)
    AND lane_id NOT IN (
     SELECT comb_lane_id FROM IMPORT_CM_LANE
     WHERE
      tc_company_id = vtc_company_id
      AND   rfp_id        = vrfp_id
      AND   round_num     = vround_num
      AND   scenario_id  = vscenario_id
      AND   import_type   = vimport_type
     );
  END LOOP;
 ELSE
  /* set the error code if packages are there for this lane */
  UPDATE IMPORT_CM_LANE
  SET error_code = 21
  WHERE tc_company_id = vtc_company_id
      AND   rfp_id        = vrfp_id
      AND   round_num     = vround_num
      AND   scenario_id  = vscenario_id
      AND   import_type   = vimport_type
      AND   comb_lane_id IN (SELECT DISTINCT lane_id FROM COMB_LANE_DTL WHERE package_id IS NOT NULL);
        /* Verifying if any record got failed */
  IF SQL%FOUND THEN
  --INSERT INTO TRACK(PROC_NAME,PROC_CTIME,PROC_COMMENT) VALUES('1',1,'FAIL-02');
   RetVal := 'FAIL';
  END IF;
 END IF;

 /* Above is package addition */
/*-------------------------the existing proc run to find the invalid lanes and put the error code -------------*/
  FOR c3 IN cur_comb_lane(vtc_company_id, vrfp_id, vround_num, vscenario_id, vimport_type ) LOOP
 INSERT INTO TRACK(PROC_NAME,PROC_CTIME,PROC_COMMENT) VALUES('1',1,c3.comb_lane_id);
 v_comb_lane_id := c3.comb_lane_id;
 v_error_code := c3.error_code;
 /* stop processing if the number of errors exceed v_max_allowed_errors_const records */
 SELECT COUNT(*) INTO v_count
 FROM IMPORT_CM_LANE
 WHERE tc_company_id = vtc_company_id
  AND rfp_id        = vrfp_id
  AND round_num     = vround_num
  AND scenario_id  = vscenario_id
  AND import_type   = vimport_type
  AND error_code IS NOT NULL;
 /* if the max no of errors exceed, fail the operation */
 IF( v_count >= v_max_allowed_errors_const ) THEN
  /* Exit the procedure directly from here */
  RAISE_APPLICATION_ERROR(-20101, 'More than 100 erors');
 END IF;

 /* Performance fix -> if the error code is already marked, continue with next. Fix as informix does not support dynamic cursor */
 IF (v_error_code IS NOT NULL) THEN
    GOTO end_loop_cur_comb_lane;
 END IF;
 IF UPPER(v_good_lane) = 'Y' THEN
  l_error_yn := 'N';
  BEGIN
   SELECT lane_id,  o_city,  o_state_prov,  o_country_code,  o_postal_code,  o_facility_id, o_facility_alias_id,  o_zone_id,
         d_city,   d_state_prov,  d_country_code,  d_postal_code,  d_facility_id, d_facility_alias_id, d_zone_id,
         CASE FREQUENCY WHEN 'D' THEN 0 WHEN 'M' THEN 3 WHEN 'W' THEN 1 WHEN 'Y' THEN 5 ELSE NULL END ,
         tc_company_id, is_rating
   INTO   cl_lane_id, cl_o_city, cl_o_state_prov, cl_o_country_code, cl_o_postal_code, cl_o_facility_id, cl_o_facility_alias_id, cl_o_zone_id,
         cl_d_city, cl_d_state_prov, cl_d_country_code, cl_d_postal_code, cl_d_facility_id, cl_d_facility_alias_id, cl_d_zone_id,
         cl_frequency,
         cl_tc_company_id, cl_is_rating
   FROM   COMB_LANE
   WHERE  tc_company_id  = vtc_company_id
   AND    lane_id        = v_comb_lane_id;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
   l_error_yn := 'Y';
   --INSERT INTO TRACK(PROC_NAME,PROC_CTIME,PROC_COMMENT) VALUES('1',1,'FAIL-03');
   RetVal := 'FAIL';
   Import_Cm_Lane_Update(vTc_company_id, vRfp_id, vRound_num, vScenario_id, vImport_type, '1', v_comb_lane_id);
          GOTO end_loop_cur_comb_lane;
  END;

  /* Get the city, state, country from the facility if facility_id is specified */
        IF cl_o_facility_id IS NOT NULL THEN
   BEGIN
    SELECT city, STATE_PROV, country_code, POSTAL_CODE
    INTO cl_o_city, cl_o_state_prov, cl_o_country_code, cl_o_postal_code
    FROM   FACILITY
    WHERE  facility_id = cl_o_facility_id;
   EXCEPTION
   WHEN NO_DATA_FOUND THEN
    l_error_yn := 'Y';
	--INSERT INTO TRACK(PROC_NAME,PROC_CTIME,PROC_COMMENT) VALUES('1',1,'FAIL-04');
    RetVal := 'FAIL';
    Import_Cm_Lane_Update(vTc_company_id, vRfp_id, vRound_num, vScenario_id, vImport_type, '4', v_comb_lane_id);
    GOTO end_loop_cur_comb_lane;
   END;
        END IF;
  /* Get the city, state, country from the facility if facility_id is specified */
        IF (cl_d_facility_id IS NOT NULL) THEN
   BEGIN
    SELECT city, STATE_PROV, country_code, POSTAL_CODE
     INTO cl_d_city, cl_d_state_prov, cl_d_country_code, cl_d_postal_code
     FROM   FACILITY
     WHERE  facility_id = cl_d_facility_id;
   EXCEPTION
    WHEN NO_DATA_FOUND THEN
     l_error_yn := 'Y';
	 --INSERT INTO TRACK(PROC_NAME,PROC_CTIME,PROC_COMMENT) VALUES('1',1,'FAIL-05');
     RetVal := 'FAIL';
     Import_Cm_Lane_Update(vTc_company_id, vRfp_id, vRound_num, vScenario_id, vImport_type, '6', v_comb_lane_id);
     GOTO end_loop_cur_comb_lane;
   END;
        END IF;

  /*****************BEGIN Check for lane customization for the current lane ************/
  l_error_no := Is_Address_Valid(vTc_company_id, vRfp_id, vRound_num, vimport_type,
      cl_o_city,
      cl_o_state_prov, cl_o_country_code, cl_o_postal_code, cl_o_facility_id,
      cl_o_facility_alias_id, cl_o_zone_id, cl_d_city, cl_d_state_prov,
      cl_d_country_code, cl_d_postal_code, cl_d_facility_id, cl_d_facility_alias_id,
      cl_d_zone_id);
  /* Set the error code if it has been set in any of the above lane customization checks */
  IF(l_error_no <> '00') THEN
  --INSERT INTO TRACK(PROC_NAME,PROC_CTIME,PROC_COMMENT) VALUES('1',1,'FAIL-06');
   RetVal := 'FAIL';
   l_error_yn := 'Y';
   /* If the rfp has the Origin Port/Origin Airport/Destination Port/ Destination Airport/ Destination customername mandatory */
   IF (l_error_no = 42 OR l_error_no = 43 OR l_error_no = 50 OR l_error_no = 51 OR l_error_no = 57) THEN
    /* Update the error_code for all the lanes */
    UPDATE IMPORT_CM_LANE
    SET error_code = l_error_no
    WHERE tc_company_id = vtc_company_id
        AND   rfp_id        = vrfp_id
        AND   round_num     = vround_num
        AND   scenario_id  = vscenario_id
        AND   import_type   = vimport_type;
    /* Exit the for loop, as all lanes will fail */
    EXIT;
   ELSE
    /* For others, set the error code for the lane and continue with next lane */
    Import_Cm_Lane_Update(vtc_company_id, vrfp_id, vround_num, vscenario_id, vimport_type, l_error_no, v_comb_lane_id);
    GOTO end_loop_cur_comb_lane;
   END IF;
  END IF;
  /* if rebid lane import check if any other lane customization is made required */
  IF(vImport_type = 1) THEN
   /* Initialize with zero */
   v_count := 0;
   SELECT COUNT(*)
   INTO v_count
   FROM RFPCOLPROPERTIES
   WHERE datatable = 'L' AND
    rfpid = vrfp_id AND
    round_num = vround_num AND
    isrequired = 1 AND
    colname NOT IN ('ORIGINFACILITYCODE','ORIGINAIRPORT','ORIGINPORT','ORIGINCITY'
      ,'ORIGINSTATEPROV','ORIGINPOSTALCODE','ORIGINCOUNTRYCODE','ORIGINZONECODE'
      ,'DESTINATIONFACILITYCODE','DESTINATIONAIRPORT' ,'DESTINATIONPORT','DESTINATIONCITY'
      ,'DESTINATIONSTATEPROV' ,'DESTINATIONPOSTALCODE','DESTINATIONCOUNTRYCODE','DESTINATIONZONECODE'
      ,'CUSTOMERNAME' ,'VOLUME','VOLUMEFREQUENCY','DISTANCEUOM'
      );
   IF(v_count <> 0) THEN
    rfp_laneattributeversion := v_laneattributeversion + 1;
   END IF;
  END IF;

  /***************** END Check for lane customization for the current lane ************/
  /* check for matching entries */
  FOR c11 IN cur_lanebid_customization(vrfp_id,vround_num) LOOP
   v_colname := c11.colname;
   v_col_required := c11.isrequired;
   IF(v_colname = 'FLATCHARGE') THEN
    IF (v_col_required = 1 ) THEN
     v_is_fc_selected := 'R';
    ELSE
     v_is_fc_selected := 'Y';
    END IF;
   ELSIF(v_colname = 'MINIMUMCHARGE') THEN
    IF (v_col_required = 1 ) THEN
     v_is_minc_selected := 'R';
    ELSE
     v_is_minc_selected := 'Y';
    END IF;
   ELSIF(v_colname = 'RATEPERDISTANCE') THEN
    IF (v_col_required = 1 ) THEN
     v_is_rpd_selected := 'R';
    ELSE
     v_is_rpd_selected := 'Y';
    END IF;
   END IF;
  END LOOP;

        /****************** BEGIN check for rating/routing *************************/
  IF (rfp_exporttoratingrouting = 1 AND vImport_type = 1) THEN
   /* Check if the comb lane exists as rebid laned in the rfp already */
   v_count := 0;
   SELECT COUNT(*) INTO v_count FROM LANE WHERE rfpid = vRfp_id AND lane_type = 1 AND comblaneid = v_comb_lane_id;
   IF( v_count > 0 ) THEN
    l_error_yn := 'Y';
	--INSERT INTO TRACK(PROC_NAME,PROC_CTIME,PROC_COMMENT) VALUES('1',1,'FAIL-07');
    RetVal := 'FAIL';
    Import_Cm_Lane_Update(vTc_company_id, vRfp_id, vRound_num, vScenario_id, vImport_type, '25', v_comb_lane_id);
    GOTO end_loop_cur_comb_lane;
   END IF;
   /* If not, then check for rating/routing compliance checking with other lanes as well*/
   IF(l_error_yn = 'N') THEN
    is_rating_routing_ok :=  Is_Rating_Routing_Compliant(vTc_company_id, vRfp_id, vRound_num, cl_o_city,
           cl_o_state_prov, cl_o_country_code, cl_o_postal_code, cl_o_facility_id,
           cl_o_facility_alias_id, cl_o_zone_id, cl_d_city, cl_d_state_prov,
           cl_d_country_code, cl_d_postal_code, cl_d_facility_id, cl_d_facility_alias_id,
           cl_d_zone_id);
    IF(is_rating_routing_ok  = 'N') THEN
	--INSERT INTO TRACK(PROC_NAME,PROC_CTIME,PROC_COMMENT) VALUES('1',1,'FAIL-08');
     RetVal := 'FAIL';
     Import_Cm_Lane_Update(vTc_company_id, vRfp_id, vRound_num, vScenario_id, vImport_type, '23', v_comb_lane_id);
     GOTO end_loop_cur_comb_lane;
    END IF;
   END IF; /* else if passed the comblanedup for rebid */
  END IF; /* end if rating/routing checks */
  /***************** END Check for lane customization for the current lane ************/
        --Not to proceed if is  rating <> 1
        /* IF cl_is_rating <> 1 THEN
   l_error_yn := 'Y';
   --INSERT INTO TRACK(PROC_NAME,PROC_CTIME,PROC_COMMENT) VALUES('1',1,'FAIL-09');
   RetVal := 'FAIL';
   import_cm_lane_update(vTc_company_id, vRfp_id, vRound_num, vScenario_id, vImport_type, '2', v_comb_lane_id);
   GOTO end_loop_cur_comb_lane;
        END IF; */

        --Check Origin Facility Alias Id in Basedata.
        IF cl_o_facility_alias_id IS NOT NULL THEN
   SELECT COUNT(*) INTO v_count
   FROM   FACILITY_ALIAS
   WHERE  facility_alias_id = cl_o_facility_alias_id
    AND    facility_id = cl_o_facility_id
    AND    tc_company_id  = vtc_company_id;
   IF v_count <= 0 THEN
    l_error_yn := 'Y';
	--INSERT INTO TRACK(PROC_NAME,PROC_CTIME,PROC_COMMENT) VALUES('1',1,'FAIL-10');
    RetVal := 'FAIL';
    Import_Cm_Lane_Update(vtc_company_id, vrfp_id, vround_num, vscenario_id, vimport_type, '5', v_comb_lane_id);
    GOTO end_loop_cur_comb_lane;
   END IF;
        END IF;
        --Check Destination Facility Alias Id in Basedata.
        IF cl_d_facility_alias_id IS NOT NULL THEN
    SELECT COUNT(*) INTO v_count
    FROM   FACILITY_ALIAS
    WHERE  facility_alias_id = cl_d_facility_alias_id
    AND    facility_id       = cl_d_facility_id
    AND    tc_company_id     = vtc_company_id;
    IF v_count <= 0 THEN
   l_error_yn := 'Y';
   --RetVal := 'FAIL';
   INSERT INTO TRACK(PROC_NAME,PROC_CTIME,PROC_COMMENT) VALUES('1',1,'FAIL-11');
   Import_Cm_Lane_Update(vtc_company_id, vrfp_id, vround_num, vscenario_id, vimport_type, '7', v_comb_lane_id);
   GOTO end_loop_cur_comb_lane;
    END IF;
        END IF;

        --Check Origin Zone Id in basedata.
        IF cl_o_zone_id IS NOT NULL THEN
    SELECT COUNT(*) INTO v_count
    FROM   ZONE
    WHERE  tc_company_id = vtc_company_id
    AND    zone_id       = cl_o_zone_id;
    IF v_count <= 0 THEN
   l_error_yn := 'Y';
   --INSERT INTO TRACK(PROC_NAME,PROC_CTIME,PROC_COMMENT) VALUES('1',1,'FAIL-12');
   RetVal := 'FAIL';
   Import_Cm_Lane_Update(vtc_company_id, vrfp_id, vround_num, vscenario_id, vimport_type, '8', v_comb_lane_id);
   GOTO end_loop_cur_comb_lane;
    ELSE
   SELECT zone_name INTO cl_o_zone_code FROM ZONE WHERE zone_id = cl_o_zone_id;
    END IF;
        END IF;
  /* Check destination Zone Id in basedata. */
        IF cl_d_zone_id IS NOT NULL THEN
    SELECT COUNT(*) INTO v_count
    FROM   ZONE
    WHERE  tc_company_id = vtc_company_id
    AND    zone_id       = cl_o_zone_id;
    IF v_count <= 0 THEN
   l_error_yn := 'Y';
   --INSERT INTO TRACK(PROC_NAME,PROC_CTIME,PROC_COMMENT) VALUES('1',1,'FAIL-13');
   RetVal := 'FAIL';
   Import_Cm_Lane_Update(vTc_company_id, vRfp_id, vRound_num, vScenario_id, vImport_type, '22', v_comb_lane_id);
   GOTO end_loop_cur_comb_lane;
    ELSE
   SELECT zone_name INTO cl_d_zone_code FROM ZONE WHERE zone_id = cl_d_zone_id;
    END IF;
        END IF;
  v_lanevolume := 0;
  IF vhistoricalaward = 1  THEN 
  /* Loop through the comb lane details */
  FOR c5 IN cur_lane_details(vTc_company_id, v_comb_lane_id, rfp_basedatamode) LOOP
   /* Get cursor variable in local variable to minimize porting issues */
   cld_obcarriercodeid := c5.CARRIER_CODE;
   cld_carrierid := c5.carrier_id;
   --cld_historicalaward := c5.is_preferred;
   cld_lane_dtl_seq := c5.lane_dtl_seq;
   cld_equipment_code := c5.equipment_id;
   cld_service_level :=  c5.service_level_id;
   cld_protection_level := c5.protection_level_id;
   cld_mot := c5.MOT;
   cld_commoditycode := c5.commodity_code_id;
   cld_is_rating := c5.is_rating;
   cld_weekly_commitment := c5.weekly_commitment;
   cld_daily_commitment := c5.daily_commitment;
   cld_montly_commitment := c5.montly_commitment;
   cld_yearly_commitment := c5.yearly_commitment;
   cld_weekly_capacity := c5.weekly_capacity;
   cld_daily_capacity :=  c5.daily_capacity;
   cld_montly_capacity :=  c5.montly_capacity;
   cld_yearly_capacity := c5.yearly_capacity;
   cld_package_id := c5.package_id;
   cld_package_name := c5.package_name;
   --trace('v_comb_lane_id=' || v_comb_lane_id || ' seq= '|| cld_lane_dtl_seq);
   /* get the cm-carrier code */
    SELECT tp_company_id INTO t_tp_company_id
    FROM   CARRIER_CODE
    WHERE  tc_company_id  = vtc_company_id
    AND    CARRIER_CODE   = cld_obcarriercodeid
    AND carrier_id = cld_carrierid;
   /* check if obcarriercode exists */
   BEGIN
    SELECT obcarriercodeid INTO t_obcarriercodeid
    FROM   OBCARRIERCODE
    WHERE  tccompanyid = vtc_company_id
    AND    carriercode = cld_obcarriercodeid
    AND carrier_id = cld_carrierid;
   EXCEPTION
    WHEN NO_DATA_FOUND THEN
     t_obcarriercodeid := NULL;
   END;
   /* if not in permanent table  */
    IF t_obcarriercodeid IS NULL THEN
    /* check if obcarriercode exists in temp table */
    BEGIN
     SELECT obcarriercodeid INTO t_obcarriercodeid
     FROM   OBCARRIERCODE_GTT
     WHERE  tccompanyid = vtc_company_id
     AND    carriercode = cld_obcarriercodeid
     AND carrier_id = cld_carrierid;
    EXCEPTION
     WHEN NO_DATA_FOUND THEN
      t_obcarriercodeid := NULL;
    END;
    /* if not even in temp table then create the entry */
    IF t_obcarriercodeid IS NULL THEN
     /* create entry into the temp table */
     SELECT obcarriercode_sequence.NEXTVAL INTO t_obcarriercodeid FROM dual;
     INSERT INTO OBCARRIERCODE_GTT VALUES (t_obcarriercodeid, t_tp_company_id,vtc_company_id, cld_obcarriercodeid,cld_carrierid );
    END IF;
    END IF;
   /* Initialize the rates to null */
   rlds_rate := NULL;
   v_flatcharge := NULL;
   v_minimumcharge := NULL;
   v_rateperdistance := NULL;
    /* Get flat rate from rating_lane_dtl_seq for corrousponding comb Lane detail table record. */
    IF (v_is_fc_selected = 'Y' OR v_is_fc_selected = 'R') THEN
    BEGIN
     SELECT rate   INTO   v_flatcharge
     FROM   RATING_LANE_DTL_RATE
     WHERE  tc_company_id    = vtc_company_id
      AND    lane_id          = v_comb_lane_id
      AND rating_lane_dtl_seq = cld_lane_dtl_seq
      AND RATE_CALC_METHOD = 'FC';
     rlds_rate := v_flatcharge;
    EXCEPTION
     WHEN NO_DATA_FOUND THEN
      IF ( v_is_fc_selected = 'R') THEN
      l_error_yn := 'Y';
	  --INSERT INTO TRACK(PROC_NAME,PROC_CTIME,PROC_COMMENT) VALUES('1',1,'FAIL-14');
      RetVal := 'FAIL';
      Import_Cm_Lane_Update(vtc_company_id, vrfp_id, vround_num, vscenario_id, vimport_type, '18', v_comb_lane_id);
      GOTO end_loop_cur_comb_lane;
     END IF;
    END;
    END IF;
    /* Get mincharge rate from rating_lane_dtl_seq for corrousponding comb Lane detail table record. */
    IF( v_is_minc_selected = 'Y' OR v_is_minc_selected = 'R' ) THEN
    BEGIN
     SELECT rate   INTO   v_minimumcharge
     FROM   RATING_LANE_DTL_RATE
     WHERE  tc_company_id    = vtc_company_id
      AND    lane_id          = v_comb_lane_id
      AND rating_lane_dtl_seq = cld_lane_dtl_seq
      AND RATE_CALC_METHOD = 'FC';
    EXCEPTION
     WHEN NO_DATA_FOUND THEN
       /* Throw error if it is a required field */
       IF ( v_is_minc_selected = 'R') THEN
       l_error_yn := 'Y';
	   --INSERT INTO TRACK(PROC_NAME,PROC_CTIME,PROC_COMMENT) VALUES('1',1,'FAIL-15');
       RetVal := 'FAIL';
       Import_Cm_Lane_Update(vtc_company_id, vrfp_id, vround_num, vscenario_id, vimport_type, '17', v_comb_lane_id);
       GOTO end_loop_cur_comb_lane;
       END IF;
    END;
    END IF;
    /* Get RatePerDistance from rating_lane_dtl_seq for corrousponding comb Lane detail table record. */
    IF (v_is_rpd_selected = 'Y' OR v_is_rpd_selected = 'R' ) THEN
    BEGIN
     SELECT rate    INTO   v_rateperdistance
     FROM   RATING_LANE_DTL_RATE
     WHERE  tc_company_id    = vtc_company_id
      AND    lane_id          = v_comb_lane_id
      AND rating_lane_dtl_seq = cld_lane_dtl_seq
      AND RATE_CALC_METHOD = 'RPD';
     rlds_rate := v_rateperdistance;
    EXCEPTION
     WHEN NO_DATA_FOUND THEN
       /* Throw error if this is a required field */
       IF (v_is_rpd_selected = 'R') THEN
       l_error_yn := 'Y';
	   --INSERT INTO TRACK(PROC_NAME,PROC_CTIME,PROC_COMMENT) VALUES('1',1,'FAIL-16');
       RetVal := 'FAIL';
       Import_Cm_Lane_Update(vtc_company_id, vrfp_id, vround_num, vscenario_id, vimport_type, '19', v_comb_lane_id);
       GOTO end_loop_cur_comb_lane;
       END IF;
    END;
    END IF;
   v_lanebid_lanecapacity := NULL;
   if cld_weekly_commitment is not null AND cld_weekly_commitment > 0 then
    v_lanebid_lanecapacity := cld_weekly_commitment;
    v_lanevolume := v_lanevolume + cld_weekly_commitment;
   elsif cld_daily_commitment is  not null AND cld_daily_commitment > 0 then
    v_lanebid_lanecapacity := cld_daily_commitment*7;
    v_lanevolume := v_lanevolume + cld_daily_commitment*7;
   elsif cld_montly_commitment is not null AND cld_montly_commitment > 0 then
    v_lanebid_lanecapacity := cld_montly_commitment*7/30;
    v_lanevolume := v_lanevolume + cld_montly_commitment*7/30;
   elsif cld_yearly_commitment is not null AND cld_yearly_commitment > 0 then
    v_lanebid_lanecapacity := cld_yearly_commitment/365*7;
    v_lanevolume := v_lanevolume + cld_yearly_commitment/365*7;
   end if;
   if cld_weekly_capacity is not null AND cld_weekly_capacity > 0 then
    v_lanebid_surgecapacity := cld_weekly_capacity;
   elsif cld_daily_capacity is  not null AND cld_daily_capacity > 0 then
    v_lanebid_surgecapacity := cld_daily_capacity*7;
   elsif cld_montly_capacity is not null AND cld_montly_capacity > 0 then
    v_lanebid_surgecapacity := cld_montly_capacity*7/30;
   elsif cld_yearly_capacity is not null AND cld_yearly_capacity > 0 then
    v_lanebid_surgecapacity := cld_yearly_capacity/365*7;
   else
    v_lanebid_surgecapacity := v_lanebid_lanecapacity;
   end if;

    --Insert into temporary laneequipmenttype_gtt (valid equipment type, Service and Protection Level).
   l_count := 0;
   l_laneequipmenttype_volume := 0;
   l_laneequipmenttypeid := NULL;
   BEGIN
    SELECT COUNT(*), laneequipmenttypeid, volume INTO l_count, l_laneequipmenttypeid, l_laneequipmenttype_volume
    FROM  LANEEQUIPMENTTYPE_GTT
    WHERE comb_lane_id     = v_comb_lane_id
    AND  (equipmenttype_id = cld_equipment_code OR (equipmenttype_id IS NULL  AND cld_equipment_code IS NULL ) )
    AND  (servicetype_id     = cld_service_level  OR (servicetype_id IS NULL AND cld_service_level IS NULL) )
    AND  (protectionlevel_id = cld_protection_level OR (protectionlevel_id IS NULL AND cld_protection_level IS NULL))
    AND  (commoditycode_id = cld_commoditycode OR (commoditycode_id IS NULL AND cld_commoditycode IS NULL))
    AND (MOT = cld_mot OR (MOT IS NULL AND cld_mot IS NULL))
    GROUP BY laneequipmenttypeid, volume;
   EXCEPTION
    WHEN NO_DATA_FOUND THEN
     l_count := 0;
   END;
   /* if the entry not already present */
    IF (l_count IS NULL OR l_count = 0) THEN
    /* Get the new id */
    SELECT laneequipmenttype_sequence.NEXTVAL INTO l_laneequipmenttypeid FROM dual;
    /* create a new record */
    INSERT INTO LANEEQUIPMENTTYPE_GTT (
     laneequipmenttypeid,   laneid,  comb_lane_id, rfpid,
     volume,  FREQUENCY,   createddttm,    lastupdateddttm,
     equipmenttype_id,  servicetype_id,  protectionlevel_id, MOT, commoditycode_id, round_num
    ) VALUES(
     l_laneequipmenttypeid, v_comb_lane_id, v_comb_lane_id, vrfp_id,
     v_lanebid_lanecapacity, 1, SYSDATE, SYSDATE,
     cld_equipment_code, cld_service_level, cld_protection_level, cld_mot, cld_commoditycode, vround_num
    );
    ELSE
    /* Increment the existing volume with the current bid commitment */
    UPDATE LANEEQUIPMENTTYPE_GTT
    SET volume = l_laneequipmenttype_volume + v_lanebid_lanecapacity
    WHERE laneequipmenttypeid = l_laneequipmenttypeid;
    END IF;
   /* Bring in only winning bids, not the losing ones */
   IF (v_lanebid_lanecapacity IS NOT NULL AND v_lanebid_lanecapacity > 0 ) THEN
     IF (vimport_type = 2 OR vhistoricalaward = 1) THEN
     INSERT INTO LANEBID_GTT(
      laneid, comb_lane_id, rfpid, round_num,
      obcarriercodeid, laneequipmenttypeid, historicalaward, tpcompanyid,
      lanecapacity, surgecapacity, equipmenttype_id, servicetype_id,
      protectionlevel_id, MOT, commoditycode_id, rateperdistance, minimumcharge,flatcharge,
      costperload, historicalvolume, isawarded, createddttm,
      createduid, lastupdateddttm, lastupdateduid, packageid,
      status, erattributeversion, enable_flag, override_id
     )VALUES (
      v_comb_lane_id, v_comb_lane_id, vRfp_id, vRound_num,
      t_obcarriercodeid, l_laneequipmenttypeid,  1, t_tp_company_id,
      v_lanebid_lanecapacity, v_lanebid_surgecapacity,cld_equipment_code,cld_service_level,
      cld_protection_level, cld_mot, cld_commoditycode, v_rateperdistance, v_minimumcharge, v_flatcharge,
      rlds_rate,  v_lanebid_lanecapacity, 0,  SYSDATE,
      NULL, SYSDATE, NULL, cld_package_id,
      'Bid On' , -1, 1, override_id_seq.nextval);
     END IF;
   END IF;
  <<end_loop_cur_lane_details>>
   NULL;
  END LOOP;  -- cur_lane_details
  
  END IF; -- vhistoricalaward
  

  /* Insert the record in the temp table */
  IF l_error_yn = 'N' THEN
   INSERT INTO LANE_GTT(
    laneid, rfpid,   comblaneid,     origincity,
    originstateprov, origincountrycode, originpostalcode, originfacilitycode,
    originzonecode,  destinationcity, destinationstateprov, destinationcountrycode,
    destinationpostalcode,  destinationfacilitycode, destinationzonecode, volumefrequency,
    round_num,  createddttm, createduid, lastupdateddttm,
    lastupdateduid, lane_type, volume, business_unit_id
   )VALUES (
    v_comb_lane_id, vrfp_id, v_comb_lane_id, cl_o_city,
    cl_o_state_prov, cl_o_country_code, cl_o_postal_code, cl_o_facility_alias_id,
    cl_o_zone_code,  cl_d_city, cl_d_state_prov, cl_d_country_code,
    cl_d_postal_code, cl_d_facility_alias_id, cl_d_zone_code, 1,
    vround_num, SYSDATE, -1, SYSDATE,
    -1, vimport_type, v_lanevolume, vtc_company_id
   );
  END IF;
 END IF;     --v_good_lane = 'Y'
 <<end_loop_cur_comb_lane>>
 NULL;
  END LOOP;    -- end of  cur_comb_lane loop

/* Begin -> Before the lane is inserted, delete the incomplete package import lanes */
 /* for each package */
 FOR c6 IN cur_package_gtt LOOP
  s_package_id := c6.package_id;
  /* get the count of lanes per package that have failed */
  SELECT COUNT(*)
  INTO v_package_lane_failed_count
  FROM IMPORT_CM_LANE cm, PACKAGE_LANE_GTT pl
  WHERE cm.comb_lane_id = pl.comb_lane_id
   AND pl.package_id = s_package_id
   AND cm.error_code IS NOT NULL;
  v_isimported := 1;
  /* if some package lane is failing */
  IF(v_package_lane_failed_count > 0) THEN
   v_isimported := 0;
  END IF;
  /* update all package entries */
  UPDATE PACKAGE_GTT
  SET isimported = v_isimported
  WHERE package_id = s_package_id;
  /* update all package lane entries to mark as deleted */
  UPDATE PACKAGE_LANE_GTT
  SET isimported = v_isimported
  WHERE package_id = s_package_id;
  /* delete lane entries from lane_gtt */
  IF(v_package_lane_failed_count > 0) THEN
   /* set the package failure flag */
   v_package_failed := 'Y';
   /* update the other dependent package lane with the reason of dependency */
   UPDATE IMPORT_CM_LANE
   SET error_code = 20
   WHERE
    tc_company_id = vtc_company_id
    AND   rfp_id        = vrfp_id
    AND   round_num     = vround_num
    AND   scenario_id  = vscenario_id
    AND   import_type   = vimport_type
    AND   error_code IS NULL
    AND   isimplicit IS NULL
    AND   comb_lane_id IN
     (SELECT lane_id FROM PACKAGE_LANE_GTT WHERE isimported = 0 AND package_id = s_package_id);
   /* Set isimplicit = 2, for explicitly selected package lane */
   UPDATE IMPORT_CM_LANE
   SET isimplicit = 2
   WHERE
    tc_company_id = vTc_company_id
    AND   rfp_id = vrfp_id
    AND   round_num = vround_num
    AND   scenario_id = vscenario_id
    AND   import_type = vimport_type
    AND isimplicit IS NULL
    AND comb_lane_id IN (SELECT comb_lane_id FROM PACKAGE_LANE_GTT);
   /* delete all the implicit import_cm_lane entries that have not failed */
   DELETE FROM IMPORT_CM_LANE
   WHERE
    tc_company_id = vtc_company_id
    AND   rfp_id = vrfp_id
    AND   round_num = vround_num
    AND   scenario_id = vscenario_id
    AND   import_type = vimport_type
    AND   isimplicit = 1
    AND error_code IS NULL;
  END IF;
  /* check if the package is already present for the rfp */
  BEGIN
   SELECT COUNT(*), packageid INTO v_count , v_package_id_seq
   FROM PACKAGE
   WHERE cmpackageid = s_package_id AND rfpid = vRfp_id AND round_num =vRound_num
   GROUP BY packageid;
  EXCEPTION
   WHEN NO_DATA_FOUND THEN
    v_count := 0;
  END;
  /* if the package is successful, get the new package id, this is done right here for performance reasons */
  IF (v_isimported = 1) THEN
   /* if the package is not already present, create one */
   IF(v_count IS NULL OR v_count = 0 ) THEN
    SELECT package_sequence.NEXTVAL INTO v_package_id_seq FROM dual;
   END IF;
   /* update the package_id accordingly */
   UPDATE PACKAGE_GTT SET package_id = v_package_id_seq WHERE cm_package_id = s_package_id;
   /* Replace the package_id with the new package id */
   UPDATE PACKAGE_LANE_GTT SET package_id = v_package_id_seq WHERE package_id = s_package_id;
   /* update the packageid for lanebid_gtt */
   UPDATE LANEBID_GTT SET packageid = v_package_id_seq WHERE packageid = s_package_id;
  END IF;
 END LOOP;

 /* Exit the procedure directly from here as the package failed */
 IF (v_package_failed = 'Y') THEN
  COMMIT;
  RAISE_APPLICATION_ERROR(-20101, 'package failed');
 END IF;
 /* delete all the implicit import_cm_lane entries to prevent the error messages from going to user */
 DELETE FROM IMPORT_CM_LANE
 WHERE
  tc_company_id = vtc_company_id
  AND   rfp_id        = vrfp_id
  AND   round_num     = vround_num
  AND   scenario_id  = vscenario_id
  AND   import_type   = vimport_type
  AND   isimplicit = 1;
 /* End -> Before the lane is inserted, delete the incomplete package import lanes */
 /*-----------------Insert into Temporary tables into Permanent tables.------------------*/
 v_count := 0;
 SELECT COUNT(*) INTO v_count FROM LANE_GTT WHERE 1=1;
 IF v_count > 0 THEN
  /* create the permanent table entries*/
  INSERT INTO OBCARRIERCODE SELECT * FROM OBCARRIERCODE_GTT;

  FOR c7 IN cur_lane_gtt LOOP
   v_comb_lane_id :=  c7.laneid;
   --get the existing lane id for a given comb lane id and round_num(as same comblaneid can be a rebid/additional)
   BEGIN
    SELECT laneid INTO t_laneid
    FROM LANE
     WHERE rfpid = vrfp_id AND
      comblaneid =  v_comb_lane_id AND
      round_num = vround_num;
   EXCEPTION
    WHEN NO_DATA_FOUND THEN
     /* get max laneid from lane and add 1. */
     SELECT NVL(MAX(laneid),0)+1 INTO t_laneid FROM LANE WHERE rfpid = vrfp_id;
   END;
   --update laneid into temporary (t_laneid) table.
   UPDATE LANE_GTT
   SET    laneid = t_laneid
   WHERE rfpid = vrfp_id
    AND   comblaneid = v_comb_lane_id
    AND   round_num = vround_num;
   /* Delete old records from lane,lanebid,laneequipmenttype tables if exist. */
   BEGIN
    SELECT COUNT(*), laneid
    INTO v_count,v_laneid
    FROM LANE
    WHERE comblaneid = v_comb_lane_id AND round_num = vround_num AND rfpid = vrfp_id AND lane_type = 2
    GROUP BY laneid;
   EXCEPTION
    WHEN NO_DATA_FOUND THEN
     v_count := 0;
   END;
   IF (v_count > 0) THEN
    /* Quick fix -> delete the entries. The same entries will be re-created again */
    DELETE FROM LANE    WHERE laneid = v_laneid AND round_num = vround_num AND rfpid = vrfp_id;
    DELETE FROM LANEBID WHERE laneid = v_laneid AND round_num = vround_num AND rfpid = vrfp_id;
    DELETE FROM LANEEQUIPMENTTYPE WHERE laneid = v_laneid AND round_num = vround_num AND rfpid = vrfp_id;
   END IF;

   --insert into actual table (lane) from  temp. table lane_gtt
   INSERT INTO LANE (
    laneid,rfpid,originshipsite,originshipsitecode,originport,origincity,originstateprov,origincountrycode,originpostalcode,
    destinationshipsite, destinationport,destinationshipsitecode,destinationcity,destinationstateprov,destinationcountrycode,
    destinationpostalcode,customername,COMMODITYCODE,commoditycodedesc,harmonizedcode,RATETYPE,isroundtrip,averageweight,
    volumefrequency,volume,weightbreak1,weightbreak2,weightbreak3,weightbreak4,weightbreak5,weightbreak6,weightbreak7,
    weightbreak8,weightbreak9,WEIGHTUOM,weightbreakuom,mileage,DISTANCEUOM,lclcubicspace,lclcubicspaceuom,PICKUPTYPE,
    SERVICECRITERIA,FREIGHTCLASS,ROUTINGTYPE,isconference,transittime,transittimeuom,isoversized,oversizednote,ishazardous,
    hazardousnote,isperishable,perishablenote,other1,other2,other3,iscancelled,isdirty,createduid,createddttm,lastupdateduid,
   lastupdateddttm,custtext1,custtext2,custtext3,custtext4,custtext5,custtext6,custtext7,custtext8,custtext9,custtext10,custint1,
   custint2,custint3,custint4,custint5,custint6,custint7,custint8,custint9,custint10,custdouble1,custdouble2,custdouble3,custdouble4,
   custdouble5,custdouble6,custdouble7,custdouble8,custdouble9,custdouble10,originfacilitycode,originairport,destinationfacilitycode,
   destinationairport,historicalcost,numstops,numcontainers,laneattributeversion,round_num,shipmentratedcost,shipmentvolume,
   shipmentvolumeerror,WEIGHTBREAKRATETYPE,comblaneid,originzonecode,destinationzonecode,lane_type, business_unit_id
          )
    SELECT
    laneid,rfpid,originshipsite,originshipsitecode,originport,origincity,originstateprov,origincountrycode,originpostalcode,
    destinationshipsite, destinationport,destinationshipsitecode,destinationcity,destinationstateprov,destinationcountrycode,
    destinationpostalcode,customername,COMMODITYCODE,commoditycodedesc,harmonizedcode,RATETYPE,isroundtrip,averageweight,
    volumefrequency,volume,weightbreak1,weightbreak2,weightbreak3,weightbreak4,weightbreak5,weightbreak6,weightbreak7,
    weightbreak8,weightbreak9,WEIGHTUOM,weightbreakuom,mileage,DISTANCEUOM,lclcubicspace,lclcubicspaceuom,PICKUPTYPE,
    SERVICECRITERIA,FREIGHTCLASS,ROUTINGTYPE,isconference,transittime,transittimeuom,isoversized,oversizednote,ishazardous,
    hazardousnote,isperishable,perishablenote,other1,other2,other3,iscancelled,isdirty,createduid,createddttm,lastupdateduid,
   lastupdateddttm,custtext1,custtext2,custtext3,custtext4,custtext5,custtext6,custtext7,custtext8,custtext9,custtext10,custint1,
   custint2,custint3,custint4,custint5,custint6,custint7,custint8,custint9,custint10,custdouble1,custdouble2,custdouble3,custdouble4,
   custdouble5,custdouble6,custdouble7,custdouble8,custdouble9,custdouble10,originfacilitycode,originairport,destinationfacilitycode,
   destinationairport,historicalcost,numstops,numcontainers,laneattributeversion,round_num,shipmentratedcost,shipmentvolume,
   shipmentvolumeerror,WEIGHTBREAKRATETYPE,comblaneid,originzonecode,destinationzonecode,lane_type, business_unit_id
    FROM LANE_GTT
     WHERE rfpid     = vrfp_id
     AND   laneid    = t_laneid
     AND   round_num = vround_num
     AND   comblaneid = v_comb_lane_id;
   /* update the laneid for packages with the new laneid */
   UPDATE PACKAGE_LANE_GTT
   SET lane_id = t_laneid
   WHERE comb_lane_id = v_comb_lane_id;
   --update laneid into temporary (lanebid_gtt) table.
   v_count := 0;
   SELECT COUNT(*) INTO v_count FROM LANEBID_GTT
   WHERE rfpid = vrfp_id
   AND   comb_lane_id = v_comb_lane_id
   AND   round_num = vround_num;
   IF v_count > 0 THEN
    UPDATE LANEBID_GTT
    SET    laneid = t_laneid
    WHERE rfpid     = vrfp_id
    AND   comb_lane_id    = v_comb_lane_id
    AND   round_num = vround_num;

    SELECT currencycode INTO v_currencycode FROM RFP WHERE rfpid = vrfp_id and round_num=1;
    --insert into actual table (lanebid) from lanebid_gtt.
    INSERT INTO LANEBID(laneequipmenttypeid, tpcompanyid, rfpid, laneid, historicalaward, historicalvolume, laneauctionstrategy, strategytype, strategybaseline, strategypercentage, lowestbid, transittime, transittimeuom, lanecapacity, surgecapacity, minimumcharge, flatcharge, baserate, inclusiverate, weightbreakcharge1, weightbreakcharge2, weightbreakcharge3, weightbreakcharge4, weightbreakcharge5, weightbreakcharge6, weightbreakcharge7, weightbreakcharge8, weightbreakcharge9, weightcapacity, WEIGHTUOM, rateperuom, flightfrequency, sailingallocation, SAILINGFREQUENCY, CHARGEBASIS, comments, isawarded, awardamount, awardispercentage, createduid, createddttm, lastupdateduid, lastupdateddttm, obcarriercodeid, equipmenttype_id, servicetype_id, protectionlevel_id,MOT, commoditycode_id, costperload, ratepersize, rateperdistance, packageid, custtext1, custtext2, custtext3, custtext4, custtext5, custtext6, custtext7, custtext8, custtext9, custtext10, custint1, custint2, custint3, custint4, custint5, custint6, custint7, custint8, custint9, custint10, custdouble1, custdouble2, custdouble3, custdouble4, custdouble5, custdouble6, custdouble7, custdouble8, custdouble9, custdouble10, round_num, ratediscount, discountbidcost, erattributeversion, status, override_id, enable_flag, currencycode)
     SELECT laneequipmenttypeid, tpcompanyid, rfpid, laneid, historicalaward, historicalvolume, laneauctionstrategy, strategytype, strategybaseline, strategypercentage, lowestbid, transittime, transittimeuom, lanecapacity, surgecapacity, minimumcharge, flatcharge, baserate, inclusiverate, weightbreakcharge1, weightbreakcharge2, weightbreakcharge3, weightbreakcharge4, weightbreakcharge5, weightbreakcharge6, weightbreakcharge7, weightbreakcharge8, weightbreakcharge9, weightcapacity, WEIGHTUOM, rateperuom, flightfrequency, sailingallocation, SAILINGFREQUENCY, CHARGEBASIS, comments, isawarded, awardamount, awardispercentage, createduid, createddttm, lastupdateduid, lastupdateddttm, obcarriercodeid, equipmenttype_id, servicetype_id, protectionlevel_id, MOT, commoditycode_id, costperload, ratepersize, rateperdistance, packageid, custtext1, custtext2, custtext3, custtext4, custtext5, custtext6, custtext7, custtext8, custtext9, custtext10, custint1, custint2, custint3, custint4, custint5, custint6, custint7, custint8, custint9, custint10, custdouble1, custdouble2, custdouble3, custdouble4, custdouble5, custdouble6, custdouble7, custdouble8, custdouble9, custdouble10, round_num, ratediscount, discountbidcost, erattributeversion, status, override_id, enable_flag, NVL(currencycode,v_currencycode)
     FROM LANEBID_GTT
     WHERE rfpid     = vrfp_id
      AND   comb_lane_id    = v_comb_lane_id
      AND   round_num = vround_num;
          END IF; -- if v_count > 0
   --update laneid into temporary (lanebid_gtt) table.
         v_count := 0;
   SELECT COUNT(*) INTO v_count FROM LANEEQUIPMENTTYPE_GTT
   WHERE rfpid     = vrfp_id
   AND   comb_lane_id    = v_comb_lane_id
   AND   round_num = vround_num;
   IF v_count > 0 THEN
    UPDATE LANEEQUIPMENTTYPE_GTT
    SET    laneid = t_laneid
    WHERE rfpid     = vrfp_id
    AND   comb_lane_id    = v_comb_lane_id
    AND   round_num = vround_num;
   END IF;

   -- insert into scenario adjustment when importing the additional lanes with historical award
   IF vimport_type = 2 THEN

    INSERT INTO SCENARIOADJUSTMENT (
     laneequipmenttypeid , obcarriercodeid, scenarioid, rfpid,
     laneid, equipmenttype_id, servicetype_id, protectionlevel_id,MOT, commoditycode_id,
     inboundfacilitycode, outboundfacilitycode, costperload, globaladjpct,
     facilityadjpct, laneadjpct, carrieradjpct,  packageid,
     historicalcost,historicalaward, needsautoaward, lanevolume, capacity,
     incumbent, createddttm , bid_round_num, laneincluded,autoawardamount, packageawardamount,
     numdays )
    SELECT lb.laneequipmenttypeid , lb.obcarriercodeid,  vscenario_id, vrfp_id,
     lb.laneid, lb.equipmenttype_id, lb.servicetype_id, lb.protectionlevel_id,lb.MOT, lb.commoditycode_id,
     l.destinationfacilitycode, l.originfacilitycode, lb.costperload, 0,
     0, 0, 0, lb.packageid,
     0, lb.historicalaward,1, l.volume, lb.lanecapacity,
     1, SYSDATE, vround_num,0,CASE lb.packageid WHEN 0 THEN lb.historicalvolume ELSE NULL END,
     CASE lb.packageid WHEN 0 THEN NULL ELSE lb.historicalvolume END,
     DECODE(l.volumefrequency,
       0, 1,
       1, 7,
       2, 14,
       3, 365/12,
       4, 365/2,
       5, 365,
       NULL
       ) freq
    FROM
     LANE_GTT l,
     LANEBID_GTT lb
    WHERE l.rfpid = lb.rfpid AND
     l.laneid = lb.laneid AND
     l.rfpid = vrfp_id AND
     l.laneid = t_laneid AND NOT EXISTS
      (SELECT * FROM SCENARIOADJUSTMENT
       WHERE rfpid=vrfp_id AND laneid=t_laneid AND scenarioid = vscenario_id);
    INSERT INTO SCENARIO_ADDITIONAL_LANES VALUES(vrfp_id, t_laneid, vscenario_id);
    INSERT INTO TRACK(PROC_NAME,PROC_CTIME,PROC_COMMENT) VALUES('1',1,'172');
    FOR c8 IN cur_lanebid_gtt(t_laneid) LOOP
     INSERT INTO TRACK(PROC_NAME,PROC_CTIME,PROC_COMMENT) VALUES('1',1,'173');
     v_obcarriercodeid := c8.obcarriercodeid;
     v_lanecapacity := c8.lanecapacity;
     v_package_id := c8.packageid;

    END LOOP; -- for cur_lanebid_gtt
   END IF;

  END LOOP; --for t_lane

  INSERT INTO LANEEQUIPMENTTYPE(
   laneequipmenttypeid, laneid, rfpid, volume,
   FREQUENCY, createduid, createddttm, lastupdateduid,
   lastupdateddttm, equipmenttype_id, servicetype_id, protectionlevel_id,MOT, commoditycode_id,
   round_num)
  SELECT
   laneequipmenttypeid, laneid, rfpid, volume,
   FREQUENCY, createduid, createddttm, lastupdateduid,
   lastupdateddttm, equipmenttype_id, servicetype_id, protectionlevel_id,MOT, commoditycode_id,
   round_num
  FROM LANEEQUIPMENTTYPE_GTT;
  INSERT INTO LANE_ACTIVE
  SELECT rfpid, laneid, round_num, 1 FROM LANE_GTT
   WHERE  laneid NOT IN (
    SELECT laneid FROM LANE_ACTIVE WHERE rfpid = vrfp_id)
         AND rfpid = vrfp_id ;
  IF (vimport_type = 2 OR vhistoricalaward = 1) THEN
   INSERT INTO RFPPARTICIPANT (
    obcarriercodeid, tpcompanyid, rfpid, createduid,
    incumbent, round_num, is_additional
    )
   SELECT  DISTINCT lb.obcarriercodeid, lb.tpcompanyid, vrfp_id, -1,
    1, vround_num, 1
    FROM LANEBID_GTT lb
    WHERE  obcarriercodeid NOT IN (
     SELECT obcarriercodeid FROM RFPPARTICIPANT
      WHERE rfpid = lb.rfpid AND round_num = vround_num AND is_additional=1);
  END IF;
  IF vimport_type = 2 THEN
   Update_Rfpresponsestatus(vrfp_id, vround_num);
  END IF;
 END IF; --v_count of t_laneid

 /* insert packages and package lanes */
 FOR c9 IN cur_packages_imported LOOP
  v_package_id_seq := c9.package_id;
  /* get the total volume of the lanes containing the package */
  v_lanecapacity := 0;
  SELECT SUM(volume) INTO v_lanecapacity FROM LANE l, PACKAGE_LANE_GTT pl
  WHERE l.rfpid = vRfp_id AND
   l.laneid = pl.lane_id AND
   pl.package_id = v_package_id_seq;
  /* create package entries */
  INSERT INTO PACKAGE(packageid, packagename, rfpid, round_num, cmpackageid, totalvolume)
   SELECT package_id, package_name, vRfp_id, vRound_num, cm_package_id, v_lanecapacity
   FROM PACKAGE_GTT
   WHERE package_id = v_package_id_seq
   AND NOT EXISTS (SELECT 1 FROM PACKAGE WHERE packageid=v_package_id_seq AND rfpid=vRfp_id);
  /* insert only lanes that are not already present in packagelane for this package */
  INSERT INTO PACKAGELANE(packageid, rfpid, laneid)
   SELECT package_id, vRfp_id, lane_id
   FROM PACKAGE_LANE_GTT
   WHERE package_id = v_package_id_seq AND
    isimported <> 0 AND lane_id NOT IN(
     SELECT laneid FROM PACKAGELANE
     WHERE packageid = v_package_id_seq AND
      rfpid = vRfp_id
    );
 END LOOP; -- end cur_packages_imported

 /* set the status to error if none of the records where processes successfully */
 SELECT COUNT(*) INTO v_count
 FROM  IMPORT_CM_LANE
 WHERE tc_company_id = vtc_company_id
 AND   rfp_id        = vrfp_id
 AND   round_num     = vround_num
 AND   scenario_id  = vscenario_id
 AND   import_type   = vimport_type
 AND error_code IS NULL;
 /* no records retrieved in the above query */
 IF(v_count = 0 ) THEN
  RetVal := 'ERROR';
 END IF;
 v_contract_type := 66;

 FOR c10 IN cur_contract_types LOOP
  v_equipmenttypeid := c10.equipmenttype_id;
  v_servicetype := c10.servicetype_id;
  v_protectionlevel := c10.protectionlevel_id;
  v_mot := c10.MOT;
  v_commoditycode := c10.commoditycode_id;
  IF v_equipmenttypeid IS NULL AND v_servicetype IS NULL AND v_protectionlevel IS NULL
   AND v_mot IS NULL AND v_commoditycode IS NULL THEN
   v_count := 0;
   SELECT COUNT(*)
   INTO v_count
   FROM OB200CONTRACT_TYPE
   WHERE rfpid=vrfp_id AND
    contract_type=0;
   IF(v_count = 0) THEN
    INSERT INTO OB200CONTRACT_TYPE(
     contract_type, equipmenttype_id, servicetype_id, protectionlevel_id, MOT, commoditycode_id, rfpid
    )VALUES (
     0, v_equipmenttypeid, v_servicetype, v_protectionlevel, v_mot, v_commoditycode, vrfp_id
    );
   END IF;
  ELSE
   v_count := 0;
   SELECT COUNT(*) INTO  v_count
   FROM OB200CONTRACT_TYPE
   WHERE rfpid=vrfp_id
    AND (equipmenttype_id = v_equipmenttypeid OR (equipmenttype_id IS NULL AND v_equipmenttypeid IS NULL ))
    AND (servicetype_id = v_servicetype OR (servicetype_id IS NULL AND v_servicetype IS NULL ))
    AND (protectionlevel_id = v_protectionlevel OR (protectionlevel_id IS NULL AND v_protectionlevel IS NULL ))
    AND (commoditycode_id = v_commoditycode OR (commoditycode_id IS NULL AND v_commoditycode IS NULL ))
    AND (MOT = v_mot OR (MOT IS NULL AND v_mot IS NULL));
   IF (v_count = 0 ) THEN
    SELECT MAX(contract_type) INTO l_contract_type FROM OB200CONTRACT_TYPE
     WHERE rfpid=vrfp_id;
    IF l_contract_type IS NULL THEN
     INSERT INTO OB200CONTRACT_TYPE(
      contract_type, equipmenttype_id, servicetype_id, protectionlevel_id,MOT, commoditycode_id, rfpid
     )VALUES (
      0, NULL, NULL, NULL, NULL, NULL, vrfp_id
     );
     v_contract_type := 0;
    ELSE
     v_contract_type := l_contract_type + 1;
    END IF;
    INSERT INTO OB200CONTRACT_TYPE(
     contract_type, equipmenttype_id, servicetype_id, protectionlevel_id, MOT, commoditycode_id, rfpid
    )VALUES (
     v_contract_type, v_equipmenttypeid, v_servicetype, v_protectionlevel, v_mot, v_commoditycode, vrfp_id
    );
    v_contract_type := v_contract_type + 1;
   END IF;
  END IF;
 END LOOP; --cur_contract_types

 /*************************** stmts that should execute only in the end of the function *****************/
 /* Hack -> since the trigger updates the laneattribute version increment the attribute version of rfp and others */
 /* Increment the rfp laneattribute version */
 UPDATE RFP
 SET laneattributeversion = rfp_laneattributeversion
 WHERE rfpid = vrfp_id
 AND round_num = vround_num;
 /* set the laneattribute version again, as the insert trigger removes the previous setting */
 BEGIN
     UPDATE LANE
     SET laneattributeversion = rfp_laneattributeversion
     WHERE rfpid = vrfp_id
     AND round_num = vround_num
     AND comblaneid NOT IN (SELECT comblaneid FROM LANE_GTT);
 EXCEPTION when NO_DATA_FOUND THEN
  -- do nothing
   v_count :=1;
 END;

 RETURN retval;
END Import_Cm_Lanes;
/
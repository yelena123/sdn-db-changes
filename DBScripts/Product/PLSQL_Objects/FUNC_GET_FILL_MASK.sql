CREATE OR REPLACE FUNCTION GET_FILL_MASK (v_arealen       IN NUMBER,
                                            v_zonelen       IN NUMBER,
                                            v_aislelen      IN NUMBER,
                                            v_baylen        IN NUMBER,
                                            v_lvllen        IN NUMBER,
                                            v_positionlen   IN NUMBER)
   RETURN NUMBER
AS
   v_maskval    NUMBER (10);
   v_pos        NUMBER;

   TYPE bits32 IS VARRAY (32) OF CHAR (1);

   v_bitsmask   bits32
                   := bits32 (0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0);
BEGIN
   IF v_arealen > 0
   THEN
      v_pos := v_arealen - 1;
      v_bitsmask (32 - v_pos) := 1;
   END IF;

   IF v_zonelen > 0
   THEN
      v_pos := (v_zonelen - 1);
      v_bitsmask (32 - v_pos) := 1;
   END IF;

   IF v_aislelen > 0
   THEN
      v_pos := (v_aislelen - 1);
      v_bitsmask (32 - v_pos) := 1;
   END IF;

   IF v_baylen > 0
   THEN
      v_pos := (v_baylen - 1);
      v_bitsmask (32 - v_pos) := 1;
   END IF;

   IF v_lvllen > 0
   THEN
      v_pos := (v_lvllen - 1);
      v_bitsmask (32 - v_pos) := 1;
   END IF;

   IF v_positionlen > 0
   THEN
      v_pos := (v_positionlen - 1);
      v_bitsmask (32 - v_pos) := 1;
   END IF;

   SELECT BIN_TO_NUM (v_bitsmask (1),
                      v_bitsmask (2),
                      v_bitsmask (3),
                      v_bitsmask (4),
                      v_bitsmask (5),
                      v_bitsmask (6),
                      v_bitsmask (7),
                      v_bitsmask (8),
                      v_bitsmask (9),
                      v_bitsmask (10),
                      v_bitsmask (11),
                      v_bitsmask (12),
                      v_bitsmask (13),
                      v_bitsmask (14),
                      v_bitsmask (15),
                      v_bitsmask (16),
                      v_bitsmask (17),
                      v_bitsmask (18),
                      v_bitsmask (19),
                      v_bitsmask (20),
                      v_bitsmask (21),
                      v_bitsmask (22),
                      v_bitsmask (23),
                      v_bitsmask (24),
                      v_bitsmask (25),
                      v_bitsmask (26),
                      v_bitsmask (27),
                      v_bitsmask (28),
                      v_bitsmask (29),
                      v_bitsmask (30),
                      v_bitsmask (31),
                      v_bitsmask (32))
     INTO v_maskval
     FROM DUAL;

   RETURN (v_maskval);
END;
/
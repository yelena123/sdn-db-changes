create or replace function wm_get_exp_results_for_job_dtl
(
    p_val_job_dtl_id    in val_job_dtl.val_job_dtl_id%type
)
return varchar2
as
    v_result_list_csv varchar2(4000);
begin
    select listagg(to_char(ver.row_num) || ',' || to_char(ver.pos) || ',' || ver.expected_value, ',')
        within group(order by ver.row_num, ver.pos)
    into v_result_list_csv
    from val_expected_results ver
    where ver.val_job_dtl_id = p_val_job_dtl_id;
    
    return v_result_list_csv;
end;
/
show errors;

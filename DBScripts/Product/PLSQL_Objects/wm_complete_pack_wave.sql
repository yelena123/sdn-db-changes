create or replace procedure wm_complete_pack_wave
(
    p_whse             in whse_master.whse%type,
    p_pick_wave_nbr    in ship_wave_parm.pick_wave_nbr%type,
    p_user_id          in ucl_user.user_name%type,
    p_old_pwph_id      in pack_wave_parm_hdr.pack_wave_parm_id%type,
    p_sys_cd_flag_lvl  in number
)
as
    v_new_pwph_id   pack_wave_parm_hdr.pack_wave_parm_id%type
        := pack_wave_parm_hdr_id_seq.nextval;
begin
    -- create a work type hdr rec
    insert into pack_wave_parm_hdr
    (
        pack_wave_desc, rec_type, max_pack_wave, bal_alg, chute_util, 
        start_sorter_grp, start_lvl, chute_assign_type, force_cat_flag, 
        chk_critcl_dim_flag, incl_actv_flag, incl_resv_flag, 
        incl_case_pick_flag, inc_mxd_flag, pkt_sel_strike, crit_nbr,
        create_date_time, mod_date_time, user_id, pack_wave_parm_id,
        pack_wave_parm_hdr_id
    )
    select pwph.pack_wave_desc, 'A', pwph.max_pack_wave, pwph.bal_alg,
        pwph.chute_util, pwph.start_sorter_grp, pwph.start_lvl, 
        pwph.chute_assign_type, pwph.force_cat_flag, pwph.chk_critcl_dim_flag,
        pwph.incl_actv_flag, pwph.incl_resv_flag, pwph.incl_case_pick_flag, 
        pwph.inc_mxd_flag, pwph.pkt_sel_strike, pwph.crit_nbr, sysdate, 
        sysdate, p_user_id, v_new_pwph_id, v_new_pwph_id
    from pack_wave_parm_hdr pwph
    where pwph.pack_wave_parm_id = p_old_pwph_id;
    wm_cs_log('New PWPH created ' || sql%rowcount, p_sql_log_level => 2);

    -- repoint the pick wave to it
    update wave_parm wp
    set wp.pack_wave_parm_id = v_new_pwph_id, wp.mod_date_time = sysdate,
        wp.user_id = p_user_id
    where wp.wave_nbr = p_pick_wave_nbr and wp.whse = p_whse;
    wm_cs_log('Wave parm updated ' || sql%rowcount, p_sql_log_level => 2);

    insert into pack_wave_parm_dtl
    (
        rec_type, stat_code, create_date_time, mod_date_time, user_id,
        pack_wave_parm_id, pack_wave_parm_dtl_id, pack_wave_parm_hdr_id, 
        sorter_grp, pack_wave_nbr, pack_wave_seq_nbr
    )
    select 'A', 0, sysdate, sysdate, p_user_id, v_new_pwph_id,
        pack_wave_parm_dtl_id_seq.nextval, v_new_pwph_id, iv.sorter_grp,
        p_pick_wave_nbr || lpad(to_char(iv.pack_wave_seq_nbr), 3, '0'), 
        iv.pack_wave_seq_nbr
    from
    (
        select distinct t2.pack_wave_seq_nbr, t2.sorter_grp
        from tmp_pack_wave_selected_lpns t1
        join tmp_pack_wave_selected_chutes t2 on t2.chute_row_id = t1.chute_row_id
    ) iv;
    wm_cs_log('New PWPD''s created ' || sql%rowcount);

    merge into lpn l
    using
    (
        select t1.lpn_id, t1.chute_assign_type, t2.chute_id,
            p_pick_wave_nbr || lpad(to_char(t2.pack_wave_seq_nbr), 3, '0') pack_wave_nbr,
            row_number() over(partition by t2.chute_id, t2.pack_wave_seq_nbr
                order by t1.lpn_id) lpn_print_group_code
        from tmp_pack_wave_selected_lpns t1
        join tmp_pack_wave_selected_chutes t2 on t2.chute_row_id = t1.chute_row_id
    ) iv on (iv.lpn_id = l.lpn_id)
    when matched then
    update set l.chute_assign_type = iv.chute_assign_type, 
        l.chute_id = iv.chute_id, l.pack_wave_nbr = iv.pack_wave_nbr,
        l.lpn_print_group_code = iv.lpn_print_group_code,
        l.created_dttm = sysdate, l.created_source = p_user_id;
    wm_cs_log('Lpns updated ' || sql%rowcount);

    merge into alloc_invn_dtl aid
    using
    (
        select aid.alloc_invn_dtl_id,
            p_pick_wave_nbr || lpad(to_char(t2.pack_wave_seq_nbr), 3, '0') pack_wave_nbr
        from tmp_pack_wave_selected_lpns t1
        join tmp_pack_wave_selected_chutes t2 on t2.chute_row_id = t1.chute_row_id
        join alloc_invn_dtl aid on aid.carton_nbr = t1.tc_lpn_id
            and aid.whse = p_whse and aid.stat_code < 90
            and aid.task_genrtn_ref_code = '1'
            and aid.task_genrtn_ref_nbr = p_pick_wave_nbr
    ) iv on (iv.alloc_invn_dtl_id = aid.alloc_invn_dtl_id)
    when matched then
    update set aid.task_genrtn_ref_code = '44', aid.mod_date_time = sysdate,
        aid.user_id = p_user_id, aid.task_genrtn_ref_nbr = iv.pack_wave_nbr;
    wm_cs_log('AID''s updated ' || sql%rowcount);

    -- fetch the sorter group belonging to last chute assigned and stamp on sys_code
    if (p_sys_cd_flag_lvl = 0)
    then
        update sys_code sc
        set sc.code_id = (select min(pwpd.sorter_grp) keep (dense_rank first order by pwpd.pack_wave_seq_nbr desc)
            from pack_wave_parm_dtl pwpd
            where pwpd.pack_wave_parm_id = v_new_pwph_id and pwpd.rec_type = 'A')
        where sc.rec_type = 'S' and sc.code_type = '695';
    elsif (p_sys_cd_flag_lvl = 1)
    then
        update whse_sys_code sc
        set sc.code_id = (select min(pwpd.sorter_grp) keep (dense_rank first order by pwpd.pack_wave_seq_nbr desc)
            from pack_wave_parm_dtl pwpd
            where pwpd.pack_wave_parm_id = v_new_pwph_id and pwpd.rec_type = 'A')
        where sc.rec_type = 'S' and sc.code_type = '695' and sc.whse = p_whse;
    end if;
    wm_cs_log('last sorter group used stamped on sys code ' || sql%rowcount, p_sql_log_level => 2);
end;
/
show errors;
CREATE OR REPLACE
PACKAGE scenario_man_cons_pkg
IS
   PROCEDURE synchronize_scenarios (cwsid INTEGER,              --workspace ID
                                                  tccompanyid INTEGER --companyID
                                                                     );

   PROCEDURE insert_scenario_order (orderid          INTEGER,       --order ID
                                    cwsid            INTEGER,   --workspace ID
                                    tccompanyid      INTEGER,      --companyID
                                    cwsscenarioid    INTEGER);

   PROCEDURE insert_scenario_shipments (orderid          INTEGER,   --order ID
                                        cwsid            INTEGER, --workspace ID
                                        tccompanyid      INTEGER,  --companyID
                                        cwsscenarioid    INTEGER);

   PROCEDURE insert_scenario_stops (orderid          INTEGER,       --order ID
                                    cwsid            INTEGER,   --workspace ID
                                    tccompanyid      INTEGER,      --companyID
                                    cwsscenarioid    INTEGER);

  PROCEDURE insert_scenario_virtual_trip (orderid         INTEGER, --order ID
                                          cwsid           INTEGER, --workspace ID
                                          tccompanyid     INTEGER,  --companyID
                                          cwsscenarioid   INTEGER  );

   PROCEDURE insert_scenario_ord_movs (orderid          INTEGER,    --order ID
                                       cwsid            INTEGER, --workspace ID
                                       tccompanyid      INTEGER,   --companyID
                                       cwsscenarioid    INTEGER);

   PROCEDURE load_scenario_workspace (cwsid INTEGER, cwsscenarioid INTEGER);

   PROCEDURE cancel_scenario_workspace (cwsid INTEGER, cwsscenarioid INTEGER);

   PROCEDURE load_scenario_draft_shipments (
      shipmentlistrefcur       OUT man_cons_pkg.cursor_type,
      -- Cws shipment info
      movementlistrefcur       OUT man_cons_pkg.cursor_type,
      -- Cws order movement info
      origmovementlistrefcur   OUT man_cons_pkg.cursor_type,
      -- Cws order movement info
      stoplistrefcur           OUT man_cons_pkg.cursor_type,
      -- Cws stop info
      ordersummarylistrefcur   OUT man_cons_pkg.cursor_type,
      -- CwsOrderSummary info
      shipperid                    INTEGER,                      -- company id
      cwsid                        INTEGER,                    -- workspace id
      cwsscenarioid                INTEGER,                     -- scenario id
      cwsshipmentlistcsv           VARCHAR2,
      -- specific list of draft shipments to load
      ldsoperationcode             NUMBER,
      -- a code # from the wrapper (see LDS_OP_*)
      ldsloaderflagword            NUMBER,
      -- a bitmask from the wrapper (see LDS_LOAD_*)
      cwsprocessid                 NUMBER-- cws Porcess Id representing group of orders in progress for accepting.
      );
END scenario_man_cons_pkg;
/

create or replace
PACKAGE BODY SCENARIO_MAN_CONS_PKG AS
 PROCEDURE synchronize_scenarios (
      cwsid         INTEGER,                                   --workspace ID
      tccompanyid   INTEGER                                       --companyID
   )
   IS
      where_clause             VARCHAR2 (5000);
      sql_statement            VARCHAR2 (5000);
      scenario_id_cursor       man_cons_pkg.cursor_type;
      cwsscenarioid            cws_scenario.scenario_id%TYPE;
      orderidstoinsertcursor   man_cons_pkg.cursor_type;
      orderidstoremovecursor   man_cons_pkg.cursor_type;
      vorderid                 cws_order.order_id%TYPE;
/*
LOGIC:
iterate over scenario list
for all CWS_ORDER records that do not exist in the current scenario, copy over CWS_ORDER, CWS_MOVEMENT, CWS_SHIPMENT, CWS_STOP (if it doesn't exist already). Mark shipment needs_refresh
 compare CWS_Order last refreshed TS and CWS_SCENARIO_ORDER last refreshed TS. If (1) > (2), Mark Shipment Needs Refresh.
remove all scenario orders/movements that don't exist in the workspace. Mark the shipments they are on needs_refresh.
*/
   BEGIN
--build scenario list
      where_clause :=
         ' where cons_workspace_id = :cwsId and tc_company_id = :tcCompanyId ';
      sql_statement :=
                      'select scenario_id from cws_scenario ' || where_clause;

      OPEN scenario_id_cursor FOR sql_statement USING cwsid, tccompanyid;

      <<scenario_id_cursor_loop>>
      LOOP
         FETCH scenario_id_cursor
          INTO cwsscenarioid;

         EXIT WHEN scenario_id_cursor%NOTFOUND;
         --get all the orders that don't exist in the scenario tables
         sql_statement :=
               'select wso.order_id from cws_order wso where not exists '
            || '( select sco.order_id from cws_scenario_orders sco where sco.order_id = wso.order_id '
            || '  and sco.scenario_id = :cwsScenarioId '
            || ') or exists '
            || '( select scno.order_id from cws_scenario_orders scno where scno.order_id = wso.order_id '
            || '  and scno.last_refreshed_dttm < wso.last_refreshed_dttm'
            || '  and scno.scenario_id = :cwsScenarioId)';

         OPEN orderidstoinsertcursor FOR sql_statement
         USING cwsscenarioid, cwsscenarioid;

         <<orderidstoinsertcursor_loop>>
         LOOP
            FETCH orderidstoinsertcursor
             INTO vorderid;

            EXIT WHEN orderidstoinsertcursor%NOTFOUND;
            --these are orders that do not exist in the CWS_SCENARIO table.
            insert_scenario_order (vorderid,
                                   cwsid,
                                   tccompanyid,
                                   cwsscenarioid
                                  );                  --inserts scenario order
            insert_scenario_shipments (vorderid,
                                       cwsid,
                                       tccompanyid,
                                       cwsscenarioid
                                      ); --inserts related shipments and stops
            insert_scenario_ord_movs (vorderid,
                                      cwsid,
                                      tccompanyid,
                                      cwsscenarioid
                                     );              --inserts order movements
            insert_scenario_stops (vorderid, cwsid, tccompanyid,
                                   cwsscenarioid);    --inserts Shipment Stops
			insert_scenario_virtual_trip (vorderid, cwsid, tccompanyid,
                                   cwsscenarioid);    --inserts Virtual Trip 
         END LOOP orderidstoinsertcursor_loop;

         --get all the scenario orders that don't exist in the workspace tables
         sql_statement :=
               'select sco.order_id from cws_scenario_orders sco where '
            || ' not exists (select cwo.order_id from cws_order cwo where cwo.order_id = sco.order_id) '
            || ' and sco.scenario_id = :cwsScenarioId ';

         OPEN orderidstoremovecursor FOR sql_statement USING cwsscenarioid;

         <<orderidstoremovecursor_loop>>
         LOOP
            FETCH orderidstoremovecursor
             INTO vorderid;

            EXIT WHEN orderidstoremovecursor%NOTFOUND;

            --these are orders that do not exist in the CWS_SCENARIO table.
			
			DELETE FROM cws_scenario_virtual_trip scvt
                  WHERE scvt.scenario_id = cwsscenarioid;
							  
            DELETE FROM cws_scenario_stop scstop
                  WHERE scstop.scenario_id = cwsscenarioid
                    AND EXISTS (
                           SELECT 1
                             FROM cws_scenario_order_movement scom
                            WHERE scom.scenario_id = scstop.scenario_id
                              AND scom.ws_shipment_id = scstop.ws_shipment_id
                              AND scom.order_id = vorderid);

            DELETE FROM cws_scenario_shipment scshipment
                  WHERE scshipment.scenario_id = cwsscenarioid
                    AND EXISTS (
                           SELECT 1
                             FROM cws_scenario_order_movement scom
                            WHERE scom.scenario_id = scshipment.scenario_id
                              AND scom.ws_shipment_id =
                                                     scshipment.ws_shipment_id
                              AND scom.order_id = vorderid);

            DELETE FROM cws_scenario_order_movement scom
                  WHERE scom.scenario_id = cwsscenarioid
                    AND scom.order_id = vorderid;

            DELETE FROM cws_scenario_orders
                  WHERE scenario_id = cwsscenarioid AND order_id = vorderid;
         END LOOP orderidstoremovecursor_loop;
      END LOOP scenario_id_cursor_loop;
   END synchronize_scenarios;

   PROCEDURE insert_scenario_order (
      orderid         INTEGER,                                      --order ID
      cwsid           INTEGER,                                  --workspace ID
      tccompanyid     INTEGER,                                     --companyID
      cwsscenarioid   INTEGER
   )
   IS
   BEGIN
      INSERT INTO cws_scenario_orders
                  (scenario_id, order_id, last_refreshed_dttm)
         SELECT cwsscenarioid, orderid, cwo.last_refreshed_dttm
           FROM cws_order cwo
          WHERE cwo.order_id = orderid
            AND cwo.cons_workspace_id = cwsid
            AND NOT EXISTS (SELECT 1
                              FROM cws_scenario_orders sco
                             WHERE sco.order_id = orderid);

      -- Update the Last Refreshed DTTM of all the orders in the scenario workspace
      UPDATE cws_scenario_orders scno
         SET last_refreshed_dttm = (SELECT last_refreshed_dttm
                                      FROM cws_order wso
                                     WHERE wso.order_id = scno.order_id);
   END insert_scenario_order;

   PROCEDURE insert_scenario_shipments (
      orderid         INTEGER,                                      --order ID
      cwsid           INTEGER,                                  --workspace ID
      tccompanyid     INTEGER,                                     --companyID
      cwsscenarioid   INTEGER
   )
   IS
   BEGIN
      INSERT INTO cws_scenario_shipment
                  (scenario_id, cons_workspace_id, ws_shipment_id,
                   tc_company_id, orig_shipment_id, tc_shipment_id,
                   last_refreshed_dttm, last_updated_dttm, needs_refresh,
                   shipment_comps_enabled, is_hazmat, is_perishable,
                   promote_to_avail_flag, has_hard_check_error,
                   has_soft_check_error, o_address, o_city, o_state_prov,
                   o_postal_code, o_county, o_country_code, d_address,
                   d_city, d_state_prov, d_postal_code, d_county,
                   d_country_code, feasible_carrier_code,
                   save_mot_as_dsg_flag, save_equip_as_dsg_flag,
                   save_sl_as_dsg_flag, save_cc_as_dsg_flag, trans_resp_code,
                   billing_method, commodity_class, dsg_carrier_code,
                   total_distance, direct_distance, out_of_route_distance,
                   distance_uom, size1_value, size1_uom, size2_value,
                   size2_uom, size3_value, size3_uom, size4_value, size4_uom,
                   num_stops, pickup_start_dttm, pickup_end_dttm, pickup_tz,
                   delivery_start_dttm, delivery_end_dttm, delivery_tz,
                   initial_cost, baseline_cost, estimated_cost,
                   xdock_sort_cost, needs_validation, delivery_req,
                   dropoff_pickup, packaging, feasible_driver_type,
                   equip_util_per, wave_id, dsg_driver_type,
                   save_dt_as_dsg_flag, save_tract_as_dsg_flag,
                   is_wave_man_changed, is_time_feas_enabled, move_type,
                   carrier_id, dsg_carrier_id, feasible_carrier_id,
                   dsg_equipment_id, dsg_equipment2_id,
                   feasible_equipment_id, feasible_equipment2_id, dsg_mot_id,
                   feasible_mot_id, product_class_id, protection_level_id,
                   dsg_service_level_id, feasible_service_level_id,
                   size1_uom_id, size2_uom_id, size3_uom_id, size4_uom_id,
                   asg_mot_id, booking_id, static_route_id, is_filo,
                   is_cooler_at_nose, cws_process_id, transit_time_minutes,
                   is_loading_seq_modified, is_locked, last_ws_updated_dttm)
         SELECT cwsscenarioid, cons_workspace_id, ws_shipment_id,
                tc_company_id, orig_shipment_id, tc_shipment_id,
                last_refreshed_dttm, last_updated_dttm, needs_refresh,
                shipment_comps_enabled, is_hazmat, is_perishable,
                promote_to_avail_flag, has_hard_check_error,
                has_soft_check_error, o_address, o_city, o_state_prov,
                o_postal_code, o_county, o_country_code, d_address, d_city,
                d_state_prov, d_postal_code, d_county, d_country_code,
                feasible_carrier_code, save_mot_as_dsg_flag,
                save_equip_as_dsg_flag, save_sl_as_dsg_flag,
                save_cc_as_dsg_flag, trans_resp_code, billing_method,
                commodity_class, dsg_carrier_code, total_distance,
                direct_distance, out_of_route_distance, distance_uom,
                size1_value, size1_uom, size2_value, size2_uom, size3_value,
                size3_uom, size4_value, size4_uom, num_stops,
                pickup_start_dttm, pickup_end_dttm, pickup_tz,
                delivery_start_dttm, delivery_end_dttm, delivery_tz,
                initial_cost, baseline_cost, estimated_cost, xdock_sort_cost,
                needs_validation, delivery_req, dropoff_pickup, packaging,
                feasible_driver_type, equip_util_per, wave_id,
                dsg_driver_type, save_dt_as_dsg_flag, save_tract_as_dsg_flag,
                is_wave_man_changed, is_time_feas_enabled, move_type,
                carrier_id, dsg_carrier_id, feasible_carrier_id,
                dsg_equipment_id, dsg_equipment2_id, feasible_equipment_id,
                feasible_equipment2_id, dsg_mot_id, feasible_mot_id,
                product_class_id, protection_level_id, dsg_service_level_id,
                feasible_service_level_id, size1_uom_id, size2_uom_id,
                size3_uom_id, size4_uom_id, asg_mot_id, booking_id,
                static_route_id, is_filo, is_cooler_at_nose, cws_process_id,
                transit_time_minutes, is_loading_seq_modified, is_locked,
                last_ws_updated_dttm
           FROM cws_shipment cwshipment
          WHERE cwshipment.cons_workspace_id = cwsid        -- same workspace
            AND EXISTS                        --order and shipment are related
                      (
                   SELECT 1
                     FROM cws_order_movement cwom
                    WHERE cwom.order_id = orderid
                      AND cwom.ws_shipment_id = cwshipment.ws_shipment_id
                      AND cwom.cons_workspace_id =
                                                  cwshipment.cons_workspace_id)
            AND NOT EXISTS  --the shipment doesn't already exist in this table
                          (
                   SELECT 1
                     FROM cws_scenario_shipment scshipment
                    WHERE scshipment.ws_shipment_id =
                                                     cwshipment.ws_shipment_id
                      AND scshipment.cons_workspace_id =
                                                  cwshipment.cons_workspace_id);

      --mark all related shipments needs_refresh
      UPDATE cws_scenario_shipment scshipment
         SET needs_refresh = 1
       WHERE EXISTS (
                SELECT 1
                  FROM cws_order_movement cwom
                 WHERE cwom.order_id = orderid
                   AND cwom.ws_shipment_id = scshipment.ws_shipment_id
                   AND cwom.cons_workspace_id = scshipment.cons_workspace_id);
   END insert_scenario_shipments;

   PROCEDURE insert_scenario_stops (
      orderid         INTEGER,                                      --order ID
      cwsid           INTEGER,                                  --workspace ID
      tccompanyid     INTEGER,                                     --companyID
      cwsscenarioid   INTEGER
   )
   IS
   BEGIN
      INSERT INTO cws_scenario_stop
                  (scenario_id, cons_workspace_id, ws_shipment_id, stop_seq,ON_STATIC_ROUTE,
                   stop_comps_enabled, time_window_comps_enabled,
                   stop_location_name, facility_id, facility_alias_id,
                   dock_id, address,address_2,address_3, city, state_prov, postal_code, county,
                   country_code, handler, drop_hook_indicator, is_appt_reqd,
                   stop_tz, arrival_start_dttm, arrival_end_dttm,
                   departure_start_dttm, departure_end_dttm,
                   contact_first_name, contact_surname, contact_phone_number,
                   distance, distance_uom, capacity, wave_id,
                   wave_option_list_id, wave_option_id, is_wave_man_changed,
                   segment_id, segment_stop_seq, size_uom_id,
                   stop_time_modified, stop_action, hitch_time,
                   order_window_violation, dock_hour_violation,
                   wave_option_violation)
         SELECT cwsscenarioid, cons_workspace_id, ws_shipment_id, stop_seq,ON_STATIC_ROUTE,
                stop_comps_enabled, time_window_comps_enabled,
                stop_location_name, facility_id, facility_alias_id, dock_id,
                address,address_2,address_3, city, state_prov, postal_code, county, country_code,
                handler, drop_hook_indicator, is_appt_reqd, stop_tz,
                arrival_start_dttm, arrival_end_dttm, departure_start_dttm,
                departure_end_dttm, contact_first_name, contact_surname,
                contact_phone_number, distance, distance_uom, capacity,
                wave_id, wave_option_list_id, wave_option_id,
                is_wave_man_changed, segment_id, segment_stop_seq,
                size_uom_id, stop_time_modified, stop_action, hitch_time,
                order_window_violation, dock_hour_violation,
                wave_option_violation
           FROM cws_stop cwstop
          WHERE cwstop.cons_workspace_id = cwsid            -- same workspace
            AND EXISTS                            --order and stop are related
                      (
                   SELECT 1
                     FROM cws_order_movement cwom
                    WHERE cwom.order_id = orderid
                      AND cwom.ws_shipment_id = cwstop.ws_shipment_id
                      AND cwom.cons_workspace_id = cwstop.cons_workspace_id)
            AND NOT EXISTS      --the stop doesn't already exist in this table
                          (
                   SELECT 1
                     FROM cws_scenario_stop scstop
                    WHERE scstop.ws_shipment_id = cwstop.ws_shipment_id
                      AND scstop.cons_workspace_id = cwstop.cons_workspace_id
                      AND scstop.stop_seq = cwstop.stop_seq);

      --mark all stops comps_enabled
      UPDATE cws_scenario_stop scstop
         SET stop_comps_enabled = 1
       WHERE EXISTS (
                SELECT 1
                  FROM cws_order_movement cwom
                 WHERE cwom.order_id = orderid
                   AND cwom.ws_shipment_id = scstop.ws_shipment_id
                   AND cwom.cons_workspace_id = scstop.cons_workspace_id);
   END insert_scenario_stops;
   PROCEDURE insert_scenario_virtual_trip (
      orderid         INTEGER,                                      --order ID
      cwsid           INTEGER,                                  --workspace ID
      tccompanyid     INTEGER,                                     --companyID
      cwsscenarioid   INTEGER
   )
   IS
   BEGIN
      INSERT INTO cws_scenario_virtual_trip
                  ( cons_workspace_id,scenario_id, virtual_trip_id,virtual_trip_sequence,ws_shipment_id, dead_head_distance,
                    distance_uom, dead_head_transit_time,is_tandem
                   )
         SELECT cons_workspace_id,cwsscenarioid, virtual_trip_id,virtual_trip_sequence,ws_shipment_id, dead_head_distance,
                    distance_uom, dead_head_transit_time,is_tandem
          FROM cws_virtual_trip cwvt
          WHERE cwvt.cons_workspace_id = cwsid            -- same workspace
          AND NOT EXISTS      
                          (
                   SELECT 1
                     FROM cws_scenario_virtual_trip scvt
                     WHERE scvt.ws_shipment_id = cwvt.ws_shipment_id
                      AND scvt.cons_workspace_id = cwvt.cons_workspace_id
                      AND scvt.virtual_trip_id = cwvt.virtual_trip_id
					  AND scvt.virtual_trip_sequence = cwvt.virtual_trip_sequence
					  );

   END insert_scenario_virtual_trip;

/*
 insert scenario order movements using records in the cws_order_movement table. Invoked by SYNCHRONIZE_SCENARIOS
*/
   PROCEDURE insert_scenario_ord_movs (
      orderid         INTEGER,                                      --order ID
      cwsid           INTEGER,                                  --workspace ID
      tccompanyid     INTEGER,                                     --companyID
      cwsscenarioid   INTEGER
   )
   IS
   BEGIN
      DELETE FROM cws_scenario_order_movement cwsom
            WHERE cwsom.cons_workspace_id = cwsid
              AND cwsom.order_id = orderid
              AND scenario_id = cwsscenarioid
              AND tc_company_id = tccompanyid;

      INSERT INTO cws_scenario_order_movement
                  (scenario_id, cons_workspace_id, order_id, ws_shipment_id,
                   movement_type, order_shipment_seq, tc_company_id,
                   path_set_id, path_id, pickup_stop_seq, delivery_stop_seq,
                   is_relay, compartment_no, cws_order_movement_id,
                   order_split_id, order_loading_seq)
         SELECT cwsscenarioid, cons_workspace_id, order_id, ws_shipment_id,
                movement_type, order_shipment_seq, tc_company_id, path_set_id,
                path_id, pickup_stop_seq, delivery_stop_seq, is_relay,
                compartment_no, cws_order_movement_id, order_split_id,
                order_loading_seq
           FROM cws_order_movement cwsom
          WHERE cwsom.cons_workspace_id = cwsid              -- same workspace
            AND cwsom.order_id = orderid                          --same order
            AND cwsom.tc_company_id = tccompanyid;              --same company
   END insert_scenario_ord_movs;

-- Author: Abijeeth
-- Cancel the current workspace and Load the saved scenario into the workspace.
   PROCEDURE load_scenario_workspace (cwsid INTEGER, cwsscenarioid INTEGER)
   IS
   BEGIN
      -- Workflow:
      -- 1. Clear the workspace tables (Shipment, Stop, Order Movement)
      -- 2. Load the Workspace Shipment Table (set needs_refresh as 1)
      -- 3. Load the Workspace Stop Table
      -- 4. Load the Workspace Order Movement Table

      -- 1. Clear the workspace tables (Shipment, Stop, Order Movement)
      DELETE FROM cws_order_movement
            WHERE cons_workspace_id = cwsid
              AND order_id IN (SELECT order_id
                                 FROM cws_order
                                WHERE cws_process_id = 0);

   DELETE FROM cws_virtual_trip
            WHERE cons_workspace_id = cwsid
              AND ws_shipment_id IN (SELECT ws_shipment_id
                                       FROM cws_shipment
                                      WHERE cws_process_id = 0);								
								
      DELETE FROM cws_stop
            WHERE cons_workspace_id = cwsid
              AND ws_shipment_id IN (SELECT ws_shipment_id
                                       FROM cws_shipment
                                      WHERE cws_process_id = 0);

      DELETE FROM cws_shipment
            WHERE cons_workspace_id = cwsid AND cws_process_id = 0;

      -- 2. Load the Workspace Shipment Table
      INSERT INTO cws_shipment
                  (cons_workspace_id, ws_shipment_id, tc_company_id,
                   orig_shipment_id, tc_shipment_id, last_refreshed_dttm,
                   last_updated_dttm, needs_refresh, shipment_comps_enabled,
                   is_hazmat, is_perishable, promote_to_avail_flag,
                   has_hard_check_error, has_soft_check_error, o_address,
                   o_city, o_state_prov, o_postal_code, o_county,
                   o_country_code, d_address, d_city, d_state_prov,
                   d_postal_code, d_county, d_country_code,
                   feasible_carrier_code, save_mot_as_dsg_flag,
                   save_equip_as_dsg_flag, save_sl_as_dsg_flag,
                   save_cc_as_dsg_flag, trans_resp_code, billing_method,
                   commodity_class, dsg_carrier_code, total_distance,
                   direct_distance, out_of_route_distance, distance_uom,
                   size1_value, size1_uom, size2_value, size2_uom,
                   size3_value, size3_uom, size4_value, size4_uom, num_stops,
                   pickup_start_dttm, pickup_end_dttm, pickup_tz,
                   delivery_start_dttm, delivery_end_dttm, delivery_tz,
                   initial_cost, baseline_cost, estimated_cost,
                   xdock_sort_cost, needs_validation, delivery_req,
                   dropoff_pickup, packaging, feasible_driver_type,
                   equip_util_per, wave_id, dsg_driver_type,
                   save_dt_as_dsg_flag, save_tract_as_dsg_flag,
                   is_wave_man_changed, is_time_feas_enabled, move_type,
                   carrier_id, dsg_carrier_id, feasible_carrier_id,
                   dsg_equipment_id, dsg_equipment2_id, feasible_equipment_id,
                   feasible_equipment2_id, dsg_mot_id, feasible_mot_id,
                   product_class_id, protection_level_id,
                   dsg_service_level_id, feasible_service_level_id,
                   size1_uom_id, size2_uom_id, size3_uom_id, size4_uom_id,
                   asg_mot_id, booking_id, static_route_id, is_filo,
                   is_cooler_at_nose, cws_process_id, transit_time_minutes,
                   is_loading_seq_modified)
         (SELECT cons_workspace_id, ws_shipment_id, tc_company_id,
                 orig_shipment_id, tc_shipment_id, last_refreshed_dttm,
                 last_updated_dttm, 1, shipment_comps_enabled, is_hazmat,
                 is_perishable, promote_to_avail_flag, has_hard_check_error,
                 has_soft_check_error, o_address, o_city, o_state_prov,
                 o_postal_code, o_county, o_country_code, d_address, d_city,
                 d_state_prov, d_postal_code, d_county, d_country_code,
                 feasible_carrier_code, save_mot_as_dsg_flag,
                 save_equip_as_dsg_flag, save_sl_as_dsg_flag,
                 save_cc_as_dsg_flag, trans_resp_code, billing_method,
                 commodity_class, dsg_carrier_code, total_distance,
                 direct_distance, out_of_route_distance, distance_uom,
                 size1_value, size1_uom, size2_value, size2_uom, size3_value,
                 size3_uom, size4_value, size4_uom, num_stops,
                 pickup_start_dttm, pickup_end_dttm, pickup_tz,
                 delivery_start_dttm, delivery_end_dttm, delivery_tz,
                 initial_cost, baseline_cost, estimated_cost, xdock_sort_cost,
                 needs_validation, delivery_req, dropoff_pickup, packaging,
                 feasible_driver_type, equip_util_per, wave_id,
                 dsg_driver_type, save_dt_as_dsg_flag, save_tract_as_dsg_flag,
                 is_wave_man_changed, is_time_feas_enabled, move_type,
                 carrier_id, dsg_carrier_id, feasible_carrier_id,
                 dsg_equipment_id, dsg_equipment2_id, feasible_equipment_id,
                 feasible_equipment2_id, dsg_mot_id, feasible_mot_id,
                 product_class_id, protection_level_id, dsg_service_level_id,
                 feasible_service_level_id, size1_uom_id, size2_uom_id,
                 size3_uom_id, size4_uom_id, asg_mot_id, booking_id,
                 static_route_id, is_filo, is_cooler_at_nose, cws_process_id,
                 transit_time_minutes, is_loading_seq_modified
            FROM cws_scenario_shipment
           WHERE scenario_id = cwsscenarioid AND cons_workspace_id = cwsid);

	  UPDATE CWS_SHIPMENT SET LAST_WS_UPDATED_DTTM=(SELECT CWS_SCENARIO_SHIPMENT.LAST_WS_UPDATED_DTTM  
	         FROM CWS_SCENARIO_SHIPMENT WHERE SCENARIO_ID=cwsscenarioid
	            and cons_workspace_id = cwsid 
				and CWS_SCENARIO_SHIPMENT.ws_shipment_id = CWS_SHIPMENT.ws_shipment_id)         
	            where cws_shipment.cons_workspace_id = cwsid 
	            and cws_shipment.ws_shipment_id in (select cws_scenario_shipment.ws_shipment_id 
                from cws_scenario_shipment where scenario_id = cwsscenarioid AND cons_workspace_id = cwsid);    
		   
      -- 3. Load the Workspace Stop Table
      INSERT INTO cws_stop
                  (cons_workspace_id, ws_shipment_id, stop_seq,ON_STATIC_ROUTE,
                   stop_comps_enabled, time_window_comps_enabled,
                   stop_location_name, facility_id, facility_alias_id,
                   dock_id, address,address_2,address_3,  city, state_prov, postal_code, county,
                   country_code, handler, drop_hook_indicator, is_appt_reqd,
                   stop_tz, arrival_start_dttm, arrival_end_dttm,
                   departure_start_dttm, departure_end_dttm,
                   contact_first_name, contact_surname, contact_phone_number,
                   distance, distance_uom, capacity, wave_id,
                   wave_option_list_id, wave_option_id, is_wave_man_changed,
                   segment_id, segment_stop_seq, size_uom_id,
                   stop_time_modified, stop_action, hitch_time,
                   order_window_violation, dock_hour_violation,
                   wave_option_violation)
         (SELECT cons_workspace_id, ws_shipment_id, stop_seq,ON_STATIC_ROUTE,
                 stop_comps_enabled, time_window_comps_enabled,
                 stop_location_name, facility_id, facility_alias_id, dock_id,
                 address,address_2,address_3,city, state_prov, postal_code, county, country_code,
                 handler, drop_hook_indicator, is_appt_reqd, stop_tz,
                 arrival_start_dttm, arrival_end_dttm, departure_start_dttm,
                 departure_end_dttm, contact_first_name, contact_surname,
                 contact_phone_number, distance, distance_uom, capacity,
                 wave_id, wave_option_list_id, wave_option_id,
                 is_wave_man_changed, segment_id, segment_stop_seq,
                 size_uom_id, stop_time_modified, stop_action, hitch_time,
                 order_window_violation, dock_hour_violation,
                 wave_option_violation
            FROM cws_scenario_stop
           WHERE scenario_id = cwsscenarioid AND cons_workspace_id = cwsid);
 -- 4. Load the Workspace virtual_trip Table
      INSERT INTO cws_virtual_trip
                  (cons_workspace_id, virtual_trip_id,virtual_trip_sequence,ws_shipment_id, 
				  dead_head_distance,distance_uom, dead_head_transit_time,is_tandem)     
         (SELECT cons_workspace_id, virtual_trip_id,virtual_trip_sequence,ws_shipment_id, 
				  dead_head_distance,distance_uom, dead_head_transit_time,is_tandem
            FROM cws_scenario_virtual_trip
           WHERE scenario_id = cwsscenarioid AND cons_workspace_id = cwsid);
      -- 5. Load the Workspace Order Movement Table
      INSERT INTO cws_order_movement
                  (cons_workspace_id, order_id, ws_shipment_id, movement_type,
                   order_shipment_seq, tc_company_id, path_set_id, path_id,
                   pickup_stop_seq, delivery_stop_seq, is_relay,
                   compartment_no, cws_order_movement_id, order_split_id,
                   order_loading_seq)
         (SELECT cons_workspace_id, order_id, ws_shipment_id, movement_type,
                 order_shipment_seq, tc_company_id, path_set_id, path_id,
                 pickup_stop_seq, delivery_stop_seq, is_relay, compartment_no,
                 cws_order_movement_id, order_split_id, order_loading_seq
            FROM cws_scenario_order_movement
           WHERE scenario_id = cwsscenarioid AND cons_workspace_id = cwsid);

      --COMMIT;
   END load_scenario_workspace;

-- Cancel the scenario workspace by removing all entries in all scenario tables.
   PROCEDURE cancel_scenario_workspace (cwsid INTEGER, cwsscenarioid INTEGER)
   IS
   BEGIN
      DELETE FROM cws_scenario_order_movement
            WHERE cons_workspace_id = cwsid AND scenario_id = cwsscenarioid;
      DELETE FROM cws_scenario_virtual_trip
            WHERE cons_workspace_id = cwsid AND scenario_id = cwsscenarioid;
      DELETE FROM cws_scenario_stop
            WHERE cons_workspace_id = cwsid AND scenario_id = cwsscenarioid;

      DELETE FROM cws_scenario_shipment
            WHERE cons_workspace_id = cwsid AND scenario_id = cwsscenarioid;

      DELETE FROM cws_scenario_orders
            WHERE scenario_id = cwsscenarioid;

      DELETE FROM cws_scenario
            WHERE cons_workspace_id = cwsid AND scenario_id = cwsscenarioid;

      --COMMIT;
   END cancel_scenario_workspace;

   PROCEDURE load_scenario_draft_shipments (
      shipmentlistrefcur       OUT   man_cons_pkg.cursor_type,
      -- Cws shipment info
      movementlistrefcur       OUT   man_cons_pkg.cursor_type,
      -- Cws order movement info
      origmovementlistrefcur   OUT   man_cons_pkg.cursor_type,
      -- Cws order movement info
      stoplistrefcur           OUT   man_cons_pkg.cursor_type,
      -- Cws stop info
      ordersummarylistrefcur   OUT   man_cons_pkg.cursor_type,
      -- CwsOrderSummary info
      shipperid                      INTEGER,                    -- company id
      cwsid                          INTEGER,                  -- workspace id
      cwsscenarioid                  INTEGER,                   -- scenario id
      cwsshipmentlistcsv             VARCHAR2,
      -- specific list of draft shipments to load
      ldsoperationcode               NUMBER,
      -- a code # from the wrapper (see LDS_OP_*)
      ldsloaderflagword              NUMBER,
      -- a bitmask from the wrapper (see LDS_LOAD_*)
      cwsprocessid                   NUMBER
   -- cws Porcess Id representing group of orders in progress for accepting.
   )
   IS
      cwsshipmentidarr              man_cons_pkg.int_array;
      shipment_list_cursor          man_cons_pkg.cursor_type;
      movement_list_cursor          man_cons_pkg.cursor_type;
      origmovment_list_cursor       man_cons_pkg.cursor_type;
      stop_list_cursor              man_cons_pkg.cursor_type;
      order_summary_cursor          man_cons_pkg.cursor_type;
      -- Operation codes (only 1 value may be used)
      lds_op_get_shipment_id_list   INTEGER                  := 1;
      -- Load given list of shipment id's
      lds_op_get_needs_refresh      INTEGER                  := 2;
      -- Load shipments needing refresh
      lds_op_get_all_in_workspace   INTEGER                  := 3;
                                           -- Load all shipments in workspace
      -- Load flags that can be OR'd together
      lds_load_shipment             INTEGER                  := 0;
      -- CwsShipment's; must be on!  bitwhacking:
      lds_load_movements            INTEGER                  := 1;
      -- CwsOrderMovement's (current)
      lds_load_stops                INTEGER                  := 2;
      -- CwsStop's
      lds_load_display_orders       INTEGER                  := 4;
      -- CwsOrderSummary's
      lds_load_real_orders          INTEGER                  := 8;
      -- Order's (these are not loaded here)
      lds_load_original_movements   INTEGER                  := 16;
      -- CwsOrderMovement's (original)
      processid                     INTEGER                  := 0;
   BEGIN
      IF (cwsprocessid > 0)
      THEN
         processid := cwsprocessid;
      END IF;

      -- First get the shipment id list for the specified operation, in the correct order, into the global temp table.
      DELETE      man_cons_shipment_list_gtt;

      IF ldsoperationcode = lds_op_get_all_in_workspace
      THEN
         INSERT INTO man_cons_shipment_list_gtt
                     (row_num, shipment_id)
            SELECT   ROWNUM, cwsshp.ws_shipment_id
                FROM cws_scenario_shipment cwsshp
               WHERE cwsshp.cons_workspace_id = cwsid
                 AND cwsshp.cws_process_id = processid
                 AND cwsshp.scenario_id = cwsscenarioid
            ORDER BY cwsshp.ws_shipment_id;
      ELSIF ldsoperationcode = lds_op_get_needs_refresh
      THEN
         INSERT INTO man_cons_shipment_list_gtt
                     (row_num, shipment_id)
            SELECT   ROWNUM, cwsshp.ws_shipment_id
                FROM cws_scenario_shipment cwsshp
               WHERE cwsshp.cons_workspace_id = cwsid
                 AND cwsshp.cws_process_id = processid
                 AND cwsshp.needs_refresh = 1
                 AND cwsshp.scenario_id = cwsscenarioid
            ORDER BY cwsshp.ws_shipment_id;
      ELSIF ldsoperationcode = lds_op_get_shipment_id_list
      THEN
         cwsshipmentidarr :=
                           man_cons_pkg.csv_to_int_array (cwsshipmentlistcsv);
         FORALL i IN cwsshipmentidarr.FIRST .. cwsshipmentidarr.LAST
            INSERT INTO man_cons_shipment_list_gtt
                 VALUES (cwsshipmentidarr (i), cwsshipmentidarr (i));
      ELSE
         NULL;                                               -- no shipments!
      END IF;

      --COMMIT;

         -- Materialize the shipment resultset based on the id's and the order of the row #'s
      -- WATCH: Keep in synch with CwsShipment ResultSet ctor
      OPEN shipment_list_cursor FOR
         SELECT   cws_scenario_shipment.cons_workspace_id cons_workspace_id,
                  ws_shipment_id, tc_company_id, scenario_id,
                  orig_shipment_id, tc_shipment_id, last_refreshed_dttm,
                  last_updated_dttm, needs_refresh,NEEDS_RESEQUENCE, shipment_comps_enabled,
                  shipment_comps_enabled, is_hazmat, is_perishable, is_filo,
                  is_cooler_at_nose, promote_to_avail_flag,
                  has_hard_check_error, has_soft_check_error, o_address,
                  o_city, o_state_prov, o_postal_code, o_county,
                  o_country_code, d_address, d_city, d_state_prov,
                  d_postal_code, d_county, d_country_code, feasible_mot_id,
                  feasible_equipment_id, feasible_service_level_id,
                  feasible_carrier_id, delivery_req, dropoff_pickup,
                  packaging, dsg_mot_id, dsg_equipment_id,
                  dsg_service_level_id, dsg_carrier_id, save_mot_as_dsg_flag,
                  save_equip_as_dsg_flag, save_sl_as_dsg_flag,
                  save_cc_as_dsg_flag, product_class_id, trans_resp_code,
                  billing_method, protection_level_id, commodity_class,
                  initial_cost, baseline_cost, estimated_cost,
                  xdock_sort_cost, total_distance, direct_distance,
                  out_of_route_distance, distance_uom, size1_value,
                  size1_uom_id, size2_value, size2_uom_id, size3_value,
                  size3_uom_id, size4_value, size4_uom_id, num_stops,
                  pickup_start_dttm, pickup_end_dttm, pickup_tz,
                  delivery_start_dttm, delivery_end_dttm, delivery_tz,
                  needs_validation, feasible_driver_type,
                  feasible_equipment2_id, wave_id, dsg_equipment2_id,
                  dsg_driver_type, save_tract_as_dsg_flag,
                  save_dt_as_dsg_flag, is_wave_man_changed, move_type,
                  asg_mot_id, booking_id, feasible_carrier_code,
                  is_time_feas_enabled, dsg_carrier_code, static_route_id,
                  cws_process_id, transit_time_minutes,
                  is_loading_seq_modified
             FROM man_cons_shipment_list_gtt mcslgtt, cws_scenario_shipment
            WHERE cws_scenario_shipment.cons_workspace_id = cwsid
              AND cws_scenario_shipment.cws_process_id = processid
              AND mcslgtt.shipment_id = cws_scenario_shipment.ws_shipment_id
              AND cws_scenario_shipment.scenario_id = cwsscenarioid
         ORDER BY mcslgtt.row_num;

      -- Materialize the current order movement resultset
      -- WATCH: Keep in synch with CwsOrderMovement ResultSet ctor
      IF BITAND (ldsloaderflagword, lds_load_movements) <> 0
      THEN
         OPEN movement_list_cursor FOR
            SELECT   cws_scenario_order_movement.cons_workspace_id
                                                           cons_workspace_id,
                     order_id, ws_shipment_id, scenario_id, movement_type,
                     order_shipment_seq, tc_company_id, path_set_id, path_id,
                     pickup_stop_seq, delivery_stop_seq, order_loading_seq,
                     is_relay, compartment_no, order_split_id
                FROM man_cons_shipment_list_gtt mcslgtt,
                     cws_scenario_order_movement
               WHERE cws_scenario_order_movement.cons_workspace_id = cwsid
                 AND mcslgtt.shipment_id =
                                    cws_scenario_order_movement.ws_shipment_id
                 AND cws_scenario_order_movement.scenario_id = cwsscenarioid
            ORDER BY mcslgtt.row_num, cws_scenario_order_movement.order_id;
      ELSE
         OPEN movement_list_cursor FOR
            SELECT NULL
              FROM DUAL;
      END IF;

      -- Materialize the original order movement resultset
      -- WATCH: Keep in synch with CwsOrderMovement ResultSet ctor
      IF BITAND (ldsloaderflagword, lds_load_original_movements) <> 0
      THEN
         OPEN origmovment_list_cursor FOR
            SELECT   cws_original_movement.cons_workspace_id
                                                           cons_workspace_id,
                     order_id,
                     cws_original_movement.orig_shipment_id orig_shipment_id,
                     cws_scenario_shipment.scenario_id scenario_id,
                     movement_type, order_shipment_seq,
                     cws_original_movement.tc_company_id tc_company_id,
                     path_set_id, path_id, pickup_stop_seq,
                     delivery_stop_seq, order_loading_seq, is_relay,
                     compartment_no, order_split_id
                FROM man_cons_shipment_list_gtt mcslgtt,
                     cws_scenario_shipment,
                     cws_original_movement
               WHERE cws_scenario_shipment.cons_workspace_id = cwsid
                 AND cws_scenario_shipment.cws_process_id = processid
                 AND mcslgtt.shipment_id =
                                          cws_scenario_shipment.ws_shipment_id
                 AND cws_original_movement.cons_workspace_id = cwsid
                 AND cws_scenario_shipment.scenario_id = cwsscenarioid
                 AND cws_original_movement.orig_shipment_id =
                                        cws_scenario_shipment.orig_shipment_id
            ORDER BY mcslgtt.row_num, cws_original_movement.order_id;
      ELSE
         OPEN origmovment_list_cursor FOR
            SELECT NULL
              FROM DUAL;
      END IF;

      -- Materialize the stop resultset
      -- WATCH: Keep in synch with CwsStop ResultSet ctor
      IF BITAND (ldsloaderflagword, lds_load_stops) <> 0
      THEN
         OPEN stop_list_cursor FOR
            SELECT   cons_workspace_id, ws_shipment_id, scenario_id,
                     stop_seq,ON_STATIC_ROUTE, stop_comps_enabled, time_window_comps_enabled,
                     stop_location_name, facility_id, facility_alias_id,
                     dock_id, address,address_2,address_3,city, state_prov, county, postal_code,
                     country_code, cws_scenario_stop.handler,
                     drop_hook_indicator, is_appt_reqd, stop_tz,
                     arrival_start_dttm, arrival_end_dttm,
                     departure_start_dttm, departure_end_dttm,
                     contact_first_name, contact_surname,
                     contact_phone_number, distance, distance_uom,
                     size_uom_id, capacity, wave_id, wave_option_list_id,

                     wave_option_id,
					 (select WAVE_NAME from WAVE wa where wa.WAVE_ID = cws_scenario_stop.WAVE_ID AND (cws_scenario_stop.STOP_ACTION = 'DL' OR cws_scenario_stop.STOP_ACTION = 'RD')) WAVE_NAME,
					(select WAVE_OPTION_NAME from WAVE_OPTION wo where wo.WAVE_OPTION_ID = cws_scenario_stop.WAVE_OPTION_ID AND (cws_scenario_stop.STOP_ACTION = 'DL' OR cws_scenario_stop.STOP_ACTION = 'RD')) WAVE_OPTION_NAME,
					 is_wave_man_changed, segment_id,
                     segment_stop_seq, stop_time_modified, stop_action,
                     hitch_time, order_window_violation, dock_hour_violation,
                     wave_option_violation
                FROM man_cons_shipment_list_gtt mcslgtt, cws_scenario_stop
               WHERE cws_scenario_stop.cons_workspace_id = cwsid
                 AND mcslgtt.shipment_id = cws_scenario_stop.ws_shipment_id
                 AND cws_scenario_stop.scenario_id = cwsscenarioid
            ORDER BY mcslgtt.row_num, cws_scenario_stop.stop_seq;
      ELSE
         OPEN stop_list_cursor FOR
            SELECT NULL
              FROM DUAL;
      END IF;

      -- Materialize the display orders resultset
      -- WATCH: Keep in synch with CwsOrderSummary ResultSet ctor
      IF BITAND (ldsloaderflagword, lds_load_display_orders) <> 0
      THEN
         OPEN order_summary_cursor FOR
            SELECT   ord.order_id, ord.tc_order_id,
                     cwsscnord.scenario_id scenario_id,
                     (SELECT facility_name
                        FROM facility_alias
                       WHERE facility_alias_id =
                                      ord.o_facility_alias_id
                         AND tc_company_id = ord.tc_company_id)
                                                             o_facility_name,
                     ord.o_city, ord.o_state_prov,
                     (SELECT facility_name
                        FROM facility_alias
                       WHERE facility_alias_id =
                                      ord.d_facility_alias_id
                         AND tc_company_id = ord.tc_company_id)
                                                             d_facility_name,
                     ord.d_city, ord.d_state_prov, ord.pickup_start_dttm,
                     ord.pickup_end_dttm, ord.pickup_tz,
                     ord.delivery_start_dttm, ord.delivery_end_dttm,
                     ord.delivery_tz, ord.protection_level_id,
                     ord.normalized_baseline_cost normalized_baseline_cost,
                     cwsord.size1, cwsord.size1_uom_id, cwsord.size2,
                     cwsord.size2_uom_id, cwsord.size3, cwsord.size3_uom_id,
                     cwsord.size4, cwsord.size4_uom_id, ord.product_class_id,
                     cwsom.pickup_stop_seq, cwsom.delivery_stop_seq,
                     (SELECT DISTINCT WAVOPT.WAVE_ID FROM WAVE_OPTION_LIST WAVOPT WHERE WAVOPT.FACILITY_ID=ORD.D_FACILITY_ID AND WAVOPT.WAVE_ID=ORD.WAVE_ID AND WAVOPT.WAVE_OPTION_ID=ORD.WAVE_OPTION_ID AND WAVOPT.DOCK_ID=ORD.D_DOCK_ID AND WAVOPT.TC_COMPANY_ID=ORD.TC_COMPANY_ID AND WAVOPT.MARK_FOR_DELETION = 0) wave_id,
					 ORD.IS_PARTIALLY_PLANNED, (SELECT DISTINCT WAVOPT.WAVE_OPTION_ID FROM WAVE_OPTION_LIST WAVOPT WHERE WAVOPT.FACILITY_ID=ORD.D_FACILITY_ID AND WAVOPT.WAVE_ID=ORD.WAVE_ID AND WAVOPT.WAVE_OPTION_ID=ORD.WAVE_OPTION_ID AND WAVOPT.DOCK_ID=ORD.D_DOCK_ID AND WAVOPT.TC_COMPANY_ID=ORD.TC_COMPANY_ID AND WAVOPT.MARK_FOR_DELETION = 0) WAVE_OPTION_ID, ord.compartment_no, 0, 0,
                     cwsom.ws_shipment_id
                FROM man_cons_shipment_list_gtt mcslgtt,
                     cws_scenario_order_movement cwsom,
                     cws_order cwsord,
                     cws_scenario_orders cwsscnord,
                     orders ord
               WHERE (    cwsom.cons_workspace_id = cwsid
                      AND cwsord.cws_process_id = processid
                      AND cwsscnord.scenario_id = cwsscenarioid
                     )
                 AND (cwsord.order_id = cwsscnord.order_id)
                 AND (mcslgtt.shipment_id = cwsom.ws_shipment_id)
                 AND (cwsord.cons_workspace_id = cwsom.cons_workspace_id)
                 AND (cwsord.order_id = cwsom.order_id)
                 AND (ord.order_id = cwsord.order_id)
                 AND (cwsom.order_split_id IS NULL)
            UNION
            SELECT   ord.order_id, ord.tc_order_id, cwsscnord.scenario_id,
                     (SELECT facility_name
                        FROM facility_alias
                       WHERE facility_alias_id =
                                      ord.o_facility_alias_id
                         AND tc_company_id = ord.tc_company_id)
                                                              o_facility_name,
                     ord.o_city, ord.o_state_prov,
                     (SELECT facility_name
                        FROM facility_alias
                       WHERE facility_alias_id =
                                      ord.d_facility_alias_id
                         AND tc_company_id = ord.tc_company_id)
                                                              d_facility_name,
                     ord.d_city, ord.d_state_prov, ord.pickup_start_dttm,
                     ord.pickup_end_dttm, ord.pickup_tz,
                     ord.delivery_start_dttm, ord.delivery_end_dttm,
                     ord.delivery_tz, ord.protection_level_id,
                     os.baseline_cost normalized_baseline_cost, cwsos.size1,
                     cwsos.size1_uom_id, cwsos.size2, cwsos.size2_uom_id,
                     cwsos.size3, cwsos.size3_uom_id, cwsos.size4,
                     cwsos.size4_uom_id, ord.product_class_id,
                     cwsom.pickup_stop_seq, cwsom.delivery_stop_seq,
                     (SELECT DISTINCT WAVOPT.WAVE_ID FROM WAVE_OPTION_LIST WAVOPT WHERE WAVOPT.FACILITY_ID=ORD.D_FACILITY_ID AND WAVOPT.WAVE_ID=ORD.WAVE_ID AND WAVOPT.WAVE_OPTION_ID=ORD.WAVE_OPTION_ID AND WAVOPT.DOCK_ID=ORD.D_DOCK_ID AND WAVOPT.TC_COMPANY_ID=ORD.TC_COMPANY_ID AND WAVOPT.MARK_FOR_DELETION = 0) WAVE_ID,
					 ORD.IS_PARTIALLY_PLANNED, (SELECT DISTINCT WAVOPT.WAVE_OPTION_ID FROM WAVE_OPTION_LIST WAVOPT WHERE WAVOPT.FACILITY_ID=ORD.D_FACILITY_ID AND WAVOPT.WAVE_ID=ORD.WAVE_ID AND WAVOPT.WAVE_OPTION_ID=ORD.WAVE_OPTION_ID AND WAVOPT.DOCK_ID=ORD.D_DOCK_ID AND WAVOPT.TC_COMPANY_ID=ORD.TC_COMPANY_ID AND WAVOPT.MARK_FOR_DELETION = 0) WAVE_OPTION_ID, ord.compartment_no, cwsos.order_split_id,
                     os.order_split_seq, cwsom.ws_shipment_id
                FROM man_cons_shipment_list_gtt mcslgtt,
                     cws_order_movement cwsom,
                     cws_order cwsord,
                     cws_scenario_orders cwsscnord,
                     order_split os,
                     cws_order_split cwsos,
                     orders ord
               WHERE (    cwsom.cons_workspace_id = cwsid
                      AND cwsord.cws_process_id = processid
                      AND cwsscnord.scenario_id = cwsscenarioid
                     )
                 AND (cwsord.order_id = cwsscnord.order_id)
                 AND (mcslgtt.shipment_id = cwsom.ws_shipment_id)
                 AND (cwsord.cons_workspace_id = cwsom.cons_workspace_id)
                 AND (cwsord.order_id = cwsom.order_id)
                 AND (os.order_split_id = cwsom.order_split_id)
                 AND (cwsos.order_split_id = cwsom.order_split_id)
                 AND (ord.order_id = cwsord.order_id)
            ORDER BY pickup_stop_seq, delivery_stop_seq, tc_order_id;
      ELSE
         OPEN order_summary_cursor FOR
            SELECT NULL
              FROM DUAL;
      END IF;

      -- Set return ref cursors
      shipmentlistrefcur := shipment_list_cursor;
      movementlistrefcur := movement_list_cursor;
      origmovementlistrefcur := origmovment_list_cursor;
      stoplistrefcur := stop_list_cursor;
      ordersummarylistrefcur := order_summary_cursor;
   END load_scenario_draft_shipments;
END SCENARIO_MAN_CONS_PKG;
/
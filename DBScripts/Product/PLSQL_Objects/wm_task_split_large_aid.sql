create or replace procedure wm_task_split_large_aid
(
    p_max_wt      in task_rule_parm.max_wt%type,
    p_max_vol     in task_rule_parm.max_vol%type
)
as
begin
    -- populate id using sort_id, exploded aid's
    merge into tmp_task_creation_selected_aid t
    using
    (
        with t1 as
        (
            select t2.alloc_invn_dtl_id, t2.qty_alloc, t2.vol/t2.qty_alloc ea_tmp,
                trunc(p_max_vol / (t2.vol/t2.qty_alloc)) max_ea_per_aid,
                ceil(t2.qty_alloc / trunc(p_max_vol / (t2.vol/t2.qty_alloc))) num_splits_per_aid
            from tmp_task_creation_selected_aid t2
            where t2.cntr_nbr is null and t2.carton_nbr is null and t2.vol > p_max_vol 
                and t2.qty_alloc > 1 and trunc(p_max_vol / (t2.vol/t2.qty_alloc)) > 0
                and p_max_vol != 999999999
        ),
        t3 as
        (
            select t4.alloc_invn_dtl_id, t4.qty_alloc, t4.wt/t4.qty_alloc ea_tmp,
                trunc(p_max_wt / (t4.wt/t4.qty_alloc)) max_ea_per_aid,
                ceil(t4.qty_alloc / trunc(p_max_wt / (t4.wt/t4.qty_alloc))) num_splits_per_aid
            from tmp_task_creation_selected_aid t4
            where t4.cntr_nbr is null and t4.carton_nbr is null and t4.wt > p_max_wt 
                and t4.qty_alloc > 1 and trunc(p_max_wt / (t4.wt/t4.qty_alloc)) > 0
                and p_max_wt != 999999999
        ),
        t6 as
        (
            select t1.alloc_invn_dtl_id, t1.qty_alloc, t1.ea_tmp, t1.max_ea_per_aid,
                t1.num_splits_per_aid
            from t1
            where not exists
            (
                select 1
                from t3
                where t3.alloc_invn_dtl_id = t1.alloc_invn_dtl_id
            )
            union all
            select t3.alloc_invn_dtl_id, t3.qty_alloc, t3.ea_tmp, t3.max_ea_per_aid,
                t3.num_splits_per_aid
            from t3
            where not exists
            (
                select 1
                from t1
                where t3.alloc_invn_dtl_id = t1.alloc_invn_dtl_id
            )
            union all
            select t3.alloc_invn_dtl_id, t3.qty_alloc, 
                case when t3.max_ea_per_aid < t1.max_ea_per_aid then t3.ea_tmp
                    else t1.ea_tmp end ea_tmp, 
                case when t3.max_ea_per_aid < t1.max_ea_per_aid then t3.max_ea_per_aid
                    else t1.max_ea_per_aid end max_ea_per_aid,
                case when t3.max_ea_per_aid < t1.max_ea_per_aid then t3.num_splits_per_aid
                    else t1.num_splits_per_aid end num_splits_per_aid
            from t3
            join t1 on t3.alloc_invn_dtl_id = t1.alloc_invn_dtl_id
        ),
        row_gen as
        (
            select rownum aid_seq_nbr
            from dual
            connect by rownum <= (select max(t6.num_splits_per_aid) from t6)
        ),
        t5 as
        (
            select t6.*, rg.aid_seq_nbr,
                sum(t6.max_ea_per_aid) over(partition by t6.alloc_invn_dtl_id 
                    order by rg.aid_seq_nbr) rng_sum_max_ea
            from t6
            join row_gen rg on rg.aid_seq_nbr <= t6.num_splits_per_aid
        )
        select t5.alloc_invn_dtl_id, t5.aid_seq_nbr, t2.parent_aid_id, t2.rule_sort_id, 
            t2.alloc_invn_code, t2.cntr_nbr, t2.pull_locn_id, t2.invn_need_type, t2.task_type, 
            t2.task_prty, t2.task_batch, t2.alloc_uom, t2.alloc_uom_qty, t2.vol, t2.wt,
            t2.dest_locn_id, t2.task_cmpl_ref_code, t2.task_cmpl_ref_nbr,
            t2.line_item_id, t2.carton_nbr, t2.uom, t2.labor_rate, t2.capcty, t2.xpectd_durtn, 
            t2.task_id, t2.task_seq_nbr, t2.cd_master_id, t2.break_list1, t2.break_list2, 
            t2.break_list3, t2.break_list4, t2.break_list5, t2.item_id, t2.th_task_cmpl_ref_code, 
            t2.th_task_cmpl_ref_nbr, t2.qty_alloc orig_qty_alloc,
            case when t5.rng_sum_max_ea <= t5.qty_alloc then t5.max_ea_per_aid 
                else t5.qty_alloc - t5.rng_sum_max_ea + t5.max_ea_per_aid end new_qty_alloc,
            t2.task_genrtn_ref_nbr, t2.task_genrtn_ref_code
        from t5
        join tmp_task_creation_selected_aid t2 on t2.alloc_invn_dtl_id = t5.alloc_invn_dtl_id
    ) iv on (iv.alloc_invn_dtl_id = t.alloc_invn_dtl_id and iv.aid_seq_nbr = 1)
    when matched then
    update set t.qty_alloc = iv.new_qty_alloc, t.vol = t.vol * new_qty_alloc / qty_alloc,
        t.wt = t.wt * new_qty_alloc / qty_alloc
    when not matched then
    insert
    (
        alloc_invn_dtl_id, parent_aid_id, rule_sort_id, alloc_invn_code, cntr_nbr, pull_locn_id,
        invn_need_type, task_type, task_prty, task_batch, alloc_uom, alloc_uom_qty, qty_alloc,
        dest_locn_id, task_cmpl_ref_code, task_cmpl_ref_nbr, line_item_id, carton_nbr,
        vol, wt, uom, labor_rate, capcty, xpectd_durtn, task_id, task_seq_nbr, cd_master_id,
        break_list1, break_list2, break_list3, break_list4, break_list5, item_id,
        th_task_cmpl_ref_code, th_task_cmpl_ref_nbr, task_genrtn_ref_nbr, task_genrtn_ref_code
    )
    values
    (
        alloc_invn_dtl_id_seq.nextval, coalesce(iv.parent_aid_id, iv.alloc_invn_dtl_id), 
        iv.rule_sort_id, iv.alloc_invn_code, iv.cntr_nbr, iv.pull_locn_id, iv.invn_need_type, 
        iv.task_type, iv.task_prty, iv.task_batch, iv.alloc_uom, iv.alloc_uom_qty, iv.new_qty_alloc,
        iv.dest_locn_id, iv.task_cmpl_ref_code, iv.task_cmpl_ref_nbr, iv.line_item_id, iv.carton_nbr, 
        iv.vol * iv.new_qty_alloc / iv.orig_qty_alloc, iv.wt * iv.new_qty_alloc / iv.orig_qty_alloc, 
        iv.uom, iv.labor_rate, iv.capcty, iv.xpectd_durtn, iv.task_id, iv.task_seq_nbr, 
        iv.cd_master_id, iv.break_list1, iv.break_list2, iv.break_list3, iv.break_list4, 
        iv.break_list5, iv.item_id, iv.th_task_cmpl_ref_code, iv.th_task_cmpl_ref_nbr, 
        iv.task_genrtn_ref_nbr, iv.task_genrtn_ref_code
    );
    wm_cs_log('Exploded large aid ' || sql%rowcount);
    
    -- resequence the entire list
    merge into tmp_task_creation_selected_aid t
    using
    (
        select t.alloc_invn_dtl_id, row_number() over(order by t.rule_sort_id, t.alloc_invn_dtl_id) id
        from tmp_task_creation_selected_aid t
    ) iv on (iv.alloc_invn_dtl_id = t.alloc_invn_dtl_id)
    when matched then
    update set t.id = iv.id;
    wm_cs_log('Sequenced all aids for tasking ' || sql%rowcount);
end;
/
show errors;

--Deleting the permission for the buttons in CLPNLOTMAPPING UI if exists as they are not required.
delete from UI_MENU_ITEM_PERMISSION where ui_menu_item_id in ( SELECT UI_MENU_ITEM_ID FROM ui_MENu_item where link in ('#{reyeCLPNLotMappingBackingBean.addActionDetail}', '#{reyeCLPNLotMappingBackingBean.editActionDetail}', '#{reyeCLPNLotMappingBackingBean.copyActionDetail}', 'return validateFieldBeforeSave()', 'deleteLot()'));
commit;
--Deleting the UI_MENU_ITEM entries as these were conflicting against the base entries
delete from UI_MENU_ITEM where link in ('#{reyeCLPNLotMappingBackingBean.addActionDetail}', '#{reyeCLPNLotMappingBackingBean.editActionDetail}', '#{reyeCLPNLotMappingBackingBean.copyActionDetail}', 'return validateFieldBeforeSave()', 'deleteLot()');
commit;
--Inserting new ScreenID '99990001' for "REYECLPNLotMapping.xhtml"

MERGE INTO UI_MENU_SCREEN MSCREEN
     USING (SELECT '99990001' SCREEN_ID FROM DUAL) B
        ON (MSCREEN.SCREEN_ID = B.SCREEN_ID
        AND MENU_ID='1')
WHEN NOT MATCHED
THEN
	INSERT 
		(UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID)
		  VALUES
			(UI_MENU_SCREEN_SEQ.nextval,
			 '1',
			 '99990001',
			 'SYSTEM',
			 null,
			 null);


--1st record "Add"

MERGE INTO UI_MENU_ITEM MITEM
     USING (SELECT '#{reyeCLPNLotMappingBackingBean.addActionDetail}' LINK FROM DUAL) B
        ON (MITEM.LINK = B.LINK)
WHEN NOT MATCHED
THEN
	Insert  
        (UI_MENU_ITEM_ID,
         UI_MENU_SCREEN_ID,
         MENU_ID,
         IS_DISPLAY,
         DISPLAY_TEXT,
         LINK,
         LINK_TYPE,
         LOCN_FLAG,
         ITEM_SEQ_NBR,
         UI_MENU_COMP_ID,
        IS_MORE,
         AJAX_ACTION,
         AJAX_RERENDER,
         AJAX_ONCOMPLETE,
         RENDERED,
         IMAGE_URL)
      values
        (UI_MENU_ITEM_SEQ.nextval,
         (select ui_menu_screen_id from UI_MENU_SCREEN where screen_id = '99990001' and menu_id='1'),
         '1',
         '0',
         'Add',
         '#{reyeCLPNLotMappingBackingBean.addActionDetail}',
         'JSFA',
         '7',
         (SELECT (COALESCE(max(item_seq_nbr), 0) + 1)
            FROM UI_MENU_ITEM
           WHERE UI_MENU_SCREEN_ID in (select ui_menu_screen_id from UI_MENU_SCREEN where screen_id = '99990001' and menu_id='1')),
         'dataForm:entityListView:dataTable',
         '0',
         null,
         null,
         null,
         'TRUE',
         null);


		 
		 
		 
--2nd record "Edit"
MERGE INTO UI_MENU_ITEM MITEM
     USING (SELECT '#{reyeCLPNLotMappingBackingBean.editActionDetail}' LINK FROM DUAL) B
        ON (MITEM.LINK = B.LINK)
WHEN NOT MATCHED
THEN
	Insert  
			(UI_MENU_ITEM_ID,
			 UI_MENU_SCREEN_ID,
			 MENU_ID,
			 IS_DISPLAY,
			 DISPLAY_TEXT,
			 LINK,
			 LINK_TYPE,
			 LOCN_FLAG,
			 ITEM_SEQ_NBR,
			 UI_MENU_COMP_ID,
			IS_MORE,
			 AJAX_ACTION,
			 AJAX_RERENDER,
			 AJAX_ONCOMPLETE,
			 RENDERED,
			 IMAGE_URL)
		  values
			(UI_MENU_ITEM_SEQ.nextval,
			 (select ui_menu_screen_id from UI_MENU_SCREEN where screen_id = '99990001' and menu_id='1'),
			 '1',
			 '0',
			 'Edit',
			 '#{reyeCLPNLotMappingBackingBean.editActionDetail}',
			 'JSFA',
			 '7',
			 (SELECT (COALESCE(max(item_seq_nbr), 0) + 1)
				FROM UI_MENU_ITEM
			   WHERE UI_MENU_SCREEN_ID in (select ui_menu_screen_id from UI_MENU_SCREEN where screen_id = '99990001' and menu_id='1')),
			 'dataForm:entityListView:dataTable',
			 '0',
			 null,
			 null,
			 null,
			 'TRUE',
			 null);
		 
--3rd Record "Copy"

MERGE INTO UI_MENU_ITEM MITEM
     USING (SELECT '#{reyeCLPNLotMappingBackingBean.copyActionDetail}' LINK FROM DUAL) B
        ON (MITEM.LINK = B.LINK)
WHEN NOT MATCHED
THEN
	Insert  
			(UI_MENU_ITEM_ID,
			 UI_MENU_SCREEN_ID,
			 MENU_ID,
			 IS_DISPLAY,
			 DISPLAY_TEXT,
			 LINK,
			 LINK_TYPE,
			 LOCN_FLAG,
			 ITEM_SEQ_NBR,
			 UI_MENU_COMP_ID,
			IS_MORE,
			 AJAX_ACTION,
			 AJAX_RERENDER,
			 AJAX_ONCOMPLETE,
			 RENDERED,
			 IMAGE_URL)
		  values
			(UI_MENU_ITEM_SEQ.nextval,
			 (select ui_menu_screen_id from UI_MENU_SCREEN where screen_id = '99990001' and menu_id='1'),
			 '1',
			 '0',
			 'Copy',
			 '#{reyeCLPNLotMappingBackingBean.copyActionDetail}',
			 'JSFA',
			 '7',
			 (SELECT (COALESCE(max(item_seq_nbr), 0) + 1)
				FROM UI_MENU_ITEM
			   WHERE UI_MENU_SCREEN_ID in (select ui_menu_screen_id from UI_MENU_SCREEN where screen_id = '99990001' and menu_id='1')),
			 'dataForm:entityListView:dataTable',
			 '0',
			 null,
			 null,
			 null,
			 'TRUE',
			 null);
		 
		 

--4th record "Save" 
MERGE INTO UI_MENU_ITEM MITEM
     USING (SELECT 'return validateFieldBeforeSave()' LINK FROM DUAL) B
        ON (MITEM.LINK = B.LINK)
WHEN NOT MATCHED
THEN		 
	Insert  
			(UI_MENU_ITEM_ID,
			 UI_MENU_SCREEN_ID,
			 MENU_ID,
			 IS_DISPLAY,
			 DISPLAY_TEXT,
			 LINK,
			 LINK_TYPE,
			 LOCN_FLAG,
			 ITEM_SEQ_NBR,
			 UI_MENU_COMP_ID,
			IS_MORE,
			 AJAX_ACTION,
			 AJAX_RERENDER,
			 AJAX_ONCOMPLETE,
			 RENDERED,
			 IMAGE_URL)
		  values
			(UI_MENU_ITEM_SEQ.nextval,
			 (select ui_menu_screen_id from UI_MENU_SCREEN where screen_id = '99990001' and menu_id='1'),
			 '1',
			 '0',
			 'Save',
			 'return validateFieldBeforeSave()',
			 'JS',
			 '7',
			 (SELECT (COALESCE(max(item_seq_nbr), 0) + 1)
				FROM UI_MENU_ITEM
			   WHERE UI_MENU_SCREEN_ID in (select ui_menu_screen_id from UI_MENU_SCREEN where screen_id = '99990001' and menu_id='1')),
			 'dataForm:syncTab',
			 '0',
			 null,
			 null,
			 null,
			 '#{reyeCLPNLotMappingBackingBean.isEditClicked || reyeCLPNLotMappingBackingBean.isAdd}',
			 null);
		 
		 
--5th record "Delete"
MERGE INTO UI_MENU_ITEM MITEM
     USING (SELECT 'deleteLot()' LINK FROM DUAL) B
        ON (MITEM.LINK = B.LINK)
WHEN NOT MATCHED
THEN
	Insert  
			(UI_MENU_ITEM_ID,
			 UI_MENU_SCREEN_ID,
			 MENU_ID,
			 IS_DISPLAY,
			 DISPLAY_TEXT,
			 LINK,
			 LINK_TYPE,
			 LOCN_FLAG,
			 ITEM_SEQ_NBR,
			 UI_MENU_COMP_ID,
			IS_MORE,
			 AJAX_ACTION,
			 AJAX_RERENDER,
			 AJAX_ONCOMPLETE,
			 RENDERED,
			 IMAGE_URL)
		  values
			(UI_MENU_ITEM_SEQ.nextval,
			 (select ui_menu_screen_id from UI_MENU_SCREEN where screen_id = '99990001' and menu_id='1'),
			 '1',
			 '0',
			 'Delete',
			 'deleteLot()',
			 'JS',
			 '7',
			 (SELECT (COALESCE(max(item_seq_nbr), 0) + 1)
				FROM UI_MENU_ITEM
			   WHERE UI_MENU_SCREEN_ID in (select ui_menu_screen_id from UI_MENU_SCREEN where screen_id = '99990001' and menu_id='1')),
			 'dataForm:entityListView:dataTable',
			 '0',
			 null,
			 null,
			 null,
			 'TRUE',
			 null); 
			 
COMMIT;			 
CREATE OR REPLACE procedure wm_auton_find_or_create_shpmt
(
    p_date              in date,
    p_ship_via          in ship_via.ship_via%type,
    p_order_id          in orders.order_id%type,
    p_user_id           in varchar2,
    p_is_static_retail_rte_flow in number,
    p_stat_route_ind    in number,
    p_carrier_id        carrier_code.carrier_id%type,
    p_carrier_code      carrier_code.carrier_code%type,
    p_mot_id            ship_via.mot_id%type,
    p_service_level_id  ship_via.service_level_id%type,
    p_sv_tc_company_id  company.company_id%type,
    p_out_shipment_id   out shipment.shipment_id%type
)
as
    pragma autonomous_transaction;
    v_time_zone_id      postal_code.time_zone_id%type;
    v_root_bu           company.company_id%type;
    v_sql               varchar2(1000);
    v_tc_shipment_id    shipment.tc_shipment_id%type;
begin
    select c.company_id
    into v_root_bu
    from company c
    where c.parent_company_id = -1
    start with c.company_id = p_sv_tc_company_id
    connect by nocycle prior c.parent_company_id = c.company_id;
    -- lock postal code
    begin
        select pc.time_zone_id
        into v_time_zone_id
        from postal_code pc
        where exists
        (
            select 1
            from orders o
            where o.order_id = p_order_id and upper(pc.city) = upper(o.d_city)
                and pc.state_prov = o.d_state_prov
                and pc.postal_code = o.d_postal_code
                and pc.country_code = o.d_country_code
        )
        for update;
    exception
        when others then
            raise;
    end;

    -- search again
     if (p_is_static_retail_rte_flow = 1)
     then

         -- find an existing shpmt that matches the order found above
         with giv as
         (
             select o.d_facility_id, o.o_facility_id,
                 o.billing_method, o.d_address_1, o.d_address_2, o.d_address_3,
                 o.d_state_prov, o.d_postal_code, o.d_county, o.d_country_code, d_city
             from orders o
             where o.order_id = p_order_id
         )
        select max(s.shipment_id)
         into p_out_shipment_id
         from shipment s
         join giv on 1 = 1
         left join manifest_hdr mh on mh.manifest_id = s.manifest_id
         where trunc(s.scheduled_pickup_dttm) = p_date and s.assigned_ship_via = p_ship_via
             and s.shipment_closed_indicator = 0 and s.lpn_assignment_stopped = 'N'
             and s.is_cancelled = 0 and s.has_import_error = 0
             and s.creation_type in (20, 40) and s.wms_status_code < 55
             and s.o_facility_number	= giv.o_facility_id
             and s.tc_company_id = p_sv_tc_company_id
             and coalesce(s.billing_method,-1)	= coalesce(giv.billing_method, -1)
             and coalesce(mh.manifest_status_id, 10) = 10
             and
             (
                 s.d_facility_number = giv.d_facility_id
                 or
                 (
                     giv.d_facility_id is null
                     and exists
                     (
                         select 1
                         from stop st
                         where st.shipment_id = s.shipment_id and st.stop_seq = 2
                             and coalesce(st.address_1, ' ') = coalesce(giv.d_address_1, ' ')
                             and coalesce(st.address_2, ' ') = coalesce(giv.d_address_2, ' ')
                             and coalesce(st.address_3, ' ') = coalesce(giv.d_address_3, ' ')
                             and coalesce(st.city, ' ') = coalesce(giv.d_city, ' ')
                             and coalesce(st.state_prov, ' ') = coalesce(giv.d_state_prov, ' ')
                             and coalesce(st.postal_code, ' ') = coalesce(giv.d_postal_code, ' ')
                             and coalesce(st.county, ' ') = coalesce(giv.d_county, ' ')
                              and coalesce(st.country_code, ' ') = coalesce(giv.d_country_code, ' ')
                     )
                 )
             )
             and
             case
             when (p_stat_route_ind = 0) and
             not exists
             (
                 select 1
                 from lpn l
                 where l.shipment_id = s.shipment_id
                     and l.static_route_id is not null
             ) then 1
             when (p_stat_route_ind = 1) and
             not exists
             (
                 select 1
                 from lpn l
                 where l.shipment_id = s.shipment_id
                     and l.static_route_id is null
             )then 1
             end = 1;
     else
         -- find an existing shpmt that matches the order found above
         with giv as
         (
             select o.d_facility_id, o.o_facility_id,
                 o.billing_method, o.d_address_1, o.d_address_2, o.d_address_3,
                 o.d_state_prov, o.d_postal_code, o.d_county, o.d_country_code, d_city
             from orders o
             where o.order_id = p_order_id
         )
        select max(s.shipment_id)
         into p_out_shipment_id
         from shipment s
         join giv on 1 = 1
         left join manifest_hdr mh on mh.manifest_id = s.manifest_id
         where trunc(s.scheduled_pickup_dttm) = p_date and s.assigned_ship_via = p_ship_via
             and s.shipment_closed_indicator = 0 and s.lpn_assignment_stopped = 'N'
             and s.is_cancelled = 0 and s.has_import_error = 0
             and s.creation_type in (20, 40) and s.wms_status_code < 55
             and s.o_facility_number	= giv.o_facility_id
             and s.tc_company_id = p_sv_tc_company_id
             and coalesce(s.billing_method,-1)	= coalesce(giv.billing_method, -1)
             and coalesce(mh.manifest_status_id, 10) = 10
             and
             (
                 s.d_facility_number = giv.d_facility_id
                 or
                 (
                     giv.d_facility_id is null
                     and exists
                     (
                         select 1
                         from stop st
                         where st.shipment_id = s.shipment_id and st.stop_seq = 2
                             and coalesce(st.address_1, ' ') = coalesce(giv.d_address_1, ' ')
                             and coalesce(st.address_2, ' ') = coalesce(giv.d_address_2, ' ')
                             and coalesce(st.address_3, ' ') = coalesce(giv.d_address_3, ' ')
                             and coalesce(st.city, ' ') = coalesce(giv.d_city, ' ')
                             and coalesce(st.state_prov, ' ') = coalesce(giv.d_state_prov, ' ')
                             and coalesce(st.postal_code, ' ') = coalesce(giv.d_postal_code, ' ')
                             and coalesce(st.county, ' ') = coalesce(giv.d_county, ' ')
                              and coalesce(st.country_code, ' ') = coalesce(giv.d_country_code, ' ')
                     )
                 )
             );
     end if;

    if (p_out_shipment_id is null)
    then
        -- create a new shipment
        p_out_shipment_id := shipment_id_seq.nextval;

        v_sql := ' select seq_shipment_id_' || to_char(v_root_bu) || '.nextval'
            || ' from dual ';
        execute immediate v_sql into v_tc_shipment_id;
        insert into shipment
        (
            shipment_id, tc_company_id, tc_shipment_id, shipment_status,
            update_sent, creation_type, is_cancelled, cycle_execution_dttm,
            o_facility_number, o_facility_id, o_address, o_city, o_state_prov,
            o_postal_code, o_county, o_country_code, d_facility_number,
            d_facility_id, d_address, d_city, d_state_prov, d_postal_code,
            d_county, d_country_code, billing_method, assigned_carrier_code,
            pickup_start_dttm, pickup_end_dttm, delivery_start_dttm,
            delivery_end_dttm, num_stops, num_docks, pickup_tz, delivery_tz,
            o_stop_location_name, d_stop_location_name, status_change_dttm,
            shipment_type, assigned_carrier_id, assigned_mot_id,
            assigned_service_level_id, region_id, inbound_region_id,
            outbound_region_id, created_source_type,
            created_source, created_dttm, last_updated_source_type,
            last_updated_source, last_updated_dttm, lpn_assignment_stopped,
            assigned_ship_via, trans_plan_owner, trans_resp_code, is_hazmat,
            has_notes, has_alerts, has_tracking_msg, tracking_msg_problem,
            scheduled_pickup_dttm, wms_status_code
        )
        select p_out_shipment_id, p_sv_tc_company_id, 'CS' || lpad(to_char(v_tc_shipment_id), 8 , '0'),
            case when p_ship_via is null then 20 else 60 end, 0, 20, 0, sysdate,
            o.o_facility_id, o.o_facility_alias_id,
            o.o_address_1 || o.o_address_2 || o.o_address_3, o.o_city, o.o_state_prov,
            o.o_postal_code, o.o_county, o.o_country_code, o.d_facility_id,
            o.d_facility_alias_id, o.d_address_1 || o.d_address_2 || o.d_address_3,
            o.d_city, o.d_state_prov, o.d_postal_code,
            o.d_county, o.d_country_code, o.billing_method, p_carrier_code,
            p_date, p_date, p_date, p_date, 2, 2, coalesce(orig.facility_tz, 3),
            coalesce(dest.facility_tz, 3), orig_fa.facility_name,
            dest_fa.facility_name, sysdate, 'STD', p_carrier_id, p_mot_id,
            p_service_level_id, orig.inbound_region_id,
            orig.inbound_region_id, orig.outbound_region_id, 5, p_user_id,
            sysdate, 5, p_user_id, sysdate, 'N', p_ship_via, 1, 'SHP', o.is_hazmat,
            0 has_notes, 0 has_alerts, 0 has_tracking_msg, 0 tracking_msg_problem,
            p_date, 0
        from orders o
        join facility orig on orig.facility_id = o.o_facility_id
-- check: left join here?
        left join facility dest on dest.facility_id = o.d_facility_id
        left join facility_alias orig_fa on orig_fa.facility_id = o.o_facility_id
            and orig_fa.is_primary = 1
        left join facility_alias dest_fa on dest_fa.facility_id = o.d_facility_id
            and dest_fa.is_primary = 1
        where o.order_id = p_order_id;

        insert into stop
        (
            shipment_id, stop_seq, stop_location_name, facility_alias_id,
            facility_id, address_1, address_2, address_3, city, state_prov,
            postal_code, county, country_code, stop_tz, stop_status, tc_company_id
        )
        with giv as
        (
            select s.o_stop_location_name, s.d_stop_location_name, s.o_facility_id,
                s.d_facility_id, s.o_facility_number, s.d_facility_number,s.tc_company_id,
                coalesce(s.delivery_tz, v_time_zone_id, 3) delivery_tz,
                coalesce(s.pickup_tz, 3) pickup_tz
            from shipment s
            where s.shipment_id = p_out_shipment_id
        )
        select p_out_shipment_id, iv.stop_seq,
            decode(iv.stop_seq, 1, giv.o_stop_location_name, giv.d_stop_location_name),
            decode(iv.stop_seq, 1, giv.o_facility_id, giv.d_facility_id),
            decode(iv.stop_seq, 1, giv.o_facility_number, giv.d_facility_number),
            decode(iv.stop_seq, 1, o.o_address_1, o.d_address_1),
            decode(iv.stop_seq, 1, o.o_address_2, o.d_address_2),
            decode(iv.stop_seq, 1, o.o_address_3, o.d_address_3),
            decode(iv.stop_seq, 1, o.o_city, o.d_city),
            decode(iv.stop_seq, 1, o.o_state_prov, o.d_state_prov),
            decode(iv.stop_seq, 1, o.o_postal_code, o.d_postal_code),
            decode(iv.stop_seq, 1, o.o_county, o.d_county),
            decode(iv.stop_seq, 1, o.o_country_code, o.d_country_code),
            decode(iv.stop_seq, 1, giv.pickup_tz, giv.delivery_tz), 10,
            giv.tc_company_id
        from orders o, giv
        join
        (
            select 1 stop_seq from dual
            union all
            select 2 stop_seq from dual
        ) iv on 1 = 1
        where o.order_id = p_order_id;

        insert into stop_action
        (
            shipment_id, stop_seq, stop_action_seq, action_type
        )
        select s.shipment_id, s.stop_seq, 1, decode(s.stop_seq, 1, 'PU', 'DL')
        from stop s
        where s.shipment_id = p_out_shipment_id;
    end if;

        commit;
end;
/
show errors;

create or replace procedure wm_load_pw_lpn_data
(
    p_facility_id	        in facility.facility_id%type,
    p_pick_wave_nbr	      in ship_wave_parm.pick_wave_nbr%type,
    p_incl_actv_flag	    in pack_wave_parm_hdr.incl_actv_flag%type,
    p_incl_resv_flag	    in pack_wave_parm_hdr.incl_resv_flag%type,
    p_incl_case_pick_flag in pack_wave_parm_hdr.incl_case_pick_flag%type,
    p_incl_mxd_flag	      in pack_wave_parm_hdr.inc_mxd_flag%type,
    p_chk_critcl_dim_flag in pack_wave_parm_hdr.chk_critcl_dim_flag%type
)
as
begin
    -- the invn_from logic is from C++ code
    if (p_chk_critcl_dim_flag in ('1', 'Y'))
    then
        insert into tmp_pack_wave_selected_lpns
        (
            lpn_id, tc_lpn_id, vol, wt, units, order_id, id, critcl_dim_1,
            critcl_dim_2, critcl_dim_3
        )
        select l.lpn_id, l.tc_lpn_id, 
            sum(coalesce(nullif(pd.unit_vol, 0), ic.unit_volume) * ld.initial_qty),
            sum(coalesce(nullif(pd.unit_wt, 0), ic.unit_weight) * ld.initial_qty), 
            l.total_lpn_qty, l.order_id, 
            row_number() over(order by min(pd.wave_seq_nbr), l.order_id, l.lpn_id),
            max
            (
                case when iw.critcl_dim_1 >= iw.critcl_dim_2
                    and iw.critcl_dim_1 >= iw.critcl_dim_3 then iw.critcl_dim_1
                when iw.critcl_dim_2 >= iw.critcl_dim_1
                    and iw.critcl_dim_2 >= iw.critcl_dim_3 then iw.critcl_dim_2
                else iw.critcl_dim_3
                end
            ) critcl_dim_1,
            max
            (
                case when iw.critcl_dim_1 >= iw.critcl_dim_2
                    and iw.critcl_dim_1 <= iw.critcl_dim_3 then iw.critcl_dim_1
                when iw.critcl_dim_2 >= iw.critcl_dim_1
                    and iw.critcl_dim_2 <= iw.critcl_dim_3 then iw.critcl_dim_2
                else iw.critcl_dim_3
          end
            ) critcl_dim_2,
            max
            (
                case when iw.critcl_dim_1 <= iw.critcl_dim_2
                    and iw.critcl_dim_1 <= iw.critcl_dim_3 then iw.critcl_dim_1
                when iw.critcl_dim_2 <= iw.critcl_dim_1
                    and iw.critcl_dim_2 <= iw.critcl_dim_3 then iw.critcl_dim_2
                else iw.critcl_dim_3
                end
            ) critcl_dim_3
        from lpn l
        join lpn_detail ld on ld.lpn_id = l.lpn_id
        join item_cbo ic on ic.item_id = ld.item_id
        join pkt_dtl pd on pd.pkt_ctrl_nbr = ld.internal_order_id
            and pd.pkt_seq_nbr = ld.internal_order_dtl_id
        join item_wms iw on iw.item_id = ld.item_id
        where l.c_facility_id = p_facility_id and l.wave_nbr = p_pick_wave_nbr
            and l.inbound_outbound_indicator = 'O'
            and
            (
                (p_incl_actv_flag in ('1', 'Y')
                    and l.lpn_creation_code in (4, 5))
                or (p_incl_resv_flag in ('1', 'Y')
                    and l.lpn_creation_code in (1, 2, 7))
                or (p_incl_case_pick_flag in ('1', 'Y')
                    and l.lpn_creation_code in (3, 6))
                or (p_incl_mxd_flag in ('1', 'Y')
                    and l.lpn_creation_code in (11, 12, 13, 18))
            )
        group by l.lpn_id, l.order_id, l.tc_lpn_id, l.total_lpn_qty;
    else
        -- do not retrieve sku dimension information
        insert into tmp_pack_wave_selected_lpns
        (
            lpn_id, vol, wt, units, critcl_dim_1, critcl_dim_2,
            critcl_dim_3, tc_lpn_id, order_id, id
        )
        select l.lpn_id,
            sum(coalesce(nullif(pd.unit_vol, 0), ic.unit_volume) * ld.initial_qty),
            sum(coalesce(nullif(pd.unit_wt, 0), ic.unit_weight) * ld.initial_qty),
            l.total_lpn_qty, 0 critcl_dim_1, 0 critcl_dim_2, 0 critcl_dim_3, 
            l.tc_lpn_id, l.order_id, 
            row_number() over(order by min(pd.wave_seq_nbr), l.order_id, l.lpn_id)
        from lpn l
        join lpn_detail ld on ld.lpn_id = l.lpn_id
        join item_cbo ic on ic.item_id = ld.item_id
        join pkt_dtl pd on pd.pkt_ctrl_nbr = ld.internal_order_id
            and pd.pkt_seq_nbr = ld.internal_order_dtl_id
        where l.c_facility_id = p_facility_id and l.wave_nbr = p_pick_wave_nbr
            and l.inbound_outbound_indicator = 'O'
            and
            (
                (p_incl_actv_flag in ('1', 'Y')
                    and l.lpn_creation_code in (4, 5))
                or (p_incl_resv_flag in ('1', 'Y')
                    and l.lpn_creation_code in (1, 2, 7))
                or (p_incl_case_pick_flag in ('1', 'Y')
                    and l.lpn_creation_code in (3, 6))
                or (p_incl_mxd_flag in ('1', 'Y')
                    and l.lpn_creation_code in (11, 12, 13, 18))
            )
        group by l.order_id, l.lpn_id, l.total_lpn_qty, l.tc_lpn_id;
    end if;
    wm_cs_log('Loaded lpns ' || sql%rowcount);
end;
/
show errors;

CREATE OR REPLACE PROCEDURE Xmllaneupload(I_RFPID INTEGER) AS

  RFPMODE         INTEGER;
  TC              INTEGER;
  RATINGROUTING   INTEGER;
  S_ALLOWPACKAGES INTEGER;

  HASSEASON INTEGER;
  HASEQUIPMENT INTEGER;
  HASSERVICE INTEGER;
  HASPROTECTION INTEGER;
  HASORIGINFAC INTEGER;
  HASDESTFAC   INTEGER;

  HASORIGINSTATE   INTEGER;
  HASDESTSTATE     INTEGER;
  HASORIGINCOUNTRY INTEGER;
  HASDESTCOUNTRY   INTEGER;
  HASORIGINZIP     INTEGER;
  HASORIGINZONE    INTEGER;
  HASDESTZIP       INTEGER;
  HASORIGINCITY    INTEGER;
  HASDESTCITY      INTEGER;
  HASDESTZONE      INTEGER;

  ROUNDNUM                 INTEGER;
  CURRENTATTRIBUTEVERSION  INTEGER;
  PREVIOUSATTRIBUTEVERSION INTEGER;
  CUSTOM_FIELD_CONDITIONS  VARCHAR2(2000);
  L_COMPLEX_OPTIMIZATION   INTEGER;

  V_DYNSQL          VARCHAR2(1000);
  S_RFPID           INTEGER;
  S_OBCARRIERCODEID INTEGER;
  S_PACKAGENAME     VARCHAR2(40);
  S_ROUND_NUM       INTEGER;
  S_LANEID          INTEGER;
  K                 INTEGER;
  TMPCONDITION      INTEGER;
  TYPE CURSOR_TYPE IS REF CURSOR;
  CURSOR_CUSTOM_FIELD CURSOR_TYPE;

  S_COLNAME VARCHAR2(32);

  VALSEQ             INTEGER;
  VALLANEID          INTEGER;
  VALRFPID           INTEGER;
  VALVOLUME          NUMBER(24, 3);
  VALFREQUENCY       INTEGER;
  VALCREATEDUID      VARCHAR2(40);
  VALCREATEDDTTM     VARCHAR2(40);
  VALLASTUPDATEDUID  VARCHAR2(40);
  VALLASTUPDATEDDTTM VARCHAR2(40);
  VALPROTECTIONLEVEL_ID INTEGER;
  VALEQUIPMENTTYPE_ID INTEGER;
  VALSERVICETYPE_ID     INTEGER;
  VALMOT             VARCHAR2(16);
  VALCOMMODITYCODE_ID INTEGER;
  VALROUNDNUM        INTEGER;

  V_EQUIPMENTTYPE_ID LANEEQUIPMENTTYPE.EQUIPMENTTYPE_ID%TYPE;
  V_SERVICETYPE_ID     LANEEQUIPMENTTYPE.SERVICETYPE_ID%TYPE;
  V_PROTECTIONLEVEL_ID LANEEQUIPMENTTYPE.PROTECTIONLEVEL_ID%TYPE;
  V_MOT		    LANEEQUIPMENTTYPE.MOT%TYPE;
  V_COMMODITYCODE_ID LANEEQUIPMENTTYPE.COMMODITYCODE_ID%TYPE;

  --err_num         INTEGER;
  --isam_err_num    INTEGER;
  --error_data_var  VARCHAR2(200);

  C_UPLOAD_TYPE_ID INTEGER := 1; -- used for xmlerror type
  C_ERRORS         INTEGER; -- used to check if there were errors in uploading lanes

  CURSOR CURSOR_TEST IS SELECT                      LANEID,
                                                    RFPID,
                                                    VOLUME,
                                                    FREQUENCY,
                                                    CREATEDUID,
                                                    CREATEDDTTM,
                                                    LASTUPDATEDUID,
                                                    LASTUPDATEDDTTM,
                                                    PROTECTIONLEVEL_ID,
                                                    EQUIPMENTTYPE_ID,
                                                    MOT,
                                                    SERVICETYPE_ID,
                                                    COMMODITYCODE_ID,
                                                    ROUNDNUM ROUND_NUM

                                            FROM   XMLLANEEQUIPMENTTYPE XLET
                                            WHERE  XLET.RFPID = I_RFPID
                                            AND    NOT EXISTS
                                             (SELECT 1
                                                    FROM   LANEEQUIPMENTTYPE LE
                                                    WHERE  LE.RFPID =
                                                           XLET.RFPID
                                                    AND    LE.LANEID =
                                                           XLET.LANEID
                                                    AND    (((LE.PROTECTIONLEVEL_ID IS NULL) AND
                                                          (XLET.PROTECTIONLEVEL_ID IS NULL)) OR
                                                          ((LE.PROTECTIONLEVEL_ID IS NOT NULL) AND
                                                          (XLET.PROTECTIONLEVEL_ID IS NOT NULL) AND
                                                          (XLET.PROTECTIONLEVEL_ID =
                                                          LE.PROTECTIONLEVEL_ID)))
                                                    AND    (((LE.SERVICETYPE_ID IS NULL) AND
                                                          (XLET.SERVICETYPE_ID IS NULL)) OR
                                                          ((LE.SERVICETYPE_ID IS NOT NULL) AND
                                                          (XLET.SERVICETYPE_ID IS NOT NULL) AND
                                                          (XLET.SERVICETYPE_ID =
                                                          LE.SERVICETYPE_ID)))
 						    AND    (((LE.MOT IS NULL) AND
                                                          (XLET.MOT IS NULL)) OR
                                                          ((LE.MOT IS NOT NULL) AND
                                                          (XLET.MOT IS NOT NULL) AND
                                                          (XLET.MOT =
                                                          LE.MOT)))
						    AND  (((LE.COMMODITYCODE_ID IS NULL) AND
							  (XLET.COMMODITYCODE_ID IS NULL)) OR
							  ((LE.COMMODITYCODE_ID IS NOT NULL) AND
							  (XLET.COMMODITYCODE_ID IS NOT NULL) AND
							  (XLET.COMMODITYCODE_ID =
							  LE.COMMODITYCODE_ID)))
                                                    AND    (((LE.EQUIPMENTTYPE_ID IS NULL) AND
                                                          (XLET.EQUIPMENTTYPE_ID IS NULL)) OR
                                                          ((LE.EQUIPMENTTYPE_ID IS NOT NULL) AND
                                                          (XLET.EQUIPMENTTYPE_ID IS NOT NULL) AND
                                                          (XLET.EQUIPMENTTYPE_ID =
                                                          LE.EQUIPMENTTYPE_ID)))) ORDER BY LANEEQUIPMENTTYPEID ASC;
BEGIN
-- get current round for rfp
  SELECT MAX(ROUND_NUM) INTO ROUNDNUM FROM RFP WHERE RFPID = I_RFPID;

-- get rfp info : mode, shipper id, trasnfered to CM, packages allowed */
  SELECT MODEID, TCCOMPANYID, EXPORTTORATINGROUTING, ALLOWPACKAGES
  INTO   RFPMODE, TC, RATINGROUTING, S_ALLOWPACKAGES
  FROM   RFP
  WHERE  RFPID = I_RFPID
  AND    ROUND_NUM = ROUNDNUM;

-- get if Equipment is selected in rfp lane customization
  SELECT COUNT(*)
  INTO HASEQUIPMENT
  FROM RFPCOLPROPERTIES
  WHERE  RFPID = I_RFPID
  AND  ROUND_NUM = ROUNDNUM
  AND  COLNAME = 'EQUIPMENTTYPE_ID';
-- get if Service level is selected in rfp lane customization
  SELECT COUNT(*)
  INTO HASSERVICE
  FROM RFPCOLPROPERTIES
  WHERE  RFPID = I_RFPID
  AND  ROUND_NUM = ROUNDNUM
  AND  COLNAME = 'SERVICETYPE_ID';
-- get if Protection level is selected in rfp lane customization
  SELECT COUNT(*)
  INTO HASPROTECTION
  FROM RFPCOLPROPERTIES
  WHERE  RFPID = I_RFPID
  AND  ROUND_NUM = ROUNDNUM
  AND  COLNAME = 'PROTECTIONLEVEL_ID';
-- get if Season is selected in rfp lane customization
  SELECT COUNT(*)
  INTO HASSEASON
  FROM RFPCOLPROPERTIES
  WHERE  RFPID = I_RFPID
  AND  ROUND_NUM = ROUNDNUM
  AND  COLNAME = 'SEASON_ID';

-- get if origin facility is selected in rfp lane customization
  SELECT COUNT(*)
  INTO   HASORIGINFAC
  FROM   RFPCOLPROPERTIES
  WHERE  RFPID = I_RFPID
  AND    ROUND_NUM = ROUNDNUM
  AND    COLNAME = 'ORIGINFACILITYCODE'
  ;

-- get if destination facility is selected in rfp lane customization
  SELECT COUNT(*)
  INTO   HASDESTFAC
  FROM   RFPCOLPROPERTIES
  WHERE  RFPID = I_RFPID
  AND    ROUND_NUM = ROUNDNUM
  AND    COLNAME = 'DESTINATIONFACILITYCODE';



-- get current attribute version
  SELECT LANEATTRIBUTEVERSION
  INTO   CURRENTATTRIBUTEVERSION
  FROM   RFP
  WHERE  RFPID = I_RFPID
  AND    ROUND_NUM = ROUNDNUM;

-- get previous attrivute version

---- begin: round num condition1
  IF ROUNDNUM = 1
  THEN
    PREVIOUSATTRIBUTEVERSION := CURRENTATTRIBUTEVERSION;
  ELSE
    SELECT LANEATTRIBUTEVERSION
    INTO   PREVIOUSATTRIBUTEVERSION
    FROM   RFP
    WHERE  RFPID = I_RFPID
    AND    ROUND_NUM = ROUNDNUM - 1;

  END IF;
---- end round num condition1



-- check for rebid lane uploads where lane.lane_type is rebid lane

  --ok
  INSERT INTO XMLERROR
    (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)
    SELECT C_UPLOAD_TYPE_ID,
           RFPID,
           LANEID,
           'cannot overwrite rebid lane : (laneid=' || LANEID || ')'
    FROM   XMLLANE
    WHERE  RFPID = I_RFPID
    AND    LANEID IN (SELECT LANEID
                      FROM   LANE
                      WHERE  RFPID = I_RFPID
                      AND    LANE_TYPE = 1);

-- check for additional lane uploads where lane.lane_type is rebid lane


  INSERT INTO XMLERROR
    (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)
    SELECT C_UPLOAD_TYPE_ID,
           RFPID,
           LANEID,
           'cannot overwrite additional lane: (laneid=' || LANEID || ')'
    FROM   XMLLANE
    WHERE  RFPID = I_RFPID
    AND    LANEID IN (SELECT LANEID
                      FROM   LANE
                      WHERE  RFPID = I_RFPID
                      AND    LANE_TYPE = 2);

-- checking for duplicate laneid

  INSERT INTO XMLERROR
    (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)
    SELECT C_UPLOAD_TYPE_ID, RFPID, LANEID, 'invalid duplicate laneid'
    FROM   XMLLANE
    WHERE  RFPID = I_RFPID
    GROUP  BY RFPID, LANEID
    HAVING COUNT(*) > 1;

 IF HASSEASON > 0
 THEN
   INSERT INTO XMLERROR
   (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)
   SELECT C_UPLOAD_TYPE_ID,
      XL.RFPID,
      XL.LANEID,
      'business unit is not compatible with season code'
   FROM  XMLLANE XL
   WHERE  XL.RFPID = I_RFPID
   AND  XL.SEASON_ID IS NOT NULL
   AND  NOT EXISTS
    (SELECT 1
     FROM XMLLANE_SEASON XLS
     WHERE
	XLS.TC_COMPANY_ID = XL.BUSINESS_UNIT_ID AND
	XLS.RFPID = I_RFPID AND
     XLS.CODE_ID = XL.SEASON_ID);
 END IF;

  -- checking for bad equipment which doesnot exist in EQUIPMENT table');

  INSERT INTO XMLERROR
    (UPLOAD_TYPE_ID, RFPID, LANEID, LANEEQUIPMENTTYPEID, ERRORDESC)

    SELECT C_UPLOAD_TYPE_ID,
           RFPID,
           LANEID,
           LANEEQUIPMENTTYPEID,
           'invalid equipment'
    FROM   XMLLANEEQUIPMENTTYPE XLET
    WHERE  RFPID = I_RFPID
    AND    EQUIPMENTTYPE_ID IS NOT NULL
    AND    NOT EXISTS
     (SELECT 1
            FROM   EQUIPMENT E
            WHERE  --E.TC_COMPANY_ID = TC  AND
            	E.EQUIPMENT_ID = XLET.EQUIPMENTTYPE_ID);

  INSERT INTO XMLERROR
  (UPLOAD_TYPE_ID, RFPID, LANEID, LANEEQUIPMENTTYPEID, ERRORDESC)
  SELECT C_UPLOAD_TYPE_ID,
     XLET.RFPID,
     XLET.LANEID,
     XLET.LANEEQUIPMENTTYPEID,
     'business unit is not compatible with equipment'
  FROM XMLLANEEQUIPMENTTYPE XLET, XMLLANE XL
  WHERE  XLET.RFPID = I_RFPID
  AND  XLET.EQUIPMENTTYPE_ID IS NOT NULL
  AND XLET.RFPID = XL.RFPID
  AND XLET.LANEID = XL.LANEID
  AND  NOT EXISTS
   (SELECT 1
    FROM XMLLANE_EQUIPMENT XLE
    WHERE
    	XLE.TC_COMPANY_ID = XL.BUSINESS_UNIT_ID AND
    	XLE.RFPID = I_RFPID AND
    XLE.CODE_ID = XLET.EQUIPMENTTYPE_ID);

 IF HASEQUIPMENT > 0
 THEN
   INSERT INTO XMLERROR
   (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)
   SELECT C_UPLOAD_TYPE_ID,
      XL.RFPID,
      XL.LANEID,
      'business unit is not compatible with equipment'
   FROM  XMLLANE XL
   WHERE  XL.RFPID = I_RFPID
   AND  XL.EQUIPMENTTYPE_ID IS NOT NULL
   AND  NOT EXISTS
    (SELECT 1
     FROM XMLLANE_EQUIPMENT XLE
     WHERE
	XLE.TC_COMPANY_ID = XL.BUSINESS_UNIT_ID AND
	XLE.RFPID = I_RFPID AND
     XLE.CODE_ID = XL.EQUIPMENTTYPE_ID);
 END IF;

  -- checking for bad service level which doesnot exist in SERVICE_LEVEL table');

  INSERT INTO XMLERROR
    (UPLOAD_TYPE_ID, RFPID, LANEID, LANEEQUIPMENTTYPEID, ERRORDESC)
    SELECT C_UPLOAD_TYPE_ID,
           RFPID,
           LANEID,
           LANEEQUIPMENTTYPEID,
           'invalid service level'
    FROM   XMLLANEEQUIPMENTTYPE XLET
    WHERE  RFPID = I_RFPID
    AND    SERVICETYPE_ID IS NOT NULL
    AND    NOT EXISTS
     (SELECT 1
            FROM   SERVICE_LEVEL SL
            WHERE  --SL.TC_COMPANY_ID = TC  AND
            	SL.SERVICE_LEVEL_ID = XLET.SERVICETYPE_ID);

  INSERT INTO XMLERROR
  (UPLOAD_TYPE_ID, RFPID, LANEID, LANEEQUIPMENTTYPEID, ERRORDESC)
  SELECT C_UPLOAD_TYPE_ID,
     XLET.RFPID,
     XLET.LANEID,
     XLET.LANEEQUIPMENTTYPEID,
     'business unit is not compatible with service level'
  FROM XMLLANEEQUIPMENTTYPE XLET, XMLLANE XL
  WHERE  XLET.RFPID = I_RFPID
  AND  XLET.SERVICETYPE_ID IS NOT NULL
  AND XLET.RFPID = XL.RFPID
  AND XLET.LANEID = XL.LANEID
  AND  NOT EXISTS
   (SELECT 1
    FROM XMLLANE_SERVICE XLS
    WHERE
    	XLS.TC_COMPANY_ID = XL.BUSINESS_UNIT_ID AND
    	XLS.RFPID = I_RFPID AND
    	XLS.CODE_ID = XLET.SERVICETYPE_ID);

  IF HASSERVICE > 0
  THEN
	  INSERT INTO XMLERROR
	  (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)
	  SELECT C_UPLOAD_TYPE_ID,
	     XL.RFPID,
	     XL.LANEID,
	     'business unit is not compatible with service level'
	  FROM  XMLLANE XL
	  WHERE  XL.RFPID = I_RFPID
	  AND  XL.SERVICETYPE_ID IS NOT NULL
	  AND  NOT EXISTS
	   (SELECT 1
	    FROM XMLLANE_SERVICE XLS
	    WHERE
		XLS.TC_COMPANY_ID = XL.BUSINESS_UNIT_ID AND
		XLS.RFPID = I_RFPID AND
		XLS.CODE_ID = XL.SERVICETYPE_ID);
  END IF;

  -- checking for bad protection level which doesnot exist in PROTECTION_LEVEL table');

  INSERT INTO XMLERROR
    (UPLOAD_TYPE_ID, RFPID, LANEID, LANEEQUIPMENTTYPEID, ERRORDESC)
    SELECT C_UPLOAD_TYPE_ID,
           RFPID,
           LANEID,
           LANEEQUIPMENTTYPEID,
           'invalid protection level'

    FROM   XMLLANEEQUIPMENTTYPE XLET
    WHERE  RFPID = I_RFPID
    AND    PROTECTIONLEVEL_ID IS NOT NULL
    AND    NOT EXISTS
     (SELECT 1
            FROM   PROTECTION_LEVEL PL
            WHERE  --PL.TC_COMPANY_ID = TC  AND
            	PL.PROTECTION_LEVEL_ID = XLET.PROTECTIONLEVEL_ID);

  INSERT INTO XMLERROR
  (UPLOAD_TYPE_ID, RFPID, LANEID, LANEEQUIPMENTTYPEID, ERRORDESC)
  SELECT C_UPLOAD_TYPE_ID,
     XLET.RFPID,
     XLET.LANEID,
     XLET.LANEEQUIPMENTTYPEID,
     'business unit is not compatible with protection level'
  FROM XMLLANEEQUIPMENTTYPE XLET, XMLLANE XL
  WHERE  XLET.RFPID = I_RFPID
  AND  XLET.PROTECTIONLEVEL_ID IS NOT NULL
  AND XLET.RFPID = XL.RFPID
  AND XLET.LANEID = XL.LANEID
  AND  NOT EXISTS
   (SELECT 1
    FROM XMLLANE_PROTECTION XLP
    WHERE
    	XLP.TC_COMPANY_ID = XL.BUSINESS_UNIT_ID   AND
    	XLP.RFPID = I_RFPID AND
    	XLP.CODE_ID = XLET.PROTECTIONLEVEL_ID);

  IF HASPROTECTION > 0
  THEN
	  INSERT INTO XMLERROR
	  (UPLOAD_TYPE_ID, RFPID, LANEID,  ERRORDESC)
	  SELECT C_UPLOAD_TYPE_ID,
	     XL.RFPID,
	     XL.LANEID,
	     'business unit is not compatible with protection level'
	  FROM  XMLLANE XL
	  WHERE  XL.RFPID = I_RFPID
	  AND  XL.PROTECTIONLEVEL_ID IS NOT NULL
	  AND  NOT EXISTS
	   (SELECT 1
	    FROM XMLLANE_PROTECTION XLP
	    WHERE
		XLP.TC_COMPANY_ID = XL.BUSINESS_UNIT_ID   AND
		XLP.RFPID = I_RFPID AND
		XLP.CODE_ID = XL.PROTECTIONLEVEL_ID);
  END IF;

-- checking for bad mode level which doesnot exist in RFPBASEMODE table');

  INSERT INTO XMLERROR
    (UPLOAD_TYPE_ID, RFPID, LANEID, LANEEQUIPMENTTYPEID, ERRORDESC)
    SELECT C_UPLOAD_TYPE_ID,
           RFPID,
           LANEID,
           LANEEQUIPMENTTYPEID,
           'invalid mode'

    FROM   XMLLANEEQUIPMENTTYPE XLET
    WHERE  RFPID = I_RFPID
    AND    MOT IS NOT NULL
    AND    NOT EXISTS
     (SELECT 1
            FROM   RFPBASEMODE RB
            WHERE  RB.RFPID = I_RFPID
            AND    RB.MOT_CODE = XLET.MOT);

-- checking for bad commodity code which doesnot exist in COMMMODITY_CODE table');

  INSERT INTO XMLERROR
  (UPLOAD_TYPE_ID, RFPID, LANEID, LANEEQUIPMENTTYPEID, ERRORDESC)
  SELECT C_UPLOAD_TYPE_ID,
     RFPID,
     LANEID,
     LANEEQUIPMENTTYPEID,
     'invalid commodity code'
  FROM XMLLANEEQUIPMENTTYPE XLET
  WHERE  RFPID = I_RFPID
  AND  COMMODITYCODE_ID IS NOT NULL
  AND  NOT EXISTS
   (SELECT 1
    FROM COMMODITY_CODE CC
    WHERE
    --CC.tc_company_id = TC AND
    	CC.COMMODITY_CODE_ID = XLET.COMMODITYCODE_ID);

INSERT INTO XMLERROR
  (UPLOAD_TYPE_ID, RFPID, LANEID, LANEEQUIPMENTTYPEID, ERRORDESC)
  SELECT C_UPLOAD_TYPE_ID,
     XLET.RFPID,
     XLET.LANEID,
     XLET.LANEEQUIPMENTTYPEID,
     'business unit is not compatible with commodity code'
  FROM XMLLANEEQUIPMENTTYPE XLET, XMLLANE XL
  WHERE  XLET.RFPID = I_RFPID
  AND  XLET.COMMODITYCODE_ID IS NOT NULL
  AND XLET.RFPID = XL.RFPID
  AND XLET.LANEID = XL.LANEID
  AND  NOT EXISTS
   (SELECT 1
    FROM XMLLANE_COMMODITY XLC
    WHERE
        XLC.tc_company_id = XL.BUSINESS_UNIT_ID AND
        XLC.RFPID = I_RFPID AND
    	XLC.CODE_ID = XLET.COMMODITYCODE_ID);

  --checking for duplicate equipment, protection, service level combo');

  INSERT INTO XMLERROR
    (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)
    SELECT C_UPLOAD_TYPE_ID,
           RFPID,
           LANEID,
           'duplicate equipment, protection, service mode & commodity code combo'
    FROM   XMLLANEEQUIPMENTTYPE
    WHERE  RFPID = I_RFPID
    GROUP  BY RFPID, LANEID, EQUIPMENTTYPE_ID, SERVICETYPE_ID, PROTECTIONLEVEL_ID, MOT, COMMODITYCODE_ID
    HAVING COUNT(*) > 1;



  -- beging : rating routing condition
  IF RATINGROUTING = 1

  THEN

    -- CURSOR_CUSTOM_FIELD := null;

    V_DYNSQL := 'SELECT colname FROM rfpcolproperties ' ||
                ' WHERE rfpid = ' || I_RFPID || ' and round_num = ' ||
                ROUNDNUM || ' and (colname like ''CUSTINT%'') ' ||
                ' and ((GROUPID = ''O'') OR ' || ' (GROUPID = ''D'')) ';



    -- iterating through custom_field_cursor to prepare select clause ');
    --ADDED
    CUSTOM_FIELD_CONDITIONS := '';

    OPEN CURSOR_CUSTOM_FIELD FOR V_DYNSQL;
    LOOP
      EXIT WHEN CURSOR_CUSTOM_FIELD%NOTFOUND;
      FETCH CURSOR_CUSTOM_FIELD
        INTO S_COLNAME;
      CUSTOM_FIELD_CONDITIONS := NVL(CUSTOM_FIELD_CONDITIONS, '') ||
                                 ' and ((( X1.' || COALESCE(S_COLNAME, '') ||
                                 ' is null ) and ' || ' ( X2.' ||
                                 COALESCE(S_COLNAME, '') ||
                                 ' is null )) or ' || '(X1.' ||
                                 COALESCE(S_COLNAME, '') || ' = X2.' ||
                                 COALESCE(S_COLNAME, '') || '))';
    END LOOP;
    CLOSE CURSOR_CUSTOM_FIELD;

    -- NEW

    V_DYNSQL := 'SELECT colname FROM rfpcolproperties ' ||
                ' WHERE rfpid = ' || I_RFPID || ' and round_num = ' ||
                ROUNDNUM || ' AND (colname like ''CUSTDOUBLE%'') ' ||
                ' and ((GROUPID = ''O'') OR ' || ' (GROUPID = ''D'')) ';

    --trace ' test 2 : sql = '||v_dynsql;

    OPEN CURSOR_CUSTOM_FIELD FOR V_DYNSQL;
    LOOP
      EXIT WHEN CURSOR_CUSTOM_FIELD%NOTFOUND;
      FETCH CURSOR_CUSTOM_FIELD
        INTO S_COLNAME;
      -- begin: col name condition1
      IF (S_COLNAME <> '0')
      THEN
        CUSTOM_FIELD_CONDITIONS := COALESCE(CUSTOM_FIELD_CONDITIONS, '') ||
                                   ' and ((( X1.' ||
                                   COALESCE(S_COLNAME, '') ||
                                   ' is null ) and ' || ' ( X2.' ||
                                   COALESCE(S_COLNAME, '') ||
                                   ' is null )) or ' || ' (X1.' ||
                                   COALESCE(S_COLNAME, '') || ' = X2.' ||
                                   COALESCE(S_COLNAME, '') || '))';
      END IF;
      -- end: col name condition1

    END LOOP;
    CLOSE CURSOR_CUSTOM_FIELD;

    --trace ' test 2 : sql = '||v_dynsql;

    V_DYNSQL := 'SELECT colname FROM rfpcolproperties ' ||
                ' WHERE rfpid = ' || I_RFPID || ' and round_num = ' ||
                ROUNDNUM || ' and (colname like ''CUSTTEXT%'') ' ||
                ' and ((GROUPID = ''O'') OR ' || ' (GROUPID = ''D'')) ';

    OPEN CURSOR_CUSTOM_FIELD FOR V_DYNSQL;
    LOOP
      EXIT WHEN CURSOR_CUSTOM_FIELD%NOTFOUND;
      FETCH CURSOR_CUSTOM_FIELD
        INTO S_COLNAME;


      -- begin: col name condition2
      IF (S_COLNAME <> '0')
      THEN
        CUSTOM_FIELD_CONDITIONS := COALESCE(CUSTOM_FIELD_CONDITIONS, '') ||
                                   ' and ((( X1.' || NVL(S_COLNAME, '') ||
                                   ' is null ) and ' || ' ( X2.' ||
                                   NVL(S_COLNAME, '') || ' is null )) or ' ||
                                   ' (X1.' || NVL(S_COLNAME, '') ||
                                   ' = X2.' || NVL(S_COLNAME, '') || '))';
      END IF;
    -- end: col name condition2

    END LOOP;
    CLOSE CURSOR_CUSTOM_FIELD;

    -- begin: HASFAC condition : if rfp does not have origin or destination facility checked in lanecusomization
    IF HASORIGINFAC = 0 AND HASDESTFAC = 0
    THEN

      --trace '-- checking for equipment infeasible with protection level ';

      INSERT INTO XMLERROR
        (UPLOAD_TYPE_ID, RFPID, LANEID, LANEEQUIPMENTTYPEID, ERRORDESC)

        SELECT C_UPLOAD_TYPE_ID,
               XLET.RFPID,
               XLET.LANEID,
               LANEEQUIPMENTTYPEID,
               'equipment infeasible with protection level'
        FROM   XMLLANEEQUIPMENTTYPE XLET, INFEASIBILITY I
        WHERE  XLET.RFPID = I_RFPID
        AND  I.TC_COMPANY_ID IN (SELECT DISTINCT TC_COMPANY_ID FROM XMLLANE_EQUIPMENT WHERE rfpid = I_RFPID)
        AND    I.INFEASIBILITY_TYPE = 'PLEQ'
        AND    XLET.PROTECTIONLEVEL_ID = I.VALUE1
        AND    XLET.EQUIPMENTTYPE_ID = I.VALUE2;

    -- cont: HASFAC condition : if rfp has origin but no destination facility checked in lanecusomization
    ELSIF HASORIGINFAC > 0 AND HASDESTFAC = 0
      THEN

        --trace '-- checking for equipment infeasible with protection level ';

        INSERT INTO XMLERROR
          (UPLOAD_TYPE_ID, RFPID, LANEID, LANEEQUIPMENTTYPEID, ERRORDESC)
          SELECT C_UPLOAD_TYPE_ID,
                 XLET.RFPID,
                 XLET.LANEID,
                 LANEEQUIPMENTTYPEID,
                 'equipment infeasible with protection level'

          FROM   XMLLANEEQUIPMENTTYPE XLET, INFEASIBILITY I
          WHERE  XLET.RFPID = I_RFPID
          AND  I.TC_COMPANY_ID IN (SELECT DISTINCT TC_COMPANY_ID FROM XMLLANE_EQUIPMENT WHERE rfpid = I_RFPID)
          AND    I.INFEASIBILITY_TYPE = 'PLEQ'
          AND    XLET.PROTECTIONLEVEL_ID = I.VALUE1
          AND    XLET.EQUIPMENTTYPE_ID = I.VALUE2
          AND    NOT EXISTS
           (SELECT 1
                  FROM   INFEASIBILITY I2, XMLLANE XL, FACILITY_ALIAS FA
                  WHERE  XL.RFPID = XLET.RFPID
                  AND    XL.LANEID = XLET.LANEID
                  AND    FA.FACILITY_ALIAS_ID = XL.ORIGINFACILITYCODE
                  AND  I2.TC_COMPANY_ID IN (SELECT DISTINCT TC_COMPANY_ID FROM XMLLANE_FACILITY WHERE rfpid = I_RFPID)
    	          AND  FA.TC_COMPANY_ID = I2.TC_COMPANY_ID
                  AND    I2.INFEASIBILITY_TYPE = 'FAPLEQ'
                  AND    CAST(FA.FACILITY_ID AS CHAR(10)) = I2.VALUE1
                  AND    XLET.PROTECTIONLEVEL_ID = I2.VALUE2
                  AND    XLET.EQUIPMENTTYPE_ID = I2.VALUE3
                  AND    ((XL.ACTIVE = 1) OR
                        (NOT EXISTS
                         (SELECT 1
                            FROM   LANE ML
                            WHERE  ML.RFPID = I_RFPID
                            AND    ML.LANEID = XL.LANEID
                            AND    ML.ROUND_NUM < ROUNDNUM))));

      -- cont: HASFAC condition : if rfp has no origin but has destination facility checked in lanecusomization
      ELSIF HASORIGINFAC = 0 AND HASDESTFAC > 0
        THEN

          --trace '-- checking for equipment infeasible with protection level ';

          INSERT INTO XMLERROR
            (UPLOAD_TYPE_ID, RFPID, LANEID, LANEEQUIPMENTTYPEID, ERRORDESC)
            SELECT C_UPLOAD_TYPE_ID,
                   XLET.RFPID,
                   XLET.LANEID,
                   LANEEQUIPMENTTYPEID,
                   'equipment infeasible with protection level'

            FROM   XMLLANEEQUIPMENTTYPE XLET, INFEASIBILITY I
            WHERE  XLET.RFPID = I_RFPID
	    AND  I.TC_COMPANY_ID IN (SELECT DISTINCT TC_COMPANY_ID FROM XMLLANE_EQUIPMENT WHERE rfpid = I_RFPID)
            AND    I.INFEASIBILITY_TYPE = 'PLEQ'
            AND    XLET.PROTECTIONLEVEL_ID = I.VALUE1
            AND    XLET.EQUIPMENTTYPE_ID = I.VALUE2
            AND    NOT EXISTS
             (SELECT 1
                    FROM   INFEASIBILITY I2, XMLLANE XL, FACILITY_ALIAS FA
                    WHERE  XL.RFPID = XLET.RFPID
                    AND    XL.LANEID = XLET.LANEID
                    AND    FA.FACILITY_ALIAS_ID = XL.DESTINATIONFACILITYCODE
		    AND  I2.TC_COMPANY_ID IN (SELECT DISTINCT TC_COMPANY_ID FROM XMLLANE_FACILITY WHERE rfpid = I_RFPID)
		    AND  FA.TC_COMPANY_ID = I2.TC_COMPANY_ID
                    AND    I2.INFEASIBILITY_TYPE = 'FAPLEQ'
                    AND    CAST(FA.FACILITY_ID AS CHAR(10)) = I2.VALUE1
                    AND    XLET.PROTECTIONLEVEL_ID = I2.VALUE2
                    AND    XLET.EQUIPMENTTYPE_ID = I2.VALUE3
                    AND    ((XL.ACTIVE = 1) OR
                          (NOT EXISTS
                           (SELECT 1
                              FROM   LANE ML
                              WHERE  ML.RFPID = I_RFPID
                              AND    ML.LANEID = XL.LANEID
                              AND    ML.ROUND_NUM < ROUNDNUM))));

        -- cont: HASFAC condition : if rfp has both origin and destination facility checked in lanecusomization
        ELSE

          INSERT INTO XMLERROR
            (UPLOAD_TYPE_ID, RFPID, LANEID, LANEEQUIPMENTTYPEID, ERRORDESC)
            SELECT C_UPLOAD_TYPE_ID,
                   XLET.RFPID,
                   XLET.LANEID,
                   LANEEQUIPMENTTYPEID,
                   'equipment infeasible with protection level'

            FROM   XMLLANEEQUIPMENTTYPE XLET, INFEASIBILITY I
            WHERE  XLET.RFPID = I_RFPID
	    AND  I.TC_COMPANY_ID IN (SELECT DISTINCT TC_COMPANY_ID FROM XMLLANE_EQUIPMENT WHERE rfpid = I_RFPID)
            AND    I.INFEASIBILITY_TYPE = 'PLEQ'
            AND    XLET.PROTECTIONLEVEL_ID = I.VALUE1
            AND    XLET.EQUIPMENTTYPE_ID = I.VALUE2
            AND    NOT EXISTS
             (SELECT 1
                    FROM   INFEASIBILITY I2, XMLLANE XL, FACILITY_ALIAS FA
                    WHERE  XL.RFPID = XLET.RFPID
                    AND    XL.LANEID = XLET.LANEID
                    AND    (FA.FACILITY_ALIAS_ID = XL.ORIGINFACILITYCODE OR
                          FA.FACILITY_ALIAS_ID =
                          XL.DESTINATIONFACILITYCODE)
		    AND  I2.TC_COMPANY_ID IN (SELECT DISTINCT TC_COMPANY_ID FROM XMLLANE_FACILITY WHERE rfpid = I_RFPID)
		    AND  FA.TC_COMPANY_ID = I2.TC_COMPANY_ID
                    AND    I2.INFEASIBILITY_TYPE = 'FAPLEQ'
                    AND    CAST(FA.FACILITY_ID AS CHAR(10)) = I2.VALUE1
                    AND    XLET.PROTECTIONLEVEL_ID = I2.VALUE2
                    AND    XLET.EQUIPMENTTYPE_ID = I2.VALUE3
                    AND    ((XL.ACTIVE = 1) OR
                          (NOT EXISTS
                           (SELECT 1
                              FROM   LANE ML
                              WHERE  ML.RFPID = I_RFPID
                              AND    ML.LANEID = XL.LANEID
                              AND    ML.ROUND_NUM < ROUNDNUM))));

        END IF;
      -- end: HASFAC condition

        -- begin HASORIGINFAC condition : if rfp has  origin checked in lanecusomization
        IF HASORIGINFAC > 0
        THEN

          INSERT INTO XMLERROR
            (UPLOAD_TYPE_ID, RFPID, LANEID, LANEEQUIPMENTTYPEID, ERRORDESC)

            SELECT C_UPLOAD_TYPE_ID,
                   XL.RFPID,
                   XL.LANEID,
                   XLET.LANEEQUIPMENTTYPEID,
                   'equipment infeasible with origin facility'
            FROM   XMLLANE              XL,
                   XMLLANEEQUIPMENTTYPE XLET,
                   FACILITY_ALIAS       FA,
                   INFEASIBILITY        I
            WHERE  XL.RFPID = XLET.RFPID
            AND    XL.LANEID = XLET.LANEID
            AND    XL.RFPID = I_RFPID
	    AND  I.TC_COMPANY_ID IN (SELECT DISTINCT TC_COMPANY_ID FROM XMLLANE_EQUIPMENT WHERE rfpid = I_RFPID)
            AND    I.INFEASIBILITY_TYPE = 'FAEQ'
            AND    FA.FACILITY_ALIAS_ID = XL.ORIGINFACILITYCODE
	    AND  FA.TC_COMPANY_ID = I.TC_COMPANY_ID
            AND    CAST(FA.FACILITY_ID AS CHAR(10)) = I.VALUE1
            AND    XLET.EQUIPMENTTYPE_ID = I.VALUE2
            AND    ((XL.ACTIVE = 1) OR
                  (NOT EXISTS
                   (SELECT 1
                      FROM   LANE ML
                      WHERE  ML.RFPID = I_RFPID
                      AND    ML.LANEID = XL.LANEID
                      AND    ML.ROUND_NUM < ROUNDNUM)));

        END IF;
         -- end: HASORIGINFAC condition

       -- begin: HASDESTFAC condition : if rfp has  destination checked in lanecusomization
        IF HASDESTFAC > 0
        THEN
        -- check for equipment infeasible with dest facility
          INSERT INTO XMLERROR
            (UPLOAD_TYPE_ID, RFPID, LANEID, LANEEQUIPMENTTYPEID, ERRORDESC)
            SELECT C_UPLOAD_TYPE_ID,
                   XL.RFPID,
                   XL.LANEID,
                   XLET.LANEEQUIPMENTTYPEID,
                   'equipment infeasible with dest facility'
            FROM   XMLLANE              XL,
                   XMLLANEEQUIPMENTTYPE XLET,
                   FACILITY_ALIAS       FA,
                   INFEASIBILITY        I
            WHERE  XL.RFPID = XLET.RFPID
            AND    XL.LANEID = XLET.LANEID
            AND    XL.RFPID = I_RFPID
	    AND  I.TC_COMPANY_ID IN (SELECT DISTINCT TC_COMPANY_ID FROM XMLLANE_EQUIPMENT WHERE rfpid = I_RFPID)
            AND    I.INFEASIBILITY_TYPE = 'FAEQ'
            AND    FA.FACILITY_ALIAS_ID = XL.DESTINATIONFACILITYCODE
	    AND  FA.TC_COMPANY_ID = I.TC_COMPANY_ID
            AND    CAST(FA.FACILITY_ID AS CHAR(10)) = I.VALUE1
            AND    XLET.EQUIPMENTTYPE_ID = I.VALUE2
            AND    ((XL.ACTIVE = 1) OR
                  (NOT EXISTS
                   (SELECT 1
                      FROM   LANE ML
                      WHERE  ML.RFPID = I_RFPID
                      AND    ML.LANEID = XL.LANEID
                      AND    ML.ROUND_NUM < ROUNDNUM)));

        END IF;
       -- end: HASDESTFAC condition

       -- get if has origin state selected in rfp
        SELECT COUNT(*)
        INTO   HASORIGINSTATE
        FROM   RFPCOLPROPERTIES
        WHERE  RFPID = I_RFPID
        AND    ROUND_NUM = ROUNDNUM
        AND    COLNAME = 'ORIGINSTATEPROV';

       -- get if has destination state selected in rfp
        SELECT COUNT(*)
        INTO   HASDESTSTATE
        FROM   RFPCOLPROPERTIES
        WHERE  RFPID = I_RFPID
        AND    ROUND_NUM = ROUNDNUM
        AND    COLNAME = 'DESTINATIONSTATEPROV';

       -- get if has origin countrycode selected in rfp
        SELECT COUNT(*)
        INTO   HASORIGINCOUNTRY
        FROM   RFPCOLPROPERTIES
        WHERE  RFPID = I_RFPID
        AND    ROUND_NUM = ROUNDNUM
        AND    COLNAME = 'ORIGINCOUNTRYCODE';

       -- get if has origin zone code selected in rfp
        SELECT COUNT(*)
        INTO   HASORIGINZONE
        FROM   RFPCOLPROPERTIES
        WHERE  RFPID = I_RFPID
        AND    ROUND_NUM = ROUNDNUM
        AND    COLNAME = 'ORIGINZONECODE';


      -- get if has destination country code selected in rfp
        SELECT COUNT(*)
        INTO   HASDESTCOUNTRY
        FROM   RFPCOLPROPERTIES
        WHERE  RFPID = I_RFPID
        AND    ROUND_NUM = ROUNDNUM
        AND    COLNAME = 'DESTINATIONCOUNTRYCODE';


           -- get if has origin postal code selected in rfp
        SELECT COUNT(*)
        INTO   HASORIGINZIP
        FROM   RFPCOLPROPERTIES
        WHERE  RFPID = I_RFPID
        AND    ROUND_NUM = ROUNDNUM
        AND    COLNAME = 'ORIGINPOSTALCODE';

           -- get if has destination postal code selected in rfp
        SELECT COUNT(*)
        INTO   HASDESTZIP
        FROM   RFPCOLPROPERTIES
        WHERE  RFPID = I_RFPID
        AND    ROUND_NUM = ROUNDNUM
        AND    COLNAME = 'DESTINATIONPOSTALCODE';

           -- get if has origin city selected in rfp
        SELECT COUNT(*)
        INTO   HASORIGINCITY
        FROM   RFPCOLPROPERTIES
        WHERE  RFPID = I_RFPID
        AND    ROUND_NUM = ROUNDNUM
        AND    COLNAME = 'ORIGINCITY';

           -- get if has destination city selected in rfp
        SELECT COUNT(*)
        INTO   HASDESTCITY
        FROM   RFPCOLPROPERTIES
        WHERE  RFPID = I_RFPID
        AND    ROUND_NUM = ROUNDNUM
        AND    COLNAME = 'DESTINATIONCITY';

           -- get if has destination zone code selected in rfp
        SELECT COUNT(*)
        INTO   HASDESTZONE
        FROM   RFPCOLPROPERTIES
        WHERE  RFPID = I_RFPID
        AND    ROUND_NUM = ROUNDNUM
        AND    COLNAME = 'DESTINATIONZONECODE';

  -- begin: HASORIGINZONE condition
    IF HASORIGINZONE > 0
    THEN
	    -- check for Rating/Routing compatibility: invalid Zone
	    INSERT INTO XMLERROR
	    (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)
	    SELECT C_UPLOAD_TYPE_ID, XL.RFPID,  XL.LANEID,
	       'Rating/Routing compatibility: invalid origin zone'
	    FROM XMLLANE XL
	    WHERE  XL.RFPID = I_RFPID
	    AND  XL.ORIGINZONECODE IS NOT NULL
	    AND  ((XL.ACTIVE = 1) OR
	      (NOT EXISTS
	       (SELECT 1
		FROM LANE ML
		WHERE  ML.RFPID = I_RFPID
		AND  ML.LANEID = XL.LANEID
		AND  ML.ROUND_NUM < ROUNDNUM)))
	    AND  NOT EXISTS
	     (SELECT 1
		FROM XMLLANE_ZONE XLZ
		WHERE
			XLZ.TC_COMPANY_ID = XL.BUSINESS_UNIT_ID AND
			XLZ.RFPID = I_RFPID AND
		        XLZ.CODE = XL.ORIGINZONECODE);

	    INSERT INTO XMLERROR
	    (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)
	    SELECT C_UPLOAD_TYPE_ID, XL.RFPID,  XL.LANEID,
	       'Rating/Routing compatibility: Origin Zone should not be combined with other origin fields'
	    FROM XMLLANE XL
	    WHERE  XL.RFPID = I_RFPID
	    AND  XL.ORIGINZONECODE IS NOT NULL
	    AND  ((XL.ACTIVE = 1) OR
	      (NOT EXISTS
	       (SELECT 1
		FROM LANE ML
		WHERE  ML.RFPID = I_RFPID
		AND  ML.LANEID = XL.LANEID
		AND  ML.ROUND_NUM < ROUNDNUM)))
	    AND ( (HASORIGINFAC = 1 AND XL.ORIGINFACILITYCODE IS NOT NULL)
			  OR  (HASORIGINCITY = 1 AND XL.ORIGINCITY IS NOT NULL)
			  OR  (HASORIGINSTATE = 1 AND XL.ORIGINSTATEPROV IS NOT NULL)
			  OR  (HASORIGINZIP = 1 AND XL.ORIGINPOSTALCODE IS NOT NULL)
			  OR  (HASORIGINCOUNTRY = 1 AND XL.ORIGINCOUNTRYCODE IS NOT NULL) );
    END IF;

  -- begin: HASDESTZONE condition
    IF HASDESTZONE > 0
    THEN
	    -- check for Rating/Routing compatibility: bad Zone
	    INSERT INTO XMLERROR
	    (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)
	    SELECT C_UPLOAD_TYPE_ID, XL.RFPID,  XL.LANEID,
	       'Rating/Routing compatibility: invalid destination zone'
	    FROM XMLLANE XL
	    WHERE  XL.RFPID = I_RFPID
	    AND  XL.DESTINATIONZONECODE IS NOT NULL
	    AND  ((XL.ACTIVE = 1) OR
	      (NOT EXISTS
	       (SELECT 1
		FROM LANE ML
		WHERE  ML.RFPID = I_RFPID
		AND  ML.LANEID = XL.LANEID
		AND  ML.ROUND_NUM < ROUNDNUM)))
	    AND  NOT EXISTS
	     (SELECT 1
		FROM XMLLANE_ZONE XLZ
		WHERE	XLZ.TC_COMPANY_ID = XL.BUSINESS_UNIT_ID AND
			XLZ.RFPID = I_RFPID AND
		        XLZ.CODE = XL.DESTINATIONZONECODE);

	    INSERT INTO XMLERROR
	    (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)
	    SELECT C_UPLOAD_TYPE_ID, XL.RFPID,  XL.LANEID,
	       'Rating/Routing compatibility: Destination Zone should not be combined with other destination fields'
	    FROM XMLLANE XL
	    WHERE  XL.RFPID = I_RFPID
	    AND  XL.DESTINATIONZONECODE IS NOT NULL
	    AND  ((XL.ACTIVE = 1) OR
	      (NOT EXISTS
	       (SELECT 1
		FROM LANE ML
		WHERE  ML.RFPID = I_RFPID
		AND  ML.LANEID = XL.LANEID
		AND  ML.ROUND_NUM < ROUNDNUM)))
	    AND ( (HASDESTFAC = 1 AND XL.DESTINATIONFACILITYCODE IS NOT NULL)
			  OR  (HASDESTCITY = 1 AND XL.DESTINATIONCITY IS NOT NULL)
			  OR  (HASDESTSTATE = 1 AND XL.DESTINATIONSTATEPROV IS NOT NULL)
			  OR  (HASDESTZIP = 1 AND XL.DESTINATIONPOSTALCODE IS NOT NULL)
			  OR  (HASDESTCOUNTRY = 1 AND XL.DESTINATIONCOUNTRYCODE IS NOT NULL) );
    END IF;

      -- begin: HASORIGINFAC condition
        IF HASORIGINFAC > 0
        THEN
        -- setting origin details in xmlllane table
          UPDATE XMLLANE
          SET    (ORIGINCITY, ORIGINSTATEPROV, ORIGINPOSTALCODE, ORIGINCOUNTRYCODE) =

                  ((SELECT F.CITY,
                           F.STATE_PROV,
                           F.POSTAL_CODE,
                           F.COUNTRY_CODE
                    FROM   FACILITY_ALIAS FA, FACILITY F
                    WHERE  FA.TC_COMPANY_ID = TC
                    AND    FA.FACILITY_ALIAS_ID = XMLLANE.ORIGINFACILITYCODE
                    AND    F.TC_COMPANY_ID = FA.TC_COMPANY_ID
                    AND    F.FACILITY_ID = FA.FACILITY_ID))

          WHERE  RFPID = I_RFPID
          AND    ORIGINFACILITYCODE IS NOT NULL
          AND    ((XMLLANE.ACTIVE = 1) OR
                (NOT EXISTS (SELECT 1
                               FROM   LANE ML
                               WHERE  ML.RFPID = I_RFPID
                               AND    ML.LANEID = XMLLANE.LANEID
                               AND    ML.ROUND_NUM < ROUNDNUM)));


        -- check for Rating/Routing compatibility: bad origin facility
          INSERT INTO XMLERROR
            (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)
            SELECT C_UPLOAD_TYPE_ID,
                   XL.RFPID,
                   XL.LANEID,
                   'Rating/Routing compatibility: invalid origin facility'
            FROM   XMLLANE XL
            WHERE  XL.RFPID = I_RFPID
            AND    XL.ORIGINFACILITYCODE IS NOT NULL
            AND    ((XL.ACTIVE = 1) OR
                  (NOT EXISTS
                   (SELECT 1
                      FROM   LANE ML
                      WHERE  ML.RFPID = I_RFPID
                      AND    ML.LANEID = XL.LANEID
                      AND    ML.ROUND_NUM < ROUNDNUM)))
            AND    NOT EXISTS
             (SELECT 1
		FROM XMLLANE_FACILITY XLF
		WHERE	XLF.TC_COMPANY_ID = XL.BUSINESS_UNIT_ID AND
			XLF.RFPID = I_RFPID AND
			XLF.CODE = XL.ORIGINFACILITYCODE);
        END IF;
       -- end: HASORIGINFAC condition

        -- begin: HASDESTFAC condition
        IF HASDESTFAC > 0
        THEN

          UPDATE XMLLANE
          SET    (DESTINATIONCITY, DESTINATIONSTATEPROV, DESTINATIONPOSTALCODE, DESTINATIONCOUNTRYCODE) = ((SELECT F.CITY,
                                                                                                                   F.STATE_PROV,
                                                                                                                   F.POSTAL_CODE,
                                                                                                                   F.COUNTRY_CODE
                                                                                                            FROM   FACILITY_ALIAS FA,
                                                                                                                   FACILITY       F
                                                                                                            WHERE  FA.TC_COMPANY_ID = TC
                                                                                                            AND    FA.FACILITY_ALIAS_ID =
                                                                                                                   XMLLANE.DESTINATIONFACILITYCODE
                                                                                                            AND    F.TC_COMPANY_ID =
                                                                                                                   FA.TC_COMPANY_ID
                                                                                                            AND    F.FACILITY_ID =
                                                                                                                   FA.FACILITY_ID))
          WHERE  XMLLANE.RFPID = I_RFPID
          AND    XMLLANE.DESTINATIONFACILITYCODE IS NOT NULL
          AND    ((XMLLANE.ACTIVE = 1) OR
                (NOT EXISTS (SELECT 1
                               FROM   LANE ML
                               WHERE  ML.RFPID = I_RFPID
                               AND    ML.LANEID = XMLLANE.LANEID
                               AND    ML.ROUND_NUM < ROUNDNUM)));

          INSERT INTO XMLERROR
            (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)

            SELECT C_UPLOAD_TYPE_ID,
                   XL.RFPID,
                   XL.LANEID,
                   'Rating/Routing compatibility: invalid destination facility'
            FROM   XMLLANE XL
            WHERE  XL.RFPID = I_RFPID
            AND    XL.DESTINATIONFACILITYCODE IS NOT NULL
            AND    ((XL.ACTIVE = 1) OR
                  (NOT EXISTS
                   (SELECT 1
                      FROM   LANE ML
                      WHERE  ML.RFPID = I_RFPID
                      AND    ML.LANEID = XL.LANEID
                      AND    ML.ROUND_NUM < ROUNDNUM)))

            AND    NOT EXISTS
             (SELECT 1
		FROM XMLLANE_FACILITY XLF
		WHERE	XLF.TC_COMPANY_ID = XL.BUSINESS_UNIT_ID AND
			XLF.RFPID = I_RFPID AND
			XLF.CODE = XL.DESTINATIONFACILITYCODE);

        END IF;
      -- end: HASDESTFAC condition

      -- begin: HASORIGINSTATE condition
        IF HASORIGINSTATE > 0
        THEN

          INSERT INTO XMLERROR
            (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)
            SELECT C_UPLOAD_TYPE_ID,
                   XL.RFPID,
                   XL.LANEID,
                   'Rating/Routing compatibility: invalid origin state'
            FROM   XMLLANE XL
            WHERE  XL.RFPID = I_RFPID
            AND    XL.ORIGINSTATEPROV IS NOT NULL
            AND    ((XL.ACTIVE = 1) OR
                  (NOT EXISTS
                   (SELECT 1
                      FROM   LANE ML
                      WHERE  ML.RFPID = I_RFPID
                      AND    ML.LANEID = XL.LANEID
                      AND    ML.ROUND_NUM < ROUNDNUM)))
            AND    NOT EXISTS
             (SELECT 1
                    FROM   STATE_PROV S
                    WHERE  S.STATE_PROV = XL.ORIGINSTATEPROV);

          INSERT INTO XMLERROR
            (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)
            SELECT C_UPLOAD_TYPE_ID,
                   XL.RFPID,
                   XL.LANEID,
                   'Rating/Routing compatibility: must have country with origin state'
            FROM   XMLLANE XL
            WHERE  XL.RFPID = I_RFPID
            AND    ((XL.ACTIVE = 1) OR
                  (NOT EXISTS
                   (SELECT 1
                      FROM   LANE ML
                      WHERE  ML.RFPID = I_RFPID
                      AND    ML.LANEID = XL.LANEID
                      AND    ML.ROUND_NUM < ROUNDNUM)))
            AND    (HASORIGINCOUNTRY = 0 OR XL.ORIGINCOUNTRYCODE IS NULL)
            AND  (HASORIGINZONE = 0 OR XL.ORIGINZONECODE IS NULL);

        END IF;
      -- end: HASORIGINSTATE condition

      -- begin: HASDESTSTATE condition
        IF HASDESTSTATE > 0
        THEN

          INSERT INTO XMLERROR
            (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)
            SELECT C_UPLOAD_TYPE_ID,
                   XL.RFPID,
                   XL.LANEID,
                   'Rating/Routing compatibility: invalid destination state'
            FROM   XMLLANE XL
            WHERE  XL.RFPID = I_RFPID
            AND    XL.DESTINATIONSTATEPROV IS NOT NULL
            AND    ((XL.ACTIVE = 1) OR
                  (NOT EXISTS
                   (SELECT 1
                      FROM   LANE ML
                      WHERE  ML.RFPID = I_RFPID
                      AND    ML.LANEID = XL.LANEID
                      AND    ML.ROUND_NUM < ROUNDNUM)))
            AND    NOT EXISTS
             (SELECT 1
                    FROM   STATE_PROV S
                    WHERE  S.STATE_PROV = XL.DESTINATIONSTATEPROV);

          INSERT INTO XMLERROR
            (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)
            SELECT C_UPLOAD_TYPE_ID,
                   XL.RFPID,
                   XL.LANEID,
                   'Rating/Routing compatibility: must have country with destination state'

            FROM   XMLLANE XL
            WHERE  XL.RFPID = I_RFPID
            AND    ((XL.ACTIVE = 1) OR
                  (NOT EXISTS
                   (SELECT 1
                      FROM   LANE ML
                      WHERE  ML.RFPID = I_RFPID
                      AND    ML.ROUND_NUM < ROUNDNUM)))
            AND    (HASDESTCOUNTRY = 0 OR
                  XL.DESTINATIONCOUNTRYCODE IS NULL)
            AND  (HASDESTZONE = 0 OR XL.DESTINATIONZONECODE IS NULL);

        END IF;
      -- end: HASDESTSTATE condition

      -- begin: HASORIGINCOUNTRY condition
        IF HASORIGINCOUNTRY > 0
        THEN

          INSERT INTO XMLERROR
            (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)

            SELECT C_UPLOAD_TYPE_ID,
                   XL.RFPID,

                   XL.LANEID,
                   'Rating/Routing compatibility: invalid origin country'
            FROM   XMLLANE XL

            WHERE  XL.RFPID = I_RFPID
            AND    XL.ORIGINCOUNTRYCODE IS NOT NULL

            AND    ((XL.ACTIVE = 1) OR
                  (NOT EXISTS
                   (SELECT 1
                      FROM   LANE ML

                      WHERE  ML.RFPID = I_RFPID
                      AND    ML.LANEID = XL.LANEID
                      AND    ML.ROUND_NUM < ROUNDNUM)))
            AND    NOT EXISTS
             (SELECT 1

                    FROM   COUNTRY C
                    WHERE  C.COUNTRY_CODE = XL.ORIGINCOUNTRYCODE);

        END IF;
       -- end: HASORIGINCOUNTRY condition

      -- begin: HASDESTCOUNTRY condition
        IF HASDESTCOUNTRY > 0
        THEN

          INSERT INTO XMLERROR
            (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)
            SELECT C_UPLOAD_TYPE_ID,
                   XL.RFPID,
                   XL.LANEID,
                   'Rating/Routing compatibility: invalid destination country'
            FROM   XMLLANE XL
            WHERE  XL.RFPID = I_RFPID
            AND    XL.DESTINATIONCOUNTRYCODE IS NOT NULL
            AND    ((XL.ACTIVE = 1) OR
                  (NOT EXISTS
                   (SELECT 1
                      FROM   LANE ML
                      WHERE  ML.RFPID = I_RFPID
                      AND    ML.LANEID = XL.LANEID
                      AND    ML.ROUND_NUM < ROUNDNUM)))
            AND    NOT EXISTS
             (SELECT 1
                    FROM   COUNTRY C
                    WHERE  C.COUNTRY_CODE = XL.DESTINATIONCOUNTRYCODE);

        END IF;
        -- end: HASDESTCOUNTRY condition


      -- begin: HASORIGINCITY condition
        IF HASORIGINCITY > 0
        THEN

          INSERT INTO XMLERROR
            (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)
            SELECT C_UPLOAD_TYPE_ID,
                   XL.RFPID,
                   XL.LANEID,
                   'Rating/Routing compatibility: cannot have city, state with origin postal code'
            FROM   XMLLANE XL
            WHERE  XL.RFPID = I_RFPID
            AND    XL.ORIGINCITY IS NOT NULL
            AND    ((XL.ACTIVE = 1) OR
                  (NOT EXISTS
                   (SELECT 1
                      FROM   LANE ML
                      WHERE  ML.RFPID = I_RFPID
                      AND    ML.LANEID = XL.LANEID
                      AND    ML.ROUND_NUM < ROUNDNUM)))
            AND    HASORIGINZIP > 0
            AND    XL.ORIGINPOSTALCODE IS NOT NULL
            AND    NOT
                   (HASORIGINFAC = 1 AND XL.ORIGINFACILITYCODE IS NOT NULL);

        END IF;
      -- end: HASORIGINCITY condition


      -- begin: HASDESTCITY condition
        IF HASDESTCITY > 0
        THEN

          INSERT INTO XMLERROR
            (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)
            SELECT C_UPLOAD_TYPE_ID,
                   XL.RFPID,
                   XL.LANEID,
                   'Rating/Routing compatibility: cannot have city, state, destination postal code'
            FROM   XMLLANE XL
            WHERE  XL.RFPID = I_RFPID
            AND    XL.DESTINATIONCITY IS NOT NULL
            AND    ((XL.ACTIVE = 1) OR
                  (NOT EXISTS
                   (SELECT 1
                      FROM   LANE ML
                      WHERE  ML.RFPID = I_RFPID
                      AND    ML.LANEID = XL.LANEID
                      AND    ML.ROUND_NUM < ROUNDNUM)))
            AND    HASDESTZIP > 0
            AND    XL.DESTINATIONPOSTALCODE IS NOT NULL
            AND    NOT (HASDESTFAC = 1 AND
                   XL.DESTINATIONFACILITYCODE IS NOT NULL);

        END IF;
        -- end: HASDESTCITY condition




-- checking for errors

        INSERT INTO XMLERROR
          (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)
          SELECT C_UPLOAD_TYPE_ID,
                 XL.RFPID,
                 XL.LANEID,
                 'Rating/Routing compatibility: must specify origin facility, city, state, country, or postal code'
          FROM   XMLLANE XL
          WHERE  XL.RFPID = I_RFPID
          AND    ((XL.ACTIVE = 1) OR
                (NOT EXISTS (SELECT 1
                               FROM   LANE ML
                               WHERE  ML.RFPID = I_RFPID
                               AND    ML.LANEID = XL.LANEID
                               AND    ML.ROUND_NUM < ROUNDNUM)))
          AND    (HASORIGINFAC = 0 OR XL.ORIGINFACILITYCODE IS NULL)
          AND    (HASORIGINCITY = 0 OR XL.ORIGINCITY IS NULL)
          AND    (HASORIGINSTATE = 0 OR XL.ORIGINSTATEPROV IS NULL)
          AND    (HASORIGINZIP = 0 OR XL.ORIGINPOSTALCODE IS NULL)
          AND    (HASORIGINCOUNTRY = 0 OR XL.ORIGINCOUNTRYCODE IS NULL)
          AND    (HASORIGINZONE = 0 OR XL.ORIGINZONECODE IS NULL);


--checking for Rating/Routing compatibility: must specify destination facility, city, state, country, or postal code';
        INSERT INTO XMLERROR
          (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)
          SELECT C_UPLOAD_TYPE_ID,
                 XL.RFPID,
                 XL.LANEID,
                 'Rating/Routing compatibility: must specify destination facility, city, state, country, or postal code'
          FROM   XMLLANE XL
          WHERE  XL.RFPID = I_RFPID
          AND    ((XL.ACTIVE = 1) OR
                (NOT EXISTS (SELECT 1
                               FROM   LANE ML
                               WHERE  ML.RFPID = I_RFPID
                               AND    ML.LANEID = XL.LANEID
                               AND    ML.ROUND_NUM < ROUNDNUM)))
          AND    (HASDESTFAC = 0 OR XL.DESTINATIONFACILITYCODE IS NULL)
          AND    (HASDESTCITY = 0 OR XL.DESTINATIONCITY IS NULL)
          AND    (HASDESTSTATE = 0 OR XL.DESTINATIONSTATEPROV IS NULL)
          AND    (HASDESTZIP = 0 OR XL.DESTINATIONPOSTALCODE IS NULL)
          AND    (HASDESTCOUNTRY = 0 OR XL.DESTINATIONCOUNTRYCODE IS NULL)
          AND    (HASDESTZONE = 0 OR XL.DESTINATIONZONECODE IS NULL);
-- check for duplicate uploading lanes
FOR CURSOR1_SEASON IN (SELECT DISTINCT SEASON_ID,EFFECTIVE_DTTM,EXPIRY_DTTM FROM SEASON WHERE TC_COMPANY_ID IN (SELECT DISTINCT TC_COMPANY_ID FROM XMLLANE_SEASON WHERE rfpid = I_RFPID) AND SEASON_ID IN (SELECT SEASON_ID FROM XMLLANE))
 LOOP
FOR CURSOR2_SEASON IN (SELECT SEASON_ID,EFFECTIVE_DTTM,EXPIRY_DTTM FROM  SEASON WHERE TC_COMPANY_ID IN (SELECT DISTINCT TC_COMPANY_ID FROM XMLLANE_SEASON WHERE rfpid = I_RFPID)AND EFFECTIVE_DTTM<=CURSOR1_SEASON.EXPIRY_DTTM AND EXPIRY_DTTM>=CURSOR1_SEASON.EFFECTIVE_DTTM)
LOOP
INSERT INTO XMLERROR
(UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)
SELECT C_UPLOAD_TYPE_ID,
I_RFPID,
MAX(LANEID),
'Duplicate Lanes are not Allowed, Uploading lanes are duplicate'
FROM XMLLANE
WHERE  RFPID = I_RFPID
AND SEASON_ID IN (CURSOR1_SEASON.SEASON_ID, CURSOR2_SEASON.SEASON_ID)
GROUP  BY RFPID, ORIGINFACILITYCODE , ORIGINCITY , ORIGINSTATEPROV , ORIGINPOSTALCODE , ORIGINAIRPORT , ORIGINPORT ,ORIGINCOUNTRYCODE ,ORIGINCAPACITYAREACODE,
DESTINATIONFACILITYCODE, DESTINATIONCITY , DESTINATIONSTATEPROV , DESTINATIONPOSTALCODE , DESTINATIONAIRPORT , DESTINATIONPORT , DESTINATIONCOUNTRYCODE,
ORIGINZONECODE,DESTINATIONZONECODE,DESTINATIONCAPACITYAREACODE, EQUIPMENTTYPE_ID, SERVICETYPE_ID,PROTECTIONLEVEL_ID,MOT,SEASON_ID
HAVING COUNT(*) > 1;
END LOOP;
END LOOP;

INSERT INTO XMLERROR
(UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)
SELECT C_UPLOAD_TYPE_ID,
I_RFPID,
MAX(LANEID),
'Duplicate Lanes are not Allowed, Uploading lanes are duplicate'
FROM XMLLANE
WHERE  RFPID = I_RFPID
AND SEASON_ID IS NULL
GROUP  BY RFPID, ORIGINFACILITYCODE , ORIGINCITY , ORIGINSTATEPROV , ORIGINPOSTALCODE , ORIGINAIRPORT , ORIGINPORT ,ORIGINCOUNTRYCODE ,ORIGINCAPACITYAREACODE,
DESTINATIONFACILITYCODE, DESTINATIONCITY , DESTINATIONSTATEPROV , DESTINATIONPOSTALCODE , DESTINATIONAIRPORT , DESTINATIONPORT , DESTINATIONCOUNTRYCODE,
ORIGINZONECODE,DESTINATIONZONECODE,DESTINATIONCAPACITYAREACODE, EQUIPMENTTYPE_ID, SERVICETYPE_ID,PROTECTIONLEVEL_ID,MOT,SEASON_ID
HAVING COUNT(*) > 1;

INSERT INTO XMLERROR
  (UPLOAD_TYPE_ID, RFPID, LANEID, LANEEQUIPMENTTYPEID, ERRORDESC)
  SELECT C_UPLOAD_TYPE_ID,
     XL.RFPID,
     XL.LANEID,
     NVL(XLET.LANEEQUIPMENTTYPEID, 1),
     'mode should be part of Line Item'
  FROM XMLLANE XL LEFT OUTER JOIN XMLLANEEQUIPMENTTYPE XLET ON (XL.RFPID = I_RFPID AND XLET.RFPID = XL.RFPID AND XLET.LANEID = XL.LANEID)
  WHERE XL.RFPID = I_RFPID AND XLET.MOT IS NULL;
  
      END IF;-- end: ratingrouting condition

      FOR CURSOR_XMLLANE IN (SELECT XL.LANEID, L.ROUND_NUM
                             FROM   XMLLANE XL, LANE L
                             WHERE  XL.RFPID = I_RFPID
                             AND    L.RFPID = I_RFPID
                             AND    XL.LANEID = L.LANEID
                             AND    XL.CHANGED = 1
                             AND    L.ROUND_NUM =
                                    (SELECT MAX(ML.ROUND_NUM)
                                      FROM   LANE ML
                                      WHERE  ML.RFPID = I_RFPID
                                      AND    ML.LANEID = L.LANEID
                                      AND    ML.ROUND_NUM <
                                             (SELECT MAX(R.ROUND_NUM)
                                               FROM   RFP R
                                               WHERE  RFPID = I_RFPID)))

      LOOP
        S_LANEID    := CURSOR_XMLLANE.LANEID;
        S_ROUND_NUM := CURSOR_XMLLANE.ROUND_NUM;

        INSERT INTO XMLERROR
          (UPLOAD_TYPE_ID, RFPID, LANEID, ERRORDESC)

          SELECT C_UPLOAD_TYPE_ID,
                 XL.RFPID,
                 XL.LANEID,
                 'Lane can not have Lineitems'

          FROM   XMLLANE XL
          WHERE  XL.RFPID = I_RFPID
          AND    XL.LANEID NOT IN (SELECT LANEID
                                   FROM   LANE
                                   WHERE  RFPID = I_RFPID
                                   AND    LANE_TYPE <> 0)

          AND    XL.LANEID = S_LANEID
          AND    EXISTS (SELECT 1
                  FROM   XMLLANEEQUIPMENTTYPE XLE
                  WHERE  XLE.RFPID = XL.RFPID
                  AND    XLE.LANEID = XL.LANEID)
          AND    NOT EXISTS
           (SELECT 1
                  FROM   LANEEQUIPMENTTYPE LE
                  WHERE  LE.RFPID = XL.RFPID
                  AND    LE.LANEID = XL.LANEID
                  AND    (LE.PROTECTIONLEVEL_ID IS NOT NULL OR
                        LE.SERVICETYPE_ID IS NOT NULL OR
                        LE.MOT IS NOT NULL OR
                        LE.EQUIPMENTTYPE_ID IS NOT NULL OR
        		LE.COMMODITYCODE_ID IS NOT NULL));

      END LOOP;


      --CLOSE CURSOR_XMLLANE;

      --trace 'EXECUTE PROCEDURE XMLLANERFPCOLPROPERTIESUPLOAD';

      Xmllanerfpcolpropertiesupload(I_RFPID,ROUNDNUM);

      --trace '-- checking for errors';

      SELECT COUNT(*) CNT
      INTO   C_ERRORS
      FROM   XMLERROR
      WHERE  RFPID = I_RFPID
      AND    UPLOAD_TYPE_ID = C_UPLOAD_TYPE_ID;

      --trace 'error count = '||C_ERRORS;

      --trace '-- if there were no errors';

      IF C_ERRORS = 0

      THEN

        DELETE FROM PACKAGELANE
        WHERE  RFPID = I_RFPID
        AND    EXISTS (SELECT 'x'
                FROM   PACKAGE P
                WHERE  PACKAGELANE.RFPID = P.RFPID
                AND    PACKAGELANE.PACKAGEID = P.PACKAGEID
                AND    P.ROUND_NUM = ROUNDNUM)

        AND    LANEID IN (SELECT LANEID
                          FROM   LANE
                          WHERE  RFPID = I_RFPID
                          AND    ROUND_NUM = ROUNDNUM
                          AND    LANE_TYPE = 0);

        DELETE FROM PACKAGE
        WHERE  RFPID = I_RFPID
        AND    ROUND_NUM = ROUNDNUM
        AND    PACKAGEID NOT IN (SELECT DISTINCT PACKAGEID
                                 FROM   PACKAGELANE
                                 WHERE  RFPID = I_RFPID);

        DELETE FROM LANE
        WHERE  RFPID = I_RFPID
        AND    ROUND_NUM = ROUNDNUM
        AND    LANE_TYPE = 0;

        DELETE FROM LANEEQUIPMENTTYPE
        WHERE  RFPID = I_RFPID
        AND    ROUND_NUM = ROUNDNUM
        AND    LANEID NOT IN (SELECT LANEID
                              FROM   LANE
                              WHERE  RFPID = I_RFPID
                              AND    ROUND_NUM = ROUNDNUM);

        DELETE FROM LANE_ACTIVE
        WHERE  RFPID = I_RFPID
        AND    ROUND_NUM = ROUNDNUM
        AND    LANEID NOT IN (SELECT LANEID
                              FROM   LANE
                              WHERE  RFPID = I_RFPID
                              AND    ROUND_NUM = ROUNDNUM);

        INSERT INTO LANE
          (LANEID,
           RFPID,
           ORIGINSHIPSITE,
           ORIGINSHIPSITECODE,
           ORIGINPORT,
           ORIGINCITY,
           ORIGINSTATEPROV,
           ORIGINCOUNTRYCODE,
           ORIGINPOSTALCODE,
           DESTINATIONSHIPSITE,
           DESTINATIONPORT,
           DESTINATIONSHIPSITECODE,
           DESTINATIONCITY,
           DESTINATIONSTATEPROV,
           DESTINATIONCOUNTRYCODE,
           DESTINATIONPOSTALCODE,
           CUSTOMERNAME,
           COMMODITYCODE,
           COMMODITYCODEDESC,
           HARMONIZEDCODE,
           RATETYPE,
           ISROUNDTRIP,
           AVERAGEWEIGHT,
           VOLUMEFREQUENCY,
           VOLUME,
           WEIGHTBREAKRATETYPE,
           WEIGHTBREAK1,
           WEIGHTBREAK2,
           WEIGHTBREAK3,
           WEIGHTBREAK4,
           WEIGHTBREAK5,
           WEIGHTBREAK6,
           WEIGHTBREAK7,
           WEIGHTBREAK8,
           WEIGHTBREAK9,
           WEIGHTUOM,
           WEIGHTBREAKUOM,
           MILEAGE,
           DISTANCEUOM,
           LCLCUBICSPACE,
           LCLCUBICSPACEUOM,
           PICKUPTYPE,
           SERVICECRITERIA,
           FREIGHTCLASS,
           ROUTINGTYPE,
           ISCONFERENCE,
           TRANSITTIME,
           TRANSITTIMEUOM,
           ISOVERSIZED,
           OVERSIZEDNOTE,
           ISHAZARDOUS,
           HAZARDOUSNOTE,
           ISPERISHABLE,
           PERISHABLENOTE,
           OTHER1,
           OTHER2,
           OTHER3,
           ISCANCELLED,
           ISDIRTY,
           CREATEDUID,
           CREATEDDTTM,
           LASTUPDATEDUID,
           LASTUPDATEDDTTM,
           CUSTTEXT1,
           CUSTTEXT2,
           CUSTTEXT3,
           CUSTTEXT4,
           CUSTTEXT5,
           CUSTTEXT6,
           CUSTTEXT7,
           CUSTTEXT8,
           CUSTTEXT9,
           CUSTTEXT10,
           CUSTINT1,
           CUSTINT2,
           CUSTINT3,
           CUSTINT4,
           CUSTINT5,
           CUSTINT6,
           CUSTINT7,
           CUSTINT8,
           CUSTINT9,
           CUSTINT10,
           CUSTDOUBLE1,
           CUSTDOUBLE2,
           CUSTDOUBLE3,
           CUSTDOUBLE4,
           CUSTDOUBLE5,
           CUSTDOUBLE6,
           CUSTDOUBLE7,
           CUSTDOUBLE8,
           CUSTDOUBLE9,
           CUSTDOUBLE10,
           ORIGINFACILITYCODE,
           ORIGINAIRPORT,
           DESTINATIONFACILITYCODE,
           DESTINATIONAIRPORT,
           HISTORICALCOST,
           NUMSTOPS,
           NUMCONTAINERS,
           SHIPMENTRATEDCOST,
           ROUND_NUM,
           ORIGINZONECODE,
           DESTINATIONZONECODE, SEASON_ID, SERVICETYPE_ID, EQUIPMENTTYPE_ID, PROTECTIONLEVEL_ID, MOT, BUSINESS_UNIT_ID, LANEATTRIBUTEVERSION, ORIGINCAPACITYAREACODE, DESTINATIONCAPACITYAREACODE)

          SELECT LANEID,
                 RFPID,
                 ORIGINSHIPSITE,
                 ORIGINSHIPSITECODE,
                 ORIGINPORT,
                 ORIGINCITY,
                 ORIGINSTATEPROV,
                 ORIGINCOUNTRYCODE,
                 ORIGINPOSTALCODE,
                 DESTINATIONSHIPSITE,
                 DESTINATIONPORT,
                 DESTINATIONSHIPSITECODE,
                 DESTINATIONCITY,
                 DESTINATIONSTATEPROV,
                 DESTINATIONCOUNTRYCODE,
                 DESTINATIONPOSTALCODE,
                 CUSTOMERNAME,
                 COMMODITYCODE,
                 COMMODITYCODEDESC,
                 HARMONIZEDCODE,
                 RATETYPE,
                 ISROUNDTRIP,
                 AVERAGEWEIGHT,
                 VOLUMEFREQUENCY,
                 VOLUME,
                 WEIGHTBREAKRATETYPE,
                 WEIGHTBREAK1,
                 WEIGHTBREAK2,
                 WEIGHTBREAK3,
                 WEIGHTBREAK4,
                 WEIGHTBREAK5,
                 WEIGHTBREAK6,
                 WEIGHTBREAK7,
                 WEIGHTBREAK8,
                 WEIGHTBREAK9,
                 WEIGHTUOM,
                 WEIGHTBREAKUOM,
                 MILEAGE,
                 DISTANCEUOM,
                 LCLCUBICSPACE,
                 LCLCUBICSPACEUOM,
                 PICKUPTYPE,
                 SERVICECRITERIA,
                 FREIGHTCLASS,
                 ROUTINGTYPE,
                 ISCONFERENCE,
                 TRANSITTIME,
                 TRANSITTIMEUOM,
                 ISOVERSIZED,
                 OVERSIZEDNOTE,
                 ISHAZARDOUS,
                 HAZARDOUSNOTE,
                 ISPERISHABLE,
                 PERISHABLENOTE,
                 OTHER1,
                 OTHER2,
                 OTHER3,
                 ISCANCELLED,
                 ISDIRTY,
                 CREATEDUID,
                 CREATEDDTTM,
                 LASTUPDATEDUID,
                 LASTUPDATEDDTTM,
                 CUSTTEXT1,
                 CUSTTEXT2,
                 CUSTTEXT3,
                 CUSTTEXT4,
                 CUSTTEXT5,
                 CUSTTEXT6,
                 CUSTTEXT7,
                 CUSTTEXT8,
                 CUSTTEXT9,
                 CUSTTEXT10,
                 CUSTINT1,
                 CUSTINT2,
                 CUSTINT3,
                 CUSTINT4,
                 CUSTINT5,
                 CUSTINT6,
                 CUSTINT7,
                 CUSTINT8,
                 CUSTINT9,
                 CUSTINT10,
                 CUSTDOUBLE1,
                 CUSTDOUBLE2,
                 CUSTDOUBLE3,
                 CUSTDOUBLE4,
                 CUSTDOUBLE5,
                 CUSTDOUBLE6,
                 CUSTDOUBLE7,
                 CUSTDOUBLE8,
                 CUSTDOUBLE9,
                 CUSTDOUBLE10,
                 ORIGINFACILITYCODE,
                 ORIGINAIRPORT,
                 DESTINATIONFACILITYCODE,
                 DESTINATIONAIRPORT,
                 HISTORICALCOST,
                 NUMSTOPS,
                 NUMCONTAINERS,
                 SHIPMENTRATEDCOST,
                 ROUNDNUM,
                 ORIGINZONECODE,
                 DESTINATIONZONECODE, SEASON_ID, SERVICETYPE_ID, EQUIPMENTTYPE_ID, PROTECTIONLEVEL_ID, MOT, BUSINESS_UNIT_ID, CURRENTATTRIBUTEVERSION, ORIGINCAPACITYAREACODE, DESTINATIONCAPACITYAREACODE
          FROM   XMLLANE XL
          WHERE  XL.RFPID = I_RFPID
          AND    XL.CHANGED = 1
          AND    ((XL.ACTIVE = 1) OR
                (NOT EXISTS (SELECT 1
                               FROM   LANE ML
                               WHERE  ML.RFPID = I_RFPID
                               AND    ML.LANEID = XL.LANEID
                               AND    ML.ROUND_NUM < ROUNDNUM)));

        IF (CURRENTATTRIBUTEVERSION <> PREVIOUSATTRIBUTEVERSION)
        THEN

          INSERT INTO LANE
            (LANEID,
             RFPID,
             ORIGINSHIPSITE,
             ORIGINSHIPSITECODE,
             ORIGINPORT,
             ORIGINCITY,
             ORIGINSTATEPROV,
             ORIGINCOUNTRYCODE,
             ORIGINPOSTALCODE,
             DESTINATIONSHIPSITE,
             DESTINATIONPORT,
             DESTINATIONSHIPSITECODE,
             DESTINATIONCITY,
             DESTINATIONSTATEPROV,
             DESTINATIONCOUNTRYCODE,
             DESTINATIONPOSTALCODE,
             CUSTOMERNAME,
             COMMODITYCODE,
             COMMODITYCODEDESC,
             HARMONIZEDCODE,
             RATETYPE,
             ISROUNDTRIP,
             AVERAGEWEIGHT,
             VOLUMEFREQUENCY,
             VOLUME,
             WEIGHTBREAKRATETYPE,
             WEIGHTBREAK1,
             WEIGHTBREAK2,
             WEIGHTBREAK3,
             WEIGHTBREAK4,
             WEIGHTBREAK5,
             WEIGHTBREAK6,
             WEIGHTBREAK7,
             WEIGHTBREAK8,
             WEIGHTBREAK9,
             WEIGHTUOM,
             WEIGHTBREAKUOM,
             MILEAGE,
             DISTANCEUOM,
             LCLCUBICSPACE,
             LCLCUBICSPACEUOM,
             PICKUPTYPE,
             SERVICECRITERIA,
             FREIGHTCLASS,
             ROUTINGTYPE,
             ISCONFERENCE,
             TRANSITTIME,
             TRANSITTIMEUOM,
             ISOVERSIZED,
             OVERSIZEDNOTE,
             ISHAZARDOUS,
             HAZARDOUSNOTE,
             ISPERISHABLE,
             PERISHABLENOTE,
             OTHER1,
             OTHER2,
             OTHER3,
             ISCANCELLED,
             ISDIRTY,
             CREATEDUID,
             CREATEDDTTM,
             LASTUPDATEDUID,
             LASTUPDATEDDTTM,
             CUSTTEXT1,
             CUSTTEXT2,
             CUSTTEXT3,
             CUSTTEXT4,
             CUSTTEXT5,
             CUSTTEXT6,
             CUSTTEXT7,
             CUSTTEXT8,
             CUSTTEXT9,
             CUSTTEXT10,
             CUSTINT1,
             CUSTINT2,
             CUSTINT3,
             CUSTINT4,
             CUSTINT5,
             CUSTINT6,
             CUSTINT7,
             CUSTINT8,
             CUSTINT9,
             CUSTINT10,
             CUSTDOUBLE1,
             CUSTDOUBLE2,
             CUSTDOUBLE3,
             CUSTDOUBLE4,
             CUSTDOUBLE5,
             CUSTDOUBLE6,
             CUSTDOUBLE7,
             CUSTDOUBLE8,
             CUSTDOUBLE9,
             CUSTDOUBLE10,
             ORIGINFACILITYCODE,
             ORIGINAIRPORT,
             DESTINATIONFACILITYCODE,
             DESTINATIONAIRPORT,
             HISTORICALCOST,
             NUMSTOPS,
             NUMCONTAINERS,
             SHIPMENTRATEDCOST,
             ROUND_NUM,
             ORIGINZONECODE,
             DESTINATIONZONECODE, SEASON_ID, SERVICETYPE_ID, EQUIPMENTTYPE_ID, PROTECTIONLEVEL_ID, MOT, BUSINESS_UNIT_ID, ORIGINCAPACITYAREACODE, DESTINATIONCAPACITYAREACODE)

            SELECT LANEID,
                   RFPID,
                   ORIGINSHIPSITE,
                   ORIGINSHIPSITECODE,
                   ORIGINPORT,
                   ORIGINCITY,
                   ORIGINSTATEPROV,
                   ORIGINCOUNTRYCODE,
                   ORIGINPOSTALCODE,
                   DESTINATIONSHIPSITE,
                   DESTINATIONPORT,
                   DESTINATIONSHIPSITECODE,
                   DESTINATIONCITY,
                   DESTINATIONSTATEPROV,
                   DESTINATIONCOUNTRYCODE,
                   DESTINATIONPOSTALCODE,
                   CUSTOMERNAME,
                   COMMODITYCODE,
                   COMMODITYCODEDESC,
                   HARMONIZEDCODE,
                   RATETYPE,
                   ISROUNDTRIP,
                   AVERAGEWEIGHT,
                   VOLUMEFREQUENCY,
                   VOLUME,
                   WEIGHTBREAKRATETYPE,
                   WEIGHTBREAK1,
                   WEIGHTBREAK2,
                   WEIGHTBREAK3,
                   WEIGHTBREAK4,
                   WEIGHTBREAK5,
                   WEIGHTBREAK6,
                   WEIGHTBREAK7,
                   WEIGHTBREAK8,
                   WEIGHTBREAK9,
                   WEIGHTUOM,
                   WEIGHTBREAKUOM,
                   MILEAGE,
                   DISTANCEUOM,
                   LCLCUBICSPACE,
                   LCLCUBICSPACEUOM,
                   PICKUPTYPE,
                   SERVICECRITERIA,
                   FREIGHTCLASS,
                   ROUTINGTYPE,
                   ISCONFERENCE,
                   TRANSITTIME,
                   TRANSITTIMEUOM,
                   ISOVERSIZED,
                   OVERSIZEDNOTE,
                   ISHAZARDOUS,
                   HAZARDOUSNOTE,
                   ISPERISHABLE,
                   PERISHABLENOTE,
                   OTHER1,
                   OTHER2,
                   OTHER3,
                   ISCANCELLED,
                   ISDIRTY,
                   CREATEDUID,
                   CREATEDDTTM,
                   LASTUPDATEDUID,
                   LASTUPDATEDDTTM,
                   CUSTTEXT1,
                   CUSTTEXT2,
                   CUSTTEXT3,
                   CUSTTEXT4,
                   CUSTTEXT5,
                   CUSTTEXT6,
                   CUSTTEXT7,
                   CUSTTEXT8,
                   CUSTTEXT9,
                   CUSTTEXT10,
                   CUSTINT1,
                   CUSTINT2,
                   CUSTINT3,
                   CUSTINT4,
                   CUSTINT5,
                   CUSTINT6,
                   CUSTINT7,
                   CUSTINT8,
                   CUSTINT9,
                   CUSTINT10,
                   CUSTDOUBLE1,
                   CUSTDOUBLE2,
                   CUSTDOUBLE3,
                   CUSTDOUBLE4,
                   CUSTDOUBLE5,
                   CUSTDOUBLE6,
                   CUSTDOUBLE7,
                   CUSTDOUBLE8,
                   CUSTDOUBLE9,
                   CUSTDOUBLE10,
                   ORIGINFACILITYCODE,
                   ORIGINAIRPORT,
                   DESTINATIONFACILITYCODE,
                   DESTINATIONAIRPORT,
                   HISTORICALCOST,
                   NUMSTOPS,
                   NUMCONTAINERS,
                   SHIPMENTRATEDCOST,
                   ROUNDNUM,
                   ORIGINZONECODE,
                   DESTINATIONZONECODE, SEASON_ID, SERVICETYPE_ID, EQUIPMENTTYPE_ID, PROTECTIONLEVEL_ID, MOT, BUSINESS_UNIT_ID, ORIGINCAPACITYAREACODE, DESTINATIONCAPACITYAREACODE
            FROM   LANE L
            WHERE  L.RFPID = I_RFPID
            AND    L.ROUND_NUM =
                   (SELECT MAX(ML.ROUND_NUM)
                     FROM   LANE ML
                     WHERE  ML.RFPID = L.RFPID
                     AND    ML.LANEID = L.LANEID)
            AND    EXISTS
             (SELECT 'x'
                    FROM   XMLLANE XL
                    WHERE  XL.RFPID = I_RFPID
                    AND    XL.LANEID = L.LANEID
                    AND    XL.CHANGED = 0
                    AND    ((XL.ACTIVE = 1) OR
                          (NOT EXISTS
                           (SELECT 1
                              FROM   LANE ML
                              WHERE  ML.RFPID = I_RFPID
                              AND    ML.LANEID = XL.LANEID
                              AND    ML.ROUND_NUM < ROUNDNUM))))

            ;

        END IF;

        --trace '--debug step 5';

        --        trace  'step 5 ';

        IF (ROUNDNUM = 1)
        THEN

          INSERT INTO LANE_ACTIVE
            (RFPID, LANEID, ROUND_NUM, ACTIVE)
            SELECT RFPID, LANEID, ROUNDNUM, ACTIVE
            FROM   XMLLANE XL
            WHERE  XL.RFPID = I_RFPID;

          --trace '--debug step 6';

          --        trace  'step 6 ';

        ELSE

          INSERT INTO LANE_ACTIVE
            (RFPID, LANEID, ROUND_NUM, ACTIVE)
            SELECT LA.RFPID, LA.LANEID, ROUNDNUM, LA.ACTIVE
            FROM   LANE_ACTIVE LA
            WHERE  LA.RFPID = I_RFPID
            AND    LA.ROUND_NUM = ROUNDNUM - 1;

          UPDATE LANE_ACTIVE
          SET    ACTIVE = (SELECT XL.ACTIVE
                           FROM   XMLLANE XL
                           WHERE  XL.RFPID = I_RFPID
                           AND    XL.LANEID = LANE_ACTIVE.LANEID)

          WHERE  LANE_ACTIVE.RFPID = I_RFPID
          AND    LANE_ACTIVE.ROUND_NUM = ROUNDNUM
          AND    EXISTS (SELECT 1
                  FROM   XMLLANE XL
                  WHERE  XL.RFPID = LANE_ACTIVE.RFPID
                  AND    XL.LANEID = LANE_ACTIVE.LANEID);

          INSERT INTO LANE_ACTIVE
            (RFPID, LANEID, ROUND_NUM, ACTIVE)

            SELECT RFPID, LANEID, ROUNDNUM, ACTIVE
            FROM   XMLLANE XL
            WHERE  XL.RFPID = I_RFPID
            AND    NOT EXISTS (SELECT 1
                    FROM   LANE_ACTIVE LA
                    WHERE  LA.RFPID = XL.RFPID
                    AND    LA.LANEID = XL.LANEID
                    AND    LA.ROUND_NUM = ROUNDNUM)
            AND    NOT EXISTS
             (SELECT 1 FROM XMLERROR WHERE RFPID = I_RFPID);
        END IF;

        --trace '-- begin1';

        FOR CURSOR_XMLLANEEQUIPMENTTYPE IN CURSOR_TEST

        LOOP
          --FETCH CURSOR_XMLLANEEQUIPMENTTYPE     into
          --VALSEQ             := CURSOR_XMLLANEEQUIPMENTTYPE.SEQ;
          VALLANEID          := CURSOR_XMLLANEEQUIPMENTTYPE.LANEID;
          VALRFPID           := CURSOR_XMLLANEEQUIPMENTTYPE.RFPID;
          VALVOLUME          := CURSOR_XMLLANEEQUIPMENTTYPE.VOLUME;
          VALFREQUENCY       := CURSOR_XMLLANEEQUIPMENTTYPE.FREQUENCY;
          VALCREATEDUID      := CURSOR_XMLLANEEQUIPMENTTYPE.CREATEDUID;
          VALCREATEDDTTM     := CURSOR_XMLLANEEQUIPMENTTYPE.CREATEDDTTM;
          VALLASTUPDATEDUID  := CURSOR_XMLLANEEQUIPMENTTYPE.LASTUPDATEDUID;
          VALLASTUPDATEDDTTM := CURSOR_XMLLANEEQUIPMENTTYPE.LASTUPDATEDDTTM;
          VALPROTECTIONLEVEL_ID := CURSOR_XMLLANEEQUIPMENTTYPE.PROTECTIONLEVEL_ID;
          VALEQUIPMENTTYPE_ID := CURSOR_XMLLANEEQUIPMENTTYPE.EQUIPMENTTYPE_ID;
          VALSERVICETYPE_ID     := CURSOR_XMLLANEEQUIPMENTTYPE.SERVICETYPE_ID;
          VALMOT	     := CURSOR_XMLLANEEQUIPMENTTYPE.MOT;
          VALCOMMODITYCODE_ID := CURSOR_XMLLANEEQUIPMENTTYPE.COMMODITYCODE_ID;
          VALROUNDNUM        := CURSOR_XMLLANEEQUIPMENTTYPE.ROUND_NUM;

          --trace '--debug step 8';

          --        trace  'step 8 ';

          INSERT INTO LANEEQUIPMENTTYPE

            (LANEEQUIPMENTTYPEID,
             LANEID,
             RFPID,
             VOLUME,
             FREQUENCY,
             CREATEDUID,
             CREATEDDTTM,
             LASTUPDATEDUID,
             LASTUPDATEDDTTM,
             PROTECTIONLEVEL_ID,
             EQUIPMENTTYPE_ID,
             SERVICETYPE_ID,
             MOT,
             COMMODITYCODE_ID,
             ROUND_NUM)

          VALUES

            (LANEEQUIPMENTTYPE_SEQUENCE.NEXTVAL,
             VALLANEID,
             VALRFPID,
             VALVOLUME,
             VALFREQUENCY,
             VALCREATEDUID,
             VALCREATEDDTTM,
             VALLASTUPDATEDUID,
             VALLASTUPDATEDDTTM,
             VALPROTECTIONLEVEL_ID,
             VALEQUIPMENTTYPE_ID,
             VALSERVICETYPE_ID,
             VALMOT,
             VALCOMMODITYCODE_ID,
             VALROUNDNUM);

        END LOOP;
        -- CLOSE CURSOR_XMLLANEEQUIPMENTTYPE ;

        --trace '--begin2';

        FOR CURSOR_LANE IN (

                            SELECT LANEEQUIPMENTTYPE_SEQUENCE.NEXTVAL SEQ,
                                    LANEID,
                                    RFPID,
                                    VOLUME,
                                    VOLUMEFREQUENCY,
                                    CREATEDUID,
                                    CREATEDDTTM,
                                    LASTUPDATEDUID,
                                    LASTUPDATEDDTTM,
                                    NULL PROTECTIONLEVEL_ID,
                                    NULL EQUIPMENTTYPE_ID,
                                    NULL SERVICETYPE_ID,
                                    NULL MOT,
                                    NULL COMMODITYCODE_ID,
                                    ROUNDNUM ROUND_NUM
                            FROM   LANE L
                            WHERE  L.RFPID = I_RFPID
                            AND    NOT EXISTS
                             (SELECT 1
                                    FROM   LANEEQUIPMENTTYPE LEQT
                                    WHERE  LEQT.RFPID = L.RFPID
                                    AND    LEQT.LANEID = L.LANEID))

        LOOP
          --FETCH CURSOR_LANE INTO
          VALSEQ             := CURSOR_LANE.SEQ;
          VALLANEID          := CURSOR_LANE.LANEID;
          VALRFPID           := CURSOR_LANE.RFPID;
          VALVOLUME          := CURSOR_LANE.VOLUME;
          VALFREQUENCY       := CURSOR_LANE.VOLUMEFREQUENCY;
          VALCREATEDUID      := CURSOR_LANE.CREATEDUID;
          VALCREATEDDTTM     := CURSOR_LANE.CREATEDDTTM;
          VALLASTUPDATEDUID  := CURSOR_LANE.LASTUPDATEDUID;
          VALLASTUPDATEDDTTM := CURSOR_LANE.LASTUPDATEDDTTM;
          VALPROTECTIONLEVEL_ID := CURSOR_LANE.PROTECTIONLEVEL_ID;
          VALEQUIPMENTTYPE_ID := CURSOR_LANE.EQUIPMENTTYPE_ID;
          VALSERVICETYPE_ID     := CURSOR_LANE.SERVICETYPE_ID;
          VALMOT             := CURSOR_LANE.MOT;
          VALCOMMODITYCODE_ID := CURSOR_LANE.COMMODITYCODE_ID;
          VALROUNDNUM        := CURSOR_LANE.ROUND_NUM;

          INSERT INTO LANEEQUIPMENTTYPE

            (LANEEQUIPMENTTYPEID,
             LANEID,
             RFPID,
             VOLUME,
             FREQUENCY,
             CREATEDUID,
             CREATEDDTTM,
             LASTUPDATEDUID,
             LASTUPDATEDDTTM,
             PROTECTIONLEVEL_ID,
             EQUIPMENTTYPE_ID,
             SERVICETYPE_ID,
             MOT,
             COMMODITYCODE_ID,
             ROUND_NUM)

          VALUES

            (VALSEQ,
             VALLANEID,
             VALRFPID,
             VALVOLUME,
             VALFREQUENCY,
             VALCREATEDUID,
             VALCREATEDDTTM,
             VALLASTUPDATEDUID,
             VALLASTUPDATEDDTTM,
             VALPROTECTIONLEVEL_ID,
             VALEQUIPMENTTYPE_ID,
             VALSERVICETYPE_ID,
             VALMOT,
             VALCOMMODITYCODE_ID,
             VALROUNDNUM);

        END LOOP;
        --CLOSE CURSOR_LANE;

        IF S_ALLOWPACKAGES = 1
        THEN

          FOR CURSOR_XMLPACKAGE IN (SELECT DISTINCT RFPID,
                                                    OBCARRIERCODEID,
                                                    PACKAGENAME
                                    FROM   XMLPACKAGE
                                    WHERE  RFPID = I_RFPID
                                    AND    OBCARRIERCODEID IS NULL
                                    AND    NOT EXISTS
                                     (SELECT 1
                                            FROM   PACKAGE P
                                            WHERE  P.RFPID = XMLPACKAGE.RFPID
                                            AND    P.PACKAGENAME =
                                                   XMLPACKAGE.PACKAGENAME
                                            AND    P.ROUND_NUM <
                                                   (SELECT MAX(ROUND_NUM)
                                                     FROM   RFP
                                                     WHERE  RFPID = I_RFPID)))

          LOOP
            --FETCH CURSOR_XMLPACKAGE INTO
            S_RFPID           := CURSOR_XMLPACKAGE.RFPID;
            S_OBCARRIERCODEID := CURSOR_XMLPACKAGE.OBCARRIERCODEID;
            S_PACKAGENAME     := CURSOR_XMLPACKAGE.PACKAGENAME;

            INSERT INTO PACKAGE
              (PACKAGEID,
               RFPID,
               OBCARRIERCODEID,
               PACKAGENAME,
               TOTALVOLUME,
               AWARDSTATUS,
               ROUND_NUM)
            VALUES
              (PACKAGE_SEQUENCE.NEXTVAL,
               S_RFPID,
               S_OBCARRIERCODEID,
               S_PACKAGENAME,
               0,
               NULL,
               ROUNDNUM);

            INSERT INTO PACKAGELANE
              (PACKAGEID, RFPID, LANEID)
              SELECT PACKAGE_SEQUENCE.CURRVAL, XP.RFPID, XP.LANEID
              FROM   XMLPACKAGE XP
              WHERE  XP.RFPID = I_RFPID
              AND    XP.LANEID IN (SELECT DISTINCT LANEID
                                   FROM   LANE
                                   WHERE  RFPID = I_RFPID)
              AND    XP.PACKAGENAME = S_PACKAGENAME
              AND    XP.OBCARRIERCODEID IS NULL;

          END LOOP;
          -- CLOSE CURSOR_XMLPACKAGE ;

          UPDATE PACKAGE
          SET    TOTALVOLUME = (SELECT SUM(CAST(L.VOLUME * 7 AS FLOAT) /
                                           CASE L.VOLUMEFREQUENCY
                                             WHEN 0 THEN
                                              1
                                             WHEN 1 THEN
                                              7
                                             WHEN 2 THEN
                                              14
                                             WHEN 3 THEN
                                              ROUND(30.4166)
                                             WHEN 4 THEN
                                              ROUND(182.5)
                                             WHEN 5 THEN
                                              365
                                             ELSE
                                              7
                                           END)

                                FROM   LANE L, PACKAGELANE PL
                                WHERE  L.RFPID = PL.RFPID
                                AND    L.LANEID = PL.LANEID
                                AND    L.RFPID = PACKAGE.RFPID
                                AND    PL.PACKAGEID = PACKAGE.PACKAGEID
                                AND    L.ROUND_NUM =
                                       (SELECT MAX(ML.ROUND_NUM)
                                         FROM   LANE ML
                                         WHERE  ML.RFPID = L.RFPID
                                         AND    ML.LANEID = L.LANEID))

          WHERE  RFPID = I_RFPID
          AND    ROUND_NUM = ROUNDNUM
          AND    OBCARRIERCODEID IS NULL;

        END IF;

        SELECT COMPLEX_OPTIMIZATION
        INTO   L_COMPLEX_OPTIMIZATION
        FROM   RFP
        WHERE  RFPID = I_RFPID
        AND    ROUND_NUM =
               (SELECT MAX(ROUND_NUM) FROM RFP WHERE RFPID = I_RFPID);

--        IF (L_COMPLEX_OPTIMIZATION = 1)        THEN

          DELETE FROM OB200CONTRACT_TYPE WHERE RFPID = I_RFPID;

          K := 1;

          FOR CURSOR_LANEEQUIPMENTTYPE IN (SELECT DISTINCT EQUIPMENTTYPE_ID,
                                                           SERVICETYPE_ID,
                                                           PROTECTIONLEVEL_ID,
                                                           MOT,
                                                           COMMODITYCODE_ID
                                           FROM   LANEEQUIPMENTTYPE
                                           WHERE  RFPID = I_RFPID)
          LOOP
            --FETCH CURSOR_laneequipmenttype INTO
            V_EQUIPMENTTYPE_ID := CURSOR_LANEEQUIPMENTTYPE.EQUIPMENTTYPE_ID;
            V_SERVICETYPE_ID     := CURSOR_LANEEQUIPMENTTYPE.SERVICETYPE_ID;
            V_PROTECTIONLEVEL_ID := CURSOR_LANEEQUIPMENTTYPE.PROTECTIONLEVEL_ID;
            V_MOT             := CURSOR_LANEEQUIPMENTTYPE.MOT;
            V_COMMODITYCODE_ID := CURSOR_LANEEQUIPMENTTYPE.COMMODITYCODE_ID;



            IF V_EQUIPMENTTYPE_ID IS NULL AND V_SERVICETYPE_ID IS NULL AND
               V_PROTECTIONLEVEL_ID IS NULL AND V_MOT IS NULL AND V_COMMODITYCODE_ID IS NULL
            THEN
              SELECT COUNT(1)
              INTO   TMPCONDITION
              FROM   OB200CONTRACT_TYPE
              WHERE  RFPID = I_RFPID
              AND    CONTRACT_TYPE = 0 ;
              IF TMPCONDITION = 0
              THEN
                INSERT INTO OB200CONTRACT_TYPE
                  (CONTRACT_TYPE,
                   NAME,
                   EQUIPMENTTYPE_ID,
                   SERVICETYPE_ID,
                   PROTECTIONLEVEL_ID,
                   MOT,
                   COMMODITYCODE_ID,
                   RFPID)
                VALUES
                  (0,
                   NULL,
                   V_EQUIPMENTTYPE_ID,
                   V_SERVICETYPE_ID,
                   V_PROTECTIONLEVEL_ID,
                   V_MOT,
                   V_COMMODITYCODE_ID,
                   I_RFPID);
              END IF;

            ELSE
              SELECT COUNT(1)
              INTO   TMPCONDITION
              FROM   OB200CONTRACT_TYPE
              WHERE  RFPID = I_RFPID
              AND    EQUIPMENTTYPE_ID = V_EQUIPMENTTYPE_ID
              AND    SERVICETYPE_ID = V_SERVICETYPE_ID
              AND    PROTECTIONLEVEL_ID = V_PROTECTIONLEVEL_ID
              AND    MOT = V_MOT
              AND  COMMODITYCODE_ID = V_COMMODITYCODE_ID;
              IF TMPCONDITION = 0
              THEN
                INSERT INTO OB200CONTRACT_TYPE
                  (CONTRACT_TYPE,
                   NAME,
                   EQUIPMENTTYPE_ID,
                   SERVICETYPE_ID,
                   PROTECTIONLEVEL_ID,
                   MOT,
                   COMMODITYCODE_ID,
                   RFPID)
                VALUES
                  (K,
                   NULL,
                   V_EQUIPMENTTYPE_ID,
                   V_SERVICETYPE_ID,
                   V_PROTECTIONLEVEL_ID,
                   V_MOT,
                    V_COMMODITYCODE_ID,
                   I_RFPID);
                K := K + 1;
              END IF;
            END IF;

          END LOOP;
          --CLOSE CURSOR_laneequipmenttype ;


--        END IF;


      -- end if of c_errros
      END IF;

      /*
        1-l_rec_processed='||l_rec_processed );
        ROUNDNUM='||ROUNDNUM);
        rfpmode='||rfpmode);
        tc='||tc);
        ratingrouting='||ratingrouting);
        s_allowpackages='||s_allowpackages);
      */


END;
/

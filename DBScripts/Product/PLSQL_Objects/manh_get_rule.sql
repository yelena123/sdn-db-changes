create or replace function manh_get_rule
(
    p_rule_id             in pls_integer,
    p_sel_or_sort         in pls_integer,
    p_rule_type           in rule_hdr.rule_type%type default 'SS',
    p_facility_id         in facility.facility_id%type default null
) return varchar2
as
    v_rule                varchar2(4000);
    type ga_rule_cond     is table of varchar2(1000) index by binary_integer;
    va_rule_cond          ga_rule_cond;
    v_rule_select         varchar2(4000);
    v_rule_sort           varchar2(1000);
    v_ucl_user            varchar2(30);
    v_curr_datetime       timestamp with time zone:= systimestamp;
    v_curr_date           timestamp;
    v_fmt_model           varchar2(25) := 'mm/dd/yyyy hh24:mi:ss';
begin
    select us.table_owner
    into v_ucl_user
    from user_synonyms us
    where us.synonym_name = 'FACILITY';

    if (p_facility_id is not null)
    then
        -- convert to fac date/time
        select systimestamp at time zone tz.time_zone_name
        into v_curr_datetime
        from facility f
        join time_zone tz on tz.time_zone_id = f.facility_tz
        where f.facility_id = p_facility_id;
    end if;
    v_curr_date := trunc(v_curr_datetime);

    if (p_sel_or_sort = 0)
    then
        select
        case
          WHEN UPPER (oper_code) in ( '=', '<=', '<','>','>=')
              and (rule_cmpar_value is not null  OR INSTR (UPPER (rule_cmpar_value), '@DATE') <> 0) 
              and instr(upper(rule_cmpar_value), '@DATETIME') = 0 
              and (utc.data_type = 'DATE' or utc.data_type like 'TIMESTAMP%')
          THEN COALESCE(open_paran, ' ') || 'trunc'|| '(' || aliased_tbl_name|| '.'|| colm_name|| ')' || ' '
          ELSE COALESCE(open_paran, ' ') || aliased_tbl_name || '.'|| colm_name|| ' '
          END
            || ' '||
        case upper(oper_code)
            when '@DATE' then ' = to_date(''' ||
                to_char(to_date(rule_cmpar_value, v_fmt_model), v_fmt_model) || ''''
                || ',''mm/dd/yyyy hh24:mi:ss'')'
            when 'BEGINS WITH' then 'LIKE ' || '''' || rule_cmpar_value || '%'''
            when 'ENDS WITH' then 'LIKE ''%' || rule_cmpar_value || ''''     
            when 'CONTAINS' then 'LIKE ''%' || rule_cmpar_value || '%'''
            when 'NULL' then 'IS NULL'
            when 'NOT NULL' then 'IS NOT NULL'
            else upper(oper_code)
        end
        || ' ' ||
        case
            when upper(oper_code) in ('@DATE', 'BEGINS WITH', 'ENDS WITH', 'CONTAINS', 'NULL', 'NOT NULL') then ''
            else
            (
                case
                    when rule_cmpar_value is not null and substr(rule_cmpar_value, 1, 1) = '^' then
                        substr(rule_cmpar_value, 2)
                    when utc.data_type in('CHAR', 'VARCHAR2') and rule_cmpar_value is not null then
                        '''' || rule_cmpar_value || ''''
                    when utc.data_type = 'NUMBER' then rule_cmpar_value
                    when utc.data_type = 'DATE' or utc.data_type like 'TIMESTAMP%' then
                        case
                            when upper(oper_code) = 'NULL' or
                                upper(oper_code) = 'NOT NULL' then ' '
                            when instr(rule_cmpar_value, '+') <> 0 
                                and instr(upper(rule_cmpar_value), '@DATETIME') <> 0 then
                                ' to_timestamp_tz(' || '''' || to_char(v_curr_datetime
                                    + substr(rule_cmpar_value, instr(rule_cmpar_value, '+') + 1),
                                    v_fmt_model) 
                                || ''''  || ',''mm/dd/yyyy hh24:mi:ss'')'
                            when instr(rule_cmpar_value, '+') <> 0 then
                                ' to_date(' || '''' || to_char(v_curr_date
                                    + substr(rule_cmpar_value, instr(rule_cmpar_value, '+') + 1),
                                    v_fmt_model) 
                                || '''' || ',''mm/dd/yyyy hh24:mi:ss'')'
                            when instr(rule_cmpar_value, '-') <> 0 
                                    and instr(upper(rule_cmpar_value), '@DATETIME') <> 0 then
                                ' to_timestamp_tz(' || '''' || to_char(v_curr_datetime
                                    - substr(rule_cmpar_value, instr(rule_cmpar_value, '-') + 1),
                                    v_fmt_model) 
                                || '''' || ',''mm/dd/yyyy hh24:mi:ss'')'
                            when instr(rule_cmpar_value, '-') <> 0 then
                                ' to_date(' || '''' || to_char(v_curr_date
                                    - substr(rule_cmpar_value, instr(rule_cmpar_value, '-') + 1),
                                    v_fmt_model) 
                                || '''' || ',''mm/dd/yyyy hh24:mi:ss'')'
                            when instr(upper(rule_cmpar_value), '@DATETIME') <> 0 then
                                ' to_timestamp_tz(' || '''' 
                                    || to_char(v_curr_datetime, v_fmt_model)
                                || '''' || ',''mm/dd/yyyy hh24:mi:ss'')'
                            when instr(upper(rule_cmpar_value), '@DATE') <> 0 then 
                                ' to_date(' || ''''
                                    || to_char(v_curr_date, v_fmt_model) 
                                || '''' || ',''mm/dd/yyyy hh24:mi:ss'')'
                            when rule_cmpar_value is not null then
                                ' to_date(' || '''' 
                                    || to_char(to_date(rule_cmpar_value, v_fmt_model), v_fmt_model)
                                || '''' || ',''mm/dd/yyyy hh24:mi:ss'')'
                            else 
                                ' to_date(' || ''''
                                    || to_char(trunc(sysdate), 'mm/dd/yyyy')
                                || '''' || ',''mm/dd/yyyy'')'
                        end
                    else rule_cmpar_value
                end
            )
        end
        || coalesce(close_paran, '') || ' '
        || case when and_or_or = 'A' then 'AND' when and_or_or = 'O' then 'OR' else '' end
        bulk collect into va_rule_cond
        from rule_hdr
        join
        (
            select rsd.rule_id, rsd.tbl_name aliased_tbl_name, rsd.colm_name,
                rsd.sel_seq_nbr, rsd.and_or_or, rsd.close_paran,
                rsd.rule_cmpar_value, rsd.oper_code, rsd.open_paran,
                case when tbl_name in ('ALIAS_OLPN', 'ALIAS_IBPALLET') then 'LPN' 
                    when tbl_name in ('ALIAS_DLH') then 'LOCN_HDR' 
                    when tbl_name in ('ALIAS_DPL') then 'PICK_LOCN_HDR'
                    when tbl_name in ('ALIAS_DRL') then 'RESV_LOCN_HDR'
                    when rsd.tbl_name in ('PICK_LOCN_SUMM', 'PICK_LOCN_SUMM_DEST') then 'PICK_LOCN_SUMM_VW'
                    else tbl_name 
                end tbl_name
            from rule_sel_dtl rsd
            where rsd.rule_id = p_rule_id
        ) iv on iv.rule_id = rule_hdr.rule_id
        join all_tab_columns utc on iv.tbl_name = utc.table_name and iv.colm_name = utc.column_name
        where rule_type = p_rule_type and utc.owner in (user, v_ucl_user)
        order by iv.rule_id, iv.sel_seq_nbr;

        for i in 1 .. va_rule_cond.count
        loop
            v_rule_select := v_rule_select || va_rule_cond(i);
        end loop;

        if (v_rule_select is not null)
        then
            v_rule := ' ( ' || v_rule_select || ' ) ';
        end if;
    end if;

    if (p_sel_or_sort = 1)
    then
        select ' ' || ',' || rsd.tbl_name || '.' || rsd.colm_name || ' '
            || decode(rsd.sort_seq, 'A', 'ASC', 'DESC')
        bulk collect into va_rule_cond
        from rule_hdr rh
        join rule_sort_dtl rsd on rsd.rule_id = rh.rule_id
        where rh.rule_type = p_rule_type and rh.rule_id = p_rule_id
        order by rsd.rule_id, rsd.sort_seq_nbr;

        for i in 1 .. va_rule_cond.count
        loop
            v_rule_sort := v_rule_sort || va_rule_cond(i);
        end loop;

        v_rule := v_rule_sort;
    end if;

    return(v_rule);
end;
/
show errors;

create or replace procedure manh_wave_sku_substitution
(
    p_order_id              in order_line_item.order_id%type,
    p_line_item_id          in order_line_item.line_item_id%type,
    p_ship_group_id         in orders.ship_group_id%type,
    p_sku_sub               in wave_parm.sku_sub%type,
    p_item_id               in order_line_item.item_id%type,
    p_invn_type             in order_line_item.invn_type%type,
    p_prod_stat             in order_line_item.prod_stat%type,
    p_batch_nbr             in order_line_item.batch_nbr%type,
    p_item_attr_1           in order_line_item.item_attr_1%type,
    p_item_attr_2           in order_line_item.item_attr_2%type,
    p_item_attr_3           in order_line_item.item_attr_3%type,
    p_item_attr_4           in order_line_item.item_attr_4%type,
    p_item_attr_5           in order_line_item.item_attr_5%type,
    p_cntry_of_orgn         in order_line_item.cntry_of_orgn%type,
    p_batch_reqmt_type      in order_line_item.batch_requirement_type%type,
    p_sku_sub_code_id       in order_line_item.sku_sub_code_id%type,
    p_sku_sub_code_value    in order_line_item.sku_sub_code_value%type,
    p_scale                 in number,
    p_rnd_threshold         in number,
    p_facility_id           in facility.facility_id%type,
    p_max_log_lvl           in number,
    p_qty_shortage          in out number
)
as
    v_subs_sku_reqd_qty number(13,4) := 0;
    v_qty_found         number(13,4) := 0;
    v_qty_available     number(13,4) := 0;
begin
    for ss_rec in
    (
        select isdc.sub_item_id, isdc.sub_invn_type, isdc.sub_prod_stat, isdc.sub_batch_nbr, 
            isdc.sub_country_of_origin, isdc.sub_invn_attrib_01, isdc.sub_invn_attrib_02,
            isdc.sub_invn_attrib_03, isdc.sub_invn_attrib_04, isdc.sub_invn_attrib_05, 
            isdc.sub_item_qty_ratio
        from item_substitution_dtl_cbo isdc
        join item_substitution_cbo isc on isc.item_sub_hdr_id = isdc.item_sub_hdr_id
            and isc.mark_for_deletion = 0
        where isc.base_item_id = p_item_id and isc.facility_id = p_facility_id
            and isc.status = 10 and isdc.sub_item_qty_ratio >= 1 and isdc.mark_for_deletion = 0
            and (coalesce(isdc.sub_code_id, ' ') = coalesce(p_sku_sub_code_id, ' ')
                or p_sku_sub_code_id = '*' or isdc.sub_code_id = '*')
            and (coalesce(isdc.sub_code_value, ' ')  = coalesce(p_sku_sub_code_value, ' ')
                or p_sku_sub_code_value = '*' or isdc.sub_code_value = '*')
            and (coalesce(isc.reqd_invn_type, ' ') = coalesce(p_invn_type, ' ')
                or p_invn_type = '*' or isc.reqd_invn_type = '*')
            and (coalesce(isc.reqd_prod_stat, ' ') = coalesce(p_prod_stat, ' ')
                or p_prod_stat = '*' or isc.reqd_prod_stat = '*')
            and (coalesce(isc.base_batch_nbr, ' ') = coalesce(p_batch_nbr, ' ')
                or p_batch_nbr = '*' or isc.base_batch_nbr = '*')
            and (coalesce(isc.reqd_country_of_origin, ' ')  = coalesce(p_cntry_of_orgn, ' ') 
                or p_cntry_of_orgn = '*' or isc.reqd_country_of_origin = '*')
            and (coalesce(isc.base_invn_attrib_01, ' ') = coalesce(p_item_attr_1, ' ')
                or p_item_attr_1 = '*' or isc.base_invn_attrib_01 = '*')
            and (coalesce(isc.base_invn_attrib_02, ' ') = coalesce(p_item_attr_2, ' ')
                or p_item_attr_2 = '*' or isc.base_invn_attrib_02 = '*')
            and (coalesce(isc.base_invn_attrib_03, ' ') = coalesce(p_item_attr_3, ' ') 
                or p_item_attr_3 = '*' or isc.base_invn_attrib_03 = '*')
            and (coalesce(isc.base_invn_attrib_04, ' ') = coalesce(p_item_attr_4, ' ') 
                or p_item_attr_4 = '*' or isc.base_invn_attrib_04 = '*')
            and (coalesce(isc.base_invn_attrib_05, ' ') = coalesce(p_item_attr_5, ' ') 
                or p_item_attr_5 = '*' or isc.base_invn_attrib_05 = '*')
        order by isdc.rank
    )
    loop
        v_qty_found := 0;
        v_subs_sku_reqd_qty := ss_rec.sub_item_qty_ratio * p_qty_shortage;
        manh_wave_get_sku_invn_qty(ss_rec.sub_item_id, ss_rec.sub_invn_type,
            ss_rec.sub_prod_stat, ss_rec.sub_batch_nbr, 
            ss_rec.sub_invn_attrib_01, ss_rec.sub_invn_attrib_02, 
            ss_rec.sub_invn_attrib_03, ss_rec.sub_invn_attrib_04, 
            ss_rec.sub_invn_attrib_05, ss_rec.sub_country_of_origin, 
            p_batch_reqmt_type, v_subs_sku_reqd_qty, p_scale, p_rnd_threshold,
            p_max_log_lvl, v_qty_available);

        if (p_sku_sub = '1')
        then
            -- can mix and match skus during substitution
            if (trunc(v_qty_available / ss_rec.sub_item_qty_ratio) > 0)
            then
                if (v_qty_available >= v_subs_sku_reqd_qty)
                then
                    v_qty_found := v_subs_sku_reqd_qty;
                else
                    v_qty_found := ss_rec.sub_item_qty_ratio * trunc(v_qty_available / ss_rec.sub_item_qty_ratio);
                end if;
                
                p_qty_shortage := p_qty_shortage - (v_qty_found / ss_rec.sub_item_qty_ratio);

                -- this is marked as a newly split line item with a new item
                insert into tmp_ord_dtl_sku_invn
                (
                    line_item_id, item_id, invn_type, prod_stat, cntry_of_orgn, batch_nbr, 
                    item_attr_1, item_attr_2, item_attr_3, item_attr_4, item_attr_5, qty_soft_alloc,
                    order_id, rng_shortage, is_flushed, is_split_line, ship_group_id
                )
                values
                (
                    p_line_item_id, ss_rec.sub_item_id, ss_rec.sub_invn_type, ss_rec.sub_prod_stat, 
                    ss_rec.sub_country_of_origin,  ss_rec.sub_batch_nbr, ss_rec.sub_invn_attrib_01, 
                    ss_rec.sub_invn_attrib_02, ss_rec.sub_invn_attrib_03, ss_rec.sub_invn_attrib_04, 
                    ss_rec.sub_invn_attrib_05, v_qty_found, p_order_id, p_qty_shortage, 0, 2,
                    p_ship_group_id
                );
                if (p_max_log_lvl >= 2)
                then
                    wm_cs_log('SA ' || v_qty_found || ' of subs sku '
                        || ss_rec.sub_item_id || ' for line ' || p_line_item_id, 
                        p_sql_log_level => 2);
                end if;

                if (p_qty_shortage = 0)
                then
                    exit;
                end if;
            end if; -- subs sku avail
        elsif (v_qty_available >= v_subs_sku_reqd_qty)
        then
-- todo: additional get sku invn qty optimization possible for sku_sub = '1'
            -- can substitute with only one sku that fully satisfies the remaining shortage
            insert into tmp_ord_dtl_sku_invn
            (
                line_item_id, item_id, invn_type, prod_stat, cntry_of_orgn, batch_nbr, item_attr_1, 
                item_attr_2, item_attr_3, item_attr_4, item_attr_5, qty_soft_alloc, order_id, 
                rng_shortage, is_flushed, is_split_line, ship_group_id
            )
            values
            (
                p_line_item_id, ss_rec.sub_item_id, ss_rec.sub_invn_type, ss_rec.sub_prod_stat, 
                ss_rec.sub_country_of_origin, ss_rec.sub_batch_nbr, ss_rec.sub_invn_attrib_01, 
                ss_rec.sub_invn_attrib_02, ss_rec.sub_invn_attrib_03, ss_rec.sub_invn_attrib_04, 
                ss_rec.sub_invn_attrib_05, v_subs_sku_reqd_qty, p_order_id, 0, 0, 2, p_ship_group_id
            );
            if (p_max_log_lvl >= 2)
            then
                wm_cs_log('SA ' || v_subs_sku_reqd_qty || ' of subs sku '
                    || ss_rec.sub_item_id || ' for line ' || p_line_item_id, 
                    p_sql_log_level => 2);
            end if;

            p_qty_shortage := 0;
            exit;
        end if; -- mixed sku sub
    end loop; -- sku sub
end;
/

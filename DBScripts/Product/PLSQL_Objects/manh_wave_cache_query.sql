create or replace procedure manh_wave_cache_query
(
    p_wave_nbr              in pkt_dtl.wave_nbr%type,
    p_facility_id           in lpn.c_facility_id%type,
    p_invn_need_type        in int_master.invn_need_type%type,
    p_locn_class            in locn_hdr.locn_class%type,
    p_wave_type             in number,
    p_whse                  in whse_master.whse%type,
    p_user_id               in ucl_user.user_name%type,
    p_resultset             out types.cursortype
)
as
    v_wildcard_pz           number(1) := 0;
    v_max_log_lvl           number(1);
    v_code_id               msg_log.ref_code_1%type := 'WMC';
    v_prev_module_name      wm_utils.t_app_context_data;
    v_prev_action_name      wm_utils.t_app_context_data;
    v_parent_bu             company.company_id%type;
    v_parent_chk_int_lock   varchar2(1) := 'N';
    v_tc_company_id         wave_parm.tc_company_id%type := wm_utils.wm_get_user_bu(p_user_id);
begin
    select to_number(coalesce(trim(substr(sc.misc_flags, 8, 1)), '0'))
    into v_max_log_lvl
    from sys_code sc 
    where sc.rec_type = 'S' and sc.code_type = '520' and sc.code_id = v_code_id;

    delete from tmp_log_parm;
    insert into tmp_log_parm
    (
        log_lvl, pgm_id, module, msg_id, ref_code_1, ref_value_1, user_id, whse, cd_master_id
    )
    values
    (
        v_max_log_lvl, 'manh_wave_cache_query', 'WAVE', '1094', v_code_id, p_wave_nbr, 'WAVE_CACHE',
        p_whse, v_tc_company_id
    );
    
    wm_utils.get_context_info(v_prev_module_name, v_prev_action_name);
    wm_utils.set_context_info(p_module_name => 'WAVE', p_action_name => 'WAVE CACHE CREATION',
        p_client_id => p_wave_nbr);
    
    --functional requirements:
    --p_wave type: 1 (regular DO wave), 2 (workorder wave)
    --regular wave, reserve: invn from pkt_detail items and associated lpn
    --regular wave, casepick/active: invn from pkt_detail items and associated pld
    --workorder wave, reserve: invn from work_order_detail items and associated lpn
    --workorder wave, casepick/active: invn from pkt_detail items and associated pld
    --lock code condition applies only for reserve INTs
    --calculate min_qty_available only for reserve
    --if any alloc_invn_prty.pull_zone/pick_detrm_zone has '*', get invn for all zones
    
    if (p_locn_class = 'R')
    then
        -- get parent BU value using the user BU; this is because the WP BU could be null and wont
        -- always be present
        select decode(c.parent_company_id, -1, v_tc_company_id, c.parent_company_id)
        into v_parent_bu
        from company c
        where c.company_id = v_tc_company_id;
        
        -- lock check config at a parent level
        select case when exists
        (
            select 1
            from invn_need_type i
            where i.invn_need_type = p_invn_need_type and i.whse = p_whse
                and i.cd_master_id = v_parent_bu and i.chk_locks_on_allocn = 'Y'
        ) then 'Y' else 'N' end
        into v_parent_chk_int_lock
        from dual;
        
        -- store list of allocatable locks by BU
        insert into tmp_allowed_lock_code
        (
            lock_code, cd_master_id
        )
        select ialc.lock_code, ialc.cd_master_id
        from int_alloc_lock_codes ialc
        where ialc.whse = p_whse and ialc.invn_need_type = p_invn_need_type;
        
        -- remove any child bu config if parent row exists - to avoid a one to many join downstream
        delete from tmp_allowed_lock_code child_bu
        where child_bu.cd_master_id != v_parent_bu
            and exists
            (
                select 1
                from tmp_allowed_lock_code prnt_bu
                where prnt_bu.lock_code = child_bu.lock_code and prnt_bu.cd_master_id = v_parent_bu
            );
    end if;

    if (p_wave_type = 1 and p_locn_class = 'R')
    then
        select case when exists 
        (
            select 1
            from invn_alloc_prty iap
            where iap.invn_need_type = p_invn_need_type and iap.whse = p_whse
                and iap.pull_zone = '*'
        ) then 1 else 0 end
        into v_wildcard_pz
        from dual;

        open p_resultset for
        with t_items as
        (
            select distinct pkt_dtl.item_id
            from pkt_dtl
            where pkt_dtl.wave_nbr = p_wave_nbr
            and (pkt_dtl.wave_stat_code = 20
                or (pkt_dtl.wave_stat_code = 30 and pkt_dtl.cancel_qty > 0))
        ),
        t_zones as
        (
            select distinct iap.pull_zone locn_zone
            from invn_alloc_prty iap
            where iap.invn_need_type = p_invn_need_type and iap.whse = p_whse 
                and iap.pull_zone is not null
        )
        select wi.item_id, locn_hdr.pull_zone, 
            min(wi.on_hand_qty - wi.wm_allocated_qty + wi.to_be_filled_qty) min_qty_available, 
            sum(wi.on_hand_qty - wi.wm_allocated_qty + wi.to_be_filled_qty) total_qty_available,
            min(decode(lpn.tc_parent_lpn_id, null, 0, wi.on_hand_qty - wi.wm_allocated_qty + wi.to_be_filled_qty)) min_plt_qty_available, 
            sum(decode(lpn.tc_parent_lpn_id, null, 0, wi.on_hand_qty - wi.wm_allocated_qty + wi.to_be_filled_qty)) total_plt_qty_available
        from t_items
        join wm_inventory wi on wi.item_id = t_items.item_id and wi.locn_class = 'R' 
            and wi.on_hand_qty - wi.wm_allocated_qty + wi.to_be_filled_qty > 0
        join lpn on lpn.lpn_id = wi.lpn_id and lpn.lpn_facility_status between 30 and 45 
            and lpn.c_facility_id = p_facility_id
        join locn_hdr on locn_hdr.locn_id = lpn.curr_sub_locn_id
            and locn_hdr.whse = p_whse and locn_hdr.pull_zone is not null
        left join invn_need_type i on i.cd_master_id = wi.tc_company_id
            and i.whse = p_whse and i.invn_need_type = p_invn_need_type
        left join lpn_lock ll on ll.lpn_id = lpn.lpn_id
        left join tmp_allowed_lock_code talc on talc.lock_code = ll.inventory_lock_code
            and talc.cd_master_id in (v_parent_bu, wi.tc_company_id)
        where
            (
                ll.inventory_lock_code is null
                or decode(coalesce(i.chk_locks_on_allocn, v_parent_chk_int_lock), 'Y', 1, 0) = 0
                or talc.lock_code is not null
            )
            and
            (
                v_wildcard_pz = 1
                or locn_hdr.pull_zone in (select locn_zone from t_zones)
            )
        group by wi.item_id, locn_hdr.pull_zone;
    elsif (p_wave_type = 1 and p_locn_class in ('A', 'C'))
    then
        open p_resultset for
        with t_items as
        (
            select distinct pkt_dtl.item_id
            from pkt_dtl
            where pkt_dtl.wave_nbr = p_wave_nbr
            and (pkt_dtl.wave_stat_code = 20
                or (pkt_dtl.wave_stat_code = 30 and pkt_dtl.cancel_qty > 0))
        ),
        t_zones as
        (
            select distinct iap.pick_detrm_zone locn_zone
            from invn_alloc_prty iap
            where iap.invn_need_type = p_invn_need_type and iap.whse = p_whse 
                and iap.pick_detrm_zone is not null
        )
        select pick_locn_dtl.item_id, locn_hdr.pick_detrm_zone, 
            sum(wm_inventory.on_hand_qty - wm_inventory.wm_allocated_qty
                  + wm_inventory.to_be_filled_qty) min_qty_available,
            sum(wm_inventory.on_hand_qty - wm_inventory.wm_allocated_qty
                + wm_inventory.to_be_filled_qty) total_qty_available
        from locn_hdr, wm_inventory, pick_locn_dtl, t_items
        where locn_hdr.locn_id = pick_locn_dtl.locn_id
        and wm_inventory.location_dtl_id = pick_locn_dtl.pick_locn_dtl_id
        and pick_locn_dtl.item_id = t_items.item_id
        and pick_locn_dtl.ltst_sku_assign = 'Y'
        and pick_locn_dtl.pikng_lock_code is null
        and locn_hdr.pick_detrm_zone is not null
        and locn_hdr.locn_class = p_locn_class
        and locn_hdr.whse = p_whse
        and ((wm_inventory.on_hand_qty - wm_inventory.wm_allocated_qty 
                + wm_inventory.to_be_filled_qty) > 0)
        and 
        (
            exists (select 1 from t_zones where locn_zone = '*')
            or locn_hdr.pick_detrm_zone in (select locn_zone from t_zones)
        )
        group by pick_locn_dtl.item_id, locn_hdr.pick_detrm_zone, 0;
    elsif (p_wave_type = 2 and p_locn_class = 'R')
    then
        open p_resultset for
        with t_items as
        (
            select distinct work_ord_dtl.item_id
            from work_ord_hdr, work_ord_dtl
            where work_ord_hdr.work_ord_nbr = work_ord_dtl.work_ord_nbr
            and work_ord_hdr.wave_nbr = p_wave_nbr
            and work_ord_hdr.wave_stat_code = 10
            and work_ord_hdr.has_import_error = 0
            and work_ord_hdr.has_soft_check_error = 0
            and work_ord_dtl.has_import_error = 0
            and work_ord_dtl.has_soft_check_error = 0
        ),
        t_zones as
        (
            select distinct iap.pull_zone locn_zone
            from invn_alloc_prty iap
            where invn_need_type = p_invn_need_type and iap.whse = p_whse 
                and iap.pull_zone is not null
        )
        select lpn_detail.item_id, locn_hdr.pull_zone, 
            min(wi.on_hand_qty - wi.wm_allocated_qty + wi.to_be_filled_qty) min_qty_available, 
            sum(wi.on_hand_qty - wi.wm_allocated_qty + wi.to_be_filled_qty) total_qty_available,
            min(decode(lpn.tc_parent_lpn_id, null, 0, wi.on_hand_qty - wi.wm_allocated_qty + wi.to_be_filled_qty)) min_plt_qty_available, 
            sum(decode(lpn.tc_parent_lpn_id, null, 0, wi.on_hand_qty - wi.wm_allocated_qty + wi.to_be_filled_qty)) total_plt_qty_available
        from t_items
        join lpn_detail on lpn_detail.item_id = t_items.item_id
        join lpn on lpn.lpn_id = lpn_detail.lpn_id
            and lpn.lpn_facility_status between 30 and 45
            and lpn.c_facility_id = p_facility_id
        join wm_inventory wi on wi.lpn_id = lpn.lpn_id
            and wi.on_hand_qty - wi.wm_allocated_qty + wi.to_be_filled_qty > 0
        join locn_hdr on locn_hdr.locn_id = lpn.curr_sub_locn_id
            and locn_hdr.pull_zone is not null
        left join invn_need_type i on i.cd_master_id = wi.tc_company_id
            and i.whse = p_whse and i.invn_need_type = 2
        left join lpn_lock ll on ll.lpn_id = lpn.lpn_id
        left join tmp_allowed_lock_code talc on talc.lock_code = ll.inventory_lock_code
            and talc.cd_master_id in (v_parent_bu, wi.tc_company_id)
        where
            (
                ll.inventory_lock_code is null
                or decode(coalesce(i.chk_locks_on_allocn, v_parent_chk_int_lock), 'Y', 1, 0) = 0
                or talc.lock_code is not null
            )
            and
            (
                exists (select 1 from t_zones where locn_zone = '*')
                or locn_hdr.pull_zone in (select locn_zone from t_zones)
            )
        group by lpn_detail.item_id, locn_hdr.pull_zone;
    elsif (p_wave_type = 2 and p_locn_class in ('A', 'C'))
    then
        open p_resultset for
        with t_items as
        (
            select distinct work_ord_dtl.item_id
            from work_ord_hdr, work_ord_dtl
            where work_ord_hdr.work_ord_nbr = work_ord_dtl.work_ord_nbr
            and work_ord_hdr.wave_nbr = p_wave_nbr
            and work_ord_hdr.wave_stat_code = 10
            and work_ord_hdr.has_import_error = 0
            and work_ord_hdr.has_soft_check_error = 0
            and work_ord_dtl.has_import_error = 0
            and work_ord_dtl.has_soft_check_error = 0
        ),
        t_zones as
        (
            select distinct iap.pick_detrm_zone locn_zone
            from invn_alloc_prty iap
            where iap.invn_need_type = p_invn_need_type and iap.whse = p_whse 
                and iap.pick_detrm_zone is not null
        )
        select pick_locn_dtl.item_id, locn_hdr.pick_detrm_zone, 
            sum(wm_inventory.on_hand_qty - wm_inventory.wm_allocated_qty
                  + wm_inventory.to_be_filled_qty) min_qty_available,
            sum(wm_inventory.on_hand_qty - wm_inventory.wm_allocated_qty
                + wm_inventory.to_be_filled_qty) total_qty_available
        from locn_hdr, wm_inventory, pick_locn_dtl, t_items
        where locn_hdr.locn_id = pick_locn_dtl.locn_id
        and wm_inventory.location_dtl_id = pick_locn_dtl.pick_locn_dtl_id
        and pick_locn_dtl.item_id = t_items.item_id
        and pick_locn_dtl.ltst_sku_assign = 'Y'
        and pick_locn_dtl.pikng_lock_code is null
        and locn_hdr.pick_detrm_zone is not null
        and locn_hdr.locn_class = p_locn_class
        and locn_hdr.whse = p_whse
        and ((wm_inventory.on_hand_qty - wm_inventory.wm_allocated_qty 
                + wm_inventory.to_be_filled_qty) > 0)
        and 
        (
            exists (select 1 from t_zones where locn_zone = '*')
            or locn_hdr.pick_detrm_zone in (select locn_zone from t_zones)
        )
        group by pick_locn_dtl.item_id, locn_hdr.pick_detrm_zone;        
    end if;

    wm_cs_log('Wave cache built for wave_type ' || p_wave_type || ', locn class '
        || p_locn_class || ', rows: ' || sql%rowcount);
        
    wm_utils.set_context_info(p_module_name => v_prev_module_name,
        p_action_name => v_prev_action_name);    
end;
/
show errors;

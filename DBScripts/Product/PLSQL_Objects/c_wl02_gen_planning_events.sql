create or replace
procedure c_wl02_gen_planning_events
(
p_tc_shpmnt_id    IN c_load_diagram_summary.tc_shipment_id%type,
p_rc 			  OUT NUMBER
) AS
-- **
-- UNCOMMENT FOR TESTING
--DECLARE
--p_tc_shpmnt_id    c_load_diagram_summary.tc_shipment_id%type := '4000130814' ;
--p_rc 			  NUMBER;
--***
---------------------------------------------------------------------------
-- ****** PROD TYPE CURSOR
-- Configure system code L2C with all product types
-- Check the flag for product types that need to break tasks by stop
------------------------------------------------------------------------------
   CURSOR c_prod_type_cur IS
		select DISTINCT code_id as product_type, TRIM(SUBSTR(MISC_FLAGS,1,50)) as brk_by_stop
    from whse_sys_code where code_type = 'L2C' and rec_type = 'C'; 
	
--------------------------------------------------------------------------------
-- ** Define Product type list 
-- NOTES: The idea is to loop through the prod_type list 
--        and based on if the prod type has break by stop or not
--        fire queries to fetch labor_msg/dtl data sorted by stop or not
--------------------------------------------------------------------------------
   TYPE prod_list IS TABLE OF c_prod_type_cur%ROWTYPE;
     v_prod_list prod_list;

   v_prod_type_loop char(1) := '';

-- *****************************************************************************
-- CURSOR c_plnevnt_cur_Picking
-- NOTES: This Cursor returns data for a product type
--        sorted by the stop number. So use this cursor for fetching
--        picking event data for product types that need BREAK BY STOP
-- *****************************************************************************
   CURSOR c_plnevnt_cur_Picking IS
	 select DISTINCT
    -- Begin of labor_msg columns
    	(SELECT TRIM(SUBSTR(MISC_FLAGS,1,50)) FROM WHSE_SYS_CODE SC
			 WHERE SC.REC_TYPE = 'C' AND SC.CODE_TYPE = 'L2A'
			 AND TRIM(SUBSTR(MISC_FLAGS,66,1)) = 'P'
			 )		  										  AS LM_PICKING_ACT_NAME,
			CLM.USER_ID                    	  				  AS LM_PICKING_LOGIN_USER_ID,
			SH.PICKUP_START_DTTM           	  				  AS LM_PICKING_SCHED_START_TIME,
			SH.PICKUP_START_DTTM           	  				  AS LM_PICKING_SCHED_START_DATE,
			SH.O_FACILITY_ID						  	  				  AS LM_PICKING_WHSE,
			'PJ3'								  				  AS LM_PICKING_VHCL_TYPE,
			(SELECT TRIM(SUBSTR(MISC_FLAGS,51,15)) FROM WHSE_SYS_CODE SC
			 WHERE SC.REC_TYPE = 'C' AND
			 SC.CODE_TYPE = 'L2A'
			 AND TRIM(SUBSTR(MISC_FLAGS,66,1)) = 'P'
			 )							  					  AS LM_PICKING_MOD_USER_ID,
			'  '								  			  AS LM_PICKING_REF_CODE,
			CLM.TC_SHIPMENT_ID                				  AS LM_PICKING_REF_NBR,
			''							  				  	  AS LM_PICKING_DEPT,
			''							  				      AS LM_PICKING_DIV,
			' '								  				  AS LM_PICKING_SHIFT,
			CLM.TRLR_TYPE                      				  AS LM_PICKING_USER_DEF_VAL_1,
			LH.LOCN_PICK_SEQ                 				  AS LM_PICKING_MISC_TXT_2,
			CLM.ROUTE_ID                       				  AS LM_PICKING_MISC_NUM_1,
			cld.line_item_id								  				  AS LM_PICKING_MISC_NUM_2,
			'N'                                				  AS LM_PICKING_RESEND_TRAN,
			'N'                                				  AS LM_PICKING_REQ_SAM_REPLY,
			''								  				  AS LM_PICKING_TEAM_STD_GRP,
			(SELECT WHSE_LABOR_PARAMETERS.DAY_BEGIN_TIME
			 FROM whse_labor_parameters WHERE
				DAY_BEGIN_TIME IN
				(select DAY_BEGIN_TIME from
       			 whse_labor_parameters where facility_id in
					(select facility_id from facility
					 where whse = SH.O_FACILITY_ID)
					)
			)       			                             AS LM_PICKING_WHSE_DATE,   -- Added Fix
			SH.PICKUP_START_DTTM							 AS LM_PICKING_ACTUAL_END_DATE ,
			SH.PICKUP_START_DTTM							 AS LM_PICKING_ACTUAL_END_TIME ,

-- End of labor_msg	columns
-- Generic columns used inside SP
			cld.trailer_position							  AS PICKING_trailer_posn,
			cld.stop_number									  AS PICKING_stop_number,
      CLD.STORAGE_TYPE                  AS PICKING_storage_type, 
			lpad (dense_rank() over
			(order by cld.stop_number,cld.trailer_position),5,'0')
															  AS PICKING_contnr_number,
			cld.product_type								  AS PICKING_product_type,
			cst.cntr_type  AS PICKING_contnr_type,
			( select to_number(trim(substr(misc_flags,31,1)))
			  from WHSE_sys_code sc
			  where sc.rec_type = 'C' and
			  sc.code_type = 'L2B' and
			  trim(substr(misc_flags,1,4)) = cst.cntr_type and trim(substr(misc_flags,5,25)) = substr(cld.product_type, 1, 1))   AS PICKING_nbr_of_contnrs_syscode,
-- Generic columns ends
-- Start labor_msg_dtl columns
			IM.SKU_BRCD                        				  AS LMD_PICK_ITEM_BAR_CODE,
			IM.SKU_ID                          				  AS LMD_PICK_SEQ_NBR,
			IM.HNDL_ATTR_ACTIVE                				  AS LMD_PICK_HNDL_ATTR,
			0								  				  AS LMD_PICK_QTY_PER_GRAB,
			LH.DSP_LOCN							  				  AS LMD_PICK_DSP_LOCN,
			'A'								  				  AS LMD_PICK_LOCN_CLASS,
			 CLD.NUM_CASES								  				  AS LMD_PICK_QTY,
			 CLD.TC_ORDER_ID                   		   	  AS LMD_PICK_TC_ILPN_ID,
			 CLD.STORAGE_TYPE                   			  AS LMD_PICK_PALLET_ID,
			 LH.SLOT_TYPE                       			  AS LMD_PICK_LOCN_SLOT_TYPE,
			 LH.X_COORD                         			  AS LMD_PICK_LOCN_X_COORD,
			 LH.Y_COORD                         			  AS LMD_PICK_LOCN_Y_COORD,
			 LH.Z_COORD                         			  AS LMD_PICK_LOCN_Z_COORD,
			 LH.TRAVEL_AISLE                    			  AS LMD_PICK_LOCN_TRAV_AISLE,
			 LH.TRAVEL_ZONE                     			  AS LMD_PICK_LOCN_TRAV_ZONE,
			 IM.STD_BUNDL_QTY                   			  AS LMD_PICK_INNERPACK_QTY,
			 IM.STD_SUB_PACK_QTY                			  AS LMD_PICK_SKU_SUB_PACK_QTY,  -- need to confirm
			 IM.STD_PACK_QTY                    			  AS LMD_PICK_PACK_QTY,
			 IM.STD_CASE_QTY                    			  AS LMD_PICK_CASE_QTY,
			 IM.STD_TIER_QTY                    			  AS LMD_PICK_TIER_QTY,
			 IM.STD_PLT_QTY                     			  AS LMD_PICK_PALLET_QTY,
			 ''								  			  	  AS LMD_PICK_CO,
			 ' '							  				  AS LMD_PICK_DIV,
			 CLD.STOP_NUMBER ||''                   			  AS LMD_PICK_MISC,
			 '0'								  			  AS LMD_PICK_MISC_2,
			 CLD.WEIGHT                         			  AS LMD_PICK_WEIGHT,
			 CLD.VOLUME                        			  AS LMD_PICK_VOLUME,
			 IM.CRITCL_DIM_1                    			  AS LMD_PICK_CRIT_DIM_1,
			 IM.CRITCL_DIM_2                    			  AS LMD_PICK_CRIT_DIM_2,
			 IM.CRITCL_DIM_3                    			  AS LMD_PICK_CRIT_DIM_3,
			 SYSDATE								  		  AS LMD_PICK_START_DATE_TIME,
			 ' '								  			  AS LMD_PICK_HANDLING_UOM,
			 IM.SEASON                          			  AS LMD_PICK_SEASON,
			 IM.SEASON_YR                       			  AS LMD_PICK_SEASON_YR,
			 IM.STYLE                           			  AS LMD_PICK_STYLE,
			 IM.STYLE_SFX                       			  AS LMD_PICK_STYLE_SFX,
			 IM.COLOR                           			  AS LMD_PICK_COLOR,
			 IM.COLOR_SFX                       			  AS LMD_PICK_COLOR_SFX,
			 IM.SEC_DIM                         			  AS LMD_PICK_SEC_DIM,
			 IM.QUAL                            			  AS LMD_PICK_QUAL,
			 LH.LOCN_PICK_SEQ                    			  AS LMD_PICK_SIZE_RNGE_CODE,
			 IM.SIZE_REL_POSN_IN_TABLE          			  AS LMD_PICK_SIZERELPOSNINTAB,
			 CLD.TC_ORDER_ID         						  AS LMD_PICK_TC_OLPN_ID,
			 CLD.LINE_ITEM_ID								  				  AS LMD_PICK_MISC_NUM_2,
			 'N'                                			  AS LMD_PICK_LOADED,
			 LH.PUTWY_ZONE                      			  AS LMD_PICK_PUTAWAY_ZONE,
			 LH.PICK_DETRM_ZONE                 			  AS LMD_PICK_DETZONE,
			 LH.WORK_GRP                        			  AS LMD_PICK_WORK_GROUP,
			 LH.WORK_AREA                       			  AS LMD_PICK_WORK_AREA,
			 LH.PULL_ZONE                       			  AS LMD_PICK_PULL_ZONE,
			 LH.ZONE							  				  AS LMD_PICK_ASSIGNMENT_ZONE

-- End labor_msg_dtl columns
		FROM
			c_load_diagram_summary clm ,
			orders o,c_load_diagram_item_detail cld ,
			c_storage_type cst,
			item_master im,
			order_line_item oli,
			pick_locn_dtl pld,
			locn_hdr lh,
			shipment sh
		WHERE clm.tc_shipment_id = p_tc_shpmnt_id
        AND clm.load_diagram_id = cld.load_diagram_id
        AND cld.TC_ORDER_ID = O.TC_ORDER_ID
        AND ( (o.tc_shipment_id = clm.tc_shipment_id)
          OR (exists (select 1 from lpn l where l.tc_shipment_id = clm.tc_shipment_id 
                and l.order_id = o.order_id and l.inbound_outbound_indicator='O' and l.lpn_facility_status >=20) ) )
		AND o.order_id = oli.order_id
        AND cld.line_item_id = oli.line_item_id
 		AND oli.item_id = im.item_id
		AND oli.item_id = pld.item_id
 		AND pld.locn_id = lh.locn_id
    AND lh.locn_class = 'A'
		AND cld.storage_type = cst.storage_type_id
		AND clm.tc_shipment_id = sh.tc_shipment_id
    AND im.prod_type = v_prod_type_loop
		--AND exists (select 1 from whse_sys_code where rec_type = 'C' and code_type = 'L2B' and trim(substr(misc_flags,1,4)) = cst.cntr_type  and trim(substr(misc_flags,5,25)) = cld.product_type)
		and exists (select 1 from whse_sys_code where rec_type = 'C' and code_type = 'L2D' and trim(substr(misc_flags,1,4)) = cst.cntr_type)
		ORDER BY cld.stop_number,cld.storage_type,cld.trailer_position; -- Change this sequence ?????
    
-- *****************************************************************************
-- END : CURSOR c_plnevnt_cur_Picking
-- *****************************************************************************
-- *****************************************************************************
-- CURSOR c_NBrk_cur_Picking
-- NOTES: This Cursor returns data for a product type
--        sorted by the stop number. So use this cursor for fetching
--        picking event data for product types that need BREAK BY STOP
-- *****************************************************************************
   CURSOR c_NBrk_cur_Picking IS
	 select DISTINCT
    -- Begin of labor_msg columns
    	(SELECT TRIM(SUBSTR(MISC_FLAGS,1,50)) FROM WHSE_SYS_CODE SC
			 WHERE SC.REC_TYPE = 'C' AND SC.CODE_TYPE = 'L2A'
			 AND TRIM(SUBSTR(MISC_FLAGS,66,1)) = 'P'
			 )		  										  AS LM_PICKING_ACT_NAME,
			CLM.USER_ID                    	  				  AS LM_PICKING_LOGIN_USER_ID,
			SH.PICKUP_START_DTTM           	  				  AS LM_PICKING_SCHED_START_TIME,
			SH.PICKUP_START_DTTM           	  				  AS LM_PICKING_SCHED_START_DATE,
			SH.O_FACILITY_ID						  	  				  AS LM_PICKING_WHSE,
			'PJ3'								  				  AS LM_PICKING_VHCL_TYPE,
			(SELECT TRIM(SUBSTR(MISC_FLAGS,51,15)) FROM WHSE_SYS_CODE SC
			 WHERE SC.REC_TYPE = 'C' AND
			 SC.CODE_TYPE = 'L2A'
			 AND TRIM(SUBSTR(MISC_FLAGS,66,1)) = 'P'
			 )							  					  AS LM_PICKING_MOD_USER_ID,
			'  '								  			  AS LM_PICKING_REF_CODE,
			CLM.TC_SHIPMENT_ID                				  AS LM_PICKING_REF_NBR,
			''							  				  	  AS LM_PICKING_DEPT,
			''							  				      AS LM_PICKING_DIV,
			' '								  				  AS LM_PICKING_SHIFT,
			CLM.TRLR_TYPE                      				  AS LM_PICKING_USER_DEF_VAL_1,
			LH.LOCN_PICK_SEQ                 				  AS LM_PICKING_MISC_TXT_2,
			CLM.ROUTE_ID                       				  AS LM_PICKING_MISC_NUM_1,
			cld.line_item_id								  				  AS LM_PICKING_MISC_NUM_2,
			'N'                                				  AS LM_PICKING_RESEND_TRAN,
			'N'                                				  AS LM_PICKING_REQ_SAM_REPLY,
			''								  				  AS LM_PICKING_TEAM_STD_GRP,
			(SELECT WHSE_LABOR_PARAMETERS.DAY_BEGIN_TIME
			 FROM whse_labor_parameters WHERE
				DAY_BEGIN_TIME IN
				(select DAY_BEGIN_TIME from
       			 whse_labor_parameters where facility_id in
					(select facility_id from facility
					 where whse = SH.O_FACILITY_ID)
					)
			)       			                             AS LM_PICKING_WHSE_DATE,   -- Added Fix
			SH.PICKUP_START_DTTM							 AS LM_PICKING_ACTUAL_END_DATE ,
			SH.PICKUP_START_DTTM							 AS LM_PICKING_ACTUAL_END_TIME ,

-- End of labor_msg	columns
-- Generic columns used inside SP
			cld.trailer_position							  AS PICKING_trailer_posn,
			cld.stop_number									  AS PICKING_stop_number,
      CLD.STORAGE_TYPE                  AS PICKING_storage_type, 
			lpad (dense_rank() over
			(order by cld.stop_number,cld.trailer_position),5,'0')
															  AS PICKING_contnr_number,
			cld.product_type								  AS PICKING_product_type,
			cst.cntr_type  AS PICKING_contnr_type,
			( select to_number(trim(substr(misc_flags,31,1)))
			  from WHSE_sys_code sc
			  where sc.rec_type = 'C' and
			  sc.code_type = 'L2B' and
			  trim(substr(misc_flags,1,4)) = cst.cntr_type and trim(substr(misc_flags,5,25)) = substr(cld.product_type, 1, 1))   AS PICKING_nbr_of_contnrs_syscode,
-- Generic columns ends
-- Start labor_msg_dtl columns
			IM.SKU_BRCD                        				  AS LMD_PICK_ITEM_BAR_CODE,
			IM.SKU_ID                          				  AS LMD_PICK_SEQ_NBR,
			IM.HNDL_ATTR_ACTIVE                				  AS LMD_PICK_HNDL_ATTR,
			0								  				  AS LMD_PICK_QTY_PER_GRAB,
			LH.DSP_LOCN							  				  AS LMD_PICK_DSP_LOCN,
			'A'								  				  AS LMD_PICK_LOCN_CLASS,
			 CLD.NUM_CASES								  				  AS LMD_PICK_QTY,
			 CLD.TC_ORDER_ID                   		   	  AS LMD_PICK_TC_ILPN_ID,
			 CLD.STORAGE_TYPE                   			  AS LMD_PICK_PALLET_ID,
			 LH.SLOT_TYPE                       			  AS LMD_PICK_LOCN_SLOT_TYPE,
			 LH.X_COORD                         			  AS LMD_PICK_LOCN_X_COORD,
			 LH.Y_COORD                         			  AS LMD_PICK_LOCN_Y_COORD,
			 LH.Z_COORD                         			  AS LMD_PICK_LOCN_Z_COORD,
			 LH.TRAVEL_AISLE                    			  AS LMD_PICK_LOCN_TRAV_AISLE,
			 LH.TRAVEL_ZONE                     			  AS LMD_PICK_LOCN_TRAV_ZONE,
			 IM.STD_BUNDL_QTY                   			  AS LMD_PICK_INNERPACK_QTY,
			 IM.STD_SUB_PACK_QTY                			  AS LMD_PICK_SKU_SUB_PACK_QTY,  -- need to confirm
			 IM.STD_PACK_QTY                    			  AS LMD_PICK_PACK_QTY,
			 IM.STD_CASE_QTY                    			  AS LMD_PICK_CASE_QTY,
			 IM.STD_TIER_QTY                    			  AS LMD_PICK_TIER_QTY,
			 IM.STD_PLT_QTY                     			  AS LMD_PICK_PALLET_QTY,
			 ''								  			  	  AS LMD_PICK_CO,
			 ' '							  				  AS LMD_PICK_DIV,
			 CLD.STOP_NUMBER ||''                   			  AS LMD_PICK_MISC,
			 '0'								  			  AS LMD_PICK_MISC_2,
			 CLD.WEIGHT                         			  AS LMD_PICK_WEIGHT,
			 CLD.VOLUME                        			  AS LMD_PICK_VOLUME,
			 IM.CRITCL_DIM_1                    			  AS LMD_PICK_CRIT_DIM_1,
			 IM.CRITCL_DIM_2                    			  AS LMD_PICK_CRIT_DIM_2,
			 IM.CRITCL_DIM_3                    			  AS LMD_PICK_CRIT_DIM_3,
			 SYSDATE								  		  AS LMD_PICK_START_DATE_TIME,
			 ' '								  			  AS LMD_PICK_HANDLING_UOM,
			 IM.SEASON                          			  AS LMD_PICK_SEASON,
			 IM.SEASON_YR                       			  AS LMD_PICK_SEASON_YR,
			 IM.STYLE                           			  AS LMD_PICK_STYLE,
			 IM.STYLE_SFX                       			  AS LMD_PICK_STYLE_SFX,
			 IM.COLOR                           			  AS LMD_PICK_COLOR,
			 IM.COLOR_SFX                       			  AS LMD_PICK_COLOR_SFX,
			 IM.SEC_DIM                         			  AS LMD_PICK_SEC_DIM,
			 IM.QUAL                            			  AS LMD_PICK_QUAL,
			 LH.LOCN_PICK_SEQ                    			  AS LMD_PICK_SIZE_RNGE_CODE,
			 IM.SIZE_REL_POSN_IN_TABLE          			  AS LMD_PICK_SIZERELPOSNINTAB,
			 CLD.TC_ORDER_ID         						  AS LMD_PICK_TC_OLPN_ID,
			 CLD.LINE_ITEM_ID								  				  AS LMD_PICK_MISC_NUM_2,
			 'N'                                			  AS LMD_PICK_LOADED,
			 LH.PUTWY_ZONE                      			  AS LMD_PICK_PUTAWAY_ZONE,
			 LH.PICK_DETRM_ZONE                 			  AS LMD_PICK_DETZONE,
			 LH.WORK_GRP                        			  AS LMD_PICK_WORK_GROUP,
			 LH.WORK_AREA                       			  AS LMD_PICK_WORK_AREA,
			 LH.PULL_ZONE                       			  AS LMD_PICK_PULL_ZONE,
			 LH.ZONE							  				  AS LMD_PICK_ASSIGNMENT_ZONE

-- End labor_msg_dtl columns
		FROM
			c_load_diagram_summary clm ,
			orders o,c_load_diagram_item_detail cld ,
			c_storage_type cst,
			item_master im,
			order_line_item oli,
			pick_locn_dtl pld,
			locn_hdr lh,
			shipment sh
		WHERE clm.tc_shipment_id = p_tc_shpmnt_id
        AND clm.load_diagram_id = cld.load_diagram_id
        AND cld.TC_ORDER_ID = O.TC_ORDER_ID
        AND ( (o.tc_shipment_id = clm.tc_shipment_id)
          OR (exists (select 1 from lpn l where l.tc_shipment_id = clm.tc_shipment_id 
                and l.order_id = o.order_id and l.inbound_outbound_indicator='O' and l.lpn_facility_status >=20) ) )
		AND o.order_id = oli.order_id
        AND cld.line_item_id = oli.line_item_id
 		AND oli.item_id = im.item_id
		AND oli.item_id = pld.item_id
 		AND pld.locn_id = lh.locn_id
    AND lh.locn_class = 'A'
		AND cld.storage_type = cst.storage_type_id
		AND clm.tc_shipment_id = sh.tc_shipment_id
		--AND exists (select 1 from whse_sys_code where rec_type = 'C' and code_type = 'L2B' and trim(substr(misc_flags,1,4)) = cst.cntr_type  and trim(substr(misc_flags,5,25)) = cld.product_type)
		and exists (select 1 from whse_sys_code where rec_type = 'C' and code_type = 'L2D' and trim(substr(misc_flags,1,4)) = cst.cntr_type)
    and im.prod_type = v_prod_type_loop
		ORDER BY cld.storage_type,cld.trailer_position;
    
-- *****************************************************************************
-- END : CURSOR c_NBrk_cur_Picking
-- *****************************************************************************

-- Define the cusor c_plnevnt_cur_Loading to select all columns for labor_msg and labor_msg_dtl
   CURSOR c_plnevnt_cur_Loading IS
		select DISTINCT
		-- Begin of labor_msg columns

			(SELECT TRIM(SUBSTR(MISC_FLAGS,1,50)) FROM WHSE_SYS_CODE SC
			 WHERE SC.REC_TYPE = 'C' AND SC.CODE_TYPE = 'L2A'
			 AND TRIM(SUBSTR(MISC_FLAGS,66,1)) = 'L'
			 )		  										  AS LM_LOAD_ACT_NAME,
			CLM.USER_ID                    	  				  AS LM_LOAD_LOGIN_USER_ID,
			SH.PICKUP_START_DTTM           	  				  AS LM_LOAD_SCHED_START_TIME,
			SH.PICKUP_START_DTTM           	  				  AS LM_LOAD_SCHED_START_DATE,
			SH.O_FACILITY_ID						  	  				  AS LM_LOAD_WHSE,
			' '								  				  AS LM_LOAD_VHCL_TYPE,
			(SELECT TRIM(SUBSTR(MISC_FLAGS,51,15)) FROM WHSE_SYS_CODE SC
			 WHERE SC.REC_TYPE = 'C' AND
			 SC.CODE_TYPE = 'L2A'
			 AND TRIM(SUBSTR(MISC_FLAGS,66,1)) = 'L'
			 )							  					  AS LM_LOAD_MOD_USER_ID,
			'  '								  			  AS LM_LOAD_REF_CODE,
			CLM.TC_SHIPMENT_ID                				  AS LM_LOAD_REF_NBR,
			''							  				  	  AS LM_LOAD_DEPT,
			''							  				      AS LM_LOAD_DIV,
			' '								  				  AS LM_LOAD_SHIFT,
			CLM.TRLR_TYPE                      				  AS LM_LOAD_USER_DEF_VAL_1,
			CLM.TC_SHIPMENT_ID                 				  AS LM_LOAD_MISC_TXT_2,
			CLM.ROUTE_ID                       				  AS LM_LOAD_MISC_NUM_1,
			0								  				  AS LM_LOAD_MISC_NUM_2,
			'N'                                				  AS LM_LOAD_RESEND_TRAN,
			'N'                                				  AS LM_LOAD_REQ_SAM_REPLY,
			''								  				  AS LM_LOAD_TEAM_STD_GRP,
			(SELECT WHSE_LABOR_PARAMETERS.DAY_BEGIN_TIME
			 FROM whse_labor_parameters WHERE
				DAY_BEGIN_TIME IN
				(select DAY_BEGIN_TIME from
       			 whse_labor_parameters where facility_id in
					(select facility_id from facility
					 where whse = SH.O_FACILITY_ID)
					)
			)       			                             AS LM_LOAD_WHSE_DATE,   -- Added Fix
			SH.PICKUP_START_DTTM							 AS LM_LOAD_ACTUAL_END_DATE ,
			SH.PICKUP_START_DTTM							 AS LM_LOAD_ACTUAL_END_TIME ,
-- End of labor_msg	columns
-- Generic columns used inside SP
			cld.trailer_position							  AS LOAD_trailer_posn,
			cld.stop_number									  AS LOAD_stop_number,
      CLD.STORAGE_TYPE                  AS LOAD_storage_type, 
			lpad (dense_rank() over
			(order by cld.stop_number,cld.trailer_position),5,'0')    	  AS LOAD_contnr_number,
			cld.product_type								  AS LOAD_product_type,
			cst.cntr_type                             		  AS LOAD_contnr_type,
			--( select to_number(trim(substr(misc_flags,31,1)))
			--  from WHSE_sys_code sc
			--  where sc.rec_type = 'C' and
			--  sc.code_type = 'L2B' and
			--  trim(substr(misc_flags,1,4)) = cst.cntr_type and trim(substr(misc_flags,5,25)) = substr(cld.product_type, 1, 1))   
        1 AS LOAD_nbr_of_contnrs_syscode,
-- Generic columns ends
-- Start labor_msg_dtl columns
			IM.SKU_BRCD                        				  AS LMD_LOAD_ITEM_BAR_CODE,
			IM.SKU_ID                          				  AS LMD_LOAD_SEQ_NBR,
			IM.HNDL_ATTR_ACTIVE                				  AS LMD_LOAD_HNDL_ATTR,
			0								  				  AS LMD_LOAD_QTY_PER_GRAB,
			LH.DSP_LOCN							  				  AS LMD_LOAD_DSP_LOCN,
			'A'								  				  AS LMD_LOAD_LOCN_CLASS,
			 CLD.NUM_CASES								  				  AS LMD_LOAD_QTY,
			 CLD.PRODUCT_TYPE                   		   	  AS LMD_LOAD_TC_ILPN_ID,
			 CLD.STORAGE_TYPE                   			  AS LMD_LOAD_PALLET_ID,
			 LH.SLOT_TYPE                       			  AS LMD_LOAD_LOCN_SLOT_TYPE,
			 LH.X_COORD                         			  AS LMD_LOAD_LOCN_X_COORD,
			 LH.Y_COORD                         			  AS LMD_LOAD_LOCN_Y_COORD,
			 LH.Z_COORD                         			  AS LMD_LOAD_LOCN_Z_COORD,
			 LH.TRAVEL_AISLE                    			  AS LMD_LOAD_LOCN_TRAV_AISLE,
			 LH.TRAVEL_ZONE                     			  AS LMD_LOAD_LOCN_TRAV_ZONE,
			 IM.STD_BUNDL_QTY                   			  AS LMD_LOAD_INNERPACK_QTY,
			 IM.STD_SUB_PACK_QTY                			  AS LMD_LOAD_SKU_SUB_PACK_QTY,  -- need to confirm
			 IM.STD_PACK_QTY                    			  AS LMD_LOAD_PACK_QTY,
			 IM.STD_CASE_QTY                    			  AS LMD_LOAD_CASE_QTY,
			 IM.STD_TIER_QTY                    			  AS LMD_LOAD_TIER_QTY,
			 IM.STD_PLT_QTY                     			  AS LMD_LOAD_PALLET_QTY,
			 ''								  			  	  AS LMD_LOAD_CO,
			 ' '							  				  AS LMD_LOAD_DIV,
			 CLD.STOP_NUMBER                    			  AS LMD_LOAD_MISC,
			 '0'								  			  AS LMD_LOAD_MISC_2,
			 CLD.WEIGHT                         			  AS LMD_LOAD_WEIGHT,
			 CLD.VOLUME                        			  AS LMD_LOAD_VOLUME,
			 IM.CRITCL_DIM_1                    			  AS LMD_LOAD_CRIT_DIM_1,
			 IM.CRITCL_DIM_2                    			  AS LMD_LOAD_CRIT_DIM_2,
			 IM.CRITCL_DIM_3                    			  AS LMD_LOAD_CRIT_DIM_3,
			 SYSDATE								  		  AS LMD_LOAD_START_DATE_TIME,
			 ' '								  			  AS LMD_LOAD_HANDLING_UOM,
			 IM.SEASON                          			  AS LMD_LOAD_SEASON,
			 IM.SEASON_YR                       			  AS LMD_LOAD_SEASON_YR,
			 IM.STYLE                           			  AS LMD_LOAD_STYLE,
			 IM.STYLE_SFX                       			  AS LMD_LOAD_STYLE_SFX,
			 IM.COLOR                           			  AS LMD_LOAD_COLOR,
			 IM.COLOR_SFX                       			  AS LMD_LOAD_COLOR_SFX,
			 IM.SEC_DIM                         			  AS LMD_LOAD_SEC_DIM,
			 IM.QUAL                            			  AS LMD_LOAD_QUAL,
			 IM.SIZE_RANGE_CODE                 			  AS LMD_LOAD_SIZE_RNGE_CODE,
			 IM.SIZE_REL_POSN_IN_TABLE          			  AS LMD_LOAD_SIZERELPOSNINTAB,
			 CLD.TC_ORDER_ID         						  AS LMD_LOAD_TC_OLPN_ID,
			 0								  				  AS LMD_LOAD_MISC_NUM_2,
			 'N'                                			  AS LMD_LOAD_LOADED,
			 LH.PUTWY_ZONE                      			  AS LMD_LOAD_PUTAWAY_ZONE,
			 LH.PICK_DETRM_ZONE                 			  AS LMD_LOAD_PICK_DETZONE,
			 LH.WORK_GRP                        			  AS LMD_LOAD_WORK_GROUP,
			 LH.WORK_AREA                       			  AS LMD_LOAD_WORK_AREA,
			 LH.PULL_ZONE                       			  AS LMD_LOAD_PULL_ZONE,
			 LH.ZONE							  				  AS LMD_LOAD_ASSIGNMENT_ZONE

-- End labor_msg_dtl columns
		FROM
			c_load_diagram_summary clm ,
			orders o,c_load_diagram_item_detail cld ,
			c_storage_type cst,
			item_master im,
			order_line_item oli,
			pick_locn_dtl pld,
			locn_hdr lh,
			shipment sh
		WHERE clm.tc_shipment_id = p_tc_shpmnt_id
        AND clm.load_diagram_id = cld.load_diagram_id
        AND cld.TC_ORDER_ID = O.TC_ORDER_ID
        AND ( (o.tc_shipment_id = clm.tc_shipment_id)
          OR (exists (select 1 from lpn l where l.tc_shipment_id = clm.tc_shipment_id 
                and l.order_id = o.order_id and l.inbound_outbound_indicator='O' and l.lpn_facility_status >=20) ) )
		AND o.order_id = oli.order_id
        AND cld.line_item_id = oli.line_item_id
 		AND oli.item_id = im.item_id
		AND oli.item_id = pld.item_id
 		AND pld.locn_id = lh.locn_id
    and lh.locn_class = 'A'
    and cld.num_cases > 0
		AND cld.storage_type = cst.storage_type_id
		AND clm.tc_shipment_id = sh.tc_shipment_id
    --AND exists (select 1 from whse_sys_code where rec_type = 'C' and code_type = 'L2B' and trim(substr(misc_flags,1,4)) = cst.cntr_type  and trim(substr(misc_flags,5,25)) = cld.product_type)
		and exists (select 1 from whse_sys_code where rec_type = 'C' and code_type = 'L2D' and trim(substr(misc_flags,1,4)) = cst.cntr_type)
		ORDER BY cld.stop_number,cld.storage_type,cld.trailer_position;

   v_task_number number(9) := 1;
   v_tempcount number(9) := 1;
   v_lpncount number(2) := 1;
   v_labor_msg_id number(9) :=0;
   v_tran_nbr number(9);
   v_endTranNbr Number(9) :=0;
   v_nbrRecCnts Number(9) :=0;
   v_tran_seq_nbr number(9) := 1;
   v_tmp_trlr_posn_prev varchar2(30) := NULL;
   v_tmp_stop_number_prev number(8) :=0;
   v_tmp_product_type_prev varchar2(30) :=NULL;
   v_tmp_cntnr_type_prev varchar2(3) :=NULL;
   v_currcontn_type varchar2(30) :=NULL;
   v_brkbystop varchar2(2) := 'N';
   v_lpn_changed varchar2(1) := 'N';
   v_Lbr_Msg_Rows NUMBER(4) :=0;
   v_hdr_inserted varchar2(1):= 'N';

-- Store the cursor result set into a list
   TYPE dl_list1 IS TABLE OF c_plnevnt_cur_Picking%ROWTYPE;
   dl_list dl_list1;

   TYPE dl_list2 IS TABLE OF c_plnevnt_cur_Loading%ROWTYPE;
   dl_list_loading dl_list2;

   TYPE dl_list3 IS TABLE OF c_NBrk_cur_Picking%ROWTYPE;
   dl_list_nobrk dl_list3;

BEGIN

  P_RC 			:= 0;

  BEGIN
        -- Check and Clear plan data previously generated for diagram
		 SELECT COUNT(*) into v_Lbr_Msg_Rows FROM LABOR_MSG LABOR
		 WHERE LABOR.REF_NBR = p_tc_shpmnt_id
		 AND ACT_NAME like 'PLAN%';
		
      	IF v_Lbr_Msg_Rows > 0 THEN
        for i in (
				 SELECT LABOR.TRAN_NBR TRAN_NBR FROM LABOR_MSG LABOR
				 WHERE  LABOR.REF_NBR = p_tc_shpmnt_id
         AND ACT_NAME like 'PLAN%'
				 --added the status because other transactions in 99 status need not be queried
				 )
          LOOP
        			DELETE FROM e_aud_log_trvl WHERE tran_nbr = i.TRAN_NBR;
        			DELETE FROM e_aud_log_factor WHERE aud_id in
						(select aud_id from e_aud_log where tran_nbr = i.TRAN_NBR);
        			DELETE FROM e_error_log WHERE tran_nbr = i.TRAN_NBR;
        			DELETE FROM e_aud_log WHERE tran_nbr = i.TRAN_NBR;
        			DELETE FROM labor_msg_crit WHERE tran_nbr = i.TRAN_NBR;
        			DELETE FROM labor_msg_dtl_crit WHERE tran_nbr = i.TRAN_NBR;
        			DELETE FROM e_evnt_smry_elm_dtl WHERE tran_nbr = i.TRAN_NBR;
        			DELETE FROM e_evnt_smry_hdr WHERE tran_nbr= i.TRAN_NBR;
        			DELETE FROM LABOR_MSG_CRIT WHERE TRAN_NBR = i.TRAN_NBR;
        			DELETE FROM labor_msg_dtl WHERE TRAN_NBR = i.TRAN_NBR;
        			DELETE FROM e_evnt_smry_elm_dtl WHERE TRAN_NBR = i.TRAN_NBR;
        			DELETE FROM E_EVNT_SMRY_HDR WHERE TRAN_NBR = i.TRAN_NBR;
              DELETE FROM E_ZONE_MSG_LOG where TRAN_NBR =i.TRAN_NBR;
        			DELETE FROM labor_msg WHERE TRAN_NBR = i.TRAN_NBR;
            end loop;
       		COMMIT;
      	END IF;


   exception
   WHEN OTHERS THEN
      raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
   END;

-- **************************************************
--------  Start for PICKING   -----------------------

-- fectch the unique product types and loop through them
		OPEN c_prod_type_cur;
		FETCH c_prod_type_cur BULK COLLECT INTO v_prod_list;
		CLOSE c_prod_type_cur;

    --BEGIN LOOP 
    -- This loops through all the product types and 
    -- creates events based on if 
    dbms_output.put_line('Starting Product Type in no brk by stop'); 

  	FOR k IN 1 .. v_prod_list.COUNT
    LOOP
    
    v_prod_type_loop := v_prod_list(k).product_type;

	  
    IF (v_prod_list(k).brk_by_stop = 'Y') then 
    -------------------------------------------------------------------------
    -- THIS IS WHERE WE BREAK BY STOP
    -- Use cursor c_plnevent_cur_Picking
    -------------------------------------------------------------------------
    dbms_output.put_line('Starting Product Type in brk by stop : ' || v_prod_list(k).brk_by_stop); 
    dbms_output.put_line('Starting Product Type in brk by stop : ' || v_prod_type_loop); 
   
    -- fetch from the cursor to bulk collect into a list
		OPEN c_plnevnt_cur_Picking;
		FETCH c_plnevnt_cur_Picking BULK COLLECT INTO dl_list;
		CLOSE c_plnevnt_cur_Picking;

		v_nbrRecCnts := dl_list.Count;

	--  SELECT CURR_NBR INTO v_tran_nbr FROM nxt_up_cnt WHERE rec_type_id = 'WI1';
		MANH_GET_NXTUP('*','WI1',v_nbrRecCnts,P_RC,v_tran_nbr,v_endTranNbr);

	-- loop through all details
		FOR i IN 1 .. dl_list.COUNT
		LOOP
    
      IF (dl_list(i).PICKING_contnr_type ='BUN' OR
          dl_list(i).PICKING_contnr_type ='POP' OR
          dl_list(i).PICKING_contnr_type ='CKE' OR 
          dl_list(i).PICKING_contnr_type ='FRY')
      THEN
        CONTINUE;
      END IF;
		  
      -- Check if not first row from list , check prev with current column values
		  -- insert once for the first row to the header labor_msg table
		  
      IF v_hdr_inserted = 'N' Then
       -- select labor_msg_id_seq.nextval into a variable for multithreading
		   SELECT LABOR_MSG_ID_SEQ.NEXTVAL INTO v_labor_msg_id FROM dual;

		   INSERT INTO LABOR_MSG
				(ACT_NAME,LOGIN_USER_ID,SCHED_START_TIME,SCHED_START_DATE,VHCL_TYPE,MOD_USER_ID,REF_CODE,REF_NBR,
				 DEPT,DIV,SHIFT,USER_DEF_VAL_1,MISC_TXT_2,MISC_NUM_1,MISC_NUM_2,RESEND_TRAN,REQ_SAM_REPLY,TEAM_STD_GRP,
				 LAST_UPDATED_DTTM,HIBERNATE_VERSION,LAST_UPDATED_SOURCE_TYPE,CREATED_SOURCE_TYPE,CREATED_DTTM,TRAN_NBR,WHSE,LABOR_MSG_ID,MISC,
				 WHSE_DATE,ACTUAL_END_DATE,ACTUAL_END_TIME,MSG_STAT_CODE)
				VALUES
				( dl_list(i).LM_PICKING_ACT_NAME,dl_list(i).LM_PICKING_MOD_USER_ID,dl_list(i).LM_PICKING_SCHED_START_TIME,dl_list(i).LM_PICKING_SCHED_START_DATE,
				  dl_list(i).LM_PICKING_VHCL_TYPE,dl_list(i).LM_PICKING_MOD_USER_ID,dl_list(i).LM_PICKING_REF_CODE,dl_list(i).LM_PICKING_REF_NBR,
				  dl_list(i).LM_PICKING_DEPT,dl_list(i).LM_PICKING_DIV,dl_list(i).LM_PICKING_SHIFT,dl_list(i).LM_PICKING_USER_DEF_VAL_1,dl_list(i).LM_PICKING_MISC_TXT_2,
				  dl_list(i).LM_PICKING_MISC_NUM_1,dl_list(i).LM_PICKING_MISC_NUM_2,dl_list(i).LM_PICKING_RESEND_TRAN,dl_list(i).LM_PICKING_REQ_SAM_REPLY,dl_list(i).LM_PICKING_TEAM_STD_GRP
				  ,SYSDATE,1,1,1,SYSTIMESTAMP,v_tran_nbr,dl_list(i).LM_PICKING_WHSE,v_labor_msg_id,lpad(v_task_number,4,'0'),dl_list(i).LM_PICKING_WHSE_DATE,dl_list(i).LM_PICKING_ACTUAL_END_DATE,
				  dl_list(i).LM_PICKING_ACTUAL_END_TIME,50 );

		   	-- insert into details table
			INSERT INTO LABOR_MSG_DTL
			 (ITEM_NAME, ITEM_BAR_CODE,SEQ_NBR,HNDL_ATTR,QTY_PER_GRAB,DSP_LOCN,LOCN_CLASS,QTY,TC_ILPN_ID,PALLET_ID,LOCN_SLOT_TYPE,LOCN_X_COORD,
			  LOCN_Y_COORD,LOCN_Z_COORD,LOCN_TRAV_AISLE,LOCN_TRAV_ZONE,INNERPACK_QTY,PACK_QTY,CASE_QTY,TIER_QTY,PALLET_QTY,CO,DIV,
			  MISC,MISC_2,WEIGHT,VOLUME,START_DATE_TIME,HANDLING_UOM,SEASON,SEASON_YR,
			  STYLE,STYLE_SFX,COLOR,COLOR_SFX,SEC_DIM,QUAL,SIZE_RNGE_CODE,SIZE_REL_POSN_IN_TABLE,TC_OLPN_ID,MISC_NUM_2,LOADED,PUTAWAY_ZONE,
			  PICK_DETERMINATION_ZONE,WORK_GROUP,WORK_AREA,PULL_ZONE,ASSIGNMENT_ZONE,
			  LAST_UPDATED_DTTM,HIBERNATE_VERSION,LAST_UPDATED_SOURCE_TYPE,CREATED_SOURCE_TYPE,CREATED_DTTM,TRAN_NBR,LABOR_MSG_ID,LABOR_MSG_DTL_ID,
			  MSG_STAT_CODE,TRAN_SEQ_NBR,MISC_NUM_1)
			VALUES ( dl_list(i).LMD_PICK_ITEM_BAR_CODE,dl_list(i).LMD_PICK_ITEM_BAR_CODE,'10', dl_list(i).LMD_PICK_HNDL_ATTR, dl_list(i).LMD_PICK_QTY_PER_GRAB, dl_list(i).LMD_PICK_DSP_LOCN,
					dl_list(i).LMD_PICK_LOCN_CLASS, dl_list(i).LMD_PICK_QTY, dl_list(i).LMD_PICK_TC_ILPN_ID, TO_CHAR(dl_list(i).LMD_PICK_PALLET_ID),
					dl_list(i).LMD_PICK_LOCN_SLOT_TYPE,
					dl_list(i).LMD_PICK_LOCN_X_COORD, dl_list(i).LMD_PICK_LOCN_Y_COORD, dl_list(i).LMD_PICK_LOCN_Z_COORD, dl_list(i).LMD_PICK_LOCN_TRAV_AISLE,
					dl_list(i).LMD_PICK_LOCN_TRAV_ZONE, dl_list(i).LMD_PICK_INNERPACK_QTY,dl_list(i).LMD_PICK_PACK_QTY,
					dl_list(i).LMD_PICK_CASE_QTY, dl_list(i).LMD_PICK_TIER_QTY, dl_list(i).LMD_PICK_PALLET_QTY, dl_list(i).LMD_PICK_CO, dl_list(i).LMD_PICK_DIV,
					dl_list(i).PICKING_trailer_posn,--dl_list(i).LMD_PICK_MISC, 
          dl_list(i).PICKING_contnr_type,
          dl_list(i).LMD_PICK_WEIGHT, dl_list(i).LMD_PICK_VOLUME, dl_list(i).LMD_PICK_START_DATE_TIME,
					dl_list(i).LMD_PICK_HANDLING_UOM, dl_list(i).LMD_PICK_SEASON,dl_list(i).LMD_PICK_SEASON_YR, dl_list(i).LMD_PICK_STYLE, dl_list(i).LMD_PICK_STYLE_SFX,
					dl_list(i).LMD_PICK_COLOR, dl_list(i).LMD_PICK_COLOR_SFX,dl_list(i).LMD_PICK_SEC_DIM, 
          dl_list(i).LMD_PICK_QUAL, 
          dl_list(i).LMD_PICK_SIZE_RNGE_CODE,
					dl_list(i).LMD_PICK_SIZERELPOSNINTAB, 
          'LPN' || lpad(v_lpncount, 7, '0') ,
          dl_list(i).LMD_PICK_MISC_NUM_2, dl_list(i).LMD_PICK_LOADED,
					dl_list(i).LMD_PICK_PUTAWAY_ZONE,
					dl_list(i).LMD_PICK_DETZONE, dl_list(i).LMD_PICK_WORK_GROUP, dl_list(i).LMD_PICK_WORK_AREA, dl_list(i).LMD_PICK_PULL_ZONE,
					dl_list(i).LMD_PICK_ASSIGNMENT_ZONE,SYSDATE,1,1,1,SYSTIMESTAMP,v_tran_nbr,v_labor_msg_id,labor_msg_dtl_id_seq.nextval,'01',
					v_tran_seq_nbr,dl_list(i).PICKING_nbr_of_contnrs_syscode);

          v_tmp_trlr_posn_prev := dl_list(i).PICKING_trailer_posn;
    			v_tmp_stop_number_prev := dl_list(i).PICKING_stop_number;
    			v_tmp_product_type_prev := substr(dl_list(i).PICKING_product_type, 1,1);
    			v_tmp_cntnr_type_prev := dl_list(i).PICKING_contnr_type;
		   
       v_hdr_inserted := 'Y';
			 
      
      else
		  -- check if trailer position changes , count number of container confgiured in sys_code
		  IF  ( NOT( 
                substr(dl_list(i).PICKING_product_type, 1,1 ) = 'B' AND v_tmp_product_type_prev = 'B' AND trim(dl_list(i).PICKING_stop_number) = trim(v_tmp_stop_number_prev)) --Buns same stop
            AND
                ((dl_list(i).PICKING_trailer_posn <> v_tmp_trlr_posn_prev )
                OR  
                (substr(dl_list(i).PICKING_product_type, 1,1 )!=v_tmp_product_type_prev )
                OR
                (v_tmp_stop_number_prev != dl_list(i).PICKING_stop_number)
              )
            )
      THEN
				 v_tempcount := v_tempcount +1 ;
         v_lpncount := v_lpncount+1;
         v_lpn_changed := 'Y';
		  END IF;


		  IF (v_lpn_changed = 'Y' AND 
            ((dl_list(i).PICKING_nbr_of_contnrs_syscode is null) OR
          (v_tempcount > to_number(dl_list(i).PICKING_nbr_of_contnrs_syscode)) OR
          (substr(dl_list(i).PICKING_product_type, 1,1 )!=v_tmp_product_type_prev ) OR
          (v_tmp_stop_number_prev != dl_list(i).PICKING_stop_number)))             
		  THEN
      
      --- RESEQUENCE : Resequence the tran_seq_nbr based on the pick sequence so travel calculations look good
      update labor_msg_dtl l
        set tran_seq_nbr = (select new_seq from (select tran_seq_nbr, size_rnge_code, tran_nbr,
        dense_rank() over (partition by tran_nbr order by size_rnge_code, labor_msg_dtl_id ) new_seq
        from labor_msg_dtl where tran_nbr = v_tran_nbr) where tran_seq_nbr = l.tran_seq_nbr)
        where tran_nbr = v_tran_nbr;
      
      
      
         v_lpn_changed := 'N';
			   v_task_number := v_task_number + 1;
			   v_tempcount := 1;
			   v_tran_nbr := v_tran_nbr + 1;
			   v_tran_seq_nbr :=  1;

			 -- select labor_msg_id_seq.nextval into a variable for multithreading
			 SELECT LABOR_MSG_ID_SEQ.NEXTVAL INTO v_labor_msg_id FROM dual;

				INSERT INTO LABOR_MSG
				(ACT_NAME,LOGIN_USER_ID,SCHED_START_TIME,SCHED_START_DATE,VHCL_TYPE,MOD_USER_ID,REF_CODE,REF_NBR,
				 DEPT,DIV,SHIFT,USER_DEF_VAL_1,MISC_TXT_2,MISC_NUM_1,MISC_NUM_2,RESEND_TRAN,REQ_SAM_REPLY,TEAM_STD_GRP,
				 LAST_UPDATED_DTTM,HIBERNATE_VERSION,LAST_UPDATED_SOURCE_TYPE,CREATED_SOURCE_TYPE,CREATED_DTTM,TRAN_NBR,WHSE,LABOR_MSG_ID,MISC,
				 WHSE_DATE,ACTUAL_END_DATE,ACTUAL_END_TIME,MSG_STAT_CODE)
				VALUES
				( dl_list(i).LM_PICKING_ACT_NAME,dl_list(i).LM_PICKING_MOD_USER_ID,dl_list(i).LM_PICKING_SCHED_START_TIME,dl_list(i).LM_PICKING_SCHED_START_DATE,
				  dl_list(i).LM_PICKING_VHCL_TYPE,dl_list(i).LM_PICKING_MOD_USER_ID,dl_list(i).LM_PICKING_REF_CODE,dl_list(i).LM_PICKING_REF_NBR,
				  dl_list(i).LM_PICKING_DEPT,dl_list(i).LM_PICKING_DIV,dl_list(i).LM_PICKING_SHIFT,dl_list(i).LM_PICKING_USER_DEF_VAL_1,dl_list(i).LM_PICKING_MISC_TXT_2,
				  dl_list(i).LM_PICKING_MISC_NUM_1,dl_list(i).LM_PICKING_MISC_NUM_2,dl_list(i).LM_PICKING_RESEND_TRAN,dl_list(i).LM_PICKING_REQ_SAM_REPLY,dl_list(i).LM_PICKING_TEAM_STD_GRP
				  ,SYSDATE,1,1,1,SYSTIMESTAMP,v_tran_nbr,dl_list(i).LM_PICKING_WHSE,v_labor_msg_id,lpad(v_task_number,4,'0'),dl_list(i).LM_PICKING_WHSE_DATE,
				  dl_list(i).LM_PICKING_ACTUAL_END_DATE,dl_list(i).LM_PICKING_ACTUAL_END_TIME,50
				 );

		  END IF;

		-- insert into details table
		INSERT INTO LABOR_MSG_DTL
			 (ITEM_NAME, ITEM_BAR_CODE,SEQ_NBR,HNDL_ATTR,QTY_PER_GRAB,DSP_LOCN,LOCN_CLASS,QTY,TC_ILPN_ID,PALLET_ID,LOCN_SLOT_TYPE,LOCN_X_COORD,
			  LOCN_Y_COORD,LOCN_Z_COORD,LOCN_TRAV_AISLE,LOCN_TRAV_ZONE,INNERPACK_QTY,PACK_QTY,CASE_QTY,TIER_QTY,PALLET_QTY,CO,DIV,
			  MISC,MISC_2,WEIGHT,VOLUME,START_DATE_TIME,HANDLING_UOM,SEASON,SEASON_YR,
			  STYLE,STYLE_SFX,COLOR,COLOR_SFX,SEC_DIM,QUAL,SIZE_RNGE_CODE,SIZE_REL_POSN_IN_TABLE,TC_OLPN_ID,MISC_NUM_2,LOADED,PUTAWAY_ZONE,
			  PICK_DETERMINATION_ZONE,WORK_GROUP,WORK_AREA,PULL_ZONE,ASSIGNMENT_ZONE,
			  LAST_UPDATED_DTTM,HIBERNATE_VERSION,LAST_UPDATED_SOURCE_TYPE,CREATED_SOURCE_TYPE,CREATED_DTTM,TRAN_NBR,LABOR_MSG_ID,LABOR_MSG_DTL_ID,
			  MSG_STAT_CODE,TRAN_SEQ_NBR,MISC_NUM_1)
		  VALUES ( dl_list(i).LMD_PICK_ITEM_BAR_CODE,dl_list(i).LMD_PICK_ITEM_BAR_CODE,dl_list(i).LMD_PICK_MISC, dl_list(i).LMD_PICK_HNDL_ATTR, dl_list(i).LMD_PICK_QTY_PER_GRAB, dl_list(i).LMD_PICK_DSP_LOCN,
					dl_list(i).LMD_PICK_LOCN_CLASS, dl_list(i).LMD_PICK_QTY, dl_list(i).LMD_PICK_TC_ILPN_ID, TO_CHAR(dl_list(i).LMD_PICK_PALLET_ID),
					dl_list(i).LMD_PICK_LOCN_SLOT_TYPE,
					dl_list(i).LMD_PICK_LOCN_X_COORD, dl_list(i).LMD_PICK_LOCN_Y_COORD, dl_list(i).LMD_PICK_LOCN_Z_COORD, dl_list(i).LMD_PICK_LOCN_TRAV_AISLE,
					dl_list(i).LMD_PICK_LOCN_TRAV_ZONE, dl_list(i).LMD_PICK_INNERPACK_QTY,dl_list(i).LMD_PICK_PACK_QTY,
					dl_list(i).LMD_PICK_CASE_QTY, dl_list(i).LMD_PICK_TIER_QTY, dl_list(i).LMD_PICK_PALLET_QTY, dl_list(i).LMD_PICK_CO, dl_list(i).LMD_PICK_DIV,
					dl_list(i).PICKING_trailer_posn,--dl_list(i).LMD_PICK_MISC, 
          dl_list(i).PICKING_contnr_type,dl_list(i).LMD_PICK_WEIGHT, dl_list(i).LMD_PICK_VOLUME, dl_list(i).LMD_PICK_START_DATE_TIME,
					dl_list(i).LMD_PICK_HANDLING_UOM, dl_list(i).LMD_PICK_SEASON,dl_list(i).LMD_PICK_SEASON_YR, dl_list(i).LMD_PICK_STYLE, dl_list(i).LMD_PICK_STYLE_SFX,
					dl_list(i).LMD_PICK_COLOR, dl_list(i).LMD_PICK_COLOR_SFX,dl_list(i).LMD_PICK_SEC_DIM, dl_list(i).LMD_PICK_QUAL, dl_list(i).LMD_PICK_SIZE_RNGE_CODE,
					dl_list(i).LMD_PICK_SIZERELPOSNINTAB,
          'LPN' || lpad(v_lpncount, 7, '0'),
          dl_list(i).LMD_PICK_MISC_NUM_2, dl_list(i).LMD_PICK_LOADED,
					dl_list(i).LMD_PICK_PUTAWAY_ZONE,
					dl_list(i).LMD_PICK_DETZONE, dl_list(i).LMD_PICK_WORK_GROUP, dl_list(i).LMD_PICK_WORK_AREA, dl_list(i).LMD_PICK_PULL_ZONE,
					dl_list(i).LMD_PICK_ASSIGNMENT_ZONE,SYSDATE,1,1,1,SYSTIMESTAMP,v_tran_nbr,v_labor_msg_id,labor_msg_dtl_id_seq.nextval,'01',
					v_tran_seq_nbr,dl_list(i).PICKING_nbr_of_contnrs_syscode);


			v_tmp_trlr_posn_prev := dl_list(i).PICKING_trailer_posn;
			v_tmp_stop_number_prev := dl_list(i).PICKING_stop_number;
			v_tmp_product_type_prev := substr(dl_list(i).PICKING_product_type, 1, 1);
			v_tmp_cntnr_type_prev := dl_list(i).PICKING_contnr_type;
	   end if;
     
           v_tran_seq_nbr := v_tran_seq_nbr + 1;
     END LOOP;
---- RESEQUENCE : Resequence the final one
      update labor_msg_dtl l
        set tran_seq_nbr = (select new_seq from (select tran_seq_nbr, size_rnge_code, tran_nbr,
        dense_rank() over (partition by tran_nbr order by size_rnge_code, labor_msg_dtl_id ) new_seq
        from labor_msg_dtl where tran_nbr = v_tran_nbr) where tran_seq_nbr = l.tran_seq_nbr)
        where tran_nbr = v_tran_nbr;
    
    
    ELSE
    -------------------------------------------------------------------------
    -- THIS IS WHERE WE DON'T BREAK BY STOP
    -- Use Cursor c_NBrk_cur_Picking
    -- Example pompano - ref and frozen should come here
    -------------------------------------------------------------------------
   dbms_output.put_line('Starting Product Type in no brk by stop : ' || v_prod_list(k).brk_by_stop); 
   dbms_output.put_line('Starting Product Type in no brk by stop : ' || v_prod_type_loop); 
   
    -- fetch from the cursor to bulk collect into a list
		OPEN c_NBrk_cur_Picking;
		FETCH c_NBrk_cur_Picking BULK COLLECT INTO dl_list_nobrk;
		CLOSE c_NBrk_cur_Picking;

		v_nbrRecCnts := dl_list_nobrk.Count;

	--  SELECT CURR_NBR INTO v_tran_nbr FROM nxt_up_cnt WHERE rec_type_id = 'WI1';
		MANH_GET_NXTUP('*','WI1',v_nbrRecCnts,P_RC,v_tran_nbr,v_endTranNbr);

	-- loop through all details
		FOR i IN 1 .. dl_list_nobrk.COUNT
		LOOP
    
      IF (dl_list_nobrk(i).PICKING_contnr_type ='BUN' OR
          dl_list_nobrk(i).PICKING_contnr_type ='POP' OR
          dl_list_nobrk(i).PICKING_contnr_type ='CKE' OR 
          dl_list_nobrk(i).PICKING_contnr_type ='FRY')
      THEN
        CONTINUE;
      END IF;
		  
      -- Check if not first row from list , check prev with current column values
		  -- insert once for the first row to the header labor_msg table
		  
      IF v_hdr_inserted = 'N' Then
       -- select labor_msg_id_seq.nextval into a variable for multithreading
		   SELECT LABOR_MSG_ID_SEQ.NEXTVAL INTO v_labor_msg_id FROM dual;

		   INSERT INTO LABOR_MSG
				(ACT_NAME,LOGIN_USER_ID,SCHED_START_TIME,SCHED_START_DATE,VHCL_TYPE,MOD_USER_ID,REF_CODE,REF_NBR,
				 DEPT,DIV,SHIFT,USER_DEF_VAL_1,MISC_TXT_2,MISC_NUM_1,MISC_NUM_2,RESEND_TRAN,REQ_SAM_REPLY,TEAM_STD_GRP,
				 LAST_UPDATED_DTTM,HIBERNATE_VERSION,LAST_UPDATED_SOURCE_TYPE,CREATED_SOURCE_TYPE,CREATED_DTTM,TRAN_NBR,WHSE,LABOR_MSG_ID,MISC,
				 WHSE_DATE,ACTUAL_END_DATE,ACTUAL_END_TIME,MSG_STAT_CODE)
				VALUES
				( dl_list_nobrk(i).LM_PICKING_ACT_NAME,dl_list_nobrk(i).LM_PICKING_MOD_USER_ID,dl_list_nobrk(i).LM_PICKING_SCHED_START_TIME,dl_list_nobrk(i).LM_PICKING_SCHED_START_DATE,
				  dl_list_nobrk(i).LM_PICKING_VHCL_TYPE,dl_list_nobrk(i).LM_PICKING_MOD_USER_ID,dl_list_nobrk(i).LM_PICKING_REF_CODE,dl_list_nobrk(i).LM_PICKING_REF_NBR,
				  dl_list_nobrk(i).LM_PICKING_DEPT,dl_list_nobrk(i).LM_PICKING_DIV,dl_list_nobrk(i).LM_PICKING_SHIFT,dl_list_nobrk(i).LM_PICKING_USER_DEF_VAL_1,dl_list_nobrk(i).LM_PICKING_MISC_TXT_2,
				  dl_list_nobrk(i).LM_PICKING_MISC_NUM_1,dl_list_nobrk(i).LM_PICKING_MISC_NUM_2,dl_list_nobrk(i).LM_PICKING_RESEND_TRAN,dl_list_nobrk(i).LM_PICKING_REQ_SAM_REPLY,dl_list_nobrk(i).LM_PICKING_TEAM_STD_GRP
				  ,SYSDATE,1,1,1,SYSTIMESTAMP,v_tran_nbr,dl_list_nobrk(i).LM_PICKING_WHSE,v_labor_msg_id,lpad(v_task_number,4,'0'),dl_list_nobrk(i).LM_PICKING_WHSE_DATE,dl_list_nobrk(i).LM_PICKING_ACTUAL_END_DATE,
				  dl_list_nobrk(i).LM_PICKING_ACTUAL_END_TIME,50 );

		   	-- insert into details table
			INSERT INTO LABOR_MSG_DTL
			 (ITEM_NAME, ITEM_BAR_CODE,SEQ_NBR,HNDL_ATTR,QTY_PER_GRAB,DSP_LOCN,LOCN_CLASS,QTY,TC_ILPN_ID,PALLET_ID,LOCN_SLOT_TYPE,LOCN_X_COORD,
			  LOCN_Y_COORD,LOCN_Z_COORD,LOCN_TRAV_AISLE,LOCN_TRAV_ZONE,INNERPACK_QTY,PACK_QTY,CASE_QTY,TIER_QTY,PALLET_QTY,CO,DIV,
			  MISC,MISC_2,WEIGHT,VOLUME,START_DATE_TIME,HANDLING_UOM,SEASON,SEASON_YR,
			  STYLE,STYLE_SFX,COLOR,COLOR_SFX,SEC_DIM,QUAL,SIZE_RNGE_CODE,SIZE_REL_POSN_IN_TABLE,TC_OLPN_ID,MISC_NUM_2,LOADED,PUTAWAY_ZONE,
			  PICK_DETERMINATION_ZONE,WORK_GROUP,WORK_AREA,PULL_ZONE,ASSIGNMENT_ZONE,
			  LAST_UPDATED_DTTM,HIBERNATE_VERSION,LAST_UPDATED_SOURCE_TYPE,CREATED_SOURCE_TYPE,CREATED_DTTM,TRAN_NBR,LABOR_MSG_ID,LABOR_MSG_DTL_ID,
			  MSG_STAT_CODE,TRAN_SEQ_NBR,MISC_NUM_1)
			VALUES ( dl_list_nobrk(i).LMD_PICK_ITEM_BAR_CODE,dl_list_nobrk(i).LMD_PICK_ITEM_BAR_CODE,'10', dl_list_nobrk(i).LMD_PICK_HNDL_ATTR, 
          dl_list_nobrk(i).LMD_PICK_QTY_PER_GRAB, dl_list_nobrk(i).LMD_PICK_DSP_LOCN,
					dl_list_nobrk(i).LMD_PICK_LOCN_CLASS, dl_list_nobrk(i).LMD_PICK_QTY, dl_list_nobrk(i).LMD_PICK_TC_ILPN_ID, TO_CHAR(dl_list_nobrk(i).LMD_PICK_PALLET_ID),
					dl_list_nobrk(i).LMD_PICK_LOCN_SLOT_TYPE,
					dl_list_nobrk(i).LMD_PICK_LOCN_X_COORD, dl_list_nobrk(i).LMD_PICK_LOCN_Y_COORD, dl_list_nobrk(i).LMD_PICK_LOCN_Z_COORD, dl_list_nobrk(i).LMD_PICK_LOCN_TRAV_AISLE,
					dl_list_nobrk(i).LMD_PICK_LOCN_TRAV_ZONE, dl_list_nobrk(i).LMD_PICK_INNERPACK_QTY,dl_list_nobrk(i).LMD_PICK_PACK_QTY,
					dl_list_nobrk(i).LMD_PICK_CASE_QTY, dl_list_nobrk(i).LMD_PICK_TIER_QTY, dl_list_nobrk(i).LMD_PICK_PALLET_QTY, dl_list_nobrk(i).LMD_PICK_CO, dl_list_nobrk(i).LMD_PICK_DIV,
					dl_list_nobrk(i).PICKING_trailer_posn,--dl_list(i).LMD_PICK_MISC, 
          dl_list_nobrk(i).PICKING_contnr_type,
          dl_list_nobrk(i).LMD_PICK_WEIGHT, dl_list_nobrk(i).LMD_PICK_VOLUME, dl_list_nobrk(i).LMD_PICK_START_DATE_TIME,
					dl_list_nobrk(i).LMD_PICK_HANDLING_UOM, dl_list_nobrk(i).LMD_PICK_SEASON,dl_list_nobrk(i).LMD_PICK_SEASON_YR, dl_list_nobrk(i).LMD_PICK_STYLE, dl_list_nobrk(i).LMD_PICK_STYLE_SFX,
					dl_list_nobrk(i).LMD_PICK_COLOR, dl_list_nobrk(i).LMD_PICK_COLOR_SFX,dl_list_nobrk(i).LMD_PICK_SEC_DIM, 
          dl_list_nobrk(i).LMD_PICK_QUAL, 
          dl_list_nobrk(i).LMD_PICK_SIZE_RNGE_CODE,
					dl_list_nobrk(i).LMD_PICK_SIZERELPOSNINTAB, 
          'LPN' || lpad(v_lpncount, 7, '0') ,
          dl_list_nobrk(i).LMD_PICK_MISC_NUM_2, dl_list_nobrk(i).LMD_PICK_LOADED,
					dl_list_nobrk(i).LMD_PICK_PUTAWAY_ZONE,
					dl_list_nobrk(i).LMD_PICK_DETZONE, dl_list_nobrk(i).LMD_PICK_WORK_GROUP, dl_list_nobrk(i).LMD_PICK_WORK_AREA, dl_list_nobrk(i).LMD_PICK_PULL_ZONE,
					dl_list_nobrk(i).LMD_PICK_ASSIGNMENT_ZONE,SYSDATE,1,1,1,SYSTIMESTAMP,v_tran_nbr,v_labor_msg_id,labor_msg_dtl_id_seq.nextval,'01',
					v_tran_seq_nbr,dl_list_nobrk(i).PICKING_nbr_of_contnrs_syscode);

          v_tmp_trlr_posn_prev := dl_list_nobrk(i).PICKING_trailer_posn;
    			v_tmp_stop_number_prev := dl_list_nobrk(i).PICKING_stop_number;
    			v_tmp_product_type_prev := substr(dl_list_nobrk(i).PICKING_product_type, 1,1);
    			v_tmp_cntnr_type_prev := dl_list_nobrk(i).PICKING_contnr_type;
		   
       v_hdr_inserted := 'Y';
			 
      
      else
		  -- check if trailer position changes , count number of container confgiured in sys_code
		  IF  ( NOT( 
                substr(dl_list_nobrk(i).PICKING_product_type, 1,1 ) = 'B' AND v_tmp_product_type_prev = 'B' AND trim(dl_list_nobrk(i).PICKING_stop_number) = trim(v_tmp_stop_number_prev)) --Buns same stop
            AND
                ((dl_list_nobrk(i).PICKING_trailer_posn <> v_tmp_trlr_posn_prev )
                OR  
                (substr(dl_list_nobrk(i).PICKING_product_type, 1,1 )!=v_tmp_product_type_prev )                
              )
            )
      THEN
				 v_tempcount := v_tempcount +1 ;
         v_lpncount := v_lpncount+1;
         v_lpn_changed := 'Y';
		  END IF;


		  IF (v_lpn_changed = 'Y' AND 
            ((dl_list_nobrk(i).PICKING_nbr_of_contnrs_syscode is null) OR
          (v_tempcount > to_number(dl_list_nobrk(i).PICKING_nbr_of_contnrs_syscode)) OR
          (substr(dl_list_nobrk(i).PICKING_product_type, 1,1 )!=v_tmp_product_type_prev )))             
		  THEN
      
      --- RESEQUENCE : Resequence the tran_seq_nbr based on the pick sequence so travel calculations look good
      update labor_msg_dtl l
        set tran_seq_nbr = (select new_seq from (select tran_seq_nbr, size_rnge_code, tran_nbr,
        dense_rank() over (partition by tran_nbr order by size_rnge_code, labor_msg_dtl_id ) new_seq
        from labor_msg_dtl where tran_nbr = v_tran_nbr) where tran_seq_nbr = l.tran_seq_nbr)
        where tran_nbr = v_tran_nbr;
      
         v_lpn_changed := 'N';
			   v_task_number := v_task_number + 1;
			   v_tempcount := 1;
			   v_tran_nbr := v_tran_nbr + 1;
			   v_tran_seq_nbr :=  1;

			 -- select labor_msg_id_seq.nextval into a variable for multithreading
			 SELECT LABOR_MSG_ID_SEQ.NEXTVAL INTO v_labor_msg_id FROM dual;

				INSERT INTO LABOR_MSG
				(ACT_NAME,LOGIN_USER_ID,SCHED_START_TIME,SCHED_START_DATE,VHCL_TYPE,MOD_USER_ID,REF_CODE,REF_NBR,
				 DEPT,DIV,SHIFT,USER_DEF_VAL_1,MISC_TXT_2,MISC_NUM_1,MISC_NUM_2,RESEND_TRAN,REQ_SAM_REPLY,TEAM_STD_GRP,
				 LAST_UPDATED_DTTM,HIBERNATE_VERSION,LAST_UPDATED_SOURCE_TYPE,CREATED_SOURCE_TYPE,CREATED_DTTM,TRAN_NBR,WHSE,LABOR_MSG_ID,MISC,
				 WHSE_DATE,ACTUAL_END_DATE,ACTUAL_END_TIME,MSG_STAT_CODE)
				VALUES
				( dl_list_nobrk(i).LM_PICKING_ACT_NAME,dl_list_nobrk(i).LM_PICKING_MOD_USER_ID,dl_list_nobrk(i).LM_PICKING_SCHED_START_TIME,dl_list_nobrk(i).LM_PICKING_SCHED_START_DATE,
				  dl_list_nobrk(i).LM_PICKING_VHCL_TYPE,dl_list_nobrk(i).LM_PICKING_MOD_USER_ID,dl_list_nobrk(i).LM_PICKING_REF_CODE,dl_list_nobrk(i).LM_PICKING_REF_NBR,
				  dl_list_nobrk(i).LM_PICKING_DEPT,dl_list_nobrk(i).LM_PICKING_DIV,dl_list_nobrk(i).LM_PICKING_SHIFT,dl_list_nobrk(i).LM_PICKING_USER_DEF_VAL_1,dl_list_nobrk(i).LM_PICKING_MISC_TXT_2,
				  dl_list_nobrk(i).LM_PICKING_MISC_NUM_1,dl_list_nobrk(i).LM_PICKING_MISC_NUM_2,dl_list_nobrk(i).LM_PICKING_RESEND_TRAN,dl_list_nobrk(i).LM_PICKING_REQ_SAM_REPLY,dl_list_nobrk(i).LM_PICKING_TEAM_STD_GRP
				  ,SYSDATE,1,1,1,SYSTIMESTAMP,v_tran_nbr,dl_list_nobrk(i).LM_PICKING_WHSE,v_labor_msg_id,lpad(v_task_number,4,'0'),dl_list_nobrk(i).LM_PICKING_WHSE_DATE,
				  dl_list_nobrk(i).LM_PICKING_ACTUAL_END_DATE,dl_list_nobrk(i).LM_PICKING_ACTUAL_END_TIME,50
				 );

		  END IF;

		-- insert into details table
		INSERT INTO LABOR_MSG_DTL
			 (ITEM_NAME, ITEM_BAR_CODE,SEQ_NBR,HNDL_ATTR,QTY_PER_GRAB,DSP_LOCN,LOCN_CLASS,QTY,TC_ILPN_ID,PALLET_ID,LOCN_SLOT_TYPE,LOCN_X_COORD,
			  LOCN_Y_COORD,LOCN_Z_COORD,LOCN_TRAV_AISLE,LOCN_TRAV_ZONE,INNERPACK_QTY,PACK_QTY,CASE_QTY,TIER_QTY,PALLET_QTY,CO,DIV,
			  MISC,MISC_2,WEIGHT,VOLUME,START_DATE_TIME,HANDLING_UOM,SEASON,SEASON_YR,
			  STYLE,STYLE_SFX,COLOR,COLOR_SFX,SEC_DIM,QUAL,SIZE_RNGE_CODE,SIZE_REL_POSN_IN_TABLE,TC_OLPN_ID,MISC_NUM_2,LOADED,PUTAWAY_ZONE,
			  PICK_DETERMINATION_ZONE,WORK_GROUP,WORK_AREA,PULL_ZONE,ASSIGNMENT_ZONE,
			  LAST_UPDATED_DTTM,HIBERNATE_VERSION,LAST_UPDATED_SOURCE_TYPE,CREATED_SOURCE_TYPE,CREATED_DTTM,TRAN_NBR,LABOR_MSG_ID,LABOR_MSG_DTL_ID,
			  MSG_STAT_CODE,TRAN_SEQ_NBR,MISC_NUM_1)
		  VALUES ( dl_list_nobrk(i).LMD_PICK_ITEM_BAR_CODE,dl_list_nobrk(i).LMD_PICK_ITEM_BAR_CODE,dl_list_nobrk(i).LMD_PICK_MISC, 
          dl_list_nobrk(i).LMD_PICK_HNDL_ATTR, dl_list_nobrk(i).LMD_PICK_QTY_PER_GRAB, dl_list_nobrk(i).LMD_PICK_DSP_LOCN,
					dl_list_nobrk(i).LMD_PICK_LOCN_CLASS, dl_list_nobrk(i).LMD_PICK_QTY, dl_list_nobrk(i).LMD_PICK_TC_ILPN_ID, TO_CHAR(dl_list_nobrk(i).LMD_PICK_PALLET_ID),
					dl_list_nobrk(i).LMD_PICK_LOCN_SLOT_TYPE,
					dl_list_nobrk(i).LMD_PICK_LOCN_X_COORD, dl_list_nobrk(i).LMD_PICK_LOCN_Y_COORD, dl_list_nobrk(i).LMD_PICK_LOCN_Z_COORD, dl_list_nobrk(i).LMD_PICK_LOCN_TRAV_AISLE,
					dl_list_nobrk(i).LMD_PICK_LOCN_TRAV_ZONE, dl_list_nobrk(i).LMD_PICK_INNERPACK_QTY,dl_list_nobrk(i).LMD_PICK_PACK_QTY,
					dl_list_nobrk(i).LMD_PICK_CASE_QTY, dl_list_nobrk(i).LMD_PICK_TIER_QTY, dl_list_nobrk(i).LMD_PICK_PALLET_QTY, dl_list_nobrk(i).LMD_PICK_CO, dl_list_nobrk(i).LMD_PICK_DIV,
					dl_list_nobrk(i).PICKING_trailer_posn,--dl_list(i).LMD_PICK_MISC, 
          dl_list_nobrk(i).PICKING_contnr_type,dl_list_nobrk(i).LMD_PICK_WEIGHT, dl_list_nobrk(i).LMD_PICK_VOLUME, dl_list_nobrk(i).LMD_PICK_START_DATE_TIME,
					dl_list_nobrk(i).LMD_PICK_HANDLING_UOM, dl_list_nobrk(i).LMD_PICK_SEASON,dl_list_nobrk(i).LMD_PICK_SEASON_YR, dl_list_nobrk(i).LMD_PICK_STYLE, dl_list_nobrk(i).LMD_PICK_STYLE_SFX,
					dl_list_nobrk(i).LMD_PICK_COLOR, dl_list_nobrk(i).LMD_PICK_COLOR_SFX,dl_list_nobrk(i).LMD_PICK_SEC_DIM, dl_list_nobrk(i).LMD_PICK_QUAL, dl_list_nobrk(i).LMD_PICK_SIZE_RNGE_CODE,
					dl_list_nobrk(i).LMD_PICK_SIZERELPOSNINTAB,
          'LPN' || lpad(v_lpncount, 7, '0'),
          dl_list_nobrk(i).LMD_PICK_MISC_NUM_2, dl_list_nobrk(i).LMD_PICK_LOADED,
					dl_list_nobrk(i).LMD_PICK_PUTAWAY_ZONE,
					dl_list_nobrk(i).LMD_PICK_DETZONE, dl_list_nobrk(i).LMD_PICK_WORK_GROUP, dl_list_nobrk(i).LMD_PICK_WORK_AREA, dl_list_nobrk(i).LMD_PICK_PULL_ZONE,
					dl_list_nobrk(i).LMD_PICK_ASSIGNMENT_ZONE,SYSDATE,1,1,1,SYSTIMESTAMP,v_tran_nbr,v_labor_msg_id,labor_msg_dtl_id_seq.nextval,'01',
					v_tran_seq_nbr,dl_list_nobrk(i).PICKING_nbr_of_contnrs_syscode);


			v_tmp_trlr_posn_prev := dl_list_nobrk(i).PICKING_trailer_posn;
			v_tmp_stop_number_prev := dl_list_nobrk(i).PICKING_stop_number;
			v_tmp_product_type_prev := substr(dl_list_nobrk(i).PICKING_product_type, 1, 1);
			v_tmp_cntnr_type_prev := dl_list_nobrk(i).PICKING_contnr_type;
	   end if;
     
           v_tran_seq_nbr := v_tran_seq_nbr + 1;
     END LOOP; -- END LOOP THROUGH EVENT LINES
       ---- RESEQUENCE : Resequence the final one
      update labor_msg_dtl l
        set tran_seq_nbr = (select new_seq from (select tran_seq_nbr, size_rnge_code, tran_nbr,
        dense_rank() over (partition by tran_nbr order by size_rnge_code, labor_msg_dtl_id ) new_seq
        from labor_msg_dtl where tran_nbr = v_tran_nbr) where tran_seq_nbr = l.tran_seq_nbr)
        where tran_nbr = v_tran_nbr;

    END IF; --  END THE BREAK BY STOP 
    END LOOP; -- END THE LOOP OF PRODUCT TYPES
	COMMIT;
--------  End for PICKING   -----------------------

--------  Start for LOADING -----------------------
   v_task_number := 1;
   v_tempcount := 1;
   v_labor_msg_id  :=0;
   v_tran_nbr :=0;
   v_endTranNbr :=0;
   v_nbrRecCnts :=0;
   v_tran_seq_nbr := 1;
   v_tmp_trlr_posn_prev := NULL;
   v_tmp_stop_number_prev :=0;
   v_tmp_product_type_prev :=NULL;
   v_tmp_cntnr_type_prev :=NULL;
   v_currcontn_type :=NULL;
   v_brkbystop := 'N';
   v_Lbr_Msg_Rows :=0;
   v_lpncount := 1;
	-- fetch from the cursor to bulk collect into a list
		OPEN c_plnevnt_cur_Loading;
		FETCH c_plnevnt_cur_Loading BULK COLLECT INTO dl_list_loading;
		CLOSE c_plnevnt_cur_Loading;

		v_nbrRecCnts := dl_list_loading.Count;

	--  SELECT CURR_NBR INTO v_tran_nbr FROM nxt_up_cnt WHERE rec_type_id = 'WI1';
		MANH_GET_NXTUP('*','WI1',v_nbrRecCnts,P_RC,v_tran_nbr,v_endTranNbr);
		v_tran_nbr := v_tran_nbr + 1;
	-- loop through all details
    v_lpn_changed := 'N';
    v_lpncount := 1;
		FOR i IN 1 .. dl_list_loading.COUNT
		LOOP
		  -- Check if not first row from list , check prev with current column values
		  -- insert once for the first row to the header labor_msg table
		  IF i = 1 Then

			 -- select labor_msg_id_seq.nextval into a variable for multithreading
			SELECT LABOR_MSG_ID_SEQ.NEXTVAL INTO v_labor_msg_id FROM dual;

		   INSERT INTO LABOR_MSG
				(ACT_NAME,LOGIN_USER_ID,SCHED_START_TIME,SCHED_START_DATE,VHCL_TYPE,MOD_USER_ID,REF_CODE,REF_NBR,
				 DEPT,DIV,SHIFT,USER_DEF_VAL_1,MISC_TXT_2,MISC_NUM_1,MISC_NUM_2,RESEND_TRAN,REQ_SAM_REPLY,TEAM_STD_GRP,
				 LAST_UPDATED_DTTM,HIBERNATE_VERSION,LAST_UPDATED_SOURCE_TYPE,CREATED_SOURCE_TYPE,CREATED_DTTM,TRAN_NBR,WHSE,LABOR_MSG_ID,MISC,
				 WHSE_DATE,ACTUAL_END_DATE,ACTUAL_END_TIME,msg_stat_code)
				VALUES
				( dl_list_loading(i).LM_LOAD_ACT_NAME,dl_list_loading(i).LM_LOAD_MOD_USER_ID,dl_list_loading(i).LM_LOAD_SCHED_START_TIME,
				  dl_list_loading(i).LM_LOAD_SCHED_START_DATE,dl_list_loading(i).LM_LOAD_VHCL_TYPE,dl_list_loading(i).LM_LOAD_MOD_USER_ID,dl_list_loading(i).LM_LOAD_REF_CODE,
				  dl_list_loading(i).LM_LOAD_REF_NBR,dl_list_loading(i).LM_LOAD_DEPT,dl_list_loading(i).LM_LOAD_DIV,dl_list_loading(i).LM_LOAD_SHIFT,dl_list_loading(i).LM_LOAD_USER_DEF_VAL_1,
				  dl_list_loading(i).LM_LOAD_MISC_TXT_2,dl_list_loading(i).LM_LOAD_MISC_NUM_1,dl_list_loading(i).LM_LOAD_MISC_NUM_2,dl_list_loading(i).LM_LOAD_RESEND_TRAN,
				  dl_list_loading(i).LM_LOAD_REQ_SAM_REPLY,dl_list_loading(i).LM_LOAD_TEAM_STD_GRP,SYSDATE,1,1,1,SYSTIMESTAMP,v_tran_nbr, dl_list_loading(i).LM_LOAD_WHSE ,v_labor_msg_id,
				  lpad(v_task_number,4,'0'),dl_list_loading(i).LM_LOAD_WHSE_DATE,dl_list_loading(i).LM_LOAD_ACTUAL_END_DATE,
          dl_list_loading(i).LM_LOAD_ACTUAL_END_TIME,50
				);

		  -- insert into details table
		INSERT INTO LABOR_MSG_DTL
			 (ITEM_BAR_CODE,ITEM_NAME,SEQ_NBR,HNDL_ATTR,QTY_PER_GRAB,DSP_LOCN,LOCN_CLASS,QTY,TC_ILPN_ID,PALLET_ID,LOCN_SLOT_TYPE,LOCN_X_COORD,
			  LOCN_Y_COORD,LOCN_Z_COORD,LOCN_TRAV_AISLE,LOCN_TRAV_ZONE,INNERPACK_QTY,PACK_QTY,CASE_QTY,TIER_QTY,PALLET_QTY,CO,DIV,
			  MISC,MISC_2,WEIGHT,VOLUME,START_DATE_TIME,HANDLING_UOM,SEASON,SEASON_YR,
			  STYLE,STYLE_SFX,COLOR,COLOR_SFX,SEC_DIM,QUAL,SIZE_RNGE_CODE,SIZE_REL_POSN_IN_TABLE,TC_OLPN_ID,MISC_NUM_2,LOADED,PUTAWAY_ZONE,
			  PICK_DETERMINATION_ZONE,WORK_GROUP,WORK_AREA,PULL_ZONE,ASSIGNMENT_ZONE,
			  LAST_UPDATED_DTTM,HIBERNATE_VERSION,LAST_UPDATED_SOURCE_TYPE,CREATED_SOURCE_TYPE,CREATED_DTTM,TRAN_NBR,LABOR_MSG_ID,LABOR_MSG_DTL_ID,
			  MSG_STAT_CODE,TRAN_SEQ_NBR,MISC_NUM_1)
		  VALUES (  dl_list_loading(i).LMD_LOAD_ITEM_BAR_CODE,dl_list_loading(i).LMD_LOAD_ITEM_BAR_CODE,'', dl_list_loading(i).LMD_LOAD_HNDL_ATTR, dl_list_loading(i).LMD_LOAD_QTY_PER_GRAB, dl_list_loading(i).LMD_LOAD_DSP_LOCN,
					dl_list_loading(i).LMD_LOAD_LOCN_CLASS, dl_list_loading(i).LMD_LOAD_QTY, dl_list_loading(i).LMD_LOAD_TC_ILPN_ID, TO_CHAR(dl_list_loading(i).LMD_LOAD_PALLET_ID), dl_list_loading(i).LMD_LOAD_LOCN_SLOT_TYPE,
					dl_list_loading(i).LMD_LOAD_LOCN_X_COORD, dl_list_loading(i).LMD_LOAD_LOCN_Y_COORD, dl_list_loading(i).LMD_LOAD_LOCN_Z_COORD, dl_list_loading(i).LMD_LOAD_LOCN_TRAV_AISLE,
					dl_list_loading(i).LMD_LOAD_LOCN_TRAV_ZONE, dl_list_loading(i).LMD_LOAD_INNERPACK_QTY,dl_list_loading(i).LMD_LOAD_PACK_QTY,
					dl_list_loading(i).LMD_LOAD_CASE_QTY, dl_list_loading(i).LMD_LOAD_TIER_QTY, dl_list_loading(i).LMD_LOAD_PALLET_QTY, dl_list_loading(i).LMD_LOAD_CO, dl_list_loading(i).LMD_LOAD_DIV,
					dl_list_loading(i).LMD_LOAD_MISC,dl_list_loading(i).LOAD_contnr_type,dl_list_loading(i).LMD_LOAD_WEIGHT, dl_list_loading(i).LMD_LOAD_VOLUME, dl_list_loading(i).LMD_LOAD_START_DATE_TIME,
					dl_list_loading(i).LMD_LOAD_HANDLING_UOM, dl_list_loading(i).LMD_LOAD_SEASON,dl_list_loading(i).LMD_LOAD_SEASON_YR, dl_list_loading(i).LMD_LOAD_STYLE,
					dl_list_loading(i).LMD_LOAD_STYLE_SFX,dl_list_loading(i).LMD_LOAD_COLOR, dl_list_loading(i).LMD_LOAD_COLOR_SFX,dl_list_loading(i).LMD_LOAD_SEC_DIM,
					dl_list_loading(i).LMD_LOAD_QUAL, dl_list_loading(i).LMD_LOAD_SIZE_RNGE_CODE,dl_list_loading(i).LMD_LOAD_SIZERELPOSNINTAB,
          'LPN' || lpad(v_lpncount, 7, '0'),
					dl_list_loading(i).LMD_LOAD_MISC_NUM_2, dl_list_loading(i).LMD_LOAD_LOADED, dl_list_loading(i).LMD_LOAD_PUTAWAY_ZONE,
					dl_list_loading(i).LMD_LOAD_PICK_DETZONE, dl_list_loading(i).LMD_LOAD_WORK_GROUP, dl_list_loading(i).LMD_LOAD_WORK_AREA,
					dl_list_loading(i).LMD_LOAD_PULL_ZONE, dl_list_loading(i).LMD_LOAD_ASSIGNMENT_ZONE,SYSDATE,1,1,1,SYSTIMESTAMP,v_tran_nbr,
					v_labor_msg_id,labor_msg_dtl_id_seq.nextval,'01',v_tran_seq_nbr,dl_list_loading(i).LOAD_contnr_number);
          v_tmp_trlr_posn_prev := dl_list_loading(i).LOAD_trailer_posn;
    			v_tmp_stop_number_prev := dl_list_loading(i).LOAD_stop_number;
    			v_tmp_product_type_prev := substr(dl_list_loading(i).LOAD_product_type, 1, 1);
    			v_tmp_cntnr_type_prev := dl_list_loading(i).LOAD_contnr_type;

		  Else
		  -- check if trailer position changes , count number of container confgiured in sys_code
      IF  ( NOT( 
                substr(dl_list_loading(i).LOAD_product_type, 1,1 ) = 'B' 
                    AND v_tmp_product_type_prev = 'B' 
                    AND trim(dl_list_loading(i).LOAD_stop_number) = trim(v_tmp_stop_number_prev)) --Buns same stop
            AND
                ((dl_list_loading(i).LOAD_trailer_posn <> v_tmp_trlr_posn_prev )
                OR  
                (substr(dl_list_loading(i).LOAD_product_type, 1,1 )!=v_tmp_product_type_prev )
              )
            )
      THEN
      	 v_lpncount := v_lpncount+1;
         v_lpn_changed := 'Y';
		  END IF;

		 IF (v_lpn_changed = 'Y' )
		  THEN
         v_lpn_changed := 'N';
			   v_task_number := v_task_number + 1;
			   v_tran_nbr := v_tran_nbr + 1;
			   v_tran_seq_nbr :=  1;

			 -- select labor_msg_id_seq.nextval into a variable for multithreading
			 SELECT LABOR_MSG_ID_SEQ.NEXTVAL INTO v_labor_msg_id FROM dual;

				INSERT INTO LABOR_MSG
				(ACT_NAME,LOGIN_USER_ID,SCHED_START_TIME,SCHED_START_DATE,VHCL_TYPE,MOD_USER_ID,REF_CODE,REF_NBR,
				 DEPT,DIV,SHIFT,USER_DEF_VAL_1,MISC_TXT_2,MISC_NUM_1,MISC_NUM_2,RESEND_TRAN,REQ_SAM_REPLY,TEAM_STD_GRP,
				 LAST_UPDATED_DTTM,HIBERNATE_VERSION,LAST_UPDATED_SOURCE_TYPE,CREATED_SOURCE_TYPE,CREATED_DTTM,TRAN_NBR,WHSE,LABOR_MSG_ID,MISC,
				 WHSE_DATE,ACTUAL_END_DATE,ACTUAL_END_TIME,msg_stat_code)
					VALUES
				( dl_list_loading(i).LM_LOAD_ACT_NAME,dl_list_loading(i).LM_LOAD_MOD_USER_ID,dl_list_loading(i).LM_LOAD_SCHED_START_TIME,dl_list_loading(i).LM_LOAD_SCHED_START_DATE,
				  dl_list_loading(i).LM_LOAD_VHCL_TYPE,dl_list_loading(i).LM_LOAD_MOD_USER_ID,dl_list_loading(i).LM_LOAD_REF_CODE,dl_list_loading(i).LM_LOAD_REF_NBR,
				  dl_list_loading(i).LM_LOAD_DEPT,dl_list_loading(i).LM_LOAD_DIV,dl_list_loading(i).LM_LOAD_SHIFT,dl_list_loading(i).LM_LOAD_USER_DEF_VAL_1,dl_list_loading(i).LM_LOAD_MISC_TXT_2,
				  dl_list_loading(i).LM_LOAD_MISC_NUM_1,dl_list_loading(i).LM_LOAD_MISC_NUM_2,dl_list_loading(i).LM_LOAD_RESEND_TRAN,dl_list_loading(i).LM_LOAD_REQ_SAM_REPLY,dl_list_loading(i).LM_LOAD_TEAM_STD_GRP
				  ,SYSDATE,1,1,1,SYSTIMESTAMP,v_tran_nbr,dl_list_loading(i).LM_LOAD_WHSE,v_labor_msg_id,lpad(v_task_number,4,'0'),dl_list_loading(i).LM_LOAD_WHSE_DATE,dl_list_loading(i).LM_LOAD_ACTUAL_END_DATE,
				  dl_list_loading(i).LM_LOAD_ACTUAL_END_TIME,50);

		  END IF;

		  -- insert into details table
		INSERT INTO LABOR_MSG_DTL
			 (ITEM_BAR_CODE,ITEM_NAME,SEQ_NBR,HNDL_ATTR,QTY_PER_GRAB,DSP_LOCN,LOCN_CLASS,QTY,TC_ILPN_ID,PALLET_ID,LOCN_SLOT_TYPE,LOCN_X_COORD,
			  LOCN_Y_COORD,LOCN_Z_COORD,LOCN_TRAV_AISLE,LOCN_TRAV_ZONE,INNERPACK_QTY,PACK_QTY,CASE_QTY,TIER_QTY,PALLET_QTY,CO,DIV,
			  MISC,MISC_2,WEIGHT,VOLUME,START_DATE_TIME,HANDLING_UOM,SEASON,SEASON_YR,
			  STYLE,STYLE_SFX,COLOR,COLOR_SFX,SEC_DIM,QUAL,SIZE_RNGE_CODE,SIZE_REL_POSN_IN_TABLE,TC_OLPN_ID,MISC_NUM_2,LOADED,PUTAWAY_ZONE,
			  PICK_DETERMINATION_ZONE,WORK_GROUP,WORK_AREA,PULL_ZONE,ASSIGNMENT_ZONE,
			  LAST_UPDATED_DTTM,HIBERNATE_VERSION,LAST_UPDATED_SOURCE_TYPE,CREATED_SOURCE_TYPE,CREATED_DTTM,TRAN_NBR,LABOR_MSG_ID,LABOR_MSG_DTL_ID,
			  MSG_STAT_CODE,TRAN_SEQ_NBR,MISC_NUM_1)
		  VALUES (  dl_list_loading(i).LMD_LOAD_ITEM_BAR_CODE,dl_list_loading(i).LMD_LOAD_ITEM_BAR_CODE,'', dl_list_loading(i).LMD_LOAD_HNDL_ATTR, dl_list_loading(i).LMD_LOAD_QTY_PER_GRAB, dl_list_loading(i).LMD_LOAD_DSP_LOCN,
					dl_list_loading(i).LMD_LOAD_LOCN_CLASS, dl_list_loading(i).LMD_LOAD_QTY, dl_list_loading(i).LMD_LOAD_TC_ILPN_ID, TO_CHAR(dl_list_loading(i).LMD_LOAD_PALLET_ID), dl_list_loading(i).LMD_LOAD_LOCN_SLOT_TYPE,
					dl_list_loading(i).LMD_LOAD_LOCN_X_COORD, dl_list_loading(i).LMD_LOAD_LOCN_Y_COORD, dl_list_loading(i).LMD_LOAD_LOCN_Z_COORD, dl_list_loading(i).LMD_LOAD_LOCN_TRAV_AISLE,
					dl_list_loading(i).LMD_LOAD_LOCN_TRAV_ZONE, dl_list_loading(i).LMD_LOAD_INNERPACK_QTY,dl_list_loading(i).LMD_LOAD_PACK_QTY,
					dl_list_loading(i).LMD_LOAD_CASE_QTY, dl_list_loading(i).LMD_LOAD_TIER_QTY, dl_list_loading(i).LMD_LOAD_PALLET_QTY, dl_list_loading(i).LMD_LOAD_CO, dl_list_loading(i).LMD_LOAD_DIV,
					dl_list_loading(i).LMD_LOAD_MISC,dl_list_loading(i).LOAD_contnr_type,dl_list_loading(i).LMD_LOAD_WEIGHT, dl_list_loading(i).LMD_LOAD_VOLUME, dl_list_loading(i).LMD_LOAD_START_DATE_TIME,
					dl_list_loading(i).LMD_LOAD_HANDLING_UOM, dl_list_loading(i).LMD_LOAD_SEASON,dl_list_loading(i).LMD_LOAD_SEASON_YR, dl_list_loading(i).LMD_LOAD_STYLE,
					dl_list_loading(i).LMD_LOAD_STYLE_SFX,dl_list_loading(i).LMD_LOAD_COLOR, dl_list_loading(i).LMD_LOAD_COLOR_SFX,dl_list_loading(i).LMD_LOAD_SEC_DIM,
					dl_list_loading(i).LMD_LOAD_QUAL, dl_list_loading(i).LMD_LOAD_SIZE_RNGE_CODE,dl_list_loading(i).LMD_LOAD_SIZERELPOSNINTAB,
          'LPN' || lpad(v_lpncount, 7, '0'),
					dl_list_loading(i).LMD_LOAD_MISC_NUM_2, dl_list_loading(i).LMD_LOAD_LOADED, dl_list_loading(i).LMD_LOAD_PUTAWAY_ZONE,
					dl_list_loading(i).LMD_LOAD_PICK_DETZONE, dl_list_loading(i).LMD_LOAD_WORK_GROUP, dl_list_loading(i).LMD_LOAD_WORK_AREA,
					dl_list_loading(i).LMD_LOAD_PULL_ZONE, dl_list_loading(i).LMD_LOAD_ASSIGNMENT_ZONE,SYSDATE,1,1,1,SYSTIMESTAMP,v_tran_nbr,
					v_labor_msg_id,labor_msg_dtl_id_seq.nextval,'01',v_tran_seq_nbr,dl_list_loading(i).LOAD_contnr_number);

			v_tmp_trlr_posn_prev := dl_list_loading(i).LOAD_trailer_posn;
			v_tmp_stop_number_prev := dl_list_loading(i).LOAD_stop_number;
			v_tmp_product_type_prev := substr(dl_list_loading(i).LOAD_product_type, 1, 1);
			v_tmp_cntnr_type_prev := dl_list_loading(i).LOAD_contnr_type;
     end if;
        v_tran_seq_nbr := v_tran_seq_nbr + 1;
	   END LOOP;

	COMMIT;
--------  End for LOADING   -----------------------
EXCEPTION
   WHEN OTHERS THEN
      raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
     p_rc := -1;
end c_wl02_gen_planning_events;
/
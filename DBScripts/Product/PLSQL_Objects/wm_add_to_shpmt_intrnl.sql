create or replace procedure wm_add_to_shpmt_intrnl
(
    p_user_id                   in user_profile.user_id%type,
    p_tc_company_id             in shipment.tc_company_id%type,
    p_static_route_id           in static_route.static_route_id%type,
    p_lpn_id                    in lpn.lpn_id%type,
    p_order_id                  in orders.order_id%type,
    p_order_split_id            in order_split.order_split_id%type,
    p_cons_run_id               in cons_run.cons_run_id%type,
    p_load_split_data           in number,
    p_caller_id           in varchar2,
    p_act_trk_tran_nbr    in prod_trkg_tran.tran_nbr%type,
    p_act_trk_rec_count   in out number
)
as
    v_row_count           number(9) := 0;
    v_shipment_ref_code   varchar2(2) := '41';
    v_order_ref_code      varchar2(2) := '01';
    v_lpn_ref_code        varchar2(2) := '02';
    v_ship_via_ref_code   varchar2(2) := '40';
    v_rte_ref_code        varchar2(2) := '99';
    type t_lpn_id is      table of lpn.lpn_id%type index by binary_integer;
    va_lpn_id             t_lpn_id;
    v_shipment_id         shipment.shipment_id%type;
    v_tc_shipment_id      shipment.tc_shipment_id%type;
    v_assigned_ship_via   shipment.assigned_ship_via%type;
begin
    delete from tmp_order_splits_scratch t
    where exists
    (
        select 1
        from order_split os
        where os.order_split_id = t.order_split_id and os.shipment_id != t.shipment_id
    );
    v_row_count := sql%rowcount;
    wm_cs_log('Removed splits planned on different shpmts ' || v_row_count,
        p_sql_log_level => case when v_row_count > 0 then 0 else 1 end);

    delete from tmp_order_splits_scratch t
    where t.order_split_id is null
        and exists
        (
            select 1
            from orders o
            where o.order_id = t.order_id and o.shipment_id != t.shipment_id
        );
    v_row_count := sql%rowcount;
    wm_cs_log('Removed orders planned on different shpmts ' || v_row_count,
        p_sql_log_level => case when v_row_count > 0 then 0 else 1 end);

    delete from tmp_order_splits_scratch t
    where exists
    (
        select 1
        from lpn l
        where l.order_id = t.order_id and l.lpn_facility_status < 99
            and coalesce(l.order_split_id, -1) = coalesce(t.order_split_id, -1)
            and l.shipment_id != t.shipment_id
            and l.static_route_id is null
    );
    v_row_count := sql%rowcount;
    wm_cs_log('Removed orders/splits tied to lpns assigned to different shpmts ' || v_row_count,
        p_sql_log_level => case when v_row_count > 0 then 0 else 1 end);

    -- origin stops for shpmt, facility
    insert into stop
    (
        shipment_id, stop_seq, tc_company_id, facility_id, facility_alias_id, 
        stop_location_name, contact_first_name, contact_surname, contact_phone_number, 
        address_1, address_2, address_3, city, state_prov, county, postal_code, 
        country_code, stop_tz, stop_status
    )
    with giv as
    (
        select t.shipment_id, o.o_facility_id facility_id
        from tmp_order_splits_scratch t
        join orders o on o.order_id = t.order_id
        union
        select t.om_shipment_id shipment_id,
            coalesce(o.plan_d_facility_id, o.zone_skip_hub_location_id) facility_id
        from tmp_order_splits_scratch t
        join orders o on o.order_id = t.order_id
        where t.om_shipment_id is not null
            and coalesce(o.plan_d_facility_id, o.zone_skip_hub_location_id) is not null
    ) 
    select giv.shipment_id, 1, p_tc_company_id, f.facility_id, fa.facility_alias_id,
        fa.facility_name, fc.first_name, fc.surname, fc.telephone_number,
        f.address_1, f.address_2, f.address_3, f.city, f.state_prov, f.county, f.postal_code,
        f.country_code, f.facility_tz, 10 stop_status
    from giv
    join facility f on f.facility_id = giv.facility_id 
    left join facility_alias fa on fa.facility_id = f.facility_id and fa.is_primary = 1
    left join facility_contact fc on fc.facility_id = f.facility_id
    where not exists 
    (
         select 1 
         from stop st
         where st.shipment_id = giv.shipment_id and st.stop_seq = 1
            and st.facility_id = f.facility_id
    );
    wm_cs_log('Created origin stops ' || sql%rowcount);

    -- destination stops
    insert into stop
    (
        shipment_id, stop_seq, tc_company_id, facility_id, facility_alias_id,
        stop_location_name, contact_first_name, contact_surname, contact_phone_number, 
        address_1, address_2, address_3, city, state_prov, postal_code, county, 
        country_code, stop_tz, stop_status, rte_id
    )
    with giv as 
    (
        select t.shipment_id, f.facility_id, t.static_route_id,
            coalesce(f.facility_tz, o.delivery_tz, 3) stop_tz,
            decode(f.facility_id, null, o.d_address_1, f.address_1) address_1,
            decode(f.facility_id, null, o.d_address_2, f.address_2)address_2,
            decode(f.facility_id, null, o.d_address_3, f.address_3) address_3,
            decode(f.facility_id, null, o.d_city, f.city) city,
            decode(f.facility_id, null, o.d_state_prov, f.state_prov) state_prov,
            decode(f.facility_id, null, o.d_county, f.county) county,
            decode(f.facility_id, null, o.d_postal_code, f.postal_code) postal_code,
            decode(f.facility_id, null, o.d_country_code, f.country_code) country_code
        from tmp_order_splits_scratch t 
        join orders o on o.order_id = t.order_id
        left join facility f on f.facility_id
            = coalesce(o.plan_d_facility_id, o.zone_skip_hub_location_id, o.d_facility_id)
        union
        select t.om_shipment_id shipment_id, f.facility_id, null static_route_id,
            coalesce(f.facility_tz, o.delivery_tz, 3) stop_tz, 
            decode(f.facility_id, null, o.d_address_1, f.address_1) address_1,
            decode(f.facility_id, null, o.d_address_2, f.address_2)address_2,
            decode(f.facility_id, null, o.d_address_3, f.address_3) address_3,
            decode(f.facility_id, null, o.d_city, f.city) city,
            decode(f.facility_id, null, o.d_state_prov, f.state_prov) state_prov,
            decode(f.facility_id, null, o.d_county, f.county) county,
            decode(f.facility_id, null, o.d_postal_code, f.postal_code) postal_code,
            decode(f.facility_id, null, o.d_country_code, f.country_code) country_code
        from tmp_order_splits_scratch t 
        join orders o on o.order_id = t.order_id
        left join facility f on f.facility_id = o.d_facility_id
        where t.om_shipment_id is not null 
            and coalesce(o.plan_d_facility_id, o.zone_skip_hub_location_id,-1) > 0
    )
    select
        giv.shipment_id, (select coalesce(max(st.stop_seq),0) from stop st where st.shipment_id = giv.shipment_id)
         + row_number() over(partition by giv.shipment_id order by giv.facility_id) stop_seq,
        p_tc_company_id, giv.facility_id, fa.facility_alias_id, fa.facility_name,
        fc.first_name, fc.surname, fc.telephone_number, giv.address_1, 
        giv.address_2, giv.address_3, giv.city, giv.state_prov, giv.postal_code,
        giv.county, giv.country_code, giv.stop_tz, 10 stop_status, sr.route_alias_id
    from giv
    left join facility_alias fa on fa.facility_id = giv.facility_id and fa.is_primary = 1
    left join facility_contact fc on fc.facility_id = giv.facility_id
    left join static_route sr on sr.static_route_id = giv.static_route_id 
    where not exists 
    (
        select 1 
        from stop s
        where s.shipment_id = giv.shipment_id
            and coalesce(s.facility_id, -1) = coalesce(giv.facility_id, -1)
            and coalesce(s.address_1,'?') = coalesce(giv.address_1, '?')
            and coalesce(s.address_2,'?') = coalesce(giv.address_2, '?')
            and coalesce(s.address_3,'?') = coalesce(giv.address_3, '?')
            and coalesce(s.city,'?') = coalesce(giv.city, '?')
            and coalesce(s.state_prov,'?') = coalesce(giv.state_prov, '?')
            and coalesce(s.postal_code,'?') = coalesce(giv.postal_code, '?')
            and coalesce(s.county,'?') = coalesce(giv.county, '?')
            and coalesce(s.country_code,'?') = coalesce(giv.country_code, '?')
    );
    wm_cs_log('Created destination stops ' || sql%rowcount);
  
    insert into stop_action 
    (
        shipment_id, stop_seq, stop_action_seq, action_type
    )
    select st.shipment_id, st.stop_seq, 1, case when st.stop_seq = 1 then 'PU' else 'DL' end
    from stop st
    join tmp_order_splits_scratch t on t.shipment_id = st.shipment_id
    where not exists
    (
        select 1
        from stop_action sa
        where sa.shipment_id = st.shipment_id and sa.stop_seq = st.stop_seq
    )
    group by st.shipment_id, st.stop_seq;
    wm_cs_log('Created stop_action for shpmt ' || sql%rowcount);

    insert into stop_action 
    (
        shipment_id, stop_seq, stop_action_seq, action_type
    )
    select st.shipment_id, st.stop_seq, 1, case when st.stop_seq = 1 then 'PU' else 'DL' end
    from stop st
    join tmp_order_splits_scratch t on t.om_shipment_id = st.shipment_id
    where not exists
    (
        select 1
        from stop_action sa
        where sa.shipment_id = st.shipment_id and sa.stop_seq = st.stop_seq
    )
    group by st.shipment_id, st.stop_seq;
    wm_cs_log('Created stop_action rows for shpmt on 2nd leg ' || sql%rowcount);

    insert into stop_action_order 
    (
        stop_action_order_id, shipment_id, stop_seq, stop_action_seq, order_id, 
        order_split_id
    )
    with giv as
    (
        select t.order_id, t.order_split_id, t.shipment_id, 1 stop_seq
        from tmp_order_splits_scratch t
        union
        select t.order_id, t.order_split_id, t.om_shipment_id, 1 stop_seq
        from tmp_order_splits_scratch t 
        where t.om_shipment_id is not null
        union
        select t.order_id, t.order_split_id, t.shipment_id, st.stop_seq
        from tmp_order_splits_scratch t
        join orders o on t.order_id = o.order_id 
        left join static_route sr on sr.static_route_id = t.static_route_id
        join stop st on st.shipment_id = t.shipment_id
             and coalesce(st.rte_id,'?') = coalesce(sr.route_alias_id,'?')
             and (st.facility_id = coalesce(o.plan_d_facility_id, o.zone_skip_hub_location_id, o.d_facility_id)
             or ( st.facility_id is null 
             and coalesce(o.plan_d_facility_id, o.zone_skip_hub_location_id, o.d_facility_id, -1) = -1
             and coalesce(st.address_1,'?') = coalesce(o.d_address_1,'?')
             and coalesce(st.address_2,'?') = coalesce(o.d_address_2,'?')
             and coalesce(st.address_3,'?') = coalesce(o.d_address_3,'?')
             and coalesce(st.city,'?') = coalesce(o.d_city,'?')
             and coalesce(st.state_prov,'?') = coalesce(o.d_state_prov,'?')
             and coalesce(st.county,'?') = coalesce(o.d_county,'?')
             and coalesce(st.postal_code,'?') = coalesce(o.d_postal_code,'?')
             and coalesce(st.country_code,'?') = coalesce(o.d_country_code,'?')))
        where st.stop_seq > 1
        union
        select t.order_id, t.order_split_id, t.om_shipment_id, st.stop_seq
        from tmp_order_splits_scratch t
        join orders o on t.order_id = o.order_id 
        join stop st on st.shipment_id = t.om_shipment_id
             and coalesce(st.facility_id, -1) = coalesce(o.d_facility_id, -1)
             and coalesce(st.address_1,'?') = coalesce(o.d_address_1,'?')
             and coalesce(st.address_2,'?') = coalesce(o.d_address_2,'?')
             and coalesce(st.address_3,'?') = coalesce(o.d_address_3,'?')
             and coalesce(st.city,'?') = coalesce(o.d_city,'?')
             and coalesce(st.state_prov,'?') = coalesce(o.d_state_prov,'?')
             and coalesce(st.county,'?') = coalesce(o.d_county,'?')
             and coalesce(st.postal_code,'?') = coalesce(o.d_postal_code,'?')
             and coalesce(st.country_code,'?') = coalesce(o.d_country_code,'?')
        where t.om_shipment_id is not null and st.stop_seq > 1
            and coalesce(o.plan_d_facility_id, o.zone_skip_hub_location_id) is not null
    )
    select seq_stop_action_order_id.nextval, giv.shipment_id, giv.stop_seq,
        1, giv.order_id, giv.order_split_id
    from giv 
    join shipment s on s.shipment_id = giv.shipment_id and s.static_route_id is null
    where not exists
    (
        select 1
        from stop_action_order sao
        where sao.shipment_id = giv.shipment_id
            and sao.order_id = giv.order_id
            and coalesce(sao.order_split_id, -1) = coalesce(giv.order_split_id, -1)
            and sao.stop_seq = giv.stop_seq
    )
    and not exists
    (
        select 1
        from lpn l
        where l.order_id = giv.order_id and l.lpn_facility_status < 99
            and coalesce(l.order_split_id, -1) = coalesce(giv.order_split_id, -1)
            and l.static_route_id is not null
    );    
    wm_cs_log('Inserted SAO ' || sql%rowcount);

    -- this sql is currently not re-entrant if locking is used
    insert into order_movement
    (
        order_movement_id, order_id, tc_company_id, pickup_stop_seq, 
        delivery_stop_seq, movement_type, order_split_id, shipment_id
    )
    with giv as
    (
        select t.order_id, o.tc_company_id, t.order_split_id, 
            t.shipment_id, 1 pickup_stop_seq , st.stop_seq delivery_stop_seq,
            'OX' movement_type
        from tmp_order_splits_scratch t
        join orders o on t.order_id = o.order_id 
        join stop st on st.shipment_id = t.shipment_id
             and coalesce(st.facility_id, -1) = 
                 coalesce(o.plan_d_facility_id, o.zone_skip_hub_location_id,-1)
             and coalesce(st.address_1,'?') = coalesce(o.d_address_1,'?')
             and coalesce(st.address_2,'?') = coalesce(o.d_address_2,'?')
             and coalesce(st.address_3,'?') = coalesce(o.d_address_3,'?')
             and coalesce(st.city,'?') = coalesce(o.d_city,'?')
             and coalesce(st.state_prov,'?') = coalesce(o.d_state_prov,'?')
             and coalesce(st.county,'?') = coalesce(o.d_county,'?')
             and coalesce(st.postal_code,'?') = coalesce(o.d_postal_code,'?')
             and coalesce(st.country_code,'?') = coalesce(o.d_country_code,'?')
        where t.om_shipment_id is not null and st.stop_seq > 1
            and coalesce(o.plan_d_facility_id, o.zone_skip_hub_location_id) is not null
        union
        select t.order_id, o.tc_company_id, t.order_split_id, 
             t.om_shipment_id, 1 pickup_stop_seq , st.stop_seq delivery_stop_seq,
             'XD' movement_type
        from tmp_order_splits_scratch t
        join orders o on t.order_id = o.order_id 
        join stop st on st.shipment_id = t.om_shipment_id
             and coalesce(st.facility_id, -1) = coalesce(o.d_facility_id, -1)
             and coalesce(st.address_1,'?') = coalesce(o.d_address_1,'?')
             and coalesce(st.address_2,'?') = coalesce(o.d_address_2,'?')
             and coalesce(st.address_3,'?') = coalesce(o.d_address_3,'?')
             and coalesce(st.city,'?') = coalesce(o.d_city,'?')
             and coalesce(st.state_prov,'?') = coalesce(o.d_state_prov,'?')
             and coalesce(st.county,'?') = coalesce(o.d_county,'?')
             and coalesce(st.postal_code,'?') = coalesce(o.d_postal_code,'?')
             and coalesce(st.country_code,'?') = coalesce(o.d_country_code,'?')
        where t.om_shipment_id is not null and st.stop_seq > 1
            and coalesce(o.plan_d_facility_id, o.zone_skip_hub_location_id) is not null
    )   
    select seq_order_movement_id.nextval, giv.order_id, giv.tc_company_id, 
        giv.pickup_stop_seq, giv.delivery_stop_seq, giv.movement_type,
        giv.order_split_id, giv.shipment_id
    from giv;
    wm_cs_log('Inserted OM ' || sql%rowcount);

    if (p_load_split_data = 1)
    then
        select t.shipment_id, s.tc_shipment_id, s.assigned_ship_via
        into v_shipment_id, v_tc_shipment_id, v_assigned_ship_via
        from tmp_order_splits_scratch t
            join shipment s on s.shipment_id = t.shipment_id
            where rownum < 2;

        update lpn l
        set l.shipment_id = v_shipment_id, l.plan_load_id = v_shipment_id,
            l.tc_shipment_id = v_tc_shipment_id, l.ship_via = v_assigned_ship_via,
            l.last_updated_dttm = sysdate, l.last_updated_source = p_user_id
            where l.lpn_facility_status < 90
                and 
                (
                    case when p_static_route_id is not null or p_lpn_id is not null then
                        case when (l.version_nbr = p_cons_run_id
                            or (p_order_id is null and l.lpn_id = p_lpn_id)
                            or (l.parent_lpn_id = p_lpn_id and l.order_id = p_order_id 
                                and coalesce(l.order_split_id, -1) = coalesce(p_order_split_id, -1))) then 1
                        else 0
                        end
                    else 1
                    end
                ) = 1
        and exists
            (
                select 1
                from tmp_order_splits_scratch t 
                where t.order_id = l.order_id
                    and coalesce(t.order_split_id, -1) = coalesce(l.order_split_id, -1)
            )
            returning l.lpn_id
            bulk collect into va_lpn_id;
        wm_cs_log('Lpns updated ' || sql%rowcount);
    else
        -- otherwise use app populated data in tmp_lpn_list
        merge into lpn l
        using
        (
            select t.order_id, t.order_split_id, t.shipment_id, 
                s.tc_shipment_id, s.assigned_ship_via
            from tmp_order_splits_scratch t
            join shipment s on s.shipment_id = t.shipment_id
        ) iv on (iv.order_id = l.order_id 
            and coalesce(iv.order_split_id, -1) = coalesce(l.order_split_id, -1))
        when matched then
        update set l.shipment_id = iv.shipment_id, l.plan_load_id = iv.shipment_id,
            l.tc_shipment_id = iv.tc_shipment_id, l.ship_via = iv.assigned_ship_via,
            l.last_updated_dttm = sysdate, l.last_updated_source = p_user_id
        where exists
        (
            select 1
            from tmp_lpn_list t
            where t.lpn_id = l.lpn_id
        );
        wm_cs_log('Lpns on bypass flow updated ' || sql%rowcount);
if (p_act_trk_tran_nbr is not null)
        then
            select t.lpn_id
            bulk collect into va_lpn_id
            from tmp_lpn_list t;
        end if;
    end if;

    if (p_act_trk_tran_nbr is not null)
    then
        delete from tmp_act_trk_lpns;
        forall i in 1..va_lpn_id.count
        insert into tmp_act_trk_lpns t (lpn_id)
        values (va_lpn_id(i));

        insert into prod_trkg_tran
        (
            prod_trkg_tran_id, tran_type, tran_code, tran_nbr, seq_nbr, whse,
            menu_optn_name, user_id, cd_master_id, ref_code_id_1, ref_code_id_2, 
            ref_code_id_3, ref_field_1, ref_field_2, ref_field_3, mod_date_time,
            create_date_time, module_name, ref_code_id_4, ref_field_4,
            ref_code_id_5, ref_field_5, begin_date, end_date
        )
        select prod_trkg_tran_id_seq.nextval, '800', '012', p_act_trk_tran_nbr,
            p_act_trk_rec_count + rownum seq_nbr, f.whse, p_caller_id, p_user_id, 
            l.tc_company_id, v_shipment_ref_code, v_order_ref_code, v_lpn_ref_code,
            l.tc_shipment_id ref_field_1, l.tc_order_id ref_field_2, 
            l.tc_lpn_id ref_field_3, sysdate, sysdate, 'Shipping',
            v_ship_via_ref_code, l.ship_via, v_rte_ref_code, sr.route_alias_id, l.last_updated_dttm, sysdate
        from tmp_act_trk_lpns t
        join lpn l on l.lpn_id = t.lpn_id
        join facility f on f.facility_id = l.c_facility_id
        left join static_route sr on sr.static_route_id = l.static_route_id;
        p_act_trk_rec_count := p_act_trk_rec_count + sql%rowcount;
    end if;

    merge into order_split os
    using
    (
        select /*+ full(t) */ t.order_split_id, t.shipment_id, s.tc_shipment_id, 
            s.assigned_ship_via line_haul_ship_via, sh.assigned_ship_via distribution_ship_via, 
            s.assigned_carrier_id, s.assigned_service_level_id, s.assigned_mot_id,
            s.assigned_equipment_id
        from tmp_order_splits_scratch t
        join shipment s on s.shipment_id = t.shipment_id
        left join shipment sh on sh.shipment_id = t.om_shipment_id
        where t.order_split_id is not null
    ) iv on (os.order_split_id = iv.order_split_id)
    when matched then
    update set os.shipment_id = iv.shipment_id, os.order_split_status = 10,
        os.line_haul_ship_via = iv.line_haul_ship_via, 
        os.distribution_ship_via = iv.distribution_ship_via,
        os.assigned_carrier_id = iv.assigned_carrier_id, 
        os.assigned_service_level_id = iv.assigned_service_level_id,
        os.assigned_mot_id = iv.assigned_mot_id, 
        os.assigned_equipment_id = iv.assigned_equipment_id,
        os.last_updated_dttm = sysdate, os.last_updated_source = p_user_id;
    wm_cs_log('Splits updated ' || sql%rowcount);

    merge into orders o
    using
    ( 
        select /*+ full(t) */ t.order_id, t.shipment_id, s.tc_shipment_id, 
            s.assigned_ship_via line_haul_ship_via, 
            sh.assigned_ship_via distribution_ship_via, s.assigned_carrier_id, 
            s.assigned_service_level_id, s.assigned_mot_id, s.assigned_equipment_id
        from tmp_order_splits_scratch t
        join shipment s on s.shipment_id = t.shipment_id and s.static_route_id is null
        left join shipment sh on sh.shipment_id = t.om_shipment_id
        where t.order_split_id is null
    ) iv on (o.order_id = iv.order_id)
    when matched then
    update set o.shipment_id = iv.shipment_id, o.order_status = 10,
        o.tc_shipment_id = iv.tc_shipment_id,
        o.line_haul_ship_via = iv.line_haul_ship_via,
        o.distribution_ship_via = iv.distribution_ship_via,
        o.assigned_service_level_id = iv.assigned_service_level_id,
        o.assigned_carrier_id = iv.assigned_carrier_id,
        o.assigned_mot_id = iv.assigned_mot_id,
        o.assigned_equipment_id = iv.assigned_equipment_id,
        o.last_updated_dttm = sysdate, o.last_updated_source = p_user_id
    where o.has_split = 0 and o.order_status = 5
        and exists
        (
            select 1
            from stop_action_order sao
            where sao.shipment_id = iv.shipment_id
                and sao.order_id = iv.order_id
                and sao.order_split_id is null 
        );
    wm_cs_log('Orders updated ' || sql%rowcount);
end;
/

show errors;

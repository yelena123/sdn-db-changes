create or replace procedure delete_locns
(
  --  p_user_id user_profile.user_id%type,
   p_user_id VARCHAR2,
    p_whse whse_master.whse%type,
    p_cd_master_id cd_master.cd_master_id%type,
--    p_lang_id user_master.lang_id%type,
    p_lang_id VARCHAR2,
    p_timestamp date,
    p_locn_class in locn_hdr.locn_class%type,
    p_locn_parm_id locn_wiz_hdr.locn_parm_id%type,
    p_num_locns_deleted out number
)
is

--$Revision: 9$

type t_locnid is table of locn_hdr.locn_id%type index by binary_integer;
type t_dsplocn is table of locn_hdr.dsp_locn%type index by binary_integer;

vt_locnid t_locnid;
vt_dsplocn t_dsplocn;
v_count number(3);
v_count1 number(3);
v_count2 number(3);
v_count3 number(3);
v_count4 number(3);

v_msg msg_log.msg%type;
v_msg_log_id msg_log.msg_log_id%type;
v_locn_id locn_hdr.locn_id%type;
v_dsp_locn locn_hdr.dsp_locn%type;
v_sql_errm varchar2(512);
v_table_name user_constraints.table_name%type;

begin

    p_num_locns_deleted := 0;

    select locn_id, dsp_locn
    bulk collect into vt_locnid, vt_dsplocn
    from locn_wizard_locn_tmp;

    case
    when p_locn_class in ('R','S','P','0') then
        for loc in 1..vt_locnid.count
        loop
            v_locn_id := vt_locnid(loc);
            v_dsp_locn := vt_dsplocn(loc);

            delete locn_grp where locn_id = vt_locnid(loc);
            delete dock_door_master where anchor_locn_id = vt_locnid(loc)
                and exists
                (
                    select 1
                    from resv_locn_hdr rlh
                    where rlh.locn_id = vt_locnid(loc)
                        and rlh.inbd_staging_flag = 'Y'
                );
            delete resv_locn_hdr rlh where rlh.locn_id = vt_locnid(loc);
            delete locn_hdr lh where lh.locn_id = vt_locnid(loc)
                and lh.whse = p_whse;
            if sql%rowcount > 0
            then
                p_num_locns_deleted := p_num_locns_deleted+1;
            end if;
        end loop;

    when p_locn_class in ('A','C','1','2') then
        for loc in 1..vt_locnid.count
        loop
            v_locn_id := vt_locnid(loc);
            v_dsp_locn := vt_dsplocn(loc);

            dbms_output.put_line('v_locn_id='||v_locn_id||';v_dsp_locn='||v_dsp_locn);

            delete locn_grp where locn_id = vt_locnid(loc);
            delete pick_locn_dtl pld where pld.locn_id = vt_locnid(loc);
            delete pick_locn_hdr plh where plh.locn_id = vt_locnid(loc);
            delete locn_hdr lh where lh.locn_id = vt_locnid(loc)
                and lh.whse = p_whse;
            if sql%rowcount > 0
            then
                p_num_locns_deleted := p_num_locns_deleted+1;
                
                --delete related wm_inventory record
                delete from wm_inventory wm where wm.location_id = vt_locnid(loc)
                    and wm.tc_company_id = p_cd_master_id;
            end if;
        end loop;

    when p_locn_class in ('J')  then
        for loc in 1..vt_locnid.count
        loop
            v_locn_id := vt_locnid(loc);
            v_dsp_locn := vt_dsplocn(loc);

            delete store_pack_locn_dtl spld where spld.locn_id = vt_locnid(loc);
            delete store_pack_locn_hdr splh where splh.locn_id = vt_locnid(loc);
            delete locn_hdr lh where lh.locn_id = vt_locnid(loc)
                and lh.whse = p_whse;

            if sql%rowcount > 0
            then
                p_num_locns_deleted := p_num_locns_deleted+1;
            end if;
        end loop;

    when p_locn_class in ('O') then
        for loc in 1..vt_locnid.count
        loop
            v_locn_id := vt_locnid(loc);
            v_dsp_locn := vt_dsplocn(loc);

            delete pkt_consol_locn pcl where pcl.locn_id = vt_locnid(loc);
            delete locn_hdr lh where lh.locn_id = vt_locnid(loc)
                and lh.whse = p_whse;

            if sql%rowcount > 0
            then
                p_num_locns_deleted := p_num_locns_deleted+1;
            end if;
        end loop;

    when p_locn_class in ('T') then
        for loc in 1..vt_locnid.count
        loop
            v_locn_id := vt_locnid(loc);
            v_dsp_locn := vt_dsplocn(loc);

            delete locn_hdr lh where lh.locn_id = vt_locnid(loc)
                and lh.whse = p_whse;

            if sql%rowcount > 0
            then
                p_num_locns_deleted := p_num_locns_deleted + 1;
            end if;
            
        end loop;
       -- Below will Delete the DockDoor  
       when p_locn_class in ('Q') then
        for loc in 1..vt_locnid.count
        loop
            v_locn_id := vt_locnid(loc);
            v_dsp_locn := vt_dsplocn(loc);
            
    	    delete location ln where ln.location_id = vt_locnid(loc);
            delete dock_door dd where dd.dock_door_locn_id  = vt_locnid(loc);
            delete locn_hdr lh where lh.locn_id = vt_locnid(loc)
                and lh.whse = p_whse;

            if sql%rowcount > 0
            then
                p_num_locns_deleted := p_num_locns_deleted + 1;
            end if;
            
        end loop;
         -- Below will Delete the Yard  
        when p_locn_class in ('Y') then
        for loc in 1..vt_locnid.count
        loop
            v_locn_id := vt_locnid(loc);
            v_dsp_locn := vt_dsplocn(loc);
            
            SELECT count(*) into v_count
            FROM trailer_ref
            WHERE CURRENT_LOCATION_ID = vt_locnid (loc)
                OR ASSIGNED_LOCATION_ID = vt_locnid (loc);
            
            SELECT count(*) into v_count1
            FROM ilm_tasks
            WHERE SOURCE_LOCN_ID = vt_locnid (loc)
                OR DEST_LOCN_ID = vt_locnid (loc);
            
            SELECT count(*) into v_count2
            FROM ilm_yard_activity
            WHERE LOCN_ID = vt_locnid (loc);
            
            SELECT count(*) into v_count3
            FROM RFUSER_CURRENT_LOCATION
            WHERE LOCN_ID = vt_locnid (loc);
            
            SELECT count(*) into v_count4
            FROM PUTAWAY_LOCK
            WHERE LOCN_ID = vt_locnid (loc);
            
            IF v_count > 0
            THEN
                v_msg := get_msg_info ('SYSCONTROL', '1121', p_lang_id);
                v_table_name := 'TRAILER_REF';
                
                INSERT INTO msg_log (msg_log_id,
                           module,
                           msg_id,
                           pgm_id,
                           msg,
                           ref_code_1,
                           ref_value_1,
                           ref_code_5,
                           ref_value_5,
                           create_date_time,
                           mod_date_time,
                           user_id,
                           whse,
                           cd_master_id,
                           log_date_time)
                VALUES (
                        msg_log_id_seq.NEXTVAL,
                        'SYSCONTROL',
                        '1121',
                        'L-C-W_CRT_MNTN_LOC',
                        REPLACE (REPLACE (v_msg, '{0}', v_table_name),
                                 '{1}',
                                 v_dsp_locn),
                        '98',
                        v_locn_id,
                        '95',
                        p_locn_parm_id,
                        p_timestamp,
                        p_timestamp,
                        p_user_id,
                        p_whse,
                        p_cd_master_id,
                        SYSDATE);
            ELSIF v_count1 >0
            THEN
                v_msg := get_msg_info ('SYSCONTROL', '1121', p_lang_id);
                v_table_name := 'ILM_TASKS';
                
                INSERT INTO msg_log (msg_log_id,
                           module,
                           msg_id,
                           pgm_id,
                           msg,
                           ref_code_1,
                           ref_value_1,
                           ref_code_5,
                           ref_value_5,
                           create_date_time,
                           mod_date_time,
                           user_id,
                           whse,
                           cd_master_id,
                           log_date_time)
                VALUES (
                        msg_log_id_seq.NEXTVAL,
                        'SYSCONTROL',
                        '1121',
                        'L-C-W_CRT_MNTN_LOC',
                        REPLACE (REPLACE (v_msg, '{0}', v_table_name),
                                 '{1}',
                                 v_dsp_locn),
                        '98',
                        v_locn_id,
                        '95',
                        p_locn_parm_id,
                        p_timestamp,
                        p_timestamp,
                        p_user_id,
                        p_whse,
                        p_cd_master_id,
                        SYSDATE);
            ELSIF v_count2 >0
            THEN
                v_msg := get_msg_info ('SYSCONTROL', '1121', p_lang_id);
                v_table_name := 'ILM_YARD_ACTIVITY';
                
                INSERT INTO msg_log (msg_log_id,
                           module,
                           msg_id,
                           pgm_id,
                           msg,
                           ref_code_1,
                           ref_value_1,
                           ref_code_5,
                           ref_value_5,
                           create_date_time,
                           mod_date_time,
                           user_id,
                           whse,
                           cd_master_id,
                           log_date_time)
                VALUES (
                        msg_log_id_seq.NEXTVAL,
                        'SYSCONTROL',
                        '1121',
                        'L-C-W_CRT_MNTN_LOC',
                        REPLACE (REPLACE (v_msg, '{0}', v_table_name),
                                 '{1}',
                                 v_dsp_locn),
                        '98',
                        v_locn_id,
                        '95',
                        p_locn_parm_id,
                        p_timestamp,
                        p_timestamp,
                        p_user_id,
                        p_whse,
                        p_cd_master_id,
                        SYSDATE);
            ELSIF v_count3 >0
            THEN
                v_msg := get_msg_info ('SYSCONTROL', '1121', p_lang_id);
                v_table_name := 'RFUSER_CURRENT_LOCATION';
                
                INSERT INTO msg_log (msg_log_id,
                           module,
                           msg_id,
                           pgm_id,
                           msg,
                           ref_code_1,
                           ref_value_1,
                           ref_code_5,
                           ref_value_5,
                           create_date_time,
                           mod_date_time,
                           user_id,
                           whse,
                           cd_master_id,
                           log_date_time)
                VALUES (
                        msg_log_id_seq.NEXTVAL,
                        'SYSCONTROL',
                        '1121',
                        'L-C-W_CRT_MNTN_LOC',
                        REPLACE (REPLACE (v_msg, '{0}', v_table_name),
                                 '{1}',
                                 v_dsp_locn),
                        '98',
                        v_locn_id,
                        '95',
                        p_locn_parm_id,
                        p_timestamp,
                        p_timestamp,
                        p_user_id,
                        p_whse,
                        p_cd_master_id,
                        SYSDATE);
            ELSIF v_count4 >0
            THEN
                v_msg := get_msg_info ('SYSCONTROL', '1121', p_lang_id);
                v_table_name := 'PUTAWAY_LOCK';
                
                INSERT INTO msg_log (msg_log_id,
                           module,
                           msg_id,
                           pgm_id,
                           msg,
                           ref_code_1,
                           ref_value_1,
                           ref_code_5,
                           ref_value_5,
                           create_date_time,
                           mod_date_time,
                           user_id,
                           whse,
                           cd_master_id,
                           log_date_time)
                VALUES (
                        msg_log_id_seq.NEXTVAL,
                        'SYSCONTROL',
                        '1121',
                        'L-C-W_CRT_MNTN_LOC',
                        REPLACE (REPLACE (v_msg, '{0}', v_table_name),
                                 '{1}',
                                 v_dsp_locn),
                        '98',
                        v_locn_id,
                        '95',
                        p_locn_parm_id,
                        p_timestamp,
                        p_timestamp,
                        p_user_id,
                        p_whse,
                        p_cd_master_id,
                        SYSDATE);
            ELSE
                delete location ln where ln.location_id = vt_locnid(loc);
                delete yard_zone_slot yzs where yzs.locn_id  = vt_locnid(loc);
                delete locn_hdr lh where lh.locn_id = vt_locnid(loc)
                    and lh.whse = p_whse;
            end if;        

            if sql%rowcount > 0
            then
                p_num_locns_deleted := p_num_locns_deleted + 1;
            end if;
            
        end loop;
        
    end case;

    vt_locnid.delete;
    vt_dsplocn.delete;
    v_locn_id := null;
exception
    when others then
        rollback;

    p_num_locns_deleted := 0;
    v_msg := get_msg_info('SYSCONTROL', '1121', p_lang_id);
    v_sql_errm := substr(sqlerrm, 1, 200);

    begin
        select table_name
        into v_table_name
        from user_constraints
        where constraint_name = substr(v_sql_errm, instr(v_sql_errm, 'FK_'),
            instr(v_sql_errm,')') - instr(v_sql_errm,'FK_'));
    exception
        when no_data_found then
            null;
    end;

    insert into msg_log
        (msg_log_id, module, msg_id, pgm_id, msg, ref_code_1, ref_value_1,
        ref_code_5, ref_value_5, create_date_time, mod_date_time, user_id, whse,
        cd_master_id, log_date_time)
    values (msg_log_id_seq.nextval, 'SYSCONTROL', '1121', 'L-C-W_CRT_MNTN_LOC',
        replace(replace(v_msg, '{0}', v_dsp_locn), '%t', v_table_name), '98',
        v_locn_id, '95', p_locn_parm_id, p_timestamp, p_timestamp,
        p_user_id, p_whse, p_cd_master_id, sysdate);

    raise_application_error(-20001, 'error on delete_locn: ' || sqlerrm);
end;
/

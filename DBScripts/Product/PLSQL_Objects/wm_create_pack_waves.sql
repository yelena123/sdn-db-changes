create or replace procedure wm_create_pack_waves
(
    p_whse              in whse_master.whse%type,
    p_ship_wave_nbr     in ship_wave_parm.ship_wave_nbr%type,
    p_max_pack_wave     in number,
    p_user_id           in ucl_user.user_name%type,
    p_wave_parm_id      in wave_parm.wave_parm_id%type,
    p_pack_wave_parm_id in pack_wave_parm_hdr.pack_wave_parm_id%type,
    p_chute_util        in number,
    p_bal_alg           in pack_wave_parm_hdr.bal_alg%type,
    p_start_lvl         in out pack_wave_parm_hdr.start_lvl%type,
    p_sys_cd_flag_lvl   out number
)
as
    v_unassigned_lpn_exists   number(1) := 0;
    v_curr_pack_wave_seq_nbr  pack_wave_parm_dtl.pack_wave_seq_nbr%type := 0;
begin
    wm_load_pack_wave_chute_list(p_whse, p_chute_util, p_max_pack_wave,
        p_pack_wave_parm_id, p_start_lvl, p_sys_cd_flag_lvl);

    if (p_bal_alg = '4')
    then
        wm_assign_chutes_to_cartons(p_whse, p_bal_alg);
        return;
    end if;

    -- create multiple pack waves when no more cartons can be fit
    while (v_curr_pack_wave_seq_nbr < p_max_pack_wave)
    loop
        v_curr_pack_wave_seq_nbr := v_curr_pack_wave_seq_nbr + 1;

        wm_assign_chutes_to_cartons(p_whse, p_bal_alg, v_curr_pack_wave_seq_nbr);

        -- if any lpns are left unassigned, assume pack wave capacity
        -- has been exceeded
        select case when exists
        (
            select 1
            from tmp_pack_wave_selected_lpns t
            where t.chute_row_id is null
        ) then 1 else 0 end
        into v_unassigned_lpn_exists
        from dual;

        if (v_unassigned_lpn_exists = 0)
        then
            wm_cs_log('No lpns remain for assignment. Stopping after PW '
                || to_char(v_curr_pack_wave_seq_nbr));
            exit;
        end if;
    end loop; -- while(all pack waves)
end;
/
show errors;
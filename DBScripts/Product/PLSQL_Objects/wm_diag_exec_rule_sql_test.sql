create or replace procedure wm_diag_exec_rule_sql_test
(
    p_rowset                out types.cursortype,
    p_ship_wave_parm_id     in ship_wave_parm.ship_wave_parm_id%type,
    p_rule_type             in rule_hdr.rule_type%type,
    p_preview_wave_flag     in number default 0,
    p_user_id               in ucl_user.user_name%type default null
)
as
    v_rc                    number(1);
    v_whse                  facility.whse%type;
    v_tc_company_id         wave_parm.tc_company_id%type;
    v_max_log_lvl           number(1) := 0;
    v_code_id_1             sys_code.code_id%type := '100';
    v_code_id_2             sys_code.code_id%type := '101';
    v_ref_value_1           msg_log.ref_value_1%type ;    
begin
    select coalesce(wp.tc_company_id, wm_utils.wm_get_user_bu(p_user_id)), swp.whse, swp.wave_desc
    into v_tc_company_id, v_whse, v_ref_value_1
    from ship_wave_parm swp
    left join wave_parm wp on wp.wave_parm_id = swp.wave_parm_id
    where swp.ship_wave_parm_id = p_ship_wave_parm_id;
    
    delete from tmp_log_parm;
    insert into tmp_log_parm
    (
        log_lvl, pgm_id, module, msg_id, ref_code_1, ref_value_1, ref_code_2, 
        user_id, whse, cd_master_id
    )
    values
    (
        v_max_log_lvl, 'Rule Builder', 'WAVE', '3068', v_code_id_1,
        v_ref_value_1, v_code_id_2, p_user_id, v_whse, v_tc_company_id
    );

    if (p_rule_type = 'SS')
    then
        wm_diag_check_ss_rules(p_ship_wave_parm_id, p_preview_wave_flag,p_user_id, v_rc);
    else
        raise_application_error(-20050, 'Unsupported feature.');
    end if;
    open p_rowset for select v_rc as v_rc from dual;
end;
/
show errors;

create or replace force view val_sql_data_vw
(
    ident, sql_id, sql_text, raw_text, bind_var_list_csv, short_sql_text, owner, col_list_csv
)
as
select vs.ident, vs.sql_id, vs.sql_text, vs.raw_text, vs.bind_var_list bind_var_list_csv,
    to_char(substr(vs.sql_text, 1, 4000)) short_sql_text, vs.owner,
    wm_val_get_col_list_for_sql(vs.sql_id) col_list_csv
from val_sql vs
/

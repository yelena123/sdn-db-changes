create or replace
Procedure CREATEMANUALAWARDCONSTRAINTS (I_Rfpid Integer, I_Round_Num Integer, Newscene Integer )
is
  Contract_type INTEGER;
  v_freq INTEGER;
  l_volume NUMBER(22,6);
  award_type INTEGER;
Begin

    For Cursor_Scenario1 In ( Select Manualawardamount, Obcarriercodeid, Historicalaward, Laneid, Laneequipmenttypeid From Scenarioadjustment Where Rfpid= I_Rfpid And Scenarioid = NEWSCENE And Manualawardamount > 0)
    Loop

           select  oc.contract_type into Contract_type from ob200contract_type oc, laneequipmenttype le where COALESCE(oc.equipmenttype_id,-1)= COALESCE(le.equipmenttype_id,-1)
            and COALESCE(oc.servicetype_id,-1)= COALESCE(le.servicetype_id,-1) and COALESCE(oc.protectionlevel_id,-1)= COALESCE(le.protectionlevel_id,-1)
            And Coalesce(Oc.Mot,'-1-')= Coalesce(Le.Mot,'-1-') And Coalesce(Oc.Commoditycode_Id,-1)= Coalesce(Le.Commoditycode_Id,-1)
            and oc.rfpid = le.rfpid and le.rfpid = I_Rfpid and le.laneid = Cursor_Scenario1.Laneid and le.laneEquipmenttypeId =  Cursor_Scenario1.Laneequipmenttypeid;
            
           select volumefrequency, volume into v_freq, l_volume from lane where rfpid= I_rfpid and laneid =  Cursor_Scenario1.Laneid ;
		   select awardtype into award_type from RFP where rfpid = I_rfpid;
           IF award_type = 1 THEN
				Cursor_Scenario1.Manualawardamount := (Cursor_Scenario1.Manualawardamount * l_volume) / 100;
		   END IF;
           CASE 
              WHEN v_freq = 0 THEN Cursor_Scenario1.Manualawardamount := Cursor_Scenario1.Manualawardamount * 7;
              WHEN v_freq = 1 THEN Cursor_Scenario1.Manualawardamount := Cursor_Scenario1.Manualawardamount * 1;
              WHEN v_freq = 2 THEN Cursor_Scenario1.Manualawardamount := Cursor_Scenario1.Manualawardamount * 7/14.0 ;
              WHEN v_freq = 3 THEN Cursor_Scenario1.Manualawardamount := Cursor_Scenario1.Manualawardamount * 7/30.4166;
              WHEN v_freq = 4 THEN Cursor_Scenario1.Manualawardamount := Cursor_Scenario1.Manualawardamount * 7/182.5;
              WHEN v_freq = 5 THEN Cursor_Scenario1.Manualawardamount := Cursor_Scenario1.Manualawardamount * 7/365.0;
            END CASE;
              
          
          INSERT INTO Ob200override_Adv (Override_Id, Rfpid, scenarioid, type,  Enable_Flag, source, level_of_detail, contract_type, Value_Type_Id, Obcarriercodeid, bid_id, Min_Value, Max_Value, Slack, Is_Valid, comments)
          Values (Override_Id_Seq.Nextval ,I_Rfpid, Newscene, 'O', 1, 'O', 'L',contract_type, 0, Cursor_Scenario1.Obcarriercodeid , Cursor_Scenario1.Laneid, -1, Cursor_Scenario1.Manualawardamount , 0 , 1, 'SYS_AWARD_' || Cursor_Scenario1.obcarriercodeid || '_' || Cursor_Scenario1.laneid || '_' ||  Contract_type || '_' || Cursor_Scenario1.Historicalaward || '_MAX') ;

    End Loop;


    For Cursor_Scenario In ( Select Manualawardamount, Obcarriercodeid, Historicalaward, laneid, Laneequipmenttypeid From Scenarioadjustment Where Rfpid= I_Rfpid And Scenarioid = NEWSCENE and manualawardamount > 0)
    Loop

            select  oc.contract_type into Contract_type from ob200contract_type oc, laneequipmenttype le where COALESCE(oc.equipmenttype_id,-1)= COALESCE(le.equipmenttype_id,-1)
            and COALESCE(oc.servicetype_id,-1)= COALESCE(le.servicetype_id,-1) and COALESCE(oc.protectionlevel_id,-1)= COALESCE(le.protectionlevel_id,-1)
            And Coalesce(Oc.Mot,'-1-')= Coalesce(Le.Mot,'-1-') And Coalesce(Oc.Commoditycode_Id,-1)= Coalesce(Le.Commoditycode_Id,-1)
            and oc.rfpid = le.rfpid and le.rfpid = I_Rfpid and le.laneid = Cursor_Scenario.Laneid and le.laneEquipmenttypeId =  Cursor_Scenario.Laneequipmenttypeid;

           select volumefrequency, volume into v_freq, l_volume from lane where rfpid = I_rfpid and laneid =  Cursor_Scenario.Laneid ;
		   select awardtype into award_type from RFP where rfpid = I_rfpid;
           IF award_type = 1 THEN
				Cursor_Scenario.Manualawardamount := (Cursor_Scenario.Manualawardamount * l_volume) / 100;
		   END IF;
           CASE 
              WHEN v_freq = 0 THEN Cursor_Scenario.Manualawardamount := Cursor_Scenario.Manualawardamount * 7;
              WHEN v_freq = 1 THEN Cursor_Scenario.Manualawardamount := Cursor_Scenario.Manualawardamount * 1;
              WHEN v_freq = 2 THEN Cursor_Scenario.Manualawardamount := Cursor_Scenario.Manualawardamount * 7/14.0 ;
              WHEN v_freq = 3 THEN Cursor_Scenario.Manualawardamount := Cursor_Scenario.Manualawardamount * 7/30.4166;
              WHEN v_freq = 4 THEN Cursor_Scenario.Manualawardamount := Cursor_Scenario.Manualawardamount * 7/182.5;
              WHEN v_freq = 5 THEN Cursor_Scenario.Manualawardamount := Cursor_Scenario.Manualawardamount * 7/365.0;
            END CASE;
            
            
          INSERT INTO Ob200override_Adv (Override_Id, Rfpid, scenarioid, type, Enable_Flag, source, level_of_detail, contract_type, Value_Type_Id, Obcarriercodeid, bid_id, Min_Value, Max_Value, Slack, Is_Valid, comments)
          Values (Override_Id_Seq.Nextval ,I_Rfpid, Newscene, 'O', 1, 'O', 'L',contract_type, 0, Cursor_Scenario.Obcarriercodeid , Cursor_Scenario.Laneid, Cursor_Scenario.Manualawardamount , -1,  0 , 1, 'SYS_AWARD_' || Cursor_Scenario.obcarriercodeid || '_' || Cursor_Scenario.laneid || '_' ||  Contract_type || '_' || Cursor_Scenario.Historicalaward || '_MIN') ;
    End Loop;


end createmanualawardconstraints;
/
create or replace procedure wm_val_lock_unlock_sql
(
    p_sql_id    in val_sql.sql_id%type default null,
    p_ident     in val_sql.ident%type default null,
    p_lock      in val_sql.is_locked%type
)
as
    v_sql_id    val_sql.sql_id%type;
    v_ident     val_sql.ident%type;
begin
    update val_sql vs
    set vs.is_locked = p_lock
    where vs.sql_id = p_sql_id or vs.ident = p_ident
    returning vs.sql_id, vs.ident into v_sql_id, v_ident;
    dbms_output.put_line(case when p_lock = 1 then 'Locked' else 'Unlocked' end
        || ' sql ' || v_sql_id || '(' || v_ident || ')');
    
    commit;
end;
/
show errors;

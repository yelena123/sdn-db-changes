CREATE OR REPLACE PROCEDURE TRANSIENT_PURGE (
   atccompanyid    shipment.tc_company_id%TYPE,
   adays           NUMBER)
AS
   /**********************************************************************************************************************
   PROCEDURE : PURGE_OTHER_LOGS_EVENTS
   PURPOSE   : PURGE UNWANTED TRANSIENT DATA ACROSS TABLES
   AUTHOR    : RAJ S K
   CHANGES   : USED PURGE_ONLY_PROC FOR HANDLING DELETE OPERATIONS EFFECTIVELY, CODE RESTRUCTURING USING TEMPORARY TABLES
               AND ALSO INTRODUCED FIXED TIMESTAMP LOGIC ACROSS PROCEDURE
   ***********************************************************************************************************************/
   PURGE_START_TIME   DATE := TRUNC (SYSDATE);
   v_stmt             VARCHAR2 (2000);
   nsqlcode           NUMBER;
   VSQLERRM           VARCHAR2 (300);
BEGIN
   archive_pkg.log_archive (atccompanyid,
                            'TRANSIENT_PURGE - STARTS',
                            NULL,
                            NULL,
                            0);

   -- DATE BASED DELETION -----
   PURGE_ONLY_PROC (
      'ASN_EVENT',
         'created_dttm < (to_date ('''
      || purge_start_time
      || ''') - '
      || adays
      || ')');

   PURGE_ONLY_PROC (
      'ASSIGNMENT_EVENT',
         'created_dttm < (to_date ('''
      || purge_start_time
      || ''') - '
      || adays
      || ')');

   PURGE_ONLY_PROC (
      'booking_event',
         'created_dttm < (to_date ('''
      || PURGE_START_TIME
      || ''') - '
      || ADAYS
      || ')');

   DELETE FROM carrier_parameter_event
         WHERE last_updated_dttm < purge_start_time - adays;

   PURGE_ONLY_PROC (
      'claims_event',
         'created_dttm < (to_date ('''
      || purge_start_time
      || ''') - '
      || adays
      || ')');

   --Temporary table creation for using it across transient_purge procedure
   QUIET_DROP ('TABLE', 'TRANS_PURGE_GTT');

   V_STMT :=
         'CREATE TABLE TRANS_PURGE_GTT tablespace DOM_DT_TBS AS SELECT event_id FROM om_sched_event where EXECUTED_DTTM < (to_date ('''
      || PURGE_START_TIME
      || ''') - '
      || ADAYS
      || ') AND is_executed = 1';

   EXECUTE IMMEDIATE v_stmt;

   PURGE_ONLY_PROC ('SO_ORDER_CHG_VALUE',
                    'event_id IN (SELECT event_id FROM TRANS_PURGE_GTT)');

   PURGE_ONLY_PROC ('constemp_sched_event',
                    'event_id IN (SELECT event_id FROM TRANS_PURGE_GTT)');

   PURGE_ONLY_PROC ('A_PROCESS_TEMPLATE_SCHED',
                    'event_id IN (SELECT event_id FROM TRANS_PURGE_GTT)');

   PURGE_ONLY_PROC ('A_WF_EVENTMAP',
                    'event_id IN (SELECT event_id FROM TRANS_PURGE_GTT)');

   PURGE_ONLY_PROC (
      'WF_BATCH_SCHEDULER_EVENT',
      'om_sched_event_id IN (SELECT event_id FROM TRANS_PURGE_GTT)');

   QUIET_DROP ('TABLE', 'TRANS_PURGE_GTT');

   V_STMT :=
         'CREATE TABLE TRANS_PURGE_GTT tablespace DOM_DT_TBS AS SELECT LRF.REPORT_ID as REPORT_ID FROM LRF_REPORT LRF, LRF_EVENT LE, OM_SCHED_EVENT OSE WHERE LRF.EVENT_ID = LE.LRF_EVENT_ID
                  AND LE.EVENT_ID = OSE.EVENT_ID AND OSE.EXECUTED_DTTM < (to_date ('''
      || PURGE_START_TIME
      || ''') - '
      || ADAYS
      || ') AND OSE.is_executed = 1';

   EXECUTE IMMEDIATE V_STMT;

   PURGE_ONLY_PROC ('LRF_REPORT_PARAM_VALUE_DTL',
                    'REPORT_ID IN (SELECT REPORT_ID FROM TRANS_PURGE_GTT)');

   PURGE_ONLY_PROC ('LRF_REPORT_PARAM_VALUE',
                    'REPORT_ID IN (SELECT REPORT_ID FROM TRANS_PURGE_GTT)');

   PURGE_ONLY_PROC ('LRF_REPORT',
                    'REPORT_ID IN (SELECT REPORT_ID FROM TRANS_PURGE_GTT)');

   PURGE_ONLY_PROC (
      'LRF_EVENT',
         'event_id IN (SELECT event_id FROM OM_SCHED_EVENT WHERE EXECUTED_DTTM < (to_date ('''
      || PURGE_START_TIME
      || ''') - '
      || ADAYS
      || ') AND IS_EXECUTED = 1)');

   PURGE_ONLY_PROC (
      'detention_event',
         'created_dttm < (to_date ('''
      || purge_start_time
      || ''') - '
      || adays
      || ')');

   PURGE_ONLY_PROC (
      'error_log_args',
         'error_id IN (SELECT error_id FROM error_log WHERE created_dttm < (to_date ('''
      || purge_start_time
      || ''') - '
      || adays
      || '))');

   PURGE_ONLY_PROC (
      'error_log',
         'created_dttm < (to_date ('''
      || purge_start_time
      || ''') - '
      || adays
      || ')');

   PURGE_ONLY_PROC (
      'event_log',
         'event_dttm < (to_date ('''
      || purge_start_time
      || ''') - '
      || adays
      || ')');

   PURGE_ONLY_PROC (
      'ilm_appointment_events',
         'created_dttm < (to_date ('''
      || purge_start_time
      || ''') - '
      || adays
      || ')');

   PURGE_ONLY_PROC (
      'ilm_dock_door_events',
         'created_dttm < (to_date ('''
      || purge_start_time
      || ''') - '
      || adays
      || ')');

   PURGE_ONLY_PROC (
      'ilm_task_events',
         'created_dttm < (to_date ('''
      || purge_start_time
      || ''') - '
      || adays
      || ')');

   PURGE_ONLY_PROC (
      'ilm_trailer_events',
         'created_dttm < (to_date ('''
      || PURGE_START_TIME
      || ''') - '
      || ADAYS
      || ')');

   PURGE_ONLY_PROC (
      'invoice_event',
         'created_dttm < (to_date ('''
      || PURGE_START_TIME
      || ''') - '
      || ADAYS
      || ')');

   PURGE_ONLY_PROC (
      'lpn_event',
         'created_dttm < (to_date ('''
      || PURGE_START_TIME
      || ''') - '
      || ADAYS
      || ')');

   PURGE_ONLY_PROC (
      'om_sched_event_report',
         'executed_dttm < (to_date ('''
      || PURGE_START_TIME
      || ''') - '
      || ADAYS
      || ')');

   PURGE_ONLY_PROC (
      'om_sched_event',
         'executed_dttm < (to_date ('''
      || PURGE_START_TIME
      || ''') - '
      || ADAYS
      || ')');

   PURGE_ONLY_PROC (
      'order_event',
         'created_dttm < (to_date ('''
      || PURGE_START_TIME
      || ''') - '
      || ADAYS
      || ')');

   PURGE_ONLY_PROC (
      'purchase_orders_event',
         'created_dttm < (to_date ('''
      || PURGE_START_TIME
      || ''') - '
      || ADAYS
      || ')');

   DELETE FROM rating_event
         WHERE last_updated_dttm < PURGE_START_TIME - adays;

   PURGE_ONLY_PROC (
      'rts_event',
         'created_dttm < (to_date ('''
      || PURGE_START_TIME
      || ''') - '
      || ADAYS
      || ')');

   PURGE_ONLY_PROC (
      'shipment_event',
         'created_dttm < (to_date ('''
      || PURGE_START_TIME
      || ''') - '
      || ADAYS
      || ')');

   PURGE_ONLY_PROC (
      'tran_log_message',
         'tran_log_id IN (SELECT tran_log_id FROM tran_log WHERE last_updated_dttm < (to_date ('''
      || purge_start_time
      || ''') - '
      || adays
      || '))');

   PURGE_ONLY_PROC (
      'tran_log',
         'last_updated_dttm < (to_date ('''
      || purge_start_time
      || ''') - '
      || adays
      || ')');

   PURGE_ONLY_PROC (
      'wm_wax_event_log',
         'last_updated_dttm < (to_date ('''
      || purge_start_time
      || ''') - '
      || adays
      || ')');

   PURGE_ONLY_PROC (
      'A_PROCESS_LOG_HISTORY',
         '(PROCESS_ID, EVENT_TIMESTAMP) IN ( SELECT PROCESS_ID PID, MIN(EVENT_TIMESTAMP) CT  FROM
                                                  A_PROCESS_LOG_HISTORY GROUP BY PROCESS_ID having MIN(EVENT_TIMESTAMP) < (to_date ('''
      || purge_start_time
      || ''') - '
      || adays
      || '))');

   PURGE_ONLY_PROC (
      'A_PROCESS_LOG_ASYNC_TRACK',
         'CREATED_DTTM < (to_date ('''
      || purge_start_time
      || ''') - '
      || adays
      || ')');


   PURGE_ONLY_PROC (
      'A_PROCESS_LOG_ERROR',
         '(PROCESS_ID, CREATED_DTTM) IN ( SELECT PROCESS_ID PID, MIN(CREATED_DTTM) CT  FROM
                                                A_PROCESS_LOG_ERROR GROUP BY PROCESS_ID HAVING MIN(CREATED_DTTM) < (to_date ('''
      || purge_start_time
      || ''') - '
      || adays
      || '))');

   -- PURGING TABLES.
   EXECUTE IMMEDIATE ('DELETE IMPORT_RATING_LANE_DTL_RATE');

   EXECUTE IMMEDIATE ('DELETE IMPORT_LANE_ACCESSORIAL');

   EXECUTE IMMEDIATE ('DELETE IMPORT_RATING_LANE_DTL');

   EXECUTE IMMEDIATE ('DELETE IMPORT_RATING_LANE');

   EXECUTE IMMEDIATE ('DELETE RATING_LANE_ERRORS');

   QUIET_DROP ('TABLE', 'TRANS_PURGE_OBJ_GTT');

   V_STMT :=
      'CREATE TABLE TRANS_PURGE_OBJ_GTT tablespace DOM_DT_TBS AS select  ASN_ID as OBJECT_ID from ASN
                                                                        union
                                                                        select  SHIPMENT_ID as OBJECT_ID from SHIPMENT
                                                                        union
                                                                        select  ORDER_ID as OBJECT_ID from ORDERS
                                                                        union
                                                                        select  PURCHASE_ORDERS_ID as OBJECT_ID from PURCHASE_ORDERS 
                                                                        union
                                                                        select  BOOKING_ID as OBJECT_ID from BOOKING
                                                                        union
                                                                        select  PURCHASE_ORDERS_LINE_ITEM_ID as OBJECT_ID from PURCHASE_ORDERS_LINE_ITEM
                                                                        union
                                                                        select  LPN_ID as OBJECT_ID from LPN  
                                                                        union
                                                                        SELECT  LINE_ITEM_ID as OBJECT_ID FROM ORDER_LINE_ITEM';

   EXECUTE IMMEDIATE v_stmt;

   EXECUTE IMMEDIATE
      'CREATE INDEX IDX_TRN_GTT ON TRANS_PURGE_OBJ_GTT(OBJECT_ID) TABLESPACE CBO_BASE_IDX_TBS';

   QUIET_DROP ('TABLE', 'EM_TRANSIENT_PURGE_GTT');

   V_STMT :=
      ' CREATE TABLE EM_TRANSIENT_PURGE_GTT tablespace DOM_DT_TBS AS  select  distinct EOT.EVTMGT_OBJ_TYPE_ID, EEO.OBJECT_ID, EEO.EVENT_OCCURRENCE_ID 
                                                                                from    EM_OBJECT_TYPE EOT, EM_EVENT_OCCURRENCE EEO 
                                                                                where   EOT.EVTMGT_OBJ_TYPE_ID  =  EEO.EVTMGT_OBJ_TYPE_ID
                                                                                and     EOT.EVTMGT_OBJ_TYPE_ID  <> 32 
                                                                                and     EEO.OBJECT_ID           not in (select OBJECT_ID from TRANS_PURGE_OBJ_GTT)';

   EXECUTE IMMEDIATE v_stmt;

   PURGE_ONLY_PROC (
      'EM_OE_ADDINF_VAL',
      'EM_OE_ADDINF_ATTID IN (SELECT EM_OE_ADDINF_ATTID FROM EM_OE_ADDINF_ATT WHERE EVENT_OCCURRENCE_ID IN (SELECT EVENT_OCCURRENCE_ID FROM EM_TRANSIENT_PURGE_GTT))');

   PURGE_ONLY_PROC (
      'EM_OE_ADDINF_ATT',
      'EVENT_OCCURRENCE_ID IN (SELECT EVENT_OCCURRENCE_ID FROM EM_TRANSIENT_PURGE_GTT)');

   PURGE_ONLY_PROC (
      'EM_EVENT_OCCURRENCE',
      'EVENT_OCCURRENCE_ID IN (SELECT EVENT_OCCURRENCE_ID FROM EM_TRANSIENT_PURGE_GTT)');

   QUIET_DROP ('TABLE', 'EM_TRANSIENT_PURGE_GTT');

   V_STMT :=
      ' CREATE TABLE EM_TRANSIENT_PURGE_GTT tablespace DOM_DT_TBS AS  select  distinct EOT.EVTMGT_OBJ_TYPE_ID, EEO.OBJECT_ID, EEO.OBJECT_SCHED_GROUP_ID
                                                                                from    EM_OBJECT_TYPE EOT, EM_OBJECT_SCHED_GROUP EEO 
                                                                                where   EOT.EVTMGT_OBJ_TYPE_ID  =  EEO.EVTMGT_OBJ_TYPE_ID
                                                                                and     EOT.EVTMGT_OBJ_TYPE_ID  <> 32 
                                                                                and     EEO.OBJECT_ID           not in (select OBJECT_ID from TRANS_PURGE_OBJ_GTT)';

   EXECUTE IMMEDIATE V_STMT;

   PURGE_ONLY_PROC (
      'EM_OBJECT_RULE',
      'OBJ_SCHED_ID IN (SELECT OBJ_SCHED_ID FROM EM_OBJECT_SCHEDULE WHERE OBJECT_SCHED_GROUP_ID IN (SELECT OBJECT_SCHED_GROUP_ID FROM EM_TRANSIENT_PURGE_GTT))');

   PURGE_ONLY_PROC (
      'EM_LINK_OBJECT_SCHED',
      'TO_OBJECT_SCHED_EVENT_ID IN (SELECT OBJ_SCHED_EVENT_ID FROM EM_OBJECT_SCHEDULE_EVENT WHERE OBJ_SCHED_ID IN (SELECT OBJ_SCHED_ID FROM EM_OBJECT_SCHEDULE WHERE OBJECT_SCHED_GROUP_ID IN (SELECT OBJECT_SCHED_GROUP_ID FROM EM_TRANSIENT_PURGE_GTT)))');

   PURGE_ONLY_PROC (
      'EM_LINK_OBJECT_SCHED',
      'FROM_OBJECT_SCHED_ID IN (SELECT OBJ_SCHED_ID FROM EM_OBJECT_SCHEDULE WHERE OBJECT_SCHED_GROUP_ID IN (SELECT OBJECT_SCHED_GROUP_ID FROM EM_TRANSIENT_PURGE_GTT))');

   PURGE_ONLY_PROC (
      'EM_LINK_OBJECT_SCHED',
      'TO_OBJECT_SCHED_ID IN (SELECT OBJ_SCHED_ID FROM EM_OBJECT_SCHEDULE WHERE OBJECT_SCHED_GROUP_ID IN (SELECT OBJECT_SCHED_GROUP_ID FROM EM_TRANSIENT_PURGE_GTT))');

   PURGE_ONLY_PROC (
      'EM_OBJECT_SCHEDULE_EVENT',
      'OBJ_SCHED_ID IN (SELECT OBJ_SCHED_ID FROM EM_OBJECT_SCHEDULE WHERE OBJECT_SCHED_GROUP_ID IN (SELECT OBJECT_SCHED_GROUP_ID FROM EM_TRANSIENT_PURGE_GTT))');

   PURGE_ONLY_PROC (
      'EM_OBJECT_SCHEDULE',
      'OBJECT_SCHED_GROUP_ID IN (SELECT OBJECT_SCHED_GROUP_ID FROM EM_TRANSIENT_PURGE_GTT)');

   PURGE_ONLY_PROC (
      'EM_RULE_OBJ_SCHED_GROUP',
      'OBJECT_SCHED_GROUP_ID IN (SELECT OBJECT_SCHED_GROUP_ID FROM EM_TRANSIENT_PURGE_GTT)');

   PURGE_ONLY_PROC (
      'EM_OBJECT_SCHED_GROUP',
      'OBJECT_SCHED_GROUP_ID IN (SELECT OBJECT_SCHED_GROUP_ID FROM EM_TRANSIENT_PURGE_GTT)');

   PURGE_ONLY_PROC (
      'EM_OBJECTEVENTHIST',
      'OBJECT_EVENT_ID IN (SELECT OBJECT_EVENT_ID FROM EM_OBJECTEVENTHIST A, EM_TRANSIENT_PURGE_GTT B WHERE A.EVTMGT_OBJ_TYPE_ID = B.EVTMGT_OBJ_TYPE_ID AND   A.OBJECT_ID  =  B.OBJECT_ID)');

   QUIET_DROP ('TABLE', 'EM_TRANSIENT_PURGE_GTT');

   V_STMT :=
      ' CREATE TABLE EM_TRANSIENT_PURGE_GTT tablespace DOM_DT_TBS AS  select  distinct EOT.EVTMGT_OBJ_TYPE_ID, EEO.OBJECT_ID, EEO.OBJ_EVENT_NOTIF_ID
                                                                                from    EM_OBJECT_TYPE EOT, EM_OBJ_EVT_NOTIFY EEO 
                                                                                where   EOT.EVTMGT_OBJ_TYPE_ID  =  EEO.EVTMGT_OBJ_TYPE_ID
                                                                                and     EOT.EVTMGT_OBJ_TYPE_ID  <> 32 
                                                                                and     EEO.OBJECT_ID           not in (select OBJECT_ID from TRANS_PURGE_OBJ_GTT)';

   EXECUTE IMMEDIATE V_STMT;

   PURGE_ONLY_PROC (
      'EM_OBJEVTNOTIFYLOG',
      'OBJEVT_NOTIF_LOGID IN (SELECT OBJ_EVENT_NOTIF_ID FROM EM_TRANSIENT_PURGE_GTT)');

   PURGE_ONLY_PROC (
      'EM_OBJ_NOTIFY_CONTENT',
      'OBJ_EVENT_NOTIF_ID IN (SELECT OBJ_EVENT_NOTIF_ID FROM EM_TRANSIENT_PURGE_GTT)');

   PURGE_ONLY_PROC (
      'EM_OBJ_EVT_NOTIFY',
      'OBJ_EVENT_NOTIF_ID IN (SELECT OBJ_EVENT_NOTIF_ID FROM EM_TRANSIENT_PURGE_GTT)');

   DELETE FROM ARCHIVE_ERROR_LOG;

   COMMIT;

   archive_pkg.log_archive (atccompanyid,
                            'TRANSIENT_PURGE - ENDS',
                            NULL,
                            NULL,
                            0);
EXCEPTION
   WHEN OTHERS
   THEN
      nsqlcode := SQLCODE;
      VSQLERRM := SQLERRM;
      archive_pkg.log_error (nsqlcode, vsqlerrm);
      ROLLBACK;
      QUIET_DROP ('TABLE', 'TRANS_PURGE_GTT');
      QUIET_DROP ('TABLE', 'TRANS_PURGE_OBJ_GTT');
      QUIET_DROP ('TABLE', 'EM_TRANSIENT_PURGE_GTT');
END TRANSIENT_PURGE;
/
create or replace procedure wm_parcel_rate_processing(
    p_tc_company_id     in number,
    p_sl_company_id     in number,
    p_bu_company_id     in number,
    p_carrier_code   in parcel_zone_data.carrier_code%type default null,
    p_slevels_delimited   in VARCHAR2,
    p_effective_date  in parcel_rate_calender.effective_dt%type default sysdate,
    p_expiration_date in parcel_rate_calender.expiration_dt%type default sysdate,
    p_user_id          in parcel_rate_data.user_id%type default 'manh')

as
  v_serv_type          varchar2(200);
  v_service_level_type      varchar2(50);
  v_parcel_rate_data_seq_id number;
  v_parcel_rate_calendar_id   number;
  v_carrier_id              number ;
  v_calendar_count          number;
  v_slevels_delimited varchar(500);
  v_sl_table IN_LIST_TAB;
  begin
  
   /*
  Not considering service_level right now because of missing SERVIC_lEVEL in PARCEL_RATE_CALENDAR table. So if you were to import for the same BU
  twice for different date, there is no way to elegantly handling the calendar validity.
  */
  -- v_slevels_delimited :=('"' || replace( p_slevels_delimited, ',', '","') || '"');
  --SELECT * BULK COLLECT INTO v_sl_table FROM (select (column_value).getstringval() from xmltable(v_slevels_delimited));

  v_parcel_rate_calendar_id := 0;
  v_calendar_count := 0;
  -- first check if there is any existing calendar for the given carrier

   select carrier_id into v_carrier_id
    from carrier_code
    where carrier_code=p_carrier_code
    and tc_company_id =p_tc_company_id;

  select count(1) into v_calendar_count
  from parcel_rate_calender
  where carrier_id  = v_carrier_id
  and expiration_dt = p_expiration_date
  and tc_company_id = p_bu_company_id
  and effective_dt  = p_effective_date;

  if (v_calendar_count > 0) then
    select parcel_rate_calender_id
    into v_parcel_rate_calendar_id
    from parcel_rate_calender
    where carrier_id        = v_carrier_id
    and expiration_dt         = p_expiration_date
    and tc_company_id = p_bu_company_id
    and effective_dt          = p_effective_date;
  end if;

   -- if there is existing calerndar with the same date range then reuse, else create one
  if (v_parcel_rate_calendar_id > 0) then
    delete
    from parcel_rate_data
    where parcel_rate_calender_id = v_parcel_rate_calendar_id
    and SERVICE_LEVEL_ID in (select service_level_id from service_level where tc_company_id=p_bu_company_id);
  else

     -- if overlapping calendars are there then adjust the dates of existing calendars
    update parcel_rate_calender
    set expiration_dt = p_effective_date - 1
    where carrier_id  = v_carrier_id
    and tc_company_id = p_bu_company_id
    and expiration_dt > p_effective_date
    and expiration_dt < p_expiration_date;

    update parcel_rate_calender
    set effective_dt  = p_expiration_date + 1
    where carrier_id  = v_carrier_id
    and tc_company_id = p_bu_company_id
    and effective_dt  > p_effective_date
    and expiration_dt > p_expiration_date;

    select seq_parcel_rate_calender_id.nextval
    into v_parcel_rate_calendar_id
    from dual ;

    insert
    into parcel_rate_calender
      (
        parcel_rate_calender_id,
        carrier_id ,
        tc_company_id,
        effective_dt,
        expiration_dt,
        mod_date_time,
        user_id
      )
      values
      (
        v_parcel_rate_calendar_id,
        v_carrier_id,
        p_bu_company_id,
        p_effective_date,
        p_expiration_date,
        sysdate,
        p_user_id
      );

  end if;
  for ratezonerecord in
  (select sl.tc_company_id tc_company_id,
      (select mot_id
      from mot
      where mot.tc_company_id = p_tc_company_id
      and mot.mot             = ipr.mot
      ) mot_id,
      sl.service_level_id service_level_id,
      ipr.tier tier ,
      substr(ipr.zone,1,3) zone,
      ipr.from_wt from_wt,
      ipr.to_wt to_wt,
      ipr.rate_calc_mthd rate_calc_mthd,
      ipr.incr_chrg ,
      ipr.min_chrg ,
      ipr.incrmnt ,
      ipr.calc_rate ,
      p_user_id
    from service_level sl
    join (select * from inpt_carrier_service where carrier_code=p_carrier_code) ics

    on sl.service_level = ics.service_level_type
    left join inpt_parcl_rate ipr
    on ics.serv_type       = ipr.serv_type
    where sl.tc_company_id in (p_sl_company_id,p_tc_company_id)
    and ipr.zone is not null
    order by ipr.from_wt ,
      ipr.zone
  )
  loop
    insert
    into parcel_rate_data
      (
        parcel_rate_data_id,
        tc_company_id ,
        mot_id ,
        service_level_id ,
        tier ,
        zone ,
        from_wt ,
        to_wt ,
        rate_calc_mthd ,
        incr_chrg ,
        min_chrg ,
        incrmnt ,
        calc_rate ,
        mod_date_time ,
        user_id ,
        created_date_time ,
        parcel_rate_calender_id
      )
      values
      (
        seq_parcel_rate_data_id.nextval,
        p_bu_company_id,
        ratezonerecord.mot_id,
        ratezonerecord.service_level_id,
        ratezonerecord.tier,
        ratezonerecord.zone,
        ratezonerecord.from_wt,
        ratezonerecord.to_wt,
        ratezonerecord.rate_calc_mthd,
        ratezonerecord.incr_chrg,
        ratezonerecord.min_chrg,
        ratezonerecord.incrmnt,
        ratezonerecord.calc_rate,
        sysdate,
        p_user_id,
        sysdate,
        v_parcel_rate_calendar_id
      );
  end loop;
end;
/
show errors ;
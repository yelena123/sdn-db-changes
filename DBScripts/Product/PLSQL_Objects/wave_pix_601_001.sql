create or replace procedure pix_601_001
(
    p_facilityid            in number,
    p_cd_master_id          in number,
    p_userid                in varchar2,
    p_orderid               in number,
    p_rc                    out number
)
is
    v_whse                  varchar2(10);
    v_proc_stat_code        varchar2(2);
    v_xml_group_attr        varchar2(10);
    v_tran_nbr              number;
    v_pix601001             varchar2(4000);
    v_tmp_string            varchar2(1000);
    v_tmp_ref               varchar2(1000);
    
begin    
    p_rc := 0;

    select whse
         into v_whse
   from facility_alias fa join facility f
        on fa.facility_id = f.facility_id
        and fa.facility_id = p_facilityid
        and fa.mark_for_deletion = 0
        and rownum < 2;
    
    manh_get_pix_config_data  ('601','001',null,p_cd_master_id,
        v_proc_stat_code,v_xml_group_attr); 
    
    if (v_proc_stat_code <> '91') 
    then

    v_pix601001 := '';
    v_pix601001 := 'insert into pix_tran ('||
        'item_name, pix_tran_id, tran_type, '||
        'tran_code, '||
        'tran_nbr, pix_seq_nbr, proc_stat_code, whse, '||
        'season, season_yr, style, '||
        'style_sfx, color, color_sfx, '||
        'sec_dim, qual, size_desc, '||
        'size_range_code,size_rel_posn_in_table,invn_type, '||
        'prod_stat, batch_nbr, sku_attr_1, '||
        'sku_attr_2, sku_attr_3, sku_attr_4, '||
        'sku_attr_5, cntry_of_orgn, '||
        'invn_adjmt_qty, '||
        'invn_adjmt_type, uom, actn_code, create_date_time, '||
        'mod_date_time, user_id, wm_version_id, '||
        'item_id, tc_company_id, xml_group_id ';
    
    v_tmp_String := '';
    
    manh_get_pix_ref_code_col_list('601','001',null,p_cd_master_id,v_tmp_ref,v_tmp_String);
    
    v_pix601001 := v_pix601001 || v_tmp_ref;

    if (v_tmp_string is null and p_cd_master_id is not null)
    then
    
        manh_get_pix_ref_code_col_list('601','001',null,null,v_tmp_ref,v_tmp_String);
        v_pix601001 := v_pix601001 || v_tmp_ref;
        
    end if;    
    
    select pix_tran_id_seq.nextval 
        into v_tran_nbr 
    from dual ;  
       
    v_pix601001 := v_pix601001 ||' ) select item_cbo.item_name,'||
        'pix_tran_id_seq.nextval , ''601'', ''001'', '||
        to_char(v_tran_nbr)||', rownum, '||
        to_char(v_proc_stat_code) ||', '''|| v_whse ||
        ''', item_cbo.item_season, item_cbo.item_season_year, '||
        'item_cbo.item_style, item_cbo.item_style_sfx, item_cbo.item_color, '||
        'item_cbo.item_color_sfx, item_cbo.item_second_dim,
        item_cbo.item_quality, '||
        'item_cbo.item_size_desc, item_wms.size_range_code,
        item_wms.size_rel_posn_in_table , '||
        'order_line_item.invn_type, order_line_item.prod_stat,
        order_line_item.batch_nbr, '||
        'order_line_item.item_attr_1, order_line_item.item_attr_2, 
        order_line_item.item_attr_3, '||
        'order_line_item.item_attr_4, order_line_item.item_attr_5,
        order_line_item.cntry_of_orgn, '||
        'order_line_item.allocated_qty, '''', '||
        'size_uom.size_uom, ''01'',current_timestamp, current_timestamp, '''||
        p_UserId ||''', 1, order_line_item.item_id, '||
        ' order_line_item.tc_company_id, '|| ( case when v_xml_group_attr is null then 'null' else ''''|| v_xml_group_attr ||''' ' end ) || v_tmp_String ||
        ' from orders '||
        ' inner join order_line_item 
        on order_line_item.order_id = orders.order_id '||
        ' and order_line_item.is_cancelled = ''0''' ||
        ' and orders.order_id = :order_id ' ||
        ' inner join item_cbo on item_cbo.item_id = order_line_item.item_id '||
        ' inner join item_wms on item_wms.item_id = item_cbo.item_id '||
        ' inner join size_uom on size_uom.size_uom_id = order_line_item.qty_uom_id_base ';

    execute immediate v_pix601001 using p_orderid;
    
    end if;
    
exception
    when no_data_found then
        null;
end pix_601_001;    
/
show errors;

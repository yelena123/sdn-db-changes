create or replace procedure manh_wave_order_selection
(
    p_user_id                   in  ucl_user.user_name%type,
    p_facility_id               in  whse_master.whse_master_id%type,
    p_whse                      in  facility.whse%type,
    p_lock_time_out             in  number,
    p_ship_wave_parm_id         in  ship_wave_parm.ship_wave_parm_id%type,
    p_list_of_shpmts	          in  varchar2,
    p_list_of_orders 	          in  varchar2,
    p_wave_by_rule              in  varchar2,
    p_perform_fill_capacity     in  number,
    p_preview_wave_flag	        in  number,
    p_debug_level               in  number,
    p_app_instance_id           in  number,
    p_is_lock_reqd              in  number,
    p_scale                     in  number,
    p_user_bu                   in  company.company_id%type
)
as
    v_pull_all_swc              wave_parm.pull_all_swc%type;
    v_perf_rte                  ship_wave_parm.perf_rte%type;
    v_perf_pickng_wave          ship_wave_parm.perf_pickng_wave%type;
    v_perf_pulldown_from_pnh    ship_wave_parm.perf_pulldown_from_pnh%type;
    v_sku_cnstr                 wave_parm.sku_cnstr%type;
    v_sku_sub                   wave_parm.sku_sub%type;
    v_wave_type_indic           wave_parm.wave_type_indic%type;
    v_reject_distro_rule        wave_parm.reject_distro_rule%type;
    v_repl_wave                 wave_parm.repl_wave%type;
    v_ship_wave_nbr             ship_wave_parm.ship_wave_nbr%type;
    v_pick_wave_nbr             ship_wave_parm.pick_wave_nbr%type;
    v_tc_company_id             wave_parm.tc_company_id%type := 0;
    v_remaining_order_lines     wave_parm.max_order_lines%type := 0;
    v_remaining_rte_nbrs        wave_parm.retail_max_routes%type := 0;
    v_remaining_store_nbrs      wave_parm.retail_max_stores%type := 0;
    v_remaining_items           wave_parm.retail_max_skus%type := 0;
    v_remaining_qty             wave_parm.max_units%type := 0;
    v_remaining_orders          wave_parm.max_orders%type := 0;
    v_remaining_vol             wave_parm.max_vol%type := 0;
    v_remaining_wt              wave_parm.max_wt%type := 0;
    v_max_rte_nbrs              wave_parm.retail_max_routes%type := 0;
    v_max_store_nbrs            wave_parm.retail_max_stores%type := 0;
    v_max_items                 wave_parm.retail_max_skus%type := 0;
    v_max_qty                   wave_parm.max_units%type := 0;
    v_max_orders                wave_parm.max_orders%type := 0;
    v_max_vol                   wave_parm.max_vol%type := 0;
    v_max_wt                    wave_parm.max_wt%type := 0;
    v_ship_via                  wave_parm.ship_via%type;
    v_max_order_lines           wave_parm.max_order_lines%type := 0;
    v_use_locking               number(1) := 0;
    v_lock_owner                number(4) := 1;
    v_process                   varchar2(250) := 'SELECTION';
    v_comments                  varchar2(1000) := 'WM1';
    v_source_type               number(2) := 1;
    v_source                    varchar2(50) := 'WAVE';
    v_ship_date_time            wave_parm.ship_date_time%type;
    v_zero                      number(5, 4) := case p_scale when 0 then 0
        else power(10, -p_scale)/2 end;
    v_rte_wave_option           opt_param.param_value%type; 
    v_chase_wave                wave_parm.chase_wave%type;
    v_max_log_lvl               number(1);
    v_code_id                   varchar2(3) := 'SEL';
    v_prev_module_name          wm_utils.t_app_context_data;
    v_prev_action_name          wm_utils.t_app_context_data;
begin
    select coalesce(wp.pull_all_swc, 'N'),
        coalesce(nullif(wp.retail_max_routes, 0), 999999999),
        coalesce(nullif(wp.retail_max_stores, 0), 999999999),
        coalesce(nullif(wp.retail_max_skus, 0), 999999999),
        wp.tc_company_id, coalesce(nullif(wp.max_units, 0), 999999999),
        coalesce(nullif(wp.max_vol, 0), 999999999), wp.repl_wave,
        coalesce(nullif(wp.max_wt, 0), 999999999),
        coalesce(wp.sku_cnstr, '3'), coalesce(wp.sku_sub, '0'),
        coalesce(wp.wave_type_indic, '1'),
        coalesce(wp.reject_distro_rule, '4'), swp.ship_wave_nbr,
        swp.pick_wave_nbr, coalesce(swp.perf_pickng_wave, 'N'),
        case when swp.perf_rte in ('1', '2') then '1' else '0' end, 
        coalesce(swp.perf_pulldown_from_pnh, 'N'),
        coalesce(nullif(wp.max_orders, 0), 999999999), wp.ship_via,
        case when wp.force_ship_date_flag > 0
            then coalesce(wp.ship_date_time, sysdate) else null end,
        coalesce(nullif(wp.max_order_lines, 0), 999999999),
        coalesce(wp.chase_wave, 'N')
    into v_pull_all_swc, v_max_rte_nbrs, v_max_store_nbrs, v_max_items,
        v_tc_company_id, v_max_qty, v_max_vol, v_repl_wave, v_max_wt,
        v_sku_cnstr, v_sku_sub, v_wave_type_indic, v_reject_distro_rule,
        v_ship_wave_nbr, v_pick_wave_nbr, v_perf_pickng_wave, v_perf_rte,
        v_perf_pulldown_from_pnh, v_max_orders, v_ship_via, v_ship_date_time,
        v_max_order_lines, v_chase_wave
    from ship_wave_parm swp
    left join wave_parm wp on wp.wave_parm_id = swp.wave_parm_id
    where swp.ship_wave_parm_id = p_ship_wave_parm_id;

    select to_number(coalesce(trim(substr(sc.misc_flags, 8, 1)), '0')),
        case when trim(substr(sc.misc_flags, 2, 1)) = 'Y' then 1 else 0 end
    into v_max_log_lvl, v_use_locking
    from sys_code sc 
    where sc.rec_type = 'S' and sc.code_type = '520' and sc.code_id = v_code_id;

    delete from tmp_log_parm;
    insert into tmp_log_parm
    (
        log_lvl, pgm_id, module, msg_id, ref_code_1, ref_value_1, user_id, whse,
        cd_master_id
    )
    values
    (
        v_max_log_lvl, 'manh_wave_order_selection', 'WAVE', '1094', v_code_id, v_ship_wave_nbr, 
        p_user_id, p_whse, coalesce(v_tc_company_id, wm_utils.wm_get_user_bu(p_user_id))
    );
    	
    wm_utils.get_context_info(v_prev_module_name, v_prev_action_name);
    wm_utils.set_context_info(p_module_name => 'WAVE', p_action_name => 'SELECTION',
        p_client_id => v_ship_wave_nbr);
		
    wm_cs_log('Beginning Wave Selection', p_sql_log_level => 0);

    select coalesce(op.param_value, '1')
    into v_rte_wave_option
    from ship_wave_parm swp
    left join cons_template ct on ct.cons_template_id = swp.rte_wave_parm_id
    left join opt_param op on op.tc_company_id = ct.tc_company_id
        and op.opt_param_list_id = ct.opt_param_list_id
        and op.param_def_id ='routing_run_option'
        and op.param_group_id = 'CON_TEPE'
    -- check: and op.instance_num = ?
    where swp.ship_wave_parm_id = p_ship_wave_parm_id;
    
    insert into tmp_wave_selection_parms
    (
        ship_wave_nbr
    )
    values
    (
        v_ship_wave_nbr
    );

    v_lock_owner := p_app_instance_id;

--Removed the do_dtl_status to 112  update block as per CR : MACR00828891  Jira : WM-WM-27048

    -- if a vas line exists in an order that is not vas-complete, all non-vas 
    -- lines in the same order are set to held by the cbo import prior to waving
    manh_wave_load_selection_rules(p_wave_by_rule, p_ship_wave_parm_id,
        case when p_list_of_shpmts is null then 'N' else 'Y' end,
        p_perform_fill_capacity, p_preview_wave_flag, v_chase_wave);

    if (p_wave_by_rule = 'Y' and v_perf_pickng_wave = '1')
    then
        -- init running values of wave and work_type cap maximums
        v_remaining_rte_nbrs := v_max_rte_nbrs;
        v_remaining_store_nbrs := v_max_store_nbrs;
        v_remaining_items := v_max_items;
        v_remaining_qty := v_max_qty;
        v_remaining_orders := v_max_orders;
        v_remaining_wt := v_max_wt;
        v_remaining_vol := v_max_vol;
        v_remaining_order_lines := v_max_order_lines;

        insert into tmp_wave_work_type_stats
        (
            work_type, remaining_sams, remaining_units, remaining_lines,
            remaining_piks, remaining_dlrs, remaining_lpns, max_sams,
            max_units, max_lines, max_piks, max_dlrs, max_lpns
        )
        select wwts.work_type,
            coalesce(nullif(wwts.sams_capcty,0),9999999) remaining_sams,
            coalesce(nullif(wwts.units_capcty, 0), 9999999) remaining_units,
            coalesce(nullif(wwts.lines_capcty,0),999999999) remaining_lines,
            coalesce(nullif(wwts.piks_capcty, 0), 999999999) remaining_piks,
            coalesce(nullif(wwts.dlrs_capcty, 0), 999999999) remaining_dlrs,
            coalesce(nullif(wwts.carton_capcty, 0), 999999999) remaining_lpns,
            coalesce(nullif(wwts.sams_capcty,0),9999999) max_sams,
            coalesce(nullif(wwts.units_capcty, 0), 9999999) max_units,
            coalesce(nullif(wwts.lines_capcty, 0), 999999999) max_lines,
            coalesce(nullif(wwts.piks_capcty, 0), 999999999) max_piks,
            coalesce(nullif(wwts.dlrs_capcty, 0), 999999999) max_dlrs,
            coalesce(nullif(wwts.carton_capcty, 0), 999999999) max_lpns
        from wave_work_type_stats wwts
        where wwts.wave_parm_id = p_ship_wave_parm_id;
    end if; -- wave by rule and picking wave

    for rule_rec in
    (
        select t.rule_id, t.units_capcty max_qty_per_rule, t.is_wave_by_shpmt,
            t.max_orders max_orders_per_rule, t.rule_type,
            t.max_order_lines max_lines_per_rule
        from tmp_wave_selection_rules t
        order by t.prty
    )
    loop
        execute immediate 'truncate table tmp_wave_sku_invn_cache';
        execute immediate 'truncate table tmp_wave_selected_orders';
        execute immediate 'truncate table tmp_wave_rejected_lines';
        execute immediate 'truncate table tmp_wave_prepack_components';
        execute immediate 'truncate table tmp_wave_molpn_orders';

        manh_wave_load_orders_for_rule(p_wave_by_rule, p_list_of_shpmts,
            p_list_of_orders, p_facility_id, v_perf_pickng_wave, v_perf_rte,
            p_preview_wave_flag, v_wave_type_indic, v_tc_company_id,
            v_pull_all_swc, rule_rec.rule_id, rule_rec.rule_type,
            rule_rec.max_qty_per_rule, rule_rec.max_lines_per_rule,
            rule_rec.is_wave_by_shpmt, rule_rec.max_orders_per_rule, p_user_id,
            p_whse, v_ship_wave_nbr, v_pick_wave_nbr, v_chase_wave);

        if (p_is_lock_reqd = 1)
        then
            if (p_preview_wave_flag = 2)
            then
                -- repopulate for any subsequent unlocking; these were locked by
                -- the prewave earlier
                intg_insert_orders(v_lock_owner);
            else
                intg_apply_intg_locks(v_lock_owner, v_process, v_comments,
                    v_source, v_source_type);
                delete from tmp_wave_selected_orders t
                where exists
                (
                    select 1
                    from tmp_intg_orders t1
                    where t1.order_id = t.order_id and is_failed = 1
                );
            end if;
        end if;

        if (p_preview_wave_flag = 2)
        then
            -- convert; move line items from prewaved to regular statuses
            update order_line_item oli
            set oli.last_updated_source = p_user_id,
                oli.last_updated_dttm = sysdate,
                oli.wave_nbr = v_pick_wave_nbr,
                oli.ship_wave_nbr = v_ship_wave_nbr,
                oli.do_dtl_status =
                (
                    case
                        when oli.order_qty <= v_zero then 200
                        when oli.allocated_qty <= v_zero then 110
                        when oli.order_qty - oli.allocated_qty <= v_zero then
                            case
                                when coalesce(oli.units_pakd, 0) <= v_zero
                                    then 130
                                when oli.order_qty - coalesce(oli.shipped_qty, 0)
                                    <= v_zero then 190
                                when oli.order_qty - (coalesce(oli.units_pakd, 0)
                                    - coalesce(oli.user_canceled_qty, 0))
                                    <= v_zero then 150
                                else 140
                            end
                        else 120
                    end
                )
            where oli.do_dtl_status = 115
                and exists
                (
                    select 1
                    from tmp_wave_selected_orders t
                    where t.order_id = oli.order_id
                        and t.line_item_id = oli.line_item_id
                );
            wm_cs_log('Converted OLI from pre-waved status ' || sql%rowcount);
                
            if (p_is_lock_reqd = 1)
            then
                intg_unlock_rejected_orders(v_pick_wave_nbr);
            end if;
        elsif (v_perf_pickng_wave = '1')
        then
            update tmp_wave_selection_parms t
            set t.curr_rule_type = rule_rec.rule_type;

            manh_wave_load_prepack_data();

            if (v_sku_cnstr = '3')
            then
                -- for all selected orders, assume fully soft allocated
                insert into tmp_ord_dtl_sku_invn
                (
                    line_item_id, item_id, invn_type, prod_stat,
                    cntry_of_orgn, batch_nbr, item_attr_1, item_attr_2,
                    item_attr_3, item_attr_4, item_attr_5, order_id,
                    qty_soft_alloc, rng_shortage, is_flushed, ship_group_id,
                    is_split_line
                )
                select t.line_item_id, t.item_id, t.invn_type, t.prod_stat,
                    t.cntry_of_orgn, t.batch_nbr, t.item_attr_1, t.item_attr_2,
                    t.item_attr_3, t.item_attr_4, t.item_attr_5, t.order_id,
                    round(t.need_qty, p_scale) qty_soft_alloc, 0 rng_shortage,
                    0 is_flushed, t.ship_group_id, case t.do_dtl_status when 120
                        then 1 else 0 end is_split_line
                from tmp_wave_selected_orders t
                where t.pre_pack_flag = 0
                union all
                -- mark all prepack components as split lines/new items
                select t.line_item_id, t.item_id, t.invn_type, t.prod_stat,
                    t.cntry_of_orgn, t.batch_nbr, t.item_attr_1, t.item_attr_2,
                    t.item_attr_3, t.item_attr_4, t.item_attr_5, t.order_id,
                    t.qty_reqd qty_soft_alloc, 0 rng_shortage, 0 is_flushed,
-- todo: replace with swc of prepack line
                    null ship_group_id, 2 is_split_line
                from tmp_wave_prepack_components t;
                wm_cs_log('Soft allocation bypassed for OLI ' || sql%rowcount);
            else
                -- need to perform soft alloc
                manh_wave_refresh_sku_invn(p_facility_id, p_whse, v_sku_sub,
                    v_pick_wave_nbr, p_scale, v_zero);

                manh_wave_sku_invn(v_sku_cnstr, v_sku_sub, v_pull_all_swc,
                    v_reject_distro_rule, v_zero, p_scale, p_facility_id);

                manh_wave_sku_invn_rejections(p_user_id, v_sku_cnstr,
                    v_pull_all_swc, v_reject_distro_rule);
            end if;

            if (p_wave_by_rule = 'Y')
            then
                manh_wave_check_all_capacities(p_user_id, v_pull_all_swc,
                    v_reject_distro_rule, v_max_qty, v_max_vol, v_max_wt,
                    v_max_rte_nbrs, v_max_store_nbrs, v_max_items, v_max_orders,
                    v_max_order_lines, v_remaining_rte_nbrs, v_remaining_store_nbrs,
                    v_remaining_items, v_remaining_qty, v_remaining_vol,
                    v_remaining_wt, v_remaining_orders, v_remaining_order_lines);
            end if;

            -- flush all tmp table updates to the main tables, for this rule
            manh_complete_soft_allocation(v_ship_wave_nbr, v_pick_wave_nbr,
                v_sku_cnstr, p_user_id, p_preview_wave_flag, v_repl_wave,
                v_use_locking, v_zero, p_scale, v_chase_wave);

            if (p_is_lock_reqd = 1 and p_preview_wave_flag = 0)
            then
                intg_unlock_rejected_orders(v_pick_wave_nbr);
            end if;

            if (p_wave_by_rule = 'Y' and (v_remaining_qty <= v_zero
                or v_remaining_order_lines < 1 or v_remaining_wt < v_zero
                or v_remaining_vol < v_zero or v_remaining_orders < 1))
            then
                exit;
            end if;
        elsif (v_perf_rte = '1')
        then
            -- update prev routing waves on selected orders to partially done
            update cons_run cr
            set cr.postrun_status = 'PartlyRejected',
                cr.last_updated_dttm = sysdate
            where coalesce(cr.postrun_status, ' ') not in ('Rejected', 'PartlyRejected')
                and cr.rte_wave_nbr in
                (
                    select o.rte_wave_nbr
                    from orders o
                    join tmp_wave_selected_orders t on t.order_id = o.order_id
                );
            wm_cs_log('Cons_run updated ' || sql%rowcount);

            -- now stamp current routing wave
            update orders o
            set o.rte_wave_nbr = v_ship_wave_nbr, o.last_updated_dttm = sysdate,
                o.last_updated_source = p_user_id
            where exists
            (
                select 1
                from tmp_wave_selected_orders t
                where t.order_id = o.order_id
            );
            wm_cs_log('Updated rte wave nbr on orders ' || sql%rowcount);
        end if; -- prewave/picking/routing wave
        
        if (v_perf_pulldown_from_pnh = '1' and v_perf_pickng_wave = '0') 
		    then
            update order_line_item oli
            set oli.ship_wave_nbr = v_ship_wave_nbr,
                oli.last_updated_dttm = sysdate,
                oli.last_updated_source = p_user_id
            where exists
            (
                select 1
                from tmp_wave_selected_orders t
                where t.order_id = oli.order_id
                    and t.line_item_id = oli.line_item_id
            );
            wm_cs_log('PNH wave stamped on OLI ' || sql%rowcount);

            manh_wave_load_molpn_orders(p_wave_by_rule, p_list_of_shpmts, p_list_of_orders, 
                p_facility_id, v_perf_rte, v_tc_company_id, rule_rec.rule_id, rule_rec.rule_type,
                v_rte_wave_option);

            update lpn
            set lpn.wave_nbr = v_ship_wave_nbr,
                lpn.last_updated_dttm = sysdate,
                lpn.last_updated_source = p_user_id
            where lpn.lpn_facility_status < 90 and lpn.non_inventory_lpn_flag = 1
                and exists
                (
                    select 1
                    from tmp_wave_molpn_orders t
                    where t.order_id = lpn.order_id
                );
            wm_cs_log('PNH wave stamped on lpns ' || sql%rowcount);
        end if; -- pnh wave

        commit;
    end loop; -- all rules

    if (v_perf_pickng_wave = '1')
    then
        manh_wave_complete_selection(p_facility_id, p_user_id, p_whse, p_preview_wave_flag, 
            v_ship_wave_nbr, v_pick_wave_nbr, v_perf_rte, v_sku_cnstr, v_ship_via,
            coalesce(v_tc_company_id, wm_utils.wm_get_user_bu(p_user_id)), v_ship_date_time,
            p_debug_level, v_repl_wave);
    end if;

    commit;
    wm_cs_log('Completed Wave Selection', p_sql_log_level => 0);
	
	  wm_utils.set_context_info(p_module_name => v_prev_module_name,
        p_action_name => v_prev_action_name);
end;
/
show errors;
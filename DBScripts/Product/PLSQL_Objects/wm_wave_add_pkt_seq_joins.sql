create or replace procedure wm_wave_add_pkt_seq_joins
(
    p_pkt_seq_sql in out varchar2
)
as
begin
    if (instr(p_pkt_seq_sql, 'ORDERS.', 1, 1) > 0 or instr(p_pkt_seq_sql, 'FACILITY.', 1, 1) > 0 )
    then
        p_pkt_seq_sql := replace(p_pkt_seq_sql, 'where ',
            'join orders on orders.tc_order_id = pkt_hdr.original_tc_order_id '
            || ' and orders.tc_company_id = pkt_hdr.cd_master_id where ');
    end if;

    if (instr(p_pkt_seq_sql, 'FACILITY.', 1, 1) > 0)
    then
        p_pkt_seq_sql := replace(p_pkt_seq_sql, 'where ',
            'join facility on facility.facility_id = pkt_hdr.d_facility_id where ');
    end if;

    if (instr(p_pkt_seq_sql, 'ORDER_LINE_ITEM.', 1, 1) > 0)
    then
        p_pkt_seq_sql := replace(p_pkt_seq_sql, 'where ',
            'join order_line_item on order_line_item.line_item_id = pkt_dtl.reference_line_item_id'
            || ' and order_line_item.order_id = pkt_dtl.reference_order_id where ');
    end if;
    
    if (instr(p_pkt_seq_sql, 'ITEM_CBO', 1, 1) > 0)
    then
        p_pkt_seq_sql := replace(p_pkt_seq_sql, 'where ',
            'join item_cbo on item_cbo.item_id = pkt_dtl.item_id where ');
    end if;

    if (instr(p_pkt_seq_sql, 'ITEM_WMS', 1, 1) > 0)
    then
        p_pkt_seq_sql := replace(p_pkt_seq_sql, 'where ',
            'join item_wms on item_wms.item_id = pkt_dtl.item_id where ');
    end if;

    if (instr(p_pkt_seq_sql, 'ORDER_NOTE', 1, 1) > 0)
    then
        p_pkt_seq_sql := replace(p_pkt_seq_sql, 'where ',
            'join order_note on order_note.order_id = pkt_dtl.reference_order_id'
            || ' and (order_note.line_item_id = pkt_dtl.reference_line_item_id or order_note.line_item_id = 0) where ');
    end if;
end;
/
show errors;

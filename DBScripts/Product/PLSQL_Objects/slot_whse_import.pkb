create or replace package body so_whse_import
is
    c_max_lanes             constant number(2) := 99;
    c_num_wm_hist_days      constant number(2) := 7;
    c_num_item_hist_days    constant number(2) := 6;
    c_hi_res_mask           constant number(1) := 8;
    c_hi_res_mask_comp      constant number(1) := -9;
    c_pgm_id                constant val_job_seq.pgm_id%type := 'so_whse_import';

    c_bad_ship_unit         constant exceptions.msg_number%type := 204;
    c_bad_slot_unit_for_rt  constant exceptions.msg_number%type := 211;
    c_bad_bin_for_rt        constant exceptions.msg_number%type := 217;
    c_bad_plt_for_rt        constant exceptions.msg_number%type := 221;
    c_unknown_slot_unit     constant exceptions.msg_number%type := 233;
    
    c_no_uom                constant slot_item.slot_unit%type := 0;
    c_plt_uom               constant slot_item.slot_unit%type := 1;
    c_case_uom              constant slot_item.slot_unit%type := 2;
    c_inner_uom             constant slot_item.slot_unit%type := 4;
    c_each_uom              constant slot_item.slot_unit%type := 8;
    c_bin_uom               constant slot_item.slot_unit%type := 16;

    type t_row_num          is table of import_temp.row_num%type index by binary_integer;
    ga_row_num              t_row_num;
    g_user_name             exceptions.mod_user%type;
    g_whse                  locn_hdr.whse%type;
    g_is_standalone         number(1);
    g_is_weekly             number(1);
    g_facility_id           facility.facility_id%type;
    
    g_wip_each_mask         slot_item.slot_unit%type;
    g_wip_case_mask         slot_item.slot_unit%type;
    g_wip_inner_mask        slot_item.slot_unit%type;
    g_wip_plt_mask          slot_item.slot_unit%type;
    
    g_case_any_pb           bins.code%type;
    g_each_any_pb           bins.code%type;
    g_inner_any_pb          bins.code%type;
    g_plt_any_pb            bins.code%type;
    g_case_any_pp           pallets.plt_type%type;
    g_each_any_pp           pallets.plt_type%type;
    g_inner_any_pp          pallets.plt_type%type;
    g_plt_any_pp            pallets.plt_type%type;

    g_inv_ship_unit_msg         exceptions.descript%type;
    g_inv_slot_unit_for_rt_msg  exceptions.descript%type;
    g_inv_plt_for_rt_msg        exceptions.descript%type;
    g_inv_bin_for_rt_msg        exceptions.descript%type;
    g_unknown_slot_unit_msg     exceptions.descript%type;

    function bitor
    (
        p_n1                in number,
        p_n2                in number
    )
    return number
    is
    begin
        return coalesce(p_n1, 0) + coalesce(p_n2, 0) - bitand(coalesce(p_n1, 0), coalesce(p_n2, 0));
    end;

    procedure extract_pref_plts_and_bins
    as
        v_curr_token            varchar2(12);
        v_remaining_str         whse_import_prefs.default_case_slot_uom%type;
        v_pref_data             varchar2(1);
        v_input_delimiter_pos   number (5);
    begin
        for wip_rec in
        (
            select wip.default_each_slot_uom csv, c_each_uom ship_unit
            from whse_import_prefs wip
            where wip.whse_code = g_whse
            union all
            select wip.default_case_slot_uom csv, c_case_uom ship_unit
            from whse_import_prefs wip
            where wip.whse_code = g_whse
            union all
            select wip.default_ip_slot_uom csv, c_inner_uom ship_unit
            from whse_import_prefs wip
            where wip.whse_code = g_whse
            union all
            select wip.default_plt_slot_uom csv, c_plt_uom ship_unit
            from whse_import_prefs wip
            where wip.whse_code = g_whse
        )
        loop
            v_pref_data := null;
            v_remaining_str := wip_rec.csv;
            loop
-- check: need to trim? assumes no spaces
                v_input_delimiter_pos := coalesce(instr(v_remaining_str, ',', 1, 1), 0);
                if (v_input_delimiter_pos = 0)
                then
                    v_curr_token := v_remaining_str;
                    v_remaining_str := '';
                else
                    v_curr_token := substr(v_remaining_str, 1, v_input_delimiter_pos - 1);
                    v_remaining_str := substr(v_remaining_str, v_input_delimiter_pos + 1);
                end if;
            
                if (v_curr_token = 'P')
                then
                    v_pref_data := 'P';
                elsif (v_curr_token = 'B')
                then
                    v_pref_data := 'B';
                elsif (v_curr_token in ('C', 'E', 'I'))
                then
                    v_pref_data := null;
                end if;
                
                if (v_pref_data = 'P' and v_curr_token != 'P')
                then
                    case wip_rec.ship_unit
                        when c_case_uom then g_case_any_pp := v_curr_token;
                        when c_each_uom then g_each_any_pp := v_curr_token;
                        when c_inner_uom then g_inner_any_pp := v_curr_token;
                        when c_plt_uom then g_plt_any_pp := v_curr_token;
                        else null;
                    end case;
    
                    insert into tmp_pref_plts
                    (
                        ship_unit, pallet_id
                    )
                    select wip_rec.ship_unit, p.pallet_id
                    from pallets p
                    where p.plt_type = v_curr_token and p.whse_code = g_whse;
                elsif (v_pref_data = 'B' and v_curr_token != 'B')
                then
                    case wip_rec.ship_unit
                        when c_case_uom then g_case_any_pb := v_curr_token;
                        when c_each_uom then g_each_any_pb := v_curr_token;
                        when c_inner_uom then g_inner_any_pb := v_curr_token;
                        when c_plt_uom then g_plt_any_pb := v_curr_token;
                        else null;
                    end case;
    
                    insert into tmp_pref_bins
                    (
                        ship_unit, bin_id
                    )
                    select wip_rec.ship_unit, b.bin_id
                    from bins b
                    where b.code = v_curr_token and b.whse_code = g_whse;
                end if;
        
                if (v_input_delimiter_pos = 0)
                then
                    exit;
                end if;
            end loop;
        end loop;
    end;

    function uom_csv_to_mask
    (
        p_uom_csv           in varchar2
    )
    return number
    is
        v_each_exists       number(1);
        v_case_exists       number(1);
        v_inner_exists      number(1);
        v_plt_exists        number(1);
        v_bit_mask          slot_item.slot_unit%type := c_no_uom;
    begin
        -- bins do not have an associated mask
        select max(decode(t.column_value, 'E', 1, 0)), max(decode(t.column_value, 'C', 1, 0)),
            max(decode(t.column_value, 'I', 1, 0)), max(decode(t.column_value, 'P', 1, 0))
        into v_each_exists, v_case_exists, v_inner_exists, v_plt_exists
        from table(cl_parse_endpoint_name_csv(p_uom_csv)) t
        where t.column_value in ('E', 'C', 'I', 'B', 'P');
        
        v_bit_mask := bitor(v_bit_mask, case v_each_exists when 1 then c_each_uom else 0 end);
        v_bit_mask := bitor(v_bit_mask, case v_case_exists when 1 then c_case_uom else 0 end);
        v_bit_mask := bitor(v_bit_mask, case v_inner_exists when 1 then c_inner_uom else 0 end);
        v_bit_mask := bitor(v_bit_mask, case v_plt_exists when 1 then c_plt_uom else 0 end);

        return v_bit_mask;
    end;

    function get_slot_unit
    (
        p_rtl_uom_mask      in slot_item.slot_unit%type,
        p_slot_unit         in slot_item.slot_unit%type,
        p_item_uom_mask     in slot_item.slot_unit%type,
        p_ship_unit         in slot_item.ship_unit%type
    )
    return slot_item.slot_unit%type
    is
        v_rtl_uom_mask      slot_item.slot_unit%type := coalesce(p_rtl_uom_mask, 0);
        v_slot_unit         slot_item.slot_unit%type := coalesce(p_slot_unit, 0);
        v_item_uom_mask     slot_item.slot_unit%type := coalesce(p_item_uom_mask, 0);
        v_largest_uom       slot_item.slot_unit%type;
        v_uom_mask          slot_item.slot_unit%type;
    begin
        if (v_slot_unit > 0 and bitand(v_rtl_uom_mask, v_slot_unit) = v_slot_unit)
        then
            -- direct match: passed in value is allowed for rack type
            v_largest_uom := v_slot_unit;
        else
            -- begin unknown uom flow; start with matching item mask with rack type
            v_uom_mask := bitand(v_rtl_uom_mask, v_item_uom_mask);
            if (v_uom_mask = 0)
            then
                -- if no match, next match import prefs mask with rack type; note - bins do not have
                -- an associated mask
                v_uom_mask := bitand(v_rtl_uom_mask, 
                    case p_ship_unit when c_each_uom then g_wip_each_mask 
                        when c_case_uom then g_wip_case_mask when c_inner_uom then g_wip_inner_mask
                        when c_plt_uom then g_wip_plt_mask else c_no_uom end);
                if (v_uom_mask = 0)
                then
                    -- or fall through to just using the mask on the rack type
                    v_uom_mask := v_rtl_uom_mask;
                end if;
            end if;

            -- from the above mask, determine a single slot unit based on a fixed functional prty
            if (bitand(v_uom_mask, c_plt_uom) = c_plt_uom)
            then
                v_largest_uom := c_plt_uom;
            elsif (bitand(v_uom_mask, c_case_uom) = c_case_uom)
            then
                v_largest_uom := c_case_uom;
            elsif (bitand(v_uom_mask, c_bin_uom) = c_bin_uom)
            then
                v_largest_uom := c_bin_uom;
            elsif (bitand(v_uom_mask, c_inner_uom) = c_inner_uom)
            then
                v_largest_uom := c_inner_uom;
            elsif (bitand(v_uom_mask, c_each_uom) = c_each_uom)
            then
                v_largest_uom := c_each_uom;
            else
                v_largest_uom := null;
            end if;
        end if;
        
        return v_largest_uom;
    end get_slot_unit;
    
    procedure log_exception_for_msg
    (
        p_msg_txt       in exceptions.descript%type,
        p_msg_nbr       in exceptions.msg_number%type
    )
    is
    begin
        forall i in 1..ga_row_num.count
        insert into exceptions
        (
            ex_id, whse_code, slot, item, descript, msg_number, create_date_time, mod_date_time,
            mod_user
        )
        select seq_exceptions_id.nextval, g_whse, it.dsp_slot, it.sku_name, p_msg_txt, p_msg_nbr, sysdate, sysdate, g_user_name
        from import_temp it
        where it.row_num = ga_row_num(i);
    end log_exception_for_msg;

    procedure whse_import_init
    (
        p_whse              in locn_hdr.whse%type,
        p_user_name         in varchar2,
        p_is_standalone     in number,
        p_is_weekly         in number,
        p_log_lvl           in number
    )
    is
        v_tc_company_id     facility.tc_company_id%type;
    begin
        g_whse := p_whse;
        g_user_name := p_user_name;
        g_is_weekly := p_is_weekly;
        g_is_standalone := p_is_standalone;

        select f.facility_id, f.tc_company_id
        into g_facility_id, v_tc_company_id
        from facility f
        where f.whse = g_whse;

        select smm.msg_text
        into g_inv_ship_unit_msg
        from so_message_master smm 
        where smm.msg_number = c_bad_ship_unit;
        
        select smm.msg_text
        into g_inv_slot_unit_for_rt_msg
        from so_message_master smm 
        where smm.msg_number = c_bad_slot_unit_for_rt;

        select smm.msg_text
        into g_inv_plt_for_rt_msg
        from so_message_master smm 
        where smm.msg_number = c_bad_plt_for_rt;

        select smm.msg_text
        into g_inv_bin_for_rt_msg
        from so_message_master smm 
        where smm.msg_number = c_bad_bin_for_rt;

        select smm.msg_text
        into g_unknown_slot_unit_msg
        from so_message_master smm 
        where smm.msg_number = c_unknown_slot_unit;

        delete from tmp_log_parm;
        insert into tmp_log_parm
        (
            log_lvl, pgm_id, module, msg_id, ref_code_1, user_id, whse, cd_master_id, ref_value_1
        )
        values
        (
            p_log_lvl, c_pgm_id, 'SYSCONTROL', '1000', 'WIM', substr(g_user_name, 1, 15), g_whse, 
            v_tc_company_id, 'Weekly=' || to_char(g_is_weekly) || ',Standalone=' || to_char(g_is_standalone)
        );

        select uom_csv_to_mask(wip.default_each_slot_uom), uom_csv_to_mask(wip.default_case_slot_uom),
            uom_csv_to_mask(wip.default_ip_slot_uom), uom_csv_to_mask(wip.default_plt_slot_uom)
        into g_wip_each_mask, g_wip_case_mask, g_wip_inner_mask, g_wip_plt_mask
        from whse_import_prefs wip
        where wip.whse_code = g_whse;

        delete from inpt_slotinfo_width where facility_id = g_facility_id;
        delete from inpt_slotinfo_move_list where facility_id = g_facility_id;
        
        extract_pref_plts_and_bins();
    end whse_import_init;

    procedure validate_slot_unit
    is
    begin
        -- 8a i) custom slot unit match: validate plt/bin via rpx
        update tmp_slot_item t
        set t.pallet_id = null, t.slot_unit = c_case_uom
        where t.pallet_id is not null
            and not exists
            (
                select 1
                from rack_pallet_xref rpx
                where rpx.rack_level_id = t.rack_level_id and rpx.pallet_id = t.pallet_id
            )
        returning t.row_num
        bulk collect into ga_row_num;
        wm_cs_log('Invalid custom plt for rack type; defaulted to Case ' || sql%rowcount);            
        log_exception_for_msg(g_inv_slot_unit_for_rt_msg, c_bad_slot_unit_for_rt);

        update tmp_slot_item t
        set t.bin_id = null, t.slot_unit = c_case_uom
        where t.bin_id is not null
            and not exists
            (
                select 1
                from rack_bin_xref rbx
                where rbx.rack_level_id = t.rack_level_id and rbx.bin_id = t.bin_id
            )
        returning t.row_num
        bulk collect into ga_row_num;
        wm_cs_log('Invalid custom bin for rack type; defaulted to Case ' || sql%rowcount);            
        log_exception_for_msg(g_inv_slot_unit_for_rt_msg, c_bad_slot_unit_for_rt);

        -- 8b i) direct slot unit match: find plt/bin via sku_id; look in pallet_xref and bin_xref;
        -- validate via rpx, rbx
        merge into tmp_slot_item t
        using
        (
            select t.item_facility_mapping_id, min(rpx.pallet_id) pallet_id, min(rbx.bin_id) bin_id
            from tmp_slot_item t
            left join pallet_xref px on t.slot_unit = c_plt_uom 
                and px.sku_id = t.item_facility_mapping_id
            left join rack_pallet_xref rpx on rpx.rack_level_id = t.rack_level_id
                and rpx.pallet_id = px.pallet_id
            left join bin_xref bx on t.slot_unit = c_bin_uom 
                and bx.sku_id = t.item_facility_mapping_id
            left join rack_bin_xref rbx on rbx.rack_level_id = t.rack_level_id
                and rbx.bin_id = bx.bin_id
            where t.slot_unit in (c_plt_uom, c_bin_uom) and t.item_facility_mapping_id is not null
                and decode(t.slot_unit, c_plt_uom, t.pallet_id, t.bin_id) is null
            group by t.item_facility_mapping_id
            having min(px.pallet_id) is not null or min(bx.bin_id) is not null
        ) iv on (iv.item_facility_mapping_id = t.item_facility_mapping_id
            and t.slot_unit in (c_plt_uom, c_bin_uom))
        when matched then
        update set t.pallet_id = decode(t.slot_unit, c_plt_uom, iv.pallet_id, null),
            t.bin_id = decode(t.slot_unit, c_bin_uom, iv.bin_id, null)
        where decode(t.slot_unit, c_plt_uom, t.pallet_id, t.bin_id) is null;
        wm_cs_log('Determined plt/bin via sku_id ' || sql%rowcount);

        -- 8b ii, iii) direct slot unit match: use wip for plt/bin; validate via rpx/rbx
        merge into tmp_slot_item t
        using
        (
            select t.rack_level_id, t.ship_unit, min(rpx.pallet_id) any_rpx_id, 
                min(rbx.bin_id) any_rbx_id, min(tpp.pallet_id) pref_plt_id, 
                min(tpb.bin_id) pref_bin_id
            from tmp_slot_item t
            left join rack_pallet_xref rpx on t.slot_unit = c_plt_uom 
                and rpx.rack_level_id = t.rack_level_id
            left join tmp_pref_plts tpp on tpp.ship_unit = t.ship_unit 
                and tpp.pallet_id = rpx.pallet_id
            left join rack_bin_xref rbx on t.slot_unit = c_bin_uom 
                and rbx.rack_level_id = t.rack_level_id
            left join tmp_pref_bins tpb on tpb.ship_unit = t.ship_unit and tpb.bin_id = rbx.bin_id
            where t.slot_unit in (c_plt_uom, c_bin_uom) and t.rack_level_id is not null
                and decode(t.slot_unit, c_plt_uom, t.pallet_id, t.bin_id) is null
            group by t.rack_level_id, t.ship_unit
        ) iv on (t.slot_unit in (c_plt_uom, c_bin_uom) and iv.rack_level_id = t.rack_level_id 
            and coalesce(iv.ship_unit, -1) = coalesce(t.ship_unit, -1))
        when matched then
        update set t.pallet_id = decode(t.slot_unit, c_plt_uom, coalesce(iv.pref_plt_id, iv.any_rpx_id), null),
            t.bin_id = decode(t.slot_unit, c_bin_uom, coalesce(iv.pref_bin_id, iv.any_rbx_id), null)
        where decode(t.slot_unit, c_plt_uom, t.pallet_id, t.bin_id) is null;
        wm_cs_log('Determined plt/bin via whse import prefs /any rpx/any rbx ' || sql%rowcount);

        -- 8b iv) default plt/bin to any pref plt/bin
        update tmp_slot_item t
        set t.pallet_id =
        (
            case when t.slot_unit = c_plt_uom then
                decode(t.ship_unit, c_case_uom, g_case_any_pp, c_each_uom, g_each_any_pp, 
                    c_inner_uom, g_inner_any_pp, c_plt_uom, g_plt_any_pp)
                else null end
        ),
        t.bin_id =
        (
            case when t.slot_unit = c_bin_uom then
                decode(t.ship_unit, c_case_uom, g_case_any_pb, c_each_uom, g_each_any_pb, 
                    c_inner_uom, g_inner_any_pb, c_plt_uom, g_plt_any_pb)
                else null end
        )
        where t.slot_unit in (c_plt_uom, c_bin_uom)
            and decode(t.slot_unit, c_plt_uom, t.pallet_id, t.bin_id) is null;
        wm_cs_log('Defaulted to any pref plt/bin ' || sql%rowcount);

        -- 8b v) default failed plt/bin slot units to case
        update tmp_slot_item t
        set t.slot_unit = c_case_uom
        where t.slot_unit = c_plt_uom and t.pallet_id is null
        returning t.row_num
        bulk collect into ga_row_num;
        wm_cs_log('Invalid plt for rack type; defaulted to Case ' || sql%rowcount);
        log_exception_for_msg(g_inv_plt_for_rt_msg, c_bad_plt_for_rt);

        update tmp_slot_item t
        set t.slot_unit = c_case_uom
        where t.slot_unit = c_bin_uom and t.bin_id is null
        returning t.row_num
        bulk collect into ga_row_num;
        wm_cs_log('Invalid bin for rack type; defaulted to Case ' || sql%rowcount);            
        log_exception_for_msg(g_inv_bin_for_rt_msg, c_bad_bin_for_rt);        
    end validate_slot_unit;

    procedure load_slot_item_data
    is
    begin
        insert all
        when (1 = 1)
        then
        into tmp_slot_item
        (
            item_id, slot_id, row_num, pallet_id, bin_id, rack_level_id, item_facility_mapping_id,
            ship_unit, movements, hits, slot_unit
        )
        values
        (
            item_id, slot_id, row_num, pallet_id, bin_id, rack_level_id, sku_id,
            coalesce(ship_unit, c_case_uom), movement, nbr_of_picks,
            case when slot_id is null then c_no_uom when pallet_id is not null then c_plt_uom
                when bin_id is not null then c_bin_uom else slot_unit end
        )
        when (ship_unit is null)
        then
        into exceptions
        (
            ex_id, whse_code, slot, item, descript, msg_number, create_date_time, mod_date_time,
            mod_user
        )
        values
        (
            seq_exceptions_id.nextval, g_whse, dsp_slot, sku_name, g_inv_ship_unit_msg, c_bad_ship_unit, sysdate, 
            sysdate, g_user_name
        )
        when (write_inv_su_excp = 1)
        then
        into exceptions
        (
            ex_id, whse_code, slot, item, descript, msg_number, create_date_time, mod_date_time,
            mod_user
        )
        values
        (
            seq_exceptions_id.nextval, g_whse, dsp_slot, sku_name, g_inv_slot_unit_for_rt_msg, c_bad_slot_unit_for_rt, 
            sysdate, sysdate, g_user_name
        )
        with pld as
        (
            select pld.pick_locn_hdr_id, min(pld.item_id) item_id
            from pick_locn_dtl pld 
            where g_is_standalone = 0 and pld.ltst_sku_assign = 'Y'
            group by pld.pick_locn_hdr_id
        ),
        uom_xref as
        (
            select iv.*
            from
            (
                select dense_rank() over(order by scd.whse_code desc) prty, scd.business_value,
                    decode(scd.code_value, 'Bin', c_bin_uom, 'Case', c_case_uom, 'Each', c_each_uom,
                    'Pallet', c_plt_uom, 'Inner', c_inner_uom) uom
                from system_code_dtl scd
                join system_code sc on sc.sys_code_id = scd.sys_code_id and sc.code_type = '502'
                where g_is_standalone = 1 and scd.whse_code in (g_whse, '*df')
                    and scd.code_value in ('Bin', 'Case', 'Each', 'Pallet', 'Inner')
            ) iv
            where iv.prty = 1
        ),
        imp_data as
        (
            select ifmw.item_id, iv.sku_id, iv.slot_id, iv.row_num, iv.movement, iv.nbr_of_picks, 
                iv.sku_name, lh.dsp_locn dsp_slot, slot_uom.uom slot_uom, ship_uom.uom ship_uom, 
                iv.slot_unit_biz
            from
            (
                select iv1.*, decode(iv1.slot_id, null, 1, 
                    row_number() over(partition by iv1.slot_id order by iv1.row_num desc)) rn
                from
                (
                    select it.sku_id, it.slot_id, it.ship_unit ship_unit_biz, it.slot_unit slot_unit_biz,
                        it.row_num, it.movement, it.nbr_of_picks, it.sku_name,
                        row_number() over(partition by it.sku_id, it.slot_id, it.ship_unit
                            order by it.row_num desc) rn1
                    from import_temp it
                    where g_is_standalone = 1
                ) iv1
                where iv1.rn1 = 1
            ) iv
            left join item_facility_mapping_wms ifmw on ifmw.item_facility_mapping_id = iv.sku_id
            left join pick_locn_hdr plh on plh.pick_locn_hdr_id = iv.slot_id   
            left join locn_hdr lh on plh.locn_id = lh.locn_id
            left join uom_xref ship_uom on ship_uom.business_value = iv.ship_unit_biz
            left join uom_xref slot_uom on slot_uom.business_value = iv.slot_unit_biz
            where iv.rn = 1
            union all
            select pld.item_id, ifmw.item_facility_mapping_id sku_id, plh.pick_locn_hdr_id slot_id,
                rownum row_num, null movement, null nbr_of_picks, ic.item_name sku_name, lh.dsp_locn dsp_slot,
                decode(substr(sc.misc_flags, 11, 1), 'C', c_case_uom, 'P', c_plt_uom, 'E', c_each_uom,
                    'B', c_bin_uom, 'I', c_inner_uom, null) slot_uom, 
                decode(substr(sc.misc_flags, 10, 1), 'C', c_case_uom, 'P', c_plt_uom, 'E', c_each_uom,
                    'B', c_case_uom, 'I', c_inner_uom, null) ship_uom, 
                substr(sc.misc_flags, 11, 1) slot_unit_biz
            from pick_locn_hdr plh
            join locn_hdr lh on plh.locn_id = lh.locn_id and lh.sku_dedctn_type = 'P' 
                and lh.locn_class in ('A', 'C') and lh.whse = g_whse
            left join pld on pld.pick_locn_hdr_id = plh.pick_locn_hdr_id
            left join item_facility_mapping_wms ifmw on ifmw.item_id = pld.item_id
                and ifmw.facility_id = g_facility_id
            left join item_cbo ic on ic.item_id = ifmw.item_id
            left join 
            (
                -- either enterprise or whse level; never both
                select sc.code_id, sc.misc_flags
                from sys_code sc 
                where sc.rec_type = 'B' and sc.code_type = '331'
                union all
                select wsc.code_id, wsc.misc_flags
                from whse_sys_code wsc 
                where wsc.rec_type = 'B' and wsc.code_type = '331' and wsc.whse = g_whse
            ) sc on sc.code_id = lh.pick_detrm_zone
            where g_is_standalone = 0
                and not exists
                (
                    select 1
                    from task_dtl td 
                    where td.pull_locn_id = plh.locn_id and td.item_id = pld.item_id
                        and td.stat_code < 90 and td.invn_need_type in ('40', '41', '42', '43')
                )
                and not exists
                (
                    select 1
                    from alloc_invn_dtl aid 
                    where aid.pull_locn_id = plh.locn_id and aid.item_id = pld.item_id 
                        and aid.stat_code = 0 and aid.invn_need_type in ('40', '41', '42', '43')
                )
-- check: this filter is to exclude locns manually created in WM after the slot import
                and exists
                (
                    select 1
                    from pick_locn_hdr_slotting plhs
                    where plhs.pick_locn_hdr_id = plh.pick_locn_hdr_id
                )
        )
        select it.item_id, it.slot_id, decode(it.ship_uom, c_bin_uom, c_case_uom, it.ship_uom) ship_unit, b.bin_id, it.movement, it.sku_id,
            it.nbr_of_picks, it.row_num, it.dsp_slot, it.sku_name, p.pallet_id, rtl.rack_level_id,
            case when it.slot_uom in (c_case_uom, c_each_uom, c_inner_uom)
                and bitand(rtl.slot_units, it.slot_uom) = 0 then 1 else 0 end write_inv_su_excp,
            case when it.slot_id is null then c_no_uom
                -- E/C/I: check if allowed on RTL; fall thru to handle unknown
                when it.slot_uom in (c_case_uom, c_each_uom, c_inner_uom)
                    then get_slot_unit(rtl.slot_units, it.slot_uom, ifs.allow_slu, it.ship_uom)
                -- matched plt/bin tables directly
                when p.pallet_id is not null then c_plt_uom 
                when b.bin_id is not null then c_bin_uom
                -- nothing provided; fall through to handle unknown
                when it.slot_uom is null 
                    then get_slot_unit(rtl.slot_units, null, ifs.allow_slu, it.ship_uom)
                -- directly imported as plt/bin; retain
                else it.slot_uom end slot_unit
        from imp_data it
        left join item_facility_slotting ifs on ifs.item_facility_mapping_id = it.sku_id
        left join pick_locn_hdr_slotting plhs on plhs.pick_locn_hdr_id = it.slot_id
        left join rack_type_level rtl on rtl.rack_level_id = plhs.rack_level_id
        -- step 8a) custom slot unit match: get plt/bin from pallets/bins
-- check: would this work for integrated?
        left join pallets p on it.slot_uom is null and p.plt_type = it.slot_unit_biz
            and p.whse_code = g_whse
        left join bins b on it.slot_uom is null and b.code = it.slot_unit_biz
            and p.whse_code = g_whse;
        wm_cs_log('Collected import data rows, logged exceptions ' || sql%rowcount);
    end load_slot_item_data;

    procedure prorate_forecast_data
    is
    begin
        -- apportion forecast metrics by item only (per Kevin)
        merge into slot_item si
        using
        (
            select si.sku_id, count(*) num_si, min(pdf.est_pick_units) total_est_movement, 
                min(pdf.est_visits) total_est_hits
            from pick_demand_forecast pdf
            join item_facility_mapping_wms ifmw on ifmw.item_id = pdf.item_id
                and ifmw.facility_id = g_facility_id
            join slot_item si on si.sku_id = ifmw.item_facility_mapping_id
            where pdf.whse = g_whse and (pdf.est_pick_units > 0 or pdf.est_visits > 0)
            group by si.sku_id
        ) iv on (iv.sku_id = si.sku_id)
        when matched then
        update set si.est_movement = iv.total_est_movement / iv.num_si,
            si.est_hits = iv.total_est_hits / iv.num_si
        log errors (to_char(sysdate));
        wm_cs_log('Prorated pdf data ' || sql%rowcount);
    end;

    procedure load_wm_history_data
    is
        v_to_date       date := trunc(sysdate);
        v_from_date     date := trunc(sysdate) - c_num_wm_hist_days;
    begin
        insert into tmp_slot_item_hist
        ( 
            item_id, slot_id, ship_unit, num_assoc_per_item, hits, movements
        )
        with assoc_items as
        (
            -- assoc items are those in pld or pdf - i.e. current items
            select t.item_id, t.ship_unit, count(*) num_assoc_per_item
            from tmp_slot_item t
            where t.item_id is not null
            group by t.item_id, t.ship_unit
        )
        select iv.item_id, iv.pick_locn_hdr_id, iv.ship_unit, min(ai.num_assoc_per_item), 
            count(*) hits, sum(iv.movements)
        from
        (
            select hist.item_id, plh.pick_locn_hdr_id,
                hist.qty_alloc/coalesce(nullif(iq.std_pack_qty, 0), 1) movements,
                decode(substr(sc.misc_flags, 10, 1), 'C', c_case_uom, 'P', c_plt_uom, 'E', c_each_uom,
                    'B', c_case_uom, 'I', c_inner_uom, c_case_uom) ship_unit
            from
            (
                select aid.pull_locn_id, aid.item_id, aid.qty_alloc
                from alloc_invn_dtl aid
                join invn_need_type it on it.invn_need_type = aid.invn_need_type
                    and it.cd_master_id = aid.cd_master_id and it.task_cmpl_corr_upd = 6
                where aid.whse = g_whse and aid.stat_code <= 90 and aid.alloc_invn_code in (4, 5)
                    and aid.pull_locn_id is not null
                    and aid.create_date_time >= v_from_date and aid.create_date_time < v_to_date
                union all
                select td.pull_locn_id, td.item_id, td.qty_alloc
                from task_dtl td
                join task_hdr th on th.task_hdr_id = td.task_hdr_id and th.whse = g_whse
                join invn_need_type it on it.invn_need_type = td.invn_need_type
                    and it.cd_master_id = td.cd_master_id and it.task_cmpl_corr_upd = 6
                where td.alloc_invn_code in (4, 5) and td.pull_locn_id is not null
                    and td.create_date_time >= v_from_date and td.create_date_time < v_to_date
            ) hist
            join locn_hdr lh on lh.locn_id = hist.pull_locn_id
            join pick_locn_hdr plh on plh.locn_id = lh.locn_id
            left join
            (
                -- either enterprise or whse level; never both
                select sc.code_id, sc.misc_flags
                from sys_code sc 
                where sc.rec_type = 'B' and sc.code_type = '331'
                union all
                select wsc.code_id, wsc.misc_flags
                from whse_sys_code wsc 
                where wsc.rec_type = 'B' and wsc.code_type = '331' and wsc.whse = g_whse
            ) sc on sc.code_id = lh.pick_detrm_zone
            left join item_quantity iq on iq.item_id = hist.item_id
        ) iv
        left join assoc_items ai on ai.item_id = iv.item_id and ai.ship_unit = iv.ship_unit
        group by iv.item_id, iv.ship_unit, iv.pick_locn_hdr_id;
        wm_cs_log('Collected wm hist from ' || v_from_date || ' midnight to just before ' 
            || v_to_date || ' ' || sql%rowcount);

        -- upd hist data for fully assoc items - exactly matching item, locns (class or id)
        merge into tmp_slot_item t
        using
        (
            select t.item_id, t.slot_id, t.ship_unit, t.hits, t.movements
            from tmp_slot_item_hist t
        ) iv on (iv.item_id = t.item_id and iv.ship_unit = t.ship_unit
            and iv.slot_id = coalesce(t.slot_id, iv.slot_id))
        when matched then
        update set t.movements = coalesce(t.movements, 0) + iv.movements,
            t.hits = coalesce(t.hits, 0) + iv.hits;
        wm_cs_log('Updated wm hist for current items ' || sql%rowcount);

        -- and prorate partially assoc hist data on top - i.e. matching item but diff locns
        merge into tmp_slot_item t
        using
        (
            select t1.item_id, t1.ship_unit,
                sum(t1.hits/t1.num_assoc_per_item) prorated_hist_hits,
                sum(t1.movements/t1.num_assoc_per_item) prorated_hist_movements
            from tmp_slot_item_hist t1
            where t1.num_assoc_per_item is not null
                and not exists
                (
                    select 1
                    from tmp_slot_item t2
                    where t2.item_id = t1.item_id and t2.slot_id = t1.slot_id
                )
            group by t1.item_id, t1.ship_unit
        ) iv on (iv.item_id = t.item_id and iv.ship_unit = t.ship_unit)
        when matched then
        update set t.movements = coalesce(t.movements, 0) + iv.prorated_hist_movements,
            t.hits = coalesce(t.hits, 0) + iv.prorated_hist_hits;
        wm_cs_log('Prorated wm hist on non-current items ' || sql%rowcount);
    end load_wm_history_data;

    procedure unslotted_item_processing
    is
    begin
        -- nullify slot_id for unslotted items
-- check: deleting slot_item rows creates a whole host of dependency cleanup issues
-- check: condolidate rows for the same unslotted item after above update; dml below may update 
-- multiple rows for the same item; for such rows move out item history if item is slotted elsewhere
-- todo: nullify other locn data
        update slot_item si
        set si.slot_id = null, si.slot_unit = c_no_uom, si.slot_width = 0, si.score = 0,
            si.ign_for_reslot = 0, si.rec_lanes = 1, si.rec_stacking = 1, si.current_pallet = null,
            si.current_bin = null
        where si.sku_id is not null and si.slot_id is not null
            and not exists
            (
                select 1
                from tmp_slot_item t
                where t.item_facility_mapping_id = si.sku_id and t.slot_id = si.slot_id
                    and si.ship_unit = t.ship_unit
            )
            and exists
            (
                select 1
                from pick_locn_hdr plh
                join locn_hdr lh on lh.locn_id = plh.locn_id and lh.whse = g_whse
                where plh.pick_locn_hdr_id = si.slot_id
            )
        log errors (to_char(sysdate));
        wm_cs_log('Nullified slot_id on SI for unslotted items ' || sql%rowcount);
    end;

    procedure reslotted_item_processing
    is
    begin
        -- udpate/insert for items that were slotted into new locns; look for item rows
        -- in SI with no slot and reuse them; if not enough are found, try to find and reuse SI 
        -- with no item; for any remaining, create new SI rows
        merge into slot_item si
        using
        (
            -- pick up data that isnt current
            with reslotted_items as
            (
                select t.item_facility_mapping_id sku_id, t.slot_id, t.slot_unit, t.ship_unit, 
                    row_number() over(partition by t.item_facility_mapping_id, t.ship_unit
                        order by t.slot_id) item_rank
                from tmp_slot_item t
                where t.item_facility_mapping_id is not null and t.slot_id is not null
                    and not exists
                    (
                        select 1
                        from slot_item si
                        where si.sku_id = t.item_facility_mapping_id and si.slot_id = t.slot_id
                            and coalesce(si.ship_unit, c_case_uom) = t.ship_unit
                    )
            ),
            -- rank matching SI rows - item affinity
            open_item_si as
            (
                -- si.ship_unit is nullable; we default to case pick if null
                select si.slotitem_id, iv.sku_id, iv.ship_unit,
                    row_number() over(partition by iv.sku_id, iv.ship_unit 
                        order by si.slotitem_id) si_rank
                from 
                (
                    select distinct rs.sku_id, rs.ship_unit
                    from reslotted_items rs
                ) iv
                join slot_item si on si.sku_id = iv.sku_id 
                    and coalesce(si.ship_unit, c_case_uom) = iv.ship_unit
                    and si.slot_id is null
            ),
            -- match them up
            item_affinity_match as
            (
                select rs.slot_id, rs.slot_unit, rs.ship_unit, rs.sku_id, ois.slotitem_id
                from reslotted_items rs
                left join open_item_si ois on ois.sku_id = rs.sku_id and ois.ship_unit = rs.ship_unit
                    and ois.si_rank = rs.item_rank
            ),
            -- for any remaining, rank matching SI rows - slot affinity
            open_slot_si as
            (
                select iv.slot_id, si.slotitem_id,
                    row_number() over(partition by iv.slot_id order by si.slotitem_id) si_rank
                from 
                (
                    select distinct rs.slot_id
                    from item_affinity_match rs
                    where rs.slotitem_id is null
                ) iv
                join slot_item si on si.slot_id = iv.slot_id and si.sku_id is null
            )
            select t.sku_id, t.slot_id, t.slot_unit, t.ship_unit, t.slotitem_id
            from 
            (
                select rs.slot_id, rs.slot_unit, rs.ship_unit, rs.sku_id, rs.slotitem_id
                from item_affinity_match rs
                where rs.slotitem_id is not null
                -- reslotted items - match them with slot rows (with null item) for any
                -- remaining data that could not find a matching item row (with null slot)
                union all
                select rs.slot_id, rs.slot_unit, rs.ship_unit, rs.sku_id, osi.slotitem_id
                from 
                (
                    select iaf.*, row_number() over(partition by iaf.slot_id 
                        order by iaf.sku_id) slot_rank
                    from item_affinity_match iaf
                    where iaf.slotitem_id is null
                ) rs
                left join open_slot_si osi on osi.slot_id = rs.slot_id 
                    and osi.si_rank = rs.slot_rank
            ) t
        ) iv on (iv.slotitem_id = si.slotitem_id)
        when matched then
        update set si.mod_date_time = sysdate, si.mod_user = g_user_name, si.slot_id = iv.slot_id, 
            si.slot_unit = iv.slot_unit, si.ship_unit = iv.ship_unit, si.sku_id = iv.sku_id
        when not matched then
        insert
        ( 
            slotitem_id, slot_id, sku_id, slot_unit, ship_unit, create_date_time, mod_date_time, 
            mod_user
        )
        values
        (
            seq_slot_item_id.nextval, iv.slot_id, iv.sku_id, iv.slot_unit, iv.ship_unit, sysdate, 
            sysdate, g_user_name
        )
        log errors (to_char(sysdate));
        wm_cs_log('Merged reslotted/empty slot data on SI ' || sql%rowcount);
    end;

    procedure empty_slot_processing
    is
        type ta_si_id           is table of slot_item.slotitem_id%type index by binary_integer;
        va_si_id                ta_si_id;
    begin
        -- step 3: create a row each for empty slots if they dont have any other items or were
        -- disassociated from items in step 1
        -- standalone could send in a subset of data, hence we drive from plh and not the tmp table
        insert into slot_item
        ( 
            slotitem_id, slot_id, sku_id, slot_unit, ship_unit, slot_locked, slot_width, max_lanes, 
            bin_unit, ign_for_reslot, rec_lanes, rec_stacking, create_date_time, mod_date_time, 
            mod_user, opt_fluid_vol, est_mvmt_can_borrow, est_hits_can_borrow, delete_mult, score, 
            primary_move, cur_orientation
        )
        select seq_slot_item_id.nextval, plh.pick_locn_hdr_id, null sku_id, c_no_uom slot_unit, 
            c_no_uom ship_unit, decode(plh.locn_putaway_lock, null, 0, 1), lh.width, 
            coalesce(plhs.max_lanes, 1), c_no_uom bin_unit, 0 ign_for_reslot, 1 rec_lanes, 
            1 rec_stacking, sysdate, sysdate, g_user_name, -999.99 opt_fluid_vol, 
            0 est_mvmt_can_borrow, 0 est_hits_can_borrow, 0 delete_mult, 0 score, 0 primary_move, 
            0 cur_orientation
        from pick_locn_hdr plh
        join locn_hdr lh on lh.locn_id = plh.locn_id and lh.whse = g_whse
        join pick_locn_hdr_slotting plhs on plhs.pick_locn_hdr_id = plh.pick_locn_hdr_id
        where not exists
        (
            select 1
            from slot_item si
            where si.slot_id = plh.pick_locn_hdr_id
        )
        log errors (to_char(sysdate));
        wm_cs_log('Created empty slot SI rows ' || sql%rowcount);

        -- step 4: clean out previously empty slot rows that now have an item slotted
        select si.slotitem_id
        bulk collect into va_si_id
        from slot_item si
        where si.slot_id is not null and si.sku_id is null
            and exists
            (
                select 1
                from tmp_slot_item t
                where t.slot_id = si.slot_id and t.item_id is not null
            );
        wm_cs_log('Collected SI for empty slots that now have a new item reslotted ' || sql%rowcount);

        forall i in 1..va_si_id.count
        delete from haves_needs hn
        where hn.slot_item_id = va_si_id(i);
        wm_cs_log('Removed haves_needs for empty slots that now have a new item reslotted ' || sql%rowcount);

        forall i in 1..va_si_id.count
        delete from hn_failure hf
        where hf.slotitem_id = va_si_id(i);
        wm_cs_log('Removed hn_failure for empty slots that now have a new item reslotted ' || sql%rowcount);

        forall i in 1..va_si_id.count
        delete from slot_item_score sis
        where sis.slotitem_id = va_si_id(i);
        wm_cs_log('Removed scores for empty slots that now have a new item reslotted ' || sql%rowcount);

        forall i in 1..va_si_id.count
        delete from sl_failure_reasons sfr
        where sfr.slotitem_id = va_si_id(i);
        wm_cs_log('Removed sl_failure_reasons for empty slots that now have a new item reslotted ' || sql%rowcount);

        forall i in 1..va_si_id.count
        delete from slot_item si
        where si.slotitem_id = va_si_id(i);
        wm_cs_log('Removed SI for empty slots that now have a new item reslotted ' || sql%rowcount);
    end empty_slot_processing;

    procedure update_current_si_data
    (
        p_import_id           in import_offset_master.import_id%type
    )
    is
        v_sql_str             clob;
        v_sel_str             varchar2(4000);
        v_assign_str          varchar2(4000);
    begin
        -- step 5: update a) current data via locn_id b) current PDF data already summarized by 
        -- item and c) NA history data summarized by ship_unit/class; these 3 data sets 
        -- should contain mutually exclusive item lists
        v_sql_str := '
        merge into slot_item si
        using
        (
            with opp_xref as
            (
                select dense_rank() over(order by scd.whse_code desc) prty, scd.business_value,
                    decode(scd.code_value, ''OptPltPatternPrimary'', 3, ''OptPltPatternSecondary'', 5, 
                        ''OptPltPatternBoth'', 7) opp_value
                from system_code_dtl scd
                join system_code sc on sc.sys_code_id = scd.sys_code_id and sc.code_type = ''502''
                where :is_standalone = 1 and scd.whse_code in (:whse, ''*df'')
                    and scd.code_value in (''OptPltPatternPrimary'', ''OptPltPatternSecondary'', ''OptPltPatternBoth'')
            )
            select t.slot_id, t.item_facility_mapping_id, t.movements, t.hits,
                case when :is_weekly = 1 and (t.hits is not null or t.movements is not null) 
                    then 1 else 0 end update_hist,
                t.ship_unit, coalesce(plhs.max_lanes, :max_lanes) max_lanes,
                decode(plh.locn_putaway_lock, null, 0, 1) slot_locked,
                case when (lh.width is null OR lh.width = 0) then rtl.level_width else lh.width end width,				 
                to_number(it.est_inventory) est_inventory, to_number(it.est_movement) est_movement,
                to_number(it.est_hits) est_hits, it.hist_match, rtl.max_overhang_pct,
                decode(it.current_orientation, ''HWL'', 1, ''LHW'', 2, ''LWH'', 3, ''WHL'', 4, ''WLH'', 5, 0) cur_orientation,
                decode(it.ign_for_reslot, ''Y'', 1, 0) ign_for_reslot, 
                coalesce(to_number(it.rec_lanes), ifs.max_lanes) rec_lanes,
                coalesce(to_number(it.rec_stacking), ifs.max_stacking) rec_stacking, 
                coalesce(ox.opp_value, 0) opt_pallet_pattern, it.hi_residual,
                it.info1, it.info2, it.info3, it.info4, it.info5, it.info6, 
                to_number(it.si_num_1) si_num_1, to_number(it.si_num_2) si_num_2,
                to_number(it.si_num_3) si_num_3, to_number(it.si_num_4) si_num_4,
                to_number(it.si_num_5) si_num_5, to_number(it.si_num_6) si_num_6,
                it.mult_loc_grp, it.replen_group, plhs.allow_expand, t.pallet_id current_pallet, 
                t.bin_id current_bin, lh.len, 
                coalesce(nullif(case it.current_orientation when ''LHW'' then lh.ht 
                    when ''LWH'' then lh.ht when ''WHL'' then lh.width when ''HWL'' then lh.width 
                    else lh.len end, 0), 1) orientated_length <custom_sel_str>
            from tmp_slot_item t
            join item_facility_slotting ifs on ifs.item_facility_mapping_id = t.item_facility_mapping_id
            left join pick_locn_hdr plh on plh.pick_locn_hdr_id = t.slot_id
            left join locn_hdr lh on lh.locn_id = plh.locn_id
            left join pick_locn_hdr_slotting plhs on plhs.pick_locn_hdr_id = plh.pick_locn_hdr_id
            left join import_temp it on :is_standalone = 1 and it.row_num = t.row_num
            left join opp_xref ox on ox.business_value = it.opt_pallet_pattern and ox.prty = 1
            left join rack_type_level rtl on rtl.rack_level_id = t.rack_level_id
        ) iv on (iv.item_facility_mapping_id = si.sku_id and iv.ship_unit = si.ship_unit
            and coalesce(iv.slot_id, -1) = coalesce(si.slot_id, -1))
        when matched then
        update set si.calc_hits = decode(iv.update_hist, 1, iv.hits, si.calc_hits),
            si.case_mov = decode(iv.update_hist, 1, iv.movements, si.case_mov),
            si.each_mov = decode(iv.update_hist, 1, iv.movements, si.each_mov),
            si.mod_user = :user_name, si.create_date_time = sysdate, si.mod_date_time = sysdate,
            si.allow_expand = iv.allow_expand, si.max_lanes = iv.max_lanes, 
            si.slot_locked = iv.slot_locked, si.slot_width = iv.width, si.current_bin = iv.current_bin,
            si.est_inventory = iv.est_inventory, si.est_movement = iv.est_movement,
            si.est_hits = iv.est_hits, si.hist_match = iv.hist_match, 
            si.cur_orientation = iv.cur_orientation, si.ign_for_reslot = iv.ign_for_reslot, 
            si.rec_lanes = iv.rec_lanes, si.rec_stacking = iv.rec_stacking,
            si.deeps = case when floor((iv.len + iv.max_overhang_pct/100 * iv.orientated_length)
                / iv.orientated_length) < 1 then 1
                else floor((iv.len + iv.max_overhang_pct/100 * iv.orientated_length)
                / iv.orientated_length) end,
            si.pallet_mov = decode(iv.update_hist, 1, iv.movements, si.pallet_mov),
            si.inner_mov = decode(iv.update_hist, 1, iv.movements, si.inner_mov),
            si.bin_mov = decode(iv.update_hist, 1, iv.movements, si.bin_mov),
            si.pallet_inven = iv.est_inventory, si.case_inven = iv.est_inventory, 
            si.inner_inven = iv.est_inventory, si.each_inven = iv.est_inventory, si.bin_inven = iv.est_inventory, 
            si.opt_pallet_pattern = case when iv.hi_residual in (''Y'', ''1'') 
                then (iv.opt_pallet_pattern + :hi_res_mask - bitand(iv.opt_pallet_pattern, :hi_res_mask))
                else bitand(iv.opt_pallet_pattern, :hi_res_mask_comp) end,
            si.info1 = iv.info1, si.info2 = iv.info2, si.info3 = iv.info3, si.info4 = iv.info4,
            si.info5 = iv.info5, si.info6 = iv.info6, si.si_num_1 = iv.si_num_1, 
            si.si_num_2 = iv.si_num_2, si.si_num_3 = iv.si_num_3, si.si_num_4 = iv.si_num_4,
            si.si_num_5 = iv.si_num_5, si.si_num_6 = iv.si_num_6, si.mult_loc_grp = iv.mult_loc_grp,
            si.replen_group = iv.replen_group, si.current_pallet = iv.current_pallet <custom_assign_str>
            log errors (to_char(sysdate))';

        if (g_is_standalone = 1)
        then
            begin
                -- build custom select list
                select listagg('it.custom_' || iom.offset_fieldname, ',') 
                    within group(order by iom.offset_fieldname)
                into v_sel_str
                from import_offset_master iom
                where iom.import_id = p_import_id and iom.offset_type = 2 and iom.custom = 1;
                v_sel_str := case when v_sel_str is not null then ', ' else ' ' end || v_sel_str;

                -- build custom assignment list for columns not already mapped
                select listagg('si.' || lower(iom.custom_offset_field) || ' = ' || 'iv.custom_' 
                        || iom.offset_fieldname, ',') within group(order by iom.offset_fieldname)
                into v_assign_str
                from import_offset_master iom
                where iom.import_id = p_import_id and iom.offset_type = 2 and iom.custom = 1
                    and instr(v_sql_str, 'si.' || lower(iom.custom_offset_field), 1, 1) = 0;
                v_assign_str := case when v_assign_str is not null then ', ' else ' ' end || v_assign_str;
            exception
                when no_data_found then
                    null;
            end;

            -- build custom repl list for columns that are already mapped; assumptions: 1. the dml
            -- maintains column assignment text in lower case 2. assignment text source and dest
            -- column names match i.e. they are appropriately aliased in the inline view
            for repl in
            (
                select lower(iom.custom_offset_field) target_col, iom.offset_fieldname cust_col_num
                from import_offset_master iom
                where iom.import_id = p_import_id and iom.offset_type = 2 and iom.custom = 1
                    and instr(v_sql_str, 'si.' || lower(iom.custom_offset_field), 1, 1) > 0
            )
            loop
                v_sql_str := replace(v_sql_str, 'iv.' || repl.target_col, 'iv.custom_' || repl.cust_col_num);
            end loop;
        end if;
        v_sql_str := replace(v_sql_str, '<custom_sel_str>', v_sel_str);
        v_sql_str := replace(v_sql_str, '<custom_assign_str>', v_assign_str);

        execute immediate v_sql_str using g_is_standalone, g_whse, g_is_weekly, c_max_lanes,
            g_is_standalone, g_user_name, c_hi_res_mask, c_hi_res_mask, c_hi_res_mask_comp;
        wm_cs_log('Data copied to SI ' || sql%rowcount);
    end update_current_si_data;

    procedure sync_item_and_wm_history
    is
        v_start_date    date := trunc(sysdate) - c_num_item_hist_days;
        v_end_date      date := trunc(sysdate);
    begin
        -- step 5 contd: c) NA history data; this should find at least one item row so an insert 
        -- should not be necessary here
-- todo: the NA data may need to be prorated because SI may not be consolidated yet? either that or
-- consolidate prior to this step
        merge into slot_item si
        using
        (
            select ifmw.item_facility_mapping_id, sum(t.movements) movements, sum(t.hits) hits,
                t.ship_unit
            from tmp_slot_item_hist t
            join item_facility_mapping_wms ifmw on ifmw.item_id = t.item_id
                and ifmw.facility_id = g_facility_id
            where t.num_assoc_per_item is null
            group by ifmw.item_facility_mapping_id, t.ship_unit
        ) iv on (iv.item_facility_mapping_id = si.sku_id and iv.ship_unit = si.ship_unit
            and si.slot_id is null)
        when matched then
        update set si.calc_hits = iv.hits, si.case_mov = iv.movements, si.each_mov = iv.movements,
            si.mod_user = g_user_name, si.create_date_time = sysdate, si.mod_date_time = sysdate
        where iv.hits is not null or iv.movements is not null
        log errors (to_char(sysdate));
        wm_cs_log('Updated NA history data on SI ' || sql%rowcount);

        -- step 6: dump computed metrics into item history
        merge into item_history ih
        using
        (
            select si.slotitem_id, iv.hits, iv.movements, iv.ship_unit
            from
            (
                select t.slot_id, t.item_facility_mapping_id, t.ship_unit, t.movements, t.hits
                from tmp_slot_item t
                where t.item_id is not null
                union all
                select null slot_id, ifmw.item_facility_mapping_id, t.ship_unit,
                    sum(t.movements) movements, sum(t.hits) hits
                from tmp_slot_item_hist t
                join item_facility_mapping_wms ifmw on ifmw.item_id = t.item_id
                    and ifmw.facility_id = g_facility_id
                where t.num_assoc_per_item is null
                group by ifmw.item_facility_mapping_id, t.ship_unit
            ) iv
            join slot_item si on si.sku_id = iv.item_facility_mapping_id 
                and coalesce(si.slot_id, -1) = coalesce(iv.slot_id, -1)
                -- no coalesce here on the strength of ih.ship_unit being non-null; i.e. the
                -- assumption is that SI and IH will match
                and si.ship_unit = iv.ship_unit
            where iv.hits is not null or iv.movements is not null
        ) iv on (iv.slotitem_id = ih.slotitem_id
            and v_start_date between trunc(ih.start_date) and trunc(ih.end_date))
        when matched then
        update set ih.mod_date_time = sysdate, ih.mod_user = g_user_name, 
            ih.movement = iv.movements, ih.nbr_of_picks = iv.hits
        when not matched then
        insert
        (
            hist_id, slotitem_id, start_date, end_date, ship_unit, movement, nbr_of_picks,
            create_date_time, mod_date_time, mod_user
        )
        values
        (
            seq_hist_id.nextval, iv.slotitem_id, v_start_date, v_end_date, iv.ship_unit,
            iv.movements, iv.hits, sysdate, sysdate, g_user_name
        )
        log errors (to_char(sysdate));
        wm_cs_log('Synced item history ' || sql%rowcount);
    end sync_item_and_wm_history;

    procedure import_slot_data_from_wm
    (
        p_whse          in locn_hdr.whse%type,
        p_is_weekly     in number,
        p_user_name     in varchar2,
        p_is_standalone in number,
        p_import_id     in import_offset_master.import_id%type,
        p_log_lvl       in number       
    )
    is
        v_parent_txn_id             val_result_hist.parent_txn_id%type;
        v_rt_bind_list_csv          varchar2(4000);
    begin
        whse_import_init(p_whse, p_user_name, p_is_standalone, p_is_weekly, p_log_lvl);
        
        wm_cs_log('Starting item hist consolidation before import ');
        sp_consol_item_history_final(p_whse);

        v_rt_bind_list_csv := 'whse,' || g_whse || ',facility_id,' || to_char(g_facility_id) || ',';
        wm_val_exec_job(g_user_name, p_app_hook => 'ENTRY', p_pgm_id => c_pgm_id,
            p_rt_bind_list_csv => v_rt_bind_list_csv, p_log_lvl => p_log_lvl,
            p_parent_txn_id => v_parent_txn_id);

        wm_cs_log('Started whse import', p_sql_log_level => 0);
        load_slot_item_data();
        validate_slot_unit();

        -- slot units that could not be determined; disassociate item with slot
        update tmp_slot_item t
        set t.item_id = null, t.item_facility_mapping_id = null, t.slot_unit = c_no_uom
        where t.slot_unit is null and t.item_facility_mapping_id is not null
            and t.slot_id is not null
        returning t.row_num
        bulk collect into ga_row_num;
        wm_cs_log('Disassociated items from slots ' || sql%rowcount);            
        log_exception_for_msg(g_unknown_slot_unit_msg, c_unknown_slot_unit);

        if (g_is_standalone = 0 and g_is_weekly = 1)
        then
            load_wm_history_data();
        end if;

        -- begin slot_item sync
        -- step 1: items no longer in their current slots
        unslotted_item_processing();

        -- step 2: items moved between slots
        reslotted_item_processing();

        -- steps 3, 4: slots that are empty now (or) were empty prior but are no longer so
        empty_slot_processing();

        -- steps 5a, 5b current data updates (copying, defaults)
        update_current_si_data(p_import_id);

        if (g_is_weekly = 1)
        then
            if (g_is_standalone = 0)
            then
                prorate_forecast_data();
            end if;
            -- steps 5c, 6: update SI metrics on to item history
            sync_item_and_wm_history();
        end if;

        wm_cs_log('Completed whse import', p_sql_log_level => 0);
        
        sp_consol_item_history_final(p_whse);
        wm_cs_log('Completed item hist consolidation after import');

        wm_val_exec_job(g_user_name, p_app_hook => 'EXIT', p_pgm_id => c_pgm_id,
            p_rt_bind_list_csv => v_rt_bind_list_csv, p_log_lvl => p_log_lvl,
            p_parent_txn_id => v_parent_txn_id);
    end import_slot_data_from_wm;
end;
/

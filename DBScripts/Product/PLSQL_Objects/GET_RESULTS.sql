
CREATE OR REPLACE 
PROCEDURE GET_RESULTS (P_RFPID INTEGER, P_SCENARIOID INTEGER)
IS
   l_awardtype                INTEGER;
   l_round_num                INTEGER;
   l_laneequipmenttypeid      INTEGER;
   l_volume                   NUMBER (14, 3);
   l_p_award_amt              NUMBER (14, 3);
   la_discount_criterion_id   NUMBER (10, 0);
   la_costperload             NUMBER (14, 3);
   ec_record_count            NUMBER (5, 0);
BEGIN
   SELECT round_num
     INTO l_round_num
     FROM scenario
    WHERE rfpid = p_rfpid AND scenarioid = p_scenarioid;

   SELECT awardtype
     INTO l_awardtype
     FROM rfp
    WHERE rfpid = p_rfpid AND round_num = l_round_num;

   UPDATE scenarioadjustment
      SET PACKAGEAWARDAMOUNT = NULL
    WHERE     rfpid = p_rfpid
          AND scenarioid = p_scenarioid
          AND MANUALAWARDAMOUNT IS NULL
          AND PACKAGEAWARDAMOUNT != 0;

   UPDATE scenarioadjustment
      SET AUTOAWARDAMOUNT = NULL, discount_criterion_id = -1
    WHERE rfpid = p_rfpid AND scenarioid = p_scenarioid;

   FOR CURSOR_LA
      IN (SELECT la.obcarriercodeid,
                 la.laneid,
                 la.packageid,
                 la.contract_type,
                 la.asmt_volume_per_week,
                 la.asmt_pct,
                 la.asmt_sequence_num,
                 la.historicalaward,
                 la.discount_criterion_id,
                 l.volumefrequency
            FROM OB200LANE_ASMT la, ob200lane_view l
           WHERE     la.rfpid = p_rfpid
                 AND la.scenarioid = p_scenarioid
                 AND la.scenarioid = l.scenarioid
                 AND la.laneid = l.laneid
                 AND l.rfpid = la.rfpid
                 AND (la.packageid = 0
                      OR la.packageid IN
                            (SELECT packageid
                               FROM ob200package_bid_view
                              WHERE rfpid = p_rfpid
                                    AND scenarioid = p_scenarioid)))
   LOOP
      BEGIN
         IF CURSOR_LA.packageid = 0
         THEN
            SELECT DISTINCT laneequipmenttypeid
              INTO l_laneequipmenttypeid
              FROM ob200lane_bid_view
             WHERE     rfpid = p_rfpid
                   AND laneid = CURSOR_LA.laneid
                   AND obcarriercodeid = CURSOR_LA.obcarriercodeid
                   AND contract_type = CURSOR_LA.contract_type
                   AND scenarioid = P_SCENARIOID;
         ELSE
            SELECT DISTINCT laneequipmenttypeid
              INTO l_laneequipmenttypeid
              FROM ob200package_bid_view
             WHERE     rfpid = p_rfpid
                   AND laneid = CURSOR_LA.laneid
                   AND obcarriercodeid = CURSOR_LA.obcarriercodeid
                   AND contract_type = CURSOR_LA.contract_type
                   AND packageid = CURSOR_LA.packageid
                   AND scenarioid = p_scenarioid;
         END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_laneequipmenttypeid := 0;
      END;

      IF CURSOR_LA.volumefrequency = 0
      THEN
         l_volume := ROUND (CURSOR_LA.asmt_volume_per_week / (1 * 7 / 1.0), 3);
      ELSIF CURSOR_LA.volumefrequency = 1
      THEN
         l_volume := ROUND (CURSOR_LA.asmt_volume_per_week / (1 * 7 / 7.0), 3);
      ELSIF CURSOR_LA.volumefrequency = 2
      THEN
         l_volume :=
            ROUND (CURSOR_LA.asmt_volume_per_week / (1 * 7 / 14.0), 3);
      ELSIF CURSOR_LA.volumefrequency = 3
      THEN
         l_volume :=
            ROUND (
               CURSOR_LA.asmt_volume_per_week / (1 * 7 / 30.416666666666),
               3);
      ELSIF CURSOR_LA.volumefrequency = 4
      THEN
         l_volume :=
            ROUND (CURSOR_LA.asmt_volume_per_week / (1 * 7 / 182.5), 3);
      ELSIF CURSOR_LA.volumefrequency = 5
      THEN
         l_volume :=
            ROUND (CURSOR_LA.asmt_volume_per_week / (1 * 7 / 365.0), 3);
      ELSE
         l_volume := -1;
      END IF;

      IF l_awardtype = 0
      THEN
         l_p_award_amt := l_volume;
      ELSIF l_awardtype = 1
      THEN
         l_p_award_amt := CURSOR_LA.asmt_pct;
      ELSIF l_awardtype = 2
      THEN
         l_p_award_amt := CURSOR_LA.asmt_sequence_num;
      END IF;

      IF CURSOR_LA.discount_criterion_id > 0
      THEN
         la_discount_criterion_id := CURSOR_LA.discount_criterion_id;

         SELECT COUNT (*)
           INTO ec_record_count
           FROM effective_cost ec, lanebid_key lbk
          WHERE     discount_criterion_id = CURSOR_LA.discount_criterion_id
                AND ec.lanebid_key_id = lbk.lanebid_key_id
                AND lbk.rfp_id = P_RFPID
                AND lbk.laneequipmenttypeid = l_laneequipmenttypeid
                AND ec.round_num =
                       (SELECT MAX (round_num)
                          FROM lanebid lb2
                         WHERE     lb2.rfpid = P_RFPID
                               AND lb2.rfpid = lbk.rfp_id
                               AND lb2.laneid = lbk.LANE_ID
                               AND lb2.OBCARRIERCODEID = lbk.obcarriercodeid
                               AND lb2.LANEEQUIPMENTTYPEID =
                                      lbk.LANEEQUIPMENTTYPEID
                               AND lb2.laneequipmenttypeid =
                                      l_laneequipmenttypeid
                               AND lb2.historicalaward = 0
                               AND lb2.ROUND_NUM <= l_round_num);
      END IF;

      IF CURSOR_LA.discount_criterion_id > 0 AND ec_record_count > 0
      THEN
         SELECT effective_cost
           INTO la_costperload
           FROM effective_cost ec, lanebid_key lbk
          WHERE     discount_criterion_id = CURSOR_LA.discount_criterion_id
                AND ec.lanebid_key_id = lbk.lanebid_key_id
                AND lbk.rfp_id = P_RFPID
                AND lbk.laneequipmenttypeid = l_laneequipmenttypeid
                AND ec.round_num =
                       (SELECT MAX (round_num)
                          FROM lanebid lb2
                         WHERE     lb2.rfpid = P_RFPID
                               AND lb2.rfpid = lbk.rfp_id
                               AND lb2.laneid = lbk.LANE_ID
                               AND lb2.OBCARRIERCODEID = lbk.obcarriercodeid
                               AND lb2.LANEEQUIPMENTTYPEID =
                                      lbk.LANEEQUIPMENTTYPEID
                               AND lb2.laneequipmenttypeid =
                                      l_laneequipmenttypeid
                               AND lb2.historicalaward = 0
                               AND lb2.ROUND_NUM <= l_round_num);
      ELSE
         IF CURSOR_LA.packageid = 0
         THEN
            la_discount_criterion_id := -1;

            SELECT costperload
              INTO la_costperload
              FROM lanebid lb1
             WHERE     rfpid = P_RFPID
                   AND laneequipmenttypeid = l_laneequipmenttypeid
                   AND laneid = CURSOR_LA.laneid
                   AND obcarriercodeid = CURSOR_LA.obcarriercodeid
                   AND packageid = 0
                   AND historicalaward = CURSOR_LA.historicalaward
                   AND round_num =
                          (SELECT MAX (round_num)
                             FROM lanebid lb2
                            WHERE lb2.rfpid = P_RFPID
                                  AND lb2.laneequipmenttypeid =
                                         l_laneequipmenttypeid
                                  AND lb2.laneid = CURSOR_LA.laneid
                                  AND lb2.obcarriercodeid =
                                         CURSOR_LA.obcarriercodeid
                                  AND lb2.packageid = 0
                                  AND lb2.historicalaward =
                                         CURSOR_LA.historicalaward
                                  AND lb2.round_num <= l_round_num);
         END IF;
      END IF;

      IF CURSOR_LA.packageid = 0
      THEN
         UPDATE scenarioadjustment
            SET autoawardamount = l_p_award_amt,
                discount_criterion_id = la_discount_criterion_id,
                costperload = la_costperload
          WHERE     rfpid = p_rfpid
                AND obcarriercodeid = CURSOR_LA.obcarriercodeid
                AND packageid = CURSOR_LA.packageid
                AND laneid = CURSOR_LA.laneid
                AND scenarioid = p_scenarioid
                AND laneequipmenttypeid = l_laneequipmenttypeid
                AND historicalaward = CURSOR_LA.historicalaward
                AND MANUALAWARDAMOUNT IS NULL;

         UPDATE scenarioadjustment
            SET discount_criterion_id = la_discount_criterion_id,
                costperload = la_costperload
          WHERE     rfpid = p_rfpid
                AND obcarriercodeid = CURSOR_LA.obcarriercodeid
                AND packageid = CURSOR_LA.packageid
                AND laneid = CURSOR_LA.laneid
                AND scenarioid = p_scenarioid
                AND laneequipmenttypeid = l_laneequipmenttypeid
                AND historicalaward = CURSOR_LA.historicalaward
                AND MANUALAWARDAMOUNT IS NOT NULL;
      ELSE
         UPDATE scenarioadjustment
            SET PACKAGEAWARDAMOUNT = l_p_award_amt
          WHERE     rfpid = p_rfpid
                AND LANEEQUIPMENTTYPEID = l_laneequipmenttypeid
                AND obcarriercodeid = CURSOR_LA.obcarriercodeid
                AND packageid = CURSOR_LA.packageid
                AND laneid = CURSOR_LA.laneid
                AND scenarioid = p_scenarioid
                AND historicalaward = CURSOR_LA.historicalaward
                AND MANUALAWARDAMOUNT IS NULL;
      END IF;
   END LOOP;

   CALCULATESCENARIOSTATS (p_rfpid, p_scenarioid);
   POPULATE_SCENARIO_CAPACITY (p_rfpid, p_scenarioid);
   
END GET_RESULTS;
/




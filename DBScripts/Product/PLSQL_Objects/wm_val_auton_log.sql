create or replace procedure wm_val_auton_log
(
    p_msg           in msg_log.msg%type,
    p_module        in msg_log.module%type,
    p_msg_id        in msg_log.msg_id%type,
    p_ref_code_1    in msg_log.ref_code_1%type,
    p_ref_value_1   in msg_log.ref_value_1%type,
    p_user_id       in user_profile.user_id%type,
    p_pgm_id        in msg_log.pgm_id%type,
    p_ref_code_2    in msg_log.ref_code_2%type,
    p_ref_value_2   in msg_log.ref_value_2%type
)
as
    pragma autonomous_transaction;
begin
    insert into msg_log
    (
        msg_log_id, module, msg_id, ref_value_1, msg, create_date_time, 
        mod_date_time, user_id, log_date_time, pgm_id, ref_code_1, ref_code_2,
        ref_value_2
    )
    values
    (
        msg_log_id_seq.nextval, p_module, p_msg_id, p_ref_value_1, p_msg,
        sysdate, sysdate, p_user_id, sysdate, p_pgm_id, p_ref_code_1,
        p_ref_code_2, p_ref_value_2
    );
    commit;
end;
/
show errors;

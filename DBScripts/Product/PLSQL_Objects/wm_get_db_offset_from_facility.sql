create or replace function wm_get_db_offset_from_facility
(
    p_facility_id     in facility.facility_id%type
)
return number
as
    v_fac_offset_from_gmt number(6, 5);
    v_gmt_date            date := systimestamp at time zone 'GMT';
begin
    select tz.gmt_offset / 24
    into v_fac_offset_from_gmt
    from facility f
    join time_zone tz on tz.time_zone_id = f.facility_tz
    where f.facility_id = p_facility_id;

    return (sysdate - v_gmt_date) - v_fac_offset_from_gmt;
end;
/
show errors;

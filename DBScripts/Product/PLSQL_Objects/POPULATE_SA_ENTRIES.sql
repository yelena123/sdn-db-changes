CREATE OR REPLACE
PROCEDURE POPULATE_SA_ENTRIES (I_RFPID INTEGER, I_OBCARRIERCODEID INTEGER)
IS
   I_ROUND_NUM   INT;
BEGIN
   SELECT MAX (round_num)
     INTO I_ROUND_NUM
     FROM rfp
    WHERE rfpid = I_RFPID;


update scenariorule set newrule = 1 where rfpid =I_RFPID and scenarioid in ( 
     select scenarioid from scenario where rfpid = I_RFPID and round_num = I_ROUND_NUM);
   INSERT INTO scenarioadjustment (laneequipmenttypeid,
                                   obcarriercodeid,
                                   scenarioid,
                                   rfpid,
                                   laneid,
                                   historicalaward,
                                   equipmenttype_id,
                                   servicetype_id,
                                   protectionlevel_id,
                                   mot,
                                   commoditycode_id,
                                   inboundfacilitycode,
								   inboundcapacityareacode,
                                   outboundfacilitycode,
								   outboundcapacityareacode,
                                   costperload,
                                   globaladjpct,
                                   facilityadjpct,
                                   laneadjpct,
                                   carrieradjpct,
                                   manualawardamount,
                                   autoawardamount,
                                   packageawardamount,
                                   packageid,
                                   historicalcost,
                                   needsautoaward,
                                   lanevolume,
                                   capacity,
                                   incumbent,
                                   createddttm,
                                   bid_round_num,
                                   numdays)
      SELECT DISTINCT
             lb.laneequipmenttypeid,
             lb.obcarriercodeid,
             s.scenarioid,
             lb.rfpid,
             lb.laneid,
             lb.historicalaward,
             lb.equipmenttype_id,
             lb.servicetype_id,
             lb.protectionlevel_id,
             lb.mot,
             lb.commoditycode_id,
             l.destinationfacilitycode,
			 l.destinationcapacityareacode,
             l.originfacilitycode,
			 l.origincapacityareacode,
             lb.costperload,
             0,
             0,
             0,
             0,
             CAST (NULL AS INTEGER),
             CAST (NULL AS INTEGER),
             CAST (NULL AS INTEGER),
             lb.packageid,
             l.historicalcost,
             1,
             l.volume,
             lb.lanecapacity,
             rp.incumbent,
             lb.createddttm,
             lb.round_num,
             CASE
                WHEN l.volumefrequency = 0 THEN 1
                WHEN l.volumefrequency = 1 THEN 7
                WHEN l.volumefrequency = 2 THEN 14
                WHEN l.volumefrequency = 3 THEN 30.42
                WHEN l.volumefrequency = 4 THEN 182.5
                WHEN l.volumefrequency = 5 THEN 365
                ELSE CAST (NULL AS INTEGER)
             END
        FROM lanebid lb,
             lane l,
             scenario s,
             rfpparticipant rp
       WHERE     lb.obcarriercodeid = I_OBCARRIERCODEID
             AND lb.rfpid = I_RFPID
			 AND L.ROUND_NUM = (SELECT MAX (ml.round_num) FROM lane ml WHERE ml.rfpid = l.rfpid AND ml.laneid  = l.laneid)
			 AND RP.ROUND_NUM = I_ROUND_NUM
             AND s.rfpid = lb.rfpid
             AND l.rfpid = lb.rfpid
             AND rp.rfpid = lb.rfpid
             AND lb.round_num = s.round_num
             AND s.round_num = lb.round_num
             AND rp.obcarriercodeid = lb.obcarriercodeid
             AND l.laneid = lb.laneid
             AND lb.status = 'Bid On'
             AND NOT EXISTS
                        (SELECT 1
                           FROM scenarioadjustment
                          WHERE     rfpid = I_RFPID
                                AND scenarioid = s.scenarioid
                                AND obcarriercodeid = lb.obcarriercodeid
                                AND laneequipmenttypeid =
                                       lb.laneequipmenttypeid
                                AND laneid = lb.laneid
                                AND packageid = lb.packageid
                                AND historicalaward = lb.historicalaward);

   DELETE FROM scenarioadjustment
         WHERE     rfpid = I_RFPID
               AND obcarriercodeid = I_OBCARRIERCODEID
               AND packageid = 0
               AND historicalaward = 0
               AND scenarioid IN
                      (SELECT scenarioid
                         FROM scenario
                        WHERE rfpid = I_RFPID AND round_num = I_ROUND_NUM)
               AND laneequipmenttypeid IN
                      (SELECT laneequipmenttypeid
                         FROM lanebid
                        WHERE     rfpid = I_RFPID
                              AND status = 'Declined'
                              AND obcarriercodeid = I_OBCARRIERCODEID
                              AND round_num = I_ROUND_NUM
                              AND historicalaward = 0
                              AND laneequipmenttypeid =
                                     scenarioadjustment.laneequipmenttypeid
                              AND laneid = scenarioadjustment.laneid
                              AND packageid = 0);

   FOR CURSOR_RR
      IN (SELECT lb.lanecapacity lanecapacity,
                 sa.capacity capacity,
                 sa.scenarioid scenarioid,
                 sa.packageid packageid,
                 sa.historicalaward historicalaward,
                 sa.laneid laneid,
                 sa.laneequipmenttypeid laneequipmenttypeid,
                 sa.laneincluded laneincluded,
                 lb.round_num round_num
            FROM lanebid lb, scenarioadjustment sa, scenario s
           WHERE     lb.rfpid = I_RFPID
                 AND lb.obcarriercodeid = I_OBCARRIERCODEID
                 AND lb.round_num = I_ROUND_NUM
                 AND sa.rfpid = lb.rfpid
                 AND sa.obcarriercodeid = lb.obcarriercodeid
                 AND sa.laneid = lb.laneid
                 AND sa.packageid = lb.packageid
                 AND sa.laneequipmenttypeid = lb.laneequipmenttypeid
                 AND sa.historicalaward = lb.historicalaward
                 AND s.round_num = lb.round_num
                 AND s.rfpid = lb.rfpid
                 AND sa.scenarioid = s.scenarioid)
   LOOP
      IF (CURSOR_RR.lanecapacity <> CURSOR_RR.capacity)
      THEN
         UPDATE scenarioadjustment
            SET capacity = CURSOR_RR.lanecapacity,
                bid_round_num = CURSOR_RR.round_num
          WHERE     rfpid = I_RFPID
                AND scenarioid = CURSOR_RR.scenarioid
                AND obcarriercodeid = I_OBCARRIERCODEID
                AND laneequipmenttypeid = CURSOR_RR.laneequipmenttypeid
                AND laneid = CURSOR_RR.laneid
                AND scenarioid = CURSOR_RR.scenarioid
                AND rfpid = I_RFPID
                AND packageid = CURSOR_RR.packageid
                AND historicalaward = CURSOR_RR.historicalaward;
      END IF;

      IF (CURSOR_RR.laneincluded = 0)
      THEN
         UPDATE scenarioadjustment
            SET laneincluded = 0
          WHERE     rfpid = I_RFPID
                AND scenarioid = CURSOR_RR.scenarioid
                AND laneid = CURSOR_RR.laneid
                AND laneincluded = 1;
      END IF;
   END LOOP;

   FOR CURSOR_SA
      IN (SELECT DISTINCT sa.laneid, sa.scenarioid
            FROM scenarioadjustment sa, scenario s
           WHERE     sa.rfpid = I_RFPID
                 AND sa.rfpid = s.rfpid
                 AND s.round_num = I_ROUND_NUM
                 AND s.scenarioid = sa.scenarioid
                 AND sa.obcarriercodeid <> I_OBCARRIERCODEID
                 AND sa.laneincluded = 0)
   LOOP
      UPDATE scenarioadjustment
         SET laneincluded = 0
       WHERE     rfpid = I_RFPID
             AND scenarioid = CURSOR_SA.scenarioid
             AND obcarriercodeid = I_OBCARRIERCODEID
             AND laneid = CURSOR_SA.laneid
             AND laneincluded = 1;
   END LOOP;

   FOR CURSOR_RATEADJ
      IN (SELECT RFPID,
                 SCENARIOID,
                 OBCARRIERCODEID,
                 GLOBALADJPCT,
                 FACILITYADJPCT,
                 LANEADJPCT,
                 CARRIERADJPCT,
                 VOLUMEOVERRIDE,
                 VOLUMEOVERRIDEMINMAX,
                 VOLUMEOVERRIDETYPE
            FROM XMLCARRIERADJ
           WHERE RFPID = I_RFPID AND OBCARRIERCODEID = I_OBCARRIERCODEID)
   LOOP
      UPDATE scenarioadjustment
         SET GLOBALADJPCT = CURSOR_RATEADJ.GLOBALADJPCT,
             FACILITYADJPCT = CURSOR_RATEADJ.FACILITYADJPCT,
             LANEADJPCT = CURSOR_RATEADJ.LANEADJPCT,
             CARRIERADJPCT = CURSOR_RATEADJ.CARRIERADJPCT,
             VOLUMEOVERRIDE = CURSOR_RATEADJ.VOLUMEOVERRIDE,
             VOLUMEOVERRIDEMINMAX = CURSOR_RATEADJ.VOLUMEOVERRIDEMINMAX,
             VOLUMEOVERRIDETYPE = CURSOR_RATEADJ.VOLUMEOVERRIDETYPE
       WHERE     rfpid = I_RFPID
             AND scenarioid = CURSOR_RATEADJ.scenarioid
             AND obcarriercodeid = I_OBCARRIERCODEID;
   END LOOP;

   DELETE FROM XMLCARRIERADJ
         WHERE RFPID = I_RFPID AND OBCARRIERCODEID = I_OBCARRIERCODEID;

   UPDATE scenarioadjustment
      SET needsautoaward = 1
    WHERE rfpid = I_RFPID
          AND scenarioid IN
                 (SELECT scenarioid
                    FROM scenario
                   WHERE rfpid = I_RFPID AND round_NUM = I_ROUND_NUM);

   DELETE FROM ob200override_adv
         WHERE     rfpid = I_RFPID
               AND source = 'B'
               AND level_of_detail IN ('X', 'C')
               AND scenarioid IN
                      (SELECT scenarioid
                         FROM scenario
                        WHERE rfpid = I_RFPID AND round_NUM = I_ROUND_NUM);

   UPDATE scenario
      SET status = 'U'
    WHERE rfpid = I_RFPID AND status != 'D' AND round_num = I_ROUND_NUM;
END POPULATE_SA_ENTRIES;
/
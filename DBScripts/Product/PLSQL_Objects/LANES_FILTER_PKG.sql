create or replace
PACKAGE LANES_FILTER_PKG
AS
 TYPE rate_filter_curtype IS REF CURSOR;
  gv_companyHierarchy      VARCHAR2(12500);
  
 TYPE SPLIT_ARRAY IS TABLE OF VARCHAR2(50) INDEX BY BINARY_INTEGER;
 
 PROCEDURE GET_RATE_DATA_FROM_FILTER_CSV
 (
			row_count                    OUT NUMBER,
			rate_filter_refcur           OUT rate_filter_curtype,
            coid                         IN rating_lane.tc_company_id%TYPE,
            ofacid                       IN VARCHAR2,
            ocity                        IN rating_lane.o_city%TYPE,
            ocnty                        IN rating_lane.o_county%TYPE,
            ost                          IN rating_lane.o_state_prov%TYPE,
            ozip                         IN rating_lane.o_postal_code%TYPE,
            ocountry                     IN rating_lane.o_country_code%TYPE,
            ozone                        IN rating_lane.o_zone_id%TYPE,
            dfacid                       IN VARCHAR2,
            dcity                        IN rating_lane.d_city%TYPE,
            dcnty                        IN rating_lane.d_county%TYPE,
            dst                          IN rating_lane.d_state_prov%TYPE,
            dzip                         IN rating_lane.d_postal_code%TYPE,
            dcountry                     IN rating_lane.d_country_code%TYPE,
            dzone                        IN rating_lane.d_zone_id%TYPE,
            idt                          IN DATE,
            ccode                        IN VARCHAR2,
            mot                          IN rating_lane_dtl.mot_id%TYPE,
            equip                        IN rating_lane_dtl.equipment_id%TYPE,
            sl                           IN rating_lane_dtl.service_level_id%TYPE,
            pl                           IN rating_lane_dtl.protection_level_id%TYPE,
         originVia                    IN rating_lane_dtl.o_ship_via%TYPE DEFAULT NULL,
         destinationVia               IN rating_lane_dtl.d_ship_via%TYPE DEFAULT NULL,
         contractNumber               IN rating_lane_dtl.contract_number%TYPE DEFAULT NULL,
         pkgName                     IN rating_lane_dtl.PACKAGE_NAME%TYPE DEFAULT NULL,
         sailingSchdName             IN rating_lane_dtl.SAILING_SCHEDULE_NAME%TYPE DEFAULT NULL,
         search_type                  IN VARCHAR2 DEFAULT NULL,
         sort_field                   IN VARCHAR2,
         sort_direction               IN VARCHAR2,
          error_indicator              IN NUMBER,
         start_row                    IN NUMBER,
         end_row                      IN NUMBER,
         
         
          aRgQualifier                 IN RG_LANE.RG_QUALIFIER%TYPE DEFAULT NULL,
            companyIdList                IN VARCHAR2,
             COMPANYHIERARCHY   IN VARCHAR2,
             CUSTOMER                     IN VARCHAR2,
            LANE_NAME                    IN VARCHAR2,
            INCOTERM_ID                  IN NUMBER,
            BILLING_ID                   IN NUMBER,
            ROUTE_TO                     IN VARCHAR2,
            ROUTE_TYPE_1                 IN VARCHAR2,
            ROUTE_TYPE_2                 IN VARCHAR2,
            SHIP_PARAM_WHERE_CLAUSE      IN VARCHAR2,
			USE_EPI						 IN NUMBER,
			EPI_SERVICE_GROUP			 IN VARCHAR2
   ) ;

 FUNCTION fnFormatFilterLocWhereClause
        (
  aSearchType IN VARCHAR2,
        loc_prefix IN VARCHAR2,
        coid IN rating_lane.tc_company_id%TYPE,
        facid IN VARCHAR2,
        city IN rating_lane.o_city%TYPE,
        cnty IN rating_lane.o_county%TYPE,
        st IN rating_lane.o_state_prov%TYPE,
        zip IN rating_lane.o_postal_code%TYPE,
        COUNTRY IN rating_lane.o_country_code%TYPE,
        ZONE IN rating_lane.o_zone_id%TYPE
        )
    RETURN VARCHAR2;

 FUNCTION fnGetZoneListCsv
     (
     coid IN rg_lane.tc_company_id%TYPE,
     facid IN VARCHAR2,
     city IN rg_lane.o_city%TYPE,
     st IN rg_lane.o_state_prov%TYPE,
     zip IN rg_lane.o_postal_code%TYPE,
     COUNTRY IN rg_lane.o_country_code%TYPE
     )
 RETURN VARCHAR2
 ;

 FUNCTION fnFormatImportLocFilter
        (
  aSearchType IN VARCHAR2,
        loc_prefix IN VARCHAR2,
        coid IN rating_lane.tc_company_id%TYPE,
        facid IN VARCHAR2,
        city IN rating_lane.o_city%TYPE,
        cnty IN rating_lane.o_county%TYPE,
        st IN rating_lane.o_state_prov%TYPE,
        zip IN rating_lane.o_postal_code%TYPE,
        COUNTRY IN rating_lane.o_country_code%TYPE,
        ZONE IN rating_lane.o_zone_id%TYPE
        )
    RETURN VARCHAR2
 ;

 FUNCTION fnCreateCsvSubstr
 (
   aString    VARCHAR2,
   aLength    NUMBER,
   aAllLanes  BOOLEAN
 )
 RETURN VARCHAR2;
 
FUNCTION SPLIT_COMMA_SEPETATED_STR 
 (
      P_IN_STRING VARCHAR2, 
      P_DELIM VARCHAR2
 ) 
 RETURN SPLIT_ARRAY;
 
END ;
/

create or replace PACKAGE BODY LANES_FILTER_PKG
AS

	PROCEDURE get_rate_data_from_filter_csv
            (
			row_count                    OUT NUMBER,
            rate_filter_refcur           OUT rate_filter_curtype,
            coid                         IN rating_lane.tc_company_id%TYPE,
            ofacid                       IN VARCHAR2,
            ocity                        IN rating_lane.o_city%TYPE,
            ocnty                        IN rating_lane.o_county%TYPE,
            ost                          IN rating_lane.o_state_prov%TYPE,
            ozip                         IN rating_lane.o_postal_code%TYPE,
            ocountry                     IN rating_lane.o_country_code%TYPE,
            ozone                        IN rating_lane.o_zone_id%TYPE,
            dfacid                       IN VARCHAR2,
            dcity                        IN rating_lane.d_city%TYPE,
            dcnty                        IN rating_lane.d_county%TYPE,
            dst                          IN rating_lane.d_state_prov%TYPE,
            dzip                         IN rating_lane.d_postal_code%TYPE,
            dcountry                     IN rating_lane.d_country_code%TYPE,
            dzone                        IN rating_lane.d_zone_id%TYPE,
            idt                          IN DATE,
            ccode                        IN VARCHAR2,
            mot                          IN rating_lane_dtl.mot_id%TYPE,
            equip                        IN rating_lane_dtl.equipment_id%TYPE,
            sl                           IN rating_lane_dtl.service_level_id%TYPE,
            pl                           IN rating_lane_dtl.protection_level_id%TYPE,
		    originVia                    IN rating_lane_dtl.o_ship_via%TYPE DEFAULT NULL,
		    destinationVia               IN rating_lane_dtl.d_ship_via%TYPE DEFAULT NULL,
			contractNumber               IN rating_lane_dtl.contract_number%TYPE DEFAULT NULL,
		    pkgName		                 IN rating_lane_dtl.PACKAGE_NAME%TYPE DEFAULT NULL,
			sailingSchdName		         IN rating_lane_dtl.SAILING_SCHEDULE_NAME%TYPE DEFAULT NULL,
			search_type                  IN VARCHAR2 DEFAULT NULL,
			sort_field                   IN VARCHAR2,
		 	sort_direction               IN VARCHAR2,
		    error_indicator              IN NUMBER,
			start_row                    IN NUMBER,
			end_row                      IN NUMBER,

    		aRgQualifier                 IN RG_LANE.RG_QUALIFIER%TYPE DEFAULT NULL,
            companyIdList                IN VARCHAR2,
            COMPANYHIERARCHY		         IN VARCHAR2,
            CUSTOMER                     IN VARCHAR2,
            LANE_NAME                    IN VARCHAR2,
            INCOTERM_ID                  IN NUMBER,
            BILLING_ID                   IN NUMBER,
            ROUTE_TO                     IN VARCHAR2,
            ROUTE_TYPE_1                 IN VARCHAR2,
            ROUTE_TYPE_2                 IN VARCHAR2,
            SHIP_PARAM_WHERE_CLAUSE      IN VARCHAR2,
			USE_EPI						 IN NUMBER,
			EPI_SERVICE_GROUP			 IN VARCHAR2
			)
    IS

        sql_statement VARCHAR2(12500);
        sql_statement1 VARCHAR2(12500);
        sql_statement2 VARCHAR2(12500);
        sql_statement3 VARCHAR2(12500);
        sql_statement4 VARCHAR2(12500);
        cQueryFilterCust VARCHAR2(200);
        cQueryFilterBill VARCHAR2(200);
        cQueryFilterInco VARCHAR2(200);
        cQueryFilterRto VARCHAR2(200);
        cQueryFilterRt1 VARCHAR2(200);
        cQueryFilterRt2 VARCHAR2(200);
		cQueryFilterUseEpi VARCHAR2(200);
		cQueryFilterEpiServiceGroup VARCHAR2(200);
        cQueryFilterLaneName VARCHAR2(200);
            cExistClauseStartValid      CONSTANT VARCHAR2(400) :=
			        ' AND ( EXISTS ( SELECT 1 FROM COMB_LANE_dtl srld ' ||
 	                'WHERE srld.tc_company_id = rl.tc_company_id AND srld.lane_id = rl.lane_id ';
            cInClauseStartValid          VARCHAR2(400) ;
            --:=
			       -- 'and ((rl.TC_COMPANY_ID, rl.lane_id) in ' ||
					   -- '(SELECT srld.tc_company_id, srld.lane_id ' ||
						 --  'FROM COMB_LANE_dtl srld ' ||
 	            --          'WHERE srld.tc_company_id = <<<tc_company_id>>>';
            cExistClauseStartImport     CONSTANT VARCHAR2(500) :=
			        ' or  EXISTS ( SELECT 1 FROM IMPORT_COMB_LANE_DTL srld, IMPORT_COMB_LANE irl ' ||
                                       'WHERE rl.TC_COMPANY_ID = irl.TC_COMPANY_ID ' ||
                                       'AND rl.LANE_ID = irl.COMB_LANE_ID ' ||
                                       'AND srld.TC_COMPANY_ID = irl.TC_COMPANY_ID ' ||
                                       'AND srld.LANE_ID = irl.LANE_ID ' ||
                                       'AND irl.LANE_STATUS = 4' ;
            cInClauseStartImport        VARCHAR2(500);

            cExistClauseStartErrImport  CONSTANT VARCHAR2(200) :=
			        ' AND EXISTS ( SELECT 1 FROM IMPORT_COMB_LANE_dtl srld ' ||
					               'WHERE srld.tc_company_id = rl.tc_company_id AND srld.lane_id = rl.lane_id ';
            cInClauseStartErrImport  CONSTANT VARCHAR2(200) :=
			        'and (rl.TC_COMPANY_ID, rl.lane_id) in ' ||
			                    '( SELECT srld.TC_COMPANY_ID, srld.lane_id ' ||
								    'FROM IMPORT_COMB_LANE_dtl srld ' ||
					               'WHERE srld.tc_company_id = <<<tc_company_id>>>';
			-- the aliases to the rating_lane_dtl or import_rating_lane_dtl must be the same
			-- 'srld'
            cSubQueryFilterCarrier      CONSTANT VARCHAR2(85) :=
	    			              ' and srld.carrier_id in ('||ccode||')';

            cSubQueryFilterMot          CONSTANT VARCHAR2(80) :=
			                      ' and srld.mot_id = '|| mot;
            cSubQueryFilterEquipment    CONSTANT VARCHAR2(85) :=
			                      ' and srld.equipment_id = '||equip;
            cSubQueryFilterSl           CONSTANT VARCHAR2(85) :=
			                      ' and srld.SERVICE_LEVEL_id = '||sl;
            cSubQueryFilterPl           CONSTANT VARCHAR2(105) :=
			                      ' and srld.PROTECTION_LEVEL_id = '|| pl;
			      cSubQueryFilterOVia         CONSTANT VARCHAR2(85) :=
			                      ' and srld.O_SHIP_VIA = ''' || originVia ||'''';
			      cSubQueryFilterDVia         CONSTANT VARCHAR2(85) :=
			                      ' and srld.D_SHIP_VIA = '''|| destinationVia ||'''';
			      cSubQueryFilterCn           CONSTANT VARCHAR2(85) :=
			                      ' and srld.CONTRACT_NUMBER = '''|| contractNumber ||'''';
            cSubQueryFilterDate         CONSTANT VARCHAR2(150) :=
                 ' AND TO_DATE(''' || TO_CHAR(idt, 'DD-MON-YYYY') || ''',''DD-MON-YYYY'') >= effective_dt ' ||
                 ' AND TO_DATE(''' || TO_CHAR(idt, 'DD-MON-YYYY') || ''',''DD-MON-YYYY'') <= expiration_dt ';

			cSubQueryFilterPkg           CONSTANT VARCHAR2(100) :=
			                      ' and srld.package_name = '''|| pkgName ||'''';
			cSubQueryFilterSailingSch           CONSTANT VARCHAR2(100) :=
			                      ' and srld.sailing_schedule_name = '''|| sailingSchdName ||'''';

            vSubQuery                   VARCHAR2(2000) := NULL;
            vSubQueryFilter             VARCHAR2(3000) := NULL;
			      vTableName                  VARCHAR2(30);
            origin_where_clause         VARCHAR2(5000);
            destination_where_clause    VARCHAR2(5000);
           -- sql_statement		     	VARCHAR2(12500);
            buid_where_clause           VARCHAR2(200) := NULL;
			rgQualifierWhereClause      VARCHAR2(200) := NULL;
            date_where_clause			VARCHAR2(200);
            error_where_clause			VARCHAR2(200) := NULL;
            order_by_clause				VARCHAR2(200);
			vSQLFrom	                VARCHAR2(11000);
			vSQLWhere               	VARCHAR2(11000);
			vCarrierSql                 VARCHAR2(200);
			vBaseTable                  VARCHAR2(15);
			bSelectiveLaneLevel         BOOLEAN := FALSE;
            vLaneIdColumn               VARCHAR2(30);
            vRgQualifierColumn          VARCHAR2(30) := NULL;
			vSqlErrm                    VARCHAR2(255);
			vStatusClause               VARCHAR2(255) := NULL;
			oZoneName					VARCHAR2(15):=NULL;
			dZoneName					VARCHAR2(15):=NULL;
			inside_join_clause					VARCHAR2(2000);
			outside_join_clause					VARCHAR2(2000);
			-- bSelectiveLaneLevel
			-- this set to true if the lane level information is filtered by business_unit, rg_qualifier,
			-- or one of the search locations
			-- The subqueies into the detail level will then be build based on the following logic
			-- if Selective -- subquery will be an exists clause
			-- if Not Selective -- subquery will be an in clause based on the tc_company_id and lane_id and
			---     the results of the subquery
     BEGIN


            gv_companyHierarchy := COMPANYHIERARCHY;

   cInClauseStartValid := 'and ((rl.TC_COMPANY_ID, rl.lane_id) in ' ||
					   '(SELECT srld.tc_company_id, srld.lane_id ' ||
						  'FROM COMB_LANE_dtl srld ' ||
 	                     'WHERE srld.tc_company_id = <<<tc_company_id>>>' ;

  cInClauseStartImport :=
			        'or (rl.TC_COMPANY_ID, rl.lane_id) in (' ||
			                      'SELECT srld.tc_company_id, irl.LANE_ID ' ||
					                'FROM IMPORT_COMB_LANE_DTL srld, IMPORT_COMB_LANE irl ' ||
                                     'WHERE srld.TC_COMPANY_ID = <<<tc_company_id>>> ' ||
                                     'AND srld.TC_COMPANY_ID = irl.TC_COMPANY_ID ' ||
                                     'AND srld.LANE_ID = irl.LANE_ID ' ||
                                     'AND irl.LANE_STATUS = 4' ;

   cQueryFilterLaneName := 'rl.lane_name = '''||lane_name||'''';
   cQueryFilterCust := 'rl.customer_id =(Select customer_id from customer where customer_name ='''||customer||''' and tc_company_id ='||to_char(coid)||') ';
   cQueryFilterBill := 'rl.billing_method = '||TO_CHAR(billing_id);
   cQueryFilterInco := 'rl.incoterm_id = '||TO_CHAR(incoterm_id);
   cQueryFilterRto :=  'rl.route_to = ''' ||route_to||'''';
   cQueryFilterRt1 := 'rl.route_type_1 = ''' ||route_type_1||'''';
   cQueryFilterRt2 := 'rl.route_type_2 =''' ||route_type_2||'''';
   cQueryFilterUseEpi :='rl.USE_EPI = ''' ||USE_EPI||'''';
   cQueryFilterEpiServiceGroup :='rl.EPI_SERVICE_GROUP = ''' ||EPI_SERVICE_GROUP||'''';


        IF error_indicator = 0 OR error_indicator = 1 THEN
			origin_where_clause := fnFormatFilterLocWhereClause(search_type, 'o', coid, ofacid, ocity, ocnty, ost, ozip, ocountry, ozone);
			destination_where_clause := fnFormatFilterLocWhereClause(search_type,'d', coid, dfacid, dcity, dcnty, dst, dzip, dcountry, dzone);
        ELSE
			origin_where_clause := fnFormatImportLocFilter(search_type, 'o', coid, ofacid, ocity, ocnty, ost, ozip, ocountry, ozone);
			destination_where_clause := fnFormatImportLocFilter(search_type,'d', coid, dfacid, dcity, dcnty, dst, dzip, dcountry, dzone);
        END IF;
		vBaseTable := 'COMB_LANE';

		IF (ccode IS NOT NULL) THEN
		    vSubQueryFilter := vSubQueryFilter || cSubQueryFilterCarrier;
		END IF;

    IF (SHIP_PARAM_WHERE_CLAUSE IS NOT NULL) THEN
    	vSubQueryFilter := vSubQueryFilter || SHIP_PARAM_WHERE_CLAUSE ;
   END IF;

    		IF (mot IS NOT NULL) THEN
		    vSubQueryFilter := vSubQueryFilter || cSubQueryFilterMot;
		END IF;
		IF (equip IS NOT NULL) THEN
		    vSubQueryFilter := vSubQueryFilter || cSubQueryFilterEquipment;
		END IF;
		IF (sl IS NOT NULL) THEN
		    vSubQueryFilter := vSubQueryFilter || cSubQueryFilterSl;
		END IF;
		IF (pl IS NOT NULL) THEN
		    vSubQueryFilter := vSubQueryFilter || cSubQueryFilterPl;
		END IF;
		IF (originVia IS NOT NULL) THEN
		    vSubQueryFilter := vSubQueryFilter || cSubQueryFilterOVia;
		END IF;
		IF (destinationVia IS NOT NULL) THEN
		    vSubQueryFilter := vSubQueryFilter || cSubQueryFilterDVia;
		END IF;
		IF (contractNumber IS NOT NULL) THEN
		    vSubQueryFilter := vSubQueryFilter || cSubQueryFilterCn;
		END IF;
		IF (pkgName IS NOT NULL) THEN
		    vSubQueryFilter := vSubQueryFilter || cSubQueryFilterPkg;
		END IF;
		IF (sailingSchdName IS NOT NULL) THEN
		    vSubQueryFilter := vSubQueryFilter || cSubQueryFilterSailingSch;
		END IF;
		IF (idt IS NOT NULL) THEN
		    vSubQueryFilter := vSubQueryFilter || cSubQueryFilterDate;
		END IF;
  	    IF ( error_indicator = 0 OR error_indicator = 1 ) THEN  -- 0 Valid ; 2 Draft
		    --dbms_output.put_line('--- ei 0 or 2 ');
	        vTableName := vBaseTable;
			vLaneIdColumn := 'rl.lane_id';
			IF( error_indicator = 0 ) THEN
				vStatusClause := ' and rl.lane_status = 0 ';
			ELSE
				vStatusClause := ' and rl.lane_status = 1 ';
			END IF;
			IF vSubQueryFilter IS NOT NULL THEN
			    IF bSelectiveLaneLevel THEN
				    --dbms_output.put_line('bSelectiveLaneLevel');
		            vSubQuery := REPLACE(cExistClauseStartValid || vSubQueryFilter || ')' ||
					                CASE error_indicator
									    WHEN 0 THEN cExistClauseStartImport || vSubQueryFilter || '))'
										ELSE ')'
									END
									,'COMB_LANE',vBaseTable);
				ELSE
				    --dbms_output.put_line('not bSelectiveLaneLevel');
		            vSubQuery := REPLACE(cInClauseStartValid || vSubQueryFilter || ')' ||
					                CASE error_indicator
		                                WHEN 0 THEN cInClauseStartImport || vSubQueryFilter || '))'
										ELSE ')'
									END
									,'COMB_LANE',vBaseTable);
				END IF;
			END IF;
  	    ELSE
		    --dbms_output.put_line('--- ei 1 ');
	        vTableName := 'IMPORT_' || vBaseTable;
			-- vLaneIdColumn is set in the query type check above
			IF vSubQueryFilter IS NOT NULL THEN
			    IF bSelectiveLaneLevel THEN
		            vSubQuery := REPLACE(cExistClauseStartErrImport || vSubQueryFilter || ')'
     								,'COMB_LANE',vBaseTable);
				ELSE
		            vSubQuery := REPLACE(cInClauseStartErrImport || vSubQueryFilter || ')'
     								,'COMB_LANE',vBaseTable);
				END IF;
			END IF;
	   	    --:1:2error_where_clause := ' AND ( has_errors <> 0 or dtl_has_errors <> 0 ) ';
		END IF;
		vSQLFrom := ' FROM ' || vTableName || ' rl' ;
		vSQLWhere :=
			  ' WHERE rl.tc_company_id in ( ' || companyIdList  || ' ) ' || vStatusClause
			;
	   IF (origin_where_clause IS NOT NULL) THEN
	   	   vSQLWhere := vSQLWhere || ' AND ' || origin_where_clause;
		   bSelectiveLaneLevel := TRUE;
	   END IF;
	   IF (destination_where_clause IS NOT NULL) THEN
	   	   vSQLWhere := vSQLWhere || ' AND ' || destination_where_clause;
		   bSelectiveLaneLevel := TRUE;
	   END IF;
     --Adding new where clauses

 IF(lane_name IS NOT NULL)THEN
   vSQLWhere := vSQLWhere || ' AND ' || cQueryFilterLaneName;
	 bselectivelanelevel := TRUE;
END IF;

IF(customer IS NOT NULL)THEN
  vSQLWhere := vSQLWhere || ' AND ' || cQueryFilterCust;
	bselectivelanelevel := TRUE;
END IF;

IF(billing_id >= 0)THEN
  vSQLWhere := vSQLWhere || ' AND ' || cQueryFilterBill;
  bselectivelanelevel := TRUE;
END IF;

IF(incoterm_id >= 0)THEN
  vSQLWhere := vSQLWhere || ' AND ' || cQueryFilterInco ;
	bselectivelanelevel := TRUE;
END IF;

IF(route_to IS NOT NULL)THEN
   vSQLWhere := vSQLWhere || ' AND ' || cQueryFilterRto ;
   bselectivelanelevel := TRUE;
END IF;

IF(route_type_1 IS NOT NULL)THEN
  vSQLWhere := vSQLWhere || ' AND ' || cQueryFilterRt1;
	bselectivelanelevel := TRUE;
END IF;

IF(route_type_2 IS NOT NULL)THEN
  vSQLWhere := vSQLWhere || ' AND ' || cQueryFilterRt2;
  bselectivelanelevel := TRUE;
END IF;

IF(USE_EPI IS NOT NULL)THEN
  vSQLWhere := vSQLWhere || ' AND ' || cQueryFilterUseEpi;
  bselectivelanelevel := TRUE;
END IF;

IF(EPI_SERVICE_GROUP IS NOT NULL)THEN
  vSQLWhere := vSQLWhere || ' AND ' || cQueryFilterEpiServiceGroup;
  bselectivelanelevel := TRUE;
END IF;

   vSQLWhere := vSQLWhere || buid_where_clause || rgQualifierWhereClause || vSubQuery;
		order_by_clause := sort_field;
		IF (sort_field like '%IC.INCOTERM_NAME%') THEN
		inside_join_clause := inside_join_clause || ' LEFT OUTER JOIN INCOTERM IC ON rl.INCOTERM_ID = IC.INCOTERM_ID ';
		
		END IF;
		IF (sort_field like '%BM.DESCRIPTION%') THEN
		inside_join_clause := inside_join_clause || ' LEFT OUTER JOIN BILLING_METHOD BM ON rl.BILLING_METHOD = BM.BILLING_METHOD  ';
		
		END IF;
		IF (sort_field like '%ZN.ZONE_NAME%') THEN
		inside_join_clause := inside_join_clause || ' LEFT OUTER JOIN ZONE ZN ON ZN.ZONE_ID = rl.O_ZONE_ID  ';
		
		END IF;
		IF (sort_field like '%ZN1.ZONE_NAME%') THEN								
		inside_join_clause := inside_join_clause || ' LEFT OUTER JOIN ZONE ZN1 ON ZN1.ZONE_ID = rl.D_ZONE_ID  ';
		
		END IF;
		IF (sort_field like '%CU.CUSTOMER_CODE%') THEN
		inside_join_clause := inside_join_clause || ' LEFT OUTER JOIN CUSTOMER CU ON rl.CUSTOMER_ID = CU.CUSTOMER_ID  ';
		
    END IF;
    
		

		oZoneName := '''''';
		dZoneName := '''''';
		if(error_indicator = 4) then
			oZoneName := 'o_zone_name';
			dZoneName := 'd_zone_name';
		end if;


		vSQLWhere := REPLACE(vSQLWhere,'<<<tc_company_id>>>',TO_CHAR(coid));
    sql_statement1 :=', route_to, route_type_1, route_type_2, frequency ';
	   sql_statement3 := --'/* rate_route_filter.get_rate_data_from_filter_csv page detail */ ' ||
	        'SELECT ' || vLaneIdColumn || ' lane_id' ||
			',rl.tc_company_id, lane_hierarchy,o_loc_type, ' ||
            ' o_facility_id, o_facility_alias_id, o_city, o_state_prov, rl.o_county' ||
			' ,o_postal_code, o_country_code, rl.o_zone_id,'||oZoneName||' O_ZONE_NAME, ' ||
            ' d_loc_type, d_facility_id, d_facility_alias_id, d_city, d_state_prov, rl.d_county';

			sql_statement2 :=' ,d_postal_code, d_country_code, rl.d_zone_id,'||dZoneName||' D_ZONE_NAME, ' ||
			TO_CHAR(error_indicator) ||' has_errors' ||
			' ,NVL((SELECT SIGN(MAX(DECODE(ID.' || 'LANE_DTL_STATUS' || ',4,1,0)))' ||
		    '         FROM IMPORT_' || vBaseTable || ' IR, IMPORT_' || vBaseTable || '_DTL ID' ||
		    '        WHERE (rl.LANE_ID = IR.LANE_ID' ||
		    '          AND rl.TC_COMPANY_ID = IR.TC_COMPANY_ID' ||
			'          AND IR.LANE_ID = ID.LANE_ID' ||
			'          AND IR.TC_COMPANY_ID = ID.TC_COMPANY_ID)),0) DTL_HAS_ERRORS' || vRgQualifierColumn ||
	        ' FROM ' || vTableName || ' rl' || inside_join_clause ||
		    ' WHERE (rl.tc_company_id, rl.lane_id) in ' ||
 			  '(select coid,id FROM (' ||
		       ' SELECT ROWNUM rn, tc_company_id coid, lane_id id FROM (' ||
		       ' SELECT rl.tc_company_id, rl.lane_id ' ||
			    vSQLFrom || inside_join_clause || vSQLWhere ||
				order_by_clause ||
				') WHERE ROWNUM <= :ern' ||
				') WHERE RN >= :srn)' ||
			order_by_clause;
      sql_statement4 := ', lane_name, rl.customer_id, rl.billing_method, rl.incoterm_id, is_rating, is_routing, is_sailing ,USE_EPI,EPI_SERVICE_GROUP, rl.created_source,  rl.created_dttm,rl.last_updated_source,rl.last_updated_dttm  ';

	sql_statement := sql_statement3 ||sql_statement4||sql_statement1 || sql_statement2 ;

	   BEGIN
	       EXECUTE IMMEDIATE '/* rate_route_filter.get_rate_data_from_filter_csv count */ SELECT COUNT(*) ' || vSQLFrom || inside_join_clause || vSQLWhere || order_by_clause
		   INTO row_count
		   ;
	       OPEN rate_filter_refcur FOR sql_statement
		   USING end_row,start_row
		   ;

	   EXCEPTION
	   WHEN OTHERS THEN
	       vSqlErrm := SQLERRM;
           RAISE;
	   END;
    END get_rate_data_from_filter_csv;

	FUNCTION fnFormatFilterLocWhereClause
        (
		aSearchType IN VARCHAR2,
        loc_prefix IN VARCHAR2,
        coid IN rating_lane.tc_company_id%TYPE,
        facid IN VARCHAR2,
        city IN rating_lane.o_city%TYPE,
        cnty IN rating_lane.o_county%TYPE,
        st IN rating_lane.o_state_prov%TYPE,
        zip IN rating_lane.o_postal_code%TYPE,
        country IN rating_lane.o_country_code%TYPE,
        ZONE IN rating_lane.o_zone_id%TYPE
        )
    RETURN VARCHAR2
    IS
        formatted_where_clause  VARCHAR2(5000);
        vCountyClause           VARCHAR2(200) := NULL;
		    vSearchString           VARCHAR2(2000) := NULL;
        vcompanyIdList           VARCHAR2(2000) := NULL;
		    bAllLanes               BOOLEAN:= FALSE;
    BEGIN
		/* Horea 11/02/02 - if one of the fields is null the boolean value is null */
		IF( (aSearchType IS NOT NULL AND aSearchType = 'All Lanes' ) AND ( ZONE IS NULL ) ) THEN
			bAllLanes := TRUE;
		END IF;
        IF (facid IS NOT NULL) THEN

       		IF (trim(gv_companyHierarchy) IS NULL) THEN
		        vcompanyIdList:= to_char(coid);
		ELSE
		        vcompanyIdList:= gv_companyHierarchy; --sys_context( cgPackageContext ,'companyHierarchy') ;
        	END IF;

			vSearchString := Lane_Location_Pkg.fnGetFacilityCSVFromAlias3PL(vcompanyIdList,facid);
			IF NOT bAllLanes THEN
			    GOTO CreateWhereClause;
			END IF;
        END IF;
        IF (city IS NOT NULL) THEN
			vSearchString := RTRIM(vSearchString || ',' ||
			        Lane_Location_Pkg.fnGetLocationCSVFromCSV('CS',city,st||'~'||country),',');
    		IF (cnty IS NOT NULL) THEN
             	vCountyClause :=
	                ' and ( rl.' || loc_prefix || '_county IN (' || cnty || ') OR rl.' || loc_prefix || '_county IS NULL )';
		       --  fnFormatContextInClause(REPLACE(cnty,'''',''), loc_prefix || 'CNTY') || ') OR rl.' || loc_prefix || '_county IS NULL )';
			END IF;
			IF ( NOT bAllLanes ) THEN
			   GOTO CreateWhereClause;
			END IF;
        END IF;
        IF (st IS NOT NULL) THEN
			vSearchString := RTRIM(vSearchString || ',' ||
			        Lane_Location_Pkg.fnGetLocationCSVFromCSV('ST',st,country),',');
			IF NOT bAllLanes THEN
			    GOTO CreateWhereClause;
			END IF;
        END IF;
        IF (zip IS NOT NULL) THEN
			vSearchString := REPLACE(
                   RTRIM(vSearchString || ',' ||
			       Lane_Location_Pkg.fnGetLocationCSVFromCSV('P2', fnCreateCsvSubstr( zip, 2, bAllLanes) , country) || ',' ||
			       Lane_Location_Pkg.fnGetLocationCSVFromCSV('P3', fnCreateCsvSubstr( zip, 3, bAllLanes) , country) || ',' ||
			       Lane_Location_Pkg.fnGetLocationCSVFromCSV('P4', fnCreateCsvSubstr( zip, 4, bAllLanes) , country) || ',' ||
			       Lane_Location_Pkg.fnGetLocationCSVFromCSV('P5', fnCreateCsvSubstr( zip, 5, bAllLanes) , country) || ',' ||
			       Lane_Location_Pkg.fnGetLocationCSVFromCSV('P6', fnCreateCsvSubstr( zip, 6, bAllLanes) , country),',')
				   ,',''''','') ;
			IF NOT bAllLanes THEN
			    GOTO CreateWhereClause;
			END IF;
        END IF;
        IF (country IS NOT NULL) THEN
			vSearchString :=  RTRIM(vSearchString || ',' ||
			        Lane_Location_Pkg.fnGetLocationCSVFromCSV('CO',country),',');
			IF NOT bAllLanes THEN
			    GOTO CreateWhereClause;
			END IF;
        END IF;
        IF (ZONE IS NOT NULL) THEN
			vSearchString := RTRIM(vSearchString || ',' ||
			        Lane_Location_Pkg.fnGetLocationCSVFromCSV('ZN',ZONE),',');
		ELSE
            vSearchString := RTRIM(vSearchString || ',' ||
			       Lane_Location_Pkg.fnGetLocationCSVFromCSV('ZN'
				          , fnGetZoneListCsv( coid, facid, city, st, zip, country )
						  ),',') ;
        END IF;
<<CreateWhereClause>>
	   vSearchString := LTRIM(vSearchString,',');
	   IF vSearchString IS NOT NULL THEN
            formatted_where_clause := loc_prefix || '_SEARCH_LOCATION IN (' || vSearchString || ')';
	   END IF
	   ;
       IF vCountyClause IS NOT NULL THEN
	       formatted_where_clause := formatted_where_clause || vCountyClause;
	   END IF
	   ;
       RETURN formatted_where_clause;
    END fnFormatFilterLocWhereClause;
/************************************
*************************************/
FUNCTION fnGetZoneListCsv
    (
    coid IN rg_lane.tc_company_id%TYPE,
    facid IN VARCHAR2,
    city IN rg_lane.o_city%TYPE,
    st IN rg_lane.o_state_prov%TYPE,
    zip IN rg_lane.o_postal_code%TYPE,
    country IN rg_lane.o_country_code%TYPE
    )
RETURN VARCHAR2
IS
  	TYPE ZONE_CURSOR IS REF CURSOR;
    zones ZONE_CURSOR;
    vzone          ZONE.zone_id%TYPE;
    zone_list      VARCHAR2(1000);
	sql_str        VARCHAR2(4000);
	where_clause   VARCHAR2(4000) := NULL;
	date_str       VARCHAR(30);
    vcompanyIdList VARCHAR2(2000) := NULL;
	ZIPARRITEM LANES_FILTER_PKG.SPLIT_ARRAY;
BEGIN
	vcompanyIdList:= gv_companyHierarchy; --sys_context( cgPackageContext ,'companyHierarchy',4000) ;
	--CR 29473 for 3PL
	IF (vcompanyIdList IS NOT NULL)
	THEN
		sql_str :=  'select distinct z1.zone_id ' ||
            	'from zone_attribute z1 ' ||
                'where (z1.tc_company_id in (' || vcompanyIdList || ' ) ) ';
	ELSE
		sql_str :=  'select distinct z1.zone_id ' ||
            	'from zone_attribute z1 ' ||
                'where (z1.tc_company_id = ' || coid || ') ';
	END IF;

	IF( facid IS NOT NULL )
	THEN
		IF (vcompanyIdList IS NOT NULL)
		THEN
	       where_clause := where_clause || '( z1.attribute_type = ''FC'' and z1.attribute_value IN ( select to_char(facility_id) from facility_alias where facility_alias_id in ( '|| facid || ') and tc_company_id in ( ' || vcompanyIdList || ' ) ) ) ';
		ELSE
		   where_clause := where_clause || '( z1.attribute_type = ''FC'' and z1.attribute_value IN ( select to_char(facility_id) from facility_alias where facility_alias_id in ( '|| facid || ') and tc_company_id = ' || coid || ') ) ';
		END IF;
	END IF;
	IF( country IS NOT NULL )
	THEN

  	IF( where_clause IS NOT NULL and facid is not null )
		THEN
			where_clause := where_clause || ' or ' ;
		END IF;
		where_clause := where_clause || '( z1.attribute_type = ''CT'' and z1.attribute_value = ''' || country || ''' ) ';

  	IF( st IS NOT NULL )
		THEN
			where_clause := where_clause || ' or ( z1.attribute_type = ''ST'' and z1.attribute_value = ''' || st || ''' and z1.country_code = ''' || country || ''') ' ;
		END IF;

    IF(city IS NOT NULL AND st IS NOT NULL) THEN
    where_clause := where_clause || ' or ( z1.attribute_type = ''CI'' and z1.attribute_value = ' || city || ' and z1.attribute_value2 = ''' || st || ''' and z1.country_code = ''' || country || ''')' ;
    END IF;

	IF( zip IS NOT NULL ) THEN
    ZIPARRITEM := SPLIT_COMMA_SEPETATED_STR(zip,',');
    FOR I IN 1..ZIPARRITEM.COUNT LOOP
       IF (ZIPARRITEM(I) IS NOT NULL) THEN
        where_clause := where_clause ||
                           'or (z1.attribute_type = ''Z2'' and z1.attribute_value in ( ' || fnCreateCsvSubstr(ZIPARRITEM(I),2,true) || ' ) and z1.country_code = ''' || country || ''') ' ||
                           'or (z1.attribute_type = ''Z3'' and z1.attribute_value in ( ' || fnCreateCsvSubstr(ZIPARRITEM(I),3,true) || ' ) and z1.country_code = ''' || country || ''') ' ||
                           'or (z1.attribute_type = ''Z4'' and z1.attribute_value in ( ' || fnCreateCsvSubstr(ZIPARRITEM(I),4,true) || ' ) and z1.country_code = ''' || country || ''') ' ;
         IF length(TRIM(ZIPARRITEM(I))) >= 5 THEN
         where_clause := where_clause ||
                           'or (z1.attribute_type = ''Z5'' and z1.attribute_value in ( ' || fnCreateCsvSubstr(ZIPARRITEM(I),5,true) || ' ) and z1.country_code = ''' || country || ''') ' ;
         END IF;

         IF length(TRIM(ZIPARRITEM(I))) >= 6 THEN
         where_clause := where_clause ||
                           'or (z1.attribute_type = ''Z6'' and z1.attribute_value in ( ' || fnCreateCsvSubstr(ZIPARRITEM(I),6,true) || ' ) and z1.country_code = ''' || country || ''') ' ;
         END IF;
         where_clause := where_clause ||
            'or	(		z1.ATTRIBUTE_TYPE = ''PR'' ' ||
              'AND	 ' || ZIPARRITEM(I) || '  between z1.attribute_value and z1.attribute_value2 and	z1.COUNTRY_CODE = ''' || country || ''' )';
        END IF;
        END LOOP;
      END IF;
	END IF;
	IF where_clause IS NULL  OR WHERE_CLAUSE =''
	THEN
		RETURN NULL;
	END IF;
	sql_str := sql_str || ' and ( ' || where_clause || ' ) ';

    OPEN zones FOR sql_str;
    LOOP
        FETCH zones INTO vzone;
        EXIT WHEN zones%NOTFOUND;
        zone_list := zone_list || '''' || vzone || ''',';
    END LOOP;
    CLOSE zones;
	zone_list := RTRIM(zone_list,',');
    RETURN zone_list;
END;
/*************************************
*************************************/
FUNCTION fnFormatImportLocFilter
        (
		     aSearchType IN VARCHAR2,
         loc_prefix IN VARCHAR2,
         coid IN rating_lane.tc_company_id%TYPE,
         facid IN VARCHAR2,
         city IN rating_lane.o_city%TYPE,
         cnty IN rating_lane.o_county%TYPE,
         st IN rating_lane.o_state_prov%TYPE,
         zip IN rating_lane.o_postal_code%TYPE,
         country IN rating_lane.o_country_code%TYPE,
         ZONE IN rating_lane.o_zone_id%TYPE
        )
RETURN VARCHAR2
IS
    formatted_where_clause  VARCHAR2(4000);
    zones                   VARCHAR2(2000);
	bAllLanes               BOOLEAN;
	nZipPrecision           NUMBER;
	vPCInList               VARCHAR2(500);
	vZipFilter              VARCHAR2(2000)  := NULL;
BEGIN
   	bAllLanes := (NVL(aSearchType,'exact') = 'All Lanes' AND ZONE IS NULL);
    zones := fnGetZoneListCSV( coid, facid, city, st, zip, country );
    formatted_where_clause := NULL;
    IF (facid IS NOT NULL) THEN
        formatted_where_clause := formatted_where_clause ||
            '(nvl(' || loc_prefix || '_loc_type,''FA'') = ''FA''' ||
                ' and ' || loc_prefix || '_facility_alias_id in (' || facid || '))';
		IF NOT bAllLanes THEN
		    GOTO endFunction;
		END IF;
    END IF;
    IF (city IS NOT NULL) THEN
        IF (formatted_where_clause IS NOT NULL) THEN
            formatted_where_clause := formatted_where_clause || ' or ';
        END IF;
        formatted_where_clause := formatted_where_clause ||
            '(nvl('  || loc_prefix || '_loc_type,''CS'') = ''CS''' ||
                ' and ' || loc_prefix || '_city = ' || city ||
                ' and ' || loc_prefix || '_state_prov = ''' || st || '''' ||
        		' and ' || loc_prefix || '_country_code = ''' || country || '''';
		IF (cnty IS NOT NULL) THEN
           	formatted_where_clause := formatted_where_clause || ' and (' || loc_prefix || '_county = ' || cnty || ' OR ' || loc_prefix || '_county IS NULL )';
		END IF;
		formatted_where_clause := formatted_where_clause || ')';
		IF NOT bAllLanes THEN
		    GOTO endFunction ;
		END IF;
    END IF;
    IF (st IS NOT NULL) THEN
        IF (formatted_where_clause IS NOT NULL) THEN
            formatted_where_clause := formatted_where_clause || ' or ';
        END IF;
        formatted_where_clause := formatted_where_clause ||
            '(nvl(' || loc_prefix || '_loc_type,''ST'') = ''ST''' ||
                ' and ' || loc_prefix || '_state_prov = ''' || st || '''' ||
                ' and ' || loc_prefix || '_country_code = ''' || country || ''')';
		IF NOT bAllLanes THEN
		    GOTO endFunction;
		END IF;
    END IF;
    IF (zip IS NOT NULL) THEN
        nZipPrecision := 1;
		WHILE nZipPrecision < 6
		LOOP
		    nZipPrecision := nZipPrecision + 1;
			vPCInList := fnCreateCsvSubstr(zip,nZipPrecision,bAllLanes);
			IF LENGTH(vPCInList) > 2 THEN
			    vZipFilter := vZipFilter ||
	                ' or (nvl(' || loc_prefix || '_loc_type,''P' || TO_CHAR(nZipPrecision) || ''')' ||
					' = ''P' || TO_CHAR(nZipPrecision) || '''' ||
	                ' and ' || loc_prefix || '_postal_code in (' || vPCInList || ')' ||
	                ' and ' || loc_prefix || '_country_code = ''' || country || ''')' ;
			END IF
			;
		END LOOP
		;
        IF (formatted_where_clause IS NOT NULL) THEN
            formatted_where_clause := formatted_where_clause || ' or ';
        END IF;
		IF vZipFilter IS NOT NULL THEN
		    formatted_where_clause := formatted_where_clause || LTRIM(vZipFilter,' or ');
		END IF
		;
		IF NOT bAllLanes THEN
		    GOTO endFunction;
		END IF;
    END IF;
    IF (country IS NOT NULL) THEN
        IF (formatted_where_clause IS NOT NULL) THEN
            formatted_where_clause := formatted_where_clause || ' or ';
        END IF;
        formatted_where_clause := formatted_where_clause ||
            '(nvl(' || loc_prefix || '_loc_type,''CO'') = ''CO''' ||
                ' and ' || loc_prefix || '_country_code = ''' || country || ''')';
		IF NOT bAllLanes THEN
		    GOTO endFunction;
		END IF;
    END IF;
	IF (ZONE IS NOT NULL) THEN
	     IF (formatted_where_clause IS NOT NULL) THEN
	             formatted_where_clause := formatted_where_clause || ' or ';
	     END IF;
	     formatted_where_clause := formatted_where_clause ||
	         '(nvl(' || loc_prefix || '_loc_type,''ZN'') = ''ZN''' ||
	             ' and ' || loc_prefix || '_zone_id in ( ''' || ZONE || '''))';
		IF NOT bAllLanes THEN
		    GOTO endFunction;
		END IF;
	END IF;
	IF (zones IS NOT NULL) THEN
	     IF (formatted_where_clause IS NOT NULL) THEN
	             formatted_where_clause := formatted_where_clause || ' or ';
	     END IF;
	     formatted_where_clause := formatted_where_clause ||
	         '(nvl(' || loc_prefix || '_loc_type,''ZN'') = ''ZN''' ||
	             ' and ' || loc_prefix || '_zone_id in ( ' || zones || '))';
	END IF;
<<endFunction>>
    IF formatted_where_clause IS NOT NULL THEN
	   formatted_where_clause := 'lane_status = 4 and (' || formatted_where_clause ||')';
	END IF
	;
    RETURN formatted_where_clause;
END ;
/*************************************
*************************************/

FUNCTION fnCreateCsvSubstr
(
     aString    VARCHAR2,
	 aLength    NUMBER,
	 aAllLanes  BOOLEAN
)
RETURN VARCHAR2
IS
  vWorkString    VARCHAR2(500) := REPLACE( aString, '''' ) || ',';
  vResultStr     VARCHAR2(500) := NULL;
  vValue         VARCHAR2(20);
  i              NUMBER := 0;
BEGIN
	WHILE LENGTH(vWorkString) > 1 AND i < 10
	LOOP
	    i := i +1;
	    vValue := SUBSTR(vWorkString,1,INSTR(vWorkString,',') - 1);
		vWorkString := LTRIM(LTRIM(vWorkString,vValue),',');
	    IF aAllLanes THEN
    	    vValue := SUBSTR(vValue, 1, aLength);
	    END IF;
		    IF LENGTH(vValue) = aLength THEN
    	    vResultStr := vResultStr || ',''' || vValue || '''';
		END IF;
			END LOOP;
		RETURN LTRIM(NVL(vResultStr,''''''),',');
END;

FUNCTION SPLIT_COMMA_SEPETATED_STR(P_IN_STRING VARCHAR2,P_DELIM VARCHAR2)
  RETURN SPLIT_ARRAY
IS
  I      NUMBER       :=0;
  POS    NUMBER       :=0;
  LV_STR VARCHAR2(50) := P_IN_STRING;
  STRINGS SPLIT_ARRAY;
BEGIN
  POS := INSTR(LV_STR,P_DELIM,1,1);
  IF (P_IN_STRING IS NOT NULL AND  POS = 0) THEN
    STRINGS(1) := P_IN_STRING;
  END IF;
  WHILE ( POS != 0)
  LOOP
    I := I + 1;
    STRINGS(I) := SUBSTR(LV_STR,1,POS-1);
    LV_STR := SUBSTR(LV_STR,POS+1,LENGTH(LV_STR));
    POS := INSTR(LV_STR,P_DELIM,1,1);
    IF (POS = 0) THEN
      STRINGS(I+1) := LV_STR;
    END IF;
  END LOOP;
  RETURN STRINGS;
END SPLIT_COMMA_SEPETATED_STR;

END;
/

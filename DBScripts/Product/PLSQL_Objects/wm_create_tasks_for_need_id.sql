create or replace procedure wm_create_tasks_for_need_id
(
    p_user_id                 in user_profile.user_id%type,
    p_facility_id             in facility.facility_id%type,
    p_tc_company_id           in wave_parm.tc_company_id%type,
    p_need_id                 in alloc_invn_dtl.need_id%type,
    p_crit_nbr                in task_parm.crit_nbr%type
)
as
begin
    if (p_crit_nbr is null or p_need_id is null)
    then
        raise_application_error(-20050, 'Invalid input ' || p_need_id || '|' || p_crit_nbr);
    end if;

    wm_task_creation_intrnl(p_user_id, p_facility_id, p_tc_company_id, null, null, p_need_id,
        p_crit_nbr);
end;
/
show errors;

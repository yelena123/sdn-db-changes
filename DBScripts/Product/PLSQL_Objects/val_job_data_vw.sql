create or replace force view val_job_data_vw
(
    job_name, val_job_dtl_id, sql_id, owner, bind_val_list_csv, result_list_csv
)
as
select vjh.job_name, vjd.val_job_dtl_id, vjd.sql_id, vjh.owner,
    wm_get_bind_values_for_job_dtl(vjd.val_job_dtl_id),
    wm_get_exp_results_for_job_dtl(vjd.val_job_dtl_id)
from val_job_hdr vjh
join val_job_dtl vjd on vjd.val_job_hdr_id = vjh.val_job_hdr_id
/

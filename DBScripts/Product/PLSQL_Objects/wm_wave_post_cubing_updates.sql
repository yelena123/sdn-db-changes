create or replace procedure wm_wave_post_cubing_updates
(
    p_pick_wave_nbr       in lpn.wave_nbr%type ,
    p_user_id             in ucl_user.user_name%type ,
    p_facility_id         in lpn.c_facility_id%type
)
as
    v_whse                facility.whse%type;
    v_zone_pick_method    wave_parm.zone_pick_method%type;
    v_use_grp_attr        number(1);
    v_code_id             sys_code.code_id%type := '007';
    v_max_log_lvl         number(1);
    v_tc_company_id       wave_parm.tc_company_id%type;
begin
    select f.whse
    into v_whse
    from facility f
    where f.facility_id = p_facility_id;
    
    select coalesce(wp.zone_pick_method, '0'), coalesce(wp.tc_company_id, wm_utils.wm_get_user_bu(p_user_id))
    into v_zone_pick_method, v_tc_company_id
    from wave_parm wp
    where wp.wave_nbr = p_pick_wave_nbr and wp.whse = v_whse;    

    select to_number(coalesce(trim(substr(sc.misc_flags, 8, 1)), '0'))
    into v_max_log_lvl
    from sys_code sc 
    where sc.rec_type = 'S' and sc.code_type = '520' and sc.code_id = v_code_id;

    delete from tmp_log_parm;
    insert into tmp_log_parm
    (
        log_lvl, pgm_id, module, msg_id, ref_code_1, ref_value_1, user_id, whse,
        cd_master_id
    )
    values
    (
        v_max_log_lvl, 'wm_wave_post_cubing_updates', 'WAVE', '1094', v_code_id,
        to_char(p_pick_wave_nbr), p_user_id, v_whse, v_tc_company_id
    );

    merge into lpn l
    using
    (
        select ol.lpn_id, nullif(il.actual_volume, 0) vol, nullif(il.weight, 0) wt, il.length ln, il.width wd, il.height ht
        from lpn ol
        join lpn il on il.tc_lpn_id = ol.tc_reference_lpn_id and il.tc_company_id = ol.tc_company_id
            and il.c_facility_id = ol.c_facility_id and il.inbound_outbound_indicator = 'I'
        where ol.c_facility_id = p_facility_id and ol.wave_nbr = p_pick_wave_nbr
            and ol.inbound_outbound_indicator = 'O' and ol.lpn_creation_code = 1
            and exists
            (
                select 1
                from alloc_invn_dtl aid
                where aid.task_cmpl_ref_nbr = ol.tc_lpn_id and aid.cd_master_id = ol.tc_company_id
                    and aid.whse = v_whse and aid.task_genrtn_ref_nbr = p_pick_wave_nbr
                    and aid.alloc_uom = 'C' and aid.full_cntr_allocd = 'Y'
                    and aid.stat_code = 0
            )
    ) iv on (iv.lpn_id = l.lpn_id)
    when matched then
    update set l.last_updated_source = p_user_id, l.last_updated_dttm = sysdate,
        l.estimated_weight = coalesce(iv.wt, l.estimated_weight),
        l.estimated_volume = coalesce(iv.vol, l.estimated_volume),
	      l.length = coalesce(iv.ln, l.length),
	      l.width  = coalesce(iv.wd,l.width),
	      l.height = coalesce(iv.ht,l.height);
    wm_cs_log('Updated dimension data from ilpns to olpns ' || sql%rowcount);
        
    if (v_zone_pick_method = '0')
    then
        merge into lpn l
        using
        (
            select aid.carton_nbr, aid.cd_master_id,
                min(decode(lh.locn_class, 'R', lh.pull_zone, lh.pick_detrm_zone))
                    keep(dense_rank first order by lh.locn_pick_seq asc) first_zone,
                min(decode(lh.locn_class, 'R', lh.pull_zone, lh.pick_detrm_zone))
                    keep(dense_rank first order by lh.locn_pick_seq desc) last_zone,
                count(distinct decode(lh.locn_class, 'R', lh.pull_zone, lh.pick_detrm_zone)) num_zones
            from alloc_invn_dtl aid
            join locn_hdr lh on lh.locn_id = aid.pull_locn_id
                and decode(lh.locn_class, 'R', lh.pull_zone, lh.pick_detrm_zone) is not null
            where aid.task_genrtn_ref_nbr = p_pick_wave_nbr 
                and aid.task_genrtn_ref_code = '1' and aid.carton_nbr is not null
            group by aid.carton_nbr, aid.cd_master_id
        ) iv on (iv.carton_nbr = l.tc_lpn_id and iv.cd_master_id = l.tc_company_id
            and l.inbound_outbound_indicator = 'O')
        when matched then
        update set l.last_updated_source = p_user_id, l.last_updated_dttm = sysdate,
            l.first_zone = iv.first_zone, l.last_zone = iv.last_zone,
            l.nbr_of_zones = iv.num_zones;  
    elsif (v_zone_pick_method = '1')
    then
        select case when exists
        (
            select 1
            from sys_code sc
            where sc.rec_type = 'S' and sc.code_type = '042' and sc.code_id = '1'
                and substr(sc.misc_flags, 1, 1) = 'Y'
        ) then 1 else 0 end
        into v_use_grp_attr
        from dual;

        if (v_use_grp_attr = 1)
        then
            -- WMi
            merge into lpn l
            using
            (
                select aid.carton_nbr, aid.cd_master_id, min(lg.grp_attr) first_zone,
                    max(lg.grp_attr) last_zone, count(distinct lg.grp_attr) num_zones
                from alloc_invn_dtl aid
                join locn_grp lg on lg.locn_id = aid.pull_locn_id and lg.grp_type = 1
                    and lg.grp_attr is not null
                where aid.task_genrtn_ref_nbr = p_pick_wave_nbr 
                    and aid.task_genrtn_ref_code = '1' and aid.carton_nbr is not null
                group by aid.carton_nbr, aid.cd_master_id
            ) iv on (iv.carton_nbr = l.tc_lpn_id and iv.cd_master_id = l.tc_company_id
                and l.inbound_outbound_indicator = 'O' and l.c_facility_id = p_facility_id)
            when matched then
            update set l.last_updated_source = p_user_id, l.last_updated_dttm = sysdate,
                l.first_zone = iv.first_zone, l.last_zone = iv.last_zone,
                l.nbr_of_zones = iv.num_zones;
        else
            merge into lpn l
            using
            (
                select aid.carton_nbr, aid.cd_master_id,
                    min(lg.grp_attr) keep(dense_rank first order by lh.locn_pick_seq asc) first_zone,
                    min(lg.grp_attr) keep(dense_rank first order by lh.locn_pick_seq desc) last_zone,
                    count(distinct lg.grp_attr) num_zones
                from alloc_invn_dtl aid
                join locn_grp lg on lg.locn_id = aid.pull_locn_id and lg.grp_type = 1
                    and lg.grp_attr is not null
                join locn_hdr lh on lh.locn_id = lg.locn_id
                where aid.task_genrtn_ref_nbr = p_pick_wave_nbr 
                    and aid.task_genrtn_ref_code = '1' and aid.carton_nbr is not null
                group by aid.carton_nbr, aid.cd_master_id
            ) iv on (iv.carton_nbr = l.tc_lpn_id and iv.cd_master_id = l.tc_company_id
                and l.inbound_outbound_indicator = 'O' and l.c_facility_id = p_facility_id)
            when matched then
            update set l.last_updated_source = p_user_id, l.last_updated_dttm = sysdate,
                l.first_zone = iv.first_zone, l.last_zone = iv.last_zone,
                l.nbr_of_zones = iv.num_zones;
        end if;
    else
        null;
    end if;
    wm_cs_log('Updated first/last zone data on olpns ' || sql%rowcount);
end;
/
show errors;

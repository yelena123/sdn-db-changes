create or replace procedure manh_process_secondary_tasks
(
    p_task_desc             in task_hdr.task_desc%type,
    p_task_type             in task_hdr.task_type%type
)
as
begin
    -- create a secondary task for every carton
    insert into tmp_task_creation_task_seq_nbr
    (
        id, next_task_id
    )
    select row_number() over (order by iv.carton_nbr), task_hdr_id_seq.nextval
    from
    (
        select distinct t.carton_nbr
        from tmp_task_creation_selected_aid t
    ) iv;
    wm_cs_log('Calculated secondary task sequences ' || sql%rowcount);

    merge into task_dtl td
    using
    (
        select iv2.alloc_invn_dtl_id, t2.next_task_id, iv2.next_task_seq_nbr
        from
        (
            select t1.alloc_invn_dtl_id,
                dense_rank() over(order by t1.carton_nbr) id,
                row_number() over(partition by t1.carton_nbr
                    order by t1.alloc_invn_dtl_id) next_task_seq_nbr
            from tmp_task_creation_selected_aid t1
        ) iv2
        join tmp_task_creation_task_seq_nbr t2 on t2.id = iv2.id
    ) iv on (iv.alloc_invn_dtl_id = td.alloc_invn_dtl_id)
    when matched then
    update set td.next_task_desc = p_task_desc, td.next_task_type = p_task_type,
        td.mod_date_time = sysdate, td.next_task_seq_nbr = iv.next_task_seq_nbr,
        td.next_task_id = iv.next_task_id;
    wm_cs_log('Updated secondary task sequences ' || sql%rowcount);
end;
/

create or replace
PROCEDURE PRC_MENU_COUNT_IOS
AS
V_ERROR_DETAILS VARCHAR2(2000) ;
BEGIN
   EXECUTE IMMEDIATE 'TRUNCATE TABLE MENU_COUNT_GTT';
  -- IOS : CUSTOMER ORDERS=> PICK
  -- FULLFILMENT->CUSTOMER PICKUPS->PICK [USER COUNT ONLY]
  -- LEVEL 4(INSERT) START
  INSERT
  INTO MENU_COUNT_GTT
    (
      CREATED_DTTM ,
      LAST_UPDATED_DTTM ,
      MENU_COUNT_GTT_ID ,
      COMPANY_ID ,
      PERMISSION_CODE ,
      MENU_COUNT ,
      LAST_UPDATED_SOURCE ,
      FACILITY_ID
    )
    (
      -- LEVEL 3 (SEQUENCE) START
      SELECT SYSDATE ,
        SYSDATE ,
        SEQ_MENU_COUNT_GTT_ID.NEXTVAL ,
        TC_COMPANY_ID ,
        'DO_CUST_ORDER_PICKUPS' ,
        SUM ,
        USR ,
        O_FACILITY_ID
      FROM
        (
        -- LEVEL 2 (SUM) START
        SELECT TC_COMPANY_ID ,
          O_FACILITY_ID ,
          USR ,
          COUNT(1) SUM
        FROM
          (
          -- LEVEL 1(DECODE) START
          SELECT DISTINCT ORDERS.ORDER_ID ,
            ORDERS.TC_COMPANY_ID ,
            'DO_CUST_ORDER_PICKUPS',
            NULL ,
            ORDERS.O_FACILITY_ID ,
            DECODE(ORDERS.DO_STATUS,
                                100, --OPEN
                                'EXTERNAL', -- OPEN NEEDS TO BE COUNTED FOR ALL USERS. HENCE MARK AS EXTERNAL SO THAT IT IS COUNTED FOR ALL USERS.
                                110,  -- ACCEPTED
                                DECODE(ORDERS.LAST_UPDATED_SOURCE_TYPE,2,'EXTERNAL',1,ORDERS.LAST_UPDATED_SOURCE )
                  ) USR
          FROM ORDERS ORDERS ,
            ORDER_LINE_ITEM OLI
          WHERE ORDERS.ORDER_ID                = OLI.ORDER_ID
          AND OLI.ITEM_ID                     IS NOT NULL
          AND ORDERS.O_FACILITY_ID            IS NOT NULL
          -- ORDER TYPE =  CUSTOMER
          AND ORDERS.DO_TYPE                   = 20
          AND ( ( ORDERS.DELIVERY_OPTIONS      = '01' )
          OR (ORDERS.DELIVERY_OPTIONS           = '03')
          OR ( ORDERS.DELIVERY_OPTIONS         = '02'
          AND ORDERS.DESTINATION_ACTION        = '01' ) )
          AND ( ORDERS.DO_STATUS               = 100
          OR ( ORDERS.DO_STATUS               = 110
          AND ORDERS.LAST_UPDATED_SOURCE_TYPE IN ( '1' , '2' ) ) )
          AND OLI.ALLOCATION_SOURCE            ='10'
          AND OLI.DO_DTL_STATUS!               =200
          AND ORDERS.HAS_IMPORT_ERROR          =0
          AND ORDERS.ORDER_ID  NOT IN   ( SELECT LINE.ORDER_ID FROM ORDER_LINE_ITEM LINE  WHERE  LINE.ALLOCATION_SOURCE != 10 AND OLI.ORDER_ID=ORDERS.ORDER_ID  )
           -- LEVEL 1(DECODE) END
          )
        GROUP BY TC_COMPANY_ID ,
          O_FACILITY_ID ,
          USR
          -- LEVEL 2(SUM) END
        )
        -- LEVEL 3(SEQUENCE) END
    )
    -- LEVEL 4(INSERT) END
    ;
  --IOS :: CUSTOMER ORDERS=>PACK
  -- FULFILMENT->SHIP TO CUSTOMER->PICK/PACK
  --SHIP2CUST_PICK_OR_PACK
  -- LEVEL 4 (INSERT) START
  INSERT
  INTO MENU_COUNT_GTT
    (
      CREATED_DTTM ,
      LAST_UPDATED_DTTM ,
      MENU_COUNT_GTT_ID ,
      COMPANY_ID ,
      PERMISSION_CODE ,
      MENU_COUNT ,

      LAST_UPDATED_SOURCE,
      FACILITY_ID
    )
  -- LEVEL 3 ( SEQUENCE) START
  SELECT SYSDATE ,
    SYSDATE ,
    SEQ_MENU_COUNT_GTT_ID.NEXTVAL ,
    INNER_SELECT.TC_COMPANY_ID ,
    'DO_CUST_ORDER' ,
    INNER_SELECT.S2C_PICK_PACK_COUNT ,
    USR ,
    INNER_SELECT.O_FACILITY_ID
  FROM
    (
    -- LEVEL 2(COUNT) START
    SELECT TC_COMPANY_ID ,
      'DO_CUST_ORDER' ,
      COUNT(O_FACILITY_ID) S2C_PICK_PACK_COUNT ,
      USR ,
      O_FACILITY_ID
    FROM
      -- LEVEL 1(DATA) START
      (
      SELECT DISTINCT ORDERS.ORDER_ID ,
        ORDERS.TC_COMPANY_ID ,
        'DO_CUST_ORDER' ,
        DECODE(ORDERS.DO_STATUS,
              110, -- RELEASED
              DECODE(
                    ORDERS.LAST_UPDATED_SOURCE_TYPE,
                    2,
                    'EXTERNAL',
                    1,
                    ORDERS.LAST_UPDATED_SOURCE
                    ),
              130, -- DC ALLOCATED OR READY FOR PACKING
              ORDERS.LAST_UPDATED_SOURCE  ,
              140 , -- IN PACKING
              ORDERS.LAST_UPDATED_SOURCE ,
              185, -- PARTIALLY SHIPPED
              ORDERS.LAST_UPDATED_SOURCE
         )   USR ,
        ORDERS.O_FACILITY_ID
      FROM ORDERS ORDERS ,
        ORDER_LINE_ITEM OLI
      WHERE ORDERS.ORDER_ID           = OLI.ORDER_ID
      AND OLI.ITEM_ID                 IS NOT NULL
      AND ORDERS.O_FACILITY_ID        IS NOT NULL
      AND (
      ( ORDERS.DO_STATUS         IN ( 140,185 ) AND ORDERS.LAST_UPDATED_SOURCE_TYPE=1)
          OR (ORDERS.DO_STATUS IN (110, 130) ))
       -- ORDER TYPE =  CUSTOMER
      AND ORDERS.DO_TYPE              = 20
      AND ( ORDERS.DELIVERY_OPTIONS   = '03'
      OR (ORDERS.DELIVERY_OPTIONS     = '02'
      AND ORDERS.DESTINATION_ACTION   = '01' ) )
      AND OLI.ALLOCATION_SOURCE     ='10'
      AND OLI.DO_DTL_STATUS!        =200
      AND ORDERS.HAS_IMPORT_ERROR   =0
      AND  ORDERS.ORDER_ID  NOT IN  (  SELECT LINE.ORDER_ID FROM ORDER_LINE_ITEM LINE  WHERE   LINE.ALLOCATION_SOURCE != 10 AND   LINE.ORDER_ID=ORDERS.ORDER_ID  )
      )
      -- LEVEL 1(DATA) END
    GROUP BY TC_COMPANY_ID ,
      O_FACILITY_ID, USR
      -- LEVEL 2(COUNT) END
    ) INNER_SELECT
    -- LEVEL 3(SEQUENCE) END
    -- LEVEL 4 (INSERT) END
    ;
  -- IOS : TRANSFER-> PICK
  -- FULFILMENT->SHIP TO LOCATION->XFER-PICK
  -- XFER_PICK
  -- LEVEL 4(INSERT) START
  INSERT
  INTO MENU_COUNT_GTT
    (
      CREATED_DTTM ,
      LAST_UPDATED_DTTM ,
      MENU_COUNT_GTT_ID ,
      COMPANY_ID ,
      PERMISSION_CODE ,
      MENU_COUNT ,
      LAST_UPDATED_SOURCE ,
      FACILITY_ID
    )
    (
      -- LEVEL 3 (SEQUENCE) START
      SELECT SYSDATE ,
        SYSDATE ,
        SEQ_MENU_COUNT_GTT_ID.NEXTVAL ,
        TC_COMPANY_ID ,
        'DO_SHIP_TO_LOCN_PICK' ,
        SUM ,
        USR ,
        O_FACILITY_ID
      FROM (
        -- LEVEL 2(COUNT) START
        (SELECT TC_COMPANY_ID ,
          O_FACILITY_ID ,
          USR ,
          COUNT(1) SUM
        FROM
          (
          -- LEVEL 1(DECODE) START
            SELECT DISTINCT ORDERS.ORDER_ID ,
            ORDERS.TC_COMPANY_ID ,
            'DO_SHIP_TO_LOCN_PICK' ,
            NULL ,
            ORDERS.O_FACILITY_ID ,
            DECODE(ORDERS.DO_STATUS,
                        100, --OPEN
                        'EXTERNAL', -- IF IT IS OPEN, THEN ANY ASSOCIATE CAN PICK IT UP. HENCE MARK IT AS EXTERNAL.
                        110, -- ACCEPTED
                        DECODE(ORDERS.LAST_UPDATED_SOURCE_TYPE,
                                2, -- EXTERNAL SYSTEM
                                'EXTERNAL',
                                1, -- USER
                                ORDERS.LAST_UPDATED_SOURCE )
                   ) USR
          FROM ORDERS ORDERS ,
            ORDER_LINE_ITEM OLI
          WHERE ORDERS.O_FACILITY_ID   IS NOT NULL
          AND
          (
             ( ORDERS.DO_STATUS   =   100 )
          OR ( ORDERS.DO_STATUS   =   110 )
          )
          AND ORDERS.DO_TYPE                   = 60 -- 1.    ORDER TYPE = TRANSFER
          AND ORDERS.HAS_IMPORT_ERROR          = 0
          AND ORDERS.ORDER_ID                  = OLI.ORDER_ID
          AND OLI.ALLOCATION_SOURCE            ='10'
          AND OLI.DO_DTL_STATUS!               =200
          AND ORDERS.ORDER_ID  NOT IN   ( SELECT LINE.ORDER_ID FROM ORDER_LINE_ITEM LINE  WHERE  LINE.ALLOCATION_SOURCE != 10 AND OLI.ORDER_ID=ORDERS.ORDER_ID  )
          AND ORDERS.LAST_UPDATED_SOURCE_TYPE IN ( '1' , '2' )
          AND OLI.ITEM_ID                     IS NOT NULL
          AND  ORDERS.ORDER_ID  NOT IN   (
            SELECT LINE.ORDER_ID FROM ORDER_LINE_ITEM LINE  WHERE
            LINE.ALLOCATION_SOURCE != 10 AND
            OLI.ORDER_ID=ORDERS.ORDER_ID  )
            -- LEVEL 1(DECODE) END
          )
        GROUP BY TC_COMPANY_ID ,
          O_FACILITY_ID ,
          USR
        )
        -- LEVEL 2(COUNT) END
        )
        -- LEVEL 3(SEQUENCE) END
    )
    ;


  -- IOS : TRANSFER->PACK.
  -- FULFILMENT->SHIP TO LOCATION->XFER-PICK/PACK
  -- XFER_PICK_PACK
  -- LEVEL 4 ( INSERT) START
  INSERT
  INTO MENU_COUNT_GTT
    (
      CREATED_DTTM ,
      LAST_UPDATED_DTTM ,
      MENU_COUNT_GTT_ID ,
      COMPANY_ID ,
      PERMISSION_CODE ,
      MENU_COUNT ,
      USER_ID ,
      LAST_UPDATED_SOURCE ,
      FACILITY_ID
    )
  -- LEVEL 3 ( SEQUENCE) START
  SELECT SYSDATE ,
    SYSDATE ,
    SEQ_MENU_COUNT_GTT_ID.NEXTVAL ,
    INNER_SELECT.TC_COMPANY_ID ,
    'DO_TRANSFER' ,
    INNER_SELECT.XFER_PICK_PACK ,
    NULL ,
    USR ,
    INNER_SELECT.O_FACILITY_ID
  FROM
    (
    -- LEVEL 2 ( COUNT) START
    SELECT TC_COMPANY_ID ,
      'DO_TRANSFER' ,
      COUNT(O_FACILITY_ID) XFER_PICK_PACK ,
      NULL ,
      USR ,
      O_FACILITY_ID
    FROM
      -- LEVEL 1 ( DATA) START
      (
      SELECT DISTINCT ORDERS.ORDER_ID ,
        ORDERS.TC_COMPANY_ID ,
        'DO_TRANSFER' ,
        NULL ,
        DECODE(ORDERS.DO_STATUS,
              110, -- RELEASED
              DECODE(
                    ORDERS.LAST_UPDATED_SOURCE_TYPE,
                    2,
                    'EXTERNAL',
                    1,
                    ORDERS.LAST_UPDATED_SOURCE
                    ),
              130, -- DC ALLOCATED OR READY FOR PACKING
              ORDERS.LAST_UPDATED_SOURCE ,
              140 , -- IN PACKING
              ORDERS.LAST_UPDATED_SOURCE ,
              185,  -- PARTIALLY SHIPPED
              ORDERS.LAST_UPDATED_SOURCE
         )   USR ,
        ORDERS.O_FACILITY_ID
      FROM ORDERS ORDERS ,
        ORDER_LINE_ITEM OLI
      WHERE ORDERS.O_FACILITY_ID          IS NOT NULL
      AND ( ( ORDERS.DO_STATUS         IN ( 140,185 ) AND ORDERS.LAST_UPDATED_SOURCE_TYPE=1)
          OR (ORDERS.DO_STATUS IN (110,130) ))
      AND ORDERS.DO_TYPE                   = 60
      AND ORDERS.HAS_IMPORT_ERROR          =0
      AND ORDERS.ORDER_ID                  = OLI.ORDER_ID
      AND OLI.ALLOCATION_SOURCE            = '10'
      AND OLI.DO_DTL_STATUS!               =200
      AND ORDERS.ORDER_ID  NOT IN   ( SELECT LINE.ORDER_ID FROM ORDER_LINE_ITEM LINE  WHERE  LINE.ALLOCATION_SOURCE != 10 AND OLI.ORDER_ID=ORDERS.ORDER_ID  )
      AND OLI.ORDER_ID                     =ORDERS.ORDER_ID
      AND OLI.ITEM_ID                     IS NOT NULL
      AND ORDERS.LAST_UPDATED_SOURCE_TYPE IN ( '1' , '2' )
      AND  ORDERS.ORDER_ID  NOT IN   (
      SELECT LINE.ORDER_ID FROM ORDER_LINE_ITEM LINE  WHERE
      LINE.ALLOCATION_SOURCE != 10 AND
      OLI.ORDER_ID=ORDERS.ORDER_ID  )
      )
      -- LEVEL 1 ( DATA) END
    GROUP BY TC_COMPANY_ID ,
      O_FACILITY_ID ,
      USR
      -- LEVEL 2 ( COUNT) END
    ) INNER_SELECT
    -- LEVEL 3 ( SEQUENCE) END
    -- LEVEL 4 ( INSERT) END
    ;


  -- IOS : INVENTORY=>CYCLE COUNT
  --INVENTORY-> CYCLE COUNT
  -- CYCLE_COUNT
  INSERT
  INTO MENU_COUNT_GTT
    (
      CREATED_DTTM ,
      LAST_UPDATED_DTTM ,
      MENU_COUNT_GTT_ID ,
      COMPANY_ID ,
      PERMISSION_CODE ,
      MENU_COUNT ,
      USER_ID ,
      FACILITY_ID
    )
    (
      -- LEVEL 4 START  ( MENU COUNT SEQUENCE )
      SELECT SYSDATE ,
        SYSDATE ,
        SEQ_MENU_COUNT_GTT_ID.NEXTVAL ,
        COUNT_INNER_SELECT.TC_COMPANY_ID ,
        'CYCLE_COUNT' ,
        COUNT_INNER_SELECT.CYCCNT_COUNT ,
        COUNT_INNER_SELECT.ASSIGNED_USER_ID ,
        COUNT_INNER_SELECT.FACILITY_ID
      FROM
        (
        -- LEVEL 3 START  ( GROUP AND COUNT PER USER)
        SELECT TC_COMPANY_ID ,
          FACILITY_ID ,
          'CYCLE_COUNT' ,
          COUNT(ASSIGNED_USER_ID) CYCCNT_COUNT ,
          ASSIGNED_USER_ID ,
          NULL
        FROM
          (
          -- LEVEL 2 START ( FILTER DATA RANK = 1)
          SELECT TC_COMPANY_ID,
            FACILITY_ID,
            CYCLE_COUNT_REQUEST_ID,
            TC_CYCLE_COUNT_REQUEST_ID,
            CYCLE_COUNT_REQUEST_NAME,
            CYCLE_COUNT_REQUEST_STORE_ID,
            SCHEDULED_DTTM,
            COMPLETE_BY_DTTM,
            EARLIEST_START_BY_DTTM,
            CYCLE_COUNT_REQUEST_STATUS,
            ASSIGNED_USER_ID
          FROM
            (
            -- LEVEL 1 START ( JOIN CCR , CCRS , CCI , CCIA )
            SELECT CCR.TC_COMPANY_ID ,
              CCRS.FACILITY_ID,
              CCR.CYCLE_COUNT_REQUEST_ID,
              CCR.TC_CYCLE_COUNT_REQUEST_ID,
              CCR.CYCLE_COUNT_REQUEST_NAME,
              CCRS.CYCLE_COUNT_REQUEST_STORE_ID,
              CCRS.SCHEDULED_DTTM,
              CCRS.COMPLETE_BY_DTTM,
              CCRS.EARLIEST_START_BY_DTTM,
              CCRS.CYCLE_COUNT_REQUEST_STATUS,
              NVL(CCIA.ASSIGNED_USER_ID,-1) ASSIGNED_USER_ID,
              DECODE(CCIA.CYCLE_COUNT_ASSIGN_STATUS,30,30,NULL) ASSIGN_STATUS,
              RANK() OVER (PARTITION BY CCR.TC_CYCLE_COUNT_REQUEST_ID ORDER BY CCIA.ASSIGNED_USER_ID ) DATARANK
            FROM CYCLE_COUNT_REQUEST CCR
            LEFT JOIN CYCLE_COUNT_REQUEST_STORE CCRS
            ON CCRS.CYCLE_COUNT_REQUEST_ID = CCR.CYCLE_COUNT_REQUEST_ID
            LEFT JOIN CYCLE_COUNT_ITEMS CCI
            ON CCI.CYCLE_COUNT_REQUEST_STORE_ID = CCRS.CYCLE_COUNT_REQUEST_STORE_ID
            LEFT JOIN CYCLE_COUNT_ITEM_ASSIGNMENT CCIA
            ON CCIA.CYCLE_COUNT_ITEM_ID          = CCI.CYCLE_COUNT_ITEM_ID
            WHERE CCR.IS_CANCELLED               = 0
            AND CCRS.CYCLE_COUNT_REQUEST_STATUS != 40
            AND CCRS.CYCLE_COUNT_REQUEST_STATUS !=30
            AND CCI.CYCLE_COUNT_ITEM_STATUS!     =40
            AND CCI.CYCLE_COUNT_ITEM_STATUS!     =60
            AND CCI.CYCLE_COUNT_ITEM_STATUS!     =70

            AND ( ( CCRS.SCHEDULED_DTTM         >= TRUNC(SYSDATE)
            AND CCRS.SCHEDULED_DTTM              < TRUNC(SYSDATE + 1) )
            OR ( CCRS.SCHEDULED_DTTM            IS NULL
            AND CCRS.EARLIEST_START_BY_DTTM      < TRUNC(SYSDATE + 1)
            AND CCRS.COMPLETE_BY_DTTM           >= TRUNC(SYSDATE) ) )

            GROUP BY CCR.TC_COMPANY_ID ,
              CCRS.FACILITY_ID ,
              CCR.CYCLE_COUNT_REQUEST_ID,
              CCR.TC_CYCLE_COUNT_REQUEST_ID,
              CCR.CYCLE_COUNT_REQUEST_NAME,
              CCRS.CYCLE_COUNT_REQUEST_STORE_ID,
              CCRS.SCHEDULED_DTTM,
              CCRS.COMPLETE_BY_DTTM,
              CCRS.EARLIEST_START_BY_DTTM,
              CCRS.CYCLE_COUNT_REQUEST_STATUS,
              CCIA.ASSIGNED_USER_ID,
              CCIA.CYCLE_COUNT_ASSIGN_STATUS
              -- LEVEL 1 END ( JOIN CCR , CCRS , CCI , CCIA )
            )
          WHERE DATARANK     = 1
          AND ASSIGN_STATUS IS NULL
          GROUP BY TC_COMPANY_ID,
            FACILITY_ID,
            CYCLE_COUNT_REQUEST_ID,
            TC_CYCLE_COUNT_REQUEST_ID,
            CYCLE_COUNT_REQUEST_NAME,
            CYCLE_COUNT_REQUEST_STORE_ID,
            SCHEDULED_DTTM,
            COMPLETE_BY_DTTM,
            EARLIEST_START_BY_DTTM,
            CYCLE_COUNT_REQUEST_STATUS,
            ASSIGNED_USER_ID
          ORDER BY COMPLETE_BY_DTTM ASC
            -- LEVEL 2 END ( FILTER DATA RANK = 1)
          )
        GROUP BY TC_COMPANY_ID ,
          FACILITY_ID ,
          ASSIGNED_USER_ID
          -- LEVEL 3 END  ( GROUP AND COUNT PER USER)
        ) COUNT_INNER_SELECT
        -- LEVEL 4 END  ( MENU COUNT SEQUENCE )
    );


-- CYCLE COUNT. MANAGER LEVEL COUNT. USER_ID AS -2 INDICATES MANAGER AND USER_ID AS -1 INDICATES UNASSIGNED.
  INSERT
  INTO MENU_COUNT_GTT
    (
      CREATED_DTTM ,
      LAST_UPDATED_DTTM ,
      MENU_COUNT_GTT_ID ,
      COMPANY_ID ,
      PERMISSION_CODE ,
      MENU_COUNT ,
      USER_ID ,
      FACILITY_ID
    )
    (      -- LEVEL 4 START  ( MENU COUNT SEQUENCE )
      SELECT SYSDATE ,
        SYSDATE ,
        SEQ_MENU_COUNT_GTT_ID.NEXTVAL ,
        COUNT_INNER_SELECT.TC_COMPANY_ID ,
        'CYCLE_COUNT' ,
        COUNT_INNER_SELECT.CYCCNT_COUNT ,
        -2 , -- MANAGER
        COUNT_INNER_SELECT.FACILITY_ID
      FROM
        (
        -- LEVEL 3 START  ( GROUP AND COUNT PER USER)
        SELECT TC_COMPANY_ID ,
          FACILITY_ID ,
          'CYCLE_COUNT' ,
          COUNT(ASSIGNED_USER_ID) CYCCNT_COUNT ,
          -2 ,
          NULL
        FROM
          (
          -- LEVEL 2 START ( FILTER DATA RANK = 1)
          SELECT TC_COMPANY_ID,
            FACILITY_ID,
            CYCLE_COUNT_REQUEST_ID,
            TC_CYCLE_COUNT_REQUEST_ID,
            CYCLE_COUNT_REQUEST_NAME,
            CYCLE_COUNT_REQUEST_STORE_ID,
            SCHEDULED_DTTM,
            COMPLETE_BY_DTTM,
            EARLIEST_START_BY_DTTM,
            CYCLE_COUNT_REQUEST_STATUS,
            ASSIGNED_USER_ID
          FROM
            (
            -- LEVEL 1 START ( JOIN CCR , CCRS , CCI , CCIA )
            SELECT CCR.TC_COMPANY_ID ,
              CCRS.FACILITY_ID,
              CCR.CYCLE_COUNT_REQUEST_ID,
              CCR.TC_CYCLE_COUNT_REQUEST_ID,
              CCR.CYCLE_COUNT_REQUEST_NAME,
              CCRS.CYCLE_COUNT_REQUEST_STORE_ID,
              CCRS.SCHEDULED_DTTM,
              CCRS.COMPLETE_BY_DTTM,
              CCRS.EARLIEST_START_BY_DTTM,
              CCRS.CYCLE_COUNT_REQUEST_STATUS,
              NVL(CCIA.ASSIGNED_USER_ID,-1) ASSIGNED_USER_ID,
              DECODE(CCIA.CYCLE_COUNT_ASSIGN_STATUS,30,30,NULL) ASSIGN_STATUS,
              RANK() OVER (PARTITION BY CCR.TC_CYCLE_COUNT_REQUEST_ID ORDER BY CCIA.ASSIGNED_USER_ID ) DATARANK
            FROM CYCLE_COUNT_REQUEST CCR
            LEFT JOIN CYCLE_COUNT_REQUEST_STORE CCRS
            ON CCRS.CYCLE_COUNT_REQUEST_ID = CCR.CYCLE_COUNT_REQUEST_ID
            LEFT JOIN CYCLE_COUNT_ITEMS CCI
            ON CCI.CYCLE_COUNT_REQUEST_STORE_ID = CCRS.CYCLE_COUNT_REQUEST_STORE_ID
            LEFT JOIN CYCLE_COUNT_ITEM_ASSIGNMENT CCIA
            ON CCIA.CYCLE_COUNT_ITEM_ID          = CCI.CYCLE_COUNT_ITEM_ID
            WHERE CCR.IS_CANCELLED               = 0
            AND CCRS.CYCLE_COUNT_REQUEST_STATUS != 40
            AND CCRS.CYCLE_COUNT_REQUEST_STATUS !=30
            AND CCI.CYCLE_COUNT_ITEM_STATUS!     =40
            AND CCI.CYCLE_COUNT_ITEM_STATUS!     =60
            AND CCI.CYCLE_COUNT_ITEM_STATUS!     =70

            AND ( ( CCRS.SCHEDULED_DTTM         >= TRUNC(SYSDATE)
            AND CCRS.SCHEDULED_DTTM              < TRUNC(SYSDATE + 1) )
            OR ( CCRS.SCHEDULED_DTTM            IS NULL
            AND CCRS.EARLIEST_START_BY_DTTM      < TRUNC(SYSDATE + 1)
            AND CCRS.COMPLETE_BY_DTTM           >= TRUNC(SYSDATE) ) )

            GROUP BY CCR.TC_COMPANY_ID ,
              CCRS.FACILITY_ID ,
              CCR.CYCLE_COUNT_REQUEST_ID,
              CCR.TC_CYCLE_COUNT_REQUEST_ID,
              CCR.CYCLE_COUNT_REQUEST_NAME,
              CCRS.CYCLE_COUNT_REQUEST_STORE_ID,
              CCRS.SCHEDULED_DTTM,
              CCRS.COMPLETE_BY_DTTM,
              CCRS.EARLIEST_START_BY_DTTM,
              CCRS.CYCLE_COUNT_REQUEST_STATUS,
              CCIA.ASSIGNED_USER_ID,
              CCIA.CYCLE_COUNT_ASSIGN_STATUS
              -- LEVEL 1 END ( JOIN CCR , CCRS , CCI , CCIA )
            )
          WHERE DATARANK     = 1
          AND ASSIGN_STATUS IS NULL
          GROUP BY TC_COMPANY_ID,
            FACILITY_ID,
            CYCLE_COUNT_REQUEST_ID,
            TC_CYCLE_COUNT_REQUEST_ID,
            CYCLE_COUNT_REQUEST_NAME,
            CYCLE_COUNT_REQUEST_STORE_ID,
            SCHEDULED_DTTM,
            COMPLETE_BY_DTTM,
            EARLIEST_START_BY_DTTM,
            CYCLE_COUNT_REQUEST_STATUS,
            ASSIGNED_USER_ID
          ORDER BY COMPLETE_BY_DTTM ASC
            -- LEVEL 2 END ( FILTER DATA RANK = 1)
          )
        GROUP BY TC_COMPANY_ID ,
          FACILITY_ID
          -- LEVEL 3 END  ( GROUP AND COUNT PER USER)
        ) COUNT_INNER_SELECT
        -- LEVEL 4 END  ( MENU COUNT SEQUENCE )
)
;

insert into menu_count_gtt ( created_dttm , last_updated_dttm , menu_count_gtt_id , company_id , permission_code , menu_count , user_id , last_updated_source , facility_id )
select sysdate , sysdate , SEQ_MENU_COUNT_GTT_ID.nextval , inner_select.tc_company_id , 'Pullback' , inner_select.menu_count , null , usr , inner_select.o_facility_id
  from (
   -- level 2 ( count) start
  select tc_company_id , 'Pullback' , count(o_facility_id) menu_count , null , usr , o_facility_id from
  -- level 1 ( data) start
(
  select distinct orders.order_id , orders.tc_company_id , 'Pullback' ,  null , null usr,  
  orders.o_facility_id from orders orders , order_line_item oli, company_parameter cp
  where
   orders.o_facility_id is not null and
   orders.do_status in (110) and
   orders.tc_company_id = cp.tc_company_id and
   UPPER(orders.order_type) = UPPER(cp.param_value) and
   cp.param_def_id = 'PULLBACK_ORDER_TYPE' and
   orders.do_type = 40  and
   orders.has_import_error=0  and
   orders.order_id =  oli.order_id and
   oli.allocation_source ='10' and
   oli.do_dtl_status!=200    AND
   oli.do_dtl_status = 110 and
   oli.order_id=orders.order_id and
   oli.item_id is not null
    )
    -- level 1 ( data) end
   group by tc_company_id ,  o_facility_id , usr
     -- level 2 ( count) end
     )
inner_select
-- level 3 ( sequence) end
-- level 4 ( insert) end
;

insert into menu_count_gtt ( created_dttm , last_updated_dttm , menu_count_gtt_id , company_id , permission_code , menu_count , user_id , last_updated_source , facility_id )
select sysdate , sysdate , SEQ_MENU_COUNT_GTT_ID.nextval , inner_select.tc_company_id , 'Store to Store' , inner_select.menu_count , null , usr , inner_select.o_facility_id
  from (
   -- level 2 ( count) start
  select tc_company_id , 'Store to Store' , count(o_facility_id) menu_count , null , usr , o_facility_id from
  -- level 1 ( data) start
(
  select distinct orders.order_id , orders.tc_company_id , 'Store to Store' ,  null , null usr,  
  orders.o_facility_id from orders orders , order_line_item oli, company_parameter cp
  where
   orders.o_facility_id is not null and
   orders.do_status in (110) and
   orders.tc_company_id = cp.tc_company_id and
   UPPER(orders.order_type) = UPPER(cp.param_value) and
   cp.param_def_id = 'STORE_TRANSFER_ORDER_TYPE' and
   orders.do_type = 40  and
   orders.has_import_error=0  and
   orders.order_id =  oli.order_id and
   oli.allocation_source ='10' and
   oli.do_dtl_status!=200 and
   oli.order_id=orders.order_id and
   oli.item_id is not null
    )
    -- level 1 ( data) end
   group by tc_company_id ,  o_facility_id , usr
     -- level 2 ( count) end
     )
inner_select
-- level 3 ( sequence) end
-- level 4 ( insert) end
;


  --ADDS THE EXTERNAL COUNTS TO THE USER LEVEL COUNTS
  --UPDATE MENU_COUNT M1
  --SET M1.MENU_COUNT = ( NVL(M1.MENU_COUNT,0) + NVL(
   -- (SELECT NVL(M2.MENU_COUNT,0)
   -- FROM MENU_COUNT M2
   -- WHERE M2.COMPANY_ID        = M1.COMPANY_ID
   -- AND M1.FACILITY_ID         =M2.FACILITY_ID
  --  AND M2.PERMISSION_CODE     = M1.PERMISSION_CODE
  --  AND M2.LAST_UPDATED_SOURCE = 'EXTERNAL'
  --  ),0) )
-- WHERE M1.PERMISSION_CODE   IN ( 'DO_CUST_ORDER_PICKUPS' , 'DO_CUST_ORDER' , 'DO_SHIP_TO_LOCN_PICK' , 'DO_TRANSFER' )
--  AND M1.LAST_UPDATED_SOURCE <> 'EXTERNAL';

EXECUTE IMMEDIATE 'TRUNCATE TABLE MENU_COUNT';

INSERT INTO MENU_COUNT SELECT * FROM MENU_COUNT_GTT;

COMMIT ;

EXCEPTION
  WHEN OTHERS THEN ROLLBACK;
   V_ERROR_DETAILS := SUBSTR(TO_CHAR(SQLCODE) ||'<--->'|| SQLERRM ,1,2000);
   DBMS_OUTPUT.PUT_LINE( 'ERROR ---> '  ||  V_ERROR_DETAILS ) ;



END PRC_MENU_COUNT_IOS;
/

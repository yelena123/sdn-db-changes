CREATE OR REPLACE procedure purge_cl_message
(
    p_commit_frequency  in number default 100000,
    p_purge_days        in number default 0
)
as
v_msg                          varchar2(1000);
dstart                         timestamp(6);
v_sql                          varchar2(9000);
v_cl_message_purged            pls_integer := 0;
v_cl_endpoint_queue_purged     pls_integer := 0;
v_cl_message_keys_purged       pls_integer := 0;
v_purge_days                   number(9) := 0;
v_whse                         varchar2(10) := '';

TYPE tIDs IS TABLE OF number(38) INDEX BY binary_integer;
arrIDs      tIDs;
arrIDs_1    tIDs;

istart                      pls_integer default 0;
istop                       pls_integer default 0;
v_commit_frequency          number(9) default 100000;

begin
    --$Revision: 2$
    if p_purge_days > 7
    then
       v_msg := 'CL MESSAGE PURGE: p_purge_days value must be less than or equal to 7. Exiting...';
       insert into msg_log(msg_log_id, module, msg_id, pgm_id, msg, whse,log_date_time, create_date_time, mod_date_time)
       values(msg_log_id_seq.nextval, 'SYSCONTROL', '9999', 'purge_cl_message', v_msg, v_whse,sysdate, sysdate, sysdate);
    else
       v_purge_days := p_purge_days;
       if p_commit_frequency < 100000
       then
          v_commit_frequency := 100000;
       else
          v_commit_frequency := p_commit_frequency;
       end if;
       
       dstart := systimestamp;

       v_msg := 'CL MESSAGE PURGE started...';
       insert into msg_log(msg_log_id, module, msg_id, pgm_id, msg, whse,
          log_date_time, create_date_time, mod_date_time)
       values(msg_log_id_seq.nextval, 'SYSCONTROL', '9999', 'purge_cl_message', v_msg, v_whse,
          sysdate, sysdate, sysdate);
          
          
       -- insert into tmp_cl_endpoint_queue(endpoint_queue_id)
       select endpoint_queue_id
       bulk collect into arrIDs
       from cl_endpoint_queue
       where when_status_changed <= dstart - v_purge_days
       and status <> 10; -- busy

       -- select purge-able msg ids for cl_message and cl_message_keys
       --insert into tmp_cl_message(msg_id)
       select msg_id
       bulk collect into arrIDs_1
       from
       ( select distinct o.msg_id
         from cl_endpoint_queue o
         where when_status_changed <= dstart - v_purge_days
         and status <> 10 -- busy
         union
         select msg_id
         from cl_message cm
         where not exists (
                           select 1
                           from cl_endpoint_queue ceq
                           where ceq.msg_id = cm.msg_id
                           )
       );   
       
        istart := 0;
        istop  := 0;       

        while istop < arrIDs.count 
        loop
            istart := istop + 1;
            if (arrIDs.count < (istop + v_commit_frequency)) or
               (v_commit_frequency = -1) then
                -- commit size is less than array size or
                -- we're told to skip incremental commits (pcommit_size=-1)
                istop := arrIDs.count;
            else
                istop := istop + v_commit_frequency;
            end if;
        
            forall i in istart..istop
                EXECUTE IMMEDIATE 'insert into cl_endpoint_queue_history '
                                || 'select * from cl_endpoint_queue ceq '
                                || 'where ceq.endpoint_queue_id = :B1'  using arrIDs(i);
                                
            -- delete the same
            forall i in istart..istop 
                EXECUTE IMMEDIATE 'DELETE FROM cl_endpoint_queue ceq WHERE ceq.endpoint_queue_id = :B2' USING arrIDs(i);
          
            -- increment delete count
            v_cl_endpoint_queue_purged := v_cl_endpoint_queue_purged + sql%rowcount;                             
            commit;            
        end loop;
        
        istart := 0;
        istop  := 0;

        while istop < arrIDs_1.count 
        loop
            istart := istop + 1;
            if (arrIDs_1.count < (istop + v_commit_frequency)) or
               (v_commit_frequency = -1) then
                -- commit size is less than array size or
                -- we're told to skip incremental commits (pcommit_size=-1)
                istop := arrIDs_1.count;
            else
                istop := istop + v_commit_frequency;
            end if;
            
            forall i in istart..istop
                EXECUTE IMMEDIATE 'insert into cl_message_keys_history '
                                || 'select * from cl_message_keys cmk '
                                || 'where cmk.msg_id = :B1'  using arrIDs_1(i);
                                
            -- delete the same
            forall i in istart..istop 
                EXECUTE IMMEDIATE 'DELETE FROM cl_message_keys cmk WHERE cmk.msg_id = :B2' USING arrIDs_1(i);
          
            -- increment delete count
            v_cl_message_keys_purged := v_cl_message_keys_purged + sql%rowcount;                           
            commit;            
        end loop;
        
        istart := 0;
        istop  := 0;        
        
        while istop < arrIDs_1.count 
        loop
            istart := istop + 1;
            if (arrIDs_1.count < (istop + v_commit_frequency)) or
               (v_commit_frequency = -1) then
                -- commit size is less than array size or
                -- we're told to skip incremental commits (pcommit_size=-1)
                istop := arrIDs_1.count;
            else
                istop := istop + v_commit_frequency;
            end if;
        
            forall i in istart..istop
                EXECUTE IMMEDIATE 'insert into cl_message_history '
                                || 'select * from cl_message cm '
                                || 'where cm.msg_id= :B1'  using arrIDs_1(i);
                                
            -- delete the same
            forall i in istart..istop 
                EXECUTE IMMEDIATE 'DELETE FROM cl_message cm WHERE cm.msg_id= :B2' USING arrIDs_1(i);
          
            -- increment delete count
            v_cl_message_purged := v_cl_message_purged + sql%rowcount;   

            commit;            
        
       end loop;
  
       v_msg := v_cl_endpoint_queue_purged || ' cl_endpoint_queue records purged...';
       insert into msg_log(msg_log_id, module, msg_id, pgm_id, msg, whse,
          log_date_time, create_date_time, mod_date_time)
       values(msg_log_id_seq.nextval, 'SYSCONTROL', '9999', 'purge_cl_message', v_msg, v_whse,
          sysdate, sysdate, sysdate);

       v_msg := v_cl_message_keys_purged || ' cl_message_keys records purged...';
       insert into msg_log(msg_log_id, module, msg_id, pgm_id, msg, whse,
          log_date_time, create_date_time, mod_date_time)
       values(msg_log_id_seq.nextval, 'SYSCONTROL', '9999', 'purge_cl_message', v_msg, v_whse,
          sysdate, sysdate, sysdate);

       v_msg := v_cl_message_purged || ' cl_message records purged...';
       insert into msg_log(msg_log_id, module, msg_id, pgm_id, msg, whse,
          log_date_time, create_date_time, mod_date_time)
       values(msg_log_id_seq.nextval, 'SYSCONTROL', '9999', 'purge_cl_message', v_msg, v_whse,
          sysdate, sysdate, sysdate);

       v_msg := 'CL MESSAGE PURGE completed...';
       insert into msg_log(msg_log_id, module, msg_id, pgm_id, msg, whse,
          log_date_time, create_date_time, mod_date_time)
       values(msg_log_id_seq.nextval, 'SYSCONTROL', '9999', 'purge_cl_message', v_msg, v_whse,
          sysdate, sysdate, sysdate);

       commit;
    end if;
exception
when others then
    rollback;
    v_msg := 'error on purge_cl_message: '|| substr(sqlerrm,1,200);
    insert into msg_log (msg_log_id, module, msg_id, pgm_id, msg, whse,
        log_date_time, create_date_time, mod_date_time)
    values(msg_log_id_seq.nextval, 'SYSCONTROL', '8888', 'purge_cl_message', v_msg, v_whse,
        sysdate, sysdate, sysdate);
    commit;
    raise_application_error(-20001, 'error on purge_cl_message: ' || sqlerrm);
end;
/
show errors;
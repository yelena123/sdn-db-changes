create or replace procedure wave_pix_620_08
(
    p_ship_wave_nbr     in  varchar2,
    p_whse              in  varchar2,
    p_user_name         in  ucl_user.user_name%type,
    p_scale             in  number default 0
) 
as  
    v_facility_id        facility.facility_id%type;
    v_proc_stat_code     varchar2(2);
    v_xml_group_attr     varchar2(10);
    v_pix62008           varchar2(4000);
    v_pix62001           varchar2(4000);
    v_pix6200X           varchar2(4000);
    e_pix6200X           varchar2(4000);
    v_ins_ref_col_string      varchar2(1000);
    v_sel_ref_col_string      varchar2(1000);
    v_rnd_threshold           number(5, 4) := case p_scale when 0 then 0
        else power(10, -p_scale)/2 end;
    v_prev_module_name           wm_utils.t_app_context_data;
    v_prev_action_name           wm_utils.t_app_context_data;
    va_bu_cnfg            wm_utils.ta_bu_cnfg;
    v_cnfg_str            varchar2(32000);
    v_bu_inlist_clause    varchar2(1000);	
		
begin
   
    select f.facility_id 
    into v_facility_id
    from facility f
    where whse = p_whse
    and mark_for_deletion = 0
    and rownum < 2;
	
    wm_utils.get_context_info(v_prev_module_name, v_prev_action_name);
    wm_utils.set_context_info(p_module_name => 'WAVE', p_action_name => '620_08_PIX',
        p_client_id => p_ship_wave_nbr);

    wm_merge_pix_cnfg_for_elgbl_bu(p_ucl_user_name => p_user_name,
        p_facility_id => v_facility_id, p_tran_type => '620', p_tran_code => '08', p_actn_code => '01',
        pa_bu_cnfg => va_bu_cnfg);

    v_cnfg_str := va_bu_cnfg.first;
    while (v_cnfg_str is not null)
    loop
	    
        wm_extract_pix_cnfg_data(v_cnfg_str, v_proc_stat_code, v_xml_group_attr,
            v_ins_ref_col_string, v_sel_ref_col_string);
        
        if (v_proc_stat_code != '91' )
        then
            v_pix62008 := ' insert into pix_tran '
                || ' ('
                || '    invn_adjmt_qty, proc_stat_code, cntry_of_orgn, item_id, '
                || '    uom, sku_attr_1, sku_attr_2, sku_attr_3, sku_attr_4, '
                || '    sku_attr_5, invn_adjmt_type, invn_type, prod_stat, '
                || '    batch_nbr, tc_company_id, company_code, season, season_yr, '
                || '    style, style_sfx,color, color_sfx, sec_dim,qual, '
                || '    size_desc, size_range_code, size_rel_posn_in_table, '
                || '    item_name, tran_type, tran_code, tran_nbr, pix_seq_nbr, pix_tran_id, '
                || '    whse, actn_code, sys_user_id, create_date_time, mod_date_time, '
                || '    user_id, wm_version_id, facility_id, xml_group_id '|| v_ins_ref_col_string
                || ' )'
                || ' with giv as '
                || ' ( '
                || ' select alloc_invn_dtl.invn_type, alloc_invn_dtl.prod_stat,'
                || '     alloc_invn_dtl.batch_nbr, alloc_invn_dtl.sku_attr_1,'
                || '     alloc_invn_dtl.sku_attr_2, alloc_invn_dtl.sku_attr_3,'
                || '     alloc_invn_dtl.sku_attr_4, alloc_invn_dtl.sku_attr_5,'
                || '     alloc_invn_dtl.cntry_of_orgn, sum(alloc_invn_dtl.qty_alloc)'
                || '         invn_adjmt_qty,'
                || '     alloc_invn_dtl.alloc_uom, alloc_invn_dtl.item_id,'
                || '     alloc_invn_dtl.cd_master_id'
                || ' from alloc_invn_dtl'
                || ' join invn_need_type on invn_need_type.invn_need_type ='
                || '     alloc_invn_dtl.invn_need_type'
                || '     and invn_need_type.whse = alloc_invn_dtl.whse'
                || '     and invn_need_type.cd_master_id = alloc_invn_dtl.cd_master_id'
                || '     and alloc_invn_dtl.task_genrtn_ref_nbr = :p_ship_wave_nbr '
                || '     and alloc_invn_dtl.whse = :p_whse '
                || '     and alloc_invn_dtl.task_genrtn_ref_code in (''1'', ''44'')'
                || '     and (alloc_invn_dtl.invn_need_type in (53, 54) or'
                || '         invn_need_type.task_cmpl_corr_upd = 6)'
                || '     and alloc_invn_dtl.stat_code <= 91'
                || ' group by alloc_invn_dtl.cntry_of_orgn,'
                || '     alloc_invn_dtl.item_id, alloc_invn_dtl.alloc_uom,'
                || '     alloc_invn_dtl.sku_attr_1, alloc_invn_dtl.sku_attr_2,'
                || '     alloc_invn_dtl.sku_attr_3, alloc_invn_dtl.sku_attr_4,'
                || '     alloc_invn_dtl.sku_attr_5, alloc_invn_dtl.invn_type,'
                || '     alloc_invn_dtl.prod_stat, alloc_invn_dtl.batch_nbr,'
                || '     alloc_invn_dtl.cd_master_id'
                || ' ) '
                || ' select iv.invn_adjmt_qty, iv.pix_create_stat_code,' 
                || '     iv.cntry_of_orgn, iv.item_id, iv.alloc_uom,'
                || '     iv.sku_attr_1, iv.sku_attr_2, iv.sku_attr_3,'
                || '     iv.sku_attr_4, iv.sku_attr_5, ''A'' invn_adjmt_type, '
                || '      iv.invn_type, iv.prod_stat, iv.batch_nbr, iv.cd_master_id,'
                || '     company.company_code, item_cbo.item_season,'
                || '     item_cbo.item_season_year, item_cbo.item_style,'
                || '     item_cbo.item_style_sfx, item_cbo.item_color,'
                || '     item_cbo.item_color_sfx, item_cbo.item_second_dim,'
                || '     item_cbo.item_quality, item_cbo.item_size_desc,'
                || '     item_wms.size_range_code, item_wms.size_rel_posn_in_table,'
                || '     item_cbo.item_name, ''620'', ''08'', pix_tran_id_seq.nextval, '
                || '     rownum pix_seq_nbr, pix_tran_id_seq.nextval, '''|| p_whse || ''','
                || '     ''01'', '''|| p_user_name || ''', sysdate, sysdate, '''|| p_user_name || ''', 1, '
                ||       v_facility_id || ', '
                || ( case when v_xml_group_attr is null then 'null' else ''''
                || v_xml_group_attr ||''' ' end )|| v_sel_ref_col_string  
                || ' from '
                || '     ( '
                || '     select giv.cntry_of_orgn, giv.item_id, giv.alloc_uom,' 
                || '         giv.sku_attr_1, giv.sku_attr_2,giv.sku_attr_3,'
                || '         giv.sku_attr_4, giv.sku_attr_5, giv.invn_type,'
                || '          giv.prod_stat, giv.batch_nbr,'
                || '         giv.cd_master_id, giv.invn_adjmt_qty,'
                || '         row_number() over(partition by giv.cntry_of_orgn,'
                || '         giv.item_id, giv.alloc_uom, giv.sku_attr_1, giv.sku_attr_2,'
                || '         giv.sku_attr_3, giv.sku_attr_4, giv.sku_attr_5, '
                || '         giv.invn_type, giv.prod_stat,' 
                || '          giv.batch_nbr, giv.cd_master_id '
                || '     order by pix_tran_code.cd_master_id nulls last) priority,'
                || '     coalesce(pix_tran_code.pix_create_stat_code, 10)' 
                || '         pix_create_stat_code'
                || '     from giv '
                || '     left join pix_tran_code on coalesce(pix_tran_code.cd_master_id,'
                || '         giv.cd_master_id) = giv.cd_master_id'
                || '     and pix_tran_code.tran_type = ''620'''
                || '     and pix_tran_code.tran_code = ''08'''
                || '     and pix_tran_code.actn_code = ''01'''
                || '     ) iv '
                || ' join item_cbo on item_cbo.item_id = iv.item_id'
                || ' join item_wms on item_wms.item_id = item_cbo.item_id'
                || ' join company on company.company_id = iv.cd_master_id'
                || ' where iv.priority < 2';

            execute immediate v_pix62008 using p_ship_wave_nbr, p_whse;
            wm_log('Released 620 08 01 ' || sql%rowcount, p_ref_code_1 => 'PIX',
                p_ref_value_1 => p_ship_wave_nbr);  
        end if;
        v_cnfg_str := va_bu_cnfg.next(v_cnfg_str);
    end loop;
	
    v_pix62001
        :=  'insert into pix_tran '                               
        || '('
        || '   item_name, pix_tran_id, tran_type, tran_code, tran_nbr,'     
        || '   pix_seq_nbr, proc_stat_code, whse, season, season_yr, style,' 
        || '   style_sfx, color, color_sfx, sec_dim, qual, size_desc,'  
        || '   size_range_code,size_rel_posn_in_table,invn_type, prod_stat,' 
        || '   batch_nbr, sku_attr_1, sku_attr_2, sku_attr_3, sku_attr_4,'  
        || '   sku_attr_5, cntry_of_orgn, invn_adjmt_qty, invn_adjmt_type,'  
        || '   uom, actn_code, sys_user_id, create_date_time, mod_date_time, user_id,'
        || '   wm_version_id, item_id, facility_id, tc_company_id,'         
        || '   company_code, xml_group_id ';
    
    for pix_rec in
    (
        select '620' tran_type, '01' tran_code, '01' actn_code from dual
        union all
        select '620' tran_type, '03' tran_code, '01' actn_code from dual
    )
    loop
        wm_merge_pix_cnfg_for_elgbl_bu(p_ucl_user_name => p_user_name,
            p_facility_id => v_facility_id, p_tran_type => pix_rec.tran_type,
            p_tran_code => pix_rec.tran_code, p_actn_code => pix_rec.actn_code,
            pa_bu_cnfg => va_bu_cnfg);
        
        v_cnfg_str := va_bu_cnfg.first;
        
        while (v_cnfg_str is not null)
        loop
		
        v_bu_inlist_clause := ' and orders.tc_company_id in (' || va_bu_cnfg(v_cnfg_str) || ')';
        
        wm_extract_pix_cnfg_data(v_cnfg_str, v_proc_stat_code, v_xml_group_attr,
            v_ins_ref_col_string, v_sel_ref_col_string);
            
        v_pix6200X := v_pix62001 || v_ins_ref_col_string;
    

   ---Need to discuss if we need to check this in generic proc	
   /* if (v_sel_ref_col_string is null and p_cd_master_id is not null)
    then
        manh_get_pix_ref_code_col_list('620','01','01',null,
            v_ins_ref_col_string_01,v_sel_ref_col_string_01);
        v_pix62001 := v_pix62001 || v_ins_ref_col_string_01;
    end if;
        
    if (v_sel_ref_col_string_03 is null and p_cd_master_id is not null)
    then
        manh_get_pix_ref_code_col_list('620','03','01',null,
            v_ins_ref_col_string_03,v_sel_ref_col_string_03);
        v_pix62003 := v_pix62003 || v_ins_ref_col_string_03;
    end if;   */ 

        if (v_proc_stat_code != '91' )
        then
            e_pix6200X := v_pix6200X                                       
                ||' ) '
                ||'select item_cbo.item_name, pix_tran_id_seq.nextval,'                                                     
                ||'    ''620'',:tran_code, pix_tran_id_seq.nextval,'
                ||'    rownum, '
                ||     coalesce(v_proc_stat_code,'10') ||', '''||p_Whse ||''''    
                ||'    , item_cbo.item_season, item_cbo.item_season_year,' 
                ||'    item_cbo.item_style, item_cbo.item_style_sfx,'             
                ||'    item_cbo.item_color, item_cbo.item_color_sfx,'             
                ||'    item_cbo.item_second_dim, item_cbo.item_quality,'
                ||'    item_cbo.item_size_desc, item_wms.size_range_code,'
                ||'    item_wms.size_rel_posn_in_table, order_line_item.invn_type,'
                ||'    order_line_item.prod_stat,order_line_item.batch_nbr, '      
                ||'    order_line_item.item_attr_1, order_line_item.item_attr_2,'
                ||'    order_line_item.item_attr_3, order_line_item.item_attr_4,' 
                ||'    order_line_item.item_attr_5, order_line_item.cntry_of_orgn,'
                ||'    order_line_item.allocated_qty, ''A'', size_uom.size_uom,' 
                ||'    ''01'', '''|| p_user_name ||''', current_timestamp, current_timestamp, '
                ||'    '''|| p_user_name ||''', 1, order_line_item.item_id,'
                ||     v_facility_id   ||', order_line_item.tc_company_id, '        
                ||'    company.company_code, '
                || ( case when v_xml_group_attr is null then 'null' else ''''
                || v_xml_group_attr ||''' ' end ) 
                || replace(v_sel_ref_col_string,'ORDER_LINE_ITEM.TC_ORDER_LINE_ID',
                   'prnt.tc_order_line_id') 
                ||     ' '
                ||'from orders '                                          
                ||'join order_line_item on order_line_item.order_id = orders.order_id'
                ||'    and order_line_item.is_cancelled = ''0''' 
                ||'    and order_line_item.allocated_qty > ' || to_char(v_rnd_threshold)
                ||'    and orders.is_original_order = 1 '                  
                ||'    and coalesce(orders.wm_order_status,0) != 12 '
                ||'    and orders.o_facility_id = :facility_id '  
                || case when instr(v_sel_ref_col_string, 'ORDER_LINE_ITEM.TC_ORDER_LINE_ID') > 0 then 
                  'join order_line_item prnt on prnt.order_id = orders.order_id '
                ||'    and prnt.line_item_id = coalesce(order_line_item.substituted_parent_line_id, order_line_item.line_item_id) ' 
                  else ' ' end
                ||'join item_cbo on item_cbo.item_id = order_line_item.item_id '             
                ||'join item_wms on item_wms.item_id = item_cbo.item_id '             
                ||'join size_uom on size_uom.size_uom_id = order_line_item.qty_uom_id_base '
                ||'join company on company.company_id = orders.tc_company_id '           
                ||'    and order_line_item.wave_nbr = :ship_wave_nbr '        
                || (case when pix_rec.tran_code = '01' then 
                  '    and (order_line_item.fulfillment_type=''1'' or order_line_item.fulfillment_type is null) '
                         when pix_rec.tran_code = '03' then 
                  '    and order_line_item.fulfillment_type=''3'' ' 
                    else ' ' end)			  
                || v_bu_inlist_clause;
			
            execute immediate e_pix6200X using pix_rec.tran_code, v_facility_id, p_ship_wave_nbr;
            wm_log('Released 620 ' || pix_rec.tran_code ||' '||  pix_rec.actn_code 
                ||' '|| sql%rowcount, p_ref_code_1 => 'PIX', p_ref_value_1 => p_ship_wave_nbr);
        end if;
        
        v_cnfg_str := va_bu_cnfg.next(v_cnfg_str);
        end loop;
    end loop;	
    
    wm_utils.set_context_info(p_module_name => v_prev_module_name,
        p_action_name => v_prev_action_name);
exception
    when no_data_found then
        return;
end;
/
show errors;

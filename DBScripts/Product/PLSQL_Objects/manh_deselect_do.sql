create or replace procedure manh_deselect_do
(
    p_result        out number,
    p_user_id       in varchar2,
    p_facility_id   in number,
    p_company_id    in number,
    p_order_id      in number,
    p_line_item_id  in number,
    p_is_orig_order in number,
    p_pick_wave_nbr in varchar2,
    p_debug_level   in number,
    p_preview_flag  in number default 0
)
as
    v_order_qty           order_line_item.order_qty%type := 0;
    v_adjusted_order_qty      order_line_item.adjusted_order_qty%type := 0;
    v_allocated_qty       order_line_item.allocated_qty%type := 0;
    v_subs_parent_line_id order_line_item.substituted_parent_line_id%type := 0;
    v_ref_order_id        order_line_item.reference_order_id%type := 0;
    v_ref_line_item_id    order_line_item.reference_line_item_id%type := 0;
    v_is_chase_created_line  order_line_item.is_chase_created_line%type := 0;
    v_is_oli_unselected   number(9);
    v_is_oli_shipped      number(9);
begin
    if (p_is_orig_order = 1) 
    then
        -- Check if this line was substituted or not...
        select order_qty,
               adjusted_order_qty,allocated_qty,
               coalesce(substituted_parent_line_id, 0),
               is_chase_created_line
          into v_order_qty, v_adjusted_order_qty, 
               v_allocated_qty,v_subs_parent_line_id,
               v_is_chase_created_line
          from order_line_item
         where order_id = p_order_id
           and line_item_id = p_line_item_id
           and wave_nbr = p_pick_wave_nbr;
        
        -- Above line was not original and do update on substituted...
        if v_subs_parent_line_id > 0 AND p_line_item_id <> v_subs_parent_line_id
        then
          
           if v_is_chase_created_line = 1
           then
              -- Find agg line of Original and put the qty back from the 
              -- newly created chase line.
              select coalesce(reference_order_id, 0),
                     coalesce(reference_line_item_id, 0)
                into v_ref_order_id, v_ref_line_item_id
                from order_line_item
               where order_id = p_order_id
                 and line_item_id = v_subs_parent_line_id
                 and do_dtl_status < 190;     
              
              -- Above no record or DO_DTL_STATUS = 190 [Shipped]
              -- then assume this to be original line
              -- and update substituted_parent_line_item_id to null.
            
              -- Update on original as well aggregated line.
              update order_line_item
                 set order_qty         = order_qty + v_order_qty,
                     adjusted_order_qty    = adjusted_order_qty + v_adjusted_order_qty,
                     allocated_qty     = allocated_qty + v_allocated_qty,
                     user_canceled_qty = user_canceled_qty + v_order_qty,
                     do_dtl_status     = (
                                         case
                                             when order_qty + v_order_qty <= 0 then 200
                                             when allocated_qty + v_allocated_qty <= 0 then 110
                                             when (order_qty + v_order_qty) - (allocated_qty + v_allocated_qty) <= 0 then
                                                 case
                                                     when coalesce(units_pakd, 0) <= 0
                                                         then 130
                                                     when (order_qty + v_order_qty) - coalesce(shipped_qty, 0)
                                                         <= 0 then 190
                                                     when (order_qty + v_order_qty) - coalesce(units_pakd, 0)
                                                         - coalesce(user_canceled_qty + v_order_qty, 0)
                                                         <= 0 then 150
                                                     else 140
                                                 end
                                             else 120
                                         end
                                        ),
                     last_updated_dttm = sysdate,
                     last_updated_source = p_user_id
               where line_item_id in (v_subs_parent_line_id , v_ref_line_item_id)
                 and order_id in (p_order_id, v_ref_order_id);
                
              -- Increment quantities on ORDER_LINE_ITEM_SIZE and SPLIT_ORDER_SIZES for
              -- given v_ref_order_id and v_ref_line_item_id      
              update order_split_size oss1
                 set oss1.split_size_value = (select oss2.split_size_value
                                                from order_split_size oss2
                                               where oss2.order_id = p_order_id
                                                 and oss2.line_item_id =
                                                     p_line_item_id
                                                 and oss1.split_size_uom_id =
                                                     oss2.split_size_uom_id)
              where oss1.order_id = v_ref_order_id
                 and oss1.line_item_id = v_ref_line_item_id;

              update order_line_item_size olis1
                 set olis1.size_value = (select olis2.size_value
                                           from order_line_item_size olis2
                                          where olis2.order_id = p_order_id
                                            and olis2.line_item_id = p_line_item_id
                                            and olis1.size_uom_id =
                                                olis2.size_uom_id)
               where olis1.order_id = v_ref_order_id
                 and OLIS1.LINE_ITEM_ID = V_REF_LINE_ITEM_ID;                
           else
              update order_line_item
                 set order_qty      = order_qty + v_order_qty,
                     adjusted_order_qty = adjusted_order_qty + v_adjusted_order_qty,
                     do_dtl_status  = case when allocated_qty = 0 then 110 else 120 end,
                     last_updated_dttm = sysdate,
                     last_updated_source = p_user_id
               where line_item_id = v_subs_parent_line_id
                 and order_id = p_order_id; 
           end if; 
           
           -- Transfer the quantities...
           update order_split_size oss1
              set split_size_value = (select split_size_value
                                       from order_split_size oss2
                                      where oss2.order_id = p_order_id
                                        and oss2.line_item_id = p_line_item_id
                                        and oss1.split_size_uom_id =
                                            oss2.split_size_uom_id)
            where oss1.order_id = p_order_id
              and oss1.line_item_id = v_subs_parent_line_id;    

          update order_line_item_size olis1
             set size_value = (select size_value
                                 from order_line_item_size olis2
                                where olis2.order_id = p_order_id
                                  and olis2.line_item_id = p_line_item_id
                                  and olis1.size_uom_id = olis2.size_uom_id)
           where olis1.order_id = p_order_id
             and olis1.line_item_id = v_subs_parent_line_id;
    
          
           -- Delete the susbtituted one...
           delete from order_split_size
           where order_id = p_order_id
           and line_item_id = p_line_item_id;
           
           delete from order_line_item_size
           where order_id = p_order_id
            and line_item_id = p_line_item_id;
           
           delete from order_line_item
           where order_id = p_order_id
           and line_item_id = p_line_item_id;
        else
           update order_line_item
           set allocated_qty = 0, do_dtl_status = 110, wave_nbr = null,
           last_updated_dttm = sysdate,
           last_updated_source = p_user_id
           where order_id = p_order_id
           and line_item_id = p_line_item_id
           and wave_nbr = p_pick_wave_nbr
           and ALLOCATED_QTY > 0;
        end if;

        -- below portion to be executed for preview flag deselection
        if (p_preview_flag = 1)
        then
        select count(1)
        into v_is_oli_unselected
        from order_line_item
        where order_id = p_order_id
        and DO_DTL_STATUS = 110;
        
        select count(1)
        into v_is_oli_shipped
        from order_line_item
        where order_id = p_order_id
        and DO_DTL_STATUS between 120 and 190;
        
        if (v_is_oli_unselected > 0) then
          if (v_is_oli_shipped > 0) then
            update orders set do_status = 120 where order_id = p_order_id;
          else
            update orders set do_status = 110 where order_id = p_order_id;
          end if;
        end if;
        end if;
     -- caller is not expected to pass me other than original order , hence commenting below portion.     
    elsif(p_preview_flag = 1) then
    for rec in
    (
        select order_id, line_item_id
          from order_line_item
         where reference_order_id = p_order_id
           and reference_line_item_id = p_line_item_id
           and wave_nbr = p_pick_wave_nbr
           and allocated_qty > 0
    )
    loop
      -- Check if this line was substituted or not...
      select order_qty,
             ADJUSTED_ORDER_QTY,
             coalesce(substituted_parent_line_id, 0)
        into v_order_qty, v_ADJUSTED_ORDER_QTY, v_subs_parent_line_id
        from order_line_item
       where order_id = rec.order_id
         and line_item_id = rec.line_item_id
         and wave_nbr = p_pick_wave_nbr;
    
      -- Above line was not original and do update on substituted...
      if v_subs_parent_line_id > 0 AND
         rec.line_item_id <> v_subs_parent_line_id then
        -- Find Original...
        select coalesce(reference_order_id, 0),
               coalesce(reference_line_item_id, 0)
          into v_ref_order_id, v_ref_line_item_id
          from order_line_item
         where order_id = rec.order_id
           and line_item_id = v_subs_parent_line_id
           and do_dtl_status < 190;
      
        -- Above no record or DO_DTL_STATUS = 190 [Shipped] then assume this to be original line
        -- and update substituted_parent_line_item_id to null.
      
        -- Update on original...
        update order_line_item
           set order_qty      = order_qty + v_order_qty,
               ADJUSTED_ORDER_QTY = ADJUSTED_ORDER_QTY + v_ADJUSTED_ORDER_QTY,
               do_dtl_status  = case when allocated_qty = 0 then 110 else 120 end
         where line_item_id = v_subs_parent_line_id
           and order_id = rec.order_id;
      
        -- Transfer the quantities...
      
        update order_split_size oss1
           set split_size_value = (select split_size_value
                                     from order_split_size oss2
                                    where oss2.order_id = rec.order_id
                                      and oss2.line_item_id =
                                          rec.line_item_id
                                      and oss1.split_size_uom_id =
                                          oss2.split_size_uom_id)
         where order_id = rec.order_id
           and oss1.line_item_id = v_subs_parent_line_id;
      
        update order_line_item_size olis1
           set size_value = (select size_value
                               from order_line_item_size olis2
                              where olis2.order_id = rec.order_id
                                and olis2.line_item_id = rec.line_item_id
                                and olis1.size_uom_id = olis2.size_uom_id)
         where order_id = rec.order_id
           and olis1.line_item_id = v_subs_parent_line_id;
      
        if v_ref_line_item_id > 0 then
          update order_line_item
             set order_qty      = order_qty + v_order_qty,
                 ADJUSTED_ORDER_QTY = ADJUSTED_ORDER_QTY + v_ADJUSTED_ORDER_QTY,
                 do_dtl_status  = case when allocated_qty = 0 then 110 else 120 end
           where order_id = v_ref_order_id
             and line_item_id = v_ref_line_item_id;
        
          -- Increment quantities on ORDER_LINE_ITEM_SIZE and SPLIT_ORDER_SIZES for
          -- given v_ref_order_id and v_ref_line_item_id
        
          update order_split_size oss1
             set oss1.split_size_value = (select oss2.split_size_value
                                            from order_split_size oss2
                                           where oss2.order_id = rec.order_id
                                             and oss2.line_item_id =
                                                 rec.line_item_id
                                             and oss1.split_size_uom_id =
                                                 oss2.split_size_uom_id)
           where oss1.order_id = v_ref_order_id
             and oss1.line_item_id = v_ref_line_item_id;
        
          update order_line_item_size olis1
             set olis1.size_value = (select olis2.size_value
                                       from order_line_item_size olis2
                                      where olis2.order_id = rec.order_id
                                        and olis2.line_item_id =
                                            rec.line_item_id
                                        and olis1.size_uom_id =
                                            olis2.size_uom_id)
           where olis1.order_id = v_ref_order_id
             and olis1.line_item_id = v_ref_line_item_id;
        
        else
        
          update order_line_item
             set allocated_qty = 0, do_dtl_status = 110, wave_nbr = null
           where order_id = rec.order_id
             and line_item_id = rec.line_item_id
             and wave_nbr = p_pick_wave_nbr
             and allocated_qty > 0;
        
        end if;
      
        -- Delete the susbtituted one...
      
        delete from order_split_size
         where order_id = rec.order_id
           and line_item_id = rec.line_item_id;
      
        delete from order_line_item_size
         where order_id = rec.order_id
           and line_item_id = rec.line_item_id;
      
        delete from order_line_item
         where order_id = rec.order_id
           and line_item_id = rec.line_item_id;
      
      else
      
        update order_line_item
           set allocated_qty = 0, do_dtl_status = 110, wave_nbr = null
         where order_id = rec.order_id
           and line_item_id = rec.line_item_id
           and wave_nbr = p_pick_wave_nbr
           and allocated_qty > 0;
      
      end if;
    
      select count(1)
        into v_is_oli_unselected
        from order_line_item
       where order_id = p_order_id
         and do_dtl_status = 110;
    
      select count(1)
        into v_is_oli_shipped
        from order_line_item
       where order_id = p_order_id
         and do_dtl_status between 120 and 190;
    
      if (v_is_oli_unselected > 0) then
        if (v_is_oli_shipped > 0) then
          update orders set do_status = 120 where order_id = p_order_id;
        else
          update orders set do_status = 110 where order_id = p_order_id;
        end if;
      end if;
    
    end loop;
    
    select count(1)
      into v_is_oli_unselected
      from order_line_item
     where order_id = p_order_id
       and do_dtl_status = 110;
    
    select count(1)
      into v_is_oli_shipped
      from order_line_item
     where order_id = p_order_id
       and do_dtl_status between 120 and 190;
    
    if (v_is_oli_unselected > 0) then
      if (v_is_oli_shipped > 0) then
        update orders set do_status = 120 where order_id = p_order_id;
      else
        update orders set do_status = 110 where order_id = p_order_id;
      end if;
    end if;
    
    end if;
    
    p_result := 0;
end;
/

CREATE OR REPLACE PROCEDURE wm_dom_sync_txn_data (
   p_company_id   IN     NUMBER,
   p_whse         IN     VARCHAR2,
   p_sync_type    IN     VARCHAR2,
   p_txn_data        OUT types.cursortype)
AS
BEGIN
   IF (p_sync_type = 'ASN')
   THEN
      OPEN p_txn_data FOR
           SELECT a.tc_company_id,
                  a.destination_facility_alias_id,
                  ad.tc_purchase_orders_id,
                  ad.tc_po_line_id,
                  a.tc_asn_id,
                  ad.sku_name,
                  (NVL (SUM (ad.shipped_qty), 0)
                   - NVL (SUM (ad.received_qty), 0)),
                  TO_CHAR (SYSDATE, 'YYYYMMDD')
             FROM asn_detail ad JOIN asn a ON a.asn_id = ad.asn_id
            WHERE     a.asn_status < 40
                  AND a.tc_company_id = p_company_id
                  AND a.destination_facility_alias_id = p_whse
         GROUP BY a.tc_company_id,
                  ad.sku_name,
                  a.tc_asn_id,
                  ad.tc_purchase_orders_id,
                  ad.tc_po_line_id,
                  a.destination_facility_alias_id;
   ELSIF (p_sync_type = 'DO_LINE')
   THEN
      OPEN p_txn_data FOR
           SELECT o.tc_company_id,
                  o.o_facility_alias_id,
                  o.tc_order_id,
                  oli.tc_order_line_id,
                  CASE
                     WHEN oli.do_dtl_status IN (100, 105, 112, 115)
                     THEN
                        5
                     WHEN oli.do_dtl_status = 110
                     THEN
                        10
                     WHEN oli.do_dtl_status IN
                             (130, 140, 150, 160, 165, 170, 180)
                     THEN
                        20
                     WHEN oli.do_dtl_status = 185
                     THEN
                        30
                     WHEN oli.do_dtl_status IN (190, 195)
                     THEN
                        40
                     WHEN oli.do_dtl_status = 200
                     THEN
                        50
                     WHEN oli.do_dtl_status = 120
                     THEN
                        60
                  END
                     AS stat_code,
                  oli.item_name,
                  COALESCE (SUM (oli.order_qty), 0) AS order_qty,
                  COALESCE (SUM (oli.shipped_qty), 0) AS shipped_qty,
                  COALESCE (SUM (oli.user_canceled_qty), 0) AS canceled_qty,
                  TO_CHAR (SYSDATE, 'YYYYMMDD')
             FROM    orders o
                  JOIN
                     order_line_item oli
                  ON o.order_id = oli.order_id
            WHERE     o.is_original_order = 1
                  AND o.tc_company_id = p_company_id
                  AND o.o_facility_alias_id = p_whse
         GROUP BY o.tc_company_id,
                  o.o_facility_alias_id,
                  o.tc_order_id,
                  oli.tc_order_line_id,
                  oli.do_dtl_status,
                  oli.item_name;
   ELSIF (p_sync_type = 'INV')
   THEN
      OPEN p_txn_data FOR
           SELECT wi.tc_company_id,
                  fa.facility_alias_id,
                  ic.item_name,
                  wi.cntry_of_orgn,
                  wi.batch_nbr,
                  wi.product_status,
                  wi.inventory_type,
                  wi.item_attr_1,
                  wi.item_attr_2,
                  wi.item_attr_3,
                  wi.item_attr_4,
                  wi.item_attr_5,
                  COALESCE (
                     SUM (DECODE (wi.allocatable, 'N', 0, wi.on_hand_qty)),
                     0)
                     avail_qty,
                  COALESCE (
                     SUM (DECODE (wi.allocatable, 'N', wi.on_hand_qty, 0)),
                     0)
                     unalloc_qty,
                  TO_CHAR (SYSDATE, 'YYYYMMDD')
             FROM wm_inventory wi
                  JOIN item_cbo ic
                     ON ic.item_id = wi.item_id
                  JOIN facility_alias fa
                     ON fa.facility_id = wi.c_facility_id
            WHERE wi.tc_company_id = p_company_id
                  AND fa.facility_alias_id = p_whse
         GROUP BY wi.tc_company_id,
                  fa.facility_alias_id,
                  ic.item_name,
                  wi.cntry_of_orgn,
                  wi.batch_nbr,
                  wi.product_status,
                  wi.inventory_type,
                  wi.item_attr_1,
                  wi.item_attr_2,
                  wi.item_attr_3,
                  wi.item_attr_4,
                  wi.item_attr_5;
   ELSIF (p_sync_type = 'PO')
   THEN
      OPEN p_txn_data FOR
           SELECT po.tc_company_id,
                  po.d_facility_alias_id,
                  po.tc_purchase_orders_id,
                  poli.tc_po_line_id,
                  poli.sku,
                  NVL (SUM (poli.order_qty), 0),
                    NVL (SUM (poli.order_qty), 0)
                  - NVL (SUM (poli.allocated_qty), 0)
                  - NVL (SUM (poli.shipped_qty), 0),
                  TO_CHAR (SYSDATE, 'YYYYMMDD')
             FROM    purchase_orders_line_item poli
                  JOIN
                     purchase_orders po
                  ON po.purchase_orders_id = poli.purchase_orders_id
            WHERE     po.purchase_orders_status < 950
                  AND poli.purchase_orders_line_status != 940
                  AND po.tc_company_id = p_company_id
                  AND po.d_facility_alias_id = p_whse
         GROUP BY po.tc_company_id,
                  poli.sku,
                  po.tc_purchase_orders_id,
                  po.d_facility_alias_id,
                  poli.tc_po_line_id;
   ELSE
      raise_application_error (-20050, 'Unsupported sync type.');
   END IF;
END;
/

SHOW ERRORS;
create or replace procedure wm_val_auton_log_result
(
    p_user_id         in user_profile.user_id%type,
    p_parent_txn_id   in val_result_hist.txn_id%type,
    p_txn_id          in val_result_hist.txn_id%type,
    p_val_job_dtl_id  in val_job_dtl.val_job_dtl_id%type,
    p_msg             in val_result_hist.msg%type,
    p_last_run_passed in val_result_hist.last_run_passed%type,
    p_app_hook        in val_result_hist.app_hook%type,
    p_pgm_id          in val_result_hist.pgm_id%type
)
as
    pragma autonomous_transaction;
begin
    insert into val_result_hist
    (
        parent_txn_id, txn_id, user_id, create_date_time, val_job_dtl_id, last_run_passed, msg,
        app_hook, pgm_id
    )
    values
    (
        p_parent_txn_id, p_txn_id, p_user_id, sysdate, p_val_job_dtl_id, p_last_run_passed, p_msg,
        p_app_hook, p_pgm_id
    );
    
    commit;
end;
/
show errors;

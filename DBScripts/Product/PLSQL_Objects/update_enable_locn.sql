create or replace procedure update_enable_locn
(
    p_locn_class in locn_wiz_hdr.locn_class%type,
    p_mod_date_time in locn_hdr.mod_date_time%type,
    p_crt_upd_locn out number
)
as
begin
    p_crt_upd_locn := 0;

    update locn_hdr 
    set locn_class = case 
        when locn_class = '0' then 'R'
        when locn_class = '1' then 'A'
        when locn_class = '2' then 'C'
    end,
    mod_date_time = p_mod_date_time
    where locn_class in ('0', '1', '2')
        and locn_id in 
        (
            select locn_id
            from locn_wizard_locn_tmp tl
        );
        
    update WM_INVENTORY 
    set locn_class = case 
        when locn_class = '0'  then 'R'
        when locn_class = '1'  then 'A'
        when locn_class = '2'  then 'C'
    end,
    last_updated_dttm = p_mod_date_time
    where locn_class in ('0', '1', '2')
        and location_id in 
        (
            select locn_id
            from locn_wizard_locn_tmp tl
        );    

    p_crt_upd_locn := sql%rowcount;
end;
/

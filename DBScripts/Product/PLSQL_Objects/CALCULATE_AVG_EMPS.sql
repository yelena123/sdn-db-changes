CREATE OR REPLACE 
FUNCTION CALCULATE_AVG_EMPS (WHSE IN VARCHAR2, YEARVAL IN VARCHAR2)
   RETURN INT
IS
   --INITIALIZE THE VARIABLES--
   LOOPVAR       INT := 0;
   OFFSET        INT := 0;
   PAYPERIOD     INT := 0;
   PAYFREQ       INT := 0;
   EMPCOUNT      INT := 0;
   AVGEMPCOUNT   INT := 0;
BEGIN
   --FETCH THE PAY_FREQ FROM WHSE_PAY TABLE--
   SELECT w1.PAY_FREQ
     INTO PAYFREQ
     FROM WHSE_PAY w1, WHSE_PAY w2
    WHERE w1.whse = w2.whse
          AND w1.whse_pay_id > w2.whse_pay_id;

   --VALIDATE THE INPUT VALUES--
   IF     CALCULATE_AVG_EMPS.WHSE IS NOT NULL
      AND CALCULATE_AVG_EMPS.YEARVAL IS NOT NULL
      AND PAYFREQ IS NOT NULL
   THEN
      --SET THE PAY_PERIODS BASED ON PAY_FREQ VALUE--
      --PAY-FREQ=1(DAILY), 2(WEEKLY), 3(BI-WEEKLY), 4(MONTHLY)--
      IF (PAYFREQ = '1')
      THEN
         PAYPERIOD := 365;
         OFFSET := 1;
      ELSIF (PAYFREQ = '2')
      THEN
         PAYPERIOD := 52;
         OFFSET := 7;
      ELSIF (PAYFREQ = '3')
      THEN
         PAYPERIOD := 26;
         OFFSET := 14;
      ELSIF (PAYFREQ = '4')
      THEN
         PAYPERIOD := 12;
         OFFSET := 1;
      END IF;                                                         --END IF

      --CHECK FOR MONTHLY PAY_PERIOD CONFIGURATION--
      IF (PAYPERIOD = 12)
      THEN
         WHILE (LOOPVAR < PAYPERIOD)
         LOOP
            SELECT COUNT (*)
              INTO EMPCOUNT
              FROM E_EMP_DTL OE
             WHERE OE.EFF_DATE_TIME =
                      (SELECT MAX (IE.EFF_DATE_TIME)
                         FROM E_EMP_DTL IE
                        WHERE IE.EMP_ID = OE.EMP_ID
                              AND IE.EFF_DATE_TIME <=
                                     (FN_LM_LAST_DAY (
                                         TO_DATE (
                                               OFFSET * LOOPVAR
                                            || '01/'
                                            || CALCULATE_AVG_EMPS.YEARVAL
                                            || '23:59',
                                            'MM/DD/YYYY

HH24:MI'),
                                         WHSE)))
                   AND OE.EMP_STAT_ID != '2'
                   AND OE.WHSE = CALCULATE_AVG_EMPS.WHSE;

            --CALCULATE THE TOTAL NUMBER OF EMPLOYEES BASED ON CONFIGURATION--
            AVGEMPCOUNT := AVGEMPCOUNT + EMPCOUNT;
            LOOPVAR := LOOPVAR + 1;
         END LOOP;

         --CALCULATE THE AVERAGE VALUE--
         AVGEMPCOUNT := AVGEMPCOUNT / PAYPERIOD;
      ELSE                --CHECK FOR DAILY, WEEKLY, BI-WEEKLY CONFIGURATION--
         WHILE (LOOPVAR < PAYPERIOD)
         LOOP
            SELECT COUNT (*)
              INTO EMPCOUNT
              FROM E_EMP_DTL OE
             WHERE OE.EFF_DATE_TIME =
                      (SELECT MAX (IE.EFF_DATE_TIME)
                         FROM E_EMP_DTL IE
                        WHERE IE.EMP_ID = OE.EMP_ID
                              AND IE.EFF_DATE_TIME <=
                                     (TO_DATE (
                                            '01/01/'
                                         || CALCULATE_AVG_EMPS.YEARVAL
                                         || '23:59',
                                         'MM/DD/YYYY HH24:MI'))
                                     + (OFFSET * LOOPVAR))
                   AND OE.EMP_STAT_ID != '2'
                   AND OE.WHSE = CALCULATE_AVG_EMPS.WHSE;

            --CALCULATE THE TOTAL NUMBER OF EMPLOYEES BASED ON CONFIGURATION--
            AVGEMPCOUNT := AVGEMPCOUNT + EMPCOUNT;
            LOOPVAR := LOOPVAR + 1;
         END LOOP;

         --CALCULATE THE AVERAGE VALUE--
         AVGEMPCOUNT := AVGEMPCOUNT / PAYPERIOD;
      END IF;
   END IF;

   --RETURN THE AVERAGE NUMBER OF EMPLOYEES COUNT
   RETURN CEIL (AVGEMPCOUNT);
EXCEPTION
   WHEN OTHERS
   THEN
      RETURN SQLCODE;
END;
/
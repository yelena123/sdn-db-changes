create or replace
PROCEDURE PRC_GLOBAL_INVENTORY_REFRESH
AS
   n_run_id                        NUMBER;
   v_error_details                 VARCHAR2 (2000);
   ts_source_last_updated_dttm     TIMESTAMP (6);
   ts_n_source_last_updated_dttm   TIMESTAMP (6);
   ts_last_run_dttm                TIMESTAMP (6);
   ts_new_run_dttm                 TIMESTAMP (6);
   v_sql                           VARCHAR2 (4000);
   n_count                         NUMBER := 0;
   v_query                         varchar2(5000);
 BEGIN
   ts_new_run_dttm := SYSTIMESTAMP;


   v_sql :=
      'SELECT SOURCE_LAST_UPDATED_DTTM FROM INVENTORY_REFRESH_LOG WHERE LAST_RUN_ID = ( SELECT MAX(last_run_id) FROM INVENTORY_REFRESH_LOG WHERE  ERROR_DETAILS IS NULL ) ';

   SELECT INVENTORY_REFRESH_LOG_SEQ.NEXTVAL INTO n_run_id FROM DUAL;

   BEGIN
      EXECUTE IMMEDIATE v_sql INTO ts_source_last_updated_dttm;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ts_source_last_updated_dttm :=
            TO_TIMESTAMP ('01/01/1900 12:00:00.000000 AM',
                          'fmMMfm/fmDDfm/YYYY fmHH12fm:MI:SS.FF AM');
   END;

   v_sql :=
      'SELECT  COUNT(*) , MAX(LAST_UPDATED_DTTM) FROM I_PERPETUAL WHERE LAST_UPDATED_DTTM > :last_run_dttm  ';

   EXECUTE IMMEDIATE v_sql
      INTO n_count, ts_n_source_last_updated_dttm
      USING ts_source_last_updated_dttm;

   ts_n_source_last_updated_dttm :=
      NVL (ts_n_source_last_updated_dttm, ts_source_last_updated_dttm);

   EXECUTE IMMEDIATE 'TRUNCATE TABLE GLOBAL_ITEM_INVENTORY ';

   INSERT INTO GLOBAL_ITEM_INVENTORY_GTT
      SELECT TC_COMPANY_ID,
             FACILITY_ID,
             ITEM_ID,
             QUANTITY_UOM_ID,
             BUSINESS_PARTNER,
             ORDERED_TOTAL,
             INBOUND_TOTAL,
             ON_HAND_TOTAL
        FROM (  SELECT i.TC_COMPANY_ID AS TC_COMPANY_ID,
                       i.FACILITY_ID AS FACILITY_ID,
                       i.ITEM_ID AS ITEM_ID,
 					   CASE
					    WHEN i.QUANTITY_UOM_ID IS NULL
						   THEN
							  (SELECT
                CASE
                  WHEN ib.BASE_STORAGE_UOM_ID IS NULL
                   THEN
                      (SELECT TO_NUMBER(PARAM_VALUE) FROM COMPANY_PARAMETER WHERE PARAM_DEF_ID='default_item_base_uom' and  tc_company_id=i.TC_COMPANY_ID)
                    ELSE
                      ib.BASE_STORAGE_UOM_ID
                    END  FROM ITEM_CBO ib WHERE ib.ITEM_ID=i.ITEM_ID and ib. COMPANY_ID=i.TC_COMPANY_ID)
						  ELSE
					     i.QUANTITY_UOM_ID
					   END  AS QUANTITY_UOM_ID
					   ,
                       i.BUSINESS_PARTNER AS BUSINESS_PARTNER,
                       SUM (
                          CASE
                             WHEN i.OBJECT_TYPE_ID =
                                     (SELECT SYS_CODE_ID
                                        FROM SYS_CODE
                                       WHERE     REC_TYPE = 'S'
                                             AND CODE_TYPE = 'I10'
                                             AND CODE_ID = 'PO')
                             THEN
                                (SELECT NVL (SUM (i_inner.AVAILABLE_QUANTITY),
                                             0)
                                        + NVL (
                                             SUM (
                                                i_inner.AVAILABLE_SOON_QUANTITY),
                                             0)
										+ NVL (
                                             SUM (
                                                i_inner.UNAVAILABLE_QUANTITY),
                                             0)
                                   FROM I_PERPETUAL i_inner
                                  WHERE i_inner.IS_DELETED = 0
                                        AND i.inventory_id =
                                               i_inner.inventory_id)
                             ELSE
                                0
                          END)
                          AS ORDERED_TOTAL,
                       SUM (
                          CASE
                             WHEN i.OBJECT_TYPE_ID =
                                     (SELECT SYS_CODE_ID
                                        FROM SYS_CODE
                                       WHERE     REC_TYPE = 'S'
                                             AND CODE_TYPE = 'I10'
                                             AND CODE_ID = 'ASN')
                             THEN
                                (SELECT NVL (SUM (i_inner.AVAILABLE_QUANTITY),
                                             0)
                                        + NVL (
                                             SUM (
                                                i_inner.AVAILABLE_SOON_QUANTITY),
                                             0)
										+ NVL (
                                             SUM (
                                                i_inner.UNAVAILABLE_QUANTITY),
                                             0)
                                   FROM I_PERPETUAL i_inner
                                  WHERE i_inner.IS_DELETED = 0
                                        AND i.inventory_id =
                                               i_inner.inventory_id)
                             ELSE
                                0
                          END)
                          AS INBOUND_TOTAL,
                       SUM (
                          CASE
                             WHEN i.OBJECT_TYPE_ID =
                                     (SELECT SYS_CODE_ID
                                        FROM SYS_CODE
                                       WHERE     REC_TYPE = 'S'
                                             AND CODE_TYPE = 'I10'
                                             AND CODE_ID = 'OH')
                             THEN
                                (SELECT NVL (SUM (i_inner.AVAILABLE_QUANTITY),
                                             0)
                                        + NVL (
                                             SUM (
                                                i_inner.AVAILABLE_SOON_QUANTITY),
                                             0)
										+ NVL (
                                             SUM (
                                                i_inner.UNAVAILABLE_QUANTITY),
                                             0)
                                   FROM I_PERPETUAL i_inner
                                  WHERE i_inner.IS_DELETED = 0
                                        AND i.inventory_id =
                                               i_inner.inventory_id)
                             ELSE
                                0
                          END)
                          AS ON_HAND_TOTAL
                  FROM I_PERPETUAL i
                 WHERE i.IS_DELETED = 0
              GROUP BY i.FACILITY_ID,
                       i.ITEM_ID,
                       i.BUSINESS_PARTNER,
                       i.TC_COMPANY_ID,
                       i.QUANTITY_UOM_ID);

   COMMIT;

   INSERT INTO GLOBAL_ITEM_INVENTORY (
	   SELECT TC_COMPANY_ID,
             FACILITY_ID,
             ITEM_ID,
             QUANTITY_UOM_ID,
             BUSINESS_PARTNER,
             SUM(ORDERED_TOTAL),
             SUM(INBOUND_TOTAL),
             SUM(ON_HAND_TOTAL)
        FROM
		  GLOBAL_ITEM_INVENTORY_GTT GROUP BY
				TC_COMPANY_ID,
             FACILITY_ID,
             ITEM_ID,
             QUANTITY_UOM_ID,
             BUSINESS_PARTNER);

	COMMIT;

	--EXECUTE IMMEDIATE 'TRUNCATE TABLE GLOBAL_ITEM_INVENTORY_GTT ';

   INSERT INTO INVENTORY_REFRESH_LOG (LAST_RUN_ID,
                                      SOURCE_LAST_UPDATED_DTTM,
                                      LAST_RUN_DTTM,
                                      ERROR_DETAILS)
        VALUES (n_run_id,
                ts_n_source_last_updated_dttm,
                ts_new_run_dttm,
                NULL);

   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      v_error_details :=
         SUBSTR (TO_CHAR (SQLCODE) || '<--->' || SQLERRM, 1, 2000);

      INSERT INTO INVENTORY_REFRESH_LOG (LAST_RUN_ID,
                                         SOURCE_LAST_UPDATED_DTTM,
                                         LAST_RUN_DTTM,
                                         ERROR_DETAILS)
           VALUES (INVENTORY_REFRESH_LOG_SEQ.NEXTVAL,
                   ts_n_source_last_updated_dttm,
                   ts_new_run_dttm,
                   v_error_details);

      DBMS_OUTPUT.PUT_LINE ('ERROR ---> ' || v_error_details);
      COMMIT;
END;
/

create or replace procedure manh_wave_lpn_sequencing
(
    p_facility_id       in  facility.facility_id%type,
    p_ship_wave_parm_id in  ship_wave_parm.ship_wave_parm_id%type,
    p_user_id           in  ucl_user.user_name%type,
    p_lock_time_out     in  number
)
as
    v_lpn_count         number(6) := 0;
    v_pick_wave_nbr     ship_wave_parm.pick_wave_nbr%type;
    v_select_rule       varchar2(4000);
    v_sort_rule         varchar2(4000);
    v_rule_sql          varchar2(32600);
    v_rule_type         varchar2(2) := 'CQ';
    v_code_id           sys_code.code_id%type := '009';
    v_max_log_lvl       number(1);
    v_tc_company_id     facility.tc_company_id%type;
    v_whse              facility.whse%type;
	  v_prev_module_name  varchar2(64);
    v_prev_action_name  varchar2(64);
begin
    select swp.pick_wave_nbr, swp.whse, coalesce(wp.tc_company_id, wm_utils.wm_get_user_bu(p_user_id))
    into v_pick_wave_nbr, v_whse, v_tc_company_id
    from ship_wave_parm swp
    join wave_parm wp on wp.wave_parm_id = swp.wave_parm_id
    where swp.ship_wave_parm_id = p_ship_wave_parm_id;

    select to_number(coalesce(trim(substr(sc.misc_flags, 8, 1)), '0'))
    into v_max_log_lvl
    from sys_code sc 
    where sc.rec_type = 'S' and sc.code_type = '520' and sc.code_id = v_code_id;

    delete from tmp_log_parm;
    insert into tmp_log_parm
    (
        log_lvl, pgm_id, module, msg_id, ref_code_1, ref_value_1, user_id, whse,
        cd_master_id
    )
    values
    (
        v_max_log_lvl, 'manh_wave_lpn_sequencing', 'WAVE', '1094', v_code_id,
        to_char(p_ship_wave_parm_id), p_user_id, v_whse, v_tc_company_id
    );
    
    wm_utils.get_context_info(v_prev_module_name, v_prev_action_name);
    wm_utils.set_context_info(p_module_name => 'Wave', p_action_name => 'Lpn Sequencing',
        p_client_id => v_pick_wave_nbr);
		
    for rule_rec in
    (
        select wrp.rule_id, wrp.rule_prty
        from wave_rule_parm wrp
        where wrp.wave_parm_id = p_ship_wave_parm_id and wrp.stat_code = 0
            and exists
            (
                select 1 
                from rule_hdr rh
                join rule_sel_dtl rsd on rsd.rule_id = rh.rule_id
                where rh.rule_id = wrp.rule_id and rh.rule_type = v_rule_type
           )
        order by wrp.rule_prty
    )
    loop
        -- per CV, expected behavior when no selection criteria is defined
        -- is to process the remaining lpns in this wave
        select manh_get_rule(rule_rec.rule_id, 0, v_rule_type)
        into v_select_rule
        from dual;

        select manh_get_rule(rule_rec.rule_id, 1, v_rule_type)
        into v_sort_rule
        from dual;

        -- collect lpns for this rule, generate seq nbrs; the selection criteria
        -- could include dtl lvl tables, so we need the grouping and resequencing
        v_rule_sql :=
               ' merge into lpn l'
            || ' using'
            || ' ('
            || '    select iv2.lpn_id lpn_id,'
            || '        :lpn_count + row_number() over(order by iv2.seq_with_holes) seq_with_no_holes'
            || '    from'
            || '    ('
            || '        select iv1.lpn_id lpn_id, min(iv1.seq) seq_with_holes'
            || '        from'
            || '        ('
            || '            select lpn.lpn_id lpn_id,'
            || '                row_number() over(order by sort_criteria) seq'
            || '            from lpn'
            || '            where lpn.c_facility_id = :facility_id'
            || '                and lpn.wave_nbr = :pick_wave_nbr '
            || '                and lpn.lpn_facility_status = 0 ' 
            || '                and lpn.inbound_outbound_indicator = ''O'''            
            || case when v_select_rule is null then ' ' else ' and ' || v_select_rule end
            || '        ) iv1'
            || '        group by iv1.lpn_id'
            || '    ) iv2'
            || ' ) iv3 on (iv3.lpn_id = l.lpn_id)'
            || ' when matched then update'
            || ' set l.selection_rule_id = :rule_id, l.seq_rule_priority = :prty,'
            || '    l.wave_seq_nbr = iv3.seq_with_no_holes, l.lpn_facility_status = 10,'
            || '    l.printing_rule_id = :rule_id, l.last_updated_dttm = sysdate,'
            || '    l.last_updated_source = :user_id';

        -- add order by clause
        if (v_sort_rule is null)
        then
            v_rule_sql := replace(v_rule_sql, 'sort_criteria', 'lpn.lpn_id');
        else
            v_rule_sql := replace(v_rule_sql, 'sort_criteria',
                substr(v_sort_rule, 3, length(v_sort_rule)) || ', lpn.lpn_id');
        end if;

        manh_wave_add_lpn_seq_joins(v_rule_sql);

        execute immediate v_rule_sql using v_lpn_count, p_facility_id,
            v_pick_wave_nbr, rule_rec.rule_id, rule_rec.rule_prty,
            rule_rec.rule_id, p_user_id;
        v_lpn_count := v_lpn_count + sql%rowcount;
        wm_cs_log('Sequenced ' || v_lpn_count || ' olpns via rule ' || rule_rec.rule_id);

        commit;
    end loop;
	
    wm_utils.set_context_info(p_module_name => v_prev_module_name,
        p_action_name => v_prev_action_name);
end;
/

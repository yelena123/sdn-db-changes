create or replace
PACKAGE Order_State_Pkg
IS

   function isLastLegOfOrder
( v_shipment_id IN stop_action_order.shipment_id%type,
v_order_id IN stop_action_order.order_id%type,
v_order_split_id in stop_action_order.order_split_id%type)
  return boolean;

  function getMaxStopSeqFromTrkMsg ( v_shipment_id IN tracking_message.shipment_id%type )
	return  number;


 function getOrderStatusForShipment (v_shipment_id IN SHIPMENT.SHIPMENT_ID%TYPE,
                                           v_order_id IN number,
                                           v_order_split_id IN number
                                           )
	return  number;


 function getOrderStatus (v_shipment_id IN number,
                         v_order_id IN number,
                         v_order_split_id IN number)
	return  number;

  function updtSpltStGetOrdSt (
        v_shipment_id order_split.shipment_id%type,
        v_order_id order_split.order_id%type,
        v_order_split_id order_split.order_split_id%type,
        v_isNetwork number,
  	  ALASTUPDATEDSOURCE   order_split.LAST_UPDATED_SOURCE%TYPE,
        ALASTUPDATEDTYPE     order_split.LAST_UPDATED_SOURCE_TYPE%TYPE
     )
   return number;


  PROCEDURE PRSETORDERSTATUS (
      ATCCOMPANYID         SHIPMENT.TC_COMPANY_ID%TYPE,
      ASHIPMENTID          SHIPMENT.SHIPMENT_ID%TYPE,
      ALASTUPDATEDSOURCE   SHIPMENT.LAST_UPDATED_SOURCE%TYPE,
      ALASTUPDATEDTYPE     SHIPMENT.LAST_UPDATED_SOURCE_TYPE%TYPE
   );
   
   PROCEDURE PRSETORDERSTATUS (
      ATCCOMPANYID         SHIPMENT.TC_COMPANY_ID%TYPE,
      ASHIPMENTID          SHIPMENT.SHIPMENT_ID%TYPE,
      ALASTUPDATEDSOURCE   SHIPMENT.LAST_UPDATED_SOURCE%TYPE,
      ALASTUPDATEDTYPE     SHIPMENT.LAST_UPDATED_SOURCE_TYPE%TYPE,
        SHIPMENT_STATUS        NUMBER
   );

   function updtSpltStGetOrdSt (
      v_shipment_id order_split.shipment_id%type,
      v_order_id order_split.order_id%type,
      v_order_split_id order_split.order_split_id%type,
      v_isNetwork number,
      ALASTUPDATEDSOURCE   order_split.LAST_UPDATED_SOURCE%TYPE,
      ALASTUPDATEDTYPE     order_split.LAST_UPDATED_SOURCE_TYPE%TYPE,
      v_shipment_status    number
   )
   return number;
   
   function getOrderStatus (v_shipment_id IN number,
                         v_order_id IN number,
                         v_order_split_id IN number,
                         v_shipment_status  IN  number)
    return  number;
  
  
 function getOrderStatusForShipment (v_shipment_id IN SHIPMENT.SHIPMENT_ID%TYPE,
                                           v_order_id IN number,
                                           v_order_split_id IN number,
                                           v_shipment_status IN number
                                           )
    return  number;

END;
/

create or replace
PACKAGE BODY ORDER_STATE_PKG
AS


function isLastLegOfOrder
( v_shipment_id IN stop_action_order.shipment_id%type,
v_order_id IN stop_action_order.order_id%type,
v_order_split_id in stop_action_order.order_split_id%type)
  return boolean
  as
  v_isLastLeg boolean;
  v_order_delivery_stop stop_action_order.stop_seq%type;
  v_shp_fac_id stop.facility_id%type;
  v_dest_fac_id orders.d_facility_id%type;
  v_shp_addr varchar2(500);
  v_dest_fac_addr varchar2(500);
  v_plan_dest_fac_id orders.PLAN_D_FACILITY_ID%type;

BEGIN
begin
if (v_order_split_id > 0) then
  begin
  SELECT MAX(stop_seq)
  INTO v_order_delivery_stop
  FROM stop_action_order SAO
  WHERE SAO.order_id  = v_order_id
  AND SAO.shipment_id = v_shipment_id
  AND SAO.order_split_id = v_order_split_id;
  EXCEPTION
  WHEN NO_DATA_FOUND
  THEN
     DBMS_OUTPUT.PUT_LINE ('No data found - isLastLegOfOrder');
  when others
  then
    DBMS_OUTPUT.PUT_LINE ('No data found - isLastLegOfOrder'|| SQLERRM);
  end;
else
  begin
  SELECT MAX(stop_seq)
  INTO v_order_delivery_stop
  FROM stop_action_order SAO
  WHERE SAO.order_id  = v_order_id
  AND SAO.shipment_id = v_shipment_id;
  EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
       DBMS_OUTPUT.PUT_LINE ('No data found - isLastLegOfOrder');
    when others
    then
      DBMS_OUTPUT.PUT_LINE ('No data found - isLastLegOfOrder'|| SQLERRM);
  end;
end if;
begin
SELECT ST.facility_id,
  (lower(trim(COALESCE(Address_1,''))))
  ||'|'||(lower(trim(COALESCE(Address_2,''))))
  ||'|'||(lower(trim(COALESCE(Address_3,''))))
  ||'|'||(lower(trim(COALESCE(City,''))))
  ||'|'||(lower(trim(COALESCE(State_prov,''))))
  ||'|'||(lower(trim(COALESCE(county,''))))
  ||'|'||(lower(trim(COALESCE(country_code,''))))
  ||'|'||(lower(trim(COALESCE(postal_code,''))))
  INTO v_shp_fac_id, v_shp_addr
FROM STOP ST
WHERE ST.shipment_id = v_shipment_id
AND ST.stop_seq      = v_order_delivery_stop;

EXCEPTION
WHEN NO_DATA_FOUND
THEN
   DBMS_OUTPUT.PUT_LINE ('No data found - isLastLegOfOrder');
when others
then
  DBMS_OUTPUT.PUT_LINE ('No data found - isLastLegOfOrder'|| SQLERRM);

end;
begin
SELECT PLAN_D_FACILITY_ID,
  d_facility_id,
  (lower(trim(COALESCE(d_Address_1,''))))
  ||'|'||(lower(trim(COALESCE(d_Address_2,''))))
  ||'|'||(lower(trim(COALESCE(d_Address_3,''))))
  ||'|'||(lower(trim(COALESCE(d_City,''))))
  ||'|'||(lower(trim(COALESCE(d_State_prov,''))))
  ||'|'||(lower(trim(COALESCE(d_county,''))))
  ||'|'||(lower(trim(COALESCE(d_country_code,''))))
  ||'|'||(lower(trim(COALESCE(d_postal_code,''))))
INTO v_plan_dest_fac_id, v_dest_fac_id, v_dest_fac_addr
FROM orders
WHERE orders.order_id = v_order_id;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
   DBMS_OUTPUT.PUT_LINE ('No data found - isLastLegOfOrder');
when others
then
  DBMS_OUTPUT.PUT_LINE ('No data found - isLastLegOfOrder'|| SQLERRM);
end;
-- compare the shipment stop's fac id with order's plan destination, if it
-- does not exist, compare with order's destination fac id. If that also
-- is not present then this is not last leg of shipment.
-- in case shipment stop does not have fac id, get the address and compare with
-- plan destination addr iff plan destination fac id is not present
-- Similarly, check if destination addr. is same as stop's addr. iff destination
-- fac is not present on the order

if v_shp_fac_id is not null then
		if v_plan_dest_fac_id is not null then
			if v_shp_fac_id = v_plan_dest_fac_id then
				return true;
      end if;
		else
			if v_dest_fac_id is not null then
				if v_shp_fac_id = v_dest_fac_id then
					return true;
        end if;
      end if;
    end if;
else
		if v_plan_dest_fac_id is null then
			if v_dest_fac_id is null then
				if v_shp_addr = v_dest_fac_addr then
					return true;
        end if;
      end if;
    end if;
end if;
EXCEPTION
      WHEN NO_DATA_FOUND
      THEN DBMS_OUTPUT.PUT_LINE ('No data found - isLastLegOfOrder');
end;
return false;
end;


function getMaxStopSeqFromTrkMsg ( v_shipment_id IN tracking_message.shipment_id%type )
	return  number
  IS
  v_stop_seq tracking_message.stop_seq%type;
  v_no_of_accpt_msg NUMBER := 0;
  v_last_acc_trk_msg_id tracking_message.tracking_msg_id%type;
	Begin
    Begin
      select count(tracking_msg_id) into v_no_of_accpt_msg from tracking_message tm where tm.message_type = 10 and tm.import_errors_exist = 0 and tm.shipment_id = v_shipment_id;
      EXCEPTION
      WHEN NO_DATA_FOUND
      THEN DBMS_OUTPUT.PUT_LINE ('No data found - no accepted tracking messasge');
    end;
  -- if there are more than 1 'accept' tracking message, then get the max. stop sequence of
  -- the shipment for which arrival/departure tracking message is received after the latest accept message
  -- otherwise go by the max. stop sequence for the shipment, directly
	if v_no_of_accpt_msg > 1 then
    begin
		select max(tm.tracking_msg_id) into v_last_acc_trk_msg_id from tracking_message tm where tm.message_type = 10 and tm.import_errors_exist = 0 and tm.shipment_id = v_shipment_id and tm.import_errors_exist = 0;
		select max(tm.stop_seq) into v_stop_seq from tracking_message tm where (tm.message_type = 45 or tm.message_type = 50) and tm.shipment_id = v_shipment_id and tm.tracking_msg_id > v_last_acc_trk_msg_id and tm.import_errors_exist = 0;
    EXCEPTION
      WHEN NO_DATA_FOUND
      THEN v_stop_seq := -1;
    end;
	else
  begin
		select max(tm.stop_seq) into v_stop_seq from tracking_message tm where (tm.message_type = 45 or tm.message_type = 50) and tm.shipment_id = v_shipment_id and import_errors_exist = 0;
    EXCEPTION
      WHEN NO_DATA_FOUND
      THEN v_stop_seq := -1;
  end;
	end if;
	return v_stop_seq;
	End;


function getOrderStatusForShipment (v_shipment_id IN SHIPMENT.SHIPMENT_ID%TYPE,
                                           v_order_id IN number,
                                           v_order_split_id IN number
                                           )
	return  number
  IS
  v_order_status number := 10;
  v_shipment_status SHIPMENT.SHIPMENT_STATUS%TYPE := 0;
  v_booking_id number := -1;
  v_min_stop_sequence number := -1;
  v_max_stop_sequence number := -1;
  v_max_stop_seq_from_trk_msg number := -1;
Begin

begin

begin
SELECT shipment.shipment_status
INTO v_shipment_status
FROM shipment
WHERE shipment.shipment_id = v_shipment_id;
EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment');
      when others
      then
        DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment'|| SQLERRM);
end;
IF (v_shipment_status       >= 10 AND v_shipment_status <= 60) THEN
  v_order_status            := 10;
ELSE IF (v_shipment_status     >= 80 AND v_shipment_status <= 95 ) THEN
    IF (isLastLegOfOrder(v_shipment_id, v_order_id, v_order_split_id) = true) THEN
      v_order_status        := 80;
    ELSE
      v_order_status := 70;
    END IF;
  ELSE IF (v_shipment_status = 70) THEN
    begin
      SELECT shipment.booking_id
      INTO v_booking_id
      FROM shipment
      WHERE shipment.shipment_id = shipment_id;
      EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment');
      when others
      then
        DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment'|| SQLERRM);
    end;
      IF (v_booking_id            >= 0) THEN
        v_order_status            := 70;
      ELSE
        IF (v_order_split_id > 0) THEN
        begin
          SELECT MIN(stop_seq)
          INTO v_min_stop_sequence
          FROM stop_action_order SAO
          WHERE SAO.shipment_id  = v_shipment_id
          AND SAO.order_id       = v_order_id
          AND SAO.order_split_id = v_order_split_id;
          EXCEPTION
          WHEN NO_DATA_FOUND
          THEN
             DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment');
          when others
          then
            DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment'|| SQLERRM);
        end;
        ELSE
          begin
          SELECT MIN(stop_seq)
          INTO v_min_stop_sequence
          FROM stop_action_order SAO
          WHERE SAO.shipment_id = v_shipment_id
          AND SAO.order_id      = v_order_id;
          EXCEPTION
          WHEN NO_DATA_FOUND
          THEN
             DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment');
          when others
          then
            DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment'|| SQLERRM);
          end;
        END IF;
        v_max_stop_seq_from_trk_msg    := getMaxStopSeqFromTrkMsg(v_shipment_id);
        IF v_max_stop_seq_from_trk_msg >= v_min_stop_sequence THEN
          v_order_status               := 70;
        END IF;
        IF (isLastLegOfOrder(v_shipment_id, v_order_id, v_order_split_id) = true) THEN
          IF (v_order_split_id  > 0) THEN
            begin
              SELECT MAX(stop_seq)
              INTO v_max_stop_sequence
              FROM stop_action_order SAO
              WHERE SAO.shipment_id  = v_shipment_id
              AND SAO.order_id       = v_order_id
              AND SAO.order_split_id = v_order_split_id;
            EXCEPTION
              WHEN NO_DATA_FOUND
              THEN
                 DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment');
              when others
              then
                DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment'|| SQLERRM);
            end;
          ELSE
            begin
              SELECT MAX(stop_seq)
              INTO v_max_stop_sequence
              FROM stop_action_order SAO
              WHERE SAO.shipment_id = v_shipment_id
              AND SAO.order_id      = v_order_id;
              EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                   DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment');
                when others
                then
                  DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment'|| SQLERRM);
            end;
          END IF;
          IF (v_max_stop_seq_from_trk_msg >= v_max_stop_sequence) THEN
            v_order_status                := 80;
          END IF;
        END IF;
      END IF;
    END IF;
  END IF;
END IF;
EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment');
end;
    RETURN v_order_status;
  END;



function getOrderStatus (v_shipment_id IN number,
                         v_order_id IN number,
                         v_order_split_id IN number)
	return  number
  IS
  v_final_stat_of_order number := 10;
  v_ordStatForCurShipment number := 10;

Begin

    Begin
    -- Arrives at the order status based on the shipments this order is travelling on (called only for network shipments)
    FOR V_ORDER_SHIPMENT IN
            (SELECT   ORDRMVMT.SHIPMENT_ID,
                  ORDRMVMT.ORDER_SHIPMENT_SEQ ORDER_SHIPMENT_SEQ
             FROM ORDER_MOVEMENT ORDRMVMT, SHIPMENT SHPMNT
            WHERE ORDRMVMT.ORDER_ID = V_ORDER_ID
              AND (   ORDRMVMT.ORDER_SPLIT_ID = V_ORDER_SPLIT_ID
                   OR ORDRMVMT.ORDER_SPLIT_ID IS NULL
                  )
              AND ORDRMVMT.SHIPMENT_ID = SHPMNT.SHIPMENT_ID
         order by ORDRMVMT.ORDER_SHIPMENT_SEQ, ORDRMVMT.SHIPMENT_ID
            )
         LOOP
        v_ordStatForCurShipment := getOrderStatusForShipment(V_ORDER_SHIPMENT.shipment_id,v_order_id,v_order_split_id);
        if(v_ordStatForCurShipment > v_final_stat_of_order) then
            v_final_stat_of_order := v_ordStatForCurShipment;
        end if;
        if (v_final_stat_of_order = 80) then
            exit;
        end if;
    end loop;
    EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatus');
    end;
return v_final_stat_of_order;
End;



function updtSpltStGetOrdSt (
      v_shipment_id order_split.shipment_id%type,
      v_order_id order_split.order_id%type,
      v_order_split_id order_split.order_split_id%type,
      v_isNetwork number,
	  ALASTUPDATEDSOURCE   order_split.LAST_UPDATED_SOURCE%TYPE,
      ALASTUPDATEDTYPE     order_split.LAST_UPDATED_SOURCE_TYPE%TYPE
   )
   return number
  IS
  v_min_stat_of_split_orders number := 10;
  v_NewStatOfSpltOrder number := 10;
  v_split_order_status number := 10;

  Begin
    if v_isNetwork = 1 then
      v_NewStatOfSpltOrder := getOrderStatus(v_shipment_id,v_order_id,v_order_split_id);
    else
      v_NewStatOfSpltOrder := getOrderStatusForShipment(v_shipment_id,v_order_id,v_order_split_id);
    end if;
    update order_split set order_split_status = v_NewStatOfSpltOrder,
                      LAST_UPDATED_DTTM = Getdate (),
                      LAST_UPDATED_SOURCE = ALASTUPDATEDSOURCE,
                      LAST_UPDATED_SOURCE_TYPE = ALASTUPDATEDTYPE
			where order_split.order_id = v_order_id and order_split.order_split_id = v_order_split_id;
    v_min_stat_of_split_orders := v_NewStatOfSpltOrder;
    begin
    FOR V_SPLIT_ORDER IN (SELECT order_split.order_split_status
           FROM order_split
          WHERE order_split.order_id = v_order_id
          order by ORDER_SPLIT.ORDER_ID)
    LOOP
      if (v_min_stat_of_split_orders > V_SPLIT_ORDER.order_split_status) then
        v_min_stat_of_split_orders := V_SPLIT_ORDER.order_split_status;
      end if;
    end loop;
    EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         DBMS_OUTPUT.PUT_LINE ('No data found - updtSpltStGetOrdSt');
    end;
	return v_min_stat_of_split_orders;
  End;

   PROCEDURE PRSETORDERSTATUS (
      ATCCOMPANYID         SHIPMENT.TC_COMPANY_ID%TYPE,
      ASHIPMENTID          SHIPMENT.SHIPMENT_ID%TYPE,
      ALASTUPDATEDSOURCE   SHIPMENT.LAST_UPDATED_SOURCE%TYPE,
      ALASTUPDATEDTYPE     SHIPMENT.LAST_UPDATED_SOURCE_TYPE%TYPE
   )

   as
  statBasedOnCurShip number := 10;
  NewStatOfOrder number := 10;

Begin

  BEGIN
--Retrieve and update status of orders on this shipment (only network orders) from order_movement table
  FOR V_ORDER IN (SELECT ORDMVMT.ORDER_ID ORDER_ID,
                NVL (ORDMVMT.ORDER_SPLIT_ID, 0) ORDER_SPLIT_ID
           FROM ORDER_MOVEMENT ORDMVMT
          WHERE ORDMVMT.SHIPMENT_ID = ASHIPMENTID
          order by 1,2)
  LOOP
    NewStatOfOrder := 10;
	if V_ORDER.ORDER_SPLIT_ID > 0 then
     NewStatOfOrder := updtSpltStGetOrdSt(ASHIPMENTID,V_ORDER.order_id,V_ORDER.order_split_id,1,ALASTUPDATEDSOURCE,ALASTUPDATEDTYPE);
     null;
	else
		NewStatOfOrder := getOrderStatus(ASHIPMENTID,V_ORDER.order_id,0);
	end if;
  update orders set order_status = NewStatOfOrder,
                      LAST_UPDATED_DTTM = Getdate (),
                      LAST_UPDATED_SOURCE = ALASTUPDATEDSOURCE,
                      LAST_UPDATED_SOURCE_TYPE = ALASTUPDATEDTYPE
			where ( order_id = V_ORDER.order_id OR parent_order_id = V_ORDER.order_id );
end loop;
EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         DBMS_OUTPUT.PUT_LINE ('No data found - orders on shipment');
END;
BEGIN
--Retrieve and update status of orders travelling on this shipment only
  FOR V_ORDER IN (SELECT ORDERTABLE.ORDER_ID ORDER_ID
           FROM ORDERS ORDERTABLE
          WHERE ORDERTABLE.SHIPMENT_ID = ASHIPMENTID
          order by 1)
    LOOP
      statBasedOnCurShip := getOrderStatusForShipment(ASHIPMENTID,V_ORDER.order_id,0);

      update orders set order_status = statBasedOnCurShip,
                      LAST_UPDATED_DTTM = Getdate (),
                      LAST_UPDATED_SOURCE = ALASTUPDATEDSOURCE,
                      LAST_UPDATED_SOURCE_TYPE = ALASTUPDATEDTYPE
			where ( order_id = V_ORDER.order_id OR parent_order_id = V_ORDER.order_id );
  end loop;
  EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         DBMS_OUTPUT.PUT_LINE ('No data found - orders on just this shipment');
END;
--Retrieve and update status of split orders travelling on this shipment only
begin
  FOR V_ORDER IN (SELECT ORDERSPLIT.ORDER_ID ORDER_ID, ORDERSPLIT.ORDER_SPLIT_ID ORDER_SPLIT_ID
           FROM ORDER_SPLIT ORDERSPLIT
          WHERE ORDERSPLIT.SHIPMENT_ID = ASHIPMENTID
          order by 1,2)
    LOOP
      statBasedOnCurShip := updtSpltStGetOrdSt(ASHIPMENTID,V_ORDER.order_id,V_ORDER.order_split_id,0,ALASTUPDATEDSOURCE,ALASTUPDATEDTYPE);
      update orders set order_status = statBasedOnCurShip,
                      LAST_UPDATED_DTTM = Getdate (),
                      LAST_UPDATED_SOURCE = ALASTUPDATEDSOURCE,
                      LAST_UPDATED_SOURCE_TYPE = ALASTUPDATEDTYPE
	  where ( order_id = V_ORDER.order_id OR parent_order_id = V_ORDER.order_id );
  end loop;
  EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         DBMS_OUTPUT.PUT_LINE ('No data found - splits on just this shipment');
end;
End;

function updtSpltStGetOrdSt (
      v_shipment_id order_split.shipment_id%type,
      v_order_id order_split.order_id%type,
      v_order_split_id order_split.order_split_id%type,
      v_isNetwork number,
	  ALASTUPDATEDSOURCE   order_split.LAST_UPDATED_SOURCE%TYPE,
      ALASTUPDATEDTYPE     order_split.LAST_UPDATED_SOURCE_TYPE%TYPE,
	  v_shipment_status    number
   )
   return number
  IS
  v_min_stat_of_split_orders number := 10;
  v_NewStatOfSpltOrder number := 10;
  v_split_order_status number := 10;

  Begin
    if v_isNetwork = 1 then
      v_NewStatOfSpltOrder := getOrderStatus(v_shipment_id,v_order_id,v_order_split_id,v_shipment_status);
    else
      v_NewStatOfSpltOrder := getOrderStatusForShipment(v_shipment_id,v_order_id,v_order_split_id,v_shipment_status);
    end if;
    update order_split set order_split_status = v_NewStatOfSpltOrder,
                      LAST_UPDATED_DTTM = Getdate (),
                      LAST_UPDATED_SOURCE = ALASTUPDATEDSOURCE,
                      LAST_UPDATED_SOURCE_TYPE = ALASTUPDATEDTYPE
			where order_split.order_id = v_order_id and order_split.order_split_id = v_order_split_id;
    v_min_stat_of_split_orders := v_NewStatOfSpltOrder;
    begin
    FOR V_SPLIT_ORDER IN (SELECT order_split.order_split_status
           FROM order_split
          WHERE order_split.order_id = v_order_id
          order by ORDER_SPLIT.ORDER_ID)
    LOOP
      if (v_min_stat_of_split_orders > V_SPLIT_ORDER.order_split_status) then
        v_min_stat_of_split_orders := V_SPLIT_ORDER.order_split_status;
      end if;
    end loop;
    EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         DBMS_OUTPUT.PUT_LINE ('No data found - updtSpltStGetOrdSt');
    end;
	return v_min_stat_of_split_orders;
  End;
  
  function getOrderStatus (v_shipment_id IN number,
                         v_order_id IN number,
                         v_order_split_id IN number,
						 v_shipment_status  IN  number)
	return  number
  IS
  v_final_stat_of_order number := 10;
  v_ordStatForCurShipment number := 10;

Begin

    Begin
    -- Arrives at the order status based on the shipments this order is travelling on (called only for network shipments)
    FOR V_ORDER_SHIPMENT IN
            (SELECT   ORDRMVMT.SHIPMENT_ID,
                  ORDRMVMT.ORDER_SHIPMENT_SEQ ORDER_SHIPMENT_SEQ
             FROM ORDER_MOVEMENT ORDRMVMT, SHIPMENT SHPMNT
            WHERE ORDRMVMT.ORDER_ID = V_ORDER_ID
              AND (   ORDRMVMT.ORDER_SPLIT_ID = V_ORDER_SPLIT_ID
                   OR ORDRMVMT.ORDER_SPLIT_ID IS NULL
                  )
              AND ORDRMVMT.SHIPMENT_ID = SHPMNT.SHIPMENT_ID
         order by ORDRMVMT.ORDER_SHIPMENT_SEQ, ORDRMVMT.SHIPMENT_ID
            )
         LOOP
		 
		if(v_shipment_id <> V_ORDER_SHIPMENT.shipment_id) then
			v_ordStatForCurShipment := getOrderStatusForShipment(V_ORDER_SHIPMENT.shipment_id,v_order_id,v_order_split_id);
		else
			v_ordStatForCurShipment := getOrderStatusForShipment(V_ORDER_SHIPMENT.shipment_id,v_order_id,v_order_split_id,v_shipment_status);
		end if;	
        if(v_ordStatForCurShipment > v_final_stat_of_order) then
            v_final_stat_of_order := v_ordStatForCurShipment;		
        end if;
        if (v_final_stat_of_order = 80) then
            exit;
        end if;
    end loop;
    EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatus');
    end;
return v_final_stat_of_order;
End;


function getOrderStatusForShipment (v_shipment_id IN SHIPMENT.SHIPMENT_ID%TYPE,
                                           v_order_id IN number,
                                           v_order_split_id IN number,
										   v_shipment_status IN number
                                           )
	return  number
  IS
  v_order_status number := 10;  
  v_booking_id number := -1;
  v_min_stop_sequence number := -1;
  v_max_stop_sequence number := -1;
  v_max_stop_seq_from_trk_msg number := -1;
Begin

begin

IF (v_shipment_status       >= 10 AND v_shipment_status <= 60) THEN
  v_order_status            := 10;
ELSE IF (v_shipment_status     >= 80 AND v_shipment_status <= 95 ) THEN
    IF (isLastLegOfOrder(v_shipment_id, v_order_id, v_order_split_id) = true) THEN
      v_order_status        := 80;
    ELSE
      v_order_status := 70;
    END IF;
  ELSE IF (v_shipment_status = 70) THEN
    begin
      SELECT shipment.booking_id
      INTO v_booking_id
      FROM shipment
      WHERE shipment.shipment_id = shipment_id;
      EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment');
      when others
      then
        DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment'|| SQLERRM);
    end;
      IF (v_booking_id            >= 0) THEN
        v_order_status            := 70;
      ELSE
        IF (v_order_split_id > 0) THEN
        begin
          SELECT MIN(stop_seq)
          INTO v_min_stop_sequence
          FROM stop_action_order SAO
          WHERE SAO.shipment_id  = v_shipment_id
          AND SAO.order_id       = v_order_id
          AND SAO.order_split_id = v_order_split_id;
          EXCEPTION
          WHEN NO_DATA_FOUND
          THEN
             DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment');
          when others
          then
            DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment'|| SQLERRM);
        end;
        ELSE
          begin
          SELECT MIN(stop_seq)
          INTO v_min_stop_sequence
          FROM stop_action_order SAO
          WHERE SAO.shipment_id = v_shipment_id
          AND SAO.order_id      = v_order_id;
          EXCEPTION
          WHEN NO_DATA_FOUND
          THEN
             DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment');
          when others
          then
            DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment'|| SQLERRM);
          end;
        END IF;
        v_max_stop_seq_from_trk_msg    := getMaxStopSeqFromTrkMsg(v_shipment_id);
        IF v_max_stop_seq_from_trk_msg >= v_min_stop_sequence THEN
          v_order_status               := 70;
        END IF;
        IF (isLastLegOfOrder(v_shipment_id, v_order_id, v_order_split_id) = true) THEN
          IF (v_order_split_id  > 0) THEN
            begin
              SELECT MAX(stop_seq)
              INTO v_max_stop_sequence
              FROM stop_action_order SAO
              WHERE SAO.shipment_id  = v_shipment_id
              AND SAO.order_id       = v_order_id
              AND SAO.order_split_id = v_order_split_id;
            EXCEPTION
              WHEN NO_DATA_FOUND
              THEN
                 DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment');
              when others
              then
                DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment'|| SQLERRM);
            end;
          ELSE
            begin
              SELECT MAX(stop_seq)
              INTO v_max_stop_sequence
              FROM stop_action_order SAO
              WHERE SAO.shipment_id = v_shipment_id
              AND SAO.order_id      = v_order_id;
              EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                   DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment');
                when others
                then
                  DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment'|| SQLERRM);
            end;
          END IF;
          IF (v_max_stop_seq_from_trk_msg >= v_max_stop_sequence) THEN
            v_order_status                := 80;
          END IF;
        END IF;
      END IF;
    END IF;
  END IF;
END IF;
EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         DBMS_OUTPUT.PUT_LINE ('No data found - getOrderStatusForShipment');
end;
    RETURN v_order_status;
  END;

  PROCEDURE PRSETORDERSTATUS (
      ATCCOMPANYID         SHIPMENT.TC_COMPANY_ID%TYPE,
      ASHIPMENTID          SHIPMENT.SHIPMENT_ID%TYPE,
      ALASTUPDATEDSOURCE   SHIPMENT.LAST_UPDATED_SOURCE%TYPE,
      ALASTUPDATEDTYPE     SHIPMENT.LAST_UPDATED_SOURCE_TYPE%TYPE,
	  SHIPMENT_STATUS      NUMBER
   )

   as
  statBasedOnCurShip number := 10;
  NewStatOfOrder number := 10;

Begin

  BEGIN
--Retrieve and update status of orders on this shipment (only network orders) from order_movement table
  FOR V_ORDER IN (SELECT ORDMVMT.ORDER_ID ORDER_ID,
                NVL (ORDMVMT.ORDER_SPLIT_ID, 0) ORDER_SPLIT_ID
           FROM ORDER_MOVEMENT ORDMVMT
          WHERE ORDMVMT.SHIPMENT_ID = ASHIPMENTID
          order by 1,2)
  LOOP
    NewStatOfOrder := 10;
	if V_ORDER.ORDER_SPLIT_ID > 0 then
     NewStatOfOrder := updtSpltStGetOrdSt(ASHIPMENTID,V_ORDER.order_id,V_ORDER.order_split_id,1,ALASTUPDATEDSOURCE,ALASTUPDATEDTYPE,SHIPMENT_STATUS);
     null;
	else
		NewStatOfOrder := getOrderStatus(ASHIPMENTID,V_ORDER.order_id,0,SHIPMENT_STATUS);
	end if;
  update orders set order_status = NewStatOfOrder,
                      LAST_UPDATED_DTTM = Getdate (),
                      LAST_UPDATED_SOURCE = ALASTUPDATEDSOURCE,
                      LAST_UPDATED_SOURCE_TYPE = ALASTUPDATEDTYPE
			where order_id = V_ORDER.order_id;
end loop;
EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         DBMS_OUTPUT.PUT_LINE ('No data found - orders on shipment');
END;
BEGIN
--Retrieve and update status of orders travelling on this shipment only
  FOR V_ORDER IN (SELECT ORDERTABLE.ORDER_ID ORDER_ID
           FROM ORDERS ORDERTABLE
          WHERE ORDERTABLE.SHIPMENT_ID = ASHIPMENTID
          order by 1)
    LOOP
      statBasedOnCurShip := getOrderStatusForShipment(ASHIPMENTID,V_ORDER.order_id,0,SHIPMENT_STATUS);

      update orders set order_status = statBasedOnCurShip,
                      LAST_UPDATED_DTTM = Getdate (),
                      LAST_UPDATED_SOURCE = ALASTUPDATEDSOURCE,
                      LAST_UPDATED_SOURCE_TYPE = ALASTUPDATEDTYPE
			where order_id = V_ORDER.order_id;
  end loop;
  EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         DBMS_OUTPUT.PUT_LINE ('No data found - orders on just this shipment');
END;
--Retrieve and update status of split orders travelling on this shipment only
begin
  FOR V_ORDER IN (SELECT ORDERSPLIT.ORDER_ID ORDER_ID, ORDERSPLIT.ORDER_SPLIT_ID ORDER_SPLIT_ID
           FROM ORDER_SPLIT ORDERSPLIT
          WHERE ORDERSPLIT.SHIPMENT_ID = ASHIPMENTID
          order by 1,2)
    LOOP
      statBasedOnCurShip := updtSpltStGetOrdSt(ASHIPMENTID,V_ORDER.order_id,V_ORDER.order_split_id,0,ALASTUPDATEDSOURCE,ALASTUPDATEDTYPE,SHIPMENT_STATUS);
      update orders set order_status = statBasedOnCurShip,
                      LAST_UPDATED_DTTM = Getdate (),
                      LAST_UPDATED_SOURCE = ALASTUPDATEDSOURCE,
                      LAST_UPDATED_SOURCE_TYPE = ALASTUPDATEDTYPE
	  where order_id = V_ORDER.order_id;
  end loop;
  EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         DBMS_OUTPUT.PUT_LINE ('No data found - splits on just this shipment');
end;
End;
END;
/
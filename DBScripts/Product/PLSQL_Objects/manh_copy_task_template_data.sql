create or replace procedure manh_copy_task_template_data
(   
    p_user_id           in ucl_user.user_name%type,
    p_whse              in whse_master.whse%type,
    p_crit_nbr          in wave_task_crit.crit_nbr%type,
    p_need_id           in alloc_invn_dtl.need_id%type,
    p_facility_id       in facility.facility_id%type, 
    p_tc_company_id     in wave_parm.tc_company_id%type,
    o_need_id           out alloc_invn_dtl.need_id%type,
    p_need_type         out task_parm.need_type%type,
    p_task_parm_id      out task_parm.task_parm_id%type
)
as
    v_need_id alloc_invn_dtl.need_id%type := p_need_id;
    v_crit_desc task_parm.crit_desc%type;
    v_rec_type task_parm.rec_type%type;
begin
    -- the app was already coded first: this is a wrapper for app code to
    -- access the wave task creation rule copying functionality
    begin
        select tp.task_parm_id, tp.rec_type, coalesce(tp.need_type, 'C'),
            tp.crit_desc
        into p_task_parm_id, v_rec_type, p_need_type, v_crit_desc
        from task_parm tp
        where tp.whse = p_whse and tp.rec_type in ('T', 'N') 
            and tp.crit_nbr = p_crit_nbr;
    exception
        when no_data_found then
            raise_application_error(-20050, 
                'Invalid task creation criteria for crit_nbr ' || p_crit_nbr);
    end;

    if (p_need_type != 'A' and p_need_type != 'C')
    then
    -- only allocations and cycle count are serviced by the proc; nothing to do
        return;
    end if;

    if (v_rec_type = 'N')
    then
        -- do not make copies
        o_need_id := p_crit_nbr;
        return;
    end if;
    
    -- now make copies for this task parm tree
    manh_creat_task_parm_rule_tree(p_need_type, p_user_id, p_whse, v_crit_desc,
        p_facility_id, coalesce(p_tc_company_id, wm_utils.wm_get_user_bu(p_user_id)), p_task_parm_id, v_need_id);
    o_need_id := v_need_id;
end;
/

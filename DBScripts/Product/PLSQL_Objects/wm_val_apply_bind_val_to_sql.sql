create or replace procedure wm_val_apply_bind_val_to_sql
(
    p_cursor          in number,
    p_sql_id          in val_job_dtl.sql_id%type,
    p_val_job_dtl_id  in val_job_dtl.val_job_dtl_id%type,
    p_txn_id          in val_result_hist.txn_id%type,
    p_bind_var_csv    in val_sql.bind_var_list%type,
    p_bind_val_csv    in varchar2
)
as
begin
    -- run time bind values can override pre-configured values
    -- apply bind values via a csv arg list when sqls are tested for auto-pop
    for bind_rec in
    (
        select t.bind_var,
            decode(substr(t.bind_var, 1, 2), 'i_', null, t.bind_val) bind_val, 
            decode(substr(t.bind_var, 1, 2), 'i_', to_number(t.bind_val), null) bind_val_int
        from table(wm_val_parse_csv_bind_data(p_bind_val_csv)) t
        where p_bind_val_csv is not null
        union all
        select vsb.bind_var, coalesce(vrb.bind_val, vsb.bind_val) bind_val,
            coalesce(vrb.bind_val_int, vsb.bind_val_int) bind_val_int
        from val_sql_binds vsb
        left join val_runtime_binds vrb on vrb.bind_var = vsb.bind_var
            and vrb.val_job_dtl_id = vsb.val_job_dtl_id and vrb.txn_id = p_txn_id
        where vsb.val_job_dtl_id = p_val_job_dtl_id
    )
    loop
        if (instr(p_bind_var_csv, bind_rec.bind_var, 1, 1) = 0)
        then
            raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to configured bind var :' || bind_rec.bind_var 
                || ' not found in parsed bind list for job dtl ' || p_val_job_dtl_id);
        end if;

        if (substr(bind_rec.bind_var, 1, 2) = 'i_')
        then
            dbms_sql.bind_variable(p_cursor, bind_rec.bind_var, bind_rec.bind_val_int);
            if (p_txn_id is not null)
            then
                wm_val_log('Bound <' || bind_rec.bind_val_int || '> to :' || bind_rec.bind_var,
                    p_sql_log_lvl => 2);
            end if;
        else
            dbms_sql.bind_variable(p_cursor, bind_rec.bind_var, bind_rec.bind_val);
            if (p_txn_id is not null)
            then
                wm_val_log('Bound <' || bind_rec.bind_val || '> to :' || bind_rec.bind_var,
                    p_sql_log_lvl => 2);
            end if;
        end if;
    end loop;
end;
/
show errors;

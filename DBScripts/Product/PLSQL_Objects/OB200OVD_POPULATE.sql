create or replace
PROCEDURE OB200OVD_POPULATE
 (P_RFPID INTEGER,
  P_SCENARIOID INTEGER
 ) IS
      COLLECT_CAPACITY INTEGER;
      l_round_num INTEGER;
      v_ovd_exists INTEGER;
      v_round_num INTEGER;
BEGIN
    SELECT  round_num into l_round_num
    	FROM scenario
    	WHERE rfpid=P_RFPID      AND scenarioid=P_SCENARIOID;
     SELECT      COLLECTCAPACITY into collect_capacity
    	FROM rfp
    	WHERE rfpid = P_RFPID   AND round_num = l_round_num;
   BEGIN
   select  distinct 1 into v_ovd_exists  from ob200override_adv
      			where rfpid=P_RFPID and scenarioid=P_SCENARIOID
   			and source = 'B' and (level_of_detail='X' or level_of_detail='C')  ;
   exception
   	when no_data_found then
   		v_ovd_exists := 0;
   END;
   IF COLLECT_CAPACITY >= 1 and NVL(V_OVD_EXISTS,0) !=1 THEN
    FOR CURSOR_RR IN
    (
	SELECT  max(round_num) max_round_num , obcarriercodeid
			FROM rfpresponse
			WHERE rfpid=p_rfpid
			    AND rfpresponsestatus=1
			    AND round_num <= l_round_num group by obcarriercodeid)
	LOOP
	BEGIN
 	SELECT max(cbr.round_num) into v_round_num from capacitybidrfp cbr, rfpresponse rr
        		where cbr.rfpid = rr.rfpid
        			and cbr.obcarriercodeid = rr.obcarriercodeid
				and cbr.obcarriercodeid = CURSOR_RR.obcarriercodeid
        			and cbr.rfpid = p_rfpid
        			and rr.rfpresponsestatus = 1
        			and cbr.round_num <=l_round_num
        			and cbr.round_num = rr.round_num group by cbr.obcarriercodeid;
        exception
   	when no_data_found then
   		v_round_num := null;
   	END;
	INSERT INTO OB200OVERRIDE_ADV
    	(
		SCENARIOID ,
		 OVERRIDE_ID ,
		ENABLE_FLAG ,
		SOURCE ,
		LEVEL_OF_DETAIL ,
		TYPE ,
		EQUIP_TYPE ,
		CONTRACT_TYPE ,
		OBCARRIERCODEID ,
		MAX_VALUE ,
		MIN_VALUE ,
		TIMESTAMP ,
		IS_VALID ,
		ADDITIONAL_CRITERIA ,
		RFPID ,
		ROUND_NUM
    	)
    	SELECT
		p_scenarioid,
		override_id_seq.nextval,
		1,
		'B',
		'X',
		'O',
		'A',
		0,
		cbf.OBCARRIERCODEID,
		cbf.WEEKLYCAPACITY,
		-1,
		sysdate,
		1,
		 (case cbf.DIRECTION when 'OUT' then 'ORIGINFACILITYCODE' else 'DESTINATIONFACILITYCODE' end) ||'=' || '''' || FACILITYCODE || '''' addcrt,
		P_RFPID ,
		cbf.round_num
    			FROM CAPACITYBIDFACILITY cbf
    			WHERE rfpid=P_RFPID
        			AND cbf.OBCARRIERCODEID=CURSOR_RR.obcarriercodeid
        			AND cbf.round_num = v_round_num
					 AND cbf.FACILITYCODE != '-'
        			AND WEEKLYCAPACITY is not null;
					
INSERT INTO OB200OVERRIDE_ADV
    	(
		SCENARIOID ,
		 OVERRIDE_ID ,
		ENABLE_FLAG ,
		SOURCE ,
		LEVEL_OF_DETAIL ,
		TYPE ,
		EQUIP_TYPE ,
		CONTRACT_TYPE ,
		OBCARRIERCODEID ,
		MAX_VALUE ,
		MIN_VALUE ,
		TIMESTAMP ,
		IS_VALID ,
		ADDITIONAL_CRITERIA ,
		RFPID ,
		ROUND_NUM
    	)
    	SELECT
		p_scenarioid,
		override_id_seq.nextval,
		1,
		'B',
		'X',
		'O',
		'A',
		0,
		cbf.OBCARRIERCODEID,
		cbf.WEEKLYCAPACITY,
		-1,
		sysdate,
		1,
		 (case cbf.DIRECTION when 'OUT' then 'ORIGINCAPACITYAREACODE' else 'DESTINATIONCAPACITYAREACODE' end) ||'=' || '''' || AREACODE || '''' addcrt,
		P_RFPID ,
		cbf.round_num
    			FROM CAPACITYBIDFACILITY cbf
    			WHERE rfpid=P_RFPID
        			AND cbf.OBCARRIERCODEID=CURSOR_RR.obcarriercodeid
        			AND cbf.round_num = v_round_num
					AND cbf.AREACODE != '-'
        			AND WEEKLYCAPACITY is not null;					
 	INSERT INTO OB200OVERRIDE_ADV
    	(
		SCENARIOID ,
		 OVERRIDE_ID ,
		ENABLE_FLAG ,
		SOURCE ,
		LEVEL_OF_DETAIL ,
		TYPE ,
		EQUIP_TYPE ,
		CONTRACT_TYPE ,
		OBCARRIERCODEID ,
		MAX_VALUE ,
		MIN_VALUE ,
		TIMESTAMP ,
		IS_VALID ,
		RFPID ,
		ROUND_NUM
    	)
    	SELECT
  		p_scenarioid,
		override_id_seq.nextval,
		1,
		'B',
		'C',
		'O',
		'A',
		0,
		cbr.OBCARRIERCODEID,
		cbr.WEEKLYCAPACITY,
		-1,
		sysdate,
		1,
		P_RFPID,
		cbr.round_num
    FROM CAPACITYBIDRFP cbr
    WHERE RFPID = P_RFPID
        AND cbr.OBCARRIERCODEID = CURSOR_RR.obcarriercodeid
        AND cbr.round_num = v_round_num;
END LOOP;
END IF;
END OB200OVD_POPULATE;
/
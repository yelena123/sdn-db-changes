call quiet_drop('procedure', 'manh_wave_check_sams_capcty');
call quiet_drop('procedure', 'manh_wave_check_dlrs_capcty');
call quiet_drop('procedure', 'manh_wave_check_lines_capcty');
call quiet_drop('procedure', 'manh_wave_check_lpns_capcty');
call quiet_drop('procedure', 'manh_wave_check_piks_capcty');
call quiet_drop('procedure', 'manh_wave_check_units_capcty');

create or replace procedure wm_wave_check_owt_hdr_capcty
(
    p_user_id                   in ucl_user.user_name%type,
    p_pull_all_swc              in wave_parm.pull_all_swc%type,
    p_reject_distro_rule        in wave_parm.reject_distro_rule%type,
    p_remaining_capcty          in out number,
    p_work_type                 in wave_work_type_stats.work_type%type,
    p_cap_col_str               varchar2
)
as
    v_used_capcty               wave_work_type_stats.sams_capcty%type;
    v_cap_check_sql             varchar2(4000);
    v_first_rejected_line       number(9);
    v_rsn_code                  order_line_item.reason_code%type
        := case p_cap_col_str when 'nbr_of_units' then '54' when 'sam' then '53' 
            when 'nbr_of_piks' then '52' when 'nbr_of_lpns' then '51' when 'nbr_of_lines' then '50'
            when 'nbr_of_dlrs' then '49' else null end;
begin
    -- exclude lines that were a part of selection due to swc; collect soft
    -- alloc lines with lines that exceed the remaining cap; aside from 
    -- these specific lines, collect a) swcs b) items c) orders
    v_cap_check_sql :=
    'with sa_lines as
    (
        select t1.order_id, t2.id,
            row_number() over(partition by t2.id order by owt.line_item_id nulls last) rn,
            decode(owt.line_item_id, null, 0, owt._CAP_COL_STR_) * t1.qty_soft_alloc/t2.order_qty line_val,
            first_value(t2.id) over(partition by t1.order_id order by t2.id, owt.line_item_id) min_id,
            first_value(decode(owt.line_item_id, null, owt._CAP_COL_STR_, 0)) over(partition by t1.order_id 
                order by owt.line_item_id nulls first, t2.id) hdr_val
        from tmp_ord_dtl_sku_invn t1
        join order_work_type owt on owt.order_id = t1.order_id
            and (owt.line_item_id = t1.line_item_id or owt.line_item_id is null)
            and owt.work_type = :work_type and owt._CAP_COL_STR_ > 0
        join tmp_wave_selected_orders t2 on t2.order_id = t1.order_id
            and t2.line_item_id = t1.line_item_id and t2.is_swc_pull = 0
        where t1.is_split_line < 4
    )
    select coalesce(min(iv.id), 0), coalesce(min(iv.rng_sum - iv.curr_line_capcty), 0)
    from
    (
        select sa_lines.id, 
            decode(sa_lines.min_id, sa_lines.id, sa_lines.hdr_val, 0) + sa_lines.line_val curr_line_capcty,
            sum(decode(sa_lines.min_id, sa_lines.id, sa_lines.hdr_val, 0) + sa_lines.line_val) 
                over(order by sa_lines.id) rng_sum
        from sa_lines
        where sa_lines.rn = 1
    ) iv
    where iv.rng_sum > :remaining_capcty';
    v_cap_check_sql := replace(v_cap_check_sql, '_CAP_COL_STR_', p_cap_col_str);

    execute immediate v_cap_check_sql 
    into v_first_rejected_line, v_used_capcty
    using p_work_type, p_remaining_capcty;
    wm_cs_log('First rejected line was ' || v_first_rejected_line || '; used '|| v_used_capcty
        || ' when remaining ' || p_cap_col_str || ' was ' || p_remaining_capcty);

    p_remaining_capcty := p_remaining_capcty - v_used_capcty;
    if (v_first_rejected_line = 0)
    then
        return;
    end if;

    delete from tmp_wave_rejected_lines;
    insert into tmp_wave_rejected_lines
    (
        order_id, line_item_id, item_id, ship_group_id
    )
    select t3.order_id, t3.line_item_id,
        case when p_reject_distro_rule = '2' then t3.item_id 
            else null end item_id,
        case when p_pull_all_swc = 'Y' then t3.ship_group_id
            else null end ship_group_id
    from tmp_wave_selected_orders t3
    where t3.id >= v_first_rejected_line and t3.is_swc_pull = 0
        and exists
        (
            select 1
            from tmp_ord_dtl_sku_invn t2
            where t2.order_id = t3.order_id and t2.line_item_id = t3.line_item_id
                and t2.is_split_line < 4
        )
        and exists
        (
            select 1
            from order_work_type owt
            where owt.order_id = t3.order_id 
                and (owt.line_item_id = t3.line_item_id or owt.line_item_id is null)
                and owt.work_type = p_work_type
        );
    wm_cs_log('Rejected lines captured for ' || p_cap_col_str || ' check ' || sql%rowcount);

    manh_wave_capcty_rejections(p_user_id, p_pull_all_swc, p_reject_distro_rule, v_rsn_code);
end;
/
show errors;

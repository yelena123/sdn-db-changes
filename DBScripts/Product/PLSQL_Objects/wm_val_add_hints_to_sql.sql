create or replace procedure wm_val_add_hints_to_sql
(
    p_hints         in val_sql.sql_hints%type,
    p_ident         in val_sql.ident%type default null,
    p_sql_id        in val_sql.sql_id%type default null
)
as
    v_sql_id        val_sql.sql_id%type;
    v_ident         val_sql.ident%type;
begin
    update val_sql vs
    set vs.sql_hints = trim(p_hints), vs.mod_date_time = sysdate
    where vs.ident = p_ident or vs.sql_id = p_sql_id
    returning vs.sql_id, vs.ident into v_sql_id, v_ident;
   
    commit;
end;
/
show errors;

create or replace procedure manh_wave_build_selection_rule
(
    p_wave_by_rule          in varchar2,
    p_list_of_shpmts        in varchar2,
    p_list_of_orders        in varchar2,
    p_facility_id           in whse_master.whse_master_id%type,
    p_perf_pickng_wave      in ship_wave_parm.perf_pickng_wave%type,
    p_perf_rte              in ship_wave_parm.perf_rte%type,
    p_preview_wave_flag     in number,
    p_wave_type_indic       in wave_parm.wave_type_indic%type,
    p_tc_company_id         in wave_parm.tc_company_id%type,
    p_pull_all_swc          in wave_parm.pull_all_swc%type,
    p_rule_id               in wave_rule_parm.rule_id%type,
    p_rule_type             in rule_hdr.rule_type%type,
    p_is_wave_by_shpmt      in varchar2,
    p_chase_wave            in wave_parm.chase_wave%type,
    p_order_selection_sql   in out varchar2,
    p_diag_mode             in number default 0
)
as
    v_need_qty_str          varchar2(500);
begin
    p_order_selection_sql :=
        '    insert into tmp_wave_selected_orders'
        || ' ('
        || '    line_item_id, order_qty, allocated_qty, dsg_static_route_id,'
        || '    d_facility_id, item_id, ship_group_id, order_id, is_swc_pull,'
        || '    pre_pack_flag, ppack_grp_code, assort_nbr, invn_type,prod_stat,'
        || '    cntry_of_orgn, batch_nbr, item_attr_1, item_attr_2,'
        || '    item_attr_3, item_attr_4, item_attr_5, sku_sub_code_id,'
        || '    sku_sub_code_value, do_dtl_status, batch_requirement_type, '
        || '    perform_vas, id, need_qty'
        || ' ) ';

    p_order_selection_sql := p_order_selection_sql
        || ' select oli.line_item_id,'
        || case when p_chase_wave in ('1','2') then ' oli.user_canceled_qty order_qty,' 
            else ' oli.order_qty, ' end
        || case when p_chase_wave in ('1','2') then ' 0 allocated_qty,' 
            else ' oli.allocated_qty, ' end 
        || '    o.dsg_static_route_id, o.d_facility_id, oli.item_id, o.ship_group_id, o.order_id, '
        || '    0 is_swc_pull,'
        || '    case when oli.ppack_qty = 0 '
        || '        and oli.assort_nbr is not null and oli.ppack_grp_code is not null '
        || '        then 1 else 0 end pre_pack_flag,'
        || '    oli.ppack_grp_code, oli.assort_nbr, oli.invn_type, oli.prod_stat, oli.cntry_of_orgn,'
        || '    oli.batch_nbr, oli.item_attr_1, oli.item_attr_2, oli.item_attr_3, oli.item_attr_4,'
        || '    oli.item_attr_5, oli.sku_sub_code_id, oli.sku_sub_code_value, oli.do_dtl_status,'
        || '    coalesce(oli.batch_requirement_type, ''2'') batch_requirement_type,'
        || '    decode(oli.vas_process_type, ''1'', decode(o.wm_order_status, 20, 2, 1), 0) perform_vas, '
        || '    iv.id, ';

    v_need_qty_str := case when p_chase_wave in ('1', '2') then ' oli.user_canceled_qty ' 
                          else ' oli.order_qty - oli.allocated_qty' end;
      
    p_order_selection_sql := p_order_selection_sql || v_need_qty_str
        || ' from order_line_item oli'
        || ' join orders o on o.order_id = oli.order_id'
        || ' join'
        || ' ('
        || ' select iv2.order_id, iv2.line_item_id, min(iv2.id) id'
        || ' from'
        || ' ('
        || ' select orders.order_id, order_line_item.line_item_id,'
        || ' row_number() over(order by sort_criteria) id ' ;

    manh_wave_add_selection_joins(p_is_wave_by_shpmt, p_chase_wave,
        p_perf_pickng_wave, p_wave_type_indic, p_preview_wave_flag, p_perf_rte,
        p_tc_company_id, p_wave_by_rule, p_rule_type, 
        p_rule_id, p_list_of_shpmts, p_list_of_orders, p_facility_id, p_order_selection_sql);

    p_order_selection_sql := p_order_selection_sql
        || case when p_diag_mode = 1 then ' and 1 = 2' else ' ' end
        || case when p_is_wave_by_shpmt = 'Y' then ' and shipment.o_facility_number = ' || p_facility_id  else ' ' end
        || ' ) iv2'
        || ' group by iv2.order_id, iv2.line_item_id'
        || ' ) iv on iv.order_id = oli.order_id and iv.line_item_id = oli.line_item_id'
        || case when p_perf_pickng_wave = '1' and p_preview_wave_flag != 2 
           then ' where (' || v_need_qty_str || ') > 0' else ' ' end;
end;
/
show errors;

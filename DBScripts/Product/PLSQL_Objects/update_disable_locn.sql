create or replace procedure update_disable_locn
(
    p_locn_class in locn_wiz_hdr.locn_class%type,
    p_mod_date_time in locn_hdr.mod_date_time%type,
    p_crt_upd_locn out number
)
as
begin    
    p_crt_upd_locn := 0;
    
    update locn_hdr 
    set locn_class = case 
        when locn_class = 'R' then '0'
        when locn_class = 'A' then '1'
        when locn_class = 'C' then '2'
    end,
    mod_date_time = p_mod_date_time
    where locn_class in ('R', 'A', 'C')
        and locn_id in 
        (
            select locn_id
            from locn_wizard_locn_tmp tl
        );
        
    update WM_INVENTORY 
    set locn_class = case 
        when locn_class = 'R' then '0'
        when locn_class = 'A' then '1'
        when locn_class = 'C' then '2'
    end,
    last_updated_dttm = p_mod_date_time
    where locn_class in ('R', 'A', 'C')
        and location_id in 
        (
            select locn_id
            from locn_wizard_locn_tmp tl
        );    

    p_crt_upd_locn := sql%rowcount;
end;
/

create or replace procedure manh_wave_load_selection_rules
(
    p_wave_by_rule               in varchar2,
    p_ship_wave_parm_id          in ship_wave_parm.ship_wave_parm_id%type,
    p_is_wave_from_shpmt_ui      in varchar2,
    p_perform_fill_capacity      in number,
    p_preview_wave_flag          in number,
    p_chase_wave                 in wave_parm.chase_wave%type
)
as
begin
    if (coalesce(p_wave_by_rule, 'N') != 'Y')
    then
        -- a dummy row for wave by orders/shpmts; we disregard rule capacities
        insert into tmp_wave_selection_rules
        (
            rule_id, units_capcty, max_orders, max_order_lines, is_wave_by_shpmt
        )
        values
        (
            0, 0, 0, 0, p_is_wave_from_shpmt_ui
        );
        return;
    end if;

    -- assumption: the fill cap rule is always configured with lowest prty
    insert into tmp_wave_selection_rules
    (
        rule_id, units_capcty, max_orders, max_order_lines, prty, rule_type,
        is_wave_by_shpmt
    )
    select wrp.rule_id, coalesce(nullif(wrp.units_capcty, 0), 999999999),
        coalesce(nullif(wrp.max_orders, 0), 999999999), 
        coalesce(nullif(wrp.max_order_lines, 0), 999999999),
        row_number() over(order by wrp.rule_prty), rh.rule_type,
        case when exists
        (
            select 1
            from rule_sel_dtl rsd
            where rsd.rule_id = wrp.rule_id
                and 
                (
                    rsd.tbl_name = 'SHIPMENT'
                    or (rsd.tbl_name = 'ORDERS'
                        and rsd.colm_name in ('SHIPMENT_ID', 'TC_SHIPMENT_ID')
                        and upper(rsd.oper_code) not like '%NULL%')
                )
        ) then 'Y' else 'N' end is_wave_by_shpmt
    from wave_rule_parm wrp
    join rule_hdr rh on rh.rule_id = wrp.rule_id
    where wrp.wave_parm_id = p_ship_wave_parm_id and wrp.stat_code = 0
        and exists 
           (select 1 
            from rule_sel_dtl rsd
            where rsd.rule_id = rh.rule_id)
        and (rh.rule_type = (case p_preview_wave_flag when 2 then 'PW' else 'SS' end)
            or (rh.rule_type = 'PF' and p_perform_fill_capacity = 1)
            or (rh.rule_type = 'CS' and p_chase_wave in ('1','2'))); 
    wm_cs_log('Found ' || sql%rowcount || ' rules for SWP ID ' || p_ship_wave_parm_id);
end;
/
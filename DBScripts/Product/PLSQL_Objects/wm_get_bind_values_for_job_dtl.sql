create or replace function wm_get_bind_values_for_job_dtl
(
    p_val_job_dtl_id    in val_job_dtl.val_job_dtl_id%type
)
return varchar2
as
    v_bind_val_list_csv varchar2(2000);
begin
    select listagg(vsb.bind_var || ',' || coalesce(vsb.bind_val, to_char(vsb.bind_val_int)), ',')
        within group(order by vsb.bind_var)
    into v_bind_val_list_csv
    from val_sql_binds vsb
    where vsb.val_job_dtl_id = p_val_job_dtl_id;
    
    return v_bind_val_list_csv;
end;
/
show errors;

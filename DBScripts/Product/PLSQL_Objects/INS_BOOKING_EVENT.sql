CREATE OR REPLACE PROCEDURE INS_BOOKING_EVENT (
   vBookingID    IN BOOKING_EVENT.booking_ID%TYPE,
   vFieldName    IN BOOKING_EVENT.FIELD_NAME%TYPE,
   vOldValue     IN BOOKING_EVENT.OLD_VALUE%TYPE,
   vNewValue     IN BOOKING_EVENT.NEW_VALUE%TYPE,
   vSourceType   IN BOOKING_EVENT.CREATED_SOURCE_TYPE%TYPE,
   vSource       IN BOOKING_EVENT.CREATED_SOURCE%TYPE)
AS

   vEventSeq   NUMBER (4, 0);
BEGIN

--   SELECT NVL (MAX (EVENT_SEQ), 0) + 1
--     INTO vEventSeq
--     FROM BOOKING_EVENT
--    WHERE BOOKING_ID = vBookingID;

   INSERT INTO booking_EVENT (BOOKING_ID,
                              EVENT_SEQ,
                              FIELD_NAME,
                              OLD_VALUE,
                              NEW_VALUE,
                              CREATED_SOURCE_TYPE,
                              CREATED_SOURCE,
                              CREATED_DTTM)
        VALUES (vBookingID,
                SEQ_BOOKING_EVENT_ID.nextval,
                vFieldName,
                vOldValue,
                vNewValue,
                vSourceType,
                vSource,
                SYSDATE);

END;
/

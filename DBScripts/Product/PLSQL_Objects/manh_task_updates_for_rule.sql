create or replace procedure manh_task_updates_for_rule
as
begin
    merge into task_hdr th
    using
    (
        select t.task_id, sum(t.xpectd_durtn) xpectd_durtn, min(t.item_id) an_item_id,
            count(distinct t.item_id) item_count,
            max(dlh.work_grp) keep(dense_rank first
            order by t.task_seq_nbr) start_dest_work_grp,
            max(dlh.work_area) keep(dense_rank first
            order by t.task_seq_nbr) start_dest_work_area,
            max(dlh.work_grp) keep(dense_rank first
            order by t.task_seq_nbr desc) end_dest_work_grp,
            max(dlh.work_area) keep(dense_rank first 
            order by t.task_seq_nbr desc) end_dest_work_area,
            max(plh.area) keep(dense_rank first
            order by t.task_seq_nbr) begin_area,
            max(plh.zone) keep(dense_rank first
            order by t.task_seq_nbr) begin_zone,
            max(plh.aisle) keep(dense_rank first
            order by t.task_seq_nbr) begin_aisle,        
            max(plh.work_grp) keep(dense_rank first
            order by t.task_seq_nbr) start_curr_work_grp,
            max(plh.work_area) keep(dense_rank first
            order by t.task_seq_nbr) start_curr_work_area,
            max(plh.area) keep(dense_rank first
            order by t.task_seq_nbr desc) end_area,
            max(plh.zone) keep(dense_rank first
            order by t.task_seq_nbr desc) end_zone,
            max(plh.aisle) keep(dense_rank first
            order by t.task_seq_nbr desc) end_aisle,        
            max(plh.work_grp) keep(dense_rank first
            order by t.task_seq_nbr desc) end_curr_work_grp,
            max(plh.work_area) keep(dense_rank first
            order by t.task_seq_nbr desc) end_curr_work_area
        from tmp_task_creation_selected_aid t
        left join locn_hdr dlh on dlh.locn_id = t.dest_locn_id
        left join locn_hdr plh on plh.locn_id = t.pull_locn_id
        group by t.task_id
    ) iv on (iv.task_id = th.task_id)
    when matched then
    update set th.mod_date_time = sysdate, th.xpectd_durtn = iv.xpectd_durtn, 
        th.begin_area = iv.begin_area, th.begin_zone = iv.begin_zone, 
        th.begin_aisle = iv.begin_aisle, th.end_aisle = iv.end_aisle,
        th.start_curr_work_grp = iv.start_curr_work_grp,
        th.start_curr_work_area = iv.start_curr_work_area, 
        th.start_dest_work_grp = iv.start_dest_work_grp, 
        th.start_dest_work_area = iv.start_dest_work_area, 
        th.end_area = iv.end_area, th.end_zone = iv.end_zone, 
        th.end_curr_work_grp = iv.end_curr_work_grp,
        th.end_curr_work_area = iv.end_curr_work_area, 
        th.end_dest_work_grp = iv.end_dest_work_grp, 
        th.end_dest_work_area = iv.end_dest_work_area, 
        th.item_id = decode(iv.item_count, 1, iv.an_item_id, null);
    wm_cs_log('Updated expected duration on TH ' || sql%rowcount);
end;
/
show errors;

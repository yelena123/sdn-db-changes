create or replace PROCEDURE PURGE_ONLY_PROC
(
  P_PROD_TABLE_NAME     VARCHAR2,
  p_query               VARCHAR2
)
is
/*****************************************************************************************************************************
PROCEDURE   : PURGE_ONLY_PROC
PURPOSE     : DELETE TABLES USING PARALLELISM BASED ON THE CONDITIONS PROVIDED, CAN BE USED FOR ANY TABLE DELETE
AUTHOR      : RAJ S K
******************************************************************************************************************************
MODIFIED ON/BY: 29/06/2016 / RAJ S K     
DETAILS       : FETCHING COMMIT FREQUENCY ONLY FROM PARAM_DEF TABLE AND CHANGING DEFAULT TO 100000 IF PARAMETER DOES NOT EXIST
******************************************************************************************************************************/
      v_ins_sql           VARCHAR2 (32767);
      v_del_sql           VARCHAR2 (32767);
      v_sel_sql           VARCHAR2 (32767);
      t_col_list          CLOB;
      n_parallel_level    PLS_INTEGER := 10;
      n_chunk_size        PLS_INTEGER := 1000000;
      n_count             PLS_INTEGER := 0;
      v_schema_owner      VARCHAR2 (100);
      v_prod_table_name   VARCHAR2 (30);
      V_REC_COUNT         PLS_INTEGER := 0;
      gcommitfrequency    PLS_INTEGER;
      gvprocedurename     VARCHAR2 (100);
      varchiveflag        NUMBER (1) := 0;
      NSQLCODE          NUMBER;
      VSQLERRM          varchar2 (300);
BEGIN
        v_prod_table_name := UPPER(p_prod_table_name);
        GVPROCEDURENAME   := V_PROD_TABLE_NAME;
        
        select  NVL(PARAM_DEF.DFLT_VALUE, 100000)
        into    GCOMMITFREQUENCY
        FROM    param_def
        where   PARAM_DEF.PARAM_DEF_ID = 'ARCHIVE_COMMIT_FREQUENCY'
        and     PARAM_DEF.PARAM_GROUP_ID = 'ARCHIVE'
        and     PARAM_DEF.IS_DISABLED = 0;

        n_chunk_size := gcommitfrequency ;

        SELECT  SYS_CONTEXT ('userenv', 'current_schema')
        INTO    V_SCHEMA_OWNER
        FROM    DUAL;

        --Check for existance of records to be purged
        v_sel_sql := 'SELECT COUNT(1) FROM ' || V_PROD_TABLE_NAME || ' WHERE ' || p_query;

        EXECUTE IMMEDIATE (v_sel_sql) INTO v_rec_count;

        IF v_rec_count > 0
        THEN
            -- Clenaing up any running delete task
            SELECT  COUNT(1)
            INTO    N_COUNT
            from    USER_PARALLEL_EXECUTE_TASKS
            WHERE   task_name = lower('del_records_task');

            IF n_count > 0
            THEN
              dbms_parallel_execute.drop_task ('del_records_task');
            END IF;

           -- create tasks to run in parallel chunks
           dbms_parallel_execute.create_task (task_name => 'del_records_task');

            v_del_sql :=  'DELETE  FROM ' || v_prod_table_name || ' WHERE ' || p_query || ' AND rowid between :start_id and :end_id';

            --creating chunks and assigning taskname to it
            dbms_parallel_execute.create_chunks_by_rowid (task_name => 'del_records_task', table_owner => v_schema_owner, table_name => v_prod_table_name, by_row => TRUE, chunk_size => n_chunk_size);

            --execute task in parallel
            dbms_parallel_execute.run_task (task_name => 'del_records_task', sql_stmt => v_del_sql, language_flag => DBMS_SQL.native, parallel_level => n_parallel_level);

            n_count := 0;
            COMMIT;

        END IF;

        ARCHIVE_PKG.LOG_ARCHIVE (null, GVPROCEDURENAME, V_REC_COUNT, null, SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
END;
/
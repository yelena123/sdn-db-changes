create or replace package body wm_utils
is
    c_app_context_data_sz constant number(2) := 64;

    function str_trunc
    (
        p_str in varchar2
    )
    return varchar2
    as
    begin
        return substr(p_str, 1, c_app_context_data_sz);
    end;
    
    function wm_get_user_bu
    (
        p_user_id ucl_user.user_name%type
    )
    return number
    as
        v_default_bu    wave_parm.tc_company_id%type;    
    begin
        select ud.parameter_value
        into v_default_bu
        from ucl_user uu
        join user_default ud on uu.user_name = p_user_id
            and uu.ucl_user_id = ud.ucl_user_id
            and ud.parameter_name ='USER_DEFAULT_BU_ID';
        
        return(v_default_bu);
    end;
    
    procedure set_context_info
    (
        p_module_name in varchar2,
        p_action_name in varchar2,
        p_client_id   in varchar2 default null
    )
    as
    begin
        dbms_application_info.set_module(module_name => p_module_name, action_name => p_action_name);
        if (p_client_id is null)
        then
            -- probably better than explicitly nullifying it
            dbms_session.clear_identifier();
        else
            dbms_session.set_identifier(client_id => str_trunc(p_client_id));
        end if;
    end;

    procedure get_context_info
    (
        p_module_name out varchar2,
        p_action_name out varchar2
    )
    as
    begin
        dbms_application_info.read_module(p_module_name, p_action_name);
    end;
end;
/

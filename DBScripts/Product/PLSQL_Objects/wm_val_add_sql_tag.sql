create or replace procedure wm_val_add_sql_tag
(
    p_2_tuple       in varchar2,
    p_ident         in val_sql.ident%type default null,
    p_sql_id        in val_sql.sql_id%type default null
)
as
    v_sql_id        val_sql.sql_id%type := p_sql_id;
    v_2_tuple       varchar2(20) := replace(p_2_tuple, ' ', '');
begin
    if (v_sql_id is null)
    then
        select vs.sql_id
        into v_sql_id
        from val_sql vs
        where vs.ident = p_ident;
    end if;

    insert into val_sql_tags
    (
        type, sql_id, tag
    )
    values
    (
        substr(v_2_tuple, 1, instr(v_2_tuple, ',') - 1), v_sql_id,
        substr(v_2_tuple, instr(v_2_tuple, ',') + 1)
    );
end;
/
show errors;

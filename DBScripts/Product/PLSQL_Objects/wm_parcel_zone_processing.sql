create or replace procedure wm_parcel_zone_processing(
    p_tc_company_id     in number,
    p_sl_company_id     in number,
    p_bu_company_id     in number,
    p_carrier_code in parcel_zone_data.carrier_code%type default null,
    p_origin_zip     in varchar2)
as
    v_serv_type          VARCHAR2(5000);
    v_transit_time       VARCHAR2(5000);
    v_service_level_type VARCHAR2(500);
    v_carrier_id         NUMBER;
    v_postal_code_like   VARCHAR2(10);
begin
  -- this procedure moves records from inpt_parcl_zone table to parcel_zone_data
  -- multiple rows compressed into a single record to minimize the data set
  -- first delete zone records for the given carrier code and origin zip if already exists


    delete
    from parcel_zone_data
    where dest_from_zip        is not null
    and dest_to_zip          is not null
    and length(dest_from_zip) >1
    and length(dest_to_zip)   >1
    and (dest_cntry  = 'US'
    or dest_cntry    = 'PR')
    and carrier_code = p_carrier_code;

     -- delete international zone records for the given carrier code, it will be re-created.
    delete
    from parcel_zone_data
    where  carrier_code = p_carrier_code
    and dest_cntry  != 'US'
    and dest_cntry    != 'PR';


  -- create new set of records from inpt_parcl_zone

    for zonerecord in
    ( select distinct coalesce(origin_from_zip,p_origin_zip) origin_from_zip,
      coalesce(origin_to_zip,p_origin_zip) origin_to_zip, 
      dest_from_zip,
      dest_to_zip,
      dest_cntry
    from inpt_parcl_zone
    where dest_from_zip        is not null
    and dest_to_zip          is not null
    and length(dest_from_zip) >1
    and length(dest_to_zip)   >1
    and (dest_cntry = 'US'
    or dest_cntry   = 'PR')
    and serv_type  in
      (select serv_type
      from inpt_carrier_service
      where carrier_code = p_carrier_code
      )
    union all
    select distinct origin_from_zip,
      origin_to_zip,
      dest_from_zip,
      dest_to_zip,
      dest_cntry
    from inpt_parcl_zone
    where dest_cntry != 'US'
    and dest_cntry   != 'PR'
    and serv_type    in
      (select serv_type
      from inpt_carrier_service
      where carrier_code = p_carrier_code
      )
    )
    loop
      v_serv_type    := '';
      v_transit_time :='';
      for servtyperecord in
      (select distinct serv_type,
        trim(zone) zone,
        days_from_orgn
      from inpt_parcl_zone
      where  ( origin_from_zip = zonerecord.origin_from_zip or origin_from_zip is null)
      and (origin_to_zip   =zonerecord.origin_to_zip
      or origin_to_zip    is null)
      and dest_from_zip    =zonerecord.dest_from_zip
      and dest_to_zip      =zonerecord.dest_to_zip
      and dest_cntry       = zonerecord.dest_cntry
      )
      loop
        begin
          select distinct service_level_type
          into v_service_level_type
          from inpt_carrier_service
          where inpt_carrier_service.serv_type=servtyperecord.serv_type;
          v_serv_type                     := v_serv_type || v_service_level_type || '-' ||servtyperecord.zone || '|';
          v_transit_time                  := v_transit_time || v_service_level_type || '-' || servtyperecord.days_from_orgn || '|';
        exception
        when no_data_found then
          continue;
        end;
      end loop ;
      insert
      into parcel_zone_data
        (
          parcel_zone_data_id,
          carrier_code,
          origin_from_zip,
          origin_to_zip,
          dest_from_zip,
          dest_to_zip ,
          dest_cntry,
          zones_per_sl,
          transit_days_per_sl,
          created_date_time
        )
        values
        (
          seq_parcel_rate_data_id.nextval,
          p_carrier_code,
          zonerecord.origin_from_zip,
          zonerecord.origin_to_zip,
          zonerecord.dest_from_zip,
          zonerecord.dest_to_zip,
          zonerecord.dest_cntry,
          substr(v_serv_type,1,length(v_serv_type)      -1),
          substr(v_transit_time,1,length(v_transit_time)-1),
          sysdate
        );
    end loop;


  select carrier_id
  into v_carrier_id
  from carrier_code
  where carrier_code=p_carrier_code
  and tc_company_id =p_tc_company_id;
  -- delete existing transit time records for the given carrier and
  -- create new records
  delete
  from parcel_transit_time
  where carrier_id  =v_carrier_id
  and tc_company_id =p_bu_company_id
  and origin_zip    = p_origin_zip;
  v_postal_code_like  := p_origin_zip || '%' ;
  insert
  into parcel_transit_time
    (
      parcel_transit_time_id,
      tc_company_id,
      carrier_id,
      service_level_id,
      origin_zip,
      dest_from_zip,
      dest_to_zip,
      days_from_origin,
      dest_cntry,
      created_date_time
    )
    (select seq_parcel_rate_data_id.nextval,
        p_bu_company_id,
        v_carrier_id,
        sl.service_level_id,
        case
          when length(itr.postal_code) > 5
              then substr(itr.postal_code,1,5)
          else p_origin_zip
        end origin_zip ,
        case
          when length(itr.postal_code) = 3
          then ( itr.postal_code
            || '00' )
          when length(itr.postal_code) = 5
          then itr.postal_code
          when length(itr.postal_code) > 5
          and length(substr(itr.postal_code,6,5)) = 3
          then (substr(itr.postal_code,6,5)
            || '00')
          else substr(itr.postal_code,6,5)
        end dest_from_zip,
        case
          when length(itr.postal_code) = 3
          then ( itr.postal_code
            || '99' )
          when length(itr.postal_code) = 5
          then itr.postal_code
          when (length(itr.postal_code) > 5
          and length(substr(itr.postal_code,6,5)) = 3)
          then ( substr(itr.postal_code,6,5)
            || '99' )
          else substr(itr.postal_code,6,5)
        end dest_to_zip ,
        itr.transit_time_in_days,
        'US',
        sysdate
      from service_level sl
      left join inpt_carrier_service ics
      on sl.service_level = ics.service_level_type
      left join inpt_transit_time itr
      on ics.serv_type       = itr.serv_type
      where sl.tc_company_id = p_sl_company_id
      and itr.carrier_code   = p_carrier_code
      --and itr.postal_code like v_postal_code_like
    );
end ;
/

show errors;
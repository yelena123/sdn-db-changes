create or replace procedure wm_val_exec_job
(
    p_user_id           in  user_profile.user_id%type,
    p_app_hook          in  val_job_seq.app_hook%type,
    p_pgm_id            in  val_job_seq.pgm_id%type,
    p_rt_bind_list_csv  in  varchar2,
    p_log_lvl           in  number default 0,
    p_parent_txn_id     in out val_result_hist.parent_txn_id%type
)
as
    v_cursor            int;
begin
    if (p_parent_txn_id is null)
    then
        p_parent_txn_id := val_txn_id_seq.nextval;
    end if;

    delete from tmp_val_parm t;
    insert into tmp_val_parm
    (
        user_id, parent_txn_id, pgm_id, log_lvl, module, msg_id, ref_code_1, ref_value_2, app_hook,
        val_pgm_id
    )
    values
    (
        p_user_id, p_parent_txn_id, 'wm_val_exec_job', p_log_lvl, 'WAVE', '1094', 'VAL',
        p_app_hook || '(' || p_pgm_id || ')', p_app_hook, p_pgm_id
    );

    wm_val_log('->Beginning (' || p_app_hook || ', ' || p_pgm_id || ') validation; runtime binds are <'
        || substr(p_rt_bind_list_csv, 1, 400) || '>');

    for job_rec in
    (
        select rownum rn, vjs.job_name
        from val_job_seq vjs
        where vjs.app_hook = p_app_hook and vjs.pgm_id = p_pgm_id
        order by vjs.seq_nbr
    )
    loop
        if (job_rec.rn = 1)
        then
            v_cursor := dbms_sql.open_cursor;
        end if;

        update tmp_val_parm t
        set t.txn_id = val_txn_id_seq.nextval;

        if (p_rt_bind_list_csv is not null)
        then
            wm_val_extract_runtime_binds(p_user_id, job_rec.job_name, p_rt_bind_list_csv,
                wm_val_get_txn_id());
        end if;

        wm_val_log('-->Beginning ' || job_rec.job_name);
        wm_val_exec_job_intrnl(p_user_id, job_rec.job_name, v_cursor);
        wm_val_log('-->Completed ' || job_rec.job_name);
    end loop;

    if (dbms_sql.is_open(v_cursor) = true)
    then
        dbms_sql.close_cursor(v_cursor);
    end if;
    wm_val_log('->Completed (' || p_app_hook || ', ' || p_pgm_id || ') validation');
end;
/
show errors;

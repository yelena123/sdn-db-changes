create or replace
PROCEDURE createdefaultcapacityselection  (
        I_RFPID int
    )  IS
 v_capacityType INTEGER;
 v_round_num INTEGER;
BEGIN

select max(round_num)
 into v_round_num
 from rfp where rfpid= I_RFPID ;
 
-- Update the facility selections.
 select distinct collectcapacity 
 into v_capacityType
 from rfp where rfpid= I_RFPID and round_num=v_round_num;

INSERT INTO capacityselectionrfp ( rfpid )
SELECT DISTINCT rfpid  FROM rfp  WHERE rfpid=i_rfpid
    AND rfpid NOT IN ( SELECT  rfpid  FROM capacityselectionrfp ) ;


If(V_Capacitytype=1 Or V_Capacitytype=3)
THEN
-- Insert the Facility Out selection
Insert Into Capacityselectionfacility ( Rfpid, Facilitycode,Areacode,City,State,Postalcode,Country,Direction,Round_Num )
SELECT DISTINCT rfpid, originfacilitycode,null,origincity,originstateprov,originpostalcode,origincountrycode,'OUT',round_num FROM lane l WHERE rfpid=i_rfpid and round_num<=v_round_num
    AND originfacilitycode IS NOT NULL
    AND    length(originfacilitycode)  <> 0
    --AND originfacilitycode not like ''
    AND lane_type <> 2
    AND originfacilitycode NOT IN ( SELECT facilitycode  FROM capacityselectionfacility
    Where Rfpid=I_Rfpid And Direction='OUT' and Facilitycode IS NOT NULL  ) ;
    
    -- Insert the Facility In selection
Insert Into Capacityselectionfacility ( Rfpid, Facilitycode,Areacode,City,State,Postalcode,Country,Direction,Round_Num )
SELECT DISTINCT rfpid, destinationfacilitycode,null,destinationcity,destinationstateprov,destinationpostalcode,destinationcountrycode,'IN',round_num FROM lane l WHERE rfpid=i_rfpid and round_num<=v_round_num
    AND destinationfacilitycode IS NOT NULL
    AND    length(destinationfacilitycode)  <> 0
    --AND destinationfacilitycode not like ''
    AND lane_type <> 2
    And Destinationfacilitycode Not In ( Select Facilitycode From Capacityselectionfacility
    Where Rfpid=I_Rfpid And Direction='IN' And Facilitycode Is Not Null) ;
  END IF;
  
  If(V_Capacitytype=2 Or V_Capacitytype=3)
   THEN
-- Insert the Area Out selection
Insert Into Capacityselectionfacility ( Rfpid, Facilitycode,Areacode,City,State,Postalcode,Country, Direction,Round_Num )
SELECT DISTINCT rfpid, null,origincapacityareacode,null,null,null,origincountrycode,'OUT',round_num FROM lane l WHERE rfpid=i_rfpid and round_num<=v_round_num
     AND (originfacilitycode IS  NULL OR originfacilitycode IS NOT NULL)
    AND origincapacityareacode IS NOT NULL

    AND    length(origincapacityareacode)  <> 0
    --AND originfacilitycode not like ''
    AND lane_type <> 2
    AND origincapacityareacode NOT IN ( SELECT areacode  FROM capacityselectionfacility
    WHERE rfpid=i_rfpid AND direction='OUT' and Areacode IS NOT NULL ) ;

-- Insert the Area In selection
Insert Into Capacityselectionfacility ( Rfpid, Facilitycode,Areacode,City,State,Postalcode,Country,Direction,Round_Num )
SELECT DISTINCT rfpid, null,destinationcapacityareacode,null,null,null,destinationcountrycode,'IN',round_num FROM lane l WHERE rfpid=i_rfpid and round_num<=v_round_num
     AND (destinationfacilitycode IS NULL OR destinationfacilitycode IS NOT NULL)
    AND destinationcapacityareacode IS NOT NULL
    AND    length(destinationcapacityareacode)  <> 0
    --AND destinationfacilitycode not like ''
    AND lane_type <> 2
    AND destinationcapacityareacode NOT IN ( SELECT areacode FROM capacityselectionfacility
    Where Rfpid=I_Rfpid And Direction='IN' And Areacode Is Not Null) ;
 End If;
    
    IF(v_capacityType=1 or v_capacitytype=3)
THEN
DELETE FROM capacityselectionfacility  WHERE capacityselectionfacility.direction='IN' and Capacityselectionfacility.Facilitycode is not null
    AND capacityselectionfacility.rfpid=i_rfpid AND NOT EXISTS  ( SELECT  destinationfacilitycode
    FROM lane l WHERE l.destinationfacilitycode=capacityselectionfacility.facilitycode
        AND l.rfpid=i_rfpid
          AND l.destinationfacilitycode IS NOT NULL
        AND lane_type <> 2 ) ;


-- Delete the facility in selection in case some lanes are deleted.
DELETE FROM capacityselectionfacility  WHERE capacityselectionfacility.direction='OUT' and Capacityselectionfacility.Facilitycode is not null
    AND capacityselectionfacility.rfpid=i_rfpid AND NOT EXISTS  ( SELECT  originfacilitycode
    FROM lane l    WHERE l.originfacilitycode=capacityselectionfacility.facilitycode
        AND l.rfpid=i_rfpid
          AND l.originfacilitycode IS NOT NULL
        AND lane_type <> 2 ) ;
  END IF;
  
 IF(v_capacityType=2 or v_capacitytype=3)
   THEN 
 DELETE FROM capacityselectionfacility  WHERE capacityselectionfacility.direction='IN' and Capacityselectionfacility.areacode is not null
    AND capacityselectionfacility.rfpid=i_rfpid AND NOT EXISTS  ( SELECT  destinationcapacityareacode
    FROM lane l WHERE l.destinationcapacityareacode=capacityselectionfacility.areacode
        AND l.rfpid=i_rfpid
        AND l.destinationcapacityareacode IS NOT NULL
        AND lane_type <> 2 ) ;


-- Delete the facility in selection in case some lanes are deleted.
DELETE FROM capacityselectionfacility  WHERE capacityselectionfacility.direction='OUT' and Capacityselectionfacility.areacode is not null
    AND capacityselectionfacility.rfpid=i_rfpid AND NOT EXISTS  ( SELECT  origincapacityareacode
    FROM lane l    WHERE l.origincapacityareacode=capacityselectionfacility.areacode
        AND l.rfpid=i_rfpid
         AND l.origincapacityareacode IS NOT NULL
        AND lane_type <> 2 ) ;       
        

END IF;
END createdefaultcapacityselection;
/
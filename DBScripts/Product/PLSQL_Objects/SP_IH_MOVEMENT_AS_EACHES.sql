CREATE OR REPLACE PROCEDURE SP_IH_MOVEMENT_AS_EACHES
AS
BEGIN
   EXECUTE IMMEDIATE
      'insert into testtemp SELECT hist_id,
                (CASE IH.ship_unit
                    WHEN 2
                    then
                       (IH.movement * IM.each_per_cs)
                    WHEN 4
                    then
                       ( (IH.movement / IM.each_per_inn) * IM.each_per_cs)
                    WHEN 1
                    THEN
                       ( (IH.movement * si.pallet_hi
                          * (CASE si.pallete_pattern
                                WHEN 0 THEN IM.wh_ti
                                WHEN 1 THEN IM.ord_ti
                                WHEN 2 THEN IM.ven_ti
                             end))
                        * IM.each_per_cs)
                 END)
                   val
           FROM item_history IH
                JOIN slot_item si
                   ON si.slotitem_id = IH.slotitem_id
                JOIN so_item_master IM
                   on IM.SKU_ID = SI.SKU_ID
          WHERE IH.ship_unit != 8  and IH.movement > 0 ';


   EXECUTE IMMEDIATE
      'update ITEM_HISTORY a set a.movement= (select VAL from  TESTTEMP B where a.HIST_ID=B.HIST_ID) where a.SHIP_UNIT !=8 and a.movement > 0';
END SP_IH_MOVEMENT_AS_EACHES;
/
SHOW ERRORS;
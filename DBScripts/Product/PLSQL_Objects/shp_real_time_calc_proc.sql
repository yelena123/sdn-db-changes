create or replace 
PROCEDURE SHP_REAL_TIME_CALC_PROC
(
  StopSeq IN NUMBER,
  ShipmentId IN NUMBER,
  CompanyId IN NUMBER,
  NewApptDttm IN DATE,
  ArrivalStartDttm IN DATE,
  ArrivalEndDttm IN DATE
) IS
 early_value VARCHAR2(50 CHAR);
 late_value VARCHAR2(50 CHAR);
 lowerDate DATE;
 upperDate DATE;
 shipArrStatus NUMBER;
 degreeEarlyLateInDays NUMBER;
 degreeEarlyLateInMin NUMBER;
BEGIN
   BEGIN
    SELECT PARAM_VALUE INTO early_value
      FROM COMPANY_PARAMETER
        WHERE TC_COMPANY_ID= CompanyId AND PARAM_DEF_ID='shipment_minutes_before_expected_time';
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      early_value:='30';
  END;

   BEGIN
    SELECT PARAM_VALUE INTO late_value
      FROM COMPANY_PARAMETER
        WHERE TC_COMPANY_ID= CompanyId AND PARAM_DEF_ID='shipment_minutes_after_expected_time';
   EXCEPTION
    WHEN NO_DATA_FOUND THEN
      late_value:='20';
  END;

   IF early_value='' THEN
    early_value:='30';
  END IF;
  IF late_value='' THEN
    late_value:='20';
  END IF;

  IF NewApptDttm IS NOT NULL THEN
  lowerDate := NewApptDttm-cast(early_value AS NUMBER)/1440;
  upperDate := NewApptDttm+cast(late_value AS NUMBER)/1440;
 else
  lowerDate := ArrivalStartDttm-cast(early_value AS NUMBER)/1440;
  upperDate := ArrivalEndDttm+cast(late_value AS NUMBER)/1440;

 END IF;
  shipArrStatus := -2;

  FOR CURSOR_STATUS IN ( SELECT TRACKING_MSG_ID,NXT_STOP_ETA FROM tracking_message WHERE message_type=60 AND
          tc_company_id=CompanyId  AND stop_seq=StopSeq  AND shipment_id=ShipmentId AND latitude IS NOT NULL AND longitude IS NOT NULL
          AND latitude!=0.0 AND longitude!=0.0 ORDER BY EVENT_DTTM ASC)
  LOOP
  IF CURSOR_STATUS.NXT_STOP_ETA IS NOT NULL THEN
    IF (lowerDate < CURSOR_STATUS.NXT_STOP_ETA and upperDate > CURSOR_STATUS.NXT_STOP_ETA ) THEN
      UPDATE TRACKING_MESSAGE SET TRACKING_INDICATOR= 0 WHERE TRACKING_MSG_ID=CURSOR_STATUS.TRACKING_MSG_ID;
      shipArrStatus := 0;
      degreeEarlyLateInDays := 0;
    
    ELSIF (lowerDate > CURSOR_STATUS.NXT_STOP_ETA) THEN
      UPDATE TRACKING_MESSAGE SET TRACKING_INDICATOR= 1 WHERE TRACKING_MSG_ID=CURSOR_STATUS.TRACKING_MSG_ID;
      shipArrStatus := 1;
      degreeEarlyLateInDays := CURSOR_STATUS.NXT_STOP_ETA - lowerDate;
     
    ELSIF (upperDate < CURSOR_STATUS.NXT_STOP_ETA) THEN
      UPDATE TRACKING_MESSAGE SET TRACKING_INDICATOR= -1 WHERE TRACKING_MSG_ID=CURSOR_STATUS.TRACKING_MSG_ID;
      shipArrStatus := -1;
      degreeEarlyLateInDays := CURSOR_STATUS.NXT_STOP_ETA - upperDate;
     
    END IF;
  END IF;
  END LOOP;
   IF degreeEarlyLateInDays IS NOT NULL THEN
    degreeEarlyLateInMin := degreeEarlyLateInDays * 24 *60;
   ELSE
    degreeEarlyLateInMin := null;
   END IF;
  
   UPDATE shipment_extn_tlm SET ship_arr_status= ShipArrStatus WHERE shipment_id=ShipmentId;
   UPDATE shipment_extn_tlm SET DEGREE_LATE_EARLY = degreeEarlyLateInMin where shipment_id=ShipmentId;
END SHP_REAL_TIME_CALC_PROC;
/
create or replace procedure manh_wave_do_status_change_pix
(
    p_facility_id     in facility.facility_id%type,
    p_user_id         in ucl_user.user_name%type,
    p_whse            in facility.whse%type,
    p_pick_wave_nbr   in ship_wave_parm.pick_wave_nbr%type
)
as
    v_proc_stat_code        varchar2(2);
    v_xml_group_attr        varchar2(10);
    v_ref_codes_insert_list varchar2(1000);
    v_ref_codes_select_list varchar2(1000);
    v_insert_sql            varchar2(3000);
    va_bu_cnfg              wm_utils.ta_bu_cnfg;
    v_cnfg_str              varchar2(32000);
    v_bu_inlist_clause      varchar2(1000);
begin
    wm_merge_pix_cnfg_for_elgbl_bu(p_user_id, p_facility_id, p_tran_type => '601',
        p_tran_code => '001', p_actn_code => null, pa_bu_cnfg => va_bu_cnfg);
        
    v_cnfg_str := va_bu_cnfg.first;
    while (v_cnfg_str is not null)
    loop
        v_bu_inlist_clause := ' and orders.tc_company_id in (' || va_bu_cnfg(v_cnfg_str) || ')';
        wm_extract_pix_cnfg_data(v_cnfg_str, v_proc_stat_code, v_xml_group_attr,
            v_ref_codes_insert_list, v_ref_codes_select_list);
    
    v_insert_sql :=
        'insert into pix_tran'
        || '('
        || '    tran_type, tran_code, sys_user_id, user_id, create_date_time, mod_date_time,'
        || '    pix_seq_nbr, tran_nbr, proc_stat_code, facility_id, company_code, whse,'
        || '    tc_company_id, pix_tran_id, xml_group_id'
        ||      v_ref_codes_insert_list
        || ') '
        || 'select ''601'', ''001'', :user_id, :user_id, sysdate, sysdate, 1 pix_seq_nbr,'
        || '    pix_tran_id_seq.nextval, :proc_stat_code, :p_facility_id,  c.company_code,'
        || '    :p_whse, orders.tc_company_id, pix_tran_id_seq.nextval, '''
        ||      v_xml_group_attr || '''' || v_ref_codes_select_list || ' '
        || 'from orders '
        || 'join tmp_wave_old_order_status t on t.order_id = orders.order_id '
        || 'join company c on c.company_id = orders.tc_company_id '
        || 'where t.do_status != orders.do_status and orders.do_status >= 115 '
        || '    and exists'
        || '    ('
        || '        select 1'
        || '        from sys_code sc'
        || '        inner_clause sc.rec_type = ''S'' and sc.code_type = ''501'''
        || '            and substr(sc.misc_flags, 1, 1) = ''1'''
        || '            and to_number(sc.code_id) = orders.do_status'
        || '    )'
        || v_bu_inlist_clause;

    -- this needs to be used with caution; for single line orders only
    if (instr(v_insert_sql, 'ORDER_LINE_ITEM.', 1, 1) > 0)
    then
        v_insert_sql := replace(v_insert_sql, 'where ',
            'join order_line_item on order_line_item.order_id = orders.order_id where ');
    end if;
    
    if ( instr(v_insert_sql, 'TASK_DTL.', 1, 1)  > 0 or instr(v_insert_sql, 'TASK_HDR.', 1, 1) > 0)
    then
        v_insert_sql := replace(v_insert_sql, 'where ',
            ' left join task_dtl on task_dtl.line_item_id = order_line_item.line_item_id where ');
    end if;
    
    if instr(v_insert_sql, 'TASK_HDR.', 1, 1) > 0
    then
        v_insert_sql := replace(v_insert_sql, 'where ',
            ' left join task_hdr on task_hdr.task_id = task_dtl.task_id where ');
    end if;

    v_insert_sql := replace(v_insert_sql, 'inner_clause', 'where');
    
    execute immediate v_insert_sql using p_user_id, p_user_id, v_proc_stat_code, p_facility_id,
        p_whse;
    
    wm_cs_log('Created DO status change pix ' || sql%rowcount);
        -- move along to the sql for the next set of BU's
    v_cnfg_str := va_bu_cnfg.next(v_cnfg_str);
    end loop;
end;
/
show errors;

create or replace procedure manh_wave_aggregate_with_lock
(
    p_user_id       in  ucl_user.user_name%type,
    p_facility_id   in  number,
	  p_rte_wave_nbr  in ship_wave_parm.rte_wave_nbr%type,
    p_pick_wave_nbr in  varchar2,
    p_ship_wave_nbr in  varchar2,
    p_tc_company_id in  number,
	  p_commit_freq   in  number
)
as
    type t_d_facility_id is table of orders.d_facility_id%type index by binary_integer;
    va_active_stores    t_d_facility_id;
    v_num_stores_to_process number(5) := 0;
    v_agg_ol_status     number(3) ;
    v_select_list       varchar2(4000) ;
    v_where_clause      varchar2(4000) ;
    v_aggr_order_sql    varchar2(4000) ;
    v_aggr_group_sql    varchar2(4000) ;    
begin
    select to_number(wp.aggr_ord_lifecycle_status)
    into v_agg_ol_status
    from whse_parameters wp
    where wp.whse_master_id = p_facility_id ;
	
    manh_aggr_build_col_list(p_tc_company_id, v_select_list, v_where_clause) ;
    v_aggr_order_sql := 
        '    insert into tmp_aggregation_orders'
        || ' ('
        || '    order_id, parent_order_id, d_facility_alias_id, d_facility_id, '
        || '    order_group_num '
        || ' )'
        || ' select o.order_id, o.parent_order_id, o.d_facility_alias_id,o.d_facility_id, '
        || '     dense_rank() over(order by o.d_facility_id, o.tc_company_id ' ;

    v_aggr_order_sql := v_aggr_order_sql || v_select_list ;
    
    v_aggr_order_sql := v_aggr_order_sql
        || ') '
        || ' from orders o '
        || ' join '
        || ' ('
        || ' select oli.order_id '
        || ' from order_line_item oli '
        || ' where oli.wave_nbr = :pick_wave_nbr '
        || '    and oli.do_dtl_status between 120 and 130 '
        || ' union '
        || ' select o2.order_id '
        || ' from orders o2 '
        || ' where o2.parent_order_id is not null '
        || '    and exists '
        || '    ( '
        || '         select null '
        || '         from order_line_item oli '
        || '         where oli.order_id = o2.order_id '
        || '             and oli.wave_nbr = :pick_wave_nbr and oli.do_dtl_status = 200 '
        || '    )'
        || ' ) iv on (iv.order_id = o.order_id) '
        || ' where o.o_facility_id = :facility_id and o.is_original_order = 1 ';
    
    execute immediate v_aggr_order_sql using p_pick_wave_nbr, p_pick_wave_nbr, p_facility_id ;
    wm_cs_log('Collected Orig orders ' || sql%rowcount, p_sql_log_level => 0);

    -- build sql to load the agg order groups
    v_aggr_group_sql := 
        '    insert into tmp_aggregation_order_groups '
        || '     ( '
        || '        order_group_num, order_id, parent_order_id '
        || '     ) '
        || '     select iv.order_group_num, iv.order_id_within_group order_id, '
        || '         coalesce(iv.parent_order_id, agg.order_id, '
        || '                     seq_order_id.nextval) parent_order_id '
        || '     from '
        || '     ( '
        || '         select t.order_group_num, max(t.order_id) order_id_within_group, '
        || '             max(t.parent_order_id) parent_order_id '
        || '         from tmp_aggregation_orders t '
        || '         where t.d_facility_id = :va_active_stores '
        || '         group by t.order_group_num '
        || '     ) iv '
        || ' join orders orig on orig.order_id = iv.order_id_within_group '
        || ' left join orders agg on agg.d_facility_id = orig.d_facility_id '
        || '     and agg.o_facility_id = orig.o_facility_id '
        || '     and orig.do_type = agg.do_type '
        || '     and coalesce(orig.tc_company_id, 0) = coalesce(agg.tc_company_id, 0) ' ;
        
    v_aggr_group_sql := v_aggr_group_sql || v_where_clause ;
    
    v_aggr_group_sql := v_aggr_group_sql
        || '     and agg.o_facility_id = :facility_id '
        || '     and agg.is_original_order = 0 and agg.do_status < :agg_ol_status ' ;
        
    insert into tmp_store_master
    (
        d_facility_id
    )
    select distinct t.d_facility_id
    from tmp_aggregation_orders t;
    v_num_stores_to_process := sql%rowcount;
    wm_cs_log('Stores to process ' || v_num_stores_to_process, p_sql_log_level => 0);

    loop
        if (v_num_stores_to_process = 1)
        then
            -- block on this one
            select f.facility_id
            bulk collect into va_active_stores
            from facility f
            where exists
                (
                    select 1
                    from tmp_store_master t
                    where t.d_facility_id = f.facility_id
                )
                and rownum <= p_commit_freq
            for update;
        else
            -- otherwise, cherry-pick stores to process
            select f.facility_id
            bulk collect into va_active_stores
            from facility f
            where exists
                (
                    select 1
                    from tmp_store_master t
                    where t.d_facility_id = f.facility_id
                )
                and rownum <= p_commit_freq
            for update skip locked;
        end if;

        if (va_active_stores.count > 0)
        then
        -- orders within the same 'group' point to exactly one aggr order; so if
        -- any of them were previously aggregated, that aggr order is now
        -- assumed by all 'new' orders within the group that are being waved for
        -- the first time
        forall i in 1..va_active_stores.count
            execute immediate v_aggr_group_sql using va_active_stores(i), p_facility_id, v_agg_ol_status ; 
        wm_cs_log('Order groups found in locked store collection ' || sql%rowcount, p_sql_log_level => 2);

        -- create any aggregated orders wherever the sequence above was used;
        -- use order values from any one single order in the group
        insert into orders
        (
            order_id, tc_order_id, is_original_order, o_facility_id, store_nbr,
            bill_to_facility_name, bill_facility_id, bill_to_address_1,
            bill_to_address_2, bill_to_address_3, bill_to_city, bill_to_state_prov,
            bill_to_county, bill_to_postal_code, bill_to_country_code, is_hazmat,
            is_perishable, movement_option, business_partner_id, customer_id,
            billing_method, bill_acct_nbr, do_type, shipment_id, plan_o_facility_id,
            plan_d_facility_id, tc_company_id, trans_resp_code,
            dsg_service_level_id, dsg_carrier_id, dsg_equipment_id,
            dsg_tractor_equipment_id, dsg_mot_id, order_status, creation_type,
            created_source, created_dttm, last_updated_dttm,
            last_updated_source, pickup_start_dttm, pickup_end_dttm,
            delivery_start_dttm, delivery_end_dttm, o_facility_alias_id,
            o_facility_name, lpn_cubing_indic, inbound_region_id,
            outbound_region_id, allow_pre_billing, prtl_ship_conf_flag,
            o_address_1, o_address_2, o_address_3, o_city, o_state_prov,
            o_postal_code, o_county, o_country_code, d_facility_id,
            d_facility_alias_id, d_facility_name, d_address_1, d_address_2,
            d_address_3, d_city, d_state_prov, d_postal_code, d_county,
            d_country_code, pre_pack_flag, d_name, d_contact, d_email, d_fax_number,
            d_phone_number, d_dock_door_id, dsg_static_route_id,
            dsg_hub_location_id, dsg_ship_via, o_dock_id, o_contact, o_phone_number,
            o_dock_door_id, o_fax_number, o_email, duty_tax_payment_type,
            lpn_label_type, pack_slip_type , tax_id, contnt_label_type, nbr_of_contnt_label,
            nbr_of_label, pallet_content_label_type,rte_wave_nbr,
            ship_group_id, rte_to, rte_type_1, rte_type_2, major_minor_order,
            acct_rcvbl_acct_nbr, acct_rcvbl_code, actual_cost,
            actual_cost_currency_code, actual_shipped_dttm, addr_code, addr_valid,
            advt_code, advt_date, aes_itn, assigned_carrier_id,
            assigned_equipment_id, assigned_mot_id, assigned_service_level_id,
            assigned_static_route_id, baseline_carrier_id, baseline_cost,
            baseline_cost_currency_code, baseline_mot_id, baseline_service_level_id,
            batch_id, bill_facility_alias_id, bill_of_lading_number,
            bill_to_contact_name, bill_to_email, bill_to_fax_number, bill_to_name,
            bill_to_phone_number, bill_to_title, block_auto_consolidate,
            block_auto_create, bol_break_attr, budg_cost, budg_cost_currency_code,
            cancel_dttm, chute_id, cod_amount,
            cod_currency_code, cod_funds, cod_return_company_name,commodity_code_id,
            compartment_no, cons_run_id, created_source_type, cubing_status,
            cust_broker_acct_nbr, dc_ctr_nbr, declared_value, delivery_options,
            delivery_req, delivery_tz, destination_action,
            dest_ship_thru_facility_id, dest_ship_thru_fac_alias_id,
            distribution_ship_via, docs_only_shpmt, driver_type_id, dropoff_pickup,
            duty_and_tax, duty_tax_acct_nbr, dv_currency_code, dynamic_request_sent,
            dynamic_routing_reqd, d_dock_id, equipment_type, ext_purchase_order,
            freight_class, freight_forwarder_acct_nbr, ftsr_nbr, global_locn_nbr,
            has_alerts, has_em_notify_flag, has_import_error, has_notes,
            has_soft_check_errors, has_split, hibernate_version, importer_defn,
            incoterm_facility_alias_id, incoterm_facility_id, incoterm_id,
            incoterm_loc_ava_dttm, incoterm_loc_ava_time_zone_id,
            intl_goods_desc, in_transit_allocation, is_back_ordered,
            is_booking_required, is_cancelled, is_customer_pickup,is_direct_allowed,
            is_imported, is_order_reconciled, is_partially_planned,
            is_suspended, lane_name, lang_id, last_run_id, last_updated_source_type,
            line_haul_ship_via, major_order_grp_attr, manifest_nbr, mark_for,
            merchandizing_department_id, mhe_flag, mhe_ord_state, monetary_value,
            move_type, must_release_by_dttm, mv_currency_code, non_machineable,
            normalized_baseline_cost, order_consol_locn_id, order_consol_profile,
            order_date_dttm, order_loading_seq, order_print_dttm, order_received,
            order_recon_dttm, order_type, original_assigned_ship_via,
            origin_ship_thru_facility_id, origin_ship_thru_fac_alias_id,
            orig_budg_cost, override_billing_method, packaging, pack_slip_prt_cnt,
            pack_wave_nbr, pallet_cubing_indic, parent_order_id, parent_type,
            partial_lpn_option, parties_related, path_id, path_set_id, pickup_tz,
            plan_due_dttm, plan_d_facility_alias_id, plan_leg_d_alias_id,
            plan_leg_o_alias_id, plan_o_facility_alias_id, pnh_flag,
            pre_sticker_code, primary_maxi_addr_nbr, priority, product_class_id,
            prod_sched_ref_number, protection_level_id, prtl_ship_conf_status,
            purchase_order_id, purchase_order_number, ref_field_1, ref_field_2,
            ref_field_3, ref_field_4, ref_field_5, ref_field_6, ref_field_7, ref_field_8,
            ref_field_9, ref_field_10, ref_num1, ref_num2, ref_num3, ref_num4, ref_num5, 
            release_destination, repl_wave_nbr, rte_attr, rte_swc_nbr,
            sched_delivery_dttm, sched_dow, sched_pickup_dttm,
            secondary_maxi_addr_nbr, ship_group_sequence, shpng_chrg, stage_indic,
            template_id, trans_plan_owner, un_number_id, wave_id, wave_option_id,
            weight_uom_id_base, zone_skip_hub_location_id, tc_shipment_id, 
            ref_shipment_nbr, ref_stop_seq, nbr_of_pakng_slips, apply_lpntype_for_order, is_guaranteed_delivery, 
            po_type_attr, epi_service_group, importer_of_record_nbr,
            b13a_export_decl_nbr, return_addr_code, channel_type, bol_type, direction,
            dsg_voyage_flight,effective_rank, freight_revenue, freight_revenue_currency_code,
            haz_offeror_name, is_d_pobox, is_routed, major_order_ctrl_nbr,
            manif_type, movement_type, order_shipment_seq, picklist_id, shipping_channel,
            tc_order_id_u, trans_plan_direction, upsmi_cost_center
        )
        select t1.parent_order_id order_id, to_char(t1.parent_order_id) tc_order_id,
            0 is_original_order, p_facility_id o_facility_id, o.store_nbr,
            o.bill_to_facility_name, o.bill_facility_id, o.bill_to_address_1,
            o.bill_to_address_2, o.bill_to_address_3, o.bill_to_city,
            o.bill_to_state_prov, o.bill_to_county, o.bill_to_postal_code,
            o.bill_to_country_code, o.is_hazmat, o.is_perishable, o.movement_option,
            o.business_partner_id, o.customer_id, o.billing_method, o.bill_acct_nbr,
            o.do_type, o.shipment_id, o.plan_o_facility_id, o.plan_d_facility_id,
            o.tc_company_id, o.trans_resp_code, o.dsg_service_level_id,
            o.dsg_carrier_id, o.dsg_equipment_id, o.dsg_tractor_equipment_id,
            o.dsg_mot_id, 5, o.creation_type, p_user_id created_source,
            sysdate created_dttm, sysdate last_updated_dttm,
            p_user_id last_updated_source, o.pickup_start_dttm, o.pickup_end_dttm,
            o.delivery_start_dttm, o.delivery_end_dttm, o.o_facility_alias_id,
            o.o_facility_name, o.lpn_cubing_indic, o.inbound_region_id,
            o.outbound_region_id, allow_pre_billing, 1 prtl_ship_conf_flag,
            o.o_address_1, o.o_address_2, o.o_address_3,
            o.o_city, o.o_state_prov, o.o_postal_code, o.o_county, o.o_country_code,
            o.d_facility_id, o.d_facility_alias_id, o.d_facility_name,
            o.d_address_1, o.d_address_2, o.d_address_3, o.d_city, o.d_state_prov,
            o.d_postal_code, o.d_county, o.d_country_code, o.pre_pack_flag,
            o.d_name, o.d_contact, o.d_email, o.d_fax_number, o.d_phone_number,
            o.d_dock_door_id, o.dsg_static_route_id,
            o.dsg_hub_location_id, o.dsg_ship_via, o.o_dock_id, o.o_contact,
            o.o_phone_number, o.o_dock_door_id, o.o_fax_number, o.o_email,
            o.duty_tax_payment_type, o.lpn_label_type, o.pack_slip_type, o.tax_id,
            o.contnt_label_type, o.nbr_of_contnt_label, o.nbr_of_label, 
            o.pallet_content_label_type, p_rte_wave_nbr, o.ship_group_id,
            o.rte_to, o.rte_type_1, o.rte_type_2,
            null,
            o.acct_rcvbl_acct_nbr, o.acct_rcvbl_code, o.actual_cost,
            o.actual_cost_currency_code, o.actual_shipped_dttm, o.addr_code, o.addr_valid,
            o.advt_code, o.advt_date, o.aes_itn, o.assigned_carrier_id,
            o.assigned_equipment_id, o.assigned_mot_id, o.assigned_service_level_id,
            o.assigned_static_route_id, o.baseline_carrier_id, o.baseline_cost,
            o.baseline_cost_currency_code, o.baseline_mot_id, o.baseline_service_level_id,
            o.batch_id, o.bill_facility_alias_id, o.bill_of_lading_number,
            o.bill_to_contact_name, o.bill_to_email, o.bill_to_fax_number, o.bill_to_name,
            o.bill_to_phone_number, o.bill_to_title, o.block_auto_consolidate,
            o.block_auto_create, o.bol_break_attr, o.budg_cost, o.budg_cost_currency_code,
            o.cancel_dttm, o.chute_id, o.cod_amount,
            o.cod_currency_code, o.cod_funds, o.cod_return_company_name,commodity_code_id,
            o.compartment_no, o.cons_run_id, o.created_source_type, o.cubing_status,
            o.cust_broker_acct_nbr, o.dc_ctr_nbr, o.declared_value, o.delivery_options,
            o.delivery_req, o.delivery_tz, o.destination_action,
            o.dest_ship_thru_facility_id, o.dest_ship_thru_fac_alias_id,
            o.distribution_ship_via, o.docs_only_shpmt, o.driver_type_id, o.dropoff_pickup,
            o.duty_and_tax, o.duty_tax_acct_nbr, o.dv_currency_code, o.dynamic_request_sent,
            o.dynamic_routing_reqd, o.d_dock_id, o.equipment_type, o.ext_purchase_order,
            o.freight_class, o.freight_forwarder_acct_nbr, o.ftsr_nbr, o.global_locn_nbr,
            o.has_alerts, o.has_em_notify_flag, o.has_import_error, o.has_notes,
            o.has_soft_check_errors, o.has_split, o.hibernate_version, o.importer_defn,
            o.incoterm_facility_alias_id, o.incoterm_facility_id, o.incoterm_id,
            o.incoterm_loc_ava_dttm, o.incoterm_loc_ava_time_zone_id,
            o.intl_goods_desc, o.in_transit_allocation, o.is_back_ordered,
            o.is_booking_required, o.is_cancelled, o.is_customer_pickup,is_direct_allowed,
            o.is_imported, o.is_order_reconciled, o.is_partially_planned,
            o.is_suspended, o.lane_name, o.lang_id, o.last_run_id, o.last_updated_source_type,
            o.line_haul_ship_via, o.major_order_grp_attr, o.manifest_nbr, o.mark_for,
            o.merchandizing_department_id, o.mhe_flag, o.mhe_ord_state, o.monetary_value,
            o.move_type, o.must_release_by_dttm, o.mv_currency_code, o.non_machineable,
            o.normalized_baseline_cost, o.order_consol_locn_id, o.order_consol_profile,
            o.order_date_dttm, o.order_loading_seq, o.order_print_dttm, o.order_received,
            o.order_recon_dttm, o.order_type, o.original_assigned_ship_via,
            o.origin_ship_thru_facility_id, o.origin_ship_thru_fac_alias_id,
            o.orig_budg_cost, o.override_billing_method, o.packaging, o.pack_slip_prt_cnt,
            o.pack_wave_nbr, o.pallet_cubing_indic, o.parent_order_id, o.parent_type,
            o.partial_lpn_option, o.parties_related, o.path_id, o.path_set_id, o.pickup_tz,
            o.plan_due_dttm, o.plan_d_facility_alias_id, o.plan_leg_d_alias_id,
            o.plan_leg_o_alias_id, o.plan_o_facility_alias_id, o.pnh_flag,
            o.pre_sticker_code, o.primary_maxi_addr_nbr, o.priority, o.product_class_id,
            o.prod_sched_ref_number, o.protection_level_id, o.prtl_ship_conf_status,
            o.purchase_order_id, o.purchase_order_number, o.ref_field_1, o.ref_field_2,
            o.ref_field_3, o.ref_field_4, o.ref_field_5, o.ref_field_6, o.ref_field_7, o.ref_field_8,
            o.ref_field_9, o.ref_field_10, o.ref_num1, o.ref_num2, o.ref_num3, o.ref_num4, o.ref_num5, 
            o.release_destination, o.repl_wave_nbr, o.rte_attr, o.rte_swc_nbr,
            o.sched_delivery_dttm, o.sched_dow, o.sched_pickup_dttm,
            o.secondary_maxi_addr_nbr, o.ship_group_sequence, o.shpng_chrg, o.stage_indic,
            o.template_id, o.trans_plan_owner, o.un_number_id, o.wave_id, o.wave_option_id,
            o.weight_uom_id_base, o.zone_skip_hub_location_id, o.tc_shipment_id,
            o.ref_shipment_nbr, o.ref_stop_seq, o.nbr_of_pakng_slips, o.apply_lpntype_for_order, 
            o.is_guaranteed_delivery, o.po_type_attr, o.epi_service_group, o.importer_of_record_nbr,
            o.b13a_export_decl_nbr, o.return_addr_code, o.channel_type, o.bol_type, o.direction,
            o.dsg_voyage_flight,o.effective_rank, o.freight_revenue, o.freight_revenue_currency_code,
            o.haz_offeror_name, o.is_d_pobox, o.is_routed, o.major_order_ctrl_nbr,
            o.manif_type, o.movement_type, o.order_shipment_seq, o.picklist_id, o.shipping_channel,
            o.tc_order_id_u, o.trans_plan_direction, o.upsmi_cost_center
        from tmp_aggregation_order_groups t1
        join orders o on o.order_id = t1.order_id
        where o.o_facility_id = p_facility_id
            and not exists
            (
                select 1
                from orders o1
                where o1.order_id = t1.parent_order_id
            );
        wm_cs_log('New Agg orders created ' || sql%rowcount, p_sql_log_level => 2);

        -- point original orders to agg orders
            merge into orders orig
            using
            (
                select t1.parent_order_id, t2.order_id
                from tmp_aggregation_order_groups t1
                join tmp_aggregation_orders t2
                    on t2.order_group_num = t1.order_group_num
                where t2.parent_order_id is null
            ) iv on (iv.order_id = orig.order_id)
            when matched then
            update set orig.parent_order_id = iv.parent_order_id;
            wm_cs_log('New Orig orders aggregated ' || sql%rowcount, p_sql_log_level => 2);

        -- agg status updates are necessary since we may add lines from new
        -- orders to an existing agg order not created in this wave
            merge into orders agg
            using
            (
                select orig.parent_order_id, sum(orig.est_lpn) sum_est_lpn,
                    sum(orig.est_pallet_bridged) sum_est_plt_br,
                    sum(orig.est_lpn_bridged) sum_est_lpn_br,
                    sum(orig.est_pallet) sum_est_plt, min(orig.do_status) min_do_status,
                    min(orig.pickup_start_dttm) pickup_start_dttm,
                    max(orig.pickup_end_dttm) pickup_end_dttm,
                    min(orig.delivery_start_dttm) delv_start_dttm,
                    max(orig.delivery_end_dttm) delv_end_dttm,
                    sum(orig.total_nbr_of_units) total_nbr_of_units
                from orders orig
                join orders agg on agg.order_id = orig.parent_order_id and agg.do_status < 150
                where orig.do_status < 190
                    and exists
                    (
                        select 1
                        from tmp_aggregation_order_groups t1
                        where t1.parent_order_id = orig.parent_order_id
                    )
                group by orig.parent_order_id
            ) iv on (iv.parent_order_id = agg.order_id)
            when matched then
            update set
                agg.est_lpn = iv.sum_est_lpn, agg.est_pallet = iv.sum_est_plt,
                agg.est_pallet_bridged = iv.sum_est_plt_br, agg.est_lpn_bridged = iv.sum_est_lpn_br,
                agg.total_nbr_of_units = iv.total_nbr_of_units,
                agg.do_status =
                (
                    case 
                    when agg.do_status > 130 and iv.min_do_status = 130 then 140
                    when iv.min_do_status = 150 and exists
                    (
                        select 1 
                        from lpn l 
                        where l.order_id = agg.order_id and l.lpn_facility_status < 20
                    ) then 140 
                    else iv.min_do_status
                    end
                ),
                agg.pickup_start_dttm = iv.pickup_start_dttm,
                agg.pickup_end_dttm = iv.pickup_end_dttm,
                agg.delivery_start_dttm = iv.delv_start_dttm,
                agg.delivery_end_dttm = iv.delv_end_dttm,
                agg.rte_wave_nbr = 
                    (case when agg.order_status = 5 then p_rte_wave_nbr 
                        else agg.rte_wave_nbr end);
            wm_cs_log('Status updated on Agg orders ' || sql%rowcount, p_sql_log_level => 2);

            -- we're done with these stores on the shpmt
            v_num_stores_to_process := v_num_stores_to_process - va_active_stores.count;
            forall i in 1..va_active_stores.count
            delete from tmp_store_master t
            where t.d_facility_id = va_active_stores(i);
            va_active_stores.delete;
            commit;
        end if;

        if (v_num_stores_to_process <= 0)
        then
            exit;
        end if;
    end loop;
end;
/

show errors;
create or replace procedure wm_val_create_job_hdr
(
    p_job_name          in val_job_hdr.job_name%type,
    p_job_desc          in val_job_hdr.job_desc%type,
    p_vjh_id            out number
)
as
begin
    delete from val_job_hdr vjh
    where vjh.job_name = p_job_name;

    -- create a scenario as a 'job' and link pertinent sqls to it as job dtls
    p_vjh_id := val_job_hdr_id_seq.nextval;
    insert into val_job_hdr
    (
        val_job_hdr_id, job_name, job_desc, create_date_time, mod_date_time
    )
    values
    (
        p_vjh_id, p_job_name, p_job_desc, sysdate, sysdate
    );
end;
/
show errors;

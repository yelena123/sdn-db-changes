create or replace procedure manh_task_get_016_msg_sql

(
    p_user_id         in user_profile.user_id%type,
    p_rule_id         in rule_hdr.rule_id%type,
    p_rule_type       in rule_hdr.rule_type%type,
    p_els_actvty_code in labor_activity.name%type,
    p_crit_type       in event_rule_parm.crit_type%type,
    p_crit_value      in event_rule_parm.crit_value%type,
    p_tbl_name        in event_rule_parm.tbl_name%type, 
    p_colm_name       in event_rule_parm.colm_name%type,
    p_facility_id     in facility.facility_id%type,
    p_msg_insert_sql  out varchar2
)
as
    v_rule_sel_dtl_count     number(4) := 0;
    v_select_clause          varchar2(10000) := ' ';
    v_task_hdr_search_only   number(1) := 1;
    v_whse                   facility.whse%type;  
begin
    -- if a table other than th exists for which we need to join, then all
    -- td's are included in the input list to generate app criteria messages
    if (coalesce(p_tbl_name, 'TASK_HDR') != 'TASK_HDR')
    then
        v_task_hdr_search_only := 0;
    end if;
    
    select f.whse
    into v_whse
    from facility f
    where f.facility_id = p_facility_id;

    select count(*)
    into v_rule_sel_dtl_count
    from rule_sel_dtl
    where rule_id = p_rule_id;

    if (v_rule_sel_dtl_count > 0)
    then
        -- only LM rules are allowed to not have a rule_sel_dtl
        v_select_clause := manh_get_rule(p_rule_id, 0, p_rule_type);

        select case when exists
        (
            select 1
            from rule_sel_dtl rsd
            where rsd.rule_id = p_rule_id and rsd.tbl_name != 'TASK_HDR'
        ) then 0 else 1 end
        into v_task_hdr_search_only
        from dual;
    end if;

    -- generate messages for matching task hdr's/dtl's containing this 
    -- particular rule_type/els_actvty_code/hook type; if rules are defined
    -- such that there are multiple results for each hdr/dtl, then as many rows
    -- will be inserted
    p_msg_insert_sql
        := ' insert into labor_tran_crit'
        || ' ('
        || '    labor_tran_crit_id, tran_nbr, whse, created_dttm,'
        || '    last_updated_dttm, created_source, last_updated_source,'
        || '    labor_tran_id, crit_type, crit_val, crit_seq_nbr'
        || ' )'
        || ' select labor_tran_crit_id_seq.nextval, t.tran_nbr, :whse,'
        || '    sysdate, sysdate, ''' || p_user_id || ''', ''' || p_user_id
        || '    '', lt.labor_tran_id, ''' || p_crit_type || ''' crit_type, '
        || (case when p_crit_value is not null then '''' || p_crit_value ||''''
               else p_tbl_name || '.' || p_colm_name end) || ' crit_value,'
        || '     (row_number() over(partition by t.tran_nbr order by lt.labor_tran_id) '
        || '        + coalesce(t1.max_crit_seq_nbr, 0)) crit_seq_nbr'
        || ' from tmp_task_creation_lm_task_dtls t'
        || ' join labor_tran lt on lt.tran_nbr = t.tran_nbr'
        || ' left join'
        || ' ('
        || '     select t2.tran_nbr, max(crit_seq_nbr) max_crit_seq_nbr'
        || '     from labor_tran_crit ltc'
        || '     join tmp_task_creation_lm_task_dtls t2 '
        || '         on t2.tran_nbr = ltc.tran_nbr'
        || '     inner_clause t2.els_actvty_code = '
        || '         ''' || p_els_actvty_code || ''''
        || '     group by t2.tran_nbr'
        || ' ) t1 on t1.tran_nbr = t.tran_nbr'
        || ' where t.els_actvty_code = ''' || p_els_actvty_code || ''''
        || (case when v_rule_sel_dtl_count > 0 
                then ' and ' || v_select_clause else ' ' end)
        || (case when p_crit_value is null
                then ' and ' || p_tbl_name || '.'||p_colm_name || ' is not null'
                else ' ' end)
        || (case when v_task_hdr_search_only = 1 then ' and t.task_seq_nbr = 1' 
                else ' ' end);

    manh_add_joins_for_lm_rule(p_facility_id, v_whse, p_msg_insert_sql);

    p_msg_insert_sql := replace(p_msg_insert_sql, 'inner_clause', 'where');
end;
/

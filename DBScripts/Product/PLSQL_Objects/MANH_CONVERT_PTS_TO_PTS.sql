CREATE OR REPLACE PROCEDURE MANH_CONVERT_PTS_TO_PTS (
   p_tc_lpn_id             IN     LPN.TC_LPN_ID%TYPE,
   p_tc_company_id         IN     DISPOSITION_TYPE.TC_COMPANY_ID%TYPE,
   p_login_user_id         IN     VARCHAR2,
   p_disposition_type      IN     DISPOSITION_TYPE.DISPOSITION_TYPE%TYPE,
   p_next_up_number_ilpn   IN      varchar2,
   p_reuse_ilpn_as_pallet  IN      NUMBER,
   p_debug_flag            IN     NUMBER,
   p_whse                  IN     DISPOSITION_TYPE.WHSE%TYPE,
   p_exception_disp_type   IN     VARCHAR2
)
IS
   stmt                          VARCHAR2 (1000);
   v_error_details               VARCHAR2 (2000);
   v_lpn_count                   NUMBER;
   v_remaining_qty               NUMBER;
   v_allocated_qty               NUMBER;
   v_ilpn_id                     LPN.LPN_ID%TYPE;
   v_ilpn_curr_sub_locn_id       LPN.CURR_SUB_LOCN_ID%TYPE;
   v_ilpn_dest_sub_locn_id       LPN.DEST_SUB_LOCN_ID%TYPE;
   v_olpn_id                     LPN.LPN_ID%TYPE;
   v_lpn_detail_id               LPN_DETAIL.LPN_DETAIL_ID%TYPE;
   v_olpn_detail_id              LPN_DETAIL.LPN_DETAIL_ID%TYPE;
   v_tc_ilpn_id                  LPN.TC_LPN_ID%TYPE;
   v_tc_olpn_id                  LPN.TC_LPN_ID%TYPE;
   v_item_id                     LPN_DETAIL.ITEM_ID%TYPE;
   v_uom_qty                     ITEM_PACKAGE_CBO.QUANTITY%TYPE;
   v_tc_order_id                 ORDERS.TC_ORDER_ID%TYPE;
   v_order_id                    ORDERS.ORDER_ID%TYPE;
   v_line_item_id                ORDER_LINE_ITEM.LINE_ITEM_ID%TYPE;
   v_allow_shippable_inners      WHSE_PARAMETERS.ALLOW_SHIPPABLE_INNERS%TYPE;
   v_process_shippable_inners    DISPOSITION_TYPE.PROCESS_SHIPPABLE_INNERS%TYPE;
   v_handle_unalloc_lpns         DISPOSITION_TYPE.HANDLING_UNALLOC_LPN%TYPE;
   v_break_lpn                   DISPOSITION_TYPE.BREAK_LPN_FLAG%TYPE;
   v_invn_need_type              NUMBER;
   v_alloc_uom                   SIZE_UOM.SIZe_UOM%TYPE;
   v_wm_inventory_qty            NUMBER;
   v_reference_lpn_id            LPN.TC_REFERENCE_LPN_ID%TYPE;
   v_lpn_facility_status         LPN.LPN_FACILITY_STATUS%TYPE;
   v_total_unalloc_qty           ALLOC_INVN_DTL.QTY_ALLOC%TYPE;
   v_total_size_value            LPN_DETAIL.SIZE_VALUE%TYPE;
   v_tc_asn_id                   ASN.TC_ASN_ID%TYPE;
   --[MACR00844007]
   v_child_lpn_size_value  LPN_DETAIL.SIZE_VALUE%TYPE;
   v_facility_id                 facility.facility_id%type;
   v_prev_module_name            wm_utils.t_app_context_data;
   v_prev_action_name            wm_utils.t_app_context_data;	
   
   ------------variables for allocation cursor---------------
   v_lpn_rec_sec_ITEM_ID         LPN_DETAIL.ITEM_ID%TYPE;
   v_lpn_rec_sec_SKU_ATTR_1      ALLOC_INVN_DTL.SKU_ATTR_1%TYPE;
   v_lpn_rec_sec_SKU_ATTR_2      ALLOC_INVN_DTL.SKU_ATTR_2%TYPE;
   v_lpn_rec_sec_SKU_ATTR_3      ALLOC_INVN_DTL.SKU_ATTR_3%TYPE;
   v_lpn_rec_sec_SKU_ATTR_4      ALLOC_INVN_DTL.SKU_ATTR_4%TYPE;
   v_lpn_rec_sec_SKU_ATTR_5      ALLOC_INVN_DTL.SKU_ATTR_5%TYPE;
   v_lpn_rec_sec_INVN_TYPE       ALLOC_INVN_DTL.INVN_TYPE%TYPE;
   v_lpn_rec_sec_BATCH_NBR       ALLOC_INVN_DTL.BATCH_NBR%TYPE;
   v_lpn_rec_sec_PROD_STAT       ALLOC_INVN_DTL.PROD_STAT%TYPE;
   v_lpn_rec_sec_CNTRY_OF_ORGN   ALLOC_INVN_DTL.CNTRY_OF_ORGN%TYPE;

   ------------variables for allocation cursor---------------

   --Cursor for lpn records created in INT 60 from c++
   CURSOR CURSOR_LPN_DETAIL
   IS
      SELECT /*+ PARALLEL */
             ---------------------------------LPN RECORDS----------------------------------------
             L.TC_LPN_ID AS TC_LPN_ID,
             L.LPN_ID AS LPN_ID,
             L.TC_COMPANY_ID,
             L.LPN_MONETARY_VALUE,
             L.C_FACILITY_ID,
             L.O_FACILITY_ID,
             L.O_FACILITY_ALIAS_ID,
             L.MANIFEST_NBR,
             L.SHIP_VIA,
             L.MASTER_BOL_NBR,
             L.BOL_NBR,
             L.INIT_SHIP_VIA,
             L.PATH_ID,
             L.REPRINT_COUNT,
             L.FREIGHT_CHARGE,
             L.LENGTH,
             L.WIDTH,
             L.HEIGHT,
             L.TC_SHIPMENT_ID,
             L.TOTAL_LPN_QTY,
             L.ESTIMATED_VOLUME,
             L.WEIGHT,
             L.ACTUAL_VOLUME,
             L.LPN_LABEL_TYPE,
             L.HIBERNATE_VERSION,
             L.CREATED_SOURCE,
             L.C_FACILITY_ALIAS_ID,
             L.PICK_SUB_LOCN_ID,
             L.INTERNAL_ORDER_ID,
             L.TRAILER_STOP_SEQ_NBR,
             L.WAVE_NBR,
             L.WAVE_SEQ_NBR,
             L.WAVE_STAT_CODE,
             L.STAGE_INDICATOR,
             L.PICK_DELIVERY_DURATION,
             L.LPN_BREAK_ATTR,
             L.SEQ_RULE_PRIORITY,
             L.NBR_OF_ZONES,
             L.PALLET_X_OF_Y,
             L.LPN_NBR_X_OF_Y,
             L.LOAD_SEQUENCE,
             L.SELECTION_RULE_ID,
             L.SINGLE_LINE_LPN,
             L.LPN_FACILITY_STAT_UPDATED_DTTM,
             L.ESTIMATED_WEIGHT,
             L.D_FACILITY_ID,
             L.D_FACILITY_ALIAS_ID,
             L.CUBE_UOM,
             L.QTY_UOM_ID_BASE,
             L.WEIGHT_UOM_ID_BASE,
             L.VOLUME_UOM_ID_BASE,
             ---------------------------------LPN_DETAIL RECORDS----------------------------------
             LD.LPN_DETAIL_ID AS LPN_DETAIL_ID,
             LD.ITEM_ID AS ITEM_ID,
             LD.BUSINESS_PARTNER_ID,
             LD.GTIN,
             LD.INCUBATION_DATE,
             LD.EXPIRATION_DATE,
             LD.SHIP_BY_DATE,
             LD.SELL_BY_DTTM,
             LD.CONSUMPTION_PRIORITY_DTTM,
             LD.MANUFACTURED_DTTM,
             LD.CNTRY_OF_ORGN,
             LD.INVENTORY_TYPE,
             LD.PRODUCT_STATUS,
             LD.ITEM_ATTR_1,
             LD.ITEM_ATTR_2,
             LD.ITEM_ATTR_3,
             LD.ITEM_ATTR_4,
             LD.ITEM_ATTR_5,
             LD.ASN_DTL_ID,
             LD.ASSORT_NBR,
             LD.CUT_NBR,
             LD.PURCHASE_ORDERS_ID,
             LD.TC_PURCHASE_ORDERS_ID,
             LD.PURCHASE_ORDERS_LINE_ID,
             LD.TC_PURCHASE_ORDERS_LINE_ID,
             LD.INSTRTN_CODE_1,
             LD.INSTRTN_CODE_2,
             LD.INSTRTN_CODE_3,
             LD.INSTRTN_CODE_4,
             LD.INSTRTN_CODE_5,
             LD.VENDOR_ITEM_NBR,
             LD.MANUFACTURED_PLANT,
             LD.BATCH_NBR,
             LD.ASSIGNED_QTY,
             LD.PREPACK_GROUP_CODE,
             LD.PACK_CODE,
             LD.STD_PACK_QTY,
             LD.STD_SUB_PACK_QTY,
             LD.STD_BUNDLE_QTY,
             LD.VOLUME_UOM_ID,
             LD.WEIGHT_UOM_ID,
             LD.QTY_UOM_ID,
             WM_INVENTORY_ID,
             WM.TRANSITIONAL_INVENTORY_TYPE,
             WM.LAST_UPDATED_SOURCE_TYPE,
             WM.TO_BE_FILLED_QTY,
             WM.WM_VERSION_ID,
             WM.CREATED_SOURCE_TYPE,
             WM.TO_BE_CONSOLIDATED_QTY,
             WM.LOCATION_DTL_ID,
             WM.PACK_QTY,
             WM.SUB_PACK_QTY,
             WM.LOCN_CLASS,
             WM.ON_HAND_QTY
        FROM LPN_DETAIL LD
             INNER JOIN LPN L
                ON     LD.LPN_ID = L.LPN_ID
                   AND L.TC_LPN_ID = p_tc_lpn_id
                   AND LD.TC_COMPANY_ID = p_tc_company_id
                   and l.c_facility_id = v_facility_id
                   and l.inbound_outbound_indicator = 'I'
             INNER JOIN WM_INVENTORY WM
                ON     WM.LPN_ID = LD.LPN_ID
                   AND WM.LPN_DETAIL_ID = LD.LPN_DETAIL_ID
                   AND WM.TC_COMPANY_ID = LD.TC_COMPANY_ID
                   AND WM.MARKED_FOR_DELETE = 'N';

   --cursor for child lpns created after breaking
   CURSOR CURSOR_CHILD_LPNS
   IS
        SELECT /*+ PARALLEL */
               ---------------------------------LPN RECORDS----------------------------------------
               L.TC_LPN_ID AS TC_LPN_ID,
               L.LPN_ID AS LPN_ID,
               L.TC_COMPANY_ID,
               L.LPN_MONETARY_VALUE,
               L.C_FACILITY_ID,
               L.O_FACILITY_ID,
               L.O_FACILITY_ALIAS_ID,
               L.MANIFEST_NBR,
               L.SHIP_VIA,
               L.MASTER_BOL_NBR,
               L.BOL_NBR,
               L.INIT_SHIP_VIA,
               L.PATH_ID,
               L.REPRINT_COUNT,
               L.FREIGHT_CHARGE,
               L.LENGTH,
               L.WIDTH,
               L.HEIGHT,
               L.TC_SHIPMENT_ID,
               L.TOTAL_LPN_QTY,
               L.ESTIMATED_VOLUME,
               L.WEIGHT,
               L.ACTUAL_VOLUME,
               L.LPN_LABEL_TYPE,
               L.HIBERNATE_VERSION,
               L.CREATED_SOURCE,
               L.C_FACILITY_ALIAS_ID,
               L.PICK_SUB_LOCN_ID,
               L.INTERNAL_ORDER_ID,
               L.TRAILER_STOP_SEQ_NBR,
               L.WAVE_NBR,
               L.WAVE_SEQ_NBR,
               L.WAVE_STAT_CODE,
               L.STAGE_INDICATOR,
               L.PICK_DELIVERY_DURATION,
               L.LPN_BREAK_ATTR,
               L.SEQ_RULE_PRIORITY,
               L.NBR_OF_ZONES,
               L.PALLET_X_OF_Y,
               L.LPN_NBR_X_OF_Y,
               L.LOAD_SEQUENCE,
               L.SELECTION_RULE_ID,
               L.SINGLE_LINE_LPN,
               L.LPN_FACILITY_STAT_UPDATED_DTTM,
               L.ESTIMATED_WEIGHT,
               L.D_FACILITY_ID,
               L.D_FACILITY_ALIAS_ID,
               L.CUBE_UOM,
               L.QTY_UOM_ID_BASE,
               L.WEIGHT_UOM_ID_BASE,
               L.VOLUME_UOM_ID_BASE,
               ---------------------------------LPN_DETAIL RECORDS----------------------------------
               LD.LPN_DETAIL_ID AS LPN_DETAIL_ID,
               LD.ITEM_ID AS ITEM_ID,
               LD.BUSINESS_PARTNER_ID,
               LD.GTIN,
               LD.INCUBATION_DATE,
               LD.EXPIRATION_DATE,
               LD.SHIP_BY_DATE,
               LD.SELL_BY_DTTM,
               LD.CONSUMPTION_PRIORITY_DTTM,
               LD.MANUFACTURED_DTTM,
               LD.CNTRY_OF_ORGN,
               LD.PRODUCT_STATUS,
               LD.ITEM_ATTR_1,
               LD.ITEM_ATTR_2,
               LD.ITEM_ATTR_3,
               LD.ITEM_ATTR_4,
               LD.ITEM_ATTR_5,
               LD.ASN_DTL_ID,
               LD.ASSORT_NBR,
               LD.CUT_NBR,
               LD.PURCHASE_ORDERS_ID,
               LD.TC_PURCHASE_ORDERS_ID,
               LD.PURCHASE_ORDERS_LINE_ID,
               LD.TC_PURCHASE_ORDERS_LINE_ID,
               LD.INSTRTN_CODE_1,
               LD.INSTRTN_CODE_2,
               LD.INSTRTN_CODE_3,
               LD.INSTRTN_CODE_4,
               LD.INSTRTN_CODE_5,
               LD.VENDOR_ITEM_NBR,
               LD.MANUFACTURED_PLANT,
               LD.BATCH_NBR,
               LD.ASSIGNED_QTY,
               LD.PREPACK_GROUP_CODE,
               LD.PACK_CODE,
               LD.STD_PACK_QTY,
               LD.STD_SUB_PACK_QTY,
               LD.STD_BUNDLE_QTY,
               LD.SIZE_VALUE,
               LD.VOLUME_UOM_ID,
               LD.WEIGHT_UOM_ID,
               LD.QTY_UOM_ID,
               WM.WM_INVENTORY_ID,
               AD.STD_PACK_QTY AS AD_STD_PACK_QTY,
               AD.STD_SUB_PACK_QTY AS AD_STD_SUB_PACK_QTY,
               AD.STD_CASE_QTY
          FROM LPN_DETAIL LD
               INNER JOIN LPN L
                  ON     LD.LPN_ID = L.LPN_ID
                     AND L.TC_REFERENCE_LPN_ID = v_reference_lpn_id
                     AND L.INBOUND_OUTBOUND_INDICATOR = 'I'
                     AND L.LPN_FACILITY_STATUS < 50
                     AND L.TC_COMPANY_ID = p_tc_company_id
                     and l.c_facility_id = v_facility_id
               INNER JOIN WM_INVENTORY WM
                  ON     WM.LPN_ID = LD.LPN_ID
                     AND WM.LPN_DETAIL_ID = LD.LPN_DETAIL_ID
                     AND WM.ITEM_ID = v_lpn_rec_sec_ITEM_ID
                     AND WM.TC_COMPANY_ID = LD.TC_COMPANY_ID
                     AND WM.MARKED_FOR_DELETE = 'N'
                LEFT OUTER JOIN ASN A
              ON     A.TC_ASN_ID = L.TC_ASN_ID                     
                 AND A.TC_COMPANY_ID = LD.TC_COMPANY_ID
            LEFT OUTER JOIN ASN_DETAIL AD
              ON     AD.ASN_DETAIL_ID = LD.ASN_DTL_ID 
                AND AD.ASN_ID = A.ASN_ID 
                AND AD.SKU_ID = LD.ITEM_ID                
                AND AD.TC_COMPANY_ID = LD.TC_COMPANY_ID
      --ORDER BY MOD (LD.SIZE_VALUE, v_uom_qty), L.LPN_ID;
        order by L.LPN_ID;

   --cursor for allocations
   CURSOR CURSOR_ALLOCATIONS
   IS
        SELECT                                                 /*+ PARALLEL */
              AID.ALLOC_INVN_DTL_ID,
               AID.ITEM_ID,
               AID.TC_ORDER_ID,
               AID.PULL_LOCN_ID,
               AID.DEST_LOCN_ID,
               AID.CNTR_NBR,
               AID.INVN_NEED_TYPE,
               AID.QTY_ALLOC,
               AID.QTY_PULLD,
               AID.ALLOC_INVN_CODE,
               AID.TASK_PRTY,
               AID.TASK_TYPE,
               AID.TASK_BATCH,            
               AID.ALLOC_UOM,
               AID.ALLOC_UOM_QTY,
               AID.ORIG_REQMT,
               AID.TASK_CMPL_REF_CODE,
               AID.TASK_CMPL_REF_NBR,
               AID.TASK_GENRTN_REF_CODE,
               AID.TASK_GENRTN_REF_NBR,
               AID.BATCH_NBR,
               AID.TRANS_INVN_TYPE,
               AID.FULL_CNTR_ALLOCD,
               AID.NEED_ID,
               AID.REQD_BATCH_NBR,
               AID.CARTON_SEQ_NBR,
               AID.SUBSTITUTION_FLAG,
               AID.PIKR_NBR,
               AID.PULL_LOCN_SEQ_NBR,
               AID.DEST_LOCN_SEQ_NBR,
               AID.STAT_CODE,
               PD.PKT_DTL_ID,
               PD.PICK_LOCN_ID,
               PD.PKT_CTRL_NBR,
               PD.PKT_SEQ_NBR,
               PD.REFERENCE_ORDER_ID AS ORDER_ID,
               PD.REFERENCE_LINE_ITEM_ID AS LINE_ITEM_ID
          FROM ALLOC_INVN_DTL AID, PKT_DTL PD
         WHERE     AID.WHSE = p_whse
               AND AID.CNTR_NBR = p_tc_lpn_id
               AND AID.INVN_NEED_TYPE = 60
               AND AID.STAT_CODE < 90
               AND AID.ITEM_ID = v_lpn_rec_sec_ITEM_ID
               AND AID.cd_master_id = p_tc_company_id
               AND COALESCE (AID.SKU_ATTR_1, '*') =
                      COALESCE (v_lpn_rec_sec_SKU_ATTR_1, '*')
               AND COALESCE (AID.SKU_ATTR_2, '*') =
                      COALESCE (v_lpn_rec_sec_SKU_ATTR_2, '*')
               AND COALESCE (AID.SKU_ATTR_3, '*') =
                      COALESCE (v_lpn_rec_sec_SKU_ATTR_3, '*')
               AND COALESCE (AID.SKU_ATTR_4, '*') =
                      COALESCE (v_lpn_rec_sec_SKU_ATTR_4, '*')
               AND COALESCE (AID.SKU_ATTR_5, '*') =
                      COALESCE (v_lpn_rec_sec_SKU_ATTR_5, '*')
               AND COALESCE (AID.INVN_TYPE, '*') =
                      COALESCE (v_lpn_rec_sec_INVN_TYPE, '*')
               AND COALESCE (AID.BATCH_NBR, '*') =
                      COALESCE (v_lpn_rec_sec_BATCH_NBR, '*')
               AND COALESCE (AID.PROD_STAT, '*') =
                      COALESCE (v_lpn_rec_sec_PROD_STAT, '*')
               AND COALESCE (AID.CNTRY_OF_ORGN, '*') =
                      COALESCE (v_lpn_rec_sec_CNTRY_OF_ORGN, '*')
               AND AID.PKT_CTRL_NBR = PD.PKT_CTRL_NBR
               AND AID.PKT_SEQ_NBR = PD.PKT_SEQ_NBR
      --ORDER BY MOD (QTY_ALLOC, v_uom_qty);
      order by AID.ALLOC_INVN_DTL_ID;
BEGIN
   wm_log (
         'PTS-->PTS Conversion : TC_LPN_ID: '
      || p_tc_lpn_id
      || ' ::  '
      || 'pts to pts started...',
      p_debug_flag,
      'INVMGMT');
	
    wm_utils.get_context_info(v_prev_module_name, v_prev_action_name);
    wm_utils.set_context_info(p_module_name => 'THD', p_action_name => 'CONVERT_PTS_TO_PTS',
        p_client_id => p_tc_lpn_id);	

    select f.facility_id
    into v_facility_id
    from facility f
    where f.whse = p_whse;
    
   SELECT LPN_ID,
          LPN_FACILITY_STATUS,
          TC_ASN_ID,
          CURR_SUB_LOCN_ID,
          DEST_SUB_LOCN_ID
     INTO v_ilpn_id,
          v_lpn_facility_status,
          v_tc_asn_id,
          v_ilpn_curr_sub_locn_id,
          v_ilpn_dest_sub_locn_id
     FROM LPN
    WHERE     TC_LPN_ID = p_tc_lpn_id
          AND INBOUND_OUTBOUND_INDICATOR = 'I'
          AND TC_COMPANY_ID = p_tc_company_id
          and c_facility_id = v_facility_id;


   --INVOKE CURSOR TO BREAK THE LPNS
   wm_log (
         'PTS-->PTS Conversion : TC_LPN_ID: '
      || p_tc_lpn_id
      || ' ::  '
      || 'invoking break ilpn sp',
      p_debug_flag,
      'INVMGMT');
   
   MANH_BREAK_ILPNS(p_tc_lpn_id, p_tc_company_id, p_login_user_id, p_disposition_type,
                     coalesce(p_next_up_number_ilpn ,'P05'), p_debug_flag, p_whse, v_facility_id);
   wm_log (
         'PTS-->PTS Conversion : TC_LPN_ID: '
      || p_tc_lpn_id
      || ' ::  '
      || 'invoked break ilpn sp',
      p_debug_flag,
      'INVMGMT');
   v_invn_need_type := 60;

   --load the configuration

   SELECT CASE
             WHEN EXISTS
                     (SELECT '1'
                        FROM WHSE_PARAMETERS WP, WHSE_MASTER W
                       WHERE     W.WHSE = p_whse
                             AND WP.WHSE_MASTER_ID = W.WHSE_MASTER_ID
                             AND WP.ALLOW_SHIPPABLE_INNERS = '1')
             THEN
                '1'
             ELSE
                '0'
          END
     INTO v_allow_shippable_inners
     FROM DUAL;

   SELECT BREAK_LPN_FLAG, HANDLING_UNALLOC_LPN, PROCESS_SHIPPABLE_INNERS
     INTO v_break_lpn, v_handle_unalloc_lpns, v_process_shippable_inners
     FROM DISPOSITION_TYPE
    WHERE     DISPOSITION_TYPE = p_disposition_type
          AND WHSE = p_whse
          AND TC_COMPANY_ID = p_tc_company_id;

   wm_log (
         'PTS-->PTS Conversion : TC_LPN_ID: '
      || p_tc_lpn_id
      || ' ::  '
      || 'Configuration.....BREAK_LPN_FLAG-->'
      || v_break_lpn
      || 'HANDLING_UNALLOC_LPN-->'
      || v_handle_unalloc_lpns
      || 'PROCESS_SHIPPABLE_INNERS-->'
      || v_process_shippable_inners,
      p_debug_flag,
      'INVMGMT');

   --[MACR00844007]    
        --INSERT THE CHILD LPNS SIZE VALUE INTO THE TEMP TABLE    
            INSERT INTO TMP_LPN_SIZE_VALUE 
            SELECT LD.LPN_ID,LD.LPN_DETAIL_ID,LD.SIZE_VALUE
            FROM 
            LPN_DETAIL LD
            INNER JOIN LPN L ON LD.LPN_ID = L.LPN_ID AND L.TC_REFERENCE_LPN_ID = p_tc_lpn_id AND INBOUND_OUTBOUND_INDICATOR='I' AND L.LPN_FACILITY_STATUS < 50 AND L.TC_COMPANY_ID = p_tc_company_id
            and l.c_facility_id = v_facility_id
            ORDER BY  MOD(LD.SIZE_VALUE,v_uom_qty),LD.LPN_ID,LD.LPN_DETAIL_ID;


   --START OF LOOP FOR INT 60 LPNS
   FOR lpn_detail_rec IN CURSOR_LPN_DETAIL
   LOOP
      v_item_id := lpn_detail_rec.ITEM_ID;
      v_wm_inventory_qty := lpn_detail_rec.ON_HAND_QTY;
      v_reference_lpn_id := lpn_detail_rec.tc_lpn_id;
      wm_log (
            'PTS-->PTS Conversion : TC_LPN_ID: '
         || p_tc_lpn_id
         || ' ::  '
         || 'Looping lpn_detail_id:'
         || lpn_detail_rec.LPN_DETAIL_ID,
         p_debug_flag,
         'INVMGMT');
   --[MACR00844007]
   --deleted lines

      --Setting the parameters for allocation cursor query
      v_lpn_rec_sec_ITEM_ID := lpn_detail_rec.ITEM_ID;
      v_lpn_rec_sec_SKU_ATTR_1 := lpn_detail_rec.ITEM_ATTR_1;
      v_lpn_rec_sec_SKU_ATTR_2 := lpn_detail_rec.ITEM_ATTR_2;
      v_lpn_rec_sec_SKU_ATTR_3 := lpn_detail_rec.ITEM_ATTR_3;
      v_lpn_rec_sec_SKU_ATTR_4 := lpn_detail_rec.ITEM_ATTR_4;
      v_lpn_rec_sec_SKU_ATTR_5 := lpn_detail_rec.ITEM_ATTR_5;
      v_lpn_rec_sec_INVN_TYPE := lpn_detail_rec.INVENTORY_TYPE;
      v_lpn_rec_sec_BATCH_NBR := lpn_detail_rec.BATCH_NBR;
      v_lpn_rec_sec_PROD_STAT := lpn_detail_rec.PRODUCT_STATUS;
      v_lpn_rec_sec_CNTRY_OF_ORGN := lpn_detail_rec.CNTRY_OF_ORGN;
   --[MACR00844007]
    --deleted the lines

       wm_log (
            'PTS-->PTS Conversion : TC_LPN_ID: '
         || p_tc_lpn_id
         || ' ::  '
         || 'parameters for allocation query :AID.WHSE = '
         || p_whse
         || ' and AID.CNTR_NBR = '
         || p_tc_lpn_id
         || 'AND AID.INVN_NEED_TYPE = '
         || 60
         || ' AND AID.cd_master_id = '
         || p_tc_company_id,
         p_debug_flag,
         'INVMGMT');
      wm_log (
            'PTS-->PTS Conversion : TC_LPN_ID: '
         || p_tc_lpn_id
         || ' ::  '
         || v_lpn_rec_sec_ITEM_ID
         || '--'
         || v_lpn_rec_sec_SKU_ATTR_1
         || '--'
         || v_lpn_rec_sec_SKU_ATTR_2
         || '--'
         || v_lpn_rec_sec_SKU_ATTR_3
         || '--'
         || v_lpn_rec_sec_SKU_ATTR_4
         || '--'
         || v_lpn_rec_sec_SKU_ATTR_5
         || '--INVENTORY_TYPE'
         || v_lpn_rec_sec_INVN_TYPE
         || '--BATCH_NBR'
         || v_lpn_rec_sec_BATCH_NBR
         || '--'
         || v_lpn_rec_sec_PROD_STAT
         || '--'
         || v_lpn_rec_sec_CNTRY_OF_ORGN,
         p_debug_flag,
         'INVMGMT');

      --START OF LOOP FOR ALLOCATIONS
      FOR alloc_rec IN CURSOR_ALLOCATIONS
      LOOP
         v_allocated_qty := alloc_rec.QTY_ALLOC;
         v_tc_order_id := alloc_rec.TC_ORDER_ID;
         --get the aggregate order and aggregate line item id
         v_order_id := alloc_rec.ORDER_ID;
         v_line_item_id := alloc_rec.LINE_ITEM_ID;
         wm_log (
               'PTS-->PTS Conversion : TC_LPN_ID: '
            || p_tc_lpn_id
            || ' ::  '
            || 'Current Allocation is---->'
            || alloc_rec.alloc_invn_dtl_id,
            p_debug_flag,
            'INVMGMT');

         -- START OF LOOP FOR CHILD LPNS ASSOCIATED WITH THE INTO 60 LPN_DETAIL BASED ON CNTR_NBR AND ITEM
        --[MACR00844007]
 
         wm_log('PTS-->PTS Conversion : TC_LPN_ID: '||p_tc_lpn_id||' ::  '||'Fetching child records for allocation:'||alloc_rec.alloc_invn_dtl_id,p_debug_flag,'INVMGMT');
         <<CHILD_LPNS_LOOP>>
         FOR child_lpn_detail_rec IN CURSOR_CHILD_LPNS
         LOOP
              --[MACR00844007]
             --deleted lines
              --[MACR00844007]
                wm_log(
                'PTS-->PTS Conversion : TC_LPN_ID: '
                ||p_tc_lpn_id
                ||' ::  '
                ||'Current  child record: lpn_id : '
                ||child_lpn_detail_rec.lpn_id
                ||' lpn_detail_id: '
                ||child_lpn_detail_rec.LPN_DETAIL_ID,
                p_debug_flag,
                'INVMGMT');
                                
                 --calculate the uom_qty for that lpn_detail line
                  -- if shippable inner is allowed then fetch the quantity from the item table
                  IF v_break_lpn = '1'
                     AND (v_allow_shippable_inners = '1'
                          OR v_allow_shippable_inners = '2')
                     AND v_process_shippable_inners = '1'
                  THEN         
                     --WM-27145:Use allocation UOM if minimum shippable inner uom is not defined on the item                  
                     SELECT COALESCE (ic.min_ship_inner_uom_id, (SELECT du.allocation_uom_id                          
                     FROM disposition_type du
                    WHERE du.disposition_type = p_disposition_type 
                      AND du.whse=p_whse AND du.tc_company_id  = p_tc_company_id))
                          INTO v_alloc_uom
                          FROM item_cbo ic
                         WHERE ic.item_id = v_item_id;
                             
                  --if shippable inner is not allowed then get the value from lpn_detail
                  ELSIF v_break_lpn = '1'
                  THEN
                     SELECT du.allocation_uom_id
                          INTO v_alloc_uom
                          FROM disposition_type du
                         WHERE du.disposition_type = p_disposition_type 
                           AND du.whse=p_whse AND du.tc_company_id  = p_tc_company_id;
                  END IF;

                  v_uom_qty := 0;
                  
                  IF v_alloc_uom = 'K'
                        THEN
                            v_uom_qty := COALESCE(child_lpn_detail_rec.STD_PACK_QTY, child_lpn_detail_rec.AD_STD_PACK_QTY, 0);            
                    ELSIF v_alloc_uom = 'S'
                        THEN
                            v_uom_qty := COALESCE(child_lpn_detail_rec.STD_SUB_PACK_QTY, child_lpn_detail_rec.AD_STD_SUB_PACK_QTY, 0);            
                    ELSIF v_alloc_uom = 'C'
                        THEN
                            v_uom_qty := COALESCE(child_lpn_detail_rec.STD_CASE_QTY, 0);
                    END IF;
                    
                    IF v_uom_qty = 0
                     THEN
                     SELECT COALESCE (ipc.quantity, 0)
                       INTO v_uom_qty 
                       FROM item_cbo ic, item_package_cbo ipc, size_uom su,STANDARD_UOM suom
                      WHERE     ic.item_id = v_item_id
                            AND ic.item_id = ipc.item_id
                            AND su.STANDARD_UOM = suom.STANDARD_UOM
                            AND suom.ABBREVIATION = v_alloc_uom
                            AND su.size_uom_id = ipc.package_uom_id
                            AND ipc.mark_for_deletion = 0
                            AND ipc.is_std = 1
                            AND ROWNUM < 2;
                        END IF;

                                
                                SELECT SIZE_VALUE INTO v_child_lpn_size_value FROM TMP_LPN_SIZE_VALUE WHERE LPN_ID = child_lpn_detail_rec.LPN_ID AND LPN_DETAIL_ID = child_lpn_detail_rec.LPN_DETAIL_ID;
                                IF v_child_lpn_size_value = 0 THEN
                                        v_child_lpn_size_value := child_lpn_detail_rec.SIZE_VALUE;
                                END IF;
                                wm_log('current size_value:'||v_child_lpn_size_value,p_debug_flag,'INVMGMT');

                                            wm_log('PTS-->PTS Conversion : TC_LPN_ID: '||p_tc_lpn_id||' ::  '||'v_allocated_qty'||v_allocated_qty||' v_child_lpn_size_value'||v_child_lpn_size_value,p_debug_flag,'INVMGMT');
                                            IF v_allocated_qty > 0 
                                                    AND v_allocated_qty >= v_child_lpn_size_value
                                                              --[MACR00844007]
                                                            --deleted line
                                                         THEN
                                                        wm_log(
                                                                'PTS-->PTS Conversion : TC_LPN_ID: '
                                                                ||p_tc_lpn_id
                                                                ||' ::  '
                                                                ||'v_allocated_qty >= v_child_lpn_size_value',
                                                                p_debug_flag,
                                                                'INVMGMT');
                                                        wm_log(
                                                                'PTS-->PTS Conversion : TC_LPN_ID: '
                                                                ||p_tc_lpn_id
                                                                ||' ::  '
                                                                ||'v_allocated_qty'
                                                                ||v_allocated_qty
                                              --[MACR00844007]                
                                                                ||' v_child_lpn_size_value'
                                                                ||v_child_lpn_size_value,
                                                                p_debug_flag,
                                                                'INVMGMT');
               -----------------------------------------child lpn inserts and updates-------------------------------------
             --Update the location on the CHILD LPN's inventory    
               UPDATE WM_INVENTORY
                  SET LOCATION_ID = v_ilpn_curr_sub_locn_id,
                      LAST_UPDATED_DTTM = SYSTIMESTAMP,
                      LAST_UPDATED_SOURCE = p_login_user_id
                WHERE WM_INVENTORY_ID = child_lpn_detail_rec.WM_INVENTORY_ID;

               wm_log (
                     'PTS-->PTS Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'Updated child lpn inventory'
                  || child_lpn_detail_rec.WM_INVENTORY_ID
                  || ' location:'
                  || v_ilpn_curr_sub_locn_id,
                  p_debug_flag,
                  'INVMGMT');

               --update the child lpn
               UPDATE LPN
                  SET LPN_FACILITY_STATUS = 50,
                      DEST_SUB_LOCN_ID = alloc_rec.dest_locn_id,
                      --TC_ORDER_ID = alloc_rec.TC_ORDER_ID,
                      --ORDER_ID = alloc_rec.ORDER_ID,
                      LAST_UPDATED_SOURCE = p_login_user_id,
                      LAST_UPDATED_DTTM = SYSTIMESTAMP,
                      HIBERNATE_VERSION =
                         (COALESCE (HIBERNATE_VERSION, 0) + 1)
                WHERE LPN_ID = child_lpn_detail_rec.LPN_ID;

               wm_log (
                     'PTS-->PTS Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'Updated child lpn Orders'
                  || alloc_rec.ORDER_ID,
                  p_debug_flag,
                  'INVMGMT');

               UPDATE LPN_DETAIL
                  SET --distribution_order_dtl_id = alloc_rec.LINE_ITEM_ID,
                      LPN_DETAIL_STATUS = 70,
                      LAST_UPDATED_SOURCE = p_login_user_id,
                      LAST_UPDATED_DTTM = SYSTIMESTAMP,
                      HIBERNATE_VERSION =
                         (COALESCE (HIBERNATE_VERSION, 0) + 1)
                WHERE LPN_ID = child_lpn_detail_rec.LPN_ID
                      AND LPN_DETAIL_ID = child_lpn_detail_rec.LPN_DETAIL_ID;

               --create allocation record for the child lpn
               INSERT INTO alloc_invn_dtl (alloc_invn_dtl_id,
                                           whse,
                                           item_id,
                                           batch_nbr,
                                           alloc_invn_code,
                                           cntr_nbr,
                                           trans_invn_type,
                                           pull_locn_id,
                                           invn_need_type,
                                           task_type,
                                           task_prty,
                                           task_batch,
                                           alloc_uom,
                                           alloc_uom_qty,
                                           full_cntr_allocd,
                                           orig_reqmt,
                                           qty_alloc,
                                           dest_locn_id,
                                           task_genrtn_ref_code,
                                           task_genrtn_ref_nbr,
                                           task_cmpl_ref_code,
                                           task_cmpl_ref_nbr,
                                           need_id,
                                           reqd_batch_nbr,
                                           pkt_ctrl_nbr,
                                           pkt_seq_nbr,
                                           carton_nbr,
                                           carton_seq_nbr,
                                           pikr_nbr,
                                           pull_locn_seq_nbr,
                                           dest_locn_seq_nbr,
                                           stat_code,
                                           create_date_time,
                                           mod_date_time,
                                           user_id,
                                           cd_master_id,
                                           invn_type,
                                           prod_stat,
                                           sku_attr_1,
                                           sku_attr_2,
                                           sku_attr_3,
                                           sku_attr_4,
                                           sku_attr_5,
                                           TC_ORDER_ID,
                                           LINE_ITEM_ID,
                                           SUBSTITUTION_FLAG)
                  SELECT ALLOC_INVN_DTL_ID_SEQ.NEXTVAL,
                         p_whse,
                         v_item_id,
                         alloc_rec.batch_nbr,
                         alloc_rec.ALLOC_INVN_CODE,
                         child_lpn_detail_rec.tc_lpn_id,
                         alloc_rec.trans_invn_type,
                         alloc_rec.pull_locn_id,
                         v_invn_need_type,
                         alloc_rec.TASK_TYPE,
                         alloc_rec.TASK_PRTY,
                         alloc_rec.task_batch,
                         'U',
                         v_uom_qty as ALLOC_UOM_QTY,
                         alloc_rec.full_cntr_allocd,
                         v_child_lpn_size_value,
                         --[MACR00844007]
                         v_child_lpn_size_value,
                         alloc_rec.dest_locn_id,
                         alloc_rec.task_genrtn_ref_code,
                         child_lpn_detail_rec.tc_lpn_id,
                         alloc_rec.task_cmpl_ref_code,
                         alloc_rec.pkt_ctrl_nbr,
                         alloc_rec.need_id,
                         alloc_rec.reqd_batch_nbr,
                         alloc_rec.pkt_ctrl_nbr,
                         alloc_rec.pkt_seq_nbr,
                         v_tc_ilpn_id,
                         alloc_rec.carton_seq_nbr,
                         alloc_rec.pikr_nbr,
                         alloc_rec.pull_locn_seq_nbr,
                         alloc_rec.dest_locn_seq_nbr,
                         alloc_rec.stat_code,
                         SYSTIMESTAMP,
                         SYSTIMESTAMP,
                         p_login_user_id,
                         lpn_detail_rec.tc_company_id,
                         lpn_detail_rec.inventory_type,
                         lpn_detail_rec.product_status,
                         lpn_detail_rec.item_attr_1,
                         lpn_detail_rec.item_attr_2,
                         lpn_detail_rec.item_attr_3,
                         lpn_detail_rec.item_attr_4,
                         lpn_detail_rec.item_attr_5,
                         v_tc_order_id,
                         v_line_item_id,
                         alloc_rec.SUBSTITUTION_FLAG
                    FROM DUAL;

               wm_log (
                     'PTS-->PTS Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'created allocations',
                  p_debug_flag,
                  'INVMGMT');

               -----------------------------------------parent table inserts and updates-------------------------------------

               --Update the parent lpn detail
               UPDATE LPN_DETAIL
              --[MACR00844007]
                  SET SIZE_VALUE = SIZE_VALUE - v_child_lpn_size_value,
                      LAST_UPDATED_SOURCE = p_login_user_id,
                      LAST_UPDATED_DTTM = SYSTIMESTAMP,
                      HIBERNATE_VERSION =
                         (COALESCE (HIBERNATE_VERSION, 0) + 1)
                WHERE LPN_DETAIL_ID = lpn_detail_rec.LPN_DETAIL_ID
                      AND LPN_ID = lpn_detail_rec.LPN_ID;

               wm_log (
                     'PTS-->PTS Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'updated the parent lpn_detail',
                  p_debug_flag,
                  'INVMGMT');

               --decrement the allocation qty by the child lpn's size value
              --[MACR00844007]
             --deleted lines
               --decrement the inventory qty by the child lpn's size value
               --[MACR00844007]
                UPDATE WM_INVENTORY
                  SET ON_HAND_QTY = ON_HAND_QTY - v_child_lpn_size_value,
                      WM_ALLOCATED_QTY = WM_ALLOCATED_QTY - v_child_lpn_size_value,
                      LAST_UPDATED_DTTM = SYSTIMESTAMP,
                      LAST_UPDATED_SOURCE = p_login_user_id
                WHERE WM_INVENTORY_ID = lpn_detail_rec.WM_INVENTORY_ID;

               wm_log (
                     'PTS-->PTS Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'updated the parent inventory',
                  p_debug_flag,
                  'INVMGMT');
               --[MACR00844007]
               v_allocated_qty :=
                  v_allocated_qty - v_child_lpn_size_value;
               v_wm_inventory_qty :=
                  v_wm_inventory_qty - v_child_lpn_size_value;
            ELSIF v_allocated_qty > 0 
                  AND v_child_lpn_size_value >= v_allocated_qty
            THEN
                --[MACR00844007]
                wm_log(
                    'PTS-->PTS Conversion : TC_LPN_ID: '
                    ||p_tc_lpn_id
                    ||' ::  '
                    ||'Size Value > v_allocated_qty:',
                    p_debug_flag,
                    'INVMGMT');
               wm_log (
                     'PTS-->PTS Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'v_allocated_qty :'
                  || v_allocated_qty
                --[MACR00844007]
                  || ' v_child_lpn_size_value'
                  || v_child_lpn_size_value,
                  p_debug_flag,
                  'INVMGMT');

                --[MACR00844007]
                UPDATE TMP_LPN_SIZE_VALUE SET SIZE_VALUE = SIZE_VALUE - v_allocated_qty WHERE LPN_ID = child_lpn_detail_rec.LPN_ID AND LPN_DETAIL_ID = child_lpn_detail_rec.LPN_DETAIL_ID;
                wm_log('PTS-->PTS Conversion : TC_LPN_ID: '||p_tc_lpn_id||' ::  '||'updated child record size_value',p_debug_flag,'INVMGMT');
                --[MACR00844007]
                IF  v_child_lpn_size_value > v_allocated_qty THEN
                        wm_log(
                        'PTS-->PTS Conversion : TC_LPN_ID: '
                        ||p_tc_lpn_id
                        ||' ::  '
                        ||'child lpn:'
                        ||child_lpn_detail_rec.LPN_ID
                        || 'is partially allocated',
                        p_debug_flag,
                        'INVMGMT');
                        
                        UPDATE LPN SET   
                                        LPN_FACILITY_STATUS=45 ,
                                        DEST_SUB_LOCN_ID=v_ilpn_dest_sub_locn_id,
                                        CURR_SUB_LOCN_ID=v_ilpn_curr_sub_locn_id,
                                       -- TC_ORDER_ID = alloc_rec.TC_ORDER_ID,
                                       --ORDER_ID = alloc_rec.ORDER_ID,
                                        LAST_UPDATED_SOURCE = p_login_user_id, 
                                        LAST_UPDATED_DTTM = SYSTIMESTAMP,
                                        HIBERNATE_VERSION = 
                                                (COALESCE(HIBERNATE_VERSION,0)+1)
                                        WHERE  LPN_ID = child_lpn_detail_rec.LPN_ID;
                ELSE
                        wm_log(
                        'PTS-->PTS Conversion : TC_LPN_ID: '
                        ||p_tc_lpn_id
                        ||' ::  '
                        ||'child lpn:'
                        ||child_lpn_detail_rec.LPN_ID
                        || 'is consumed',
                        p_debug_flag,
                        'INVMGMT');                                                    
                        UPDATE LPN SET 
                                                LPN_FACILITY_STATUS=50 ,    
                                                DEST_SUB_LOCN_ID=v_ilpn_dest_sub_locn_id,
                                                CURR_SUB_LOCN_ID=v_ilpn_curr_sub_locn_id,
                                                --TC_ORDER_ID = alloc_rec.TC_ORDER_ID,
                                                --ORDER_ID = alloc_rec.ORDER_ID,
                                                LAST_UPDATED_SOURCE = p_login_user_id, LAST_UPDATED_DTTM = SYSTIMESTAMP,
                                HIBERNATE_VERSION = (COALESCE(HIBERNATE_VERSION,0)+1)
                        WHERE  LPN_ID = child_lpn_detail_rec.LPN_ID;
                END IF;

                wm_log('PTS-->PTS Conversion : TC_LPN_ID: '||p_tc_lpn_id||' ::  '||'v_allocated_qty'||v_allocated_qty||' v_child_lpn_size_value'||v_child_lpn_size_value,p_debug_flag,'INVMGMT');
               -----------------------------------------child lpn inserts and updates-------------------------------------
                                                                                                                               --create allocation record for the child lpn                                
                                                                 insert into alloc_invn_dtl
                                                                    (
                                                                         alloc_invn_dtl_id, whse, item_id,
                                                                         batch_nbr, alloc_invn_code, cntr_nbr,
                                                                         trans_invn_type, pull_locn_id, invn_need_type,
                                                                         task_type, task_prty, task_batch,
                                                                         alloc_uom, alloc_uom_qty, full_cntr_allocd,
                                                                         orig_reqmt, qty_alloc, dest_locn_id,
                                                                         task_genrtn_ref_code, task_genrtn_ref_nbr, task_cmpl_ref_code,
                                                                         task_cmpl_ref_nbr, need_id, reqd_batch_nbr,
                                                                         pkt_ctrl_nbr, pkt_seq_nbr, carton_nbr,
                                                                         carton_seq_nbr, pikr_nbr, pull_locn_seq_nbr,
                                                                         dest_locn_seq_nbr, stat_code, create_date_time,
                                                                         mod_date_time, user_id, cd_master_id,
                                                                         invn_type, prod_stat, sku_attr_1,
                                                                         sku_attr_2, sku_attr_3, sku_attr_4,
                                                                         sku_attr_5,TC_ORDER_ID,LINE_ITEM_ID
                                                                    )
                                                                    select
                                                                        ALLOC_INVN_DTL_ID_SEQ.nextval , p_whse, v_item_id,
                                                                        null as manufactured_lot,1, child_lpn_detail_rec.tc_lpn_id,
                                                                        0, alloc_rec.pull_locn_id, v_invn_need_type,
                                                                        cast(null as varchar(2)), 40, cast(null as varchar(6)),
                                                                        'U', 1 ,'Y',
                                                                        v_allocated_qty,v_allocated_qty,alloc_rec.dest_locn_id,
                                                                        to_char(14),child_lpn_detail_rec.tc_lpn_id,to_char(2),
                                                                        alloc_rec.pkt_ctrl_nbr,cast(null as varchar(10)),null as manufactured_lot,
                                                                        alloc_rec.pkt_ctrl_nbr,alloc_rec.pkt_seq_nbr,v_tc_ilpn_id,
                                                                        child_lpn_detail_rec.lpn_detail_id,0,0,
                                                                        0,0,systimestamp,
                                                                        systimestamp,p_login_user_id,lpn_detail_rec.tc_company_id,
                                                                        lpn_detail_rec.inventory_type,lpn_detail_rec.product_status,lpn_detail_rec.item_attr_1,
                                                                        lpn_detail_rec.item_attr_2,lpn_detail_rec.item_attr_3,lpn_detail_rec.item_attr_4,
                                                                        lpn_detail_rec.item_attr_5,v_tc_order_id,v_line_item_id 
                                                                    from dual;

               --Update the location on the CHILD LPN's inventory
               UPDATE WM_INVENTORY
                  SET LOCATION_ID = v_ilpn_curr_sub_locn_id,
                      LAST_UPDATED_DTTM = SYSTIMESTAMP,
                      LAST_UPDATED_SOURCE = p_login_user_id
                WHERE WM_INVENTORY_ID = child_lpn_detail_rec.WM_INVENTORY_ID;

               wm_log (
                     'PTS-->PTS Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'updated the child lpn inventory',
                  p_debug_flag,
                  'INVMGMT');
                --[MACR00844007]
                --deleted lines


              /* UPDATE LPN_DETAIL
                  SET distribution_order_dtl_id = alloc_rec.LINE_ITEM_ID,*/
                --[MACR00844007]      
                --deleted lines
                          
               /*       LAST_UPDATED_SOURCE = p_login_user_id,
                      LAST_UPDATED_DTTM = SYSTIMESTAMP,
                      HIBERNATE_VERSION =
                         (COALESCE (HIBERNATE_VERSION, 0) + 1)
                WHERE LPN_ID = child_lpn_detail_rec.LPN_ID
                      AND LPN_DETAIL_ID = child_lpn_detail_rec.LPN_DETAIL_ID;*/

               -----------------------------------------parent table inserts and updates-------------------------------------
               UPDATE LPN_DETAIL
                  SET SIZE_VALUE = SIZE_VALUE - v_allocated_qty,
                      LAST_UPDATED_SOURCE = p_login_user_id,
                      LAST_UPDATED_DTTM = SYSTIMESTAMP,
                      HIBERNATE_VERSION =
                         (COALESCE (HIBERNATE_VERSION, 0) + 1)
                WHERE LPN_DETAIL_ID = lpn_detail_rec.LPN_DETAIL_ID
                      AND LPN_ID = lpn_detail_rec.LPN_ID;

               wm_log (
                     'PTS-->PTS Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'updated the parent lpn_detail',
                  p_debug_flag,
                  'INVMGMT');
            --[MACR00844007]
            --deleted lines
               wm_log (
                     'PTS-->PTS Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'updated the parent allocation record',
                  p_debug_flag,
                  'INVMGMT');

               --update the inventory record for the parent int 60 lpn detail
               UPDATE WM_INVENTORY
                  SET WM_ALLOCATED_QTY = WM_ALLOCATED_QTY - v_allocated_qty,
                      LOCATION_ID = v_ilpn_curr_sub_locn_id,
                      LAST_UPDATED_DTTM = SYSTIMESTAMP,
                      LAST_UPDATED_SOURCE = p_login_user_id
                WHERE WM_INVENTORY_ID = lpn_detail_rec.WM_INVENTORY_ID;

               wm_log (
                     'PTS-->PTS Conversion : TC_LPN_ID: '
                  || p_tc_lpn_id
                  || ' ::  '
                  || 'updated the parent inventory',
                  p_debug_flag,
                  'INVMGMT');
    
               v_wm_inventory_qty :=
            --[MACR00844007]                
                  v_wm_inventory_qty - v_allocated_qty;
            --[MACR00844007]
               v_allocated_qty := 
                  v_allocated_qty - v_allocated_qty;
               wm_log(
                    'PTS-->PTS Conversion : TC_LPN_ID: '
                    ||p_tc_lpn_id
                    ||' ::  '
                    ||' This Allocation is complete',
                    p_debug_flag,
                    'INVMGMT');
              -- wm_log('PTS-->PTS Conversion : TC_LPN_ID: '||p_tc_lpn_id||' ::  '||'Remaining Allocation :'||v_allocated_qty,p_debug_flag,'INVMGMT'); 
              -- wm_log('PTS-->PTS Conversion : TC_LPN_ID: '||p_tc_lpn_id||' ::  '||'Remaining On Hand Qty :'||v_wm_inventory_qty,p_debug_flag,'INVMGMT');         
            --EXIT CHILD_LPNS_LOOP;
            END IF;
            --[MACR00844007]
               wm_log('PTS-->PTS Conversion : TC_LPN_ID: '||p_tc_lpn_id||' ::  '||'Remaining Allocation :'||v_allocated_qty,p_debug_flag,'INVMGMT'); 
               wm_log('PTS-->PTS Conversion : TC_LPN_ID: '||p_tc_lpn_id||' ::  '||'Remaining On Hand Qty :'||v_wm_inventory_qty,p_debug_flag,'INVMGMT'); 
               IF  v_allocated_qty = 0 THEN
                wm_log('PTS-->PTS Conversion : TC_LPN_ID: '||p_tc_lpn_id||' ::  '||' This Allocation is complete...',p_debug_flag,'INVMGMT');
                EXIT CHILD_LPNS_LOOP;
               END IF;
         END LOOP;

                --[MACR00844007]
         --IF v_allocated_qty = 0
         --THEN
            wm_log (
                  'PTS-->PTS Conversion : TC_LPN_ID: '
               || p_tc_lpn_id
               || ' ::  '
               || 'allocated qty is zero..deleting allocation :'
               || alloc_rec.ALLOC_INVN_DTL_ID,
               p_debug_flag,
               'INVMGMT');

                            DELETE                             /*+ PARALLEL */
                                  FROM  ALLOC_INVN_DTL
                                  WHERE ALLOC_INVN_DTL_ID =
                                           alloc_rec.ALLOC_INVN_DTL_ID;
                --[MACR00844007]
         --END IF;
      END LOOP;

      IF v_wm_inventory_qty = 0
      THEN
         wm_log (
               'PTS-->PTS Conversion : TC_LPN_ID: '
            || p_tc_lpn_id
            || ' ::  '
            || 'inventory on hand qty is zero..deleting inventory :'
            || lpn_detail_rec.WM_INVENTORY_ID,
            p_debug_flag,
            'INVMGMT');

                         DELETE                                /*+ PARALLEL */
                               FROM  WM_INVENTORY
                               WHERE WM_INVENTORY_ID =
                                        lpn_detail_rec.WM_INVENTORY_ID;
      END IF;
   END LOOP;

   -------------Palletize logic-----------------------------------
   SELECT SUM (LD.SIZE_VALUE)
     INTO v_total_size_value
     FROM LPN_DETAIL LD, LPN L
    WHERE L.LPN_ID = v_ilpn_id AND LD.LPN_ID = L.LPN_ID;

   SELECT LPN_FACILITY_STATUS
     INTO v_lpn_facility_status
     FROM LPN
    WHERE LPN_ID = v_ilpn_id;

   IF v_total_size_value = 0 and p_reuse_ilpn_as_pallet = 1
   THEN
      wm_log (
            'PTS-->PTS Conversion : TC_LPN_ID: '
         || p_tc_lpn_id
         || ' ::  '
         || 'palletization complete',
         p_debug_flag,
         'INVMGMT');

      UPDATE LPN
         SET LPN_TYPE = 2,
             LAST_UPDATED_SOURCE = p_login_user_id,
             LAST_UPDATED_DTTM = SYSTIMESTAMP,
             HIBERNATE_VERSION = (COALESCE (HIBERNATE_VERSION, 0) + 1)
       WHERE tc_lpn_id = p_tc_lpn_id AND TC_COMPANY_ID = p_tc_company_id
           and c_facility_id = v_facility_id and inbound_outbound_indicator = 'I';

      UPDATE LPN
         SET PARENT_LPN_ID = v_ilpn_id, tc_parent_lpn_id = p_tc_lpn_id
       WHERE     TC_REFERENCE_LPN_ID = p_tc_lpn_id
             AND LPN_FACILITY_STATUS = 50
             AND TC_COMPANY_ID = p_tc_company_id
             and c_facility_id = v_facility_id
             and inbound_outbound_indicator = 'I';

      wm_log (
            'PTS-->PTS Conversion : TC_LPN_ID: '
         || p_tc_lpn_id
         || ' ::  '
         || 'palletization complete',
         p_debug_flag,
         'INVMGMT');
   END IF;

   ----------------------------------------------------------------------
   --if the unallocated lpns are to be rolled back then delete the child lpns and the corresponding inventory records created by the  break SP
   SELECT COUNT (1)
     INTO v_total_unalloc_qty
     FROM lpn l
    WHERE     l.lpn_facility_status < 45
          AND l.tc_reference_lpn_id = p_tc_lpn_id
          AND TC_COMPANY_ID = p_tc_company_id
          and l.c_facility_id = v_facility_id
          and l.inbound_outbound_indicator = 'I';

   wm_log (
         'PTS-->PTS Conversion : TC_LPN_ID: '
      || p_tc_lpn_id
      || ' ::  '
      || 'Total Unallocated Units'
      || v_total_unalloc_qty,
      p_debug_flag,
      'INVMGMT');

   IF v_total_unalloc_qty > 0
   THEN
      IF v_handle_unalloc_lpns = '1'
      THEN
         UPDATE LPN
            SET LPN_STATUS = 45,
                LPN_FACILITY_STATUS = 10,
                LAST_UPDATED_SOURCE = p_login_user_id,
                LAST_UPDATED_DTTM = SYSTIMESTAMP,
                HIBERNATE_VERSION = (COALESCE (HIBERNATE_VERSION, 0) + 1),
                LPN_FACILITY_STAT_UPDATED_DTTM = SYSTIMESTAMP,
                DISPOSITION_TYPE = p_exception_disp_type
          WHERE LPN_ID = v_ilpn_id;

         DELETE FROM WM_INVENTORY WI
               WHERE EXISTS
                        (SELECT 1
                           FROM LPN L
                          WHERE     WI.LPN_ID = L.LPN_ID
                --[MACR00844007]
                                AND L.lpn_facility_status < 45
                                AND L.TC_REFERENCE_LPN_ID = p_tc_lpn_id
                                and l.tc_company_id = p_tc_company_id
                                and l.c_facility_id = v_facility_id
                                and l.inbound_outbound_indicator = 'I');

                         --delete all the child lpns not consumed
                         DELETE                                /*+ PARALLEL */
                               FROM  LPN_DETAIL
                               WHERE LPN_ID IN
                                        (SELECT LPN_ID
                                           FROM LPN
                --[MACR00844007]
                                          WHERE LPN.LPN_FACILITY_STATUS < 45
                                                AND LPN.TC_REFERENCE_LPN_ID = p_tc_lpn_id
                                                and lpn.tc_company_id = p_tc_company_id
                                                and lpn.c_facility_id = v_facility_id
                                                and lpn.inbound_outbound_indicator = 'I');

                         DELETE                                /*+ PARALLEL */
                               FROM  LPN
                --[MACR00844007]                
                               WHERE lpn_facility_status < 45
                                     AND TC_REFERENCE_LPN_ID = p_tc_lpn_id
                                     and tc_company_id = p_tc_company_id
                                     and c_facility_id = v_facility_id
                                     and inbound_outbound_indicator = 'I';
      ELSIF v_handle_unalloc_lpns = '2'
      THEN
         --since all the lpn_details for the lpn didnt get allocated , move the lpn status to partially allocated.
         UPDATE LPN
            SET LAST_UPDATED_SOURCE = p_login_user_id,
                LAST_UPDATED_DTTM = SYSTIMESTAMP,
                HIBERNATE_VERSION = (COALESCE (HIBERNATE_VERSION, 0) + 1),
                LPN_STATUS = 70,
                LPN_FACILITY_STATUS = 95,
                LPN_FACILITY_STAT_UPDATED_DTTM = SYSTIMESTAMP
          WHERE LPN_ID = v_ilpn_id;

         UPDATE LPN_DETAIL
            SET SIZE_VALUE = 0,
                LAST_UPDATED_SOURCE = p_login_user_id,
                LAST_UPDATED_DTTM = SYSTIMESTAMP,
                HIBERNATE_VERSION = (COALESCE (HIBERNATE_VERSION, 0) + 1)
          WHERE LPN_ID = v_ilpn_id;


         DELETE FROM WM_INVENTORY
               WHERE TC_LPN_ID = p_tc_lpn_id;

         UPDATE LPN
            SET LAST_UPDATED_SOURCE = p_login_user_id,
                LAST_UPDATED_DTTM = SYSTIMESTAMP,
                HIBERNATE_VERSION = (COALESCE (HIBERNATE_VERSION, 0) + 1),
                LPN_FACILITY_STAT_UPDATED_DTTM = SYSTIMESTAMP,
                DISPOSITION_TYPE = p_exception_disp_type
                --[MACR00844007]    
          WHERE LPN_FACILITY_STATUS < 45
                AND TC_REFERENCE_LPN_ID = p_tc_lpn_id
                and tc_company_id = p_tc_company_id
                and c_facility_id = v_facility_id
                and inbound_outbound_indicator = 'I';
         
         --[WM-22821]         
         UPDATE WM_INVENTORY 
            SET WM_ALLOCATED_QTY=0,
                LAST_UPDATED_SOURCE = p_login_user_id,
                LAST_UPDATED_DTTM = SYSTIMESTAMP,
                WM_VERSION_ID = (COALESCE (WM_VERSION_ID, 0) + 1)
             WHERE LPN_ID IN
                        (SELECT LPN_ID
                           FROM LPN
                          WHERE LPN.LPN_FACILITY_STATUS < 45
                                AND LPN.TC_REFERENCE_LPN_ID = p_tc_lpn_id
                and lpn.tc_company_id = p_tc_company_id
                and lpn.c_facility_id = v_facility_id
                and lpn.inbound_outbound_indicator = 'I');
      END IF;
   END IF;

   IF v_total_size_value = 0 
   THEN
      IF p_reuse_ilpn_as_pallet = 1
      THEN
          UPDATE LPN
             SET LAST_UPDATED_SOURCE = p_login_user_id,
                 LAST_UPDATED_DTTM = SYSTIMESTAMP,
                 HIBERNATE_VERSION = (COALESCE (HIBERNATE_VERSION, 0) + 1),
                 LPN_STATUS = 70,
                 LPN_FACILITY_STATUS = 10,
                 LPN_FACILITY_STAT_UPDATED_DTTM = SYSTIMESTAMP
           WHERE LPN_ID = v_ilpn_id;
      ELSE 
        UPDATE LPN
             SET LAST_UPDATED_SOURCE = p_login_user_id,
                 LAST_UPDATED_DTTM = SYSTIMESTAMP,
                 HIBERNATE_VERSION = (COALESCE (HIBERNATE_VERSION, 0) + 1),
                 LPN_STATUS = 70,
                 LPN_FACILITY_STATUS = 95,
                 LPN_FACILITY_STAT_UPDATED_DTTM = SYSTIMESTAMP
           WHERE LPN_ID = v_ilpn_id;
        END IF;
   END IF;
END ;
/
SHOW ERRORS;
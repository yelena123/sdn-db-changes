create or replace procedure wm_upld_tasks_for_lm_planning
(
    p_user_id           in user_profile.user_id%type,
    p_job_config_id     in job_config.job_config_id%type
)
as
    v_tc_company_id     company.company_id%type;
    v_facility_id       facility.facility_id%type;
    v_last_proc_crit_1  job_config.last_proc_crit_1%type;
    v_last_run_time     date;
    v_is_lm_enabled     int;
    v_current_date      date := sysdate;
    v_whse              facility.whse%type;
    v_job_hist_id       job_hist.job_hist_id%type;
    v_job_code          job_config.job_code%type;
    v_num_rows          job_hist.nbr_of_records_processed%type;
    type t_rec is       record
                        (
                            task_id             task_hdr.task_id%type,
                            labor_tran_id       labor_tran.labor_tran_id%type,
                            vehicle_type        task_action.vehicle_type%type,
                            els_actvty_code     tran_master.els_actvty_code%type,
                            menu_mode           task_action.menu_mode%type,
                            task_genrtn_ref_nbr task_hdr.task_genrtn_ref_nbr%type
                        );
    type t_table is     table of t_rec;
    va_lt_rec           t_table;
begin
    select jc.facility_id, jc.tc_company_id, jc.last_proc_crit_1, jc.job_code
    into v_facility_id, v_tc_company_id, v_last_proc_crit_1, v_job_code
    from job_config jc
    where jc.job_config_id = p_job_config_id;

    if (v_last_proc_crit_1 is null)
    then
        -- first ever run; start with the first task's timestamp and rewind that a little
        select min(th.create_date_time) - 1
        into v_last_run_time
        from task_hdr th;
    else
        v_last_run_time := to_date(v_last_proc_crit_1, 'mm/dd/yyyy hh24:mi:ss');
    end if;

    select case when exists
    (
        select 1
        from whse_parameters wp 
        where wp.whse_master_id = v_facility_id and wp.els_instl in ('1', '2')
            and wp.lm_monitor_instl_flag in ( '1','Y')
    ) then 1 else 0 end
    into v_is_lm_enabled
    from dual;

    if (v_is_lm_enabled = 0)
    then
        return;
    end if;

    select f.whse
    into v_whse
    from facility f
    where f.facility_id = v_facility_id;

    v_job_hist_id := job_hist_id_seq.nextval;
    insert into job_hist
    (
        job_code, facility_id, tc_company_id, job_start_dttm, modified_dttm, user_id, created_dttm,
        job_hist_id
    )
    select v_job_code, v_facility_id, v_tc_company_id, v_current_date, v_current_date, p_user_id, 
        sysdate, v_job_hist_id
    from dual;

    commit;

    select th.task_id, labor_tran_id_seq.nextval, ta.vehicle_type, tm.els_actvty_code, ta.menu_mode,
        decode(th.task_genrtn_ref_code, '1', th.task_genrtn_ref_nbr, null)
    bulk collect into va_lt_rec
    from task_hdr as of timestamp v_current_date th, task_action ta, tran_master tm
    where ta.invn_need_type = th.invn_need_type and ta.task_type = th.task_type
        and ta.whse = th.whse and ta.task_name = tm.task_name and tm.els_actvty_code is not null
        and ta.stop_labor_mon_msg = 0 and ta.menu_mode in ('I', 'C')
        and th.stat_code < 99 and th.whse = v_whse and th.create_date_time > v_last_run_time 
        and th.create_date_time <= v_current_date;
    
    forall i in 1..va_lt_rec.count
    insert into labor_tran
    (
        labor_tran_id, vhcl_type, whse, created_dttm, last_updated_dttm, msg_stat_code, 
        sched_start_date, sched_start_time, ref_code, ref_nbr, act_name, login_user_id, mod_user_id,
        tran_nbr, created_source, last_updated_source, resend_tran, req_sam_reply, status, task_nbr,
        wave_nbr
    )
    values
    (
        va_lt_rec(i).labor_tran_id, va_lt_rec(i).vehicle_type, v_whse, sysdate, sysdate, 40, sysdate, 
        sysdate, '32', va_lt_rec(i).task_id, va_lt_rec(i).els_actvty_code, 'MONITOR', 'MONITOR', 
        va_lt_rec(i).labor_tran_id, p_user_id, p_user_id, 'N', 'N', 10, to_char(va_lt_rec(i).task_id),
        va_lt_rec(i).task_genrtn_ref_nbr
    );

    forall i in 1..va_lt_rec.count
    insert into labor_tran_dtl
    (
        labor_tran_dtl_id, labor_tran_id, tran_seq_nbr, loaded, qty, tc_ilpn_id, tc_olpn_id,
        item_name, item_bar_code, weight, volume, dsp_locn, locn_class, pallet_id, created_source,
        last_updated_source, tran_nbr
    )
    select labor_tran_dtl_id_seq.nextval, va_lt_rec(i).labor_tran_id, td.task_seq_nbr, 'Y',
        td.qty_alloc, td.cntr_nbr, td.carton_nbr, ic.item_name, ic.item_bar_code,
        coalesce(td.qty_alloc * ic.unit_weight, 0), coalesce(td.qty_alloc * ic.unit_volume, 0), 
        lh.locn_id, lh.locn_class, plt.tc_lpn_id, p_user_id, p_user_id, va_lt_rec(i).labor_tran_id
    from task_dtl td
    join item_cbo ic on ic.item_id = td.item_id
    left join locn_hdr lh on lh.locn_id = decode(va_lt_rec(i).menu_mode, 'I', td.pull_locn_id, td.dest_locn_id)
    left join lpn l on l.tc_lpn_id = td.cntr_nbr and l.tc_company_id = td.cd_master_id
        and l.inbound_outbound_indicator = 'I' and l.c_facility_id = v_facility_id
        and l.parent_lpn_id is not null
    left join lpn plt on plt.tc_lpn_id = l.tc_parent_lpn_id and plt.tc_company_id = l.tc_company_id
        and plt.inbound_outbound_indicator = 'I' and plt.c_facility_id = v_facility_id
    where task_id = va_lt_rec(i).task_id;

    update job_config jc
    set jc.last_proc_crit_1 = to_char(v_current_date, 'mm/dd/yyyy hh24:mi:ss'), 
        jc.modified_dttm = sysdate, jc.user_id = p_user_id
    where jc.job_config_id = p_job_config_id;

    v_num_rows := va_lt_rec.count;
    update job_hist jh
    set jh.modified_dttm = sysdate, jh.job_end_dttm = sysdate,
        jh.nbr_of_records_processed = v_num_rows
    where jh.job_hist_id = v_job_hist_id;

    commit;
end;
/
show errors;

create or replace procedure wm_add_joins_for_batch_invc
(
    p_rule_sql in out varchar2
)
as
begin
    if (instr(p_rule_sql, 'FACILITY.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            'join facility on facility.facility_id = lpn.d_facility_id '
            || ' where ');
    end if;

    if (instr(p_rule_sql, 'ORDERS.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            'join orders on orders.order_id = lpn.order_id '
            || ' where ');
    end if;

    if (instr(p_rule_sql, 'LPN_DETAIL.', 1, 1) > 0
        or instr(p_rule_sql, 'ORDER_NOTE.', 1, 1) > 0
        or instr(p_rule_sql, 'ITEM_CBO.', 1, 1) > 0
        or instr(p_rule_sql, 'ITEM_WMS.', 1, 1) > 0
        or instr(p_rule_sql, 'ITEM_FACILITY_MAPPING_WMS.', 1, 1) > 0
        or instr(p_rule_sql, 'ORDER_LINE_ITEM.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            'join lpn_detail on lpn_detail.lpn_id = lpn.lpn_id '
            || ' where ');
    end if;
    
    if (instr(p_rule_sql, 'ORDER_LINE_ITEM.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            'join order_line_item on order_line_item.order_id = lpn.order_id '
            || ' and order_line_item.line_item_id = lpn_detail.distribution_order_dtl_id '
            || ' where ');
    end if;

    if (instr(p_rule_sql, 'ORDER_NOTE.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            'join order_note on order_note.order_id = lpn.order_id '
            || ' and order_note.line_item_id = lpn_detail.distribution_order_dtl_id '
            || ' where ');
    end if;

    if (instr(p_rule_sql, 'ITEM_CBO.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            'join item_cbo on item_cbo.item_id = lpn_detail.item_id '
            || ' where ');
    end if;

    if (instr(p_rule_sql, 'ITEM_WMS.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            'join item_wms on item_wms.item_id = lpn_detail.item_id '
            || ' where ');
    end if;

    if (instr(p_rule_sql, 'ITEM_FACILITY_MAPPING_WMS.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            ' join item_facility_mapping_wms on '
            || ' item_facility_mapping_wms.item_id = lpn_detail.item_id '
            || ' and item_facility_mapping_wms.facility_id = lpn.c_facility_id '
            || ' where ');
    end if;

    if (instr(p_rule_sql, 'LOCN_HDR.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            'join locn_hdr on locn_hdr.locn_id = lpn.pick_sub_locn_id '
            || ' where ');
    end if;

    if (instr(p_rule_sql, 'PICK_LOCN_HDR.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            'join pick_locn_hdr on pick_locn_hdr.locn_id = lpn.pick_sub_locn_id '
            || ' where ');
    end if;

    if (instr(p_rule_sql, 'SHIPMENT.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            'join shipment on (shipment.shipment_id = lpn.shipment_id and shipment.tc_shipment_id = lpn.tc_shipment_id) '
            || ' and shipment.shipment_closed_indicator = 1 where ');
    end if;
    
    if (instr(p_rule_sql, 'MANIFEST_HDR.', 1, 1) > 0)
    then
        p_rule_sql := replace(p_rule_sql, 'where ',
            'join manifest_hdr on manifest_hdr.tc_manifest_id = lpn.manifest_nbr '
            || ' where ');
    end if;    
end;
/

CREATE OR REPLACE 
PACKAGE RATING_SUB_VALIDATION_PKG
AS
   FUNCTION VALIDATE_BUSINESS_UNIT (
      vTCCompanyId      IN company.COMPANY_ID%TYPE,
      vBusinessUnitId   IN business_unit.BUSINESS_UNIT%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_BUSINESS_UNIT (
      vBusinessUnitId IN business_unit.BUSINESS_UNIT%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_FACILITY (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                               vFacilityId    IN facility.FACILITY_ID%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_FACILITY_ALIAS (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vFacilityAliasId   IN facility_alias.FACILITY_ALIAS_ID%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_ZONE (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                           vZone          IN zone.ZONE_ID%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_STATE_PROV (vStateProv IN state_prov.STATE_PROV%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_POSTAL_CODE (
      vPostalCode IN postal_code.POSTAL_CODE%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_COUNTRY (vCountry IN country.COUNTRY_CODE%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_CARRIER_CODE (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vCarrierCode   IN carrier_code.CARRIER_CODE%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_CARRIER_STATUS (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vCarrierCode   IN carrier_code.CARRIER_CODE%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_MODE (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                           vMode          IN mot.MOT%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_SERVICE_LEVEL (
      vTCCompanyId    IN company.COMPANY_ID%TYPE,
      vServiceLevel   IN service_level.SERVICE_LEVEL%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_EQUIPMENT (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vEquipment     IN equipment.EQUIPMENT_CODE%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_PROTECTION_LEVEL (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vProtectionLevel   IN protection_level.PROTECTION_LEVEL%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_SIZE_UOM (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                               vSizeUOM       IN size_uom.SIZE_UOM%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_ACCESSORIAL_CODE (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vAccessorialCode   IN accessorial_code.ACCESSORIAL_CODE%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_DISTANCE_UOM (
      vDistanceUOM IN distance_uom.DISTANCE_UOM%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_COMMODITY_CLASS (
      vCommodityClass IN commodity_class.COMMODITY_CLASS%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_CUSTOMER (vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
                               vCustomerId    IN CUSTOMER.CUSTOMER_ID%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_CUSTOMER_CODE (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vCustomerCode   IN CUSTOMER.CUSTOMER_CODE%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_INCOTERM (vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
                               vIncotermId    IN INCOTERM.INCOTERM_ID%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_INCOTERM_NAME (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vIncotermName   IN INCOTERM.INCOTERM_NAME%TYPE)
      RETURN NUMBER;

   FUNCTION VALIDATE_SHIP_VIA (
      vShipViaBUID   IN COMPANY.COMPANY_ID%TYPE,
      vShipVia       IN FACILITY_ALIAS.FACILITY_ALIAS_ID%TYPE)
      RETURN NUMBER;

   FUNCTION validate_state_prov (vstateprov   IN state_prov.state_prov%TYPE,
                                 vcountry     IN country.country_code%TYPE)
      RETURN NUMBER;
END RATING_SUB_VALIDATION_PKG;
/

CREATE OR REPLACE 
PACKAGE BODY RATING_SUB_VALIDATION_PKG
AS
   FUNCTION VALIDATE_BUSINESS_UNIT (
      vTCCompanyId      IN company.COMPANY_ID%TYPE,
      vBusinessUnitId   IN business_unit.BUSINESS_UNIT%TYPE)
      RETURN NUMBER
   IS
      vPassFail            NUMBER;
      vBusinessUnitCount   NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (COMPANY_NAME)
        INTO vBusinessUnitCount
        FROM COMPANY
       WHERE PARENT_COMPANY_ID = vTCCompanyId AND COMPANY_NAME = vBusinessUnitId;

      IF (vBusinessUnitCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_BUSINESS_UNIT;

   FUNCTION VALIDATE_BUSINESS_UNIT (
      vBusinessUnitId IN business_unit.BUSINESS_UNIT%TYPE)
      RETURN NUMBER
   IS
      vPassFail            NUMBER;
      vBusinessUnitCount   NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (COMPANY_NAME)
        INTO vBusinessUnitCount
        FROM COMPANY
       WHERE               /*TC_COMPANY_ID = vTCCompanyId
                        AND*/
            COMPANY_NAME =
                vBusinessUnitId;

      IF (vBusinessUnitCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_BUSINESS_UNIT;

   FUNCTION VALIDATE_FACILITY (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                               vFacilityId    IN facility.FACILITY_ID%TYPE)
      RETURN NUMBER
   IS
      vPassFail        NUMBER;
      vFacilityCount   NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (FACILITY_ID)
        INTO vFacilityCount
        FROM FACILITY
       WHERE TC_COMPANY_ID = vTCCompanyId AND FACILITY_ID = vFacilityId;

      IF (vFacilityCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_FACILITY;

   FUNCTION VALIDATE_FACILITY_ALIAS (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vFacilityAliasId   IN facility_alias.FACILITY_ALIAS_ID%TYPE)
      RETURN NUMBER
   IS
      vPassFail             NUMBER;
      vFacilityAliasCount   NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (FACILITY_ALIAS_ID)
        INTO vFacilityAliasCount
        FROM FACILITY_ALIAS
       WHERE TC_COMPANY_ID = vTCCompanyId
             AND FACILITY_ALIAS_ID = vFacilityAliasId;

      IF (vFacilityAliasCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_FACILITY_ALIAS;

   FUNCTION VALIDATE_ZONE (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                           vZone          IN zone.ZONE_ID%TYPE)
      RETURN NUMBER
   IS
      vPassFail    NUMBER;
      vZoneCount   NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (ZONE_ID)
        INTO vZoneCount
        FROM ZONE
       WHERE TC_COMPANY_ID = vTCCompanyId AND ZONE_ID = vZone;

      IF (vZoneCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_ZONE;

   FUNCTION VALIDATE_STATE_PROV (vStateProv IN state_prov.STATE_PROV%TYPE)
      RETURN NUMBER
   IS
      vPassFail     NUMBER;
      vStateCount   NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (STATE_PROV)
        INTO vStateCount
        FROM STATE_PROV
       WHERE STATE_PROV = vStateProv;

      IF (vStateCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_STATE_PROV;

   FUNCTION VALIDATE_POSTAL_CODE (
      vPostalCode IN postal_code.POSTAL_CODE%TYPE)
      RETURN NUMBER
   IS
      vPassFail      NUMBER;
      vPostalCount   NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (POSTAL_CODE)
        INTO vPostalCount
        FROM POSTAL_CODE
       WHERE POSTAL_CODE = vPostalCode;

      IF (vPostalCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_POSTAL_CODE;

   FUNCTION VALIDATE_COUNTRY (vCountry IN country.COUNTRY_CODE%TYPE)
      RETURN NUMBER
   IS
      vPassFail       NUMBER;
      vCountryCount   NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (COUNTRY_CODE)
        INTO vCountryCount
        FROM COUNTRY
       WHERE COUNTRY_CODE = vCountry;

      IF (vCountryCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_COUNTRY;

   FUNCTION VALIDATE_CARRIER_CODE (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vCarrierCode   IN carrier_code.CARRIER_CODE%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
      vCount      NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (CARRIER_CODE)
        INTO vCount
        FROM CARRIER_CODE
       WHERE TC_COMPANY_ID = vTCCompanyId AND CARRIER_CODE = vCarrierCode;

      IF (vCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_CARRIER_CODE;

   FUNCTION VALIDATE_CARRIER_STATUS (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vCarrierCode   IN carrier_code.CARRIER_CODE%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
      vCount      NUMBER;
   BEGIN
      vPassFail := 0;

      --  Change 6/26/2003:  removed check for CARRIER_CODE_STATUS = 0 and replaced with MARK_FOR_DELETION = 0.
      --  We should always load rated regardless of the CARRIER_CODE_STATUS; however, if the CARRIER_CODE
      --  DOES NOT EXIST or is MARK_FOR_DELTION then an error should be triggered.
      SELECT COUNT (CARRIER_CODE)
        INTO vCount
        FROM CARRIER_CODE
       WHERE     TC_COMPANY_ID = vTCCompanyId
             AND CARRIER_CODE = vCarrierCode
             AND MARK_FOR_DELETION = 0;

      IF (vCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_CARRIER_STATUS;

   FUNCTION VALIDATE_MODE (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                           vMode          IN mot.MOT%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
      vCount      NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (MOT)
        INTO vCount
        FROM MOT
       WHERE     TC_COMPANY_ID = vTCCompanyId
             AND MOT = vMode
             AND MARK_FOR_DELETION = 0;

      IF (vCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_MODE;

   FUNCTION VALIDATE_SERVICE_LEVEL (
      vTCCompanyId    IN company.COMPANY_ID%TYPE,
      vServiceLevel   IN service_level.SERVICE_LEVEL%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
      vCount      NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (SERVICE_LEVEL)
        INTO vCount
        FROM SERVICE_LEVEL
       WHERE TC_COMPANY_ID = vTCCompanyId AND SERVICE_LEVEL = vServiceLevel;

      IF (vCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_SERVICE_LEVEL;

   FUNCTION VALIDATE_EQUIPMENT (
      vTCCompanyId   IN company.COMPANY_ID%TYPE,
      vEquipment     IN equipment.EQUIPMENT_CODE%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
      vCount      NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (EQUIPMENT_CODE)
        INTO vCount
        FROM EQUIPMENT
       WHERE TC_COMPANY_ID = vTCCompanyId AND EQUIPMENT_CODE = vEquipment;

      IF (vCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_EQUIPMENT;

   FUNCTION VALIDATE_PROTECTION_LEVEL (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vProtectionLevel   IN protection_level.PROTECTION_LEVEL%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
      vCount      NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (PROTECTION_LEVEL)
        INTO vCount
        FROM PROTECTION_LEVEL
       WHERE TC_COMPANY_ID = vTCCompanyId
             AND PROTECTION_LEVEL = vProtectionLevel;

      IF (vCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_PROTECTION_LEVEL;

   FUNCTION VALIDATE_SIZE_UOM (vTCCompanyId   IN company.COMPANY_ID%TYPE,
                               vSizeUOM       IN size_uom.SIZE_UOM%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
      vCount      NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (TC_COMPANY_ID)
        INTO vCount
        FROM SIZE_UOM
       WHERE TC_COMPANY_ID = vTCCompanyId AND SIZE_UOM = vSizeUOM;

      IF (vCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_SIZE_UOM;

   FUNCTION VALIDATE_ACCESSORIAL_CODE (
      vTCCompanyId       IN company.COMPANY_ID%TYPE,
      vAccessorialCode   IN accessorial_code.ACCESSORIAL_CODE%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
      vCount      NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (ACCESSORIAL_CODE)
        INTO vCount
        FROM ACCESSORIAL_CODE
       WHERE TC_COMPANY_ID = vTCCompanyId
             AND ACCESSORIAL_CODE = vAccessorialCode;

      IF (vCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_ACCESSORIAL_CODE;

   FUNCTION VALIDATE_DISTANCE_UOM (
      vDistanceUOM IN distance_uom.DISTANCE_UOM%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
      vCount      NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (distance_uom)
        INTO vCount
        FROM DISTANCE_UOM
       WHERE DISTANCE_UOM = vDistanceUOM;

      IF (vCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_DISTANCE_UOM;

   FUNCTION VALIDATE_COMMODITY_CLASS (
      vCommodityClass IN commodity_class.COMMODITY_CLASS%TYPE)
      RETURN NUMBER
   IS
      vPassFail   NUMBER;
      vCount      NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (commodity_class)
        INTO vCount
        FROM COMMODITY_CLASS
       WHERE commodity_class = vCommodityClass;

      IF (vCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_COMMODITY_CLASS;

   FUNCTION VALIDATE_CUSTOMER (vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
                               vCustomerId    IN CUSTOMER.CUSTOMER_ID%TYPE)
      RETURN NUMBER
   IS
      vPassFail        NUMBER;
      vCustomerCount   NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (CUSTOMER_ID)
        INTO vCustomerCount
        FROM CUSTOMER
       WHERE TC_COMPANY_ID = vTCCompanyId AND CUSTOMER_ID = vCustomerId;

      IF (vCustomerCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_CUSTOMER;

   FUNCTION VALIDATE_CUSTOMER_CODE (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vCustomerCode   IN CUSTOMER.CUSTOMER_CODE%TYPE)
      RETURN NUMBER
   IS
      vPassFail        NUMBER;
      vCustomerCount   NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (CUSTOMER_ID)
        INTO vCustomerCount
        FROM CUSTOMER
       WHERE TC_COMPANY_ID = vTCCompanyId AND CUSTOMER_CODE = vCustomerCode;

      IF (vCustomerCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_CUSTOMER_CODE;

   FUNCTION VALIDATE_INCOTERM (vTCCompanyId   IN COMPANY.COMPANY_ID%TYPE,
                               vIncotermId    IN INCOTERM.INCOTERM_ID%TYPE)
      RETURN NUMBER
   IS
      vPassFail        NUMBER;
      vIncotermCount   NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (INCOTERM_ID)
        INTO vIncotermCount
        FROM INCOTERM
       WHERE INCOTERM_ID = vIncotermId;

      IF (vIncotermCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_INCOTERM;

   FUNCTION VALIDATE_INCOTERM_NAME (
      vTCCompanyId    IN COMPANY.COMPANY_ID%TYPE,
      vIncotermName   IN INCOTERM.INCOTERM_NAME%TYPE)
      RETURN NUMBER
   IS
      vPassFail        NUMBER;
      vIncotermCount   NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (INCOTERM_ID)
        INTO vIncotermCount
        FROM INCOTERM
       WHERE INCOTERM_NAME = vIncotermName;

      IF (vIncotermCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_INCOTERM_NAME;

   FUNCTION VALIDATE_SHIP_VIA (
      vShipViaBUID   IN COMPANY.COMPANY_ID%TYPE,
      vShipVia       IN FACILITY_ALIAS.FACILITY_ALIAS_ID%TYPE)
      RETURN NUMBER
   IS
      vPassFail       NUMBER;
      vShipViaCount   NUMBER;
      vShipViaId      NUMBER;
      vFacTypeBits    SMALLINT;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (FACILITY_ID)
        INTO vShipViaCount
        FROM FACILITY_ALIAS
       WHERE TC_COMPANY_ID = vShipViaBUID AND FACILITY_ALIAS_ID = vShipVia;

      IF (vShipViaCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      IF (vPassFail = 0)
      THEN
         SELECT FACILITY_ID
           INTO vShipViaId
           FROM FACILITY_ALIAS
          WHERE TC_COMPANY_ID = vShipViaBUID AND FACILITY_ALIAS_ID = vShipVia;

         -- CHECKING FOR TYPE PORT
         SELECT BITAND (FACILITY_TYPE_BITS, 16)
           INTO vFacTypeBits
           FROM FACILITY
          WHERE FACILITY_ID = vShipViaId;

         IF (vFacTypeBits <> 16)
         THEN
            vPassFail := 1;
         END IF;
      END IF;

      RETURN vPassFail;
   END VALIDATE_SHIP_VIA;

   FUNCTION VALIDATE_STATE_PROV (vStateProv   IN state_prov.STATE_PROV%TYPE,
                                 vCountry     IN COUNTRY.COUNTRY_CODE%TYPE)
      RETURN NUMBER
   IS
      vPassFail     NUMBER;
      vStateCount   NUMBER;
   BEGIN
      vPassFail := 0;

      SELECT COUNT (STATE_PROV)
        INTO vStateCount
        FROM STATE_PROV
       WHERE STATE_PROV = vStateProv AND COUNTRY_CODE = vCountry;

      IF (vStateCount < 1)
      THEN
         vPassFail := 1;
      END IF;

      RETURN vPassFail;
   END VALIDATE_STATE_PROV;
END RATING_SUB_VALIDATION_PKG;
/



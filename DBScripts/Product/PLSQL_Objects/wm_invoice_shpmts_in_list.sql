create or replace procedure wm_invoice_shpmts_in_list
(
    p_invc_batch_nbr    in number,
    p_user_id           in user_profile.user_id%type,
    p_tc_company_id     in number
)
as
begin
    -- per Shashank, do not check for non cubed qty when invoicing a shpmt
    insert into outpt_shipment_retn
    (
        invc_batch_nbr,tc_shipment_id, plt_type, track_value, proc_stat_code,
        track_ob_retn_level, retn_track_qty, proc_dttm
    )
    select p_invc_batch_nbr, s.tc_shipment_id, ros.plt_type, ros.track_value, 0,
        ros.track_ob_retn_level, sum(coalesce(ros.retn_track_qty, 0)), sysdate
    from retn_ob_summ ros
    join retn_ob_hist roh on roh.whse = ros.whse and roh.plt_type = ros.plt_type
        and roh.track_value = ros.track_value
        and roh.track_ob_retn_level = ros.track_ob_retn_level
    join shipment s on s.tc_shipment_id = roh.load_nbr
        and s.tc_company_id = p_tc_company_id
        and s.shipment_status between 20 and 80
        and s.shipment_closed_indicator = 1
    where exists
        (
            select 1
            from tmp_invc_lpn_list t
            where t.shipment_id = s.shipment_id
        )
        and not exists
        (
            select 1
            from lpn l
            where l.shipment_id = s.shipment_id and l.lpn_facility_status < 90
        )
    group by s.tc_shipment_id, ros.plt_type, ros.track_value,
        ros.track_ob_retn_level
    log errors (to_char(sysdate));
    wm_cs_log('Outpt_shipment_retn insert ' || sql%rowcount);

    update shipment s
    set s.wms_status_code = 80, s.shipment_status = 80,
        s.last_updated_source = p_user_id, s.last_updated_dttm = sysdate
    where s.shipment_status between 20 and 80
        and s.shipment_closed_indicator = 1
        and exists
        (
            select 1
            from tmp_invc_lpn_list t
            where t.shipment_id = s.shipment_id
        )
        and not exists
        (
            select 1
            from lpn l
            where l.shipment_id = s.shipment_id and l.lpn_facility_status < 90
        );
    wm_cs_log('Shipments invoiced ' || sql%rowcount);
end;
/

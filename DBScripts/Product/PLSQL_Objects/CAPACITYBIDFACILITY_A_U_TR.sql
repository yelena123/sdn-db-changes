
CREATE OR REPLACE TRIGGER CAPACITYBIDFACILITY_A_U_TR
   AFTER UPDATE
   ON CAPACITYBIDFACILITY
   REFERENCING OLD AS O NEW AS N
   FOR EACH ROW
BEGIN
   CAPACITYBIDFACILITY_A_U_PROC (:N.LAST_UPDATED_SOURCE_TYPE,
                                 :N.LAST_UPDATED_SOURCE,
                                 :O.RFPID,
                                 :N.ROUND_NUM,
                                 :O.OBCARRIERCODEID,
                                 :O.FACILITYCODE,
								 :O.AREACODE,
                                 :O.DIRECTION,
                                 :O.WEEKLYCAPACITY,
                                 :N.WEEKLYCAPACITY,
                                 :O.SURGECAPACITY,
                                 :N.SURGECAPACITY);
END;
/



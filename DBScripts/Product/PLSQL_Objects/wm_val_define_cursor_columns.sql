create or replace procedure wm_val_define_cursor_columns
(
    p_cursor          in number,
    p_sql_id          in val_job_dtl.sql_id%type,
    p_txn_id          in val_result_hist.txn_id%type
)
as
    c_max_char_len      constant number(3) := 50;
    v_int_col           number(9);
    v_varchar2_col      val_expected_results.expected_value%type;
begin
    -- define datatype for each selected column in the sql
    for col_rec in
    (
        select vpsl.pos, vpsl.col_type
        from val_parsed_select_list vpsl
        where vpsl.sql_id = p_sql_id
        order by vpsl.pos
    )
    loop
        if (col_rec.col_type = 2)
        then
            dbms_sql.define_column(p_cursor, col_rec.pos, v_int_col);
            if (p_txn_id is not null)
            then
                wm_val_log('Defined integer col at pos ' || col_rec.pos, p_sql_log_lvl => 2);
            end if;
        elsif (col_rec.col_type in (1, 96))
        then
            dbms_sql.define_column(p_cursor, col_rec.pos, v_varchar2_col, c_max_char_len);
            if (p_txn_id is not null)
            then
                wm_val_log('Defined varchar2 col at pos ' || col_rec.pos, p_sql_log_lvl => 2);
            end if;
        else
            raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to finding unsupported data type '
                || col_rec.col_type);
        end if;
    end loop;
end;
/
show errors;

create or replace procedure wm_task_creation_intrnl
(
    p_user_id                 in ucl_user.user_name%type,
    p_facility_id             in facility.facility_id%type,
    p_tc_company_id           in wave_parm.tc_company_id%type,
    p_task_genrtn_ref_nbr_csv in varchar2,
    p_task_genrtn_ref_code    in alloc_invn_dtl.task_genrtn_ref_code%type,
    p_need_id                 in alloc_invn_dtl.need_id%type,
    p_crit_nbr                in task_parm.crit_nbr%type
)
as
    v_prev_module_name      wm_utils.t_app_context_data;
    v_prev_action_name      wm_utils.t_app_context_data;
    v_max_log_lvl           number(1);
    v_code_id               sys_code.code_id%type := '008';
    v_whse                  facility.whse%type;
    v_rls_date_time         task_hdr.rls_date_time%type := sysdate;
begin
    wm_utils.get_context_info(v_prev_module_name, v_prev_action_name);
    wm_utils.set_context_info(p_module_name => case p_task_genrtn_ref_code when '1' then 'PICK WAVE'
        when '4' then 'PNH WAVE' when '44' then 'PACK WAVE RLS' else 'NON WAVE' end, 
        p_action_name => 'TASK CREATION', p_client_id => p_task_genrtn_ref_nbr_csv);	

    select to_number(coalesce(trim(substr(sc.misc_flags, 8, 1)), '0'))
    into v_max_log_lvl
    from sys_code sc 
    where sc.rec_type = 'S' and sc.code_type = '520' and sc.code_id = v_code_id;

    select f.whse
    into v_whse
    from facility f
    where f.facility_id = p_facility_id;

    delete from tmp_task_lpn_min_locn_seq;
    delete from tmp_log_parm;
    insert into tmp_log_parm
    (
        log_lvl, pgm_id, module, msg_id, ref_code_1, ref_value_1, user_id, whse, cd_master_id
    )
    values
    (
        v_max_log_lvl, 'wm_task_creation_intrnl', 'WAVE', '1094', v_code_id,
        coalesce(substr(p_task_genrtn_ref_nbr_csv, 1, 128), p_need_id), p_user_id, v_whse, 
        coalesce(p_tc_company_id, wm_utils.wm_get_user_bu(p_user_id))
    );
    wm_cs_log('Beginning task creation');

    for tp_rec in
    (
        -- pick wave, pnh wave
        select tp.task_parm_id, tp.crit_nbr need_id,
            p_task_genrtn_ref_nbr_csv task_genrtn_ref_nbr_csv
        from task_parm tp
        join wave_task_crit wtc on wtc.crit_nbr = tp.crit_nbr and wtc.whse = tp.whse
            and wtc.wave_nbr = p_task_genrtn_ref_nbr_csv
        where p_task_genrtn_ref_code in ('1', '4') and tp.whse = v_whse and tp.rec_type = 'N'
            and tp.need_type = 'A'
        -- pack wave
        union all
        select tp.task_parm_id, tp.crit_nbr need_id,
            p_task_genrtn_ref_nbr_csv task_genrtn_ref_nbr_csv
        from task_parm tp
        where p_task_genrtn_ref_code = '44' and tp.whse = v_whse and tp.rec_type = 'N'
            and tp.need_type = 'A'
            and tp.crit_nbr in
            (
                select pwph.crit_nbr
                from pack_wave_parm_hdr pwph 
                join pack_wave_parm_dtl pwpd on pwpd.pack_wave_parm_id = pwph.pack_wave_parm_id
                join table(cl_parse_endpoint_name_csv(p_task_genrtn_ref_nbr_csv)) t
                    on t.column_value = pwpd.pack_wave_nbr
            )
        -- need_id flows
        union all
        select tp.task_parm_id, p_need_id need_id, null task_genrtn_ref_nbr_csv
        from task_parm tp 
        where tp.crit_nbr = p_crit_nbr and tp.whse = v_whse and tp.rec_type = 'N'
            and tp.need_type = 'A'
    )
    loop
        -- commits internally per rule
        wm_crt_tasks_for_task_parm(tp_rec.task_parm_id, p_user_id, p_facility_id, v_whse, 
            coalesce(p_tc_company_id, wm_utils.wm_get_user_bu(p_user_id)), tp_rec.task_genrtn_ref_nbr_csv, p_task_genrtn_ref_code, 
            tp_rec.need_id, v_rls_date_time);
    end loop;

    wm_cs_log('Completed task creation');
    wm_utils.set_context_info(p_module_name => v_prev_module_name,
        p_action_name => v_prev_action_name);
end;
/
show errors;

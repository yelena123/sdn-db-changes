create or replace function wm_val_get_col_list_for_sql
(
    p_sql_id        in val_sql.sql_id%type
)
return varchar2
as
    v_col_list_csv  varchar2(1000);
begin
    select listagg(vpsl.col_name, ',') within group(order by vpsl.pos)
    into v_col_list_csv
    from val_parsed_select_list vpsl
    where vpsl.sql_id = p_sql_id;
    
    return v_col_list_csv;
end;
/
show errors;

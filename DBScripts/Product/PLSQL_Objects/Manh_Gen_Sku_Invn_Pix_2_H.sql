CREATE OR REPLACE PACKAGE Manh_Gen_Sku_Invn_Pix
IS
    -- This package defines the process of PM 672

    -- This procedure runs the Sku Inventory PIX process depending on the parameters passed.
    -- #param p_Login_User_Id [<i>Required</i>] Specifies the User Id value. The value must be a valid <code>USER_MASTER.LOGIN_USER_ID</code>.
    -- #param p_Warehouse [<i>Optional</i>] Specifies the Warehouse value. If not passed, the <code>WHSE</code> value for the Login_User_Id will be used; <b>Default: NULL</b>
    -- #param p_Company [<i>Optional</i>] Specifies the Company value. If not passed, the <code>CO</code> value for the Login_User_Id will be used; <b>Default: NULL</b>
    -- #param p_Division [<i>Optional</i>] Specifies the Division value. If not passed, the <code>DIV</code> value for the Login_User_Id will be used; <b>Default: NULL</b>
    -- #param p_Lock_Time_Out [<i>Optional</i>] Specifies the Wait time in number of seconds proir to acquiring a Lock. <b>Default: 10</b>
    -- #param p_Default_Pix_Level [<i>Optional</i>] Specifies the Level of PIX generation.  Valid values are 'D'-Detail, 'S'-Summary; <b>Default: 'D'</b>
    -- #param p_Case_Lock_Code [<i>Optional</i>] Specifies whether to summarize by <code>CASE_LOCK.INVN_LOCK_CODE</code> for Un-allocable Case Inventory. Valid values are 'Y'-Yes, 'N'-No; <b>Default: 'N'</b>
    -- #param p_Trans_Invn_Type [<i>Optional</i>] Specifies whether to summarize by <code>TRANS_INVN.TRANS_INVN_TYPE</code> for Un-allocable Transitional Inventory. Valid values are 'Y'-Yes, 'N'-No; <b>Default: 'N'</b>
    -- #param p_Update_Unproc_605_Pix [<i>Optional</i>] Specifies whether to update any existsing <code>PIX_TRAN</code> records to processed; <b>Default: 'N'</b>
    -- #param p_Include_UnAlloc_Invn [<i>Optional</i>] Specifies whether to include Un-Allocable Inventory records during PIX generation; <b>Default: 'N'</b>
    -- #param p_Group_Sku_Atributes [<i>Optional</i>] Specifies whether the PIXs needs to be grouped by SKU Attributes as well during PIX generation; <b>Default: 'N'</b>
    -- #param p_Include_Cons_Prty_Date [<i>Optional</i>] Specifies whether the PIXs need to be group by <code>CONS_PRTY_DATE</code>. Valid values are 'Y'-Yes, 'N'-No; <b>Default: 'N'</b>
    -- #param p_Include_Srl_Nbr_Pix [<i>Optional</i>] Specifies whether to process Serial# PIXs. Valid values are 'Y'-Yes, 'N'-No; <b>Default: 'N'</b>
    -- #param p_Season [<i>Optional</i>] Specifies whether to filter by <code>SEASON</code> SKU Component. If <code>NULL</code> this parameter will be ignored. <b>Default: NULL</b>
    -- #param p_Season_Yr [<i>Optional</i>] Specifies whether to filter by <code>SEASON_YR</code> SKU Component. If <code>NULL</code> this parameter will be ignored. <b>Default: NULL</b>
    -- #param p_Style [<i>Optional</i>] Specifies whether to filter by <code>STYLE</code> SKU Component. If <code>NULL</code> this parameter will be ignored. <b>Default: NULL</b>
    -- #param p_Style_Sfx [<i>Optional</i>] Specifies whether to filter by <code>STYLE_SFX</code> SKU Component. If <code>NULL</code> this parameter will be ignored. <b>Default: NULL</b>
    -- #param p_Color [<i>Optional</i>] Specifies whether to filter by <code>COLOR</code> SKU Component. If <code>NULL</code> this parameter will be ignored. <b>Default: NULL</b>
    -- #param p_Color_Sfx [<i>Optional</i>] Specifies whether to filter by <code>COLOR_SFX</code> SKU Component. If <code>NULL</code> this parameter will be ignored. <b>Default: NULL</b>
    -- #param p_Sec_Dim [<i>Optional</i>] Specifies whether to filter by <code>SEC_DIM</code> SKU Component. If <code>NULL</code> this parameter will be ignored. <b>Default: NULL</b>
    -- #param p_Qual [<i>Optional</i>] Specifies whether to filter by <code>QUAL</code> SKU Component. If <code>NULL</code> this parameter will be ignored. <b>Default: NULL</b>
    -- #param p_Size_Desc [<i>Optional</i>] Specifies whether to filter by <code>SIZE_DESC</code> SKU Component. If <code>'*'</code> all INVN_TYPE values are considered. <b>Default: NULL</b>
    -- #param p_Invn_Type [<i>Optional</i>] Specifies whether to filter by <code>INVN_TYPE</code> SKU Attribute.  If <code>NULL</code> this parameter will be ignored. Valid values are '*'-All, 'F'-Finished, 'R'-Raw; <b>Default: '*'</b>
    -- #param p_Prod_Stat [<i>Optional</i>] Spefifies whether to filter by <code>PROD_STAT</code> SKU Attribute.  If <code>NULL</code> this parameter will be ignored. <b>Default: NULL</b>
    -- #param p_Create_Zero_Inventory [<i>Optional</i>] Specifies whether to create PIX even if inventory is zero.  Valid values are 'Y'-Yes, 'N'-No; <b>Default: 'N'</b>
    -- #param p_Reconcile [<i>Optional</i>] Specifies whether Reconcilation needs to done as well.  Valid values are 'Y'-Yes, 'N'-No; <b>Default: 'N'</b>
    -- #param p_Auto_Apply_Variance [<i>Optional</i>] Specifies whether the Variances need to be applied by this process.  Valid values are 0-No, 1-Manual, 2-UI only; <b>Default: 0</b>
    -- #param p_Debug [<i>Optional</i>] Specifies whether debug information should be displayed.  Valid values are 'Y'-Yes, 'N'-No; <b>Default: 'N'</b>
    -- #param p_ReturnCode [<i>Required</i>] This is the return parameter defined as INT datatype. Returns a 0 for success; else returns 1 for failure
    PROCEDURE Generate_Pix
    (
        p_login_user_id          IN       VARCHAR2,
      p_company_id               IN       NUMBER DEFAULT NULL,
      p_facility_id              IN       NUMBER DEFAULT NULL,    
      p_lock_time_out            IN       VARCHAR2 DEFAULT 10,
      p_default_pix_level        IN       VARCHAR2 DEFAULT 'D',
      p_case_lock_code           IN       VARCHAR2 DEFAULT 'N',
      p_active_summ_by_lock      IN       varchar2 default 'N',
      p_CasePick_summ_by_Lock    IN       VARCHAR2 DEFAULT 'N',
      p_trans_invn_type          IN       VARCHAR2 DEFAULT 'N',
      p_update_unproc_605_pix    IN       VARCHAR2 DEFAULT 'N',
      p_include_unalloc_invn     IN       VARCHAR2 DEFAULT 'N',
      p_group_sku_attributes     IN       VARCHAR2 DEFAULT 'N',
      p_include_cons_prty_date   IN       VARCHAR2 DEFAULT 'N',
      p_include_srl_nbr_pix      IN       VARCHAR2 DEFAULT 'N',
      p_season                   IN       VARCHAR2 DEFAULT NULL,
      p_season_yr                IN       VARCHAR2 DEFAULT NULL,
      p_style                    IN       VARCHAR2 DEFAULT NULL,
      p_style_sfx                IN       VARCHAR2 DEFAULT NULL,
      p_color                    IN       VARCHAR2 DEFAULT NULL,
      p_color_sfx                IN       VARCHAR2 DEFAULT NULL,
      p_sec_dim                  IN       VARCHAR2 DEFAULT NULL,
      p_qual                     IN       VARCHAR2 DEFAULT NULL,
      p_size_desc                IN       VARCHAR2 DEFAULT NULL,
      p_invn_type                IN       VARCHAR2 DEFAULT '*',
      p_prod_stat                IN       VARCHAR2 DEFAULT NULL,
      p_create_zero_inventory    IN       VARCHAR2 DEFAULT 'N',
      p_reconcile                IN       VARCHAR2 DEFAULT 'N',
      p_auto_apply_variance      IN       INT DEFAULT 0,
      p_debug                    IN       VARCHAR2 DEFAULT 'N',
      p_item_name                IN       VARCHAR2,
      p_returncode               OUT      INT
    );
END Manh_Gen_Sku_Invn_Pix;
/
create or replace procedure manh_aggr_build_col_list
(
    p_tc_company_id   	 in  number,
    p_select_list        out varchar2,
    p_where_clause       out varchar2
)
as
    v_col_select_list    varchar2(4000) ;
    v_col_where_list     varchar2(4000) ;
    v_col_where_list_tmp varchar2(200) ;
    v_root_bu            company.company_id%type;
begin
    -- assumption!the config is at the enterprise level (although currently defined on company parm).
    -- it is easier to add a future extension to this enhc if the config is made at the origin facility level
    -- it is not supported at the actual company level
    -- wm assumes a 2 level hierarchy, per Ravi M.
    select coalesce(nullif(c.parent_company_id, -1), c.company_id)
    into v_root_bu
    from company c
    where c.company_id = p_tc_company_id;    
    
    for cur_rec in 
    ( 
        select param_def_id 
        from company_parameter 
        where param_def_id in 
            ( 
                select param_def_id 
                from param_def 
                where param_group_id = 'COM_WM' 
                    and param_subgroup_id = 'AGG_CRIT'
            )
            and tc_company_id = v_root_bu and param_value = 'true'
            and param_def_id not in ('D_FACILITY_ID', 'TC_COMPANY_ID')
    ) 
    loop
        v_col_select_list := v_col_select_list || ',' || cur_rec.param_def_id ;
        
        select 
            case 
                when nullable = 'N' then 'and orig.'|| column_name || ' = agg.' || column_name
                when nullable = 'Y' then
                case 
                    when data_type = 'VARCHAR2' or data_type = 'CHAR'
                        then 'and (coalesce(orig.' || column_name ||  ', '' '') = coalesce(agg.'
                             || column_name || ', '' '')) '
                    when data_type = 'NUMBER'
                        then 'and (coalesce(orig.' || column_name ||  ', 0) = coalesce(agg.'
                             || column_name || ', 0)) '
                    when data_type = 'DATE'
                        then 'and (coalesce(to_char(orig.' || column_name ||  '), '' '') = coalesce(to_char(agg.'
                             || column_name || '), '' '')) '
                end
            end
        into v_col_where_list_tmp
        from user_tab_columns
        where table_name = 'ORDERS'
            and upper(column_name) = upper ( cur_rec.param_def_id ); 
   
        v_col_where_list := v_col_where_list || ' ' ||   v_col_where_list_tmp ;
    end loop;
    
    p_select_list := v_col_select_list ;
    p_where_clause := v_col_where_list ;    
end ;
/
show errors;

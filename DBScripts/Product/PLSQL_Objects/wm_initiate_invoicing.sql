create or replace procedure wm_initiate_invoicing
(
    p_invc_batch_nbr    in number,
    p_user_id           in varchar2,
    p_whse              in facility.whse%type,
    p_tc_company_id     in number,
    p_debug_flag        in number,
    p_lock_time_out     in number,
    p_lang_id           in varchar2,
    p_list_of_shpmts    in varchar2,
    p_list_of_orders    in varchar2,
    p_invc_parm_id      in invc_parm.invc_parm_id%type,
    p_pre_bill_flag     in number default 0,
    p_list_of_manifests in varchar2 default null
)
as
    v_pre_bill_flag      number(1);
    v_genrt_carton_asn   number(1);
    v_srl_trk_flag       number(1); 
    v_check_manifest     number(1);
    v_facility_id        facility.facility_id%type;
    v_max_log_lvl        number(1);
    v_mode_id            sys_code.code_id%type := '014';
    v_ref_value_1        msg_log.ref_value_1%type := to_char(p_invc_batch_nbr) || '|'
        || coalesce(substr(p_list_of_shpmts, 1, 115), substr(p_list_of_orders, 1, 115), 
            substr(p_list_of_manifests, 1, 115), to_char(p_invc_parm_id));
    v_prev_module_name   wm_utils.t_app_context_data;
    v_prev_action_name   wm_utils.t_app_context_data;
    v_pgm_id             constant val_job_seq.pgm_id%type := 'wm_initiate_invoicing';
    v_parent_txn_id      val_result_hist.parent_txn_id%type;
    v_rt_bind_list_csv   varchar2(4000);
    v_invoke_val         number(1);
    v_num_ord_to_process number(5) := 0;
    v_commit_freq        number(5) := 0;
    v_invc_ship_or_manif number(1) :=0;
begin
    if (p_list_of_shpmts is null and p_list_of_orders is null and p_list_of_manifests is null
        and p_invc_parm_id is null)
    then
        raise_application_error(-20050, 'Need to wave by DO or shpmt or manifest or batch.');
    end if;

    select f.facility_id
    into v_facility_id
    from facility f
    where f.whse = p_whse;

    v_rt_bind_list_csv := 'whse,' || p_whse || ',i_facility_id,' || v_facility_id 
        || ',i_tc_company_id,' || p_tc_company_id || ',i_invc_batch_nbr,' || p_invc_batch_nbr 
        || ',list_of_shpmts,' || p_list_of_shpmts || ',list_of_orders,' || p_list_of_orders 
        || ',i_invc_parm_id,' || p_invc_parm_id || ',list_of_manifests,' || p_list_of_manifests 
        || ',';    
    v_invc_ship_or_manif := case when p_list_of_shpmts is not null then 1 
        when p_list_of_manifests is not null then 1 else 0 end;
    
    select to_number(coalesce(trim(substr(sc.misc_flags, 3, 5)), '99999')), 
        to_number(coalesce(trim(substr(sc.misc_flags, 8, 1)), '0'))
    into v_commit_freq, v_max_log_lvl
    from sys_code sc 
    where sc.rec_type = 'S' and sc.code_type = '520' and sc.code_id = v_mode_id;
    v_commit_freq := case v_commit_freq when 0 then 99999 else v_commit_freq end;
    
    delete from tmp_log_parm;
    insert into tmp_log_parm
    (
        log_lvl, pgm_id, module, msg_id, ref_code_1, ref_value_1, user_id, whse, cd_master_id
    )
    values
    (
        v_max_log_lvl, 'wm_initiate_invoicing', 'WAVE', '1094', v_mode_id, v_ref_value_1, p_user_id,
        p_whse, p_tc_company_id
    );
    
    wm_utils.get_context_info(v_prev_module_name, v_prev_action_name);
    wm_utils.set_context_info(p_module_name => 'INVOICING', p_action_name => 
        case 
            when p_list_of_shpmts is not null then 'INVOICE BY ORDER'
            when p_list_of_orders is not null then 'INVOICE BY SHIPMENT'
            else 'INVOICE BY RULES'
        end,
        p_client_id => v_ref_value_1);
        
    select case when exists
    (
        select 1
        from sys_code sc
        where sc.rec_type = 'S' and sc.code_type = 'D56' and sc.code_id = '002'
            and substr(sc.misc_flags, 1, 1) = 'Y'
    ) then 1 else 0 end
    into v_invoke_val
    from dual;

    if (v_invoke_val = 1)
    then
        wm_val_exec_job (p_user_id, p_app_hook => 'ENTRY', p_pgm_id => v_pgm_id, 
            p_rt_bind_list_csv => v_rt_bind_list_csv, p_log_lvl => v_max_log_lvl, 
            p_parent_txn_id => v_parent_txn_id);
    end if;
    
    wm_cs_log('Invoicing started'); 
    select case when upper(coalesce(wp.genrt_carton_asn, 'Y')) in ('Y', '1') then 1 else 0 end,
        case when wp.srl_trk_flag = '1' then 1 else 0 end
    into v_genrt_carton_asn, v_srl_trk_flag
    from whse_parameters wp
    where wp.whse_master_id = v_facility_id;
    v_pre_bill_flag := case when p_pre_bill_flag = 1 and v_genrt_carton_asn = 1 then 2 
        else p_pre_bill_flag end;

    select case when exists
    (
        select 1
        from sys_code sc
        where sc.rec_type = 'S' and sc.code_type = '445' and sc.code_id = '1'
            and substr(sc.misc_flags, 1, 1) = 'Y'
    ) then 1 else 0 end
    into v_check_manifest
    from dual;
    
    -- clean up of the master tables before load
    delete from tmp_invc_lpn_mastr_list t;           
    delete from tmp_invc_order_mastr_list t;
    
    -- load the master tables here
    if (p_invc_parm_id > 0)
    then
        -- by batch invoicing rules
        wm_invc_load_lpns_for_rule(p_whse, p_invc_parm_id, v_check_manifest);
    else
        -- by DO/shpmt; for invoice by DO, we also load orders
        wm_invc_load_lpns_via_list(p_list_of_shpmts, p_list_of_orders, p_list_of_manifests,
            v_check_manifest, v_pre_bill_flag, v_facility_id, v_num_ord_to_process);
    end if;

    if (p_list_of_orders is null)
    then
        -- we exclude invoice by DO here because we already captured the orders
        -- list along with the lpns earlier - that flow could also have orders
        -- with zero lpns; pre bill calls are always invoice-by-DO
        insert into tmp_invc_order_mastr_list
        (
            order_id
        )
        select distinct t.order_id
        from tmp_invc_lpn_mastr_list t;    
        v_num_ord_to_process := sql%rowcount;
        wm_cs_log('Total Orders collected ' || v_num_ord_to_process);
    end if;
    
    -- total order count drives the commit freq loop
    -- if commit freq not set then process as batch
    -- else process as per the count in loop
    -- todo! write messages per loop; i - loop number?
    loop
        if (v_invc_ship_or_manif = 1 or v_commit_freq = 99999)
        then 
            -- load all data into scratch tables
            insert into tmp_invc_order_list
            (
                order_id
            )
            select t.order_id
            from tmp_invc_order_mastr_list t;
            
            insert into tmp_invc_lpn_list
            (
                lpn_id, order_id, tc_lpn_id, lpn_facility_status, shipment_id,
                manifest_nbr, tc_parent_lpn_id
            )
            select l.lpn_id, l.order_id, l.tc_lpn_id, l.lpn_facility_status, l.shipment_id,
                decode(v_check_manifest, 1, l.manifest_nbr, null), l.tc_parent_lpn_id
            from lpn l
            join tmp_invc_lpn_mastr_list t on t.lpn_id = l.lpn_id;
        else 
            insert into tmp_invc_order_list
            (
                order_id
            )
            select t.order_id
            from tmp_invc_order_mastr_list t
            where rownum <= v_commit_freq;
            wm_cs_log('Orders collected ' || sql%rowcount);
            
            -- load data into scratch tables per commit freq quota
            insert into tmp_invc_lpn_list
            (
                lpn_id, order_id, tc_lpn_id, lpn_facility_status, shipment_id,
                manifest_nbr, tc_parent_lpn_id
            )
            select l.lpn_id, l.order_id, l.tc_lpn_id, l.lpn_facility_status, l.shipment_id,
                decode(v_check_manifest, 1, l.manifest_nbr, null), l.tc_parent_lpn_id
            from lpn l
            join tmp_invc_lpn_mastr_list t on t.lpn_id = l.lpn_id
            where exists
            (
                select 1
                from tmp_invc_order_list t1
                where t1.order_id = t.order_id
            );
            wm_cs_log('lpn tagged to orders being processed ' || sql%rowcount);
        end if;
    
        if (v_pre_bill_flag = 0)
        then
            --todo! add skip logic for commit freq batch for log_and_abort action
            wm_validate_lpns_for_invoicing(p_user_id, p_lang_id, p_whse, v_check_manifest);
        end if;
   
        update tmp_invc_order_list t
        set (t.do_status, t.prtl_ship_conf_flag) =
        (
            select o.do_status, o.prtl_ship_conf_flag
            from orders o
            where o.order_id = t.order_id
        );
        wm_cs_log('Orders fed to invoicing ' || sql%rowcount);
    
        if (v_invoke_val = 1)
        then
            wm_val_exec_job(p_user_id, p_app_hook => 'POST_DATA_LOAD', p_pgm_id => v_pgm_id, 
                p_rt_bind_list_csv => v_rt_bind_list_csv, p_log_lvl => v_max_log_lvl, 
                p_parent_txn_id => v_parent_txn_id);
	    	end if;
    
        if (v_pre_bill_flag = 0)
        then
            wm_validate_orders_for_invc(v_facility_id);
        end if;
    
        wm_invoice_olpns_in_list(p_invc_batch_nbr, p_user_id, p_whse, v_facility_id, p_tc_company_id,
            v_pre_bill_flag, v_genrt_carton_asn, v_srl_trk_flag);

        wm_invoice_orders_in_list(p_invc_batch_nbr, p_user_id, p_tc_company_id, v_pre_bill_flag);

        if (v_pre_bill_flag = 0)
        then
            -- per c++ code, incidental invoicing of closed shpmts is to be supported
            wm_invoice_shpmts_in_list(p_invc_batch_nbr, p_user_id, p_tc_company_id);
        
            wm_complete_invoicing_process(p_invc_batch_nbr, p_user_id, p_tc_company_id, v_facility_id,
                p_whse);
        end if;

        v_num_ord_to_process := v_num_ord_to_process - v_commit_freq;
        if ( v_invc_ship_or_manif = 1 or v_commit_freq = 99999 or v_num_ord_to_process <= 0 )
        then
            exit;
        else
            delete from tmp_invc_order_mastr_list t
            where rownum <= v_commit_freq;
            
            delete from tmp_invc_lpn_mastr_list t
            where not exists
            (
                select 1
                from tmp_invc_order_mastr_list t1
                where t1.order_id = t.order_id
            );
        end if;
        commit ;
    end loop;

	  wm_cs_log('Invoicing completed');
    commit;

    if (v_invoke_val = 1)
    then
        wm_val_exec_job(p_user_id, p_app_hook => 'EXIT', p_pgm_id => v_pgm_id, 
            p_rt_bind_list_csv => v_rt_bind_list_csv, p_log_lvl => v_max_log_lvl, 
            p_parent_txn_id => v_parent_txn_id);
    end if;
    
    wm_utils.set_context_info(p_module_name => v_prev_module_name,
        p_action_name => v_prev_action_name);
end;
/
show errors;
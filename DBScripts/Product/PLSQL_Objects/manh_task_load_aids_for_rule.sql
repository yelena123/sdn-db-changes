create or replace procedure manh_task_load_aids_for_rule
(
    p_whse                    in facility.whse%type,
    p_facility_id             in facility.facility_id%type,
    p_rule_id                 in rule_hdr.rule_id%type,
    p_sec_task_int            in task_rule_parm.sec_task_int%type,
    p_task_genrtn_ref_nbr_csv in task_hdr.task_genrtn_ref_nbr%type,
    p_task_genrtn_ref_code    in task_hdr.task_genrtn_ref_code%type,
    p_need_id                 in alloc_invn_dtl.need_id%type,
    p_group_lpn_to_tasks      in varchar2,
    p_dest_locn_id            in task_rule_parm.dest_locn_id%type
)
as
    p_task_rule_sql     varchar2(18000);
    v_insert_list       varchar2(1000);
    v_inner_select_list varchar2(1000);
    v_select_rule       varchar2(12000);
    v_sort_rule         varchar2(2000);
    -- these are capped at 5 for now
    v_num_total_breaks  number(1) := 5;
    v_rule_type         rule_hdr.rule_type%type := 'TS';
begin
    -- get the selection and sequencing portions of the rule
    select manh_get_rule(p_rule_id, 0, v_rule_type)
    into v_select_rule
    from dual;

    if (v_select_rule is null)
    then
        raise_application_error('-20050', 'No criteria found for rule'
            || p_rule_id);
    end if;

    select manh_get_rule(p_rule_id, 1, v_rule_type)
    into v_sort_rule
    from dual;

    -- build the insert and select lists for the break columns
    for rec in
    (
        select iv.tbl_name, iv.colm_name, iv.row_num
        from
        (
            select 'ALLOC_INVN_DTL' tbl_name, 'CARTON_NBR' colm_name, 1 row_num
            from dual
            where p_group_lpn_to_tasks = 'Y'
            union all
            select rsd.tbl_name, rsd.colm_name, 
                row_number() over(order by rsd.sort_seq_nbr) 
                + case when p_group_lpn_to_tasks = 'Y' then 1 else 0 end row_num
            from rule_sort_dtl rsd
            where rsd.rule_id = p_rule_id and rsd.break_list = 'Y'
                and rsd.break_capcty > 0
        ) iv
        where iv.row_num <= v_num_total_breaks
    )
    loop
        v_insert_list := v_insert_list || ', break_list' 
            || to_char(rec.row_num);
        v_inner_select_list := v_inner_select_list || ', to_char('
            || rec.tbl_name || '.' || rec.colm_name || ') break_list'
            || to_char(rec.row_num);
    end loop;

    p_task_rule_sql
        := ' insert into tmp_task_creation_selected_aid'
        || ' ('
        || '    rule_sort_id, alloc_invn_dtl_id, alloc_invn_code, cntr_nbr, pull_locn_id,'
        || '    invn_need_type, task_type, task_prty, task_batch, alloc_uom,'
        || '    alloc_uom_qty, qty_alloc, dest_locn_id, task_cmpl_ref_code, '
        || '    task_cmpl_ref_nbr, line_item_id, carton_nbr, '
        || '    cd_master_id, item_id, task_genrtn_ref_nbr, task_genrtn_ref_code ' || v_insert_list
        || ' )'
        || ' select min(row_num) row_num,'
        || '    alloc_invn_dtl_id, alloc_invn_code, cntr_nbr, pull_locn_id,'
        || '    invn_need_type, task_type, task_prty, task_batch, alloc_uom,'
        || '    alloc_uom_qty, qty_alloc, dest_locn_id, task_cmpl_ref_code, '
        || '    task_cmpl_ref_nbr, line_item_id, carton_nbr, '
        || '    cd_master_id, item_id, task_genrtn_ref_nbr, task_genrtn_ref_code ' || v_insert_list
        || ' from'
        || ' ('
        || '    select row_number() over(sort_criteria) row_num,'
        || '        alloc_invn_dtl.alloc_invn_dtl_id, '
        || '        alloc_invn_dtl.alloc_invn_code, alloc_invn_dtl.cntr_nbr,'
        || '        alloc_invn_dtl.pull_locn_id, alloc_invn_dtl.invn_need_type,'
        || '        alloc_invn_dtl.task_type, alloc_invn_dtl.task_prty,'
        || '        alloc_invn_dtl.task_batch, alloc_invn_dtl.alloc_uom,'
        || '        alloc_invn_dtl.alloc_uom_qty, alloc_invn_dtl.qty_alloc,'
        || '        case when alloc_invn_dtl.invn_need_type in (10, 53, 54)  
                        then coalesce(:dest_locn_id, alloc_invn_dtl.dest_locn_id) else alloc_invn_dtl.dest_locn_id end dest_locn_id,'
        || '        alloc_invn_dtl.task_cmpl_ref_code, alloc_invn_dtl.task_cmpl_ref_nbr,'
        || '        alloc_invn_dtl.line_item_id, alloc_invn_dtl.carton_nbr, '
        || '        alloc_invn_dtl.cd_master_id, alloc_invn_dtl.item_id, '
        || '        alloc_invn_dtl.task_genrtn_ref_nbr, alloc_invn_dtl.task_genrtn_ref_code ' || v_inner_select_list
        || '    from alloc_invn_dtl'
        || '    where alloc_invn_dtl.whse = :whse and alloc_invn_dtl.stat_code = :stat_code'
        || '        and ' || v_select_rule;

    -- order by clause addendum
    v_sort_rule 
        := ' order by ' 
        || case when p_group_lpn_to_tasks = 'N' then ' '
            else 't.min_locn_pick_seq, t.carton_nbr, ' end
        || case when v_sort_rule is null then ' LOCN_HDR.LOCN_PICK_SEQ, '
            else substr(v_sort_rule, 3, length(v_sort_rule)) || ', ' end
        || case when instr(p_task_rule_sql, 'PICKING_SHORT_ITEM.', 1, 1) > 0
            or instr(v_sort_rule, 'PICKING_SHORT_ITEM.', 1, 1) > 0
            then ' PICKING_SHORT_ITEM.PICKING_SHORT_ITEM_ID, ' else ' ' end
        || case when instr(p_task_rule_sql, 'ORDER_NOTE.', 1, 1) > 0
            or instr(v_sort_rule, 'ORDER_NOTE.', 1, 1) > 0
            then ' ORDER_NOTE.NOTE_CODE, ' else ' ' end
        || 'alloc_invn_dtl.alloc_invn_dtl_id';
    p_task_rule_sql := replace(p_task_rule_sql, 'sort_criteria', v_sort_rule);

    -- functional flows - pick/pack/pnh/need_id
    if (p_task_genrtn_ref_nbr_csv is not null)
    then
        p_task_rule_sql := replace(p_task_rule_sql, 'where ',
            ' join table(cl_parse_endpoint_name_csv(:ref_nbr_csv)) t_csv on t_csv.column_value '
            || ' = alloc_invn_dtl.task_genrtn_ref_nbr where ');
        
        p_task_rule_sql := p_task_rule_sql
            || '    and alloc_invn_dtl.task_genrtn_ref_code = :ref_code';
    else
        p_task_rule_sql := p_task_rule_sql
            || '    and alloc_invn_dtl.need_id = :need_id';
    end if;

    if (p_group_lpn_to_tasks = 'Y')
    then
        p_task_rule_sql := replace(p_task_rule_sql, 'where ',
            ' join tmp_task_lpn_min_locn_seq t on t.carton_nbr = '
            || ' alloc_invn_dtl.carton_nbr where ');
    end if;

    -- rule joins
    if (instr(p_task_rule_sql, 'ORDERS', 1, 1) > 0
        or instr(p_task_rule_sql, 'ORDER_LINE_ITEM', 1, 1) > 0
        or instr(p_task_rule_sql, 'ORDER_NOTE.', 1, 1) > 0
        or instr(p_task_rule_sql, 'FACILITY.', 1, 1) > 0
        or instr(p_task_rule_sql, 'FACILITY_ALIAS.', 1, 1) > 0)
    then
        p_task_rule_sql := replace(p_task_rule_sql, 'where ',
            ' join orders on orders.tc_order_id = alloc_invn_dtl.tc_order_id '
            || ' and orders.tc_company_id = alloc_invn_dtl.cd_master_id where ');

        if (instr(p_task_rule_sql, 'ORDER_NOTE.', 1, 1) > 0)
        then
            p_task_rule_sql := replace(p_task_rule_sql, 'where ',
                'join order_note on order_note.order_id = orders.order_id '
                || ' and (order_note.line_item_id = alloc_invn_dtl.line_item_id or order_note.line_item_id = 0 ) '
                || ' where ');
        end if;

        if (instr(p_task_rule_sql, 'FACILITY.', 1, 1) > 0
            or instr(p_task_rule_sql, 'FACILITY_ALIAS.', 1, 1) > 0)
        then
            p_task_rule_sql := replace(p_task_rule_sql, 'where ',
                ' join facility on facility.facility_id = orders.d_facility_id '
                || ' and facility.mark_for_deletion = 0 where ');
    
            if (instr(p_task_rule_sql, 'FACILITY_ALIAS.', 1, 1) > 0)
            then
                p_task_rule_sql := replace(p_task_rule_sql, 'where ',
                    ' join facility_alias on facility_alias.facility_id = facility.facility_id '
                    || ' and facility_alias.mark_for_deletion = 0 and facility_alias.is_primary = 1 where ');
            end if;
        end if;

        if (instr(p_task_rule_sql, 'ORDER_LINE_ITEM', 1, 1) > 0)
        then
            p_task_rule_sql := replace(p_task_rule_sql, 'where ',
                ' join order_line_item on order_line_item.order_id = orders.order_id '
                || ' and order_line_item.line_item_id = alloc_invn_dtl.line_item_id where ');
        end if;
    end if;

    if (instr(p_task_rule_sql, 'PICK_LOCN_SUMM.', 1, 1) > 0)
    then
        p_task_rule_sql := replace(p_task_rule_sql, 'where ',
            ' join pick_locn_summ_vw pick_locn_summ on pick_locn_summ.locn_id = alloc_invn_dtl.pull_locn_id'
            || ' and pick_locn_summ.item_id = alloc_invn_dtl.item_id where ');
    end if;

    if (instr(p_task_rule_sql, 'PICK_LOCN_SUMM_DEST.', 1, 1) > 0)
    then
        p_task_rule_sql := replace(p_task_rule_sql, 'where ',
            ' join pick_locn_summ_vw pick_locn_summ_dest on pick_locn_summ_dest.locn_id = alloc_invn_dtl.dest_locn_id'
            || ' and pick_locn_summ_dest.item_id = alloc_invn_dtl.item_id where ');
    end if;

    if (instr(p_task_rule_sql, 'PICKING_SHORT_ITEM.', 1, 1) > 0)
    then
        p_task_rule_sql := replace(p_task_rule_sql, 'where ',
            ' join picking_short_item on picking_short_item.line_item_id = alloc_invn_dtl.line_item_id'
            || ' and picking_short_item.stat_code = 90 where ');
    end if;

    if (instr(p_task_rule_sql, 'ALIAS_OLPN', 1, 1) > 0
        or instr(p_task_rule_sql, 'CHUTE_MASTER', 1, 1) > 0)
    then
        p_task_rule_sql := replace(p_task_rule_sql, 'where ',
            ' join lpn olpn on olpn.tc_lpn_id = alloc_invn_dtl.carton_nbr and olpn.tc_company_id = alloc_invn_dtl.cd_master_id '
            || ' and olpn.inbound_outbound_indicator = ''O'' '
            || ' and olpn.c_facility_id = ' || p_facility_id || ' where ');

        if (instr(p_task_rule_sql, 'CHUTE_MASTER', 1, 1) > 0)
        then
            p_task_rule_sql := replace(p_task_rule_sql, 'where ',
                ' join chute_master on chute_master.chute_id = olpn.chute_id where ');
        end if;
        p_task_rule_sql := replace(p_task_rule_sql, 'ALIAS_OLPN', 'olpn');
    end if;

    if (instr(p_task_rule_sql, 'ALIAS_IBPALLET', 1, 1) > 0)
    then
        p_task_rule_sql := replace(p_task_rule_sql, 'where ',
            ' join lpn plt on plt.tc_lpn_id = alloc_invn_dtl.cntr_nbr and plt.tc_company_id = alloc_invn_dtl.cd_master_id '
            || ' and plt.inbound_outbound_indicator = ''I'' and plt.lpn_type = 2 '
            || ' and plt.c_facility_id = ' || p_facility_id || ' where ');

        p_task_rule_sql := replace(p_task_rule_sql, 'ALIAS_IBPALLET', 'plt');
    end if;

    if (instr(p_task_rule_sql, 'ALIAS_DLH', 1, 1) > 0)
    then
        p_task_rule_sql := replace(p_task_rule_sql, 'where ',
            ' join locn_hdr dlh on dlh.locn_id = alloc_invn_dtl.dest_locn_id where ');

        p_task_rule_sql := replace(p_task_rule_sql, 'ALIAS_DLH', 'dlh');
    end if;
                
    if (instr(p_task_rule_sql, 'LPN.', 1, 1) > 0)
    then
        p_task_rule_sql := replace(p_task_rule_sql, 'where ',
            ' join lpn on lpn.tc_lpn_id = alloc_invn_dtl.cntr_nbr and lpn.tc_company_id = alloc_invn_dtl.cd_master_id '
            || ' and lpn.inbound_outbound_indicator = ''I'' and lpn.lpn_type = 1 '
            || ' and lpn.c_facility_id = ' || p_facility_id || ' where ');
    end if;
	
    if (instr(p_task_rule_sql, 'ITEM_CBO', 1, 1) > 0)
    then
        p_task_rule_sql := replace(p_task_rule_sql, 'where ',
            ' join item_cbo on item_cbo.item_id = alloc_invn_dtl.item_id '
            || ' where ');
    end if;

    if (instr(p_task_rule_sql, 'ITEM_WMS', 1, 1) > 0)
    then
        p_task_rule_sql := replace(p_task_rule_sql, 'where ',
            'join item_wms on item_wms.item_id = alloc_invn_dtl.item_id '
            || ' where ');
    end if;

    if (instr(p_task_rule_sql, 'ITEM_FACILITY_MAPPING_WMS', 1, 1) > 0)
    then
        p_task_rule_sql := replace(p_task_rule_sql, 'where ',
            ' join item_facility_mapping_wms '
            || '    on item_facility_mapping_wms.item_id '
            || '    = alloc_invn_dtl.item_id '
            || '    and item_facility_mapping_wms.facility_id = ' 
            || to_char(p_facility_id) ||' where ');
    end if;

    if (instr(p_task_rule_sql, 'LOCN_HDR', 1, 1) > 0)
    then
        p_task_rule_sql := replace(p_task_rule_sql, 'where ',
            ' join locn_hdr on locn_hdr.locn_id = alloc_invn_dtl.pull_locn_id '
            || ' where ');
    end if;
	
	if (instr(p_task_rule_sql, 'ALIAS_DPL', 1, 1) > 0)
    then
        p_task_rule_sql := replace(p_task_rule_sql, 'where ',
            ' join pick_locn_hdr dpl on dpl.locn_id = alloc_invn_dtl.dest_locn_id where ');

        p_task_rule_sql := replace(p_task_rule_sql, 'ALIAS_DPL', 'dpl');
    end if;
	
	if (instr(p_task_rule_sql, 'PICK_LOCN_HDR', 1, 1) > 0)
    then
        p_task_rule_sql := replace(p_task_rule_sql, 'where ',
            ' join pick_locn_hdr on pick_locn_hdr.locn_id = alloc_invn_dtl.pull_locn_id '
            || ' where ');
    end if;
	
	if (instr(p_task_rule_sql, 'ALIAS_DRL', 1, 1) > 0)
    then
        p_task_rule_sql := replace(p_task_rule_sql, 'where ',
            ' join resv_locn_hdr drl on drl.locn_id = alloc_invn_dtl.dest_locn_id where ');

        p_task_rule_sql := replace(p_task_rule_sql, 'ALIAS_DRL', 'drl');
    end if;

    if (instr(p_task_rule_sql, 'RESV_LOCN_HDR', 1, 1) > 0)
    then
        p_task_rule_sql := replace(p_task_rule_sql, 'where ',
            ' join resv_locn_hdr on resv_locn_hdr.locn_id = alloc_invn_dtl.pull_locn_id '
            || ' where ');
    end if;      

    if (instr(p_task_rule_sql, 'LOCN_GRP', 1, 1) > 0)
    then
        p_task_rule_sql := replace(p_task_rule_sql, 'where ',
            ' left join locn_grp on ((locn_grp.locn_id '
            || '    = alloc_invn_dtl.pull_locn_id) '
            || ' or (locn_grp.locn_id = alloc_invn_dtl.dest_locn_id '
            || ' and locn_grp.grp_type = 51 )) '
            || ' where ');
    end if;
    
    p_task_rule_sql := p_task_rule_sql
        || ' ) t1'
        || ' group by alloc_invn_dtl_id, alloc_invn_code, cntr_nbr, pull_locn_id, invn_need_type,'
        || '    task_type, task_prty, task_batch, alloc_uom, alloc_uom_qty, qty_alloc, dest_locn_id,'
        || '    task_cmpl_ref_code, task_cmpl_ref_nbr, line_item_id, carton_nbr, cd_master_id,'
        || '    item_id, task_genrtn_ref_nbr, task_genrtn_ref_code ' || v_insert_list;

    if (p_task_genrtn_ref_nbr_csv is not null)
    then
        execute immediate p_task_rule_sql using p_dest_locn_id, p_task_genrtn_ref_nbr_csv, p_whse, 
            case when p_sec_task_int = 1 then 91 else 0 end, p_task_genrtn_ref_code;
    else
        execute immediate p_task_rule_sql using p_dest_locn_id, p_whse, 50, p_need_id;
    end if;
    wm_cs_log('Loaded ' || sql%rowcount || ' AID for rule ' || p_rule_id);
end;
/
show errors;

create or replace function pix_620_10
(
    p_whse   		  varchar2,
    p_cd_master_id        number
)
return varchar2
is
    v_pix62010 	  		 varchar2(4000);
    v_tmp_string 		 varchar2(4000);
    v_tmp_ref 		         varchar2(4000);
    v_proc_stat_code 	         varchar2(2);
    v_xml_group_attr             varchar2(10);  
begin
    
    manh_get_pix_config_data  ('620','10','01',p_cd_master_id,v_proc_stat_code,v_xml_group_attr);     
    
    if (v_proc_stat_code <> '91') 
    then    
    v_pix62010 := 'insert into pix_tran(pix_tran_id, tran_nbr, tran_type, tran_code, '||
        'pix_seq_nbr, proc_stat_code, case_nbr, whse, tc_company_id , item_name, season, '||
        'season_yr, style, style_sfx, color, color_sfx, sec_dim, qual, '||
        'size_desc, size_range_code, size_rel_posn_in_table, invn_type, '||
        'prod_stat, batch_nbr, sku_attr_1, sku_attr_2, sku_attr_3, '||
        'sku_attr_4, sku_attr_5, cntry_of_orgn, invn_adjmt_qty, '||
        'invn_adjmt_type, wt_adjmt_qty, wt_adjmt_type, actn_code, '||
        'create_date_time, mod_date_time, user_id, '||
        'wm_version_id,item_id, facility_id, xml_group_id ';
       
    v_tmp_String := '';
    
    for small_cursor_rec in (
        select distinct rcl.tbl_name, rcl.colm_name, ptcd.ref_code_id, ptcd.pix_tran_code_seq
            from pix_tran_code ptc
                inner join pix_tran_code_dtl ptcd
                on ptcd.pix_tran_code_id = ptc.pix_tran_code_id
                inner join pix_ref_code_master prcm
                on prcm.ref_code_id = ptcd.ref_code_id
                inner join rule_colm_list rcl
                on rcl.rule_colm_id = prcm.rule_colm_id
                    where ptc.tran_type = '620'
                    and ptc.tran_code = '10'
                    and ptc.actn_code in ('*', '01')
                    and ptc.cd_master_id = p_cd_master_id )
    loop
        v_pix62010 := v_pix62010 || ',  ref_code_id_' ||
            rtrim(to_char(small_cursor_rec.pix_tran_code_seq)) ||
            ',   ref_field_' ||
            rtrim(to_char(small_cursor_rec.pix_tran_code_seq));
        v_tmp_String := v_tmp_String || ',    ''' ||
            small_cursor_rec.ref_code_id || ''','|| small_cursor_rec.tbl_name ||
            '.' || small_cursor_rec.colm_name;
    end loop;     
     
    v_pix62010 := v_pix62010 || ') select pix_tran_id_seq.nextval, pix_tran_id_seq.nextval ,''620'', ''10'',  '||
    	' rownum ,'||coalesce(v_proc_stat_code,'10')||', lpn.tc_reference_lpn_id , '''|| p_whse||
    	''', lpn.tc_company_id , item_cbo.item_name ,item_cbo.item_season,item_cbo.item_season_year,'||
    	' item_cbo.item_style, item_cbo.item_style_sfx,item_cbo.item_color,item_cbo.item_color_sfx,'||
    	' item_cbo.item_second_dim,item_cbo.item_quality,item_cbo.item_size_desc,'||
        ' item_wms.size_range_code,item_wms.size_rel_posn_in_table,'||
        ' lpn_detail.inventory_type, lpn_detail.product_status, lpn_detail.batch_nbr,'||
	' lpn_detail.item_attr_1 , lpn_detail.item_attr_2  ,   lpn_detail.item_attr_3 ,'||
	' lpn_detail.item_attr_4, lpn_detail.item_attr_5, lpn_detail.cntry_of_orgn,'||
        ' lpn_detail.size_value, ''S'', lpn_detail.weight, ''S'', ''01'' , '||
        ' sysdate, sysdate, :p_login_user_id , 1, lpn_detail.item_id, lpn.c_facility_id, '
        || ( case when v_xml_group_attr is null then 'null' else ''''|| v_xml_group_attr ||''' ' end )
        || v_tmp_String ||
        ' from lpn_detail inner join lpn on lpn.lpn_id = lpn_detail.lpn_id '||
        ' left outer join lpn_lock on lpn.lpn_id = lpn_lock.lpn_id '||
        ' inner join item_cbo on lpn_detail.item_id = item_cbo.item_id '||
        ' inner join item_wms on  item_wms.item_id = item_cbo.item_id '||
        ' inner join order_line_item on order_line_item.item_id = lpn_detail.item_id '||
        ' and order_line_item.line_item_id = lpn_detail.distribution_order_dtl_id '||
        ' where lpn_detail.lpn_id = :v_olpn_id';
            
    return v_pix62010;
    end if;    
end pix_620_10;
/
show errors;

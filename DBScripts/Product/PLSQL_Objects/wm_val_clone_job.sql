create or replace procedure wm_val_clone_job
(
    p_old_job_name        in val_job_hdr.job_name%type,
    p_new_job_name        in val_job_hdr.job_name%type,
    p_new_job_desc        in val_job_hdr.job_desc%type default null
)
as
    v_new_vjh_id    val_job_hdr.val_job_hdr_id%type;
    v_new_vjd_id    val_job_dtl.val_job_dtl_id%type;
begin
    wm_val_create_job_hdr(p_job_name => p_new_job_name,
        p_job_desc => p_new_job_desc, p_vjh_id => v_new_vjh_id);
    
    for dtl_rec in
    (
        select vjd.val_job_dtl_id old_dtl, vs.ident ident
        from val_job_dtl vjd
        join val_sql vs on vs.sql_id = vjd.sql_id
        where vjd.val_job_hdr_id =
        (
            select vjh.val_job_hdr_id
            from val_job_hdr vjh
            where vjh.job_name = p_old_job_name
        )
    )
    loop
        wm_val_create_job_dtl(v_new_vjh_id, p_ident => dtl_rec.ident,
            p_vjd_id => v_new_vjd_id);

        for bind_rec in
        (
            select vsb.bind_var, vsb.bind_val
            from val_sql_binds vsb
            where vsb.val_job_dtl_id = dtl_rec.old_dtl
        )
        loop
            wm_val_add_bind_var_to_sql(v_new_vjd_id,
                p_bind_var => bind_rec.bind_var, p_bind_val => bind_rec.bind_val);
        end loop;
        
        for result_rec in
        (
            select ver.row_num, ver.pos, ver.expected_value
            from val_expected_results ver
            where ver.val_job_dtl_id = dtl_rec.old_dtl
        )
        loop
            wm_val_add_result_to_sql(v_new_vjd_id, p_row_num => result_rec.row_num,
                p_pos => result_rec.pos, 
                p_expected_value => result_rec.expected_value);
        end loop;
    end loop;

    commit;
end;
/
show errors;

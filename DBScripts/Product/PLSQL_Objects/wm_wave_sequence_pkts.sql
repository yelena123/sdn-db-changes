create or replace procedure wm_wave_sequence_pkts
(
    p_ship_wave_parm_id in  ship_wave_parm.ship_wave_parm_id%type,
    p_user_id           in  ucl_user.user_name%type
)
as
    v_pkt_count         number(9) := 0;
    v_pick_wave_nbr     ship_wave_parm.pick_wave_nbr%type;
    v_ship_wave_nbr     ship_wave_parm.ship_wave_nbr%type;
    v_select_rule       varchar2(4000);
    v_sort_rule         varchar2(4000);
    v_rule_sql          varchar2(32600);
    v_rule_type         varchar2(2) := 'PQ';
    v_code_id           sys_code.code_id%type := 'PSQ';
    v_max_log_lvl       number(1);
    v_tc_company_id     wave_parm.tc_company_id%type;
    v_whse              facility.whse%type;
    v_prev_module_name  wm_utils.t_app_context_data;
    v_prev_action_name  wm_utils.t_app_context_data;
begin
    select swp.ship_wave_nbr, swp.pick_wave_nbr, swp.whse, coalesce(wp.tc_company_id, wm_utils.wm_get_user_bu(p_user_id))
    into v_ship_wave_nbr, v_pick_wave_nbr, v_whse, v_tc_company_id
    from ship_wave_parm swp
    join wave_parm wp on wp.wave_parm_id = swp.wave_parm_id
    where swp.ship_wave_parm_id = p_ship_wave_parm_id;

    select to_number(coalesce(trim(substr(sc.misc_flags, 8, 1)), '0'))
    into v_max_log_lvl
    from sys_code sc 
    where sc.rec_type = 'S' and sc.code_type = '520' and sc.code_id = v_code_id;

    delete from tmp_log_parm;
    insert into tmp_log_parm
    (
        log_lvl, pgm_id, module, msg_id, ref_code_1, ref_value_1, user_id, whse, cd_master_id
    )
    values
    (
        v_max_log_lvl, 'wm_wave_pkt_sequencing', 'WAVE', '1094', v_code_id, v_pick_wave_nbr,
        p_user_id, v_whse, v_tc_company_id
    );
    
    wm_utils.get_context_info(v_prev_module_name, v_prev_action_name);
    wm_utils.set_context_info(p_module_name => 'WAVE', p_action_name => 'PKT SEQUENCING',
        p_client_id => v_ship_wave_nbr);

    wm_cs_log('Beginning wave pkt sequencing', p_sql_log_level => 0);
    for rule_rec in
    (
        select iv.rule_id
        from
        (
            select wrp.rule_id, wrp.rule_prty
            from wave_rule_parm wrp
            where wrp.wave_parm_id = p_ship_wave_parm_id and wrp.stat_code = 0
                and exists
                (
                    select 1 
                    from rule_hdr rh
                    join rule_sel_dtl rsd on rsd.rule_id = rh.rule_id
                    where rh.rule_id = wrp.rule_id and rh.rule_type = v_rule_type
               )
            union all
            select null rule_id, null rule_prty
            from dual
        ) iv
        order by iv.rule_prty nulls last
    )
    loop
        v_select_rule := null;
        v_sort_rule := null;
        if (rule_rec.rule_id is not null)
        then
            select manh_get_rule(rule_rec.rule_id, 0, v_rule_type)
            into v_select_rule
            from dual;
    
            select substr(manh_get_rule(rule_rec.rule_id, 1, v_rule_type), 3)
            into v_sort_rule
            from dual;
        end if;

        v_rule_sql :=
               ' merge into pkt_dtl pd'
            || ' using'
            || ' ('
            || '    select iv.pkt_dtl_id, :pkt_count + '
            || '        row_number() over(order by min(iv.seq)) wave_seq_nbr'
            || '    from'
            || '    ('
            || '        select pkt_dtl.pkt_dtl_id,'
            || '            row_number() over(order by sort_criteria) seq'
            || '        from pkt_dtl'
            || '        join pkt_hdr on pkt_hdr.pkt_hdr_id = pkt_dtl.pkt_hdr_id'
            || '        where pkt_hdr.whse = :whse'
            || '            and pkt_dtl.wave_nbr = :pick_wave_nbr '
            || '            and pkt_dtl.wave_stat_code = 10 ' 
            || case when v_select_rule is null then ' ' else ' and ' || v_select_rule end
            || '    ) iv'
            || '    group by iv.pkt_dtl_id'
            || ' ) iv2 on (iv2.pkt_dtl_id = pd.pkt_dtl_id)'
            || ' when matched then update'
            || ' set pd.wave_seq_nbr = iv2.wave_seq_nbr, pd.wave_stat_code = 20,'
            || '    pd.mod_date_time = sysdate, pd.user_id = :user_id';

        v_sort_rule := v_sort_rule || case when v_sort_rule is null then ' ' else ', ' end
            || case when instr(v_rule_sql, 'ORDER_NOTE.', 1, 1) > 0
                then ' order_note.note_type, order_note.note_code, ' else ' ' end
            || ' pkt_dtl.pkt_ctrl_nbr, pkt_dtl.pkt_dtl_id';

        v_rule_sql := replace(v_rule_sql, 'sort_criteria', v_sort_rule);
        wm_wave_add_pkt_seq_joins(v_rule_sql);

        execute immediate v_rule_sql using v_pkt_count, v_whse, v_pick_wave_nbr, p_user_id;
        v_pkt_count := v_pkt_count + sql%rowcount;
        wm_cs_log('Sequenced pkts for rule <' || coalesce(to_char(rule_rec.rule_id), 'default')
            || '>, running count: ' || v_pkt_count);
    end loop;
    commit;

    wm_cs_log('Completed wave pkt sequencing', p_sql_log_level => 0);
	
    wm_utils.set_context_info(p_module_name => v_prev_module_name,
        p_action_name => v_prev_action_name);
end;
/
show errors;

create or replace procedure wm_invc_load_lpns_via_list
(
    p_shpmt_list      in varchar2,
    p_order_list      in varchar2,
    p_manifest_list   in varchar2,
    p_check_manifest  in number,
    p_pre_bill_flag   in number,
    p_facility_id     in facility.facility_id%type,
    p_num_ord_to_process in out number
)
as
    v_remaining_str   varchar2(3000) := coalesce(p_shpmt_list, p_manifest_list, p_order_list);
    v_curr_val        varchar2(10);
    v_curr_delimiter_pos number(6) := 0;
begin
    v_remaining_str := rtrim(ltrim(trim(v_remaining_str), '('), ')');
    
    loop
        v_curr_delimiter_pos := coalesce(instr(v_remaining_str, ',', 1, 1), 0);
        if (v_curr_delimiter_pos = 0)
        then
            v_curr_val := v_remaining_str;
            v_remaining_str := '';
        else
            v_curr_val := substr(v_remaining_str, 1, v_curr_delimiter_pos - 1);
            v_remaining_str := substr(v_remaining_str, v_curr_delimiter_pos + 1);
        end if;

        if (p_shpmt_list is not null)
        then
            insert into tmp_invc_lpn_mastr_list
            (
                lpn_id, order_id
            )
            select l.lpn_id, l.order_id
            from lpn l
            join shipment s on s.shipment_id = l.shipment_id or s.tc_shipment_id = l.tc_shipment_id
            where s.shipment_id = to_number(trim(v_curr_val))
                and s.shipment_closed_indicator = 1 and l.order_id is not null
                and l.lpn_facility_status >= 50 and l.lpn_facility_status < 90
                and l.lpn_type = 1 and l.inbound_outbound_indicator = 'O' 
                and l.c_facility_id = p_facility_id
                and not exists
                (
                    select 1
                    from tmp_invc_lpn_mastr_list t
                    where t.lpn_id = l.lpn_id
                )
            log errors (to_char(sysdate));
            wm_cs_log('Lpns found for shpmt ' || v_curr_val || ' were ' || sql%rowcount);
        elsif (p_manifest_list is not null)
        then
            insert into tmp_invc_lpn_mastr_list
            (
                lpn_id, order_id
            )
            select l.lpn_id, l.order_id
            from lpn l
            join shipment s on s.shipment_id = l.shipment_id
            join manifest_hdr mh on mh.manifest_id = s.manifest_id
            where s.manifest_id = to_number(trim(v_curr_val))
                and l.order_id is not null
                and l.lpn_facility_status >= 40 and l.lpn_facility_status < 90
                and l.lpn_type = 1 and l.inbound_outbound_indicator = 'O'
                and l.c_facility_id = p_facility_id
                and not exists
                (
                    select 1
                    from tmp_invc_lpn_mastr_list t
                    where t.lpn_id = l.lpn_id
                )
            log errors (to_char(sysdate));
            wm_cs_log('Lpns found for manifest ' || v_curr_val || ' were ' || sql%rowcount);
        else
            if (p_pre_bill_flag in (0, 2))
            then
                insert into tmp_invc_lpn_mastr_list
                (
                    lpn_id, order_id
                )
                select l.lpn_id, l.order_id
                from lpn l
                join orders o on o.order_id = l.order_id
                    and (p_pre_bill_flag = 0 or o.is_original_order = 1)
                where l.order_id = to_number(trim(v_curr_val))
                    and l.lpn_facility_status >= 20 and l.lpn_facility_status < 90
                    and l.lpn_type = 1 and l.inbound_outbound_indicator = 'O'
                    and l.c_facility_id = p_facility_id
                    and not exists
                    (
                        select 1
                        from tmp_invc_lpn_mastr_list t
                        where t.lpn_id = l.lpn_id
                    )
                log errors (to_char(sysdate));
                wm_cs_log('Lpns found for order ' || v_curr_val || ' were ' || sql%rowcount);
            end if;

            if (to_number(trim(v_curr_val)) is not null)
            then
                insert into tmp_invc_order_mastr_list (order_id)
                values (to_number(trim(v_curr_val)));
                p_num_ord_to_process := p_num_ord_to_process + 1;
            end if;
        end if;

        if (v_curr_delimiter_pos = 0)
        then
            exit;
        end if;
    end loop;
end;
/
show errors;

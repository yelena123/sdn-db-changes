create or replace force view val_sql_tag_summ_vw
(
    sql_id, sql_ident, type_list, tag_list, type_tag_list
)
as
select vs.sql_id, vs.ident sql_ident, 
    listagg(decode(vst.type, '1', 'select', '2', 'from', '3', 'via', '4', 'module', '5', 'vertical', '6', 'desc', vst.type), ' || ')
        within group(order by vst.type) type_list,
    listagg(vst.tag, ' || ') within group(order by vst.type) tag_list,
    listagg(decode(vst.type, '1', 'select', '2', 'from', '3', 'via', '4', 'module', '5', 'vertical', '6', 'desc', vst.type) || ' '
        || vst.tag, ' || ') within group(order by vst.type) type_tag_list
from val_sql_tags vst
join val_sql vs on vs.sql_id = vst.sql_id
group by vs.sql_id, vs.ident
/

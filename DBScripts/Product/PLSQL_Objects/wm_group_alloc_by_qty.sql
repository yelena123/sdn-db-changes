create or replace procedure wm_group_alloc_by_qty
(
    p_user_id         in ucl_user.user_name%type,
    p_facility_id     in number,
    p_pick_wave_nbr   in wave_parm.wave_nbr%type
)
as
    v_whse          facility.whse%type;
    v_code_id       sys_code.code_id%type := 'GRP';
    v_ref_value_1   msg_log.ref_value_1%type
        := 'p_pick_wave_nbr:' || to_char (p_pick_wave_nbr);
    v_max_log_lvl   number (1) := 1;
    v_tc_company_id wave_parm.tc_company_id%type := 0;
begin
    select f.whse
    into v_whse
    from facility f
    where f.facility_id = p_facility_id; 

    select coalesce(wp.tc_company_id, wm_utils.wm_get_user_bu(p_user_id))
    into v_tc_company_id 
    from wave_parm wp
    where wp.wave_nbr = p_pick_wave_nbr and wp.whse = v_whse;

    delete from tmp_log_parm;
    insert into tmp_log_parm
    (
        log_lvl, pgm_id, module, msg_id, ref_code_1, ref_value_1, user_id, cd_master_id
    )
    values
    (
        v_max_log_lvl, 'wm_group_alloc_by_qty', 'WAVE', '1094', v_code_id, 
        v_ref_value_1, p_user_id, v_tc_company_id
    );

    wm_cs_log ('Beginning wm_group_alloc_by_qty');

    -- collect allocation qty from AID's
    insert into tmp_lpn_allocated_qty
    (
        tc_lpn_id, lpn_type, allocated_qty
    )
    select giv.tc_lpn_id, giv.lpn_type, giv.allocated_qty
    from 
    (  
        select aid.cntr_nbr tc_lpn_id, 'C' lpn_type, 
            sum (aid.qty_alloc) allocated_qty,
            count (distinct aid.invn_need_type) mutipleints
        from lpn l
        join alloc_invn_dtl aid on aid.cntr_nbr = l.tc_lpn_id
            and l.inbound_outbound_indicator = 'I'
            and l.lpn_type = 1
            and l.c_facility_id = p_facility_id
            and l.lpn_facility_status = 50
         where aid.task_genrtn_ref_nbr = p_pick_wave_nbr
             and aid.stat_code < 90
             and aid.alloc_uom in ('K', 'S', 'B', 'U')  
             and l.parent_lpn_id is null   
             and aid.whse = v_whse
        group by aid.cntr_nbr, 'C'
        having count (distinct aid.invn_need_type) = 1
        union
        select l.tc_parent_lpn_id tc_lpn_id, 'P' lpn_type,
            sum (aid.qty_alloc) allocated_qty,
            count (distinct aid.invn_need_type)
        from lpn l
        join alloc_invn_dtl aid on aid.cntr_nbr = l.tc_lpn_id
            and l.inbound_outbound_indicator = 'I'
            and l.lpn_type = 1
            and l.c_facility_id = p_facility_id
            and l.lpn_facility_status = 50
        where aid.task_genrtn_ref_nbr = p_pick_wave_nbr
            and aid.stat_code < 90
            --and aid.alloc_uom = 'C'  
            and l.parent_lpn_id is not null
            and aid.whse = v_whse
        group by l.tc_parent_lpn_id, 'P'
        having count (distinct aid.invn_need_type) = 1
    ) giv;
    wm_cs_log ('Selected allocation details: ' || sql%rowcount);

    -- collect actual qty from wm inventory
    insert into tmp_lpn_actual_qty
    (
        tc_lpn_id, actual_qty
    )
    select giv.tc_lpn_id, giv.actual_qty
    from 
    (  
        select l.tc_parent_lpn_id tc_lpn_id, sum (w.on_hand_qty) actual_qty
        from tmp_lpn_allocated_qty t
        join lpn l on t.tc_lpn_id = l.tc_parent_lpn_id
            and l.inbound_outbound_indicator = 'I'
            and l.lpn_type = 1
        join wm_inventory w on l.lpn_id = w.lpn_id
        where t.lpn_type = 'P'
        group by l.tc_parent_lpn_id
        union
        select l.tc_lpn_id tc_lpn_id, sum (w.on_hand_qty) actual_qty
        from tmp_lpn_allocated_qty t
        join lpn l on t.tc_lpn_id = l.tc_lpn_id
            and l.inbound_outbound_indicator = 'I'
            and l.lpn_type = 1
        join wm_inventory w on l.lpn_id = w.lpn_id
        where t.lpn_type = 'C'
        group by l.tc_lpn_id
    ) giv;
    wm_cs_log ('Selected actual inventory lpn count: ' || sql%rowcount);

    merge into alloc_invn_dtl aid
    using
    (
        select giv.tc_lpn_id
        from 
        (
            select l.tc_lpn_id
            from tmp_lpn_actual_qty tactual
            join tmp_lpn_allocated_qty tallocated on tactual.tc_lpn_id = tallocated.tc_lpn_id
            join lpn l on l.tc_parent_lpn_id = tallocated.tc_lpn_id
                and l.inbound_outbound_indicator = 'I'
                and l.lpn_type = 1
            where tallocated.allocated_qty = tactual.actual_qty
                and tallocated.lpn_type = 'P'
            union
            select tactual.tc_lpn_id
            from tmp_lpn_actual_qty tactual
            join tmp_lpn_allocated_qty tallocated on tactual.tc_lpn_id = tallocated.tc_lpn_id
            where tallocated.allocated_qty = tactual.actual_qty
                and tallocated.lpn_type = 'C'
        ) giv
    ) iv on (aid.cntr_nbr = iv.tc_lpn_id 
        and aid.task_genrtn_ref_nbr = p_pick_wave_nbr and aid.stat_code < 90)
    when matched then
    update set aid.task_batch = 'P', aid.mod_date_time = sysdate, --Modified to task_batch from MISC_ALPHA_FIELD_1 
        aid.user_id = p_user_id;
    wm_cs_log ('Completed wm_group_alloc_by_qty ' || sql%rowcount);

    commit;
end;
/
show errors;

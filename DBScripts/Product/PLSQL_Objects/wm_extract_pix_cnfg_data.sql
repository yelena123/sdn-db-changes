create or replace procedure wm_extract_pix_cnfg_data
(
    p_cnfg_str            in varchar2,
    p_proc_stat_code      out pix_tran_code.pix_create_stat_code%type,
    p_xml_group_attr      out pix_tran.xml_group_id%type,
    p_ins_ref_col_string  out varchar2,
    p_sel_ref_col_string  out varchar2
)
as
    v_start_pos           number(5);
    v_end_pos             number(5);
begin
    v_start_pos := 1;
    v_end_pos := instr(p_cnfg_str, '|', 1, 1) - 1;
    p_proc_stat_code := substr(p_cnfg_str, v_start_pos, v_end_pos - v_start_pos + 1);
    v_start_pos := v_end_pos + 2;
    v_end_pos := instr(p_cnfg_str, '|', 1, 2) - 1;
    p_xml_group_attr := substr(p_cnfg_str, v_start_pos, v_end_pos - v_start_pos + 1);
    v_start_pos := v_end_pos + 2;
    v_end_pos := instr(p_cnfg_str, '|', 1, 3) - 1;
    p_ins_ref_col_string := substr(p_cnfg_str, v_start_pos, v_end_pos - v_start_pos + 1);
    v_start_pos := v_end_pos + 2;
    p_sel_ref_col_string := substr(p_cnfg_str, v_start_pos);
end;
/
show errors;

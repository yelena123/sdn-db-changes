CREATE OR REPLACE PROCEDURE sp_slot_count
(
    p_range_id     IN     NUMBER,
    p_whse_code    IN     VARCHAR2,
    p_slot_count   IN OUT NUMBER
)
AS
BEGIN
    sp_get_range_ids (p_range_id, 'RANGE_IDS_TABLE11');
    p_slot_count := 0;

    BEGIN
        SELECT COUNT (DISTINCT slot_item.slot_id)
        INTO p_slot_count
        FROM slot_item 
        JOIN slot ON slot.slot_id = slot_item.slot_id
            AND slot.whse_code = p_whse_code
        WHERE slot_item.slot_id IS NOT NULL
            AND slot.my_range IN (SELECT * FROM RANGE_IDS_TABLE11);
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            NULL;
    END;

    EXECUTE IMMEDIATE ' TRUNCATE TABLE RANGE_IDS_TABLE11 ';

    IF (p_slot_count < 0)
    THEN
        p_slot_count := 0;
    END IF;
END;
/
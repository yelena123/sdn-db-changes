create or replace type t_num as table of number(9)
/
create or replace procedure wm_unplan_pakd_order
(
    p_user_id       in user_profile.user_id%type,
    p_order_id      in orders.order_id%type
)
as
    va_order_list   t_num := t_num(p_order_id);
begin
    wm_unplan_pakd_orders_in_list(p_user_id, va_order_list);
end;
/
show errors;

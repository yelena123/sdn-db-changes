create or replace PROCEDURE PURGE_OTHER_LOGS_EVENTS
AS
/**********************************************************************************************************************
PROCEDURE : PURGE_OTHER_LOGS_EVENTS
PURPOSE   : PURGE MISCELLANEOUS LOGS AND EVENT TABLES
AUTHOR    : RAJ S K 	
CHANGES   : USED PURGE_ONLY_PROC FOR HANDLING DELETE OPERATIONS EFFECTIVELY, CODE RESTRUCTURING USING TEMPORARY TABLES
            AND ALSO INTRODUCED FIXED TIMESTAMP LOGIC ACROSS PROCEDURE 
***********************************************************************************************************************/
      PURGE_START_TIME  date := TRUNC(sysdate); 
      v_stmt            VARCHAR2 (2000);    
      nsqlcode          NUMBER;
      vsqlerrm          VARCHAR2 (300);      
      DO_E_DAYS         NUMBER (4);
      PO_E_DAYS         NUMBER (4);
      ASN_E_DAYS        NUMBER (4);
      EVL_DAYS          NUMBER (4);
      OSE_DAYS          NUMBER (4);
      PRT_DAYS          NUMBER (4);
      TL_DAYS           NUMBER (4);
      EL_DAYS           NUMBER (4);
      AFW_DAYS          NUMBER (4);
      ALLOC_DAYS        NUMBER (4);
      PO_DAYS           NUMBER (4);
BEGIN
      SELECT NVL(DAYS,9999) INTO DO_E_DAYS FROM PURGE_DAYS WHERE MODULE_NAME = 'DOM' AND PURGE_TYPE = 'DO_E';
      SELECT NVL(DAYS,9999) INTO PO_E_DAYS FROM PURGE_DAYS WHERE MODULE_NAME = 'DOM' AND PURGE_TYPE = 'PO_E';
      SELECT NVL(DAYS,9999) INTO ASN_E_DAYS FROM PURGE_DAYS WHERE MODULE_NAME = 'DOM' AND PURGE_TYPE = 'ASN_E';
      SELECT NVL(DAYS,9999) INTO EVL_DAYS FROM PURGE_DAYS WHERE MODULE_NAME = 'DOM' AND PURGE_TYPE = 'EVL';
      SELECT NVL(DAYS,9999) INTO OSE_DAYS FROM PURGE_DAYS WHERE MODULE_NAME = 'DOM' AND PURGE_TYPE = 'OSE';
      SELECT NVL(DAYS,9999) INTO PRT_DAYS FROM PURGE_DAYS WHERE MODULE_NAME = 'DOM' AND PURGE_TYPE = 'PRT';
      SELECT NVL(DAYS,9999) INTO TL_DAYS FROM PURGE_DAYS WHERE MODULE_NAME = 'DOM' AND PURGE_TYPE = 'TL';
      SELECT NVL(DAYS,9999) INTO EL_DAYS FROM PURGE_DAYS WHERE MODULE_NAME = 'DOM' AND PURGE_TYPE = 'EL';
      select NVL(DAYS,9999) into ALLOC_DAYS from PURGE_DAYS where MODULE_NAME = 'DOM' and PURGE_TYPE = 'ALLOC';
      SELECT NVL(DAYS,9999) INTO PO_DAYS FROM PURGE_DAYS WHERE MODULE_NAME = 'DOM' AND PURGE_TYPE = 'PO';
      SELECT NVL(DAYS,9999) INTO AFW_DAYS FROM PURGE_DAYS WHERE MODULE_NAME = 'DOM' AND PURGE_TYPE = 'AFW';

      archive_pkg.log_archive (0000, 'PURGE_OTHER_LOGS_EVENTS - STARTS', NULL, NULL, 0);
	  
      PURGE_ONLY_PROC ('ORDER_EVENT', 'created_dttm < (to_date (''' || purge_start_time || ''') - ' || DO_E_DAYS || ')');
      
      PURGE_ONLY_PROC ('PURCHASE_ORDERS_EVENT', 'created_dttm < (to_date (''' || purge_start_time || ''') - ' || PO_E_DAYS || ')');
      
      PURGE_ONLY_PROC ('ASN_EVENT', 'created_dttm < (to_date (''' || purge_start_time || ''') - ' || ASN_E_DAYS || ')');
      
      PURGE_ONLY_PROC ('EVENT_LOG', 'EVENT_DTTM < (to_date (''' || purge_start_time || ''') - ' || EVL_DAYS || ')');

      QUIET_DROP ('TABLE', 'PURGE_ONLY_GTT');

      V_STMT := 'CREATE TABLE PURGE_ONLY_GTT tablespace DOM_DT_TBS AS SELECT SE.EVENT_ID FROM OM_SCHED_EVENT SE WHERE SE.IS_EXECUTED = 1 AND SE.SCHEDULED_DTTM < (to_date (''' || purge_start_time || ''') - ' || OSE_DAYS || ')
                                                AND EVENT_ID NOT IN ( SELECT PT.EVENT_ID FROM A_PROCESS_TEMPLATE_SCHED PT WHERE PT.EVENT_ID IS NOT NULL 
                                                                      UNION
                                                                      SELECT AW.EVENT_ID FROM A_WF_EVENTMAP AW WHERE AW.EVENT_ID IS NOT NULL 
                                                                      UNION
                                                                      SELECT SO.EVENT_ID FROM SO_ORDER_CHG_VALUE SO WHERE SO.EVENT_ID IS NOT NULL 
                                                                      union
                                                                      SELECT LE.EVENT_ID FROM LRF_EVENT LE WHERE LE.EVENT_ID IS NOT NULL)';
      execute immediate V_STMT; 

      PURGE_ONLY_PROC ('WF_BATCH_SCHEDULER_EVENT', 'OM_SCHED_EVENT_ID IN (SELECT EVENT_ID FROM PURGE_ONLY_GTT)');      

      PURGE_ONLY_PROC ('CONSTEMP_SCHED_EVENT', 'EVENT_ID IN (SELECT EVENT_ID FROM PURGE_ONLY_GTT)');      

      PURGE_ONLY_PROC ('OM_SCHED_EVENT', 'EVENT_ID IN (SELECT EVENT_ID FROM PURGE_ONLY_GTT)');      

      PURGE_ONLY_PROC ('A_UNRELEASABLE_ALLOC', 'A_PROCESSRUNID IN (SELECT A_PROCESS_RUN_ID FROM A_PROCESS_RUN WHERE START_DTTM < (to_date (''' || purge_start_time || ''') - ' || PRT_DAYS || '))');

      PURGE_ONLY_PROC ('A_PROCESS_RUN', 'START_DTTM < (to_date (''' || purge_start_time || ''') - ' || PRT_DAYS || ')');

      PURGE_ONLY_PROC ('TRAN_LOG_MESSAGE','TRAN_LOG_ID IN ( SELECT TRAN_LOG_ID FROM TRAN_LOG WHERE LAST_UPDATED_DTTM < (to_date (''' || PURGE_START_TIME || ''') - ' || TL_DAYS || '))');                                              

      PURGE_ONLY_PROC ('TRAN_LOG', 'LAST_UPDATED_DTTM < (to_date (''' || purge_start_time || ''') - ' || TL_DAYS || ')');

      PURGE_ONLY_PROC ('ERROR_LOG_ARGS','ERROR_ID IN ( SELECT ERROR_ID FROM ERROR_LOG WHERE CREATED_DTTM < (to_date (''' || PURGE_START_TIME || ''') - ' || EL_DAYS || '))');                                              

      PURGE_ONLY_PROC ('ERROR_LOG', 'CREATED_DTTM < (to_date (''' || PURGE_START_TIME || ''') - ' || EL_DAYS || ')');

      PURGE_ONLY_PROC ('A_FACILITY_WORKLOAD', 'FW_DATE < (to_date (''' || PURGE_START_TIME || ''') - ' || AFW_DAYS || ')');

      QUIET_DROP ('TABLE', 'PURGE_ONLY_GTT');

      V_STMT := 'CREATE TABLE PURGE_ONLY_GTT tablespace DOM_DT_TBS AS SELECT PURCHASE_ORDERS_ID FROM PURCHASE_ORDERS WHERE PURCHASE_ORDERS_TYPE = 2 AND PURCHASE_ORDERS_STATUS IN (940, 950)
                                                                      AND LAST_UPDATED_DTTM < (to_date (''' || purge_start_time || ''') - ' || PO_DAYS || ')';
      execute immediate V_STMT; 


      PURGE_ONLY_PROC ('A_ALLOC_COST_TRACE_CARTON', 'COST_TRACE_ID IN (SELECT COST_TRACE_ID FROM A_ALLOC_COST_TRACE WHERE ORDER_ID IN (SELECT PURCHASE_ORDERS_ID FROM PURGE_ONLY_GTT))');      

      PURGE_ONLY_PROC ('A_ALLOC_COST_TRACE', 'ORDER_ID IN (SELECT PURCHASE_ORDERS_ID FROM PURGE_ONLY_GTT)');     

      PURGE_ONLY_PROC ('A_ALLOCATION_TRACE_DETAIL', '(TC_PURCHASE_ORDER_ID, TC_COMPANY_ID) IN (SELECT TC_PURCHASE_ORDERS_ID, TC_COMPANY_ID FROM PURCHASE_ORDERS 
                                                      WHERE PURCHASE_ORDERS_ID IN (SELECT PURCHASE_ORDERS_ID FROM PURGE_ONLY_GTT))');      

      PURGE_ONLY_PROC ('A_FACILITY_TRACE_DETAIL', 'A_ORDERLINEID IN (SELECT PURCHASE_ORDERS_LINE_ITEM_ID FROM PURCHASE_ORDERS_LINE_ITEM WHERE PURCHASE_ORDERS_ID IN (SELECT PURCHASE_ORDERS_ID FROM PURGE_ONLY_GTT))');      

      PURGE_ONLY_PROC ('A_FACILITY_TRACE', 'A_ORDERLINEID IN (SELECT PURCHASE_ORDERS_LINE_ITEM_ID FROM PURCHASE_ORDERS_LINE_ITEM WHERE PURCHASE_ORDERS_ID IN (SELECT PURCHASE_ORDERS_ID FROM PURGE_ONLY_GTT))');      

      delete from ARCHIVE_ERROR_LOG;
      commit;

      archive_pkg.log_archive (0000, 'PURGE_OTHER_LOGS_EVENTS - ENDS', NULL, NULL, 0);	  
	  
      EXCEPTION
      WHEN OTHERS THEN
          nsqlcode := SQLCODE;
          VSQLERRM := SQLERRM;
          ARCHIVE_PKG.LOG_ERROR (NSQLCODE, VSQLERRM);
          QUIET_DROP ('TABLE', 'PURGE_ONLY_GTT');
          ROLLBACK;
      
end PURGE_OTHER_LOGS_EVENTS;
/


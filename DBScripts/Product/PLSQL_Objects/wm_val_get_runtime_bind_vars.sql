create or replace procedure wm_val_get_runtime_bind_vars
(
    p_user_id           in varchar2,
    p_job_name          in val_job_hdr.job_name%type,
    p_rt_bind_list_csv  in varchar2,
    p_txn_id            in val_result_hist.txn_id%type
)
as
    v_rt_bind_list_csv_lc varchar2(1000) := lower(p_rt_bind_list_csv);
    v_bind_val            val_runtime_binds.bind_val%type;
    v_bind_val_int        val_runtime_binds.bind_val_int%type;
    v_bind_val_len        number(5);
    v_start_pos           number(5);
    pragma autonomous_transaction;
begin
    for sql_rec in
    (
        select vjd.val_job_dtl_id, vsb.bind_var
        from val_job_hdr vjh
        join val_job_dtl vjd on vjd.val_job_hdr_id = vjh.val_job_hdr_id
            and vjd.is_locked = 0
        join val_sql_binds vsb on vsb.val_job_dtl_id = vjd.val_job_dtl_id
            and vsb.bind_val is null
        where vjh.job_name = p_job_name and vjh.is_locked = 0
    )
    loop
        v_start_pos := instr(v_rt_bind_list_csv_lc, sql_rec.bind_var, 1, 1);
        if (v_start_pos = 0)
        then
            continue;
        end if;
        
        v_start_pos := v_start_pos + length(sql_rec.bind_var) + 1;
        v_bind_val_len := instr(p_rt_bind_list_csv, ',', v_start_pos, 1)
            - v_start_pos;
        v_bind_val := substr(p_rt_bind_list_csv, v_start_pos, v_bind_val_len);

        insert into val_runtime_binds
        (
            txn_id, val_job_dtl_id, bind_var, bind_val, user_id, 
            create_date_time, mod_date_time, bind_val_int
        )
        values
        (
            p_txn_id, sql_rec.val_job_dtl_id, sql_rec.bind_var, v_bind_val,
            p_user_id, sysdate, sysdate, v_bind_val_int
        );
    end loop;
    
    commit;
end;
/
show errors;

create or replace procedure manh_wave_check_all_capacities
( 
    p_user_id               in ucl_user.user_name%type,
    p_pull_all_swc          in wave_parm.pull_all_swc%type,
    p_reject_distro_rule    in wave_parm.reject_distro_rule%type,
    p_max_qty               in wave_parm.max_units%type,
    p_max_vol               in wave_parm.max_vol%type,
    p_max_wt                in wave_parm.max_wt%type,
    p_max_rte_nbrs          in wave_parm.retail_max_routes%type,
    p_max_store_nbrs        in wave_parm.retail_max_stores%type,
    p_max_items             in wave_parm.retail_max_skus%type,
    p_max_orders            in wave_parm.max_orders%type,
    p_max_order_lines       in wave_parm.max_order_lines%type,
    p_remaining_rte_nbrs    in out wave_parm.retail_max_routes%type,
    p_remaining_store_nbrs  in out wave_parm.retail_max_stores%type,
    p_remaining_items       in out wave_parm.retail_max_skus%type,
    p_remaining_qty         in out wave_parm.max_units%type,
    p_remaining_vol         in out wave_parm.max_vol%type,
    p_remaining_wt          in out wave_parm.max_wt%type,
    p_remaining_orders      in out wave_parm.max_orders%type,
    p_remaining_order_lines in out wave_parm.max_order_lines%type
)
as
    v_remaining_sams        wave_work_type_stats.sams_capcty%type;
    v_remaining_units       wave_work_type_stats.units_capcty%type;
    v_remaining_lines       wave_work_type_stats.lines_capcty%type;
    v_remaining_piks        wave_work_type_stats.piks_capcty%type;
    v_remaining_dlrs        wave_work_type_stats.dlrs_capcty%type;
    v_remaining_lpns        wave_work_type_stats.carton_capcty%type;
begin
    -- perform wave and work type cap checks if the 'dont care' value isnt
    -- setup; this is for one rule
    if (p_max_rte_nbrs < 999999999)
    then
        manh_wave_check_rte_capcty(p_user_id, p_pull_all_swc,
            p_reject_distro_rule, p_remaining_rte_nbrs);
    end if;

    if (p_max_store_nbrs < 999999999)
    then
        manh_wave_check_store_capcty(p_user_id, p_pull_all_swc,
            p_reject_distro_rule, p_remaining_store_nbrs);
    end if;

    if (p_max_items < 999999999)
    then
        manh_wave_check_item_capcty(p_user_id, p_pull_all_swc,
            p_reject_distro_rule, p_remaining_items);
    end if;

    if (p_max_wt < 999999999)
    then
        manh_wave_check_wt_capcty(p_user_id, p_pull_all_swc,
            p_reject_distro_rule, p_remaining_wt);
    end if;

    if (p_max_vol < 999999999)
    then
        manh_wave_check_vol_capcty(p_user_id, p_pull_all_swc,
            p_reject_distro_rule, p_remaining_vol);
    end if;

    if (p_max_qty < 999999999)
    then
        manh_wave_check_qty_capcty(p_user_id, p_pull_all_swc,
            p_reject_distro_rule, p_remaining_qty);
    end if;

    if (p_max_orders < 999999999)
    then
        manh_wave_check_orders_capcty(p_user_id, p_pull_all_swc,
            p_reject_distro_rule, p_remaining_orders);
    end if;

    for wts_rec in
    (
        select t.work_type, t.max_sams, t.remaining_sams, t.max_units,
            t.remaining_units, t.max_lines, t.remaining_lines, t.max_piks,
            t.remaining_piks, t.max_dlrs, t.remaining_dlrs, t.max_lpns,
            t.remaining_lpns
        from tmp_wave_work_type_stats t
    )
    loop
        v_remaining_sams := wts_rec.remaining_sams;
        v_remaining_units := wts_rec.remaining_units;
        v_remaining_lines := wts_rec.remaining_lines;
        v_remaining_piks := wts_rec.remaining_piks;
        v_remaining_dlrs := wts_rec.remaining_dlrs;
        v_remaining_lpns := wts_rec.remaining_lpns;

        if (wts_rec.max_lines < 999999999)
        then
            wm_wave_check_owt_hdr_capcty(p_user_id, p_pull_all_swc, p_reject_distro_rule, 
                v_remaining_sams, wts_rec.work_type, p_cap_col_str => 'nbr_of_lines');
        end if;

        if (wts_rec.max_sams < 9999999)
        then
            wm_wave_check_owt_hdr_capcty(p_user_id, p_pull_all_swc, p_reject_distro_rule, 
                v_remaining_sams, wts_rec.work_type, p_cap_col_str => 'sam');
        end if;

        if (wts_rec.max_units < 9999999)
        then
            wm_wave_check_owt_hdr_capcty(p_user_id, p_pull_all_swc, p_reject_distro_rule, 
                v_remaining_sams, wts_rec.work_type, p_cap_col_str => 'nbr_of_units');
        end if;

        if (wts_rec.max_piks < 999999999)
        then
            wm_wave_check_owt_hdr_capcty(p_user_id, p_pull_all_swc, p_reject_distro_rule, 
                v_remaining_sams, wts_rec.work_type, p_cap_col_str => 'nbr_of_piks');
        end if;

        if (wts_rec.max_dlrs < 999999999)
        then
            wm_wave_check_owt_hdr_capcty(p_user_id, p_pull_all_swc, p_reject_distro_rule, 
                v_remaining_dlrs, wts_rec.work_type, p_cap_col_str => 'nbr_of_dlrs');
        end if;

        if (wts_rec.max_lpns < 999999999)
        then
            wm_wave_check_owt_hdr_capcty(p_user_id, p_pull_all_swc, p_reject_distro_rule, 
                v_remaining_sams, wts_rec.work_type, p_cap_col_str => 'nbr_of_lpns');
        end if;

        -- reset wt caps; the implicit cursor does not reference these values again
        update tmp_wave_work_type_stats t
        set t.remaining_sams = v_remaining_sams, t.remaining_units = v_remaining_units, 
            t.remaining_lines = v_remaining_lines, t.remaining_piks = v_remaining_piks, 
            t.remaining_dlrs = v_remaining_dlrs, t.remaining_lpns = v_remaining_lpns
        where t.work_type = wts_rec.work_type;
    end loop;

    if (p_max_order_lines < 999999999)
    then
        manh_check_wp_lines_capcty(p_user_id, p_pull_all_swc,
            p_reject_distro_rule, p_remaining_order_lines);
    end if;

    -- reset available capacities after all checks are completed
    select 
        case when p_max_rte_nbrs < 999999999 
            then p_remaining_rte_nbrs - count(distinct t1.dsg_static_route_id)
            else 999999999 end,
        case when p_max_store_nbrs < 999999999 
            then p_remaining_store_nbrs - count(distinct t1.d_facility_id)
            else 999999999 end,
        case when p_max_items < 999999999 
            then p_remaining_items - count(distinct t1.item_id)
            else 999999999 end,
        case when p_max_qty < 999999999 
            then p_remaining_qty - coalesce(sum(t2.qty_soft_alloc), 0)
            else 999999999 end,
        case when p_max_wt < 999999999 then p_remaining_wt
            - coalesce(sum(t2.qty_soft_alloc * ic.unit_weight), 0)
            else 999999999 end,
        case when p_max_vol < 999999999 then p_remaining_vol 
            - coalesce(sum(t2.qty_soft_alloc * ic.unit_volume), 0)
            else 999999999 end,
	case when p_max_orders < 999999999
	    then p_remaining_orders - count(distinct t1.order_id)
            else 999999999 end,
        case when p_max_order_lines < 999999999 
            then p_remaining_order_lines - count(distinct t1.line_item_id)
            else 999999999 end
    into p_remaining_rte_nbrs, p_remaining_store_nbrs, p_remaining_items, 
        p_remaining_qty, p_remaining_wt, p_remaining_vol,  p_remaining_orders,p_remaining_order_lines
    from tmp_wave_selected_orders t1
    join tmp_ord_dtl_sku_invn t2 on t2.order_id = t1.order_id
        and t2.line_item_id = t1.line_item_id
    join item_cbo ic on ic.item_id = t2.item_id;
    wm_cs_log('Remaining rte cap ' || p_remaining_rte_nbrs || ',store cap '
        || p_remaining_store_nbrs || ',item cap ' || p_remaining_items
        || ',qty cap ' || p_remaining_qty || ',wt cap ' || p_remaining_wt
        || ',vol cap ' || p_remaining_vol || ',orders cap ' || p_remaining_orders);
end;
/
show errors;

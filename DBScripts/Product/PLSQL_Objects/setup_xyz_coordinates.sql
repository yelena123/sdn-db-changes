create or replace procedure setup_xyz_coordinates
(
    p_locn_parm_id in number,
    p_locn_seq_nbr in number,
    p_stat_code in number,
    p_locn_class in varchar2,  
    p_x_coord_chk in out varchar2,
    p_y_coord_chk in out varchar2,
    p_z_coord_chk in out varchar2,
    p_v_x_coord in out locn_hdr.x_coord%type,
    p_v_y_coord in out locn_hdr.y_coord%type,
    p_v_z_coord in out locn_hdr.z_coord%type,
    p_repl_x_coord_chk in out varchar2,
    p_repl_y_coord_chk in out varchar2,
    p_repl_z_coord_chk in out varchar2,    
    p_v_repl_x_coord in out pick_locn_hdr.repl_x_coord%type,
    p_v_repl_y_coord in out pick_locn_hdr.repl_y_coord%type,
    p_v_repl_z_coord in out pick_locn_hdr.repl_z_coord%type
)
as

--$Revision: 3$

begin
    
    if p_stat_code in (50,82,83,85)
    then
        if get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'01','04'
            ,p_stat_code => p_stat_code) = -9 and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'01','05'
            ,p_stat_code => p_stat_code) = -9 and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'01','06'
            ,p_stat_code => p_stat_code) = -9 and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'01','07'
            ,p_stat_code => p_stat_code) = -9 and
            p_x_coord_chk = 'N' 
        then
            p_x_coord_chk := 'Y';
            p_v_x_coord   := -9;
        end if;

        if get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'02','04'
            ,p_stat_code => p_stat_code) = -9 and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'02','05'
            ,p_stat_code => p_stat_code) = -9 and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'02','06'
            ,p_stat_code => p_stat_code) = -9 and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'02','07'
            ,p_stat_code => p_stat_code) = -9 and
            p_y_coord_chk = 'N' 
        then
            p_y_coord_chk := 'Y';
            p_v_y_coord   := -9;
        end if;

        if get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'03','04'
            ,p_stat_code => p_stat_code) = -9 and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'03','05'
            ,p_stat_code => p_stat_code) = -9 and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'03','06'
            ,p_stat_code => p_stat_code) = -9 and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'03','07'
            ,p_stat_code => p_stat_code) = -9 and
            p_z_coord_chk = 'N' 
        then
            p_z_coord_chk := 'Y';
            p_v_z_coord   := -9;
        end if;
    end if;

    if p_stat_code in (10,50,82,83,85)
    then
        if get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'01','04'
            ,p_stat_code => p_stat_code) is not null and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'01','05'
            ,p_stat_code => p_stat_code) is not null and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'01','06'
            ,p_stat_code => p_stat_code) is not null and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'01','07'
            ,p_stat_code => p_stat_code) is not null and
            p_x_coord_chk = 'N' 
        then
            p_x_coord_chk := get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr
                ,'01','05');
        end if;

        if get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'02','04'
            ,p_stat_code => p_stat_code) is not null and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'02','05'
            ,p_stat_code => p_stat_code) is not null and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'02','06'
            ,p_stat_code => p_stat_code) is not null and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'02','07'
            ,p_stat_code => p_stat_code) is not null and
            p_y_coord_chk = 'N' 
        then
            p_y_coord_chk := get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,
                '02','05');
        end if;

        if get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'03','04'
            ,p_stat_code => p_stat_code) is not null and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'03','05'
            ,p_stat_code => p_stat_code) is not null and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'03','06'
            ,p_stat_code => p_stat_code) is not null and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'03','07'
            ,p_stat_code => p_stat_code) is not null and
            p_z_coord_chk = 'N' 
        then
            p_z_coord_chk := get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr
                ,'03','05');
        end if;
    end if;

    if p_stat_code in (50,82,83,85) and p_x_coord_chk = 'N'
    then
        p_v_x_coord := null;
    end if;

    if p_stat_code in (50,82,83,85) and p_y_coord_chk = 'N'
    then
        p_v_y_coord := null;
    end if;

    if p_stat_code in (50,82,83,85) and p_z_coord_chk = 'N'
    then
        p_v_z_coord := null;
    end if;

    if p_locn_class in ('A','C') 
    then
        if p_stat_code in (50,83,85)
        then
            if get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'64','69'
                ,p_stat_code => p_stat_code) = -9 and
                get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'64','70'
                ,p_stat_code => p_stat_code) = -9 and
                get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'64','71'
                ,p_stat_code => p_stat_code) = -9 and
                get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'64','72'
                ,p_stat_code => p_stat_code) = -9 and
                p_repl_x_coord_chk = 'N' 
            then
                p_repl_x_coord_chk := 'Y';
                p_v_repl_x_coord := -9 ;
            end if;

            if get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'65','69'
                ,p_stat_code => p_stat_code) = -9 and
                get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'65','70'
                ,p_stat_code => p_stat_code) = -9 and
                get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'65','71'
                ,p_stat_code => p_stat_code) = -9 and
                get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'65','72'
                ,p_stat_code => p_stat_code) = -9 and
                p_repl_y_coord_chk = 'N' 
            then
                p_repl_y_coord_chk := 'Y';
                p_v_repl_y_coord := -9 ;
            end if;

            if get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'66'
                ,'69',p_stat_code => p_stat_code) = -9 and
                get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'66','70'
                ,p_stat_code => p_stat_code) = -9 and
                get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'66','71'
                ,p_stat_code => p_stat_code) = -9 and
                get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'66','72'
                ,p_stat_code => p_stat_code) = -9 and
                p_repl_z_coord_chk = 'N' 
            then
                p_repl_z_coord_chk := 'Y';
                p_v_repl_z_coord := -9 ;
            end if;
        end if;
      
        if get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'64','69'
            ,p_stat_code => p_stat_code) is not null and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'64','70'
            ,p_stat_code => p_stat_code) is not null and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'64','71'
            ,p_stat_code => p_stat_code) is not null and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'64','72'
            ,p_stat_code => p_stat_code) is not null and
            p_repl_x_coord_chk = 'N' 
        then
            p_repl_x_coord_chk := get_locn_prop_value(p_locn_parm_id
                ,p_locn_seq_nbr,'64','70');
        end if;

        if get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'65','69'
            ,p_stat_code => p_stat_code) is not null and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'65','70'
            ,p_stat_code => p_stat_code) is not null and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'65','71'
            ,p_stat_code => p_stat_code) is not null and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'65','72'
            ,p_stat_code => p_stat_code) is not null and
            p_repl_y_coord_chk = 'N' 
        then
            p_repl_y_coord_chk := get_locn_prop_value(p_locn_parm_id
                ,p_locn_seq_nbr,'65','70');
        end if;

        if get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'66','69'
            ,p_stat_code => p_stat_code) is not null and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'66','70'
            ,p_stat_code => p_stat_code) is not null and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'66','71'
            ,p_stat_code => p_stat_code) is not null and
            get_locn_prop_value(p_locn_parm_id,p_locn_seq_nbr,'66','72'
            ,p_stat_code => p_stat_code) is not null and
            p_repl_z_coord_chk = 'N' 
        then
            p_repl_z_coord_chk  := get_locn_prop_value(p_locn_parm_id
                ,p_locn_seq_nbr,'66','70');
        end if;

   
        if p_stat_code in (50,83,85) and p_repl_x_coord_chk = 'N'
        then
            p_v_repl_x_coord  := null;
        end if;

        if p_stat_code in (50,83,85) and p_repl_y_coord_chk = 'N'
        then
            p_v_repl_y_coord  := null;
        end if;

        if p_stat_code in (50,83,85) and p_repl_z_coord_chk = 'N'
        then
            p_v_repl_z_coord  := null;
        end if;
    end if;
end;
/

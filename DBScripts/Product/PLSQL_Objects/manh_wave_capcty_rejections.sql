create or replace procedure manh_wave_capcty_rejections
(
    p_user_id                   in ucl_user.user_name%type,
    p_pull_all_ship_group_id    in wave_parm.pull_all_swc%type,
    p_reject_distro_rule        in wave_parm.reject_distro_rule%type,
    p_rsn_code                  in order_line_item.reason_code%type
)
as
    v_rsn_code          order_line_item.reason_code%type := p_rsn_code;
    v_ship_wave_nbr     ship_wave_parm.ship_wave_nbr%type;
    v_rule_type         rule_hdr.rule_type%type;
    v_swc_failed        varchar2(2) := '6';
    v_item_grp_failed   varchar2(2) := '10';
    v_order_grp_failed  varchar2(2) := '8';
begin
    select t.ship_wave_nbr, t.curr_rule_type
    into v_ship_wave_nbr, v_rule_type
    from tmp_wave_selection_parms t;

    if (v_rule_type = 'PF')
    then
        v_order_grp_failed  := '31';
        v_rsn_code := case p_rsn_code when '1' then '26' when '2' then '27'
            when '3' then '29' when '4' then '28' when '5' then '30'
            else p_rsn_code end;
    end if;

    if (p_pull_all_ship_group_id = 'Y')
    then
        if (p_reject_distro_rule = '1')
        then
            -- reject soft allocs by swc/order
            delete from tmp_ord_dtl_sku_invn t
            where exists
            (
                select 1
                from tmp_wave_rejected_lines t1
                where t1.order_id = t.order_id
                    or t1.ship_group_id = t.ship_group_id
            );
			wm_cs_log('Rejected lines by swc/order ' || sql%rowcount, p_sql_log_level => 1);
            -- group rejection by swc
            update order_line_item oli
            set oli.last_updated_source = p_user_id,
                oli.last_updated_dttm = current_timestamp,
                oli.reason_code = v_swc_failed, oli.ship_wave_nbr = v_ship_wave_nbr
            where exists
            (
                select 1
                from tmp_wave_selected_orders t1
                where t1.order_id = oli.order_id 
                    and t1.line_item_id = oli.line_item_id
                    and exists
                    (
                        select 1
                        from tmp_wave_rejected_lines t2
                        where t2.ship_group_id = t1.ship_group_id
                    )
            );
			wm_cs_log('Group rejection by swc ' || sql%rowcount, p_sql_log_level => 1);
            -- group rejection by order
            update order_line_item oli
            set oli.last_updated_source = p_user_id,
                oli.last_updated_dttm = current_timestamp,
                oli.reason_code = v_order_grp_failed, oli.ship_wave_nbr = v_ship_wave_nbr
            where exists
            (
                select 1
                from tmp_wave_selected_orders t1
                where t1.order_id = oli.order_id 
                    and t1.line_item_id = oli.line_item_id
                    and exists
                    (
                        select 1
                        from tmp_wave_rejected_lines t2
                        where t2.order_id = t1.order_id
                    )
            );
			wm_cs_log('Group rejection by order ' || sql%rowcount, p_sql_log_level => 1);
            -- primary rejection for specific lines
            update order_line_item oli
            set oli.last_updated_source = p_user_id,
                oli.last_updated_dttm = current_timestamp,
                oli.reason_code = v_rsn_code, oli.ship_wave_nbr = v_ship_wave_nbr
            where exists
            (
                select 1
                from tmp_wave_rejected_lines t2
                where t2.order_id = oli.order_id
                    and t2.line_item_id = oli.line_item_id
            );
			wm_cs_log('Primary rejection of specific lines ' || sql%rowcount, p_sql_log_level => 1);
            -- remove all lines with these deleted lines/orders/swcs
            delete from tmp_wave_selected_orders t
            where exists
            (
                select 1
                from tmp_wave_rejected_lines t1
                where t1.order_id = t.order_id
                    or t1.ship_group_id = t.ship_group_id
            );
			wm_cs_log('Undo lines based on swc/order ' || sql%rowcount, p_sql_log_level => 1);
        elsif (p_reject_distro_rule = '2')
        then
            -- reject by swc/item
            delete from tmp_ord_dtl_sku_invn t
            where exists
            (
                select 1
                from tmp_wave_rejected_lines t1
                where (t1.order_id = t.order_id
                    and t1.line_item_id = t.line_item_id)
                    or t1.item_id = t.item_id
                    or t1.ship_group_id = t.ship_group_id
            );
			wm_cs_log('Rejected lines by swc/item ' || sql%rowcount, p_sql_log_level => 1);
            -- group rejection by swc
            update order_line_item oli
            set oli.last_updated_source = p_user_id,
                oli.last_updated_dttm = current_timestamp,
                oli.reason_code = v_swc_failed, oli.ship_wave_nbr = v_ship_wave_nbr
            where exists
            (
                select 1
                from tmp_wave_selected_orders t1
                where t1.order_id = oli.order_id 
                    and t1.line_item_id = oli.line_item_id
                    and exists
                    (
                        select 1
                        from tmp_wave_rejected_lines t2
                        where t2.ship_group_id = t1.ship_group_id
                    )
            );
			wm_cs_log('Group rejection by swc ' || sql%rowcount, p_sql_log_level => 1);
            -- group rejection by item
            update order_line_item oli
            set oli.last_updated_source = p_user_id,
                oli.last_updated_dttm = current_timestamp,
                oli.reason_code = v_item_grp_failed, oli.ship_wave_nbr = v_ship_wave_nbr
            where exists
                (
                    select 1
                    from tmp_wave_selected_orders t1
                    where t1.order_id = oli.order_id 
                        and t1.line_item_id = oli.line_item_id
                )
                and exists
                (
                    select 1
                    from tmp_wave_rejected_lines t2
                    where t2.item_id = oli.item_id
                );
			wm_cs_log('Group rejection by item ' || sql%rowcount, p_sql_log_level => 1);
            -- primary rejection for specific orders
            update order_line_item oli
            set oli.last_updated_source = p_user_id,
                oli.last_updated_dttm = current_timestamp,
                oli.reason_code = v_rsn_code, oli.ship_wave_nbr = v_ship_wave_nbr
            where exists
            (
                select 1
                from tmp_wave_rejected_lines t2
                where t2.order_id = oli.order_id
                    and t2.line_item_id = oli.line_item_id
            );
			wm_cs_log('Primary rejection of specific lines ' || sql%rowcount, p_sql_log_level => 1);
            -- remove all lines with these deleted lines/swcs/items
            delete from tmp_wave_selected_orders t
            where exists
            (
                select 1
                from tmp_wave_rejected_lines t1
                where (t1.order_id = t.order_id
                    and t1.line_item_id = t.line_item_id)
                    or t1.item_id = t.item_id
                    or t1.ship_group_id = t.ship_group_id
            );
			wm_cs_log('Undo of lines due to swc/items/specific lines' || sql%rowcount, p_sql_log_level => 1);
        else
            -- reject soft allocs by swc/line
            delete from tmp_ord_dtl_sku_invn t
            where exists
            (
                select 1
                from tmp_wave_rejected_lines t1
                where (t1.order_id = t.order_id
                    and t1.line_item_id = t.line_item_id)
                    or t1.ship_group_id = t.ship_group_id
            );
			wm_cs_log('Rejected SA by swc/line ' || sql%rowcount, p_sql_log_level => 1);
            -- group rejection by swc
            update order_line_item oli
            set oli.last_updated_source = p_user_id,
                oli.last_updated_dttm = current_timestamp,
                oli.reason_code = v_swc_failed, oli.ship_wave_nbr = v_ship_wave_nbr
            where exists
            (
                select 1
                from tmp_wave_selected_orders t1
                where t1.order_id = oli.order_id
                    and t1.line_item_id = oli.line_item_id
                    and exists
                    (
                        select 1
                        from tmp_wave_rejected_lines t2
                        where t2.ship_group_id = t1.ship_group_id
                    )
            );
			wm_cs_log('Group rejection by swc ' || sql%rowcount, p_sql_log_level => 1);
            -- primary rejection for specific orders
            update order_line_item oli
            set oli.last_updated_source = p_user_id,
                oli.last_updated_dttm = current_timestamp,
                oli.reason_code = v_rsn_code, oli.ship_wave_nbr = v_ship_wave_nbr
            where exists
            (
                select 1
                from tmp_wave_rejected_lines t2
                where t2.order_id = oli.order_id
                    and t2.line_item_id = oli.line_item_id
            );
			wm_cs_log('Primary rejections of specific lines ' || sql%rowcount, p_sql_log_level => 1);
            -- remove all orders with these deleted items/swcs
            delete from tmp_wave_selected_orders t
            where exists
            (
                select 1
                from tmp_wave_rejected_lines t1
                where (t1.order_id = t.order_id
                    and t1.line_item_id = t.line_item_id)
                    or t1.ship_group_id = t.ship_group_id
            );
			wm_cs_log('Undo OLI due to specific lines/swcs ' || sql%rowcount, p_sql_log_level => 1);
        end if;
    else
        -- no swc processing
        if (p_reject_distro_rule = '1')
        then
            -- reject soft allocs by order/line
            delete from tmp_ord_dtl_sku_invn t
            where exists
            (
                select 1
                from tmp_wave_rejected_lines t1
                where t1.order_id = t.order_id
            );
			wm_cs_log('Rejected lines by order ' || sql%rowcount, p_sql_log_level => 1);
            -- group rejection by order
            update order_line_item oli
            set oli.last_updated_source = p_user_id,
                oli.last_updated_dttm = current_timestamp,
                oli.reason_code = v_order_grp_failed, oli.ship_wave_nbr = v_ship_wave_nbr
            where exists
                (
                    select 1
                    from tmp_wave_selected_orders t1
                    where t1.order_id = oli.order_id 
                        and t1.line_item_id = oli.line_item_id
                )
                and exists
                (
                    select 1
                    from tmp_wave_rejected_lines t1
                    where t1.order_id = oli.order_id
                );
			wm_cs_log('Group rejection by order ' || sql%rowcount, p_sql_log_level => 1);
            -- primary rejections for specific lines
            update order_line_item oli
            set oli.last_updated_source = p_user_id,
                oli.last_updated_dttm = current_timestamp,
                oli.reason_code = v_rsn_code, oli.ship_wave_nbr = v_ship_wave_nbr
            where exists
            (
                select 1
                from tmp_wave_rejected_lines t1
                where t1.order_id = oli.order_id
                    and t1.line_item_id = oli.line_item_id
            );
			wm_cs_log('Primary rejection of specific lines ' || sql%rowcount, p_sql_log_level => 1);
            -- remove orders and leave them unselected
            delete from tmp_wave_selected_orders t
            where exists
            (
                select 1
                from tmp_wave_rejected_lines t1
                where t1.order_id = t.order_id
            );
			wm_cs_log('Undo of lines by order ' || sql%rowcount, p_sql_log_level => 1);
        elsif (p_reject_distro_rule = '2')
        then
            -- reject soft allocs by item/line
            delete from tmp_ord_dtl_sku_invn t
            where exists
            (
                select 1
                from tmp_wave_rejected_lines t1
                where (t1.order_id = t.order_id
                    and t1.line_item_id = t.line_item_id)
                    or t1.item_id = t.item_id
            );
			wm_cs_log('Rejected SA by item/specific line' || sql%rowcount, p_sql_log_level => 1);
            -- group rejection by item
            update order_line_item oli
            set oli.last_updated_source = p_user_id,
                oli.last_updated_dttm = current_timestamp,
                oli.reason_code = v_item_grp_failed, oli.ship_wave_nbr = v_ship_wave_nbr
            where exists
                (
                    select 1
                    from tmp_wave_selected_orders t1
                    where t1.order_id = oli.order_id 
                        and t1.line_item_id = oli.line_item_id
                )
                and exists
                (
                    select 1
                    from tmp_wave_rejected_lines t2
                    where t2.item_id = oli.item_id
                );
			wm_cs_log('Group rejection by item ' || sql%rowcount, p_sql_log_level => 1);
            -- primary rejections for specific lines
            update order_line_item oli
            set oli.last_updated_source = p_user_id,
                oli.last_updated_dttm = current_timestamp,
                oli.reason_code = v_rsn_code, oli.ship_wave_nbr = v_ship_wave_nbr
            where exists
            (
                select 1
                from tmp_wave_rejected_lines t2
                where t2.order_id = oli.order_id
                    and t2.line_item_id = oli.line_item_id
            );
			wm_cs_log('Primary rejections of specific lines ' || sql%rowcount, p_sql_log_level => 1);
            -- remove orders and leave them unselected
            delete from tmp_wave_selected_orders t   
            where exists
            (
                select 1
                from tmp_wave_rejected_lines t1
                where (t1.order_id = t.order_id
                    and t1.line_item_id = t.line_item_id)
                    or t1.item_id = t.item_id
            );
			wm_cs_log('Undo OLI by item/specific line' || sql%rowcount, p_sql_log_level => 1);
        else
            -- reject specific lines only
            delete from tmp_ord_dtl_sku_invn t
            where exists
            (
                select 1
                from tmp_wave_rejected_lines t1
                where t1.order_id = t.order_id
                    and t1.line_item_id = t.line_item_id
            );
			wm_cs_log('Rejected specific lines ' || sql%rowcount, p_sql_log_level => 1);
            -- primary rejections by line
            update order_line_item oli
            set oli.reason_code = v_rsn_code, oli.last_updated_source = p_user_id,
                oli.last_updated_dttm = current_timestamp,
                oli.ship_wave_nbr = v_ship_wave_nbr
            where exists
            (
                select 1
                from tmp_wave_rejected_lines t2
                where t2.order_id = oli.order_id
                    and t2.line_item_id = oli.line_item_id
            );
			wm_cs_log('Primary rejections of specific lines ' || sql%rowcount, p_sql_log_level => 1);
            -- remove orders and leave them unselected
            delete from tmp_wave_selected_orders t   
            where exists
            (
                select 1
                from tmp_wave_rejected_lines t1
                where t1.order_id = t.order_id
                    and t1.line_item_id = t.line_item_id
            );
			wm_cs_log('Undo OLI by specific lines ' || sql%rowcount, p_sql_log_level => 1);
        end if;
    end if;
end;
/

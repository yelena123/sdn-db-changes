create or replace procedure manh_diag_find_selection_rules
(
    p_facility_id           in facility.facility_id%type,
    p_ship_wave_nbr         in ship_wave_parm.ship_wave_nbr%type,
    p_preview_wave_flag     in number
)
as
    v_sql_piece_count       number(2) := 0;
    v_msg_length            number(9) := 0;
    v_msg_log_length        number(5) := 512;
    v_start_pos             number(5) := 0;
    v_str_length            number(5) := 0;
    v_ship_wave_parm_id     ship_wave_parm.ship_wave_parm_id%type;
    v_whse                  facility.whse%type;
    v_perf_pickng_wave      ship_wave_parm.perf_pickng_wave%type;
    v_perf_rte              ship_wave_parm.perf_rte%type;
    v_wave_type_indic       wave_parm.wave_type_indic%type;
    v_tc_company_id         wave_parm.tc_company_id%type;
    v_pull_all_swc          wave_parm.pull_all_swc%type;
    v_order_selection_sql   varchar2(32600);
    v_rte_wave_option       opt_param.param_value%type; 
begin
    select f.whse
    into v_whse
    from facility f
    where f.facility_id = p_facility_id;

    select swp.ship_wave_parm_id, coalesce(wp.tc_company_id, -1),
        coalesce(wp.wave_type_indic, '1'), coalesce(swp.perf_pickng_wave, 'N'),
        coalesce(swp.perf_rte, 'N'), coalesce(wp.pull_all_swc, 'N')
    into v_ship_wave_parm_id, v_tc_company_id, v_wave_type_indic, v_perf_pickng_wave,
        v_perf_rte, v_pull_all_swc
    from ship_wave_parm swp
    left join wave_parm wp on wp.wave_parm_id = swp.wave_parm_id
    where swp.ship_wave_nbr = p_ship_wave_nbr and swp.whse = v_whse;

    select coalesce(op.param_value, '1')
    into v_rte_wave_option
    from ship_wave_parm swp
    left join cons_template ct on ct.cons_template_id = swp.rte_wave_parm_id
    left join opt_param op on op.tc_company_id = ct.tc_company_id
        and op.opt_param_list_id = ct.opt_param_list_id
        and op.param_def_id ='routing_run_option'
        and op.param_group_id = 'CON_TEPE'
    -- check: and op.instance_num = ?
    where swp.ship_wave_parm_id = v_ship_wave_parm_id;    
    
    for rec in
    (
        select wrp.rule_id, wrp.rule_prty seq, rh.rule_type, 
            case when exists
            (
                select 1 
                from rule_sel_dtl rsd
                where rsd.rule_id = wrp.rule_id
                    and 
                    (
                        rsd.tbl_name = 'SHIPMENT'
                        or (rsd.tbl_name = 'ORDERS' 
                            and rsd.colm_name in ('SHIPMENT_ID', 'TC_SHIPMENT_ID'))
                    )
            ) then 'Y' else 'N' end is_wave_by_shpmt
        from wave_rule_parm wrp
        join rule_hdr rh on rh.rule_id = wrp.rule_id
        where wrp.wave_parm_id = v_ship_wave_parm_id and wrp.whse = v_whse
            and wrp.stat_code = 0
        order by wrp.rule_prty
    )
    loop
        manh_wave_build_selection_rule('Y', null, null, p_facility_id,
            v_perf_pickng_wave, v_perf_rte, p_preview_wave_flag,
            v_wave_type_indic, v_tc_company_id, v_pull_all_swc, rec.rule_id,
            rec.rule_type, rec.is_wave_by_shpmt, null,v_order_selection_sql);
        --dbms_output.put_line(v_order_selection_sql);

        v_msg_length := length(v_order_selection_sql);
        v_start_pos := 1;
        v_sql_piece_count := 0;
        while(v_start_pos <= v_msg_length)
        loop
            v_sql_piece_count := v_sql_piece_count + 1;
            v_str_length := case when length(substr(v_order_selection_sql, 
                v_start_pos)) > v_msg_log_length then v_msg_log_length
                else length(substr(v_order_selection_sql, v_start_pos)) end;
            insert into msg_log
            (
                msg_log_id, module, msg_id, pgm_id, ref_value_1, ref_value_2, 
                ref_value_3, ref_value_4, msg, create_date_time, mod_date_time,
                whse, log_date_time
            )
            values
            (
                msg_log_id_seq.nextval, 'WAVE', '1094', 'Rule Builder', 
                p_ship_wave_nbr, rec.rule_id, rec.seq, v_sql_piece_count,
                substr(v_order_selection_sql, v_start_pos, v_str_length), 
                sysdate, sysdate, v_whse, sysdate
            );
            v_start_pos := v_start_pos + v_str_length;
        end loop;
    end loop;
    
    commit;
end;
/

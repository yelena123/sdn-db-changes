create or replace procedure wm_val_exec_sql_intrnl
(
    p_cursor          in number,
    p_sql_id          in val_job_dtl.sql_id%type,
    p_sql_text        in val_sql.sql_text%type,
    p_bind_var_csv    in val_sql.bind_var_list%type,
    p_val_job_dtl_id  in val_job_dtl.val_job_dtl_id%type,
    p_bind_val_csv    in varchar2,
    p_txn_id          in val_result_hist.txn_id%type,
    p_result_csv      out varchar2
)
as
    v_sql_passed        number(1);
    v_num_actual_rows   number(9);
    v_num_expected_rows number(9);
    v_curr_row_num      number(9);
    v_int_col           number(9);
    v_varchar2_col      varchar2(50);
    v_msg               val_result_hist.msg%type;
begin
    p_result_csv := null;
    v_sql_passed := 0;
    dbms_sql.parse(p_cursor, p_sql_text, dbms_sql.native);

    wm_val_apply_bind_val_to_sql(p_cursor, p_sql_id, p_val_job_dtl_id, p_txn_id, p_bind_var_csv,
        p_bind_val_csv);

    wm_val_define_cursor_columns(p_cursor, p_sql_id, p_txn_id);

    select coalesce(max(ver.row_num), 0)
    into v_num_expected_rows
    from val_expected_results ver
    where ver.val_job_dtl_id = p_val_job_dtl_id;

    -- run it and fetch returned rows
    v_num_actual_rows := dbms_sql.execute(p_cursor);
    if (p_txn_id is not null)
    then
        wm_val_log('Sql touched ' || v_num_actual_rows || ' rows', p_sql_log_lvl => 2);
        if (v_num_expected_rows = 0)
        then
            -- data not setup or not a select stmt
            wm_val_add_job_result(p_val_job_dtl_id, p_msg => 'Ok', p_last_run_passed => 1);
            return;
        end if;
    end if;

    /*
    -- enable this for sql injection
    if (v_num_actual_rows != v_num_expected_rows)
    then
        update val_job_dtl vjd
        set vjd.mod_date_time = sysdate, vjd.last_run_passed = 0
        where vjd.val_job_dtl_id = p_val_job_dtl_id;
        dbms_output.put_line('Skipped processing Job dtl ' || p_val_job_dtl_id || ',sql ' || p_sql_id
            || ' expected rows ' || v_num_expected_rows || ', actual ' || v_num_actual_rows);
        
        -- skip validations of row level output
        continue;
    end if;
    */

    v_sql_passed := 0;
    v_curr_row_num := 0;
    while (dbms_sql.fetch_rows(p_cursor) > 0)
    loop
        -- for each selected column, based on datatype, get selected data
        -- into appropriate vars and compare
        v_curr_row_num := v_curr_row_num + 1;
        if (p_txn_id is not null)
        then
            wm_val_log('Processing row ' || v_curr_row_num, p_sql_log_lvl => 2);
            if (v_curr_row_num > v_num_expected_rows)
            then
                raise_application_error(-20050, 'Sql ' || p_sql_id
                    || ' failed due to fetching more rows than expected (' 
                    || v_num_expected_rows || ')');
            end if;
        end if;

        for col_rec in
        (
            -- for the situation where results are partially/fully configured but the sql is still
            -- tested anyway for auto-pop, ignore the configured results entirely
            select vpsl.pos, vpsl.col_type, ver.expected_value
            from val_parsed_select_list vpsl
            join val_expected_results ver on ver.pos = vpsl.pos and ver.row_num = v_curr_row_num
                and ver.val_job_dtl_id = p_val_job_dtl_id
            where vpsl.sql_id = p_sql_id and p_txn_id is not null
            union all
            select vpsl.pos, vpsl.col_type, null expected_value
            from val_parsed_select_list vpsl
            where vpsl.sql_id = p_sql_id and p_txn_id is null
            order by pos
        )
        loop
            v_msg := null;
            v_varchar2_col := null;
            v_int_col := null;
            if (col_rec.col_type = 2)
            then
                dbms_sql.column_value(p_cursor, col_rec.pos, v_int_col);
                v_sql_passed := case when v_int_col = to_number(col_rec.expected_value)
                    then 1 else 0 end;
            elsif (col_rec.col_type in (1, 96))
            then
                dbms_sql.column_value(p_cursor, col_rec.pos, v_varchar2_col);
                v_sql_passed := case when v_varchar2_col = col_rec.expected_value
                    then 1 else 0 end;
            else
                raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to finding unsupported data type '
                || col_rec.col_type);
            end if;
            
            if (p_txn_id is null)
            then
                -- a UI request to exec sql for auto population of expected result data
                p_result_csv := p_result_csv || coalesce(v_varchar2_col, to_char(v_int_col)) || ',';
            else
                wm_val_log('Expecting ' || col_rec.expected_value || ' at ('
                    || col_rec.pos || ',' || v_curr_row_num || '), found '
                    || coalesce(v_varchar2_col, to_char(v_int_col), 'null'), 
                    p_sql_log_lvl => 2);
            end if;

            if (p_txn_id is not null and v_sql_passed = 0)
            then
                raise_application_error(-20050, 'Sql ' || p_sql_id
                    || ' failed due to finding ' 
                    || coalesce(v_varchar2_col, to_char(v_int_col), 'null')
                    || ' in (row ' || v_curr_row_num || ', Pos ' || col_rec.pos
                    || '), expected ' || col_rec.expected_value );
            end if;
        end loop; -- columns fetch

        if (p_txn_id is not null and v_sql_passed = 0)
        then
            raise_application_error(-20050, 'Sql ' || p_sql_id
                || ' failed due to not finding matching result data');
        end if;
    end loop; -- rows fetch

    if (p_txn_id is not null)
    then
        if (v_curr_row_num < v_num_expected_rows and v_msg is null)
        then
            raise_application_error(-20050, 'Sql ' || p_sql_id || 
                ' failed due to returning fewer rows (' || v_curr_row_num 
                || ') than expected (' || v_num_expected_rows || ')');
        end if;

        wm_val_add_job_result(p_val_job_dtl_id, case v_sql_passed when 0 then v_msg else 'Ok' end, 
            v_sql_passed);
    end if;
end;
/
show errors;

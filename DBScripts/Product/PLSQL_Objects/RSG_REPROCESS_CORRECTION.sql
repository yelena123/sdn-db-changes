create or replace
PROCEDURE     RSG_REPROCESS_CORRECTION 
IS
  
  VTOTALS_DATE DATE;
  VWHSE VARCHAR2(3);
  VREFLECT_STD_ID NUMBER(9);
  
  CURSOR TOT_DATE IS SELECT TOTALS_DATE,WHSE from E_RSG_REPROCESS where reflect_std_id is null order by TOTALS_DATE;
  CURSOR REFLECT_GROUP IS select reflect_std_id from e_reflect_std where whse=VWHSE and reflect_group_code in (select DISTINCT reflect_group_code from e_emp_reflect_std where whse=VWHSE and  getwhsedatetime(REFLECT_STD_EMP_BEG_DATE_TIME,VWHSE) = getwhsedatetime(VTOTALS_DATE,VWHSE) );
  BEGIN
   OPEN TOT_DATE;
   LOOP
      FETCH TOT_DATE INTO VTOTALS_DATE, VWHSE;
      EXIT WHEN TOT_DATE%NOTFOUND;
         OPEN REFLECT_GROUP;
         LOOP
         FETCH REFLECT_GROUP INTO VREFLECT_STD_ID;
         EXIT WHEN REFLECT_GROUP%NOTFOUND;
      /* Update record */
      UPDATE  E_RSG_REPROCESS SET  REFLECT_STD_ID = VREFLECT_STD_ID, MOD_DATE_TIME= SYSDATE where whse = VWHSE and getwhsedatetime(totals_date,VWHSE) =getwhsedatetime(VTOTALS_DATE,VWHSE) and REFLECT_STD_ID is null;
          /* Insert record, if update fails */
       IF (SQL%ROWCOUNT <= 0) THEN
        INSERT INTO e_rsg_reprocess(REP_ID , TOTALS_DATE , REPROCESS_STATUS , WHSE , CREATE_DATE_TIME , MOD_DATE_TIME , USER_ID , MISC_TXT_1 , MISC_TXT_2 , MISC_NUM_1 , MISC_NUM_2 , VERSION_ID , REFLECT_STD_ID) values 
        (RSG_REP_ID.nextval,VTOTALS_DATE,0,VWHSE,sysdate,sysdate,'LMADMIN',null,null,0,0,0,VREFLECT_STD_ID);
       END IF;
       COMMIT;
    END LOOP;
    CLOSE REFLECT_GROUP;
    END LOOP;
    CLOSE TOT_DATE;
END;
/
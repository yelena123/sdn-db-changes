

DECLARE v_exists NUMBER;
BEGIN
SELECT COUNT(*) INTO v_exists FROM user_tables WHERE table_name ='C_EAN_PICKING';
IF v_exists = 0 THEN
EXECUTE IMMEDIATE 'CREATE TABLE C_EAN_PICKING (
                SEQ_NBR NUMBER(10) NOT NULL, 
				EAN_STRING VARCHAR2(100 CHAR) NOT NULL,
                ITEM VARCHAR2(50 CHAR) NOT NULL,
                OLPN VARCHAR2(50 CHAR),
                PROD_DATE VARCHAR2(10 CHAR),
                BATCH_NBR VARCHAR2(20 CHAR),
				COUNTER NUMBER(5) NOT NULL, 
				GTIN VARCHAR2(100 CHAR),
                CREATED_USER_ID VARCHAR2(100 CHAR), 
                CREATE_DATE_TIME TIMESTAMP,
                PRIMARY KEY (SEQ_NBR))';
end if;
end;
/

create or replace PROCEDURE C_WM63_CREATE_EAN_PICKING (
	p_SeqNbr		IN     C_EAN_PICKING.SEQ_NBR%type,
	p_EANBarcode	IN     C_EAN_PICKING.EAN_STRING%type,
	p_ItemName		IN     C_EAN_PICKING.ITEM%type,
	p_Olpn          IN     C_EAN_PICKING.OLPN%type,
	p_ProdDate      IN     C_EAN_PICKING.PROD_DATE%type,
	p_BatchNbr      IN     C_EAN_PICKING.BATCH_NBR%type,
	p_Counter       IN     C_EAN_PICKING.COUNTER%type,
	p_GTIN	        IN     C_EAN_PICKING.GTIN%type,
	p_User       	IN     C_EAN_PICKING.CREATED_USER_ID%type,
	p_result        OUT NUMBER)
   
   as
   recordExists         	NUMBER;
   BEGIN
   p_result := 0;
   recordExists :=0;
   
       
   select NVL(sum(SEQ_NBR),0) into  recordExists
				from C_EAN_PICKING 
				where OLPN	= p_Olpn
				and ITEM	= p_ItemName
				and COUNTER = p_Counter;
	
	if recordExists = 0 then
				
   insert into C_EAN_PICKING (
                SEQ_NBR , 
				EAN_STRING ,
                ITEM ,
                OLPN ,
                PROD_DATE,
                BATCH_NBR,
                COUNTER , 
				GTIN,
                CREATED_USER_ID, 
                CREATE_DATE_TIME)
                values
                (p_SeqNbr,p_EANBarcode,p_ItemName,p_Olpn,p_ProdDate,p_BatchNbr,p_Counter,p_GTIN,p_User,sysdate);
	else
		update C_EAN_PICKING set EAN_STRING = p_EANBarcode, prod_date = p_ProdDate, batch_nbr = p_BatchNbr, gtin = p_GTIN
				where OLPN	= p_Olpn
				and ITEM	= p_ItemName
				and COUNTER = p_Counter;
	end if;
	
  commit;
	EXCEPTION
		when others then 	
    p_result:= 1;
		 wm_cs_log('C_WM63_CREATE_EAN_PICKING error ' || SQLERRM);
     RAISE_APPLICATION_ERROR (-20010, SQLERRM);

     
				
END;
/

create or replace PROCEDURE C_WM63_GET_SUPPLIER_BARCODE (
	
	p_EANBarcode	IN     C_EAN_PICKING.EAN_STRING%type,
	p_ItemName		IN     C_EAN_PICKING.ITEM%type,	
	p_result        OUT NUMBER)
   
   as
   recordExists         	NUMBER;
   BEGIN
   p_result := 0;
   recordExists :=0;
   
    select NVL(count(ix.item_id),0) into  recordExists from item_cbo ic,ITEM_SUPPLIER_XREF_CBO ix
	where ic.item_id = ix.item_id
	and ic.item_name = p_ItemName
	and ix.SUPPLIER_ITEM_BARCODE = p_EANBarcode; 
	
	if recordExists > 0 then
		p_result := 1;
	end if;
	
	EXCEPTION
		when others then 	
    p_result:= 0;
		 wm_cs_log('C_WM63_GET_SUPPLIER_BARCODE error ' || SQLERRM);
     RAISE_APPLICATION_ERROR (-20010, SQLERRM);
			
END;
/

create or replace procedure C_WM63_SPLIT_UPDATE
(
    p_lpndetailid       in LPN_DETAIL.LPN_DETAIL_ID%type ,
	p_sizeValue       	in LPN_DETAIL.SIZE_VALUE%type ,
    p_prodDate     		in LPN_DETAIL.MANUFACTURED_DTTM%type,
	p_batchNbr     		in LPN_DETAIL.BATCH_NBR%type,
	p_gtin	     		in LPN_DETAIL.GTIN%type,
	p_splitUpdate		in NUMBER
)
as
    v_whse                	facility.whse%type;
	vlpndetailid			LPN_DETAIL.LPN_DETAIL_ID%type;
	vwminventoryid			WM_INVENTORY.WM_INVENTORY_ID%type;
      
begin 
  	
	wm_cs_log('C_WM63_SPLIT_UPDATE Update Start');
	
	if p_splitUpdate = 1 then
		
		select LPN_DETAIL_ID_SEQ.nextval into vlpndetailid from dual;
		select WM_INVENTORY_ID_SEQ.nextval into vwminventoryid from dual;
		
		Insert into LPN_DETAIL (TC_COMPANY_ID,LPN_ID,LPN_DETAIL_ID,LPN_DETAIL_STATUS,INTERNAL_ORDER_DTL_ID,DISTRIBUTION_ORDER_DTL_ID,RECEIVED_QTY,BUSINESS_PARTNER_ID,ITEM_ID,GTIN,STD_PACK_QTY,STD_SUB_PACK_QTY,STD_BUNDLE_QTY,INCUBATION_DATE,EXPIRATION_DATE,SHIP_BY_DATE,SELL_BY_DTTM,CONSUMPTION_PRIORITY_DTTM,MANUFACTURED_DTTM,CNTRY_OF_ORGN,INVENTORY_TYPE,PRODUCT_STATUS,ITEM_ATTR_1,ITEM_ATTR_2,ITEM_ATTR_3,ITEM_ATTR_4,ITEM_ATTR_5,ASN_DTL_ID,PACK_WEIGHT,ESTIMATED_WEIGHT,ESTIMATED_VOLUME,SIZE_VALUE,WEIGHT,QTY_UOM_ID,WEIGHT_UOM_ID,VOLUME_UOM_ID,ASSORT_NBR,CUT_NBR,PURCHASE_ORDERS_ID,TC_PURCHASE_ORDERS_ID,PURCHASE_ORDERS_LINE_ID,TC_PURCHASE_ORDERS_LINE_ID,HIBERNATE_VERSION,INTERNAL_ORDER_ID,INSTRTN_CODE_1,INSTRTN_CODE_2,INSTRTN_CODE_3,INSTRTN_CODE_4,INSTRTN_CODE_5,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,VENDOR_ITEM_NBR,MANUFACTURED_PLANT,BATCH_NBR,ASSIGNED_QTY,PREPACK_GROUP_CODE,PACK_CODE,SHIPPED_QTY,INITIAL_QTY,QTY_CONV_FACTOR,QTY_UOM_ID_BASE,WEIGHT_UOM_ID_BASE,VOLUME_UOM_ID_BASE,ITEM_NAME,TC_ORDER_LINE_ID,HAZMAT_UOM,HAZMAT_QTY,REF_FIELD_1,REF_FIELD_2,REF_FIELD_3,REF_FIELD_4,REF_FIELD_5,REF_FIELD_6,REF_FIELD_7,REF_FIELD_8,REF_FIELD_9,REF_FIELD_10,REF_NUM1,REF_NUM2,REF_NUM3,REF_NUM4,REF_NUM5)
		select
			TC_COMPANY_ID,LPN_ID,vlpndetailid,LPN_DETAIL_STATUS,INTERNAL_ORDER_DTL_ID,DISTRIBUTION_ORDER_DTL_ID,RECEIVED_QTY,BUSINESS_PARTNER_ID,ITEM_ID,p_gtin,STD_PACK_QTY,STD_SUB_PACK_QTY,STD_BUNDLE_QTY,INCUBATION_DATE,EXPIRATION_DATE,SHIP_BY_DATE,SELL_BY_DTTM,CONSUMPTION_PRIORITY_DTTM,p_prodDate,CNTRY_OF_ORGN,INVENTORY_TYPE,PRODUCT_STATUS,ITEM_ATTR_1,ITEM_ATTR_2,ITEM_ATTR_3,ITEM_ATTR_4,ITEM_ATTR_5,ASN_DTL_ID,PACK_WEIGHT,ESTIMATED_WEIGHT,ESTIMATED_VOLUME,p_sizeValue,WEIGHT,QTY_UOM_ID,WEIGHT_UOM_ID,VOLUME_UOM_ID,ASSORT_NBR,CUT_NBR,PURCHASE_ORDERS_ID,TC_PURCHASE_ORDERS_ID,PURCHASE_ORDERS_LINE_ID,TC_PURCHASE_ORDERS_LINE_ID,HIBERNATE_VERSION,INTERNAL_ORDER_ID,INSTRTN_CODE_1,INSTRTN_CODE_2,INSTRTN_CODE_3,INSTRTN_CODE_4,INSTRTN_CODE_5,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,VENDOR_ITEM_NBR,MANUFACTURED_PLANT,p_batchNbr,ASSIGNED_QTY,PREPACK_GROUP_CODE,PACK_CODE,SHIPPED_QTY,INITIAL_QTY,QTY_CONV_FACTOR,QTY_UOM_ID_BASE,WEIGHT_UOM_ID_BASE,VOLUME_UOM_ID_BASE,ITEM_NAME,TC_ORDER_LINE_ID,HAZMAT_UOM,HAZMAT_QTY,REF_FIELD_1,REF_FIELD_2,REF_FIELD_3,REF_FIELD_4,REF_FIELD_5,REF_FIELD_6,REF_FIELD_7,REF_FIELD_8,REF_FIELD_9,REF_FIELD_10,REF_NUM1,REF_NUM2,REF_NUM3,REF_NUM4,REF_NUM5
		from
		LPN_DETAIL
		where 
		lpn_detail_id = p_lpndetailid;
		
		Insert into WM_INVENTORY (TC_COMPANY_ID,LOCATION_ID,TC_LPN_ID,TRANSITIONAL_INVENTORY_TYPE,INVENTORY_TYPE,PRODUCT_STATUS,CNTRY_OF_ORGN,ITEM_ATTR_1,ITEM_ATTR_2,ITEM_ATTR_3,ITEM_ATTR_4,ITEM_ATTR_5,LOCN_CLASS,ALLOCATABLE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,INBOUND_OUTBOUND_INDICATOR,LPN_DETAIL_ID,C_FACILITY_ID,BATCH_NBR,LPN_ID,WM_INVENTORY_ID,ON_HAND_QTY,WM_ALLOCATED_QTY,TO_BE_FILLED_QTY,WM_VERSION_ID,CREATED_SOURCE_TYPE,TO_BE_CONSOLIDATED_QTY,LOCATION_DTL_ID,PACK_QTY,SUB_PACK_QTY,ITEM_ID,MARKED_FOR_DELETE,RECALL_ID,CUMULATIVE_QTY) 
		select 
		TC_COMPANY_ID,LOCATION_ID,TC_LPN_ID,TRANSITIONAL_INVENTORY_TYPE,INVENTORY_TYPE,PRODUCT_STATUS,CNTRY_OF_ORGN,ITEM_ATTR_1,ITEM_ATTR_2,ITEM_ATTR_3,ITEM_ATTR_4,ITEM_ATTR_5,LOCN_CLASS,ALLOCATABLE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,INBOUND_OUTBOUND_INDICATOR,vlpndetailid,C_FACILITY_ID,p_batchNbr,LPN_ID,vwminventoryid,p_sizeValue,WM_ALLOCATED_QTY,TO_BE_FILLED_QTY,WM_VERSION_ID,CREATED_SOURCE_TYPE,TO_BE_CONSOLIDATED_QTY,LOCATION_DTL_ID,PACK_QTY,SUB_PACK_QTY,ITEM_ID,MARKED_FOR_DELETE,RECALL_ID,CUMULATIVE_QTY
		from WM_INVENTORY
		where 
		lpn_detail_id = p_lpndetailid;
	
	else
		update lpn_detail set MANUFACTURED_DTTM = p_prodDate, BATCH_NBR = p_batchNbr, gtin = p_gtin
		where lpn_detail_id = p_lpndetailid;
	end if;
	
	commit;
	
	wm_cs_log(' Split LPN Detail '|| ', lpnDetailId : '||p_lpndetailid || ', sizeValue : ' || p_sizeValue || ', prodDate : ' || p_prodDate || 
				', 	batchNbr : ' || p_batchNbr || ', p_gtin : ' || p_gtin || ', p_splitUpdate : ' || p_splitUpdate || ', new vlpndetailid : ' || vlpndetailid);
				
	-- lpn_catch_weight will have 1 record per unit so update new lpn_detail_id for those many records (rownum < (p_sizeValue+1))
	if p_splitUpdate = 1 then
		update lpn_catch_weight set lpn_detail_id = vlpndetailid where lpn_detail_id = p_lpndetailid and rownum < (p_sizeValue+1);
	end if;
	
	commit;
	wm_cs_log('C_WM63_SPLIT_UPDATE Update end');
	
	EXCEPTION
		when others then 	
				wm_cs_log('C_WM63_SPLIT_UPDATE Exception while updating lpn_detail_id :  ' ||p_lpndetailid ||
				' Exception : ' || SQLERRM);					
	
end;

/

create or replace procedure C_WM63_SPLIT_OLPN_DETAIL
(
	p_olpn_id             in lpn_detail.lpn_id%type,
    p_olpn       		  in C_EAN_PICKING.olpn%type,
	p_item       		  in C_EAN_PICKING.item%type,
	p_result        	  OUT NUMBER
)
as
    v_whse                	facility.whse%type;
    oLPNCount         		number;
	deleteOriginalLpnDtl		number;
	vSize_Value              lpn_detail.size_value%type;
	
begin 

	p_result := 0;
  delete from tmp_log_parm;
  commit;
  insert into tmp_log_parm
    (
        log_lvl, pgm_id, module, msg_id, ref_code_1, ref_value_1, user_id, whse,
        cd_master_id
    )
    values
    (
        1, 'C_WM63_SPLIT_OLPN_DETAIL', 'CUST', '1094',
        '1', p_olpn, '1', '1', '1'
    );
	
	wm_cs_log('C_WM63_SPLIT_OLPN_DETAIL MOD =======> Starts for olpn : ' || p_olpn);	
	
	FOR loop_olpn IN

	  ( SELECT
		  item_id as vITEMID,
		  lpn_id as vLPNID,
		  lpn_detail_id as vLDETAILID,
		  TO_NUMBER(size_value) as vSIZEVALUE
		FROM LPN_DETAIL
		where lpn_id = p_olpn_id and lpn_detail_status = '90'
	  )

  LOOP
  
	wm_cs_log(' Processing for olpn : ' || loop_olpn.vLPNID || ' for Item : '|| loop_olpn.vITEMID);
	
	BEGIN
	SELECT nvl(count(counter),0) into oLPNCount
		FROM C_EAN_PICKING cep, item_cbo ic
		where cep.olpn = p_olpn		
		and ic.item_id = loop_olpn.vITEMID
		and ic.item_name = cep.item
		--and item = loop_olpn.vITEMNAME
		--and exists (select 1 from item_cbo ic where ic.item_id = loop_olpn.vITEMID and ic.item_name = cep.item)
		GROUP BY olpn;
	EXCEPTION
		when NO_DATA_FOUND then
				wm_cs_log('C_WM63_SPLIT_OLPN_DETAIL Exception NO_DATA_FOUND for  p_olpn : ' || p_olpn || ' and item : '|| loop_olpn.vITEMID);				
				oLPNCount:=0;
		END;
		
	if oLPNCount = 0 then
		wm_cs_log('C_WM63_SPLIT_OLPN_DETAIL MOD, No records found for item : '|| loop_olpn.vITEMID );
		continue;
	end if;
	
	SELECT sum(NVL(size_value,0)) into vSize_Value  
	FROM LPN_DETAIL ch
	where lpn_id = p_olpn_id --and lpn_detail_status = '90'
	and item_id = loop_olpn.vITEMID;
		
	if oLPNCount < vSize_Value then
		wm_cs_log('C_WM63_SPLIT_OLPN_DETAIL MOD, EAN records scanned less than sizeValue for item : '|| loop_olpn.vITEMID || ' vSize_Value: ' || vSize_Value);		
		exit;
	end if;
	
	DECLARE
  
		CURSOR cEANPICKING
		IS
			SELECT olpn as vOLPN,
		  ITEM as vITEM,
		  TO_DATE(prod_date, 'YYMMDD') as vPROD_DATE,
		  BATCH_NBR as vBATCH_NBR,
		  GTIN		as vGTIN,
		  count(counter) as vSIZE_VALUE
		FROM C_EAN_PICKING cep, item_cbo ic
		where cep.olpn = p_olpn 		
		and ic.item_id = loop_olpn.vITEMID
		and ic.item_name = cep.item
		--and item = loop_olpn.vITEMNAME
		--and exists (select 1 from item_cbo ic where ic.item_id = loop_olpn.vITEMID and ic.item_name = cep.item)
		GROUP BY olpn,
		  item,
		  prod_date,
		  BATCH_NBR,
		  gtin;		   
		
		l_cEANPICKING_rec cEANPICKING%ROWTYPE;
		
		BEGIN	 
			deleteOriginalLpnDtl :=0;
			FOR l_cEANPICKING_rec 
			IN cEANPICKING   
			LOOP 
				wm_cs_log(' Split LPN Detail with  oLPNId: ' || loop_olpn.vLPNID || ', oLPNCount : ' || oLPNCount);
				
				-- if CEANPicking has more than one record for a single olpn then split lpnDtl with prodDate/batchNbr/gtin
				if oLPNCount >1 then
					deleteOriginalLpnDtl :=1;
					C_WM63_SPLIT_UPDATE(loop_olpn.vLDETAILID,l_cEANPICKING_rec.vSIZE_VALUE,l_cEANPICKING_rec.vPROD_DATE,
						l_cEANPICKING_rec.vBATCH_NBR,l_cEANPICKING_rec.vGTIN,1);	
				else
					C_WM63_SPLIT_UPDATE(loop_olpn.vLDETAILID,l_cEANPICKING_rec.vSIZE_VALUE,l_cEANPICKING_rec.vPROD_DATE,
						l_cEANPICKING_rec.vBATCH_NBR,l_cEANPICKING_rec.vGTIN,0);	
				end if;
						
			END LOOP; -- END loop l_cEANPICKING_rec    
			
			if deleteOriginalLpnDtl = 1 then
				delete wm_inventory where lpn_detail_id = loop_olpn.vLDETAILID;
				delete lpn_detail where lpn_detail_id = loop_olpn.vLDETAILID;
				
			end if;
		
		EXCEPTION
		when others then 	
				wm_cs_log('C_WM63_SPLIT_OLPN_DETAIL Exception while loop l_cEANPICKING_rec ' || SQLERRM);	
				p_result := 1;
				RAISE_APPLICATION_ERROR (-20010, SQLERRM);
		END; -- End cEANPICKING
		
	END LOOP loop_olpn;
	  
   wm_cs_log('C_WM63_SPLIT_OLPN_DETAIL MOD =======> Ends for oLPN : ' || p_olpn);
   delete from tmp_log_parm;
   commit;
  --end if;
end;
/

create or replace procedure manh_wave_add_selection_joins
(
    p_is_wave_by_shpmt      in varchar2,
    p_chase_wave            in wave_parm.chase_wave%type,
    p_perf_pickng_wave      in ship_wave_parm.perf_pickng_wave%type,
    p_wave_type_indic       in wave_parm.wave_type_indic%type,
    p_preview_wave_flag     in number,
    p_perf_rte              in ship_wave_parm.perf_rte%type,
    p_tc_company_id         in wave_parm.tc_company_id%type,
    p_wave_by_rule          in varchar2,
    p_rule_type             in rule_hdr.rule_type%type,
    p_rule_id               in wave_rule_parm.rule_id%type,
    p_list_of_shpmts        in varchar2,
    p_list_of_orders        in varchar2,
    p_facility_id           in facility.facility_id%type,
    p_order_selection_sql   in out varchar2
)
as
    v_do_type               orders.do_type%type 
        := case when p_wave_type_indic = '1' then 20 else 10 end;
    v_select_rule           varchar2(4000);
    v_sort_rule             varchar2(4000);
    v_order_selection_sql   varchar2(32600) := lower(p_order_selection_sql);
begin
    p_order_selection_sql := p_order_selection_sql
        || ' from orders'
        || ' join order_line_item on order_line_item.order_id '
        || '    = orders.order_id'
        || ' where orders.o_facility_id = :facility_id'
        || '    and coalesce(order_line_item.fulfillment_type, ''1'') != ''2'''
        || '    and orders.has_import_error = 0';

    if (p_perf_pickng_wave = '1')
    then
        --distro_type 1 will have OnHand (LPN) type orders only; distro_type 3 does not need
        --allocation_source filter as we should return all OnHand/ASN/PO types for this.
        p_order_selection_sql := p_order_selection_sql
            -- added below condition for chase just to ensure  
            -- aggregate lines are selected
            || case when p_chase_wave in ('1','2') 
                    then ' and orders.parent_order_id is null ' 
                    else ' and orders.is_original_order = 1 ' end
            || ' and orders.do_type = ' || v_do_type;

        if (v_do_type = 20)
        then
            -- (a) if order is vas-complete, select all lines (b) if not,
            -- disregard vas-complete lines (i.e. include all vas-pending lines
            -- (or) all non-vas lines if there is no vas line in the order)
-- check: non-retail vas only?
            p_order_selection_sql := p_order_selection_sql
                || ' and (orders.wm_order_status = 20 '
                || '    or not(order_line_item.vas_process_type = ''1'' '
                || '        and order_line_item.pick_locn_id is not null)) ';
        end if;

        if (p_preview_wave_flag = 2)
        then 
            -- convert a preview wave
            p_order_selection_sql := p_order_selection_sql
                || ' and orders.do_status = 115 '
                || ' and order_line_item.do_dtl_status = 115';
        elsif p_chase_wave in ('1','2') 
        then
            -- for chase wave, select only the packed lines with cancelled_qty > 0
            -- and is not chased before.
            p_order_selection_sql := p_order_selection_sql
                || ' and coalesce(order_line_item.is_chase_created_line,0) != 1 '
                || ' and (order_line_item.do_dtl_status between 120 and 180 and order_line_item.user_canceled_qty > 0)';
        else
            -- a regular or pre wave
            p_order_selection_sql := p_order_selection_sql
                || ' and orders.do_status in (110 ' || case when p_preview_wave_flag = 1 then ')' else ', 120)' end
                || ' and order_line_item.do_dtl_status in (110, 120)';
        end if;
    else
        p_order_selection_sql := p_order_selection_sql
            || ' and orders.parent_order_id is null ';

        if (p_perf_rte = '1')
        then
            p_order_selection_sql := p_order_selection_sql
                || ' and orders.do_status in (110, 115, 120, 130, 140, 150, 160, 165)'
                || ' and order_line_item.do_dtl_status '
                || '    in (110, 115, 120, 130, 140, 150, 160) ';
        else
            -- pnh pulldown wave
            p_order_selection_sql := p_order_selection_sql
                || ' and orders.do_status between 120 and 165'
                || ' and order_line_item.do_dtl_status between 120 and 160';
        end if;
    end if;

-- check: pull with swc: can order having same swc belong to different companies (3pl)?
    if (p_tc_company_id is not null)
    then
        p_order_selection_sql := p_order_selection_sql 
            || ' and orders.tc_company_id = :tc_company_id ';
        
        p_order_selection_sql := replace(p_order_selection_sql, 'where ',
            'join wm_eligible_bu_vw bv on bv.business_unit_id = orders.tc_company_id '
            || ' and bv.user_name = :ucl_user_name and bv.facility_id = :facility_id '
            || ' where ');
    else
        p_order_selection_sql := replace(p_order_selection_sql, 'where',
            'join wm_eligible_bu_vw bv on bv.business_unit_id = orders.tc_company_id '
            || ' and bv.user_name = :ucl_user_name and bv.facility_id = :facility_id '
            || ' where ');	
    end if;

    if (p_wave_by_rule = 'Y')
    then
        select manh_get_rule(p_rule_id, 0, p_rule_type, p_facility_id)
        into v_select_rule
        from dual;

        -- select clause is mandatory
        if (v_select_rule is null)
        then
            raise_application_error(-20050, 'No Selection rule found.');
        end if;

        -- get the order by clause
        select substr(manh_get_rule(p_rule_id, 1, p_rule_type), 3)
        into v_sort_rule
        from dual;

        v_sort_rule := v_sort_rule || case when v_sort_rule is null then ' ' else ', ' end
            || case when p_is_wave_by_shpmt = 'Y' and p_chase_wave in ('1', '2')
                    then ' shipment.shipment_id, lpn.lpn_id, ' else ' ' end
            || case when instr(v_order_selection_sql, 'picking_short_item.', 1, 1) > 0
                then ' picking_short_item.picking_short_item_id, ' else ' ' end
            || case when instr(v_order_selection_sql, 'stop.', 1, 1) > 0
                then ' stop.stop_seq, ' else ' ' end
            || case when instr(v_order_selection_sql, 'order_note.', 1, 1) > 0
                then ' order_note.note_code, ' else ' ' end
            || ' order_line_item.line_item_id';

        p_order_selection_sql := replace(p_order_selection_sql, 'sort_criteria',
            v_sort_rule);
    elsif (p_list_of_orders is not null)
    then
        -- orders/loads are quoted lists of csv's, in parantheses
        v_select_rule := ' orders.tc_order_id in ' || p_list_of_orders ;
        p_order_selection_sql := replace(p_order_selection_sql,
            'sort_criteria', 'order_line_item.line_item_id');
    elsif (p_list_of_shpmts is not null)
    then
        v_select_rule := ' shipment.shipment_id in ' || p_list_of_shpmts;
        p_order_selection_sql := replace(p_order_selection_sql, 'sort_criteria',
            'shipment.pickup_start_dttm, order_line_item.line_item_id');
    else
        raise_application_error(-20050, 
            'Cannot wave by anything other than rules/orders/shpmts');
    end if;

    -- build sql
    p_order_selection_sql := p_order_selection_sql || ' and ' || v_select_rule;
    v_order_selection_sql := lower(p_order_selection_sql);

    if (p_chase_wave not in ('1', '2')
        and instr(v_order_selection_sql, 'lpn.', 1, 1) > 0)
    then
        p_order_selection_sql := replace(p_order_selection_sql, 'where ',
            'join lpn on lpn.tc_order_id = orders.tc_order_id '
            || ' and lpn.c_facility_id = orders.o_facility_id '
            || ' and lpn.tc_company_id = orders.tc_company_id where ');
    end if;

    if (instr(v_order_selection_sql, 'order_note.', 1, 1) > 0)
    then
        p_order_selection_sql := replace(p_order_selection_sql, 'where ',
            'join order_note on order_note.order_id = order_line_item.order_id '
            || ' and (order_note.line_item_id = order_line_item.line_item_id or order_note.line_item_id = 0 ) '
            || ' where ');
    end if;

    if (instr(v_order_selection_sql, 'item_cbo.', 1, 1) > 0)
    then
        p_order_selection_sql := replace(p_order_selection_sql, 'where ',
            'join item_cbo on item_cbo.item_id = order_line_item.item_id '
            || ' where ');
    end if;

    if (instr(v_order_selection_sql, 'item_wms.', 1, 1) > 0)
    then
        p_order_selection_sql := replace(p_order_selection_sql, 'where ',
            'join item_wms on item_wms.item_id = order_line_item.item_id '
            || ' where ');
    end if;

    if (instr(v_order_selection_sql, 'item_facility_mapping_wms.', 1, 1) > 0)
    then
        p_order_selection_sql := replace(p_order_selection_sql, 'where ',
            ' join item_facility_mapping_wms on '
            || ' item_facility_mapping_wms.item_id = order_line_item.item_id '
            || ' and item_facility_mapping_wms.facility_id '
            || ' = orders.o_facility_id where ');
    end if;

    if (instr(v_order_selection_sql, 'facility.', 1, 1) > 0)
    then
        p_order_selection_sql := replace(p_order_selection_sql, 'where ',
            'join facility on facility.facility_id = orders.d_facility_id '
            || ' where ');
    end if;

    if (instr(v_order_selection_sql, 'facility_alias.', 1, 1) > 0)
    then
        p_order_selection_sql := replace(p_order_selection_sql, 'where ',
            'join facility_alias on facility_alias.facility_id '
            || ' = orders.d_facility_id and facility_alias.facility_alias_id '
            || ' = orders.d_facility_alias_id where ');
    end if;

    if (instr(v_order_selection_sql, 'locn_hdr.', 1, 1) > 0)
    then
        p_order_selection_sql := replace(p_order_selection_sql, 'where ',
            'join locn_hdr on locn_hdr.locn_id = order_line_item.pick_locn_id '
            || ' where ');
    end if;

    if (instr(v_order_selection_sql, 'carrier_code.', 1, 1) > 0)
    then
        p_order_selection_sql := replace(p_order_selection_sql, 'where ',
            'join carrier_code on carrier_code.carrier_id = orders.dsg_carrier_id '
            || ' where ');
    end if;
    
    if (p_chase_wave in ('1','2'))
    then    
       if (instr(v_order_selection_sql, 'picking_short_item.', 1, 1) > 0)
       then
           p_order_selection_sql := replace(p_order_selection_sql, 'where ',
               'join picking_short_item on picking_short_item.line_item_id '
               || ' = order_line_item.line_item_id '
               || ' and picking_short_item.stat_code = 0 where ');
       end if;
    end if;

    if (p_is_wave_by_shpmt = 'Y')
    then
       if (p_chase_wave in ('1','2'))
       then
            p_order_selection_sql := replace(p_order_selection_sql, 'where ',
                ' left join lpn on lpn.order_id = orders.order_id '
                || ' and lpn.lpn_facility_status < 99 '
                ||' join shipment on shipment.shipment_id '
                || '    in (orders.shipment_id, lpn.shipment_id)'
                || '  and shipment.shipment_closed_indicator = 0 '
                || '  and shipment.is_cancelled = 0 where ');
        else
            p_order_selection_sql := replace(p_order_selection_sql, 'where ',
                ' left join order_movement on order_movement.order_id = orders.order_id '
                || '    and order_movement.tc_company_id = orders.tc_company_id '
                || ' join shipment on shipment.shipment_id = coalesce(orders.shipment_id, order_movement.shipment_id) '
                || '  and shipment.shipment_closed_indicator = 0 '
                || '  and shipment.is_cancelled = 0 where ');
        end if;

        if (instr(v_order_selection_sql, 'stop.', 1, 1) > 0)
        then
-- check: facility filter
            p_order_selection_sql := replace(p_order_selection_sql, 'where ',
                ' join stop on stop.shipment_id = shipment.shipment_id where ');
        end if;
    else
        if (instr(v_order_selection_sql, 'stop.', 1, 1) > 0)
        then

            p_order_selection_sql := replace(p_order_selection_sql, 'where ',
                ' join stop on stop.shipment_id = orders.shipment_id where ');
        end if;
    end if;
end;
/
show errors;

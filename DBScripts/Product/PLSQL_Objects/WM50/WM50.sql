MERGE INTO MESSAGE_MASTER L
USING (SELECT '110305001' KEY
FROM DUAL) B
ON (L.KEY = B.KEY)
WHEN NOT MATCHED THEN
  insert values(SEQ_MESSAGE_MASTER_ID.NEXTVAL,'5001','110305001','wm','CUST','(Field(s) that are NULL) is/are missing configuration','ErrorMessage','USER','WARNING',null,'Y',sysdate,null); 
COMMIT;
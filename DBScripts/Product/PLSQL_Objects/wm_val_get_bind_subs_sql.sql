create or replace function wm_val_get_bind_subs_sql
(
    p_val_job_dtl_id    in val_job_dtl.val_job_dtl_id%type,
    p_txn_id            in val_result_hist.txn_id%type,
    p_sql_text          in val_sql.sql_text%type
)
return clob
as
    v_sql_text          val_sql.sql_text%type := p_sql_text; 
begin
    for bind_rec in
    (
        select lower(vsb.bind_var) bind_var, coalesce(vrb.bind_val, vsb.bind_val) bind_val,
            coalesce(vrb.bind_val_int, vsb.bind_val_int) bind_val_int
        from val_sql_binds vsb
        left join val_runtime_binds vrb on vrb.bind_var = vsb.bind_var
            and vrb.val_job_dtl_id = vsb.val_job_dtl_id and vrb.txn_id = p_txn_id
        where vsb.val_job_dtl_id = p_val_job_dtl_id
    )
    loop
        if (bind_rec.bind_var like 'i_%')
        then
            v_sql_text := replace(v_sql_text, ':' || bind_rec.bind_var,
                to_char(bind_rec.bind_val_int));
            v_sql_text := replace(v_sql_text, ':' || upper(bind_rec.bind_var),
                to_char(bind_rec.bind_val_int));
        else
            v_sql_text := replace(v_sql_text, ':' || bind_rec.bind_var,
                '''' || bind_rec.bind_val || '''');
            v_sql_text := replace(v_sql_text, ':' || upper(bind_rec.bind_var),
                '''' || bind_rec.bind_val || '''');
        end if;
    end loop;

    return v_sql_text;
end;
/
show errors;

create or replace procedure manh_break_aids_for_tasking
(
    p_task_capcty           in task_rule_parm.task_capcty%type,
    p_max_wt                in task_rule_parm.max_wt%type,
    p_max_vol               in task_rule_parm.max_vol%type,
    p_rule_id               in rule_hdr.rule_id%type,
    p_cart_plan_id          in task_rule_parm.cart_plan_id%type,
    p_group_lpn_to_tasks    in varchar2,
    p_max_lpns_per_task     in number
)
as
    -- support a max of 5 for now
    v_num_total_breaks          number(1) := 5;
    v_num_configured_breaks     number(2) := 0;
    v_running_wt                number(13,4) := 0.0;
    v_running_vol               number(13,4) := 0.0;
    v_running_capcty            number(13,5) := 0.0;
    v_running_break_capcty1     number(3) := 0;
    v_running_break_capcty2     number(3) := 0;
    v_running_break_capcty3     number(3) := 0;
    v_running_break_capcty4     number(3) := 0;
    v_running_break_capcty5     number(3) := 0;   
    v_max_break_capcty1         number(3) := 999;
    v_max_break_capcty2         number(3) := 999;
    v_max_break_capcty3         number(3) := 999;
    v_max_break_capcty4         number(3) := 999;
    v_max_break_capcty5         number(3) := 999;
    v_prev_break_list1          varchar2(50);
    v_prev_break_list2          varchar2(50);
    v_prev_break_list3          varchar2(50);
    v_prev_break_list4          varchar2(50);
    v_prev_break_list5          varchar2(50);
    v_curr_task_id              task_hdr.task_id%type;
    v_last_updated_aid_id       number(9) := 0;
    v_prev_task_cmpl_ref_nbr    varchar(40);
    v_prev_task_cmpl_ref_code   varchar(3);
    v_log_lvl                   number(1) := wm_get_curr_log_lvl();
begin
    -- get break cap maximums from configured rules
    for rec in
    (
        select iv.break_capcty, iv.row_num
        from
        (
            select p_max_lpns_per_task break_capcty, 1 row_num
            from dual
            where p_group_lpn_to_tasks = 'Y'
            union all
            select rsd.break_capcty, row_number() over(order by rsd.sort_seq_nbr)
                + case when p_group_lpn_to_tasks = 'Y' then 1 else 0 end row_num
            from rule_sort_dtl rsd
            where rsd.rule_id = p_rule_id and rsd.break_list = 'Y'
                and rsd.break_capcty > 0
        ) iv
        where iv.row_num <= v_num_total_breaks
    )
    loop
        v_num_configured_breaks := v_num_configured_breaks + 1;
        case 
            when (rec.row_num = 1)
                then v_max_break_capcty1 := rec.break_capcty;
            when (rec.row_num = 2)
                then v_max_break_capcty2 := rec.break_capcty;
            when (rec.row_num = 3)
                then v_max_break_capcty3 := rec.break_capcty;
            when (rec.row_num = 4)
                then v_max_break_capcty4 := rec.break_capcty;
            else -- the fifth and last
                v_max_break_capcty5 := rec.break_capcty;
        end case;
    end loop;

    -- mark task info on all selected aid's
    v_curr_task_id := task_hdr_id_seq.nextval;
    for rec in
    (
        select t.id, t.capcty, t.wt, t.vol, t.break_list1, t.break_list2,
            t.break_list3, t.break_list4, t.break_list5, t.task_cmpl_ref_code,
            t.task_cmpl_ref_nbr, t.item_id
        from tmp_task_creation_selected_aid t
        order by t.id
    )
    loop
        -- if any of these capacities are exceeded, we need a new hdr
        -- assumption: no single aid exists that is > task capcty
        if ((v_running_capcty + rec.capcty > p_task_capcty)
            or (v_running_wt + rec.wt > p_max_wt)
            or (v_running_vol + rec.vol > p_max_vol)
            or (v_running_break_capcty1 = v_max_break_capcty1
                and rec.break_list1 is not null
                and (v_prev_break_list1 is null
                    or rec.break_list1 != v_prev_break_list1))
            or (v_running_break_capcty2 = v_max_break_capcty2
                and rec.break_list2 is not null
                and (v_prev_break_list2 is null
                    or rec.break_list2 != v_prev_break_list2))
            or (v_running_break_capcty3 = v_max_break_capcty3
                and rec.break_list3 is not null
                and (v_prev_break_list3 is null
                    or rec.break_list3 != v_prev_break_list3))
            or (v_running_break_capcty4 = v_max_break_capcty4
                and rec.break_list4 is not null
                and (v_prev_break_list4 is null
                    or rec.break_list4 != v_prev_break_list4))
            or (v_running_break_capcty5 = v_max_break_capcty5
                and rec.break_list5 is not null
                and (v_prev_break_list5 is null
                    or rec.break_list5 != v_prev_break_list5)))
        then
            -- update this batch of aid's with the curr task_id to
            -- unite them under one task_hdr
            update tmp_task_creation_selected_aid t
            set t.task_id = v_curr_task_id,
                t.th_task_cmpl_ref_code = v_prev_task_cmpl_ref_code,
                t.th_task_cmpl_ref_nbr = v_prev_task_cmpl_ref_nbr
            where t.id > v_last_updated_aid_id and t.id < rec.id;
            if (v_log_lvl = 2)
            then
                wm_cs_log('Grouped ' || sql%rowcount || ' AIDs between ' 
                    || to_char(v_last_updated_aid_id + 1) || ' and ' || to_char(rec.id - 1) 
                    || ' under task ' || v_curr_task_id);
            end if;

            v_last_updated_aid_id := rec.id - 1;

            -- prepare for the next task_hdr; reset caps
            v_curr_task_id := task_hdr_id_seq.nextval;
            v_running_capcty := 0;
            v_running_wt := 0;
            v_running_vol := 0;
            v_running_break_capcty1 := 0;
            v_running_break_capcty2 := 0;
            v_running_break_capcty3 := 0;
            v_running_break_capcty4 := 0;
            v_running_break_capcty5 := 0;        
            v_prev_break_list1 := null;
            v_prev_break_list2 := null;
            v_prev_break_list3 := null;
            v_prev_break_list4 := null;
            v_prev_break_list5 := null;
            v_prev_task_cmpl_ref_nbr := null;
            v_prev_task_cmpl_ref_code := null;
        end if;

        v_running_capcty := v_running_capcty + rec.capcty;
        v_running_wt := v_running_wt + rec.wt;
        v_running_vol := v_running_vol + rec.vol;
        
        if p_cart_plan_id is not null
        then
            v_prev_task_cmpl_ref_code := 15;
            
        elsif (v_prev_task_cmpl_ref_code is null)
        then
            v_prev_task_cmpl_ref_code := rec.task_cmpl_ref_code;
        elsif (v_prev_task_cmpl_ref_code != '*'
            and v_prev_task_cmpl_ref_code != rec.task_cmpl_ref_code)
        then
            v_prev_task_cmpl_ref_code := '*';
        end if;
        

        if p_cart_plan_id is null
        then
            if (v_prev_task_cmpl_ref_nbr is null)
            then
    	          v_prev_task_cmpl_ref_nbr := rec.task_cmpl_ref_nbr;
            elsif (v_prev_task_cmpl_ref_nbr != '*' 
                and v_prev_task_cmpl_ref_nbr != rec.task_cmpl_ref_nbr)
            then
                v_prev_task_cmpl_ref_nbr := '*';
            end if;
         end if;       

        -- break when new value is not null and (old is null or old != new)
        if (v_num_configured_breaks >= 1 and rec.break_list1 is not null
            and (v_prev_break_list1 is null 
                or rec.break_list1 != v_prev_break_list1))
        then
            v_running_break_capcty1 := v_running_break_capcty1 + 1;
            v_prev_break_list1 := rec.break_list1;
        end if;
        if (v_num_configured_breaks >= 2 and rec.break_list2 is not null
            and (v_prev_break_list2 is null 
                or rec.break_list2 != v_prev_break_list2))
        then
            v_running_break_capcty2 := v_running_break_capcty2 + 1;
            v_prev_break_list2 := rec.break_list2;
        end if;
        if (v_num_configured_breaks >= 3 and rec.break_list3 is not null
            and (v_prev_break_list3 is null 
                or rec.break_list3 != v_prev_break_list3))
        then
            v_running_break_capcty3 := v_running_break_capcty3 + 1;
            v_prev_break_list3 := rec.break_list3;
        end if;
        if (v_num_configured_breaks >= 4 and rec.break_list4 is not null
            and (v_prev_break_list4 is null 
                or rec.break_list4 != v_prev_break_list4))
        then
            v_running_break_capcty4 := v_running_break_capcty4 + 1;
            v_prev_break_list4 := rec.break_list4;
        end if;
        if (v_num_configured_breaks >= 5 and rec.break_list5 is not null
            and (v_prev_break_list5 is null 
                or rec.break_list5 != v_prev_break_list5))
        then
            v_running_break_capcty5 := v_running_break_capcty5 + 1;
            v_prev_break_list5 := rec.break_list5;
        end if;      
    end loop;

    -- update the last set of aid's
    update tmp_task_creation_selected_aid t
    set t.task_id = v_curr_task_id, t.th_task_cmpl_ref_code = v_prev_task_cmpl_ref_code,
        t.th_task_cmpl_ref_nbr = v_prev_task_cmpl_ref_nbr
    where t.id > v_last_updated_aid_id;
    if (v_log_lvl = 2)
    then
        wm_cs_log('Grouped ' || sql%rowcount || ' AIDs above ' || v_last_updated_aid_id || ' under task '
            || v_curr_task_id);
    end if;    
end;
/
show errors;

CREATE OR REPLACE PROCEDURE MANH_UNDO_INITIATE_OLI_PIX (
   p_whse            IN VARCHAR2,
   p_cd_master_id    IN NUMBER,
   p_login_user_id   IN VARCHAR2)
AS
   v_facility_id             facility.facility_id%TYPE;
   v_proc_stat_code_02       VARCHAR2 (2);
   v_proc_stat_code_03       VARCHAR2 (2);
   v_xml_group_attr_02       VARCHAR2 (10);
   v_xml_group_attr_03       VARCHAR2 (10);
   v_pix62002                VARCHAR2 (4000);
   tmp_v_pix62002            VARCHAR2 (4000);
   v_pix62003                VARCHAR2 (4000);
   tmp_v_pix62003            VARCHAR2 (4000);
   v_ins_ref_col_string_02   VARCHAR2 (1000);
   v_ins_ref_col_string_03   VARCHAR2 (1000);
   v_sel_ref_col_string_02   VARCHAR2 (1000);
   v_sel_ref_col_string_03   VARCHAR2 (1000);


   -- PIX needs to be written only for the original line, even in case of split line items.
   -- because LPN Disposition would write 620 XX 01, allocation PIX'es for the original line from which it gets allocated/split.
   -- generate only one 620 XX 02 PIX from one order line

   CURSOR SELECTED_LINE_ITEM
   IS
        SELECT OLI.ORDER_ID,
               OLI.LINE_ITEM_ID,
               SUM (tmp.allocated_qty) AS TO_BE_DECREMENTED_QTY
          FROM TMP_UNDO_INITIATE_ORDER_DTLS TMP, ORDER_LINE_ITEM OLI
         WHERE TMP.order_id = OLI.order_id
               AND ( (TMP.SUBSTITUTED_PARENT_LINE_ID IS NOT NULL
                      AND TMP.SUBSTITUTED_PARENT_LINE_ID = OLI.LINE_ITEM_ID)
                    OR (TMP.SUBSTITUTED_PARENT_LINE_ID IS NULL
                        AND TMP.LINE_ITEM_ID = OLI.LINE_ITEM_ID))
      GROUP BY OLI.ORDER_ID, OLI.LINE_ITEM_ID;
BEGIN
   SELECT facility_id
     INTO v_facility_id
     FROM facility
    WHERE whse = p_whse AND mark_for_deletion = 0 AND ROWNUM < 2;

   -- FULFILLMENT_TYPE - Receipt, 02
   manh_get_pix_config_data ('620',
                             '02',
                             '02',
                             p_cd_master_id,
                             v_proc_stat_code_02,
                             v_xml_group_attr_02);

   -- FULFILLMENT_TYPE - Receipt / Reserve, 03
   manh_get_pix_config_data ('620',
                             '03',
                             '02',
                             p_cd_master_id,
                             v_proc_stat_code_03,
                             v_xml_group_attr_03);

   v_pix62002 :=
         'insert into pix_tran '
      || '('
      || '   item_name, pix_tran_id, tran_type, tran_code, tran_nbr,'
      || '   pix_seq_nbr, proc_stat_code, whse, season, season_yr, style,'
      || '   style_sfx, color, color_sfx, sec_dim, qual, size_desc,'
      || '   size_range_code,size_rel_posn_in_table,invn_type, prod_stat,'
      || '   batch_nbr, sku_attr_1, sku_attr_2, sku_attr_3, sku_attr_4,'
      || '   sku_attr_5, cntry_of_orgn, invn_adjmt_qty, invn_adjmt_type,'
      || '   uom, actn_code, create_date_time, mod_date_time, user_id,'
      || '   wm_version_id, item_id, facility_id, tc_company_id,'
      || '   company_code, xml_group_id ';
   v_pix62003 := v_pix62002;

   v_sel_ref_col_string_02 := '';
   v_sel_ref_col_string_03 := '';

   manh_get_pix_ref_code_col_list ('620',
                                   '02',
                                   '02',
                                   p_cd_master_id,
                                   v_ins_ref_col_string_02,
                                   v_sel_ref_col_string_02);
   manh_get_pix_ref_code_col_list ('620',
                                   '03',
                                   '02',
                                   p_cd_master_id,
                                   v_ins_ref_col_string_03,
                                   v_sel_ref_col_string_03);

   v_pix62002 := v_pix62002 || v_ins_ref_col_string_02;
   v_pix62003 := v_pix62003 || v_ins_ref_col_string_03;

   IF (v_sel_ref_col_string_02 IS NULL AND p_cd_master_id IS NOT NULL)
   THEN
      manh_get_pix_ref_code_col_list ('620',
                                      '02',
                                      '02',
                                      NULL,
                                      v_ins_ref_col_string_02,
                                      v_sel_ref_col_string_02);
      v_pix62002 := v_pix62002 || v_ins_ref_col_string_02;
   END IF;

   IF (v_sel_ref_col_string_03 IS NULL AND p_cd_master_id IS NOT NULL)
   THEN
      manh_get_pix_ref_code_col_list ('620',
                                      '03',
                                      '02',
                                      NULL,
                                      v_ins_ref_col_string_03,
                                      v_sel_ref_col_string_03);
      v_pix62003 := v_pix62003 || v_ins_ref_col_string_03;
   END IF;

   FOR TMP_LINE_ITEM IN SELECTED_LINE_ITEM
   LOOP
      tmp_v_pix62002 :=
            v_pix62002
         || ' ) '
         || 'select item_cbo.item_name, pix_tran_id_seq.nextval,'
         || '    ''620'',''02'', pix_tran_id_seq.nextval,'
         || '    rownum, '
         || COALESCE (v_proc_stat_code_02, '10')
         || ', '''
         || p_Whse
         || ''''
         || '    , item_cbo.item_season, item_cbo.item_season_year,'
         || '    item_cbo.item_style, item_cbo.item_style_sfx,'
         || '    item_cbo.item_color, item_cbo.item_color_sfx,'
         || '    item_cbo.item_second_dim, item_cbo.item_quality,'
         || '    item_cbo.item_size_desc, item_wms.size_range_code,'
         || '    item_wms.size_rel_posn_in_table, order_line_item.invn_type,'
         || '    order_line_item.prod_stat,order_line_item.batch_nbr, '
         || '    order_line_item.item_attr_1, order_line_item.item_attr_2,'
         || '    order_line_item.item_attr_3, order_line_item.item_attr_4,'
         || '    order_line_item.item_attr_5, order_line_item.cntry_of_orgn,'
         || TMP_LINE_ITEM.TO_BE_DECREMENTED_QTY
         || ', ''S'', size_uom.size_uom,'
         || '    ''02'', current_timestamp, current_timestamp, '
         || '    '''
         || p_Login_User_Id
         || ''', 1, order_line_item.item_id,'
         || v_facility_id
         || ', order_line_item.tc_company_id, '
         || '    company.company_code, '
         || (CASE
                WHEN v_xml_group_attr_02 IS NULL THEN 'null'
                ELSE '''' || v_xml_group_attr_02 || ''' '
             END)
         || v_sel_ref_col_string_02
         || ' '
         || '
            from ORDER_LINE_ITEM  order_line_item
            join orders on 
                orders.order_id = '
         || TMP_LINE_ITEM.ORDER_ID
         || '
                and order_line_item.line_item_id = '
         || TMP_LINE_ITEM.LINE_ITEM_ID
         || '
                and orders.is_original_order = 1
                and orders.tc_company_id = '
         || p_cd_master_id
         || '
                and orders.o_facility_id = '
         || v_facility_id
         || '
            join item_cbo on item_cbo.item_id = order_line_item.item_id
            join item_wms on item_wms.item_id = order_line_item.item_id
            join size_uom on size_uom.size_uom_id = order_line_item.qty_uom_id_base
            join company on company.company_id = '
         || p_cd_master_id
         || '
                and order_line_item.fulfillment_type=''2''';


      tmp_v_pix62003 :=
            v_pix62003
         || ' ) '
         || 'select item_cbo.item_name, pix_tran_id_seq.nextval,'
         || '    ''620'',''03'', pix_tran_id_seq.nextval,'
         || '    rownum, '
         || COALESCE (v_proc_stat_code_03, '10')
         || ', '''
         || p_Whse
         || ''''
         || '    , item_cbo.item_season, item_cbo.item_season_year,'
         || '    item_cbo.item_style, item_cbo.item_style_sfx,'
         || '    item_cbo.item_color, item_cbo.item_color_sfx,'
         || '    item_cbo.item_second_dim, item_cbo.item_quality,'
         || '    item_cbo.item_size_desc, item_wms.size_range_code,'
         || '    item_wms.size_rel_posn_in_table, order_line_item.invn_type,'
         || '    order_line_item.prod_stat,order_line_item.batch_nbr, '
         || '    order_line_item.item_attr_1, order_line_item.item_attr_2,'
         || '    order_line_item.item_attr_3, order_line_item.item_attr_4,'
         || '    order_line_item.item_attr_5, order_line_item.cntry_of_orgn,'
         || TMP_LINE_ITEM.TO_BE_DECREMENTED_QTY
         || ', ''S'', size_uom.size_uom,'
         || '    ''02'', current_timestamp, current_timestamp, '
         || '    '''
         || p_Login_User_Id
         || ''', 1, order_line_item.item_id,'
         || v_facility_id
         || ', order_line_item.tc_company_id, '
         || '    company.company_code, '
         || (CASE
                WHEN v_xml_group_attr_03 IS NULL THEN 'null'
                ELSE '''' || v_xml_group_attr_03 || ''' '
             END)
         || v_sel_ref_col_string_03
         || ' '
         || '
            from ORDER_LINE_ITEM  order_line_item
            join orders on 
                orders.order_id = '
         || TMP_LINE_ITEM.ORDER_ID
         || '
                and order_line_item.line_item_id = '
         || TMP_LINE_ITEM.LINE_ITEM_ID
         || '
                and orders.is_original_order = 1
                and orders.tc_company_id = '
         || p_cd_master_id
         || '
                and orders.o_facility_id = '
         || v_facility_id
         || '
            join item_cbo on item_cbo.item_id = order_line_item.item_id
            join item_wms on item_wms.item_id = order_line_item.item_id
            join size_uom on size_uom.size_uom_id = order_line_item.qty_uom_id_base
            join company on company.company_id = '
         || p_cd_master_id
         || '
                and order_line_item.fulfillment_type=''3''';


      IF (v_proc_stat_code_02 != 91)
      THEN
         EXECUTE IMMEDIATE tmp_v_pix62002;
      END IF;

      IF (v_proc_stat_code_03 != 91)
      THEN
         EXECUTE IMMEDIATE tmp_v_pix62003;
      END IF;
   END LOOP;
END;
/

SHOW ERRORS;
create or replace procedure manh_merge_pkts
(
   p_return_status    out number,
   p_pick_wave_nbr    in varchar2,
   p_user_id	        in varchar2,
   p_facility_id      in number,
   p_debug_level      in number
)
as
    v_whse            facility.whse%type;
    v_tc_company_id   facility.tc_company_id%type;
    v_code_id         sys_code.code_id%type := 'MRG';
    v_max_log_lvl     number(1);
begin
-- todo: remove retval
    p_return_status := 0;

    select f.whse, f.tc_company_id
    into v_whse, v_tc_company_id
    from facility f
    where f.facility_id = p_facility_id;

    select to_number(coalesce(trim(substr(sc.misc_flags, 8, 1)), '0'))
    into v_max_log_lvl
    from sys_code sc 
    where sc.rec_type = 'S' and sc.code_type = '520' and sc.code_id = v_code_id;

    delete from tmp_log_parm;
    insert into tmp_log_parm
    (
        log_lvl, pgm_id, module, msg_id, ref_code_1, ref_value_1, user_id, whse,
        cd_master_id
    )
    values
    (
        v_max_log_lvl, 'manh_merge_pkts', 'WAVE', '1094', v_code_id,
        to_char(p_pick_wave_nbr), p_user_id, v_whse, v_tc_company_id
    );

    wm_cs_log('Beginning Merge of retail orders', p_sql_log_level => 0);
    manh_merge_create_bulk_pkts(v_whse, p_user_id, p_pick_wave_nbr);

    -- cache rtl pd's that need to be moved fully or partially to a bulk pkt
    insert into tmp_merge_rtl_pkt_dtl
    (
        reference_order_id, reference_line_item_id, pkt_qty,
        rtl_to_be_distroed_qty, pkt_hdr_id, pkt_dtl_id, cd_master_id,
        original_tc_order_id, reference_line_item_id_bulk
    )
    select pd.reference_order_id, pd.reference_line_item_id, pd.pkt_qty,
        pd.rtl_to_be_distroed_qty, ph.pkt_hdr_id, pd.pkt_dtl_id,
        ph.cd_master_id, ph.original_tc_order_id,
        case when pd.pkt_qty > pd.rtl_to_be_distroed_qty
            then seq_line_item_id.nextval else 0 end reference_line_item_id_bulk
        from pkt_hdr ph
        join pkt_dtl pd on pd.pkt_ctrl_nbr = ph.pkt_ctrl_nbr
    where ph.whse = v_whse and ph.stat_code < 40 and ph.rtl_pkt_flag = '1'
        and coalesce(ph.major_minor_pkt, 'X') != 'P'
        and pd.wave_nbr = p_pick_wave_nbr and pd.rtl_to_be_distroed_qty > 0;
    wm_cs_log('Collected retail PDs ', sql%rowcount);

    -- create agg oli for the bulk qty if both qty's exist and a split is needed
    insert into order_line_item
    (
        order_id, order_qty, line_item_id, do_dtl_status, orig_order_qty, adjusted_order_qty,
        wave_nbr, tc_order_line_id, allocated_qty, substituted_parent_line_id,
        parent_line_item_id, created_source, created_dttm, last_updated_source,
        last_updated_dttm, item_id, prod_stat, batch_nbr, cntry_of_orgn,
        item_attr_1, item_attr_2, item_attr_3, item_attr_4, item_attr_5,
        pick_locn_id, ship_wave_nbr, ppack_qty, is_hazmat,
        is_stackable, has_errors,
        created_source_type, actual_cost,
        actual_cost_currency_code, actual_shipped_dttm, allocation_source,
        allocation_source_id, allocation_source_line_id, alloc_line_id,
        alloc_type, assort_nbr, batch_requirement_type, budg_cost,
        budg_cost_currency_code, chute_assign_type,
        commodity_class, commodity_code_id, critcl_dim_1, critcl_dim_2,
        critcl_dim_3, cube_multiple_qty, customer_item,custom_tag,
        delivery_end_dttm, delivery_reference_number, delivery_start_dttm,
        event_code, exp_info_code,
        ext_sys_line_item_id, fulfillment_type, hibernate_version,
        internal_order_id, internal_order_seq_nbr, is_cancelled, is_emergency,
        item_name, last_updated_source_type, line_type,
        lpn_brk_attrib, lpn_size, lpn_type, manufacturing_dttm,
        master_order_id, merchandizing_department_id, merch_grp, merch_type,
        minor_order_nbr, mo_line_item_id, received_qty, shipped_qty,
        qty_uom_id_base, mv_currency_code, mv_size_uom_id,
        order_consol_attr, order_line_id,
        qty_uom_id, orig_budg_cost, package_type_id, pack_rate, pack_zone,
        pallet_type, partl_fill,
        pickup_end_dttm, pickup_reference_number, pickup_start_dttm,
        pick_locn_assign_type,
        planned_ship_date, ppack_grp_code, price,
        price_tkt_type, priority, product_class_id,
        protection_level_id,
        purchase_order_line_number, reason_code,
        ref_field1, ref_field2, ref_field3, repl_proc_type,
        repl_wave_nbr, repl_wave_run, retail_price,
        rtl_to_be_distroed_qty, rts_id, rts_line_item_id,
        serial_number_required_flag, shelf_days, single_unit_flag,
        sku_break_attr, sku_gtin, sku_sub_code_id, sku_sub_code_value,
        stack_diameter_standard_uom, stack_diameter_value,
        stack_height_standard_uom, stack_height_value,
        stack_length_standard_uom, stack_length_value, stack_rank,
        stack_width_standard_uom, stack_width_value, std_bundle_qty,
        std_lpn_qty, std_lpn_vol, std_lpn_wt, std_pack_qty, std_pallet_qty,
        std_sub_pack_qty, store_dept, tc_company_id,
        total_monetary_value, unit_cost, unit_monetary_value, unit_price_amount,
        unit_tax_amount, unit_vol, unit_wt, un_number_id, user_canceled_qty,
        vas_process_type, wave_proc_type,
        qty_conv_factor, received_weight,
        shipped_weight, weight_uom_id_base, weight_uom_id, received_volume,
        shipped_volume, volume_uom_id_base, volume_uom_id, size1_uom_id,
        received_size1, shipped_size1, size2_uom_id, received_size2,
        shipped_size2, planned_weight,
        planned_volume, size1_value, size2_value, units_pakd, invn_type,
        description, ext_purchase_order,is_chase_created_line
    )
    select t.reference_order_id order_id, t.rtl_to_be_distroed_qty order_qty,
        t.reference_line_item_id_bulk line_item_id, 130 do_dtl_status,
        t.rtl_to_be_distroed_qty orig_order_qty, t.rtl_to_be_distroed_qty adjusted_order_qty, 
        p_pick_wave_nbr wave_nbr,
        to_char(t.reference_line_item_id_bulk) tc_order_line_id,
        t.rtl_to_be_distroed_qty allocated_qty,
        t.reference_line_item_id substituted_parent_line_id,
        t.reference_line_item_id parent_line_item_id, p_user_id created_source,
        sysdate created_dttm, p_user_id last_updated_source,
        sysdate last_updated_dttm, oli.item_id, oli.prod_stat, oli.batch_nbr,
        oli.cntry_of_orgn, oli.item_attr_1, oli.item_attr_2, oli.item_attr_3,
        oli.item_attr_4, oli.item_attr_5, oli.pick_locn_id, oli.ship_wave_nbr,
        oli.ppack_qty, oli.is_hazmat,
        oli.is_stackable, oli.has_errors,
        oli.created_source_type, oli.actual_cost,
        oli.actual_cost_currency_code, oli.actual_shipped_dttm,
        oli.allocation_source, oli.allocation_source_id,
        oli.allocation_source_line_id, oli.alloc_line_id, oli.alloc_type,
        oli.assort_nbr, oli.batch_requirement_type, oli.budg_cost,
        oli.budg_cost_currency_code,
        oli.chute_assign_type, oli.commodity_class, oli.commodity_code_id,
        oli.critcl_dim_1, oli.critcl_dim_2, oli.critcl_dim_3,
        oli.cube_multiple_qty, oli.customer_item, oli.custom_tag,
        oli.delivery_end_dttm, oli.delivery_reference_number,
        oli.delivery_start_dttm, oli.event_code,
        oli.exp_info_code, oli.ext_sys_line_item_id, oli.fulfillment_type,
        oli.hibernate_version, oli.internal_order_id,
        oli.internal_order_seq_nbr, oli.is_cancelled, oli.is_emergency,
        oli.item_name, oli.last_updated_source_type,
        oli.line_type, oli.lpn_brk_attrib, oli.lpn_size,
        oli.lpn_type, oli.manufacturing_dttm, oli.master_order_id,
        oli.merchandizing_department_id, oli.merch_grp, oli.merch_type,
        oli.minor_order_nbr, oli.mo_line_item_id, oli.received_qty,
        oli.shipped_qty, oli.qty_uom_id_base,
        oli.mv_currency_code, oli.mv_size_uom_id,
        oli.order_consol_attr, oli.order_line_id,
        oli.qty_uom_id, oli.orig_budg_cost, oli.package_type_id, oli.pack_rate,
        oli.pack_zone, oli.pallet_type, oli.partl_fill,
        oli.pickup_end_dttm, oli.pickup_reference_number,
        oli.pickup_start_dttm, oli.pick_locn_assign_type,
        oli.planned_ship_date, oli.ppack_grp_code,
        oli.price, oli.price_tkt_type, oli.priority, oli.product_class_id,
        oli.protection_level_id,
        oli.purchase_order_line_number,
        oli.reason_code, oli.ref_field1,
        oli.ref_field2, oli.ref_field3,
        oli.repl_proc_type, oli.repl_wave_nbr, oli.repl_wave_run,
        oli.retail_price, oli.rtl_to_be_distroed_qty,
        oli.rts_id, oli.rts_line_item_id,
        oli.serial_number_required_flag, oli.shelf_days, oli.single_unit_flag,
        oli.sku_break_attr, oli.sku_gtin, oli.sku_sub_code_id,
        oli.sku_sub_code_value, oli.stack_diameter_standard_uom,
        oli.stack_diameter_value, oli.stack_height_standard_uom,
        oli.stack_height_value, oli.stack_length_standard_uom,
        oli.stack_length_value, oli.stack_rank, oli.stack_width_standard_uom,
        oli.stack_width_value, oli.std_bundle_qty, oli.std_lpn_qty,
        oli.std_lpn_vol, oli.std_lpn_wt, oli.std_pack_qty, oli.std_pallet_qty,
        oli.std_sub_pack_qty, oli.store_dept,
        oli.tc_company_id, oli.total_monetary_value, oli.unit_cost,
        oli.unit_monetary_value, oli.unit_price_amount, oli.unit_tax_amount,
        oli.unit_vol, oli.unit_wt, oli.un_number_id, oli.user_canceled_qty,
        oli.vas_process_type, oli.wave_proc_type,
        oli.qty_conv_factor, oli.received_weight, oli.shipped_weight,
        oli.weight_uom_id_base, oli.weight_uom_id, oli.received_volume,
        oli.shipped_volume, oli.volume_uom_id_base, oli.volume_uom_id,
        oli.size1_uom_id, oli.received_size1, oli.shipped_size1,
        oli.size2_uom_id, oli.received_size2, oli.shipped_size2,
        oli.planned_weight * t.rtl_to_be_distroed_qty/t.pkt_qty planned_weight,
        oli.planned_volume * t.rtl_to_be_distroed_qty/t.pkt_qty planned_volume,
        oli.size1_value * t.rtl_to_be_distroed_qty/t.pkt_qty size1_value,
        oli.size2_value * t.rtl_to_be_distroed_qty/t.pkt_qty size2_value,
        0 units_pakd, oli.invn_type, oli.description, oli.ext_purchase_order,
        oli.is_chase_created_line
    from tmp_merge_rtl_pkt_dtl t
    join order_line_item oli on oli.order_id = t.reference_order_id
        and oli.line_item_id = t.reference_line_item_id
    where t.pkt_qty > t.rtl_to_be_distroed_qty;
    wm_cs_log('Created agg OLI for bulk qty ', sql%rowcount);

    -- create bulk pd's under all PPs containing the bulk qtys; there should be
    -- exactly one PP meeting the ph join criteria
    insert into pkt_dtl
    (
        pkt_hdr_id, pkt_ctrl_nbr, pkt_seq_nbr, pkt_dtl_id, orig_pkt_qty,
        pkt_qty, user_id, create_date_time, mod_date_time, reference_order_id,
        reference_line_item_id, shelf_days, item_id, prod_stat, batch_nbr,
        cntry_of_orgn, sku_attr_1, sku_attr_2, sku_attr_3, sku_attr_4,
        sku_attr_5, orig_pkt_line_nbr, wave_nbr, rtl_to_be_distroed_qty,
        pick_locn_id, ship_wave_nbr, distro_w_ctrl_nbr, alloc_type,
        vas_proc_type, sku_break_attr, wave_proc_type, ppack_grp_code,
        ppack_qty, assort_nbr, carton_break_attr, qty_uom_id_base, qty_uom_id,
        qty_conv_factor, invn_type, actual_cost, orig_ord_line_nbr, po_nbr,
        std_bundl_qty, std_sub_pack_qty, std_pack_qty, std_case_qty, 
        std_plt_qty, cube_mult_qty, std_case_wt, std_case_vol, unit_wt, unit_vol, 
        cust_sku, plt_type, carton_type, carton_size, critcl_dim_1, critcl_dim_2, 
        critcl_dim_3, sngl_item_carton_flag, uom, pre_cube_flag, line_item_stat, 
        rsn_code, upc_pre_digit, upc_vendor_code, upc_srl_prod_nbr,
        upc_post_digit, price, retail_price, stat_code, custom_tag, frt_class, 
        pick_rate, pack_rate, ovrsz_len, pick_locn_assign_type, 
        i2o5_pack_qty_ref, i2o5_case_qty_ref, i2o5_tier_qty_ref, repl_proc_type, 
        line_type, temp_zone, pkt_consol_attr, distro_nbr, 
        distro_type, sku_sub_code_id, sku_sub_code_value, store_nbr, shpmt_nbr, 
        merch_type, merch_grp, store_dept, event_code, in_store_date, pack_zone,
        ticket_type, distro_break_attr, partl_fill,
        spl_instr_code_1, spl_instr_code_2, spl_instr_code_3, 
        spl_instr_code_4, spl_instr_code_5, ord_type,
        batch_reqmt_type, chute_assign_type, srl_nbr_reqd_flag, srl_nbr_reqd, 
        tms_po_seq, distro_plan_load_nbr, distro_tms_flag, ref_field_1, 
        ref_field_2, ref_field_3, po_size_uom, po_size_value, 
        po_ready_to_ship_flag, budget_cost, curr_code, distro_plan_shpmt_nbr, 
        distro_tms_stat_code, distro_tms_to_id, tms_to_line_nbr, tms_po_pkt, 
        distro_plan_bol, price_tkt_type, 
        pkg_type_instance, pkg_type, parent_line_item_id, un_nbr, commodity_code, 
        monetary_value, mv_currency_code, unit_monetary_value, mv_size_uom, 
        is_hazmat_flag, exp_info_code, minor_pkt_ctrl_nbr, 
        original_line_item_id, original_order_id, chute_id, 
        pkt_grp_code, is_consolidated, orig_item_id, tc_order_line_id, sngl_unit_flag
    )
    select ph.pkt_hdr_id pkt_hdr_id, to_char(ph.pkt_hdr_id) pkt_ctrl_nbr,
        pkt_dtl_id_seq.nextval pkt_seq_nbr, pkt_dtl_id_seq.nextval pkt_dtl_id,
        t1.rtl_to_be_distroed_qty orig_pkt_qty,
        t1.rtl_to_be_distroed_qty pkt_qty, p_user_id, sysdate create_date_time,
        sysdate mod_date_time, t1.reference_order_id,
        case when t1.pkt_qty > t1.rtl_to_be_distroed_qty
            then t1.reference_line_item_id_bulk else t1.reference_line_item_id
            end reference_line_item_id,
        pd.shelf_days, pd.item_id, pd.prod_stat, pd.batch_nbr, pd.cntry_of_orgn,
        pd.sku_attr_1, pd.sku_attr_2, pd.sku_attr_3, pd.sku_attr_4,
        pd.sku_attr_5, pd.orig_pkt_line_nbr, p_pick_wave_nbr wave_nbr,
        t1.rtl_to_be_distroed_qty, pd.pick_locn_id, pd.ship_wave_nbr,
        ph.original_tc_order_id distro_w_ctrl_nbr, pd.alloc_type,
        pd.vas_proc_type, pd.sku_break_attr, pd.wave_proc_type,
        pd.ppack_grp_code, pd.ppack_qty, pd.assort_nbr, pd.carton_break_attr,
        pd.qty_uom_id_base, pd.qty_uom_id, pd.qty_conv_factor, pd.invn_type,
        pd.actual_cost, pd.orig_ord_line_nbr, pd.po_nbr,
        pd.std_bundl_qty, pd.std_sub_pack_qty, pd.std_pack_qty, pd.std_case_qty,
        pd.std_plt_qty, pd.cube_mult_qty, pd.std_case_wt, pd.std_case_vol, 
        pd.unit_wt, pd.unit_vol, pd.cust_sku, pd.plt_type, pd.carton_type, 
        pd.carton_size, pd.critcl_dim_1, pd.critcl_dim_2, pd.critcl_dim_3, 
        pd.sngl_item_carton_flag, pd.uom, pd.pre_cube_flag, pd.line_item_stat,
        pd.rsn_code, pd.upc_pre_digit, pd.upc_vendor_code, pd.upc_srl_prod_nbr,
        pd.upc_post_digit, pd.price, pd.retail_price, pd.stat_code, 
        pd.custom_tag, pd.frt_class, pd.pick_rate, 
        pd.pack_rate, pd.ovrsz_len, pd.pick_locn_assign_type, 
        pd.i2o5_pack_qty_ref, pd.i2o5_case_qty_ref, pd.i2o5_tier_qty_ref, 
        pd.repl_proc_type, pd.line_type, pd.temp_zone, 
        pd.pkt_consol_attr, pd.distro_nbr, pd.distro_type, pd.sku_sub_code_id, 
        pd.sku_sub_code_value, pd.store_nbr, pd.shpmt_nbr, pd.merch_type, 
        pd.merch_grp, pd.store_dept, pd.event_code, pd.in_store_date, 
        pd.pack_zone, pd.ticket_type, pd.distro_break_attr,
        pd.partl_fill, pd.spl_instr_code_1, 
        pd.spl_instr_code_2, pd.spl_instr_code_3, pd.spl_instr_code_4, 
        pd.spl_instr_code_5, pd.ord_type, pd.batch_reqmt_type, 
        pd.chute_assign_type, pd.srl_nbr_reqd_flag, pd.srl_nbr_reqd,
        pd.tms_po_seq, pd.distro_plan_load_nbr, pd.distro_tms_flag, 
        pd.ref_field_1, pd.ref_field_2, pd.ref_field_3, pd.po_size_uom, 
        pd.po_size_value, pd.po_ready_to_ship_flag, pd.budget_cost, 
        pd.curr_code, pd.distro_plan_shpmt_nbr, pd.distro_tms_stat_code, 
        pd.distro_tms_to_id, pd.tms_to_line_nbr, pd.tms_po_pkt, 
        pd.distro_plan_bol, pd.price_tkt_type,
        pd.pkg_type_instance, pd.pkg_type, 
        pd.parent_line_item_id, pd.un_nbr, pd.commodity_code, 
        pd.monetary_value, pd.mv_currency_code, pd.unit_monetary_value, 
        pd.mv_size_uom, pd.is_hazmat_flag, 
        pd.exp_info_code, pd.minor_pkt_ctrl_nbr,
        pd.original_line_item_id, pd.original_order_id, pd.chute_id, 
        pd.pkt_grp_code, pd.is_consolidated, pd.orig_item_id,
        pd.tc_order_line_id, ph.sngl_unit_flag
    from tmp_merge_rtl_pkt_dtl t1
    join pkt_hdr ph on ph.cd_master_id = t1.cd_master_id
        and ph.original_tc_order_id = t1.original_tc_order_id
    join pkt_dtl pd on pd.pkt_dtl_id = t1.pkt_dtl_id
    where ph.whse = v_whse and ph.rtl_pkt_flag = '1'
        and ph.major_minor_pkt = 'P';
    wm_cs_log('Created bulk PDs ', sql%rowcount);

    -- remove the bulk qty (retain INT 2 qty) from agg oli that has been split
    update order_line_item oli
    set (oli.adjusted_order_qty, oli.order_qty, oli.pick_locn_id, oli.allocated_qty,
        oli.last_updated_source, oli.last_updated_dttm, oli.planned_weight,
        oli.planned_volume, oli.size1_value, oli.size2_value,
        oli.rtl_to_be_distroed_qty, oli.orig_order_qty) =
    (
        select (t.pkt_qty - t.rtl_to_be_distroed_qty) adjusted_order_qty,
            (t.pkt_qty - t.rtl_to_be_distroed_qty) order_qty, null pick_locn_id,
            (t.pkt_qty - t.rtl_to_be_distroed_qty) allocated_qty,
            p_user_id last_updated_source, sysdate last_updated_dttm,
            oli.planned_weight * (t.pkt_qty - t.rtl_to_be_distroed_qty)
            /t.pkt_qty planned_weight,
            oli.planned_volume * (t.pkt_qty - t.rtl_to_be_distroed_qty)
            /t.pkt_qty planned_volume,
            oli.size1_value * (t.pkt_qty - t.rtl_to_be_distroed_qty)
            /t.pkt_qty size1_value,
            oli.size2_value * (t.pkt_qty - t.rtl_to_be_distroed_qty)
            /t.pkt_qty size2_value, 0 rtl_to_be_distroed_qty,
            (t.pkt_qty - t.rtl_to_be_distroed_qty) orig_order_qty
        from tmp_merge_rtl_pkt_dtl t
        where t.reference_order_id = oli.order_id
            and t.reference_line_item_id = oli.line_item_id
    )
    where exists
    (
        select 1
        from tmp_merge_rtl_pkt_dtl t
        where t.reference_order_id = oli.order_id
            and t.reference_line_item_id = oli.line_item_id
            and t.pkt_qty > t.rtl_to_be_distroed_qty
    );
    wm_cs_log('Removed bulk qty from agg OLI ', sql%rowcount);

    -- remove RP pd's that were completely moved to the bulk pkt
    delete from pkt_dtl pd
    where exists
    (
        select 1
        from tmp_merge_rtl_pkt_dtl t
        where t.pkt_dtl_id = pd.pkt_dtl_id
            and t.pkt_qty = t.rtl_to_be_distroed_qty
    );
    wm_cs_log('Removed retail PDs that had only bulk qty ', sql%rowcount);

    -- remove ph only if all of the pkt was allocated in bulk
    delete from pkt_hdr ph
    where exists
    (
        select 1
        from tmp_merge_rtl_pkt_dtl t
        where t.pkt_hdr_id = ph.pkt_hdr_id
    )
    and not exists
    (
        select 1
        from pkt_dtl pd
        where pd.pkt_hdr_id = ph.pkt_hdr_id
    );
    wm_cs_log('Removed retail PH that only had bulk qty ', sql%rowcount);

    -- keep rtl pd's having INT 2 qty
    update pkt_dtl pd
    set (pd.pkt_qty, pd.orig_pkt_qty, pd.user_id, pd.mod_date_time,
        pd.pick_locn_id, pd.rtl_to_be_distroed_qty) =
    (
        select (t.pkt_qty - t.rtl_to_be_distroed_qty) pkt_qty,
            (t.pkt_qty - t.rtl_to_be_distroed_qty) orig_pkt_qty,
            p_user_id user_id, sysdate mod_date_time, null pick_locn_id,
            0 rtl_to_be_distroed_qty
        from tmp_merge_rtl_pkt_dtl t
        where t.pkt_dtl_id = pd.pkt_dtl_id
    )
    where exists
    (
        select 1
        from tmp_merge_rtl_pkt_dtl t
        where t.pkt_dtl_id = pd.pkt_dtl_id
            and t.pkt_qty > t.rtl_to_be_distroed_qty
    );
    wm_cs_log('Reduced bulk qty from retail PDs ', sql%rowcount);

    -- begin the original oli splitting process only if INT 2 qty exists
    insert into tmp_merge_selected_orders
    (
        order_id, line_item_id, reference_order_id, reference_line_item_id_int2,
        reference_line_item_id_bulk, allocated_qty, rng_allocated_qty, int2_qty,
        id
    )
    select oli.order_id, oli.line_item_id, oli.reference_order_id,
        t.reference_line_item_id reference_line_item_id_int2,
        t.reference_line_item_id_bulk, oli.allocated_qty,
        sum(oli.allocated_qty) over(partition by t.reference_order_id,
            t.reference_line_item_id order by oli.line_item_id)
            rng_allocated_qty,
        (t.pkt_qty - t.rtl_to_be_distroed_qty) int2_qty,
        row_number() over(partition by t.reference_order_id,
            t.reference_line_item_id order by oli.line_item_id) id
    from order_line_item oli
    join orders o on o.order_id = oli.order_id
    join tmp_merge_rtl_pkt_dtl t on t.reference_order_id =oli.reference_order_id
        and t.reference_line_item_id = oli.reference_line_item_id
    where o.o_facility_id = p_facility_id and oli.wave_nbr = p_pick_wave_nbr
        and oli.do_dtl_status in (120, 130)
        and t.pkt_qty > t.rtl_to_be_distroed_qty;
    wm_cs_log('Collected orig OLI for splitting ', sql%rowcount);

    -- mark very first line item in every group that needs to be split; a group
    -- is the (reference_order_id, reference_line_item_id_int2) 2-tuple
    insert into tmp_merge_split_orders
    (
        reference_order_id, reference_line_item_id_int2, id
    )
    select t.reference_order_id, t.reference_line_item_id_int2, min(t.id)
    from tmp_merge_selected_orders t
    where t.rng_allocated_qty > t.int2_qty
    group by t.reference_order_id, t.reference_line_item_id_int2;
    wm_cs_log('Calculated mid point for splitting groups of orig lines ', sql%rowcount);

    -- create new split original oli to point to the bulk agg oli
    insert into order_line_item
    (
        order_id, line_item_id, do_dtl_status, order_qty, orig_order_qty, adjusted_order_qty,
        tc_order_line_id, allocated_qty, substituted_parent_line_id, wave_nbr,
        parent_line_item_id, created_source, created_dttm, last_updated_source,
        last_updated_dttm, item_id, prod_stat, batch_nbr, cntry_of_orgn,
        item_attr_1, item_attr_2, item_attr_3, item_attr_4, item_attr_5,
        reference_order_id, reference_line_item_id, pick_locn_id, ship_wave_nbr,
        ppack_qty, is_hazmat,
        is_stackable, has_errors,
        created_source_type, actual_cost, actual_cost_currency_code,
        actual_shipped_dttm, allocation_source, allocation_source_id,
        allocation_source_line_id, alloc_line_id, alloc_type,
        assort_nbr, batch_requirement_type, budg_cost, budg_cost_currency_code,
        chute_assign_type, commodity_class,
        commodity_code_id, critcl_dim_1, critcl_dim_2, critcl_dim_3,
        cube_multiple_qty, customer_item,custom_tag, delivery_end_dttm,
        delivery_reference_number, delivery_start_dttm,
        event_code, exp_info_code, ext_sys_line_item_id, fulfillment_type,
        hibernate_version, internal_order_id, internal_order_seq_nbr,
        is_cancelled, is_emergency, item_name,
        last_updated_source_type, line_type, lpn_brk_attrib,
        lpn_size, lpn_type, manufacturing_dttm, master_order_id,
        merchandizing_department_id, merch_grp, merch_type, minor_order_nbr,
        mo_line_item_id, received_qty, shipped_qty, qty_uom_id_base,
        mv_currency_code, mv_size_uom_id,
        order_consol_attr, order_line_id, qty_uom_id,
        orig_budg_cost, package_type_id, pack_rate, pack_zone,
        pallet_type, partl_fill, pickup_end_dttm,
        pickup_reference_number, pickup_start_dttm, pick_locn_assign_type,
        planned_ship_date, ppack_grp_code, price, price_tkt_type, priority,
        product_class_id, protection_level_id,
        purchase_order_line_number,
        reason_code, ref_field1, ref_field2,  ref_field3,
        repl_proc_type, repl_wave_nbr, repl_wave_run,
        retail_price, rtl_to_be_distroed_qty, rts_id,
        rts_line_item_id, serial_number_required_flag,
        shelf_days, single_unit_flag, sku_break_attr, sku_gtin, sku_sub_code_id,
        sku_sub_code_value, stack_diameter_standard_uom, stack_diameter_value,
        stack_height_standard_uom, stack_height_value,
        stack_length_standard_uom, stack_length_value, stack_rank,
        stack_width_standard_uom, stack_width_value, std_bundle_qty,
        std_lpn_qty, std_lpn_vol, std_lpn_wt, std_pack_qty, std_pallet_qty,
        std_sub_pack_qty, store_dept, tc_company_id,
        total_monetary_value, unit_cost, unit_monetary_value, unit_price_amount,
        unit_tax_amount, unit_vol, unit_wt, un_number_id, user_canceled_qty,
        vas_process_type, wave_proc_type,
        qty_conv_factor, received_weight,
        shipped_weight, weight_uom_id_base, weight_uom_id, received_volume,
        shipped_volume, volume_uom_id_base, volume_uom_id, size1_uom_id,
        received_size1, shipped_size1, size2_uom_id, received_size2,
        shipped_size2, planned_weight,
        planned_volume, size1_value, size2_value, units_pakd, invn_type,
        description, ext_purchase_order,is_chase_created_line
    )
    select oli.order_id, seq_line_item_id.nextval line_item_id,
        130 do_dtl_status, (t1.rng_allocated_qty - t1.int2_qty) order_qty,
         oli.orig_order_qty orig_order_qty,
        (t1.rng_allocated_qty - t1.int2_qty) adjusted_order_qty,
        to_char(seq_line_item_id.nextval) tc_order_line_id,
        (t1.rng_allocated_qty - t1.int2_qty) allocated_qty,
        coalesce(oli.substituted_parent_line_id, oli.line_item_id) substituted_parent_line_id, p_pick_wave_nbr,
	coalesce(oli.parent_line_item_id, oli.line_item_id) parent_line_item_id, p_user_id,
        sysdate created_dttm, p_user_id last_updated_source,
        sysdate last_updated_dttm, oli.item_id, oli.prod_stat, oli.batch_nbr,
        oli.cntry_of_orgn, oli.item_attr_1, oli.item_attr_2, oli.item_attr_3,
        oli.item_attr_4, oli.item_attr_5, t1.reference_order_id,
        t1.reference_line_item_id_bulk reference_line_item_id, oli.pick_locn_id,
        oli.ship_wave_nbr, oli.ppack_qty, oli.is_hazmat,
        oli.is_stackable,
        oli.has_errors, oli.created_source_type,
        oli.actual_cost, oli.actual_cost_currency_code, oli.actual_shipped_dttm,
        oli.allocation_source, oli.allocation_source_id,
        oli.allocation_source_line_id, oli.alloc_line_id, oli.alloc_type,
        oli.assort_nbr, oli.batch_requirement_type, oli.budg_cost,
        oli.budg_cost_currency_code,
        oli.chute_assign_type, oli.commodity_class, oli.commodity_code_id,
        oli.critcl_dim_1, oli.critcl_dim_2, oli.critcl_dim_3,
        oli.cube_multiple_qty, oli.customer_item, oli.custom_tag,
        oli.delivery_end_dttm, oli.delivery_reference_number,
        oli.delivery_start_dttm, oli.event_code,
        oli.exp_info_code, oli.ext_sys_line_item_id, oli.fulfillment_type,
        oli.hibernate_version, oli.internal_order_id,
        oli.internal_order_seq_nbr, oli.is_cancelled, oli.is_emergency,
        oli.item_name, oli.last_updated_source_type,
        oli.line_type, oli.lpn_brk_attrib, oli.lpn_size,
        oli.lpn_type, oli.manufacturing_dttm, oli.master_order_id,
        oli.merchandizing_department_id, oli.merch_grp, oli.merch_type,
        oli.minor_order_nbr, oli.mo_line_item_id, oli.received_qty,
        oli.shipped_qty, oli.qty_uom_id_base,
        oli.mv_currency_code, oli.mv_size_uom_id,
        oli.order_consol_attr, oli.order_line_id,
        oli.qty_uom_id, oli.orig_budg_cost, oli.package_type_id, oli.pack_rate,
        oli.pack_zone, oli.pallet_type, oli.partl_fill,
        oli.pickup_end_dttm, oli.pickup_reference_number,
        oli.pickup_start_dttm, oli.pick_locn_assign_type,
        oli.planned_ship_date, oli.ppack_grp_code,
        oli.price, oli.price_tkt_type, oli.priority, oli.product_class_id,
        oli.protection_level_id,
        oli.purchase_order_line_number,
        oli.reason_code, oli.ref_field1,
        oli.ref_field2, oli.ref_field3,
        oli.repl_proc_type, oli.repl_wave_nbr, oli.repl_wave_run,
        oli.retail_price, oli.rtl_to_be_distroed_qty,
        oli.rts_id, oli.rts_line_item_id,
        oli.serial_number_required_flag, oli.shelf_days, oli.single_unit_flag,
        oli.sku_break_attr, oli.sku_gtin, oli.sku_sub_code_id,
        oli.sku_sub_code_value, oli.stack_diameter_standard_uom,
        oli.stack_diameter_value, oli.stack_height_standard_uom,
        oli.stack_height_value, oli.stack_length_standard_uom,
        oli.stack_length_value, oli.stack_rank, oli.stack_width_standard_uom,
        oli.stack_width_value, oli.std_bundle_qty, oli.std_lpn_qty,
        oli.std_lpn_vol, oli.std_lpn_wt, oli.std_pack_qty, oli.std_pallet_qty,
        oli.std_sub_pack_qty, oli.store_dept,
        oli.tc_company_id, oli.total_monetary_value, oli.unit_cost,
        oli.unit_monetary_value, oli.unit_price_amount, oli.unit_tax_amount,
        oli.unit_vol, oli.unit_wt, oli.un_number_id, oli.user_canceled_qty,
        oli.vas_process_type, oli.wave_proc_type,
        oli.qty_conv_factor, oli.received_weight, oli.shipped_weight,
        oli.weight_uom_id_base, oli.weight_uom_id, oli.received_volume,
        oli.shipped_volume, oli.volume_uom_id_base, oli.volume_uom_id,
        oli.size1_uom_id, oli.received_size1, oli.shipped_size1,
        oli.size2_uom_id, oli.received_size2, oli.shipped_size2,
        oli.planned_weight * (t1.rng_allocated_qty - t1.int2_qty)
            /oli.allocated_qty planned_weight,
        oli.planned_volume * (t1.rng_allocated_qty - t1.int2_qty)
            /oli.allocated_qty planned_volume,
        oli.size1_value * (t1.rng_allocated_qty - t1.int2_qty)
            /oli.allocated_qty size1_value,
        oli.size2_value * (t1.rng_allocated_qty - t1.int2_qty)
            /oli.allocated_qty size2_value,
        0 units_pakd, oli.invn_type, oli.description, oli.ext_purchase_order,
        oli.is_chase_created_line
    from order_line_item oli
    join tmp_merge_selected_orders t1 on t1.order_id = oli.order_id
        and t1.line_item_id = oli.line_item_id
    join tmp_merge_split_orders t2 on t2.id = t1.id
        and t2.reference_order_id = t1.reference_order_id
        and t2.reference_line_item_id_int2 = t1.reference_line_item_id_int2
    where t1.int2_qty - t1.rng_allocated_qty + t1.allocated_qty > 0;
    wm_cs_log('Split mid point of orig line groups; repointed to bulk agg OLI ', sql%rowcount);

    -- repoint orig oli above the id threshold from the INT 2 agg oli to the
    -- bulk agg oli; the oli corresponding to the id threshold may (or may not)
    -- need to be split since the oli qty's may not (or may) be completely self
    -- contained within the INT 2 agg oli qty
-- check: partial allocations?
    update order_line_item oli
    set (oli.last_updated_dttm, oli.last_updated_source, oli.order_qty,
        oli.allocated_qty, oli.adjusted_order_qty, oli.reference_line_item_id,
        oli.planned_weight, oli.planned_volume, oli.size1_value,
        oli.size2_value) =
    (
        select sysdate last_updated_dttm, p_user_id last_updated_source,
            case when t1.id = t2.id
	        and t1.int2_qty - t1.rng_allocated_qty + t1.allocated_qty > 0
	        then t1.int2_qty - t1.rng_allocated_qty + oli.order_qty
	        else oli.order_qty end order_qty,
            case when t1.id = t2.id
	        and t1.int2_qty - t1.rng_allocated_qty + t1.allocated_qty > 0
	        then t1.int2_qty - t1.rng_allocated_qty + t1.allocated_qty
	        else oli.allocated_qty end allocated_qty,
            case when t1.id = t2.id
	        and t1.int2_qty - t1.rng_allocated_qty + t1.allocated_qty > 0
	        then oli.adjusted_order_qty + t1.int2_qty - t1.rng_allocated_qty
	        else oli.adjusted_order_qty end adjusted_order_qty,
            case when t1.id > t2.id or (t1.id = t2.id
	        and t1.int2_qty - t1.rng_allocated_qty + t1.allocated_qty = 0)
	        then t1.reference_line_item_id_bulk
	        else oli.reference_line_item_id end reference_line_item_id,
            (case when t1.id = t2.id
	        and t1.int2_qty - t1.rng_allocated_qty + t1.allocated_qty > 0
	        then t1.int2_qty - t1.rng_allocated_qty + t1.allocated_qty
	        else oli.allocated_qty end) * oli.planned_weight
	        /oli.allocated_qty planned_weight,
            (case when t1.id = t2.id
	        and t1.int2_qty - t1.rng_allocated_qty + t1.allocated_qty > 0
	        then t1.int2_qty - t1.rng_allocated_qty + t1.allocated_qty
	        else oli.allocated_qty end) * oli.planned_volume
	        /oli.allocated_qty planned_volume,
            (case when t1.id = t2.id
	        and t1.int2_qty - t1.rng_allocated_qty + t1.allocated_qty > 0
	        then t1.int2_qty - t1.rng_allocated_qty + t1.allocated_qty
	        else oli.allocated_qty end) * oli.size1_value
	        /oli.allocated_qty size1_value,
            (case when t1.id = t2.id
	        and t1.int2_qty - t1.rng_allocated_qty + t1.allocated_qty > 0
	        then t1.int2_qty - t1.rng_allocated_qty + t1.allocated_qty
	        else oli.allocated_qty end) * oli.size2_value
	        /oli.allocated_qty size2_value
        from tmp_merge_selected_orders t1
        join tmp_merge_split_orders t2
            on t2.reference_order_id = t1.reference_order_id
            and t2.reference_line_item_id_int2
	        = t1.reference_line_item_id_int2
        where t1.order_id = oli.order_id and t1.line_item_id = oli.line_item_id
    )
    where oli.wave_nbr = p_pick_wave_nbr and oli.do_dtl_status in (120, 130)
        and exists
        (
            select 1
            from tmp_merge_selected_orders t1
            join tmp_merge_split_orders t2 on t1.id >= t2.id
	        and t2.reference_order_id = t1.reference_order_id
	        and t2.reference_line_item_id_int2
	            = t1.reference_line_item_id_int2
            where t1.order_id = oli.order_id
	        and t1.line_item_id = oli.line_item_id
        );
    wm_cs_log('Repointed orig OLI above mid point to bulk agg OLI ', sql%rowcount);

-- todo?: null out pick_locn_id from the INT 2 orig oli; per CV, this may not be
-- really necessary as nothing downstream is likely to be affected by it

    commit;
    wm_cs_log('Completed Merge of retail orders', p_sql_log_level => 0);
end;
/
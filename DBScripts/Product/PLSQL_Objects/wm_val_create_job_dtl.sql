create or replace procedure wm_val_create_job_dtl
(
    p_vjh_id    in val_job_hdr.val_job_hdr_id%type,
    p_ident     in val_sql.ident%type default null,
    p_sql_id    in val_job_dtl.sql_id%type default null,
    p_user_id   in user_profile.user_id%type default null,
    p_vjd_id    out val_job_dtl.val_job_dtl_id%type
)
as
    v_sql_id    val_job_dtl.sql_id%type := p_sql_id;
begin
    if ((p_ident is null and p_sql_id is null)
        or (p_ident is not null and p_sql_id is not null))
    then
        raise_application_error(-20050, 'Invalid SQL data.');
    end if;

    if (p_ident is not null)
    then
        begin
            select vs.sql_id
            into v_sql_id 
            from val_sql vs 
            where vs.ident = p_ident;
        exception
            when no_data_found then
                raise_application_error(-20050, 'Cant find sql with identifier ' || p_ident);
        end;
    end if;

    p_vjd_id := val_job_dtl_id_seq.nextval;
    insert into val_job_dtl
    (
        val_job_hdr_id, val_job_dtl_id, sql_id, create_date_time, mod_date_time,
        user_id
    )
    values
    (
        p_vjh_id, p_vjd_id, v_sql_id, sysdate, sysdate, p_user_id
    );
end;
/
show errors;

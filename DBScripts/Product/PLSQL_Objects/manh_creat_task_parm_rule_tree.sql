create or replace procedure manh_creat_task_parm_rule_tree
(
    p_need_type     in task_parm.need_type%type,
    p_user_id       in ucl_user.user_name%type,
    p_whse          in facility.whse%type,
    p_crit_desc     in task_parm.crit_desc%type,
    p_facility_id   in facility.facility_id%type,
    p_tc_company_id in wave_parm.tc_company_id%type,
    p_task_parm_id  in out task_parm.task_parm_id%type,
    p_need_id       in out task_hdr.need_id%type
)
as
    v_generated_task_parm_id task_parm.task_parm_id%type := 0;
    v_current_timestamp date := sysdate;
    v_need_id_int       number(9) := 0;
    v_whse              facility.whse%type;
-- todo: these need to go when a sequence is used
    v_rec_type_id       nxt_up_cnt.rec_type_id%type;
    v_pfx_field         nxt_up_cnt.pfx_field%type;
    v_pfx_len           nxt_up_cnt.pfx_len%type;
    v_pfx_type	        nxt_up_cnt.pfx_type%type;
    v_rev_order         nxt_up_cnt.rev_order%type;
    v_start_nbr         nxt_up_cnt.start_nbr%type;
    v_end_nbr           nxt_up_cnt.end_nbr%type;
    v_nbr_len           nxt_up_cnt.nbr_len%type;
    v_incr_value        nxt_up_cnt.incr_value%type;
    v_nxt_start_nbr     nxt_up_cnt.nxt_start_nbr%type;
    v_nxt_end_nbr       nxt_up_cnt.nxt_end_nbr%type;
    v_chk_digit_type    nxt_up_cnt.chk_digit_type%type;
    v_chk_digit_len     nxt_up_cnt.chk_digit_len%type;
    v_repeat_range      nxt_up_cnt.repeat_range%type;
    v_cd_master_id      nxt_up_cnt.cd_master_id%type;
    v_nxt_up_cnt_id     nxt_up_cnt.nxt_up_cnt_id%type;
    v_dummy_rc          int;
begin
    -- make copies of all rule data; get need_id for task creation
-- todo: this really needs to be a sequence
    if (p_need_id is null or length(p_need_id) = 0)
    then
        wm_auton_get_nxt_up_cnt_id(p_facility_id, to_char(p_tc_company_id), 'P11',
            120, p_user_id, 1, v_whse, v_rec_type_id, v_pfx_field, v_pfx_len,
            v_start_nbr, v_end_nbr, v_need_id_int, v_nbr_len, v_incr_value,
            v_nxt_start_nbr, v_nxt_end_nbr, v_chk_digit_type, v_chk_digit_len,
            v_repeat_range, v_cd_master_id, v_nxt_up_cnt_id, v_pfx_type,
            v_rev_order, v_dummy_rc);
        v_need_id_int := v_need_id_int + 1;
        p_need_id := 'N' || rtrim(to_char(v_need_id_int));
    end if;

    v_generated_task_parm_id := task_parm_id_seq.nextval;
    insert into task_parm
    (
        task_parm_id, whse, rec_type, crit_nbr, crit_desc, need_type,
        create_date_time, mod_date_time, user_id
    )
    values
    (
        v_generated_task_parm_id, p_whse, 'N',  p_need_id, p_crit_desc, p_need_type,
        v_current_timestamp, v_current_timestamp, p_user_id
    );

    -- cache rule info
    insert into tmp_task_creation_rules
    (
        orig_rule_id, new_rule_id
    )
	select rh.rule_id orig_rule_id, rule_hdr_id_seq.nextval new_rule_id
	from rule_hdr rh
	join task_rule_parm trp on trp.rule_id = rh.rule_id
	where trp.task_parm_id = p_task_parm_id;

    insert into rule_hdr
    (
        rule_id, rec_type, rule_grp, rule_type, rule_name, rule_desc, stat_code,
        create_date_time, mod_date_time, user_id, rule_hdr_id
    )
    select t.new_rule_id rule_id, 'A' rec_type, rh.rule_grp, rh.rule_type,
        rh.rule_name, rh.rule_desc, rh.stat_code, v_current_timestamp,
        v_current_timestamp, p_user_id, t.new_rule_id rule_hdr_id
    from rule_hdr rh
    join tmp_task_creation_rules t on t.orig_rule_id = rh.rule_id
    join task_rule_parm trp on trp.rule_id = rh.rule_id
    where trp.task_parm_id = p_task_parm_id;

    insert into task_rule_parm
    (
        task_parm_id, prty, task_capcty_uom, task_capcty, task_prty, task_type,
        create_partl_capcty_task, task_create_stat_code, task_desc, task_batch,
        prt_task_list, max_wt, max_vol, create_date_time, mod_date_time,
        user_id, create_sec_tasks, sec_task_int, labor_rate, pick_to_tote_flag,
        rpt_prtr_reqstr, cart_plan_id, rule_id, rule_hdr_id, task_rule_parm_id,
        group_lpn_to_tasks, max_lpns_per_task,perf_pick_path_opt, dest_locn_id
    )
    select v_generated_task_parm_id task_parm_id, trp.prty, trp.task_capcty_uom,
        coalesce(nullif(trp.task_capcty, 0), 99999) task_capcty, trp.task_prty,
        trp.task_type, trp.create_partl_capcty_task, trp.task_create_stat_code,
        trp.task_desc, trp.task_batch, trp.prt_task_list,
        coalesce(nullif(trp.max_wt, 0),999999999) max_wt,
        coalesce(nullif(trp.max_vol, 0), 999999999) max_vol,
        v_current_timestamp, v_current_timestamp, p_user_id,
        trp.create_sec_tasks, trp.sec_task_int, trp.labor_rate,
        trp.pick_to_tote_flag, trp.rpt_prtr_reqstr, trp.cart_plan_id,
        t.new_rule_id rule_id, t.new_rule_id rule_hdr_id,
        task_rule_parm_id_seq.nextval task_rule_parm_id, trp.group_lpn_to_tasks,
        trp.max_lpns_per_task,trp.perf_pick_path_opt, trp.dest_locn_id
    from task_rule_parm trp
    join task_parm tp on tp.task_parm_id = trp.task_parm_id
    join tmp_task_creation_rules t on t.orig_rule_id = trp.rule_id
    where tp.task_parm_id = p_task_parm_id;

    insert into rule_sel_dtl
    (
        open_paran, tbl_name, colm_name, oper_code, rule_cmpar_value, and_or_or,
        close_paran, create_date_time, mod_date_time, user_id, sel_seq_nbr,
        rule_id, rule_sel_dtl_id
    )
    select rsd.open_paran, rsd.tbl_name, rsd.colm_name, rsd.oper_code,
        rsd.rule_cmpar_value, rsd.and_or_or, rsd.close_paran,
        v_current_timestamp, v_current_timestamp, p_user_id, rsd.sel_seq_nbr,
        t.new_rule_id rule_id, rule_sel_dtl_id_seq.nextval rule_sel_dtl_id
    from rule_sel_dtl rsd
    join tmp_task_creation_rules t on t.orig_rule_id = rsd.rule_id
    join task_rule_parm trp on trp.rule_id = rsd.rule_id
    where trp.task_parm_id = p_task_parm_id;

    insert into rule_sort_dtl
    (
        sort_seq_nbr, tbl_name, colm_name, sort_seq, break_list,
        create_date_time, mod_date_time, user_id, break_capcty, rule_id,
        rule_sort_dtl_id
    )
    select rsd.sort_seq_nbr, rsd.tbl_name, rsd.colm_name, rsd.sort_seq,
        rsd.break_list, v_current_timestamp, v_current_timestamp, p_user_id,
        rsd.break_capcty, t.new_rule_id rule_id,
        rule_sort_dtl_id_seq.nextval rule_sort_dtl_id
    from rule_sort_dtl rsd
    join tmp_task_creation_rules t on t.orig_rule_id = rsd.rule_id
    join task_rule_parm trp on trp.rule_id = rsd.rule_id
    where trp.task_parm_id = p_task_parm_id;

    insert into task_prt_sort_dtl
    (
        tbl_name, colm_name, sort_seq, create_date_time, mod_date_time, user_id,
        sort_seq_break, sort_seq_nbr, rule_id, task_prt_sort_dtl_id
    )
    select tpsd.tbl_name, tpsd.colm_name, tpsd.sort_seq, v_current_timestamp,
        v_current_timestamp, p_user_id, tpsd.sort_seq_break, tpsd.sort_seq_nbr,
        t.new_rule_id rule_id, task_prt_sort_dtl_id_seq.nextval
    from task_prt_sort_dtl tpsd
    join tmp_task_creation_rules t on t.orig_rule_id = tpsd.rule_id
    join task_rule_parm trp on trp.rule_id = tpsd.rule_id
    where trp.task_parm_id = p_task_parm_id;

    -- use the copied rule structure from now on
    p_task_parm_id := v_generated_task_parm_id;
end;
/

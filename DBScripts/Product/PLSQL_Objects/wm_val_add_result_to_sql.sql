create or replace procedure wm_val_add_result_to_sql
(
    p_vjd_id          in val_job_dtl.val_job_dtl_id%type,
    p_row_num         in val_expected_results.row_num%type, 
    p_pos             in val_expected_results.pos%type,
    p_expected_value  in val_expected_results.expected_value%type
)
as
begin
    -- persist expected result data
    insert into val_expected_results
    (
        val_job_dtl_id, row_num, pos, expected_value
    )
    values
    (
        p_vjd_id, p_row_num, p_pos, p_expected_value
    );
end;
/
show errors;

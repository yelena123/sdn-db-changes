CREATE OR REPLACE PROCEDURE MANH_ADDPLTTOORDER_VALIDATE
(
    p_lpn_nbr in varchar2,
    p_order_id in number,
    p_whse in varchar2,
    p_facility_id in number,
    p_cd_master_id in number,
    p_lpn_type in number,
    p_scale in number,
    p_rc out integer
)
is
/*****************************************************************************
PROCEDURE NAME: MANH_ADDPLTTOorder_VALIDATE
OUTPUT PARAMETERS:
p_Rc          NUMBER  = --RETURN VALUE:
-- 0    -> OK (success)
-- 1    -> No case found for that Pallet ID (nothing to do)
-- 2    -> Unallocatable case lock exists
-- 3    -> Non matching vendor between order and at least one case
-- 4    -> Carton exists with the same number as a case
-- 5    -> No detail with qty > 0 for at least one case
-- 6    -> At least one case has phys_entity_code = 'L'
-- 7    -> Non matching CD_MASTER_ID between order and at least one case
-- 8    -> Invalid stat_code for at least one case
-- 9    -> OB Pallet already exists with same Tc_Lpn_Id as that of IB Pallet.
-- 99   -> DB and other error (exception raised anyway in this case).
-- 95   -> Invalid Lpn Type
-- -1   -> Record does not exist
****************************************************************************/
    v_rnd_threshold             number(5, 4) := case p_scale when 0 then 0
        else power(10, -p_scale)/2 end;
    v_ib_flag                   number (2);
		
begin
p_rc:=1;

select case  
           when exists (
                            select 1 
                            from LPN 
                            where tc_lpn_id = p_lpn_nbr
                                and lpn_Type = 2
                                and Inbound_Outbound_Indicator = 'O' 
                                and tc_company_id = p_cd_master_id
                        )  
            then 1 else 0 end  into v_ib_flag from dual;
            
case p_lpn_type
   when 1 
   then
       select coalesce(max(rc),-1) into p_rc from 
       (
            select case
                                       
                      when
                           LPN.LPN_FACILITY_STATUS not between 10 and 30
                      then
                           8

                      when
                           LPN.TC_COMPANY_ID <> ORDERS.TC_COMPANY_ID
                      then
                           7

                      when
                           LPN.PHYSICAL_ENTITY_CODE = 'L' -- Location
                      then
                           6

                      when
                           not exists
                	    (select 1
                	     from
                	           WM_INVENTORY
                	     where
                	           LPN.TC_LPN_ID = WM_INVENTORY.TC_LPN_ID AND
                	           LPN.TC_COMPANY_ID = WM_INVENTORY.TC_COMPANY_ID AND
				   LPN.C_FACILITY_ID = WM_INVENTORY.C_FACILITY_ID AND
				   WM_INVENTORY.ON_HAND_QTY > v_rnd_threshold
                            )
                      then
                           5

                      when
                           exists
                	   (select 1
                	     from
                	 	   LPN B
                             where
                                   LPN.TC_LPN_ID = B.TC_LPN_ID
                                   and B.LPN_FACILITY_STATUS < 90 AND
                                   B.INBOUND_OUTBOUND_INDICATOR = 'O' AND
                                   B.LPN_TYPE = 1 AND
                                   B.C_FACILITY_ID = LPN.C_FACILITY_ID
                           )
                      then
                           4

                      when
                           DO_TYPE = 40
                	   and exists
                	   (select 1
                	    from
                	         BUSINESS_PARTNER
                            where
                              	 LPN.BUSINESS_PARTNER_ID = BUSINESS_PARTNER.BUSINESS_PARTNER_ID
                            and  BUSINESS_PARTNER.BUSINESS_PARTNER_ID <> ORDERS.BUSINESS_PARTNER_ID
                    	   )
                      then 3

                      when 
                      exists(
                              select 1 
                              from INVN_NEED_TYPE 
                              where INVN_NEED_TYPE.INVN_NEED_TYPE = 85
                              and INVN_NEED_TYPE.WHSE = p_whse
                              and INVN_NEED_TYPE.CHK_LOCKS_ON_ALLOCN='N')
                      or
                          exists 
                          ( 
                              select 1 
                              from INVN_NEED_TYPE 
                              where INVN_NEED_TYPE.INVN_NEED_TYPE = 85 
                              and INVN_NEED_TYPE.WHSE = p_whse 
                              and INVN_NEED_TYPE.CHK_LOCKS_ON_ALLOCN = 'Y' 
                              and not exists 
                                  (select 1 from LPN_LOCK 
                                   where LPN_LOCK.TC_LPN_ID = LPN.TC_LPN_ID 
                                   and not exists
                                       (
                                           select 1
                                           from INT_ALLOC_LOCK_CODES
                                           where INT_ALLOC_LOCK_CODES.INVN_NEED_TYPE= INVN_NEED_TYPE.INVN_NEED_TYPE
                                           and INT_ALLOC_LOCK_CODES.INVN_NEED_TYPE = 85
                                           and INT_ALLOC_LOCK_CODES.WHSE = p_whse
                                           and INT_ALLOC_LOCK_CODES.CD_MASTER_ID = ORDERS.TC_COMPANY_ID
                                           and INT_ALLOC_LOCK_CODES.LOCK_CODE = LPN_LOCK.INVENTORY_LOCK_CODE
                                       )
                                  )
                          )
                      then
                         0
                      else
                        2
                  end as rc

                  from
                        LPN, ORDERS
                  where
        		LPN.TC_LPN_ID = p_lpn_nbr AND
        		LPN.C_FACILITY_ID = p_facility_id AND
        		LPN.INBOUND_OUTBOUND_INDICATOR = 'I' AND
        		LPN.LPN_TYPE = 1 AND
        		ORDERS.O_FACILITY_ID = LPN.C_FACILITY_ID AND
        		ORDERS.IS_CANCELLED = 0 AND
        		ORDERS.ORDER_ID = p_order_id
        	);

   when 2 then
               if (v_ib_flag = 1) 
               then 
               p_rc:=9;
               else 
               select coalesce(max(rc),-1) into p_rc from (select
                  case
                       when
                            LPN.LPN_FACILITY_STATUS not between 10 and 30
                       then
                       	    8

                       when
                            LPN.TC_COMPANY_ID <> ORDERS.TC_COMPANY_ID
                       then
                            7

                       when
                            LPN.LPN_TYPE = 4 -- Location
                       then
                            6

                       when
                            not exists
                            (select 1
                             from
                                   WM_INVENTORY
                             where
                                   LPN.TC_LPN_ID = WM_INVENTORY.TC_LPN_ID AND
   				   LPN.TC_COMPANY_ID = WM_INVENTORY.TC_COMPANY_ID AND
   				   LPN.C_FACILITY_ID = WM_INVENTORY.C_FACILITY_ID AND
   				   WM_INVENTORY.ON_HAND_QTY > v_rnd_threshold
                            )
                       then
                            5

                       when
                            exists
                            (select 1
                             from
                                   LPN B
                             where
                                   LPN.TC_LPN_ID = B.TC_LPN_ID
                                   and B.LPN_FACILITY_STATUS < 90 AND
                                   B.INBOUND_OUTBOUND_INDICATOR = 'O' AND
                                   B.LPN_TYPE = 1 AND
                                   B.C_FACILITY_ID = LPN.C_FACILITY_ID
                            )
                       then
                            4

                       when
		 	    DO_TYPE = 40
                            and exists
                           (select 1
                	    from
                	         BUSINESS_PARTNER
                            where
                              	 LPN.BUSINESS_PARTNER_ID = BUSINESS_PARTNER.BUSINESS_PARTNER_ID
                            and  BUSINESS_PARTNER.BUSINESS_PARTNER_ID <> ORDERS.BUSINESS_PARTNER_ID
                       	   )
                      then
                           3

                      when 
                      exists(
                              select 1 
                              from INVN_NEED_TYPE 
                              where INVN_NEED_TYPE.INVN_NEED_TYPE = 85
                              and INVN_NEED_TYPE.WHSE = p_whse
                              and INVN_NEED_TYPE.CHK_LOCKS_ON_ALLOCN='N')
                      or
                          exists 
                          ( 
                              select 1 
                              from INVN_NEED_TYPE 
                              where INVN_NEED_TYPE.INVN_NEED_TYPE = 85 
                              and INVN_NEED_TYPE.WHSE = p_whse 
                              and INVN_NEED_TYPE.CHK_LOCKS_ON_ALLOCN = 'Y' 
                              and not exists 
                                  (select 1 from LPN_LOCK 
                                   where LPN_LOCK.TC_LPN_ID = LPN.TC_LPN_ID 
                                   and not exists
                                       (
                                           select 1
                                           from INT_ALLOC_LOCK_CODES
                                           where INT_ALLOC_LOCK_CODES.INVN_NEED_TYPE= INVN_NEED_TYPE.INVN_NEED_TYPE
                                           and INT_ALLOC_LOCK_CODES.INVN_NEED_TYPE = 85
                                           and INT_ALLOC_LOCK_CODES.WHSE = p_whse
                                           and INT_ALLOC_LOCK_CODES.CD_MASTER_ID = ORDERS.TC_COMPANY_ID
                                           and INT_ALLOC_LOCK_CODES.LOCK_CODE = LPN_LOCK.INVENTORY_LOCK_CODE
                                       )
                                  )
                          )
                      then
                         0
                      else
                        2
                  end as rc

                  from
                  	LPN, ORDERS
             	  where
                        LPN.C_FACILITY_ID = p_facility_id AND
                        LPN.INBOUND_OUTBOUND_INDICATOR = 'I' AND
                        LPN.LPN_TYPE = 1 AND
                        ORDERS.O_FACILITY_ID = LPN.C_FACILITY_ID AND
                        ORDERS.IS_CANCELLED = 0 AND
                        ORDERS.ORDER_ID = p_order_id AND
                        EXISTS ( SELECT 1 FROM LPN D WHERE D.LPN_ID = LPN.PARENT_LPN_ID AND
                        D.TC_LPN_ID = p_lpn_nbr AND D.C_FACILITY_ID = LPN.C_FACILITY_ID) ) T;
   end if;                     
   else
      p_rc  := 95;
end case;
end;
/

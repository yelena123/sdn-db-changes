create or replace procedure wm_validate_orders_for_invc
(
    p_facility_id			     facility.facility_id%type
)
as
    v_prtl_ship_conf_flag  number(1) := 0;
begin
    select case when exists
    (
        select 1
        from whse_parameters wp
        where wp.whse_master_id = p_facility_id and wp.gen_ship_conf = '2'
    ) then 0 else 1 end
    into v_prtl_ship_conf_flag
    from dual;

    delete from tmp_invc_order_list t
    where exists
    (
        select 1
        from orders o
        where o.order_id = t.order_id and o.has_import_error = 1 and o.do_status < 190
    );
    wm_cs_log('Orders with import errors ' || sql%rowcount); 

    -- this is in effect for retail as well - originals are not invoiced
    -- until the agg order is ready; consequently, the flag has to be 0 if
    -- we need to invoice minors sooner, in retail
    -- remove data for orders that arent going to be completely invoiced:
    -- this is so if a) < packed or b) has an lpn that isnt in the lpn list;
    -- orders in 190/200 status need to have shorts reported - they will
    -- contribute no invoicable lpns for this run
    delete from tmp_invc_order_list t
    where (v_prtl_ship_conf_flag = 0 or t.prtl_ship_conf_flag < 1)
        and
        (
            t.do_status < 150
            or exists
            (
                select 1
                from lpn l
                where l.order_id = t.order_id and l.lpn_facility_status < 90
                    and not exists
                    (
                        select 1
                        from tmp_invc_lpn_list t2
                        where t2.lpn_id = l.lpn_id
                    )
            )
        );
    wm_cs_log('Orders that could not be partially invoiced ' || sql%rowcount);

    -- we (mostly in the DO flow) are allowed to invoice 190/200 status
    -- orders; otherwise, DO's need to have at least one validated lpn remaining
    -- here to get invoiced
    delete from tmp_invc_order_list t
    where t.do_status < 190
		and not exists
        (
            select 1
            from tmp_invc_lpn_list l
            where l.order_id = t.order_id
        );
    wm_cs_log('Orders with no lpns to invoice ' || sql%rowcount);

    -- remove lpns on ineligible orders
    delete from tmp_invc_lpn_list l
    where not exists
    (
        select 1
        from tmp_invc_order_list o
        where o.order_id = l.order_id
    );
    wm_cs_log('Lpns dropped due to orders being ineligible ' || sql%rowcount);
end;
/
show errors;

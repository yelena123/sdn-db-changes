create or replace function wm_val_get_parent_txn_id
return val_result_hist.parent_txn_id%type
as
    v_parent_txn_id val_result_hist.parent_txn_id%type;
begin
    select t.parent_txn_id
    into v_parent_txn_id
    from tmp_val_parm t;
    
    return v_parent_txn_id;
end;
/
show errors;

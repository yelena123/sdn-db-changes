CREATE OR REPLACE PACKAGE Purge_pkg
AS
    PROCEDURE log_error (asqlcode   IN archive_error_log.sql_code%TYPE,
                         asqlerrm   IN archive_error_log.sql_errm%TYPE);

    PROCEDURE log_archive (
        apurge_group     IN archive_run_log.purge_group%TYPE,
        apurge_days      IN archive_run_log.purge_days%TYPE,
        aprocedurename   IN archive_run_log.procedure_name%TYPE,
        arecordcount     IN archive_run_log.record_count%TYPE,
        aruntime         IN archive_run_log.run_time%TYPE,
        pbucketid        IN archive_run_log.bucket_id%TYPE);

    PROCEDURE insert_del_Data_bulk (p_prod_table_name   IN VARCHAR2,
                                    p_arch_table_name   IN VARCHAR2,
                                    p_temp_table           VARCHAR2,
                                    p_column_name       IN VARCHAR2,
                                    p_query                VARCHAR2,
                                    p_archive_flag         VARCHAR2);

    PROCEDURE update_purge_status (p_status IN VARCHAR2);

    TYPE rt_msglog IS RECORD
    (
        r_module          msg_log.module%TYPE:= 'SYSCONTROL',
        r_msg_id          msg_log.msg_id%TYPE:= '9999',
        r_lang_id         message_display.to_lang%TYPE:= 'en',
        r_whse_id         whse_master.whse_master_id%TYPE:= NULL,
        r_cd_master_id    msg_log.cd_master_id%TYPE:= NULL,
        r_start_dttm      TIMESTAMP (6),
        r_end_dttm        TIMESTAMP (6),
        r_rows_archived   PLS_INTEGER:= 0,
        r_rows_deleted    PLS_INTEGER:= 0,
        r_table_name      msg_log.msg%TYPE,
        r_company_id      msg_log.cd_master_id%TYPE,
        r_pgm_id          VARCHAR2 (50),
        r_purge_code      NUMBER (10),
        r_machine_id      VARCHAR2 (128),
        r_terminal        VARCHAR2 (128),
        r_func_name       VARCHAR2 (128):= NULL,
        r_sub_pgm_name    VARCHAR2 (128):= NULL
    );

    rec_msglog   rt_msglog;

    PROCEDURE log_msg (p_rec_MsgLog IN rt_MsgLog);

    FUNCTION get_config_days (p_purge_group IN VARCHAR2)
        RETURN PLS_INTEGER;

    PROCEDURE execute_purge (p_purge_group IN VARCHAR2);

    PROCEDURE event_purge;

    PROCEDURE inv_bussiness_rules_purge_proc;

    PROCEDURE commerce_purge_proc;

    PROCEDURE inv_purge_proc;

    PROCEDURE purge_staging_event_process;

    PROCEDURE inv_log_purge_proc;

    PROCEDURE co_base_data_purge;

    PROCEDURE exp_table_dump (p_directory      IN VARCHAR2,
                              p_table_name     IN VARCHAR2,
                              p_table_filter   IN VARCHAR2,
                              p_time           IN VARCHAR2);

    PROCEDURE imp_table_dump (p_directory    IN VARCHAR2,
                              p_table_name   IN VARCHAR2,
                              p_time         IN VARCHAR2);

    PROCEDURE purge_history_data (p_datapump_directory   IN VARCHAR2,
                                  p_table_name           IN VARCHAR2,
                                  p_retention_days       IN NUMBER);

    PROCEDURE log_purge;

    PROCEDURE mobile_purge;

    PROCEDURE dwm_purge;

    PROCEDURE stats_snapshot_purge;
END purge_pkg;
/

CREATE OR REPLACE PACKAGE BODY purge_pkg
AS
    gvprocedurename    VARCHAR2 (100);
    atccompanyid       company.company_id%TYPE;
    varchiveflag       NUMBER (1) := 0;
    nsqlcode           NUMBER;
    vsqlerrm           VARCHAR2 (300);
    garchivebasedttm   DATE := TRUNC (SYSDATE);
    gv_job_class_chk   PLS_INTEGER := 0;
    gcommitfrequency   PLS_INTEGER := 10000000;
    n_parallel_level   PLS_INTEGER := 0;
    gdelete_error      EXCEPTION;
    noparameter        EXCEPTION;
    nodatafound        EXCEPTION;
    gpurge_days        PLS_INTEGER := 0;
    gpurge_group       VARCHAR2 (10);

    PROCEDURE log_error (asqlcode   IN archive_error_log.sql_code%TYPE,
                         asqlerrm   IN archive_error_log.sql_errm%TYPE)
    AS
        PRAGMA AUTONOMOUS_TRANSACTION;
    BEGIN
        INSERT INTO archive_error_log (archive_dttm,
                                       sql_code,
                                       sql_errm,
                                       procedure_name)
             VALUES (SYSDATE,
                     asqlcode,
                     asqlerrm,
                     gvprocedurename);

        COMMIT;
    END log_error;

    PROCEDURE log_archive (
        apurge_group     IN archive_run_log.purge_group%TYPE,
        apurge_days      IN archive_run_log.purge_days%TYPE,
        aprocedurename   IN archive_run_log.procedure_name%TYPE,
        arecordcount     IN archive_run_log.record_count%TYPE,
        aruntime         IN archive_run_log.run_time%TYPE,
        pbucketid        IN archive_run_log.bucket_id%TYPE)
    IS
        PRAGMA AUTONOMOUS_TRANSACTION;
    BEGIN
        INSERT INTO archive_run_log (archive_dttm,
                                     purge_group,
                                     purge_days,
                                     procedure_name,
                                     record_count,
                                     run_time,
                                     bucket_id)
             VALUES (SYSDATE,
                     apurge_group,
                     apurge_days,
                     aprocedurename,
                     arecordcount,
                     (DBMS_UTILITY.get_time - aruntime) / 100,
                     pbucketid);

        COMMIT;
    END log_archive;

    PROCEDURE insert_del_data_bulk (p_prod_table_name   IN VARCHAR2,
                                    p_arch_table_name   IN VARCHAR2,
                                    p_temp_table           VARCHAR2,
                                    p_column_name       IN VARCHAR2,
                                    p_query                VARCHAR2,
                                    p_archive_flag         VARCHAR2)
    IS
        v_del_sql           VARCHAR2 (32767);
        v_sel_sql           VARCHAR2 (32767);
        n_chunk_size        PLS_INTEGER := 10000000;
        n_count             PLS_INTEGER := 0;
        v_schema_owner      VARCHAR2 (100);
        v_prod_table_name   VARCHAR2 (30);
        v_rec_count         PLS_INTEGER := 0;
        v_terminal          VARCHAR2 (100);
        v_machine_id        VARCHAR2 (100);
        v_logstart_time     NUMBER (13, 2);
    BEGIN
        v_prod_table_name := UPPER (p_prod_table_name);
        n_chunk_size := (gcommitfrequency / n_parallel_level);
        v_logstart_time := DBMS_UTILITY.get_time;

        SELECT SYS_CONTEXT ('userenv', 'current_schema'),
               SYS_CONTEXT ('userenv', 'terminal'),
               SYS_CONTEXT ('userenv', 'module')
          INTO v_schema_owner, v_terminal, v_machine_id
          FROM DUAL;

        rec_msglog.r_pgm_id := 'purge_pkg_Pkg';
        rec_msglog.r_terminal := v_terminal;
        rec_msglog.r_machine_id := v_machine_id;
        rec_msglog.r_start_dttm := TRUNC (SYSDATE);
        rec_msglog.r_table_name := v_prod_table_name;
        rec_msglog.r_company_id := atccompanyid;
        rec_msglog.r_sub_pgm_name :=
            SUBSTR (gvprocedurename, 1, INSTR (gvprocedurename, '-') - 2);

        --Check for existance of records to be archived

        IF p_query IS NULL
        THEN
            v_sel_sql :=
                   'SELECT COUNT(1) FROM '
                || v_prod_table_name
                || ' a '
                || ' WHERE '
                || p_column_name
                || ' IN (SELECT '
                || p_column_name
                || ' FROM '
                || p_temp_table
                || ' WHERE IS_ARCHIVED=-1)';
        ELSE
            v_sel_sql :=
                   'SELECT COUNT(1) FROM '
                || v_prod_table_name
                || ' a '
                || ' WHERE '
                || p_query;
        END IF;


        EXECUTE IMMEDIATE (v_sel_sql) INTO v_rec_count;

        IF v_rec_count > 0
        THEN
            -- drop task  del_records_task if it exist
            SELECT COUNT (*)
              INTO n_count
              FROM user_parallel_execute_tasks
             WHERE task_name = 'del_records_task';

            IF (n_count > 0)
            THEN
                DBMS_PARALLEL_EXECUTE.drop_task ('del_records_task');
            END IF;

            -- create tasks to run in parallel chunks

            DBMS_PARALLEL_EXECUTE.create_task (
                task_name   => 'del_records_task');

            IF p_query IS NULL
            THEN
                v_del_sql :=
                       'DELETE  FROM '
                    || v_prod_table_name
                    || ' a '
                    || ' WHERE '
                    || p_column_name
                    || ' IN (SELECT '
                    || p_column_name
                    || ' FROM '
                    || p_temp_table
                    || ' WHERE IS_ARCHIVED=-1)'
                    || ' AND rowid between :start_id and :end_id';
            ELSE
                v_del_sql :=
                       'DELETE  FROM '
                    || v_prod_table_name
                    || ' a '
                    || ' WHERE '
                    || p_query
                    || ' AND rowid between :start_id and :end_id';
            END IF;

            --creating chunks and assigning taskname to it

            DBMS_PARALLEL_EXECUTE.create_chunks_by_rowid (
                task_name     => 'del_records_task',
                table_owner   => v_schema_owner,
                table_name    => v_prod_table_name,
                by_row        => TRUE,
                chunk_size    => n_chunk_size);

            --execute task in parallel

            IF gv_job_class_chk = 1
            THEN
                DBMS_PARALLEL_EXECUTE.run_task (
                    task_name        => 'del_records_task',
                    sql_stmt         => v_del_sql,
                    language_flag    => DBMS_SQL.native,
                    parallel_level   => n_parallel_level,
                    job_class        => 'MA_ARCH_JOB_CLASS');
            ELSE
                DBMS_PARALLEL_EXECUTE.run_task (
                    task_name        => 'del_records_task',
                    sql_stmt         => v_del_sql,
                    language_flag    => DBMS_SQL.native,
                    parallel_level   => n_parallel_level);
            END IF;

            n_count := 0;

            SELECT COUNT (1)
              INTO n_count
              FROM user_parallel_execute_chunks
             WHERE     status = 'PROCESSED_WITH_ERROR'
                   AND task_name = 'del_records_task';

            IF n_count > 0
            THEN
                --Log the error message using the dictionary table

                SELECT ERROR_CODE, error_message
                  INTO nsqlcode, vsqlerrm
                  FROM user_parallel_execute_chunks
                 WHERE     LOWER (task_name) = 'del_records_task'
                       AND status = 'PROCESSED_WITH_ERROR'
                       AND ROWNUM < 2;

                gvprocedurename := v_prod_table_name;
                log_error (nsqlcode, vsqlerrm);
                RAISE gdelete_error;
            END IF;

            COMMIT;
        END IF;

        rec_msglog.r_end_dttm := TRUNC (SYSDATE);
        rec_msglog.r_rows_archived := v_rec_count;
        log_msg (rec_msglog);
        log_archive (gpurge_group,
                     gpurge_days,
                     v_prod_table_name,
                     v_rec_count,
                     v_logstart_time,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
    END insert_del_data_bulk;

    PROCEDURE update_purge_status (p_status IN VARCHAR2)
    AS
        PRAGMA AUTONOMOUS_TRANSACTION;
    BEGIN
        IF p_status = 'Failed'
        THEN
            UPDATE ARCHIVE_STATUS
               SET arch_run_status = 'Purge_Pkg.Inventory - Failed',
                   arch_end_dttm = SYSDATE
             WHERE arch_run_status LIKE 'Purge_Pkg.Inventory%';

            UPDATE ARCHIVE_STATUS
               SET arch_run_status = 'Purge_Pkg.EventLog - Failed',
                   arch_end_dttm = SYSDATE
             WHERE arch_run_status LIKE 'Purge_Pkg.EventLog%';

            UPDATE ARCHIVE_STATUS
               SET arch_run_status = 'Purge_Pkg - Failed',
                   arch_end_dttm = SYSDATE
             WHERE arch_run_status LIKE 'Purge_Pkg - %';
        ELSIF p_status = 'Success'
        THEN
            UPDATE ARCHIVE_STATUS
               SET arch_run_status = 'Purge_Pkg.Inventory - Completed',
                   arch_end_dttm = SYSDATE
             WHERE arch_run_status = 'Purge_Pkg.Inventory - Running';

            UPDATE ARCHIVE_STATUS
               SET arch_run_status = 'Purge_Pkg.EventLog - Completed',
                   arch_end_dttm = SYSDATE
             WHERE arch_run_status = 'Purge_Pkg.EventLog - Running';

            UPDATE ARCHIVE_STATUS
               SET arch_run_status = 'Purge_Pkg - Completed',
                   arch_end_dttm = SYSDATE
             WHERE arch_run_status = 'Purge_Pkg - Running';
        END IF;

        COMMIT;
    EXCEPTION
        WHEN OTHERS
        THEN
            log_error (SQLCODE,
                       'Error - Purge_pkg.Update_Purge_Status ' || SQLERRM);
    END update_purge_status;

    PROCEDURE log_msg (p_rec_MsgLog IN rt_MsgLog)
    IS
        PRAGMA AUTONOMOUS_TRANSACTION;
        v_parm_list   VARCHAR2 (500);
        vmsg          VARCHAR2 (1500);
    BEGIN
        vmsg :=
               p_rec_MsgLog.r_rows_archived
            || ' records Purged from table '
            || p_rec_MsgLog.r_table_name;

        INSERT INTO msg_log (MSG_LOG_ID,
                             MODULE,
                             MSG_ID,
                             PGM_ID,
                             SUB_PGM_NAME,
                             FUNC_NAME,
                             LOG_DATE_TIME,
                             MSG,
                             TERMINAL,
                             CREATE_DATE_TIME,
                             MOD_DATE_TIME,
                             MACHINE_ID,
                             WHSE,
                             CD_MASTER_ID)
             VALUES (msg_log_id_seq.NEXTVAL,
                     p_rec_MsgLog.r_module,
                     p_rec_MsgLog.r_msg_id,
                     p_rec_MsgLog.r_pgm_id,
                     p_rec_MsgLog.r_sub_pgm_name,
                     p_rec_MsgLog.r_func_name,
                     p_rec_MsgLog.r_start_dttm,
                     vmsg,
                     p_rec_MsgLog.r_terminal,
                     p_rec_MsgLog.r_start_dttm,
                     p_rec_MsgLog.r_end_dttm,
                     p_rec_MsgLog.r_machine_id,
                     p_rec_MsgLog.r_whse_id,
                     p_rec_MsgLog.r_cd_master_id);

        COMMIT;
    EXCEPTION
        WHEN OTHERS
        THEN
            log_error (SQLCODE, 'msg_log:Exception => ' || SQLERRM);
    END log_msg;

    FUNCTION get_config_days (p_purge_group IN VARCHAR2)
        RETURN PLS_INTEGER
    IS
        v_days   PLS_INTEGER := 0;
    BEGIN
        SELECT no_of_days
          INTO v_days
          FROM purge_criteria
         WHERE     PURGE_MODULE = 'OLM'
               AND UPPER (purge_group) = UPPER (p_purge_group);

        RETURN v_days;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            RAISE nodatafound;
    END get_config_days;

    PROCEDURE event_purge
    AS
    BEGIN
        gpurge_days := get_config_days ('EVENT');
        gvprocedurename := 'Purge_Pkg.Event_Purge - Begin';
        log_archive (gpurge_group,
                     gpurge_days,
                     gvprocedurename,
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
        insert_del_Data_bulk (
            'ORDER_EVENT',
            '',
            '',
            '',
               'created_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        insert_del_Data_bulk (
            'PURCHASE_ORDERS_EVENT',
            '',
            '',
            '',
               'created_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        insert_del_Data_bulk (
            'ASN_EVENT',
            '',
            '',
            '',
               'created_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        QUIET_DROP ('TABLE', 'EVENT_PURGE_GTT');

        EXECUTE IMMEDIATE
               'CREATE TABLE event_purge_gtt TABLESPACE DOM_DT_TBS NOLOGGING AS SELECT event_id FROM OM_SCHED_EVENT WHERE is_executed = 1 AND (scheduled_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ' OR executed_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ' )';

        insert_del_Data_bulk (
            'WF_BATCH_SCHEDULER_EVENT',
            '',
            '',
            '',
            'om_sched_event_id IN (SELECT event_id FROM event_purge_gtt)',
            'N');

        insert_del_Data_bulk (
            'CONSTEMP_SCHED_EVENT',
            '',
            '',
            '',
            'event_id IN (SELECT event_id FROM event_purge_gtt)',
            'N');

        insert_del_Data_bulk (
            'OM_SCHED_EVENT_REPORT',
            '',
            '',
            '',
            'event_id IN (SELECT event_id FROM event_purge_gtt)',
            'N');

        insert_del_Data_bulk (
            'OM_SCHED_EVENT',
            '',
            '',
            '',
            'event_id IN (SELECT event_id FROM event_purge_gtt)',
            'N');

        insert_del_Data_bulk (
            'SO_ORDER_CHG_VALUE',
            '',
            '',
            '',
            'event_id IN (SELECT event_id FROM event_purge_gtt)',
            'N');

        insert_del_Data_bulk (
            'A_PROCESS_TEMPLATE_SCHED',
            '',
            '',
            '',
            'event_id IN (SELECT event_id FROM event_purge_gtt)',
            'N');

        insert_del_Data_bulk (
            'A_WF_EVENTMAP',
            '',
            '',
            '',
            'event_id IN (SELECT event_id FROM event_purge_gtt)',
            'N');

        QUIET_DROP ('TABLE', 'EVENT_PURGE_GTT1');

        EXECUTE IMMEDIATE
               'CREATE TABLE event_purge_gtt1
			TABLESPACE DOM_DT_TBS
			NOLOGGING
			AS
			   SELECT lrf.report_id
				 FROM lrf_report lrf, lrf_event le, om_sched_event ose
				WHERE     lrf.event_id = le.lrf_event_id
					  AND le.event_id = ose.event_id
					  AND ose.is_executed = 1
					  AND ose.executed_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days;

        insert_del_Data_bulk (
            'LRF_REPORT_PARAM_VALUE_DTL',
            '',
            '',
            '',
            'report_id IN (SELECT report_id FROM event_purge_gtt1)',
            'N');

        insert_del_Data_bulk (
            'LRF_REPORT_PARAM_VALUE',
            '',
            '',
            '',
            'report_id IN (SELECT report_id FROM event_purge_gtt1)',
            'N');

        insert_del_Data_bulk (
            'LRF_REPORT',
            '',
            '',
            '',
            'report_id IN (SELECT report_id FROM event_purge_gtt1)',
            'N');

        insert_del_Data_bulk (
            'LRF_EVENT',
            '',
            '',
            '',
            'event_id IN (SELECT event_id FROM event_purge_gtt)',
            'N');

        QUIET_DROP ('TABLE', 'EVENT_PURGE_GTT');

        EXECUTE IMMEDIATE
               'CREATE TABLE event_purge_gtt TABLESPACE DOM_DT_TBS NOLOGGING AS SELECT a_process_run_id FROM a_process_run WHERE start_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days;

        insert_del_Data_bulk (
            'A_UNRELEASABLE_ALLOC',
            '',
            '',
            '',
            'a_processrunid IN (SELECT a_process_run_id FROM event_purge_gtt)',
            'N');

        insert_del_Data_bulk (
            'A_PROCESS_RUN',
            '',
            '',
            '',
            'a_process_run_id IN (SELECT a_process_run_id FROM event_purge_gtt)',
            'N');

        insert_del_Data_bulk (
            'A_FACILITY_WORKLOAD',
            '',
            '',
            '',
               'FW_DATE < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        QUIET_DROP ('TABLE', 'EVENT_PURGE_GTT');
        QUIET_DROP ('TABLE', 'EVENT_PURGE_GTT1');

        EXECUTE IMMEDIATE
               'CREATE TABLE event_purge_gtt TABLESPACE DOM_DT_TBS NOLOGGING AS SELECT purchase_orders_id , tc_purchase_orders_id
		 FROM purchase_orders WHERE purchase_orders_type = 2 AND purchase_orders_status IN (940,950)
		 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days;

        EXECUTE IMMEDIATE
            'CREATE TABLE event_purge_gtt1 TABLESPACE DOM_DT_TBS NOLOGGING AS SELECT purchase_orders_line_item_id 
		FROM purchase_orders_line_item WHERE purchase_orders_id IN (SELECT purchase_orders_id FROM event_purge_gtt)';

        insert_del_Data_bulk (
            'A_ALLOC_COST_TRACE_CARTON',
            '',
            '',
            '',
            'cost_trace_id IN (SELECT cost_trace_id FROM a_alloc_cost_trace WHERE order_id IN (SELECT purchase_orders_id FROM event_purge_gtt))',
            'N');

        insert_del_Data_bulk (
            'A_ALLOC_COST_TRACE',
            '',
            '',
            '',
            'order_id IN (SELECT purchase_orders_id FROM event_purge_gtt)',
            'N');

        insert_del_Data_bulk (
            'A_ALLOCATION_TRACE_DETAIL',
            '',
            '',
            '',
            'tc_purchase_order_id IN (SELECT tc_purchase_order_id FROM event_purge_gtt)',
            'N');

        insert_del_Data_bulk (
            'A_FACILITY_TRACE_DETAIL',
            '',
            '',
            '',
            'a_orderlineid IN (SELECT purchase_orders_line_item_id FROM event_purge_gtt1)',
            'N');

        insert_del_Data_bulk (
            'A_FACILITY_TRACE',
            '',
            '',
            '',
            'a_orderlineid IN (SELECT purchase_orders_line_item_id FROM event_purge_gtt1)',
            'N');

        insert_del_Data_bulk (
            'ASSIGNMENT_EVENT',
            '',
            '',
            '',
               'created_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        insert_del_Data_bulk (
            'BOOKING_EVENT',
            '',
            '',
            '',
               'created_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        insert_del_Data_bulk (
            'CARRIER_PARAMETER_EVENT',
            '',
            '',
            '',
               'last_updated_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        insert_del_Data_bulk (
            'CLAIMS_EVENT',
            '',
            '',
            '',
               'created_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        insert_del_Data_bulk (
            'DETENTION_EVENT',
            '',
            '',
            '',
               'created_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        insert_del_Data_bulk (
            'ILM_APPOINTMENT_EVENTS',
            '',
            '',
            '',
               'created_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        insert_del_Data_bulk (
            'ILM_DOCK_DOOR_EVENTS',
            '',
            '',
            '',
               'created_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        insert_del_Data_bulk (
            'ILM_TASK_EVENTS',
            '',
            '',
            '',
               'created_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        insert_del_Data_bulk (
            'ILM_TRAILER_EVENTS',
            '',
            '',
            '',
               'created_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        insert_del_Data_bulk (
            'INVOICE_EVENT',
            '',
            '',
            '',
               'created_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        insert_del_Data_bulk (
            'LPN_EVENT',
            '',
            '',
            '',
               'created_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        insert_del_Data_bulk (
            'RATING_EVENT',
            '',
            '',
            '',
               'last_updated_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        insert_del_Data_bulk (
            'RTS_EVENT',
            '',
            '',
            '',
               'created_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        insert_del_Data_bulk (
            'SHIPMENT_EVENT',
            '',
            '',
            '',
               'created_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        EXECUTE IMMEDIATE ('DELETE IMPORT_RATING_LANE_DTL_RATE');

        EXECUTE IMMEDIATE ('DELETE IMPORT_LANE_ACCESSORIAL');

        EXECUTE IMMEDIATE ('DELETE IMPORT_RATING_LANE_DTL');

        EXECUTE IMMEDIATE ('DELETE IMPORT_RATING_LANE');

        EXECUTE IMMEDIATE ('DELETE RATING_LANE_ERRORS');

        QUIET_DROP ('TABLE', 'EVENT_PURGE_GTT');
        QUIET_DROP ('TABLE', 'EVENT_PURGE_GTT1');

        EXECUTE IMMEDIATE
            'CREATE TABLE event_purge_gtt TABLESPACE DOM_DT_TBS NOLOGGING AS 
			SELECT asn_id AS object_id FROM asn
			UNION
			SELECT shipment_id AS object_id FROM shipment
			UNION
			SELECT order_id AS object_id FROM orders
			UNION
			SELECT purchase_orders_id AS object_id FROM purchase_orders
			UNION
			SELECT booking_id AS object_id FROM booking
			UNION
			SELECT purchase_orders_line_item_id AS object_id
			  FROM purchase_orders_line_item
			UNION
			SELECT lpn_id AS object_id FROM LPN
			UNION
			SELECT line_item_id AS object_id FROM ORDER_LINE_ITEM';

        EXECUTE IMMEDIATE
            'CREATE TABLE event_purge_gtt1 TABLESPACE DOM_DT_TBS NOLOGGING AS 
		 SELECT DISTINCT eot.evtmgt_obj_type_id, eeo.object_id, eeo.event_occurrence_id
		  FROM em_object_type eot, em_event_occurrence eeo
		 WHERE     eot.evtmgt_obj_type_id = eeo.evtmgt_obj_type_id
			   AND eot.evtmgt_obj_type_id <> 32
			   AND eeo.object_id NOT IN (SELECT object_id
										   FROM event_purge_gtt)';

        insert_del_Data_bulk (
            'EM_OE_ADDINF_VAL',
            '',
            '',
            '',
            'em_oe_addinf_attid IN (SELECT em_oe_addinf_attid FROM em_oe_addinf_att WHERE event_occurrence_id IN (SELECT event_occurrence_id FROM event_purge_gtt1))',
            'N');

        insert_del_Data_bulk (
            'EM_OE_ADDINF_ATT',
            '',
            '',
            '',
            'event_occurrence_id IN (SELECT event_occurrence_id FROM event_purge_gtt1)',
            'N');

        insert_del_Data_bulk (
            'EM_EVENT_OCCURRENCE',
            '',
            '',
            '',
            'event_occurrence_id IN (SELECT event_occurrence_id FROM event_purge_gtt1)',
            'N');

        QUIET_DROP ('TABLE', 'EVENT_PURGE_GTT1');
        QUIET_DROP ('TABLE', 'EVENT_PURGE_GTT2');

        EXECUTE IMMEDIATE
            'CREATE TABLE event_purge_gtt1 TABLESPACE DOM_DT_TBS NOLOGGING AS 
		 SELECT DISTINCT eot.evtmgt_obj_type_id, eeo.object_id, eeo.object_sched_group_id
		  FROM em_object_type eot, em_object_sched_group eeo
		 WHERE eot.evtmgt_obj_type_id = eeo.evtmgt_obj_type_id
			   AND eot.evtmgt_obj_type_id <> 32
			   AND eeo.object_id NOT IN (SELECT object_id
										   FROM event_purge_gtt)';

        EXECUTE IMMEDIATE
            'CREATE TABLE event_purge_gtt2 TABLESPACE DOM_DT_TBS NOLOGGING AS SELECT obj_sched_id 
	  FROM em_object_schedule WHERE object_sched_group_id IN (SELECT object_sched_group_id FROM event_purge_gtt1)';

        insert_del_Data_bulk (
            'EM_OBJECT_RULE',
            '',
            '',
            '',
            'obj_sched_id IN (SELECT obj_sched_id FROM event_purge_gtt2)',
            'N');

        insert_del_Data_bulk (
            'EM_LINK_OBJECT_SCHED',
            '',
            '',
            '',
            'to_object_sched_event_id IN (SELECT obj_sched_event_id FROM em_object_schedule_event WHERE obj_sched_id IN (SELECT OBJ_SCHED_ID FROM event_purge_gtt2))',
            'N');

        insert_del_Data_bulk (
            'EM_LINK_OBJECT_SCHED',
            '',
            '',
            '',
            'from_object_sched_id IN (SELECT obj_sched_id FROM event_purge_gtt2)',
            'N');

        insert_del_Data_bulk (
            'EM_LINK_OBJECT_SCHED',
            '',
            '',
            '',
            'TO_OBJECT_SCHED_ID IN (SELECT obj_sched_id FROM event_purge_gtt2)',
            'N');

        insert_del_Data_bulk (
            'EM_OBJECT_SCHEDULE_EVENT',
            '',
            '',
            '',
            'OBJ_SCHED_ID IN (SELECT obj_sched_id FROM event_purge_gtt2)',
            'N');

        insert_del_Data_bulk (
            'EM_OBJECT_SCHEDULE',
            '',
            '',
            '',
            'OBJ_SCHED_ID IN (SELECT obj_sched_id FROM event_purge_gtt2)',
            'N');

        insert_del_Data_bulk (
            'EM_RULE_OBJ_SCHED_GROUP',
            '',
            '',
            '',
            'object_sched_group_id IN (SELECT object_sched_group_id FROM event_purge_gtt1)',
            'N');

        insert_del_Data_bulk (
            'EM_OBJECT_SCHED_GROUP',
            '',
            '',
            '',
            'object_sched_group_id IN (SELECT object_sched_group_id FROM event_purge_gtt1)',
            'N');

        insert_del_Data_bulk (
            'EM_OBJECTEVENTHIST',
            '',
            '',
            '',
            '(evtmgt_obj_type_id,object_id) IN (SELECT evtmgt_obj_type_id,object_id FROM event_purge_gtt1)',
            'N');

        QUIET_DROP ('TABLE', 'EVENT_PURGE_GTT1');

        EXECUTE IMMEDIATE
            'CREATE TABLE event_purge_gtt1 TABLESPACE DOM_DT_TBS NOLOGGING AS SELECT DISTINCT 
		eot.evtmgt_obj_type_id, eeo.object_id, eeo.obj_event_notif_id
		  FROM em_object_type eot, em_obj_evt_notify eeo
		 WHERE     eot.evtmgt_obj_type_id = eeo.evtmgt_obj_type_id
			   AND eot.evtmgt_obj_type_id <> 32
			   AND eeo.object_id nOT IN (SELECT object_id
										   FROM event_purge_gtt)';

        insert_del_Data_bulk (
            'EM_OBJEVTNOTIFYLOG',
            '',
            '',
            '',
            'objevt_notif_logid IN (SELECT obj_event_notif_id FROM event_purge_gtt1)',
            'N');

        insert_del_Data_bulk (
            'EM_OBJ_NOTIFY_CONTENT',
            '',
            '',
            '',
            'obj_event_notif_id IN (SELECT obj_event_notif_id FROM event_purge_gtt1)',
            'N');

        insert_del_Data_bulk (
            'EM_OBJ_EVT_NOTIFY',
            '',
            '',
            '',
            'obj_event_notif_id IN (SELECT obj_event_notif_id FROM event_purge_gtt1)',
            'N');
        QUIET_DROP ('TABLE', 'EVENT_PURGE_GTT');
        QUIET_DROP ('TABLE', 'EVENT_PURGE_GTT1');
        QUIET_DROP ('TABLE', 'EVENT_PURGE_GTT2');

        gvprocedurename := 'Purge_Pkg.Event_Purge - End';
        log_archive (gpurge_group,
                     gpurge_days,
                     gvprocedurename,
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
    EXCEPTION
        WHEN nodatafound
        THEN
            RAISE noparameter;
        WHEN OTHERS
        THEN
            nsqlcode := SQLCODE;
            vsqlerrm := SQLERRM;
            RAISE gdelete_error;
    END event_purge;

    PROCEDURE exp_table_dump (p_directory      IN VARCHAR2,
                              p_table_name     IN VARCHAR2,
                              p_table_filter   IN VARCHAR2,
                              p_time           IN VARCHAR2)
    IS
        v_handle               NUMBER;                           -- job handle
        v_current_time         DATE := SYSDATE; -- consistent timestamp for files, job_name etc.
        v_start_time           DATE;                -- start time for log file
        v_logfile_name         VARCHAR2 (200);                 -- logfile name
        v_dumpfile_name        VARCHAR2 (200);                 -- logfile name
        v_line_no              INTEGER := 0;                  -- debug line no
        v_sqlcode              NUMBER;                              -- sqlcode
        v_compatible           VARCHAR2 (40) := 'COMPATIBLE'; -- default is 'COMPATIBLE'
        vc_job_mode   CONSTANT VARCHAR2 (200) := 'TABLE';          -- Job mode
        garchivebasedttm       DATE := SYSDATE;
        p_schema_name          VARCHAR2 (100);

        percent_done           NUMBER;           -- Percentage of job complete
        job_state              VARCHAR2 (30);    -- To keep track of job state
        le                     ku$_LogEntry;     -- For WIP and error messages
        js                     ku$_JobStatus; -- The job status from get_status
        jd                     ku$_JobDesc; -- The job description from get_status
        sts                    ku$_Status; -- The status object returned by get_status
        rtn_code               NUMBER := 0;
        ind                    NUMBER;                           -- Loop index
        verror                 EXCEPTION;
    BEGIN
        SELECT USER INTO p_schema_name FROM DUAL;

        n_parallel_level := 6;
        v_logfile_name :=
            'expdp_' || p_table_name || '_' || p_time || '_export.log';
        v_dumpfile_name :=
            'expdp_' || p_table_name || '_' || p_time || '_%U.dmp';

        -- Open the job
        BEGIN
            v_handle :=
                DBMS_DATAPUMP.open (
                    operation   => 'EXPORT',
                    job_mode    => vc_job_mode,
                    job_name    =>    'EXPORT_'
                                   || vc_job_mode
                                   || '_'
                                   || TO_CHAR (v_current_time,
                                               'YYYY_MMDD_HH24MI'),
                    version     => v_compatible);
        EXCEPTION
            WHEN OTHERS
            THEN
                DBMS_OUTPUT.put_line (
                    SUBSTR ('Failure in dbms_datapump.open', 1, 255));
                RAISE;
        END;

        -- Add a logfile
        DBMS_DATAPUMP.add_file (
            handle      => v_handle,
            filename    => v_logfile_name,
            directory   => p_directory,
            filetype    => DBMS_DATAPUMP.ku$_file_type_log_file);


        -- Add a datafile
        DBMS_DATAPUMP.add_file (
            handle      => v_handle,
            filename    => v_dumpfile_name,
            directory   => p_directory,
            filetype    => DBMS_DATAPUMP.ku$_file_type_dump_file);
        v_line_no := 500;                                     -- debug line no


        -- Filter for the schemma
        DBMS_DATAPUMP.metadata_filter (
            handle   => v_handle,
            name     => 'SCHEMA_LIST',
            VALUE    => '''' || p_schema_name || '''');

        --Filter for the table
        DBMS_DATAPUMP.metadata_filter (
            handle   => v_handle,
            name     => 'NAME_LIST',
            VALUE    => '''' || p_table_name || '''');


        -- Add a subquery
        DBMS_DATAPUMP.data_filter (handle   => v_handle,
                                   name     => 'SUBQUERY',
                                   VALUE    => p_table_filter);

        DBMS_DATAPUMP.set_parallel (handle   => v_handle,
                                    degree   => n_parallel_level);
        DBMS_DATAPUMP.metadata_filter (
            handle   => v_handle,
            name     => 'EXCLUDE_PATH_EXPR',
            VALUE    => 'IN (''INDEX'', ''SYNONYMS'',''GRANTS'',''STATISTICS'',''CONSTRAINT'',''REF_CONSTRAINT'')');
        -- Get the start time
        v_start_time := SYSDATE;

        -- Add a start time to the log file
        DBMS_DATAPUMP.log_entry (
            handle          => v_handle,
            MESSAGE         =>    'Job Start at '
                               || TO_CHAR (v_start_time, 'DD-Mon-RR HH24:MI:SS'),
            log_file_only   => 0);

        -- Start the job
        DBMS_DATAPUMP.start_job (handle => v_handle, cluster_ok => 0);

        percent_done := 0;
        job_state := 'UNDEFINED';

        -- dbms_output.put_line('here1');
        WHILE (job_state != 'COMPLETED') AND (job_state != 'STOPPED')
        LOOP
            DBMS_DATAPUMP.get_status (
                v_handle,
                  DBMS_DATAPUMP.ku$_status_job_error
                + DBMS_DATAPUMP.ku$_status_job_status
                + DBMS_DATAPUMP.ku$_status_wip
                + DBMS_DATAPUMP.ku$_job_complete_errors,
                -1,
                job_state,
                sts);
            js := sts.job_status;

            -- If the percentage done changed, display the new value.
            IF js.percent_done != percent_done
            THEN
                DBMS_OUTPUT.put_line (
                    '*** Job percent done = ' || TO_CHAR (js.percent_done));
                percent_done := js.percent_done;
            END IF;

            -- If any work-in-progress (WIP) or error messages were received for the job,
            -- display them.

            --            IF (BITAND (sts.mask, DBMS_DATAPUMP.ku$_status_wip) != 0)
            --            THEN
            --               le := sts.wip;
            --            ELSE
            IF (BITAND (sts.mask, DBMS_DATAPUMP.ku$_status_job_error) != 0)
            THEN
                le := sts.error;
            ELSIF (BITAND (sts.mask, DBMS_DATAPUMP.ku$_job_complete_errors) !=
                       0)
            THEN
                le := sts.error;
            ELSE
                le := NULL;
            END IF;

            --            END IF;

            IF le IS NOT NULL
            THEN
                ind := le.FIRST;

                WHILE ind IS NOT NULL
                LOOP
                    DBMS_OUTPUT.put_line (le (ind).LogText);
                    ind := le.NEXT (ind);
                    RAISE verror;
                END LOOP;
            END IF;
        END LOOP;

        -- Indicate that the job finished and detach from it.
        IF job_state != 'COMPLETED'
        THEN
            rtn_code := 1;
        END IF;

        DBMS_DATAPUMP.detach (handle => v_handle);
    EXCEPTION
        WHEN verror
        THEN
            raise_application_error (
                -20006,
                   'Export Failed, please check the logfile - '
                || v_logfile_name
                || ' at database datapump directory - '
                || p_directory);
        WHEN OTHERS
        THEN
            BEGIN
                DBMS_DATAPUMP.detach (handle => v_handle);
            EXCEPTION
                WHEN OTHERS
                THEN
                    NULL;
            END;

            DBMS_OUTPUT.put_line (
                SUBSTR ('Value of v_line_no=' || TO_CHAR (v_line_no), 1, 255));
            RAISE;
    END exp_table_dump;

    PROCEDURE imp_table_dump (p_directory    IN VARCHAR2,
                              p_table_name   IN VARCHAR2,
                              p_time         IN VARCHAR2)
    AS
        v_handle           NUMBER;                               -- job handle
        v_current_time     DATE := SYSDATE; -- consistent timestamp for files, job_name etc.
        v_start_time       DATE;                    -- start time for log file
        v_dumpfile_name    VARCHAR2 (200);
        v_logfile_name     VARCHAR2 (200);                     -- logfile name
        --   v_default_dir          VARCHAR (30) := 'DATA_PUMP_DIR';        -- directory
        --v_default_dir      VARCHAR (30) := 'CDTMW';                    -- directory
        v_line_no          INTEGER := 0;                      -- debug line no
        v_sqlcode          NUMBER;                                  -- sqlcode
        garchivebasedttm   DATE := SYSDATE;
        p_schema_name      VARCHAR2 (100);

        percent_done       NUMBER;               -- Percentage of job complete
        job_state          VARCHAR2 (30);        -- To keep track of job state
        le                 ku$_LogEntry;         -- For WIP and error messages
        js                 ku$_JobStatus;    -- The job status from get_status
        jd                 ku$_JobDesc; -- The job description from get_status
        sts                ku$_Status; -- The status object returned by get_status
        rtn_code           NUMBER := 0;
        ind                NUMBER;                               -- Loop index
        verror             EXCEPTION;
    BEGIN
        SELECT USER INTO p_schema_name FROM DUAL;

        n_parallel_level := 6;

        BEGIN
            v_handle :=
                DBMS_DATAPUMP.open (
                    operation   => 'IMPORT',
                    job_mode    => 'TABLE',
                    job_name    =>    'Import_Table_'
                                   || TO_CHAR (v_current_time,
                                               'YYYY_MMDD_HH24MI_SS'),
                    version     => 'COMPATIBLE');
        EXCEPTION
            WHEN OTHERS
            THEN
                DBMS_OUTPUT.put_line (
                    SUBSTR ('Failure in dbms_datapump.open', 1, 255));
                RAISE;
        END;

        v_logfile_name :=
            'impdp_' || p_table_name || '_' || p_time || '_import.log';
        v_dumpfile_name :=
            'expdp_' || p_table_name || '_' || p_time || '_%U.dmp';

        DBMS_DATAPUMP.add_file (
            handle      => v_handle,
            filename    => v_logfile_name,
            directory   => p_directory,
            filetype    => DBMS_DATAPUMP.ku$_file_type_log_file);

        DBMS_DATAPUMP.add_file (
            handle      => v_handle,
            filename    => v_dumpfile_name,
            directory   => p_directory,
            filetype    => DBMS_DATAPUMP.ku$_file_type_dump_file);

        DBMS_DATAPUMP.metadata_filter (
            handle   => v_handle,
            name     => 'SCHEMA_LIST',
            VALUE    => '''' || p_schema_name || '''');


        DBMS_DATAPUMP.metadata_filter (
            handle   => v_handle,
            name     => 'NAME_LIST',
            VALUE    => '''' || p_table_name || '''');

        DBMS_DATAPUMP.SET_PARAMETER (handle   => v_handle,
                                     name     => 'INCLUDE_METADATA',
                                     VALUE    => 0);

        DBMS_DATAPUMP.SET_PARAMETER (handle   => v_handle,
                                     name     => 'TABLE_EXISTS_ACTION',
                                     VALUE    => 'TRUNCATE');

        DBMS_DATAPUMP.set_parallel (handle   => v_handle,
                                    degree   => n_parallel_level);
        DBMS_DATAPUMP.start_job (handle => v_handle, cluster_ok => 0);

        percent_done := 0;
        job_state := 'UNDEFINED';

        -- dbms_output.put_line('here1');
        WHILE (job_state != 'COMPLETED') AND (job_state != 'STOPPED')
        LOOP
            DBMS_DATAPUMP.get_status (
                v_handle,
                  DBMS_DATAPUMP.ku$_status_job_error
                + DBMS_DATAPUMP.ku$_status_job_status
                + DBMS_DATAPUMP.ku$_status_wip
                + DBMS_DATAPUMP.ku$_job_complete_errors,
                -1,
                job_state,
                sts);
            js := sts.job_status;

            -- If the percentage done changed, display the new value.
            IF js.percent_done != percent_done
            THEN
                DBMS_OUTPUT.put_line (
                    '*** Job percent done = ' || TO_CHAR (js.percent_done));
                percent_done := js.percent_done;
            END IF;

            -- If any work-in-progress (WIP) or error messages were received for the job,
            -- display them.

            --            IF (BITAND (sts.mask, DBMS_DATAPUMP.ku$_status_wip) != 0)
            --            THEN
            --               le := sts.wip;
            --            ELSE
            IF (BITAND (sts.mask, DBMS_DATAPUMP.ku$_status_job_error) != 0)
            THEN
                le := sts.error;
            ELSIF (BITAND (sts.mask, DBMS_DATAPUMP.ku$_job_complete_errors) !=
                       0)
            THEN
                le := sts.error;
            ELSE
                le := NULL;
            END IF;

            --            END IF;

            IF le IS NOT NULL
            THEN
                ind := le.FIRST;

                WHILE ind IS NOT NULL
                LOOP
                    DBMS_OUTPUT.put_line (le (ind).LogText);
                    ind := le.NEXT (ind);
                    RAISE verror;
                END LOOP;
            END IF;
        END LOOP;

        -- Indicate that the job finished and detach from it.
        IF job_state != 'COMPLETED'
        THEN
            rtn_code := 1;
        END IF;

        DBMS_DATAPUMP.detach (handle => v_handle);
    EXCEPTION
        WHEN verror
        THEN
            raise_application_error (
                -20006,
                   'Import Failed, please check the logfile - '
                || v_logfile_name
                || ' at database datapump directory - '
                || p_directory);
        WHEN OTHERS
        THEN
            BEGIN
                DBMS_DATAPUMP.detach (handle => v_handle);
            EXCEPTION
                WHEN OTHERS
                THEN
                    NULL;
            END;

            DBMS_OUTPUT.put_line (
                SUBSTR ('Value of v_line_no=' || TO_CHAR (v_line_no), 1, 255));
            RAISE;
    END imp_table_dump;

    PROCEDURE purge_history_data (p_datapump_directory   IN VARCHAR2,
                                  p_table_name           IN VARCHAR2,
                                  p_retention_days       IN NUMBER)
    AS
        v_where_clause     VARCHAR2 (1000);
        garchivebasedttm   DATE := SYSDATE;
        v_sql              VARCHAR2 (1000);
        v_count_tab1       NUMBER := 0;
        v_count_tab2       NUMBER := 0;
        v_count_tab3       NUMBER := 0;
        v_count_tab4       NUMBER := 0;
        v_count_tab5       NUMBER := 0;
        v_count_tab6       NUMBER := 0;
        v_count_tab7       NUMBER := 0;
        v_temp             VARCHAR2 (1000);
        v_timestamp        VARCHAR2 (10);
        v_notsupported     EXCEPTION;
        v_cons_count       NUMBER := 0;
    BEGIN
        EXECUTE IMMEDIATE
            'alter session set NLS_TIMESTAMP_FORMAT = ''YYYY-MM-DD HH24:MI:SS.FF''';

        EXECUTE IMMEDIATE
            'alter session set NLS_DATE_FORMAT = ''YYYY-MM-DD HH24:MI:SS''';

        SELECT TO_CHAR (SYSDATE, 'HHSS') INTO v_timestamp FROM DUAL;

        IF p_table_name = 'I_AVAIL_INVENTORY_EVENT'
        THEN
            BEGIN
                log_archive (
                    NULL,
                    NULL,
                    'Purge_History_Data - I_AVAIL_INVENTORY_EVENT - Begin',
                    NULL,
                    NULL,
                    SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

                QUIET_DROP ('TABLE', 'COMMERCE_PURGE_GTT4');
                QUIET_DROP ('TABLE', 'COMMERCE_PURGE_GTT5');

                BEGIN
                    EXECUTE IMMEDIATE
                        'LOCK TABLE MASTER_COMMERCE_EVENT IN SHARE MODE NOWAIT';

                    EXECUTE IMMEDIATE
                        'LOCK TABLE I_AVAIL_INVENTORY_EVENT_DETAIL IN SHARE MODE NOWAIT';

                    EXECUTE IMMEDIATE
                        'LOCK TABLE I_AVAIL_INVENTORY_EVENT IN SHARE MODE NOWAIT';
                EXCEPTION
                    WHEN OTHERS
                    THEN
                        log_error (SQLCODE,
                                   'Table locked by other transaction');
                        raise_application_error (
                            -20005,
                            'Table locked by other transaction');
                END;

                EXECUTE IMMEDIATE
                       'CREATE TABLE COMMERCE_PURGE_GTT4 TABLESPACE DOM_DT_TBS AS SELECT EVENT_ID FROM MASTER_COMMERCE_EVENT WHERE LAST_UPDATED_DTTM < TO_DATE('''
                    || garchivebasedttm
                    || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
                    || p_retention_days;

                EXECUTE IMMEDIATE
                    'CREATE TABLE COMMERCE_PURGE_GTT5 TABLESPACE DOM_DT_TBS AS SELECT AVAIL_INV_EVENT_ID FROM I_AVAIL_INVENTORY_EVENT WHERE EVENT_ID IN (SELECT EVENT_ID FROM COMMERCE_PURGE_GTT4)';

                v_where_clause :=
                    'WHERE AVAIL_INV_EVENT_ID NOT IN (SELECT AVAIL_INV_EVENT_ID FROM COMMERCE_PURGE_GTT5)';

                v_sql :=
                    'SELECT COUNT(1) FROM I_AVAIL_INVENTORY_EVENT_DETAIL WHERE AVAIL_INV_EVENT_ID IN (SELECT AVAIL_INV_EVENT_ID FROM COMMERCE_PURGE_GTT5)';

                EXECUTE IMMEDIATE (v_sql) INTO v_count_tab1;

                exp_table_dump (p_datapump_directory,
                                'I_AVAIL_INVENTORY_EVENT_DETAIL',
                                v_where_clause,
                                v_timestamp);

                v_where_clause :=
                    'WHERE EVENT_ID NOT IN (SELECT EVENT_ID FROM COMMERCE_PURGE_GTT4)';

                v_sql :=
                    'SELECT COUNT(1) FROM I_AVAIL_INVENTORY_EVENT WHERE EVENT_ID IN (SELECT EVENT_ID FROM COMMERCE_PURGE_GTT4)';

                EXECUTE IMMEDIATE (v_sql) INTO v_count_tab2;

                exp_table_dump (p_datapump_directory,
                                'I_AVAIL_INVENTORY_EVENT',
                                v_where_clause,
                                v_timestamp);

                EXECUTE IMMEDIATE
                    'ALTER TABLE I_AVAIL_INVENTORY_EVENT_DETAIL DISABLE CONSTRAINT I_AVL_INVTRY_EVENT_DTL_FK0';

                imp_table_dump (p_datapump_directory,
                                'I_AVAIL_INVENTORY_EVENT',
                                v_timestamp);

                log_archive (NULL,
                             NULL,
                             'I_AVAIL_INVENTORY_EVENT',
                             v_count_tab2,
                             NULL,
                             SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

                imp_table_dump (p_datapump_directory,
                                'I_AVAIL_INVENTORY_EVENT_DETAIL',
                                v_timestamp);

                EXECUTE IMMEDIATE
                    'ALTER TABLE I_AVAIL_INVENTORY_EVENT_DETAIL ENABLE CONSTRAINT I_AVL_INVTRY_EVENT_DTL_FK0';

                log_archive (NULL,
                             NULL,
                             'I_AVAIL_INVENTORY_EVENT_DETAIL',
                             v_count_tab1,
                             NULL,
                             SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
                log_archive (
                    NULL,
                    NULL,
                    'Purge_History_Data - I_AVAIL_INVENTORY_EVENT - End',
                    NULL,
                    NULL,
                    SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
            EXCEPTION
                WHEN OTHERS
                THEN
                    log_error (SQLCODE,
                               'Error in Purge_History_Data Procedure');

                    SELECT COUNT (*)
                      INTO v_cons_count
                      FROM user_constraints
                     WHERE     constraint_name = 'I_AVL_INVTRY_EVENT_DTL_FK0'
                           AND status = 'DISABLED';

                    IF v_cons_count = 1
                    THEN
                        EXECUTE IMMEDIATE
                            'ALTER TABLE I_AVAIL_INVENTORY_EVENT_DETAIL ENABLE CONSTRAINT I_AVL_INVTRY_EVENT_DTL_FK0';
                    END IF;

                    raise_application_error (
                        -20005,
                        'Error in history_purge_data procedure execution.');
            END;
        ELSIF p_table_name = 'MASTER_COMMERCE_EVENT'
        THEN
            BEGIN
                log_archive (
                    NULL,
                    NULL,
                    'Purge_History_Data - MASTER_COMMERCE_EVENT - Begin',
                    NULL,
                    NULL,
                    SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

                QUIET_DROP ('TABLE', 'PURGE_STAGING_EVENT_GTT2');
                QUIET_DROP ('TABLE', 'PURGE_STAGING_EVENT_GTT3');

                BEGIN
                    EXECUTE IMMEDIATE
                        'LOCK TABLE MASTER_COMMERCE_EVENT IN SHARE MODE NOWAIT';

                    EXECUTE IMMEDIATE
                        'LOCK TABLE I_AVAIL_INVENTORY_EVENT_DETAIL IN SHARE MODE NOWAIT';

                    EXECUTE IMMEDIATE
                        'LOCK TABLE I_AVAIL_INVENTORY_EVENT IN SHARE MODE NOWAIT';

                    EXECUTE IMMEDIATE
                        'LOCK TABLE I_FULFILLMENT_OUTAGE_EVENT IN SHARE MODE NOWAIT';

                    EXECUTE IMMEDIATE
                        'LOCK TABLE I_REBUILD_STAGING IN SHARE MODE NOWAIT';

                    EXECUTE IMMEDIATE
                        'LOCK TABLE I_AVAIL_SYNC_STAGING IN SHARE MODE NOWAIT';
                EXCEPTION
                    WHEN OTHERS
                    THEN
                        log_error (SQLCODE,
                                   'Table locked by other transaction');
                        raise_application_error (
                            -20005,
                            'Table locked by other transaction');
                END;

                EXECUTE IMMEDIATE
                       'CREATE TABLE PURGE_STAGING_EVENT_GTT2 TABLESPACE DOM_DT_TBS AS SELECT event_id FROM MASTER_COMMERCE_EVENT WHERE EVENT_STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC IN (''Success'', ''Validation Failure'', ''Error'')) AND  LAST_UPDATED_DTTM < TO_DATE('''
                    || garchivebasedttm
                    || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
                    || p_retention_days;

                EXECUTE IMMEDIATE
                    'CREATE TABLE PURGE_STAGING_EVENT_GTT3 TABLESPACE DOM_DT_TBS AS SELECT AVAIL_INV_EVENT_ID FROM I_AVAIL_INVENTORY_EVENT WHERE event_id IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

                v_where_clause :=
                    'WHERE AVAIL_INV_EVENT_ID NOT IN (SELECT AVAIL_INV_EVENT_ID FROM PURGE_STAGING_EVENT_GTT3)';

                v_sql :=
                    'SELECT COUNT(1) FROM I_AVAIL_INVENTORY_EVENT_DETAIL WHERE AVAIL_INV_EVENT_ID IN (SELECT AVAIL_INV_EVENT_ID FROM PURGE_STAGING_EVENT_GTT3)';

                EXECUTE IMMEDIATE (v_sql) INTO v_count_tab1;

                exp_table_dump (p_datapump_directory,
                                'I_AVAIL_INVENTORY_EVENT_DETAIL',
                                v_where_clause,
                                v_timestamp);

                v_where_clause :=
                    'WHERE event_id NOT IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

                v_sql :=
                    'SELECT COUNT(1) FROM I_AVAIL_INVENTORY_EVENT WHERE event_id IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

                EXECUTE IMMEDIATE (v_sql) INTO v_count_tab2;

                exp_table_dump (p_datapump_directory,
                                'I_AVAIL_INVENTORY_EVENT',
                                v_where_clause,
                                v_timestamp);
                v_where_clause :=
                    'WHERE event_id NOT IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

                v_sql :=
                    'SELECT COUNT(1) FROM I_FULFILLMENT_OUTAGE_EVENT WHERE event_id IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

                EXECUTE IMMEDIATE (v_sql) INTO v_count_tab3;

                exp_table_dump (p_datapump_directory,
                                'I_FULFILLMENT_OUTAGE_EVENT',
                                v_where_clause,
                                v_timestamp);

                v_where_clause :=
                    'WHERE rebuild_event_id NOT IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';
                v_sql :=
                    'SELECT COUNT(1) FROM I_REBUILD_EVENT WHERE rebuild_event_id IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

                EXECUTE IMMEDIATE (v_sql) INTO v_count_tab4;

                exp_table_dump (p_datapump_directory,
                                'I_REBUILD_EVENT',
                                v_where_clause,
                                v_timestamp);

                v_where_clause :=
                    'WHERE event_id NOT IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

                v_sql :=
                    'SELECT COUNT(1) FROM I_REBUILD_STAGING WHERE event_id IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

                EXECUTE IMMEDIATE (v_sql) INTO v_count_tab5;

                exp_table_dump (p_datapump_directory,
                                'I_REBUILD_STAGING',
                                v_where_clause,
                                v_timestamp);

                v_where_clause :=
                    'WHERE event_id NOT IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

                v_sql :=
                    'SELECT COUNT(1) FROM I_AVAIL_SYNC_STAGING WHERE event_id IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

                EXECUTE IMMEDIATE (v_sql) INTO v_count_tab6;

                exp_table_dump (p_datapump_directory,
                                'I_AVAIL_SYNC_STAGING',
                                v_where_clause,
                                v_timestamp);

                v_where_clause :=
                    'WHERE event_id NOT IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

                v_sql :=
                    'SELECT COUNT(1) FROM MASTER_COMMERCE_EVENT WHERE event_id IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

                EXECUTE IMMEDIATE (v_sql) INTO v_count_tab7;

                exp_table_dump (p_datapump_directory,
                                'MASTER_COMMERCE_EVENT',
                                v_where_clause,
                                v_timestamp);

                EXECUTE IMMEDIATE
                    'ALTER TABLE I_AVAIL_INVENTORY_EVENT_DETAIL DISABLE CONSTRAINT I_AVL_INVTRY_EVENT_DTL_FK0';

                EXECUTE IMMEDIATE
                    'ALTER TABLE I_AVAIL_INVENTORY_EVENT DISABLE CONSTRAINT I_AVAIL_INVENTORY_EVENT_FK0';

                EXECUTE IMMEDIATE
                    'ALTER TABLE I_FULFILLMENT_OUTAGE_EVENT DISABLE CONSTRAINT I_FULFILLMENT_OUTAGE_EVENT_FK0';


                SELECT    'ALTER TABLE '
                       || B.TABLE_NAME
                       || ' DISABLE CONSTRAINT '
                       || B.CONSTRAINT_NAME
                  INTO v_temp
                  FROM user_constraints A, user_constraints B
                 WHERE     A.CONSTRAINT_NAME = B.R_CONSTRAINT_NAME
                       AND a.table_name = 'MASTER_COMMERCE_EVENT'
                       AND b.table_name = 'I_REBUILD_STAGING'
                       AND B.CONSTRAINT_TYPE = 'R';

                EXECUTE IMMEDIATE (v_temp);

                SELECT    'ALTER TABLE '
                       || B.TABLE_NAME
                       || ' DISABLE CONSTRAINT '
                       || B.CONSTRAINT_NAME
                  INTO v_temp
                  FROM user_constraints A, user_constraints B
                 WHERE     A.CONSTRAINT_NAME = B.R_CONSTRAINT_NAME
                       AND a.table_name = 'MASTER_COMMERCE_EVENT'
                       AND b.table_name = 'I_AVAIL_SYNC_STAGING'
                       AND B.CONSTRAINT_TYPE = 'R';

                EXECUTE IMMEDIATE (v_temp);

                imp_table_dump (p_datapump_directory,
                                'MASTER_COMMERCE_EVENT',
                                v_timestamp);

                log_archive (NULL,
                             NULL,
                             'MASTER_COMMERCE_EVENT',
                             v_count_tab7,
                             NULL,
                             SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

                imp_table_dump (p_datapump_directory,
                                'I_AVAIL_SYNC_STAGING',
                                v_timestamp);

                SELECT    'ALTER TABLE '
                       || B.TABLE_NAME
                       || ' ENABLE CONSTRAINT '
                       || B.CONSTRAINT_NAME
                  INTO v_temp
                  FROM user_constraints A, user_constraints B
                 WHERE     A.CONSTRAINT_NAME = B.R_CONSTRAINT_NAME
                       AND a.table_name = 'MASTER_COMMERCE_EVENT'
                       AND b.table_name = 'I_AVAIL_SYNC_STAGING'
                       AND B.CONSTRAINT_TYPE = 'R';

                EXECUTE IMMEDIATE (v_temp);

                log_archive (NULL,
                             NULL,
                             'I_AVAIL_SYNC_STAGING',
                             v_count_tab6,
                             NULL,
                             SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

                imp_table_dump (p_datapump_directory,
                                'I_REBUILD_EVENT',
                                v_timestamp);

                log_archive (NULL,
                             NULL,
                             'I_REBUILD_EVENT',
                             v_count_tab5,
                             NULL,
                             SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

                imp_table_dump (p_datapump_directory,
                                'I_REBUILD_STAGING',
                                v_timestamp);

                SELECT    'ALTER TABLE '
                       || B.TABLE_NAME
                       || ' ENABLE CONSTRAINT '
                       || B.CONSTRAINT_NAME
                  INTO v_temp
                  FROM user_constraints A, user_constraints B
                 WHERE     A.CONSTRAINT_NAME = B.R_CONSTRAINT_NAME
                       AND a.table_name = 'MASTER_COMMERCE_EVENT'
                       AND b.table_name = 'I_REBUILD_STAGING'
                       AND B.CONSTRAINT_TYPE = 'R';

                EXECUTE IMMEDIATE (v_temp);

                log_archive (NULL,
                             NULL,
                             'I_REBUILD_STAGING',
                             v_count_tab5,
                             NULL,
                             SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

                imp_table_dump (p_datapump_directory,
                                'I_FULFILLMENT_OUTAGE_EVENT',
                                v_timestamp);

                EXECUTE IMMEDIATE
                    'ALTER TABLE I_FULFILLMENT_OUTAGE_EVENT ENABLE CONSTRAINT I_FULFILLMENT_OUTAGE_EVENT_FK0';

                log_archive (NULL,
                             NULL,
                             'I_FULFILLMENT_OUTAGE_EVENT',
                             v_count_tab3,
                             NULL,
                             SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

                imp_table_dump (p_datapump_directory,
                                'I_AVAIL_INVENTORY_EVENT',
                                v_timestamp);

                EXECUTE IMMEDIATE
                    'ALTER TABLE I_AVAIL_INVENTORY_EVENT ENABLE CONSTRAINT I_AVAIL_INVENTORY_EVENT_FK0';

                log_archive (NULL,
                             NULL,
                             'I_AVAIL_INVENTORY_EVENT',
                             v_count_tab2,
                             NULL,
                             SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

                imp_table_dump (p_datapump_directory,
                                'I_AVAIL_INVENTORY_EVENT_DETAIL',
                                v_timestamp);

                EXECUTE IMMEDIATE
                    'ALTER TABLE I_AVAIL_INVENTORY_EVENT_DETAIL ENABLE CONSTRAINT I_AVL_INVTRY_EVENT_DTL_FK0';

                log_archive (NULL,
                             NULL,
                             'I_AVAIL_INVENTORY_EVENT_DETAIL',
                             v_count_tab1,
                             NULL,
                             SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

                log_archive (
                    NULL,
                    NULL,
                    'Purge_History_Data - MASTER_COMMERCE_EVENT - End',
                    NULL,
                    NULL,
                    SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
            EXCEPTION
                WHEN OTHERS
                THEN
                    log_error (SQLCODE,
                               'Error in Purge_History_Data Procedure');

                    SELECT COUNT (*)
                      INTO v_cons_count
                      FROM user_constraints
                     WHERE     constraint_name = 'I_AVL_INVTRY_EVENT_DTL_FK0'
                           AND status = 'DISABLED';

                    IF v_cons_count = 1
                    THEN
                        EXECUTE IMMEDIATE
                            'ALTER TABLE I_AVAIL_INVENTORY_EVENT_DETAIL ENABLE CONSTRAINT I_AVL_INVTRY_EVENT_DTL_FK0';
                    END IF;

                    v_cons_count := 0;

                    SELECT COUNT (*)
                      INTO v_cons_count
                      FROM user_constraints
                     WHERE     constraint_name =
                                   'I_AVAIL_INVENTORY_EVENT_FK0'
                           AND status = 'DISABLED';

                    IF v_cons_count = 1
                    THEN
                        EXECUTE IMMEDIATE
                            'ALTER TABLE I_AVAIL_INVENTORY_EVENT ENABLE CONSTRAINT I_AVAIL_INVENTORY_EVENT_FK0';
                    END IF;

                    v_cons_count := 0;

                    SELECT COUNT (*)
                      INTO v_cons_count
                      FROM user_constraints
                     WHERE     constraint_name =
                                   'I_FULFILLMENT_OUTAGE_EVENT_FK0'
                           AND status = 'DISABLED';

                    IF v_cons_count = 1
                    THEN
                        EXECUTE IMMEDIATE
                            'ALTER TABLE I_FULFILLMENT_OUTAGE_EVENT ENABLE CONSTRAINT I_FULFILLMENT_OUTAGE_EVENT_FK0';
                    END IF;

                    v_cons_count := 0;

                    SELECT COUNT (*)
                      INTO v_cons_count
                      FROM user_constraints A, user_constraints B
                     WHERE     A.CONSTRAINT_NAME = B.R_CONSTRAINT_NAME
                           AND a.table_name = 'MASTER_COMMERCE_EVENT'
                           AND b.table_name = 'I_REBUILD_STAGING'
                           AND B.CONSTRAINT_TYPE = 'R'
                           AND b.status = 'DISABLED';

                    IF v_cons_count = 1
                    THEN
                        SELECT    'ALTER TABLE '
                               || B.TABLE_NAME
                               || ' ENABLE CONSTRAINT '
                               || B.CONSTRAINT_NAME
                          INTO v_temp
                          FROM user_constraints A, user_constraints B
                         WHERE     A.CONSTRAINT_NAME = B.R_CONSTRAINT_NAME
                               AND a.table_name = 'MASTER_COMMERCE_EVENT'
                               AND b.table_name = 'I_REBUILD_STAGING'
                               AND B.CONSTRAINT_TYPE = 'R';

                        EXECUTE IMMEDIATE (v_temp);
                    END IF;

                    v_cons_count := 0;

                    SELECT COUNT (*)
                      INTO v_cons_count
                      FROM user_constraints A, user_constraints B
                     WHERE     A.CONSTRAINT_NAME = B.R_CONSTRAINT_NAME
                           AND a.table_name = 'MASTER_COMMERCE_EVENT'
                           AND b.table_name = 'I_AVAIL_SYNC_STAGING'
                           AND B.CONSTRAINT_TYPE = 'R'
                           AND b.status = 'DISABLED';

                    IF v_cons_count = 1
                    THEN
                        SELECT    'ALTER TABLE '
                               || B.TABLE_NAME
                               || ' ENABLE CONSTRAINT '
                               || B.CONSTRAINT_NAME
                          INTO v_temp
                          FROM user_constraints A, user_constraints B
                         WHERE     A.CONSTRAINT_NAME = B.R_CONSTRAINT_NAME
                               AND a.table_name = 'MASTER_COMMERCE_EVENT'
                               AND b.table_name = 'I_AVAIL_SYNC_STAGING'
                               AND B.CONSTRAINT_TYPE = 'R';

                        EXECUTE IMMEDIATE (v_temp);
                    END IF;

                    raise_application_error (
                        -20005,
                        'Error in history_purge_data procedure execution.');
            END;
        ELSIF p_table_name = 'I_INV_EVENT_LOG'
        THEN
            BEGIN
                log_archive (NULL,
                             NULL,
                             'Purge_History_Data - I_INV_EVENT_LOG - Begin',
                             NULL,
                             NULL,
                             SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

                QUIET_DROP ('TABLE', 'INV_PURGE_PROC_GTT2');

                BEGIN
                    EXECUTE IMMEDIATE
                        'LOCK TABLE I_INV_EVENT_LOG IN SHARE MODE NOWAIT';

                    EXECUTE IMMEDIATE
                        'LOCK TABLE I_PERPETUAL_INV_LOG IN SHARE MODE NOWAIT';

                    EXECUTE IMMEDIATE
                        'LOCK TABLE I_SEGMENTED_INV_LOG IN SHARE MODE NOWAIT';
                EXCEPTION
                    WHEN OTHERS
                    THEN
                        log_error (SQLCODE,
                                   'Table locked by other transaction');
                        raise_application_error (
                            -20005,
                            'Table locked by other transaction');
                END;

                EXECUTE IMMEDIATE
                       'CREATE TABLE INV_PURGE_PROC_GTT2 TABLESPACE DOM_DT_TBS AS SELECT INV_EVENT_LOG_ID from I_INV_EVENT_LOG
          WHERE CREATED_DTTM < TO_DATE('''
                    || garchivebasedttm
                    || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
                    || p_retention_days;

                v_where_clause :=
                    'WHERE INV_EVENT_LOG_ID NOT IN (SELECT INV_EVENT_LOG_ID FROM INV_PURGE_PROC_GTT2)';

                v_sql :=
                    'SELECT COUNT(1) FROM I_PERPETUAL_INV_LOG WHERE INV_EVENT_LOG_ID IN (SELECT INV_EVENT_LOG_ID FROM INV_PURGE_PROC_GTT2)';

                EXECUTE IMMEDIATE (v_sql) INTO v_count_tab1;

                exp_table_dump (p_datapump_directory,
                                'I_PERPETUAL_INV_LOG',
                                v_where_clause,
                                v_timestamp);
                v_where_clause :=
                    'WHERE INV_EVENT_LOG_ID NOT IN (SELECT INV_EVENT_LOG_ID FROM INV_PURGE_PROC_GTT2)';

                v_sql :=
                    'SELECT COUNT(1) FROM I_SEGMENTED_INV_LOG WHERE INV_EVENT_LOG_ID IN (SELECT INV_EVENT_LOG_ID FROM INV_PURGE_PROC_GTT2)';

                EXECUTE IMMEDIATE (v_sql) INTO v_count_tab2;

                exp_table_dump (p_datapump_directory,
                                'I_SEGMENTED_INV_LOG',
                                v_where_clause,
                                v_timestamp);
                v_where_clause :=
                    'WHERE INV_EVENT_LOG_ID NOT IN (SELECT INV_EVENT_LOG_ID FROM INV_PURGE_PROC_GTT2)';

                v_sql :=
                    'SELECT COUNT(1) FROM I_INV_EVENT_LOG WHERE INV_EVENT_LOG_ID IN (SELECT INV_EVENT_LOG_ID FROM INV_PURGE_PROC_GTT2)';

                EXECUTE IMMEDIATE (v_sql) INTO v_count_tab3;

                exp_table_dump (p_datapump_directory,
                                'I_INV_EVENT_LOG',
                                v_where_clause,
                                v_timestamp);

                EXECUTE IMMEDIATE
                    'ALTER TABLE I_SEGMENTED_INV_LOG DISABLE CONSTRAINT I_SEGMENTED_INV_LOG_FK0';

                EXECUTE IMMEDIATE
                    'ALTER TABLE I_PERPETUAL_INV_LOG DISABLE CONSTRAINT I_PERPETUAL_INV_LOG_FK0';

                imp_table_dump (p_datapump_directory,
                                'I_INV_EVENT_LOG',
                                v_timestamp);

                log_archive (NULL,
                             NULL,
                             'I_INV_EVENT_LOG',
                             v_count_tab3,
                             NULL,
                             SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

                imp_table_dump (p_datapump_directory,
                                'I_SEGMENTED_INV_LOG',
                                v_timestamp);

                EXECUTE IMMEDIATE
                    'ALTER TABLE I_SEGMENTED_INV_LOG ENABLE CONSTRAINT I_SEGMENTED_INV_LOG_FK0';

                log_archive (NULL,
                             NULL,
                             'I_SEGMENTED_INV_LOG',
                             v_count_tab2,
                             NULL,
                             SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

                imp_table_dump (p_datapump_directory,
                                'I_PERPETUAL_INV_LOG',
                                v_timestamp);

                EXECUTE IMMEDIATE
                    'ALTER TABLE I_PERPETUAL_INV_LOG ENABLE CONSTRAINT I_PERPETUAL_INV_LOG_FK0';

                log_archive (NULL,
                             NULL,
                             'I_PERPETUAL_INV_LOG',
                             v_count_tab1,
                             NULL,
                             SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

                log_archive (NULL,
                             NULL,
                             'Purge_History_Data - I_INV_EVENT_LOG - End',
                             NULL,
                             NULL,
                             SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
            EXCEPTION
                WHEN OTHERS
                THEN
                    log_error (SQLCODE,
                               'Error in Purge_History_Data Procedure');

                    SELECT COUNT (*)
                      INTO v_cons_count
                      FROM user_constraints
                     WHERE     constraint_name = 'I_SEGMENTED_INV_LOG_FK0'
                           AND status = 'DISABLED';

                    IF v_cons_count = 1
                    THEN
                        EXECUTE IMMEDIATE
                            'ALTER TABLE I_SEGMENTED_INV_LOG ENABLE CONSTRAINT I_SEGMENTED_INV_LOG_FK0';
                    END IF;

                    v_cons_count := 0;

                    SELECT COUNT (*)
                      INTO v_cons_count
                      FROM user_constraints
                     WHERE     constraint_name = 'I_PERPETUAL_INV_LOG_FK0'
                           AND status = 'DISABLED';

                    IF v_cons_count = 1
                    THEN
                        EXECUTE IMMEDIATE
                            'ALTER TABLE I_PERPETUAL_INV_LOG ENABLE CONSTRAINT I_PERPETUAL_INV_LOG_FK0';
                    END IF;

                    raise_application_error (
                        -20005,
                        'Error in history_purge_data procedure execution.');
            END;
        ELSIF p_table_name = 'MASTER_STAGING_DATA'
        THEN
            BEGIN
                log_archive (
                    NULL,
                    NULL,
                    'Purge_History_Data - MASTER_STAGING_DATA - Begin',
                    NULL,
                    NULL,
                    SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

                quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT');

                BEGIN
                    EXECUTE IMMEDIATE
                        'LOCK TABLE MASTER_STAGING_DATA IN SHARE MODE NOWAIT';

                    EXECUTE IMMEDIATE
                        'LOCK TABLE I_EVENT_DEPENDENCY IN SHARE MODE NOWAIT';

                    EXECUTE IMMEDIATE
                        'LOCK TABLE I_EVENT_STAGING IN SHARE MODE NOWAIT';

                    EXECUTE IMMEDIATE
                        'LOCK TABLE I_SYNC_PROCESS_CONTROL IN SHARE MODE NOWAIT';

                    EXECUTE IMMEDIATE
                        'LOCK TABLE I_SYNC_STAGING IN SHARE MODE NOWAIT';

                    EXECUTE IMMEDIATE
                        'LOCK TABLE I_PLAN_DETAIL_STAGING IN SHARE MODE NOWAIT';

                    EXECUTE IMMEDIATE
                        'LOCK TABLE I_SEGMENT_PLAN_STAGING IN SHARE MODE NOWAIT';
                EXCEPTION
                    WHEN OTHERS
                    THEN
                        log_error (SQLCODE,
                                   'Table locked by other transaction');
                        raise_application_error (
                            -20005,
                            'Table locked by other transaction');
                END;

                EXECUTE IMMEDIATE
                       'CREATE TABLE PURGE_STAGING_EVENT_GTT TABLESPACE DOM_DT_TBS AS SELECT event_id,SYNC_TRANSACTION_ID
            FROM MASTER_STAGING_DATA WHERE EVENT_STATUS IN
                                                 (SELECT SYS_CODE_ID
                                                    FROM SYS_CODE
                                                   WHERE CODE_TYPE = ''I08''
                                                         AND REC_TYPE = ''S''
                                                         AND CODE_DESC IN
                                                                (''Success'',
                                                                 ''Validation Failure'',
                                                                 ''Error''))
                                              AND LAST_UPDATED_DTTM <
                                                     TO_DATE('''
                    || garchivebasedttm
                    || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
                    || p_retention_days;

                v_where_clause :=
                    'WHERE event_id NOT IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT)';

                v_sql :=
                    'SELECT COUNT(1) FROM I_EVENT_DEPENDENCY WHERE event_id IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT)';

                EXECUTE IMMEDIATE (v_sql) INTO v_count_tab1;

                exp_table_dump (p_datapump_directory,
                                'I_EVENT_DEPENDENCY',
                                v_where_clause,
                                v_timestamp);
                v_where_clause :=
                    'WHERE event_id NOT IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT)';

                v_sql :=
                    'SELECT COUNT(1) FROM I_EVENT_STAGING WHERE event_id IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT)';

                EXECUTE IMMEDIATE (v_sql) INTO v_count_tab2;

                exp_table_dump (p_datapump_directory,
                                'I_EVENT_STAGING',
                                v_where_clause,
                                v_timestamp);
                v_where_clause :=
                    'WHERE TRANSACTION_NUMBER NOT IN (SELECT SYNC_TRANSACTION_ID FROM PURGE_STAGING_EVENT_GTT WHERE SYNC_TRANSACTION_ID IS NOT NULL)';

                v_sql :=
                    'SELECT COUNT(1) FROM I_SYNC_PROCESS_CONTROL WHERE TRANSACTION_NUMBER NOT IN (SELECT SYNC_TRANSACTION_ID FROM PURGE_STAGING_EVENT_GTT WHERE SYNC_TRANSACTION_ID IS NOT NULL)';

                EXECUTE IMMEDIATE (v_sql) INTO v_count_tab3;

                exp_table_dump (p_datapump_directory,
                                'I_SYNC_PROCESS_CONTROL',
                                v_where_clause,
                                v_timestamp);
                v_where_clause :=
                    'WHERE TRANSACTION_NUMBER NOT IN (SELECT SYNC_TRANSACTION_ID FROM PURGE_STAGING_EVENT_GTT WHERE SYNC_TRANSACTION_ID IS NOT NULL)';

                v_sql :=
                    'SELECT COUNT(1) FROM I_SYNC_STAGING WHERE TRANSACTION_NUMBER NOT IN (SELECT SYNC_TRANSACTION_ID FROM PURGE_STAGING_EVENT_GTT WHERE SYNC_TRANSACTION_ID IS NOT NULL)';

                EXECUTE IMMEDIATE (v_sql) INTO v_count_tab4;

                exp_table_dump (p_datapump_directory,
                                'I_SYNC_STAGING',
                                v_where_clause,
                                v_timestamp);
                v_where_clause :=
                    'WHERE EVENT_ID NOT IN (SELECT EVENT_ID FROM PURGE_STAGING_EVENT_GTT)';

                v_sql :=
                    'SELECT COUNT(1) FROM I_PLAN_DETAIL_STAGING WHERE EVENT_ID IN (SELECT EVENT_ID FROM PURGE_STAGING_EVENT_GTT)';

                EXECUTE IMMEDIATE (v_sql) INTO v_count_tab5;

                exp_table_dump (p_datapump_directory,
                                'I_PLAN_DETAIL_STAGING',
                                v_where_clause,
                                v_timestamp);
                v_where_clause :=
                    'WHERE EVENT_ID NOT IN (SELECT EVENT_ID FROM PURGE_STAGING_EVENT_GTT)';

                v_sql :=
                    'SELECT COUNT(1) FROM I_SEGMENT_PLAN_STAGING WHERE EVENT_ID IN (SELECT EVENT_ID FROM PURGE_STAGING_EVENT_GTT)';

                EXECUTE IMMEDIATE (v_sql) INTO v_count_tab6;

                exp_table_dump (p_datapump_directory,
                                'I_SEGMENT_PLAN_STAGING',
                                v_where_clause,
                                v_timestamp);
                v_where_clause :=
                    'WHERE EVENT_ID NOT IN (SELECT EVENT_ID FROM PURGE_STAGING_EVENT_GTT)';

                v_sql :=
                    'SELECT COUNT(1) FROM MASTER_STAGING_DATA WHERE EVENT_ID IN (SELECT EVENT_ID FROM PURGE_STAGING_EVENT_GTT)';

                EXECUTE IMMEDIATE (v_sql) INTO v_count_tab7;

                exp_table_dump (p_datapump_directory,
                                'MASTER_STAGING_DATA',
                                v_where_clause,
                                v_timestamp);

                EXECUTE IMMEDIATE
                    'ALTER TABLE I_SEGMENT_PLAN_STAGING DISABLE CONSTRAINT I_SEGMENT_PLAN_FK0';

                EXECUTE IMMEDIATE
                    'ALTER TABLE I_EVENT_STAGING DISABLE CONSTRAINT I_EVENT_STAGING_FK0';

                EXECUTE IMMEDIATE
                    'ALTER TABLE I_EVENT_DEPENDENCY DISABLE CONSTRAINT FK_I_EVENT_DEPENDENCY';

                imp_table_dump (p_datapump_directory,
                                'MASTER_STAGING_DATA',
                                v_timestamp);

                log_archive (NULL,
                             NULL,
                             'MASTER_STAGING_DATA',
                             v_count_tab7,
                             NULL,
                             SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

                imp_table_dump (p_datapump_directory,
                                'I_SEGMENT_PLAN_STAGING',
                                v_timestamp);

                EXECUTE IMMEDIATE
                    'ALTER TABLE I_SEGMENT_PLAN_STAGING ENABLE CONSTRAINT I_SEGMENT_PLAN_FK0';

                log_archive (NULL,
                             NULL,
                             'I_SEGMENT_PLAN_STAGING',
                             v_count_tab6,
                             NULL,
                             SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

                imp_table_dump (p_datapump_directory,
                                'I_PLAN_DETAIL_STAGING',
                                v_timestamp);

                log_archive (NULL,
                             NULL,
                             'I_PLAN_DETAIL_STAGING',
                             v_count_tab5,
                             NULL,
                             SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

                imp_table_dump (p_datapump_directory,
                                'I_SYNC_STAGING',
                                v_timestamp);

                log_archive (NULL,
                             NULL,
                             'I_SYNC_STAGING',
                             v_count_tab4,
                             NULL,
                             SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

                imp_table_dump (p_datapump_directory,
                                'I_SYNC_PROCESS_CONTROL',
                                v_timestamp);

                log_archive (NULL,
                             NULL,
                             'I_SYNC_PROCESS_CONTROL',
                             v_count_tab3,
                             NULL,
                             SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

                imp_table_dump (p_datapump_directory,
                                'I_EVENT_STAGING',
                                v_timestamp);

                EXECUTE IMMEDIATE
                    'ALTER TABLE I_EVENT_STAGING ENABLE CONSTRAINT I_EVENT_STAGING_FK0';

                log_archive (NULL,
                             NULL,
                             'I_EVENT_STAGING',
                             v_count_tab2,
                             NULL,
                             SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

                imp_table_dump (p_datapump_directory,
                                'I_EVENT_DEPENDENCY',
                                v_timestamp);

                EXECUTE IMMEDIATE
                    'ALTER TABLE I_EVENT_DEPENDENCY ENABLE CONSTRAINT FK_I_EVENT_DEPENDENCY';

                log_archive (NULL,
                             NULL,
                             'I_EVENT_DEPENDENCY',
                             v_count_tab1,
                             NULL,
                             SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

                log_archive (
                    NULL,
                    NULL,
                    'Purge_History_Data - MASTER_STAGING_DATA - End',
                    NULL,
                    NULL,
                    SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
            EXCEPTION
                WHEN OTHERS
                THEN
                    log_error (SQLCODE,
                               'Error in Purge_History_Data Procedure');

                    SELECT COUNT (*)
                      INTO v_cons_count
                      FROM user_constraints
                     WHERE     constraint_name = 'I_SEGMENT_PLAN_FK0'
                           AND status = 'DISABLED';

                    IF v_cons_count = 1
                    THEN
                        EXECUTE IMMEDIATE
                            'ALTER TABLE I_SEGMENT_PLAN_STAGING ENABLE CONSTRAINT I_SEGMENT_PLAN_FK0';
                    END IF;

                    v_cons_count := 0;

                    SELECT COUNT (*)
                      INTO v_cons_count
                      FROM user_constraints
                     WHERE     constraint_name = 'I_EVENT_STAGING_FK0'
                           AND status = 'DISABLED';

                    IF v_cons_count = 1
                    THEN
                        EXECUTE IMMEDIATE
                            'ALTER TABLE I_EVENT_STAGING ENABLE CONSTRAINT I_EVENT_STAGING_FK0';
                    END IF;

                    v_cons_count := 0;

                    SELECT COUNT (*)
                      INTO v_cons_count
                      FROM user_constraints
                     WHERE     constraint_name = 'FK_I_EVENT_DEPENDENCY'
                           AND status = 'DISABLED';

                    IF v_cons_count = 1
                    THEN
                        EXECUTE IMMEDIATE
                            'ALTER TABLE I_EVENT_DEPENDENCY ENABLE CONSTRAINT FK_I_EVENT_DEPENDENCY';
                    END IF;

                    raise_application_error (
                        -20005,
                        'Error in history_purge_data procedure execution.');
            END;
        ELSE
            RAISE v_notsupported;
        END IF;
    EXCEPTION
        WHEN v_notsupported
        THEN
            log_error (SQLCODE, 'Purge_History_Data - Not Supported Table');
            raise_application_error (
                -20005,
                   'Not Supported for Table - '
                || p_table_name
                || '. Supported only for I_AVAIL_INVENTORY_EVENT, MASTER_COMMERCE_EVENT, MASTER_STAGING_DATA, I_INV_EVENT_LOG tables.');
        WHEN OTHERS
        THEN
            log_error (SQLCODE, 'Error in Purge_History_Data Procedure');
            raise_application_error (
                -20005,
                'Error in history_purge_data procedure execution.');
    END purge_history_data;


    PROCEDURE inv_bussiness_rules_purge_proc
    AS
    BEGIN
        gpurge_days := get_config_days ('INVENTORY_BUSS');
        gvprocedurename := 'Purge_Pkg.INV_Bussiness_Rules_Purge_Proc - Begin';
        log_archive (gpurge_group,
                     gpurge_days,
                     gvprocedurename,
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

        quiet_drop ('TABLE', 'INV_BUSSINESS_RULES_PURGE_GTT');

        EXECUTE IMMEDIATE
               'CREATE TABLE INV_BUSSINESS_RULES_PURGE_GTT TABLESPACE DOM_DT_TBS AS SELECT A_identity FROM A_PROCESS_TEMPLATE WHERE (IS_DELETED = 1
            AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ') OR (EXPIRY_DATE < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')';


        INSERT_DEL_DATA_BULK (
            'A_ALLOC_GROUP_PARAM',
            '',
            '',
            '',
            'DOM_PROCESS_TEMPLATE_ID IN (SELECT  A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');


        INSERT_DEL_DATA_BULK (
            'A_DO_ATTR_TEMPLATE_VALUES',
            '',
            '',
            '',
            'TEMPLATE_ID IN (SELECT  A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'A_DO_MAX_SIZE',
            '',
            '',
            '',
            'A_PROCESS_TEMPLATE_ID IN (SELECT  A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'A_SEGMENTED_INVENTORY',
            '',
            '',
            '',
            'DOM_PROCESS_TEMPLATE_ID IN (SELECT  A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'A_SEGMENT_TEMPLATE_PARAM',
            '',
            '',
            '',
            'DOM_PROCESS_TEMPLATE_ID IN (SELECT  A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'A_SO_OUTBOUND_CHG_MGMT',
            '',
            '',
            '',
            'A_PROCESS_TEMPLATE_ID IN (SELECT  A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'A_PROC_TMPL_FILTER',
            '',
            '',
            '',
            'DOM_PROCESS_TEMPLATE_ID IN (SELECT  A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'RLM_FEE',
            '',
            '',
            '',
            'TEMPLATE_ID IN (SELECT  A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'A_BAND_DETAILS',
            '',
            '',
            '',
            'ALLOC_FULFILL_PARAM_ID IN (select ALLOC_FULFILL_PARAM_ID from A_ALLOC_FULFILL_PARAM where DOM_PROCESS_TEMPLATE_ID IN (SELECT  A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT))',
            'N');

        INSERT_DEL_DATA_BULK (
            'A_TIER_FACILITY_RANK',
            '',
            '',
            '',
            'ALLOC_FULFILL_PARAM_ID IN (select ALLOC_FULFILL_PARAM_ID from A_ALLOC_FULFILL_PARAM where DOM_PROCESS_TEMPLATE_ID IN (SELECT  A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT))',
            'N');

        INSERT_DEL_DATA_BULK (
            'A_ALLOC_RULE_SEGEMENT',
            '',
            '',
            '',
            'ALLOC_FULFILL_PARAM_ID IN (select ALLOC_FULFILL_PARAM_ID from A_ALLOC_FULFILL_PARAM where DOM_PROCESS_TEMPLATE_ID IN (SELECT  A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT))',
            'N');

        INSERT_DEL_DATA_BULK (
            'A_ALLOC_FULFILL_PARAM',
            '',
            '',
            '',
            'DOM_PROCESS_TEMPLATE_ID IN (SELECT  A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'A_ALLOC_GROUP_PARAM',
            '',
            '',
            '',
            'DOM_PROCESS_TEMPLATE_ID IN (SELECT  A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');


        INSERT_DEL_DATA_BULK (
            'A_ALLOC_TIER_PARAM',
            '',
            '',
            '',
            'DOM_PROCESS_TEMPLATE_ID IN (SELECT  A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'A_RANKING_SEQUENCE',
            '',
            '',
            '',
            'DOM_PROCESS_TEMPLATE_ID IN (SELECT  A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'A_PROCESS_TEMPLATE',
            '',
            '',
            '',
            'A_identity IN (SELECT  A_identity FROM INV_BUSSINESS_RULES_PURGE_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'WF_FAILED_ENTITY',
            '',
            '',
            '',
               'CREATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        quiet_drop ('TABLE', 'INV_BUSSINESS_RULES_PURGE_GTT');
        gvprocedurename := 'Purge_Pkg.INV_Bussiness_Rules_Purge_Proc - End';
        log_archive (gpurge_group,
                     gpurge_days,
                     gvprocedurename,
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
    EXCEPTION
        WHEN nodatafound
        THEN
            RAISE noparameter;
        WHEN OTHERS
        THEN
            nsqlcode := SQLCODE;
            vsqlerrm := SQLERRM;
            RAISE gdelete_error;
    END inv_bussiness_rules_purge_proc;

    PROCEDURE commerce_purge_proc
    AS
    BEGIN
        gpurge_days := get_config_days ('INVENTORY_COMM');
        gvprocedurename := 'Purge_Pkg.Commerce_Purge_Proc - Begin';
        log_archive (gpurge_group,
                     gpurge_days,
                     gvprocedurename,
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

        quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT');
        quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT1');
        quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT2');
        quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT3');
        quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT4');
        quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT5');

        EXECUTE IMMEDIATE
               'CREATE TABLE COMMERCE_PURGE_GTT TABLESPACE DOM_DT_TBS AS SELECT VIEW_RULESET_ID FROM I_VIEW_RULESET WHERE IS_DELETED = 1
            AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days;

        EXECUTE IMMEDIATE
               'CREATE TABLE COMMERCE_PURGE_GTT1 TABLESPACE DOM_DT_TBS AS SELECT CONS_TEMPLATE_ID FROM CONS_TEMPLATE WHERE FILTER_ID IN
        (SELECT FILTER_ID FROM filter WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')';

        EXECUTE IMMEDIATE
               'CREATE TABLE COMMERCE_PURGE_GTT2 TABLESPACE DOM_DT_TBS AS SELECT FILTER_ID FROM filter WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days;

        EXECUTE IMMEDIATE
               'CREATE TABLE COMMERCE_PURGE_GTT3 TABLESPACE DOM_DT_TBS AS SELECT VIEW_DEF_ID FROM I_VIEW_DEF WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days;

        EXECUTE IMMEDIATE
               'CREATE TABLE COMMERCE_PURGE_GTT4 TABLESPACE DOM_DT_TBS AS SELECT EVENT_ID FROM MASTER_COMMERCE_EVENT WHERE LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days;

        EXECUTE IMMEDIATE
            'CREATE TABLE COMMERCE_PURGE_GTT5 TABLESPACE DOM_DT_TBS AS SELECT AVAIL_INV_EVENT_ID FROM I_AVAIL_INVENTORY_EVENT WHERE EVENT_ID IN (SELECT EVENT_ID FROM COMMERCE_PURGE_GTT4)';

        INSERT_DEL_DATA_BULK (
            'I_ASSOCIATED_AVAILABILITY',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_1',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_1 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_2',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_2 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_3',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_3 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_4',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_4 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_5',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_5 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_6',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_6 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_7',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_7 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_8',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_8 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_9',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_9 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_10',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_10 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_11',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_11 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_12',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_12 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_13',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_13 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_14',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_14 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_15',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_15 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_16',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_16 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_17',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_17 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_18',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_18 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_19',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_19 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_20',
            '',
            '',
            '',
               'AVAILABILITY_ID in (SELECT AVAILABILITY_ID FROM I_AVAILABILITY_20 WHERE IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_1',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_2',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_3',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_4',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_5',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_6',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_7',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_8',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_9',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_10',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_11',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_12',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_13',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_14',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_15',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_16',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_17',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_18',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_19',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_20',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_EXCLUSION',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_VIEW_RULESET_FILTER',
            '',
            '',
            '',
            'VIEW_RULESET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_1',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_1 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_2',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_2 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_3',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_3 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_4',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_4 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_5',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_5 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_6',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_6 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_7',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_7 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_8',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_8 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_9',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_9 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_10',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_10 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_11',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_11 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_12',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_12 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_13',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_13 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_14',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_14 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_15',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_15 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_16',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_16 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_17',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_17 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_18',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_18 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_19',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_19 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_20',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_20 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');

        INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_1',
                              '',
                              '',
                              '',
                              'MARK_FOR_DELETE = 1',
                              'N');
        INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_2',
                              '',
                              '',
                              '',
                              'MARK_FOR_DELETE = 1',
                              'N');
        INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_3',
                              '',
                              '',
                              '',
                              'MARK_FOR_DELETE = 1',
                              'N');
        INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_4',
                              '',
                              '',
                              '',
                              'MARK_FOR_DELETE = 1',
                              'N');
        INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_5',
                              '',
                              '',
                              '',
                              'MARK_FOR_DELETE = 1',
                              'N');
        INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_6',
                              '',
                              '',
                              '',
                              'MARK_FOR_DELETE = 1',
                              'N');
        INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_7',
                              '',
                              '',
                              '',
                              'MARK_FOR_DELETE = 1',
                              'N');
        INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_8',
                              '',
                              '',
                              '',
                              'MARK_FOR_DELETE = 1',
                              'N');
        INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_9',
                              '',
                              '',
                              '',
                              'MARK_FOR_DELETE = 1',
                              'N');
        INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_10',
                              '',
                              '',
                              '',
                              'MARK_FOR_DELETE = 1',
                              'N');
        INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_11',
                              '',
                              '',
                              '',
                              'MARK_FOR_DELETE = 1',
                              'N');
        INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_12',
                              '',
                              '',
                              '',
                              'MARK_FOR_DELETE = 1',
                              'N');
        INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_13',
                              '',
                              '',
                              '',
                              'MARK_FOR_DELETE = 1',
                              'N');
        INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_14',
                              '',
                              '',
                              '',
                              'MARK_FOR_DELETE = 1',
                              'N');
        INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_15',
                              '',
                              '',
                              '',
                              'MARK_FOR_DELETE = 1',
                              'N');
        INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_16',
                              '',
                              '',
                              '',
                              'MARK_FOR_DELETE = 1',
                              'N');
        INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_17',
                              '',
                              '',
                              '',
                              'MARK_FOR_DELETE = 1',
                              'N');
        INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_18',
                              '',
                              '',
                              '',
                              'MARK_FOR_DELETE = 1',
                              'N');
        INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_19',
                              '',
                              '',
                              '',
                              'MARK_FOR_DELETE = 1',
                              'N');
        INSERT_DEL_DATA_BULK ('I_AVAILABILITY_EXCLUSION_20',
                              '',
                              '',
                              '',
                              'MARK_FOR_DELETE = 1',
                              'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_1',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_1 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_2',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_2 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_3',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_3 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_4',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_4 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_5',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_5 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_6',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_6 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_7',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_7 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_8',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_8 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_9',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_9 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_10',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_10 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_11',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_11 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_12',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_12 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_13',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_13 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_14',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_14 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_15',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_15 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_16',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_16 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_17',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_17 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_18',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_18 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_19',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_19 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_20',
            '',
            '',
            '',
            '(AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_20 WHERE RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)))',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_1',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_2',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_3',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_4',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_5',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_6',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_7',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_8',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_9',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_10',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_11',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_12',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_13',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_14',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_15',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_16',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_17',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_18',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_19',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_20',
            '',
            '',
            '',
               'STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC =''Success'') AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_1',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_2',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_3',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_4',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_5',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_6',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_7',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_8',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_9',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_10',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_11',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_12',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_13',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_14',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_15',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_16',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_17',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_18',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_19',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_20',
            '',
            '',
            '',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_1_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_2_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_3_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_4_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_5_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_6_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_7_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_8_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_9_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_10_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_11_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_12_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_13_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_14_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_15_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_16_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_17_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_18_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_19_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_20_PRV',
            ' ',
            ' ',
            ' ',
            'RULE_SET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_VIEW_RULESET',
            '',
            '',
            '',
            'VIEW_RULESET_ID IN (SELECT  VIEW_RULESET_ID FROM COMMERCE_PURGE_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'CONSTEMP_DEST_REGION_MAP',
            '',
            '',
            '',
            'CONS_TEMPLATE_ID IN (SELECT  CONS_TEMPLATE_ID FROM COMMERCE_PURGE_GTT1)',
            'N');

        INSERT_DEL_DATA_BULK (
            'CONSTEMP_ORIG_REGION_MAP',
            '',
            '',
            '',
            'CONS_TEMPLATE_ID IN (SELECT  CONS_TEMPLATE_ID FROM COMMERCE_PURGE_GTT1)',
            'N');

        INSERT_DEL_DATA_BULK (
            'CONSTEMP_PRODCLASS_MAP',
            '',
            '',
            '',
            'CONS_TEMPLATE_ID IN (SELECT  CONS_TEMPLATE_ID FROM COMMERCE_PURGE_GTT1)',
            'N');

        INSERT_DEL_DATA_BULK (
            'CONS_OPT_RULE',
            '',
            '',
            '',
            'CONS_TEMPLATE_ID IN (SELECT  CONS_TEMPLATE_ID FROM COMMERCE_PURGE_GTT1)',
            'N');

        INSERT_DEL_DATA_BULK (
            'CONS_TEMPLATE_RES_DRIVER',
            '',
            '',
            '',
            'CONS_TEMPLATE_ID IN (SELECT  CONS_TEMPLATE_ID FROM COMMERCE_PURGE_GTT1)',
            'N');

        INSERT_DEL_DATA_BULK (
            'CONS_TEMPLATE_RES_EQUIP',
            '',
            '',
            '',
            'CONS_TEMPLATE_ID IN (SELECT  CONS_TEMPLATE_ID FROM COMMERCE_PURGE_GTT1)',
            'N');

        INSERT_DEL_DATA_BULK (
            'CONS_TEMPLATE',
            '',
            '',
            '',
            'FILTER_ID IN (SELECT  FILTER_ID FROM COMMERCE_PURGE_GTT2)',
            'N');

        INSERT_DEL_DATA_BULK (
            'A_PROC_TMPL_FILTER',
            '',
            '',
            '',
            'FILTER_ID IN (SELECT  FILTER_ID FROM COMMERCE_PURGE_GTT2)',
            'N');

        INSERT_DEL_DATA_BULK (
            'CONTACT_FILTER_MAP',
            '',
            '',
            '',
            'FILTER_ID IN (SELECT  FILTER_ID FROM COMMERCE_PURGE_GTT2)',
            'N');


        INSERT_DEL_DATA_BULK (
            'FILTER_CRITERIA',
            '',
            '',
            '',
            'FILTER_ID IN (SELECT  FILTER_ID FROM COMMERCE_PURGE_GTT2)',
            'N');

        INSERT_DEL_DATA_BULK (
            'FILTER_GROUPBY_DETAIL',
            '',
            '',
            '',
            'FILTER_ID IN (SELECT  FILTER_ID FROM COMMERCE_PURGE_GTT2)',
            'N');

        INSERT_DEL_DATA_BULK (
            'FILTER_GROUP_BY',
            '',
            '',
            '',
            'FILTER_ID IN (SELECT  FILTER_ID FROM COMMERCE_PURGE_GTT2)',
            'N');

        INSERT_DEL_DATA_BULK (
            'FILTER_GROUP_DEFINITION',
            '',
            '',
            '',
            'FILTER_ID IN (SELECT  FILTER_ID FROM COMMERCE_PURGE_GTT2)',
            'N');

        INSERT_DEL_DATA_BULK (
            'FILTER_ORDERBY_DETAIL',
            '',
            '',
            '',
            'FILTER_ID IN (SELECT  FILTER_ID FROM COMMERCE_PURGE_GTT2)',
            'N');

        INSERT_DEL_DATA_BULK (
            'FILTER_DETAIL',
            '',
            '',
            '',
            'FILTER_ID IN (SELECT  FILTER_ID FROM COMMERCE_PURGE_GTT2)',
            'N');

        INSERT_DEL_DATA_BULK (
            'FILTER',
            '',
            '',
            '',
            'FILTER_ID IN (SELECT  FILTER_ID FROM COMMERCE_PURGE_GTT2)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ALERT_STAGING',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_FUL_OUTAGE_RULE',
            '',
            '',
            '',
               '(IS_DELETED = 1 OR status =  (SELECT SYS_CODE_ID FROM SYS_CODE WHERE REC_TYPE = ''S'' AND CODE_TYPE = ''I23'' AND CODE_ID = ''Expired'')) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');

        --2016 OLM STARTS
        INSERT_DEL_DATA_BULK (
            'I_COMMERCE_STATS_EVENT_ERROR',
            '',
            '',
            '',
               'COMMERCE_VIEW_STATS_EVENT_ID IN (SELECT COMMERCE_VIEW_STATS_EVENT_ID FROM I_COMMERCE_VIEW_STATS_EVENT WHERE LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_COMMERCE_VIEW_STATS_EVENT',
            '',
            '',
            '',
               'LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        --2016 OLM ENDS

        INSERT_DEL_DATA_BULK (
            'I_COMMERCE_EVENT',
            '',
            '',
            '',
               'EVENT_ID IN (SELECT  EVENT_ID FROM MASTER_COMMERCE_EVENT WHERE LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAIL_INVENTORY_EVENT_DETAIL',
            '',
            '',
            '',
            'AVAIL_INV_EVENT_ID IN (SELECT AVAIL_INV_EVENT_ID FROM COMMERCE_PURGE_GTT5)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAIL_INVENTORY_EVENT',
            '',
            '',
            '',
            'EVENT_ID IN (SELECT EVENT_ID FROM COMMERCE_PURGE_GTT4)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_FULFILLMENT_OUTAGE_EVENT',
            '',
            '',
            '',
            'EVENT_ID IN (SELECT EVENT_ID FROM COMMERCE_PURGE_GTT4)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_VIEW_OUTAGE_REASON',
            '',
            '',
            '',
            'VIEW_CONFIG_ID IN (SELECT VIEW_CONFIG_ID FROM I_VIEW_CONFIG WHERE VIEW_DEF_ID IN (SELECT  VIEW_DEF_ID FROM COMMERCE_PURGE_GTT3))',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_VIEW_SYNC_DETAIL',
            '',
            '',
            '',
            'VIEW_CONFIG_ID IN (SELECT VIEW_CONFIG_ID FROM I_VIEW_CONFIG WHERE VIEW_DEF_ID IN (SELECT  VIEW_DEF_ID FROM COMMERCE_PURGE_GTT3))',
            'N');


        INSERT_DEL_DATA_BULK (
            'I_VIEW_AVAIL_RULE_DETAIL',
            '',
            '',
            '',
            ' VIEW_AVAIL_RULE_ID IN (SELECT VIEW_AVAIL_RULE_ID FROM I_VIEW_AVAIL_RULE WHERE VIEW_CONFIG_ID IN (SELECT VIEW_CONFIG_ID 
                                              FROM I_VIEW_CONFIG WHERE VIEW_DEF_ID IN (SELECT  VIEW_DEF_ID FROM COMMERCE_PURGE_GTT3)))',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_VIEW_AVAIL_RULE',
            '',
            '',
            '',
            ' VIEW_CONFIG_ID IN (SELECT VIEW_CONFIG_ID FROM I_VIEW_CONFIG WHERE VIEW_DEF_ID IN (SELECT  VIEW_DEF_ID FROM COMMERCE_PURGE_GTT3))',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_VIEW_STORE_EXCLUSION',
            '',
            '',
            '',
            ' VIEW_CONFIG_ID IN (SELECT VIEW_CONFIG_ID FROM I_VIEW_CONFIG WHERE VIEW_DEF_ID IN (SELECT  VIEW_DEF_ID FROM COMMERCE_PURGE_GTT3))',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_VIEW_NETWORK_PROTECTION',
            '',
            '',
            '',
            ' VIEW_CONFIG_ID IN (SELECT VIEW_CONFIG_ID FROM I_VIEW_CONFIG WHERE VIEW_DEF_ID IN (SELECT  VIEW_DEF_ID FROM COMMERCE_PURGE_GTT3))',
            'N');


        INSERT_DEL_DATA_BULK (
            'I_VIEW_METADATA_MAPPING',
            '',
            '',
            '',
            'VIEW_DEF_ID IN (SELECT  VIEW_DEF_ID FROM COMMERCE_PURGE_GTT3)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_REBUILD_STAGING',
            '',
            '',
            '',
            'VIEW_DEF_ID IN (SELECT VIEW_DEF_ID FROM COMMERCE_PURGE_GTT3)',
            'N');

         INSERT_DEL_DATA_BULK (
            'I_AVAIL_SYNC_STAGING',
            '',
            '',
            '',
            'VIEW_DEF_ID IN (SELECT VIEW_DEF_ID FROM COMMERCE_PURGE_GTT3)',
            'N');				
			
        INSERT_DEL_DATA_BULK (
            'I_VIEW_CONFIG',
            '',
            '',
            '',
            'VIEW_DEF_ID IN (SELECT  VIEW_DEF_ID FROM COMMERCE_PURGE_GTT3)',
            'N');


        INSERT_DEL_DATA_BULK (
            'I_VIEW_DEF_THRESHOLD',
            '',
            '',
            '',
            'VIEW_DEF_ID IN (SELECT  VIEW_DEF_ID FROM COMMERCE_PURGE_GTT3)',
            'N');


        INSERT_DEL_DATA_BULK (
            'I_VIEW_DEF',
            '',
            '',
            '',
            'VIEW_DEF_ID IN (SELECT  VIEW_DEF_ID FROM COMMERCE_PURGE_GTT3)',
            'N');

        quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT');
        quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT1');
        quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT2');
        quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT3');
        quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT4');
        quiet_drop ('TABLE', 'COMMERCE_PURGE_GTT5');
        gvprocedurename := 'Purge_Pkg.Commerce_Purge_Proc - End';
        log_archive (gpurge_group,
                     gpurge_days,
                     gvprocedurename,
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
    EXCEPTION
        WHEN nodatafound
        THEN
            RAISE noparameter;
        WHEN OTHERS
        THEN
            nsqlcode := SQLCODE;
            vsqlerrm := SQLERRM;
            RAISE gdelete_error;
    END commerce_purge_proc;

    PROCEDURE inv_purge_proc
    AS
    BEGIN
        gpurge_days := get_config_days ('INVENTORY_INV');
        gvprocedurename := 'Purge_Pkg.INV_Purge_Proc - Begin';
        log_archive (gpurge_group,
                     gpurge_days,
                     gvprocedurename,
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

        quiet_drop ('TABLE', 'INV_PURGE_PROC_GTT');
        quiet_drop ('TABLE', 'INV_PURGE_PROC_GTT1');
        quiet_drop ('TABLE', 'INV_PURGE_PROC_GTT2');

        EXECUTE IMMEDIATE
               'CREATE TABLE INV_PURGE_PROC_GTT TABLESPACE DOM_DT_TBS AS SELECT I_SEGMENTED.INVENTORY_ID FROM I_SEGMENTED
         LEFT OUTER JOIN I_ALLOCATION IA on IA.INVENTORY_ID = I_SEGMENTED.INVENTORY_ID
        WHERE (OBJECT_TYPE_ID IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I10'' AND REC_TYPE = ''S'' AND SHORT_DESC=''OH'') AND IS_DELETED  =1) OR (OBJECT_TYPE_ID IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I10'' AND REC_TYPE = ''S'' AND SHORT_DESC=''OH'')
        AND (AVAILABLE_QUANTITY =0 OR AVAILABLE_QUANTITY IS NULL) AND (AVAILABLE_SOON_QUANTITY =0 OR AVAILABLE_SOON_QUANTITY IS NULL) AND (UNAVAILABLE_QUANTITY =0 OR UNAVAILABLE_QUANTITY IS NULL)
        AND (IA.ALLOCATED_QUANTITY =0 OR IA.ALLOCATED_QUANTITY IS NULL) AND (IA.DO_QUANTITY =0 OR IA.DO_QUANTITY IS NULL)
        AND I_SEGMENTED.LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')';

        EXECUTE IMMEDIATE
               'CREATE TABLE INV_PURGE_PROC_GTT1 TABLESPACE DOM_DT_TBS AS SELECT IP.INVENTORY_ID FROM I_PERPETUAL IP
         LEFT OUTER JOIN I_ALLOCATION IA on IA.INVENTORY_ID = IP.INVENTORY_ID
        WHERE (OBJECT_TYPE_ID IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I10'' AND REC_TYPE = ''S'' AND SHORT_DESC=''OH'') AND IS_DELETED  =1) OR (OBJECT_TYPE_ID IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I10'' AND REC_TYPE = ''S'' AND SHORT_DESC=''OH'')
        AND (AVAILABLE_QUANTITY =0 OR AVAILABLE_QUANTITY IS NULL) AND (AVAILABLE_SOON_QUANTITY =0 OR AVAILABLE_SOON_QUANTITY IS NULL) AND (UNAVAILABLE_QUANTITY =0 OR UNAVAILABLE_QUANTITY IS NULL)
        AND (IA.ALLOCATED_QUANTITY =0 OR IA.ALLOCATED_QUANTITY IS NULL) AND (IA.DO_QUANTITY =0 OR IA.DO_QUANTITY IS NULL)
        AND IP.LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')';

        EXECUTE IMMEDIATE
               'CREATE TABLE INV_PURGE_PROC_GTT2 TABLESPACE DOM_DT_TBS AS SELECT INV_EVENT_LOG_ID from I_INV_EVENT_LOG
          WHERE CREATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days;


        INSERT_DEL_DATA_BULK (
            'I_ASSOCIATED_AVAILABILITY',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_1',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_1 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_2',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_2 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_3',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_3 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_4',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_4 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_5',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_5 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_6',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_6 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_7',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_7 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_8',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_8 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_9',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_9 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_10',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_10 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_11',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_11 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_12',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_12 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_13',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_13 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_14',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_14 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_15',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_15 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_16',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_16 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_17',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_17 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_18',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_18 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_19',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_19 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_20',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_20 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_1',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_1 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_2',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_2 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_3',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_3 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_4',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_4 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_5',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_5 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_6',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_6 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_7',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_7 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_8',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_8 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_9',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_9 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_10',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_10 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_11',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_11 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_12',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_12 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_13',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_13 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_14',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_14 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_15',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_15 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_16',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_16 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_17',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_17 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_18',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_18 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_19',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_19 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_20',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_20 WHERE SEGMENTED_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT))',
            'N');


        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_1',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_2',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_3',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_4',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_5',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_6',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_7',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_8',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_9',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_10',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_11',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_12',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_13',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_14',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_15',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_16',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_17',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_18',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_19',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_20',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_1',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_2',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_3',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_4',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_5',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_6',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_7',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_8',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_9',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_10',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_11',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_12',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_13',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_14',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_15',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_16',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_17',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_18',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_19',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_20',
            '',
            '',
            '',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_ALLOCATION',
            '',
            '',
            '',
            'SEGMENTED_INVENTORY_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_INVENTORY_EVENT_DETAIL',
            '',
            '',
            '',
            'SEGMENTED_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_SEGMENTED_INV_LOG',
            '',
            '',
            '',
            'INVENTORY_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_ASSOCIATED_AVAILABILITY',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_1',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_1 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_2',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_2 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_3',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_3 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_4',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_4 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_5',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_5 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_6',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_6 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_7',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_7 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_8',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_8 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_9',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_9 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_10',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_10 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_11',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_11 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_12',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_12 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_13',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_13 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_14',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_14 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_15',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_15 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_16',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_16 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_17',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_17 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_18',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_18 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_19',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_19 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_EXCLUSION_20',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_20 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_1',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_1 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_2',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_2 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_3',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_3 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_4',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_4 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_5',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_5 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_6',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_6 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_7',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_7 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_8',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_8 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_9',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_9 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_10',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_10 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_11',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_11 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_12',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_12 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_13',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_13 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_14',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_14 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_15',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_15 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_16',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_16 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_17',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_17 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_18',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_18 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_19',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_19 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_20',
            '',
            '',
            '',
            'AVAIL_BY_INV_ID IN (SELECT IDENTITY FROM I_AVAIL_BY_INV_20 WHERE PERPETUAL_INV_ID in (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1))',
            'N');


        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_1',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_2',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_3',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_4',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_5',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_6',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_7',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_8',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_9',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_10',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_11',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_12',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_13',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_14',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_15',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_16',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_17',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_18',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_19',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_20',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_1',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_2',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_3',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_4',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_5',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_6',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_7',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_8',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_9',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_10',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_11',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_12',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_13',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_14',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_15',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_16',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_17',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_18',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_19',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_20',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_1_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_2_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_3_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_4_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_5_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_6_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_7_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_8_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_9_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_10_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_11_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_12_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_13_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_14_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_15_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_16_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_17_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_18_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_19_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_20_PRV',
            ' ',
            ' ',
            ' ',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_1_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_2_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_3_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_4_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_5_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_6_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_7_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_8_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_9_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_10_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_11_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_12_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_13_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_14_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_15_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_16_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_17_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_18_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_19_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_BY_INV_20_PRV',
            ' ',
            ' ',
            ' ',
            'SEGMENTED_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_ALLOCATION',
            '',
            '',
            '',
            'INVENTORY_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_INVENTORY_EVENT_DETAIL',
            '',
            '',
            '',
            'PERPETUAL_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_EXCLUSION',
            '',
            '',
            '',
            'PERPETUAL_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_PERPETUAL_INV_LOG',
            '',
            '',
            '',
            'INVENTORY_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_ONHAND_MOVEMENT',
            '',
            '',
            '',
            'INVENTORY_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');

        -- DOM-22217
        INSERT_DEL_DATA_BULK (
            'I_PERPETUAL_INV_LOG',
            '',
            '',
            '',
            'INV_EVENT_LOG_ID IN (SELECT INV_EVENT_LOG_ID FROM INV_PURGE_PROC_GTT2)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_SEGMENTED_INV_LOG',
            '',
            '',
            '',
            'INV_EVENT_LOG_ID IN (SELECT INV_EVENT_LOG_ID FROM INV_PURGE_PROC_GTT2)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_INV_EVENT_LOG',
            '',
            '',
            '',
            'INV_EVENT_LOG_ID IN (SELECT  INV_EVENT_LOG_ID FROM INV_PURGE_PROC_GTT2)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_SEGMENTED',
            '',
            '',
            '',
            'INVENTORY_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_SEGMENTED',
            '',
            '',
            '',
            'REFERENCE_INV_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_PERPETUAL',
            '',
            '',
            '',
            'INVENTORY_ID IN  (SELECT  INVENTORY_ID FROM INV_PURGE_PROC_GTT1)',
            'N');

        quiet_drop ('TABLE', 'INV_PURGE_PROC_GTT');
        quiet_drop ('TABLE', 'INV_PURGE_PROC_GTT1');
        quiet_drop ('TABLE', 'INV_PURGE_PROC_GTT2');
        gvprocedurename := 'Purge_Pkg.INV_Purge_Proc - End';
        log_archive (gpurge_group,
                     gpurge_days,
                     gvprocedurename,
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
    EXCEPTION
        WHEN nodatafound
        THEN
            RAISE noparameter;
        WHEN OTHERS
        THEN
            nsqlcode := SQLCODE;
            vsqlerrm := SQLERRM;
            RAISE gdelete_error;
    END inv_purge_proc;

    PROCEDURE purge_staging_event_process
    AS
    BEGIN
        gpurge_days := get_config_days ('INVENTORY_STAG');
        gvprocedurename := 'Purge_Pkg.Purge_Staging_Event_Process - Begin';
        log_archive (gpurge_group,
                     gpurge_days,
                     gvprocedurename,
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

        -- Master Staging Data begins

        quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT');

        EXECUTE IMMEDIATE
               'CREATE TABLE PURGE_STAGING_EVENT_GTT TABLESPACE DOM_DT_TBS AS SELECT event_id,SYNC_TRANSACTION_ID
            FROM MASTER_STAGING_DATA WHERE EVENT_STATUS IN
                                                 (SELECT SYS_CODE_ID
                                                    FROM SYS_CODE
                                                   WHERE CODE_TYPE = ''I08''
                                                         AND REC_TYPE = ''S''
                                                         AND CODE_DESC IN
                                                                (''Success'',
                                                                 ''Validation Failure'',
                                                                 ''Error''))
                                              AND LAST_UPDATED_DTTM <
                                                     TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days;


        INSERT_DEL_DATA_BULK (
            'I_EVENT_DEPENDENCY',
            '',
            '',
            '',
            'event_id IN (SELECT  event_id FROM PURGE_STAGING_EVENT_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_EVENT_STAGING',
            '',
            '',
            '',
            'event_id IN (SELECT  event_id FROM PURGE_STAGING_EVENT_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_SYNC_PROCESS_CONTROL',
            '',
            '',
            '',
            'TRANSACTION_NUMBER IN (SELECT  SYNC_TRANSACTION_ID FROM PURGE_STAGING_EVENT_GTT WHERE SYNC_TRANSACTION_ID IS NOT NULL)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_SYNC_STAGING',
            '',
            '',
            '',
            'TRANSACTION_NUMBER IN (SELECT  SYNC_TRANSACTION_ID FROM PURGE_STAGING_EVENT_GTT WHERE SYNC_TRANSACTION_ID IS NOT NULL)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_PLAN_DETAIL_STAGING',
            '',
            '',
            '',
            'EVENT_ID IN (SELECT  EVENT_ID FROM PURGE_STAGING_EVENT_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_SEGMENT_PLAN_STAGING',
            '',
            '',
            '',
            'EVENT_ID IN (SELECT  EVENT_ID FROM PURGE_STAGING_EVENT_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'MASTER_STAGING_DATA',
            '',
            '',
            '',
            'EVENT_ID IN (SELECT  EVENT_ID FROM PURGE_STAGING_EVENT_GTT)',
            'N');

        quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT');

        -- Master Staging Data ends

        quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT1');

        EXECUTE IMMEDIATE
            'CREATE TABLE PURGE_STAGING_EVENT_GTT1 TABLESPACE DOM_DT_TBS AS SELECT SYS_CODE_ID
                                                    FROM SYS_CODE
                                                   WHERE CODE_TYPE = ''I08''
                                                         AND REC_TYPE = ''S''
                                                         AND CODE_DESC IN
                                                                (''Success'',
                                                                 ''Validation Failure'',
                                                                 ''Error'')';

        -- Relay Staging Data begins

        INSERT_DEL_DATA_BULK (
            'I_RELAY_STAGING',
            '',
            '',
            '',
               'RELAY_STATUS IN (SELECT  SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_ERROR',
            '',
            '',
            '',
               'LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''',''YYYY-MM-DD HH24:MI:SS'') - 30',
            'N');

        -- Relay Staging Data ends


        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_1',
            '',
            '',
            '',
               'STATUS IN (SELECT  SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_2',
            '',
            '',
            '',
               'STATUS IN (SELECT  SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_3',
            '',
            '',
            '',
               'STATUS IN (SELECT  SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_4',
            '',
            '',
            '',
               'STATUS IN (SELECT  SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_5',
            '',
            '',
            '',
               'STATUS IN (SELECT  SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_6',
            '',
            '',
            '',
               'STATUS IN (SELECT  SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_7',
            '',
            '',
            '',
               'STATUS IN (SELECT  SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_8',
            '',
            '',
            '',
               'STATUS IN (SELECT  SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_9',
            '',
            '',
            '',
               'STATUS IN (SELECT  SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_10',
            '',
            '',
            '',
               'STATUS IN (SELECT  SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_11',
            '',
            '',
            '',
               'STATUS IN (SELECT  SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_12',
            '',
            '',
            '',
               'STATUS IN (SELECT  SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_13',
            '',
            '',
            '',
               'STATUS IN (SELECT  SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_14',
            '',
            '',
            '',
               'STATUS IN (SELECT  SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_15',
            '',
            '',
            '',
               'STATUS IN (SELECT  SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_16',
            '',
            '',
            '',
               'STATUS IN (SELECT  SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_17',
            '',
            '',
            '',
               'STATUS IN (SELECT  SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_18',
            '',
            '',
            '',
               'STATUS IN (SELECT  SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_19',
            '',
            '',
            '',
               'STATUS IN (SELECT  SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAIL_EVENT_STATUS_20',
            '',
            '',
            '',
               'STATUS IN (SELECT  SYS_CODE_ID FROM PURGE_STAGING_EVENT_GTT1) AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || '',
            'N');

        quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT1');
        quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT2');
        quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT4');

        EXECUTE IMMEDIATE
               'CREATE TABLE PURGE_STAGING_EVENT_GTT2 TABLESPACE DOM_DT_TBS AS SELECT event_id FROM MASTER_COMMERCE_EVENT WHERE EVENT_STATUS IN (SELECT SYS_CODE_ID FROM SYS_CODE WHERE CODE_TYPE = ''I08'' AND REC_TYPE = ''S'' AND CODE_DESC IN (''Success'', ''Validation Failure'', ''Error'')) AND  LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days;

        EXECUTE IMMEDIATE
            'CREATE TABLE PURGE_STAGING_EVENT_GTT4 TABLESPACE DOM_DT_TBS AS SELECT AVAIL_INV_EVENT_ID FROM I_AVAIL_INVENTORY_EVENT WHERE event_id IN (SELECT event_id FROM PURGE_STAGING_EVENT_GTT2)';

        INSERT_DEL_DATA_BULK (
            'I_AVAIL_INVENTORY_EVENT_DETAIL',
            '',
            '',
            '',
            'AVAIL_INV_EVENT_ID IN (SELECT AVAIL_INV_EVENT_ID FROM PURGE_STAGING_EVENT_GTT4)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAIL_INVENTORY_EVENT',
            '',
            '',
            '',
            'event_id IN (SELECT  event_id FROM PURGE_STAGING_EVENT_GTT2)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_FULFILLMENT_OUTAGE_EVENT',
            '',
            '',
            '',
            'event_id IN (SELECT  event_id FROM PURGE_STAGING_EVENT_GTT2)',
            'N');


        INSERT_DEL_DATA_BULK (
            'I_REBUILD_EVENT',
            '',
            '',
            '',
            'rebuild_event_id in (SELECT  event_id FROM PURGE_STAGING_EVENT_GTT2)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_REBUILD_STAGING',
            '',
            '',
            '',
            'event_id in (SELECT  event_id FROM PURGE_STAGING_EVENT_GTT2)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_AVAIL_SYNC_STAGING',
            '',
            '',
            '',
            'event_id in (SELECT  event_id FROM PURGE_STAGING_EVENT_GTT2)',
            'N');


        INSERT_DEL_DATA_BULK (
            'MASTER_COMMERCE_EVENT',
            '',
            '',
            '',
            'event_id IN (SELECT  event_id FROM PURGE_STAGING_EVENT_GTT2)',
            'N');

        quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT2');
        quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT3');

        EXECUTE IMMEDIATE
               'CREATE TABLE PURGE_STAGING_EVENT_GTT3 TABLESPACE DOM_DT_TBS AS SELECT SKU_LOCATION_ID FROM SKU_LOCATION  WHERE MARK_FOR_DELETION = 1 AND LAST_UPDATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') -'
            || gpurge_days;


        INSERT_DEL_DATA_BULK (
            'SKU_FORECAST',
            '',
            '',
            '',
            'SKU_LOCATION_ID IN (SELECT  SKU_LOCATION_ID FROM PURGE_STAGING_EVENT_GTT3)',
            'N');

        INSERT_DEL_DATA_BULK (
            'SKU_LOCATION_NEED',
            '',
            '',
            '',
            'SKU_LOCATION_ID IN (SELECT  SKU_LOCATION_ID FROM PURGE_STAGING_EVENT_GTT3)',
            'N');

        INSERT_DEL_DATA_BULK (
            'SKU_LOCATION',
            '',
            '',
            '',
            'SKU_LOCATION_ID IN (SELECT  SKU_LOCATION_ID FROM PURGE_STAGING_EVENT_GTT3)',
            'N');


        INSERT_DEL_DATA_BULK (
            'ITEM_AVAILABILITY',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') -'
            || gpurge_days
            || '',
            'N');


        quiet_drop ('TABLE', 'PURGE_STAGING_EVENT_GTT3');


        INSERT_DEL_DATA_BULK (
            'I_AVAIL_SYNC_OB_PROCESS_DTL',
            '',
            '',
            '',
               'SYNC_PROCESS_END_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') -'
            || gpurge_days
            || '',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_SYNC_OB_PROCESS_CONTROL',
            '',
            '',
            '',
               'SYNC_PROCESS_END_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') -'
            || gpurge_days
            || '',
            'N');
        gvprocedurename := 'Purge_Pkg.Purge_Staging_Event_Process - End';
        log_archive (gpurge_group,
                     gpurge_days,
                     gvprocedurename,
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
    EXCEPTION
        WHEN nodatafound
        THEN
            RAISE noparameter;
        WHEN OTHERS
        THEN
            nsqlcode := SQLCODE;
            vsqlerrm := SQLERRM;
            RAISE gdelete_error;
    END purge_staging_event_process;

    PROCEDURE inv_log_purge_proc
    AS
    BEGIN
        gpurge_days := get_config_days ('INVENTORY_LOG');
        gvprocedurename := 'Purge_Pkg.INV_Log_Purge_Proc - Begin';
        log_archive (gpurge_group,
                     gpurge_days,
                     gvprocedurename,
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

        quiet_drop ('TABLE', 'INV_LOG_PURGE_PROC_GTT');

        EXECUTE IMMEDIATE
               'CREATE TABLE INV_LOG_PURGE_PROC_GTT TABLESPACE DOM_DT_TBS AS SELECT INV_EVENT_LOG_ID from I_INV_EVENT_LOG
          WHERE CREATED_DTTM < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days;


        INSERT_DEL_DATA_BULK (
            'I_PERPETUAL_INV_LOG',
            '',
            '',
            '',
            'INV_EVENT_LOG_ID IN (SELECT  INV_EVENT_LOG_ID FROM INV_LOG_PURGE_PROC_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_SEGMENTED_INV_LOG',
            '',
            '',
            '',
            'INV_EVENT_LOG_ID IN (SELECT  INV_EVENT_LOG_ID FROM INV_LOG_PURGE_PROC_GTT)',
            'N');

        INSERT_DEL_DATA_BULK (
            'I_INV_EVENT_LOG',
            '',
            '',
            '',
            'INV_EVENT_LOG_ID IN (SELECT  INV_EVENT_LOG_ID FROM INV_LOG_PURGE_PROC_GTT)',
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_1',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_2',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_3',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_4',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_5',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_6',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_7',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_8',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_9',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_10',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_11',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_12',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_13',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_14',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_15',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_16',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_17',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_18',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_19',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');
        INSERT_DEL_DATA_BULK (
            'I_AVAILABILITY_ACTIVITY_20',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        INSERT_DEL_DATA_BULK (
            'A_INVENTORY_EVENT',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        INSERT_DEL_DATA_BULK (
            'A_INVENTORY_EVENT_HISTORY',
            '',
            '',
            '',
               'CREATED_DTTM  < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        INSERT_DEL_DATA_BULK (
            'I_ALLOCATION_AUDIT',
            '',
            '',
            '',
               '(PERPETUAL_ID, ALLOCATION_VERSION) NOT IN (SELECT PERPETUAL_ID, MAX (ALLOCATION_VERSION) FROM I_ALLOCATION_AUDIT
                WHERE CREATED_DTTM  < TO_DATE('''
            || TO_CHAR (garchivebasedttm, 'yyyy-mon-dd hh24:mi:ss')
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ' AND  ALLOCATION_VERSION > 0 GROUP BY PERPETUAL_ID) AND ALLOCATION_VERSION > 0 AND
				CREATED_DTTM  < TO_DATE('''
            || TO_CHAR (garchivebasedttm, 'yyyy-mon-dd hh24:mi:ss')
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        quiet_drop ('TABLE', 'INV_LOG_PURGE_PROC_GTT');
        gvprocedurename := 'Purge_Pkg.INV_Log_Purge_Proc - End';
        log_archive (gpurge_group,
                     gpurge_days,
                     gvprocedurename,
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
    EXCEPTION
        WHEN nodatafound
        THEN
            RAISE noparameter;
        WHEN OTHERS
        THEN
            nsqlcode := SQLCODE;
            vsqlerrm := SQLERRM;
            RAISE gdelete_error;
    END inv_log_purge_proc;

    PROCEDURE co_base_data_purge
    AS
    BEGIN
        gpurge_days := get_config_days ('INVENTORY_COBASE');
        gvprocedurename := 'Purge_Pkg.CO_Base_Data_Purge - Begin';
        log_archive (gpurge_group,
                     gpurge_days,
                     gvprocedurename,
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

        INSERT_DEL_DATA_BULK (
            'A_CHARGE_DETAIL',
            '',
            '',
            '',
               'MARK_FOR_DELETION = 1 AND LAST_UPDATED_DTTM < (TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');

        INSERT_DEL_DATA_BULK (
            'A_DISCOUNT_DETAIL',
            '',
            '',
            '',
               'MARK_FOR_DELETION = 1 AND LAST_UPDATED_DTTM < (TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');

        INSERT_DEL_DATA_BULK (
            'A_TAX_DETAIL',
            '',
            '',
            '',
               'MARK_FOR_DELETION = 1 AND LAST_UPDATED_DTTM < (TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');

        INSERT_DEL_DATA_BULK (
            'A_SHIPPING_CHARGE_RULE',
            '',
            '',
            '',
               'END_DATE < (TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');

        INSERT_DEL_DATA_BULK (
            'A_SHIPPING_CHARGE_RULE',
            '',
            '',
            '',
               'IS_DELETED =1 AND LAST_UPDATED_DTTM < (TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');

        INSERT_DEL_DATA_BULK (
            'A_SHIPPING_CHARGE_MATRIX',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < (TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');

        INSERT_DEL_DATA_BULK (
            'A_DELIVERYZONE',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < (TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');

        INSERT_DEL_DATA_BULK (
            'A_PROMOTION_DETAIL',
            '',
            '',
            '',
               'MARK_FOR_DELETION = 1 AND LAST_UPDATED_DTTM < (TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');

        INSERT_DEL_DATA_BULK (
            'A_PROMOTED_PROD_ASSOCIATION',
            '',
            '',
            '',
               'MARK_FOR_DELETION = 1 AND LAST_UPDATED_DTTM < (TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');

        INSERT_DEL_DATA_BULK (
            'A_CUSTOMER_INFO',
            '',
            '',
            '',
               'IS_DELETED = 1 AND LAST_UPDATED_DTTM < (TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');

        INSERT_DEL_DATA_BULK ('A_CO_REASON_CODE',
                              '',
                              '',
                              '',
                              'MARK_FOR_DELETION = 1',
                              'N');

        INSERT_DEL_DATA_BULK (
            'A_CO_NOTE',
            '',
            '',
            '',
            'NOTE_TYPE_ID IN (SELECT NOTE_TYPE_ID FROM A_CO_NOTE_TYPE WHERE MARK_FOR_DELETION = 1)',
            'N');

        INSERT_DEL_DATA_BULK ('A_CO_NOTE_TYPE',
                              '',
                              '',
                              '',
                              'MARK_FOR_DELETION = 1',
                              'N');

        INSERT_DEL_DATA_BULK ('A_CO_APPEASEMENT',
                              '',
                              '',
                              '',
                              'MARK_FOR_DELETION = 1',
                              'N');
        gvprocedurename := 'Purge_Pkg.CO_Base_Data_Purge - End';
        log_archive (gpurge_group,
                     gpurge_days,
                     gvprocedurename,
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
    EXCEPTION
        WHEN nodatafound
        THEN
            RAISE noparameter;
        WHEN OTHERS
        THEN
            nsqlcode := SQLCODE;
            vsqlerrm := SQLERRM;
            RAISE gdelete_error;
    END co_base_data_purge;

    PROCEDURE log_purge
    IS
        v_message   VARCHAR2 (20);
    BEGIN
        gpurge_days := get_config_days ('LOG');
        gvprocedurename := 'Purge_Pkg.Log_Purge - Begin';
        log_archive (gpurge_group,
                     gpurge_days,
                     gvprocedurename,
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

        insert_del_Data_bulk (
            'A_PROCESS_LOG_ASYNC_TRACK',
            '',
            '',
            '',
               'active_thread_count = 0 AND created_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        insert_del_Data_bulk (
            'A_PROCESS_LOG_ERROR',
            '',
            '',
            '',
               '(PROCESS_ID, TO_DATE (TO_CHAR (CREATED_DTTM, ''DD/MM/YYYY HH:MI:SS''),
                 ''DD/MM/YYYY HH:MI:SS'')) IN
          (  SELECT PROCESS_ID PID, MIN ( TO_DATE (TO_CHAR (CREATED_DTTM, ''DD/MM/YYYY HH:MI:SS''),
                                ''DD/MM/YYYY HH:MI:SS'')) CT
               FROM A_PROCESS_LOG_ERROR
           GROUP BY PROCESS_ID
             HAVING MIN (TO_DATE (TO_CHAR (CREATED_DTTM, ''DD/MM/YYYY HH:MI:SS''),
                          ''DD/MM/YYYY HH:MI:SS'')) < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');

        insert_del_Data_bulk (
            'A_PROCESS_LOG_HISTORY',
            '',
            '',
            '',
               '(PROCESS_ID, TO_DATE (TO_CHAR (EVENT_TIMESTAMP, ''DD/MM/YYYY HH:MI:SS''),
                 ''DD/MM/YYYY HH:MI:SS'')) IN
          (  SELECT PROCESS_ID PID, MIN ( TO_DATE (TO_CHAR (EVENT_TIMESTAMP, ''DD/MM/YYYY HH:MI:SS''),
                                ''DD/MM/YYYY HH:MI:SS'')) CT
               FROM A_PROCESS_LOG_HISTORY
           GROUP BY PROCESS_ID
             HAVING MIN (TO_DATE (TO_CHAR (EVENT_TIMESTAMP, ''DD/MM/YYYY HH:MI:SS''),
                          ''DD/MM/YYYY HH:MI:SS'')) < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');

        insert_del_Data_bulk (
            'error_log_args',
            '',
            '',
            '',
               'error_id IN (SELECT error_id FROM ERROR_LOG WHERE created_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days
            || ')',
            'N');

        insert_del_Data_bulk (
            'ERROR_LOG',
            '',
            '',
            '',
               'created_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        insert_del_Data_bulk (
            'EVENT_LOG',
            '',
            '',
            '',
               'event_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        insert_del_Data_bulk (
            'WM_WAX_EVENT_LOG',
            '',
            '',
            '',
               'last_updated_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days,
            'N');

        quiet_drop ('TABLE', 'LOG_PURGE_GTT');

        BEGIN
            SELECT UPPER (purge_status)
              INTO v_message
              FROM purge_criteria
             WHERE purge_module = 'OLM' AND purge_group = 'LOG';

            IF v_message = 'SUCCESS'
            THEN
                EXECUTE IMMEDIATE
                       'CREATE TABLE log_purge_gtt TABLESPACE DOM_DT_TBS NOLOGGING AS SELECT tran_log_id, message_id FROM tran_log WHERE has_errors = 0 AND last_updated_dttm < TO_DATE('''
                    || garchivebasedttm
                    || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
                    || gpurge_days;
            ELSIF v_message = 'ERROR'
            THEN
                EXECUTE IMMEDIATE
                       'CREATE TABLE log_purge_gtt TABLESPACE DOM_DT_TBS NOLOGGING AS SELECT tran_log_id, message_id FROM tran_log WHERE has_errors = 1 AND last_updated_dttm < TO_DATE('''
                    || garchivebasedttm
                    || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
                    || gpurge_days;
            ELSE
                EXECUTE IMMEDIATE
                       'CREATE TABLE log_purge_gtt TABLESPACE DOM_DT_TBS NOLOGGING AS SELECT tran_log_id, message_id FROM tran_log WHERE LAST_UPDATED_DTTM < TO_DATE('''
                    || garchivebasedttm
                    || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
                    || gpurge_days;
            END IF;
        EXCEPTION
            WHEN OTHERS
            THEN
                RAISE noparameter;
        END;

        insert_del_Data_bulk (
            'TRAN_LOG_RESPONSE_MESSAGE',
            '',
            '',
            '',
            'message_id IN (SELECT message_id FROM log_purge_gtt)',
            'N');

        insert_del_Data_bulk (
            'TRAN_LOG_MESSAGE',
            '',
            '',
            '',
            'tran_log_id IN (SELECT tran_log_id FROM log_purge_gtt)',
            'N');

        insert_del_Data_bulk (
            'TRAN_LOG',
            '',
            '',
            '',
            'tran_log_id IN (SELECT tran_log_id FROM log_purge_gtt)',
            'N');
        quiet_drop ('TABLE', 'LOG_PURGE_GTT');
        gvprocedurename := 'Purge_Pkg.Log_Purge - End';
        log_archive (gpurge_group,
                     gpurge_days,
                     gvprocedurename,
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
    EXCEPTION
        WHEN nodatafound
        THEN
            RAISE noparameter;
        WHEN OTHERS
        THEN
            nsqlcode := SQLCODE;
            vsqlerrm := SQLERRM;
            RAISE gdelete_error;
    END log_purge;

    PROCEDURE mobile_purge
    IS
    BEGIN
        gpurge_days := get_config_days ('MOBILE');
        gvprocedurename := 'Purge_Pkg.Mobile_Purge - Begin';
        log_archive (gpurge_group,
                     gpurge_days,
                     gvprocedurename,
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);

        quiet_drop ('TABLE', 'MOBILE_PURGE_GTT');
        quiet_drop ('TABLE', 'MOBILE_PURGE_GTT1');

        EXECUTE IMMEDIATE
               'CREATE TABLE mobile_purge_gtt TABLESPACE DOM_DT_TBS NOLOGGING AS SELECT mobile_session_id FROM mobile_session WHERE created_dttm < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days;

        EXECUTE IMMEDIATE
            'CREATE TABLE mobile_purge_gtt1 TABLESPACE DOM_DT_TBS NOLOGGING AS SELECT  mobile_transaction_log_id FROM mobile_transaction_log 
            WHERE mobile_session_id IN (SELECT mobile_session_id FROM  mobile_purge_gtt)';

        insert_del_Data_bulk (
            'MOBILE_TRANSACTION_REQUEST',
            '',
            '',
            '',
            'mobile_transaction_log_id IN (SELECT mobile_transaction_log_id FROM mobile_purge_gtt1)',
            'N');

        insert_del_Data_bulk (
            'MOBILE_TRANSACTION_MESSAGE',
            '',
            '',
            '',
            'mobile_transaction_log_id IN (SELECT mobile_transaction_log_id FROM mobile_purge_gtt1)',
            'N');

        insert_del_Data_bulk (
            'MOBILE_TRANSACTION_LOG',
            '',
            '',
            '',
            'mobile_session_id IN (SELECT mobile_session_id FROM mobile_purge_gtt)',
            'N');

        insert_del_Data_bulk (
            'MOBILE_SYNC_OBJECT',
            '',
            '',
            '',
            'mobile_device_id IN (SELECT mobile_device_id FROM mobile_device WHERE current_session_id IN (SELECT mobile_session_id FROM mobile_purge_gtt))',
            'N');

        insert_del_Data_bulk (
            'MOBILE_DEVICE',
            '',
            '',
            '',
            'current_session_id IN (SELECT mobile_session_id FROM mobile_purge_gtt)',
            'N');

        insert_del_Data_bulk (
            'MOBILE_SESSION',
            '',
            '',
            '',
            'mobile_session_id IN (SELECT mobile_session_id FROM mobile_purge_gtt)',
            'N');

        quiet_drop ('TABLE', 'MOBILE_PURGE_GTT');
        quiet_drop ('TABLE', 'MOBILE_PURGE_GTT1');
        gvprocedurename := 'Purge_Pkg.Mobile_Purge - End';
        log_archive (gpurge_group,
                     gpurge_days,
                     gvprocedurename,
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
    EXCEPTION
        WHEN nodatafound
        THEN
            RAISE noparameter;
        WHEN OTHERS
        THEN
            nsqlcode := SQLCODE;
            vsqlerrm := SQLERRM;
            RAISE gdelete_error;
    END mobile_purge;

    PROCEDURE dwm_purge
    IS
        v_status   VARCHAR2 (50);
    BEGIN
        gpurge_days := get_config_days ('DWM');
        gvprocedurename := 'Purge_Pkg.DWM_Purge - Begin';
        log_archive (gpurge_group,
                     gpurge_days,
                     gvprocedurename,
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
        quiet_drop ('TABLE', 'DWM_PURGE_GTT');

        BEGIN
            SELECT INITCAP (purge_status)
              INTO v_status
              FROM purge_criteria
             WHERE purge_module = 'OLM' AND purge_group = 'DWM';

            IF v_status IS NOT NULL
            THEN
                EXECUTE IMMEDIATE
                       'CREATE TABLE DWM_PURGE_GTT TABLESPACE DOM_DT_TBS NOLOGGING AS SELECT work_request_id FROM dwm_work_request WHERE submitted_dttm < TO_DATE('''
                    || garchivebasedttm
                    || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
                    || gpurge_days
                    || ' AND STATUS IN ('''
                    || REPLACE (v_status, ',', ''',''')
                    || ''')';
            ELSE
                EXECUTE IMMEDIATE
                       'CREATE TABLE DWM_PURGE_GTT TABLESPACE DOM_DT_TBS NOLOGGING AS SELECT work_request_id FROM dwm_work_request WHERE submitted_dttm < TO_DATE('''
                    || garchivebasedttm
                    || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
                    || gpurge_days;
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                RAISE noparameter;
        END;

        insert_del_Data_bulk (
            'DWM_WORK_EVENT_LOG',
            '',
            '',
            '',
            'work_request_id IN (SELECT work_request_id FROM dwm_purge_gtt)',
            'N');

        insert_del_Data_bulk (
            'DWM_WORK_REQUEST_ERROR',
            '',
            '',
            '',
            'work_request_id IN (SELECT work_request_id FROM dwm_purge_gtt)',
            'N');

        insert_del_Data_bulk (
            'DWM_WORK_REQUEST',
            '',
            '',
            '',
            'work_request_id IN (SELECT work_request_id FROM dwm_purge_gtt)',
            'N');
        quiet_drop ('TABLE', 'DWM_PURGE_GTT');
        gvprocedurename := 'Purge_Pkg.DWM_Purge - End';
        log_archive (gpurge_group,
                     gpurge_days,
                     gvprocedurename,
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
    EXCEPTION
        WHEN nodatafound
        THEN
            RAISE noparameter;
        WHEN OTHERS
        THEN
            nsqlcode := SQLCODE;
            vsqlerrm := SQLERRM;
            RAISE gdelete_error;
    END dwm_purge;

    PROCEDURE stats_snapshot_purge
    IS
    BEGIN
        gpurge_days := get_config_days ('STATS_SNAPSHOT');
        gvprocedurename := 'Purge_Pkg.Stats_Snapshot_Purge - Begin';
        log_archive (gpurge_group,
                     gpurge_days,
                     gvprocedurename,
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
        quiet_drop ('TABLE', 'STATS_SNAPSHOT_GTT');

        EXECUTE IMMEDIATE
               'CREATE TABLE STATS_SNAPSHOT_GTT TABLESPACE DOM_DT_TBS NOLOGGING AS SELECT snapshot_id FROM stats_snapshot WHERE snapshot_start_time < TO_DATE('''
            || garchivebasedttm
            || ''', ''YYYY-MM-DD HH24:MI:SS'') - '
            || gpurge_days;

        insert_del_Data_bulk (
            'STATS_SNAPSHOT_EVENT_DATA',
            '',
            '',
            '',
            'snapshot_id IN (SELECT snapshot_id FROM stats_snapshot_gtt)',
            'N');

        insert_del_Data_bulk (
            'STATS_SNAPSHOT_EVENT',
            '',
            '',
            '',
            'snapshot_id IN (SELECT snapshot_id FROM stats_snapshot_gtt)',
            'N');

        insert_del_Data_bulk (
            'STATS_SNAPSHOT',
            '',
            '',
            '',
            'snapshot_id IN (SELECT snapshot_id FROM stats_snapshot_gtt)',
            'N');

        quiet_drop ('TABLE', 'STATS_SNAPSHOT_GTT');
        gvprocedurename := 'Purge_Pkg.Stats_Snapshot_Purge - End';
        log_archive (gpurge_group,
                     gpurge_days,
                     gvprocedurename,
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
    EXCEPTION
        WHEN nodatafound
        THEN
            RAISE noparameter;
        WHEN OTHERS
        THEN
            nsqlcode := SQLCODE;
            vsqlerrm := SQLERRM;
            RAISE gdelete_error;
    END stats_snapshot_purge;

    PROCEDURE execute_purge (p_purge_group IN VARCHAR2)
    AS
        v_count           NUMBER := 0;
        v_packagestatus   VARCHAR2 (40);
        purgeinprogress   EXCEPTION;
    BEGIN
        log_archive (NULL,
                     NULL,
                     'Purge_Pkg.Execute_Purge - Begin',
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
        gpurge_group := UPPER (p_purge_group);

        EXECUTE IMMEDIATE
            'alter session set NLS_TIMESTAMP_FORMAT = ''YYYY-MM-DD HH24:MI:SS.FF''';

        EXECUTE IMMEDIATE
            'alter session set NLS_DATE_FORMAT = ''YYYY-MM-DD HH24:MI:SS''';

        -- Get Dynamic Parallel Level and Commit Frequency
        SELECT NVL (company_parameter.param_value, param_def.dflt_value)
          INTO gcommitfrequency
          FROM param_def, company_parameter
         WHERE     param_def.param_def_id = company_parameter.param_def_id(+)
               AND param_def.param_group_id =
                       company_parameter.param_group_id(+)
               AND param_def.param_def_id = 'ARCHIVE_COMMIT_FREQUENCY'
               AND param_def.param_group_id = 'ARCHIVE'
               AND param_def.is_disabled = 0;

        SELECT NVL (company_parameter.param_value, param_def.dflt_value)
          INTO n_parallel_level
          FROM param_def, company_parameter
         WHERE     param_def.param_def_id = company_parameter.param_def_id(+)
               AND param_def.param_group_id =
                       company_parameter.param_group_id(+)
               AND param_def.param_def_id = 'ARCHIVE_PARALLEL_THREADS'
               AND param_def.param_group_id = 'ARCHIVE'
               AND param_def.is_disabled = 0;

        -- Check to execute the purge process in one node in case of RAC environment using  MA_ARCH_JOB_CLASS.
        -- This job class need to be created expilicitly only in case RAC setup. If the job class is not created, purge process will execute using DEFAULT_JOB_CLASS
        SELECT COUNT (1)
          INTO gv_job_class_chk
          FROM all_scheduler_job_classes
         WHERE job_class_name = 'MA_ARCH_JOB_CLASS';

        IF gpurge_group = 'ALL'
        THEN
            SELECT COUNT (1)
              INTO v_count
              FROM archive_status
             WHERE arch_run_status LIKE '%Running';

            IF v_count = 0
            THEN
                v_packagestatus := 'Purge_Pkg - Begin';
            ELSE
                RAISE purgeinprogress;
            END IF;

            DELETE FROM archive_status
                  WHERE arch_run_status LIKE 'Purge_Pkg%';

            INSERT INTO archive_status (arch_begin_dttm, arch_run_status)
                 VALUES (SYSDATE, 'Purge_Pkg - Running');

            --
            --         INSERT INTO archive_status (arch_begin_dttm, arch_run_status)
            --              VALUES (SYSDATE, 'Purge_Pkg.EventLog - Running');

            COMMIT;

            inv_bussiness_rules_purge_proc;
            commerce_purge_proc;
            inv_purge_proc;
            purge_staging_event_process;
            inv_log_purge_proc;
            co_base_data_purge;
            event_purge;
            log_purge;
            mobile_purge;
            dwm_purge;
            stats_snapshot_purge;
        ELSIF gpurge_group = 'INVENTORY'
        THEN
            SELECT COUNT (1)
              INTO v_count
              FROM archive_status
             WHERE arch_run_status LIKE '%Running';

            IF v_count = 0
            THEN
                v_packagestatus := 'Purge_Pkg - Begin';
            ELSE
                SELECT arch_run_status
                  INTO v_packagestatus
                  FROM archive_status
                 WHERE arch_run_status LIKE '%Running';
            END IF;

            IF v_packagestatus IN
                   ('Purge_Pkg.Inventory - Running', 'Purge_Pkg - Running')
            THEN
                RAISE purgeinprogress;
            END IF;

            IF v_packagestatus LIKE 'Purge_Pkg.EventLog - Running'
            THEN
                n_parallel_level := n_parallel_level / 2;
            END IF;

            DELETE FROM archive_status
                  WHERE    arch_run_status LIKE '%Completed'
                        OR arch_run_status LIKE '%Failed';

            INSERT INTO archive_status (arch_begin_dttm, arch_run_status)
                 VALUES (SYSDATE, 'Purge_Pkg.Inventory - Running');

            COMMIT;
            inv_bussiness_rules_purge_proc;
            commerce_purge_proc;
            inv_purge_proc;
            purge_staging_event_process;
            inv_log_purge_proc;
            co_base_data_purge;
        ELSIF gpurge_group = 'EVENTLOG'
        THEN
            SELECT COUNT (1)
              INTO v_count
              FROM archive_status
             WHERE arch_run_status LIKE '%Running';

            IF v_count = 0
            THEN
                v_packagestatus := 'Purge_Pkg - Begin';
            ELSE
                SELECT arch_run_status
                  INTO v_packagestatus
                  FROM archive_status
                 WHERE arch_run_status LIKE '%Running';
            END IF;

            IF v_packagestatus IN
                   ('Purge_Pkg.EventLog - Running', 'Purge_Pkg - Running')
            THEN
                RAISE purgeinprogress;
            END IF;

            IF v_packagestatus LIKE 'Purge_Pkg.Inventory - Running'
            THEN
                n_parallel_level := n_parallel_level / 2;
            END IF;

            DELETE FROM archive_status
                  WHERE    arch_run_status LIKE '%Completed'
                        OR arch_run_status LIKE '%Failed';

            INSERT INTO archive_status (arch_begin_dttm, arch_run_status)
                 VALUES (SYSDATE, 'Purge_Pkg.EventLog - Running');

            COMMIT;
            event_purge;
            log_purge;
            mobile_purge;
            dwm_purge;
            stats_snapshot_purge;
        END IF;

        log_archive (NULL,
                     NULL,
                     'Purge_Pkg.Execute_Purge - End',
                     NULL,
                     NULL,
                     SEQ_ARCHIVE_RUN_LOG.NEXTVAL);
        update_purge_status ('Success');
    EXCEPTION
        WHEN purgeinprogress
        THEN
            log_error (-20005, 'EXCEPTION : Purge_Pkg - PurgeInProgress');
            raise_application_error (
                -20005,
                'ERROR: Another Purge Process is already in progress.');
        WHEN gdelete_error
        THEN
            update_purge_status ('Failed');
            log_error (nsqlcode, 'Error in Purge_Pkg: ' || vsqlerrm);
            raise_application_error (
                -20005,
                'ERROR: Please check ARCHIVE_ERROR_LOG Table for more details.');
        WHEN noparameter
        THEN
            update_purge_status ('Failed');
            log_error (
                nsqlcode,
                'Error in Purge_Pkg: Configure number of days against purge group in PURGE_CRITERIA table.');
            raise_application_error (
                -20005,
                'ERROR: Please configure the input parameters in PURGE_CRITERIA table.');
        WHEN OTHERS
        THEN
            ROLLBACK;

            update_purge_status ('Failed');
            nsqlcode := SQLCODE;
            vsqlerrm := SQLERRM;
            log_error (nsqlcode, 'Error in Purge_Pkg: ' || vsqlerrm);
    END execute_purge;
END purge_pkg;
/
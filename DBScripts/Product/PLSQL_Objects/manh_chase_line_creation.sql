create or replace procedure manh_chase_line_creation
(
    p_ship_wave_nbr     in order_line_item.ship_wave_nbr%type,
    p_pick_wave_nbr     in order_line_item.wave_nbr%type,
    p_zero              in number,
    p_user_id           in ucl_user.user_name%type
)
as
   v_agg_line_user_canceled_qty    order_line_item.user_canceled_qty%type;
   v_chase_line_allocable_qty      order_line_item.allocated_qty%type;
   v_orig_line_adjusted_order_qty  order_line_item.adjusted_order_qty%type;
   v_orig_line_order_qty           order_line_item.order_qty%type;
   v_orig_line_allocated_qty       order_line_item.allocated_qty%type;
   v_orig_line_canceled_qty        order_line_item.user_canceled_qty%type;
   v_orig_line_received_qty        order_line_item.received_qty%type;
   v_cancel_qty_df_pst_neg         integer;

  -- get all the aggregated lines from temporary tables in a loop and loop through all the original order lines.
   cursor agg_line is
      select t.order_id, t.line_item_id,t.qty_soft_alloc user_canceled_qty
             , o.is_original_order
      from tmp_ord_dtl_sku_invn t
      join order_line_item oli on oli.order_id = t.order_id
           and oli.line_item_id = t.line_item_id
      join orders o on oli.order_id = o.order_id
      join tmp_wave_selected_orders t2 on t2.order_id = t.order_id
           and t2.line_item_id = t.line_item_id
      join item_cbo ic on ic.item_id = t.item_id
      where t.is_flushed = 0 and t.is_split_line < 4;

   v_agg_line  agg_line%ROWTYPE;

   -- loop through each original order line item using AGG line, and create child chase lines
   -- based on the allocable qty
   cursor orig_line is
      select oli.order_id, oli.line_item_id,oli.adjusted_order_qty, oli.order_qty,
             oli.allocated_qty,oli.user_canceled_qty, oli.received_qty from order_line_item oli
      where oli.reference_order_id= v_agg_line.order_id
      and oli.reference_line_item_id= v_agg_line.line_item_id
      and oli.user_canceled_qty > 0
      order by oli.priority asc,oli.user_canceled_qty desc;

   v_orig_line orig_line%ROWTYPE;

begin

   open agg_line;
   LOOP
      FETCH agg_line INTO v_agg_line;
      EXIT WHEN agg_line%NOTFOUND;

      v_agg_line_user_canceled_qty := v_agg_line.user_canceled_qty;

      if ( v_agg_line.is_original_order = 0 )
      then
         OPEN orig_line;
         LOOP
            FETCH orig_line INTO v_orig_line;
            EXIT WHEN orig_line%NOTFOUND;
            -- This is just to verify whether the agg line canceled qty is
            -- greater than original or not, if it is greater loop through all the original lines
            -- canceled qty and create chase lines, else create for first and exit.
            v_cancel_qty_df_pst_neg := case when (v_agg_line_user_canceled_qty  - v_orig_line.user_canceled_qty) >= p_zero
                                            then 1
                                            else -1 end;

            if abs(v_agg_line_user_canceled_qty - v_orig_line.user_canceled_qty ) <= v_agg_line_user_canceled_qty
            then
               if v_cancel_qty_df_pst_neg = 1
               then
                  v_chase_line_allocable_qty := v_orig_line.user_canceled_qty;
               else
                  v_chase_line_allocable_qty := v_agg_line_user_canceled_qty;
               end if;

               -- Insert a new child chase line for the originial line of AGG line
               -- with the user_canceled_qty as order_qty also point parent_line_item_id
               -- and substituted_parent_line_id of a original line.
               insert into order_line_item
               (
                  order_id, parent_line_item_id,substituted_parent_line_id, line_item_id,
                  ppack_qty, orig_order_qty, adjusted_order_qty, order_qty,allocated_qty,
                  do_dtl_status,is_chase_created_line, created_source, created_dttm, item_id,
                  invn_type, prod_stat, cntry_of_orgn, batch_nbr, item_attr_1,
                  item_attr_2, item_attr_3, item_attr_4, item_attr_5, ship_wave_nbr,
                  wave_nbr, is_hazmat,
                  is_stackable, has_errors,
                  created_source_type,
                  tc_order_line_id, actual_cost, actual_cost_currency_code,
                  actual_shipped_dttm, allocation_source, allocation_source_id,
                  allocation_source_line_id, alloc_line_id, alloc_type,
                  assort_nbr, batch_requirement_type, budg_cost, budg_cost_currency_code,
                  chute_assign_type, commodity_class,
                  commodity_code_id, critcl_dim_1, critcl_dim_2, critcl_dim_3,
                  cube_multiple_qty, customer_item,custom_tag, delivery_end_dttm,
                  delivery_reference_number, delivery_start_dttm,
                  event_code, exp_info_code, ext_sys_line_item_id, fulfillment_type,
                  hibernate_version, internal_order_id, internal_order_seq_nbr,
                  is_cancelled, is_emergency, item_name,
                  last_updated_dttm, last_updated_source, last_updated_source_type,
                  line_type, lpn_brk_attrib, lpn_size, lpn_type,
                  manufacturing_dttm, master_order_id, merchandizing_department_id,
                  merch_grp, merch_type, minor_order_nbr, mo_line_item_id, received_qty,
                  shipped_qty, qty_uom_id_base, mv_currency_code,
                  mv_size_uom_id, order_consol_attr,
                  order_line_id, qty_uom_id, orig_budg_cost, package_type_id, pack_rate,
                  pack_zone, pallet_type, partl_fill,
                  pickup_end_dttm, pickup_reference_number, pickup_start_dttm,
                  pick_locn_assign_type,
                  planned_ship_date, ppack_grp_code, price,
                  price_tkt_type, priority, product_class_id,
                  protection_level_id,
                  purchase_order_line_number, reason_code,
                  ref_field1, ref_field2, ref_field3, ref_field4, ref_field5, 
                  ref_field6, ref_field7, ref_field8, ref_field9, ref_field10, repl_proc_type,
                  repl_wave_nbr, repl_wave_run, retail_price,
                  rtl_to_be_distroed_qty, rts_id, rts_line_item_id,
                  serial_number_required_flag, shelf_days, single_unit_flag,
                  sku_break_attr, sku_gtin, sku_sub_code_id, sku_sub_code_value,
                  stack_diameter_standard_uom, stack_diameter_value,
                  stack_height_standard_uom, stack_height_value,
                  stack_length_standard_uom, stack_length_value, stack_rank,
                  stack_width_standard_uom, stack_width_value, std_bundle_qty,
                  std_lpn_qty, std_lpn_vol, std_lpn_wt, std_pack_qty, std_pallet_qty,
                  std_sub_pack_qty, store_dept, tc_company_id,
                  total_monetary_value, unit_cost, unit_monetary_value, unit_price_amount,
                  unit_tax_amount, unit_vol, unit_wt, un_number_id, user_canceled_qty,
                  vas_process_type, wave_proc_type,
                  qty_conv_factor, received_weight,
                  planned_weight, shipped_weight, weight_uom_id_base, weight_uom_id,
                  planned_volume, received_volume, shipped_volume, volume_uom_id_base,
                  volume_uom_id, size1_uom_id, size1_value, received_size1, shipped_size1,
                  size2_uom_id, size2_value, received_size2, shipped_size2,
                  units_pakd, description,  ext_purchase_order, purchase_order_number, ref_num1, ref_num2, ref_num3,
				  ref_num4, freight_revenue_currency_code,ref_num5
               )
                  select oli.order_id,
                  case
                  when oli.parent_line_item_id > 0 and oli.parent_line_item_id <> oli.line_item_id
                  then oli.parent_line_item_id
                  else oli.line_item_id end parent_line_item_id,
                  case
                  when oli.substituted_parent_line_id > 0 and oli.substituted_parent_line_id <> oli.line_item_id
                  then oli.substituted_parent_line_id
                  else oli.line_item_id end substituted_parent_line_id,
                  seq_line_item_id.nextval line_item_id,oli.ppack_qty,
                  oli.orig_order_qty orig_order_qty,
                  v_chase_line_allocable_qty adjusted_order_qty,
                      v_chase_line_allocable_qty order_qty,
                      v_chase_line_allocable_qty allocated_qty,
                  130,1,
                  p_user_id created_source, sysdate created_dttm, oli.item_id, oli.invn_type,
                  oli.prod_stat, oli.cntry_of_orgn, oli.batch_nbr, oli.item_attr_1, oli.item_attr_2,
                  oli.item_attr_3, oli.item_attr_4, oli.item_attr_5, p_ship_wave_nbr,
                  p_pick_wave_nbr wave_nbr, oli.is_hazmat,
                  oli.is_stackable, oli.has_errors,
                  oli.created_source_type,
                  to_char(seq_line_item_id.nextval) tc_order_line_id,
                  oli.actual_cost, oli.actual_cost_currency_code, oli.actual_shipped_dttm,
                  oli.allocation_source, oli.allocation_source_id,
                  oli.allocation_source_line_id, oli.alloc_line_id, oli.alloc_type,
                  oli.assort_nbr, oli.batch_requirement_type, oli.budg_cost,
                  oli.budg_cost_currency_code,
                  oli.chute_assign_type, oli.commodity_class, oli.commodity_code_id,
                  oli.critcl_dim_1, oli.critcl_dim_2, oli.critcl_dim_3,
                  oli.cube_multiple_qty, oli.customer_item, oli.custom_tag,
                  oli.delivery_end_dttm, oli.delivery_reference_number,
                  oli.delivery_start_dttm, oli.event_code,
                  oli.exp_info_code, oli.ext_sys_line_item_id, oli.fulfillment_type,
                  oli.hibernate_version, oli.internal_order_id,
                  oli.internal_order_seq_nbr,oli.is_cancelled,oli.is_emergency,
                  oli.item_name, sysdate last_updated_dttm,
                  p_user_id last_updated_source, oli.last_updated_source_type,
                  oli.line_type, oli.lpn_brk_attrib, oli.lpn_size,
                  oli.lpn_type, oli.manufacturing_dttm, oli.master_order_id,
                  oli.merchandizing_department_id, oli.merch_grp, oli.merch_type,
                  oli.minor_order_nbr, oli.mo_line_item_id, 0 received_qty,
                  0 shipped_qty, oli.qty_uom_id_base,
                  oli.mv_currency_code, oli.mv_size_uom_id,
                  oli.order_consol_attr, oli.order_line_id,
                  oli.qty_uom_id, oli.orig_budg_cost, oli.package_type_id, oli.pack_rate,
                  oli.pack_zone, oli.pallet_type, oli.partl_fill,
                  oli.pickup_end_dttm, oli.pickup_reference_number,
                  oli.pickup_start_dttm, oli.pick_locn_assign_type,
                  oli.planned_ship_date, oli.ppack_grp_code,
                  oli.price, oli.price_tkt_type, oli.priority, oli.product_class_id,
                  oli.protection_level_id,
                  oli.purchase_order_line_number,'45' reason_code,
                  oli.ref_field1, oli.ref_field2, oli.ref_field3, oli.ref_field4, 
                  oli.ref_field5, oli.ref_field6, oli.ref_field7, oli.ref_field8, 
                  oli.ref_field9, oli.ref_field10,
                  oli.repl_proc_type, '' repl_wave_nbr, oli.repl_wave_run,
                  oli.retail_price, oli.rtl_to_be_distroed_qty,
                  oli.rts_id, oli.rts_line_item_id,
                  oli.serial_number_required_flag, oli.shelf_days, oli.single_unit_flag,
                  oli.sku_break_attr, oli.sku_gtin, oli.sku_sub_code_id,
                  oli.sku_sub_code_value, oli.stack_diameter_standard_uom,
                  oli.stack_diameter_value, oli.stack_height_standard_uom,
                  oli.stack_height_value, oli.stack_length_standard_uom,
                  oli.stack_length_value, oli.stack_rank, oli.stack_width_standard_uom,
                  oli.stack_width_value,oli.std_bundle_qty ,oli.std_lpn_qty ,oli.std_lpn_vol ,
                  oli.std_lpn_wt ,oli.std_pack_qty ,oli.std_pallet_qty ,oli.std_sub_pack_qty ,
                  oli.store_dept, oli.tc_company_id,
                  oli.total_monetary_value, oli.unit_cost, oli.unit_monetary_value,
                  oli.unit_price_amount, oli.unit_tax_amount, oli.unit_vol, oli.unit_wt,
                  oli.un_number_id, 0 user_canceled_qty, oli.vas_process_type,
                  oli.wave_proc_type,oli.qty_conv_factor,oli.received_weight,
                  case when oli.weight_uom_id_base is null then null
                       else oli.planned_weight * v_chase_line_allocable_qty/oli.order_qty
                       end planned_weight,
                  oli.shipped_weight, oli.weight_uom_id_base, oli.weight_uom_id,
                  case when oli.volume_uom_id_base is null then null
                       else oli.planned_volume * v_chase_line_allocable_qty/oli.order_qty
                       end planned_volume
                      ,oli.received_volume, oli.shipped_volume,
                  oli.volume_uom_id_base, oli.volume_uom_id, oli.size1_uom_id,
                  oli.size1_value * v_chase_line_allocable_qty/oli.order_qty size1_value,
                  oli.received_size1, oli.shipped_size1, oli.size2_uom_id,
                  oli.size2_value * v_chase_line_allocable_qty/oli.order_qty size2_value,
                  oli.received_size2, oli.shipped_size2,
                  0 units_pakd, oli.description, oli.ext_purchase_order, oli.purchase_order_number, oli.ref_num1, oli.ref_num2,
				  oli.ref_num3, oli.ref_num4, oli.freight_revenue_currency_code, oli.ref_num5
               from order_line_item oli
               where order_id=v_orig_line.order_id
               and line_item_id=v_orig_line.line_item_id;

               v_orig_line_adjusted_order_qty := abs(v_orig_line.adjusted_order_qty - v_chase_line_allocable_qty);
               v_orig_line_order_qty      := abs(v_orig_line.order_qty - v_chase_line_allocable_qty);
               v_orig_line_allocated_qty  := abs(v_orig_line.allocated_qty - v_chase_line_allocable_qty);
               v_orig_line_canceled_qty   := abs(v_orig_line.user_canceled_qty - v_chase_line_allocable_qty);
               v_orig_line_received_qty   := abs(v_orig_line.received_qty - v_chase_line_allocable_qty);

               -- update the original order line to reduce the quantity with the chase line created.(i.e user_canceled_qty)
               update order_line_item oli
               set oli.adjusted_order_qty = v_orig_line_adjusted_order_qty
                   ,oli.order_qty = v_orig_line_order_qty
                   ,oli.allocated_qty = v_orig_line_allocated_qty
                   ,oli.user_canceled_qty = v_orig_line_canceled_qty
                   ,oli.received_qty = v_orig_line_received_qty
                   ,oli.last_updated_dttm = sysdate
                   ,oli.last_updated_source = p_user_id
                   ,oli.do_dtl_status =(
                                            case
                                                when v_orig_line_order_qty <= p_zero then 200
                                                when v_orig_line_allocated_qty <= p_zero then 110
                                                when v_orig_line_order_qty - v_orig_line_allocated_qty <= p_zero then
                                                    case
                                                        when coalesce(oli.units_pakd, 0) <= p_zero
                                                            then 130
                                                        when v_orig_line_order_qty - coalesce(oli.shipped_qty, 0)
                                                            <= p_zero then 190
                                                        when v_orig_line_order_qty - coalesce(oli.units_pakd, 0)
                                                            <= p_zero then 150
                                                        else 140
                                                    end
                                                else 120
                                            end
                                           ),
                  oli.is_cancelled = case when v_orig_line_order_qty <= p_zero then 1 else 0 end
               where order_id=v_orig_line.order_id
               and line_item_id=v_orig_line.line_item_id;

               -- reduce the agg line canceled_qty value by the original line for which chase is
               -- created and exit when it is 0
               v_agg_line_user_canceled_qty := v_agg_line_user_canceled_qty - v_orig_line.user_canceled_qty;
               EXIT WHEN v_agg_line_user_canceled_qty <= p_zero;
            end if;
         end loop;
         close orig_line;
      else
         v_chase_line_allocable_qty := v_agg_line_user_canceled_qty;
         insert into order_line_item
         (
             order_id, parent_line_item_id,substituted_parent_line_id, line_item_id,
             ppack_qty, orig_order_qty,adjusted_order_qty, order_qty,allocated_qty,
             do_dtl_status,is_chase_created_line, created_source, created_dttm, item_id,
             invn_type, prod_stat, cntry_of_orgn, batch_nbr, item_attr_1,
             item_attr_2, item_attr_3, item_attr_4, item_attr_5, ship_wave_nbr,
             wave_nbr, is_hazmat,
             is_stackable, has_errors,
             created_source_type,
             tc_order_line_id, actual_cost, actual_cost_currency_code,
             actual_shipped_dttm, allocation_source, allocation_source_id,
             allocation_source_line_id, alloc_line_id, alloc_type,
             assort_nbr, batch_requirement_type, budg_cost, budg_cost_currency_code,
             chute_assign_type, commodity_class,
             commodity_code_id, critcl_dim_1, critcl_dim_2, critcl_dim_3,
             cube_multiple_qty, customer_item,custom_tag, delivery_end_dttm,
             delivery_reference_number, delivery_start_dttm,
             event_code, exp_info_code, ext_sys_line_item_id, fulfillment_type,
             hibernate_version, internal_order_id, internal_order_seq_nbr,
             is_cancelled, is_emergency, item_name,
             last_updated_dttm, last_updated_source, last_updated_source_type,
             line_type, lpn_brk_attrib, lpn_size, lpn_type,
             manufacturing_dttm, master_order_id, merchandizing_department_id,
             merch_grp, merch_type, minor_order_nbr, mo_line_item_id, received_qty,
             shipped_qty, qty_uom_id_base, mv_currency_code,
             mv_size_uom_id, order_consol_attr,
             order_line_id, qty_uom_id, orig_budg_cost, package_type_id, pack_rate,
             pack_zone, pallet_type, partl_fill,
             pickup_end_dttm, pickup_reference_number, pickup_start_dttm,
             pick_locn_assign_type,
             planned_ship_date, ppack_grp_code, price,
             price_tkt_type, priority, product_class_id,
             protection_level_id,
             purchase_order_line_number, reason_code,
             ref_field1, ref_field2, ref_field3, ref_field4, ref_field5, 
             ref_field6, ref_field7, ref_field8, ref_field9, ref_field10, repl_proc_type,
             repl_wave_nbr, repl_wave_run, retail_price,
             rtl_to_be_distroed_qty, rts_id, rts_line_item_id,
             serial_number_required_flag, shelf_days, single_unit_flag,
             sku_break_attr, sku_gtin, sku_sub_code_id, sku_sub_code_value,
             stack_diameter_standard_uom, stack_diameter_value,
             stack_height_standard_uom, stack_height_value,
             stack_length_standard_uom, stack_length_value, stack_rank,
             stack_width_standard_uom, stack_width_value, std_bundle_qty,
             std_lpn_qty, std_lpn_vol, std_lpn_wt, std_pack_qty, std_pallet_qty,
             std_sub_pack_qty, store_dept, tc_company_id,
             total_monetary_value, unit_cost, unit_monetary_value, unit_price_amount,
             unit_tax_amount, unit_vol, unit_wt, un_number_id, user_canceled_qty,
             vas_process_type, wave_proc_type,
             qty_conv_factor, received_weight,
             planned_weight, shipped_weight, weight_uom_id_base, weight_uom_id,
             planned_volume, received_volume, shipped_volume, volume_uom_id_base,
             volume_uom_id, size1_uom_id, size1_value, received_size1, shipped_size1,
             size2_uom_id, size2_value, received_size2, shipped_size2,
             units_pakd, description,  ext_purchase_order, purchase_order_number, ref_num1, ref_num2, ref_num3,
			 ref_num4, freight_revenue_currency_code,ref_num5
         )
             select oli.order_id, oli.line_item_id parent_line_item_id,
             oli.line_item_id substituted_parent_line_id,
             seq_line_item_id.nextval line_item_id,oli.ppack_qty,
             oli.orig_order_qty orig_order_qty,
             v_chase_line_allocable_qty adjusted_order_qty,
                 v_chase_line_allocable_qty order_qty,
                 v_chase_line_allocable_qty allocated_qty,
             130,1,
             p_user_id created_source, sysdate created_dttm, oli.item_id, oli.invn_type,
             oli.prod_stat, oli.cntry_of_orgn, oli.batch_nbr, oli.item_attr_1, oli.item_attr_2,
             oli.item_attr_3, oli.item_attr_4, oli.item_attr_5, p_ship_wave_nbr,
             p_pick_wave_nbr wave_nbr, oli.is_hazmat,
             oli.is_stackable, oli.has_errors,
             oli.created_source_type,
             to_char(seq_line_item_id.nextval) tc_order_line_id,
             oli.actual_cost, oli.actual_cost_currency_code, oli.actual_shipped_dttm,
             oli.allocation_source, oli.allocation_source_id,
             oli.allocation_source_line_id, oli.alloc_line_id, oli.alloc_type,
             oli.assort_nbr, oli.batch_requirement_type, oli.budg_cost,
             oli.budg_cost_currency_code,
             oli.chute_assign_type, oli.commodity_class, oli.commodity_code_id,
             oli.critcl_dim_1, oli.critcl_dim_2, oli.critcl_dim_3,
             oli.cube_multiple_qty, oli.customer_item, oli.custom_tag,
             oli.delivery_end_dttm, oli.delivery_reference_number,
             oli.delivery_start_dttm, oli.event_code,
             oli.exp_info_code, oli.ext_sys_line_item_id, oli.fulfillment_type,
             oli.hibernate_version, oli.internal_order_id,
             oli.internal_order_seq_nbr,oli.is_cancelled,oli.is_emergency,
             oli.item_name, sysdate last_updated_dttm,
             p_user_id last_updated_source, oli.last_updated_source_type,
             oli.line_type, oli.lpn_brk_attrib, oli.lpn_size,
             oli.lpn_type, oli.manufacturing_dttm, oli.master_order_id,
             oli.merchandizing_department_id, oli.merch_grp, oli.merch_type,
             oli.minor_order_nbr, oli.mo_line_item_id, 0 received_qty,
             0 shipped_qty, oli.qty_uom_id_base,
             oli.mv_currency_code, oli.mv_size_uom_id,
             oli.order_consol_attr, oli.order_line_id,
             oli.qty_uom_id, oli.orig_budg_cost, oli.package_type_id, oli.pack_rate,
             oli.pack_zone, oli.pallet_type, oli.partl_fill,
             oli.pickup_end_dttm, oli.pickup_reference_number,
             oli.pickup_start_dttm, oli.pick_locn_assign_type,
             oli.planned_ship_date, oli.ppack_grp_code,
             oli.price, oli.price_tkt_type, oli.priority, oli.product_class_id,
             oli.protection_level_id,
             oli.purchase_order_line_number,'45' reason_code,
             oli.ref_field1, oli.ref_field2, oli.ref_field3, oli.ref_field4, 
             oli.ref_field5, oli.ref_field6, oli.ref_field7, oli.ref_field8, 
             oli.ref_field9, oli.ref_field10,
             oli.repl_proc_type, '' repl_wave_nbr, oli.repl_wave_run,
             oli.retail_price, oli.rtl_to_be_distroed_qty,
             oli.rts_id, oli.rts_line_item_id,
             oli.serial_number_required_flag, oli.shelf_days, oli.single_unit_flag,
             oli.sku_break_attr, oli.sku_gtin, oli.sku_sub_code_id,
             oli.sku_sub_code_value, oli.stack_diameter_standard_uom,
             oli.stack_diameter_value, oli.stack_height_standard_uom,
             oli.stack_height_value, oli.stack_length_standard_uom,
             oli.stack_length_value, oli.stack_rank, oli.stack_width_standard_uom,
             oli.stack_width_value,oli.std_bundle_qty ,oli.std_lpn_qty ,oli.std_lpn_vol ,
             oli.std_lpn_wt ,oli.std_pack_qty ,oli.std_pallet_qty ,oli.std_sub_pack_qty ,
             oli.store_dept, oli.tc_company_id,
             oli.total_monetary_value, oli.unit_cost, oli.unit_monetary_value,
             oli.unit_price_amount, oli.unit_tax_amount, oli.unit_vol, oli.unit_wt,
             oli.un_number_id, 0 user_canceled_qty, oli.vas_process_type,
             oli.wave_proc_type,oli.qty_conv_factor,oli.received_weight,
             case when oli.weight_uom_id_base is null then null
                  else oli.planned_weight * v_chase_line_allocable_qty/oli.order_qty
                  end planned_weight,
             oli.shipped_weight, oli.weight_uom_id_base, oli.weight_uom_id,
             case when oli.volume_uom_id_base is null then null
                  else oli.planned_volume * v_chase_line_allocable_qty/oli.order_qty
                  end planned_volume
                 ,oli.received_volume, oli.shipped_volume,
             oli.volume_uom_id_base, oli.volume_uom_id, oli.size1_uom_id,
             oli.size1_value * v_chase_line_allocable_qty/oli.order_qty size1_value,
             oli.received_size1, oli.shipped_size1, oli.size2_uom_id,
             oli.size2_value * v_chase_line_allocable_qty/oli.order_qty size2_value,
             oli.received_size2, oli.shipped_size2,
             0 units_pakd, oli.description, oli.ext_purchase_order, oli.purchase_order_number, oli.ref_num1, oli.ref_num2,
			 oli.ref_num3, oli.ref_num4, oli.freight_revenue_currency_code,oli.ref_num5
         from order_line_item oli
         where order_id=v_agg_line.order_id
         and line_item_id=v_agg_line.line_item_id;
      end if;

      -- reduce the user_canceled_qty values from AGG line item.
      update order_line_item oli
      set oli.adjusted_order_qty = abs(oli.adjusted_order_qty - v_agg_line.user_canceled_qty)
          ,oli.order_qty = abs(oli.order_qty - v_agg_line.user_canceled_qty)
          ,oli.allocated_qty = abs(oli.allocated_qty - v_agg_line.user_canceled_qty)
          ,oli.user_canceled_qty= abs(oli.user_canceled_qty - v_agg_line.user_canceled_qty)
          ,oli.received_qty= abs(oli.received_qty - v_agg_line.user_canceled_qty)
          ,oli.last_updated_dttm = sysdate
          ,oli.last_updated_source = p_user_id
          ,oli.do_dtl_status =(
                               case
                                   when abs(oli.order_qty - v_agg_line.user_canceled_qty) <= p_zero then 200
                                   when abs(oli.order_qty - oli.allocated_qty) <= p_zero then
                                       case
                                           when coalesce(oli.units_pakd,0) <= p_zero then 130
                                           when abs((oli.order_qty - v_agg_line.user_canceled_qty)- oli.shipped_qty) <= p_zero then 190
                                           when abs((oli.order_qty - v_agg_line.user_canceled_qty)- oli.units_pakd) <= p_zero then 150
                                           else 140
                                       end
                                   else 120 
                               end
                              ),
          oli.is_cancelled = case when abs(oli.order_qty - v_agg_line.user_canceled_qty) <= p_zero
              then 1 else 0 end
      where order_id=v_agg_line.order_id
      and line_item_id=v_agg_line.line_item_id;

   end loop;
   close agg_line;

   -- mark these soft allocations as having been taken care of
   update tmp_ord_dtl_sku_invn t
   set t.is_flushed = 1
   where t.is_flushed = 0;
   commit;
end;
/
show errors;

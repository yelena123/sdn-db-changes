CREATE OR REPLACE FORCE VIEW PIX_ACTIVITY
(
   tran_type,
   tran_code,
   actn_code,
   activity,
   transaction_type,
   activity_id
)
AS
   (SELECT DISTINCT
           tran_type,
           tran_code,
           actn_code,
           (CASE
               WHEN tran_type = '100' AND tran_code = '01'
               THEN
                  'WMInHouseRcpt'
               WHEN tran_type = '100' AND tran_code = '02'
               THEN
                  'WMOutSourceRcpt'
               WHEN tran_type = '100' AND tran_code = '03'
               THEN
                  'WMReturnRcpt'
               WHEN tran_type = '100' AND tran_code = '04'
               THEN
                  'WMXdockrcpt'
               WHEN tran_type = '300' AND tran_code = '01'
               THEN
                  'WMMaintiLPN'
               WHEN tran_type = '300' AND tran_code = '02'
               THEN
                  'WMMaintoLPN'
               WHEN tran_type = '300' AND tran_code = '04'
               THEN
                  'WMMaintActive'
               WHEN tran_type = '300' AND tran_code = '05'
               THEN
                  'WMMaintTrans'
               WHEN tran_type = '300' AND tran_code = '09'
               THEN
                  'WMMaintiLPNPick'
               WHEN tran_type = '606' AND tran_code = '01'
               THEN
                  'WMTransUnalloc'
               WHEN     tran_type = '606'
                    AND tran_code = '02'
                    AND actn_code <> '02'
               THEN
                  'WMiLPNUnalloc'
               WHEN     tran_type = '606'
                    AND tran_code = '02'
                    AND actn_code = '02'
               THEN
                  'WMTransUnalloc'
               WHEN     tran_type = '606'
                    AND tran_code IN ('03', '04')
                    AND actn_code = '*'
               THEN
                  'WMRcptUnalloc'
               WHEN     tran_type = '606'
                    AND tran_code = '04'
                    AND actn_code <> '*'
               THEN
                  'WMActiveUnalloc'
               WHEN tran_type = '606' AND tran_code = '05'
               THEN
                  'WMWOUnalloc'
               WHEN tran_type = '606' AND tran_code = '09'
               THEN
                  'WMiLPNPkUnalloc'
               WHEN tran_type = '608' AND tran_code = '12'
               THEN
                  'WMLockCodeChg'
            END)
              activity,
           (CASE
               WHEN tran_type = '100'
               THEN
                  'Receipt'
               WHEN     tran_type = '300'
                    AND tran_code = '01'
                    AND actn_code IN ('08', '10', '21', '30')
               THEN
                  'Receipt'
               WHEN     tran_type = '300'
                    AND tran_code IN ('04', '09')
                    AND actn_code IN ('21', '30')
               THEN
                  'Receipt'
               WHEN     tran_type = '606'
                    AND tran_code = '02'
                    AND actn_code IN ('08', '10', '21', '30')
               THEN
                  'Receipt'
               WHEN     tran_type = '606'
                    AND tran_code IN ('03', '04')
                    AND actn_code = '*'
               THEN
                  'Receipt'
               WHEN     tran_type = '606'
                    AND tran_code IN ('04', '09')
                    AND actn_code IN ('30')
               THEN
                  'Receipt'
               WHEN tran_type = '300' AND tran_code = '01'
                    AND actn_code IN
                           ('01',
                            '02',
                            '07',
                            '09',
                            '11',
                            '13',
                            '14',
                            '15',
                            '17',
                            '18')
               THEN
                  'Adjustment'
               WHEN tran_type = '300' AND tran_code = '02'
               THEN
                  'Adjustment'
               WHEN     tran_type = '300'
                    AND tran_code IN ('04', '09')
                    AND (actn_code IN ('*', '14'))
               THEN
                  'Adjustment'
               WHEN tran_type = '300' AND tran_code = '05'
               THEN
                  'Adjustment'
               WHEN tran_type = '606' AND tran_code = '01'
               THEN
                  'Adjustment'
               WHEN tran_type = '606' AND tran_code = '02'
                    AND actn_code IN
                           ('01',
                            '07',
                            '09',
                            '11',
                            '13',
                            '14',
                            '15',
                            '17',
                            '18')
               THEN
                  'Adjustment'
               WHEN     tran_type = '606'
                    AND tran_code IN ('04', '09')
                    AND actn_code IN ('03', '14')
               THEN
                  'Adjustment'
               WHEN tran_type = '606' AND tran_code = '05'
               THEN
                  'Adjustment'
               WHEN     tran_type = '606'
                    AND tran_code = '02'
                    AND actn_code IN ('02')
               THEN
                  'Adjustment'
               WHEN     tran_type = '606'
                    AND tran_code = '02'
                    AND actn_code IN ('05', '06', '19', '20')
               THEN
                  'Movement'
               WHEN     tran_type = '606'
                    AND tran_code IN ('04', '09')
                    AND actn_code IN ('01', '02')
               THEN
                  'Movement'
               WHEN tran_type = '608' AND tran_code = '12'
               THEN
                  'Movement'
            END)
              transaction_type,
           (CASE
               WHEN     tran_type = '100'
                    AND tran_code = '01'
                    AND actn_code = '*'
               THEN
                  '100-01-*'
               WHEN     tran_type = '100'
                    AND tran_code = '02'
                    AND actn_code = '*'
               THEN
                  '100-02-*'
               WHEN     tran_type = '100'
                    AND tran_code = '03'
                    AND actn_code = '*'
               THEN
                  '100-03-*'
               WHEN     tran_type = '100'
                    AND tran_code = '04'
                    AND actn_code = '*'
               THEN
                  '100-04-*'
               WHEN     tran_type = '300'
                    AND tran_code = '01'
                    AND actn_code = '01'
               THEN
                  '300-01-01'
               WHEN     tran_type = '300'
                    AND tran_code = '01'
                    AND actn_code = '02'
               THEN
                  '300-01-02'
               WHEN     tran_type = '300'
                    AND tran_code = '01'
                    AND actn_code = '07'
               THEN
                  '300-01-07'
               WHEN     tran_type = '300'
                    AND tran_code = '01'
                    AND actn_code = '08'
               THEN
                  '300-01-08'
               WHEN     tran_type = '300'
                    AND tran_code = '01'
                    AND actn_code = '09'
               THEN
                  '300-01-09'
               WHEN     tran_type = '300'
                    AND tran_code = '01'
                    AND actn_code = '10'
               THEN
                  '300-01-10'
               WHEN     tran_type = '300'
                    AND tran_code = '01'
                    AND actn_code = '11'
               THEN
                  '300-01-11'
               WHEN     tran_type = '300'
                    AND tran_code = '01'
                    AND actn_code = '13'
               THEN
                  '300-01-13'
               WHEN     tran_type = '300'
                    AND tran_code = '01'
                    AND actn_code = '14'
               THEN
                  '300-01-14'
               WHEN     tran_type = '300'
                    AND tran_code = '01'
                    AND actn_code = '15'
               THEN
                  '300-01-15'
               WHEN     tran_type = '300'
                    AND tran_code = '01'
                    AND actn_code = '17'
               THEN
                  '300-01-17'
               WHEN     tran_type = '300'
                    AND tran_code = '01'
                    AND actn_code = '18'
               THEN
                  '300-01-18'
               WHEN     tran_type = '300'
                    AND tran_code = '01'
                    AND actn_code = '21'
               THEN
                  '300-01-21'
               WHEN     tran_type = '300'
                    AND tran_code = '01'
                    AND actn_code = '30'
               THEN
                  '300-01-30'
               WHEN     tran_type = '300'
                    AND tran_code = '02'
                    AND actn_code = '*'
               THEN
                  '300-02-*'
               WHEN     tran_type = '300'
                    AND tran_code = '04'
                    AND actn_code = '*'
               THEN
                  '300-04-*'
               WHEN     tran_type = '300'
                    AND tran_code = '04'
                    AND actn_code = '14'
               THEN
                  '300-04-14'
               WHEN     tran_type = '300'
                    AND tran_code = '04'
                    AND actn_code = '21'
               THEN
                  '300-04-21'
               WHEN     tran_type = '300'
                    AND tran_code = '04'
                    AND actn_code = '30'
               THEN
                  '300-04-30'
               WHEN     tran_type = '300'
                    AND tran_code = '05'
                    AND actn_code = '*'
               THEN
                  '300-05-*'
               WHEN     tran_type = '300'
                    AND tran_code = '05'
                    AND actn_code = '02'
               THEN
                  '300-05-02'
               WHEN     tran_type = '300'
                    AND tran_code = '05'
                    AND actn_code = '03'
               THEN
                  '300-05-03'
               WHEN     tran_type = '300'
                    AND tran_code = '05'
                    AND actn_code = '05'
               THEN
                  '300-05-05'
               WHEN     tran_type = '300'
                    AND tran_code = '09'
                    AND actn_code = '*'
               THEN
                  '300-09-*'
               WHEN     tran_type = '300'
                    AND tran_code = '09'
                    AND actn_code = '14'
               THEN
                  '300-09-14'
               WHEN     tran_type = '300'
                    AND tran_code = '09'
                    AND actn_code = '21'
               THEN
                  '300-09-21'
               WHEN     tran_type = '300'
                    AND tran_code = '09'
                    AND actn_code = '30'
               THEN
                  '300-09-30'
               WHEN     tran_type = '603'
                    AND tran_code = '01'
                    AND actn_code = '*'
               THEN
                  '603-01-*'
               WHEN     tran_type = '603'
                    AND tran_code = '01'
                    AND actn_code = '03'
               THEN
                  '603-01-03'
               WHEN     tran_type = '606'
                    AND tran_code = '01'
                    AND actn_code = '*'
               THEN
                  '606-01-*'
               WHEN     tran_type = '606'
                    AND tran_code = '02'
                    AND actn_code = '01'
               THEN
                  '606-02-01'
               WHEN     tran_type = '606'
                    AND tran_code = '02'
                    AND actn_code = '02'
               THEN
                  '606-02-02'
               WHEN     tran_type = '606'
                    AND tran_code = '02'
                    AND actn_code = '05'
               THEN
                  '606-02-05'
               WHEN     tran_type = '606'
                    AND tran_code = '02'
                    AND actn_code = '06'
               THEN
                  '606-02-06'
               WHEN     tran_type = '606'
                    AND tran_code = '02'
                    AND actn_code = '07'
               THEN
                  '606-02-07'
               WHEN     tran_type = '606'
                    AND tran_code = '02'
                    AND actn_code = '08'
               THEN
                  '606-02-08'
               WHEN     tran_type = '606'
                    AND tran_code = '02'
                    AND actn_code = '09'
               THEN
                  '606-02-09'
               WHEN     tran_type = '606'
                    AND tran_code = '02'
                    AND actn_code = '10'
               THEN
                  '606-02-10'
               WHEN     tran_type = '606'
                    AND tran_code = '02'
                    AND actn_code = '11'
               THEN
                  '606-02-11'
               WHEN     tran_type = '606'
                    AND tran_code = '02'
                    AND actn_code = '13'
               THEN
                  '606-02-13'
               WHEN     tran_type = '606'
                    AND tran_code = '02'
                    AND actn_code = '14'
               THEN
                  '606-02-14'
               WHEN     tran_type = '606'
                    AND tran_code = '02'
                    AND actn_code = '15'
               THEN
                  '606-02-15'
               WHEN     tran_type = '606'
                    AND tran_code = '02'
                    AND actn_code = '17'
               THEN
                  '606-02-17'
               WHEN     tran_type = '606'
                    AND tran_code = '02'
                    AND actn_code = '18'
               THEN
                  '606-02-18'
               WHEN     tran_type = '606'
                    AND tran_code = '02'
                    AND actn_code = '19'
               THEN
                  '606-02-19'
               WHEN     tran_type = '606'
                    AND tran_code = '02'
                    AND actn_code = '20'
               THEN
                  '606-02-20'
               WHEN     tran_type = '606'
                    AND tran_code = '02'
                    AND actn_code = '21'
               THEN
                  '606-02-21'
               WHEN     tran_type = '606'
                    AND tran_code = '02'
                    AND actn_code = '30'
               THEN
                  '606-02-30'
               WHEN     tran_type = '606'
                    AND tran_code = '03'
                    AND actn_code = '*'
               THEN
                  '606-03-*'
               WHEN     tran_type = '606'
                    AND tran_code = '04'
                    AND actn_code = '*'
               THEN
                  '606-04-*'
               WHEN     tran_type = '606'
                    AND tran_code = '04'
                    AND actn_code = '01'
               THEN
                  '606-04-01'
               WHEN     tran_type = '606'
                    AND tran_code = '04'
                    AND actn_code = '02'
               THEN
                  '606-04-02'
               WHEN     tran_type = '606'
                    AND tran_code = '04'
                    AND actn_code = '03'
               THEN
                  '606-04-03'
               WHEN     tran_type = '606'
                    AND tran_code = '04'
                    AND actn_code = '14'
               THEN
                  '606-04-14'
               WHEN     tran_type = '606'
                    AND tran_code = '04'
                    AND actn_code = '30'
               THEN
                  '606-04-30'
               WHEN     tran_type = '606'
                    AND tran_code = '05'
                    AND actn_code = '02'
               THEN
                  '606-05-02'
               WHEN     tran_type = '606'
                    AND tran_code = '05'
                    AND actn_code = '03'
               THEN
                  '606-05-03'
               WHEN     tran_type = '606'
                    AND tran_code = '05'
                    AND actn_code = '05'
               THEN
                  '606-05-05'
               WHEN     tran_type = '606'
                    AND tran_code = '09'
                    AND actn_code = '01'
               THEN
                  '606-09-01'
               WHEN     tran_type = '606'
                    AND tran_code = '09'
                    AND actn_code = '02'
               THEN
                  '606-09-02'
               WHEN     tran_type = '606'
                    AND tran_code = '09'
                    AND actn_code = '03'
               THEN
                  '606-09-03'
               WHEN     tran_type = '606'
                    AND tran_code = '09'
                    AND actn_code = '14'
               THEN
                  '606-09-14'
               WHEN     tran_type = '606'
                    AND tran_code = '09'
                    AND actn_code = '30'
               THEN
                  '606-09-30'
               WHEN     tran_type = '608'
                    AND tran_code = '12'
                    AND actn_code = '05'
               THEN
                  '608-12-05'
               WHEN     tran_type = '608'
                    AND tran_code = '12'
                    AND actn_code = '06'
               THEN
                  '608-12-06'
               WHEN     tran_type = '608'
                    AND tran_code = '12'
                    AND actn_code = '19'
               THEN
                  '608-12-19'
               WHEN     tran_type = '608'
                    AND tran_code = '12'
                    AND actn_code = '20'
               THEN
                  '608-12-20'
               WHEN     tran_type = '608'
                    AND tran_code = '12'
                    AND actn_code = '21'
               THEN
                  '608-12-21'
               WHEN     tran_type = '608'
                    AND tran_code = '12'
                    AND actn_code = '22'
               THEN
                  '608-12-22'
            END)
              activity_id
      FROM pix_tran_code
     WHERE tran_type IN ('100', '300', '606', '608'));
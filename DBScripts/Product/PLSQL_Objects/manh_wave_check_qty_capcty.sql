create or replace procedure manh_wave_check_qty_capcty
(
    p_user_id                   in ucl_user.user_name%type,
    p_pull_all_swc              in wave_parm.pull_all_swc%type,
    p_reject_distro_rule        in wave_parm.reject_distro_rule%type,
    p_remaining_qty_capcty      in wave_parm.max_units%type
)
as
    v_rsn_code varchar2(1) default '2';
    v_first_rejected_line number(9) := 0;
begin
    -- exclude lines that were a part of selection due to swc; collect soft
    -- alloc lines with lines that exceed the remaining cap; aside from 
    -- these specific lines, collect a) swcs b) items c) orders
    select coalesce(min(iv.id), 0)
    into v_first_rejected_line
    from
    (
        select sum(t2.qty_soft_alloc) over(order by t1.id) rng_sum, t1.id id
        from tmp_wave_selected_orders t1
        join tmp_ord_dtl_sku_invn t2 on t2.order_id = t1.order_id
            and t2.line_item_id = t1.line_item_id
        where t1.is_swc_pull = 0
    ) iv
    where iv.rng_sum > p_remaining_qty_capcty;
    wm_cs_log('First rejected line was ' || v_first_rejected_line 
        || ' when remaining Qty cap was ' || p_remaining_qty_capcty, p_sql_log_level => 1);

    if (v_first_rejected_line = 0)
    then
        return;
    end if;
            
    delete from tmp_wave_rejected_lines;
    insert into tmp_wave_rejected_lines
    (
        order_id, line_item_id, item_id, ship_group_id
    )
    select t3.order_id, t3.line_item_id,
        case when p_reject_distro_rule = '2' then t3.item_id 
            else null end item_id,
        case when p_pull_all_swc = 'Y' then t3.ship_group_id
            else null end ship_group_id
    from tmp_wave_selected_orders t3
    where t3.id >= v_first_rejected_line and t3.is_swc_pull = 0
        and exists
        (
            select 1
            from tmp_ord_dtl_sku_invn t2
            where t2.order_id = t3.order_id
                and t2.line_item_id = t3.line_item_id
        );
    wm_cs_log('Rejected lines captured for Qty check ' || sql%rowcount, p_sql_log_level => 1);

    manh_wave_capcty_rejections(p_user_id, p_pull_all_swc, p_reject_distro_rule,
        v_rsn_code);
end;
/

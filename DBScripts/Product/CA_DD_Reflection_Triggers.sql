EXEC quiet_drop('TRIGGER','CREDIT_CARD_TRIGGER') ;
EXEC quiet_drop('TRIGGER','COMPANY_AI')  ;
EXEC quiet_drop('TRIGGER','A_CUSTOM_ATTR_TRIGGER')  ;
EXEC quiet_drop('TRIGGER','COMPANY_AI_TLM') ;
--EXEC quiet_drop('TRIGGER','COMPANY_CA');
EXEC quiet_drop('TRIGGER','USR_AFT_DEL');
EXEC quiet_drop('TRIGGER','USR_AFT_INS') ;
EXEC quiet_drop('TRIGGER','COMPANY_APP_DOM') ;
EXEC SEQUPDT('PRICE_TYPE_CBO','PRICE_TYPE_ID','PRICE_TYPE_ID_SEQ');


CREATE or REPLACE TRIGGER CREDIT_CARD_TRIGGER 
AFTER 
INSERT ON &1..COMPANY
REFERENCING OLD AS o NEW AS n 
FOR EACH ROW
BEGIN
INSERT INTO A_CREDIT_CARD 
(CARD_ID,COMPANY_ID,CARD_TYPE_ID,CARD_NAME,DESCRIPTION, 
MARK_FOR_DELETION, SYSTEM_DEFINED) 
select SEQ_A_CREDIT_CARD.nextval,:n.company_id,CARD_TYPE_ID, CARD_NAME, 
DESCRIPTION, 0 , 1
from A_CREDIT_CARD where COMPANY_ID = 0;
end;
/

ALTER TRIGGER CREDIT_CARD_TRIGGER ENABLE;



CREATE OR REPLACE TRIGGER COMPANY_AI
 AFTER INSERT
 ON &1..COMPANY
 REFERENCING NEW AS NEW
 FOR EACH ROW
DECLARE
 V_DL_ID     NUMBER;
 V_COUNT     NUMBER;
 VGENID      NUMBER;
 V_SHIP_ID   NUMBER;
BEGIN
 SELECT COMPANY_TYPE_ID
 INTO V_SHIP_ID
 FROM COMPANY_TYPE
WHERE LOWER (DESCRIPTION) = LOWER ('Shipper');
 IF (V_SHIP_ID = :NEW.COMPANY_TYPE_ID)
 THEN
  INSERT INTO RS_CONFIG (RS_CONFIG_ID,
                         TC_COMPANY_ID,
                         RS_CONFIG_NAME,
                         IS_VALID,
                         IS_ACTIVE,
                         CREATED_SOURCE_TYPE,
                         CREATED_SOURCE,
                         LAST_UPDATED_SOURCE_TYPE,
                         LAST_UPDATED_SOURCE)
       VALUES (SEQ_RS_CONFIG_ID.NEXTVAL,
               :NEW.COMPANY_ID,
               'Default',
               1,
               1,
               2,
               'LDAP',
               2,
               'LDAP');
  INSERT INTO PERFORMANCE_FACTOR (PERFORMANCE_FACTOR_ID,
                                  TC_COMPANY_ID,
                                  PERFORMANCE_FACTOR_NAME,
                                  PF_TYPE,
                                  DESCRIPTION,
                                  DEFAULT_VALUE,
                                  IS_USE_FLAG,
                                  CREATED_SOURCE_TYPE,
                                  CREATED_SOURCE,
                                  LAST_UPDATED_SOURCE_TYPE,
                                  LAST_UPDATED_SOURCE)
       VALUES (SEQ_PERFORMANCE_FACTOR_ID.NEXTVAL,
               :NEW.COMPANY_ID,
               'ON TIME',
               3,
               'On Time',
               0,
               0,
               2,
               'LDAP',
               2,
               'LDAP');
  INSERT INTO PERFORMANCE_FACTOR (PERFORMANCE_FACTOR_ID,
                                  TC_COMPANY_ID,
                                  PERFORMANCE_FACTOR_NAME,
                                  PF_TYPE,
                                  DESCRIPTION,
                                  DEFAULT_VALUE,
                                  IS_USE_FLAG,
                                  CREATED_SOURCE_TYPE,
                                  CREATED_SOURCE,
                                  LAST_UPDATED_SOURCE_TYPE,
                                  LAST_UPDATED_SOURCE)
       VALUES (SEQ_PERFORMANCE_FACTOR_ID.NEXTVAL,
               :NEW.COMPANY_ID,
               'ACCEPT RATIO',
               2,
               'Accept Rat',
               0,
               0,
               2,
               'LDAP',
               2,
               'LDAP');
  INSERT INTO RS_AREA (TC_COMPANY_ID,
                       RS_AREA_ID,
                       RS_AREA,
                       DESCRIPTION,
                       COMMENTS,
                       CREATED_SOURCE,
                       LAST_UPDATED_SOURCE)
       VALUES (
                 :NEW.COMPANY_ID,
                 RS_AREA_ID_SEQ.NEXTVAL,
                 'RSArealess',
                 'RSArealess',
                 'For Internal Use Only.  Used for shipments that can not be assigned to a RSArea.',
                 '0',
                 '0');
  INSERT INTO ILM_REASON_CODES (REASON_CODE_ID,
                                TYPE,
                                TC_COMPANY_ID,
                                DESCRIPTION)
       VALUES (17,
               'TRLR',
               :NEW.COMPANY_ID,
               'Yard Audit Lock');
  INSERT INTO ILM_REASON_CODES (REASON_CODE_ID,
                                TYPE,
                                TC_COMPANY_ID,
                                DESCRIPTION)
       VALUES (12,
               'TASK',
               :NEW.COMPANY_ID,
               'A trailer already in location');
  INSERT INTO CLAIM_ACTION_CODE (CLAIM_ACTION_CODE,
                                 CLAIM_ACTION_TYPE,
                                 TC_COMPANY_ID,
                                 DESCRIPTION)
     SELECT CLAIM_ACTION_CODE,
            CLAIM_ACTION_TYPE,
            :NEW.COMPANY_ID,
            DESCRIPTION
       FROM CLAIM_SYSTEM_ACTION_CODE SA
      WHERE NOT EXISTS
                   (SELECT 1
                      FROM CLAIM_ACTION_CODE A
                     WHERE A.CLAIM_ACTION_CODE = SA.CLAIM_ACTION_CODE
                           AND A.TC_COMPANY_ID = :NEW.COMPANY_ID);
 END IF;
 INSERT INTO DISTRIBUTION_LIST (TC_COMPANY_ID,
                              DESCRIPTION,
                              IS_PREFERRED_LIST,
                              TC_CONTACT_ID)
    VALUES (:NEW.COMPANY_ID,
            'All Company Carriers',
            1,
            0);
 INSERT INTO DISTRIBUTION_LIST (TC_COMPANY_ID,
                              DESCRIPTION,
                              IS_PREFERRED_LIST,
                              TC_CONTACT_ID)
    VALUES (:NEW.COMPANY_ID,
            'All Lane Carriers',
            2,
            0);
 UPDATE RS_CONFIG
  SET RS_CONFIG_RANK = NULL
WHERE TC_COMPANY_ID = :NEW.COMPANY_ID;
 INSERT INTO INVOICE_RESP_CODE
    VALUES (SEQ_INVOICE_RESP_CODE_ID.NEXTVAL,
            :NEW.COMPANY_ID,
            'ADTR',
            'FAP Auditor',
            0,
            0);
 INSERT INTO INVOICE_RESP_CODE
    VALUES (SEQ_INVOICE_RESP_CODE_ID.NEXTVAL,
            :NEW.COMPANY_ID,
            'CARR',
            'Carrier',
            0,
            1);
 INSERT INTO INVOICE_RESP_CODE
    VALUES (SEQ_INVOICE_RESP_CODE_ID.NEXTVAL,
            :NEW.COMPANY_ID,
            'CRLS',
            'Carrier Relations',
            0,
            0);
 INSERT INTO INVOICE_RESP_CODE
    VALUES (SEQ_INVOICE_RESP_CODE_ID.NEXTVAL,
            :NEW.COMPANY_ID,
            'MNGR',
            'FAP Manager',
            0,
            0);
 INSERT INTO INVOICE_RESP_CODE
    VALUES (SEQ_INVOICE_RESP_CODE_ID.NEXTVAL,
            :NEW.COMPANY_ID,
            'DTRC',
            'Detention Resolution Center',
            0,
            0);
 INSERT INTO PRICE_TYPE_CBO (PRICE_TYPE_ID,
                           COMPANY_ID,
                           AUDIT_CREATED_SOURCE,
                           AUDIT_LAST_UPDATED_SOURCE,
                           OWNED_BY,
                           AUDIT_LAST_UPDATED_DTTM,
                           VERSION,
                           AUDIT_CREATED_DTTM,
                           NAME,
                           DESCRIPTION,
                           AUDIT_LAST_UPDATED_SOURCE_TYPE,
                           AUDIT_CREATED_SOURCE_TYPE,
                           AUDIT_TRANSACTION,
                           AUDIT_PARTY_ID,
                           MARK_FOR_DELETION)
    VALUES (PRICE_TYPE_ID_SEQ.NEXTVAL,
            :NEW.COMPANY_ID,
            '1',
            '1',
            11,
            SYSDATE,
            1,
            SYSDATE,
            'REGULAR',
            '11',
            1,
            11,
            '11',
            11,
            0);
 INSERT INTO PRICE_TYPE_CBO (PRICE_TYPE_ID,
                           COMPANY_ID,
                           AUDIT_CREATED_SOURCE,
                           AUDIT_LAST_UPDATED_SOURCE,
                           OWNED_BY,
                           AUDIT_LAST_UPDATED_DTTM,
                           VERSION,
                           AUDIT_CREATED_DTTM,
                           NAME,
                           DESCRIPTION,
                           AUDIT_LAST_UPDATED_SOURCE_TYPE,
                           AUDIT_CREATED_SOURCE_TYPE,
                           AUDIT_TRANSACTION,
                           AUDIT_PARTY_ID,
                           MARK_FOR_DELETION)
    VALUES (PRICE_TYPE_ID_SEQ.NEXTVAL,
            :NEW.COMPANY_ID,
            '1',
            '1',
            11,
            SYSDATE,
            1,
            SYSDATE,
            'PROMOTIONAL',
            '12',
            1,
            11,
            '11',
            11,
            0);
 IF (:NEW.PARENT_COMPANY_ID = -1)
 THEN
  INSERT INTO OM_SCHED_EVENT (EVENT_ID,
                              SCHEDULED_DTTM,
                              EVENT_OBJECTS,
                              EVENT_TIMESTAMP,
                              OM_CATEGORY,
                              EVENT_TYPE,
                              EVENT_EXP_DATE,
                              EVENT_CNT,
                              EVENT_FREQ_IN_DAYS,
                              EVENT_FREQ_PER_DAY,
                              EXECUTED_DTTM,
                              IS_EXECUTED,
                              EVENT_FREQ_IN_DAY_OF_MONTH)
       VALUES (
                 SEQ_EVENT_ID.NEXTVAL,
                 SYSDATE,
                 '{eventProcessorClass=com.manh.baseservices.util.defaultbasedata.AutoCreateBaseDataScheduler, shipperPK='
                 || TO_CHAR (:NEW.COMPANY_ID)
                 || '}',
                 SYSDATE,
                 null,
                 0,
                 NULL,
                 0,
                 0,
                 0,
                 NULL,
                 0,
                 0);
 END IF;
END;
/

ALTER TRIGGER COMPANY_AI ENABLE;

CREATE OR REPLACE TRIGGER A_CUSTOM_ATTR_TRIGGER
AFTER
INSERT ON &1..COMPANY
REFERENCING OLD AS o NEW AS n
FOR EACH ROW
BEGIN
IF(:n.company_type_id = 4)
THEN
  insert into CUSTOM_ATTRIBUTE
  (TC_COMPANY_ID, ATTRIBUTE_NAME, DATA_TYPE, LABEL, OBJECT_TYPE, CUSTOM_ATTRIBUTE_ID,
  CREATE_SOURCE_TYPE, CREATED_SOURCE, CREATED_DTTM, LAST_UPDATED_SOURCE_TYPE, LAST_UPDATED_SOURCE, LAST_UPDATED_DTTM,
  MARK_FOR_DELETION, IS_VALUE_REQUIRED, IS_LOCALIZABLE, IS_REQUIRED_FOR_REPORT_FILTER, IS_PARTICIPATES_IN_REPORTS, IS_PARTICIPATES_RR_QUERY,
  IS_REQUIRED_FOR_FILTER, IS_PARTICIPATES_IN_FILTER, FOR_RESOURCE_SELECTION, IS_MULTI_VALUE, TC_VIEW_OPTION, MAX_NUMBER, STRING_LENGTH, TP_VIEW_OPTION)
  SELECT :n.company_id, ATTRIBUTE_NAME, DATA_TYPE, LABEL, OBJECT_TYPE, SEQ_CUSTOM_ATTRIBUTE_ID.nextVal,
  CREATE_SOURCE_TYPE, CREATED_SOURCE, CREATED_DTTM, LAST_UPDATED_SOURCE_TYPE, LAST_UPDATED_SOURCE, LAST_UPDATED_DTTM,
  MARK_FOR_DELETION, IS_VALUE_REQUIRED, IS_LOCALIZABLE, IS_REQUIRED_FOR_REPORT_FILTER, IS_PARTICIPATES_IN_REPORTS, IS_PARTICIPATES_RR_QUERY,
  IS_REQUIRED_FOR_FILTER, IS_PARTICIPATES_IN_FILTER, FOR_RESOURCE_SELECTION, IS_MULTI_VALUE, TC_VIEW_OPTION, MAX_NUMBER, STRING_LENGTH, TP_VIEW_OPTION
  FROM CUSTOM_ATTRIBUTE
  WHERE TC_COMPANY_ID=0;
end IF;
end;
/

ALTER TRIGGER A_CUSTOM_ATTR_TRIGGER ENABLE;


CREATE OR REPLACE TRIGGER COMPANY_AI_TLM
 AFTER INSERT
 ON &1..COMPANY
 REFERENCING NEW AS NEW
 FOR EACH ROW
BEGIN
INSERT INTO custom_rule_type
        (custom_rule_type_id, tc_company_id, custom_rule_type_desc
        )
 VALUES (seq_custom_rule_type_id.NEXTVAL, :NEW.COMPANY_ID, 'JOURNAL ENTRY'
        );
INSERT INTO custom_rule_field
        (custom_rule_field_id, tc_company_id, custom_rule_field_desc
        )
 VALUES (seq_custom_rule_field_id.NEXTVAL, :NEW.COMPANY_ID, 'Division'
        );
INSERT INTO custom_rule_type_field_map
        (custom_rule_type_id, custom_rule_field_id, tc_company_id
        )
 VALUES (seq_custom_rule_type_id.currval, seq_custom_rule_field_id.CURRVAL, :NEW.COMPANY_ID
        );
INSERT INTO custom_rule_field
        (custom_rule_field_id, tc_company_id, custom_rule_field_desc
        )
 VALUES (seq_custom_rule_field_id.NEXTVAL, :NEW.COMPANY_ID, 'Claim Category'
        );
INSERT INTO custom_rule_type_field_map
        (custom_rule_type_id, custom_rule_field_id, tc_company_id
        )
 VALUES (seq_custom_rule_type_id.currval, seq_custom_rule_field_id.CURRVAL, :NEW.COMPANY_ID
        );
INSERT INTO custom_rule_field
        (custom_rule_field_id, tc_company_id, custom_rule_field_desc
        )
 VALUES (seq_custom_rule_field_id.NEXTVAL, :NEW.COMPANY_ID, 'Claim Group'
        );
INSERT INTO custom_rule_type_field_map
        (custom_rule_type_id, custom_rule_field_id, tc_company_id
        )
 VALUES (seq_custom_rule_type_id.currval, seq_custom_rule_field_id.CURRVAL, :NEW.COMPANY_ID
        );
INSERT INTO custom_rule_field
        (custom_rule_field_id, tc_company_id, custom_rule_field_desc
        )
 VALUES (seq_custom_rule_field_id.NEXTVAL, :NEW.COMPANY_ID, 'Claim Type'
        );
INSERT INTO custom_rule_type_field_map
        (custom_rule_type_id, custom_rule_field_id, tc_company_id
        )
 VALUES (seq_custom_rule_type_id.currval, seq_custom_rule_field_id.CURRVAL, :NEW.COMPANY_ID
        );
INSERT INTO custom_rule_field
        (custom_rule_field_id, tc_company_id, custom_rule_field_desc
        )
 VALUES (seq_custom_rule_field_id.NEXTVAL, :NEW.COMPANY_ID, 'Claim Action Type'
        );
INSERT INTO custom_rule_type_field_map
        (custom_rule_type_id, custom_rule_field_id, tc_company_id
        )
 VALUES (seq_custom_rule_type_id.currval, seq_custom_rule_field_id.CURRVAL, :NEW.COMPANY_ID
        );
INSERT INTO custom_rule_field
        (custom_rule_field_id, tc_company_id, custom_rule_field_desc
        )
 VALUES (seq_custom_rule_field_id.NEXTVAL, :NEW.COMPANY_ID, 'Claim Amount Type'
        );
INSERT INTO custom_rule_type_field_map
        (custom_rule_type_id, custom_rule_field_id, tc_company_id
        )
 VALUES (seq_custom_rule_type_id.currval, seq_custom_rule_field_id.CURRVAL, :NEW.COMPANY_ID
        );
INSERT INTO custom_rule_field
        (custom_rule_field_id, tc_company_id, custom_rule_field_desc
        )
 VALUES (seq_custom_rule_field_id.NEXTVAL, :NEW.COMPANY_ID, 'Claim Action Code'
        );
INSERT INTO custom_rule_type_field_map
        (custom_rule_type_id, custom_rule_field_id, tc_company_id
        )
 VALUES (seq_custom_rule_type_id.currval, seq_custom_rule_field_id.CURRVAL, :NEW.COMPANY_ID
        );
INSERT INTO custom_rule_field
        (custom_rule_field_id, tc_company_id, custom_rule_field_desc
        )
 VALUES (seq_custom_rule_field_id.NEXTVAL, :NEW.COMPANY_ID, 'Business Group'
        );
INSERT INTO custom_rule_type_field_map
        (custom_rule_type_id, custom_rule_field_id, tc_company_id
        )
 VALUES (seq_custom_rule_type_id.currval, seq_custom_rule_field_id.CURRVAL, :NEW.COMPANY_ID
        );
INSERT INTO claim_amount_type
        (claim_amount_type_id, tc_company_id, claim_amount_type_desc,
         created_source, created_source_type, last_updated_source,
         last_updated_source_type
        )
 VALUES (seq_claim_amount_type_id.NEXTVAL, :NEW.COMPANY_ID, 'Total Claim Amount',
         'MANUAL', 1, 'MANUAL',
         1
        );
INSERT INTO claim_amount_type
        (claim_amount_type_id, tc_company_id, claim_amount_type_desc,
         created_source, created_source_type, last_updated_source,
         last_updated_source_type
        )
 VALUES (seq_claim_amount_type_id.NEXTVAL, :NEW.COMPANY_ID, 'Merchandise Amount',
         'MANUAL', 1, 'MANUAL',
         1
        );
INSERT INTO claim_amount_type
        (claim_amount_type_id, tc_company_id, claim_amount_type_desc,
         created_source, created_source_type, last_updated_source,
         last_updated_source_type
        )
 VALUES (seq_claim_amount_type_id.NEXTVAL, :NEW.COMPANY_ID, 'Freight Amount',
         'MANUAL', 1, 'MANUAL',
         1
        );
INSERT INTO claim_amount_type
        (claim_amount_type_id, tc_company_id, claim_amount_type_desc,
         created_source, created_source_type, last_updated_source,
         last_updated_source_type
        )
 VALUES (seq_claim_amount_type_id.NEXTVAL, :NEW.COMPANY_ID, 'Recoup Amount',
         'MANUAL', 1, 'MANUAL',
         1
        );
INSERT INTO claim_amount_type
        (claim_amount_type_id, tc_company_id, claim_amount_type_desc,
         created_source, created_source_type, last_updated_source,
         last_updated_source_type
        )
 VALUES (seq_claim_amount_type_id.NEXTVAL, :NEW.COMPANY_ID, 'Salvage Amount',
         'MANUAL', 1, 'MANUAL',
         1
        );
INSERT INTO claim_amount_type
        (claim_amount_type_id, tc_company_id, claim_amount_type_desc,
         created_source, created_source_type, last_updated_source,
         last_updated_source_type
        )
 VALUES (seq_claim_amount_type_id.NEXTVAL, :NEW.COMPANY_ID, 'Settlement Amount',
         'MANUAL', 1, 'MANUAL',
         1
        );
INSERT INTO claim_amount_type
        (claim_amount_type_id, tc_company_id, claim_amount_type_desc,
         created_source, created_source_type, last_updated_source,
         last_updated_source_type
        )
 VALUES (seq_claim_amount_type_id.NEXTVAL, :NEW.COMPANY_ID, 'Payment Amount',
         'MANUAL', 1, 'MANUAL',
         1
        );
INSERT INTO claim_amount_type
        (claim_amount_type_id, tc_company_id, claim_amount_type_desc,
         created_source, created_source_type, last_updated_source,
         last_updated_source_type
        )
 VALUES (seq_claim_amount_type_id.NEXTVAL, :NEW.COMPANY_ID, 'Handling Amount',
         'MANUAL', 1, 'MANUAL',
         1
        );
INSERT INTO claim_amount_type
        (claim_amount_type_id, tc_company_id, claim_amount_type_desc,
         created_source, created_source_type, last_updated_source,
         last_updated_source_type
        )
 VALUES (seq_claim_amount_type_id.NEXTVAL, :NEW.COMPANY_ID, 'Trans Claim Amount',
         'MANUAL', 1, 'MANUAL',
         1
        );
insert into activity_type
        (ACTIVITY_TYPE_ID,TC_COMPANY_ID,ACTIVITY_NAME,
        CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,
LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,
ACTIVITY_DESC,IS_DRIVING,IS_DAILY_DUTY,IS_WEEKLY_DUTY,
IS_RESTING,REQUIRES_REASON_CODE,MARK_FOR_DELETION,BASE_ACTIVITY_TYPE_ID )
values (SEQ_ACTIVITY_TYPE_ID.NEXTVAL,:NEW.COMPANY_ID, 'DRIVING',
1, 'SS', sysdate, 1, 'SS', sysdate, 'Driving', 0, 0, 0, 0, 0, 0, 4 );
insert into activity_type
(ACTIVITY_TYPE_ID,TC_COMPANY_ID,ACTIVITY_NAME,
        CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,
LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,
ACTIVITY_DESC,IS_DRIVING,IS_DAILY_DUTY,IS_WEEKLY_DUTY,
IS_RESTING,REQUIRES_REASON_CODE,MARK_FOR_DELETION,BASE_ACTIVITY_TYPE_ID )
values ( SEQ_ACTIVITY_TYPE_ID.NEXTVAL,:NEW.COMPANY_ID, 'ONDUTY',
1, 'SS', sysdate, 1, 'SS', sysdate, 'On Duty Not Driving', 0, 0, 0, 0, 0, 0, 8 );
insert into activity_type
(ACTIVITY_TYPE_ID,TC_COMPANY_ID,ACTIVITY_NAME,
        CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,
LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,
ACTIVITY_DESC,IS_DRIVING,IS_DAILY_DUTY,IS_WEEKLY_DUTY,
IS_RESTING,REQUIRES_REASON_CODE,MARK_FOR_DELETION,BASE_ACTIVITY_TYPE_ID )
values ( SEQ_ACTIVITY_TYPE_ID.NEXTVAL,:NEW.COMPANY_ID, 'OFFDUTY',
1, 'SS', sysdate, 1, 'SS', sysdate, 'Off Duty', 0, 0, 0, 0, 0, 0, 36 );
insert into activity_type
(ACTIVITY_TYPE_ID,TC_COMPANY_ID,ACTIVITY_NAME,
        CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,
LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,
ACTIVITY_DESC,IS_DRIVING,IS_DAILY_DUTY,IS_WEEKLY_DUTY,
IS_RESTING,REQUIRES_REASON_CODE,MARK_FOR_DELETION,BASE_ACTIVITY_TYPE_ID )
values ( SEQ_ACTIVITY_TYPE_ID.NEXTVAL,:NEW.COMPANY_ID, 'SLEEPER',
1, 'SS', sysdate, 1, 'SS', sysdate, 'Sleeper Berth', 0, 0, 0, 0, 0, 0, 36 );
INSERT INTO PERFORMANCE_FACTOR
            (PERFORMANCE_FACTOR_ID,
             TC_COMPANY_ID,
             PERFORMANCE_FACTOR_NAME,
             PF_TYPE,
             DESCRIPTION,
             DEFAULT_VALUE,
             IS_USE_FLAG,
             CREATED_SOURCE_TYPE,
             CREATED_SOURCE,
             LAST_UPDATED_SOURCE_TYPE,
             LAST_UPDATED_SOURCE,
             CREATED_DTTM,
             LAST_UPDATED_DTTM,
             USE_RG_LOOKUP)
VALUES      ( SEQ_PERFORMANCE_FACTOR_ID.NEXTVAL, :NEW.COMPANY_ID, 'Unsafe Driving',
              1,
              'Uns_Dri', 1, 1, 2,
              'LDAP', 2, 'LDAP',SYSDATE, SYSDATE, 1 );
INSERT INTO PERFORMANCE_FACTOR
            (PERFORMANCE_FACTOR_ID,
             TC_COMPANY_ID,
             PERFORMANCE_FACTOR_NAME,
             PF_TYPE,
             DESCRIPTION,
             DEFAULT_VALUE,
             IS_USE_FLAG,
             CREATED_SOURCE_TYPE,
             CREATED_SOURCE,
             LAST_UPDATED_SOURCE_TYPE,
             LAST_UPDATED_SOURCE,
             CREATED_DTTM,
             LAST_UPDATED_DTTM,
             USE_RG_LOOKUP)
VALUES      ( SEQ_PERFORMANCE_FACTOR_ID.NEXTVAL, :NEW.COMPANY_ID, 'Fatigued Driving',
              1,
              'Fatg_Dri', 1, 1, 2,
              'LDAP', 2, 'LDAP',SYSDATE, SYSDATE, 1 );
INSERT INTO PERFORMANCE_FACTOR
            (PERFORMANCE_FACTOR_ID,
             TC_COMPANY_ID,
             PERFORMANCE_FACTOR_NAME,
             PF_TYPE,
             DESCRIPTION,
             DEFAULT_VALUE,
             IS_USE_FLAG,
             CREATED_SOURCE_TYPE,
             CREATED_SOURCE,
             LAST_UPDATED_SOURCE_TYPE,
             LAST_UPDATED_SOURCE,
             CREATED_DTTM,
             LAST_UPDATED_DTTM,
             USE_RG_LOOKUP)
VALUES      ( SEQ_PERFORMANCE_FACTOR_ID.NEXTVAL, :NEW.COMPANY_ID, 'Driver Fitness',
              1,
              'Dri_Fit', 1, 1, 2,
              'LDAP', 2, 'LDAP',SYSDATE, SYSDATE, 1 );
INSERT INTO PERFORMANCE_FACTOR
            (PERFORMANCE_FACTOR_ID,
             TC_COMPANY_ID,
             PERFORMANCE_FACTOR_NAME,
             PF_TYPE,
             DESCRIPTION,
             DEFAULT_VALUE,
             IS_USE_FLAG,
             CREATED_SOURCE_TYPE,
             CREATED_SOURCE,
             LAST_UPDATED_SOURCE_TYPE,
             LAST_UPDATED_SOURCE,
             CREATED_DTTM,
             LAST_UPDATED_DTTM,
             USE_RG_LOOKUP)
VALUES      ( SEQ_PERFORMANCE_FACTOR_ID.NEXTVAL, :NEW.COMPANY_ID, 'Drug/ Alcohol',
              1,
              'Drug_Alc', 1, 1, 2,
              'LDAP', 2, 'LDAP',SYSDATE, SYSDATE, 1 );
INSERT INTO PERFORMANCE_FACTOR
            (PERFORMANCE_FACTOR_ID,
             TC_COMPANY_ID,
             PERFORMANCE_FACTOR_NAME,
             PF_TYPE,
             DESCRIPTION,
             DEFAULT_VALUE,
             IS_USE_FLAG,
             CREATED_SOURCE_TYPE,
             CREATED_SOURCE,
             LAST_UPDATED_SOURCE_TYPE,
             LAST_UPDATED_SOURCE,
             CREATED_DTTM,
             LAST_UPDATED_DTTM,
             USE_RG_LOOKUP)
VALUES      ( SEQ_PERFORMANCE_FACTOR_ID.NEXTVAL, :NEW.COMPANY_ID, 'Vehicle Maint.',
              1,
              'Veh_Maint.', 1, 1, 2,
              'LDAP', 2, 'LDAP',SYSDATE, SYSDATE, 1 );
INSERT INTO PERFORMANCE_FACTOR
            (PERFORMANCE_FACTOR_ID,
             TC_COMPANY_ID,
             PERFORMANCE_FACTOR_NAME,
             PF_TYPE,
             DESCRIPTION,
             DEFAULT_VALUE,
             IS_USE_FLAG,
             CREATED_SOURCE_TYPE,
             CREATED_SOURCE,
             LAST_UPDATED_SOURCE_TYPE,
             LAST_UPDATED_SOURCE,
             CREATED_DTTM,
             LAST_UPDATED_DTTM,
             USE_RG_LOOKUP)
VALUES      ( SEQ_PERFORMANCE_FACTOR_ID.NEXTVAL, :NEW.COMPANY_ID, 'Cargo related',
              1,
              'Carg_rel', 1, 1, 2,
              'LDAP', 2, 'LDAP',SYSDATE, SYSDATE, 1 );
INSERT INTO PERFORMANCE_FACTOR
            (PERFORMANCE_FACTOR_ID,
             TC_COMPANY_ID,
             PERFORMANCE_FACTOR_NAME,
             PF_TYPE,
             DESCRIPTION,
             DEFAULT_VALUE,
             IS_USE_FLAG,
             CREATED_SOURCE_TYPE,
             CREATED_SOURCE,
             LAST_UPDATED_SOURCE_TYPE,
             LAST_UPDATED_SOURCE,
             CREATED_DTTM,
             LAST_UPDATED_DTTM,
             USE_RG_LOOKUP)
VALUES      ( SEQ_PERFORMANCE_FACTOR_ID.NEXTVAL, :NEW.COMPANY_ID, 'Crash Indicator',
              1,
              'Crash_Ind', 1, 1, 2,
              'LDAP', 2, 'LDAP',SYSDATE, SYSDATE, 1 );
END;
/

ALTER TRIGGER COMPANY_AI_TLM ENABLE;

CREATE OR REPLACE TRIGGER USR_AFT_DEL
 AFTER DELETE
 ON &1..UCL_USER
 FOR EACH ROW
BEGIN
 DELETE FROM USER_PROFILE
     WHERE LOGIN_USER_ID = :OLD.USER_NAME;
END;
/

ALTER TRIGGER USR_AFT_DEL ENABLE;

CREATE OR REPLACE TRIGGER USR_AFT_INS
 AFTER INSERT
 ON &1..UCL_USER
 FOR EACH ROW
BEGIN
 INSERT INTO USER_PROFILE (USER_PROFILE_ID,
                         LOGIN_USER_ID,
                         MENU_ID,
                         RF_MENU_ID,
                         CREATE_DATE_TIME,
                         MOD_DATE_TIME,
                         USER_ID)
    VALUES (:NEW.UCL_USER_ID,
            :NEW.USER_NAME,
            2124,
            3111,
            :NEW.CREATED_DTTM,
            :NEW.CREATED_DTTM,
            'SYSTEM');
END;
/

ALTER TRIGGER USR_AFT_INS ENABLE;


CREATE OR REPLACE TRIGGER COMPANY_APP_DOM
   AFTER INSERT
   ON &1..COMPANY_APP
   REFERENCING NEW AS NEW
   FOR EACH ROW
DECLARE
   l_chk      VARCHAR2 (10);
   noOfRows   NUMBER (2);
   DOM_APP    COMPANY_APP.APP_ID%TYPE;
BEGIN
   SELECT APP_ID
     INTO DOM_APP
     FROM APP
    WHERE APP_SHORT_NAME = 'DOM';

   IF (DOM_APP = :NEW.APP_ID)
   THEN
      SELECT COUNT (PAYMENT_RULE_ID)
        INTO noOfRows
        FROM A_PAYMENT_RULE
       WHERE COMPANY_ID = :NEW.company_id;

      IF (noOfRows = 0)
      THEN
         --A_CO_REASON_CODE
         INSERT INTO A_CO_REASON_CODE (REASON_CODE_ID,
                                       TC_COMPANY_ID,
                                       REASON_CODE,
                                       DESCRIPTION,
                                       REASON_CODE_TYPE,
                                       MARK_FOR_DELETION,
                                       SYSTEM_DEFINED)
            SELECT SEQ_CO_REASON_CODE.NEXTVAL,
                   :NEW.company_id,
                   REASON_CODE,
                   DESCRIPTION,
                   REASON_CODE_TYPE,
                   0,
                   SYSTEM_DEFINED
              FROM A_CO_REASON_CODE
             WHERE TC_COMPANY_ID = 0;

         --A_CO_NOTE_TYPE
         INSERT INTO A_CO_NOTE_TYPE (NOTE_TYPE_ID,
                                     TC_COMPANY_ID,
                                     NOTE_TYPE,
                                     DESCRIPTION,
                                     MARK_FOR_DELETION,
                                     SYSTEM_DEFINED,
                                     NOTE_CATEGORY_ID)
            SELECT SEQ_CO_NOTE_TYPE.NEXTVAL,
                   :NEW.company_id,
                   NOTE_TYPE,
                   DESCRIPTION,
                   0,
                   SYSTEM_DEFINED,
                   NOTE_CATEGORY_ID
              FROM A_CO_NOTE_TYPE
             WHERE TC_COMPANY_ID = 0;

         --A_PAYMENT_RULE
         INSERT INTO A_PAYMENT_RULE (PAYMENT_RULE_ID,
                                     ENTITY_TYPE_ID,
                                     PAYMENT_METHOD,
                                     RULE_NAME,
                                     DESCRIPTION,
                                     REALTIME_AUTH_REQUIRED,
                                     AUTH_EXP_DAYS,
                                     CHARGE_SEQUENCE,
                                     REFUND_SEQUENCE,
                                     VALID_FOR_RETURN,
                                     TOTAL_NO_OF_TRIES,
                                     SETTLEMENT_EXP_DAYS,
                                     AUTHORIZATION_REQUIRED,
                                     STATUS_CHECK_DAYS,
                                     COMPANY_ID,
                                     CREATED_SOURCE,
                                     CREATED_SOURCE_TYPE,
                                     CREATED_DTTM,
                                     LAST_UPDATED_SOURCE,
                                     LAST_UPDATED_SOURCE_TYPE,
                                     LAST_UPDATED_DTTM,
                                     GROUP_RULE_ID,
                                     PAYMENT_ORIGIN)
            SELECT SEQ_PAYMENT_RULE_ID.NEXTVAL,
                   ENTITY_TYPE_ID,
                   PAYMENT_METHOD,
                   RULE_NAME,
                   DESCRIPTION,
                   REALTIME_AUTH_REQUIRED,
                   AUTH_EXP_DAYS,
                   CHARGE_SEQUENCE,
                   REFUND_SEQUENCE,
                   VALID_FOR_RETURN,
                   TOTAL_NO_OF_TRIES,
                   SETTLEMENT_EXP_DAYS,
                   AUTHORIZATION_REQUIRED,
                   STATUS_CHECK_DAYS,
                   :NEW.company_id,
                   CREATED_SOURCE,
                   CREATED_SOURCE_TYPE,
                   CREATED_DTTM,
                   LAST_UPDATED_SOURCE,
                   LAST_UPDATED_SOURCE_TYPE,
                   LAST_UPDATED_DTTM,
                   GROUP_RULE_ID,
                   PAYMENT_ORIGIN
              FROM A_PAYMENT_RULE
             WHERE COMPANY_ID = 0;

         --WF_BUSINESS_PROCESS
         INSERT INTO WF_BUSINESS_PROCESS (BUSINESS_PROCESS_ID,
                                          TC_COMPANY_ID,
                                          DOMAIN_GROUP_ID,
                                          BUSINESS_PROCESS_NAME,
                                          FILENAME,
                                          DIAGRAM,
                                          EXPRESSION,
                                          BUSINESS_PROCESS_GRAPH,
                                          CREATED_DTTM,
                                          CREATED_BY,
                                          MARKED_FOR_DELETION,
                                          IMAGE,
                                          DOMAIN_GROUP_NAME,
                                          is_default)
            SELECT SEQ_WF_BUSINESS_PROCESS.NEXTVAL,
                   :NEW.COMPANY_ID,
                   DOMAIN_GROUP_ID,
                   BUSINESS_PROCESS_NAME,
                   FILENAME,
                   DIAGRAM,
                   EXPRESSION,
                   BUSINESS_PROCESS_GRAPH,
                   CREATED_DTTM,
                   CREATED_BY,
                   MARKED_FOR_DELETION,
                   IMAGE,
                   DOMAIN_GROUP_NAME,
                   1
              FROM WF_BUSINESS_PROCESS_DEFAULT
             WHERE tc_company_id = 0;

         --A_CO_APPEASEMENT
         INSERT INTO A_CO_APPEASEMENT (APPEASEMENT_ID,
                                       TC_COMPANY_ID,
                                       APPEASEMENT,
                                       DESCRIPTION,
                                       APPEASEMENT_TYPE,
                                       APPEASEMENT_VALUE,
                                       MARK_FOR_DELETION,
                                       SYSTEM_DEFINED)
            SELECT SEQ_CO_APPEASEMENT.NEXTVAL,
                   :NEW.company_id,
                   APPEASEMENT,
                   DESCRIPTION,
                   APPEASEMENT_TYPE,
                   APPEASEMENT_VALUE,
                   0,
                   SYSTEM_DEFINED
              FROM A_CO_APPEASEMENT
             WHERE TC_COMPANY_ID = 0;

         --A_GATEWAY_INFO
         INSERT INTO A_GATEWAY_INFO (GATEWAY_INFO_ID,
                                     COMPANY_ID,
                                     PDS_ENABLED,
                                     PDS_ENDPOINT,
                                     GATEWAY_NAME,
                                     ENDPOINT,
                                     REQUEST_TRANSLATOR_NAME,
                                     RESPONSE_TRANSLATOR_NAME,
                                     TRANSLATOR_TYPE,
                                     INTEGRATION_THROUGH_IF,
                                     USE_SIMULATOR,
                                     GATEWAY_TYPE,
                                     GATEWAY_MODE,
                                     CERTIFICATE_LOCATION,
                                     CREATED_SOURCE,
                                     CREATED_SOURCE_TYPE,
                                     CREATED_DTTM,
                                     LAST_UPDATED_SOURCE,
                                     LAST_UPDATED_SOURCE_TYPE,
                                     LAST_UPDATED_DTTM,
                                     SYSTEM_DEFINED,
                                     PAYMENT_ORIGIN)
            SELECT SEQ_GATEWAY_INFO.NEXTVAL,
                   :NEW.company_id,
                   GW.PDS_ENABLED,
                   GW.PDS_ENDPOINT,
                   GW.GATEWAY_NAME,
                   GW.ENDPOINT,
                   GW.REQUEST_TRANSLATOR_NAME,
                   GW.RESPONSE_TRANSLATOR_NAME,
                   GW.TRANSLATOR_TYPE,
                   GW.INTEGRATION_THROUGH_IF,
                   GW.USE_SIMULATOR,
                   GW.GATEWAY_TYPE,
                   GW.GATEWAY_MODE,
                   GW.CERTIFICATE_LOCATION,
                   GW.CREATED_SOURCE,
                   GW.CREATED_SOURCE_TYPE,
                   SYSDATE,
                   GW.LAST_UPDATED_SOURCE,
                   GW.LAST_UPDATED_SOURCE_TYPE,
                   SYSDATE,
                   GW.SYSTEM_DEFINED,
                   PAYMENT_ORIGIN
              FROM A_GATEWAY_INFO GW
             WHERE GW.COMPANY_ID = 0;

         --A_GATEWAY_INFO-DETAIL
         INSERT INTO A_GATEWAY_INFO_DETAIL (GATEWAY_INFO_ID, KEY, VALUE)
            SELECT GINFO.GATEWAY_INFO_ID, GINFOD.KEY, GINFOD.VALUE
              FROM A_GATEWAY_INFO_DETAIL GINFOD,
                   A_GATEWAY_INFO DGINFO,
                   A_GATEWAY_INFO GINFO
             WHERE     DGINFO.COMPANY_ID = 0
                   AND GINFO.GATEWAY_NAME = DGINFO.GATEWAY_NAME
                   AND GINFOD.GATEWAY_INFO_ID = DGINFO.GATEWAY_INFO_ID
                   AND GINFO.COMPANY_ID = :NEW.company_id;
      END IF;
   END IF;
END;
/

ALTER TRIGGER COMPANY_APP_DOM ENABLE;
create or replace
TRIGGER COMPANY_APP_DOM
   AFTER INSERT
   ON COMPANY_APP
   REFERENCING NEW AS NEW
   FOR EACH ROW
DECLARE
   l_chk varchar2(10);
   noOfRows number(2);
   DOM_APP COMPANY_APP.APP_ID%TYPE;

BEGIN
SELECT APP_ID INTO DOM_APP FROM APP WHERE APP_SHORT_NAME ='DOM';

IF ( DOM_APP = :NEW.APP_ID) THEN

select count(PAYMENT_RULE_ID) into noOfRows from A_PAYMENT_RULE where COMPANY_ID = :NEW.company_id;

IF(noOfRows = 0 ) THEN

--A_CO_REASON_CODE
INSERT INTO A_CO_REASON_CODE
(REASON_CODE_ID,TC_COMPANY_ID,REASON_CODE,DESCRIPTION,REASON_CODE_TYPE,
MARK_FOR_DELETION, SYSTEM_DEFINED)
select SEQ_CO_REASON_CODE.nextval,:NEW.company_id,REASON_CODE,
DESCRIPTION, REASON_CODE_TYPE, 0 , SYSTEM_DEFINED
from A_CO_REASON_CODE where TC_COMPANY_ID = 0;

--A_CO_NOTE_TYPE
INSERT INTO A_CO_NOTE_TYPE
(NOTE_TYPE_ID,TC_COMPANY_ID,NOTE_TYPE,DESCRIPTION,
MARK_FOR_DELETION, SYSTEM_DEFINED, NOTE_CATEGORY_ID)
select SEQ_CO_NOTE_TYPE.nextval,:NEW.company_id,NOTE_TYPE,
DESCRIPTION, 0 , SYSTEM_DEFINED, NOTE_CATEGORY_ID
from A_CO_NOTE_TYPE where TC_COMPANY_ID = 0;

--A_PAYMENT_RULE
INSERT INTO A_PAYMENT_RULE(PAYMENT_RULE_ID, ENTITY_TYPE_ID, PAYMENT_METHOD, RULE_NAME, DESCRIPTION,
 REALTIME_AUTH_REQUIRED, AUTH_EXP_DAYS, CHARGE_SEQUENCE, REFUND_SEQUENCE,
 VALID_FOR_RETURN, TOTAL_NO_OF_TRIES,SETTLEMENT_EXP_DAYS,AUTHORIZATION_REQUIRED, STATUS_CHECK_DAYS,COMPANY_ID,
 CREATED_SOURCE, CREATED_SOURCE_TYPE, CREATED_DTTM,
 LAST_UPDATED_SOURCE, LAST_UPDATED_SOURCE_TYPE, LAST_UPDATED_DTTM, GROUP_RULE_ID, PAYMENT_ORIGIN)
select SEQ_PAYMENT_RULE_ID.nextval, ENTITY_TYPE_ID, PAYMENT_METHOD, RULE_NAME, DESCRIPTION,
 REALTIME_AUTH_REQUIRED, AUTH_EXP_DAYS, CHARGE_SEQUENCE, REFUND_SEQUENCE,
 VALID_FOR_RETURN, TOTAL_NO_OF_TRIES,SETTLEMENT_EXP_DAYS,AUTHORIZATION_REQUIRED, STATUS_CHECK_DAYS, :NEW.company_id ,
 CREATED_SOURCE, CREATED_SOURCE_TYPE, CREATED_DTTM,
 LAST_UPDATED_SOURCE, LAST_UPDATED_SOURCE_TYPE, LAST_UPDATED_DTTM, GROUP_RULE_ID, PAYMENT_ORIGIN
from A_PAYMENT_RULE where COMPANY_ID = 0;

--WF_BUSINESS_PROCESS
INSERT INTO WF_BUSINESS_PROCESS
(
  BUSINESS_PROCESS_ID,TC_COMPANY_ID,DOMAIN_GROUP_ID,BUSINESS_PROCESS_NAME,FILENAME,
  DIAGRAM,EXPRESSION,BUSINESS_PROCESS_GRAPH,CREATED_DTTM,CREATED_BY,
  MARKED_FOR_DELETION,IMAGE,DOMAIN_GROUP_NAME, is_default
)
select
  SEQ_WF_BUSINESS_PROCESS.nextval,:NEW.COMPANY_ID,
  DOMAIN_GROUP_ID,BUSINESS_PROCESS_NAME,FILENAME,
  DIAGRAM,EXPRESSION,BUSINESS_PROCESS_GRAPH,CREATED_DTTM,CREATED_BY,
  MARKED_FOR_DELETION,IMAGE,DOMAIN_GROUP_NAME, 1
from  WF_BUSINESS_PROCESS_DEFAULT where tc_company_id = 0;

--A_CO_APPEASEMENT
INSERT INTO A_CO_APPEASEMENT
(APPEASEMENT_ID,TC_COMPANY_ID,APPEASEMENT,DESCRIPTION, APPEASEMENT_TYPE, APPEASEMENT_VALUE,
MARK_FOR_DELETION, SYSTEM_DEFINED)
select SEQ_CO_APPEASEMENT.nextval,:NEW.company_id,APPEASEMENT,
DESCRIPTION, APPEASEMENT_TYPE, APPEASEMENT_VALUE, 0, SYSTEM_DEFINED
from A_CO_APPEASEMENT where TC_COMPANY_ID = 0;

--A_GATEWAY_INFO
INSERT INTO A_GATEWAY_INFO
(GATEWAY_INFO_ID, COMPANY_ID, PDS_ENABLED, PDS_ENDPOINT, GATEWAY_NAME,
ENDPOINT, REQUEST_TRANSLATOR_NAME, RESPONSE_TRANSLATOR_NAME, TRANSLATOR_TYPE,
INTEGRATION_THROUGH_IF, USE_SIMULATOR, GATEWAY_TYPE, GATEWAY_MODE, CERTIFICATE_LOCATION,
CREATED_SOURCE, CREATED_SOURCE_TYPE, CREATED_DTTM, LAST_UPDATED_SOURCE, LAST_UPDATED_SOURCE_TYPE, LAST_UPDATED_DTTM, SYSTEM_DEFINED, PAYMENT_ORIGIN)
SELECT SEQ_GATEWAY_INFO.NEXTVAL, :NEW.company_id, GW.PDS_ENABLED, GW.PDS_ENDPOINT, GW.GATEWAY_NAME,
GW.ENDPOINT, GW.REQUEST_TRANSLATOR_NAME, GW.RESPONSE_TRANSLATOR_NAME, GW.TRANSLATOR_TYPE,
GW.INTEGRATION_THROUGH_IF, GW.USE_SIMULATOR, GW.GATEWAY_TYPE, GW.GATEWAY_MODE, GW.CERTIFICATE_LOCATION,
GW.CREATED_SOURCE, GW.CREATED_SOURCE_TYPE, SYSDATE, GW.LAST_UPDATED_SOURCE, GW.LAST_UPDATED_SOURCE_TYPE, SYSDATE, GW.SYSTEM_DEFINED, PAYMENT_ORIGIN
FROM A_GATEWAY_INFO GW WHERE GW.COMPANY_ID=0;

--A_GATEWAY_INFO-DETAIL
INSERT INTO A_GATEWAY_INFO_DETAIL (GATEWAY_INFO_ID, KEY, VALUE)
 SELECT GINFO.GATEWAY_INFO_ID,
		GINFOD.KEY,
		GINFOD.VALUE
   FROM A_GATEWAY_INFO_DETAIL GINFOD, A_GATEWAY_INFO DGINFO, A_GATEWAY_INFO GINFO
  WHERE
   DGINFO.COMPANY_ID = 0
   AND GINFO.GATEWAY_NAME = DGINFO.GATEWAY_NAME
   AND GINFOD.GATEWAY_INFO_ID = DGINFO.GATEWAY_INFO_ID
   AND GINFO.COMPANY_ID = :NEW.company_id;
   
-- A_GATEWAY_ACCOUNT_DETAIL
INSERT INTO A_GATEWAY_ACCOUNT_DETAIL (GATEWAY_ACCOUNT_DETAIL_ID, GATEWAY_INFO_ID, ACCOUNT_KEY, ACCOUNT_VALUE, IS_SENSITIVE)
 SELECT SEQ_GATEWAY_ACCOUNT_DETAIL.nextval, 
 GINFO.GATEWAY_INFO_ID,
		GAD.ACCOUNT_KEY,
		GAD.ACCOUNT_VALUE,
    GAD.IS_SENSITIVE
   FROM A_GATEWAY_ACCOUNT_DETAIL GAD, A_GATEWAY_INFO DGINFO, A_GATEWAY_INFO GINFO
  WHERE
   DGINFO.COMPANY_ID = 0
   AND GINFO.COMPANY_ID = :NEW.company_id
     AND GAD.GATEWAY_INFO_ID = DGINFO.GATEWAY_INFO_ID 
     AND GINFO.GATEWAY_NAME = DGINFO.GATEWAY_NAME
    AND ((GINFO.PAYMENT_ORIGIN is null and DGINFO.PAYMENT_ORIGIN is null) or GINFO.PAYMENT_ORIGIN = DGINFO.PAYMENT_ORIGIN)
    AND ((GINFO.SITE_ID is null and DGINFO.SITE_ID is null) or GINFO.SITE_ID = DGINFO.SITE_ID);


END IF;
END IF;

END;
/

ALTER TRIGGER COMPANY_APP_DOM ENABLE;

exit;
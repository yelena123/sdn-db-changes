EXEC quiet_drop('TRIGGER','COMPANY_AI')  ;
EXEC quiet_drop('TRIGGER','USR_AFT_DEL');
EXEC quiet_drop('TRIGGER','USR_AFT_INS') ;
EXEC quiet_drop('TRIGGER','CMPNY_AFT_INS') ;
EXEC SEQUPDT('PRICE_TYPE_CBO','PRICE_TYPE_ID','PRICE_TYPE_ID_SEQ');


CREATE OR REPLACE TRIGGER CMPNY_AFT_INS
 AFTER INSERT
 ON COMPANY
 REFERENCING NEW AS N
 FOR EACH ROW
BEGIN
 INSERT INTO CD_MASTER (CD_MASTER_ID,
                      company_name,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID)
    VALUES (:N.COMPANY_ID,
            :N.COMPANY_NAME,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM');
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'AUTO_CREATE_BATCH_FLAG',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'BATCH_CTRL_FLAG',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'BATCH_ROLE_ID',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'CASE_LOCK_CODE_EXP_REC',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'CASE_LOCK_CODE_HELD',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'COLOR_MASK',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'COLOR_OFFSET',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'COLOR_SEPTR',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'COLOR_SFX_MASK',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'COLOR_SFX_OFFSET',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'COLOR_SFX_SEPTR',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'DFLT_BATCH_STAT_CODE',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'DSP_ITEM_DESC_FLAG',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'LOCK_CODE_INVALID',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'PICK_LOCK_CODE_EXP_REC',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'PICK_LOCK_CODE_HELD',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'PROC_WHSE_XFER',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'QUAL_MASK',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'QUAL_OFFSET',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'QUAL_SEPTR',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'RECV_BATCH',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SEASON_MASK',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SEASON_OFFSET',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SEASON_SEPTR',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SEASON_YR_MASK',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SEASON_YR_OFFSET',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SEASON_YR_SEPTR',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SEC_DIM_MASK',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SEC_DIM_OFFSET',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SEC_DIM_SEPTR',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SIZE_DESC_MASK',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SIZE_DESC_OFFSET',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SIZE_DESC_SEPTR',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SKU_MASK',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'SKU_OFFSET_MASK',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'STYLE_MASK',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'STYLE_OFFSET',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'STYLE_SEPTR',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'STYLE_SFX_MASK',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'STYLE_SFX_OFFSET',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'STYLE_SFX_SEPTR',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
 INSERT INTO CD_MASTER_DTL (CD_MASTER_ID,
                          COLUMN_NAME,
                          COLUMN_VALUE,
                          CREATE_DATE_TIME,
                          MOD_DATE_TIME,
                          USER_ID,
                          CD_MASTER_DTL_ID)
    VALUES (:N.COMPANY_ID,
            'UCC_EAN_CO_PFX',
            NULL,
            :N.CREATED_DTTM,
            :N.CREATED_DTTM,
            'SYSTEM',
            CD_MASTER_DTL_ID_SEQ.NEXTVAL);
END;
/

ALTER TRIGGER CMPNY_AFT_INS ENABLE;


-- DBTicket WM-76869
CREATE OR REPLACE TRIGGER USR_AFT_INS
AFTER INSERT ON UCL_USER
FOR EACH ROW
BEGIN

    IF (:NEW.COPY_FROM_USER IS  NULL)
    THEN 
        INSERT INTO USER_PROFILE
        (
         USER_PROFILE_ID,LOGIN_USER_ID,MENU_ID,RF_MENU_ID,
         CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID
        )
        VALUES
        (
         :NEW.UCL_USER_ID,:NEW.USER_NAME,2124,3111,
         :NEW.CREATED_DTTM,:NEW.CREATED_DTTM,'SYSTEM'
        );
   
    ELSE
        
        INSERT INTO USER_PROFILE
        (
         USER_PROFILE_ID,LOGIN_USER_ID,MENU_ID,RF_MENU_ID,
         CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID
        ) 
        ( SELECT :NEW.UCL_USER_ID,:NEW.USER_NAME,MENU_ID,RF_MENU_ID, 
              :NEW.CREATED_DTTM,:NEW.CREATED_DTTM ,'SYSTEM' 
          FROM USER_PROFILE 
          WHERE USER_PROFILE.LOGIN_USER_ID = :NEW.COPY_FROM_USER
        );
   
        INSERT INTO USER_TASK_GRP
        ( LOGIN_USER_ID, TASK_GRP, STAT_CODE, USER_ID, CREATE_DATE_TIME,
          MOD_DATE_TIME, TASK_GRP_JUMP_PRTY, REOCCUR_TASK_INTERVAL, USER_TASK_GRP_ID,
          USER_PROFILE_ID, WM_VERSION_ID, REOCCUR_INT
        )  
        ( SELECT :NEW.USER_NAME, TASK_GRP, STAT_CODE, USER_ID, CREATE_DATE_TIME,
              MOD_DATE_TIME, TASK_GRP_JUMP_PRTY, REOCCUR_TASK_INTERVAL, USER_TASK_GRP_ID_SEQ.NEXTVAL  ,
              USER_PROFILE_ID, WM_VERSION_ID, REOCCUR_INT 
          FROM USER_TASK_GRP 
          WHERE LOGIN_USER_ID = :NEW.COPY_FROM_USER
        );
   
        INSERT INTO LRF_USER_PRO 
        ( USER_PRO_ID,EMPLYE_ID,USER_ID,PRTR_REQSTR,
          CREATED_DTTM,LAST_UPDATED_DTTM
        )
        ( SELECT USER_PROFILE_ID_SEQ.NEXTVAL,EMPLYE_ID,:NEW.USER_NAME,PRTR_REQSTR,
              :NEW.CREATED_DTTM,:NEW.CREATED_DTTM 
          FROM LRF_USER_PRO 
          WHERE USER_ID =  :NEW.COPY_FROM_USER
        );   
    END IF ;
     
END;
/

ALTER TRIGGER USR_AFT_INS ENABLE;

CREATE OR REPLACE TRIGGER USR_AFT_DEL
 AFTER DELETE
 ON UCL_USER
 FOR EACH ROW
BEGIN
 DELETE FROM USER_PROFILE
     WHERE LOGIN_USER_ID = :OLD.USER_NAME;
END;
/

ALTER TRIGGER USR_AFT_DEL ENABLE;






-----DB-1365
CREATE OR REPLACE TRIGGER COMPANY_AI
 AFTER INSERT
 ON COMPANY
 REFERENCING NEW AS NEW
 FOR EACH ROW
DECLARE
 V_DL_ID     NUMBER;
 V_COUNT     NUMBER;
 VGENID      NUMBER;
 V_SHIP_ID   NUMBER;
BEGIN
 SELECT COMPANY_TYPE_ID
 INTO V_SHIP_ID
 FROM COMPANY_TYPE
WHERE LOWER (DESCRIPTION) = LOWER ('Shipper');
 IF (V_SHIP_ID = :NEW.COMPANY_TYPE_ID)
 THEN
  INSERT INTO RS_CONFIG (RS_CONFIG_ID,
                         TC_COMPANY_ID,
                         RS_CONFIG_NAME,
                         IS_VALID,
                         IS_ACTIVE,
                         CREATED_SOURCE_TYPE,
                         CREATED_SOURCE,
                         LAST_UPDATED_SOURCE_TYPE,
                         LAST_UPDATED_SOURCE)
       VALUES (SEQ_RS_CONFIG_ID.NEXTVAL,
               :NEW.COMPANY_ID,
               'Default',
               1,
               1,
               2,
               'LDAP',
               2,
               'LDAP');
  INSERT INTO PERFORMANCE_FACTOR (PERFORMANCE_FACTOR_ID,
                                  TC_COMPANY_ID,
                                  PERFORMANCE_FACTOR_NAME,
                                  PF_TYPE,
                                  DESCRIPTION,
                                  DEFAULT_VALUE,
                                  IS_USE_FLAG,
                                  CREATED_SOURCE_TYPE,
                                  CREATED_SOURCE,
                                  LAST_UPDATED_SOURCE_TYPE,
                                  LAST_UPDATED_SOURCE)
       VALUES (SEQ_PERFORMANCE_FACTOR_ID.NEXTVAL,
               :NEW.COMPANY_ID,
               'ON TIME',
               3,
               'On Time',
               0,
               0,
               2,
               'LDAP',
               2,
               'LDAP');
  INSERT INTO PERFORMANCE_FACTOR (PERFORMANCE_FACTOR_ID,
                                  TC_COMPANY_ID,
                                  PERFORMANCE_FACTOR_NAME,
                                  PF_TYPE,
                                  DESCRIPTION,
                                  DEFAULT_VALUE,
                                  IS_USE_FLAG,
                                  CREATED_SOURCE_TYPE,
                                  CREATED_SOURCE,
                                  LAST_UPDATED_SOURCE_TYPE,
                                  LAST_UPDATED_SOURCE)
       VALUES (SEQ_PERFORMANCE_FACTOR_ID.NEXTVAL,
               :NEW.COMPANY_ID,
               'ACCEPT RATIO',
               2,
               'Accept Rat',
               0,
               0,
               2,
               'LDAP',
               2,
               'LDAP');
  INSERT INTO RS_AREA (TC_COMPANY_ID,
                       RS_AREA_ID,
                       RS_AREA,
                       DESCRIPTION,
                       COMMENTS,
                       CREATED_SOURCE,
                       LAST_UPDATED_SOURCE)
       VALUES (
                 :NEW.COMPANY_ID,
                 RS_AREA_ID_SEQ.NEXTVAL,
                 'RSArealess',
                 'RSArealess',
                 'For Internal Use Only.  Used for shipments that can not be assigned to a RSArea.',
                 '0',
                 '0');
  INSERT INTO ILM_REASON_CODES (REASON_CODE_ID,
                                TYPE,
                                TC_COMPANY_ID,
                                DESCRIPTION)
       VALUES (17,
               'TRLR',
               :NEW.COMPANY_ID,
               'Yard Audit Lock');
  INSERT INTO ILM_REASON_CODES (REASON_CODE_ID,
                                TYPE,
                                TC_COMPANY_ID,
                                DESCRIPTION)
       VALUES (12,
               'TASK',
               :NEW.COMPANY_ID,
               'A trailer already in location');
  INSERT INTO CLAIM_ACTION_CODE (CLAIM_ACTION_CODE,
                                 CLAIM_ACTION_TYPE,
                                 TC_COMPANY_ID,
                                 DESCRIPTION)
     SELECT CLAIM_ACTION_CODE,
            CLAIM_ACTION_TYPE,
            :NEW.COMPANY_ID,
            DESCRIPTION
       FROM CLAIM_SYSTEM_ACTION_CODE SA
      WHERE NOT EXISTS
                   (SELECT 1
                      FROM CLAIM_ACTION_CODE A
                     WHERE A.CLAIM_ACTION_CODE = SA.CLAIM_ACTION_CODE
                           AND A.TC_COMPANY_ID = :NEW.COMPANY_ID);
 END IF;
 INSERT INTO DISTRIBUTION_LIST (TC_COMPANY_ID,
                              DESCRIPTION,
                              IS_PREFERRED_LIST,
                              TC_CONTACT_ID)
    VALUES (:NEW.COMPANY_ID,
            'All Company Carriers',
            1,
            0);
 INSERT INTO DISTRIBUTION_LIST (TC_COMPANY_ID,
                              DESCRIPTION,
                              IS_PREFERRED_LIST,
                              TC_CONTACT_ID)
    VALUES (:NEW.COMPANY_ID,
            'All Lane Carriers',
            2,
            0);
 UPDATE RS_CONFIG
  SET RS_CONFIG_RANK = NULL
WHERE TC_COMPANY_ID = :NEW.COMPANY_ID;
 INSERT INTO INVOICE_RESP_CODE
    VALUES (SEQ_INVOICE_RESP_CODE_ID.NEXTVAL,
            :NEW.COMPANY_ID,
            'ADTR',
            'FAP Auditor',
            0,
            0);
 INSERT INTO INVOICE_RESP_CODE
    VALUES (SEQ_INVOICE_RESP_CODE_ID.NEXTVAL,
            :NEW.COMPANY_ID,
            'CARR',
            'Carrier',
            0,
            1);
 INSERT INTO INVOICE_RESP_CODE
    VALUES (SEQ_INVOICE_RESP_CODE_ID.NEXTVAL,
            :NEW.COMPANY_ID,
            'CRLS',
            'Carrier Relations',
            0,
            0);
 INSERT INTO INVOICE_RESP_CODE
    VALUES (SEQ_INVOICE_RESP_CODE_ID.NEXTVAL,
            :NEW.COMPANY_ID,
            'MNGR',
            'FAP Manager',
            0,
            0);
 INSERT INTO INVOICE_RESP_CODE
    VALUES (SEQ_INVOICE_RESP_CODE_ID.NEXTVAL,
            :NEW.COMPANY_ID,
            'DTRC',
            'Detention Resolution Center',
            0,
            0);
 INSERT INTO PRICE_TYPE_CBO (PRICE_TYPE_ID,
                           COMPANY_ID,
                           AUDIT_CREATED_SOURCE,
                           AUDIT_LAST_UPDATED_SOURCE,
                           OWNED_BY,
                           AUDIT_LAST_UPDATED_DTTM,
                           VERSION,
                           AUDIT_CREATED_DTTM,
                           NAME,
                           DESCRIPTION,
                           AUDIT_LAST_UPDATED_SOURCE_TYPE,
                           AUDIT_CREATED_SOURCE_TYPE,
                           AUDIT_TRANSACTION,
                           AUDIT_PARTY_ID,
                           MARK_FOR_DELETION)
    VALUES (PRICE_TYPE_ID_SEQ.NEXTVAL,
            :NEW.COMPANY_ID,
            '1',
            '1',
            11,
            SYSDATE,
            1,
            SYSDATE,
            'REGULAR',
            '11',
            1,
            11,
            '11',
            11,
            0);
 INSERT INTO PRICE_TYPE_CBO (PRICE_TYPE_ID,
                           COMPANY_ID,
                           AUDIT_CREATED_SOURCE,
                           AUDIT_LAST_UPDATED_SOURCE,
                           OWNED_BY,
                           AUDIT_LAST_UPDATED_DTTM,
                           VERSION,
                           AUDIT_CREATED_DTTM,
                           NAME,
                           DESCRIPTION,
                           AUDIT_LAST_UPDATED_SOURCE_TYPE,
                           AUDIT_CREATED_SOURCE_TYPE,
                           AUDIT_TRANSACTION,
                           AUDIT_PARTY_ID,
                           MARK_FOR_DELETION)
    VALUES (PRICE_TYPE_ID_SEQ.NEXTVAL,
            :NEW.COMPANY_ID,
            '1',
            '1',
            11,
            SYSDATE,
            1,
            SYSDATE,
            'PROMOTIONAL',
            '12',
            1,
            11,
            '11',
            11,
            0);
 IF (:NEW.PARENT_COMPANY_ID = -1)
 THEN
  INSERT INTO OM_SCHED_EVENT (EVENT_ID,
                              SCHEDULED_DTTM,
                              EVENT_OBJECTS,
                              EVENT_TIMESTAMP,
                              OM_CATEGORY,
                              EVENT_TYPE,
                              EVENT_EXP_DATE,
                              EVENT_CNT,
                              EVENT_FREQ_IN_DAYS,
                              EVENT_FREQ_PER_DAY,
                              EXECUTED_DTTM,
                              IS_EXECUTED,
                              EVENT_FREQ_IN_DAY_OF_MONTH)
       VALUES (
                 SEQ_EVENT_ID.NEXTVAL,
                 SYSDATE,
                 '{eventProcessorClass=com.manh.baseservices.util.defaultbasedata.AutoCreateBaseDataScheduler, shipperPK='
                 || TO_CHAR (:NEW.COMPANY_ID)
                 || '}',
                 SYSDATE,
                 null,
                 0,
                 NULL,
                 0,
                 0,
                 0,
                 NULL,
                 0,
                 0);
 END IF;
END;
/

ALTER TRIGGER COMPANY_AI ENABLE;

exit;
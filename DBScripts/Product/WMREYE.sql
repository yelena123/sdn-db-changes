-- DBTicket WM-103275
-- SF case: 3982480
	
update FILTER_LAYOUT set HIBERNATE_FIELD_NAME='waveParm.shpmtNbr'  where OBJECT_TYPE='WM_SHIP_WAVE_INQ' and FIELD_NAME='WAVE_PARM.SHPMT_NBR';
commit;

-- DBTicket WM-103276
-- SF case: 3992519

update FILTER_LAYOUT set HIBERNATE_FIELD_NAME = 's.codeId' where OBJECT_TYPE = 'ITEM_FACILITY_WMOS' and FIELD_NAME = 'ITEM_FACILITY_WMOS.PUTWY_TYPE';
commit;


-- DBTicket WM-103277
-- SF case: 3994154 Customer Item Category Overrides REYECustProdTypeOverride.xhtml

MERGE INTO XMENU_ITEM XI
     USING (SELECT 'Customer Item Category Overrides' NAME, 'Distribution' BASE_SECTION_NAME, '/wm/ld/ui/REYECustProdTypeOverride.jsflps' NAVIGATION_KEY
              FROM DUAL) D
        ON (XI.NAME = D.NAME
            AND XI.BASE_SECTION_NAME = D.BASE_SECTION_NAME
            AND XI.NAVIGATION_KEY = D.NAVIGATION_KEY)
WHEN NOT MATCHED
THEN
INSERT      (xmenu_item_id,
             NAME,
             short_name,
             navigation_key,
             tile_key,
             icon,
             bgcolor,
             screen_mode,
             screen_version,
             base_section_name,
             base_part_name,
             is_default)
VALUES     ((SELECT Max(xmenu_item_id) + 1
             FROM   xmenu_item),
            'Customer Item Category Overrides',
            'cust_item_cat_over',
            '/wm/ld/ui/REYECustProdTypeOverride.jsflps',
            '',
            'default-icon',
            '#002E5F',
            0,
            1,
            'Distribution',
            'GENERAL',
            0);  
            

  MERGE INTO XMENU_ITEM_APP XIA
     USING (SELECT (select xmenu_item_id from xmenu_item where name like 'Customer Item Category Overrides') XMENU_ITEM_ID, 36 APP_ID
              FROM DUAL) D
        ON (XIA.XMENU_ITEM_ID = D.XMENU_ITEM_ID
            AND XIA.APP_ID = D.APP_ID)
WHEN NOT MATCHED
THEN
            insert (XMENU_ITEM_ID, APP_ID)
values
  ((select xmenu_item_id from xmenu_item where name like 'Customer Item Category Overrides'), 36);
  
  
        MERGE INTO XMENU_ITEM_PERMISSION XIP
     USING (SELECT (select xmenu_item_id from xmenu_item where name like 'Customer Item Category Overrides') XMENU_ITEM_ID, 'WMARUNWV' PERMISSION_CODE
              FROM DUAL) D
        ON (XIP.XMENU_ITEM_ID = D.XMENU_ITEM_ID
            AND XIP.PERMISSION_CODE = D.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
  insert (XMENU_ITEM_ID, PERMISSION_CODE)
values
  ((select xmenu_item_id from xmenu_item where name like 'Customer Item Category Overrides'), 'WMARUNWV');
  
  
Insert into xbase_menu_item (XBASE_MENU_ITEM_ID,XBASE_MENU_PART_ID,XMENU_ITEM_ID,SEQUENCE) values 
(
(select max(XBASE_MENU_ITEM_ID)+1  from xbase_menu_item),
23,
(select xmenu_item_id from xmenu_item where name like 'Customer Item Category Overrides'),
(select max(XBASE_MENU_ITEM_ID)+1  from xbase_menu_item)
);

/* ################################################################################################################################## 
Item Categories
REYEItemCat.xhtml
*/

MERGE INTO XMENU_ITEM XI
     USING (SELECT 'Item Categories' NAME, 'Distribution' BASE_SECTION_NAME, '/wm/ld/ui/REYEItemCat.jsflps' NAVIGATION_KEY
              FROM DUAL) D
        ON (XI.NAME = D.NAME
            AND XI.BASE_SECTION_NAME = D.BASE_SECTION_NAME
            AND XI.NAVIGATION_KEY = D.NAVIGATION_KEY)
WHEN NOT MATCHED
THEN
INSERT      (xmenu_item_id,
             NAME,
             short_name,
             navigation_key,
             tile_key,
             icon,
             bgcolor,
             screen_mode,
             screen_version,
             base_section_name,
             base_part_name,
             is_default)
VALUES     ((SELECT Max(xmenu_item_id) + 1
             FROM   xmenu_item),
            'Item Categories',
            'cust_item_cat',
            '/wm/ld/ui/REYEItemCat.jsflps',
            '',
            'default-icon',
            '#002E5F',
            0,
            1,
            'Distribution',
            'GENERAL',
            0);  
            
            
            MERGE INTO XMENU_ITEM_APP XIA
     USING (SELECT (select xmenu_item_id from xmenu_item where name like 'Item Categories') XMENU_ITEM_ID, 36 APP_ID
              FROM DUAL) D
        ON (XIA.XMENU_ITEM_ID = D.XMENU_ITEM_ID
            AND XIA.APP_ID = D.APP_ID)
WHEN NOT MATCHED
THEN
            insert (XMENU_ITEM_ID, APP_ID)
values
  ((select xmenu_item_id from xmenu_item where name like 'Item Categories'), 36);
  
  
        MERGE INTO XMENU_ITEM_PERMISSION XIP
     USING (SELECT (select xmenu_item_id from xmenu_item where name like 'Item Categories') XMENU_ITEM_ID, 'WMARUNWV' PERMISSION_CODE
              FROM DUAL) D
        ON (XIP.XMENU_ITEM_ID = D.XMENU_ITEM_ID
            AND XIP.PERMISSION_CODE = D.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
  insert (XMENU_ITEM_ID, PERMISSION_CODE)
values
 ((select xmenu_item_id from xmenu_item where name like 'Item Categories'), 'WMARUNWV');
  
Insert into xbase_menu_item (XBASE_MENU_ITEM_ID,XBASE_MENU_PART_ID,XMENU_ITEM_ID,SEQUENCE) values 
(
(select max(XBASE_MENU_ITEM_ID)+1  from xbase_menu_item),
23,
(select xmenu_item_id from xmenu_item where name like 'Item Categories'),
(select max(XBASE_MENU_ITEM_ID)+1  from xbase_menu_item)
);
commit;

-- DBTicket WM-103278
-- SF case: 4000363

MERGE INTO MESSAGE_MASTER A
USING (SELECT '1377' MSG_ID, '110801377' KEY, 'wm' ILS_MODULE, 'ErrorMessage' BUNDLE_NAME FROM DUAL) B
ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN MATCHED
THEN
	UPDATE SET MSG_TYPE = 'ERROR';
	
commit;

-- DBTicket WM-103279
-- SF case: 4008946 query to insert proper description for the CTRL-K key , which is part of WM14 mod.

 MERGE INTO MESSAGE_MASTER A
USING (SELECT '9999' MSG_ID, '110209999' KEY, 'wm' ILS_MODULE, 'ErrorMessage' BUNDLE_NAME FROM DUAL) B
ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
insert (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG) values(SEQ_MESSAGE_MASTER_ID.nextval,9999,110209999,'wm', 'CTRLKEY','CTRL-K = Lot Entry','ErrorMessage','SYSTEM','ERROR',NULL,'N');

MERGE INTO RF_BIND_KEY A
USING (SELECT 'FWDCUST' COMMAND FROM DUAL) B
ON (A.COMMAND = B.COMMAND)
WHEN MATCHED
THEN
	UPDATE SET description = '110209999'
WHEN NOT MATCHED
THEN
	INSERT (RF_BIND_KEY_ID,PROFILE,COMMAND,COMMAND_KEY,COMMAND_CHAR,LABEL,DESCRIPTION,TRANSACTION_NAME,
	CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM) 
	VALUES (seq_rf_bind_key_id.nextVal, 'DEFLT','FWDCUST','CTRL', 'K', 
	'Fwd Cust','110209999', 'Receive LPN', 1,'WMADMIN',getdate(),1,'WMADMIN', getdate());
	
commit;

-- DBTicket WM-103280
-- SF case: 4009894 WM27

MERGE INTO XMENU_ITEM XI
     USING (SELECT 'LPN/LOT Maintenance' NAME, 'Distribution' BASE_SECTION_NAME, '/cbo/transactional/lpn/view/REYECLPNLotMapping.jsflps' NAVIGATION_KEY
              FROM DUAL) D
        ON (XI.NAME = D.NAME
            AND XI.BASE_SECTION_NAME = D.BASE_SECTION_NAME
            AND XI.NAVIGATION_KEY = D.NAVIGATION_KEY)
WHEN NOT MATCHED
THEN
INSERT       (xmenu_item_id,
             NAME,
             short_name,
             navigation_key,
             tile_key,
             icon,
             bgcolor,
             screen_mode,
             screen_version,
             base_section_name,
             base_part_name,
             is_default)
VALUES     ((SELECT Max(xmenu_item_id) + 1
             FROM   xmenu_item),
            'LPN/LOT Maintenance',
            'LPN_LOT_Maintenance',
            '/cbo/transactional/lpn/view/REYECLPNLotMapping.jsflps',
            '',
            'default-icon',
            '#002E5F',
            0,
            1,
            'Distribution',
            'GENERAL',
            0);  
            
            
            MERGE INTO XMENU_ITEM_APP XIA
     USING (SELECT (select xmenu_item_id from xmenu_item where name like 'LPN/LOT Maintenance') XMENU_ITEM_ID, 36 APP_ID
              FROM DUAL) D
        ON (XIA.XMENU_ITEM_ID = D.XMENU_ITEM_ID
            AND XIA.APP_ID = D.APP_ID)
WHEN NOT MATCHED
THEN
            insert (XMENU_ITEM_ID, APP_ID)
values
  ((select xmenu_item_id from xmenu_item where name like 'LPN/LOT Maintenance'), 36);
  
  
        MERGE INTO XMENU_ITEM_PERMISSION XIP
     USING (SELECT (select xmenu_item_id from xmenu_item where name like 'LPN/LOT Maintenance') XMENU_ITEM_ID, 'WMARUNWV' PERMISSION_CODE
              FROM DUAL) D
        ON (XIP.XMENU_ITEM_ID = D.XMENU_ITEM_ID
            AND XIP.PERMISSION_CODE = D.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
  insert (XMENU_ITEM_ID, PERMISSION_CODE)
values
 ((select xmenu_item_id from xmenu_item where name like 'LPN/LOT Maintenance'), 'WMARUNWV');
  
Insert into xbase_menu_item (XBASE_MENU_ITEM_ID,XBASE_MENU_PART_ID,XMENU_ITEM_ID,SEQUENCE) values 
(
(select max(XBASE_MENU_ITEM_ID)+1  from xbase_menu_item),
23,
(select xmenu_item_id from xmenu_item where name like 'LPN/LOT Maintenance'),
(select max(XBASE_MENU_ITEM_ID)+1  from xbase_menu_item)
);
commit;

-- DBTicket WM-103281
-- C_PERMISSION_NAVIGATION_SCRIPT.sql
@DBScripts/Product/PLSQL_Objects/C_PERMISSION_NAVIGATION_SCRIPT.sql;
commit;

-- DBTicket WM-103282
-- WM05.sql
@DBScripts/Product/PLSQL_Objects/WM05.sql;
commit;

-- DBTicket WM-103711
-- SF4036790
--update query to change name from 'Generate Load Diagram' to 'Generate Load Plan'
update label set value = 'Generate Load Plan' where label_id = '606940' and key like 'GenerateLoadDiagram' and bundle_name = 'CBOGrid';
commit;
--update query to change name from 'View Load Diagram' to 'View Load Plan'
update label set value = 'View Load Plan' where label_id = '606941' and key like 'ViewLoadDiagram' and bundle_name = 'CBOGrid';
commit;
--update query to change name from 'Print Load Diagram' to 'Print Load Plan'
update label set value = 'Print Load Plan' where label_id = '606942' and key like 'PrintLoadDiagram' and bundle_name = 'CBOGrid';
commit;

-- DBTicket WM-103714
-- SF4017078
@DBScripts/Product/PLSQL_Objects/SF4017078.sql;

-- DBTicket WM-104182
-- DB_SCRIPT_SHIPMENT_MAUI_WM01.sql
@DBScripts/Product/PLSQL_Objects/DB_SCRIPT_SHIPMENT_MAUI_WM01.sql;
commit;

-- DBTicket WM-104183
-- FIX for SF# 4040891
update label_display set disp_value = 'Shipment ID' where disp_key='Document ID';
commit;

-- DBTicket WM-104184
-- SF4009155
MERGE INTO MESSAGE_DISPLAY A
USING (SELECT '110302801' DISP_KEY FROM DUAL) B
ON ( A.DISP_KEY = B.DISP_KEY )
WHEN MATCHED
THEN
               UPDATE SET DISP_VALUE = 'The following LPNs have not been loaded %s';
               
commit;

-- DBTicket WM-104185
-- SF4044943 c_wl02_gen_planning_events.sql
@DBScripts/Product/PLSQL_Objects/c_wl02_gen_planning_events.sql;

-- DBTicket WM-104854
-- Fix for SF 4040213
@DBScripts/Product/PLSQL_Objects/DB_SCRIPT_SHIPMENT_MAUI_WM01.sql;
@DBScripts/Product/PLSQL_Objects/SF4040213.sql;

commit;

-- DBTicket WM-105094
-- Fix for SF 4060654
@DBScripts/Product/PLSQL_Objects/DB_SCRIPT_SHIPMENT_MAUI_WM01.sql;

commit;

-- DBTicket WM-105162
-- Fix for SF 4044943
@DBScripts/Product/PLSQL_Objects/c_wl02_gen_planning_events.sql;

commit;

-- DBTicket WM-105238
-- Fix for SF 4063354
@DBScripts/Product/PLSQL_Objects/MISSING_PERMISSION_FIX.sql;

commit;

-- DBTicket WM-105538
-- Fix for SF 4062588
@DBScripts/Product/PLSQL_Objects/SF4062588.sql;

commit;

-- DBTicket WM-106068
-- Fix for SF 4073089
@DBScripts/Product/PLSQL_Objects/SF4073089.sql;

commit;


-- DBTicket WM-106122
-- Fix for SF 4074031
@DBScripts/Product/PLSQL_Objects/MISSING_PERMISSION_FIX.sql;

commit;

-- DBTicket WM-108085
-- Fix for SF 4039508, SF 4040019
@DBScripts/Product/PLSQL_Objects/C_WM26_ASN_RECEIVING_UPDATE.sql;

commit;

-- DBTicket WM-108281
-- Fix for SF 4106970
@DBScripts/Product/PLSQL_Objects/WM38.sql;

commit;

-- DBTicket WM-108394
-- Fix for SF 4108671
@DBScripts/Product/PLSQL_Objects/C_WM01_ALLOC_TRAILER_POS.sql;

commit;


-- DBTicket WM-108962
-- DB Script for WM19A, SF 4002713
@DBScripts/Product/PLSQL_Objects/WM19A/WM19A.sql
@DBScripts/Product/PLSQL_Objects/WM19A/WM19A_Label_Display_Scripts.sql
@DBScripts/Product/PLSQL_Objects/C_WM10_TASK_CREATION_UPDATES.sql
commit;

-- DBTicket WM-109867
-- Fix for SF 4113518
@DBScripts/Product/PLSQL_Objects/wm_invoice_orders_in_list.sql
commit;

-- DBTicket WM-109129
-- DB Script for WM45, SF 4002640
@DBScripts/Product/PLSQL_Objects/WM45/WM45Mod.sql
commit;

-- DBTicket WM-110296
-- DB Changes for WM49, SF 4002701
@DBScripts/Product/PLSQL_Objects/manh_gen_sku_invn_pix_WM49.sql
commit;

-- DBTicket WM-110100
-- Fix for SF 4002688
@DBScripts/Product/PLSQL_Objects/WM47.sql;

commit;

-- DBTicket WM-110669
-- DB Changes to fix SF 4128262
@DBScripts/Product/PLSQL_Objects/C_WM01_ALLOC_TRAILER_POS.sql
@DBScripts/Product/PLSQL_Objects/C_WM01_CREATE_CARTON_CHASETASK.sql
@DBScripts/Product/PLSQL_Objects/C_WM01_ALLOC_TRAILER_CHASETASK.sql

commit;

-- DBTicket WM-110671
-- Fix for SF 4129719
@DBScripts/Product/PLSQL_Objects/wm25.sql
commit; 

-- DBTicket WM-110841
-- Fix for SF 4135736
@DBScripts/Product/PLSQL_Objects/Custom_Message_Master_1401_1402.sql
commit;

-- DBTicket WM-110996
-- DB Script for WM50, SF 4002614
@DBScripts/Product/PLSQL_Objects/WM50/WM50.sql
commit;

-- DBTicket WM-110284
-- DB Script for WM41, SF 4002635
@DBScripts/Product/PLSQL_Objects/WM41/WM41.sql
commit;

-- DBTicket WM-111789
-- DB Script for WM19A, SF 4168782
@DBScripts/Product/PLSQL_Objects/wm25.sql
commit;

-- DBTicket WM-113011
-- DB Changes for WM49, SF 4180743 | Ext CR changes
@DBScripts/Product/PLSQL_Objects/manh_gen_sku_invn_pix_WM49.sql
commit;

-- DBTicket WM-113282
-- DB Changes for WM11, SF 4039751
@DBScripts/Product/PLSQL_Objects/c_wm11_SWFO_Interface.sql
@DBScripts/Product/PLSQL_Objects/SF4040213.sql
commit;

-- DBTicket WM-114025
--Archive/Purge changes, custom changes merged in proper path.
@DBScripts/Product/PLSQL_Objects/PurgeAndArchive/PurgeAndArchive.sql;
@DBScripts/Product/PLSQL_Objects/wm_archive_pkg.sql;
commit;

-- DBTicket WM-116030
-- SF 4242067 | WM40 Message master entry.
@DBScripts/Product/PLSQL_Objects/WM40.sql
commit;

-- DBTicket WM-117901 
-- SF  4274846 | Created the index on PROD_TRKG_TRAN
@DBScripts/Product/PLSQL_Objects/SF4274846_PROD_TRKG_TRAN_IND_1.sql
commit;

-- DBTicket YM-7521 
-- SF   4206403 | MSG MASTER ENTRY
@DBScripts/Product/PLSQL_Objects/SF4206403.sql
commit;

-- DBTicket WM-119827
-- SF 4300649 | Archive/Purge changes
@DBScripts/Product/PLSQL_Objects/wm_archive_pkg.sql;
commit;

-- DBTicket WM-119936
-- SF 4289098 
UPDATE APPLICATION_CONFIGURATION SET VALUE = 0 WHERE KEY='activemq.queue.prefetch.size';
commit;

-- DBTicket WM-119369
-- SF 4293205 | Adding missng sql script
@DBScripts/Product/PLSQL_Objects/SF4293205.sql;
commit;

-- DBTicket WM-125053
-- SF4403821
@DBScripts/Product/PLSQL_Objects/manh_gen_sku_invn_pix_WM49.sql
commit;

-- DBTicket WM-125055
-- SF4379761 - Port base CR WM39449
update navigation 
set parent_id = (select navigation_id from navigation where title = 'RF Menu' and type = 'ANC')
where title in ('Audit OB Pallet','Reprint iLPN') and type = 'RF';

commit;

-- DBTicket WM-128566
@DBScripts/Product/PLSQL_Objects/WM52/WM52.sql
commit;

--DBTicket WM-129369
@DBScripts/Product/PLSQL_Objects/C_WM26_ASN_RECEIVING_UPDATE.sql
commit;


--DBTicket LM-17305
--SF4468984 -- index creation for UX_E_INTERM_TA_STG_IDX2
@DBScripts/Product/PLSQL_Objects/SF4468984_UX_E_INTERM_TA_STG_IDX2.sql
commit;

--DBTicket WM-135102
--SF4500146 Porting 2011 fix WM:125957
@DBScripts/Product/PLSQL_Objects/C_WM01_ALLOC_TRAILER_POS.sql
commit;

-- DBTicket WM-134224
-- SF 4515185 | Archive/Purge changes
@DBScripts/Product/PLSQL_Objects/wm_archive_pkg.sql;
commit;

-- DBTicket LM-17607
--SF 4509764 | Archive/Purge changes
@DBScripts/Product/PLSQL_Objects/lm_archive_pkg.sql
commit;

--DBTicket WM-138184
@DBScripts/Product/PLSQL_Objects/C_WM01_ALLOC_TRAILER_CHASETASK.sql
@DBScripts/Product/PLSQL_Objects/C_WM01_ALLOC_TRAILER_POS.sql
commit;

--DBTicket WM-139162
@DBScripts/Product/PLSQL_Objects/C_WM01_SHORT_CANCEL_UPDATE.sql
commit;


--DBTicket WM-143759
Insert into resources (RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
SELECT SEQ_RESOURCE_ID.NEXTVAL,'/wm/common/ui/RFBarcodeMenu.xhtml','ACM',1,null
FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM RESOURCES WHERE URI = '/wm/common/ui/RFBarcodeMenu.xhtml' AND MODULE = 'ACM');
 
Insert into resource_permission (RESOURCE_ID,PERMISSION_CODE) 
SELECT (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/common/ui/RFBarcodeMenu.xhtml'),'WMACOMSCRN'
FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM RESOURCE_PERMISSION WHERE RESOURCE_ID IN (SELECT RESOURCE_ID FROM RESOURCES WHERE URI = '/wm/common/ui/RFBarcodeMenu.xhtml')); 
commit;

--DBTicket WM-143383
@DBScripts/Product/PLSQL_Objects/WM54/WM54DBScripts.sql
commit;

--DBTicket WM-145169
@DBScripts/Product/PLSQL_Objects/WM56/WM56.sql
commit;

--DBTicket WM-147633
@DBScripts/Product/PLSQL_Objects/WM57/WM57DB.sql
commit;

--DBTicket WM-149542
@DBScripts/Product/PLSQL_Objects/SF4732784-InboundRules.sql
commit;

--DBTicket WM-150091
@DBScripts/Product/PLSQL_Objects/WM58/WM58DB.sql
commit;

--DBTicket WM-151608
@DBScripts/Product/PLSQL_Objects/WM57/WM57DB-NewCodeId.sql
commit;



-- DBTicket WM-153015
@DBScripts/Product/PLSQL_Objects/C_WM10_TASK_CREATION_UPDATES.sql
commit;

--DBTicket WM-152510
@DBScripts/Product/PLSQL_Objects/WM55/WM55DB.sql
commit;

--DBTicket WM-154115
@DBScripts/Product/PLSQL_Objects/WM59/WM59CodeId.sql
commit;

--DBTicket WM-155974
@DBScripts/Product/PLSQL_Objects/WM58/WM58EanScreen.sql
commit;

--DBTicket WM-157937
@DBScripts/Product/PLSQL_Objects/SF4733647.sql
commit;

--DBTicket WM-158837
ALTER TABLE C_ILPN_LOT_MAPPING MODIFY (BATCH_NBR varchar2(25));
commit;



--DBTicket WM-162558
@DBScripts/Product/PLSQL_Objects/AGV/AGV_DBScripts.sql
commit;

--DBTicket WM-164149
@DBScripts/Product/PLSQL_Objects/WM60/WM60DBScripts.sql
commit;

--DBTicket WM-166850#
@DBScripts/Product/PLSQL_Objects/WM61/WM61DBScripts.sql
commit;

-- DBTicket WM-164108 PLSQL
@DBScripts/Product/PLSQL_Objects/C_WM01_ALLOC_TRAILER_POS.sql
commit;

-- DBTicket WM-169870 PLSQL
@DBScripts/Product/PLSQL_Objects/C_WM01_SHORT_CANCEL_UPDATE.sql
commit;

-- DBTicket WM-171812 PLSQL
@DBScripts/Product/PLSQL_Objects/C_WM10_TASK_CREATION_UPDATES.sql
commit;

-- DBTicket WM-171674##
@DBScripts/Product/PLSQL_Objects/WM63.sql
commit;

-- DBTicket WM-171166
@DBScripts/Product/PLSQL_Objects/WM62.sql
@DBScripts/Product/PLSQL_Objects/C_WM26_ASN_RECEIVING_UPDATE.sql
commit;

-- DBTicket WM-175186
@DBScripts/Product/PLSQL_Objects/WM64/WM64.sql
commit;

-- DBTicket WM-176886
@DBScripts/Product/PLSQL_Objects/SF5278224.sql
commit;

